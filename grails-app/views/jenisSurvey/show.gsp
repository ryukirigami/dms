
<%@ page import="com.kombos.customerprofile.JenisSurvey" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'jenisSurvey.label', default: 'Survey')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteJenisSurvey;

$(function(){
	deleteJenisSurvey=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/jenisSurvey/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadJenisSurveyTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-jenisSurvey" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="jenisSurvey"
			class="table table-bordered table-hover">
			<tbody>


				<g:if test="${jenisSurveyInstance?.m131ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m131ID-label" class="property-label"><g:message
					code="jenisSurvey.m131ID.label" default="Kode Survey" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="m131ID-label">
						%{--<ba:editableValue
								bean="${jenisSurveyInstance}" field="m131ID"
								url="${request.contextPath}/JenisSurvey/updatefield" type="text"
								title="Enter m131ID" onsuccess="reloadJenisSurveyTable();" />--}%

								<g:fieldValue bean="${jenisSurveyInstance}" field="m131ID"/>

						</span></td>

				</tr>
				</g:if>

				<g:if test="${jenisSurveyInstance?.m131JenisSurvey}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m131JenisSurvey-label" class="property-label"><g:message
					code="jenisSurvey.m131JenisSurvey.label" default="Jenis Survey" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="m131JenisSurvey-label">
						%{--<ba:editableValue
								bean="${jenisSurveyInstance}" field="m131JenisSurvey"
								url="${request.contextPath}/JenisSurvey/updatefield" type="text"
								title="Enter m131JenisSurvey" onsuccess="reloadJenisSurveyTable();" />--}%

								<g:fieldValue bean="${jenisSurveyInstance}" field="m131JenisSurvey"/>

						</span></td>

				</tr>
				</g:if>
                                
                <g:if test="${jenisSurveyInstance?.m131TipeSurvey}">
                <tr>
                <td class="span2" style="text-align: right;">
                    <span id="m131TipeSurvey-label" class="property-label"><g:message code="jenisSurvey.m131TipeSurvey.label" default="Tipe Survey" />:</span>
                </td>
                <td class="span3">
                    <span class="property-value" aria-labelledby="m131TipeSurvey-label">
                        <g:if test="${jenisSurveyInstance.m131TipeSurvey == '0'}">
                             Customer
                        </g:if>
                        <g:else>
                             Vehicle
                        </g:else>
                    </span>
                </td>

                </tr>
                </g:if>
    
				%{--<g:if test="${jenisSurveyInstance?.staDel}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="staDel-label" class="property-label"><g:message--}%
					%{--code="jenisSurvey.staDel.label" default="Sta Del" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="staDel-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${jenisSurveyInstance}" field="staDel"--}%
								%{--url="${request.contextPath}/JenisSurvey/updatefield" type="text"--}%
								%{--title="Enter staDel" onsuccess="reloadJenisSurveyTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${jenisSurveyInstance}" field="staDel"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%

				<g:if test="${jenisSurveyInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="jenisSurvey.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${jenisSurveyInstance}" field="lastUpdProcess"
								url="${request.contextPath}/JenisSurvey/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadJenisSurveyTable();" />--}%

								<g:fieldValue bean="${jenisSurveyInstance}" field="lastUpdProcess"/>

						</span></td>

				</tr>
				</g:if>

				<g:if test="${jenisSurveyInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="jenisSurvey.dateCreated.label" default="Date Created" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${jenisSurveyInstance}" field="dateCreated"
								url="${request.contextPath}/JenisSurvey/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadJenisSurveyTable();" />--}%

								<g:formatDate date="${jenisSurveyInstance?.dateCreated}" type="datetime" style="MEDIUM" />

						</span></td>

				</tr>
				</g:if>

            <g:if test="${jenisSurveyInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="jenisSurvey.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${jenisSurveyInstance}" field="createdBy"
                                url="${request.contextPath}/JenisSurvey/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadJenisSurveyTable();" />--}%

                        <g:fieldValue bean="${jenisSurveyInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

				<g:if test="${jenisSurveyInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="jenisSurvey.lastUpdated.label" default="Last Updated" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${jenisSurveyInstance}" field="lastUpdated"
								url="${request.contextPath}/JenisSurvey/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadJenisSurveyTable();" />--}%

								<g:formatDate date="${jenisSurveyInstance?.lastUpdated}" type="datetime" style="MEDIUM" />

						</span></td>

				</tr>
				</g:if>

            <g:if test="${jenisSurveyInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="jenisSurvey.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${jenisSurveyInstance}" field="updatedBy"
                                url="${request.contextPath}/JenisSurvey/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadJenisSurveyTable();" />--}%

                        <g:fieldValue bean="${jenisSurveyInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${jenisSurveyInstance?.id}"
					update="[success:'jenisSurvey-form',failure:'jenisSurvey-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteJenisSurvey('${jenisSurveyInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
