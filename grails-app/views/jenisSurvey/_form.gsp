<%@ page import="com.kombos.customerprofile.JenisSurvey" %>

<g:if test="${jenisSurveyInstance?.m131ID}">
<div class="control-group fieldcontain ${hasErrors(bean: jenisSurveyInstance, field: 'm131ID', 'error')} required" onkeypress="return isNumberKey(event)" >
	<label class="control-label" for="m131ID">
		<g:message code="jenisSurvey.m131ID.label" default="Kode Survey" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	${jenisSurveyInstance?.m131ID}
	</div>
</div>
</g:if>

<div class="control-group fieldcontain ${hasErrors(bean: jenisSurveyInstance, field: 'm131JenisSurvey', 'error')} required">
	<label class="control-label" for="m131JenisSurvey">
		<g:message code="jenisSurvey.m131JenisSurvey.label" default="Jenis Survey" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m131JenisSurvey" maxlength="20" required="" value="${jenisSurveyInstance?.m131JenisSurvey}"/>
	</div>
</div>

<g:javascript>
    document.getElementById("m131JenisSurvey").focus();
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: jenisSurveyInstance, field: 'm131TipeSurvey', 'error')} required">
    <label class="control-label" for="m131TipeSurvey">
        <g:message code="jenisSurvey.m131TipeSurvey.label" default="Tipe Survey" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:radioGroup name="m131TipeSurvey"
                      labels="['Customer','Vehicle']"
                      values="[0,1]" value="${jenisSurveyInstance.m131TipeSurvey}">
        <p>${it.radio} ${it.label}</p>
        </g:radioGroup>
	</div>
</div>

%{--<div class="control-group fieldcontain ${hasErrors(bean: jenisSurveyInstance, field: 'staDel', 'error')} required">--}%
	%{--<label class="control-label" for="staDel">--}%
		%{--<g:message code="jenisSurvey.staDel.label" default="Sta Del" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="staDel" maxlength="1" required="" value="${jenisSurveyInstance?.staDel}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: jenisSurveyInstance, field: 'createdBy', 'error')} ">--}%
	%{--<label class="control-label" for="createdBy">--}%
		%{--<g:message code="jenisSurvey.createdBy.label" default="Created By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="createdBy" value="${jenisSurveyInstance?.createdBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: jenisSurveyInstance, field: 'updatedBy', 'error')} ">--}%
	%{--<label class="control-label" for="updatedBy">--}%
		%{--<g:message code="jenisSurvey.updatedBy.label" default="Updated By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="updatedBy" value="${jenisSurveyInstance?.updatedBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: jenisSurveyInstance, field: 'lastUpdProcess', 'error')} ">--}%
	%{--<label class="control-label" for="lastUpdProcess">--}%
		%{--<g:message code="jenisSurvey.lastUpdProcess.label" default="Last Upd Process" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="lastUpdProcess" value="${jenisSurveyInstance?.lastUpdProcess}"/>--}%
	%{--</div>--}%
%{--</div>--}%

