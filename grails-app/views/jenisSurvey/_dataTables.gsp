
<%@ page import="com.kombos.customerprofile.JenisSurvey" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="jenisSurvey_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="jenisSurvey.m131ID.label" default="Kode Survey" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="jenisSurvey.m131JenisSurvey.label" default="Jenis Survey" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="jenisSurvey.m131TipeSurvey.label" default="Tipe Survey" /></div>
            </th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="jenisSurvey.staDel.label" default="Sta Del" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="jenisSurvey.createdBy.label" default="Created By" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="jenisSurvey.updatedBy.label" default="Updated By" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="jenisSurvey.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>

		</tr>
		<tr>


			<th style="border-top: none;padding: 5px;">
				<div id="filter_m131ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m131ID" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_m131JenisSurvey" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_m131JenisSurvey" class="search_init" />
                </div>
            </th>

			<th style="border-top: none;padding: 5px;">

                <div id="filter_m131TipeSurvey" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    %{--<g:select name="search_m131TipeSurvey" from="${surveyTypes}" optionKey="code" optionValue="description" noSelection="['':'All']"/>--}%
                    <select name="search_m131TipeSurvey" id="search_m131TipeSurvey" style="width:100%" onchange="reloadJenisSurveyTable();">
                        <option value="">All</option>
                        <option value="0">Customer</option>
                        <option value="1">Vehicle</option>
                    </select>
                </div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_staDel" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_createdBy" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_updatedBy" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var jenisSurveyTable;
var reloadJenisSurveyTable;
$(function(){

	reloadJenisSurveyTable = function() {
		jenisSurveyTable.fnDraw();
	}

    var recordsJenisSurveyPerPage = [];
    var anJenisSurveySelected;
    var jmlRecJenisSurveyPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	jenisSurveyTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	jenisSurveyTable = $('#jenisSurvey_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsJenisSurvey = $("#jenisSurvey_datatables tbody .row-select");
            var jmlJenisSurveyCek = 0;
            var nRow;
            var idRec;
            rsJenisSurvey.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsJenisSurveyPerPage[idRec]=="1"){
                    jmlJenisSurveyCek = jmlJenisSurveyCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsJenisSurveyPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecJenisSurveyPerPage = rsJenisSurvey.length;
            if(jmlJenisSurveyCek==jmlRecJenisSurveyPerPage && jmlRecJenisSurveyPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m131ID",
	"mDataProp": "m131ID",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"120px",
	"bVisible": true
}

,

{
	"sName": "m131JenisSurvey",
	"mDataProp": "m131JenisSurvey",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"80px",
	"bVisible": true
}

,

{
	"sName": "m131TipeSurvey",
	"mDataProp": "m131TipeSurvey",
	"aTargets": [2],
	"mRender": function ( data, type, row ) {
            if(data=="0"){
                  return "Customer";
             }else
             {
                  return "Vehicle";
             }

        },
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"80px",
	"bVisible": true
}

,

{
	"sName": "staDel",
	"mDataProp": "staDel",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": false
}

,

{
	"sName": "createdBy",
	"mDataProp": "createdBy",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": false
}

,

{
	"sName": "updatedBy",
	"mDataProp": "updatedBy",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": false
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var m131ID = $('#filter_m131ID input').val();
						if(m131ID){
							aoData.push(
									{"name": 'sCriteria_m131ID', "value": m131ID}
							);
						}

						var m131JenisSurvey = $('#filter_m131JenisSurvey input').val();
						if(m131JenisSurvey){
							aoData.push(
									{"name": 'sCriteria_m131JenisSurvey', "value": m131JenisSurvey}
							);
						}

						var m131TipeSurvey = $('#search_m131TipeSurvey').val();
						if(m131TipeSurvey){
							aoData.push(
									{"name": 'sCriteria_m131TipeSurvey', "value": m131TipeSurvey}
							);
						}

						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}

						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}

						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}

						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
	$('.select-all').click(function(e) {

        $("#jenisSurvey_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsJenisSurveyPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsJenisSurveyPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#jenisSurvey_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsJenisSurveyPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anJenisSurveySelected = jenisSurveyTable.$('tr.row_selected');
            if(jmlRecJenisSurveyPerPage == anJenisSurveySelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsJenisSurveyPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>



