
<%@ page import="com.kombos.reception.CustomerIn" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="customerPilihan_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="customerPilihan.t400NomorAntrian.label" default="Nomor Antrian" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="customerPilihan.noPolisi.label" default="Nomor Polisi" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="customerPilihan.namaCustomer.label" default="Nama Customer" /></div>
        </th>

    </tr>
    <tr>


        <th style="border-top: none;padding: 5px;">
            <div id="filter_noAntri" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_noAntri" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_noPol" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_noPol" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_namaCustomer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_namaCustomer" class="search_init" />
            </div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var customerPilihanTable;
var reloadCustomerPilihanTable;
$(function(){

	reloadCustomerPilihanTable = function() {
		customerPilihanTable.fnDraw();
	}

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	customerPilihanTable.fnDraw();
		}
	});

	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	customerPilihanTable = $('#customerPilihan_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesCustomerList")}",
		"aoColumns": [


{
	"sName": "t400NomorAntrian",
	"mDataProp": "t400NomorAntrian",
	"aTargets": [1],
		"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">'+data;
	},
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "fullNoPol",
	"mDataProp": "fullNoPol",
	"aTargets": [1],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "namaCustomer",
	"mDataProp": "namaCustomer",
	"aTargets": [1],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var noAntri = $('#filter_noAntri input').val();
						if(noAntri){
							aoData.push(
									{"name": 'sCriteria_noAntri', "value": noAntri}
							);
						}

						var noPol = $('#filter_noPol input').val();
						if(noPol){
							aoData.push(
									{"name": 'sCriteria_noPol', "value": noPol}
							);
						}

						var customer = $('#filter_namaCustomer input').val();
						if(customer){
							aoData.push(
									{"name": 'sCriteria_customer', "value": customer}
							);
						}

                        aoData.push(
                                {"name": 'kategori', "value": $("#idKategori").text()}
                        );

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>



