
<%@ page import="com.kombos.baseapp.sec.shiro.User; com.kombos.reception.CustomerIn" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'customerIn.label', default: 'CustomerIn')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
            var i = 0 ;
            function getReceptionih(){
                var noAntri = $("#labelCurrentNoAntrian").text();
                var find = ' ';
                var re = new RegExp(find, 'g');
                var noAntriBaru = noAntri.indexOf(" ")>-1 ? noAntri.replace(re,"") : noAntri
                if(!noAntriBaru || noAntriBaru==null || noAntriBaru=="null" || noAntriBaru=="" || noAntriBaru=="TidakAdaAntrian"){
                    alert("Tidak ada customer yang di panggil");
                    return
                }
                if(confirm("Apakah Anda yakin akan melakukan Reception ?")){
                    window.location.href='#/Reception';
                    loadPath('reception?noAntri='+noAntriBaru);
                }
            }

            function callNextCustomer(){
                i=0;
                var idLoket = $("#labelCurrentNoAntrian").text();
                var id = $("#idLoketPilih").val();
                var idKategori = $("#idKategori").text();
                var urlz = window.location.href;
                $.ajax({
                    url:'${request.contextPath}/inputNomorLoket/callCustomer',
                    type: "POST", // Always use POST when deleting data
                    data : {idLoket : idLoket,id : id, idK : idKategori,urlz : urlz},
                    success : function(data){
                        $("#labelCurrentNoAntrian").text(data.noAntri);
                        reloadCustomerInTable();
                        if(data.ada==true){
                            makeSound(data.urls);
                        }
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        alert('Internal Server Error');
                    }
                });
            }

            var longDelay = [0,2000,800,800,800,800,800]
            function makeSound(urls){
                setTimeout(function () {
                    var audio = new Audio(urls[i].replace("null",""));
                    audio.play();
                    i++;
                    if (i < urls.length) {
                        makeSound(urls);
                    }
                }, longDelay[i])
            }

            function reCall(){
                i=0;
                var idLoket = $("#labelCurrentNoAntrian").text();
                var find = ' ';
                var re = new RegExp(find, 'g');
                var noAntriBaru = idLoket.indexOf(" ")>-1 ? idLoket.replace(re,"") : idLoket
                var urlz = window.location.href;
                if(!noAntriBaru || noAntriBaru==null || noAntriBaru=="null" || noAntriBaru=="" || noAntriBaru=="TidakAdaAntrian"){
                    alert("Tidak ada customer yang di panggil");
                    return
                }
                $.ajax({
                    url:'${request.contextPath}/inputNomorLoket/reCall',
                    type: "POST", // Always use POST when deleting data
                    data : {idLoket : noAntriBaru,urlz : urlz},
                    success : function(data){
                        $("#labelCurrentNoAntrian").text(data.noAntri);
                        toastr.success("Panggil "+data.noAntri);
                        if(data.ada==true){
                             makeSound(data.urls);
                        }
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        alert('Internal Server Error');
                    }
                });
            }


            function skip(){
                var idLoket = $("#labelCurrentNoAntrian").text();
                alert(idLoket)
                $.ajax({
                    url:'${request.contextPath}/inputNomorLoket/skip',
                    type: "POST", // Always use POST when deleting data
                    data : {idLoket : idLoket},
                    success : function(data){
                        $("#labelCurrentNoAntrian").text(data);
                        toastr.success("Panggil "+data);
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        alert('Internal Server Error');
                    }
                });

            }


            function callCustomerPilihan(){
                $("#customerPilihanContent").empty();
                $.ajax({
                    type:'POST',
                    url:'${request.contextPath}/inputNomorLoket/listCustomer',
                    success:function(data,textStatus){
                            $("#customerPilihanContent").html(data);
                            $("#customerPilihanModal").modal({
                                "backdrop" : "dynamic",
                                "keyboard" : true,
                                "show" : true
                            }).css({'width': '700px','margin-left': function () {return -($(this).width() / 2);}});

                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){},
                    complete:function(XMLHttpRequest,textStatus){
                        $('#spinner').fadeOut();
                    }
                });
            }

        </g:javascript>

	</head>
	<body>
	<div class="navbar box-header no-border">
	</div>
	<div class="box">
		<div class="span12" id="customerIn-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <table  style="width: 80%">
                <tr>
                    <td style="width: 50%">
                        <table class="table table-bordered" >
                        <tr>
                            <td >
                                <label class="control-label" >
                                    Nama SA
                                </label>
                            </td>
                            <td style="width:60%;">
                                <div class="controls">
                                    <label class="control-label" >
           ${out.println(User.findByUsernameAndStaDel(org.apache.shiro.SecurityUtils.subject.principal.toString(),"0")?.fullname)}
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label" >
                                    Nomor Loket
                                </label>
                            </td>
                            <td>
                                <div class="controls">
                                    ${loketId}
                                    <g:hiddenField name="idLoketPilih" id="idLoketPilih" value="${idLoket}" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label" >
                                    Kategori
                                </label>
                            </td>
                            <td>
                                <div class="controls">
                                    <span name="idKategori" id="idKategori" >${kategori=='1'?"GR":"BP"}</span>
                                </div>
                            </td>
                        </tr>
                    </table>
                    </td>
                </tr>
            </table>



			
			<div style="width:40%; text-align: right;">

				<a href="javascript:void(0);" onclick="reloadCustomerInTable();" class="btn cancel">Refresh</a>

			</div>
				
			<g:render template="dataTables" />
			
			<table style="width:100%;">
				<tr>
					<td style="width:40%;">
						<a onclick="callNextCustomer();" class="btn cancel">Panggil Customer Selanjutnya</a>
                        <a onclick="reCall();" class="btn cancel">Panggil Lagi <i class="icon-bullhorn"></i> </a>
						<a onclick="callCustomerPilihan();" class="btn cancel">Panggil Customer Pilihan</a>
					</td>
					<td style="width:30%;">
						<table>
							<tr>
								<td>
								<table class="table table-bordered">
									<tr>
										<td>
											Nomor Sedang Dipanggil
										</td>
									</tr>
									<tr>
										<td>
                                            <input type="hidden" id="currentNoAntrian" name="currentNoAntrian" value="${currentNoAntrian}" />
											<span id="labelCurrentNoAntrian" style="font-size: 30px; height: 100px;">${currentNoAntrian ? currentNoAntrian : " "}</span>
										</td>
									</tr>
								</table>
								</td>
								<td>
									%{--<a class="btn cancel" onclick="window.location.href = '#/reception';" style="font-size: 30px; height: 50px;line-height: 50px;">Reception</a>--}%
									<a class="btn cancel" name="btnReception" id="btnReception" onclick="getReceptionih();" style="font-size: 30px; height: 50px;line-height: 50px;">Reception</a>
                                </td>
							</tr>
						</table>		
					</td>
					<td style="width:30%;">
						%{--Complaint--}%
					</td>
				</tr>
			</table>
		</div>
		<div class="span7" id="customerIn-form" style="display: none;"></div>
	</div>
    <div id="customerPilihanModal" class="modal fade">
        <div class="modal-dialog" style="width: 700px;">
            <div class="modal-content" style="width: 700px;">
                <!-- dialog body -->

                <div class="modal-body" style="max-height: 500px;">
                    <div id="customerPilihanContent"/>
                    <div class="iu-content"></div>

                </div>
            </div>
        </div>
    </div>
    %{--<td class="span3"><span class="property-value"--}%
                            %{--aria-labelledby="audioPath-label">--}%
        %{--<audio id="audio1" controls>--}%
            %{--<source src="${resource(dir: 'audio', file: "${suara}")}" type="audio/mpeg">--}%
            %{--Your browser does not support this audio format.--}%
        %{--</audio>--}%
        %{--<br/>--}%
        %{--<g:link action="downloadAudio" params="[audioPath: ${suara}"> ${suara} </g:link>--}%

    %{--</span></td>--}%
</body>
</html>
