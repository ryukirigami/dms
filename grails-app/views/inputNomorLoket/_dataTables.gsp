
<%@ page import="com.kombos.reception.CustomerIn" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="customerIn_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerIn.t400NomorAntrian.label" default="Nomor Antrian" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerIn.noPolisi.label" default="Nomor Polisi" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerIn.t400TglCetakNoAntrian.label" default="Jam Datang" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerIn.t301TglJamRencana.label" default="Janji Datang" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerIn.m404NoLoket.label" default="Nomor Loket" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerIn.t400TglJamReception.label" default="Jam Reception"/></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerIn.t400xNamaUserIn.label" default="Jam Produksi" /></div>
			</th>
		</tr>
	</thead>
</table>

<g:javascript>
var customerInTable;
var reloadCustomerInTable;
$(function(){
	
	reloadCustomerInTable = function() {
		customerInTable.fnDraw();
	}

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	customerInTable.fnDraw();
		}
	});

	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	customerInTable = $('#customerIn_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [


{
	"sName": "t400NomorAntrian",
	"mDataProp": "t400NomorAntrian",
	"aTargets": [1],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "fullNoPol",
	"mDataProp": "fullNoPol",
	"aTargets": [1],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "t400TglCetakNoAntrian",
	"mDataProp": "t400TglCetakNoAntrian",
	"aTargets": [1],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "t301TglJamRencana",
	"mDataProp": "t301TglJamRencana",
	"aTargets": [1],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "m404NoLoket",
	"mDataProp": "m404NoLoket",
	"aTargets": [1],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "t400TglJamReception",
	"mDataProp": "t400TglJamReception",
	"aTargets": [1],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "t451TglJamPlan",
	"mDataProp": "t451TglJamPlan",
	"aTargets": [1],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        aoData.push(
                                {"name": 'kategori', "value": $("#idKategori").text()}
                        );

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
