
<%@ page import="com.kombos.baseapp.sec.shiro.User; com.kombos.administrasi.Loket; com.kombos.reception.Appointment" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'appointment.label', default: 'Appointment')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
		var getListCustomerIn;
		$(function(){ 
			getListCustomerIn = function(){
				var loketId = $("#loket").val();
				var kategori = "";
				var selected = $("input[type='radio'][name='kategori']:checked");
				if (selected.length > 0) {
					kategori = selected.val();
				}	
				if(loketId=="" || loketId==null){
                    alert("Data loket masih kosong");
                    return
                }
				loadPath('inputNomorLoket/list?loketId='+loketId+'&kategori='+kategori);
			}
		});
		</g:javascript>
	</head>
	<body>
	<form>
        <div style="text-align: center;" class="box">
            <div class="navbar box-header no-border" style=" display: inline-block; margin-left: auto; margin-right: auto;">
                <span class="pull-left">Input Nomor Loket</span>
            </div><br>
            <div id="inputNomorLoket" class="box" style="width:50%; display: inline-block; margin-left: auto; margin-right: auto;">
                <table class="table table-bordered">
                    <tr>
                        <td style="width:40%;">
                            <label class="control-label" style="font-size: 20px; height: 30px; text-align: left;line-height: 30px;">
                            Nama SA
                            </label>
                        </td>
                        <td style="width:60%;">
                            <div class="controls">
                                <label class="control-label" style="font-size: 20px; height: 30px; text-align: left;line-height: 30px;">
                                ${out.println(User.findByUsernameAndStaDel(org.apache.shiro.SecurityUtils.subject.principal.toString(),"0")?.fullname)}
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label" style="font-size: 20px; height: 30px; text-align: left;line-height: 30px;">
                            Nomor Loket
                            </label>
                        </td>
                        <td>
                            <div class="controls">
                                <g:select style="font-size: 20px; height: 30px; text-align: left;line-height: 30px;" id="loket" name="loket.id" from="${Loket.createCriteria().list(){eq("companyDealer",session?.userCompanyDealer);order("m404NoLoket", "asc")}}" optionValue="${{it.m404NoLoket}}" optionKey="id" required="" class="many-to-one"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label" style="font-size: 20px; height: 30px; text-align: left;line-height: 30px;">
                            Kategori
                            <!--span class="required-indicator">*</span-->
                            </label>
                        </td>
                        <td>
                            <div class="controls">
                                <g:radio style="font-size: 20px; height: 30px; text-align: left;line-height: 30px;" name="kategori" value="1" checked="true"/>GR
                                <g:radio style="font-size: 20px; height: 30px; text-align: left;line-height: 30px;" name="kategori" value="2" />BP
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="controls">
                                <!--a class="btn cancel" href="javascript:void(0);" onclick="javascript:loadPath('inputNomorLoket/list?loket.id=1&kategori=1')" style="font-size: 20px; height: 50px;line-height: 50px; width:97%;">OK</a-->
                                <a class="btn cancel" href="javascript:void(0);" onclick="javascript:getListCustomerIn()" style="font-size: 20px; height: 30px;line-height: 30px; width:97%;">OK</a>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
	</form>
    </body>
</html>
