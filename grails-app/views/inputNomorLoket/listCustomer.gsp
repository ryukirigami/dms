<%--
  Created by IntelliJ IDEA.
  User: Ahmad Fawaz
  Date: 19/01/15
  Time: 0:47
--%>


<%@ page import="com.kombos.reception.CustomerIn" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="customer.pilihan.label" default="Panggil Customer Pilihan" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
        function callSpecialCustomer(){
            i=0;
            var recordsToDelete = []
            $("#listCustomer-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    recordsToDelete.push(id);
                }
            });
            if(recordsToDelete.length<1){
                alert("Anda belum memilih data");
                return
            }
            if(recordsToDelete.length>1){
                alert("Anda hanya dapat memilih 1 data");
                return
            }
            var idLoket = $("#idLoketPilih").val();
            var urlz = window.location.href;
            $.ajax({
    		url:'${request.contextPath}/inputNomorLoket/callSpecialCustomer',
    		type: "POST", // Always use POST when deleting data
    		data: { id: recordsToDelete[0], idLoket : idLoket ,urlz : urlz},
    		success: function(data) {
    		    toastr.success("Add success");
                $("#labelCurrentNoAntrian").text(data.noAntri);
                reloadCustomerInTable();
                reloadCustomerPilihanTable();
                makeSound(data.urls);
    		}
		});

        }

    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
</div>
<div class="box">
    <div class="span12" id="listCustomer-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <g:render template="dataTablesCustomer" />
        <br/>
        <div class="pull-right">
            <a onclick="callSpecialCustomer();" class="btn cancel">Panggil Customer</a>
            <a data-dismiss="modal" class="btn cancel">Close</a>
        </div>
    </div>
    <div class="span7" id="listCustomer-form" style="display: none;"></div>
</div>
</body>
</html>