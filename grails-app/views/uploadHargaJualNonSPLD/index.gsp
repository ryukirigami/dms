<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'operation.label', default: 'Upload Harga Jual Non SPLD')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <r:require modules="highslide" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/swfobject/2.1/swfobject.js"></script>

    <g:javascript>

        <g:if test="${jmlhDataError && jmlhDataError>0}">
            $('#save').attr("disabled", true);
        </g:if>

        var saveForm;
        $(function(){

            function progress(e){
                if(e.lengthComputable){
                    //kalo mau pake progress bar
                    //$('progress').attr({value:e.loaded,max:e.total});
                }
            }

            saveForm = function() {

        <g:if test="${jsonData}">
            var sendData = ${jsonData};
                        $.ajax({
                            url:'${request.getContextPath()}/uploadHargaJualNonSPLD/upload',
                            type: 'POST',
                            xhr: function() {
                                var myXhr = $.ajaxSettings.xhr();
                                if(myXhr.upload){
                                    myXhr.upload.addEventListener('progress',progress, false);
                                }
                                return myXhr;
                            },
                            success: function (res) {
                                $('#hargaJualNonSPLDTable').empty();
                                $('#hargaJualNonSPLDTable').append(res);
                            },
                            error: function (data, status, e){
                                alert(e);
                            },
                            complete: function(xhr, status) {

                            },
                            data: JSON.stringify(sendData),
                            contentType: "application/json; charset=utf-8",
                            traditional: true,
                            cache: false
                        });
        </g:if>
        <g:else>
            alert('No File Selected');
        </g:else>
        }


    });

    </g:javascript>

</head>
<body>
<div id="hargaJualNonSPLDTable">
    <div class="navbar box-header no-border">
        <span class="pull-left">
            <g:message code="default.list.label" args="[entityName]" />
        </span>
        <ul class="nav pull-right">
            <li></li>
            <li></li>
            <li class="separator"></li>
        </ul>
    </div>
    <div class="box">
        <div class="span12" id="operation-table">
            <fieldset>
                <form id="uploadHargaJualNonSPLD-save" class="form-vertical" method="post" action="${request.getContextPath()}/uploadHargaJualNonSPLD/save" onsubmit="saveForm();return false">
                    <table>
                        <tr>
                            <td>
                                <a href="${request.getContextPath()}/formatFileUpload/UploadHargaJualNonSPLD.xls" >* File Example Upload</a>
                                <br/><br/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <fieldset class="form">
                                    <g:render template="form"/>
                                </fieldset>
                            </td>
                            <td>
                                <fieldset class="buttons controls">

                                    <g:field type="button" onclick="submitForm();" class="btn btn-primary create" name="view" id="view" value="${message(code: 'default.button.upload.label', default: 'Upload')}"/>
                                    <g:submitButton class="btn btn-primary create"  onclick="return confirm('${message(code: 'default.button.save.confirm.message', default: 'Anda yakin data akan disimpan?')}');" name="save" id="save" value="${message(code: 'default.button.view.label', default: 'Save')}" />

                                    &nbsp;&nbsp;&nbsp;


                                    <g:if test="${flash.message}">
                                        ${flash.message}
                                    </g:if>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </form>
                <g:javascript>
                        var submitForm;
                        $(function(){

                            function progress(e){
                                if(e.lengthComputable){
                                    //kalo mau pake progress bar
                                    //$('progress').attr({value:e.loaded,max:e.total});
                                }
                            }

                            submitForm = function() {

                                var form = new FormData($('#uploadHargaJualNonSPLD-save')[0]);

                                $.ajax({
                                    url:'${request.getContextPath()}/uploadHargaJualNonSPLD/view',
                                    type: 'POST',
                                    xhr: function() {
                                        var myXhr = $.ajaxSettings.xhr();
                                        if(myXhr.upload){
                                            myXhr.upload.addEventListener('progress',progress, false);
                                        }
                                        return myXhr;
                                    },
                                    success: function (res) {
                                        $('#hargaJualNonSPLDTable').empty();
                                        $('#hargaJualNonSPLDTable').append(res);
                                        //reloadHargaJualNonSPLDTable();
                                    },
                                    //add error handler for when a error occurs if you want!
                                    error: function (data, status, e){
                                        alert(e);
                                    },
                                    complete: function(xhr, status) {

                                    },
                                    data: form,
                                    cache: false,
                                    contentType: false,
                                    processData: false
                                });

                            }
                        });
                </g:javascript>
            </fieldset>
            <br>

            <table  id="uploadHargaJualNonSPLDTable" class="display table table-striped table-bordered table-hover dataTable" width="100%" cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px; width: 1284px;">
                <tr>
                    <th>
                        Tanggal Berlaku
                    </th>
                    <th>
                        Kode Goods
                    </th>
                    <th>
                        Persen kenaikan harga
                    </th>
                </tr>
                <g:if test="${htmlData}">
                    ${htmlData}
                </g:if>
                <g:else>
                    <tr class="odd">
                        <td class="dataTables_empty" valign="top" colspan="9">No data available in table</td>
                    </tr>
                </g:else>
            </table>
        </div>
    </div>
</div>
</body>

<g:javascript>
</g:javascript>
</html>
