<%@ page import="com.kombos.customerprofile.FA" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'FA.label', default: 'FA')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <r:require modules="baseapplayout"/>
    <style>
    .form-horizontal .control-label {
        float: left;
        width: 250px;
        padding-top: 5px;
        text-align: right;
    }
    </style>
    <g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
                                   onLoading="jQuery('#spinner').fadeIn(1);"
                                   onSuccess="loadForm(data, textStatus);"
                                   onComplete="jQuery('#spinner').fadeOut();"/>
        break;
    case '_DELETE_' :
        bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/FA/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/FA/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#FA-form').empty();
    	$('#FA-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#FA-table").hasClass("span12")){
   			$("#FA-table").toggleClass("span12 span5");
        }
        $("#FA-form").css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
   		if($("#FA-table").hasClass("span5")){
   			$("#FA-table").toggleClass("span5 span12");
   		}
        $("#FA-form").css("display","none");
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#FA-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/FA/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		success: function(data){
    		    if(data=="gagal"){
                    alert('Data sedang digunakan di tabel lain, sehingga tidak bisa dihapus.')
    		    }
                reloadFATable();
    		}
//    		,
//    		complete: function(xhr, status) {
//        		reloadFATable();
//    		}
		});
		
   	}

});
    </g:javascript>
</head>

<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]"/></span>
    <ul class="nav pull-right">
        <li><a class="pull-right box-action" href="#"
               style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
                    class="icon-plus"></i>&nbsp;&nbsp;
        </a></li>
        <li><a class="pull-right box-action" href="#"
               style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
                    class="icon-remove"></i>&nbsp;&nbsp;
        </a></li>
        <li class="separator"></li>
    </ul>
</div>

<div class="box">
    <div class="span12" id="FA-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <div class="buttons controls">
            <g:if test="${isFrom=="fa"}">
                <g:submitButton class="btn btn-primary create" onclick="window.location.href = '#/partsJobFieldAction'" name="inputPartsJob" id="inputPartsJob" value="Input Parts & Job yang Terlibat" />
                <g:submitButton class="btn btn-primary create" onclick="window.location.href = '#/addDetailFieldAction'" name="FADetail" id="FADetail" value="Add/View FA Detail" />
                <g:submitButton class="btn btn-primary create" onclick="window.location.href = '#/uploaDetailFieldAction'" name="UploadFADetail" id="UploadFADetail" value="Upload/View FA Detail" />
            </g:if>
        </div>
        <g:render template="dataTables"/>
    </div>

    <div class="span7" id="FA-form" style="display: none;"></div>
</div>
</body>
</html>
