<%@ page import="com.kombos.customerprofile.FA" %>



<div class="control-group fieldcontain ${hasErrors(bean: FAInstance, field: 'm185TanggalFA', 'error')} required">
    <label class="control-label" for="m185TanggalFA">
        <g:message code="FA.m185TanggalFA.label" default="M185 Tanggal FA"/>
        <span class="required-indicator">&nbsp;&nbsp;*&nbsp;&nbsp;</span>
    </label>

    <div class="controls">
        <ba:datePicker name="m185TanggalFA" required="true" precision="day" value="${FAInstance?.m185TanggalFA}"
                       format="dd/mm/yyyy"/>
    </div>
</div>
<g:javascript>
    document.getElementById("m185TanggalFA").focus();
</g:javascript>
%{--<div class="control-group fieldcontain ${hasErrors(bean: FAInstance, field: 'jenisFA', 'error')} required">--}%
%{--<label class="control-label" for="jenisFA">--}%
%{--<g:message code="FA.jenisFA.label" default="Jenis FA"/>--}%
%{--<span class="required-indicator">&nbsp;&nbsp;*&nbsp;&nbsp;</span>--}%
%{--</label>--}%

%{--<div class="controls">--}%
%{--<g:select id="jenisFA" name="jenisFA.id" from="${com.kombos.maintable.JenisFA.list()}" optionKey="id"--}%
%{--value="${FAInstance?.jenisFA?.id}" class="many-to-one"/>--}%
%{--</div>--}%
%{--</div>--}%

<div class="control-group fieldcontain ${hasErrors(bean: FAInstance, field: 'm185NomorSurat', 'error')} required">
    <label class="control-label" for="m185NomorSurat">
        <g:message code="FA.m185NomorSurat.label" default="M185 Nomor Surat"/>
        <span class="required-indicator">&nbsp;&nbsp;*&nbsp;&nbsp;</span>
    </label>

    <div class="controls">
        <g:textField name="m185NomorSurat" maxlength="50" required="" value="${FAInstance?.m185NomorSurat}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: FAInstance, field: 'm185NamaFA', 'error')} required">
    <label class="control-label" for="m185NamaFA">
        <g:message code="FA.m185NamaFA.label" default="M185 Nama FA"/>
        <span class="required-indicator">&nbsp;&nbsp;*&nbsp;&nbsp;</span>
    </label>

    <div class="controls">
        <g:textField name="m185NamaFA" maxlength="50" required="" value="${FAInstance?.m185NamaFA}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: FAInstance, field: 'm185ThnBlnRakit1', 'error')} required">
    <label class="control-label" for="m185ThnBlnRakit1">
        <g:message code="FA.m185ThnBlnRakit1.label" default="M185 Thn Bln Rakit1"/>
        <span class="required-indicator">&nbsp;&nbsp;*&nbsp;&nbsp;</span>
    </label>

    <div class="controls">
        <g:textField name="m185ThnBlnRakit1" size="4" maxlength="4" required="" style="width:50px"
                     onkeyup="checkNumber(this);"
                     value="${FAInstance?.m185ThnBlnRakit1?.toString()?.substring(0, 4)}"/> /
        <g:textField name="m185ThnBlnRakit11" size="2" maxlength="2" required="" style="width:30px"
                     onkeyup="checkNumber(this);"
                     value="${FAInstance?.m185ThnBlnRakit1?.toString()?.substring(4)}"/>
        &nbsp;4 digit tahun / 2 digit bulan
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: FAInstance, field: 'm185ThnBlnRakit2', 'error')} required">
    <label class="control-label" for="m185ThnBlnRakit2">
        <g:message code="FA.m185ThnBlnRakit2.label" default="M185 Thn Bln Rakit2"/>
        <span class="required-indicator">&nbsp;&nbsp;*&nbsp;&nbsp;</span>
    </label>

    <div class="controls">
        <g:textField name="m185ThnBlnRakit2" size="4" maxlength="4" required="" style="width:50px"
                     onkeyup="checkNumber(this);"
                     value="${FAInstance?.m185ThnBlnRakit2?.toString()?.substring(0, 4)}"/> /
        <g:textField name="m185ThnBlnRakit21" size="2" maxlength="2" required="" style="width:30px"
                     onkeyup="checkNumber(this);"
                     value="${FAInstance?.m185ThnBlnRakit2?.toString()?.substring(4)}"/>
        &nbsp;4 digit tahun / 2 digit bulan
    </div>
</div>

