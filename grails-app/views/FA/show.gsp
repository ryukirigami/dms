

<%@ page import="com.kombos.customerprofile.FA" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'FA.label', default: 'FA')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteFA;

$(function(){ 
	deleteFA=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/FA/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				if(data=="gagal"){
                    alert('Data sedang digunakan di tabel lain, sehingga tidak bisa dihapus.')
   				}else{
                    reloadFATable();
                    expandTableLayout();
   				}
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-FA" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="FA"
			class="table table-bordered table-hover">
			<tbody>


				<g:if test="${FAInstance?.m185TanggalFA}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m185TanggalFA-label" class="property-label"><g:message
					code="FA.m185TanggalFA.label" default="M185 Tanggal FA" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m185TanggalFA-label">
						%{--<ba:editableValue
								bean="${FAInstance}" field="m185TanggalFA"
								url="${request.contextPath}/FA/updatefield" type="text"
								title="Enter m185TanggalFA" onsuccess="reloadFATable();" />--}%
							
								<g:formatDate date="${FAInstance?.m185TanggalFA}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${FAInstance?.jenisFA}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="jenisFA-label" class="property-label"><g:message
					code="FA.jenisFA.label" default="Jenis FA" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="jenisFA-label">
						%{--<ba:editableValue
								bean="${FAInstance}" field="jenisFA"
								url="${request.contextPath}/FA/updatefield" type="text"
								title="Enter jenisFA" onsuccess="reloadFATable();" />--}%
							
								<g:link controller="jenisFA" action="show" id="${FAInstance?.jenisFA?.id}">${FAInstance?.jenisFA?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${FAInstance?.m185NomorSurat}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m185NomorSurat-label" class="property-label"><g:message
					code="FA.m185NomorSurat.label" default="M185 Nomor Surat" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m185NomorSurat-label">
						%{--<ba:editableValue
								bean="${FAInstance}" field="m185NomorSurat"
								url="${request.contextPath}/FA/updatefield" type="text"
								title="Enter m185NomorSurat" onsuccess="reloadFATable();" />--}%
							
								<g:fieldValue bean="${FAInstance}" field="m185NomorSurat"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${FAInstance?.m185NamaFA}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m185NamaFA-label" class="property-label"><g:message
					code="FA.m185NamaFA.label" default="M185 Nama FA" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m185NamaFA-label">
						%{--<ba:editableValue
								bean="${FAInstance}" field="m185NamaFA"
								url="${request.contextPath}/FA/updatefield" type="text"
								title="Enter m185NamaFA" onsuccess="reloadFATable();" />--}%
							
								<g:fieldValue bean="${FAInstance}" field="m185NamaFA"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${FAInstance?.m185ThnBlnRakit1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m185ThnBlnRakit1-label" class="property-label"><g:message
					code="FA.m185ThnBlnRakit1.label" default="M185 Thn Bln Rakit1" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m185ThnBlnRakit1-label">
						%{--<ba:editableValue
								bean="${FAInstance}" field="m185ThnBlnRakit1"
								url="${request.contextPath}/FA/updatefield" type="text"
								title="Enter m185ThnBlnRakit1" onsuccess="reloadFATable();" />--}%
							
								<g:fieldValue bean="${FAInstance}" field="m185ThnBlnRakit1"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${FAInstance?.m185ThnBlnRakit2}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m185ThnBlnRakit2-label" class="property-label"><g:message
					code="FA.m185ThnBlnRakit2.label" default="M185 Thn Bln Rakit2" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m185ThnBlnRakit2-label">
						%{--<ba:editableValue
								bean="${FAInstance}" field="m185ThnBlnRakit2"
								url="${request.contextPath}/FA/updatefield" type="text"
								title="Enter m185ThnBlnRakit2" onsuccess="reloadFATable();" />--}%
							
								<g:fieldValue bean="${FAInstance}" field="m185ThnBlnRakit2"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
            <g:if test="${FAInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="FA.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">

                        <g:formatDate date="${FAInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${FAInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="FA.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">

                        <g:fieldValue bean="${FAInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${FAInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="FA.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">

                        <g:fieldValue bean="${FAInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>


            <g:if test="${FAInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="FA.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">

                        <g:formatDate date="${FAInstance?.lastUpdated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${FAInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="FA.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">

                        <g:fieldValue bean="${FAInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${FAInstance?.id}"
					update="[success:'FA-form',failure:'FA-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteFA('${FAInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
