
<%@ page import="com.kombos.customerprofile.FA" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="FA_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>



			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="FA.m185TanggalFA.label" default="M185 Tanggal FA" /></div>
			</th>




			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="FA.m185NomorSurat.label" default="M185 Nomor Surat" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;width: 50%;">
				<div><g:message code="FA.m185DeskripsiFA.label" default="Deskripsi Field Action" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="FA.m185ThnBlnRakit1.label" default="M185 Thn Bln Rakit1" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="FA.m185ThnBlnRakit2.label" default="M185 Thn Bln Rakit2" /></div>
			</th>



		
		</tr>
		<tr>
		
	


			<th style="border-top: none;padding: 5px;">
				<div id="filter_m185TanggalFA" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m185TanggalFA" value="date.struct">
					<input type="hidden" name="search_m185TanggalFA_day" id="search_m185TanggalFA_day" value="">
					<input type="hidden" name="search_m185TanggalFA_month" id="search_m185TanggalFA_month" value="">
					<input type="hidden" name="search_m185TanggalFA_year" id="search_m185TanggalFA_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m185TanggalFA_dp" value="" id="search_m185TanggalFA" class="search_init">
				</div>
			</th>

	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m185NomorSurat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m185NomorSurat" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;width: 50%;">
				<div id="filter_m185NamaFA" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m185NamaFA" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m185ThnBlnRakit1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" onkeyup="checkNumber(this);" name="search_m185ThnBlnRakit1" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m185ThnBlnRakit2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" onkeyup="checkNumber(this);" name="search_m185ThnBlnRakit2" class="search_init" />
				</div>
			</th>
	


		</tr>
	</thead>
</table>

<g:javascript>
var FATable;
var reloadFATable;
$(function(){
	
	reloadFATable = function() {
		FATable.fnDraw();
	}

    var recordsFAPerPage = [];//new Array();
    var anFASelected;
    var jmlRecFAPerPage=0;
    var id;

	$('#search_m185TanggalFA').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m185TanggalFA_day').val(newDate.getDate());
			$('#search_m185TanggalFA_month').val(newDate.getMonth()+1);
			$('#search_m185TanggalFA_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			FATable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	FATable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	FATable = $('#FA_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
				"fnDrawCallback": function () {
            var rsFA = $("#FA_datatables tbody .row-select");
            var jmlFACek = 0;
            var nRow;
            var idRec;
            rsFA.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsFAPerPage[idRec]=="1"){
                    jmlFACek = jmlFACek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsFAPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecFAPerPage = rsFA.length;
            if(jmlFACek==jmlRecFAPerPage && jmlRecFAPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m185TanggalFA",
	"mDataProp": "m185TanggalFA",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m185NomorSurat",
	"mDataProp": "m185NomorSurat",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m185NamaFA",
	"mDataProp": "m185NamaFA",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m185ThnBlnRakit1",
	"mDataProp": "m185ThnBlnRakit1",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m185ThnBlnRakit2",
	"mDataProp": "m185ThnBlnRakit2",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m185ID = $('#filter_m185ID input').val();
						if(m185ID){
							aoData.push(
									{"name": 'sCriteria_m185ID', "value": m185ID}
							);
						}

						var m185TanggalFA = $('#search_m185TanggalFA').val();
						var m185TanggalFADay = $('#search_m185TanggalFA_day').val();
						var m185TanggalFAMonth = $('#search_m185TanggalFA_month').val();
						var m185TanggalFAYear = $('#search_m185TanggalFA_year').val();
						
						if(m185TanggalFA){
							aoData.push(
									{"name": 'sCriteria_m185TanggalFA', "value": "date.struct"},
									{"name": 'sCriteria_m185TanggalFA_dp', "value": m185TanggalFA},
									{"name": 'sCriteria_m185TanggalFA_day', "value": m185TanggalFADay},
									{"name": 'sCriteria_m185TanggalFA_month', "value": m185TanggalFAMonth},
									{"name": 'sCriteria_m185TanggalFA_year', "value": m185TanggalFAYear}
							);
						}
	
						var jenisFA = $('#filter_jenisFA input').val();
						if(jenisFA){
							aoData.push(
									{"name": 'sCriteria_jenisFA', "value": jenisFA}
							);
						}
	
						var m185NomorSurat = $('#filter_m185NomorSurat input').val();
						if(m185NomorSurat){
							aoData.push(
									{"name": 'sCriteria_m185NomorSurat', "value": m185NomorSurat}
							);
						}
	
						var m185NamaFA = $('#filter_m185NamaFA input').val();
						if(m185NamaFA){
							aoData.push(
									{"name": 'sCriteria_m185NamaFA', "value": m185NamaFA}
							);
						}
	
						var m185ThnBlnRakit1 = $('#filter_m185ThnBlnRakit1 input').val();
						if(m185ThnBlnRakit1){
							aoData.push(
									{"name": 'sCriteria_m185ThnBlnRakit1', "value": m185ThnBlnRakit1}
							);
						}
	
						var m185ThnBlnRakit2 = $('#filter_m185ThnBlnRakit2 input').val();
						if(m185ThnBlnRakit2){
							aoData.push(
									{"name": 'sCriteria_m185ThnBlnRakit2', "value": m185ThnBlnRakit2}
							);
						}
	
						var m185xNamaUser = $('#filter_m185xNamaUser input').val();
						if(m185xNamaUser){
							aoData.push(
									{"name": 'sCriteria_m185xNamaUser', "value": m185xNamaUser}
							);
						}
	
						var m185xNamaDivisi = $('#filter_m185xNamaDivisi input').val();
						if(m185xNamaDivisi){
							aoData.push(
									{"name": 'sCriteria_m185xNamaDivisi', "value": m185xNamaDivisi}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
		$('.select-all').click(function(e) {

        $("#FA_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsFAPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsFAPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#FA_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsFAPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anFASelected = FATable.$('tr.row_selected');
            if(jmlRecFAPerPage == anFASelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsFAPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
