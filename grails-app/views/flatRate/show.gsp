

<%@ page import="com.kombos.administrasi.FlatRate" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'flatRate.label', default: 'Input Flat Rate')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteFlatRate;

$(function(){ 
	deleteFlatRate=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/flatRate/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadFlatRateTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-flatRate" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="flatRate"
			class="table table-bordered table-hover">
			<tbody>

            <g:if test="${flatRateInstance?.t113TMT}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t113TMT-label" class="property-label"><g:message
                                code="flatRate.t113TMT.label" default="Tanggal Mulai Berlaku" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t113TMT-label">
                        %{--<ba:editableValue
                                bean="${flatRateInstance}" field="t113TMT"
                                url="${request.contextPath}/FlatRate/updatefield" type="text"
                                title="Enter t113TMT" onsuccess="reloadFlatRateTable();" />--}%

                        <g:formatDate date="${flatRateInstance?.t113TMT}" />

                    </span></td>

                </tr>
            </g:if>
				<g:if test="${flatRateInstance?.operation}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="operation-label" class="property-label"><g:message
					code="flatRate.operation.label" default="Job" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="operation-label">
						%{--<ba:editableValue
								bean="${flatRateInstance}" field="operation"
								url="${request.contextPath}/FlatRate/updatefield" type="text"
								title="Enter operation" onsuccess="reloadFlatRateTable();" />--}%
							
								${flatRateInstance?.operation?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${flatRateInstance?.baseModel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="baseModel-label" class="property-label"><g:message
					code="flatRate.baseModel.label" default="Kode Base Model" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="baseModel-label">
						%{--<ba:editableValue
								bean="${flatRateInstance}" field="baseModel"
								url="${request.contextPath}/FlatRate/updatefield" type="text"
								title="Enter baseModel" onsuccess="reloadFlatRateTable();" />--}%
							
								${flatRateInstance?.baseModel?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>


				<g:if test="${flatRateInstance?.t113FlatRate}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t113FlatRate-label" class="property-label"><g:message
					code="flatRate.t113FlatRate.label" default="Sales Flat Rate" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t113FlatRate-label">
						%{--<ba:editableValue
								bean="${flatRateInstance}" field="t113FlatRate"
								url="${request.contextPath}/FlatRate/updatefield" type="text"
								title="Enter t113FlatRate" onsuccess="reloadFlatRateTable();" />--}%
							
								<g:fieldValue bean="${flatRateInstance}" field="t113FlatRate"/>
							
						</span></td>
					
				</tr>
				</g:if>


            <g:if test="${flatRateInstance?.t113ProductionRate}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t113ProductionRate-label" class="property-label"><g:message
                                code="flatRate.t113FlatRate.label" default="Production Flat Rate" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t113ProductionRate-label">
                        %{--<ba:editableValue
                                bean="${flatRateInstance}" field="t113FlatRate"
                                url="${request.contextPath}/FlatRate/updatefield" type="text"
                                title="Enter t113FlatRate" onsuccess="reloadFlatRateTable();" />--}%

                        <g:fieldValue bean="${flatRateInstance}" field="t113ProductionRate"/>

                    </span></td>

                </tr>
            </g:if>

				<g:if test="${flatRateInstance?.t113StaPriceMenu}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t113StaPriceMenu-label" class="property-label"><g:message
					code="flatRate.t113StaPriceMenu.label" default="Price Menu" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t113StaPriceMenu-label">
						%{--<ba:editableValue
								bean="${flatRateInstance}" field="t113StaPriceMenu"
								url="${request.contextPath}/FlatRate/updatefield" type="text"
								title="Enter t113StaPriceMenu" onsuccess="reloadFlatRateTable();" />--}%
							


                        <g:if test="${flatRateInstance?.t113StaPriceMenu == '1'}">
                            <g:message code="flatRate.t113StaPriceMenu.label.ya" default="Ya" />
                        </g:if>
                        <g:else>
                            <g:message code="cflatRate.t113StaPriceMenu.label.tidak" default="Tidak" />
                        </g:else>
                        </span></td>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${flatRateInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="flatRate.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${flatRateInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/FlatRate/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadFlatRateTable();" />--}%

                        <g:fieldValue bean="${flatRateInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${flatRateInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="flatRate.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${flatRateInstance}" field="dateCreated"
                                url="${request.contextPath}/FlatRate/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadFlatRateTable();" />--}%

                        <g:formatDate date="${flatRateInstance?.dateCreated}" type="datetime" style="MEDIUM"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${flatRateInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="flatRate.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${flatRateInstance}" field="createdBy"
								url="${request.contextPath}/FlatRate/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadFlatRateTable();" />--}%
							
								<g:fieldValue bean="${flatRateInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${flatRateInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="flatRate.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${flatRateInstance}" field="lastUpdated"
								url="${request.contextPath}/FlatRate/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadFlatRateTable();" />--}%
							
								<g:formatDate date="${flatRateInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${flatRateInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="flatRate.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${flatRateInstance}" field="updatedBy"
                                url="${request.contextPath}/FlatRate/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadFlatRateTable();" />--}%

                        <g:fieldValue bean="${flatRateInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.close.label" default="Close" /></a>
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${flatRateInstance?.id}"
					update="[success:'flatRate-form',failure:'flatRate-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteFlatRate('${flatRateInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
