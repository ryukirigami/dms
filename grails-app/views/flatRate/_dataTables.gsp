<%@ page import="com.kombos.administrasi.FlatRate" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="flatRate_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="flatRate.t113TMT.label" default="Tanggal Mulai Berlaku" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="flatRate.operation.label" default="Job" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="flatRate.baseModel.label" default="Base Model (Kode)" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="flatRate.t113FlatRate.label" default="Sales Flate Rate" /></div>
        </th>

        %{--Production Rate--}%
        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="flatRate.t113ProductionRate.label" default="Production Rate" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="flatRate.t113StaPriceMenu.label" default="Price Menu" /></div>
        </th>


    </tr>
    <tr>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_t113TMT" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
                <input type="hidden" name="search_t113TMT" value="date.struct">
                <input type="hidden" name="search_t113TMT_day" id="search_t113TMT_day" value="">
                <input type="hidden" name="search_t113TMT_month" id="search_t113TMT_month" value="">
                <input type="hidden" name="search_t113TMT_year" id="search_t113TMT_year" value="">
                <input type="text" data-date-format="dd/mm/yyyy" name="search_t113TMT_dp" value="" id="search_t113TMT" class="search_init">
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_operation" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_operation" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_baseModel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_baseModel" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_t113SalesFlateRate" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_t113SalesFlateRate" class="search_init" onkeypress="return isNumberKey(event)"/>
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_t113ProductionRate" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_t113ProductionRate" class="search_init" onkeypress="return isNumberKey(event)"/>
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_t113StaPriceMenu" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <select name="search_t113StaPriceMenu" id="search_t113StaPriceMenu" style="width:100%" onchange="reloadFlatRateTable();">
                    <option value=""></option>
                    <option value="1">Ya</option>
                    <option value="0">Tidak</option>
                </select>
            </div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var flatRateTable;
var reloadFlatRateTable;
$(function(){

    var recordsflatRateperpage = [];//new Array();
    var anflatRateSelected;
    var jmlRecflatRatePerPage=0;
    var id;


	reloadFlatRateTable = function() {
		flatRateTable.fnDraw();
	}

	
	$('#search_t113TMT').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t113TMT_day').val(newDate.getDate());
			$('#search_t113TMT_month').val(newDate.getMonth()+1);
			$('#search_t113TMT_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			flatRateTable.fnDraw();	
	});

	$("#search_t113StaPriceMenu").change(function(){
		flatRateTable.fnDraw();
	 	e.stopPropagation();

	});


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	flatRateTable.fnDraw();
		}
	});

	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	flatRateTable = $('#flatRate_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsflatRate = $("#flatRate_datatables tbody .row-select");
            var jmlflatRateCek = 0;
            var nRow;
            var idRec;
            rsflatRate.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsflatRateperpage[idRec]=="1"){
                    jmlflatRateCek = jmlflatRateCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsflatRateperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecflatRatePerPage = rsflatRate.length;
            if(jmlflatRateCek==jmlRecflatRatePerPage && jmlRecflatRatePerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
	"sName": "t113TMT",
	"mDataProp": "t113TMT",
	"aTargets": [2],
	"mRender": function ( data, type, row )
	{
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "operation",
	"mDataProp": "operation",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}
,
{
	"sName": "baseModel",
	"mDataProp": "baseModel",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{//1 Sales Flate Rate
	"sName": "t113FlatRate",
	"mDataProp": "t113FlatRate",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "t113ProductionRate",
	"mDataProp": "t113ProductionRate",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "t113StaPriceMenu",
	"mDataProp": "t113StaPriceMenu",
	"aTargets": [5],
	"mRender": function ( data, type, row ) {

            if(data=="1"){
                  return "Ya";
             }else
             {
                  return "Tidak";
             }

        },
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var operation = $('#filter_operation input').val();
						if(operation){
							aoData.push(
									{"name": 'sCriteria_operation', "value": operation}
							);
						}
	
						var baseModel = $('#filter_baseModel input').val();
						if(baseModel){
							aoData.push(
									{"name": 'sCriteria_baseModel', "value": baseModel}
							);
						}

						var t113TMT = $('#search_t113TMT').val();
						var t113TMTDay = $('#search_t113TMT_day').val();
						var t113TMTMonth = $('#search_t113TMT_month').val();
						var t113TMTYear = $('#search_t113TMT_year').val();
						
						if(t113TMT){
							aoData.push(
									{"name": 'sCriteria_t113TMT', "value": "date.struct"},
									{"name": 'sCriteria_t113TMT_dp', "value": t113TMT},
									{"name": 'sCriteria_t113TMT_day', "value": t113TMTDay},
									{"name": 'sCriteria_t113TMT_month', "value": t113TMTMonth},
									{"name": 'sCriteria_t113TMT_year', "value": t113TMTYear}
							);
						}
	
						var t113FlatRate = $('#filter_t113SalesFlateRate input').val();
						if(t113FlatRate){
							aoData.push(
									{"name": 'sCriteria_t113SalesFlateRate', "value": t113FlatRate}
							);
						}
//2 ServiceRate
						var t113ProductionRate = $('#filter_t113ProductionRate input').val();
						if(t113ProductionRate){
							aoData.push(
									{"name": 'sCriteria_t113ProductionRate', "value": t113FlatRate}
							);
						}
	
						var t113StaPriceMenu = $('#search_t113StaPriceMenu').val();
						if(t113StaPriceMenu){
							aoData.push(
							{"name": 'sCriteria_t113StaPriceMenu', "value": t113StaPriceMenu}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
	 $('.select-all').click(function(e) {

        $("#flatRate_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            if(this.checked){
                recordsflatRateperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsflatRateperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#flatRate_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsflatRateperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anflatRateSelected = flatRateTable.$('tr.row_selected');
            if(jmlRecflatRatePerPage == anflatRateSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsflatRateperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>