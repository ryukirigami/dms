<%@ page import="com.kombos.administrasi.BaseModel; com.kombos.administrasi.Operation; com.kombos.administrasi.FlatRate" %>
%{--<r:require modules="autoNumeric" />--}%

<g:javascript>
$(function(){
			$('#namaOps').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/flatRate/listOps', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});

		function detailOps(){
                var noJobs = $('#namaOps').val();
                $.ajax({
                    url:'${request.contextPath}/flatRate/detailOps?noJobs='+noJobs,
                    type: "POST", // Always use POST when deleting data
                    success : function(data){
                        $('#idJobs').val("");
                        if(data.hasil=="ada"){
                            $('#idJobs').val(data.id);
                        }
                            console.log("idJobs : " + data.id)
                    }
                })
        }
</g:javascript>



<div class="control-group fieldcontain ${hasErrors(bean: flatRateInstance, field: 't113TMT', 'error')} required">
	<label class="control-label" for="t113TMT">
		<g:message code="flatRate.t113TMT.label" default="Tanggal Mulai Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<ba:datePicker name="t113TMT" precision="day"  value="${flatRateInstance?.t113TMT}"  format="dd/mm/yyyy" required="true"/>
	</div>
</div>
<g:javascript>
    document.getElementById("t113TMT").focus();
    function disableSpace(event){
        var charCode = (event.which) ? event.which : event.keyCode
        if (charCode == 32) {
            return false;
        }
    }
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: flatRateInstance, field: 'operation', 'error')} required">
    <label class="control-label" for="operation">
        <g:message code="flatRate.operation.label" default="Job" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        %{--<g:select id="operation" name="operation.id" from="${Operation.createCriteria().list(){eq("staDel", "0");order("m053NamaOperation", "asc")}}" optionKey="id" required="" value="${flatRateInstance?.operation?.id}" onkeypress="return disableSpace(event);" autocomplete="off" class="many-to-one"/>--}%
        <g:textField name="namaOps" id="namaOps" class="typeahead" maxlength="225" value="${flatRateInstance?.operation?.m053Id?flatRateInstance?.operation?.m053Id?.trim()+' || '+flatRateInstance?.operation?.m053NamaOperation:""}" autocomplete="off" onblur="detailOps();"/>
        <g:hiddenField name="jobsId" id="idJobs"  value="${flatRateInstance?.operation?.id}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: flatRateInstance, field: 'baseModel', 'error')} required">
    <label class="control-label" for="baseModel">
        <g:message code="flatRate.baseModel.label" default="Kode Base Model" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="baseModel" name="baseModel.id" from="${BaseModel.createCriteria().list(){eq("staDel", "0");order("m102KodeBaseModel", "asc")}}" optionValue="${{it.m102KodeBaseModel +" "+ it.m102NamaBaseModel}}" onkeypress="return disableSpace(event);" autocomplete="off" optionKey="id" required="" value="${flatRateInstance?.baseModel?.id}" class="many-to-one"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: flatRateInstance, field: 't113FlatRate', 'error')} required">

    <label class="control-label" for="t113FlatRate">
		<g:message code="flatRate.t113FlatRate.label" default="Sales Flat Rate" />
		<span class="required-indicator">*</span>
	</label>

	<div class="controls">
	<g:textField name="t113FlatRate" value="${fieldValue(bean: flatRateInstance, field: 't113FlatRate')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: flatRateInstance, field: 't113ProductionRate', 'error')} required">

    <label class="control-label" for="t113ProductionRate">
        <g:message code="flatRate.t113ProductionRate.label" default="Production Flat Rate" />
        <span class="required-indicator">*</span>
    </label>

    <div class="controls">
        <g:textField name="t113ProductionRate" value="${fieldValue(bean: flatRateInstance, field: 't113ProductionRate')}" required=""/>
    </div>

</div>



<div class="control-group fieldcontain ${hasErrors(bean: flatRateInstance, field: 't113StaPriceMenu', 'error')} required">
	<label class="control-label" for="t113StaPriceMenu">
		<g:message code="flatRate.t113StaPriceMenu.label" default="Price Menu" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	 <g:radioGroup name="t113StaPriceMenu" id="t113StaPriceMenu" values="['1','0']" labels="['Ya','Tidak']" value="${flatRateInstance?.t113StaPriceMenu}" required="">
        ${it.radio} ${it.label}
    </g:radioGroup>
	</div>
</div>

<g:javascript>
    $(function(){
      $(".auto").autoNumeric('init',{
          mDec: '5',
          aSep:''
      });
    })
</g:javascript>