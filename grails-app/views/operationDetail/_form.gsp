<%@ page import="com.kombos.administrasi.Operation; com.kombos.administrasi.OperationDetail" %>



<div class="control-group fieldcontain ${hasErrors(bean: operationDetailInstance, field: 'jobID', 'error')} required">
	<label class="control-label" for="jobID">
		<g:message code="operationDetail.jobID.label" default="Kode Job Paket" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="jobID" name="jobID.id" from="${Operation.createCriteria().list {eq("staDel", "0");order("m053NamaOperation", "asc")}}" optionKey="id" required="" value="${operationDetailInstance?.jobID?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: operationDetailInstance, field: 'jobIdDetail', 'error')} required">
	<label class="control-label" for="jobIdDetail">
		<g:message code="operationDetail.jobIdDetail.label" default="Kode Job Detail" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="jobIdDetail" name="jobIdDetail.id" from="${Operation.createCriteria().list {eq("staDel", "0");order("m053NamaOperation", "asc")}}" optionKey="id" required="" value="${operationDetailInstance?.jobIdDetail?.id}" class="many-to-one"/>
	</div>
</div>

<g:javascript>
    document.getElementById('jobID').focus();
</g:javascript>
%{--<div class="control-group fieldcontain ${hasErrors(bean: operationDetailInstance, field: 'staDel', 'error')} required">--}%
	%{--<label class="control-label" for="staDel">--}%
		%{--<g:message code="operationDetail.staDel.label" default="Sta Del" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="staDel" required="" value="${operationDetailInstance?.staDel}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: operationDetailInstance, field: 'createdBy', 'error')} ">--}%
	%{--<label class="control-label" for="createdBy">--}%
		%{--<g:message code="operationDetail.createdBy.label" default="Created By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="createdBy" value="${operationDetailInstance?.createdBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: operationDetailInstance, field: 'updatedBy', 'error')} ">--}%
	%{--<label class="control-label" for="updatedBy">--}%
		%{--<g:message code="operationDetail.updatedBy.label" default="Updated By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="updatedBy" value="${operationDetailInstance?.updatedBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: operationDetailInstance, field: 'lastUpdProcess', 'error')} ">--}%
	%{--<label class="control-label" for="lastUpdProcess">--}%
		%{--<g:message code="operationDetail.lastUpdProcess.label" default="Last Upd Process" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="lastUpdProcess" value="${operationDetailInstance?.lastUpdProcess}"/>--}%
	%{--</div>--}%
%{--</div>--}%

