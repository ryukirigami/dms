
<%@ page import="com.kombos.parts.StokOPName" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>


<table id="stokOPName_datatables_${idTable}" cellpadding="0" cellspacing="0" border="0"
       class="display table table-striped table-bordered table-hover" style="table-layout: fixed; ">
    <thead>
    <tr>
        <th></th>
        <th style="border-bottom: none;padding: 5px;">
            <g:message code="stokOPName.t132ID.label" default="Kode Stock Opname"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="stokOPName.t132Tanggal.label" default="Tanggal Stock Opname"/>
        </th>

        <th style="border-bottom: none; padding: 5px;"/>

    </tr>
    </thead>
</table>

<g:javascript>
var stokOPNameTable;
var reloadStokOPNameTable;
$(function(){
    var anOpen = [];
	$('#stokOPName_datatables_${idTable} td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = stokOPNameTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${request.contextPath}/stokOPName/sublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = stokOPNameTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});

  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			stokOPNameTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

	$('#view').click(function(e){
        stokOPNameTable.fnDraw();
	});

    $('#stokOPName_datatables_${idTable} a.edit').live('click', function (e) {
        e.preventDefault();

        var nRow = $(this).parents('tr')[0];

        if ( nEditing !== null && nEditing != nRow ) {
            restoreRow( stokOPNameTable, nEditing );
            editRow( stokOPNameTable, nRow );
            nEditing = nRow;
        }
        else if ( nEditing == nRow && this.innerHTML == "Save" ) {
            saveRow( stokOPNameTable, nEditing );
            nEditing = null;
        }
        else {
            editRow( stokOPNameTable, nRow );
            nEditing = nRow;
        }
    } );

	$('#view').click(function(){
        stokOPNameTable.fnDraw();
	});

	$('#pencilEdit').click(function(){
	    alert('teeeeeeeessss');
	});
	$('#clear').click(function(e){
        $('#search_t132Tanggal').val("");
        $('#search_t132Tanggal_day').val("");
        $('#search_t132Tanggal_month').val("");
        $('#search_t132Tanggal_year').val("");
        $('#search_t132Tanggalakhir').val("");
        $('#search_t132Tanggalakhir_day').val("");
        $('#search_t132Tanggalakhir_month').val("");
        $('#search_t132Tanggalakhir_year').val("");
        $('#search_t132ID').val("");
//        reloadAuditTrailTable();
	});

	reloadStokOPNameTable = function() {
		stokOPNameTable.fnDraw();
	}


    stokOPNameTable = $('#stokOPName_datatables_${idTable}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"bDestroy": true,
		"aoColumns": [
{
   "mDataProp": null,
   "sClass": "control center",
   "bSortable": false,
   "sWidth":"10px",
   "sDefaultContent": '<i class="icon-plus"></i>'
}
,
{
	"sName": "t132ID",
	"mDataProp": "t132ID",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
	    return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"><input type="hidden" name="nomorStok" id="nomorStok" value="'+data+'">&nbsp;&nbsp;'+data+'</a><a class="pull-right cell-action" name="pencilEdit" id="pencilEdit" href="javascript:void(0);" onClick="edit(\''+data+'\')">&nbsp;&nbsp;<i class="icon-pencil" ></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": false,
	"sWidth":"396px",
	"bVisible": true
}
,
{
	"sName": "t132Tanggal",
	"mDataProp": "t132Tanggal",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"396px",
	"bVisible": true
}
,
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"396px",
	"sDefaultContent": '',
	"bVisible": true
}

],
    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
    
                        var t132Tanggal = $('#search_t132Tanggal').val();
						var t132TanggalDay = $('#search_t132Tanggal_day').val();
						var t132TanggalMonth = $('#search_t132Tanggal_month').val();
						var t132TanggalYear = $('#search_t132Tanggal_year').val();
						
						if(t132Tanggal){
							aoData.push(
									{"name": 'sCriteria_t132Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_t132Tanggal_dp', "value": t132Tanggal},
									{"name": 'sCriteria_t132Tanggal_day', "value": t132TanggalDay},
									{"name": 'sCriteria_t132Tanggal_month', "value": t132TanggalMonth},
									{"name": 'sCriteria_t132Tanggal_year', "value": t132TanggalYear}
							);
						}

                        var t132Tanggalakhir = $('#search_t132Tanggalakhir').val();
                        var t132TanggalDayakhir = $('#search_t132Tanggalakhir_day').val();
                        var t132TanggalMonthakhir = $('#search_t132Tanggalakhir_month').val();
                        var t132TanggalYearakhir = $('#search_t132Tanggalakhir_year').val();

                        if(t132Tanggalakhir){
                            aoData.push(
                                    {"name": 'sCriteria_t132Tanggalakhir', "value": "date.struct"},
                                    {"name": 'sCriteria_t132Tanggalakhir_dp', "value": t132Tanggalakhir},
                                    {"name": 'sCriteria_t132Tanggalakhir_day', "value": t132TanggalDayakhir},
                                    {"name": 'sCriteria_t132Tanggalakhir_month', "value":t132TanggalMonthakhir},
                                    {"name": 'sCriteria_t132Tanggalakhir_year', "value": t132TanggalYearakhir}
                            );
                        }

						var t132ID = $('#search_t132ID').val();
						if(t132ID){
							aoData.push(
									{"name": 'sCriteria_t132ID', "value": t132ID}
							);
						}

    $.ajax({ "dataType": 'json',
        "type": "POST",
        "url": sSource,
        "data": aoData ,
        "success": function (json) {
            fnCallback(json);
           },
        "complete": function () {
           }
    });
}
});
});

    $('#pencilEdit').click(function(){
        console.log('masuk pencil gan')
    });
</g:javascript>
