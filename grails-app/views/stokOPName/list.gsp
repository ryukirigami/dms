
<%@ page import="com.kombos.parts.StokOPName" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'stokOPName.label', default: 'Stock Opname')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>
			var show;
			var edit;
			$(function(){ 
			
				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :
                          window.location.replace('#/inputStokOPName')
							$('#spinner').fadeIn(1);
                            $.ajax({
                                url: '${request.contextPath}/inputStokOPName',
                                type: "GET",dataType:"html",
                                complete : function (req, err) {
                                    $('#main-content').html(req.responseText);
                                    $('#spinner').fadeOut();
                                }
                            });
                            break;
                        case '_DELETE_' :
                            bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
								function(result){
									if(result){
									    var recordsToDelete = [];
									    var nomorDelete = [];
                                        $('#stokOPName-table tbody .row-select').each(function() {
                                            if(this.checked){
                                                var id = $(this).next("input:hidden").val();
                                                var nomor = $('#nomorStok').val();
                                                recordsToDelete.push(id);
                                                nomorDelete.push(nomor);
                                            }
                                        });
                                        var json = JSON.stringify(recordsToDelete);

                                        $.ajax({
                                            url:'${request.contextPath}/stokOPName/massdelete',
                                            type: "POST", // Always use POST when deleting data
                                            data: { ids: json },
                                            complete: function(xhr, status) {
                                                reloadStokOPNameTable();
                                            }
                                        });
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('stokOPName','${request.contextPath}/stokOPName/show/'+id);
				};
				
				edit = function(id) {
				    $('#spinner').fadeIn(1);
					window.location.replace('#/editStokOPName')
                    $.ajax({url: '${request.contextPath}/inputStokOPName?idUbah='+id,
                        type:"GET",dataType: "html",
                        complete : function (req, err) {
                            $('#main-content').html(req.responseText);
                            $('#spinner').fadeOut();
                            loading = false;
                        }
                    });
				};

                shrinkTableLayout = function(){
                    $("#stokOPName-table").hide();
                    $("#stokOPName-form").css("display","block");
                }

                expandTableLayout = function(){
                    $("#stokOPName-table").show();
                    $("#stokOPName-form").css("display","none");
                }


});

var checkin = $('#search_t132Tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
        var newDate = new Date(ev.date)
        newDate.setDate(newDate.getDate() + 1);
        checkout.setValue(newDate);
        checkin.hide();
        $('#search_t132Tanggalakhir')[0].focus();
    }).data('datepicker');
    
    var checkout = $('#search_t132Tanggalakhir').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

        return true;
    }

        function printKartuStok(){
           var checkID =[];
           var id;
            $('#stokOPName-table tbody .row-select').each(function() {
                var id = $(this).next("input:hidden").val();
                checkID.push(id);
            });

            if(checkID.length<1){
                alert('Data Masih Kosong');
                return;
            }

            var ids =  JSON.stringify(checkID);
            var tipe = $('#fileFormat').val()
            if(tipe=="" || tipe==null){
                alert('Pilih Jenis File yang Akan Diprint');
            }else{
                $('#fileFormat').val('');
                window.location = "${request.contextPath}/stokOPName/printKartuStok?fileFormat="+tipe+"&IDPrint="+ids;
            }
        }

    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="stockOPName.view.label" default="View Stock Opname" /></span>
    <ul class="nav pull-right">
        <li><a class="pull-right box-action" href="inputStokOPName"
               style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
                    class="icon-plus"></i>&nbsp;&nbsp;
        </a></li>
        <li><a class="pull-right box-action" href="#"
               style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
                    class="icon-remove"></i>&nbsp;&nbsp;
        </a></li>
        <li class="separator"></li>
    </ul>
</div>
<div class="box">
    <div class="span12" id="stokOPName-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>

        <table>
            <tr>
                <td style="padding: 5px">
                    <g:message code="stokOPName.t132Tanggal.label" default="Tanggal Stock Opname"/>
                </td>
                <td style="padding: 5px">
                    <ba:datePicker style="width:70px" name="search_t132Tanggal" precision="day" value="" format="dd-MM-yyyy"/>&nbsp;s.d.&nbsp;
                    <ba:datePicker style="width:70px" name="search_t132Tanggalakhir" precision="day" value="" format="dd-MM-yyyy"/>
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    <g:message code="stokOPName.t132ID.label" default="Kode Stock Opname"/>
                </td>
                <td style="padding: 5px">
                    <g:textField style="width:462px" name="search_t132ID" value="" onkeypress="return isNumberKey(event);"/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <a class="pull-right box-action" style="display: block;" >
                        &nbsp;&nbsp;
                        <i>
                        <g:field type="button" style="padding: 5px" class="btn cancel pull-right box-action"
                                 name="clear" id="clear" value="${message(code: 'stokOPName.clearsearch.label', default: 'Clear Search')}" />
                        </i>
                        &nbsp;&nbsp;
                    </a>
                    <a class="pull-right box-action" style="display: block;" >
                        &nbsp;&nbsp;
                        <i>
                        <g:field type="button" style="padding: 5px" class="btn cancel pull-right box-action"
                                 name="view" id="view" value="${message(code: 'stokOPName.search.label', default: 'Search')}" />
                        </i>
                        &nbsp;&nbsp;
                    </a>
                </td>
            </tr>
        </table>
        <br/><br/>
        <g:render template="dataTables" />
        <a class="pull-right box-action" style="display: block;" >
            &nbsp;&nbsp;
            <i>
                <g:field type="button" onclick="window.location.replace('#/inputHasilStokOpname')" style="padding: 5px"
                 class="btn cancel pull-right box-action" name="inputHasilStokOpname" id="inputHasilStokOpname" value="${message(code: 'stokopname.button.inputHasilStokOpname.label', default: 'Input Hasil Stok Opname')}" />
            </i>
            &nbsp;&nbsp;
        </a>
        <a class="pull-right box-action" style="display: block;" >
            &nbsp;&nbsp;
            <i>
                <g:field type="button" style="padding: 5px" class="btn cancel pull-right box-action"
                         name="printBAstok" id="printBAstok" value="${message(code: 'stokOPName.button.printBAstok.label', default: 'Print BA Stok Taking + Lampiran')}" />
            </i>
            &nbsp;&nbsp;
        </a>
        <a class="pull-right box-action" style="display: block;" >
            &nbsp;&nbsp;
            <g:select name="fileFormat" id="fileFormat" from="${['Pdf','Excel']}" noSelection="['':'Pilih Format File']" />
            <i>
            <g:field type="button" onclick="printKartuStok();" style="padding: 5px" class="btn cancel pull-right box-action"
                     name="printStok" id="printStok" value="${message(code: 'stokopname.button.printstok.label', default: 'Print Kartu Stock')}"/>
            </i>
            &nbsp;&nbsp;
        </a>
    </div>
    <div class="span7" id="stokOPName-form" style="display: none;"></div>
</div>
</body>
</html>
