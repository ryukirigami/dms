
<%@ page import="com.kombos.administrasi.FullModelCode" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'fullModelCode.label', default: 'Upload File Full Model Code')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />

            <g:javascript>

                <g:if test="${jmlhDataError && jmlhDataError>0}">
                    $('#save').attr("disabled", true);
                </g:if>

                var saveForm;
                $(function(){

                    function progress(e){
                        if(e.lengthComputable){
                            //kalo mau pake progress bar
                            //$('progress').attr({value:e.loaded,max:e.total});
                        }
                    }

                    saveForm = function() {

                    <g:if test="${jsonData}">
                        var sendData = ${jsonData};
                        $.ajax({
                            url:'${request.getContextPath()}/uploadFullModelCode/upload',
                            type: 'POST',
                            xhr: function() {
                                var myXhr = $.ajaxSettings.xhr();
                                if(myXhr.upload){
                                    myXhr.upload.addEventListener('progress',progress, false);
                                }
                                return myXhr;
                            },
                            //add beforesend handler to validate or something
                            //beforeSend: functionname,
                            success: function (res) {
                                $('#fullModelCodeTable').empty();
                                $('#fullModelCodeTable').append(res);
                                //reloadOperationTable();
                            },
                            //add error handler for when a error occurs if you want!
                            error: function (data, status, e){
                                alert(e);
                            },
                            complete: function(xhr, status) {

                            },
                            data: JSON.stringify(sendData),
                            contentType: "application/json; charset=utf-8",
                            traditional: true,
                            cache: false
                        });
                    </g:if>
                    <g:else>
                        alert('No File Selected');
                    </g:else>
                    }


                });

            </g:javascript>

	</head>

	<body>
    <div id="fullModelCodeTable">
        <div class="navbar box-header no-border">
            <span class="pull-left">
                <g:message code="default.list.label" args="[entityName]" />
            </span>
            <ul class="nav pull-right">
                <li></li>
                <li></li>
                <li class="separator"></li>
            </ul>
        </div>
        <div class="box">
            <div class="span12" id="fullModelCode-table">
                <fieldset>
                    <form id="uploadFullModelCode-save" class="form-vertical" action="${request.getContextPath()}/uploadFullModelCode/save" method="post" onsubmit="submitForm();return false;">
                        <table>
                            <tr>
                                <td>
                                    <a href="${request.getContextPath()}/formatFileUpload/UploadFullModelCode.xls" >* File Example Upload</a>
                                    <br/><br/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <fieldset class="form">
                                        <g:render template="formFullCode"/>
                                    </fieldset>
                                </td>
                                <td>
                                    <fieldset class="buttons controls">
                                        <g:submitButton class="btn btn-primary create" name="view" id="view" value="${message(code: 'default.button.view.label', default: 'Upload')}" />
                                        <g:field type="button" onclick="saveForm();" class="btn btn-primary create" name="save" id="save" value="${message(code: 'default.button.upload.label', default: 'Simpan')}"/>
                                        &nbsp;&nbsp;&nbsp;
                                        <g:if test="${flash.message}">
                                            ${flash.message}
                                        </g:if>
                                    </fieldset>
                                </td>
                            </tr>
                        </table>
                    </form>
                    <g:javascript>
                        var submitForm;
                        $(function(){

                            function progress(e){
                                if(e.lengthComputable){
                                    //kalo mau pake progress bar
                                    //$('progress').attr({value:e.loaded,max:e.total});
                                }
                            }

                            submitForm = function() {

                                var form = new FormData($('#uploadFullModelCode-save')[0]);

                                $.ajax({
                                    url:'${request.getContextPath()}/uploadFullModelCode/view',
                                    type: 'POST',
                                    xhr: function() {
                                        var myXhr = $.ajaxSettings.xhr();
                                        if(myXhr.upload){
                                            myXhr.upload.addEventListener('progress',progress, false);
                                        }
                                        return myXhr;
                                    },
                                    //add beforesend handler to validate or something
                                    //beforeSend: functionname,
                                    success: function (res) {
                                        $('#fullModelCodeTable').empty();
                                        $('#fullModelCodeTable').append(res);
                                        //reloadOperationTable();
                                    },
                                    //add error handler for when a error occurs if you want!
                                    error: function (data, status, e){
                                        alert(e);
                                    },
                                    complete: function(xhr, status) {

                                    },
                                    data: form,
                                    cache: false,
                                    contentType: false,
                                    processData: false
                                });

                            }
                        });
                    </g:javascript>
                </fieldset>
                <br>
                <div style="max-width: 100%; overflow: auto">
                <table id="uploadFullModelCode_datatables" cellpadding="0" cellspacing="0"
                       border="0"
                       class="display table table-striped table-bordered table-hover table-selectable"
                       width="100%">
                    <thead>
                    <tr>
                            <th>
                                Kode Kategori
                            </th>
                            <th>
                                Nama Kategori
                            </th>
                            <th>
                                Kode Base Model
                            </th>
                            <th>
                                Nama Base Model
                            </th>
                            <th>
                                Kode Stir
                            </th>
                            <th>
                                Nama Stir
                            </th>
                            <th>
                                Kode Model
                            </th>
                            <th>
                                Nama Model
                            </th>
                            <th>
                                Kode Body Type
                            </th>
                            <th>
                                Nama Body Type
                            </th>
                            <th>
                                Kode Gear
                            </th>
                            <th>
                                Nama Gear
                            </th>
                            <th>
                                Kode Grade
                            </th>
                            <th>
                                Nama Grade
                            </th>
                            <th>
                                Kode Engine
                            </th>
                            <th>
                                Nama Engine
                            </th>
                            <th>
                                Kode Country
                            </th>
                            <th>
                                Nama Country
                            </th>
                            <th>
                                Kode Form Of Vehicle
                            </th>
                            <th>
                                Nama Form Of Vehicle
                            </th>
                            <th>
                                Full Model Code
                            </th>
                            <th>
                                Status Import
                            </th>
                            </tr>
                        <g:if test="${htmlData}">
                            ${htmlData}
                        </g:if>
                        <g:else>
                            <tr class="odd">
                                <td class="dataTables_empty" valign="top" colspan="25">No data available in table</td>
                            </tr>
                        </g:else>
                    </table>
                </div>
            </div>
        </div>

    <g:set var="entityName" value="${message(code: 'fullModelVinCode.label', default: 'Upload File Full Model Vin Code')}" />

    <g:javascript>

        <g:if test="${jmlhDataErrorVinCode && jmlhDataErrorVinCode>0}">
            $('#saveVinCode').attr("disabled", true);
        </g:if>

        var saveFormVinCode;
        $(function(){

            function progress(e){
                if(e.lengthComputable){
                    //kalo mau pake progress bar
                    //$('progress').attr({value:e.loaded,max:e.total});
                }
            }

            saveFormVinCode = function() {

        <g:if test="${jsonDataVinCode}">
            var sendDataVinCode = ${jsonDataVinCode};
                        $.ajax({
                            url:'${request.getContextPath()}/uploadFullModelCode/uploadVinCode',
                            type: 'POST',
                            xhr: function() {
                                var myXhr = $.ajaxSettings.xhr();
                                if(myXhr.upload){
                                    myXhr.upload.addEventListener('progress',progress, false);
                                }
                                return myXhr;
                            },
                            //add beforesend handler to validate or something
                            //beforeSend: functionname,
                            success: function (res) {
                                $('#fullModelCodeTable').empty();
                                $('#fullModelCodeTable').append(res);
                                //reloadOperationTable();
                            },
                            //add error handler for when a error occurs if you want!
                            error: function (data, status, e){
                                alert(e);
                            },
                            complete: function(xhr, status) {

                            },
                            data: JSON.stringify(sendDataVinCode),
                            contentType: "application/json; charset=utf-8",
                            traditional: true,
                            cache: false
                        });
        </g:if>
        <g:else>
            alert('No File Selected');
        </g:else>
        }


    });

    </g:javascript>

        <div class="navbar box-header no-border">
            <span class="pull-left">
                <g:message code="default.list.label" args="[entityName]" />
            </span>
            <ul class="nav pull-right">
                <li></li>
                <li></li>
                <li class="separator"></li>
            </ul>
        </div>
        <div class="box">
            <div class="span12" id="fullModelVinCode-table">
                <fieldset>
                    <form id="uploadFullModelVinCode-save" class="form-vertical" action="${request.getContextPath()}/uploadFullModelVinCode/save" method="post" onsubmit="submitFormVinCode();return false;">
                        <table>
                            <tr>
                                <td>
                                    <a href="${request.getContextPath()}/formatFileUpload/UploadFullModelVinCode.xls" >* File Example Upload</a>
                                    <br/><br/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <fieldset class="form">
                                        <g:render template="formVinCode"/>
                                    </fieldset>
                                </td>
                                <td>
                                    <fieldset class="buttons controls">
                                        <g:submitButton class="btn btn-primary create" name="view" id="view" value="${message(code: 'default.button.view.label', default: 'Upload')}" />
                                        <g:field type="button" onclick="saveFormVinCode();" class="btn btn-primary create" name="saveVinCode" id="saveVinCode" value="${message(code: 'default.button.upload.label', default: 'Simpan')}"/>
                                        &nbsp;&nbsp;&nbsp;
                                        <g:if test="${flash.message}">
                                            ${flash.message}
                                        </g:if>
                                    </fieldset>
                                </td>
                            </tr>
                        </table>
                    </form>
                    <g:javascript>
                        var submitFormVinCode;
                        $(function(){

                            function progress(e){
                                if(e.lengthComputable){
                                    //kalo mau pake progress bar
                                    //$('progress').attr({value:e.loaded,max:e.total});
                                }
                            }

                            submitFormVinCode = function() {

                                var form = new FormData($('#uploadFullModelVinCode-save')[0]);

                                $.ajax({
                                    url:'${request.getContextPath()}/uploadFullModelCode/viewVinCode',
                                    type: 'POST',
                                    xhr: function() {
                                        var myXhr = $.ajaxSettings.xhr();
                                        if(myXhr.upload){
                                            myXhr.upload.addEventListener('progress',progress, false);
                                        }
                                        return myXhr;
                                    },
                                    //add beforesend handler to validate or something
                                    //beforeSend: functionname,
                                    success: function (res) {
                                        $('#fullModelCodeTable').empty();
                                        $('#fullModelCodeTable').append(res);
                                        //reloadOperationTable();
                                    },
                                    //add error handler for when a error occurs if you want!
                                    error: function (data, status, e){
                                        alert(e);
                                    },
                                    complete: function(xhr, status) {

                                    },
                                    data: form,
                                    cache: false,
                                    contentType: false,
                                    processData: false
                                });

                            }
                        });
                    </g:javascript>
                </fieldset>
                <br>

                <table class="display table table-striped table-bordered table-hover dataTable" width="100%" cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px; width: 1284px;">
                    <tr>
                        <th>
                            WMI
                        </th>
                        <th>
                            VDS
                        </th>
                        <th>
                            CEK DIGIT
                        </th>
                        <th>
                            VIS
                        </th>
                        <th>
                            VIN CODE
                        </th>
                        <th>
                            FULL MODEL CODE
                        </th>
                        <th>
                            TAHUN
                        </th>
                        <th>
                            BULAN
                        </th>
                    </tr>
                    <g:if test="${htmlDataVinCode}">
                        ${htmlDataVinCode}
                    </g:if>
                    <g:else>
                        <tr class="odd">
                            <td class="dataTables_empty" valign="top" colspan="21">No data available in table</td>
                        </tr>
                    </g:else>
                </table>
                %{--<g:if test="${jsonData}">--}%
                %{--${jsonData}--}%
                %{--</g:if>--}%
            </div>
        </div>
    </div>
</body>
</html>
