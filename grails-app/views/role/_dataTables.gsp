
<%@ page import="com.kombos.baseapp.sec.shiro.Role" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="role_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="role.label" default="Role" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="role.maxDiscGoodwillPersen.label" default="% Disc. Goodwill" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="role.maxDiscGoodwillRp.label" default="Rp Disc. Goodwill" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="role.maxPersenSpDiscJasa.label" default="% Spesial Disc Jasa" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="role.maxPersenSpDiscPart.label" default="% Spesial Disc Parts" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="role.reportWorkshopLain.label" default="Report Workshop Lain" /></div>
			</th>


		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_role" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_role" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_maxDiscGoodwillPersen" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_maxDiscGoodwillPersen" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_maxDiscGoodwillRp" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_maxDiscGoodwillRp" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_maxPersenSpDiscJasa" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_maxPersenSpDiscJasa" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_maxPersenSpDiscPart" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_maxPersenSpDiscPart" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_lihatReport" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <select name="search_lihatReport" onchange="reloadRoleTable();" style="width:100%">
                        <option value=""></option>
                        <option value="1">Ya</option>
                        <option value="0">Tidak</option>
                    </select>
				</div>
			</th>
	
		</tr>
	</thead>
</table>

<g:javascript>
var roleTable;
var reloadRoleTable;
$(function(){
	
	reloadRoleTable = function() {
		roleTable.fnDraw();
	}

    var recordsroleperpage = [];
    var anRoleSelected;
    var jmlRecRolePerPage=0;
    var id;



$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	roleTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	roleTable = $('#role_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsRole = $("#role_datatables tbody .row-select");
            var jmlRoleCek = 0;
            var nRow;
            var idRec;
            rsRole.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsroleperpage[idRec]=="1"){
                    jmlRoleCek = jmlRoleCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsroleperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecRolePerPage = rsRole.length;
            if(jmlRoleCek==jmlRecRolePerPage && jmlRecRolePerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "name",
	"mDataProp": "name",
	"aTargets": [0],
    "mRender": function ( data, type, row ) {
//<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this">
        return '<input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show(\''+row['id']+'\');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit(\''+row['id']+'\');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "maxDiscGoodwillPersen",
	"mDataProp": "maxDiscGoodwillPersen",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "maxDiscGoodwillRp",
	"mDataProp": "maxDiscGoodwillRp",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "maxPersenSpDiscJasa",
	"mDataProp": "maxPersenSpDiscJasa",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "maxPersenSpDiscParts",
	"mDataProp": "maxPersenSpDiscParts",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "staLihatReportWorkshopLain",
	"mDataProp": "staLihatReportWorkshopLain",
	"aTargets": [5],
	"mRender": function ( data, type, row ) {
        if(data=="0"){
              return "Tidak";
        }else{
              return "Ya";
        }
    },
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	

						var role = $('#filter_role input').val();
						if(role){
							aoData.push(
									{"name": 'sCriteria_role', "value": role}
							);
						}
	
						var maxDiscGoodwillPersen = $('#filter_maxDiscGoodwillPersen input').val();
						if(maxDiscGoodwillPersen){
							aoData.push(
									{"name": 'sCriteria_maxDiscGoodwillPersen', "value": maxDiscGoodwillPersen}
							);
						}
	
						var maxDiscGoodwillRp = $('#filter_maxDiscGoodwillRp input').val();
						if(maxDiscGoodwillRp){
							aoData.push(
									{"name": 'sCriteria_maxDiscGoodwillRp', "value": maxDiscGoodwillRp}
							);
						}
	
						var maxPersenSpDiscJasa = $('#filter_maxPersenSpDiscJasa input').val();
						if(maxPersenSpDiscJasa){
							aoData.push(
									{"name": 'sCriteria_maxPersenSpDiscJasa', "value": maxPersenSpDiscJasa}
							);
						}

						var maxPersenSpDiscPart = $('#filter_maxPersenSpDiscPart input').val();
						if(maxPersenSpDiscPart){
							aoData.push(
									{"name": 'sCriteria_maxPersenSpDiscPart', "value": maxPersenSpDiscPart}
							);
						}

						var lihatReport = $('#filter_lihatReport select').val();
						if(lihatReport){
							aoData.push(
									{"name": 'sCriteria_lihatReport', "value": lihatReport}
							);
						}


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
    $('.select-all').click(function(e) {
        $("#role_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsroleperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsroleperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

    $('#role_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsroleperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anRoleSelected = roleTable.$('tr.row_selected');
            if(jmlRecRolePerPage == anRoleSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsroleperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
