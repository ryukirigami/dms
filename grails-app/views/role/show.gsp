

<%@ page import="com.kombos.baseapp.sec.shiro.Role" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'role.label', default: 'Role')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteRole;

$(function(){ 
	deleteRole=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/role/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadRoleTable();
   				expandTableLayout('role');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-role" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="role"
			class="table table-bordered table-hover">
			<tbody>


            <g:if test="${roleInstance?.name}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="name-label" class="property-label"><g:message
                                code="role.label" default="Role" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="name-label">
                        <g:fieldValue bean="${roleInstance}" field="name"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${roleInstance?.maxDiscGoodwillPersen} || ${roleInstance?.maxDiscGoodwillPersen}==0">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="maxDiscGoodwillPersen-label" class="property-label"><g:message
                                code="role.maxDiscGoodwillPersen.label" default="Maks. % Disc. GoodWill" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="maxDiscGoodwillPersen-label">
                        <g:fieldValue bean="${roleInstance}" field="maxDiscGoodwillPersen"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${roleInstance?.maxDiscGoodwillRp} || ${roleInstance?.maxDiscGoodwillRp}==0">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="maxDiscGoodwillRp-label" class="property-label"><g:message
                                code="role.maxDiscGoodwillRp.label" default="Maks. Rp Disc. GoodWill" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="maxDiscGoodwillRp-label">
                        %{--<ba:editableValue
                                bean="${roleInstance}" field="maxDiscGoodwillRp"
                                url="${request.contextPath}/Role/updatefield" type="text"
                                title="Enter maxDiscGoodwillRp" onsuccess="reloadRoleTable();" />--}%

                        <g:fieldValue bean="${roleInstance}" field="maxDiscGoodwillRp"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${roleInstance?.maxPersenSpDiscJasa} || ${roleInstance?.maxPersenSpDiscJasa}==0">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="maxPersenSpDiscJasa-label" class="property-label"><g:message
                                code="role.maxPersenSpDiscJasa.label" default="Maks. % Spesial Disc Jasa" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="maxPersenSpDiscJasa-label">
                        %{--<ba:editableValue
                                bean="${roleInstance}" field="maxPersenSpDiscJasa"
                                url="${request.contextPath}/Role/updatefield" type="text"
                                title="Enter maxPersenSpDiscJasa" onsuccess="reloadRoleTable();" />--}%

                        <g:fieldValue bean="${roleInstance}" field="maxPersenSpDiscJasa"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${roleInstance?.maxPersenSpDiscParts} || ${roleInstance?.maxPersenSpDiscParts}==0">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="maxPersenSpDiscParts-label" class="property-label"><g:message
                                code="role.maxPersenSpDiscParts.label" default="Maks. % Spesial Disc Part" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="maxPersenSpDiscParts-label">
                        %{--<ba:editableValue
                                bean="${roleInstance}" field="maxPersenSpDiscParts"
                                url="${request.contextPath}/Role/updatefield" type="text"
                                title="Enter maxPersenSpDiscParts" onsuccess="reloadRoleTable();" />--}%

                        <g:fieldValue bean="${roleInstance}" field="maxPersenSpDiscParts"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${roleInstance?.staLihatReportWorkshopLain}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="staLihatReportWorkshopLain-label" class="property-label"><g:message
                                code="role.staLihatReportWorkshopLain.label" default="Dapat Melihat Report Workshop Lain?"/>:</span>
                    </td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="staLihatReportWorkshopLain-label">
                        %{--<ba:editableValue
                        bean="${roleInstance}" field="staLihatReportWorkshopLain"
                        url="${request.contextPath}/PerhitunganNextService/updatefield" type="text"
                        title="Enter staLihatReportWorkshopLain" onsuccess="reloadPerhitunganNextServiceTable();" />--}%
                        <g:if test="${roleInstance.staLihatReportWorkshopLain == '0'}">
                            <g:message code="role.staLihatReportWorkshopLain.label" default="Tidak"/>
                        </g:if>
                        <g:else>
                            <g:message code="role.staLihatReportWorkshopLain.label" default="Ya"/>
                        </g:else>
                    </span></td>

                </tr>
            </g:if>



            <g:if test="${roleInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="role.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${roleInstance}" field="dateCreated"
								url="${request.contextPath}/Role/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadRoleTable();" />--}%
							
								<g:formatDate date="${roleInstance?.dateCreated}" format="dd/MM/yyyy hh:mm" />
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${roleInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="role.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${roleInstance}" field="createdBy"
                                url="${request.contextPath}/Role/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadRoleTable();" />--}%

                        <g:fieldValue bean="${roleInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${roleInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="role.lastUpdated.label" default="Last Update" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${roleInstance}" field="lastUpdated"
                                url="${request.contextPath}/Role/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadRoleTable();" />--}%

                        <g:formatDate date="${roleInstance?.lastUpdated}" format="dd/MM/yyyy hh:mm"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${roleInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="role.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${roleInstance}" field="updatedBy"
								url="${request.contextPath}/Role/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadRoleTable();" />--}%
							
								<g:fieldValue bean="${roleInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${roleInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="role.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${roleInstance}" field="lastUpdProcess"
								url="${request.contextPath}/Role/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadRoleTable();" />--}%

								<g:fieldValue bean="${roleInstance}" field="lastUpdProcess"/>

						</span></td>

				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('role');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${roleInstance?.id}"
					update="[success:'role-form',failure:'role-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				%{--<ba:confirm id="delete" class="btn cancel"--}%
					%{--message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"--}%
					%{--onsuccess="deleteRole('${roleInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>--}%
			</fieldset>
		</g:form>
	</div>
</body>
</html>
