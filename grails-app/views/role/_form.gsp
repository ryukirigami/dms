<%@ page import="com.kombos.baseapp.sec.shiro.Role;com.kombos.administrasi.CompanyDealer;" %>

<g:javascript>
    jQuery(function($) {
        $('.persen').autoNumeric('init',{
            vMin:'0',
            vMax:'100',
            aSep:''
        });
    });
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: roleInstance, field: 'name', 'error')} ">
    <label class="control-label" for="name">
        <g:message code="role.label" default="Role" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="name" id="name" required="" value="${roleInstance?.name}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: roleInstance, field: 'maxDiscGoodwillPersen', 'error')} ">
	<label class="control-label" for="maxDiscGoodwillPersen">
		<g:message code="role.maxDiscGoodwillPersen.label" default="Maks. % Disc. GoodWill" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:textField required="" class="persen" onkeydown="return isNumberKey(event);" name="maxDiscGoodwillPersen" value="${roleInstance?.maxDiscGoodwillPersen}" />&nbsp;%
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: roleInstance, field: 'maxDiscGoodwillRp', 'error')} ">
	<label class="control-label" for="maxDiscGoodwillRp">
		<g:message code="role.maxDiscGoodwillRp.label" default="Maks. Rp Disc. GoodWill" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:textField required="" onkeydown="return isNumberKey(event);" maxlength="8" name="maxDiscGoodwillRp" value="${roleInstance?.maxDiscGoodwillRp ? Double.valueOf(roleInstance?.maxDiscGoodwillRp).longValue() : ''}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: roleInstance, field: 'maxPersenSpDiscJasa', 'error')} ">
	<label class="control-label" for="maxPersenSpDiscJasa">
		<g:message code="role.maxPersenSpDiscJasa.label" default="Maks. % Spesial Disc Jasa" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:textField required="" class="persen" name="maxPersenSpDiscJasa" value="${roleInstance?.maxPersenSpDiscJasa}" />&nbsp;%
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: roleInstance, field: 'maxPersenSpDiscParts', 'error')} ">
    <label class="control-label" for="maxPersenSpDiscParts">
        <g:message code="role.maxPersenSpDiscParts.label" default="Maks. % Spesial Disc Part" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField required="" class="persen" name="maxPersenSpDiscParts" value="${roleInstance?.maxPersenSpDiscParts}" />&nbsp;%
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: roleInstance, field: 'staLihatReportWorkshopLain', 'error')} ">
	<label class="control-label" for="staLihatReportWorkshopLain">
		<g:message code="role.staLihatReportWorkshopLain.label" default="Dapat Melihat Report Workshop Lain?" />
	</label>
	<div class="controls">
        <g:radioGroup name="staLihatReportWorkshopLain" required="" id="staLihatReportWorkshopLain" values="['1','0']" labels="['Ya','Tidak']"  value="${roleInstance?.staLihatReportWorkshopLain}">
            ${it.radio} ${it.label}
        </g:radioGroup>
	</div>
</div>
