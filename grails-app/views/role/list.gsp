<%@ page import="com.kombos.baseapp.AppSettingParam; com.kombos.baseapp.AppSettingParamService" %>
<%@ page import="com.kombos.baseapp.sec.shiro.Role" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'role.label', default: 'Role')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			$(function(){ 
			
				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('role');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('role', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('role', '${request.contextPath}/role/massdelete', reloadRoleTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('role','${request.contextPath}/role/show/'+id);
				};
				
				edit = function(id) {
					editInstance('role','${request.contextPath}/role/edit/'+id);
				};

});
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57)){
            if(charCode == 46){
                return true
            }
            return false;
        }


        return true;
    }
</g:javascript>
	</head>
	<body>
    <div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
            <li class="separator"></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			%{--<li><a class="pull-right box-action" href="#"--}%
				%{--style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i--}%
					%{--class="icon-remove"></i>&nbsp;&nbsp;--}%
			%{--</a></li>--}%
			<li class="separator"></li>
		</ul>
	</div>
    <ul class="nav nav-tabs">
        <li><a href="javascript:loadPath('user/');">User Profile</a></li>
        <li class="active"><a href="#">User Role</a></li>
        <li><a href="javascript:loadPath('rolePermissions/list');">User Access</a></li>
        <li><a href="javascript:loadPath('userPrinter/');">User Printer</a></li>
        <li><a href="javascript:loadPath('namaForm/');">User Form Name</a></li>
    </ul>
    <div class="box">
		<div class="span12" id="role-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
		</div>
		<div class="span7" id="role-form" style="display: none;"></div>
	</div>
</body>
</html>
