
<%@ page import="com.kombos.parts.StokOPName" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>


<table id="bPJobInstruction_datatables_${idTable}" cellpadding="0" cellspacing="0" border="0"
       class="display table table-striped table-bordered table-hover table-selectable" style="table-layout: fixed; ">
    <thead>
    <tr>
        <th style="border-bottom: none;width: 15px;"></th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="wo.t401NoWo.label" default="Nomor WO"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="wo.noPolisi.label" default="Nomor Polisi"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="wo.tanggalWO.label" default="Tanggal WO"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="wo.start.label" default="Start"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="wo.stop.label" default="Stop"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="bPJobInstruction.finalInspection.label" default="Final Inspection"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="wo.teknisi.label" default="Teknisi"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="bPJobInstruction.sa.label" default="SA"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="bPJobInstruction.janjiPenyerahan.label" default="Janji Penyerahan"/>
        </th>
    </tr>

    <tr>

        <th style="border-top: none; width: 15px;">
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_noWO" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_noWO" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_noPol" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_noPol" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;"></th>

        <th style="border-top: none;padding: 5px;"></th>

        <th style="border-top: none;padding: 5px;"></th>

        <th style="border-top: none;padding: 5px;"></th>

        <th style="border-top: none;padding: 5px;"></th>

        <th style="border-top: none;padding: 5px;"></th>

        <th style="border-top: none;padding: 5px;"></th>
    </tr>

    </thead>
</table>

<g:javascript>
var bPJobInstructionTable;
var reloadBPJobInstructionTable;
$(function(){

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	bPJobInstructionTable.fnDraw();
		}
	});

    var anOpen = [];
    $('#bPJobInstruction_datatables_${idTable} td.control').live('click',function () {
        var nTr = this.parentNode;
        var i = $.inArray( nTr, anOpen );
        if ( i === -1 ) {
            $('i', this).attr( 'class', "icon-minus" );
            var oData = bPJobInstructionTable.fnGetData(nTr);
            $('#spinner').fadeIn(1);
            $.ajax({type:'POST',
                data : oData,
                url:'${request.contextPath}/BPJobInstruction/sub1list',
                success:function(data,textStatus){
                    var nDetailsRow = bPJobInstructionTable.fnOpen(nTr,data,'details');
                    $('div.innerDetails', nDetailsRow).slideDown();
                    anOpen.push( nTr );
                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });

        } else {
            $('i', this).attr( 'class', 'icon-plus' );
            $('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
                bPJobInstructionTable.fnClose( nTr );
                anOpen.splice( i, 1 );
            });
        }
    });

	reloadBPJobInstructionTable = function() {
		bPJobInstructionTable.fnDraw();
	}


    bPJobInstructionTable = $('#bPJobInstruction_datatables_${idTable}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
		   $(nRow).children().each(function(index, td) {
                if(finalIns == "Belum Final Inspection"){
                    if(index == 6){
                        if ($(td).html() == "-") {
                                $(td).css("background-color", "yellow");
                        }
                    }
                }
                if(staAmbilWo=='Belum Ambil WO'){

                    if(index == 0){
                         $(td).html(' ');
                         $(td).click(false);
                    }
                }
                 if(staAmbilWo=='Belum dan Sudah Ambil WO' && aData.ambilWO == 0){
                    if(index == 0){
                         $(td).html(' ');
                         $(td).click(false);
                    }
                }

		   });
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"bDestroy": true,
		"aoColumns": [
{
   "mDataProp": null,
   "sClass": "control center",
   "bSortable": false,
   "sWidth":"10px",
   "sDefaultContent": '<i class="icon-plus"></i>'
}
,
{
	"sName": "t401NoWO",
	"mDataProp": "t401NoWO",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
	    return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"><input type="hidden" value="'+data+'">&nbsp;&nbsp;'+data;
	},
	"bSortable": false,
	"sWidth":"230px",
	"bVisible": true
}
,
{
	"sName": "noPolisi",
	"mDataProp": "noPolisi",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"95px",
	"bVisible": true
}
,
{
	"sName": "t401TanggalWO",
	"mDataProp": "t401TanggalWO",
	"aTargets": [2],
	"sWidth":"125px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "start",
	"aTargets": [3],
	"sWidth":"135px",
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "stop",
	"aTargets": [4],
	"sWidth":"135px",
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "finalInspection",
	"aTargets": [5],
	"sWidth":"135px",
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "teknisi",
	"aTargets": [6],
	"bSortable": false,
	"sWidth":"100px",
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "t401NamaSA",
	"mDataProp": "t401NamaSA",
	"aTargets": [7],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}
,
{
	"sName": "t401TglJamJanjiPenyerahan",
	"mDataProp": "t401TglJamJanjiPenyerahan",
	"aTargets": [8],
	"sWidth":"135px",
	"bVisible": true
}
],
    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
    
                        var Tanggal = $('#search_Tanggal').val();
						var TanggalDay = $('#search_Tanggal_day').val();
						var TanggalMonth = $('#search_Tanggal_month').val();
						var TanggalYear = $('#search_Tanggal_year').val();

	                    var Tanggalakhir = $('#search_Tanggalakhir').val();
                        var TanggalDayakhir = $('#search_Tanggalakhir_day').val();
                        var TanggalMonthakhir = $('#search_Tanggalakhir_month').val();
                        var TanggalYearakhir = $('#search_Tanggalakhir_year').val();


                        if(Tanggal){
                            aoData.push(
                                    {"name": 'sCriteria_Tanggal', "value": "date.struct"},
                                    {"name": 'sCriteria_Tanggal_dp', "value": Tanggal},
                                    {"name": 'sCriteria_Tanggal_day', "value": TanggalDay},
                                    {"name": 'sCriteria_Tanggal_month', "value": TanggalMonth},
                                    {"name": 'sCriteria_Tanggal_year', "value": TanggalYear}
                            );
                        }

                        if(Tanggalakhir){
                            aoData.push(
                                    {"name": 'sCriteria_Tanggalakhir', "value": "date.struct"},
                                    {"name": 'sCriteria_Tanggalakhir_dp', "value": Tanggalakhir},
                                    {"name": 'sCriteria_Tanggalakhir_day', "value": TanggalDayakhir},
                                    {"name": 'sCriteria_Tanggalakhir_month', "value":TanggalMonthakhir},
                                    {"name": 'sCriteria_Tanggalakhir_year', "value": TanggalYearakhir}
                            );
                        }

                        var noWO = $('#filter_noWO input').val();
						if(noWO){
							aoData.push(
									{"name": 'sCriteria_noWO', "value": noWO}
							);
						}

                        var noPol = $('#filter_noPol input').val();
						if(noPol){
							aoData.push(
									{"name": 'sCriteria_noPol', "value": noPol}
							);
						}

						var staAmbilWO = $('#staAmbilWO').val();
						if(staAmbilWO){
							aoData.push(
									{"name": 'sCriteria_staAmbilWO', "value": staAmbilWO}
							);
						}

						var staJobTambah = $('#staJobTambah').val();
						if(staJobTambah){
							aoData.push(
									{"name": 'sCriteria_staJobTambah', "value": staJobTambah}
							);
						}

                        var staCarryOver = $('#staCarryOver').val();
						if(staCarryOver){
							aoData.push(
									{"name": 'sCriteria_staCarryOver', "value": staCarryOver}
							);
						}

                        var staProblemFinding = $('#staProblemFinding').val();
						if(staProblemFinding){
							aoData.push(
									{"name": 'sCriteria_staProblemFinding', "value": staProblemFinding}
							);
						}

                        var staFinalInspection = $('#staFinalInspection').val();
						if(staFinalInspection){
							aoData.push(
									{"name": 'sCriteria_staFinalInspection', "value": staFinalInspection}
							);
						}

    $.ajax({ "dataType": 'json',
        "type": "POST",
        "url": sSource,
        "data": aoData ,
        "success": function (json) {
            fnCallback(json);
           },
        "complete": function () {
           }
    });
}
});
});

</g:javascript>
