
<%@ page import="com.kombos.parts.StokOPName" %>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript>
    var bPJobInstructionSub3Table;
$(function(){

    var anOpen = [];
    $('#bPJobInstruction_datatables_sub3_${idTable} td.sub3control').live('click',function () {
        console.log('click sub 3')
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = bPJobInstructionSub3Table.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${request.contextPath}/BPJobInstruction/sub4list',
	   			success:function(data,textStatus){
	   				var nDetailsRow = bPJobInstructionSub3Table.fnOpen(nTr,data,'details');
    				$('div.inner4Details', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});

  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.inner4Details', $(nTr).next()[0]).slideUp( function () {
      			bPJobInstructionSub3Table.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

	bPJobInstructionSub3Table = $('#bPJobInstruction_datatables_sub3_${idTable}').dataTable({
		"sScrollX": "1205px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
               $(nRow).children().each(function(index, td) {
                    if(staAmbilWo=='Belum dan Sudah Ambil WO'){
                        if(index == 3){
                             $(td).html(' ');
                             $(td).click(false);
                        }
                    }

                   if(aData.staRedo != 1 && tambahJob=="Ya"){
                         if(index == 3){
                             $(td).html(' ');
                             $(td).click(false);
                        }
                    }
                });
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSub3List")}",
		"aoColumns": [
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"12px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"12px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"12px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "sClass": "sub3control center",
   "bSortable": false,
   "sWidth":"10px",
    "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "",
	"mDataProp": "teknisi",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"194px",
	"bVisible": true
}
,

{
	"sName": "",
	"mDataProp": "start",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"194px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "stop",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"194px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "idManPower",
	"aTargets": [6],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"0px",
	"bVisible": false
}
,
{
	"sName": "",
	"mDataProp": "t401NoWO",
	"aTargets": [6],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"0px",
	"bVisible": false
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						aoData.push(
									{"name": 't401NoWO', "value": "${t401NoWO}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
    </g:javascript>

</head>
<body>
<div class="inner3Details">
    <table id="bPJobInstruction_datatables_sub3_${idTable}" cellpadding="0" cellspacing="0" border="0"
           class="display table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th></th>

            <th></th>

            <th></th>

            <th></th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPJobInstruction.roleKirim.label" default="Teknisi" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPJobInstruction.namaKirim.label" default="Start" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPJobInstruction.tanggalKirim.label" default="Stop" /></div>
            </th>
        </tr>
        </thead>
    </table>

</div>
</body>
</html>


			
