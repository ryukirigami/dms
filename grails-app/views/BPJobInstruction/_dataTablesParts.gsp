
<%@ page import="com.kombos.reception.Reception" %>

<r:require modules="baseapplayout" />

<g:render template="../menu/maxLineDisplay"/>

<table id="orderMaterial_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div>
                <input type="checkbox" name="cekAll" id="cekAll" />
                <g:message code="WO.kodeOrderMaterial.label" default="Kode Parts" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="WO.namaOrderMaterial.label" default="Nama Parts" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="WO.qty.label" default="Qty" /></div>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
    var orderMaterialTable;
    var reloadOrderMaterialTable;
    $(function(){

        reloadOrderMaterialTable = function() {
            orderMaterialTable.fnClearTable();
            orderMaterialTable.fnDraw();
        }

        orderMaterialTable = $('#orderMaterial_datatables').dataTable({
            "sScrollX": "100%",
            "bScrollCollapse": true,
            "bAutoWidth" : false,
            "bPaginate" : false,
            "sInfo" : "",
            "sInfoEmpty" : "",
            "sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
            bFilter: true,
            "bStateSave": false,
            "fnInitComplete": function () {
                this.fnAdjustColumnSizing(true);
            },
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                return nRow;
            },
            "bSort": false,
            "bDestroy" : true,
            "aoColumns": [

                {
                    "sName": "",
                    "mDataProp": "kode",
                    "aTargets": [0],
                    "mRender": function ( data, type, row ) {
                        return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"><input type="hidden" value="'+data+'">&nbsp;&nbsp;'+data;
                    },
                    "bSearchable": false,
                    "bSortable": false,
                    "sWidth":"33%",
                    "bVisible": true
                }
                ,

                {
                    "sName": "",
                    "mDataProp": "nama",
                    "aTargets": [1],
                    "bSearchable": false,
                    "bSortable": false,
                    "sWidth":"33%",
                    "bVisible": true
                }
                ,
                {
                    "mDataProp": "qty",
                    "mRender": function ( data, type, row ) {
                        return '<input id="qty_'+row['id']+'" class="inline-edit" type="text" value="'+data+'">';
                    },
                    "bSearchable": false,
                    "bSortable": false,
                    "sWidth":"34%",
                    "bVisible": true
                }

            ]

        });


    });
    $('#cekAll').click(function(){
        if($('#cekAll').is(':checked')){
            $('input', orderMaterialTable.fnGetNodes()).each( function() {
                $('input', orderMaterialTable.fnGetNodes()).attr('checked',true);
            } );
        }else{
            $('input', orderMaterialTable.fnGetNodes()).each( function() {
                $('input', orderMaterialTable.fnGetNodes()).attr('checked',false);
            } );
        }
    });
</g:javascript>

