
<%@ page import="com.kombos.parts.StokOPName" %>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript>
    var bPJobInstructionSub4Table;
$(function(){

	bPJobInstructionSub4Table = $('#bPJobInstruction_datatables_sub4_${idTable}').dataTable({
		"sScrollX": "1205px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSub4List")}",
		"aoColumns": [
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"12px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"12px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"11px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"13px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"11px",
   "sDefaultContent": ''
},
{
	"sName": "",
	"mDataProp": "status",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"231px",
	"bVisible": true
}
,

{
	"sName": "",
	"mDataProp": "tanggalStatus",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"231px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "keterangan",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"231px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "problem",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"231px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "status",
	"aTargets": [5],
    "mRender": function ( data, type, row ) {
	    if(data=="Problem"){
            return '<input type="button" value=" Send Problems ..." onclick="sendProblem(\''+row['t401NoWO']+'\','+row['id']+')"> '
	    }else{
            return '-';

	    }
	},
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"231px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "foreman",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"231px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "t401NoWO",
	"aTargets": [6],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"0px",
	"bVisible": false
}
,
{
	"sName": "",
	"mDataProp": "id",
	"aTargets": [7],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"0px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						aoData.push(
									{"name": 'idManPower', "value": "${idManPower}"}
						);

						aoData.push(
									{"name": 't401NoWO', "value": "${t401NoWO}"}
						);

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
    </g:javascript>

</head>
<body>
<div class="inner4Details">
    <table id="bPJobInstruction_datatables_sub4_${idTable}" cellpadding="0" cellspacing="0" border="0"
           class="display table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th></th>

            <th></th>

            <th></th>

            <th></th>

            <th></th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPJobInstruction.status.label" default="Status" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPJobInstruction.tanggalStatus.label" default="Tanggal Status" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPJobInstruction.tglRespon.label" default="Keterangan" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPJobInstruction.problem.label" default="Problem?" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPJobInstruction.action.label" default="Action" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPJobInstruction.foremanGanti.label" default="Foreman Ganti" /></div>
            </th>
        </tr>
        </thead>
    </table>

</div>
</body>
</html>


			
