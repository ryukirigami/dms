
<%@ page import="com.kombos.baseapp.sec.shiro.Role; com.kombos.administrasi.VendorCat; com.kombos.maintable.StatusKendaraan;com.kombos.parts.Goods;com.kombos.baseapp.sec.shiro.User" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="bPJobInstruction.sendProblemFinding.label" default="Send Problem Finding" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;
	var save;
	$(function(){

        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

        $('.box-action').click(function(){
            return false;
        });

        $(function(){
			$('#parts1').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/jobInstructionGr/listKodeParts', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});
        $(function(){
			$('#parts0').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/jobInstructionGr/listNamaParts', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});

   });

        $('#parts1').click(function(){
            $('#parts0').prop('disabled',true);
            $('#partsMaterial1').prop('checked',true);
        });

        $('#waktu1').click(function(){
            $('#durasiTambah').prop('disabled',false);

        });
          $('#waktu2').click(function(){
            $('#durasiTambah').prop('disabled',true);

        });

         $('#tambahJob1').click(function(){
            $('#namaJobTambah').prop('disabled',false);

        });
          $('#tambahJob2').click(function(){
            $('#namaJobTambah').prop('disabled',true);

        });

          $('#butuhCat1').click(function(){
            $('#tagihKe1').prop('disabled',false);
             $('#tagihKe2').prop('disabled',false);


        });
          $('#butuhCat2').click(function(){
            $('#tagihKe1').prop('disabled',true);
            $('#tagihKe2').prop('disabled',true);
               $('#vendor').prop('disabled',true);
        });


    $('#tagihKe1').click(function(){
            $('#vendor').prop('disabled',true);
        });
          $('#tagihKe2').click(function(){
               $('#vendor').prop('disabled',false);
        });

     $('#butuhparts1').click(function(){
            $('#partsMaterial1').prop('disabled',false);
             $('#parts1').prop('disabled',false);
             $('#partsMaterial10').prop('disabled',false);
             $('#parts0').prop('disabled',false);
               $('#addParts').prop('disabled',false);
                 $('#deleteParts').prop('disabled',false);


        });
          $('#butuhparts2').click(function(){
           $('#partsMaterial1').prop('disabled',true);
             $('#parts1').prop('disabled',true);
              $('#partsMaterial0').prop('disabled',true);
             $('#parts0').prop('disabled',true);
               $('#addParts').prop('disabled',true);
                 $('#deleteParts').prop('disabled',true);

        });

        $('#parts0').click(function(){
            $('#partsMaterial0').prop('checked',true);
            $('#parts1').prop('disabled',true);
        });
        $('#partsMaterial0').click(function(){
            var isChecked = $('#partsMaterial0').prop('checked');
            if(isChecked==true){
                $('#parts0').prop('disabled',false);
                $('#parts1').prop('disabled',true);
            }
        });
        $('#partsMaterial1').click(function(){
            var isChecked = $('#partsMaterial1').prop('checked');
            if(isChecked==true){
                $('#parts1').prop('disabled',false);
                $('#parts0').prop('disabled',true);
            }
        });

        function addParts(){
            var value = ""
            if($('#partsMaterial1').prop('checked')){
                value=$('#parts1').val();
            }else if($('#partsMaterial0').prop('checked')){
                value=$('#parts0').val();
            }
            if(value==""){
                alert('Anda belum memilih data yang akan ditambahkan')
            }else{
                $.ajax({
                    url:'${request.contextPath}/BPJobInstruction/getDataGoods',
                    type: "POST",
                    data: { value : value},
                    success : function(data){
                        if(data.length>0){
                            var checkID =[];
                            $('#sendProblemFinding-table tbody .row-select').each(function() {
                                var id = $(this).next("input:hidden").val();
                                checkID.push(id);
                            });
                            var hasil = true
                            if(checkID.length>0){
                                for(var c=0 ;c < checkID.length;c++){
                                    if(checkID[c]==data[0].kode){
                                        hasil=false
                                        if(hasil==false){
                                        break;
                                        }
                                    }
                                }
                            }
                            if(hasil){
                                orderMaterialTable.fnAddData({
                                    'id': data[0].id,
                                    'kode': data[0].kode,
                                    'nama': data[0].nama,
                                    'qty': 0
                                });
                            }else{
                                alert('Data yang anda masukan sudah ada');
                            }
                        }
                    }
                });
            }

        }

        function deleteParts(){
            var checkID =[];
            var a=parseInt(-1);
            $('#sendProblemFinding-table tbody .row-select').each(function() {
                a=parseInt(a)+1;
                if(this.checked){
                    checkID.push(a);
                }
            });

            if(checkID.length<1){
                alert('Anda belum memilih data yang akan dihapus');
            }else{
                for(var b = (checkID.length-1) ; b >= 0 ; b--){
                    orderMaterialTable.fnDeleteRow(parseInt(checkID[b]));
                }
            }
            checkID = [];
        }

        function saveData(){
            var nomorWO = $('#nomorWO').val();
            var id = $('#id').val();
            var idRole = []
            idRole = $('#kirimAlertKe').val();
            var staProblem = "";
            var staTambahWaktu = "";
            var staTambahJob = "";
            var staPauseJob = "";
            var staButuhParts = "";
            var staButuhCat = "";
            var tagihKe = "";
            var catatanTambah = $('#catatanTambah').val();
            var durasiTambah = $('#durasiTambah').val();
            var namaJobTambah = $('#namaJobTambah').val();
            var vendor = $('#vendor').val();
            var staKendaraan = $('#staKendaraan').val();
            var tangalSend = $('#tangalSend').val();
            if($('#jenis1').prop('checked')){staProblem=$('#jenis1').val();}
            if($('#jenis2').prop('checked')){staProblem=$('#jenis2').val();}
            if($('#waktu1').prop('checked')){staTambahWaktu=$('#waktu1').val();}
            if($('#waktu2').prop('checked')){staTambahWaktu=$('#waktu1').val();}
            if($('#tambahJob1').prop('checked')){staTambahJob=$('#tambahJob1').val();}
            if($('#tambahJob2').prop('checked')){staTambahJob=$('#tambahJob2').val();}
            if($('#pauseJob1').prop('checked')){staPauseJob=$('#pauseJob1').val();}
            if($('#pauseJob2').prop('checked')){staPauseJob=$('#pauseJob2').val();}
            if($('#butuhparts1').prop('checked')){staButuhParts=$('#butuhparts1').val();}
            if($('#butuhparts2').prop('checked')){staButuhParts=$('#butuhparts1').val();}
            if($('#butuhCat1').prop('checked')){staButuhCat=$('#butuhCat1').val();}
            if($('#butuhCat2').prop('checked')){staButuhCat=$('#butuhCat1').val();}
            if($('#tagihKe1').prop('checked')){tagihKe=$('#tagihKe1').val();}
            if($('#tagihKe2').prop('checked')){tagihKe=$('#tagihKe1').val();}
            var checkID = [], checkQty = []
            $('#sendProblemFinding-table tbody .row-select').each(function() {
                var id = $(this).next("input:hidden").val();
                var nRow = $(this).parents('tr')[0];
                var aData = orderMaterialTable.fnGetData(nRow);
                var qtyInput = $('#qty_'+aData['id']).val();
                checkID.push(id);
                checkQty.push(qtyInput);
            });
            if(staProblem=="" || staProblem==null || staTambahWaktu=="" || staTambahWaktu==null || staTambahJob=="" || staTambahJob==null || staPauseJob=="" || staPauseJob==null
                || staButuhParts=="" || staButuhParts==null || staButuhCat=="" || staButuhCat==null || tagihKe=="" || tagihKe==null){
                alert('Ada data yang masih kosong');
            }else{
                var ids = JSON.stringify(idRole);
                var ids2 = JSON.stringify(checkID);
                var qtys = JSON.stringify(checkQty);
                $.ajax({
                url:'${request.contextPath}/BPJobInstruction/saveData',
                type: "POST", // Always use POST when deleting data
                data : {noWO : nomorWO , idRole : ids, idGoods : ids2, qtys : qtys , id : id, staProblem: staProblem , staTambahWaktu : staTambahWaktu,
                staTambahJob : staTambahJob,staPauseJob : staPauseJob, staButuhParts : staButuhParts, staButuhCat : staButuhCat, tagihKe : tagihKe,
                catatanTambah : catatanTambah, durasiTambah : durasiTambah,namaJobTambah : namaJobTambah,vendor:vendor,staKendaraan:staKendaraan,tanggalSend:tangalSend},
                success : function(data){
                    toastr.success('<div>Data berhasil disimpan.</div>');
                    expandTableLayout();
                    reloadBPJobInstructionTable();
                },
            error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
            }
            });

            }
        }

    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="bPJobInstruction.sendProblemFinding.label" default="Send Problem Finding" /></span>
</div>
<div class="box">
    <div class="span12" id="sendProblemFinding-table">

            <div class="row-fluid">

                <div id="kiri" class="span5">
                    <table style="width: 100%;">
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.t401NoWo.label" default="Nomor WO" />
                            </td>
                            <td>
                                <g:textField style="width:100%" value="${reception?.t401NoWO}" name="nomorWO" id="nomorWO" readonly="" />
                                <g:hiddenField name="id" id="id" value="${actual?.id}" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.noPolisi.label" default="Nomor Polisi" />
                            </td>
                            <td>
                                <g:textField style="width:100%"  name="nomorPolisi" readonly="" value="${nopol}"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.modelKendaraan.label" default="Model Kendaraan" />
                            </td>
                            <td>
                                <g:textField style="width:100%" name="modelKendaraan" value="${model}" disabled="disabled"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.namaStall.label" default="Nama Stall" />
                            </td>
                            <td>
                                <g:textField style="width:100%" name="namaStall" value="${actual?.stall?.m022NamaStall}" readonly=""/>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.teknisi.label" default="Teknisi" />
                            </td>
                            <td>
                                <g:textField style="width:100%" name="teknisi" value="${actual?.jpb?.namaManPower ? actual?.jpb?.namaManPower.t015NamaLengkap+" ("+actual?.jpb?.namaManPower?.manPowerDetail?.m015Inisial+")" : ""}" readonly=""/>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.job.label" default="Job" />
                            </td>
                            <td>
                                <g:textArea style="width:100%;resize: none" name="job" value="${job}" readonly="" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.proses.label" default="Proses" />
                            </td>
                            <td>
                                <g:textField  style="width:100%" name="proses" value="${actual?.stall?.namaProsesBP?.m190NamaProsesBP}" readonly="" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.catatan.label" default="Catatan Problem" />
                            </td>
                            <td>
                                <g:textArea style="width:100%;resize: none" name="catatan" value="${actual?.t452Ket}" readonly="" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.tanggalProblem.label" default="Tangal Problem" />
                            </td>
                            <td>
                                <g:textField  style="width:100%" name="tanggalProblem" value="${actual?.t452TglJam}" readonly="" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.jenisProblem.label" default="Jenis Problem *" />
                            </td>
                            <td>
                                <input type="radio" name="jenisProblem" id="jenis1" value="1" />Teknis &nbsp;&nbsp
                                <input type="radio" name="jenisProblem" id="jenis2" value="0" />Non-Teknis &nbsp;&nbsp
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.catatanTambah.label" default="Catatan Tambahan" />
                            </td>
                            <td>
                                <g:textArea style="width:100%;resize: none" name="catatanTambah" id="catatanTambah" value=""/>
                            </td>
                        </tr>
                    </table>
                </div>

                <div id="kanan" class="span7">
                    <table>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.staTambahWaktu.label" default="Perlu Tambahan Waktu?" />
                            </td>
                            <td>
                                <input type="radio" name="staTambahWaktu" id="waktu1" value="1" />Ya &nbsp;&nbsp
                                <input type="radio" name="staTambahWaktu" id="waktu2" value="0" />Tidak &nbsp;&nbsp
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                Durasi <g:textField name="durasiTambah" style="width: 80px" id="durasiTambah" onkeypress="return isNumberKey(event);" /> Menit
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.staTambahJob.label" default="Butuh Tambahan Job?" />
                            </td>
                            <td>
                                <input type="radio" name="staTambahJob" id="tambahJob1" value="1" />Ya &nbsp;&nbsp
                                <input type="radio" name="staTambahJob" id="tambahJob2" value="0" />Tidak &nbsp;&nbsp
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                Nama Job <g:textField name="namaJobTambah" id="namaJobTambah" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.staPauseJob.label" default="Butuh Pause Job?" />
                            </td>
                            <td>
                                <input type="radio" name="staPauseJob" id="pauseJob1" value="1" />Ya &nbsp;&nbsp
                                <input type="radio" name="staPauseJob" id="pauseJob2" value="0" />Tidak &nbsp;&nbsp
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.staButuhParts.label" default="Butuh Parts/Material?" />
                            </td>
                            <td>
                                <input type="radio" name="staButuhParts" id="butuhparts1" value="1" />Ya &nbsp;&nbsp
                                <input type="radio" name="staButuhParts" id="butuhparts2" value="0" />Tidak &nbsp;&nbsp
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 100%">
                                <span style="text-decoration: underline;">Order Parts/Material</span>
                                <br/><br/>
                                <br/>
                                <input type="radio" name="partsMaterial" id="partsMaterial1" value="1"  required="true" /> Kode  &nbsp;
                                    <g:textField name="parts1" id="parts1" class="typeahead" maxlength="11"  autocomplete="off"/> &nbsp;
                                <input type="radio" name="partsMaterial" id="partsMaterial0" value="0"  required="true" /> Nama  &nbsp;
                                    <g:textField name="parts0" id="parts0" class="typeahead" maxlength="11"  autocomplete="off"/>
                                    <g:field type="button" onclick="addParts()" style="padding: 3px;"
                                             class="btn success" name="addParts" id="addParts" value="+" />
                                    <g:field type="button" onclick="deleteParts()" style="padding: 3px;"
                                             class="btn cancel" name="deleteParts" id="deleteParts" value="delete" />
                                <br/>
                                <div >
                                    <g:render template="dataTablesParts" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.staButuhCat.label" default="Butuh Cat Ulang?" />
                            </td>
                            <td>
                                <input type="radio" name="staButuhCat" id="butuhCat1" value="1" />Ya &nbsp;&nbsp
                                <input type="radio" name="staButuhCat" id="butuhCat2" value="0" />Tidak &nbsp;&nbsp
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.tagihKe.label" default="Tagih Kepada" />
                            </td>
                            <td>
                                <input type="radio" name="tagihKe" id="tagihKe1" value="1" />Bengkel (vendor) &nbsp;&nbsp
                                <input type="radio" name="tagihKe" id="tagihKe2" value="0" />Vendor &nbsp;&nbsp
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.vendor.label" default="Vendor Cat" />
                            </td>
                            <td>
                                <g:select name="vendor" id="vendor" from="${VendorCat.createCriteria().list {eq("staDel","0")}}" optionKey="id" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.staKendaraan.label" default="Status Kendaraan" />
                            </td>
                            <td>
                                <g:select name="staKendaraan" id="staKendaraan" from="${StatusKendaraan.createCriteria().list {eq("staDel","0")}}" optionKey="id" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.kirimAlertKe.label" default="Kirim Alert Kepada" />
                            </td>
                            <td>
                                <g:checkBox name="kirimAlert1" id="kirimAlert1" value="1" checked="false" /> SA <br>
                                <g:checkBox name="kirimAlert2" id="kirimAlert2" value="2" checked="false" /> Controller
                                %{--<g:select name="kirimAlertKe" id="kirimAlertKe" from="${Role.createCriteria().list {eq("staDel","0")}}" size="5" optionKey="id" multiple="true" />--}%
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.tangalSend.label" default="Tangal Send" />
                            </td>
                            <td>
                                <g:textField name="tangalSend" id="tangalSend" readonly="" value="${tanggalSend}"/>
                            </td>
                        </tr>
                    </table>
                </div>

            </div>
    </div>
    <div>
        <g:field type="button" style="width: 100px" class="btn btn-primary create" onclick="saveData();"
                 name="btnOk" id="btnOk" value="${message(code: 'default.addJob.label', default: 'Send')}" />
        &nbsp;&nbsp;&nbsp;
        <g:field type="button" style="width: 100px" class="btn cancel" onclick="expandTableLayout();"
                 name="btnClose" id="btnClose" value="${message(code: 'default.addParts.label', default: 'Close')}" />
    </div>
</div>
</body>
</html>
