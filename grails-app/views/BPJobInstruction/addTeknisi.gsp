
<%@ page import="com.kombos.administrasi.Stall; com.kombos.administrasi.NamaManPower;" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="bPJobInstruction.updateWO.label" default="Add Teknisi" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;
	var save;
	$(function(){


   });
    save = function(){
            var nomorWO = $('#nomorWO').val();
            var tanggalTambah = $('#tanggalTambah').val();
            var teknisi = $('#teknisi').val();
            var idJpb = $('#idJpb').val();
            var stall = $('#stall').val();
            if(teknisi=="" || teknisi==null){
                alert('Anda belum melakukan perubahan');
            }else{
                $.ajax({
                url:'${request.contextPath}/BPJobInstruction/saveTeknisi',
                type: "POST", // Always use POST when deleting data
                data : {noWO : nomorWO ,tanggalTambah:tanggalTambah, teknisi : teknisi, stall : stall,idJpb : idJpb},
                success : function(data){
                    toastr.success('<div>Data berhasil disimpan.</div>');
                    expandTableLayout();
                    reloadBPJobInstructionTable();
                },
            error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
            }
            });

            }

    }
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="bPJobInstruction.updateWO.label" default="Add Teknisi" /></span>
</div>
<div class="box">
    <table>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.t401NoWo.label" default="Nomor WO" />
            </td>
            <td>
                <g:hiddenField name="idJpb" id="idJpb" value="${jpb?.id}" />
                <g:textField  name="nomorWO" id="nomorWO" readonly="" value="${reception?.t401NoWO}" />
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.noPolisi.label" default="Nomor Polisi" />
            </td>
            <td>
                <g:textField  name="nomorPolisi" id="nomorPolisi" readonly="" value="${reception?.historyCustomerVehicle?.fullNoPol}"/>
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Model Kendaraan" />
            </td>
            <td>
                <g:textField  name="modelKendaraan" id="modelKendaraan" readonly="" value="${reception?.historyCustomerVehicle?.fullModelCode?.modelName?.m104NamaModelName}"/>
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Nama Stall" />
            </td>
            <td>
                <g:select name="stall" id="stall"
                          from="${Stall.createCriteria().list {
                                    eq("staDel","0")
                                    eq("companyDealer", session.userCompanyDealer)
                                }}" optionKey="id" noSelection="['':'Pilih Nama Stall']"  value="${jpb?.stall?.id}" />
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Tanggal WO" />
            </td>
            <td>
                <g:textField  name="tanggalWO" id="tanggalWO" readonly="" value="${tanggalWO}" />
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Job" />
            </td>
            <td>
                <g:textArea name="job" id="job" style="resize: none" value="${job}" readonly="" />
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Proses" />
            </td>
            <td>
                <g:textField  name="proses" id="proses" readonly="" value="${proses}" />
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Teknisi" />
            </td>
            <td>
                <g:select  name="teknisi" id="teknisi"
                           from="${NamaManPower.createCriteria().list {
                                        eq("staDel","0")
                                        eq("companyDealer", session.userCompanyDealer)
                                        manPowerDetail{
                                            manPower{
                                                inList("m014Inisial", ["TKBD", "TKPT"])
                                            }
                                        }
                                    }}" noSelection="['':'Pilih Nama Teknisi']" optionKey="id" optionValue="${{it?.manPowerDetail?.m015Inisial+" - "+it?.t015NamaLengkap}}" />
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Tanggal Tambah" />
            </td>
            <td>
                <g:textField  name="tanggalTambah" id="tanggalTambah" readonly="" value="${tanggalTambah}" />
            </td>
        </tr>
    </table>
    <div>
        <g:field type="button" style="width: 100px" class="btn btn-primary create" onclick="save();"
                 name="btnOk" id="btnOk" value="${message(code: 'default.addJob.label', default: 'OK')}" />
        &nbsp;&nbsp;&nbsp;
        <g:field type="button" style="width: 100px" class="btn cancel" onclick="expandTableLayout();"
                 name="btnClose" id="btnClose" value="${message(code: 'default.addParts.label', default: 'Close')}" />
    </div>
</div>
</body>
</html>
