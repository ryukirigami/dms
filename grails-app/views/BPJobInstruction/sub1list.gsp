
<%@ page import="com.kombos.parts.StokOPName" %>

<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
<r:require modules="baseapplayout" />
<g:javascript>
    var bPJobInstructionSub1Table;
$(function(){
    var anOpen = [];
	$('#bPJobInstruction_datatables_sub1_${idTable} td.sub1control').live('click',function () {
		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
  		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = bPJobInstructionSub1Table.fnGetData(nTr);
    		//console.log(oData);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST', 
	   			data : oData,
	   			url:'${request.contextPath}/BPJobInstruction/sub2list',
	   			success:function(data,textStatus){
	   				var nDetailsRow = bPJobInstructionSub1Table.fnOpen(nTr,data,'details');
    				$('div.inner2Details', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
    		
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.inner2Details', $(nTr).next()[0]).slideUp( function () {
      			bPJobInstructionSub1Table.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

	bPJobInstructionSub1Table = $('#bPJobInstruction_datatables_sub1_${idTable}').dataTable({
		"sScrollX": "1205px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                if(aData.staTambah == 1 && tambahJob=="Ya"){
                     $(nRow).addClass("background-color-green");
                }
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSub1List")}",
		"aoColumns": [
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"11px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "sClass": "sub1control center",
   "bSortable": false,
   "sWidth":"10px",
   "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "",
	"mDataProp": "namaJob",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"400px",
	"bVisible": true
}
,

{
	"sName": "",
	"mDataProp": "startJob",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"455px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "stopJob",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"335px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "t401NoWO",
	"aTargets": [3],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"0px",
	"bVisible": false
}
,
{
	"sName": "",
	"mDataProp": "staTambah",
	"aTargets": [4],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"0px",
	"bVisible": false
}

,
{
	"sName": "",
	"mDataProp": "idJob",
	"aTargets": [5],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"0px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						aoData.push(
									{"name": 't401NoWO', "value": "${t401NoWO}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>

</head>
    <body>
        <div class="innerDetails">
            <table id="bPJobInstruction_datatables_sub1_${idTable}" cellpadding="0" cellspacing="0" border="0"
               class="display table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th></th>

                    <th></th>

                    <th style="border-bottom: none;padding: 5px;">
                        <div><g:message code="bPJobInstruction.namaJob.label" default="Nama Job" /></div>
                    </th>

                    <th style="border-bottom: none;padding: 5px;">
                        <div><g:message code="bPJobInstruction.start.label" default="Start" /></div>
                    </th>

                    <th style="border-bottom: none;padding: 5px;">
                        <div><g:message code="bPJobInstruction.stop.label" default="Stop" /></div>
                    </th>

                </tr>
                </thead>
            </table>

        </div>
    </body>
</html>


			
