
<%@ page import="com.kombos.parts.Satuan; com.kombos.administrasi.PartJob" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

</g:javascript>
<table id="partJob_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover  table-selectable" 
	width="100%">
	<thead>
		<tr>

			
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="partJob.fullModelCode.label" default="Full Model" /></div>
			</th>
			
			
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="partJob.operation.label" default="Job" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="partJob.namaProsesBP.label" default="Proses" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="partJob.goods.label" default="Kode Parts" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="partJob.t112Jumlah.label" default="Jumlah" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="partJob.satuan.label" default="Satuan" /></div>
			</th>


 

			 
		
		</tr>
		<tr>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_fullModelCode" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_fullModelCode" class="search_init" />
				</div>
			</th>
	
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_operation" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_operation" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_namaProsesBP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_namaProsesBP" class="search_init" />
				</div>
			</th>
	 
		
			<th style="border-top: none;padding: 5px;">
				<div id="filter_goods" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_goods" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t112Jumlah" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t112Jumlah" class="search_init" onkeypress="return isNumberKey(event)"/>
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_satuan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					%{--<input type="text" name="search_satuan" class="search_init" />--}%
                    <g:select name="search_satuan" id="search_satuan" from="${Satuan.createCriteria().list { eq('staDel','0')}}" optionKey="id"  noSelection="${['':'']}" style="width:94%"  />
				</div>
			</th>
	

		</tr>
	</thead>
</table>

<g:javascript>
var partJobTable;
var reloadPartJobTable;
$(function(){
	
	reloadPartJobTable = function() {
		partJobTable.fnDraw();
	}
 $('#search_satuan').change(function(){
        partJobTable.fnDraw();
    });
	
    var recordsGoodsPerPage = [];
    var anGoodsSelected;
    var jmlRecGoodsPerPage=0;
    var id;
    
$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	partJobTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	partJobTable = $('#partJob_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsGoods = $("#partJob_datatables tbody .row-select");
            var jmlGoodsCek = 0;
            var nRow;
            var idRec;
            rsGoods.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsGoodsPerPage[idRec]=="1"){
                    jmlGoodsCek = jmlGoodsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsGoodsPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecGoodsPerPage = rsGoods.length;
            if(jmlGoodsCek==jmlRecGoodsPerPage && jmlRecGoodsPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [



{
	"sName": "fullModelCode",
	"mDataProp": "fullModelCode",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "operation",
	"mDataProp": "operation",
	"aTargets": [1],
	
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}
,
{
	"sName": "namaProsesBP",
	"mDataProp": "namaProsesBP",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
 
,
{
	"sName": "goods",
	"mDataProp": "goods",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t112Jumlah",
	"mDataProp": "t112Jumlah",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "satuan",
	"mDataProp": "satuan",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}




],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var operation = $('#filter_operation input').val();
						if(operation){
							aoData.push(
									{"name": 'sCriteria_operation', "value": operation}
							);
						}
	
						var fullModelCode = $('#filter_fullModelCode input').val();
						if(fullModelCode){
							aoData.push(
									{"name": 'sCriteria_fullModelCode', "value": fullModelCode}
							);
						}
	
						var goods = $('#filter_goods input').val();
						if(goods){
							aoData.push(
									{"name": 'sCriteria_goods', "value": goods}
							);
						}
	
						var t112Jumlah = $('#filter_t112Jumlah input').val();
						if(t112Jumlah){
							aoData.push(
									{"name": 'sCriteria_t112Jumlah', "value": t112Jumlah}
							);
						}
	
						var satuan = $('#filter_satuan select').val();
						if(satuan){
							aoData.push(
									{"name": 'sCriteria_satuan', "value": satuan}
							);
						}
	
						var namaProsesBP = $('#filter_namaProsesBP input').val();
						if(namaProsesBP){
							aoData.push(
									{"name": 'sCriteria_namaProsesBP', "value": namaProsesBP}
							);
						}
	
					 
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#partJob_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsGoodsPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsGoodsPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#partJob_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsGoodsPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anGoodsSelected = partJobTable.$('tr.row_selected');

            if(jmlRecGoodsPerPage == anGoodsSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsGoodsPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
