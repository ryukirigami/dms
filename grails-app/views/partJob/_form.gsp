<%@ page import="com.kombos.parts.Satuan; com.kombos.parts.Goods; com.kombos.administrasi.NamaProsesBP; com.kombos.administrasi.Operation; com.kombos.administrasi.FullModelCode; com.kombos.administrasi.PartJob" %>

<g:javascript>
		$(function(){
			$('#kodeGoods').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/partJob/listGoods', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});

			$('#kodeFMC').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/partJob/listFullModelCode', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});
		function detailGoods(){
                var noGoods = $('#kodeGoods').val();
                $.ajax({
                    url:'${request.contextPath}/partJob/detailGoods?noGoods='+noGoods,
                    type: "POST", // Always use POST when deleting data
                    success : function(data){
                        $('#idGoods').val("");
                        $('#namaGoods').val("");
                        if(data.hasil=="ada"){
                            $('#idGoods').val(data.id);
                            console.log("idGoods : " + data.id)
                        }
                    }
                })
        }

        function detailFMC(){
                var noFMC = $('#kodeFMC').val();
                $.ajax({
                    url:'${request.contextPath}/partJob/detailFMC?noFMC='+noFMC,
                    type: "POST", // Always use POST when deleting data
                    success : function(data){
                        if(data.hasil=="ada"){
                            $('#idFMC').val("");
                            $('#idFMC').val(data.id);
                        }
                    }
                })
        }
function isNumberKey(evt)
{
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	return false;
	
	return true;
}

</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: partJobInstance, field: 'fullModelCode', 'error')} required">
	<label class="control-label" for="fullModelCode">
		<g:message code="partJob.fullModelCode.label" default="Full Model" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:select id="fullModelCode" name="fullModelCode.id" from="${FullModelCode.list()}" optionKey="id" required="" value="${partJobInstance?.fullModelCode?.id}" class="many-to-one"/>--}%
    <g:textField name="kodeFMC" id="kodeFMC" class="typeahead" maxlength="11" value="${partJobInstance?.fullModelCode?.t110FullModelCode}" autocomplete="off" onblur="detailFMC();"/>
    <g:hiddenField name="fullModelCode.id" id="idFMC"  value="${partJobInstance?.fullModelCode?.id}" />
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: partJobInstance, field: 'operation', 'error')} required">
	<label class="control-label" for="operation">
		<g:message code="partJob.operation.label" default="Job" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="operation" name="operation.id" from="${Operation.list()}" optionKey="id" required="" value="${partJobInstance?.operation?.id}" class="many-to-one"/>
	</div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: partJobInstance, field: 'namaProsesBP', 'error')}">
	<label class="control-label" for="namaProsesBP">
		<g:message code="partJob.namaProsesBP.label" default="Proses" />
	</label>
	<div class="controls">
	<g:select id="namaProsesBP" name="namaProsesBP.id" from="${NamaProsesBP.list()}" optionKey="id" value="${partJobInstance?.namaProsesBP?.id}" class="many-to-one" noSelection="['':'pilih']"/>
	</div>
</div>
 
<div class="control-group fieldcontain ${hasErrors(bean: partJobInstance, field: 'goods', 'error')} required">
	<label class="control-label" for="goods">
		<g:message code="partJob.goods.label" default="Kode parts" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:select id="goods" name="goods.id" from="${Goods.list()}" optionKey="id" required="" value="${partJobInstance?.goods?.id}" class="many-to-one"/>--}%
    <g:textField name="kodeGoods" id="kodeGoods" class="typeahead" maxlength="11" value="${partJobInstance?.goods?.m111ID?partJobInstance?.goods?.m111ID?.trim()+' || '+partJobInstance?.goods?.m111Nama:""}" autocomplete="off" onblur="detailGoods();"/>
    <g:hiddenField name="goods.id" id="idGoods"  value="${partJobInstance?.goods?.id}" />
	</div>
</div>

%{--<div class="control-group fieldcontain ${hasErrors(bean: partJobInstance, field: 'goods', 'error')} required">--}%
    %{--<label class="control-label" for="goods">--}%
        %{--<g:message code="partJob.goods.label" default="Nama parts" />--}%
        %{--<span class="required-indicator">*</span>--}%
    %{--</label>--}%
    %{--<div class="controls">--}%
        %{--<g:select id="goods" name="goods.id" from="${Goods.list()}" optionKey="id" required="" value="${partJobInstance?.goods?.id}" class="many-to-one"/>--}%
        %{--<g:textField name="namaGoods" id="namaGoods"  value="${partJobInstance?.goods?.m111Nama}" autocomplete="off" readonly="readonly"/>--}%
    %{--</div>--}%
%{--</div>--}%

<div class="control-group fieldcontain ${hasErrors(bean: partJobInstance, field: 't112Jumlah', 'error')} required">
	<label class="control-label" for="t112Jumlah">
		<g:message code="partJob.t112Jumlah.label" default="Jumlah" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t112Jumlah" value="${fieldValue(bean: partJobInstance, field: 't112Jumlah')}" required="" onkeypress="return isNumberKey(event)"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partJobInstance, field: 'satuan', 'error')} required">
	<label class="control-label" for="satuan">
		<g:message code="partJob.satuan.label" default="Satuan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="satuan" name="satuan.id" from="${Satuan.list()}" optionKey="id" required="" value="${partJobInstance?.satuan?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partJobInstance, field: 'm102Foto', 'error')} ">
	<label class="control-label" for="m102Foto">
		<g:message code="partJob.m102Foto.label" default="Upload Gambar" />
		
	</label>
	<div class="controls">
	<input type="file" id="m102Foto" name="m102Foto" />
	</div>
</div>


