

<%@ page import="com.kombos.administrasi.PartJob" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'partJob.label', default: 'Job Parts')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deletePartJob;

$(function(){ 
	deletePartJob=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/partJob/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadPartJobTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-partJob" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="partJob"
			class="table table-bordered table-hover">
			<tbody>
	
				<g:if test="${partJobInstance?.fullModelCode}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="fullModelCode-label" class="property-label"><g:message
					code="partJob.fullModelCode.label" default="Full Model" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="fullModelCode-label">
						%{--<ba:editableValue
								bean="${partJobInstance}" field="fullModelCode"
								url="${request.contextPath}/PartJob/updatefield" type="text"
								title="Enter fullModelCode" onsuccess="reloadPartJobTable();" />--}%
							
								%{--<g:link controller="fullModelCode" action="show" id="${partJobInstance?.fullModelCode?.id}">${partJobInstance?.fullModelCode?.encodeAsHTML()}</g:link>--}%
                        <g:fieldValue bean="${partJobInstance?.fullModelCode}" field="t110FullModelCode"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partJobInstance?.operation}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="operation-label" class="property-label"><g:message
					code="partJob.operation.label" default="Job" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="operation-label">
						%{--<ba:editableValue
								bean="${partJobInstance}" field="operation"
								url="${request.contextPath}/PartJob/updatefield" type="text"
								title="Enter operation" onsuccess="reloadPartJobTable();" />--}%
							
								%{--<g:link controller="operation" action="show" id="${partJobInstance?.operation?.id}">${partJobInstance?.operation?.encodeAsHTML()}</g:link>--}%
                        <g:fieldValue bean="${partJobInstance?.operation}" field="m053NamaOperation"/>
						</span></td>
					
				</tr>
				</g:if>
				
			<g:if test="${partJobInstance?.namaProsesBP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="namaProsesBP-label" class="property-label"><g:message
					code="partJob.namaProsesBP.label" default="Proses" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="namaProsesBP-label">
						%{--<ba:editableValue
								bean="${partJobInstance}" field="namaProsesBP"
								url="${request.contextPath}/PartJob/updatefield" type="text"
								title="Enter namaProsesBP" onsuccess="reloadPartJobTable();" />--}%
							
								%{--<g:link controller="namaProsesBP" action="show" id="${partJobInstance?.namaProsesBP?.id}">${partJobInstance?.namaProsesBP?.encodeAsHTML()}</g:link>--}%
                        <g:fieldValue bean="${partJobInstance?.namaProsesBP}" field="m190NamaProsesBP"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partJobInstance?.goods}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="goods-label" class="property-label"><g:message
					code="partJob.goods.label" default="Nama Parts" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="goods-label">
						%{--<ba:editableValue
								bean="${partJobInstance}" field="goods"
								url="${request.contextPath}/PartJob/updatefield" type="text"
								title="Enter goods" onsuccess="reloadPartJobTable();" />--}%
							
								%{--<g:link controller="goods" action="show" id="${partJobInstance?.goods?.id}">${partJobInstance?.goods?.encodeAsHTML()}</g:link>--}%
                        <g:fieldValue bean="${partJobInstance?.goods}" field="m111Nama"/>
						</span></td>
					
				</tr>
				</g:if>
			
			
				<g:if test="${partJobInstance?.t112Jumlah}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t112Jumlah-label" class="property-label"><g:message
					code="partJob.t112Jumlah.label" default="Jumlah" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t112Jumlah-label">
						%{--<ba:editableValue
								bean="${partJobInstance}" field="t112Jumlah"
								url="${request.contextPath}/PartJob/updatefield" type="text"
								title="Enter t112Jumlah" onsuccess="reloadPartJobTable();" />--}%
							
								<g:fieldValue bean="${partJobInstance}" field="t112Jumlah"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partJobInstance?.satuan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="satuan-label" class="property-label"><g:message
					code="partJob.satuan.label" default="Satuan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="satuan-label">
						%{--<ba:editableValue
								bean="${partJobInstance}" field="satuan"
								url="${request.contextPath}/PartJob/updatefield" type="text"
								title="Enter satuan" onsuccess="reloadPartJobTable();" />--}%
							
                        <g:fieldValue bean="${partJobInstance?.satuan}" field="m118Satuan1"/>
						</span></td>
					
				</tr>
				</g:if>
			
				
				<g:if test="${partJobInstance?.m102Foto}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m102Foto-label" class="property-label"><g:message
					code="partJob.m102Foto.label" default="Foto" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m102Foto-label">
						%{--<ba:editableValue
								bean="${partJobInstance}" field="m102Foto"
								url="${request.contextPath}/PartJob/updatefield" type="text"
								title="Enter m102Foto" onsuccess="reloadPartJobTable();" />--}%
						 <img class="" src="${createLink(controller:'partJob', action:'showFoto', id : partJobInstance.id, random : logostamp)}" />	
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partJobInstance?.m102FotoImageMime}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m102FotoImageMime-label" class="property-label"><g:message
					code="partJob.m102FotoImageMime.label" default="File Type" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m102FotoImageMime-label">
						%{--<ba:editableValue
								bean="${partJobInstance}" field="m102FotoImageMime"
								url="${request.contextPath}/PartJob/updatefield" type="text"
								title="Enter m102FotoImageMime" onsuccess="reloadPartJobTable();" />--}%
							
								<g:fieldValue bean="${partJobInstance}" field="m102FotoImageMime"/>
							       <a href="${createLink(controller:'partJob', action:'showFoto', id : partJobInstance.id, random : logostamp)}" >Download</a>
						</span></td>
					
				</tr>
				</g:if>
			 
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${partJobInstance?.id}"
					update="[success:'partJob-form',failure:'partJob-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deletePartJob('${partJobInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
