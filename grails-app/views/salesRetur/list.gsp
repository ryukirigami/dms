
<%@ page import="com.kombos.parts.SalesRetur" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'salesRetur.label', default: 'Retur Penjualan')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			$(function(){ 
			
				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :
							loadPath("salesRetur/formAddParts");
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
//										massDelete('salesRetur', '${request.contextPath}/salesRetur/massdelete', reloadSalesReturTable);
									 massDelete();
									}
								});                    
							
							break;
				   }    
				   return false;
				});

                massDelete = function() {
                    var recordsToDelete = [];
                    $("#salesRetur-table tbody .row-select").each(function() {
                        if(this.checked){
                            var id = $(this).next("input:hidden").val();
                            recordsToDelete.push(id);
                        }

                    });

                    var json = JSON.stringify(recordsToDelete);

                    $.ajax({
                        url:'${request.contextPath}/salesRetur/massdelete',
                        type: "POST", // Always use POST when deleting data
                        data: { ids: json },
                        complete: function(xhr, status) {
                            reloadSalesReturTable();
                        }
                    });
                }
				show = function(id) {
					showInstance('salesRetur','${request.contextPath}/salesRetur/show/'+id);
				};
				
				edit = function(id) {
					editInstance('salesRetur','${request.contextPath}/salesRetur/edit/'+id);
				};

				shrinkTableLayout = function(){
                    $("#salesRetur-table").hide();
                    $("#salesRetur-form").css("display","block");
                }

                expandTableLayout = function(){
                    try{
                        reloadComplaintTable();
                    }catch(e){}
                    $("#salesRetur-table").show();
                    $("#salesRetur-form").css("display","none");
                }

});
        printSalesRetur = function(){
           checkSalesRetur =[];
            $("#salesRetur-table tbody .row-select").each(function() {
                if(this.checked){
                 var nRow = $(this).next("#returId").val()?$(this).next("#returId").val():"-"
                    if(nRow!="-"){
                        checkSalesRetur.push(nRow);
                    }
                }
            });
            if(checkSalesRetur.length<1 ){
                alert('Silahkan Pilih Salah Satu No. Retur Order Untuk Dicetak');
                return;
            }
           var idSalesRetur =  JSON.stringify(checkSalesRetur);
           window.location = "${request.contextPath}/salesRetur/printSalesRetur?idSalesRetur="+idSalesRetur;
        }
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="salesRetur-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
            <fieldset class="buttons controls" style="padding-top: 10px;">
                <button id='printSalesReturData' onclick="printSalesRetur();" type="button" class="btn btn-primary">Print Sales Order</button>
            </fieldset>
		</div>
		<div class="span11" id="salesRetur-form" style="display: none;"></div>
	</div>
</body>
</html>
