<%--
  Created by IntelliJ IDEA.
  User: Ahmad Fawaz
  Date: 05/12/14
  Time: 16:31
--%>
<%@ page import="com.kombos.parts.Franc; com.kombos.parts.KlasifikasiGoods; com.kombos.parts.SalesRetur" %>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="salesRetur.formAddParts.label" default="Create Retur Penjualan" /></title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>
        var doSave;
        var staDetail="1";
        var idSO = "-1"
        $(function(){

            doSave = function(){
                     var idSO = $('#idSO').val();
                     var note = $('#note').val();
                    if(confirm("Apakah Data Akan Disimpan?")){
                    $.ajax({
                        url:'${request.contextPath}/salesRetur/save',
                        type: "POST", // Always use POST when deleting data
                        data : {idSO : idSO,note:note},
                        success : function(data){
                            toastr.success('<div>Data Berhasil disimpan.</div>');
                            loadPath('salesRetur/list');
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            alert('Internal Server Error');
                        }
                    });
                    }
                checkGoods = [];
            }
            $('#doNumber').typeahead({
                source: function (query, process) {
                    return $.get('${request.contextPath}/salesRetur/listDO', { query: query }, function (data) {
                        return process(data.options);
                    });
                }
            });
        })

        function doAddParts(){
            $("#addPartsContent").empty();
		    $.ajax({
		        type:'POST',
		        url:'${request.contextPath}/salesRetur/addParts',
                success:function(data,textStatus){
                        $("#addPartsContent").html(data);
                        $("#addPartsModal").modal({
                            "backdrop" : "static",
                            "keyboard" : true,
                            "show" : true
                        }).css({'width': '900px','margin-left': function () {return -($(this).width() / 2);}});

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
   		    });
        }

        function ubahJumlah(param){
            var valueParam = param.split("#");
            var dari = valueParam[0];
            var idDari = valueParam[1];
            var val1 = $("#"+dari+idDari).val();
            var val2 = $('#jumlah_'+idDari).val();
            var nilaiSkrg = 0;
            if(dari=="qty"){
                nilaiSkrg = val1=="" || val2=="" ? 0 : parseFloat(val1)*parseFloat(val2)
                $('#jumlah_'+idDari+"").val(nilaiSkrg);
            }else{
                var nilaiDsikon = parseFloat(val1)/100*parseFloat(val2);
                nilaiSkrg = parseFloat(val2) - nilaiDsikon;
                $('#jumlah_'+idDari+"").val(nilaiSkrg);
            }
            alert(param);
        }
        function detailSO(){
            if(staDetail=="1"){
                var noSo = $('#doNumber').val();
                $.ajax({
                    url:'${request.contextPath}/salesRetur/detailSO?sorNumber='+noSo,
                    type: "POST", // Always use POST when deleting data
                    success : function(data){
                        if(data.hasil=="ada"){
                            staDetail=="0"
                            idSO = data.idSO
                            $('#idSO').val(idSO);
                            reloadTabel(idSO);
                        }
                    }
                })
            }
            idSO = "-1"
            reloadTabel(idSO);
        }

        function reloadTabel(idSOReload){
            reloadGoodsTable(idSOReload);
        }
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="salesRetur.formAddParts.label" default="Create Retur Penjualan" /></span>
</div>
<div class="box">
    <div class="span12" id="formAddParts-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <table>
            <tr>
                <td style="padding: 10px">
                    <g:message code="deliveryOrder.doNumber.label" default="Nomor Delivery Order" />
                </td>
                <td style="padding: 10px">
                    <g:textField name="doNumber" id="doNumber" onblur="detailSO();" value="${deliveryOrderInstance?.doNumber?.doNumber}" class="typeahead" autocomplete="off"/>
                </td>
            </tr>
            <tr>
                <td style="padding: 10px">
                    <g:message code="deliveryOrder.note.label" default="Catatan" />
                </td>
                <td style="padding: 10px">
                    <g:textArea name="note" id="note" value="${deliveryOrderInstance?.sorNumber?.deliveryDate}" style="resize : none" />
                </td>
            </tr>
        </table>
            <br/>
            <br/>
            <br/>
            <g:render template="dataTablesAdd" />
            <fieldset class="buttons controls">
                <a class="btn cancel" onclick="loadPath('salesRetur/index');">
                    <g:message code="default.button.cancel.label" default="Cancel" />
                </a>
                <button class="btn btn-primary" name="save" onclick="doSave();" >Save</button>
            </fieldset>
        </div>
    <div class="span7" id="formAddParts-form" style="display: none;"></div>
</div>
<div id="addPartsModal" class="modal fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content" style="width: 900px;">
            <div class="modal-body" style="max-height: 800px;">
                <div id="addPartsContent"/>
                <div class="iu-content"></div>

            </div>
            <!-- dialog buttons -->
            %{--<div class="modal-footer">--}%
            %{--<button id='closePopup' type="button" class="btn btn-primary" data-dismiss="modal">Close</button>--}%
            %{--</div>--}%
        </div>
    </div>
</div>
</body>
</html>
