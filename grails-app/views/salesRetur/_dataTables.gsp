
<%@ page import="com.kombos.parts.SalesRetur" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>


<table id="salesRetur_datatables_${idTable}" cellpadding="0" cellspacing="0" border="0"
       class="display table table-striped table-bordered table-hover" style="table-layout: fixed; ">
    <thead>
    <tr>
        <th></th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="salesRetur.sorNumber.label" default="Nomor Retur" /></div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="salesRetur.deliveryDate.label" default="Tanggal Retur" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="salesRetur.sorNumber.label" default="No Sales Order" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="salesRetur.deliveryDate.label" default="Tanggal Pengiriman" /></div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var salesReturTable;
var reloadSalesReturTable;
$(function(){
    var anOpen = [];
	$('#salesRetur_datatables_${idTable} td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = salesReturTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${request.contextPath}/salesRetur/sublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = salesReturTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});

  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			salesReturTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

	reloadSalesReturTable = function() {
		salesReturTable.fnDraw();
	}


    salesReturTable = $('#salesRetur_datatables_${idTable}').dataTable({
		"sScrollX": "97%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"bDestroy": true,
		"aoColumns": [
{
   "mDataProp": null,
   "sClass": "control center",
   "bSortable": false,
   "sWidth":"10px",
   "sDefaultContent": '<i class="icon-plus"></i>'
}
,
{
	"sName": "salesRetur",
	"mDataProp": "salesRetur",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" id="returId" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"30%",
	"bVisible": true
},

{
	"sName": "returDate",
	"mDataProp": "returDate",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"20%",
	"bVisible": true
}
,

{
	"sName": "sorNumber",
	"mDataProp": "sorNumber",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"25%",
	"bVisible": true
},

{
	"sName": "deliveryDate",
	"mDataProp": "deliveryDate",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"25%",
	"bVisible": true
}

],
    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                $.ajax({ "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData ,
                    "success": function (json) {
                        fnCallback(json);
                       },
                    "complete": function () {
                       }
                });
            }
});
});

</g:javascript>
