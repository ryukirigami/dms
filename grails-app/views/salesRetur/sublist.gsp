
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
<r:require modules="baseapplayout" />
<g:javascript>
    var salesReturSubTable_${idTable};
$(function(){

	salesReturSubTable_${idTable} = $('#salesRetur_datatables_sub_${idTable}').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubList")}",
		"aoColumns": [
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"10px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "sClass": "subcontrol center",
   "bSortable": false,
   "sWidth":"10px",
   "sDefaultContent": ''
},
{
	"sName": "goods",
	"mDataProp": "kodeGoods",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"250px",
	"bVisible": true
}
,

{
	"sName": "goods",
	"mDataProp": "namaGoods",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"250px",
	"bVisible": true
}
,
{
	"sName": "goods",
	"mDataProp": "qty",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "goods",
	"mDataProp": "stockPesan",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"250px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						aoData.push(
									{"name": 'salesRetur', "value": "${salesRetur}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>

</head>
    <body>
        <div class="innerDetails">
            <table id="salesRetur_datatables_sub_${idTable}" cellpadding="0" cellspacing="0" bretur="0"
               class="display table table-striped table-bretured table-hover">
                <thead>
                <tr>
                    <th></th>

                    <th></th>

                    <th style="bretur-bottom: none;padding: 5px;">
                        <div><g:message code="salesRetur.goods.kode.label" default="Kode Part" /></div>
                    </th>

                    <th style="bretur-bottom: none;padding: 5px;">
                        <div><g:message code="salesRetur.goods.nama.label" default="Nama Part" /></div>
                    </th>

                    <th style="bretur-bottom: none;padding: 5px;">
                        <div><g:message code="salesRetur.goods.lokasi.label" default="Jumlah Retur" /></div>
                    </th>

                    <th style="bretur-bottom: none;padding: 5px;">
                        <div><g:message code="salesRetur.goods.lokasi.label" default="Qty Order" /></div>
                    </th>

                </tr>
                </thead>
            </table>

        </div>
    </body>
</html>


			
