<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
		<g:javascript>
var POSubletSubTable;
$(function(){ 

    $('#POSublet_datatables_sub_${idTable} a.edit').live('click', function (e) {
        e.preventDefault();
         
        var nRow = $(this).parents('tr')[0];
         
        if ( nEditing !== null && nEditing != nRow ) {
            restoreRow( POSubletSubTable, nEditing );
            editRow( POSubletSubTable, nRow );
            nEditing = nRow;
        }
        else if ( nEditing == nRow && this.innerHTML == "Save" ) {
            saveRow( POSubletSubTable, nEditing );
            nEditing = null;
        }
        else {
            editRow( POSubletSubTable, nRow );
            nEditing = nRow;
        }
    } );
    if(POSubletSubTable)
    	POSubletSubTable.dataTable().fnDestroy();
    POSubletSubTable = $('#POSublet_datatables_sub_${idTable}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubList")}",
		"aoColumns": [
{
               "sName": "naon",
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": ''
},
{
               "sName": "naon",
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": ''
},
{
	"sName": "",
	"mDataProp": "nomorWO",
	"aTargets": [0],
    "mRender": function ( data, type, row ) {
	    return '<input type="checkbox" class="pull-left row-select2" aria-label="Row '+data+'" title="Select this"><input type="hidden" value="'+data+'">&nbsp;&nbsp;'+data;
    },
	"bSortable": false,
	"sWidth":"266px",
	"bVisible": true
},
{
	"sName": "",
	"mDataProp": "nomorPolisi",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"183px",
	"bVisible": true
},
{
	"sName": "",
	"mDataProp": "kodeJob",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"184px",
	"bVisible": true
},
{
	"sName": "",
	"mDataProp": "namaJob",
	"bSortable": false,
	"sWidth":"150px",
	"sDefaultContent": '',
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						aoData.push(
									{"name": 'namaVendor', "value": "${namaVendor}"}
						);

						aoData.push(
									{"name": 'nomorPO', "value": "${nomorPO}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
		</g:javascript>
	</head>
	<body>
<div class="innerDetails">
<table id="POSublet_datatables_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
	class="display table table-striped table-bordered table-hover">
    <thead>
        <tr>
           <th></th>
           <th></th>
			<th style="border-bottom: none; padding: 5px; width: 200px;">
				<div>Nomor WO</div>
			</th>
			<th style="border-bottom: none; padding: 5px; width: 200px;">
				<div>Nomor Polisi</div>
			</th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div>Kode Job</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div>Nama Job</div>
            </th>
          </tr>
    </thead>
</table>
</div>
</body>
</html>
