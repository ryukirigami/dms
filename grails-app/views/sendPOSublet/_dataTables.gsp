
<%@ page import="com.kombos.parts.PO" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="POSublet_datatables_${idTable}" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>


        <th></th>
        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="PO.vendor.label" default="Vendor" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="PO.t164NoPO.label" default="No PO" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="PO.t164TglPO.label" default="Tanggal PO" /></div>
        </th>



        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="PO.tipeOrder.label" default="Perihal" /></div>
        </th>





    </tr>
   </thead>
</table>

<g:javascript>
var POSubletTable;
var reloadPOSubletTable;
$(function(){
	var anOpen = [];

	 $('#view').click(function(e){
        e.stopPropagation();
		POSubletTable.fnDraw();
//        reloadAuditTrailTable();
	});

	$('#clear').click(function(e){

        $('#search_tglStart').val("");
        $('#search_tglEnd').val("");
        $('#search_vendor').val("");
        $('#search_t164NoPO').val("");
        $('#filter_t164TglPO input').val("");
        $('#search_tipeOrder').val("");


        $("#poETABelumLengkap").prop("checked", false);
        $("#poETALengkap").prop("checked", false);
        $("#poSPLD").prop("checked", false);
        $("#poNonSPLD").prop("checked", false);
        $("#poBlmDikirim").prop("checked",false);
        $("#poSdhDikirim").prop("checked",false);


        e.stopPropagation();
		POSubletTable.fnDraw();
//        reloadAuditTrailTable();
	});

	$('#POSublet_datatables_${idTable} td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = POSubletTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${request.contextPath}/sendPOSublet/sublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = POSubletTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});

  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			POSubletTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

    $('#POSublet_datatables_${idTable} a.edit').live('click', function (e) {
        e.preventDefault();

        var nRow = $(this).parents('tr')[0];

        if ( nEditing !== null && nEditing != nRow ) {
            restoreRow( POSubletTable, nEditing );
            editRow( POSubletTable, nRow );
            nEditing = nRow;
        }
        else if ( nEditing == nRow && this.innerHTML == "Save" ) {
            saveRow( POSubletTable, nEditing );
            nEditing = null;
        }
        else {
            editRow( POSubletTable, nRow );
            nEditing = nRow;
        }
    } );

	reloadCkpTable = function() {
		POSubletTable.fnDraw();
	}

	reloadPOSubletTable = function() {
		POSubletTable.fnDraw();
	}

	
	$('#search_t164TglPO').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t164TglPO_day').val(newDate.getDate());
			$('#search_t164TglPO_month').val(newDate.getMonth()+1);
			$('#search_t164TglPO_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			POSubletTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	POSubletTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	POSubletTable = $('#POSublet_datatables_${idTable}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
               "sName": "vendor",
               "mDataProp": null,
               "sClass": "control center",
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": '<i class="icon-plus"></i>'
}
,
{
	"sName": "vendor",
	"mDataProp": "vendor",
	"aTargets": [13],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"mRender": function ( data, type, row ) {
	    return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['noPO']+'" title="Select this"><input type="hidden" value="'+row['noPO']+'">&nbsp;&nbsp;'+data;
    },
	"bVisible": true
}
,

{
	"sName": "noPO",
	"mDataProp": "noPO",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}

,


{
	"sName": "tglPO",
	"mDataProp": "tglPO",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}


,


{
	"sName": "perihal",
	"mDataProp": "perihal",
	"aTargets": [12],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"400px",
	"bVisible": true
}





],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

	                    var tglStart = $("#search_tglStart").val();
	                    var tglAkhir = $("#search_tglEnd").val();

	               //     var spld = $("#poSPLD").val();

                        var varPoApproveBlmSend = function(){
                             if($("#poApproveBlmSend").is(':checked')){
                                return 1;
                             }else{
                                return 0;
                             }
                        }


                          var varPoApproveSdhSend = function(){
                            if($("#poApproveSdhSend").is(':checked')){
                                return 1;
                            } else{
                                return 0;
                            }
                          }

                        if(varPoApproveBlmSend){
                            aoData.push(
									{"name": 'poApproveBlmSend', "value": varPoApproveBlmSend}
							);
                        }

                        if(varPoApproveSdhSend){
                            aoData.push(
									{"name": 'poApproveSdhSend', "value": varPoApproveSdhSend}
							);
                        }


	                    if(tglStart){
							aoData.push(
									{"name": 'search_tglStart', "value": tglStart}
							);
						}

						 if(tglAkhir){
							aoData.push(
									{"name": 'search_tglEnd', "value": tglAkhir}
							);
						}




						var tanggalPO = $('#filter_t164TglPO input').val();
                            if(tanggalPO){
                          //  alert("tanggal : "+tanggalPO);
                                aoData.push(
                                        {"name": 'sCriteria_tglPO', "value": tanggalPO}
                             );
						}


						var tanggalPO= $('#search_t164TglPO').val();
						var tanggalPODay = $('#search_t164TglPO_day').val();
						var tanggalPOMonth = $('#search_t164TglPO_month').val();
						var tanggalPOYear = $('#search_t164TglPO_year').val();
						var tanggalPODP= $('#search_t164TglPO_dp').val();

						if(tanggalPO){
							aoData.push(
									{"name": 'sCriteria_tanggalPO', "value": "date.struct"},
									{"name": 'sCriteria_tanggalPO_dp', "value": tanggalPODP},
									{"name": 'sCriteria_tanggalPO_day', "value": tanggalPODay},
									{"name": 'sCriteria_tanggalPO_month', "value": tanggalPOMonth},
									{"name": 'sCriteria_tanggalPO_year', "value": tanggalPOYear}
							);
						}


						var tipeOrder = $('#filter_tipeOrder input').val();
                            if(tipeOrder){
                                aoData.push(
                                        {"name": 'sCriteria_tipeOrder', "value": tipeOrder}
                             );
						}


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
