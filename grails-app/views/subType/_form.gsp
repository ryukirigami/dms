<%@ page import="com.kombos.finance.SubType" %>



<div class="control-group fieldcontain ${hasErrors(bean: subTypeInstance, field: 'subType', 'error')} required">
	<label class="control-label" for="subType">
		<g:message code="subType.subType.label" default="Sub Type" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="subType" required="" value="${subTypeInstance?.subType}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: subTypeInstance, field: 'description', 'error')} required">
	<label class="control-label" for="description">
		<g:message code="subType.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textArea name="description" required="" value="${subTypeInstance?.description}"/>
	</div>
</div>
%{--

<div class="control-group fieldcontain ${hasErrors(bean: subTypeInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="subType.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${subTypeInstance?.lastUpdProcess}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: subTypeInstance, field: 'collection', 'error')} ">
	<label class="control-label" for="collection">
		<g:message code="subType.collection.label" default="Collection" />
		
	</label>
	<div class="controls">
	
<ul class="one-to-many">
<g:each in="${subTypeInstance?.collection?}" var="c">
    <li><g:link controller="collection" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="collection" action="create" params="['subType.id': subTypeInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'collection.label', default: 'Collection')])}</g:link>
</li>
</ul>

	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: subTypeInstance, field: 'journalDetail', 'error')} ">
	<label class="control-label" for="journalDetail">
		<g:message code="subType.journalDetail.label" default="Journal Detail" />
		
	</label>
	<div class="controls">
	
<ul class="one-to-many">
<g:each in="${subTypeInstance?.journalDetail?}" var="j">
    <li><g:link controller="journalDetail" action="show" id="${j.id}">${j?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="journalDetail" action="create" params="['subType.id': subTypeInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'journalDetail.label', default: 'JournalDetail')])}</g:link>
</li>
</ul>

	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: subTypeInstance, field: 'ledgerCardDetail', 'error')} ">
	<label class="control-label" for="ledgerCardDetail">
		<g:message code="subType.ledgerCardDetail.label" default="Ledger Card Detail" />
		
	</label>
	<div class="controls">
	
<ul class="one-to-many">
<g:each in="${subTypeInstance?.ledgerCardDetail?}" var="l">
    <li><g:link controller="ledgerCardDetail" action="show" id="${l.id}">${l?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="ledgerCardDetail" action="create" params="['subType.id': subTypeInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'ledgerCardDetail.label', default: 'LedgerCardDetail')])}</g:link>
</li>
</ul>

	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: subTypeInstance, field: 'monthlyBalance', 'error')} ">
	<label class="control-label" for="monthlyBalance">
		<g:message code="subType.monthlyBalance.label" default="Monthly Balance" />
		
	</label>
	<div class="controls">
	
<ul class="one-to-many">
<g:each in="${subTypeInstance?.monthlyBalance?}" var="m">
    <li><g:link controller="monthlyBalance" action="show" id="${m.id}">${m?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="monthlyBalance" action="create" params="['subType.id': subTypeInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'monthlyBalance.label', default: 'MonthlyBalance')])}</g:link>
</li>
</ul>

	</div>
</div>

--}%
