
<%@ page import="com.kombos.finance.SubType" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="subType_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="subType.subType.label" default="Sub Type" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="subType.description.label" default="Description" /></div>
			</th>


			%{--<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="subType.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="subType.collection.label" default="Collection" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="subType.journalDetail.label" default="Journal Detail" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="subType.ledgerCardDetail.label" default="Ledger Card Detail" /></div>
			</th>
--}%

			%{--<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="subType.monthlyBalance.label" default="Monthly Balance" /></div>
			</th>--}%

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_subType" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_subType" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_description" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_description" class="search_init" />
				</div>
			</th>
	
			%{--<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_collection" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_collection" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_journalDetail" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_journalDetail" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_ledgerCardDetail" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_ledgerCardDetail" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_monthlyBalance" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_monthlyBalance" class="search_init" />
				</div>
			</th>
--}%

		</tr>
	</thead>
</table>

<g:javascript>
var subTypeTable;
var reloadSubTypeTable;
$(function(){
	
	reloadSubTypeTable = function() {
		subTypeTable.fnDraw();
	}

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	subTypeTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	subTypeTable = $('#subType_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "subType",
	"mDataProp": "subType",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "description",
	"mDataProp": "description",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

/*
,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "collection",
	"mDataProp": "collection",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "journalDetail",
	"mDataProp": "journalDetail",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "ledgerCardDetail",
	"mDataProp": "ledgerCardDetail",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "monthlyBalance",
	"mDataProp": "monthlyBalance",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
*/

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var subType = $('#filter_subType input').val();
						if(subType){
							aoData.push(
									{"name": 'sCriteria_subType', "value": subType}
							);
						}
	
						var description = $('#filter_description input').val();
						if(description){
							aoData.push(
									{"name": 'sCriteria_description', "value": description}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}
	
						var collection = $('#filter_collection input').val();
						if(collection){
							aoData.push(
									{"name": 'sCriteria_collection', "value": collection}
							);
						}
	
						var journalDetail = $('#filter_journalDetail input').val();
						if(journalDetail){
							aoData.push(
									{"name": 'sCriteria_journalDetail', "value": journalDetail}
							);
						}
	
						var ledgerCardDetail = $('#filter_ledgerCardDetail input').val();
						if(ledgerCardDetail){
							aoData.push(
									{"name": 'sCriteria_ledgerCardDetail', "value": ledgerCardDetail}
							);
						}
	
						var monthlyBalance = $('#filter_monthlyBalance input').val();
						if(monthlyBalance){
							aoData.push(
									{"name": 'sCriteria_monthlyBalance', "value": monthlyBalance}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
