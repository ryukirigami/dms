
<%@ page import="com.kombos.parts.Request" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'inputStokOPName.label', default: 'Input Stok Opname')}" />
		<title><g:message code="inputStokOPName.label" default="Input Stok Opname" /></title>
		<r:require modules="baseapplayout, autoNumeric" />
    <g:javascript disposition="head">
	var show;
	var checkOnValue;
	var cekStatus = "${status}";
    $(function(){
    $('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});
    
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#inputStokOPName-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/inputStokOPName/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadInputStokOPNameTable();
    		}
		});
		
   	}

});


  //darisini
	$("#pilihKlasifikasiGoodsModal").on("show", function() {
		$("#pilihKlasifikasiGoodsModal .btn").on("click", function(e) {
			$("#pilihKlasifikasiGoodsModal").modal('hide');
		});
	});
	$("#pilihKlasifikasiGoodsModal").on("hide", function() {
		$("#pilihKlasifikasiGoodsModal a.btn").off("click");
	});

    loadPilihKlasifikasiGoodsModal = function(){
		$("#pilihKlasifikasiGoodsContent").empty();
		$.ajax({type:'POST', url:'${request.contextPath}/pilihKlasifikasiGoods',
   			success:function(data,textStatus){
					$("#pilihKlasifikasiGoodsContent").html(data);
				    $("#pilihKlasifikasiGoodsModal").modal({
						"backdrop" : "static",
						"keyboard" : true,
						"show" : true
					}).css({'width': '1200px','margin-left': function () {return -($(this).width() / 2);}});

   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
		}


       tambahKlasifikasiGoods = function(){
            var checkGoods =[];
            $("#klasifikasiGoods-table tbody .row-select").each(function() {
                if(this.checked){
                    //var id = $(this).next("input:hidden").val();
                    var nRow = $(this).parents('tr')[0];
                    checkGoods.push(nRow);
                }
            });
            if(checkGoods.length<1){
                alert('Anda belum memilih data yang akan ditambahkan');
                loadPilihKlasifikasiGoodsModal();
            } else {
				for (var i=0;i < checkGoods.length;i++){
					var aData = klasifikasiGoodsTable.fnGetData(checkGoods[i]);
					inputStokOPNameTable.fnAddData({
						'id': aData['id'], 
						'namagoods': aData['namagoods_popup'],
						'kodegoods': aData['kodegoods_popup'],
						'lokasigoods': aData['location_popup']
                    });
				}
				
			}
      }


    updateStokOPName = function(){
        var checkGoods =[];
        var id = $('#t132ID').val();
        var tanggal = $('#t132Tanggal').val();
        $("#inputStokOPName-table tbody .row-select").each(function() {
            var add = $(this).next("input:hidden").val();
            var nRow = $(this).parents('tr')[0];
            var aData = inputStokOPNameTable.fnGetData(nRow);
            checkGoods.push(add);
        });

        var json = JSON.stringify(checkGoods);

		$.ajax({
    		url:'${request.contextPath}/inputStokOPName/update',
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json , t132ID : id , t132Tanggal : tanggal },
    		success : function(data){
                window.location.replace('#/stokOPName')
                toastr.success('<div>Update Succes.</div>');
            },
            error: function(xhr, textStatus, errorThrown) {
                toastr.error('<div>Internal server error</div>');
            }
        });
    };

    createStokOPName = function(){
		var formStokOPName = $('#inputStokOPName-table').find('form');

        var checkGoods =[];
        $("#inputStokOPName-table tbody .row-select").each(function() {
            if(this.checked){
                var id = $(this).next("input:hidden").val();
                var nRow = $(this).parents('tr')[0];
                var aData = inputStokOPNameTable.fnGetData(nRow);
                checkGoods.push(id);
            }
        });

        $("#ids").val(JSON.stringify(checkGoods));

        if($('#t132Tanggal').val() ==null ||$('#t132Tanggal').val()==""){
            alert('Tanggal Stock Opname Tidak Boleh Kosong');
        }else{
            if(checkGoods.length<1){
            alert('Anda belum memilih data yang akan ditambahkan');
            }else{
                $.ajax({
                    url:'${request.contextPath}/inputStokOPName/save',
                    type: "POST", // Always use POST when deleting data
                    data : formStokOPName.serialize(),
                    success : function(data){
                        if(data=="ada"){
                            alert('Nomor Stock Opname sudah ada');
                        }else{
                            window.location.replace('#/stokOPName')
                            toastr.success('<div>Data Created.</div>');
                            formStokOPName.find('.deleteafter').remove();
                            $("#ids").val('');
                        }
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        alert('Internal Server Error');
                    }
                });
            }
        }

        checkGoods = [];
    }


    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</g:javascript>

	</head>
	<body>
	<div class="navbar box-header no-border">
        <span class="pull-left"><g:message code="inputStokOPName.label" default="Input Stok Opname" /></span>
        <ul class="nav pull-right">
            <li></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		
		<div class="span12" id="inputStokOPName-table">
			<form id="form-inputStokOPName" class="form-horizontal">
				<input type="hidden" name="ids" id="ids" value="">
				<fieldset>
                    <div class="control-group fieldcontain ${hasErrors(bean: stokOPNameInstance, field: 't132Tanggal', 'error')} ">
                        <label class="control-label" for="t132Tanggal">
                            <g:message code="stokOPName.t132Tanggal.label" default="Tanggal Stock Opname"/>*
                        </label>
                        <div class="controls">
                            <g:if test="${status=="edit"}" >
                                <ba:datePicker required="true" name="t132Tanggal" value="${tanggalUbah}" precision="day" format="dd-MM-yyyy"/>
                            </g:if>
                            <g:else>
                                <ba:datePicker required="true" name="t132Tanggal" value="" precision="day" format="dd-MM-yyyy"/>
                            </g:else>
                        </div>
                    </div>
                    <div class="control-group fieldcontain ${hasErrors(bean: stokOPNameInstance, field: 't132ID', 'error')} ">
                        <label class="control-label" for="t132ID">
                            <g:if test="${status=="edit"}" >
                                <g:message code="stokOPName.t132ID.label" default="Kode Stock Opname"/>*
                            </g:if>
                        </label>
                        <div class="controls">
                            <g:if test="${status=="edit"}" >
                                <g:textField required="" name="t132ID" value="${idUbah}" readonly=""/>
                            </g:if>
                            %{--<g:else>--}%
                                %{--<g:textField required="" name="t132ID" value="" onkeypress="return isNumberKey(event);"/>--}%
                            %{--</g:else>--}%
                        </div>
                    </div>
				</fieldset>
			</form>
			<fieldset class="buttons controls" style="padding-bottom: 30px;">
					<button class="btn btn-primary" name="search" id="search" onclick="loadPilihKlasifikasiGoodsModal()">Search Part</button>
			</fieldset>
            
			<g:render template="dataTables" />
            <g:field type="button" onclick="window.location.replace('#/stokOPName')" class="btn cancel" name="cancel" id="cancel" value="${message(code: 'default.button.cancel.label', default: 'Cancel')}" />
            <g:if test="${status!="edit"}" >
                <g:field type="button" onclick="createStokOPName();" class="btn btn-primary create" name="tambah" id="tambah" value="${message(code: 'default.button.create.label', default: 'Save')}" />
            </g:if>
            <g:else>
                <g:field type="button" onclick="updateStokOPName();" class="btn btn-primary create" name="update" id="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
            </g:else>


		</div>
		<div class="span7" id="inputStokOPName-form" style="display: none;"></div>
	</div>
    <div id="pilihKlasifikasiGoodsModal" class="modal fade">
        <div class="modal-dialog" style="width: 1200px;">
            <div class="modal-content" style="width: 1200px;">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 500px;">
                    <div id="pilihKlasifikasiGoodsContent"/>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="iu-content"></div>

                </div>
                <!-- dialog buttons -->
                <div class="modal-footer"><button id='closePopup' type="button" class="btn btn-primary">Close</button></div>
            </div>
        </div>
    </div>
</body>
</html>
