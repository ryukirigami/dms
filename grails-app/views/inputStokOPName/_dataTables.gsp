
<%@ page import="com.kombos.parts.StokOPName" %>

<r:require modules="baseapplayout" />

<g:render template="../menu/maxLineDisplay"/>

<table id="inputStokOPName_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="stokOPName.goods.kode.label" default="Kode Part" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="stokOPName.goods.nama.label" default="Nama Part" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="stokOPName.goods.lokasi.label" default="Location" /></div>
        </th>

    </tr>
    <tr>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_kode_goods" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_kode_goods" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_nama_goods" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_nama_goods" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_lokasi_goods" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_lokasi_goods" class="search_init" />
            </div>
        </th>

    </tr>

	</thead>
</table>

<g:javascript>
var inputStokOPNameTable;
var reloadInputStokOPNameTable;
$(function(){

    reloadInputStokOPNameTable = function() {
        inputStokOPNameTable.fnDraw();
	}

    var recordsInputStokOPNamePerPage = [];//new Array();
    var anInputStokOPNameSelected;
    var jmlRecInputStokOPNamePerPage=0;
    var id;
	
	inputStokOPNameTable = $('#inputStokOPName_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsInputStokOPName = $("#inputStokOPName_datatables tbody .row-select");
            var jmlInputStokOPNameCek = 0;
            var nRow;
            var idRec;
            rsInputStokOPName.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsInputStokOPNamePerPage[idRec]=="1"){
                    jmlInputStokOPNameCek = jmlInputStokOPNameCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsInputStokOPNamePerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecInputStokOPNamePerPage = rsInputStokOPName.length;
            if(jmlInputStokOPNameCek==jmlRecInputStokOPNamePerPage && jmlRecInputStokOPNamePerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bDestroy" : true,
		"iDisplayLength" : maxLineDisplay,
		"aoColumns": [

{
    "sName": "goods",
    "mDataProp": "kodegoods",
    "aTargets": [0],
    "mRender": function ( data, type, row ) {
        return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data
    },
    "bSearchable": true,
    "bSortable": true,
    "sWidth":"300px",
    "bVisible": true
}
            ,

            {
                "sName": "goods",
                "mDataProp": "namagoods",
                "aTargets": [1],
                "bSearchable": true,
                "bSortable": true,
                "sWidth":"300px",
                "bVisible": true
            }
            ,
            {
                "sName": "goods",
                "mDataProp": "lokasigoods",
                "aTargets": [2],
                "bSearchable": true,
                "bSortable": true,
                "sWidth":"300px",
                "bVisible": true
            }

]

	});



    if(cekStatus=="edit"){
        var idUbah = '${idUbah}'
        $.ajax({
    		url:'${request.contextPath}/inputStokOPName/getTableData',
    		type: "POST", // Always use POST when deleting data
    		data: { id: idUbah },
    		success : function(data){
    		    $.each(data,function(i,item){
                    inputStokOPNameTable.fnAddData({
                        'id': item.id,
                        'namagoods': item.nama,
                        'kodegoods': item.kode,
                        'lokasigoods': item.lokasi
                    });
                });
            }
		});
    }
    $('.select-all').click(function(e) {

        $("#inputStokOPName_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsInputStokOPNamePerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsInputStokOPNamePerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#inputStokOPName_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsInputStokOPNamePerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anInputStokOPNameSelected = inputStokOPNameTable.$('tr.row_selected');
            if(jmlRecInputStokOPNamePerPage == anInputStokOPNameSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsInputStokOPNamePerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>

