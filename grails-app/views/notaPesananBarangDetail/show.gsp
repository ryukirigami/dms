

<%@ page import="com.kombos.parts.NotaPesananBarangDetail" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'notaPesananBarangDetail.label', default: 'NotaPesananBarangDetail')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteNotaPesananBarangDetail;

$(function(){ 
	deleteNotaPesananBarangDetail=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/notaPesananBarangDetail/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadNotaPesananBarangDetailTable();
   				expandTableLayout('notaPesananBarangDetail');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-notaPesananBarangDetail" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="notaPesananBarangDetail"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${notaPesananBarangDetailInstance?.cabangKeterangan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="cabangKeterangan-label" class="property-label"><g:message
					code="notaPesananBarangDetail.cabangKeterangan.label" default="Cabang Keterangan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="cabangKeterangan-label">
						%{--<ba:editableValue
								bean="${notaPesananBarangDetailInstance}" field="cabangKeterangan"
								url="${request.contextPath}/NotaPesananBarangDetail/updatefield" type="text"
								title="Enter cabangKeterangan" onsuccess="reloadNotaPesananBarangDetailTable();" />--}%
							
								<g:fieldValue bean="${notaPesananBarangDetailInstance}" field="cabangKeterangan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${notaPesananBarangDetailInstance?.cabangTglDiterima}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="cabangTglDiterima-label" class="property-label"><g:message
					code="notaPesananBarangDetail.cabangTglDiterima.label" default="Cabang Tgl Diterima" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="cabangTglDiterima-label">
						%{--<ba:editableValue
								bean="${notaPesananBarangDetailInstance}" field="cabangTglDiterima"
								url="${request.contextPath}/NotaPesananBarangDetail/updatefield" type="text"
								title="Enter cabangTglDiterima" onsuccess="reloadNotaPesananBarangDetailTable();" />--}%
							
								<g:formatDate date="${notaPesananBarangDetailInstance?.cabangTglDiterima}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${notaPesananBarangDetailInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="notaPesananBarangDetail.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${notaPesananBarangDetailInstance}" field="createdBy"
								url="${request.contextPath}/NotaPesananBarangDetail/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadNotaPesananBarangDetailTable();" />--}%
							
								<g:fieldValue bean="${notaPesananBarangDetailInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${notaPesananBarangDetailInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="notaPesananBarangDetail.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${notaPesananBarangDetailInstance}" field="dateCreated"
								url="${request.contextPath}/NotaPesananBarangDetail/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadNotaPesananBarangDetailTable();" />--}%
							
								<g:formatDate date="${notaPesananBarangDetailInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${notaPesananBarangDetailInstance?.goods}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="goods-label" class="property-label"><g:message
					code="notaPesananBarangDetail.goods.label" default="Goods" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="goods-label">
						%{--<ba:editableValue
								bean="${notaPesananBarangDetailInstance}" field="goods"
								url="${request.contextPath}/NotaPesananBarangDetail/updatefield" type="text"
								title="Enter goods" onsuccess="reloadNotaPesananBarangDetailTable();" />--}%
							
								<g:link controller="goods" action="show" id="${notaPesananBarangDetailInstance?.goods?.id}">${notaPesananBarangDetailInstance?.goods?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${notaPesananBarangDetailInstance?.hoTglDiterima}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="hoTglDiterima-label" class="property-label"><g:message
					code="notaPesananBarangDetail.hoTglDiterima.label" default="Ho Tgl Diterima" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="hoTglDiterima-label">
						%{--<ba:editableValue
								bean="${notaPesananBarangDetailInstance}" field="hoTglDiterima"
								url="${request.contextPath}/NotaPesananBarangDetail/updatefield" type="text"
								title="Enter hoTglDiterima" onsuccess="reloadNotaPesananBarangDetailTable();" />--}%
							
								<g:formatDate date="${notaPesananBarangDetailInstance?.hoTglDiterima}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${notaPesananBarangDetailInstance?.hoTglDiteruskan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="hoTglDiteruskan-label" class="property-label"><g:message
					code="notaPesananBarangDetail.hoTglDiteruskan.label" default="Ho Tgl Diteruskan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="hoTglDiteruskan-label">
						%{--<ba:editableValue
								bean="${notaPesananBarangDetailInstance}" field="hoTglDiteruskan"
								url="${request.contextPath}/NotaPesananBarangDetail/updatefield" type="text"
								title="Enter hoTglDiteruskan" onsuccess="reloadNotaPesananBarangDetailTable();" />--}%
							
								<g:formatDate date="${notaPesananBarangDetailInstance?.hoTglDiteruskan}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${notaPesananBarangDetailInstance?.jumlah}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="jumlah-label" class="property-label"><g:message
					code="notaPesananBarangDetail.jumlah.label" default="Jumlah" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="jumlah-label">
						%{--<ba:editableValue
								bean="${notaPesananBarangDetailInstance}" field="jumlah"
								url="${request.contextPath}/NotaPesananBarangDetail/updatefield" type="text"
								title="Enter jumlah" onsuccess="reloadNotaPesananBarangDetailTable();" />--}%
							
								<g:fieldValue bean="${notaPesananBarangDetailInstance}" field="jumlah"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${notaPesananBarangDetailInstance?.ktrJktTglBukaDO}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="ktrJktTglBukaDO-label" class="property-label"><g:message
					code="notaPesananBarangDetail.ktrJktTglBukaDO.label" default="Ktr Jkt Tgl Buka DO" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="ktrJktTglBukaDO-label">
						%{--<ba:editableValue
								bean="${notaPesananBarangDetailInstance}" field="ktrJktTglBukaDO"
								url="${request.contextPath}/NotaPesananBarangDetail/updatefield" type="text"
								title="Enter ktrJktTglBukaDO" onsuccess="reloadNotaPesananBarangDetailTable();" />--}%
							
								<g:formatDate date="${notaPesananBarangDetailInstance?.ktrJktTglBukaDO}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${notaPesananBarangDetailInstance?.ktrJktTglDiproses}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="ktrJktTglDiproses-label" class="property-label"><g:message
					code="notaPesananBarangDetail.ktrJktTglDiproses.label" default="Ktr Jkt Tgl Diproses" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="ktrJktTglDiproses-label">
						%{--<ba:editableValue
								bean="${notaPesananBarangDetailInstance}" field="ktrJktTglDiproses"
								url="${request.contextPath}/NotaPesananBarangDetail/updatefield" type="text"
								title="Enter ktrJktTglDiproses" onsuccess="reloadNotaPesananBarangDetailTable();" />--}%
							
								<g:formatDate date="${notaPesananBarangDetailInstance?.ktrJktTglDiproses}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${notaPesananBarangDetailInstance?.ktrJktTglDiterima}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="ktrJktTglDiterima-label" class="property-label"><g:message
					code="notaPesananBarangDetail.ktrJktTglDiterima.label" default="Ktr Jkt Tgl Diterima" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="ktrJktTglDiterima-label">
						%{--<ba:editableValue
								bean="${notaPesananBarangDetailInstance}" field="ktrJktTglDiterima"
								url="${request.contextPath}/NotaPesananBarangDetail/updatefield" type="text"
								title="Enter ktrJktTglDiterima" onsuccess="reloadNotaPesananBarangDetailTable();" />--}%
							
								<g:formatDate date="${notaPesananBarangDetailInstance?.ktrJktTglDiterima}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${notaPesananBarangDetailInstance?.ktrJktTglKrmKeCabang}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="ktrJktTglKrmKeCabang-label" class="property-label"><g:message
					code="notaPesananBarangDetail.ktrJktTglKrmKeCabang.label" default="Ktr Jkt Tgl Krm Ke Cabang" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="ktrJktTglKrmKeCabang-label">
						%{--<ba:editableValue
								bean="${notaPesananBarangDetailInstance}" field="ktrJktTglKrmKeCabang"
								url="${request.contextPath}/NotaPesananBarangDetail/updatefield" type="text"
								title="Enter ktrJktTglKrmKeCabang" onsuccess="reloadNotaPesananBarangDetailTable();" />--}%
							
								<g:formatDate date="${notaPesananBarangDetailInstance?.ktrJktTglKrmKeCabang}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${notaPesananBarangDetailInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="notaPesananBarangDetail.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${notaPesananBarangDetailInstance}" field="lastUpdProcess"
								url="${request.contextPath}/NotaPesananBarangDetail/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadNotaPesananBarangDetailTable();" />--}%
							
								<g:fieldValue bean="${notaPesananBarangDetailInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${notaPesananBarangDetailInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="notaPesananBarangDetail.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${notaPesananBarangDetailInstance}" field="lastUpdated"
								url="${request.contextPath}/NotaPesananBarangDetail/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadNotaPesananBarangDetailTable();" />--}%
							
								<g:formatDate date="${notaPesananBarangDetailInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${notaPesananBarangDetailInstance?.notaPesananBarang}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="notaPesananBarang-label" class="property-label"><g:message
					code="notaPesananBarangDetail.notaPesananBarang.label" default="Nota Pesanan Barang" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="notaPesananBarang-label">
						%{--<ba:editableValue
								bean="${notaPesananBarangDetailInstance}" field="notaPesananBarang"
								url="${request.contextPath}/NotaPesananBarangDetail/updatefield" type="text"
								title="Enter notaPesananBarang" onsuccess="reloadNotaPesananBarangDetailTable();" />--}%
							
								<g:link controller="notaPesananBarang" action="show" id="${notaPesananBarangDetailInstance?.notaPesananBarang?.id}">${notaPesananBarangDetailInstance?.notaPesananBarang?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${notaPesananBarangDetailInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="notaPesananBarangDetail.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${notaPesananBarangDetailInstance}" field="updatedBy"
								url="${request.contextPath}/NotaPesananBarangDetail/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadNotaPesananBarangDetailTable();" />--}%
							
								<g:fieldValue bean="${notaPesananBarangDetailInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('notaPesananBarangDetail');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${notaPesananBarangDetailInstance?.id}"
					update="[success:'notaPesananBarangDetail-form',failure:'notaPesananBarangDetail-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteNotaPesananBarangDetail('${notaPesananBarangDetailInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
