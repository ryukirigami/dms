<%@ page import="com.kombos.parts.NotaPesananBarangDetail" %>



<div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangDetailInstance, field: 'cabangKeterangan', 'error')} ">
	<label class="control-label" for="cabangKeterangan">
		<g:message code="notaPesananBarangDetail.cabangKeterangan.label" default="Cabang Keterangan" />
		
	</label>
	<div class="controls">
	<g:textField name="cabangKeterangan" value="${notaPesananBarangDetailInstance?.cabangKeterangan}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangDetailInstance, field: 'cabangTglDiterima', 'error')} ">
	<label class="control-label" for="cabangTglDiterima">
		<g:message code="notaPesananBarangDetail.cabangTglDiterima.label" default="Cabang Tgl Diterima" />
		
	</label>
	<div class="controls">
	<ba:datePicker name="cabangTglDiterima" precision="day" value="${notaPesananBarangDetailInstance?.cabangTglDiterima}" format="yyyy-MM-dd"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangDetailInstance, field: 'goods', 'error')} ">
	<label class="control-label" for="goods">
		<g:message code="notaPesananBarangDetail.goods.label" default="Goods" />
		
	</label>
	<div class="controls">
	<g:select id="goods" name="goods.id" from="${com.kombos.parts.Goods.list()}" optionKey="id" required="" value="${notaPesananBarangDetailInstance?.goods?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangDetailInstance, field: 'hoTglDiterima', 'error')} ">
	<label class="control-label" for="hoTglDiterima">
		<g:message code="notaPesananBarangDetail.hoTglDiterima.label" default="Ho Tgl Diterima" />
		
	</label>
	<div class="controls">
	<ba:datePicker name="hoTglDiterima" precision="day" value="${notaPesananBarangDetailInstance?.hoTglDiterima}" format="yyyy-MM-dd"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangDetailInstance, field: 'hoTglDiteruskan', 'error')} ">
	<label class="control-label" for="hoTglDiteruskan">
		<g:message code="notaPesananBarangDetail.hoTglDiteruskan.label" default="Ho Tgl Diteruskan" />
		
	</label>
	<div class="controls">
	<ba:datePicker name="hoTglDiteruskan" precision="day" value="${notaPesananBarangDetailInstance?.hoTglDiteruskan}" format="yyyy-MM-dd"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangDetailInstance, field: 'jumlah', 'error')} ">
	<label class="control-label" for="jumlah">
		<g:message code="notaPesananBarangDetail.jumlah.label" default="Jumlah" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="jumlah" value="${notaPesananBarangDetailInstance.jumlah}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangDetailInstance, field: 'ktrJktTglBukaDO', 'error')} ">
	<label class="control-label" for="ktrJktTglBukaDO">
		<g:message code="notaPesananBarangDetail.ktrJktTglBukaDO.label" default="Ktr Jkt Tgl Buka DO" />
		
	</label>
	<div class="controls">
	<ba:datePicker name="ktrJktTglBukaDO" precision="day" value="${notaPesananBarangDetailInstance?.ktrJktTglBukaDO}" format="yyyy-MM-dd"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangDetailInstance, field: 'ktrJktTglDiproses', 'error')} ">
	<label class="control-label" for="ktrJktTglDiproses">
		<g:message code="notaPesananBarangDetail.ktrJktTglDiproses.label" default="Ktr Jkt Tgl Diproses" />
		
	</label>
	<div class="controls">
	<ba:datePicker name="ktrJktTglDiproses" precision="day" value="${notaPesananBarangDetailInstance?.ktrJktTglDiproses}" format="yyyy-MM-dd"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangDetailInstance, field: 'ktrJktTglDiterima', 'error')} ">
	<label class="control-label" for="ktrJktTglDiterima">
		<g:message code="notaPesananBarangDetail.ktrJktTglDiterima.label" default="Ktr Jkt Tgl Diterima" />
		
	</label>
	<div class="controls">
	<ba:datePicker name="ktrJktTglDiterima" precision="day" value="${notaPesananBarangDetailInstance?.ktrJktTglDiterima}" format="yyyy-MM-dd"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangDetailInstance, field: 'ktrJktTglKrmKeCabang', 'error')} ">
	<label class="control-label" for="ktrJktTglKrmKeCabang">
		<g:message code="notaPesananBarangDetail.ktrJktTglKrmKeCabang.label" default="Ktr Jkt Tgl Krm Ke Cabang" />
		
	</label>
	<div class="controls">
	<ba:datePicker name="ktrJktTglKrmKeCabang" precision="day" value="${notaPesananBarangDetailInstance?.ktrJktTglKrmKeCabang}" format="yyyy-MM-dd"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangDetailInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="notaPesananBarangDetail.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${notaPesananBarangDetailInstance?.lastUpdProcess}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangDetailInstance, field: 'notaPesananBarang', 'error')} ">
	<label class="control-label" for="notaPesananBarang">
		<g:message code="notaPesananBarangDetail.notaPesananBarang.label" default="Nota Pesanan Barang" />
		
	</label>
	<div class="controls">
	<g:select id="notaPesananBarang" name="notaPesananBarang.id" from="${com.kombos.parts.NotaPesananBarang.list()}" optionKey="id" required="" value="${notaPesananBarangDetailInstance?.notaPesananBarang?.id}" class="many-to-one"/>
	</div>
</div>

