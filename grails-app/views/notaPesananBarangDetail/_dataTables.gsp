
<%@ page import="com.kombos.parts.NotaPesananBarangDetail" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="notaPesananBarangDetail_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="notaPesananBarangDetail.cabangKeterangan.label" default="Cabang Keterangan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="notaPesananBarangDetail.cabangTglDiterima.label" default="Cabang Tgl Diterima" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="notaPesananBarangDetail.goods.label" default="Goods" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="notaPesananBarangDetail.hoTglDiterima.label" default="Ho Tgl Diterima" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="notaPesananBarangDetail.hoTglDiteruskan.label" default="Ho Tgl Diteruskan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="notaPesananBarangDetail.jumlah.label" default="Jumlah" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="notaPesananBarangDetail.ktrJktTglBukaDO.label" default="Ktr Jkt Tgl Buka DO" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="notaPesananBarangDetail.ktrJktTglDiproses.label" default="Ktr Jkt Tgl Diproses" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="notaPesananBarangDetail.ktrJktTglDiterima.label" default="Ktr Jkt Tgl Diterima" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="notaPesananBarangDetail.ktrJktTglKrmKeCabang.label" default="Ktr Jkt Tgl Krm Ke Cabang" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="notaPesananBarangDetail.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="notaPesananBarangDetail.notaPesananBarang.label" default="Nota Pesanan Barang" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_cabangKeterangan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_cabangKeterangan" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_cabangTglDiterima" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_cabangTglDiterima" value="date.struct">
					<input type="hidden" name="search_cabangTglDiterima_day" id="search_cabangTglDiterima_day" value="">
					<input type="hidden" name="search_cabangTglDiterima_month" id="search_cabangTglDiterima_month" value="">
					<input type="hidden" name="search_cabangTglDiterima_year" id="search_cabangTglDiterima_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_cabangTglDiterima_dp" value="" id="search_cabangTglDiterima" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_goods" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_goods" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_hoTglDiterima" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_hoTglDiterima" value="date.struct">
					<input type="hidden" name="search_hoTglDiterima_day" id="search_hoTglDiterima_day" value="">
					<input type="hidden" name="search_hoTglDiterima_month" id="search_hoTglDiterima_month" value="">
					<input type="hidden" name="search_hoTglDiterima_year" id="search_hoTglDiterima_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_hoTglDiterima_dp" value="" id="search_hoTglDiterima" class="search_init">
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_hoTglDiteruskan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_hoTglDiteruskan" value="date.struct">
					<input type="hidden" name="search_hoTglDiteruskan_day" id="search_hoTglDiteruskan_day" value="">
					<input type="hidden" name="search_hoTglDiteruskan_month" id="search_hoTglDiteruskan_month" value="">
					<input type="hidden" name="search_hoTglDiteruskan_year" id="search_hoTglDiteruskan_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_hoTglDiteruskan_dp" value="" id="search_hoTglDiteruskan" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_jumlah" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_jumlah" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_ktrJktTglBukaDO" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_ktrJktTglBukaDO" value="date.struct">
					<input type="hidden" name="search_ktrJktTglBukaDO_day" id="search_ktrJktTglBukaDO_day" value="">
					<input type="hidden" name="search_ktrJktTglBukaDO_month" id="search_ktrJktTglBukaDO_month" value="">
					<input type="hidden" name="search_ktrJktTglBukaDO_year" id="search_ktrJktTglBukaDO_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_ktrJktTglBukaDO_dp" value="" id="search_ktrJktTglBukaDO" class="search_init">
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_ktrJktTglDiproses" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_ktrJktTglDiproses" value="date.struct">
					<input type="hidden" name="search_ktrJktTglDiproses_day" id="search_ktrJktTglDiproses_day" value="">
					<input type="hidden" name="search_ktrJktTglDiproses_month" id="search_ktrJktTglDiproses_month" value="">
					<input type="hidden" name="search_ktrJktTglDiproses_year" id="search_ktrJktTglDiproses_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_ktrJktTglDiproses_dp" value="" id="search_ktrJktTglDiproses" class="search_init">
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_ktrJktTglDiterima" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_ktrJktTglDiterima" value="date.struct">
					<input type="hidden" name="search_ktrJktTglDiterima_day" id="search_ktrJktTglDiterima_day" value="">
					<input type="hidden" name="search_ktrJktTglDiterima_month" id="search_ktrJktTglDiterima_month" value="">
					<input type="hidden" name="search_ktrJktTglDiterima_year" id="search_ktrJktTglDiterima_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_ktrJktTglDiterima_dp" value="" id="search_ktrJktTglDiterima" class="search_init">
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_ktrJktTglKrmKeCabang" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_ktrJktTglKrmKeCabang" value="date.struct">
					<input type="hidden" name="search_ktrJktTglKrmKeCabang_day" id="search_ktrJktTglKrmKeCabang_day" value="">
					<input type="hidden" name="search_ktrJktTglKrmKeCabang_month" id="search_ktrJktTglKrmKeCabang_month" value="">
					<input type="hidden" name="search_ktrJktTglKrmKeCabang_year" id="search_ktrJktTglKrmKeCabang_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_ktrJktTglKrmKeCabang_dp" value="" id="search_ktrJktTglKrmKeCabang" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_notaPesananBarang" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_notaPesananBarang" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var notaPesananBarangDetailTable;
var reloadNotaPesananBarangDetailTable;
$(function(){

	reloadNotaPesananBarangDetailTable = function() {
		notaPesananBarangDetailTable.fnDraw();
	}


	$('#search_cabangTglDiterima').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_cabangTglDiterima_day').val(newDate.getDate());
			$('#search_cabangTglDiterima_month').val(newDate.getMonth()+1);
			$('#search_cabangTglDiterima_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			notaPesananBarangDetailTable.fnDraw();
	});



	$('#search_hoTglDiterima').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_hoTglDiterima_day').val(newDate.getDate());
			$('#search_hoTglDiterima_month').val(newDate.getMonth()+1);
			$('#search_hoTglDiterima_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			notaPesananBarangDetailTable.fnDraw();
	});



	$('#search_hoTglDiteruskan').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_hoTglDiteruskan_day').val(newDate.getDate());
			$('#search_hoTglDiteruskan_month').val(newDate.getMonth()+1);
			$('#search_hoTglDiteruskan_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			notaPesananBarangDetailTable.fnDraw();
	});



	$('#search_ktrJktTglBukaDO').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_ktrJktTglBukaDO_day').val(newDate.getDate());
			$('#search_ktrJktTglBukaDO_month').val(newDate.getMonth()+1);
			$('#search_ktrJktTglBukaDO_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			notaPesananBarangDetailTable.fnDraw();
	});



	$('#search_ktrJktTglDiproses').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_ktrJktTglDiproses_day').val(newDate.getDate());
			$('#search_ktrJktTglDiproses_month').val(newDate.getMonth()+1);
			$('#search_ktrJktTglDiproses_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			notaPesananBarangDetailTable.fnDraw();
	});



	$('#search_ktrJktTglDiterima').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_ktrJktTglDiterima_day').val(newDate.getDate());
			$('#search_ktrJktTglDiterima_month').val(newDate.getMonth()+1);
			$('#search_ktrJktTglDiterima_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			notaPesananBarangDetailTable.fnDraw();
	});



	$('#search_ktrJktTglKrmKeCabang').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_ktrJktTglKrmKeCabang_day').val(newDate.getDate());
			$('#search_ktrJktTglKrmKeCabang_month').val(newDate.getMonth()+1);
			$('#search_ktrJktTglKrmKeCabang_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			notaPesananBarangDetailTable.fnDraw();
	});




$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	notaPesananBarangDetailTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	notaPesananBarangDetailTable = $('#notaPesananBarangDetail_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
	//	"bProcessing": true,
		"bServerSide": false,
	//	"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
	//	"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "cabangKeterangan",
	"mDataProp": "cabangKeterangan",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "cabangTglDiterima",
	"mDataProp": "cabangTglDiterima",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "goods",
	"mDataProp": "goods",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "hoTglDiterima",
	"mDataProp": "hoTglDiterima",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "hoTglDiteruskan",
	"mDataProp": "hoTglDiteruskan",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "jumlah",
	"mDataProp": "jumlah",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "ktrJktTglBukaDO",
	"mDataProp": "ktrJktTglBukaDO",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "ktrJktTglDiproses",
	"mDataProp": "ktrJktTglDiproses",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "ktrJktTglDiterima",
	"mDataProp": "ktrJktTglDiterima",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "ktrJktTglKrmKeCabang",
	"mDataProp": "ktrJktTglKrmKeCabang",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "notaPesananBarang",
	"mDataProp": "notaPesananBarang",
	"aTargets": [11],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var cabangKeterangan = $('#filter_cabangKeterangan input').val();
						if(cabangKeterangan){
							aoData.push(
									{"name": 'sCriteria_cabangKeterangan', "value": cabangKeterangan}
							);
						}

						var cabangTglDiterima = $('#search_cabangTglDiterima').val();
						var cabangTglDiterimaDay = $('#search_cabangTglDiterima_day').val();
						var cabangTglDiterimaMonth = $('#search_cabangTglDiterima_month').val();
						var cabangTglDiterimaYear = $('#search_cabangTglDiterima_year').val();

						if(cabangTglDiterima){
							aoData.push(
									{"name": 'sCriteria_cabangTglDiterima', "value": "date.struct"},
									{"name": 'sCriteria_cabangTglDiterima_dp', "value": cabangTglDiterima},
									{"name": 'sCriteria_cabangTglDiterima_day', "value": cabangTglDiterimaDay},
									{"name": 'sCriteria_cabangTglDiterima_month', "value": cabangTglDiterimaMonth},
									{"name": 'sCriteria_cabangTglDiterima_year', "value": cabangTglDiterimaYear}
							);
						}

						var goods = $('#filter_goods input').val();
						if(goods){
							aoData.push(
									{"name": 'sCriteria_goods', "value": goods}
							);
						}

						var hoTglDiterima = $('#search_hoTglDiterima').val();
						var hoTglDiterimaDay = $('#search_hoTglDiterima_day').val();
						var hoTglDiterimaMonth = $('#search_hoTglDiterima_month').val();
						var hoTglDiterimaYear = $('#search_hoTglDiterima_year').val();

						if(hoTglDiterima){
							aoData.push(
									{"name": 'sCriteria_hoTglDiterima', "value": "date.struct"},
									{"name": 'sCriteria_hoTglDiterima_dp', "value": hoTglDiterima},
									{"name": 'sCriteria_hoTglDiterima_day', "value": hoTglDiterimaDay},
									{"name": 'sCriteria_hoTglDiterima_month', "value": hoTglDiterimaMonth},
									{"name": 'sCriteria_hoTglDiterima_year', "value": hoTglDiterimaYear}
							);
						}

						var hoTglDiteruskan = $('#search_hoTglDiteruskan').val();
						var hoTglDiteruskanDay = $('#search_hoTglDiteruskan_day').val();
						var hoTglDiteruskanMonth = $('#search_hoTglDiteruskan_month').val();
						var hoTglDiteruskanYear = $('#search_hoTglDiteruskan_year').val();

						if(hoTglDiteruskan){
							aoData.push(
									{"name": 'sCriteria_hoTglDiteruskan', "value": "date.struct"},
									{"name": 'sCriteria_hoTglDiteruskan_dp', "value": hoTglDiteruskan},
									{"name": 'sCriteria_hoTglDiteruskan_day', "value": hoTglDiteruskanDay},
									{"name": 'sCriteria_hoTglDiteruskan_month', "value": hoTglDiteruskanMonth},
									{"name": 'sCriteria_hoTglDiteruskan_year', "value": hoTglDiteruskanYear}
							);
						}

						var jumlah = $('#filter_jumlah input').val();
						if(jumlah){
							aoData.push(
									{"name": 'sCriteria_jumlah', "value": jumlah}
							);
						}

						var ktrJktTglBukaDO = $('#search_ktrJktTglBukaDO').val();
						var ktrJktTglBukaDODay = $('#search_ktrJktTglBukaDO_day').val();
						var ktrJktTglBukaDOMonth = $('#search_ktrJktTglBukaDO_month').val();
						var ktrJktTglBukaDOYear = $('#search_ktrJktTglBukaDO_year').val();

						if(ktrJktTglBukaDO){
							aoData.push(
									{"name": 'sCriteria_ktrJktTglBukaDO', "value": "date.struct"},
									{"name": 'sCriteria_ktrJktTglBukaDO_dp', "value": ktrJktTglBukaDO},
									{"name": 'sCriteria_ktrJktTglBukaDO_day', "value": ktrJktTglBukaDODay},
									{"name": 'sCriteria_ktrJktTglBukaDO_month', "value": ktrJktTglBukaDOMonth},
									{"name": 'sCriteria_ktrJktTglBukaDO_year', "value": ktrJktTglBukaDOYear}
							);
						}

						var ktrJktTglDiproses = $('#search_ktrJktTglDiproses').val();
						var ktrJktTglDiprosesDay = $('#search_ktrJktTglDiproses_day').val();
						var ktrJktTglDiprosesMonth = $('#search_ktrJktTglDiproses_month').val();
						var ktrJktTglDiprosesYear = $('#search_ktrJktTglDiproses_year').val();

						if(ktrJktTglDiproses){
							aoData.push(
									{"name": 'sCriteria_ktrJktTglDiproses', "value": "date.struct"},
									{"name": 'sCriteria_ktrJktTglDiproses_dp', "value": ktrJktTglDiproses},
									{"name": 'sCriteria_ktrJktTglDiproses_day', "value": ktrJktTglDiprosesDay},
									{"name": 'sCriteria_ktrJktTglDiproses_month', "value": ktrJktTglDiprosesMonth},
									{"name": 'sCriteria_ktrJktTglDiproses_year', "value": ktrJktTglDiprosesYear}
							);
						}

						var ktrJktTglDiterima = $('#search_ktrJktTglDiterima').val();
						var ktrJktTglDiterimaDay = $('#search_ktrJktTglDiterima_day').val();
						var ktrJktTglDiterimaMonth = $('#search_ktrJktTglDiterima_month').val();
						var ktrJktTglDiterimaYear = $('#search_ktrJktTglDiterima_year').val();

						if(ktrJktTglDiterima){
							aoData.push(
									{"name": 'sCriteria_ktrJktTglDiterima', "value": "date.struct"},
									{"name": 'sCriteria_ktrJktTglDiterima_dp', "value": ktrJktTglDiterima},
									{"name": 'sCriteria_ktrJktTglDiterima_day', "value": ktrJktTglDiterimaDay},
									{"name": 'sCriteria_ktrJktTglDiterima_month', "value": ktrJktTglDiterimaMonth},
									{"name": 'sCriteria_ktrJktTglDiterima_year', "value": ktrJktTglDiterimaYear}
							);
						}

						var ktrJktTglKrmKeCabang = $('#search_ktrJktTglKrmKeCabang').val();
						var ktrJktTglKrmKeCabangDay = $('#search_ktrJktTglKrmKeCabang_day').val();
						var ktrJktTglKrmKeCabangMonth = $('#search_ktrJktTglKrmKeCabang_month').val();
						var ktrJktTglKrmKeCabangYear = $('#search_ktrJktTglKrmKeCabang_year').val();

						if(ktrJktTglKrmKeCabang){
							aoData.push(
									{"name": 'sCriteria_ktrJktTglKrmKeCabang', "value": "date.struct"},
									{"name": 'sCriteria_ktrJktTglKrmKeCabang_dp', "value": ktrJktTglKrmKeCabang},
									{"name": 'sCriteria_ktrJktTglKrmKeCabang_day', "value": ktrJktTglKrmKeCabangDay},
									{"name": 'sCriteria_ktrJktTglKrmKeCabang_month', "value": ktrJktTglKrmKeCabangMonth},
									{"name": 'sCriteria_ktrJktTglKrmKeCabang_year', "value": ktrJktTglKrmKeCabangYear}
							);
						}

						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						var notaPesananBarang = $('#filter_notaPesananBarang input').val();
						if(notaPesananBarang){
							aoData.push(
									{"name": 'sCriteria_notaPesananBarang', "value": notaPesananBarang}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
