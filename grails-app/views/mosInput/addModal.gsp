
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'MosAdd.label', default: 'Input Material')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
        var show;
        var loadForm;
        var shrinkTableLayout;
        var expandTableLayout;
        $(function(){

            $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

            $('.box-action').click(function(){
                return false;
            });




            loadForm = function(data, textStatus){
                $('#MosAdd-form').empty();
                $('#MosAdd-form').append(data);
            }

            shrinkTableLayout = function(){
                if($("#MosAdd-table").hasClass("span12")){
                    $("#MosAdd-table").toggleClass("span12 span5");
                }
                $("#MosAdd-form").css("display","block");
            }

            expandTableLayout = function(){
                if($("#MosAdd-table").hasClass("span5")){
                    $("#MosAdd-table").toggleClass("span5 span12");
                }
                $("#MosAdd-form").css("display","none");
            }


        });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
    <ul class="nav pull-right">
        <li></li>
        <li></li>
        %{--<li class="separator"></li>--}%
    </ul>
</div>
<div class="box">
    <div class="span12" id="MosAdd-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>

        <g:render template="addModalDataTables" />
        <g:field type="button" onclick="tambahReq();" class="btn btn-primary create" name="tambah" id="tambah" value="${message(code: 'default.button.upload.label', default: 'Add Selected')}" />
        <span style="margin-left: 390px"><button id='closeSpkDetail' type="button" class="btn btn-primary">Close</button></span>
        %{--<div class="modal-footer"><button id='closeSpkDetail' type="button" class="btn btn-primary">Close</button></div>--}%
    </div>
    <div class="span7" id="MosAdd-form" style="display: none;"></div>
</div>
</body>
</html>
