<%@ page import="com.kombos.reception.Reception; com.kombos.parts.Claim" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">

		<r:require modules="baseapplayout" />
    <g:javascript>
		$(function(){
			$('#noWo').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/mosInput/kodeList', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});
    </g:javascript>
		<g:javascript disposition="head">
        var noUrut = 1;
	var show;
	var cekStatus = "${aksi}";
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){

	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/mos/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/mos/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#mos-form').empty();
    	$('#mos-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#mos-table").hasClass("span12")){
   			$("#mos-table").toggleClass("span12 span5");
        }
        $("#mos-form").css("display","block");
   	}
   	
   	expandTableLayout = function(){
   		if($("#mos-table").hasClass("span5")){
   			$("#mos-table").toggleClass("span5 span12");
   		}
        $("#mos-form").css("display","none");
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#mos-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/mosInput/massdelete',
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadMosTable();
    		}
		});
		
   	}

});

  //darisini
	$("#mosAddModal").on("show", function() {
		$("#mosAddModal .btn").on("click", function(e) {
			$("#mosAddModal").modal('hide');
		});
	});
	$("#mosAddModal").on("hide", function() {
		$("#mosAddModal a.btn").off("click");
	});
    loadMosAddModal = function(){
    	var noWo = $('#noWo').val();
    	var tanggal = $('#tanggal').val();
    	if(noWo!="" && tanggal!=""){
            $("#mosAddContent").empty();

            $.ajax({type:'POST', url:'${request.contextPath}/mosInput/addModal',
                success:function(data,textStatus){
                        $("#mosAddContent").html(data);
                        $("#mosAddModal").modal({
                            "backdrop" : "static",
                            "keyboard" : true,
                            "show" : true
                        }).css({'width': '650px','margin-left': function () {return -($(this).width() / 2);}});

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
        }else{
            alert('Data Belum Lengkap');
        }


    }
       cek = function(){
           var noWo = $('#noWo').val();
           $.ajax({
            url:'${request.contextPath}/mosInput/cek',
            type: "POST", // Always use POST when deleting data
            data : {noWo:noWo},
            success : function(data){
                   $("#data").html(data)
            },
            error: function(xhr, textStatus, errorThrown) {
            alert('Internal Server Error');
            }
        });
       }
       tambahReq = function(){
           var checkMos =[];
            $("#MosAdd-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    var nRow = $(this).parents('tr')[0];

					checkMos.push(nRow);
                }
            });
            if(checkMos.length<1){
                alert('Anda belum memilih data yang akan ditambahkan');
                loadMosAddModal();
            }else {
				for (var i=0;i < checkMos.length;i++){

					var aData = MosAddTable.fnGetData(checkMos[i]);

					mosInputTable.fnAddData({
						'id': aData['id'],
						'norut': noUrut,
						'goods': aData['goods'],
						'goods2': aData['goods2'],
						'qty': aData['qty'],
						'satuan': aData['satuan']
						});
					noUrut++;
					$('#qty-'+aData['id']).autoNumeric('init',{
						vMin:'0',
						vMax:'999999999999999999',
						mDec: null,
						aSep:''
					});
				}
                loadMosAddModal();
			}

      }

      updateMos = function(){
            var nomor = $("#u_nomos").val();
            var formMos = $('#mos-table').find('form');
            var qtyNol = false
            var checkMos =[];
            $("#mos-table tbody .row-select").each(function() {
            if(this.checked){
                var id = $(this).next("input:hidden").val();
                var nRow = $(this).parents('tr')[0];
                var aData = mosInputTable.fnGetData(nRow);
                var qtyInput = $('#qty-' + aData['id'], nRow);

                if(qtyInput.val()==0 || qtyInput.val()==""){
                       qtyNol = true
                }
                checkMos.push(id);
                formMos.append('<input type="hidden" name="qty-'+aData['id'] + '" value="'+qtyInput[0].value+'" class="deleteafter">');
            }
            });
            if(checkMos.length<1){
                alert('Anda belum memilih data yang akan ditambahkan');
                 reloadMosTable();
            }else if(qtyNol == true){
                toastr.error('<div>Quantity Tidak Boleh NOL</div>');
                formMos.find('.deleteafter').remove();
            }else{
                $("#ids").val(JSON.stringify(checkMos));
                $.ajax({
                    url:'${request.contextPath}/mosInput/ubahData',
                    type: "POST", // Always use POST when deleting data
                    data : formMos.serialize(),
                    success : function(data){
                        window.location.href = '#/viewMaterialPerWo' ;
                        toastr.success('<div>Data Sukses Diubah.</div>');
                        formMos.find('.deleteafter').remove();
                        $("#ids").val('');
                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });

                checkMos = [];
            }
       }
       deleteData = function(){
                var checkMos =[];
                $("#mos-table tbody .row-select").each(function() {
                    if(this.checked){
                        var id = $(this).next("input:hidden").val();
                        var row = $(this).closest("tr").get(0);
                        mosInputTable.fnDeleteRow(mosInputTable.fnGetPosition(row));
                    }
                });
      }
        createRequest = function(){
            var noWo = $("#noWo").val();
            var tanggal = $("#tanggal").val();
            var noMos = $("#noMos").val();
            var qtyNol = false
            var formMos = $('#mos-table').find('form');

            var checkMos =[];
            $("#mos-table tbody .row-select").each(function() {
            if(this.checked){
                var id = $(this).next("input:hidden").val();
                var nRow = $(this).parents('tr')[0];
                var aData = mosInputTable.fnGetData(nRow);
                var qtyInput = $('#qty-' + aData['id'], nRow);
                    if(qtyInput.val()==0 || qtyInput.val()==""){
                       qtyNol = true
                    }
                checkMos.push(id);

                formMos.append('<input type="hidden" name="qty-'+aData['id'] + '" value="'+qtyInput[0].value+'" class="deleteafter">');
            }
            });
            if(checkMos.length<1){
                alert('Anda belum memilih data yang akan ditambahkan');
            }else if(tanggal=="" || noWo==""){
                toastr.error('<div>Data Belum Lengkap</div>');
            }else if(qtyNol == true){
                toastr.error('<div>Quantity Tidak Boleh NOL</div>');
                formMos.find('.deleteafter').remove();
            }else{
                var r = confirm("Anda yakin data akan disimpan?");
                if(r==true){
                 var rows = mosInputTable.fnGetNodes();
                    $("#ids").val(JSON.stringify(checkMos));
                    $.ajax({
                        url:'${request.contextPath}/mosInput/req',
                        type: "POST", // Always use POST when deleting data
                        data : formMos.serialize(),
                        success : function(data){
                          if(data=='fail'){
                            toastr.error('<div>Error</div>');
                          }else{
                            if(rows.length > 10){
                                $("#mos-table tbody .row-select").each(function() {
                                    if(this.checked){
                                        var row = $(this).closest("tr").get(0);
                                        mosInputTable.fnDeleteRow(mosInputTable.fnGetPosition(row));
                                        reloadMosTable();
                                    }
                                });
                                formMos.append('<input type="hidden" name="noMosBefore" value="'+data+'" class="deleteafter">');
                                toastr.success('<div>Sukses</div>');
                            }else{
                               window.location.href = '#/viewMaterialPerWo' ;
                               toastr.success('<div>Succes</div>');
                                formMos.find('.deleteafter').remove();
                                $("#ids").val('');
                            }

                          }

                        },
                    error: function(xhr, textStatus, errorThrown) {
                    alert('Internal Server Error');
                    }
                    });

                    checkMos = [];
                }
            }
        };

</g:javascript>

	</head>
	<body>
	<div class="navbar box-header no-border">
        <span class="pull-left">
        <g:if test="${aksi=='ubah'}">
            Edit Data Mos
        </g:if>
        <g:else>
            Input Data Mos
        </g:else>
        </span>
        <ul class="nav pull-right">
            <li>&nbsp;</li>
		</ul>
	</div>
    <br>
	<div class="box">
		<div class="span12" id="mos-table">
            <form id="form-mos" class="form-horizontal">
                <input type="hidden" name="ids" id="ids" value="">
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label" for="t951Tanggal" style="text-align: left;">
                                Nomor WO
                            </label>
                            <div id="filter_wo" class="controls">
                                <g:if test="${aksi=='ubah'}">
                                    <g:textField name="u_nowo" id="u_nowo" value="${u_noWo}" readonly="readonly" />
                                    <g:hiddenField name="aksi" value="ubah" />
                                </g:if>
                                <g:else>
                                <g:textField name="noWo" id="noWo" class="typeahead" onblur="cek()" autocomplete="off" maxlength="220"  />
                                    <span id="data" style="padding-left: 50px;"></span>
                                </g:else>
                            </div>

                        </div>
                        <div class="control-group">
                            <label class="control-label" for="t951Tanggal" style="text-align: left;">
                                Nomor MOS
                            </label>
                            <div id="filter_nomor" class="controls">
                                <g:if test="${aksi=='ubah'}">
                                    <g:textField name="u_nomos" id="u_nomos" value="${u_noMos}" readonly="readonly" />
                                </g:if>
                                <g:else>
                                    <g:textField name="noMos" id="noMos" maxlength="20" value="${noMos}" readonly="readonly"  />
                                </g:else>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="userRole" style="text-align: left;">
                                Tanggal MOS
                            </label>
                            <div id="filter_status" class="controls">
                                <g:if test="${aksi=='ubah'}">
                                    <g:textField name="u_tanggal" value="${u_tanggal}" readonly="readonly" />
                                </g:if>
                                <g:else>
                                    <ba:datePicker id="tanggal" name="tanggal" precision="day" format="dd-MM-yyyy"  value="" required="true"  />
                                </g:else>
                            </div>
                         </div>

                    </fieldset>
                   </form>
                    <fieldset>
                        <table>
                            <tr>
                                <td>
                                    <button style="width: 170px;height: 30px; border-radius: 5px" class="btn btn-primary" name="view" id="add" onclick="loadMosAddModal()">Add Parts</button>
                                </td>
                            </tr>
                        </table>
                    </fieldset><br>
			<g:render template="dataTables" />
            <g:field type="button" onclick="deleteData()" class="btn btn-cancel delete" name="deleteData" id="delData" value="${message(code: 'default.button.upload.label', default: 'Delete')}" />
            <g:if test="${aksi=='ubah'}">
                <g:field type="button" onclick="updateMos()" class="btn btn-primary create" name="Edit"  value="${message(code: 'default.button.upload.label', default: 'Update')}" />
            </g:if>
            <g:else>
                <g:field type="button" onclick="createRequest()" class="btn btn-primary create" name="tambahSave"  value="${message(code: 'default.button.upload.label', default: 'Save')}" />
            </g:else>
            <g:field type="button" onclick="window.location.replace('#/viewMaterialPerWo');" class="btn btn-cancel" name="cancel" id="cancel" value="Cancel" />
		</div>
		<div class="span7" id="mos-form" style="display: none;"></div>
	</div>
    <div id="mosAddModal" class="modal fade">
        <div class="modal-dialog" style="width: 650px;">
            <div class="modal-content" style="width: 650px;">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 650px;">
                    <div id="mosAddContent"/>
                <div class="iu-content"></div>

                </div>
            </div>
        </div>
    </div>
</body>
</html>

<g:javascript>
      <g:if test="${aksi=='create'}">
        document.getElementById("noWo").focus();
      </g:if>
</g:javascript>