<%@ page import="com.kombos.administrasi.NamaManPower" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="foreman.label" default="Status Foreman" /></title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>
			var show;
			var edit;
            var findData;
			$(function(){
                findData = function(){
                    foremanTable.fnDraw();
                };
            });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="foreman.label" default="Status Foreman" /></span>
    <ul class="nav pull-right">
        <li class="separator"></li>
    </ul>
</div>
<div class="box">
    <div class="span12" id="complaint-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <table>
            <tr>
                <td style="padding: 5px">
                    Cari Nama Foreman
                </td>
                <td style="padding: 5px">
                    <g:textField name="searchNama" id="searchNama" value="" />
                </td>
                <td style="padding: 5px">
                    <g:field type="button" class="btn cancel pull-right box-action" onclick="findData();"
                             name="view" id="view" value="${message(code: 'default.search.label', default: 'Search')}" />
                </td>
            </tr>
        </table>
        <g:render template="dataTables" />
    </div>
    <div class="span7" id="complaint-form" style="display: none;"></div>
</div>
</body>
</html>
