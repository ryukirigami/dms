
<%@ page import="com.kombos.parts.Goods" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'progressKendaraan.label', default: 'View Progress Kendaraan')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
        <g:javascript>
		$(function(){
			$('#kata_kunci').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/viewProgressKendaraan/daftar?kriteria='+$('#kriteria').val(), { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});
        </g:javascript>
		<g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	var buatRevisi;
	var loadPopupRevisi;
	$(function(){ 

	   buatRevisi = function(){
	            if($('#noWo').html=='-'){
	                alert('Harap Isi Data Terlebih Dahulu');
	            }else{
                    loadPopupRevisi();
	            }
          };
    $("#revisiModal").on("show", function() {
		$("#revisiModal .btn").on("click", function(e) {
			$("#revisiModal").modal('hide');
		});
	});
	$("#revisiModal").on("hide", function() {
		$("#revisiModal a.btn").off("click");
	});
       loadPopupRevisi = function(){
        var wo = $('#noWo').html();
        $("#revisiContent").empty();
        	$.ajax({type:'POST', url:'${request.contextPath}/viewProgressKendaraan/revisiJanji',
        	data : {noWo : wo},
   			success:function(data,textStatus){
					$("#revisiContent").html(data);
				    $("#revisiModal").modal({
						"backdrop" : "false",
						"keyboard" : true,
						"show" : true
					}).css({'width': '600px','margin-left': function () {return -($(this).width() / 2);}});

   			},error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});

       }

        function loadData(){
            var kKunci  = $('#kata_kunci').val();
            var kriteria = $('#kriteria').val();
            $.ajax({type:'POST', url:'${request.contextPath}/viewProgressKendaraan/viewData',
                data : {kriteria:kriteria, kataKunci : kKunci},
                success:function(data){
                       if(data.noWo=='-'){
                            $('#revisi').prop('disabled',true)
                            $('#revisi').removeClass('btn btn-primary');
                            $('#revisi').addClass('btn btn-cancel');
                       }else{
                            $('#revisi').prop('disabled',false)
                            $('#revisi').removeClass('btn btn-cancel');
                            $('#revisi').addClass('btn btn-primary');
                       }
                      $('#noWo').html(data.noWo);
                      $('#noPol').html(data.noPol);
                      $('#customerIn').html(data.customerIn);
                      $('#deliveryTime').html(data.deliveryTime);
                      $('#operation').html(data.operation);

                      $('#seninDate').html(data.seninDate);
                      $('#selasaDate').html(data.selasaDate);
                      $('#rabuDate').html(data.rabuDate);
                      $('#kamisDate').html(data.kamisDate);
                      $('#jumatDate').html(data.jumatDate);
                      $('#sabtuDate').html(data.sabtuDate);

                },error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
   		    });
        }
	
        $('#view').click(function(e){
            loadData();
            reloadKendaraanTungguTable();
        });
        $('#clear').click(function(e){
            $('#kata_kunci').val("");
        });
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/goods/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/goods/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#goods-form').empty();
    	$('#goods-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#goods-table").hasClass("span12")){
   			$("#goods-table").toggleClass("span12 span5");
        }
        $("#goods-form").css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
   		if($("#goods-table").hasClass("span5")){
   			$("#goods-table").toggleClass("span5 span12");
   		}
        $("#goods-form").css("display","none");
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#goods-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/goods/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadGoodsTable();
    		}
		});
		
   	}
    revisi = function(){

    }
});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
	</div>
	<div class="box">
        <div class="row-fluid" style="margin-bottom: 20px">
            <div class="span6" id="filer_span">
                <g:if test="${flash.message}">
                    <div class="message" role="status">
                        ${flash.message}
                    </div>
                </g:if>

                <fieldset>
                    <table style="padding-right: 10px">
                        <tr>
                            <td style="width: 130px">
                                <label class="control-label" for="progress_view">
                                    <g:message code="progress.view.label" default="View Berdasarkan" />&nbsp;
                                </label>&nbsp;&nbsp;
                            </td>
                            <td>
                                <div id="filter_view" class="controls">
                                    <select id="kriteria">
                                        <option value="noPol">No Polisi</option>
                                        <option value="noWo">No WO</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 130px">
                                <label class="control-label" for="progress_view">
                                    <g:message code="progress.view.label" default="Kata Kunci" />&nbsp;
                                </label>&nbsp;&nbsp;
                            </td>
                            <td>
                                <div id="kata_kuncis" class="controls">
                                    <g:textField name="kata_kunci" id="kata_kunci" autocomplete="off"/>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" >
                                <div class="controls" style="right: 0">
                                    <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-primary" name="view" id="view" >Search</button>
                                    <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-cancel" name="clear" id="clear" >Clear</button>
                                </div>
                            </td>
                        </tr>
                    </table>

                </fieldset>
            </div>
            <div class="span1 offset4">
                <button id="revisi" style="width: 200px;height: 30px; border-radius: 5px" onclick="buatRevisi();" class="btn btn-cancel" disabled>Revisi Janji Penyerahan</button>
            </div>
        </div>
		<div class="row-fluid" style="margin-bottom: 20px">
            <div class="span3" >
                <table class="table table-bordered table-dark table-hover">
                    <tbody>
                        <tr>
                            <td class="property-label">
                                NO WO
                            </td>
                            <td class="property-value" id="noWo">-
                            </td>
                        </tr>
                        <tr>
                            <td class="property-label">
                                No Polisi
                            </td>
                            <td class="property-value" id="noPol">-
                            </td>
                        </tr>
                        <tr>
                            <td class="property-label">
                                Customer In
                            </td>
                            <td class="property-value" id="customerIn">-
                            </td>
                        </tr>
                        <tr>
                            <td class="property-label">
                                 Delivery Time
                            </td>
                            <td class="property-value" id="deliveryTime">-
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="span3">
                <table class="table table-bordered table-dark table-hover">
                    <tbody>
                        <tr>
                            <td class="property-label">
                                Task List
                            </td>
                            <td class="property-value" id="operation">-
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <g:render template="datatables"/>
            </div>
            <div class="span6">
                <g:render template="datatables2"/>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <g:render template="datatables3"/>
            </div>
            <div class="span6">
                <g:render template="datatables4"/>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <g:render template="datatables5"/>
            </div>
            <div class="span6">
                <g:render template="datatables6"/>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                 <img src="images/progressKendaraan/jamAktual.png" style="width: 10px" /> : Jam Aktual  &nbsp; &nbsp; &nbsp;
                <img src="images/progressKendaraan/jamPenyerahan.png" style="width: 10px" /> : Jam Janji Penyerahan
            </div>
        </div>
    </div>
    <div id="revisiModal" class="modal fade">
        <div class="modal-dialog" style="width: 600px;">
            <div class="modal-content" style="width: 600px;">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 600px;">
                    <div id="revisiContent"/>
                    %{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}%
                    <div class="iu-content"></div>


                </div>
            </div>
        </div>
    </div>

    </body>
</html>
