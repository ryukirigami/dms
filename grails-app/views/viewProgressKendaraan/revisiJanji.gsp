<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">

    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">

    $(document).ready(function()
    {
        revisi = function(){
                var jamRevisi = $('#jamRevisi').val();
                var menitRevisi = $('#menitRevisi').val();
                var noWo = "${noWo}";
                var tglRevisi = $('#tglRevisi').val();

                $.ajax({
                    url:'${request.contextPath}/viewProgressKendaraan/editRevisi',
                    type: "POST", // Always use POST when deleting data
                    data : {menitRevisi:menitRevisi,jamRevisi : jamRevisi,noWo:noWo,tglRevisi:tglRevisi},
                    success : function(data){
                        toastr.success('<div>Ubah Sukses</div>');
                          $('#view').click();
                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });



        }
    });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    Reception - Revisi Jam Janji Penyerahan
</div>
<table class="table table-bordered table-hover">
    <tbody>
    <tr>
        <td class="span2" style="text-align: right;">
            <label class="control-label">
                Nama Customer
            </label>

        </td>

        <td class="span3">
            <label class="control-label">
                ${namaCustomer}
            </label>
        </td>
    </tr>


    <tr>
        <td class="span2" style="text-align: right;">
            <label class="control-label">
                Alamat
            </label>

        </td>

        <td class="span3">
            <label class="control-label">
                ${alamat}
               </label>
        </td>
    </tr>

    <tr>
        <td class="span2" style="text-align: right;">
            <label class="control-label">
                Telp
            </label>

        </td>

        <td class="span3">
            <label class="control-label">
                ${telp}
            </label>
        </td>
    </tr>
    <tr>
        <td class="span2" style="text-align: right;">
            <label class="control-label">
                Mobil
            </label>

        </td>

        <td class="span3">
            <label class="control-label">
                ${mobil}
            </label>
        </td>
    </tr>
    <tr>
        <td class="span2" style="text-align: right;">
            <label class="control-label">
                Tanggal/Jam Janji Penyerahan
            </label>

        </td>

        <td class="span3">
            %{--<g:datePicker name="tglRevisi" id="tglRevisi" precision="day"  format="dd-mm-yyyy"/>--}%
            <ba:datePicker name="tglRevisi" id="tglRevisi" precision="day" value="${tglPenyerahan}" format="dd-MM-yyyy" required="true"/>


            <br/>
            H :
            <select id="jamRevisi" name="jamRevisi"  style="width: 60px" required="">
                %{
                    for (int i=7;i<17;i++){
                        if(i<10){
                            if(tglPenyerahan.getHours()==i){
                                out.println('<option value="'+i+'" selected="selected">0'+i+'</option>');
                            }else{
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }


                        } else {
                            if(tglPenyerahan.getHours()==i){
                                out.println('<option value="'+i+'" selected="selected">'+i+'</option>');
                            }else{
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }

                    }
                }%
            </select>
            <select id="menitRevisi" name="menitRevisi" style="width: 60px" required="">
                %{
                    for (int i=0;i<60;i+=15){
                        if(i<10){
                            if(tglPenyerahan.getMinutes()==i){
                                out.println('<option value="'+i+'" selected="selected">0'+i+'</option>');
                            }else{
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(tglPenyerahan.getMinutes()==i){
                                out.println('<option value="'+i+'" selected="selected">'+i+'</option>');
                            }else{
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }

                        }
                    }
                }%
            </select> m
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <g:field type="button" onclick="revisi()" class="btn btn-primary create" name="btnRevisi" id="btnRevisi" value="Save" />
            <button id='closeRevisi' type="button" class="btn btn-primary">Close</button>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>

<g:javascript>
$(function(){

       $("#tglRevisi_day").css({width : '100px'});
       $("#tglRevisi_month").css({width : '100px'});
       $("#tglRevisi_year").css({width : '100px'});

   });

</g:javascript>
