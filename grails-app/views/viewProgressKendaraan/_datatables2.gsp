<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="kendaraanTunggu2_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>
        <th colspan="10" style="text-align: center">
            <b> Selasa </b>
        </th>
    </tr>
    <tr>
        <th colspan="10" style="text-align:center" id="selasaDate">

        </th>

    </tr>
    <tr>
        <th style="border-bottom: none;padding: 5px;" >
            7.00
        </th>
        <th style="border-bottom: none;padding: 5px;">
            8.00
        </th>
        <th style="border-bottom: none;padding: 5px;">
            9.00
        </th>
        <th style="border-bottom: none;padding: 5px;">
            10.00
        </th>
        <th style="border-bottom: none;padding: 5px;">
            11.00
        </th>
        <th style="border-bottom: none;padding: 5px;">
            12.00
        </th>
        <th style="border-bottom: none;padding: 5px;">
            13.00
        </th>
        <th style="border-bottom: none;padding: 5px;">
            14.00
        </th>
        <th style="border-bottom: none;padding: 5px;">
            15.00
        </th>
        <th style="border-bottom: none;padding: 5px;">
            16.00
        </th>
    </tr>


    </thead>
</table>

<g:javascript>
var kendaraanTunggu2Table;
var reloadKendaraanTungguTable;
$(function(){


     $('#view').click(function(e){
        e.stopPropagation();
		kendaraanTunggu2Table.fnDraw();
	});

	
	reloadKendaraanTungguTable = function() {
		kendaraanTunggu2Table.fnDraw();
	}
    $('#search_satuan').change(function(){
        kendaraanTunggu2Table.fnDraw();
    });
	var recordsKendaraanTungguPerPage = [];
    var anKendaraanTungguSelected;
    var jmlRecKendaraanTungguPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	kendaraanTunggu2Table.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	kendaraanTunggu2Table = $('#kendaraanTunggu2_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		"bInfo" : false,
       	'sPaginationType': 'bootstrap',

		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
               $(nRow).children().each(function(index, td) {
                        if(aData.data=='' || aData.data==null){
                            if(index == 0){
                                $(td).html("-");
                                $(td).css("background-color","white")
                            }
                            if(index == 1){
                                $(td).html("");
                                $(td).css("background-color","white")
                            }if(index == 2){
                                $(td).html("");
                                $(td).css("background-color","white")
                            }if(index == 3){
                                $(td).html("");
                                $(td).css("background-color","white")
                            }if(index == 4){
                                $(td).html("");
                                $(td).css("background-color","white")
                            }if(index == 5){
                                $(td).html("");
                                $(td).css("background-color","white")
                            }if(index == 6){
                                $(td).html("");
                                $(td).css("background-color","white")
                            }if(index == 7){
                                $(td).html("");
                                $(td).css("background-color","white")
                            }if(index == 8){
                                $(td).html("");
                                $(td).css("background-color","white")
                            }if(index == 9){
                                $(td).html("");
                                $(td).css("background-color","white")
                            }
                        }else{
                            if(index == 0){
                                if ($(td).html() == "Y") {
                                        if(aData.menitAktual<=0){
                                            $(td).html("<img src='images/progressKendaraan/jamAktual.png' style='display: block; ' title='"+aData.jamAktual+"'/>");
                                        }else if(aData.menitAktual<=15){
                                            $(td).html(" <img src='images/progressKendaraan/jamAktual.png' style='display: block; ' title='"+aData.jamAktual+"'/>");
                                        }else if(aData.menitAktual<=30){
                                            $(td).html("<img src='images/progressKendaraan/jamAktual.png' style='display: block; margin: 0 auto;' title='"+aData.jamAktual+"'/>");
                                        }else{
                                            $(td).html(" <img src='images/progressKendaraan/jamAktual.png' style='display: block; margin-left:30px;' title='"+aData.jamAktual+"'/>");
                                        }
                                }else if ($(td).html() == "J") {
                                        if(aData.menitJanji<=0){
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; ' title='"+aData.janjiPenyerahan+"'/>");
                                        }else if(aData.menitJanji<=15){
                                            $(td).html(" <img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; ' title='"+aData.janjiPenyerahan+"'/>");
                                        }else if(aData.menitJanji<=30){
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; margin: 0 auto;' title='"+aData.janjiPenyerahan+"'/>");
                                        }else{
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; margin-left:30px;' title='"+aData.janjiPenyerahan+"'/>");
                                        }
                                }else if ($(td).html() == "E") {
                                            $(td).html("&nbsp; <img src='images/progressKendaraan/jamPenyerahan.png' title='"+aData.janjiPenyerahan+"'/> &nbsp;" +
                                             "<img src='images/progressKendaraan/jamAktual.png' title='"+aData.jamAktual+"'/>");
                                }

                                if(aData.hariIni>aData.hariJanji){
                                    $(td).css("background-color","white")
                                }else if(aData.hariIni==aData.hariJanji){
                                        if(aData.jamJanji<7){
                                            $(td).css("background-color","white")
                                        }else{
                                             $(td).css("background-color","green")
                                        }
                                }

                                if(aData.hariIni < aData.hariDatang){
                                    $(td).css("background-color","white")
                                }else if(aData.hariIni==aData.hariDatang){
                                        if(aData.jamDatang>7){
                                            $(td).css("background-color","white")
                                        }else{
                                             $(td).css("background-color","green")
                                        }

                                }

                                if(aData.hariIni == aData.hariClockOn){
                                    if(aData.jamClockOn==7){
                                        $(td).css("background-color","blue")
                                    }
                                }

                                if(aData.hariIni == aData.hariPanggil){
                                    if(aData.jamPanggil==7){
                                        $(td).css("background-color","yellow")
                                        $(td).html("<a href='#' title='"+ aData.alasan +"' style='margin-left:15px'>*</a>")
                                    }
                                }

                                if(aData.hariIni == aData.hariClockOff){
                                    if(aData.jamClockOff==7){
                                        $(td).css("background-color","red")
                                    }
                                }

                            }
                            if(index == 1){
                                if ($(td).html() == "Y") {
                                        if(aData.menitAktual<=0){
                                            $(td).html("<img src='images/progressKendaraan/jamAktual.png' style='display: block; ' title='"+aData.jamAktual+"'/>");
                                        }else if(aData.menitAktual<=15){
                                            $(td).html(" <img src='images/progressKendaraan/jamAktual.png' style='display: block; ' title='"+aData.jamAktual+"'/>");
                                        }else if(aData.menitAktual<=30){
                                            $(td).html("<img src='images/progressKendaraan/jamAktual.png' style='display: block; margin: 0 auto;' title='"+aData.jamAktual+"'/>");
                                        }else{
                                            $(td).html(" <img src='images/progressKendaraan/jamAktual.png' style='display: block; margin-left:30px;' title='"+aData.jamAktual+"'/>");
                                        }
                                }else if ($(td).html() == "J") {
                                        if(aData.menitJanji<=0){
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; ' title='"+aData.janjiPenyerahan+"'/>");
                                        }else if(aData.menitJanji<=15){
                                            $(td).html(" <img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; ' title='"+aData.janjiPenyerahan+"'/>");
                                        }else if(aData.menitJanji<=30){
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; margin: 0 auto;' title='"+aData.janjiPenyerahan+"'/>");
                                        }else{
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; margin-left:30px;' title='"+aData.janjiPenyerahan+"'/>");
                                        }
                                }else if ($(td).html() == "E") {
                                            $(td).html("&nbsp; <img src='images/progressKendaraan/jamPenyerahan.png' title='"+aData.janjiPenyerahan+"'/> &nbsp;" +
                                             "<img src='images/progressKendaraan/jamAktual.png' title='"+aData.jamAktual+"'/>");
                                }

                                if(aData.hariIni>aData.hariJanji){
                                    $(td).css("background-color","white")
                                }else if(aData.hariIni==aData.hariJanji){
                                        if(aData.jamJanji<8){
                                            $(td).css("background-color","white")
                                        }else{
                                             $(td).css("background-color","green")
                                        }
                                }

                                if(aData.hariIni < aData.hariDatang){
                                    $(td).css("background-color","white")
                                }else if(aData.hariIni==aData.hariDatang){
                                        if(aData.jamDatang>8){
                                            $(td).css("background-color","white")
                                        }else{
                                             $(td).css("background-color","green")
                                        }
                                }

                                if(aData.hariIni == aData.hariClockOn){
                                    if(aData.jamClockOn==8){
                                        $(td).css("background-color","blue")
                                    }
                                }

                                if(aData.hariIni == aData.hariPanggil){
                                    if(aData.jamPanggil==8){
                                        $(td).css("background-color","yellow")
                                        $(td).html("<a href='#' title='"+ aData.alasan +"' style='margin-left:15px'>*</a>")
                                    }
                                }

                                if(aData.hariIni == aData.hariClockOff){
                                    if(aData.jamClockOff==8){
                                        $(td).css("background-color","red")
                                    }
                                }
                            }
                            if(index == 2){
                                if ($(td).html() == "Y") {
                                        if(aData.menitAktual<=0){
                                            $(td).html("<img src='images/progressKendaraan/jamAktual.png' style='display: block; ' title='"+aData.jamAktual+"'/>");
                                        }else if(aData.menitAktual<=15){
                                            $(td).html(" <img src='images/progressKendaraan/jamAktual.png' style='display: block; ' title='"+aData.jamAktual+"'/>");
                                        }else if(aData.menitAktual<=30){
                                            $(td).html("<img src='images/progressKendaraan/jamAktual.png' style='display: block; margin: 0 auto;' title='"+aData.jamAktual+"'/>");
                                        }else{
                                            $(td).html(" <img src='images/progressKendaraan/jamAktual.png' style='display: block; margin-left:30px;' title='"+aData.jamAktual+"'/>");
                                        }
                                }else if ($(td).html() == "J") {
                                        if(aData.menitJanji<=0){
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; ' title='"+aData.janjiPenyerahan+"'/>");
                                        }else if(aData.menitJanji<=15){
                                            $(td).html(" <img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; ' title='"+aData.janjiPenyerahan+"'/>");
                                        }else if(aData.menitJanji<=30){
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; margin: 0 auto;' title='"+aData.janjiPenyerahan+"'/>");
                                        }else{
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; margin-left:30px;' title='"+aData.janjiPenyerahan+"'/>");
                                        }
                                }else if ($(td).html() == "E") {
                                            $(td).html("&nbsp; <img src='images/progressKendaraan/jamPenyerahan.png' title='"+aData.janjiPenyerahan+"'/> &nbsp;" +
                                             "<img src='images/progressKendaraan/jamAktual.png' title='"+aData.jamAktual+"'/>");
                                }

                                 if(aData.hariIni>aData.hariJanji){
                                    $(td).css("background-color","white")
                                }else if(aData.hariIni==aData.hariJanji){
                                        if(aData.jamJanji<9){
                                            $(td).css("background-color","white")
                                        }else{
                                             $(td).css("background-color","green")
                                        }
                                }

                                if(aData.hariIni < aData.hariDatang){
                                    $(td).css("background-color","white")
                                }else if(aData.hariIni==aData.hariDatang){
                                        if(aData.jamDatang>9){
                                            $(td).css("background-color","white")
                                        }else{
                                             $(td).css("background-color","green")
                                        }
                                }

                                if(aData.hariIni == aData.hariClockOn){
                                    if(aData.jamClockOn==9){
                                        $(td).css("background-color","blue")
                                    }
                                }

                                if(aData.hariIni == aData.hariPanggil){
                                    if(aData.jamPanggil==9){
                                        $(td).css("background-color","yellow")
                                        $(td).html("<a href='#' title='"+ aData.alasan +"' style='margin-left:15px'>*</a>")
                                    }
                                }

                                if(aData.hariIni == aData.hariClockOff){
                                    if(aData.jamClockOff==9){
                                        $(td).css("background-color","red")
                                    }
                                }
                            }
                            if(index == 3){
                                if ($(td).html() == "Y") {
                                        if(aData.menitAktual<=0){
                                            $(td).html("<img src='images/progressKendaraan/jamAktual.png' style='display: block; ' title='"+aData.jamAktual+"'/>");
                                        }else if(aData.menitAktual<=15){
                                            $(td).html(" <img src='images/progressKendaraan/jamAktual.png' style='display: block; ' title='"+aData.jamAktual+"'/>");
                                        }else if(aData.menitAktual<=30){
                                            $(td).html("<img src='images/progressKendaraan/jamAktual.png' style='display: block; margin: 0 auto;' title='"+aData.jamAktual+"'/>");
                                        }else{
                                            $(td).html(" <img src='images/progressKendaraan/jamAktual.png' style='display: block; margin-left:30px;' title='"+aData.jamAktual+"'/>");
                                        }
                                }else if ($(td).html() == "J") {
                                        if(aData.menitJanji<=0){
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; ' title='"+aData.janjiPenyerahan+"'/>");
                                        }else if(aData.menitJanji<=15){
                                            $(td).html(" <img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; ' title='"+aData.janjiPenyerahan+"'/>");
                                        }else if(aData.menitJanji<=30){
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; margin: 0 auto;' title='"+aData.janjiPenyerahan+"'/>");
                                        }else{
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; margin-left:30px;' title='"+aData.janjiPenyerahan+"'/>");
                                        }
                                }else if ($(td).html() == "E") {
                                            $(td).html("&nbsp; <img src='images/progressKendaraan/jamPenyerahan.png' title='"+aData.janjiPenyerahan+"'/> &nbsp;" +
                                             "<img src='images/progressKendaraan/jamAktual.png' title='"+aData.jamAktual+"'/>");
                                }

                                if(aData.hariIni>aData.hariJanji){
                                    $(td).css("background-color","white")
                                }else if(aData.hariIni==aData.hariJanji){
                                        if(aData.jamJanji<10){
                                            $(td).css("background-color","white")
                                        }else{
                                             $(td).css("background-color","green")
                                        }
                                }

                                if(aData.hariIni < aData.hariDatang){
                                    $(td).css("background-color","white")
                                }else if(aData.hariIni==aData.hariDatang){
                                        if(aData.jamDatang>10){
                                            $(td).css("background-color","white")
                                        }else{
                                             $(td).css("background-color","green")
                                        }
                                }

                                if(aData.hariIni == aData.hariClockOn){
                                    if(aData.jamClockOn==10){
                                        $(td).css("background-color","blue")
                                    }
                                }

                                if(aData.hariIni == aData.hariPanggil){
                                    if(aData.jamPanggil==10){
                                        $(td).css("background-color","yellow")
                                        $(td).html("<a href='#' title='"+ aData.alasan +"' style='margin-left:15px'>*</a>")
                                    }
                                }

                                if(aData.hariIni == aData.hariClockOff){
                                    if(aData.jamClockOff==10){
                                        $(td).css("background-color","red")
                                    }
                                }


                            }
                            if(index == 4){
                                if ($(td).html() == "Y") {
                                        if(aData.menitAktual<=0){
                                            $(td).html("<img src='images/progressKendaraan/jamAktual.png' style='display: block; ' title='"+aData.jamAktual+"'/>");
                                        }else if(aData.menitAktual<=15){
                                            $(td).html(" <img src='images/progressKendaraan/jamAktual.png' style='display: block; ' title='"+aData.jamAktual+"'/>");
                                        }else if(aData.menitAktual<=30){
                                            $(td).html("<img src='images/progressKendaraan/jamAktual.png' style='display: block; margin: 0 auto;' title='"+aData.jamAktual+"'/>");
                                        }else{
                                            $(td).html(" <img src='images/progressKendaraan/jamAktual.png' style='display: block; margin-left:30px;' title='"+aData.jamAktual+"'/>");
                                        }
                                }else if ($(td).html() == "J") {
                                        if(aData.menitJanji<=0){
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; ' title='"+aData.janjiPenyerahan+"'/>");
                                        }else if(aData.menitJanji<=15){
                                            $(td).html(" <img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; ' title='"+aData.janjiPenyerahan+"'/>");
                                        }else if(aData.menitJanji<=30){
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; margin: 0 auto;' title='"+aData.janjiPenyerahan+"'/>");
                                        }else{
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; margin-left:30px;' title='"+aData.janjiPenyerahan+"'/>");
                                        }
                                }else if ($(td).html() == "E") {
                                            $(td).html("&nbsp; <img src='images/progressKendaraan/jamPenyerahan.png' title='"+aData.janjiPenyerahan+"'/> &nbsp;" +
                                             "<img src='images/progressKendaraan/jamAktual.png' title='"+aData.jamAktual+"'/>");
                                }

                                if(aData.hariIni>aData.hariJanji){
                                    $(td).css("background-color","white")
                                }else if(aData.hariIni==aData.hariJanji){
                                        if(aData.jamJanji<11){
                                            $(td).css("background-color","white")
                                        }else{
                                             $(td).css("background-color","green")
                                        }
                                }

                                if(aData.hariIni < aData.hariDatang){
                                    $(td).css("background-color","white")
                                }else if(aData.hariIni==aData.hariDatang){
                                        if(aData.jamDatang>11){
                                            $(td).css("background-color","white")
                                        }else{
                                             $(td).css("background-color","green")
                                        }
                                }

                                if(aData.hariIni == aData.hariClockOn){
                                    if(aData.jamClockOn==11){
                                        $(td).css("background-color","blue")
                                    }

                                }

                                if(aData.hariIni == aData.hariPanggil){
                                    if(aData.jamPanggil==11){
                                        $(td).css("background-color","yellow")
                                        $(td).html("<a href='#' title='"+ aData.alasan +"' style='margin-left:15px'>*</a>")
                                    }
                                }

                                if(aData.hariIni == aData.hariClockOff){
                                    if(aData.jamClockOff==11){
                                        $(td).css("background-color","red")
                                    }
                                }
                            }
                            if(index == 5){
                                if ($(td).html() == "Y") {
                                        if(aData.menitAktual<=0){
                                            $(td).html("<img src='images/progressKendaraan/jamAktual.png' style='display: block; ' title='"+aData.jamAktual+"'/>");
                                        }else if(aData.menitAktual<=15){
                                            $(td).html(" <img src='images/progressKendaraan/jamAktual.png' style='display: block; ' title='"+aData.jamAktual+"'/>");
                                        }else if(aData.menitAktual<=30){
                                            $(td).html("<img src='images/progressKendaraan/jamAktual.png' style='display: block; margin: 0 auto;' title='"+aData.jamAktual+"'/>");
                                        }else{
                                            $(td).html(" <img src='images/progressKendaraan/jamAktual.png' style='display: block; margin-left:30px;' title='"+aData.jamAktual+"'/>");
                                        }
                                }else if ($(td).html() == "J") {
                                        if(aData.menitJanji<=0){
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; ' title='"+aData.janjiPenyerahan+"'/>");
                                        }else if(aData.menitJanji<=15){
                                            $(td).html(" <img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; ' title='"+aData.janjiPenyerahan+"'/>");
                                        }else if(aData.menitJanji<=30){
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; margin: 0 auto;' title='"+aData.janjiPenyerahan+"'/>");
                                        }else{
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; margin-left:30px;' title='"+aData.janjiPenyerahan+"'/>");
                                        }
                                }else if ($(td).html() == "E") {
                                            $(td).html("&nbsp; <img src='images/progressKendaraan/jamPenyerahan.png' title='"+aData.janjiPenyerahan+"'/> &nbsp;" +
                                             "<img src='images/progressKendaraan/jamAktual.png' title='"+aData.jamAktual+"'/>");
                                }

                                if(aData.hariIni>aData.hariJanji){
                                    $(td).css("background-color","white")
                                }else if(aData.hariIni==aData.hariJanji){
                                        if(aData.jamJanji<12){
                                            $(td).css("background-color","white")
                                        }else{
                                             $(td).css("background-color","green")
                                        }
                                }

                                if(aData.hariIni < aData.hariDatang){
                                    $(td).css("background-color","white")
                                }else if(aData.hariIni==aData.hariDatang){
                                        if(aData.jamDatang>12){
                                            $(td).css("background-color","white")
                                        }else{
                                             $(td).css("background-color","green")
                                        }
                                }

                                if(aData.hariIni == aData.hariClockOn){
                                    if(aData.jamClockOn==12){
                                        $(td).css("background-color","blue")
                                    }

                                }

                                if(aData.hariIni == aData.hariPanggil){
                                    if(aData.jamPanggil==12){
                                        $(td).css("background-color","yellow")
                                        $(td).html("<a href='#' title='"+ aData.alasan +"' style='margin-left:15px'>*</a>")
                                    }
                                }

                                if(aData.hariIni == aData.hariClockOff){
                                    if(aData.jamClockOff==12){
                                        $(td).css("background-color","red")
                                    }
                                }
                            }
                            if(index == 6){
                                if ($(td).html() == "Y") {
                                        if(aData.menitAktual<=0){
                                            $(td).html("<img src='images/progressKendaraan/jamAktual.png' style='display: block; ' title='"+aData.jamAktual+"'/>");
                                        }else if(aData.menitAktual<=15){
                                            $(td).html(" <img src='images/progressKendaraan/jamAktual.png' style='display: block; ' title='"+aData.jamAktual+"'/>");
                                        }else if(aData.menitAktual<=30){
                                            $(td).html("<img src='images/progressKendaraan/jamAktual.png' style='display: block; margin: 0 auto;' title='"+aData.jamAktual+"'/>");
                                        }else{
                                            $(td).html(" <img src='images/progressKendaraan/jamAktual.png' style='display: block; margin-left:30px;' title='"+aData.jamAktual+"'/>");
                                        }
                                }else if ($(td).html() == "J") {
                                        if(aData.menitJanji<=0){
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; ' title='"+aData.janjiPenyerahan+"'/>");
                                        }else if(aData.menitJanji<=15){
                                            $(td).html(" <img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; ' title='"+aData.janjiPenyerahan+"'/>");
                                        }else if(aData.menitJanji<=30){
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; margin: 0 auto;' title='"+aData.janjiPenyerahan+"'/>");
                                        }else{
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; margin-left:30px;' title='"+aData.janjiPenyerahan+"'/>");
                                        }
                                }else if ($(td).html() == "E") {
                                            $(td).html("&nbsp; <img src='images/progressKendaraan/jamPenyerahan.png' title='"+aData.janjiPenyerahan+"'/> &nbsp;" +
                                             "<img src='images/progressKendaraan/jamAktual.png' title='"+aData.jamAktual+"'/>");
                                }

                                if(aData.hariIni>aData.hariJanji){
                                    $(td).css("background-color","white")
                                }else if(aData.hariIni==aData.hariJanji){
                                        if(aData.jamJanji<13){
                                            $(td).css("background-color","white")
                                        }else{
                                             $(td).css("background-color","green")
                                        }
                                }

                                if(aData.hariIni < aData.hariDatang){
                                    $(td).css("background-color","white")
                                }else if(aData.hariIni==aData.hariDatang){
                                        if(aData.jamDatang>13){
                                            $(td).css("background-color","white")
                                        }else{
                                             $(td).css("background-color","green")
                                        }
                                }

                                if(aData.hariIni == aData.hariClockOn){
                                    if(aData.jamClockOn==13){
                                        $(td).css("background-color","blue")
                                    }

                                }

                                if(aData.hariIni == aData.hariPanggil){
                                    if(aData.jamPanggil==13){
                                        $(td).css("background-color","yellow")
                                        $(td).html("<a href='#' title='"+ aData.alasan +"' style='margin-left:15px'>*</a>")
                                    }
                                }

                                if(aData.hariIni == aData.hariClockOff){
                                    if(aData.jamClockOff==13){
                                        $(td).css("background-color","red")
                                    }
                                }
                            }
                            if(index == 7){
                                if ($(td).html() == "Y") {
                                        if(aData.menitAktual<=0){
                                            $(td).html("<img src='images/progressKendaraan/jamAktual.png' style='display: block; ' title='"+aData.jamAktual+"'/>");
                                        }else if(aData.menitAktual<=15){
                                            $(td).html(" <img src='images/progressKendaraan/jamAktual.png' style='display: block; ' title='"+aData.jamAktual+"'/>");
                                        }else if(aData.menitAktual<=30){
                                            $(td).html("<img src='images/progressKendaraan/jamAktual.png' style='display: block; margin: 0 auto;' title='"+aData.jamAktual+"'/>");
                                        }else{
                                            $(td).html(" <img src='images/progressKendaraan/jamAktual.png' style='display: block; margin-left:30px;' title='"+aData.jamAktual+"'/>");
                                        }
                                }else if ($(td).html() == "J") {
                                        if(aData.menitJanji<=0){
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; ' title='"+aData.janjiPenyerahan+"'/>");
                                        }else if(aData.menitJanji<=15){
                                            $(td).html(" <img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; ' title='"+aData.janjiPenyerahan+"'/>");
                                        }else if(aData.menitJanji<=30){
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; margin: 0 auto;' title='"+aData.janjiPenyerahan+"'/>");
                                        }else{
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; margin-left:30px;' title='"+aData.janjiPenyerahan+"'/>");
                                        }
                                }else if ($(td).html() == "E") {
                                            $(td).html("&nbsp; <img src='images/progressKendaraan/jamPenyerahan.png' title='"+aData.janjiPenyerahan+"'/> &nbsp;" +
                                             "<img src='images/progressKendaraan/jamAktual.png' title='"+aData.jamAktual+"'/>");
                                }

                                if(aData.hariIni>aData.hariJanji){
                                    $(td).css("background-color","white")
                                }else if(aData.hariIni==aData.hariJanji){
                                        if(aData.jamJanji<14){
                                            $(td).css("background-color","white")
                                        }else{
                                             $(td).css("background-color","green")
                                        }
                                }

                                if(aData.hariIni < aData.hariDatang){
                                    $(td).css("background-color","white")
                                }else if(aData.hariIni==aData.hariDatang){
                                        if(aData.jamDatang>14){
                                            $(td).css("background-color","white")
                                        }else{
                                             $(td).css("background-color","green")
                                        }
                                }

                                if(aData.hariIni == aData.hariClockOn){
                                    if(aData.jamClockOn==14){
                                        $(td).css("background-color","blue")
                                    }

                                }

                                if(aData.hariIni == aData.hariPanggil){
                                    if(aData.jamPanggil==14){
                                        $(td).css("background-color","yellow")
                                        $(td).html("<a href='#' title='"+ aData.alasan +"' style='margin-left:15px'>*</a>")
                                    }
                                }

                                if(aData.hariIni == aData.hariClockOff){
                                    if(aData.jamClockOff==14){
                                        $(td).css("background-color","red")
                                    }

                                }
                            }
                            if(index == 8){
                               if ($(td).html() == "Y") {
                                        if(aData.menitAktual<=0){
                                            $(td).html("<img src='images/progressKendaraan/jamAktual.png' style='display: block; ' title='"+aData.jamAktual+"'/>");
                                        }else if(aData.menitAktual<=15){
                                            $(td).html(" <img src='images/progressKendaraan/jamAktual.png' style='display: block; ' title='"+aData.jamAktual+"'/>");
                                        }else if(aData.menitAktual<=30){
                                            $(td).html("<img src='images/progressKendaraan/jamAktual.png' style='display: block; margin: 0 auto;' title='"+aData.jamAktual+"'/>");
                                        }else{
                                            $(td).html(" <img src='images/progressKendaraan/jamAktual.png' style='display: block; margin-left:30px;' title='"+aData.jamAktual+"'/>");
                                        }
                                }else if ($(td).html() == "J") {
                                        if(aData.menitJanji<=0){
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; ' title='"+aData.janjiPenyerahan+"'/>");
                                        }else if(aData.menitJanji<=15){
                                            $(td).html(" <img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; ' title='"+aData.janjiPenyerahan+"'/>");
                                        }else if(aData.menitJanji<=30){
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; margin: 0 auto;' title='"+aData.janjiPenyerahan+"'/>");
                                        }else{
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; margin-left:30px;' title='"+aData.janjiPenyerahan+"'/>");
                                        }
                                }else if ($(td).html() == "E") {
                                            $(td).html("&nbsp; <img src='images/progressKendaraan/jamPenyerahan.png' title='"+aData.janjiPenyerahan+"'/> &nbsp;" +
                                             "<img src='images/progressKendaraan/jamAktual.png' title='"+aData.jamAktual+"'/>");
                                }

                                if(aData.hariIni>aData.hariJanji){
                                    $(td).css("background-color","white")
                                }else if(aData.hariIni==aData.hariJanji){
                                        if(aData.jamJanji<15){
                                            $(td).css("background-color","white")
                                        }else{
                                             $(td).css("background-color","green")
                                        }
                                }

                                if(aData.hariIni < aData.hariDatang){
                                    $(td).css("background-color","white")
                                }else if(aData.hariIni==aData.hariDatang){
                                        if(aData.jamDatang>15){
                                            $(td).css("background-color","white")
                                        }else{
                                             $(td).css("background-color","green")
                                        }
                                }

                                if(aData.hariIni == aData.hariClockOn){
                                    if(aData.jamClockOn==15){
                                        $(td).css("background-color","blue")
                                    }

                                }

                                if(aData.hariIni == aData.hariPanggil){
                                    if(aData.jamPanggil==15){
                                        $(td).css("background-color","yellow")
                                        $(td).html("<a href='#' title='"+ aData.alasan +"' style='margin-left:15px'>*</a>")
                                    }

                                }

                                if(aData.hariIni == aData.hariClockOff){
                                    if(aData.jamClockOff==15){
                                        $(td).css("background-color","red")
                                    }
                                }
                            }
                            if(index == 9){
                                if ($(td).html() == "Y") {
                                        if(aData.menitAktual<=0){
                                            $(td).html("<img src='images/progressKendaraan/jamAktual.png' style='display: block; ' title='"+aData.jamAktual+"'/>");
                                        }else if(aData.menitAktual<=15){
                                            $(td).html(" <img src='images/progressKendaraan/jamAktual.png' style='display: block; ' title='"+aData.jamAktual+"'/>");
                                        }else if(aData.menitAktual<=30){
                                            $(td).html("<img src='images/progressKendaraan/jamAktual.png' style='display: block; margin: 0 auto;' title='"+aData.jamAktual+"'/>");
                                        }else{
                                            $(td).html(" <img src='images/progressKendaraan/jamAktual.png' style='display: block; margin-left:30px;' title='"+aData.jamAktual+"'/>");
                                        }
                                }else if ($(td).html() == "J") {
                                        if(aData.menitJanji<=0){
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; ' title='"+aData.janjiPenyerahan+"'/>");
                                        }else if(aData.menitJanji<=15){
                                            $(td).html(" <img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; ' title='"+aData.janjiPenyerahan+"'/>");
                                        }else if(aData.menitJanji<=30){
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; margin: 0 auto;' title='"+aData.janjiPenyerahan+"'/>");
                                        }else{
                                            $(td).html("<img src='images/progressKendaraan/jamPenyerahan.png' style='display: block; margin-left:30px;' title='"+aData.janjiPenyerahan+"'/>");
                                        }
                                }else if ($(td).html() == "E") {
                                            $(td).html("&nbsp; <img src='images/progressKendaraan/jamPenyerahan.png' title='"+aData.janjiPenyerahan+"'/> &nbsp;" +
                                             "<img src='images/progressKendaraan/jamAktual.png' title='"+aData.jamAktual+"'/>");
                                }

                                if(aData.hariIni>aData.hariJanji){
                                    $(td).css("background-color","white")
                                }else if(aData.hariIni==aData.hariJanji){
                                        if(aData.jamJanji<16){
                                            $(td).css("background-color","white")
                                        }else{
                                             $(td).css("background-color","green")
                                        }
                                }

                                if(aData.hariIni < aData.hariDatang){
                                    $(td).css("background-color","white")
                                }else if(aData.hariIni==aData.hariDatang){
                                        if(aData.jamDatang>16){
                                            $(td).css("background-color","white")
                                        }else{
                                             $(td).css("background-color","green")
                                        }
                                }

                                if(aData.hariIni == aData.hariClockOn){
                                    if(aData.jamClockOn==16){
                                        $(td).css("background-color","blue")
                                    }

                                }

                                if(aData.hariIni == aData.hariPanggil){
                                    if(aData.jamPanggil==16){
                                        $(td).css("background-color","yellow")
                                        $(td).html("<a href='#' title='"+ aData.alasan +"' style='margin-left:15px'>*</a>")
                                    }
                                }

                                if(aData.hariIni == aData.hariClockOff){
                                    if(aData.jamClockOff==16){
                                        $(td).css("background-color","red")
                                    }

                                }
                            }
                        }

               });

			    return nRow;
		   },
		"bSort": false,
	 	"bProcessing": true,
		"bServerSide": true,
	 	"sServerMethod": "POST",
	 	"iDisplayLength" : maxLineDisplay,
		//maxLineDisplay is set in menu/maxLineDisplay.gsp
 		"sAjaxSource": "${g.createLink(action: "datatablesList2")}",
			"aoColumns": [

{
	"sName": "jam7",
	"mDataProp": "jam7",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "jam8",
	"mDataProp": "jam8",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "jam9",
	"mDataProp": "jam9",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "jam10",
	"mDataProp": "jam10",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "jam11",
	"mDataProp": "jam11",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "jam12",
	"mDataProp": "jam12",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "jam13",
	"mDataProp": "jam13",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "jam14",
	"mDataProp": "jam14",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "jam5",
	"mDataProp": "jam15",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "jam16",
	"mDataProp": "jam16",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
					    var kriteria = $('#kriteria').val();
        	            var kKunci  = $('#kata_kunci').val();
                        aoData.push(
                                            {"name": 'kriteria', "value": kriteria},
                                            {"name": 'kata_kunci', "value": kKunci}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

});
</g:javascript>