<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript>
var viewOrderCatSubSubSubTable_${idTable};
$(function(){
viewOrderCatSubSubSubTable_${idTable} = $('#viewOrderCat_datatables_sub_sub_sub_${idTable}').dataTable({
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "t",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": false,
		"bProcessing": false,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubSubSubList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"13.8px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"13.5px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"16.5px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"12px",
               "sDefaultContent": ''
},
{
	"sName": "kodeWarnaVendor",
	"mDataProp": "kodeWarnaVendor",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"> <input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;' + data;
        },
        "bSortable": false,
        "sWidth":"122.5px",
        "bVisible": true
    },
    {
        "sName": "namaWarnaVendor",
        "mDataProp": "namaWarnaVendor",
        "aTargets": [1],
        "bSortable": false,
        "sWidth":"345px",
        "bVisible": true
    },
    {
        "sName": "qty",
	    "mDataProp": "qty",
        "aTargets": [2],
        "bSortable": false,
        "sWidth":"171px",
        "bVisible": true
    },
    {
        "sName": "satuan",
        "mDataProp": "satuan",
        "aTargets": [3],
        "bSortable": false,
        "sWidth":"170px",
        "bVisible": true
    } ,
    {
        "mDataProp": null,
        "bSortable": false,
        "sWidth":"175px",
        "sDefaultContent": '',
        "bVisible": true
    }

    ],
            "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                            aoData.push(
                                       {"name": 't401NoWO', "value": "${t401NoWO}"},
                                       {"name": 'noPos', "value": "${noPos}"},
                                       {"name": 'vendorCat', "value": "${kodeWarna}"}
    						);

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
    </g:javascript>
</head>
<body>
<div class="innerinnerDetails">
    <table id="viewOrderCat_datatables_sub_sub_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
           class="display table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Kode Warna Vendor</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Nama Warna Vendor</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Qty</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Satuan</div>
            </th>
            <th style="border-bottom: none; padding: 5px;" />


        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
</body>
</html>
