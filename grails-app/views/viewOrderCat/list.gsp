<%@ page import="com.kombos.administrasi.VendorCat" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'viewOrderCat.label', default: 'View Order Cat')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>
			$(function(){ 
			$('.box-action').click(function(){
            switch($(this).attr('target')){
                case '_DELETE_' :
                    bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
                                function(result){
                                    if(result){
                                        massDelete();
                                    }
                                });
                            break;
                   }
                   return false;
            });
				 //darisini
            $("#viewOrderCatAddModal").on("show", function() {
                $("#viewOrderCatAddModal .btn").on("click", function(e) {
                    $("#viewOrderCatAddModal").modal('hide');
                });
            });
            $("#viewOrderCatAddModal").on("hide", function() {
                $("#viewOrderCatAddModal a.btn").off("click");
            });

                loadForm = function(data, textStatus){
                    $('#viewOrderCat-form').empty();
                    $('#viewOrderCat-form').append(data);
                }
                shrinkTableLayout = function(){
                    $("#viewOrderCat-table").hide();
                    $("#viewOrderCat-form").css("display","block");
                }

                expandTableLayout = function(){
                    $("#viewOrderCat-table").show();
                    $("#viewOrderCat-form").css("display","none");
                }
    massDelete = function() {
   		var recordsToDelete = [];
		$("#viewOrderCat-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});

		var json = JSON.stringify(recordsToDelete);

		$.ajax({
    		url:'${request.contextPath}/viewOrderCat/massdelete',
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		succes : function(data){
    		  toastr.success('<div>Delete Data Succes<div>')
    		},
    		complete: function(xhr, status) {
        		reloadViewOrderCatTable();
        		toastr.success('<div>Delete Data Succes<div>')
    		}
		});

   	}

    editSupply = function(id) {
        $('#spinner').fadeIn(1);
        window.location.replace('#/editSupply')
        $.ajax({url: '${request.contextPath}/supplyInput?noPos='+id,
            type:"GET",dataType: "html",
            complete : function (req, err) {
                $('#main-content').html(req.responseText);
                $('#spinner').fadeOut();
                loading = false;
            }
        });
    };
    editPos = function(id) {
        $('#spinner').fadeIn(1);
        window.location.replace('#/editPos')
        $.ajax({url: '${request.contextPath}/posInput?noPos='+id,
            type:"GET",dataType: "html",
            complete : function (req, err) {
                $('#main-content').html(req.responseText);
                $('#spinner').fadeOut();
                loading = false;
            }
        });
    };
    klik = function(){
        var check = [];
        var id = ""
        var counts = 0;
        $("#viewOrderCat-table tbody .row-select").each(function() {
            if(this.checked){
                counts++;
                id = $(this).next("#subNopos").val()?$(this).next("#subNopos").val():"-"
                if(id!="-"){
                    check.push(id)
                }
            }
        });
        if(check.length<1){
            alert('Pilih nomor POS yang akan ditambahkan');
        } else if(counts>1){
            alert('Pilih Checkbox salah satu');
        }else{
            $.ajax({
                url: '${request.contextPath}/viewOrderCat/cekNoSupply',
                data:{id:id},
                type: "POST",
                success : function(data){
                        if(data==""){
                            $.ajax({
                            url: '${request.contextPath}/supplyInput',
                            data:{id:id},
                            type: "GET",dataType:"html",
                            complete : function (req, err) {
                                $('#main-content').html(req.responseText);
                                $('#spinner').fadeOut();
                            }
                            });
                        }else{
                            alert("No Supply Sudah Ada");
                        }
                }
            });
         }
    }

    klik2 = function(){
        $('#spinner').fadeIn(1);
        $.ajax({
            url: '${request.contextPath}/posInput',
            type: "GET",dataType:"html",
            complete : function (req, err) {
                $('#main-content').html(req.responseText);
                $('#spinner').fadeOut();
            }
        });
    }
});

    var checkin = $('#search_Tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
        var newDate = new Date(ev.date)
        newDate.setDate(newDate.getDate() + 1);
        checkout.setValue(newDate);
        checkin.hide();
        $('#search_Tanggalakhir')[0].focus();
    }).data('datepicker');
    
    var checkout = $('#search_Tanggalakhir').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

        return true;
    }
    printPos = function(){
           checkPos =[];
            $("#viewOrderCat-table tbody .row-select").each(function() {
                    var nRow = $(this).next("input:hidden").val();
					checkPos.push(nRow);
            });
           var idReception =  JSON.stringify(checkPos);
           window.location = "${request.contextPath}/viewOrderCat/printPos?idReception="+idReception;
    }
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="jobInstruction.view.label" default="View Order Cat" /></span>
    <ul class="nav pull-right">
        <li><a class="pull-right" href="#/supplyInput" onclick="klik()" style="display: block;" >&nbsp;&nbsp;New Vendor Supply &nbsp;&nbsp;</a></li>
        <li class="separator"></li>
        <li><a class="pull-right" href="#/posInput" onclick="klik2()" style="display: block;" >&nbsp;&nbsp;New POS&nbsp;&nbsp;</a></li>
        <li><a class="pull-right box-action" href="#"
               style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
                    class="icon-remove"></i>&nbsp;&nbsp;
        </a></li>
        <li class="separator"></li>
    </ul>
</div>
<div class="box">
    <div class="span12" id="viewOrderCat-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <table>
            <tr>
                <td style="padding: 5px">
                    <label class="control-label" for="t951Tanggal">
                        <g:message code="sss.tanggalWO.label" default="Tanggal Delivery"/>
                    </label>
                </td>
                <td style="padding: 5px">
                    <ba:datePicker name="search_Tanggal" id="search_Tanggal" precision="day" value="" disable="true" format="dd-MM-yyyy"/>&nbsp;s.d.&nbsp;
                    <ba:datePicker name="search_Tanggalakhir" id="search_Tanggalakhir" precision="day" value="" disable="true" format="dd-MM-yyyy"/>
                </td>
                <td style="padding-left: 20px">
                    <label class="control-label" for="t951Tanggal">
                        <g:message code="bPJobInstruction.staJobTambah.label" default="Vendor Cat"/>
                    </label>
                </td>
                <td style="padding: 5px">
                    <g:select name="vendorCat" id="vendorCat" from="${VendorCat.createCriteria().list {eq("staDel","0")}}" optionKey="id" optionValue="m191Nama" noSelection="${['':'Pilih']}" />
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    <label class="control-label" for="t951Tanggal">
                       <g:message code="bPJobInstruction.staAmbilWO.label" default="Kriteria Pencarian"/>
                    </label>
                </td>
                <td style="padding: 5px">
                    <select name="kriteria" id="kriteria" style="width: 90%">
                        <option value="">Pilih</option>
                        <option value="s_noWo" >Nomor Wo</option>
                        <option value="s_noPol" >No Polisi</option>
                        <option value="s_model" >Model</option>
                        <option value="s_SA" >Nama SA</option>
                    </select>
                </td>
                <td style="padding-left: 20px" rowspan="2">
                    <label class="control-label" for="t951Tanggal">
                        <g:message code="bPJobInstruction.staCarryOver.label" default="Supply Dari Vendor"/>
                    </label>
                </td>
                <td style="padding: 5px; padding-left: 12px;" rowspan="2">
                    <input type="radio" id="belumSupply" name="supplyDarivendor" value="0"> Belum Supply Dari Vendor <br><br>
                    <input type="radio" id="sudahSupply" name="supplyDarivendor" value="1"> Sudah Supply Dari Vendor
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    <label class="control-label" for="t951Tanggal">
                        <g:message code="bPJobInstruction.staProblemFinding.label" default="Kata Kunci"/>
                    </label>
                </td>
                <td style="padding: 5px">
                    <g:textField name="kunci" id="kunci" style="width: 86%" />
                </td>
            </tr>
            <tr>
                <td colspan="3" >
                    <div class="controls" style="right: 0">
                        <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-primary" name="view" id="view" >Search</button>
                        <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-cancel clear" name="clear" id="clear" >Clear</button>
                    </div>
                </td>
            </tr>
        </table>
        <br/><br/>
        <g:render template="dataTables" />
        <button id='printMosData' onclick="printPos();" type="button" class="btn btn-primary">Print POS</button>
    <div class="span7" id="viewOrderCat-form" style="display: none; width: 1200px;"></div>
</div>
<div id="viewOrderCatAddModal" class="modal fade"style="width: 800px;">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content" style="width: 800px;">
            <!-- dialog body -->
            <div class="modal-body" style="max-height: 800px; width: 800px;">
                <div id="viewOrderCatAddContent"/>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="iu-content"></div>
            </div>

        </div>
    </div>
</div>
</body>
</html>
