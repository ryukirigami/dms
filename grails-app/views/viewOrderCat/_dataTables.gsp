
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>


<table id="viewOrderCat_datatables_${idTable}" cellpadding="0" cellspacing="0" border="0"
       class="display table table-striped table-bordered table-hover table-selectable" style="table-layout: fixed; width: 100% ">
    <thead>
    <tr>
        <th style="vertical-align: middle;">
            <div style="height: 10px"> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <g:message code="wo.t401NoWo.label" default="Nomor WO"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="wo.noPolisi.label" default="Tanggal WO"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="wo.tanggalWO.label" default="Nomor Polisi"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="wo.start.label" default="Model"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="wo.stop.label" default="Nama SA"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="viewOrderCat.finalInspection.label" default="Painting Time"/>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var viewOrderCatTable;
var reloadViewOrderCatTable;
$(function(){

    var anOpen = [];
	$('#viewOrderCat_datatables_${idTable} td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );

            if ( i === -1 ) {
                $('i', this).attr( 'class', "icon-minus" );
                var oData = viewOrderCatTable.fnGetData(nTr);
                $('#spinner').fadeIn(1);
                $.ajax({type:'POST',
                    data : oData,
                    url:'${request.contextPath}/viewOrderCat/sublist',
                    success:function(data,textStatus){
                        var nDetailsRow = viewOrderCatTable.fnOpen(nTr,data,'details');
                        $('div.innerDetails', nDetailsRow).slideDown();
                        anOpen.push( nTr );
                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){},
                    complete:function(XMLHttpRequest,textStatus){
                        $('#spinner').fadeOut();
                    }
                });

            } else {
                $('i', this).attr( 'class', 'icon-plus' );
                $('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
                    viewOrderCatTable.fnClose( nTr );
                    anOpen.splice( i, 1 );
                });
            }

	});

	$('#view').click(function(e){
        viewOrderCatTable.fnDraw();
	});
	$('#clear').click(function(e){
	    $('#vendorCat').val('');
	    $('#search_Tanggal').val('');
	    $('#search_Tanggalakhir').val('');
	    $('#kriteria').val('');
	    $('#kunci').val('');
	    $('#belumSupply').prop('checked',false);
	    $('#sudahSupply').prop('checked',false);
        viewOrderCatTable.fnDraw();
	});

    $('#viewOrderCat_datatables_${idTable} a.edit').live('click', function (e) {
        e.preventDefault();

        var nRow = $(this).parents('tr')[0];

        if ( nEditing !== null && nEditing != nRow ) {
            restoreRow( viewOrderCatTable, nEditing );
            editRow( viewOrderCatTable, nRow );
            nEditing = nRow;
        }
        else if ( nEditing == nRow && this.innerHTML == "Save" ) {
            saveRow( viewOrderCatTable, nEditing );
            nEditing = null;
        }
        else {
            editRow( viewOrderCatTable, nRow );
            nEditing = nRow;
        }
    } );



	reloadViewOrderCatTable = function() {

		viewOrderCatTable.fnDraw();
	}


    viewOrderCatTable = $('#viewOrderCat_datatables_${idTable}').dataTable({
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"bDestroy": true,
		"aoColumns": [
{
   "mDataProp": null,
   "sClass":"control center",
   "bSortable": false,
   "sWidth":"7px",
   "sDefaultContent": '<i class="icon-plus"></i>'

}
,
{
	"sName": "t401NoWO",
	"mDataProp": "t401NoWO",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
	    return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"><input type="hidden" name="cnomorWo" id="cnomorWo" value="'+data+'">&nbsp;&nbsp;'+data;
	},
	"bSortable": false,
	"sWidth":"170px",
	"bVisible": true
}
,
{
	"sName": "nopol",
	"mDataProp": "nopol",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"130px",
	"bVisible": true
}
,
{
	"sName": "t401TanggalWO",
	"mDataProp": "t401TanggalWO",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"130px",
	"bVisible": true
}
,
{
	"sName": "model",
	"mDataProp": "model",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"135px",
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "sa",
	"mDataProp": "SA",
	"aTargets": [4],
	"sWidth":"135px",
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "paintingTime",
	"mDataProp": "paintingTime",
	"aTargets": [5],
	"sWidth":"135px",
	"sDefaultContent": '',
	"bVisible": true
}

],
    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
    
                        var Tanggal = $('#search_Tanggal').val();
						var TanggalDay = $('#search_Tanggal_day').val();
						var TanggalMonth = $('#search_Tanggal_month').val();
						var TanggalYear = $('#search_Tanggal_year').val();

	                    var Tanggalakhir = $('#search_Tanggalakhir').val();
                        var TanggalDayakhir = $('#search_Tanggalakhir_day').val();
                        var TanggalMonthakhir = $('#search_Tanggalakhir_month').val();
                        var TanggalYearakhir = $('#search_Tanggalakhir_year').val();


                        if(Tanggal){
                            aoData.push(
                                    {"name": 'sCriteria_Tanggal', "value": "date.struct"},
                                    {"name": 'sCriteria_Tanggal_dp', "value": Tanggal},
                                    {"name": 'sCriteria_Tanggal_day', "value": TanggalDay},
                                    {"name": 'sCriteria_Tanggal_month', "value": TanggalMonth},
                                    {"name": 'sCriteria_Tanggal_year', "value": TanggalYear}
                            );
                        }

                        if(Tanggalakhir){
                            aoData.push(
                                    {"name": 'sCriteria_Tanggalakhir', "value": "date.struct"},
                                    {"name": 'sCriteria_Tanggalakhir_dp', "value": Tanggalakhir},
                                    {"name": 'sCriteria_Tanggalakhir_day', "value": TanggalDayakhir},
                                    {"name": 'sCriteria_Tanggalakhir_month', "value":TanggalMonthakhir},
                                    {"name": 'sCriteria_Tanggalakhir_year', "value": TanggalYearakhir}
                            );
                        }


						var vendorCat = $('#vendorCat').val();
						if(vendorCat){
							aoData.push(
									{"name": 'sCriteria_vendorCat', "value": vendorCat}
							);
						}

						var kriteria = $('#kriteria').val();
						if(kriteria){
							aoData.push(
									{"name": 'sCriteria_kriteria', "value": kriteria}
							);
						}

						var kunci = $('#kunci').val();
						if(kunci){
							aoData.push(
									{"name": 'sCriteria_kunci', "value": kunci}
							);
						}

						var belumSupply = $('#belumSupply').is(':checked');
						var sudahSupply = $('#sudahSupply').is(':checked');
						if(belumSupply){
							aoData.push(
									{"name": 'sCriteria_belumSupply', "value": belumSupply}
							);
						}
						if(sudahSupply){
							aoData.push(
									{"name": 'sCriteria_sudahSupply', "value": sudahSupply}
							);
						}


    $.ajax({ "dataType": 'json',
        "type": "POST",
        "url": sSource,
        "data": aoData ,
        "success": function (json) {
            fnCallback(json);
           },
        "complete": function () {
           }
    });
}
});
});


</g:javascript>
