

<%@ page import="com.kombos.maintable.NoEfaktur" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'noEfaktur.label', default: 'Nomor Efaktur')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteNoEfaktur;

$(function(){ 
	deleteNoEfaktur=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/noEfaktur/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadNoEfakturTable();
   				expandTableLayout('noEfaktur');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-noEfaktur" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="noEfaktur"
			class="table table-bordered table-hover">
			<tbody>

            <g:if test="${noEfakturInstance?.suffix}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="suffix-label" class="property-label"><g:message
                                code="noEfaktur.suffix.label" default="Nomor Awal" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="suffix-label">
                        %{--<ba:editableValue
                                bean="${noEfakturInstance}" field="suffix"
                                url="${request.contextPath}/NoEfaktur/updatefield" type="text"
                                title="Enter suffix" onsuccess="reloadNoEfakturTable();" />--}%

                        <g:fieldValue bean="${noEfakturInstance}" field="suffix"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${noEfakturInstance?.prefix}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="prefix-label" class="property-label"><g:message
                                code="noEfaktur.prefix.label" default="Nomor Akhir" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="prefix-label">
                        %{--<ba:editableValue
                                bean="${noEfakturInstance}" field="prefix"
                                url="${request.contextPath}/NoEfaktur/updatefield" type="text"
                                title="Enter prefix" onsuccess="reloadNoEfakturTable();" />--}%

                        <g:fieldValue bean="${noEfakturInstance}" field="prefix"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${noEfakturInstance?.noAwal}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="noAwal-label" class="property-label"><g:message
                                code="noEfaktur.noAwal.label" default="Nomor Urut Awal" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="noAwal-label">
                        %{--<ba:editableValue
                                bean="${noEfakturInstance}" field="noAwal"
                                url="${request.contextPath}/NoEfaktur/updatefield" type="text"
                                title="Enter noAwal" onsuccess="reloadNoEfakturTable();" />--}%

                        <g:fieldValue bean="${noEfakturInstance}" field="noAwal"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${noEfakturInstance?.noAkhir}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="noAkhir-label" class="property-label"><g:message
                                code="noEfaktur.noAkhir.label" default="Nomor Urut Akhir" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="noAkhir-label">
                        %{--<ba:editableValue
                                bean="${noEfakturInstance}" field="noAkhir"
                                url="${request.contextPath}/NoEfaktur/updatefield" type="text"
                                title="Enter noAkhir" onsuccess="reloadNoEfakturTable();" />--}%

                        <g:fieldValue bean="${noEfakturInstance}" field="noAkhir"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${noEfakturInstance?.tglPermohonanPajak}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="tglPermohonanPajak-label" class="property-label"><g:message
                                code="noEfaktur.tglAkhirPajak.label" default="Tgl Permohonan " />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="tglPermohonanPajak-label">
                        %{--<ba:editableValue
                                bean="${noEfakturInstance}" field="tglAkhirPajak"
                                url="${request.contextPath}/NoEfaktur/updatefield" type="text"
                                title="Enter tglAkhirPajak" onsuccess="reloadNoEfakturTable();" />--}%

                        <g:formatDate date="${noEfakturInstance?.tglPermohonanPajak}" />

                    </span></td>

                </tr>
            </g:if>
            <g:if test="${noEfakturInstance?.tglAkhirPajak}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="tglAkhirPajak-label" class="property-label"><g:message
                                code="noEfaktur.tglAkhirPajak.label" default="Tgl Akhir Pajak" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="tglAkhirPajak-label">
                        %{--<ba:editableValue
                                bean="${noEfakturInstance}" field="tglAkhirPajak"
                                url="${request.contextPath}/NoEfaktur/updatefield" type="text"
                                title="Enter tglAkhirPajak" onsuccess="reloadNoEfakturTable();" />--}%

                        <g:formatDate date="${noEfakturInstance?.tglAkhirPajak}" />

                    </span></td>

                </tr>
            </g:if>


            <g:if test="${noEfakturInstance?.jumlah}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="jumlah-label" class="property-label"><g:message
                                code="noEfaktur.sisa.label" default="Jumlah nomor Efaktur" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="jumlah-label">
                        %{--<ba:editableValue
                                bean="${noEfakturInstance}" field="sisa"
                                url="${request.contextPath}/NoEfaktur/updatefield" type="text"
                                title="Enter sisa" onsuccess="reloadNoEfakturTable();" />--}%

                        <g:fieldValue bean="${noEfakturInstance}" field="jumlah"/>

                    </span></td>

                </tr>
            </g:if>
            <g:if test="${noEfakturInstance?.sisa}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="sisa-label" class="property-label"><g:message
                                code="noEfaktur.sisa.label" default="Sisa nomor Efaktur" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="sisa-label">
                        %{--<ba:editableValue
                                bean="${noEfakturInstance}" field="sisa"
                                url="${request.contextPath}/NoEfaktur/updatefield" type="text"
                                title="Enter sisa" onsuccess="reloadNoEfakturTable();" />--}%

                        <g:fieldValue bean="${noEfakturInstance}" field="sisa"/>

                    </span></td>

                </tr>
            </g:if>


            <g:if test="${noEfakturInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="noEfaktur.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${noEfakturInstance}" field="createdBy"
                                url="${request.contextPath}/NoEfaktur/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadNoEfakturTable();" />--}%

                        <g:fieldValue bean="${noEfakturInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${noEfakturInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="noEfaktur.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${noEfakturInstance}" field="dateCreated"
                                url="${request.contextPath}/NoEfaktur/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadNoEfakturTable();" />--}%

                        <g:formatDate date="${noEfakturInstance?.dateCreated}" />

                    </span></td>

                </tr>
            </g:if>







				<g:if test="${noEfakturInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="noEfaktur.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${noEfakturInstance}" field="updatedBy"
								url="${request.contextPath}/NoEfaktur/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadNoEfakturTable();" />--}%
							
								<g:fieldValue bean="${noEfakturInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('noEfaktur');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				%{--<g:remoteLink class="btn btn-primary edit" action="edit"--}%
					%{--id="${noEfakturInstance?.id}"--}%
					%{--update="[success:'noEfaktur-form',failure:'noEfaktur-form']"--}%
					%{--on404="alert('not found');">--}%
					%{--<g:message code="default.button.edit.label" default="Edit" />--}%
				%{--</g:remoteLink>--}%
				%{--<ba:confirm id="delete" class="btn cancel"--}%
					%{--message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"--}%
					%{--onsuccess="deleteNoEfaktur('${noEfakturInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>--}%
			</fieldset>
		</g:form>
	</div>
</body>
</html>
