
<%@ page import="com.kombos.maintable.NoEfaktur" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="noEfaktur_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

            %{--suffix--}%
            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="noEfaktur.noAwal.label" default="Nomor Awal" /></div>
            </th>

            %{--prefix--}%
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="noEfaktur.noAkhir.label" default="Nomor Akhir" /></div>
			</th>

            %{--noawal dan terakhirPakai--}%
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="noEfaktur.prefix.label" default="Nomor Urut Awal" /></div>
			</th>

            %{--noAkhir--}%
            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="noEfaktur.suffix.label" default="Nomor Urut Akhir" /></div>
            </th>

            %{--tglPermohonanPajak--}%
            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="noEfaktur.tglAkhirPajak.label" default="Tgl Permohonan Pajak" /></div>
            </th>

            %{--tanggalBerakhirPAJAK--}%
            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="noEfaktur.tglAkhirPajak.label" default="Tgl Akhir Berlaku Pajak" /></div>
            </th>

            %{--jumlah--}%
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="noEfaktur.statusHabis.label" default="Jumlah Nomor Efaktur" /></div>
			</th>


            %{--Sisa--}%
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="noEfaktur.statusHabis.label" default="Sisa Nomor Efaktur" /></div>
			</th>




		</tr>
		<tr>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_suffix" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_suffix" class="search_init" />
                </div>
            </th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_prefix" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_prefix" class="search_init" />
				</div>
			</th>


			<th style="border-top: none;padding: 5px;">
				<div id="filter_noAwal" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_noAwal" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_noAkhir" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_noAkhir" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_tglPermohonanPajak" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
                    <input type="hidden" name="search_tglPermohonanPajak" value="date.struct">
                    <input type="hidden" name="search_tglPermohonanPajak_day" id="search_tglPermohonanPajak_day" value="">
                    <input type="hidden" name="search_tgltglPermohonanPajak_month" id="search_tglPermohonanPajak_month" value="">
                    <input type="hidden" name="search_tglPermohonanPajak_year" id="search_tglPermohonanPajak_year" value="">
                    <input type="text" data-date-format="dd/mm/yyyy" name="search_tglPermohonanPajak_dp" value="" id="search_tglPermohonanPajak" class="search_init">
                </div>
            </th>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_tglAkhirPajak" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
                    <input type="hidden" name="search_tglAkhirPajak" value="date.struct">
                    <input type="hidden" name="search_tglAkhirPajak_day" id="search_tglAkhirPajak_day" value="">
                    <input type="hidden" name="search_tglAkhirPajak_month" id="search_tglAkhirPajak_month" value="">
                    <input type="hidden" name="search_tglAkhirPajak_year" id="search_tglAkhirPajak_year" value="">
                    <input type="text" data-date-format="dd/mm/yyyy" name="search_tglAkhirPajak_dp" value="" id="search_tglAkhirPajak" class="search_init">
                </div>
            </th>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_jumlah" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_jumlah" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
				<div id="filter_sisa" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_sisa" class="search_init" />
				</div>
			</th>



		</tr>
	</thead>
</table>

<g:javascript>
var noEfakturTable;
var reloadNoEfakturTable;
$(function(){
	
	reloadNoEfakturTable = function() {
		noEfakturTable.fnDraw();
	}

	
	$('#search_tglAkhirPajak').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tglAkhirPajak_day').val(newDate.getDate());
			$('#search_tglAkhirPajak_month').val(newDate.getMonth()+1);
			$('#search_tglAkhirPajak_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			noEfakturTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	noEfakturTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	noEfakturTable = $('#noEfaktur_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "suffix",
	"mDataProp": "suffix",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
//		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a>';
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';

	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "prefix",
	"mDataProp": "prefix",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "noAwal",
	"mDataProp": "noAwal",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "noAkhir",
	"mDataProp": "noAkhir",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "tglPermohonanPajak",
	"mDataProp": "tglPermohonanPajak",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}


,

{
	"sName": "tglAkhirPajak",
	"mDataProp": "tglAkhirPajak",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "jumlah",
	"mDataProp": "jumlah",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "sisa",
	"mDataProp": "sisa",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						var suffix = $('#filter_suffix input').val();
						if(suffix){
							aoData.push(
									{"name": 'sCriteria_suffix', "value": suffix}
							);
						}


						var prefix = $('#filter_prefix input').val();
						if(prefix){
							aoData.push(
									{"name": 'sCriteria_prefix', "value": prefix}
							);
						}
	

						var noAwal  = $('#filter_noAwal input').val();
						if(noAwal){
							aoData.push(
									{"name": 'sCriteria_noAwal', "value": noAwal}
							);
						}

						var noAkhir = $('#filter_noAkhir input').val();
						if(noAkhir){
							aoData.push(
									{"name": 'sCriteria_noAkhir', "value": noAkhir}
							);
						}

						var tglPermohonanPajak = $('#search_tglPermohonanPajak').val();
						var tglPermohonanPajakDay = $('#search_tglPermohonanPajak_day').val();
						var tglPermohonanPajakMonth = $('#search_tglPermohonanPajak_month').val();
						var tglPermohonanPajakYear = $('#search_tglPermohonanPajak_year').val();

						if(tglPermohonanPajak){
							aoData.push(
									{"name": 'sCriteria_tglPermohonanPajak', "value": "date.struct"},
									{"name": 'sCriteria_tglPermohonanPajak_dp', "value": tglPermohonanPajak},
									{"name": 'sCriteria_tglPermohonanPajak_day', "value": tglPermohonanPajak},
									{"name": 'sCriteria_tglPermohonanPajak_month', "value": tglPermohonanPajak},
									{"name": 'sCriteria_tglPermohonanPajak_year', "value": tglPermohonanPajak}
							);
						}



						var tglAkhirPajak = $('#search_tglAkhirPajak').val();
						var tglAkhirPajakDay = $('#search_tglAkhirPajak_day').val();
						var tglAkhirPajakMonth = $('#search_tglAkhirPajak_month').val();
						var tglAkhirPajakYear = $('#search_tglAkhirPajak_year').val();

						if(tglAkhirPajak){
							aoData.push(
									{"name": 'sCriteria_tglAkhirPajak', "value": "date.struct"},
									{"name": 'sCriteria_tglAkhirPajak_dp', "value": tglAkhirPajak},
									{"name": 'sCriteria_tglAkhirPajak_day', "value": tglAkhirPajakDay},
									{"name": 'sCriteria_tglAkhirPajak_month', "value": tglAkhirPajakMonth},
									{"name": 'sCriteria_tglAkhirPajak_year', "value": tglAkhirPajakYear}
							);
						}


						var jumlah = $('#filter_jumlah input').val();
						if(jumlah){
							aoData.push(
									{"name": 'sCriteria_jumlah', "value": jumlah}
							);
						}
						var sisa = $('#filter_sisa input').val();
						if(sisa){
							aoData.push(
									{"name": 'sCriteria_sisa', "value": sisa}
							);
						}


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
