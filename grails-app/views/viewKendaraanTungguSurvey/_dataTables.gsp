<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="kendaraanTunggu_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="kendaraanTunggu.m111ID.label" default="Nama Asuransi" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="kendaraanTunggu.m111Nama.label" default="Nomor Polisi" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="kendaraanTunggu.satuan.label" default="Nomor Polisi Asuransi" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="kendaraanTunggu.satuan.label" default="Status" /></div>
            </th>

        </tr>

	</thead>
</table>

<g:javascript>
var kendaraanTungguTable;
var reloadKendaraanTungguTable;
$(function(){


     $('#view').click(function(e){
        e.stopPropagation();
		kendaraanTungguTable.fnDraw();
	});

	
	reloadKendaraanTungguTable = function() {
		kendaraanTungguTable.fnDraw();
	}
    $('#search_satuan').change(function(){
        kendaraanTungguTable.fnDraw();
    });
	var recordsKendaraanTungguPerPage = [];
    var anKendaraanTungguSelected;
    var jmlRecKendaraanTungguPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	kendaraanTungguTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	kendaraanTungguTable = $('#kendaraanTunggu_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsKendaraanTunggu = $("#kendaraanTunggu_datatables tbody .row-select");
            var jmlKendaraanTungguCek = 0;
            var nRow;
            var idRec;
            rsKendaraanTunggu.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsKendaraanTungguPerPage[idRec]=="1"){
                    jmlKendaraanTungguCek = jmlKendaraanTungguCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsKendaraanTungguPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecKendaraanTungguPerPage = rsKendaraanTunggu.length;
            if(jmlKendaraanTungguCek==jmlRecKendaraanTungguPerPage && jmlRecKendaraanTungguPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "namaAsuransi",
	"mDataProp": "namaAsuransi",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},

{
	"sName": "nomorPolisi",
	"mDataProp": "nomorPolisi",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},


{
	"sName": "nomorPolisAsuransi",
	"mDataProp": "nomorPolisAsuransi",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},

{
	"sName": 'status',
	"mDataProp": "status",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
					    var tanggal = $('#search_tanggalSPK').val();
						var tanggalDay = $('#search_tanggalSPK_day').val();
						var tanggalMonth = $('#search_tanggalSPK_month').val();
						var tanggalYear = $('#search_tanggalSPK_year').val();

						if(tanggal){
							aoData.push(
									{"name": 'sCriteria_tanggalSPK', "value": "date.struct"},
									{"name": 'sCriteria_tanggalSPK_dp', "value": tanggal},
									{"name": 'sCriteria_tanggalSPK_day', "value": tanggalDay},
									{"name": 'sCriteria_tanggalSPK_month', "value": tanggalMonth},
									{"name": 'sCriteria_tanggalSPK_year', "value": tanggalYear}
							);
						}
	


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

	$('.select-all').click(function(e) {

        $("#kendaraanTunggu_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsKendaraanTungguPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsKendaraanTungguPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#kendaraanTunggu_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsKendaraanTungguPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anKendaraanTungguSelected = kendaraanTungguTable.$('tr.row_selected');

            if(jmlRecKendaraanTungguPerPage == anKendaraanTungguSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsKendaraanTungguPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>