<%@ page import="com.kombos.customerprofile.CustomerSurvey" %>

<div class="control-group fieldcontain ${hasErrors(bean: customerSurveyInstance, field: 'customerSurvey', 'error')} ">

    <div class="controls">
        <input type="file" required="" id="fileExcel" name="fileExcel" accept="application/excel|application/vnd.ms-excel" />

    </div>
</div>
