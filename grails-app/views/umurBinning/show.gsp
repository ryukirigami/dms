

<%@ page import="com.kombos.parts.UmurBinning" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'umurBinning.label', default: 'Batas Waktu Di Binning')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteUmurBinning;

$(function(){ 
	deleteUmurBinning=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/umurBinning/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadUmurBinningTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-umurBinning" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="umurBinning"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${umurBinningInstance?.m163TglBerlaku}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m163TglBerlaku-label" class="property-label"><g:message
					code="umurBinning.m163TglBerlaku.label" default="Tanggal Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m163TglBerlaku-label">
						%{--<ba:editableValue
								bean="${umurBinningInstance}" field="m163TglBerlaku"
								url="${request.contextPath}/UmurBinning/updatefield" type="text"
								title="Enter m163TglBerlaku" onsuccess="reloadUmurBinningTable();" />--}%
							
								<g:formatDate date="${umurBinningInstance?.m163TglBerlaku}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${umurBinningInstance?.m163BatasWaktuBinning}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m163BatasWaktuBinning-label" class="property-label"><g:message
					code="umurBinning.m163BatasWaktuBinning.label" default="Batas Waktu di Binning" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m163BatasWaktuBinning-label">
						%{--<ba:editableValue
								bean="${umurBinningInstance}" field="m163BatasWaktuBinning"
								url="${request.contextPath}/UmurBinning/updatefield" type="text"
								title="Enter m163BatasWaktuBinning" onsuccess="reloadUmurBinningTable();" />--}%
							
								<g:fieldValue bean="${umurBinningInstance}" field="m163BatasWaktuBinning"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${umurBinningInstance?.companyDealer}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="companyDealer-label" class="property-label"><g:message--}%
					%{--code="umurBinning.companyDealer.label" default="Company Dealer" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="companyDealer-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${umurBinningInstance}" field="companyDealer"--}%
								%{--url="${request.contextPath}/UmurBinning/updatefield" type="text"--}%
								%{--title="Enter companyDealer" onsuccess="reloadUmurBinningTable();" />--}%
							%{----}%
								%{--<g:link controller="companyDealer" action="show" id="${umurBinningInstance?.companyDealer?.id}">${umurBinningInstance?.companyDealer?.encodeAsHTML()}</g:link>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%

            <g:if test="${umurBinningInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="umurBinning.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${umurBinningInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/UmurBinning/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadUmurBinningTable();" />--}%

                        <g:fieldValue bean="${umurBinningInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${umurBinningInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="umurBinning.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${umurBinningInstance}" field="dateCreated"
                                url="${request.contextPath}/UmurBinning/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadUmurBinningTable();" />--}%

                        <g:formatDate date="${umurBinningInstance?.dateCreated}" type="datetime" style="MEDIUM"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${umurBinningInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="umurBinning.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${umurBinningInstance}" field="createdBy"
								url="${request.contextPath}/UmurBinning/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadUmurBinningTable();" />--}%
							
								<g:fieldValue bean="${umurBinningInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${umurBinningInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="umurBinning.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${umurBinningInstance}" field="lastUpdated"
								url="${request.contextPath}/UmurBinning/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadUmurBinningTable();" />--}%
							
								<g:formatDate date="${umurBinningInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${umurBinningInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="umurBinning.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${umurBinningInstance}" field="updatedBy"
                                url="${request.contextPath}/UmurBinning/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadUmurBinningTable();" />--}%

                        <g:fieldValue bean="${umurBinningInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${umurBinningInstance?.id}"
					update="[success:'umurBinning-form',failure:'umurBinning-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteUmurBinning('${umurBinningInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
