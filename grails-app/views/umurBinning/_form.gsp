<%@ page import="com.kombos.parts.UmurBinning" %>
<r:require modules="autoNumeric" />
<script type="text/javascript">
    jQuery(function($) {
        $('.auto').autoNumeric('init',{
            vMin:'0',
            mDec: '',
            aSep:''
        });
    });
</script>


<div class="control-group fieldcontain ${hasErrors(bean: umurBinningInstance, field: 'm163TglBerlaku', 'error')} required">
	<label class="control-label" for="m163TglBerlaku">
		<g:message code="umurBinning.m163TglBerlaku.label" default="Tanggal Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<ba:datePicker name="m163TglBerlaku" precision="day"  value="${umurBinningInstance?.m163TglBerlaku}" format="dd/MM/yyyy" required="true"/>
	</div>
</div>

<g:javascript>
    document.getElementById("m163TglBerlaku").focus();
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: umurBinningInstance, field: 'm163BatasWaktuBinning', 'error')} required">
	<label class="control-label" for="m163BatasWaktuBinning">
		<g:message code="umurBinning.m163BatasWaktuBinning.label" default="Batas Waktu Di Binning" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField class="auto" name="m163BatasWaktuBinning" type="number" value="${umurBinningInstance.m163BatasWaktuBinning}" required=""/> Hari Setelah Tgl Parts datang
	</div>
</div>

%{--<div class="control-group fieldcontain ${hasErrors(bean: umurBinningInstance, field: 'companyDealer', 'error')} ">--}%
	%{--<label class="control-label" for="companyDealer">--}%
		%{--<g:message code="umurBinning.companyDealer.label" default="Company Dealer" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:select id="companyDealer" name="companyDealer.id" from="${com.kombos.baseapp.maintable.CompanyDealer.list()}" optionKey="id" value="${umurBinningInstance?.companyDealer?.id}" class="many-to-one" noSelection="['null': '']"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: umurBinningInstance, field: 'createdBy', 'error')} ">--}%
	%{--<label class="control-label" for="createdBy">--}%
		%{--<g:message code="umurBinning.createdBy.label" default="Created By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="createdBy" value="${umurBinningInstance?.createdBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: umurBinningInstance, field: 'updatedBy', 'error')} ">--}%
	%{--<label class="control-label" for="updatedBy">--}%
		%{--<g:message code="umurBinning.updatedBy.label" default="Updated By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="updatedBy" value="${umurBinningInstance?.updatedBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: umurBinningInstance, field: 'lastUpdProcess', 'error')} ">--}%
	%{--<label class="control-label" for="lastUpdProcess">--}%
		%{--<g:message code="umurBinning.lastUpdProcess.label" default="Last Upd Process" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="lastUpdProcess" value="${umurBinningInstance?.lastUpdProcess}"/>--}%
	%{--</div>--}%
%{--</div>--}%

