
<%@ page import="com.kombos.parts.UmurBinning" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>
<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</g:javascript>
<table id="umurBinning_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="umurBinning.m163TglBerlaku.label" default="Tanggal Berlaku" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="umurBinning.m163BatasWaktuBinning.label" default="Batas Waktu Di Binning" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="umurBinning.companyDealer.label" default="Company Dealer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="umurBinning.createdBy.label" default="Created By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="umurBinning.updatedBy.label" default="Updated By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="umurBinning.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>

		
		</tr>
		<tr>
		

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m163TglBerlaku" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m163TglBerlaku" value="date.struct">
					<input type="hidden" name="search_m163TglBerlaku_day" id="search_m163TglBerlaku_day" value="">
					<input type="hidden" name="search_m163TglBerlaku_month" id="search_m163TglBerlaku_month" value="">
					<input type="hidden" name="search_m163TglBerlaku_year" id="search_m163TglBerlaku_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m163TglBerlaku_dp" value="" id="search_m163TglBerlaku" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m163BatasWaktuBinning" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m163BatasWaktuBinning" class="search_init" onkeypress="return isNumberKey(event)"/>
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_companyDealer" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_createdBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_updatedBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var umurBinningTable;
var reloadUmurBinningTable;
$(function(){
	
	reloadUmurBinningTable = function() {
		umurBinningTable.fnDraw();
	}

	var recordsUmurBinningPerPage = [];
    var anUmurBinningSelected;
    var jmlRecUmurBinningPerPage=0;
    var id;
    
	$('#search_m163TglBerlaku').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m163TglBerlaku_day').val(newDate.getDate());
			$('#search_m163TglBerlaku_month').val(newDate.getMonth()+1);
			$('#search_m163TglBerlaku_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			umurBinningTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	umurBinningTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	umurBinningTable = $('#umurBinning_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsUmurBinning = $("#umurBinning_datatables tbody .row-select");
            var jmlUmurBinningCek = 0;
            var nRow;
            var idRec;
            rsUmurBinning.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsUmurBinningPerPage[idRec]=="1"){
                    jmlUmurBinningCek = jmlUmurBinningCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsUmurBinningPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecUmurBinningPerPage = rsUmurBinning.length;
            if(jmlUmurBinningCek==jmlRecUmurBinningPerPage && jmlRecUmurBinningPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m163TglBerlaku",
	"mDataProp": "m163TglBerlaku",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m163BatasWaktuBinning",
	"mDataProp": "m163BatasWaktuBinning",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "companyDealer",
	"mDataProp": "companyDealer",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "createdBy",
	"mDataProp": "createdBy",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "updatedBy",
	"mDataProp": "updatedBy",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var m163TglBerlaku = $('#search_m163TglBerlaku').val();
						var m163TglBerlakuDay = $('#search_m163TglBerlaku_day').val();
						var m163TglBerlakuMonth = $('#search_m163TglBerlaku_month').val();
						var m163TglBerlakuYear = $('#search_m163TglBerlaku_year').val();
						
						if(m163TglBerlaku){
							aoData.push(
									{"name": 'sCriteria_m163TglBerlaku', "value": "date.struct"},
									{"name": 'sCriteria_m163TglBerlaku_dp', "value": m163TglBerlaku},
									{"name": 'sCriteria_m163TglBerlaku_day', "value": m163TglBerlakuDay},
									{"name": 'sCriteria_m163TglBerlaku_month', "value": m163TglBerlakuMonth},
									{"name": 'sCriteria_m163TglBerlaku_year', "value": m163TglBerlakuYear}
							);
						}
	
						var m163BatasWaktuBinning = $('#filter_m163BatasWaktuBinning input').val();
						if(m163BatasWaktuBinning){
							aoData.push(
									{"name": 'sCriteria_m163BatasWaktuBinning', "value": m163BatasWaktuBinning}
							);
						}
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
	$('.select-all').click(function(e) {

        $("#umurBinning_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsUmurBinningPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsUmurBinningPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#umurBinning_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsUmurBinningPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anUmurBinningSelected = umurBinningTable.$('tr.row_selected');
            if(jmlRecUmurBinningPerPage == anUmurBinningSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsUmurBinningPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
