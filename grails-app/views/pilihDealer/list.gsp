
<%@ page import="com.kombos.administrasi.DealerPenjual" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'dealerPenjual.label', default: 'Pilih Dealer')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var showDealer;
	var loadForm;
	var shrinkTableLayout;
	$(function(){
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    showDealer = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/dealerPenjual/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };

    tambahDealer = function(from){
        var from = from;
        console.log('masuk tambah dealer');
        console.log('ini niiihhhhh '+from);
        checkDealer =[];
        $("#dealerPenjual-table tbody .row-select").each(function() {
            if(this.checked){
                var id = $(this).next("input:hidden").val();
                checkDealer.push(id);
            }
        });
        if(checkDealer.length<1){
            alert('Anda belum memilih data');
            loadpilihDealerModal();
        }
        var checkDealerMap = JSON.stringify(checkDealer);
        $.ajax({
            url:'${request.contextPath}/complaint/tambahDealer',
            type: "POST", // Always use POST when deleting data
            data : { ids: checkDealerMap},
            success : function(data){
                if(from=='pembelian'){
    //                    $('#dealerPembelian').val(data);
                    $('#t921DealerPembelian').val(data);
                }else if(from=='komplain'){
    //                    $('#dealerDiKomplain').val(data);
                    $('#t921DealerDiKomplain').val(data);
                }else if(from=='komplainTempat'){
    //                    $('#tempatKomplain').val(data);
                    $('#t921DealerKomplain').val(data);
                }
                $('#result_search').empty();
            },
            error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
            }
        });
        checkDealer = [];
    }

});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="dealerPenjual-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
            <table>
                <tr>
                    <td style="padding: 5px">
                        <g:field onclick="tambahDealer('pembelian');" type="button" class="btn cancel" name="dealerPembelian" id="dealerPembelian"
                                 value="${message(code: 'default.button.complaint.dealerPembelian.label', default: 'Dealer Pembelian')}" />
                    </td>
                    <td style="padding: 5px">
                        <g:field onclick="tambahDealer('komplainTempat');" type="button" class="btn cancel" name="dealerKomplainTempat" id="dealerKomplainTempat"
                                 value="${message(code: 'default.button.complaint.tempatKomplain.label', default: 'Dealer Tempat Komplain')}" />
                    </td>
                    <td style="padding: 5px">
                        <g:field type="button" onclick="tambahDealer('komplain');" class="btn cancel" name="dealerKomplain" id="dealerKomplain"
                                 value="${message(code: 'default.button.complaint.dealerKomplain.label', default: 'Dealer yang Dikomplain')}" />
                    </td>
                </tr>
                <br/>
                %{--<tr>--}%
                    %{--<td style="padding: 5px">--}%
                        %{--<g:field style="width:200px" type="button" onclick="tambahDealer('komplain');" class="btn cancel" name="dealerKomplain" id="dealerKomplain"--}%
                                 %{--value="${message(code: 'default.button.complaint.dealerKomplain.label', default: 'Dealer yang Dikomplain')}" />--}%
                    %{--</td>--}%
                    %{--<td style="padding: 5px">--}%
                        %{--<br/>--}%
                        %{--<g:textField style="width: 870px" id="dealerDiKomplain" name="dealerDiKomplain" readonly="" value=""/>--}%
                    %{--</td>--}%
                %{--</tr>--}%
            </table>
		</div>
	</div>
</body>
</html>
