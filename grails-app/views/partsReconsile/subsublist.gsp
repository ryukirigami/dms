<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript>
var partsReconsileSubSubTable_${idTable};
$(function(){
partsReconsileSubSubTable_${idTable} = $('#partsReconsile_datatables_sub_sub_${idTable}').dataTable({
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubSubList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"11px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"5px",
               "sDefaultContent": ''
},
{
	"sName": "goods",
	"mDataProp": "kodePart",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"> <input type="hidden" value="'+row['id']+'">&nbsp;&nbsp; ' + data;
    %{--return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';--}%
        },
        "bSortable": false,
        "sWidth":"108px",
        "bVisible": true
    },
    {
        "sName": "goods",
        "mDataProp": "namaPart",
        "aTargets": [1],
        "bSortable": false,
        "sWidth":"157px",
        "bVisible": true
    },
    {
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"200px",
	"sDefaultContent": '',
	"bVisible": true
    },
    {
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"200px",
	"sDefaultContent": '',
	"bVisible": true
    },
    {
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"200px",
	"sDefaultContent": '',
	"bVisible": true
    }

    ],
            "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                            aoData.push(
                                        {"name": 'noPo', "value": "${noPo}"},
                                        {"name": 'noInvoice', "value": "${noInvoice}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
    </g:javascript>
</head>
<body>
<div class="innerinnerDetails">
    <table id="partsReconsile_datatables_sub_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
           class="display table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Kode Part</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Nama Part</div>
            </th>
            <th style="border-bottom: none; padding: 5px;" />
            <th style="border-bottom: none; padding: 5px;" />
            <th style="border-bottom: none; padding: 5px;" />
        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
</body>
</html>
