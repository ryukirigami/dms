
<%@ page import="com.kombos.parts.Returns" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="title" value="View Parts Reconsile" />
    <title>${title}</title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;
	var printPartsReconsile;
	var printPartsReconsileCont;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){

	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
                                   onLoading="jQuery('#spinner').fadeIn(1);"
                                   onSuccess="loadForm(data, textStatus);"
                                   onComplete="jQuery('#spinner').fadeOut();" />
        break;
    case '_DELETE_' :
        bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});

         		break;
       }
       return false;
	});


});
        var checkin = $('#search_t017Tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
        }).on('changeDate', function(ev) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
            checkin.hide();
            $('#search_t017Tanggal2')[0].focus();
        }).data('datepicker');

        var checkout = $('#search_t017Tanggal2').datepicker({
            onRender: function(date) {
                return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');

        printPartsReconsile = function(){
           checkPartsReconsile =[];
           var vendor,noPartsReconsile,tglPartsReconsile,petugasPartsReconsile, vendor = false;
            $("#partsReconsile-table tbody .row-select").each(function() {
                if(this.checked){
                 var nRow = $(this).next("#noVendor").val()?$(this).next("#noVendor").val():"-"
                    if(nRow!="-"){
                        checkPartsReconsile.push(nRow);
                        vendor = true;
                    }
                }
            });
            if(checkPartsReconsile.length<1 ){
                alert('Silahkan Pilih Salah satu Vendor Untuk Dicetak');
                return;
            }else if(vendor==false){
                alert('Silahkan Pilih tanda centang yang ada di Vendor');
                return;
            }
           var idPartsReconsile =  JSON.stringify(checkPartsReconsile);
           window.location = "${request.contextPath}/partsReconsile/printPartsReconsile?idPartsReconsile="+idPartsReconsile;
        }
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left">${title}</span>
    <ul class="nav pull-right">

        <li class="separator"></li>
    </ul>
</div>
<div class="box">
    <div class="span12" id="partsReconsile-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <fieldset>
            <table style="padding-right: 10px">
                <tr>
                    <td style="width: 130px">
                        <label class="control-label" for="t951Tanggal">
                            <g:message code="auditTrail.tanggal.label" default="Tanggal PO " />&nbsp;
                        </label>&nbsp;&nbsp;
                    </td>
                    <td>
                        <div id="filter_m777Tgl" class="controls">
                            <ba:datePicker id="search_t017Tanggal" name="search_t017Tanggal" precision="day" format="dd/MM/yyyy"  value="" />
                            &nbsp;&nbsp;&nbsp;s.d.&nbsp;&nbsp;&nbsp;
                            <ba:datePicker id="search_t017Tanggal2" name="search_t017Tanggal2" precision="day" format="dd/MM/yyyy"  value=""  />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" >
                        <div class="controls" style="right: 0">
                            <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-primary" name="view" id="view" >View</button>
                            <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-cancel" name="clear" id="clear" >Clear</button>

                        </div>
                    </td>
                </tr>
            </table>

        </fieldset>
        <g:render template="dataTables" />
        <fieldset class="buttons controls" style="padding-top: 10px;">
            %{--<button id='printPartsReconsileData' onclick="printPartsReconsile();" type="button" class="btn btn-primary">Print Parts PartsReconsile</button>--}%
        </fieldset>

    </div>
    <div class="span7" id="partsReconsile-form" style="display: none;"></div>
</div>
</body>
</html>
