

<%@ page import="com.kombos.administrasi.Operation" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'operation.label', default: 'Repair')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteOperation;

$(function(){ 
	deleteOperation=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/operation/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadOperationTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-operation" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="operation"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${operationInstance?.section}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="section-label" class="property-label"><g:message
					code="operation.section.label" default="Section" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="section-label">
						%{--<ba:editableValue
								bean="${operationInstance}" field="section"
								url="${request.contextPath}/Operation/updatefield" type="text"
								title="Enter section" onsuccess="reloadOperationTable();" />--}%
							
								${operationInstance?.section?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${operationInstance?.serial}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="serial-label" class="property-label"><g:message
					code="operation.serial.label" default="Serial" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="serial-label">
						%{--<ba:editableValue
								bean="${operationInstance}" field="serial"
								url="${request.contextPath}/Operation/updatefield" type="text"
								title="Enter serial" onsuccess="reloadOperationTable();" />--}%
							
								${operationInstance?.serial?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${operationInstance?.m053Id}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m053Id-label" class="property-label"><g:message
					code="operation.m053Id.label" default="Kd Repair" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m053Id-label">
						%{--<ba:editableValue
								bean="${operationInstance}" field="m053Id"
								url="${request.contextPath}/Operation/updatefield" type="text"
								title="Enter m053Id" onsuccess="reloadOperationTable();" />--}%
							
								<g:fieldValue bean="${operationInstance}" field="m053Id"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${operationInstance?.m053NamaOperation}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m053NamaOperation-label" class="property-label"><g:message
					code="operation.m053NamaOperation.label" default="Nama Repair" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m053NamaOperation-label">
						%{--<ba:editableValue
								bean="${operationInstance}" field="m053NamaOperation"
								url="${request.contextPath}/Operation/updatefield" type="text"
								title="Enter m053NamaOperation" onsuccess="reloadOperationTable();" />--}%
							
								<g:fieldValue bean="${operationInstance}" field="m053NamaOperation"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${operationInstance?.m053StaLift}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m053StaLift-label" class="property-label"><g:message
					code="operation.m053StaLift.label" default="Butuh Lift" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m053StaLift-label">
						%{--<ba:editableValue
								bean="${operationInstance}" field="m053StaLift"
								url="${request.contextPath}/Operation/updatefield" type="text"
								title="Enter m053StaLift" onsuccess="reloadOperationTable();" />--}%
							
								%{--<g:fieldValue bean="${operationInstance}" field="m053StaLift"/>--}%
                        ${operationInstance?.m053StaLift=="1"?"Ya":"Tidak"}
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${operationInstance?.m053StaPaket}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m053StaPaket-label" class="property-label"><g:message
					code="operation.m053StaPaket.label" default="Status Paket" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m053StaPaket-label">
						%{--<ba:editableValue
								bean="${operationInstance}" field="m053StaPaket"
								url="${request.contextPath}/Operation/updatefield" type="text"
								title="Enter m053StaPaket" onsuccess="reloadOperationTable();" />--}%
							
								%{--<g:fieldValue bean="${operationInstance}" field="m053StaPaket"/>--}%
                        ${operationInstance?.m053StaPaket=="1"?"Ya":"Tidak"}
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${operationInstance?.kategoriJob}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kategoriJob-label" class="property-label"><g:message
					code="operation.kategoriJob.label" default="Kategori Job" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kategoriJob-label">
						%{--<ba:editableValue
								bean="${operationInstance}" field="kategoriJob"
								url="${request.contextPath}/Operation/updatefield" type="text"
								title="Enter kategoriJob" onsuccess="reloadOperationTable();" />--}%
							
								${operationInstance?.kategoriJob?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${operationInstance?.m053StaPaint}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m053StaPaint-label" class="property-label"><g:message
					code="operation.m053StaPaint.label" default="Paint" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m053StaPaint-label">
						%{--<ba:editableValue
								bean="${operationInstance}" field="m053StaPaint"
								url="${request.contextPath}/Operation/updatefield" type="text"
								title="Enter m053StaPaint" onsuccess="reloadOperationTable();" />--}%
							
								%{--<g:fieldValue bean="${operationInstance}" field="m053StaPaint"/>--}%
                        ${operationInstance?.m053StaPaint=="1"?"Ya":"Tidak"}
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${operationInstance?.m053Ket}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m053Ket-label" class="property-label"><g:message
					code="operation.m053Ket.label" default="Keterangan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m053Ket-label">
						%{--<ba:editableValue
								bean="${operationInstance}" field="m053Ket"
								url="${request.contextPath}/Operation/updatefield" type="text"
								title="Enter m053Ket" onsuccess="reloadOperationTable();" />--}%
							
								<g:fieldValue bean="${operationInstance}" field="m053Ket"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${operationInstance?.m053JobsId}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="m053JobsId-label" class="property-label"><g:message--}%
					%{--code="operation.m053JobsId.label" default="M053 Jobs Id" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="m053JobsId-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${operationInstance}" field="m053JobsId"--}%
								%{--url="${request.contextPath}/Operation/updatefield" type="text"--}%
								%{--title="Enter m053JobsId" onsuccess="reloadOperationTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${operationInstance}" field="m053JobsId"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${operationInstance?.staDel}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="staDel-label" class="property-label"><g:message--}%
					%{--code="operation.staDel.label" default="Sta Del" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="staDel-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${operationInstance}" field="staDel"--}%
								%{--url="${request.contextPath}/Operation/updatefield" type="text"--}%
								%{--title="Enter staDel" onsuccess="reloadOperationTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${operationInstance}" field="staDel"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			
				<g:if test="${operationInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="operation.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${operationInstance}" field="lastUpdProcess"
								url="${request.contextPath}/Operation/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadOperationTable();" />--}%
							
								<g:fieldValue bean="${operationInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${operationInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="operation.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${operationInstance}" field="dateCreated"
								url="${request.contextPath}/Operation/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadOperationTable();" />--}%
							
								<g:formatDate date="${operationInstance?.dateCreated}" type="datetime" style="MEDIUM"  />
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${operationInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="operation.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${operationInstance}" field="createdBy"
                                url="${request.contextPath}/Operation/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadOperationTable();" />--}%

                        <g:fieldValue bean="${operationInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${operationInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="operation.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${operationInstance}" field="lastUpdated"
								url="${request.contextPath}/Operation/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadOperationTable();" />--}%
							
								<g:formatDate date="${operationInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${operationInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="operation.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${operationInstance}" field="updatedBy"
                                url="${request.contextPath}/Operation/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadOperationTable();" />--}%

                        <g:fieldValue bean="${operationInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${operationInstance?.id}"
					update="[success:'operation-form',failure:'operation-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteOperation('${operationInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
