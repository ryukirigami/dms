<%@ page import="com.kombos.administrasi.KategoriJob; com.kombos.administrasi.Serial; com.kombos.administrasi.Section; com.kombos.administrasi.Operation" %>
<g:javascript>
    $(document).ready(function() {
        if($('#m053Ket').val().length>0){
            $('#hitung').text(250 - $('#m053Ket').val().length);
        }
        $('#m053Ket').keyup(function() {
            var len = this.value.length;
            if (len >= 250) {
                this.value = this.value.substring(0, 250);
                len = 250;
            }
            $('#hitung').text(250 - len);
        });
    });
    $("#section").change(function(){
        $('#spinner').fadeIn(1);
        $.ajax({
            url: '${request.contextPath}/operation/generateSerial/'+$("#section").val(),
            type: 'GET',
            dataType: 'html',
            success: function(data, textStatus, xhr) {
                if(data!=""){
                    $('#serial').html(data);
                }
            },
            error: function(xhr, textStatus, errorThrown) {
                alert(textStatus);
            },
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
        });
        //alert($("#section").val());
    });

</g:javascript>


<div class="control-group fieldcontain ${hasErrors(bean: operationInstance, field: 'section', 'error')} required">
	<label class="control-label" for="section">
		<g:message code="operation.section.label" default="Nama Section" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select noSelection="${['':'Select One...']}" id="section" name="section.id" from="${Section.createCriteria().list {eq("staDel", "0") ;order("m051NamaSection", "asc")}}" optionKey="id" required="" value="${operationInstance?.section?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: operationInstance, field: 'serial', 'error')} required">
	<label class="control-label" for="serial">
		<g:message code="operation.serial.label" default="Nama Serial" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="serial" name="serial.id" from="${Serial.createCriteria().list {eq("staDel", "0") section{eq("staDel","0")};order("m052NamaSerial", "asc")}}" optionKey="id" required="" value="${operationInstance?.serial?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: operationInstance, field: 'm053Id', 'error')} required">
	<label class="control-label" for="m053Id">
		<g:message code="operation.m053Id.label" default="Kode Repair" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m053Id" maxlength="7" required="" value="${operationInstance?.m053Id}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: operationInstance, field: 'm053NamaOperation', 'error')} required">
	<label class="control-label" for="m053NamaOperation">
		<g:message code="operation.m053NamaOperation.label" default="Nama Repair" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m053NamaOperation" maxlength="50" required="" value="${operationInstance?.m053NamaOperation}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: operationInstance, field: 'm053StaLift', 'error')} required">
	<label class="control-label" for="m053StaLift">
		<g:message code="operation.m053StaLift.label" default="Butuh Lift" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:textField name="m053StaLift" required="" value="${operationInstance?.m053StaLift}"/>--}%
    <g:radioGroup name="m053StaLift" required="" values="['1','0']" labels="['Ya','Tidak']" value="${operationInstance?.m053StaLift}" >
        ${it.radio} ${it.label}
    </g:radioGroup>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: operationInstance, field: 'm053StaPaket', 'error')} required">
	<label class="control-label" for="m053StaPaket">
		<g:message code="operation.m053StaPaket.label" default="Paket" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:textField name="m053StaPaket" required="" value="${operationInstance?.m053StaPaket}"/>--}%
    <g:radioGroup name="m053StaPaket" required="" values="['1','0']" labels="['Ya','Tidak']" value="${operationInstance?.m053StaPaket}" >
        ${it.radio} ${it.label}
    </g:radioGroup>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: operationInstance, field: 'kategoriJob', 'error')} required">
	<label class="control-label" for="kategoriJob">
		<g:message code="operation.kategoriJob.label" default="Kategori Job" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="kategoriJob" name="kategoriJob.id" from="${KategoriJob.createCriteria().list(){order("m055KategoriJob", "asc")}}" optionKey="id" required="" value="${operationInstance?.kategoriJob?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: operationInstance, field: 'm053StaPaint', 'error')} required">
	<label class="control-label" for="m053StaPaint">
		<g:message code="operation.m053StaPaint.label" default="Paint" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:textField name="m053StaPaint" required="" value="${operationInstance?.m053StaPaint}"/>--}%
    <g:radioGroup name="m053StaPaint" required="" values="['1','0']" labels="['Ya','Tidak']" value="${operationInstance?.m053StaPaint}" >
        ${it.radio} ${it.label}
    </g:radioGroup>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: operationInstance, field: 'm053Ket', 'error')} required">
	<label class="control-label" for="m053Ket">
		<g:message code="operation.m053Ket.label" default="Keterangan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:textField name="m053Ket"  value="${operationInstance?.m053Ket}"/>--}%
    <g:textArea rows="2" cols="50" required="" name="m053Ket"  value="${operationInstance?.m053Ket}"/>
        <br/>
        <span id="hitung">250</span> Karakter Tersisa.
	</div>
</div>

%{--<div class="control-group fieldcontain ${hasErrors(bean: operationInstance, field: 'm053JobsId', 'error')} required">--}%
	%{--<label class="control-label" for="m053JobsId">--}%
		%{--<g:message code="operation.m053JobsId.label" default="M053 Jobs Id" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="m053JobsId" required="" value="${operationInstance?.m053JobsId}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: operationInstance, field: 'staDel', 'error')} required">--}%
	%{--<label class="control-label" for="staDel">--}%
		%{--<g:message code="operation.staDel.label" default="Sta Del" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="staDel" required="" value="${operationInstance?.staDel}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: operationInstance, field: 'createdBy', 'error')} ">--}%
	%{--<label class="control-label" for="createdBy">--}%
		%{--<g:message code="operation.createdBy.label" default="Created By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="createdBy" value="${operationInstance?.createdBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: operationInstance, field: 'updatedBy', 'error')} ">--}%
	%{--<label class="control-label" for="updatedBy">--}%
		%{--<g:message code="operation.updatedBy.label" default="Updated By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="updatedBy" value="${operationInstance?.updatedBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: operationInstance, field: 'lastUpdProcess', 'error')} ">--}%
	%{--<label class="control-label" for="lastUpdProcess">--}%
		%{--<g:message code="operation.lastUpdProcess.label" default="Last Upd Process" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="lastUpdProcess" value="${operationInstance?.lastUpdProcess}"/>--}%
	%{--</div>--}%
%{--</div>--}%

<g:javascript>
    document.getElementById('section').focus();
</g:javascript>