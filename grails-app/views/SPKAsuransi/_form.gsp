<%@ page import="com.kombos.customerprofile.SPKAsuransi" %>

<g:javascript>
    var isCharOnly;
    var isNumberKey;
    $(function() {
        $('#customerVehicles').typeahead({
            source: function (query, process) {
                var word = $('#customerVehicles').val();
                return $.get('${request.contextPath}/historyCustomerVehicle/getVincodes?word='+word, { query: query }, function (data) {
                    return process(data.options);
                });
            }
        });

        isCharOnly = function(e) {
            e = e || event;
            return /[a-zA-Z]/i.test(
                    String.fromCharCode(e.charCode || e.keyCode)
            ) || !e.charCode && e.keyCode  < 48;
        }

        isNumberKey = function(evt){
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)){
                return false;
            }
            else{
                return true;
            }
        }

        $(".charonly").bind('keypress',isCharOnly);

        $(".numberonly").bind('keypress',isNumberKey);

    });
    function disableSpace(event){
        var charCode = (event.which) ? event.which : event.keyCode
        if (charCode == 32) {
            return false;
        }
    }
</g:javascript>

<fieldset>
    <legend>Data Polis Asuransi</legend>
    <div class="control-group fieldcontain ${hasErrors(bean: SPKAsuransiInstance, field: 'vendorAsuransi', 'error')} required">
        <label class="control-label" for="vendorAsuransi">
            <g:message code="SPKAsuransi.vendorAsuransi.label" default="Nama Asuransi" />
            <span class="required-indicator">*</span>
        </label>
        <div class="controls">
            <g:select id="vendorAsuransi" name="vendorAsuransi.id" from="${com.kombos.administrasi.VendorAsuransi.list()}" optionKey="id" required="" value="${SPKAsuransiInstance?.vendorAsuransi?.id}" class="many-to-one"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: SPKAsuransiInstance, field: 't193NomorPolis', 'error')} required">
        <label class="control-label" for="t193NomorPolis">
            <g:message code="SPKAsuransi.t193NomorPolis.label" default="Nomor Polis" />
            <span class="required-indicator">*</span>
        </label>
        <div class="controls">
            <g:textField class="numberonly"  name="t193NomorPolis" maxlength="10" required="" value="${SPKAsuransiInstance?.t193NomorPolis}"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: SPKAsuransiInstance, field: 't193NamaPolis', 'error')} required">
        <label class="control-label" for="t193NamaPolis">
            <g:message code="SPKAsuransi.t193NamaPolis.label" default="Nama Polis" />
            <span class="required-indicator">*</span>
        </label>
        <div class="controls">
            <g:textField name="t193NamaPolis" maxlength="50" required="" value="${SPKAsuransiInstance?.t193NamaPolis}"/>
        </div>
    </div>

    %{--<div class="control-group fieldcontain ${hasErrors(bean: SPKAsuransiInstance, field: 't193TglPolis', 'error')} required">--}%
        %{--<label class="control-label" for="t193TglPolis">--}%
            %{--<g:message code="SPKAsuransi.t193TglPolis.label" default="Tgl Polis" />--}%
            %{--<span class="required-indicator">*</span>--}%
        %{--</label>--}%
        %{--<div class="controls">--}%
            %{--<ba:datePicker format="dd/mm/yyyy" required="true" name="t193TglPolis" precision="day"  value="${SPKAsuransiInstance?.t193TglPolis}"  />--}%
        %{--</div>--}%
    %{--</div>--}%

    <div class="control-group fieldcontain ${hasErrors(bean: SPKAsuransiInstance, field: 't193TglAwal', 'error')}">
        <label class="control-label" for="t193TglAwal">
            <g:message code="SPKAsuransi.t193TglAwal.label" default="Masa Berlaku" />
        </label>
        <div class="controls">
            <ba:datePicker format="dd/mm/yyyy" required="false" name="t193TglAwal" precision="day"  value="${SPKAsuransiInstance?.t193TglAwal}"  />
            -
            <ba:datePicker format="dd/mm/yyyy" required="false" name="t193TglAkhir" precision="day"  value="${SPKAsuransiInstance?.t193TglAkhir}"  />
        </div>
    </div>

    %{--<div class="control-group fieldcontain ${hasErrors(bean: SPKAsuransiInstance, field: 't193TglAkhir', 'error')}">--}%
        %{--<label class="control-label" for="t193TglAkhir">--}%
            %{--<g:message code="SPKAsuransi.t193TglAkhir.label" default="Tgl Akhir" />--}%
        %{--</label>--}%
        %{--<div class="controls">--}%
            %{--<ba:datePicker format="dd/mm/yyyy" required="false" name="t193TglAkhir" precision="day"  value="${SPKAsuransiInstance?.t193TglAkhir}"  />--}%
        %{--</div>--}%
    %{--</div>--}%

    <div class="control-group fieldcontain ${hasErrors(bean: SPKAsuransiInstance, field: 't193AlamatPolis', 'error')} ">
        <label class="control-label" for="t193AlamatPolis">
            <g:message code="SPKAsuransi.t193AlamatPolis.label" default="Alamat Customer" />
        </label>
        <div class="controls">
            %{--<g:textField name="t193AlamatPolis" maxlength="50" value="${SPKAsuransiInstance?.t193AlamatPolis}"/>--}%
            <g:textArea rows="5" cols="200" name="t193AlamatPolis" maxlength="255" value="${SPKAsuransiInstance?.t193AlamatPolis}"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: SPKAsuransiInstance, field: 't193TelpPolis', 'error')}">
        <label class="control-label" for="t193TelpPolis">
            <g:message code="SPKAsuransi.t193TelpPolis.label" default="Nomor Telepon" />
        </label>
        <div class="controls">
            <g:textField class="numberonly" name="t193TelpPolis" maxlength="50" value="${SPKAsuransiInstance?.t193TelpPolis}"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: SPKAsuransiInstance, field: 'customerVehicle', 'error')} required">
        <label class="control-label" for="customerVehicle">
            <g:message code="SPKAsuransi.customerVehicle.label" default="VIN Code" />
            <span class="required-indicator">*</span>
        </label>
        <div class="controls">
            <g:textField name="customerVehicles" onkeypress="return disableSpace(event);" id="customerVehicles" required="" class="typeahead" value="${SPKAsuransiInstance?.customerVehicle?.t103VinCode}" autocomplete="off"/>
            %{--<g:select id="customerVehicle" name="customerVehicle.id" from="${com.kombos.customerprofile.CustomerVehicle.findAllByCompanyDealer(session.userCompanyDealer)}" optionKey="id" required="" value="${SPKAsuransiInstance?.customerVehicle?.id}" class="many-to-one"/>--}%
        </div>
    </div>
</fieldset>

<fieldset>
    <legend>Pengiriman Dokumen Asuransi</legend>
    %{--<div class="control-group fieldcontain ${hasErrors(bean: SPKAsuransiInstance, field: 't193StaJanjiKirimDok', 'error')} required">--}%
        %{--<label class="control-label" for="t193StaJanjiKirimDok">--}%
            %{--<g:message code="SPKAsuransi.t193StaJanjiKirimDok.label" default="Sta Janji Kirim Dok" />--}%
            %{--<span class="required-indicator">*</span>--}%
        %{--</label>--}%
        %{--<div class="controls">--}%
            %{--<g:textField name="t193StaJanjiKirimDok" maxlength="1" required="" value="${SPKAsuransiInstance?.t193StaJanjiKirimDok}"/>--}%
        %{--</div>--}%
    %{--</div>--}%

    <div class="control-group fieldcontain ${hasErrors(bean: SPKAsuransiInstance, field: 't193TglJanjiKirimDok', 'error')}">
        <label class="control-label" for="t193TglJanjiKirimDok">
            <g:message code="SPKAsuransi.t193TglJanjiKirimDok.label" default="Tgl Janji Kirim Dok" />
        </label>
        <div class="controls">
            <g:checkBox name="t193StaJanjiKirimDok" value="${SPKAsuransiInstance?.t193StaJanjiKirimDok}" />
            <ba:datePicker format="dd/mm/yyyy" name="t193TglJanjiKirimDok" precision="day"  value="${SPKAsuransiInstance?.t193TglJanjiKirimDok}"  />
        </div>
    </div>

    %{--<div class="control-group fieldcontain ${hasErrors(bean: SPKAsuransiInstance, field: 't193StaKirimDok', 'error')} required">--}%
        %{--<label class="control-label" for="t193StaKirimDok">--}%
            %{--<g:message code="SPKAsuransi.t193StaKirimDok.label" default="Sta Kirim Dok" />--}%
            %{--<span class="required-indicator">*</span>--}%
        %{--</label>--}%
        %{--<div class="controls">--}%
            %{--<g:textField name="t193StaKirimDok" maxlength="1" required="" value="${SPKAsuransiInstance?.t193StaKirimDok}"/>--}%
        %{--</div>--}%
    %{--</div>--}%

    <div class="control-group fieldcontain ${hasErrors(bean: SPKAsuransiInstance, field: 't193TglKirimDok', 'error')}">
        <label class="control-label" for="t193TglKirimDok">
            <g:message code="SPKAsuransi.t193TglKirimDok.label" default="Tgl Kirim Dok" />
        </label>
        <div class="controls">
            <g:checkBox name="t193StaKirimDok" value="${SPKAsuransiInstance?.t193StaKirimDok}" />
            <ba:datePicker format="dd/mm/yyyy" name="t193TglKirimDok" precision="day"  value="${SPKAsuransiInstance?.t193TglKirimDok}"  />
        </div>
    </div>


    <div class="control-group fieldcontain ${hasErrors(bean: SPKAsuransiInstance, field: 't193MetodeKirim', 'error')}">
        <label class="control-label" for="t193MetodeKirim">
            <g:message code="SPKAsuransi.t193MetodeKirim.label" default="Metode Kirim" />
        </label>
        <div class="controls">
            %{--<g:textField name="t193MetodeKirim" maxlength="50" value="${SPKAsuransiInstance?.t193MetodeKirim}"/>--}%
            <g:select name="t193MetodeKirim" maxlength="50" value="${SPKAsuransiInstance?.t193MetodeKirim}" from="${['Fax','Ekspedisi','Pos']}"/>
        </div>
    </div>

</fieldset>

<fieldset>
    <legend>Survey</legend>

    %{--<div class="control-group fieldcontain ${hasErrors(bean: SPKAsuransiInstance, field: 't193StaReminderSurvey', 'error')} required">--}%
        %{--<label class="control-label" for="t193StaReminderSurvey">--}%
            %{--<g:message code="SPKAsuransi.t193StaReminderSurvey.label" default="Sta Reminder Survey" />--}%
            %{--<span class="required-indicator">*</span>--}%
        %{--</label>--}%
        %{--<div class="controls">--}%
            %{--<g:textField name="t193StaReminderSurvey" maxlength="1" required="" value="${SPKAsuransiInstance?.t193StaReminderSurvey}"/>--}%
        %{--</div>--}%
    %{--</div>--}%

    <div class="control-group fieldcontain ${hasErrors(bean: SPKAsuransiInstance, field: 't193TglReminderSurvey', 'error')}">
        <label class="control-label" for="t193TglReminderSurvey">
            <g:message code="SPKAsuransi.t193TglReminderSurvey.label" default="Tgl Reminder Survey" />
        </label>
        <div class="controls">
            <g:checkBox name="t193StaReminderSurvey" value="${SPKAsuransiInstance?.t193StaReminderSurvey}" />
            <ba:datePicker format="dd/mm/yyyy" name="t193TglReminderSurvey" precision="day"  value="${SPKAsuransiInstance?.t193TglReminderSurvey}"  />
        </div>
    </div>


    %{--<div class="control-group fieldcontain ${hasErrors(bean: SPKAsuransiInstance, field: 't193StaActualSurvey', 'error')} required">--}%
        %{--<label class="control-label" for="t193StaActualSurvey">--}%
            %{--<g:message code="SPKAsuransi.t193StaActualSurvey.label" default="Sta Actual Survey" />--}%
            %{--<span class="required-indicator">*</span>--}%
        %{--</label>--}%
        %{--<div class="controls">--}%
            %{--<g:textField name="t193StaActualSurvey" maxlength="1" required="" value="${SPKAsuransiInstance?.t193StaActualSurvey}"/>--}%
        %{--</div>--}%
    %{--</div>--}%

    <div class="control-group fieldcontain ${hasErrors(bean: SPKAsuransiInstance, field: 't193TglActualSurvey', 'error')}">
        <label class="control-label" for="t193TglActualSurvey">
            <g:message code="SPKAsuransi.t193TglActualSurvey.label" default="Tgl Actual Survey" />
        </label>
        <div class="controls">
            <g:checkBox name="t193StaActualSurvey" value="${SPKAsuransiInstance?.t193StaActualSurvey}" />
            <ba:datePicker format="dd/mm/yyyy" name="t193TglActualSurvey" precision="day"  value="${SPKAsuransiInstance?.t193TglActualSurvey}"  />
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: SPKAsuransiInstance, field: 't193Catatan', 'error')}">
        <label class="control-label" for="t193Catatan">
            <g:message code="SPKAsuransi.t193Catatan.label" default="Catatan" />
        </label>
        <div class="controls">
            <g:textArea rows="5" cols="200" name="t193Catatan" maxlength="255" value="${SPKAsuransiInstance?.t193Catatan}"/>
        </div>
    </div>
</fieldset>

<fieldset>
    <legend>SPK Asuransi</legend>


    <div class="control-group fieldcontain ${hasErrors(bean: SPKAsuransiInstance, field: 't193IDAsuransi', 'error')} required">
        <label class="control-label" for="t193IDAsuransi">
            <g:message code="SPKAsuransi.t193IDAsuransi.label" default="ID Asuransi" />
            <span class="required-indicator">*</span>
        </label>
        <div class="controls">
            <g:textField name="t193IDAsuransi" maxlength="24" required="" value="${SPKAsuransiInstance?.t193IDAsuransi}"/>
        </div>
    </div>


    %{--<div class="control-group fieldcontain ${hasErrors(bean: SPKAsuransiInstance, field: 't193StaNomorSPK', 'error')} required">--}%
        %{--<label class="control-label" for="t193StaNomorSPK">--}%
            %{--<g:message code="SPKAsuransi.t193StaNomorSPK.label" default="Sta Nomor SPK" />--}%
            %{--<span class="required-indicator">*</span>--}%
        %{--</label>--}%
        %{--<div class="controls">--}%
            %{--<g:textField name="t193StaNomorSPK" maxlength="1" required="" value="${SPKAsuransiInstance?.t193StaNomorSPK}"/>--}%
        %{--</div>--}%
    %{--</div>--}%

    <div class="control-group fieldcontain ${hasErrors(bean: SPKAsuransiInstance, field: 't193NomorSPK', 'error')} required">
        <label class="control-label" for="t193NomorSPK">
            <g:message code="SPKAsuransi.t193NomorSPK.label" default="Nomor SPK" />
            <span class="required-indicator">*</span>
        </label>
        <div class="controls">
            <g:checkBox name="t193StaNomorSPK" value="${SPKAsuransiInstance?.t193StaNomorSPK}" />
            <g:textField class="numberonly" name="t193NomorSPK" maxlength="50" required="" value="${SPKAsuransiInstance?.t193NomorSPK}"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: SPKAsuransiInstance, field: 't193TanggalSPK', 'error')}">
        <label class="control-label" for="t193TanggalSPK">
            <g:message code="SPKAsuransi.t193TanggalSPK.label" default="Tanggal SPK" />
        </label>
        <div class="controls">
            <ba:datePicker format="dd/mm/yyyy" name="t193TanggalSPK" precision="day"  value="${SPKAsuransiInstance?.t193TanggalSPK}"  />
        </div>
    </div>


    <div class="control-group fieldcontain ${hasErrors(bean: SPKAsuransiInstance, field: 't193StaUtamaTambahan', 'error')} ">
        <label class="control-label" for="t193StaUtamaTambahan">
            <g:message code="SPKAsuransi.t193StaUtamaTambahan.label" default="Status SPK" />
        </label>
        <div class="controls">
            %{--<g:textField name="t193StaUtamaTambahan" maxlength="1" required="" value="${SPKAsuransiInstance?.t193StaUtamaTambahan}"/>--}%
            %{--<g:radio name="t193StaUtama" checked="${(SPKAsuransiInstance?.t193StaUtama==null) || (SPKAsuransiInstance?.t193StaUtama=='0')?false:true}" value="1" />Utama</br>--}%
            %{--<g:radio name="t193StaUtama" checked="${(SPKAsuransiInstance?.t193StaUtamaTambahan==null) || (SPKAsuransiInstance?.t193StaUtamaTambahan=='0')?false:true}" value="1" />Tambahan</br>--}%
            %{--<g:radio name="t193StaGanti" checked="${(SPKAsuransiInstance?.t193StaGantiTambahan==null) || (SPKAsuransiInstance?.t193StaGantiTambahan=='0')?false:true}" value="1" />Pengganti--}%
            <g:radio name="t193StaUtama" checked="${(SPKAsuransiInstance?.t193StaUtama=='1')?true:false}" value="1" />Utama</br>
            <g:radio name="t193StaUtama" checked="${(SPKAsuransiInstance?.t193StaUtama=='2')?true:false}" value="2" />Tambahan</br>
            <g:radio name="t193StaUtama" checked="${(SPKAsuransiInstance?.t193StaUtama=='3')?true:false}" value="3" />Pengganti
        </div>
    </div>

    %{--<div class="control-group fieldcontain ${hasErrors(bean: SPKAsuransiInstance, field: 't193StaGantiTambahan', 'error')} required">--}%
        %{--<label class="control-label" for="t193StaGantiTambahan">--}%
            %{--<g:message code="SPKAsuransi.t193StaGantiTambahan.label" default="Sta Ganti Tambahan" />--}%
            %{--<span class="required-indicator">*</span>--}%
        %{--</label>--}%
        %{--<div class="controls">--}%
            %{--<g:textField name="t193StaGantiTambahan" maxlength="1" required="" value="${SPKAsuransiInstance?.t193StaGantiTambahan}"/>--}%
        %{--</div>--}%
    %{--</div>--}%

    <div class="control-group fieldcontain ${hasErrors(bean: SPKAsuransiInstance, field: 'spkAsuransi', 'error')} ">
        <label class="control-label" for="spkAsuransi">
            <g:message code="SPKAsuransi.spkAsuransi.label" default="Nomor SPK Sebelumnya" />
        </label>
        <div class="controls">
            <g:select id="spkAsuransi" name="spkAsuransi.id" from="${com.kombos.customerprofile.SPKAsuransi.list()}" optionKey="id" value="${SPKAsuransiInstance?.spkAsuransi?.id}" class="many-to-one"/>
        </div>
    </div>
</fieldset>