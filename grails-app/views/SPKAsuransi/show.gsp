

<%@ page import="com.kombos.customerprofile.SPKAsuransi" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'SPKAsuransi.label', default: 'SPKAsuransi')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteSPKAsuransi;

$(function(){ 
	deleteSPKAsuransi=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/SPKAsuransi/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadSPKAsuransiTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-SPKAsuransi" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="SPKAsuransi"
			class="table table-bordered table-hover">
			<tbody>

                <g:if test="${SPKAsuransiInstance?.vendorAsuransi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="vendorAsuransi-label" class="property-label"><g:message
					code="SPKAsuransi.vendorAsuransi.label" default="Nama Asuransi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="vendorAsuransi-label">
						%{--<ba:editableValue
								bean="${SPKAsuransiInstance}" field="vendorAsuransi"
								url="${request.contextPath}/SPKAsuransi/updatefield" type="text"
								title="Enter vendorAsuransi" onsuccess="reloadSPKAsuransiTable();" />--}%
							
								%{--<g:link controller="vendorAsuransi" action="show" id="${SPKAsuransiInstance?.vendorAsuransi?.id}">${SPKAsuransiInstance?.vendorAsuransi?.encodeAsHTML()}</g:link>--}%
                                <g:fieldValue bean="${SPKAsuransiInstance}" field="vendorAsuransi"/>
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${SPKAsuransiInstance?.t193NomorPolis}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t193NomorPolis-label" class="property-label"><g:message
                                code="SPKAsuransi.t193NomorPolis.label" default="Nomor Polis" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t193NomorPolis-label">
                        %{--<ba:editableValue
                                bean="${SPKAsuransiInstance}" field="t193NomorPolis"
                                url="${request.contextPath}/SPKAsuransi/updatefield" type="text"
                                title="Enter t193NomorPolis" onsuccess="reloadSPKAsuransiTable();" />--}%

                        <g:fieldValue bean="${SPKAsuransiInstance}" field="t193NomorPolis"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${SPKAsuransiInstance?.t193NamaPolis}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t193NamaPolis-label" class="property-label"><g:message
                                code="SPKAsuransi.t193NamaPolis.label" default="Nama Polis" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t193NamaPolis-label">
                        %{--<ba:editableValue
                                bean="${SPKAsuransiInstance}" field="t193NamaPolis"
                                url="${request.contextPath}/SPKAsuransi/updatefield" type="text"
                                title="Enter t193NamaPolis" onsuccess="reloadSPKAsuransiTable();" />--}%

                        <g:fieldValue bean="${SPKAsuransiInstance}" field="t193NamaPolis"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${SPKAsuransiInstance?.t193TglAwal && SPKAsuransiInstance?.t193TglAkhir}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t193TglAwal-label" class="property-label"><g:message
                                code="SPKAsuransi.t193TglAwal.label" default="Masa Berlaku" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t193TglAwal-label">

                        <g:formatDate date="${SPKAsuransiInstance?.t193TglAwal}" format="dd-MM-yyyy" /> - <g:formatDate date="${SPKAsuransiInstance?.t193TglAkhir}" format="dd-MM-yyyy" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${SPKAsuransiInstance?.t193AlamatPolis}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t193AlamatPolis-label" class="property-label"><g:message
                                code="SPKAsuransi.t193AlamatPolis.label" default="Alamat Customer" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t193AlamatPolis-label">
                        %{--<ba:editableValue
                                bean="${SPKAsuransiInstance}" field="t193AlamatPolis"
                                url="${request.contextPath}/SPKAsuransi/updatefield" type="text"
                                title="Enter t193AlamatPolis" onsuccess="reloadSPKAsuransiTable();" />--}%

                        <g:fieldValue bean="${SPKAsuransiInstance}" field="t193AlamatPolis"/>

                    </span></td>

                </tr>
            </g:if>


            <g:if test="${SPKAsuransiInstance?.t193TelpPolis}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t193TelpPolis-label" class="property-label"><g:message
                                code="SPKAsuransi.t193TelpPolis.label" default="Nomor Telpon" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t193TelpPolis-label">
                        %{--<ba:editableValue
                                bean="${SPKAsuransiInstance}" field="t193TelpPolis"
                                url="${request.contextPath}/SPKAsuransi/updatefield" type="text"
                                title="Enter t193TelpPolis" onsuccess="reloadSPKAsuransiTable();" />--}%

                        <g:fieldValue bean="${SPKAsuransiInstance}" field="t193TelpPolis"/>

                    </span></td>

                </tr>
            </g:if>


            <g:if test="${SPKAsuransiInstance?.customerVehicle}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="customerVehicle-label" class="property-label"><g:message
					code="SPKAsuransi.customerVehicle.label" default="VIN Code" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="customerVehicle-label">
						%{--<ba:editableValue
								bean="${SPKAsuransiInstance}" field="customerVehicle"
								url="${request.contextPath}/SPKAsuransi/updatefield" type="text"
								title="Enter customerVehicle" onsuccess="reloadSPKAsuransiTable();" />--}%
							
								%{--<g:link controller="customerVehicle" action="show" id="${SPKAsuransiInstance?.customerVehicle?.id}">${SPKAsuransiInstance?.customerVehicle?.encodeAsHTML()}</g:link>--}%
                                <g:fieldValue bean="${SPKAsuransiInstance}" field="customerVehicle"/>
						</span></td>
					
				</tr>
				</g:if>

                <g:if test="${SPKAsuransiInstance?.t193TglJanjiKirimDok}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t193TglJanjiKirimDok-label" class="property-label"><g:message
                                code="SPKAsuransi.t193TglJanjiKirimDok.label" default="Tanggal Customer Janji Kirim Dokumen" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t193TglJanjiKirimDok-label">
                        %{--<ba:editableValue
                                bean="${SPKAsuransiInstance}" field="t193TglJanjiKirimDok"
                                url="${request.contextPath}/SPKAsuransi/updatefield" type="text"
                                title="Enter t193TglJanjiKirimDok" onsuccess="reloadSPKAsuransiTable();" />--}%

                        <g:formatDate date="${SPKAsuransiInstance?.t193TglJanjiKirimDok}" format="dd-MM-yyyy" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${SPKAsuransiInstance?.t193TglKirimDok}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t193TglKirimDok-label" class="property-label"><g:message
                                code="SPKAsuransi.t193TglKirimDok.label" default="Tanggal Kirim Dokumen ke Asuransi" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t193TglKirimDok-label">
                        %{--<ba:editableValue
                                bean="${SPKAsuransiInstance}" field="t193TglKirimDok"
                                url="${request.contextPath}/SPKAsuransi/updatefield" type="text"
                                title="Enter t193TglKirimDok" onsuccess="reloadSPKAsuransiTable();" />--}%

                        <g:formatDate date="${SPKAsuransiInstance?.t193TglKirimDok}" format="dd-MM-yyyy" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${SPKAsuransiInstance?.t193MetodeKirim}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t193MetodeKirim-label" class="property-label"><g:message
                                code="SPKAsuransi.t193MetodeKirim.label" default="Metode Pengiriman" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t193MetodeKirim-label">

                        <g:fieldValue bean="${SPKAsuransiInstance}" field="t193MetodeKirim"/>

                    </span></td>

                </tr>
            </g:if>


            <g:if test="${SPKAsuransiInstance?.t193TglReminderSurvey}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t193TglReminderSurvey-label" class="property-label"><g:message
                                code="SPKAsuransi.t193TglReminderSurvey.label" default="Tanggal Reminder Survey" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t193TglReminderSurvey-label">
                        %{--<ba:editableValue
                                bean="${SPKAsuransiInstance}" field="t193TglReminderSurvey"
                                url="${request.contextPath}/SPKAsuransi/updatefield" type="text"
                                title="Enter t193TglReminderSurvey" onsuccess="reloadSPKAsuransiTable();" />--}%

                        <g:formatDate date="${SPKAsuransiInstance?.t193TglReminderSurvey}" format="dd-MM-yyyy" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${SPKAsuransiInstance?.t193TglActualSurvey}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t193TglActualSurvey-label" class="property-label"><g:message
                                code="SPKAsuransi.t193TglActualSurvey.label" default="Tanggal Actual Survey" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t193TglActualSurvey-label">
                        %{--<ba:editableValue
                                bean="${SPKAsuransiInstance}" field="t193TglActualSurvey"
                                url="${request.contextPath}/SPKAsuransi/updatefield" type="text"
                                title="Enter t193TglActualSurvey" onsuccess="reloadSPKAsuransiTable();" />--}%

                        <g:formatDate date="${SPKAsuransiInstance?.t193TglActualSurvey}" format="dd-MM-yyyy" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${SPKAsuransiInstance?.t193Catatan}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t193Catatan-label" class="property-label"><g:message
                                code="SPKAsuransi.t193Catatan.label" default="Catatan" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t193Catatan-label">
                        %{--<ba:editableValue
                                bean="${SPKAsuransiInstance}" field="t193Catatan"
                                url="${request.contextPath}/SPKAsuransi/updatefield" type="text"
                                title="Enter t193Catatan" onsuccess="reloadSPKAsuransiTable();" />--}%

                        <g:fieldValue bean="${SPKAsuransiInstance}" field="t193Catatan"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${SPKAsuransiInstance?.t193NomorSPK}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t193NomorSPK-label" class="property-label"><g:message
					code="SPKAsuransi.t193NomorSPK.label" default="Nomor SPK" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t193NomorSPK-label">
						%{--<ba:editableValue
								bean="${SPKAsuransiInstance}" field="t193NomorSPK"
								url="${request.contextPath}/SPKAsuransi/updatefield" type="text"
								title="Enter t193NomorSPK" onsuccess="reloadSPKAsuransiTable();" />--}%
							
								<g:fieldValue bean="${SPKAsuransiInstance}" field="t193NomorSPK"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${SPKAsuransiInstance?.t193TanggalSPK}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t193TanggalSPK-label" class="property-label"><g:message
					code="SPKAsuransi.t193TanggalSPK.label" default="Tanggal SPK" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t193TanggalSPK-label">
						%{--<ba:editableValue
								bean="${SPKAsuransiInstance}" field="t193TanggalSPK"
								url="${request.contextPath}/SPKAsuransi/updatefield" type="text"
								title="Enter t193TanggalSPK" onsuccess="reloadSPKAsuransiTable();" />--}%
							
								<g:formatDate date="${SPKAsuransiInstance?.t193TanggalSPK}" format="dd-MM-yyyy" />
							
						</span></td>
					
				</tr>
				</g:if>
			

				<g:if test="${SPKAsuransiInstance?.t193StaUtama}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t193StaUtama-label" class="property-label"><g:message
					code="SPKAsuransi.t193StaUtama.label" default="Status SPK" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t193StaUtama-label">

								<g:if test="${SPKAsuransiInstance?.t193StaUtama=='1'}">
                                    SPK Utama
								</g:if>
                                <g:elseif test="${SPKAsuransiInstance?.t193StaUtama=='2'}">
                                    SPK Pengganti
                                </g:elseif>
							    <g:elseif test="${SPKAsuransiInstance?.t193StaUtama=='3'}">
                                    SPK Tambahan
                                </g:elseif>

						</span></td>
					
				</tr>
				</g:if>
				
				<g:if test="${SPKAsuransiInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="SPKAsuransi.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue--}%
								%{--bean="${SPKAsuransiInstance}" field="lastUpdProcess"--}%
								%{--url="${request.contextPath}/SPKAsuransi/updatefield" type="text"--}%
								%{--title="Enter lastUpdProcess" onsuccess="reloadSPKAsuransiTable();" />--}%

								<g:fieldValue bean="${SPKAsuransiInstance}" field="lastUpdProcess"/>

						</span></td>

				</tr>
				</g:if>

				<g:if test="${SPKAsuransiInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="SPKAsuransi.dateCreated.label" default="Date Created" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue--}%
								%{--bean="${SPKAsuransiInstance}" field="dateCreated"--}%
								%{--url="${request.contextPath}/SPKAsuransi/updatefield" type="text"--}%
								%{--title="Enter dateCreated" onsuccess="reloadSPKAsuransiTable();" />--}%

								<g:formatDate date="${SPKAsuransiInstance?.dateCreated}" type="datetime" style="MEDIUM" format="dd-MM-yyyy HH:mm" />

						</span></td>

				</tr>
				</g:if>


            <g:if test="${SPKAsuransiInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="SPKAsuransi.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue--}%
                                %{--bean="${SPKAsuransiInstance}" field="createdBy"--}%
                                %{--url="${request.contextPath}/SPKAsuransi/updatefield" type="text"--}%
                                %{--title="Enter createdBy" onsuccess="reloadSPKAsuransiTable();" />--}%

                        <g:fieldValue bean="${SPKAsuransiInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

				<g:if test="${SPKAsuransiInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="SPKAsuransi.lastUpdated.label" default="Last Updated" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue--}%
								%{--bean="${SPKAsuransiInstance}" field="lastUpdated"--}%
								%{--url="${request.contextPath}/SPKAsuransi/updatefield" type="text"--}%
								%{--title="Enter lastUpdated" onsuccess="reloadSPKAsuransiTable();" />--}%

								<g:formatDate date="${SPKAsuransiInstance?.lastUpdated}" type="datetime" style="MEDIUM" format="dd-MM-yyyy HH:mm" />

						</span></td>

				</tr>
				</g:if>



            <g:if test="${SPKAsuransiInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="SPKAsuransi.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue--}%
                                %{--bean="${SPKAsuransiInstance}" field="updatedBy"--}%
                                %{--url="${request.contextPath}/SPKAsuransi/updatefield" type="text"--}%
                                %{--title="Enter updatedBy" onsuccess="reloadSPKAsuransiTable();" />--}%

                        <g:fieldValue bean="${SPKAsuransiInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${SPKAsuransiInstance?.id}"
					update="[success:'SPKAsuransi-form',failure:'SPKAsuransi-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteSPKAsuransi('${SPKAsuransiInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
