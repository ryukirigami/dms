<%@ page import="com.kombos.administrasi.GroupManPower; com.kombos.administrasi.ManPowerDetail; com.kombos.administrasi.ManPower; com.kombos.administrasi.CompanyDealer; com.kombos.administrasi.NamaManPower" %>
<g:javascript>

        $(function() {
            if($('#t015Alamat').val().length>0){
               $('#hitung').text(255 - $('#t015Alamat').val().length);
            }

            $('#t015Alamat').keyup(function() {
                var len = this.value.length;
                if (len >= 255) {
                    this.value = this.value.substring(0, 255);
                    len = 255;
                }
             //   alert("helllo");
               $('#hitung').text(255 - len);
            });

            if($('#t015Ket').val().length>0){
                $('#hitungKet').text(255 - $('#t015Ket').val().length);
            }

            $('#t015Ket').keyup(function() {
                var len = this.value.length;
                if (len >= 150) {
                    this.value = this.value.substring(0, 150);
                    len = 150;
                }
                //   alert("helllo");
                $('#hitungKet').text(150 - len);
            });
        });


</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: namaManPowerInstance, field: 'companyDealer', 'error')} required">
    <label class="control-label" for="manPowerDetail">
        <g:message code="namaManPower.companyDealer.label" default="Company Dealer" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="companyDealer" name="companyDealer.id" from="${CompanyDealer.createCriteria().list{eq("staDel","0");order("m011NamaWorkshop")}}" optionKey="id" required="" value="${namaManPowerInstance?.companyDealer?.id}" class="many-to-one"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: namaManPowerInstance, field: 't015IdManPower', 'error')} required">
    <label class="control-label" for="t015IdManPower">
        <g:message code="namaManPower.t015IdManPower.label" default="ID" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="t015IdManPower" maxlength="10" required="" value="${namaManPowerInstance?.t015IdManPower}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: namaManPowerInstance, field: 't015NamaLengkap', 'error')} required">
    <label class="control-label" for="t015NamaLengkap">
        <g:message code="namaManPower.t015NamaLengkap.label" default="Nama Lengkap" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="t015NamaLengkap" maxlength="50" required="" value="${namaManPowerInstance?.t015NamaLengkap}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: namaManPowerInstance, field: 't015NamaBoard', 'error')} required">
	<label class="control-label" for="t015NamaBoard">
		<g:message code="namaManPower.t015NamaBoard.label" default="Nama Board" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t015NamaBoard" maxlength="10" required="" value="${namaManPowerInstance?.t015NamaBoard}"/>
	</div>
</div>



<div class="control-group fieldcontain ${hasErrors(bean: namaManPowerInstance, field: 'userProfile', 'error')} required">
    <label class="control-label" for="userProfile">
        <g:message code="namaManPower.userProfile.label" default="Username" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="username" maxlength="50" required="" value="${namaManPowerInstance?.userProfile}"/>
    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: namaManPowerInstance, field: 'manPowerDetail', 'error')} required">
    <label class="control-label" for="manPowerDetail">
        <g:message code="namaManPower.manPowerDetail.label" default="Level Man Power" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="manPowerDetail" name="manPowerDetail.id" from="${ManPowerDetail.createCriteria().list {eq("staDel","0");order("m015LevelManPower")}}" optionKey="id" required="" value="${namaManPowerInstance?.manPowerDetail?.id}" optionValue="${{it.m015LevelManPower + " " + "(" + it.manPower.m014JabatanManPower + ")"}}" class="many-to-one"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: namaManPowerInstance, field: 't015TglBergabungTAM', 'error')} ">
    <label class="control-label" for="t015TglBergabungTAM">
        <g:message code="namaManPower.t015TglBergabungTAM.label" default="Tanggal Bergabung" />

    </label>
    <div class="controls">
        <ba:datePicker name="t015TglBergabungTAM" precision="day" value="${namaManPowerInstance?.t015TglBergabungTAM}" format="dd/mm/yyyy" required="true"  />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: namaManPowerInstance, field: 't015StaAktif', 'error')} required">
	<label class="control-label" for="t015StaAktif">
		<g:message code="namaManPower.t015StaAktif.label" default="Aktif" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:radioGroup name="t015StaAktif" values="['1','0']" labels="['Ya','Tidak']" value="${namaManPowerInstance?.t015StaAktif}" required="" >
            ${it.radio} ${it.label}
        </g:radioGroup>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: namaManPowerInstance, field: 't015TanggalLahir', 'error')} ">
    <label class="control-label" for="t015TanggalLahir">
        <g:message code="namaManPower.t015TanggalLahir.label" default="Tanggal Lahir" />

    </label>
    <div class="controls">
        <ba:datePicker name="t015TanggalLahir" precision="day" value="${namaManPowerInstance?.t015TanggalLahir}" format="dd/mm/yyyy" required="true"  />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: namaManPowerInstance, field: 't015Alamat', 'error')} ">
	<label class="control-label" for="t015Alamat">
		<g:message code="namaManPower.t015Alamat.label" default="Alamat" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textArea cols="5" rows="3" name="t015Alamat" id="t015Alamat" maxlength="255" required="" value="${namaManPowerInstance?.t015Alamat}"/>
    </br>
        <span id="hitung">255</span> Karakter Tersisa.
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: namaManPowerInstance, field: 't015NoTelp', 'error')} ">
	<label class="control-label" for="t015NoTelp">
		<g:message code="namaManPower.t015NoTelp.label" default="No Telp" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t015NoTelp" maxlength="20" value="${namaManPowerInstance?.t015NoTelp}"  onkeypress="return isNumberKey(event)"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: namaManPowerInstance, field: 't015StatusManPower', 'error')} required">
    <label class="control-label" for="t015StatusManPower">
        <g:message code="namaManPower.t015StatusManPower.label" default="Status Karyawan" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">

        <g:radioGroup name="t015StatusManPower" values="['0','1']" labels="['Outsource','Internal']" value="${namaManPowerInstance?.t015StatusManPower}" required="" >
            ${it.radio} ${it.label}
        </g:radioGroup>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: namaManPowerInstance, field: 't015StaTHS', 'error')} required">
    <label class="control-label" for="t015StaTHS">
        <g:message code="namaManPower.t015StaTHS.label" default="THS?" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">

        <g:radioGroup name="t015StaTHS" values="['0','1']" labels="['Ya','Tidak']" value="${namaManPowerInstance?.t015StaTHS}" required="" >
            ${it.radio} ${it.label}
        </g:radioGroup>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: namaManPowerInstance, field: 't015Ket', 'error')} ">
	<label class="control-label" for="t015Ket">
		<g:message code="namaManPower.t015Ket.label" default="Keterangan" />
		
	</label>
	<div class="controls">
	<g:textArea cols="5" rows="3" name="t015Ket" id="t015Ket" maxlength="150" value="${namaManPowerInstance?.t015Ket}"/>
    </br>
        <span id="hitungKet">255</span> Karakter Tersisa.
    </div>
</div>


<div class="control-group">
    <label class="control-label" for="namaPengganti">
        <g:message code="namaManPower.namaPengganti.label" default="Nama Pengganti" />

    </label>
    <div class="controls">
        <g:select optionKey="id" optionValue="t015NamaLengkap"
                  name="listManPower" from="${com.kombos.administrasi.NamaManPower.list()}"/>
        <g:field type="button" onclick="addManPowerPengganti(listManPower.value,${namaManPowerInstance?.id});" class="btn btn-primary create" name="save" id="save" value="${message(code: 'default.button.upload.label', default: 'Tambah')}"/>

    </div>
</div>
<div class="control-group">
    <label class="control-label" for="tabelPengganti">
        <g:message code="namaManPower.namaPengganti.label" default="" />

    </label>
        <div class="controls" id="namaManPowerPengganti-form">
</div>

<g:javascript>
    document.getElementById('t015IdManPower').focus();
</g:javascript>



