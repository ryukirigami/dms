
<%@ page import="com.kombos.administrasi.NamaManPower" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="namaManPower_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="namaManPower.t015NamaLengkap.label" default="Company Dealer" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="namaManPower.t015NamaLengkap.label" default="Nama Lengkap" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="namaManPower.t015NamaBoard.label" default="Nama Board" /></div>
            </th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="namaManPower.manPowerDetail.label" default="Level Man Power" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="namaManPower.t015TglBergabungTAM.label" default="Tgl Bergabung" /></div>
            </th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="namaManPower.t015StaAktif.label" default="Status Aktif" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="namaManPower.t015TanggalLahir.label" default="Tanggal Lahir" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="namaManPower.t015Alamat.label" default="Alamat" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="namaManPower.t015NoTelp.label" default="No Telp" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="namaManPower.t015StatusManPower.label" default="Status Karyawan" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="namaManPower.t015Ket.label" default="Keterangan" /></div>
            </th>


		</tr>
		<tr>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_companyDealer" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t015NamaLengkap" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t015NamaLengkap" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t015NamaBoard" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t015NamaBoard" class="search_init" />
                </div>
            </th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_manPowerDetail" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<select name="manPowerDetailSelect" id="manPowerDetailSelect" style="width:100%" onchange="reloadNamaManPowerTable();"/>

                </div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t015TglBergabungTAM" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
                    <input type="hidden" name="search_t015TglBergabungTAM" value="date.struct">
                    <input type="hidden" name="search_t015TglBergabungTAM_day" id="search_t015TglBergabungTAM_day" value="">
                    <input type="hidden" name="search_t015TglBergabungTAM_month" id="search_t015TglBergabungTAM_month" value="">
                    <input type="hidden" name="search_t015TglBergabungTAM_year" id="search_t015TglBergabungTAM_year" value="">
                    <input type="text" data-date-format="dd/mm/yyyy" name="search_t015TglBergabungTAM_dp" value="" id="search_t015TglBergabungTAM" class="search_init">
                </div>
            </th>

	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t015StaAktif" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<select id="search_t015StaAktif" name="search_t015StaAktif" style="width:100%"  onchange="reloadNamaManPowerTable();">
                        <option value=""></option>
                        <option value="1">Ya</option>
                        <option value="0">Tidak</option>
                    </select>
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t015TanggalLahir" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
                    <input type="hidden" name="search_t015TanggalLahir" value="date.struct">
                    <input type="hidden" name="search_t015TanggalLahir_day" id="search_t015TanggalLahir_day" value="">
                    <input type="hidden" name="search_t015TanggalLahir_month" id="search_t015TanggalLahir_month" value="">
                    <input type="hidden" name="search_t015TanggalLahir_year" id="search_t015TanggalLahir_year" value="">
                    <input type="text" data-date-format="dd/mm/yyyy" name="search_t015TanggalLahir_dp" value="" id="search_t015TanggalLahir" class="search_init">
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t015Alamat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t015Alamat" class="search_init" />
                </div>
            </th>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_t015NoTelp" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t015NoTelp" class="search_init"  onkeypress="return isNumberKey(event)" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
				<div id="filter_t015StatusManPower" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<select name="search_t015StatusManPower" id="search_t015StatusManPower" style="width:100%" onchange="reloadNamaManPowerTable();">
                        <option value=""></option>
                        <option value="0">Outsource</option>
                        <option value="1">Internal</option>
                    </select>
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t015Ket" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t015Ket" class="search_init" />
                </div>
            </th>
	

		</tr>
	</thead>
</table>

<g:javascript>
var namaManPowerTable;
var reloadNamaManPowerTable;


$(function(){

    loadManPowerDetail();

	reloadNamaManPowerTable = function() {
		namaManPowerTable.fnDraw();

	}

	var recordsNamaManPowerPerPage = [];
    var anNamaManPowerSelected;
    var jmlRecNamaManPowerPerPage=0;
    var id;
    
	$('#search_t015TanggalLahir').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t015TanggalLahir_day').val(newDate.getDate());
			$('#search_t015TanggalLahir_month').val(newDate.getMonth()+1);
			$('#search_t015TanggalLahir_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			namaManPowerTable.fnDraw();	
	});

	

	$('#search_t015TglBergabungTAM').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t015TglBergabungTAM_day').val(newDate.getDate());
			$('#search_t015TglBergabungTAM_month').val(newDate.getMonth()+1);
			$('#search_t015TglBergabungTAM_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			namaManPowerTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	namaManPowerTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	namaManPowerTable = $('#namaManPower_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsNamaManPower = $("#namaManPower_datatables tbody .row-select");
            var jmlNamaManPowerCek = 0;
            var nRow;
            var idRec;
            rsNamaManPower.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsNamaManPowerPerPage[idRec]=="1"){
                    jmlNamaManPowerCek = jmlNamaManPowerCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsNamaManPowerPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecNamaManPowerPerPage = rsNamaManPower.length;
            if(jmlNamaManPowerCek==jmlRecNamaManPowerPerPage && jmlRecNamaManPowerPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
	"sName": "companyDealer",
	"mDataProp": "companyDealer",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "t015NamaLengkap",
	"mDataProp": "t015NamaLengkap",
	"aTargets": [1],
 	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t015NamaBoard",
	"mDataProp": "t015NamaBoard",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

//{
//	"sName": "manPower",
//	"mDataProp": "manPower",
//	"aTargets": [1],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,

//{
//	"sName": "companyDealer",
//	"mDataProp": "companyDealer",
//	"aTargets": [0],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"300px",
//	"bVisible": false
//}
//
//,

{
	"sName": "manPowerDetail",
	"mDataProp": "manPowerDetail",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t015TglBergabungTAM",
	"mDataProp": "t015TglBergabungTAM",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t015StaAktif",
	"mDataProp": "t015StaAktif",
	"aTargets": [5],
	"mRender": function ( data, type, row ) {

            if(data=="1"){
                  return "Ya";
             }else
             {
                  return "Tidak";
             }

        },
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t015TanggalLahir",
	"mDataProp": "t015TanggalLahir",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "t015Alamat",
	"mDataProp": "t015Alamat",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t015NoTelp",
	"mDataProp": "t015NoTelp",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "t015StatusManPower",
	"mDataProp": "t015StatusManPower",
	"aTargets": [9],
	"mRender": function ( data, type, row ) {

            if(data=="0"){
                  return "Outsource";
             }else
             {
                  return "Internal";
             }

        },
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "t015Ket",
	"mDataProp": "t015Ket",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}



],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var manPower = $('#filter_manPower input').val();
						if(manPower){
							aoData.push(
									{"name": 'sCriteria_manPower', "value": manPower}
							);
						}
	
						var manPowerDetail = $('#manPowerDetailSelect').val();
						if(manPowerDetail){
							aoData.push(
									{"name": 'sCriteria_manPowerDetail', "value": manPowerDetail}
							);
						}
	
						var t015NamaBoard = $('#filter_t015NamaBoard input').val();
						if(t015NamaBoard){
							aoData.push(
									{"name": 'sCriteria_t015NamaBoard', "value": t015NamaBoard}
							);
						}
	
						var t015NamaLengkap = $('#filter_t015NamaLengkap input').val();
						if(t015NamaLengkap){
							aoData.push(
									{"name": 'sCriteria_t015NamaLengkap', "value": t015NamaLengkap}
							);
						}
	
						var t015StaAktif = $('#search_t015StaAktif').val();
						if(t015StaAktif){
							aoData.push(
									{"name": 'sCriteria_t015StaAktif', "value": t015StaAktif}
							);
						}
	
						var t015StatusManPower = $('#search_t015StatusManPower').val();
						if(t015StatusManPower){
							aoData.push(
									{"name": 'sCriteria_t015StatusManPower', "value": t015StatusManPower}
							);
						}
	
						var userProfile = $('#filter_userProfile input').val();
						if(userProfile){
							aoData.push(
									{"name": 'sCriteria_userProfile', "value": userProfile}
							);
						}

						var t015TanggalLahir = $('#search_t015TanggalLahir').val();
						var t015TanggalLahirDay = $('#search_t015TanggalLahir_day').val();
						var t015TanggalLahirMonth = $('#search_t015TanggalLahir_month').val();
						var t015TanggalLahirYear = $('#search_t015TanggalLahir_year').val();
						
						if(t015TanggalLahir){
							aoData.push(
									{"name": 'sCriteria_t015TanggalLahir', "value": "date.struct"},
									{"name": 'sCriteria_t015TanggalLahir_dp', "value": t015TanggalLahir},
									{"name": 'sCriteria_t015TanggalLahir_day', "value": t015TanggalLahirDay},
									{"name": 'sCriteria_t015TanggalLahir_month', "value": t015TanggalLahirMonth},
									{"name": 'sCriteria_t015TanggalLahir_year', "value": t015TanggalLahirYear}
							);
						}
	
						var t015Alamat = $('#filter_t015Alamat input').val();
						if(t015Alamat){
							aoData.push(
									{"name": 'sCriteria_t015Alamat', "value": t015Alamat}
							);
						}
	
						var t015NoTelp = $('#filter_t015NoTelp input').val();
						if(t015NoTelp){
							aoData.push(
									{"name": 'sCriteria_t015NoTelp', "value": t015NoTelp}
							);
						}

						var t015TglBergabungTAM = $('#search_t015TglBergabungTAM').val();
						var t015TglBergabungTAMDay = $('#search_t015TglBergabungTAM_day').val();
						var t015TglBergabungTAMMonth = $('#search_t015TglBergabungTAM_month').val();
						var t015TglBergabungTAMYear = $('#search_t015TglBergabungTAM_year').val();
						
						if(t015TglBergabungTAM){
							aoData.push(
									{"name": 'sCriteria_t015TglBergabungTAM', "value": "date.struct"},
									{"name": 'sCriteria_t015TglBergabungTAM_dp', "value": t015TglBergabungTAM},
									{"name": 'sCriteria_t015TglBergabungTAM_day', "value": t015TglBergabungTAMDay},
									{"name": 'sCriteria_t015TglBergabungTAM_month', "value": t015TglBergabungTAMMonth},
									{"name": 'sCriteria_t015TglBergabungTAM_year', "value": t015TglBergabungTAMYear}
							);
						}
	
						var t015Ket = $('#filter_t015Ket input').val();
						if(t015Ket){
							aoData.push(
									{"name": 'sCriteria_t015Ket', "value": t015Ket}
							);
						}
	
						var t015xNamaUser = $('#filter_t015xNamaUser input').val();
						if(t015xNamaUser){
							aoData.push(
									{"name": 'sCriteria_t015xNamaUser', "value": t015xNamaUser}
							);
						}
	
						var t015xNamaDivisi = $('#filter_t015xNamaDivisi input').val();
						if(t015xNamaDivisi){
							aoData.push(
									{"name": 'sCriteria_t015xNamaDivisi', "value": t015xNamaDivisi}
							);
						}
	
						var staDel = $('#filter_companyDealer input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": staDel}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}



						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
	$('.select-all').click(function(e) {

        $("#namaManPower_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsNamaManPowerPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsNamaManPowerPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#namaManPower_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsNamaManPowerPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anNamaManPowerSelected = namaManPowerTable.$('tr.row_selected');
            if(jmlRecNamaManPowerPerPage == anNamaManPowerSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsNamaManPowerPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
