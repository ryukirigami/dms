
<%@ page import="com.kombos.administrasi.NamaManPower" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'namaManPower.label', default: 'Biodata Man Power')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	var loadManPowerPengganti;
	var deleteManPowerPengganti;
	var isNumberKey;
	var loadManPowerDetail;

	$(function(){

	loadManPowerDetail = function () {
        jQuery.getJSON('${request.contextPath}/namaManPower/loadLevelManPower/', function (data) {
                $('#manPowerDetailSelect').empty();
                $('#manPowerDetailSelect').append("<option value=''> </option>");

                if (data) {
                    jQuery.each(data, function (index, value) {
                        $('#manPowerDetailSelect').append("<option value='" + value.id + "'>" + value.namaLevel + "</option>");
                    });
                }

            });

        };


    isNumberKey = function(evt)
            {
                var charCode = (evt.which) ? evt.which : evt.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;

                return true;
            }
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/namaManPower/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/namaManPower/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#namaManPower-form').empty();
    	$('#namaManPower-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#namaManPower-table").hasClass("span12")){
   			$("#namaManPower-table").toggleClass("span12 span5");
        }
        $("#namaManPower-form").css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
   		if($("#namaManPower-table").hasClass("span5")){
   			$("#namaManPower-table").toggleClass("span5 span12");
   		}
        $("#namaManPower-form").css("display","none");
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#namaManPower-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/namaManPower/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadNamaManPowerTable();
    		}
		});
		
   	}

    addManPowerPengganti =  function(id,idMan) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'GET', url:'${request.contextPath}/namaManPower/addManPowerPengganti/',
   		    data : {id : id, idManPower : idMan},
   			success:function(data,textStatus){
   				loadFormManPower(data, textStatus);
   			//	$("#listManPower").remove("<select value="+id+"></select>");
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };

    loadManPowerPengganti =  function(idManPower) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'GET', url:'${request.contextPath}/namaManPower/loadManPowerPengganti/',
   		    data : {idManPower : idManPower},
   			success:function(data,textStatus){
   				loadFormManPower(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };

     loadFormManPower = function(data, textStatus){
		$('#namaManPowerPengganti-form').empty();
    	$('#namaManPowerPengganti-form').append(data);
   	}

   	deleteManPowerPengganti =  function(id,idManPower) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'GET', url:'${request.contextPath}/namaManPower/deleteManPowerPengganti/',
   		   data : {id : id, idManPower : idManPower},
   			success:function(data,textStatus){
   				loadFormManPower(data, textStatus);
   			//	$("#listManPower").append("<select value="+id+">"+data.namaManPower+"</select>");
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };



});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="namaManPower-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
		</div>
		<div class="span7" id="namaManPower-form" style="display: none;"></div>
	</div>
</body>
</html>
