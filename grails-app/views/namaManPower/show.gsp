

<%@ page import="com.kombos.administrasi.NamaManPower" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'namaManPower.label', default: 'Biodata Man Power')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteNamaManPower;

$(function(){ 
	deleteNamaManPower=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/namaManPower/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadNamaManPowerTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-namaManPower" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="namaManPower"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${namaManPowerInstance?.companyDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="companyDealer-label" class="property-label"><g:message
					code="namaManPower.companyDealer.label" default="Company Dealer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="companyDealer-label">
						%{--<ba:editableValue
								bean="${namaManPowerInstance}" field="companyDealer"
								url="${request.contextPath}/NamaManPower/updatefield" type="text"
								title="Enter companyDealer" onsuccess="reloadNamaManPowerTable();" />--}%
							
								${namaManPowerInstance?.companyDealer?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${namaManPowerInstance?.manPowerDetail}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="manPowerDetail-label" class="property-label"><g:message
					code="namaManPower.manPowerDetail.label" default="Level" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="manPowerDetail-label">
						%{--<ba:editableValue
								bean="${namaManPowerInstance}" field="manPowerDetail"
								url="${request.contextPath}/NamaManPower/updatefield" type="text"
								title="Enter manPowerDetail" onsuccess="reloadNamaManPowerTable();" />--}%
							
								${namaManPowerInstance?.manPowerDetail?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${namaManPowerInstance?.t015NamaBoard}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t015NamaBoard-label" class="property-label"><g:message
					code="namaManPower.t015NamaBoard.label" default="Nama Board" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t015NamaBoard-label">
						%{--<ba:editableValue
								bean="${namaManPowerInstance}" field="t015NamaBoard"
								url="${request.contextPath}/NamaManPower/updatefield" type="text"
								title="Enter t015NamaBoard" onsuccess="reloadNamaManPowerTable();" />--}%
							
								<g:fieldValue bean="${namaManPowerInstance}" field="t015NamaBoard"/>
							
						</span></td>
					
				</tr>
				</g:if>


            <g:if test="${namaManPowerInstance?.userProfile}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t015NamaBoard-label" class="property-label"><g:message
                                code="Username.userProfile.label" default="Username" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t015NamaBoard-label">
                        %{--<ba:editableValue
                                bean="${namaManPowerInstance}" field="t015NamaBoard"
                                url="${request.contextPath}/NamaManPower/updatefield" type="text"
                                title="Enter t015NamaBoard" onsuccess="reloadNamaManPowerTable();" />--}%

                        <g:fieldValue bean="${namaManPowerInstance}" field="userProfile"/>

                    </span></td>

                </tr>
            </g:if>



            <g:if test="${namaManPowerInstance?.t015NamaLengkap}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t015NamaLengkap-label" class="property-label"><g:message
					code="namaManPower.t015NamaLengkap.label" default="Nama Lengkap" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t015NamaLengkap-label">
						%{--<ba:editableValue
								bean="${namaManPowerInstance}" field="t015NamaLengkap"
								url="${request.contextPath}/NamaManPower/updatefield" type="text"
								title="Enter t015NamaLengkap" onsuccess="reloadNamaManPowerTable();" />--}%
							
								<g:fieldValue bean="${namaManPowerInstance}" field="t015NamaLengkap"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${namaManPowerInstance?.t015StaAktif}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t015StaAktif-label" class="property-label"><g:message
					code="namaManPower.t015StaAktif.label" default="Status Aktif" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t015StaAktif-label">
						%{--<ba:editableValue
								bean="${namaManPowerInstance}" field="t015StaAktif"
								url="${request.contextPath}/NamaManPower/updatefield" type="text"
								title="Enter t015StaAktif" onsuccess="reloadNamaManPowerTable();" />--}%
							
					   <g:if test="${namaManPowerInstance.t015StaAktif == '1'}">
                            <g:message code="namaManPowerInstance.t015StaAktif.label.Aktif" default="Aktif" />
                        </g:if>
                        <g:else>
                            <g:message code="namaManPowerInstance.t015StaAktif.label.Tidak" default="Tidak" />
                        </g:else>
                        </span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${namaManPowerInstance?.t015StatusManPower}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t015StatusManPower-label" class="property-label"><g:message
					code="namaManPower.t015StatusManPower.label" default="Status Karyawan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t015StatusManPower-label">
						%{--<ba:editableValue
								bean="${namaManPowerInstance}" field="t015StatusManPower"
								url="${request.contextPath}/NamaManPower/updatefield" type="text"
								title="Enter t015StatusManPower" onsuccess="reloadNamaManPowerTable();" />--}%
							

                        <g:if test="${namaManPowerInstance.t015StatusManPower == '1'}">
                            <g:message code="namaManPowerInstance.t015StatusManPower.label.internal" default="Internal" />
                        </g:if>
                        <g:else>
                            <g:message code="namaManPowerInstance.t015StatusManPower.label.outsource" default="Outsource" />
                        </g:else>
						</span></td>
					
				</tr>
				</g:if>
			
                <g:if test="${namaManPowerInstance?.t015StaTHS}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t015StaTHS-label" class="property-label"><g:message
					code="namaManPower.t015StatusManPower.label" default="THS" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="t015StaTHS-label">
						%{--<ba:editableValue
								bean="${namaManPowerInstance}" field="t015StatusManPower"
								url="${request.contextPath}/NamaManPower/updatefield" type="text"
								title="Enter t015StatusManPower" onsuccess="reloadNamaManPowerTable();" />--}%


                        <g:if test="${namaManPowerInstance.t015StaTHS == '1'}">
                            <g:message code="namaManPowerInstance.t015StaTHS.tidak.label" default="Tidak" />
                        </g:if>
                        <g:else>
                            <g:message code="namaManPowerInstance.t015StaTHS.ya.label" default="Ya" />
                        </g:else>
						</span></td>

				</tr>
				</g:if>


				<g:if test="${namaManPowerInstance?.t015TanggalLahir}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t015TanggalLahir-label" class="property-label"><g:message
					code="namaManPower.t015TanggalLahir.label" default="Tanggal Lahir" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t015TanggalLahir-label">
						%{--<ba:editableValue
								bean="${namaManPowerInstance}" field="t015TanggalLahir"
								url="${request.contextPath}/NamaManPower/updatefield" type="text"
								title="Enter t015TanggalLahir" onsuccess="reloadNamaManPowerTable();" />--}%
							
								<g:formatDate date="${namaManPowerInstance?.t015TanggalLahir}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${namaManPowerInstance?.t015Alamat}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t015Alamat-label" class="property-label"><g:message
					code="namaManPower.t015Alamat.label" default="Alamat" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t015Alamat-label">
						%{--<ba:editableValue
								bean="${namaManPowerInstance}" field="t015Alamat"
								url="${request.contextPath}/NamaManPower/updatefield" type="text"
								title="Enter t015Alamat" onsuccess="reloadNamaManPowerTable();" />--}%
							
								<g:fieldValue bean="${namaManPowerInstance}" field="t015Alamat"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${namaManPowerInstance?.t015NoTelp}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t015NoTelp-label" class="property-label"><g:message
					code="namaManPower.t015NoTelp.label" default="No Telp" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t015NoTelp-label">
						%{--<ba:editableValue
								bean="${namaManPowerInstance}" field="t015NoTelp"
								url="${request.contextPath}/NamaManPower/updatefield" type="text"
								title="Enter t015NoTelp" onsuccess="reloadNamaManPowerTable();" />--}%
							
								<g:fieldValue bean="${namaManPowerInstance}" field="t015NoTelp"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${namaManPowerInstance?.t015TglBergabungTAM}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t015TglBergabungTAM-label" class="property-label"><g:message
					code="namaManPower.t015TglBergabungTAM.label" default="Tgl Bergabung" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t015TglBergabungTAM-label">
						%{--<ba:editableValue
								bean="${namaManPowerInstance}" field="t015TglBergabungTAM"
								url="${request.contextPath}/NamaManPower/updatefield" type="text"
								title="Enter t015TglBergabungTAM" onsuccess="reloadNamaManPowerTable();" />--}%
							
								<g:formatDate date="${namaManPowerInstance?.t015TglBergabungTAM}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${namaManPowerInstance?.t015Ket}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t015Ket-label" class="property-label"><g:message
					code="namaManPower.t015Ket.label" default="Keterangan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t015Ket-label">
						%{--<ba:editableValue
								bean="${namaManPowerInstance}" field="t015Ket"
								url="${request.contextPath}/NamaManPower/updatefield" type="text"
								title="Enter t015Ket" onsuccess="reloadNamaManPowerTable();" />--}%
							
								<g:fieldValue bean="${namaManPowerInstance}" field="t015Ket"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${namaManPowerInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="namaManPower.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${namaManPowerInstance}" field="lastUpdProcess"
								url="${request.contextPath}/NamaManPower/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadNamaManPowerTable();" />--}%
							
								<g:fieldValue bean="${namaManPowerInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${namaManPowerInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="namaManPower.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${namaManPowerInstance}" field="dateCreated"
								url="${request.contextPath}/NamaManPower/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadNamaManPowerTable();" />--}%
							
								<g:formatDate date="${namaManPowerInstance?.dateCreated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>


            <g:if test="${namaManPowerInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="namaManPower.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${namaManPowerInstance}" field="createdBy"
                                url="${request.contextPath}/NamaManPower/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadNamaManPowerTable();" />--}%

                        <g:fieldValue bean="${namaManPowerInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${namaManPowerInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="namaManPower.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${namaManPowerInstance}" field="lastUpdated"
								url="${request.contextPath}/NamaManPower/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadNamaManPowerTable();" />--}%
							
								<g:formatDate date="${namaManPowerInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
            </g:if>

            <g:if test="${namaManPowerInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="namaManPower.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${namaManPowerInstance}" field="updatedBy"
                                url="${request.contextPath}/NamaManPower/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadNamaManPowerTable();" />--}%

                        <g:fieldValue bean="${namaManPowerInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${namaManPowerInstance?.id}"
					update="[success:'namaManPower-form',failure:'namaManPower-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteNamaManPower('${namaManPowerInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
