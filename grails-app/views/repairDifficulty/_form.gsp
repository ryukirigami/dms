<%@ page import="com.kombos.administrasi.RepairDifficulty" %>
<g:javascript>
    $(document).ready(function() {
        if($('#m042Keterangan').val().length>0){
            $('#hitungmundur').text(255 - $('#m042Keterangan').val().length);
        }
        $('#m042Keterangan').keyup(function() {
            var len = this.value.length;
            if (len >= 255) {
                this.value = this.value.substring(0, 255);
                len = 255;
            }
            $('#hitungmundur').text(255 - len);
        });
    });
</g:javascript>


<div class="control-group fieldcontain ${hasErrors(bean: repairDifficultyInstance, field: 'm042Tipe', 'error')} required">
	<label class="control-label" for="m042Tipe">
		<g:message code="repairDifficulty.m042Tipe.label" default="Tipe Difficulty" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m042Tipe" id="m042Tipe" maxlength="50" required="" value="${repairDifficultyInstance?.m042Tipe}"/>
	</div>
</div>
<g:javascript>
    document.getElementById('m042Tipe').focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: repairDifficultyInstance, field: 'm042Keterangan', 'error')} required">
	<label class="control-label" for="m042Keterangan">
		<g:message code="repairDifficulty.m042Keterangan.label" default="Keterangan" />
		<span class="required-indicator">*</span>
	</label>
    <div class="controls">
        <g:textArea name="m042Keterangan" id="m042Keterangan" required="" value="${repairDifficultyInstance?.m042Keterangan}"/>
        <br/>
        <span id="hitungmundur">255</span> Karakter Tersisa.
    </div>
	%{--<div class="controls">--}%
	%{--<g:textArea maxlength="255" rows="5" cols="50" name="m042Keterangan" required="" value="${repairDifficultyInstance?.m042Keterangan}"/>--}%
	%{--</div>--}%
</div>

%{--<div class="control-group fieldcontain ${hasErrors(bean: repairDifficultyInstance, field: 'm042ID', 'error')} required">--}%
	%{--<label class="control-label" for="m042ID">--}%
		%{--<g:message code="repairDifficulty.m042ID.label" default="M042 ID" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:field name="m042ID" type="number" value="${repairDifficultyInstance.m042ID}" required=""/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: repairDifficultyInstance, field: 'createdBy', 'error')} ">--}%
	%{--<label class="control-label" for="createdBy">--}%
		%{--<g:message code="repairDifficulty.createdBy.label" default="Created By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="createdBy" value="${repairDifficultyInstance?.createdBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: repairDifficultyInstance, field: 'updatedBy', 'error')} ">--}%
	%{--<label class="control-label" for="updatedBy">--}%
		%{--<g:message code="repairDifficulty.updatedBy.label" default="Updated By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="updatedBy" value="${repairDifficultyInstance?.updatedBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: repairDifficultyInstance, field: 'lastUpdProcess', 'error')} ">--}%
	%{--<label class="control-label" for="lastUpdProcess">--}%
		%{--<g:message code="repairDifficulty.lastUpdProcess.label" default="Last Upd Process" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="lastUpdProcess" value="${repairDifficultyInstance?.lastUpdProcess}"/>--}%
	%{--</div>--}%
%{--</div>--}%

