

<%@ page import="com.kombos.administrasi.RepairDifficulty" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'repairDifficulty.label', default: 'Repair Difficulty')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteRepairDifficulty;

$(function(){ 
	deleteRepairDifficulty=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/repairDifficulty/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadRepairDifficultyTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-repairDifficulty" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="repairDifficulty"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${repairDifficultyInstance?.m042Tipe}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m042Tipe-label" class="property-label"><g:message
					code="repairDifficulty.m042Tipe.label" default="Tipe Difficulty" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m042Tipe-label">
						%{--<ba:editableValue
								bean="${repairDifficultyInstance}" field="m042Tipe"
								url="${request.contextPath}/RepairDifficulty/updatefield" type="text"
								title="Enter m042Tipe" onsuccess="reloadRepairDifficultyTable();" />--}%
							
								<g:fieldValue bean="${repairDifficultyInstance}" field="m042Tipe"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${repairDifficultyInstance?.m042Keterangan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m042Keterangan-label" class="property-label"><g:message
					code="repairDifficulty.m042Keterangan.label" default="Keterangan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m042Keterangan-label">
						%{--<ba:editableValue
								bean="${repairDifficultyInstance}" field="m042Keterangan"
								url="${request.contextPath}/RepairDifficulty/updatefield" type="text"
								title="Enter m042Keterangan" onsuccess="reloadRepairDifficultyTable();" />--}%
							
								<g:fieldValue bean="${repairDifficultyInstance}" field="m042Keterangan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${repairDifficultyInstance?.m042ID}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="m042ID-label" class="property-label"><g:message--}%
					%{--code="repairDifficulty.m042ID.label" default="M042 ID" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="m042ID-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${repairDifficultyInstance}" field="m042ID"--}%
								%{--url="${request.contextPath}/RepairDifficulty/updatefield" type="text"--}%
								%{--title="Enter m042ID" onsuccess="reloadRepairDifficultyTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${repairDifficultyInstance}" field="m042ID"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			
				<g:if test="${repairDifficultyInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="repairDifficulty.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${repairDifficultyInstance}" field="lastUpdProcess"
								url="${request.contextPath}/RepairDifficulty/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadRepairDifficultyTable();" />--}%
							
								<g:fieldValue bean="${repairDifficultyInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${repairDifficultyInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="repairDifficulty.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${repairDifficultyInstance}" field="dateCreated"
								url="${request.contextPath}/RepairDifficulty/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadRepairDifficultyTable();" />--}%
							
								<g:formatDate date="${repairDifficultyInstance?.dateCreated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${repairDifficultyInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="repairDifficulty.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${repairDifficultyInstance}" field="createdBy"
                                url="${request.contextPath}/RepairDifficulty/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadRepairDifficultyTable();" />--}%

                        <g:fieldValue bean="${repairDifficultyInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${repairDifficultyInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="repairDifficulty.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${repairDifficultyInstance}" field="lastUpdated"
								url="${request.contextPath}/RepairDifficulty/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadRepairDifficultyTable();" />--}%
							
								<g:formatDate date="${repairDifficultyInstance?.lastUpdated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${repairDifficultyInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="repairDifficulty.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${repairDifficultyInstance}" field="updatedBy"
                                url="${request.contextPath}/RepairDifficulty/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadRepairDifficultyTable();" />--}%

                        <g:fieldValue bean="${repairDifficultyInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${repairDifficultyInstance?.id}"
					update="[success:'repairDifficulty-form',failure:'repairDifficulty-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteRepairDifficulty('${repairDifficultyInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
