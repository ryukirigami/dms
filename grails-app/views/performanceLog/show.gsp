

<%@ page import="com.kombos.administrasi.PerformanceLog" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'performanceLog.label', default: 'Performance Log')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deletePerformanceLog;

$(function(){ 
	deletePerformanceLog=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/performanceLog/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadPerformanceLogTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-performanceLog" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="performanceLog"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${performanceLogInstance?.m778Tanggal}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m778Tanggal-label" class="property-label"><g:message
					code="performanceLog.m778Tanggal.label" default="Tanggal" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m778Tanggal-label">
						%{--<ba:editableValue
								bean="${performanceLogInstance}" field="m778Tanggal"
								url="${request.contextPath}/PerformanceLog/updatefield" type="text"
								title="Enter m778Tanggal" onsuccess="reloadPerformanceLogTable();" />--}%
							
								<g:formatDate date="${performanceLogInstance?.m778Tanggal}" />
							
						</span></td>
					
				</tr>
				</g:if>


            <g:if test="${performanceLogInstance?.m778TglJamClick}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m778TglJamClick-label" class="property-label"><g:message
                                code="performanceLog.m778TglJamClick.label" default="Jam" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m778TglJamClick-label">
                        %{--<ba:editableValue
                                bean="${performanceLogInstance}" field="m778TglJamClick"
                                url="${request.contextPath}/PerformanceLog/updatefield" type="text"
                                title="Enter m778TglJamClick" onsuccess="reloadPerformanceLogTable();" />--}%

                        <g:formatDate format="HH:mm:ss" date="${performanceLogInstance?.m778TglJamClick}" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${performanceLogInstance?.m778DurasiDetik}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m778DurasiDetik-label" class="property-label"><g:message
                                code="performanceLog.m778DurasiDetik.label" default="Durasi" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m778DurasiDetik-label">
                        %{--<ba:editableValue
                                bean="${performanceLogInstance}" field="m778DurasiDetik"
                                url="${request.contextPath}/PerformanceLog/updatefield" type="text"
                                title="Enter m778DurasiDetik" onsuccess="reloadPerformanceLogTable();" />--}%

                        <g:fieldValue bean="${performanceLogInstance}" field="m778DurasiDetik"/>

                    </span></td>

                </tr>
            </g:if>


            <g:if test="${performanceLogInstance?.m778NamaActivity}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m778NamaActivity-label" class="property-label"><g:message
                                code="performanceLog.m778NamaActivity.label" default="Aktivitas" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m778NamaActivity-label">
                        %{--<ba:editableValue
                                bean="${performanceLogInstance}" field="m778NamaActivity"
                                url="${request.contextPath}/PerformanceLog/updatefield" type="text"
                                title="Enter m778NamaActivity" onsuccess="reloadPerformanceLogTable();" />--}%

                        <g:fieldValue bean="${performanceLogInstance}" field="m778NamaActivity"/>

                    </span></td>

                </tr>
            </g:if>

            %{--<g:if test="${performanceLogInstance?.m778Id}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="m778Id-label" class="property-label"><g:message--}%
					%{--code="performanceLog.m778Id.label" default="M778 Id" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="m778Id-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${performanceLogInstance}" field="m778Id"--}%
								%{--url="${request.contextPath}/PerformanceLog/updatefield" type="text"--}%
								%{--title="Enter m778Id" onsuccess="reloadPerformanceLogTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${performanceLogInstance}" field="m778Id"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${performanceLogInstance?.userProfile}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="userProfile-label" class="property-label"><g:message--}%
					%{--code="performanceLog.userProfile.label" default="User Profile" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="userProfile-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${performanceLogInstance}" field="userProfile"--}%
								%{--url="${request.contextPath}/PerformanceLog/updatefield" type="text"--}%
								%{--title="Enter userProfile" onsuccess="reloadPerformanceLogTable();" />--}%
							%{----}%
								%{--<g:link controller="userProfile" action="show" id="${performanceLogInstance?.userProfile?.id}">${performanceLogInstance?.userProfile?.encodeAsHTML()}</g:link>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%

				%{--<g:if test="${performanceLogInstance?.m778TglJamTampil}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="m778TglJamTampil-label" class="property-label"><g:message--}%
					%{--code="performanceLog.m778TglJamTampil.label" default="M778 Tgl Jam Tampil" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="m778TglJamTampil-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${performanceLogInstance}" field="m778TglJamTampil"--}%
								%{--url="${request.contextPath}/PerformanceLog/updatefield" type="text"--}%
								%{--title="Enter m778TglJamTampil" onsuccess="reloadPerformanceLogTable();" />--}%
							%{----}%
								%{--<g:formatDate date="${performanceLogInstance?.m778TglJamTampil}" />--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{----}%
			%{----}%
				%{--<g:if test="${performanceLogInstance?.m778xNamaUser}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="m778xNamaUser-label" class="property-label"><g:message--}%
					%{--code="performanceLog.m778xNamaUser.label" default="M778x Nama User" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="m778xNamaUser-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${performanceLogInstance}" field="m778xNamaUser"--}%
								%{--url="${request.contextPath}/PerformanceLog/updatefield" type="text"--}%
								%{--title="Enter m778xNamaUser" onsuccess="reloadPerformanceLogTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${performanceLogInstance}" field="m778xNamaUser"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${performanceLogInstance?.m778xNamaDivisi}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="m778xNamaDivisi-label" class="property-label"><g:message--}%
					%{--code="performanceLog.m778xNamaDivisi.label" default="M778x Nama Divisi" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="m778xNamaDivisi-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${performanceLogInstance}" field="m778xNamaDivisi"--}%
								%{--url="${request.contextPath}/PerformanceLog/updatefield" type="text"--}%
								%{--title="Enter m778xNamaDivisi" onsuccess="reloadPerformanceLogTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${performanceLogInstance}" field="m778xNamaDivisi"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${performanceLogInstance?.staDel}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="staDel-label" class="property-label"><g:message--}%
					%{--code="performanceLog.staDel.label" default="Sta Del" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="staDel-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${performanceLogInstance}" field="staDel"--}%
								%{--url="${request.contextPath}/PerformanceLog/updatefield" type="text"--}%
								%{--title="Enter staDel" onsuccess="reloadPerformanceLogTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${performanceLogInstance}" field="staDel"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%

            <g:if test="${performanceLogInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="performanceLog.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${performanceLogInstance}" field="dateCreated"
                                url="${request.contextPath}/performanceLog/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadperformanceLogTable();" />--}%

                        <g:formatDate date="${performanceLogInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${performanceLogInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="performanceLog.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${performanceLogInstance}" field="createdBy"
                                url="${request.contextPath}/performanceLog/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadperformanceLogTable();" />--}%

                        <g:fieldValue bean="${performanceLogInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${performanceLogInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="performanceLog.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${performanceLogInstance}" field="lastUpdated"
                                url="${request.contextPath}/performanceLog/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadperformanceLogTable();" />--}%

                        <g:formatDate date="${performanceLogInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${performanceLogInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="performanceLog.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${performanceLogInstance}" field="updatedBy"
                                url="${request.contextPath}/performanceLog/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadperformanceLogTable();" />--}%

                        <g:fieldValue bean="${performanceLogInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${performanceLogInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="performanceLog.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${performanceLogInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/performanceLog/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadperformanceLogTable();" />--}%

                        <g:fieldValue bean="${performanceLogInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${performanceLogInstance?.id}"
					update="[success:'performanceLog-form',failure:'performanceLog-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deletePerformanceLog('${performanceLogInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
