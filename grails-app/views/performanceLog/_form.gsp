<%@ page import="com.kombos.administrasi.PerformanceLog" %>



<div class="control-group fieldcontain ${hasErrors(bean: performanceLogInstance, field: 'm778Tanggal', 'error')} required">
	<label class="control-label" for="m778Tanggal">
		<g:message code="performanceLog.m778Tanggal.label" default="Tanggal" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<ba:datePicker format="dd/MM/yyyy" name="m778Tanggal" precision="day"  value="${performanceLogInstance?.m778Tanggal}"  />
	</div>
</div>

%{--<div class="control-group fieldcontain ${hasErrors(bean: performanceLogInstance, field: 'm778Id', 'error')} required">--}%
	%{--<label class="control-label" for="m778Id">--}%
		%{--<g:message code="performanceLog.m778Id.label" default="M778 Id" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="m778Id" maxlength="10" required="" value="${performanceLogInstance?.m778Id}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: performanceLogInstance, field: 'userProfile', 'error')} required">--}%
	%{--<label class="control-label" for="userProfile">--}%
		%{--<g:message code="performanceLog.userProfile.label" default="Nama User" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:select id="userProfile" name="userProfile.id" from="${com.kombos.baseapp.maintable.UserProfile.list()}" optionKey="id" required="" value="${performanceLogInstance?.userProfile?.id}" class="many-to-one"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: performanceLogInstance, field: 'm778TglJamClick', 'error')} required">--}%
    %{--<label class="control-label" for="m778TglJamClick">--}%
        %{--<g:message code="performanceLog.m778TglJamClick.label" default="Jam" />--}%
        %{--<span class="required-indicator">*</span>--}%
    %{--</label>--}%
    %{--<div class="controls">--}%
        %{--<g:datePicker name="m778TglJamClick" precision="day"  value="${performanceLogInstance?.m778TglJamClick}"  />--}%
    %{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: performanceLogInstance, field: 'm778DurasiDetik', 'error')} required">--}%
    %{--<label class="control-label" for="m778DurasiDetik">--}%
        %{--<g:message code="performanceLog.m778DurasiDetik.label" default="Durasi" />--}%
        %{--<span class="required-indicator">*</span>--}%
    %{--</label>--}%
    %{--<div class="controls">--}%
        %{--<g:field name="m778DurasiDetik" type="number" value="${performanceLogInstance.m778DurasiDetik}" required=""/>--}%
    %{--</div>--}%
%{--</div>--}%

<div class="control-group fieldcontain ${hasErrors(bean: performanceLogInstance, field: 'm778NamaActivity', 'error')} required">
	<label class="control-label" for="m778NamaActivity">
		<g:message code="performanceLog.m778NamaActivity.label" default="Aktifitas" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m778NamaActivity" maxlength="50" required="" value="${performanceLogInstance?.m778NamaActivity}"/>
	</div>
</div>



%{--<div class="control-group fieldcontain ${hasErrors(bean: performanceLogInstance, field: 'm778TglJamTampil', 'error')} required">--}%
	%{--<label class="control-label" for="m778TglJamTampil">--}%
		%{--<g:message code="performanceLog.m778TglJamTampil.label" default="M778 Tgl Jam Tampil" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:datePicker name="m778TglJamTampil" precision="day"  value="${performanceLogInstance?.m778TglJamTampil}"  />--}%
	%{--</div>--}%
%{--</div>--}%



%{--<div class="control-group fieldcontain ${hasErrors(bean: performanceLogInstance, field: 'm778xNamaUser', 'error')} required">--}%
	%{--<label class="control-label" for="m778xNamaUser">--}%
		%{--<g:message code="performanceLog.m778xNamaUser.label" default="M778x Nama User" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="m778xNamaUser" maxlength="50" required="" value="${performanceLogInstance?.m778xNamaUser}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: performanceLogInstance, field: 'm778xNamaDivisi', 'error')} required">--}%
	%{--<label class="control-label" for="m778xNamaDivisi">--}%
		%{--<g:message code="performanceLog.m778xNamaDivisi.label" default="M778x Nama Divisi" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="m778xNamaDivisi" maxlength="50" required="" value="${performanceLogInstance?.m778xNamaDivisi}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: performanceLogInstance, field: 'staDel', 'error')} required">--}%
	%{--<label class="control-label" for="staDel">--}%
		%{--<g:message code="performanceLog.staDel.label" default="Sta Del" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="staDel" maxlength="1" required="" value="${performanceLogInstance?.staDel}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: performanceLogInstance, field: 'createdBy', 'error')} ">--}%
	%{--<label class="control-label" for="createdBy">--}%
		%{--<g:message code="performanceLog.createdBy.label" default="Created By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="createdBy" value="${performanceLogInstance?.createdBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: performanceLogInstance, field: 'updatedBy', 'error')} ">--}%
	%{--<label class="control-label" for="updatedBy">--}%
		%{--<g:message code="performanceLog.updatedBy.label" default="Updated By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="updatedBy" value="${performanceLogInstance?.updatedBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: performanceLogInstance, field: 'lastUpdProcess', 'error')} ">--}%
	%{--<label class="control-label" for="lastUpdProcess">--}%
		%{--<g:message code="performanceLog.lastUpdProcess.label" default="Last Upd Process" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="lastUpdProcess" value="${performanceLogInstance?.lastUpdProcess}"/>--}%
	%{--</div>--}%
%{--</div>--}%

