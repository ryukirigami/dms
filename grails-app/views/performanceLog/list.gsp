
<%@ page import="com.kombos.administrasi.PerformanceLog" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'performanceLog.label', default: 'PerformanceLog')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/performanceLog/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/performanceLog/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#performanceLog-form').empty();
    	$('#performanceLog-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#performanceLog-table").hasClass("span12")){
   			$("#performanceLog-table").toggleClass("span12 span5");
        }
        $("#performanceLog-form").css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
   		if($("#performanceLog-table").hasClass("span5")){
   			$("#performanceLog-table").toggleClass("span5 span12");
   		}
        $("#performanceLog-form").css("display","none");
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#performanceLog-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/performanceLog/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadPerformanceLogTable();
    		}
		});
		
   	}

});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			%{--<li><a class="pull-right box-action" href="#"--}%
				%{--style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i--}%
					%{--class="icon-plus"></i>&nbsp;&nbsp;--}%
			%{--</a></li>--}%
			%{--<li><a class="pull-right box-action" href="#"--}%
				%{--style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i--}%
					%{--class="icon-remove"></i>&nbsp;&nbsp;--}%
			%{--</a></li>--}%
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="performanceLog-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <fieldset>
                <table style="padding-right: 10px">
                    <tr>
                        <td>
                            <label class="control-label" for="m778Tanggal">
                                <g:message code="performanceLog.tanggal.label" default="Tanggal" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="filter_m778Tanggal" class="controls">
                                <ba:datePicker id="search_m778Tanggal" name="search_m778Tanggal" precision="day" format="dd/MM/yyyy"  value=""  />
                                &nbsp;&nbsp;&nbsp;s.d.&nbsp;&nbsp;&nbsp;
                                <ba:datePicker id="search_m778TanggalAkhir" name="search_m778TanggalAkhir" precision="day" format="dd/MM/yyyy"  value=""  />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label" for="m778NamaActivity">
                                <g:message code="performanceLog.m778NamaActivity.label" default="Aktifitas" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="filter_m778NamaActivity" class="controls">
                                <g:textField style="width:490px" id="search_m778NamaActivity" name="search_m778NamaActivity" maxlength="50" />
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" >
                            <div class="controls" style="right: 0">
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn-primary view" name="view" id="view" >View</button>
                                &nbsp;&nbsp;
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn-primary editable-cancel" name="clear" id="clear" >Clear</button>
                            </div>
                        </td>
                    </tr>
                </table>
            </fieldset>
			<g:render template="dataTables" />
		</div>
		<div class="span7" id="performanceLog-form" style="display: none;"></div>
	</div>
</body>
</html>
