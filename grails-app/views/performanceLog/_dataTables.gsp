
<%@ page import="com.kombos.administrasi.PerformanceLog" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="performanceLog_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="performanceLog.m778Tanggal.label" default="Tanggal" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="performanceLog.m778TglJamClick.label" default="Jam" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="performanceLog.m778DurasiDetik.label" default="Durasi" /></div>
            </th>



            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="performanceLog.m778NamaActivity.label" default="Aktifitas" /></div>
            </th>

            %{----}%
            %{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="performanceLog.m778Id.label" default="M778 Id" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="performanceLog.userProfile.label" default="User Profile" /></div>--}%
			%{--</th>--}%





			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="performanceLog.m778TglJamTampil.label" default="M778 Tgl Jam Tampil" /></div>--}%
			%{--</th>--}%



			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="performanceLog.m778xNamaUser.label" default="M778x Nama User" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="performanceLog.m778xNamaDivisi.label" default="M778x Nama Divisi" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="performanceLog.staDel.label" default="Sta Del" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="performanceLog.createdBy.label" default="Created By" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="performanceLog.updatedBy.label" default="Updated By" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="performanceLog.lastUpdProcess.label" default="Last Upd Process" /></div>--}%
			%{--</th>--}%

		
		</tr>
		%{--<tr>--}%
		%{----}%

			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_m778Tanggal" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >--}%
					%{--<input type="hidden" name="search_m778Tanggal" value="date.struct">--}%
					%{--<input type="hidden" name="search_m778Tanggal_day" id="search_m778Tanggal_day" value="">--}%
					%{--<input type="hidden" name="search_m778Tanggal_month" id="search_m778Tanggal_month" value="">--}%
					%{--<input type="hidden" name="search_m778Tanggal_year" id="search_m778Tanggal_year" value="">--}%
					%{--<input type="text" data-date-format="dd/mm/yyyy" name="search_m778Tanggal_dp" value="" id="search_m778Tanggal" class="search_init">--}%
				%{--</div>--}%
			%{--</th>--}%

            %{--<th style="border-top: none;padding: 5px;">--}%
                %{--<div id="filter_m778TglJamClick" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
                    %{--<input type="text" name="search_m778TglJamClick" class="search_init" />--}%
                %{--</div>--}%
            %{--</th>--}%

            %{--<th style="border-top: none;padding: 5px;">--}%
                %{--<div id="filter_m778DurasiDetik" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
                    %{--<input type="text" name="search_m778DurasiDetik" class="search_init" />--}%
                %{--</div>--}%
            %{--</th>--}%


            %{--<th style="border-top: none;padding: 5px;">--}%
                %{--<div id="filter_m778NamaActivity" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
                    %{--<input type="text" name="search_m778NamaActivity" class="search_init" />--}%
                %{--</div>--}%
            %{--</th>--}%


            %{----}%
            %{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_m778Id" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_m778Id" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_userProfile" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_userProfile" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%

			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_m778TglJamTampil" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >--}%
					%{--<input type="hidden" name="search_m778TglJamTampil" value="date.struct">--}%
					%{--<input type="hidden" name="search_m778TglJamTampil_day" id="search_m778TglJamTampil_day" value="">--}%
					%{--<input type="hidden" name="search_m778TglJamTampil_month" id="search_m778TglJamTampil_month" value="">--}%
					%{--<input type="hidden" name="search_m778TglJamTampil_year" id="search_m778TglJamTampil_year" value="">--}%
					%{--<input type="text" data-date-format="dd/mm/yyyy" name="search_m778TglJamTampil_dp" value="" id="search_m778TglJamTampil" class="search_init">--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%

			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_m778xNamaUser" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_m778xNamaUser" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_m778xNamaDivisi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_m778xNamaDivisi" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_staDel" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_createdBy" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_updatedBy" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_lastUpdProcess" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%


		%{--</tr>--}%
	</thead>
</table>

<g:javascript>
var performanceLogTable;
var reloadPerformanceLogTable;
$(function(){
	
	reloadPerformanceLogTable = function() {
		performanceLogTable.fnDraw();
	}

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	performanceLogTable.fnDraw();
		}
	});
	$('#clear').click(function(e){
        $('#search_m778Tanggal').val("");
        $('#search_m778Tanggal_day').val("");
        $('#search_m778Tanggal_month').val("");
        $('#search_m778Tanggal_year').val("");
        $('#search_m778TanggalAkhir').val("");
        $('#search_m778TanggalAkhir_day').val("");
        $('#search_m778TanggalAkhir_month').val("");
        $('#search_m778TanggalAkhir_year').val("");
        $('#filter_m778NamaActivity input').val("");
//        reloadAuditTrailTable();
	});
	$('#view').click(function(e){
        e.stopPropagation();
		performanceLogTable.fnDraw();
//        reloadAuditTrailTable();
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	performanceLogTable = $('#performanceLog_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m778Tanggal",
	"mDataProp": "m778Tanggal",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,
{
	"sName": "m778TglJamClick",
	"mDataProp": "m778TglJamClick",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "m778DurasiDetik",
	"mDataProp": "m778DurasiDetik",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "m778NamaActivity",
	"mDataProp": "m778NamaActivity",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

//,
//
//{
//	"sName": "m778Id",
//	"mDataProp": "m778Id",
//	"aTargets": [1],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "userProfile",
//	"mDataProp": "userProfile",
//	"aTargets": [2],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//
//
//{
//	"sName": "m778TglJamTampil",
//	"mDataProp": "m778TglJamTampil",
//	"aTargets": [5],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//
//{
//	"sName": "m778xNamaUser",
//	"mDataProp": "m778xNamaUser",
//	"aTargets": [7],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "m778xNamaDivisi",
//	"mDataProp": "m778xNamaDivisi",
//	"aTargets": [8],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "staDel",
//	"mDataProp": "staDel",
//	"aTargets": [9],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "createdBy",
//	"mDataProp": "createdBy",
//	"aTargets": [10],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "updatedBy",
//	"mDataProp": "updatedBy",
//	"aTargets": [11],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "lastUpdProcess",
//	"mDataProp": "lastUpdProcess",
//	"aTargets": [12],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var m778Tanggal = $('#search_m778Tanggal').val();
						var m778TanggalDay = $('#search_m778Tanggal_day').val();
						var m778TanggalMonth = $('#search_m778Tanggal_month').val();
						var m778TanggalYear = $('#search_m778Tanggal_year').val();
						
						if(m778Tanggal){
							aoData.push(
									{"name": 'sCriteria_m778Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_m778Tanggal_dp', "value": m778Tanggal},
									{"name": 'sCriteria_m778Tanggal_day', "value": m778TanggalDay},
									{"name": 'sCriteria_m778Tanggal_month', "value": m778TanggalMonth},
									{"name": 'sCriteria_m778Tanggal_year', "value": m778TanggalYear}
							);
						}

						var m778TanggalAkhir = $('#search_m778TanggalAkhir').val();
						var m778TanggalAkhirDay = $('#search_m778TanggalAkhir_day').val();
						var m778TanggalAkhirMonth = $('#search_m778TanggalAkhir_month').val();
						var m778TanggalAkhirYear = $('#search_m778TanggalAkhir_year').val();

						if(m778TanggalAkhir){
							aoData.push(
									{"name": 'sCriteria_m778TanggalAkhir', "value": "date.struct"},
									{"name": 'sCriteria_m778TanggalAkhir_dp', "value": m778TanggalAkhir},
									{"name": 'sCriteria_m778TanggalAkhir_day', "value": m778TanggalAkhirDay},
									{"name": 'sCriteria_m778TanggalAkhir_month', "value": m778TanggalAkhirMonth},
									{"name": 'sCriteria_m778TanggalAkhir_year', "value": m778TanggalAkhirYear}
							);
						}
	
						var m778Id = $('#filter_m778Id input').val();
						if(m778Id){
							aoData.push(
									{"name": 'sCriteria_m778Id', "value": m778Id}
							);
						}
	
						var userProfile = $('#filter_userProfile input').val();
						if(userProfile){
							aoData.push(
									{"name": 'sCriteria_userProfile', "value": userProfile}
							);
						}
	
						var m778NamaActivity = $('#filter_m778NamaActivity input').val();
						if(m778NamaActivity){
							aoData.push(
									{"name": 'sCriteria_m778NamaActivity', "value": m778NamaActivity}
							);
						}

						var m778TglJamClick = $('#m778TglJamClick input').val();
                        if(m778TglJamClick){
                            aoData.push(
                                    {"name": 'sCriteria_m778TglJamClick', "value": m778TglJamClick}
                            );
                        }

						var m778TglJamTampil = $('#search_m778TglJamTampil').val();
						var m778TglJamTampilDay = $('#search_m778TglJamTampil_day').val();
						var m778TglJamTampilMonth = $('#search_m778TglJamTampil_month').val();
						var m778TglJamTampilYear = $('#search_m778TglJamTampil_year').val();
						
						if(m778TglJamTampil){
							aoData.push(
									{"name": 'sCriteria_m778TglJamTampil', "value": "date.struct"},
									{"name": 'sCriteria_m778TglJamTampil_dp', "value": m778TglJamTampil},
									{"name": 'sCriteria_m778TglJamTampil_day', "value": m778TglJamTampilDay},
									{"name": 'sCriteria_m778TglJamTampil_month', "value": m778TglJamTampilMonth},
									{"name": 'sCriteria_m778TglJamTampil_year', "value": m778TglJamTampilYear}
							);
						}
	
						var m778DurasiDetik = $('#filter_m778DurasiDetik input').val();
						if(m778DurasiDetik){
							aoData.push(
									{"name": 'sCriteria_m778DurasiDetik', "value": m778DurasiDetik}
							);
						}
	
						var m778xNamaUser = $('#filter_m778xNamaUser input').val();
						if(m778xNamaUser){
							aoData.push(
									{"name": 'sCriteria_m778xNamaUser', "value": m778xNamaUser}
							);
						}
	
						var m778xNamaDivisi = $('#filter_m778xNamaDivisi input').val();
						if(m778xNamaDivisi){
							aoData.push(
									{"name": 'sCriteria_m778xNamaDivisi', "value": m778xNamaDivisi}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
