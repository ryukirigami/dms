<%@ page import="com.kombos.hrd.PendidikanKaryawan" %>

<g:if test="${params.onDataKaryawan}">
    <g:hiddenField name="karyawanId" value="${params.karyawanId}"/>
</g:if>


<div class="control-group fieldcontain ${hasErrors(bean: pendidikanKaryawanInstance, field: 'namaSekolah', 'error')} ">
	<label class="control-label" for="namaSekolah">
		<g:message code="pendidikanKaryawan.namaSekolah.label" default="Nama Sekolah" />
		
	</label>
	<div class="controls">
	<g:textField name="namaSekolah" value="${pendidikanKaryawanInstance?.namaSekolah}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: pendidikanKaryawanInstance, field: 'jurusan', 'error')} ">
    <label class="control-label" for="jurusan">
        <g:message code="pendidikanKaryawan.jurusan.label" default="Jurusan" />

    </label>
    <div class="controls">
        <g:textField name="jurusan" value="${pendidikanKaryawanInstance?.jurusan}" />
    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: pendidikanKaryawanInstance, field: 'pendidikan', 'error')} ">
	<label class="control-label" for="pendidikan">
		<g:message code="pendidikanKaryawan.pendidikan.label" default="Pendidikan" />
		
	</label>
	<div class="controls">
	<g:select id="pendidikan" name="pendidikan.id" from="${com.kombos.hrd.Pendidikan.list()}" optionKey="id" required="" value="${pendidikanKaryawanInstance?.pendidikan?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: pendidikanKaryawanInstance, field: 'kota', 'error')} ">
    <label class="control-label" for="pendidikan">
        <g:message code="pendidikanKaryawan.kota.label" default="Kota" />

    </label>
    <div class="controls">
        <g:select id="kota" name="kota.id" from="${com.kombos.administrasi.KabKota.list()}" optionKey="id" optionValue="m002NamaKabKota" required="" value="${pendidikanKaryawanInstance?.kota?.id}" class="many-to-one"/>
    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: pendidikanKaryawanInstance, field: 'tahunMasuk', 'error')} ">
    <label class="control-label" for="tahunMasuk">
        <g:message code="pendidikanKaryawan.tahunMasuk.label" default="Tahun Masuk" />

    </label>
    <div class="controls">
        <g:textField name="tahunMasuk" id="tahunMasuk" value="${pendidikanKaryawanInstance.nemIpk}" />
    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: pendidikanKaryawanInstance, field: 'tahunLulus', 'error')} ">
	<label class="control-label" for="tahunLulus">
		<g:message code="pendidikanKaryawan.tahunLulus.label" default="Tahun Lulus" />
		
	</label>
	<div class="controls">
        <g:textField name="tahunLulus" id="tahunLulus" value="${pendidikanKaryawanInstance.nemIpk}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: pendidikanKaryawanInstance, field: 'nemIpk', 'error')} ">
    <label class="control-label" for="nemIpk">
        <g:message code="pendidikanKaryawan.nemIpk.label" default="Nem Ipk" />

    </label>
    <div class="controls">
        <g:textField name="nemIpk" id="nemIpk" value="${pendidikanKaryawanInstance.nemIpk}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: pendidikanKaryawanInstance, field: 'statusLulus', 'error')} ">
    <label class="control-label" for="statusLulus">
        <g:message code="pendidikanKaryawan.statusLulus.label" default="Status Lulus" />

    </label>
    <div class="controls">
        <g:textField name="statusLulus" value="${pendidikanKaryawanInstance?.statusLulus}" />
    </div>
</div>

