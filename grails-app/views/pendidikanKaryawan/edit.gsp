<%@ page import="com.kombos.hrd.PendidikanKaryawan" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'pendidikanKaryawan.label', default: 'PendidikanKaryawan')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="edit-pendidikanKaryawan" class="content scaffold-edit" role="main">
			<legend><g:message code="default.edit.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${pendidikanKaryawanInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${pendidikanKaryawanInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form method="post" >--}%
            <form></form> <!-- aneh kalo ga pake <form> nya ga muncul -->
			<g:formRemote class="form-horizontal" name="edit" on404="alert('not found!');" onSuccess="reloadPendidikanKaryawanTable();" update="pendidikanKaryawan-form"
				url="[controller: 'pendidikanKaryawan', action:'update']">
				<g:hiddenField name="id" value="${pendidikanKaryawanInstance?.id}" />
				<g:hiddenField name="version" value="${pendidikanKaryawanInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons controls">
					<g:actionSubmit class="btn btn-primary save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                    <a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout('pendidikanKaryawan');"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
				</fieldset>
			</g:formRemote>
			%{--</g:form>--}%
		</div>
	</body>
</html>
