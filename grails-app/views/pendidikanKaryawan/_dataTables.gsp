
<%@ page import="com.kombos.hrd.PendidikanKaryawan" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="pendidikanKaryawan_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="pendidikanKaryawan.jurusan.label" default="Jurusan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="pendidikanKaryawan.karyawan.label" default="Karyawan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="pendidikanKaryawan.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="pendidikanKaryawan.namaSekolah.label" default="Nama Sekolah" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="pendidikanKaryawan.nemIpk.label" default="Nem Ipk" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="pendidikanKaryawan.pendidikan.label" default="Pendidikan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="pendidikanKaryawan.statusLulus.label" default="Status Lulus" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="pendidikanKaryawan.tahunLulus.label" default="Tahun Lulus" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="pendidikanKaryawan.tahunMasuk.label" default="Tahun Masuk" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_jurusan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_jurusan" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_karyawan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_karyawan" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_namaSekolah" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_namaSekolah" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_nemIpk" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_nemIpk" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_pendidikan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_pendidikan" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_statusLulus" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_statusLulus" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_tahunLulus" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_tahunLulus" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_tahunMasuk" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_tahunMasuk" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var pendidikanKaryawanTable;
var reloadPendidikanKaryawanTable;
$(function(){
	
	reloadPendidikanKaryawanTable = function() {
		pendidikanKaryawanTable.fnDraw();
	}

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	pendidikanKaryawanTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	pendidikanKaryawanTable = $('#pendidikanKaryawan_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "jurusan",
	"mDataProp": "jurusan",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "karyawan",
	"mDataProp": "karyawan",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "namaSekolah",
	"mDataProp": "namaSekolah",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "nemIpk",
	"mDataProp": "nemIpk",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "pendidikan",
	"mDataProp": "pendidikan",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "statusLulus",
	"mDataProp": "statusLulus",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "tahunLulus",
	"mDataProp": "tahunLulus",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "tahunMasuk",
	"mDataProp": "tahunMasuk",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var jurusan = $('#filter_jurusan input').val();
						if(jurusan){
							aoData.push(
									{"name": 'sCriteria_jurusan', "value": jurusan}
							);
						}
	
						var karyawan = $('#filter_karyawan input').val();
						if(karyawan){
							aoData.push(
									{"name": 'sCriteria_karyawan', "value": karyawan}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}
	
						var namaSekolah = $('#filter_namaSekolah input').val();
						if(namaSekolah){
							aoData.push(
									{"name": 'sCriteria_namaSekolah', "value": namaSekolah}
							);
						}
	
						var nemIpk = $('#filter_nemIpk input').val();
						if(nemIpk){
							aoData.push(
									{"name": 'sCriteria_nemIpk', "value": nemIpk}
							);
						}
	
						var pendidikan = $('#filter_pendidikan input').val();
						if(pendidikan){
							aoData.push(
									{"name": 'sCriteria_pendidikan', "value": pendidikan}
							);
						}
	
						var statusLulus = $('#filter_statusLulus input').val();
						if(statusLulus){
							aoData.push(
									{"name": 'sCriteria_statusLulus', "value": statusLulus}
							);
						}
	
						var tahunLulus = $('#filter_tahunLulus input').val();
						if(tahunLulus){
							aoData.push(
									{"name": 'sCriteria_tahunLulus', "value": tahunLulus}
							);
						}
	
						var tahunMasuk = $('#filter_tahunMasuk input').val();
						if(tahunMasuk){
							aoData.push(
									{"name": 'sCriteria_tahunMasuk', "value": tahunMasuk}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
