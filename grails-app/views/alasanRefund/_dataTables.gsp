
<%@ page import="com.kombos.administrasi.AlasanRefund" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="alasanRefund_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>
		
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="alasanRefund.m071ID.label" default="Kode Alasan" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="alasanRefund.m071AlasanRefund.label" default="Nama Alasan" /></div>
			</th>


			
		
		</tr>
	</thead>
</table>

<g:javascript>
var alasanRefundTable;
var reloadAlasanRefundTable;
var checked = [];
$(function(){
	
	reloadAlasanRefundTable = function() {
		alasanRefundTable.fnDraw();
	}

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	alasanRefundTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	alasanRefundTable = $('#alasanRefund_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
		        var aData = alasanRefundTable.fnGetData(nRow);
		        var checkbox = $('.row-select', nRow);
		        if(checked.indexOf(""+aData['id'])>=0){
		              checkbox.attr("checked",true);
		              checkbox.parent().parent().addClass('row_selected');
		        }

		        checkbox.click(function (e) {
            	    var tc = $(this);

                    if(this.checked)
            			tc.parent().parent().addClass('row_selected');
            		else {
            			tc.parent().parent().removeClass('row_selected');
            			var selectAll = $('.select-all');
            			selectAll.removeAttr('checked');
                    }
            	 	e.stopPropagation();
                });
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m071ID",
	"mDataProp": "m071ID",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "m071AlasanRefund",
	"mDataProp": "m071AlasanRefund",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        var selectAll = $('.select-all');
                        selectAll.removeAttr('checked');
                        $("#alasanRefund_datatables tbody .row-select").each(function() {
                                    var id = $(this).next("input:hidden").val();
                                    if(checked.indexOf(""+id)>=0){
                                        checked[checked.indexOf(""+id)]="";
                                    }
                                    if(this.checked){
                                        checked.push(""+id);
                                    }
                                });
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
