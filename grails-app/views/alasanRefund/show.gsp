

<%@ page import="com.kombos.administrasi.AlasanRefund" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'alasanRefund.label', default: 'Alasan Refund')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteAlasanRefund;

$(function(){ 
	deleteAlasanRefund=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/alasanRefund/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadAlasanRefundTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-alasanRefund" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="alasanRefund"
			class="table table-bordered table-hover">
			<tbody>
				<g:if test="${alasanRefundInstance?.m071ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m071ID-label" class="property-label"><g:message
					code="alasanRefund.m071ID.label" default="Kode Alasan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m071ID-label">
						%{--<ba:editableValue
								bean="${alasanRefundInstance}" field="m071ID"
								url="${request.contextPath}/AlasanRefund/updatefield" type="text"
								title="Enter m071ID" onsuccess="reloadAlasanRefundTable();" />--}%
							
								<g:fieldValue bean="${alasanRefundInstance}" field="m071ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
				
				<g:if test="${alasanRefundInstance?.m071AlasanRefund}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m071AlasanRefund-label" class="property-label"><g:message
					code="alasanRefund.m071AlasanRefund.label" default="Nama Alasan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m071AlasanRefund-label">
						%{--<ba:editableValue
								bean="${alasanRefundInstance}" field="m071AlasanRefund"
								url="${request.contextPath}/AlasanRefund/updatefield" type="text"
								title="Enter m071AlasanRefund" onsuccess="reloadAlasanRefundTable();" />--}%
							
								<g:fieldValue bean="${alasanRefundInstance}" field="m071AlasanRefund"/>
							
						</span></td>
					
				</tr>
				</g:if>
            <g:if test="${alasanRefundInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="alasanRefundInstance.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${goodsInstance}" field="createdBy"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadGoodsTable();" />--}%

                        <g:fieldValue bean="${alasanRefundInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${alasanRefundInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="alasanRefundInstance.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${goodsInstance}" field="updatedBy"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadGoodsTable();" />--}%

                        <g:fieldValue bean="${alasanRefundInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${alasanRefundInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="alasanRefundInstance.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${goodsInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadGoodsTable();" />--}%

                        <g:fieldValue bean="${alasanRefundInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${alasanRefundInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="alasanRefundInstance.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${goodsInstance}" field="dateCreated"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadGoodsTable();" />--}%

                        <g:formatDate date="${alasanRefundInstance?.dateCreated}" format="EEEE, dd MMMM yyyy HH:mm" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${alasanRefundInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="alasanRefundInstance.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${goodsInstance}" field="lastUpdated"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadGoodsTable();" />--}%

                        <g:formatDate date="${alasanRefundInstance?.lastUpdated}" format="EEEE, dd MMMM yyyy HH:mm"/>

                    </span></td>

                </tr>
            </g:if>
				
			
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${alasanRefundInstance?.id}"
					update="[success:'alasanRefund-form',failure:'alasanRefund-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteAlasanRefund('${alasanRefundInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
