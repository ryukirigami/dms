<%@ page import="com.kombos.administrasi.AlasanRefund" %>

<div class="control-group fieldcontain ${hasErrors(bean: alasanRefundInstance, field: 'm071ID', 'error')} ">
	<label class="control-label" for="m071ID">
		<g:message code="alasanRefund.m071ID.label" default="Kode Alasan" /><span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
	<g:textField name="m071ID" value="${alasanRefundInstance?.m071ID}" maxlength="2" required=""/>
	</div>
</div>
<g:javascript>
    document.getElementById("m071ID").focus();
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: alasanRefundInstance, field: 'm071AlasanRefund', 'error')} ">
	<label class="control-label" for="m071AlasanRefund">
		<g:message code="alasanRefund.m071AlasanRefund.label" default="Nama Alasan" /><span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
	<g:textField name="m071AlasanRefund" value="${alasanRefundInstance?.m071AlasanRefund}" maxlength="20" required=""/>
	</div>
</div>

