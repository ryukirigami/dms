<%@ page import="com.kombos.customercomplaint.Complaint" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'complaint.label', default: 'Complaint')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
        <style>
            body .modal-complaint {
                /* new custom width */
                width: 1000px;
                /* must be half of the width, minus scrollbar on the left (30px) */
                margin-left: -500px;
            }

            .datepicker{z-index:1151;}
        </style>
	</head>
	<body>
		<div id="create-complaint" class="content scaffold-create" role="main">
			%{--<legend><g:message code="default.create.label" args="[entityName]" /></legend>--}%
			%{--<g:if test="${flash.message}">--}%
			<div id="divMessage" class="message" role="status"></div>
			%{--</g:if>--}%
			<g:hasErrors bean="${complaintInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${complaintInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form action="save" >--}%
            <form id="complaint-save" class="form-horizontal">

                <fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons controls">
					<a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout();"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
					%{--<g:submitButton class="btn btn-primary create" name="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />--}%
				    <input type="button" class="btn btn-primary create" onclick="submitForm();return false;" value="${message(code: 'default.button.create.label', default: 'Create')}"/>
                </fieldset>
			</form>
			%{--</g:form>--}%
            <g:javascript>
            var submitForm;
            $(function(){


                $('#btnUpload').click(function(){
                    var form = new FormData($('#complaint-save')[0]);
                    $.ajax({
                            url:'${request.contextPath}/complaint/uploadPathComplainDoc',
                            type: "POST", // Always use POST when deleting data
                            xhr: function() {
                                var myXhr = $.ajaxSettings.xhr();
                                if(myXhr.upload){
                                    myXhr.upload.addEventListener('progress',progress, false);
                                }
                                return myXhr;
                            },
                            success: function (res) {
                                reloadPathComplainDocTable();
                            },
                            //add error handler for when a error occurs if you want!
                            error: function (data, status, e){
                                alert('error');
                                reloadPathComplainDocTable();
                            },
                            data: form,
                            cache: false,
                            contentType: false,
                            processData: false
                    });
                });


                function progress(e){
                    if(e.lengthComputable){
                        //kalo mau pake progress bar
                        //$('progress').attr({value:e.loaded,max:e.total});
                    }
                }

                submitForm = function() {

                    var form = new FormData($('#complaint-save')[0]);

                    $.ajax({
                        url:'${request.getContextPath()}/complaint/save',
                        type: 'POST',
                        success: function (res) {
//                            $('#complaint-form').empty();
//                            $('#complaint-form').append(res);
                            if(res.message)
//                                alert(res.message);
                                $("#divMessage").html(res.message);
                            else
                                expandTableLayout();
                        },
                        error: function (data, status, e){
                            alert(e);
                        },
                        data: form,
                        cache: false,
                        contentType: false,
                        processData: false
                    });

				}
			});
            </g:javascript>
		</div>
	</body>
</html>
