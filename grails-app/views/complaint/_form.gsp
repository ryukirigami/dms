<%@ page import="com.kombos.customercomplaint.Complaint"%>
<%@ page import="com.kombos.administrasi.KodeKotaNoPol"%>
<%@ page import="com.kombos.customerprofile.HistoryCustomerVehicle"%>

<g:javascript>
    $('#findDataCustomer').click(function(){
       if($('#t183NoPolTengah').val()=="" || $('#t183NoPolTengah').val()==null
               || $('#t183NoPolBelakang').val()=="" || $('#t183NoPolBelakang').val()==null){
           alert('Data No Polisi Masih Ada yang Kosong');
           return false
       }
       var kodeKota = $('#noPol').val();
       var noTengah = $('#t183NoPolTengah').val();
       var noBelakang = $('#t183NoPolBelakang').val();
       setNoPol(kodeKota,noTengah,noBelakang);

        %{--$.ajax({--}%
    		%{--url:'${request.contextPath}/complaint/findNoPol',--}%
    		%{--type: "POST", // Always use POST when deleting data--}%
    		%{--data: {kode : kodeKota, tengah : noTengah, belakang : noBelakang},--}%
    		%{--dataType: 'json',--}%
    		%{--success: function(data,textStatus,xhr){--}%
    		    %{--if(data.length>0){--}%
                    %{--$('#t183NamaSTNK').attr('value',data[0].t183NamaSTNK);--}%
                    %{--$('#t183AlamatSTNK').attr('value',data[0].t183AlamatSTNK);--}%
                    %{--$('#customerVehicle').attr('value',data[0].customerVehicle);--}%
                    %{--$('#fullModelCode').attr('value',data[0].fullModelCode);--}%
                    %{--$('#fullModelCodeBaseModel').attr('value',data[0].fullModelCodeBaseModel);--}%
                    %{--$('#fullModelCodeGear').attr('value',data[0].fullModelCodeGear);--}%
                    %{--$('#warna').attr('value',data[0].warna);--}%
                    %{--$('#tahunPembuatan').attr('value',data[0].t183ThnBlnRakit);--}%
                    %{--$('#tahunDEC').attr('value',data[0].t183TglDEC);--}%
                    %{--$('#historyCustomerVehicle').attr('value',data[0].id);--}%
    		    %{--}else{--}%
    		        %{--$('#t183NamaSTNK').attr('value',"");--}%
                    %{--$('#t183AlamatSTNK').attr('value',"");--}%
                    %{--$('#customerVehicle').attr('value',"");--}%
                    %{--$('#fullModelCode').attr('value',"");--}%
                    %{--$('#fullModelCodeBaseModel').attr('value',"");--}%
                    %{--$('#fullModelCodeGear').attr('value',"");--}%
                    %{--$('#warna').attr('value',"");--}%
                    %{--$('#tahunPembuatan').attr('value',"");--}%
                    %{--$('#tahunDEC').attr('value',"");--}%
                    %{--$('#historyCustomerVehicle').attr('value',"");--}%
    		    %{--}--}%
    		%{--},--}%
    		%{--error:function(XMLHttpRequest,textStatus,errorThrown){},--}%
   			%{--complete:function(data,textStatus){--}%
   			%{--}--}%
		%{--});--}%

    });

    $('#btnDelete').click(function(){
        checkDel =[];
            $("#pathComplainDoc-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    checkDel.push(id);
                }
            });
            if(checkDel.length<1){
                alert('Anda belum memilih data yang akan dihapus');
                return false
            }
                var checkDelMap = JSON.stringify(checkDel);
                $.ajax({
                url:'${request.contextPath}/complaint/delPath',
                type: "POST", // Always use POST when deleting data
                data : { ids: checkDelMap },
                success : function(data){
                    reloadPathComplainDocTable();
                },
                error: function(xhr, textStatus, errorThrown) {
                    alert('Internal Server Error');
                }
                });

        checkDel = [];
    });

    function progress(e){
        if(e.lengthComputable){
            //kalo mau pake progress bar
            //$('progress').attr({value:e.loaded,max:e.total});
        }
    }

    function setNoPol(kodeKota,noTengah,noBelakang){
        $.ajax({
    		url:'${request.contextPath}/complaint/findNoPol',
    		type: "POST", // Always use POST when deleting data
    		data: {kode : kodeKota, tengah : noTengah, belakang : noBelakang},
    		dataType: 'json',
    		success: function(data,textStatus,xhr){
    		    if(data.length>0){
                    $('#t183NamaSTNK').attr('value',data[0].t183NamaSTNK);
                    $('#t183AlamatSTNK').attr('value',data[0].t183AlamatSTNK);
                    $('#customerVehicle').attr('value',data[0].customerVehicle);
                    $('#fullModelCode').attr('value',data[0].fullModelCode);
                    $('#fullModelCodeBaseModel').attr('value',data[0].fullModelCodeBaseModel);
                    $('#fullModelCodeGear').attr('value',data[0].fullModelCodeGear);
                    $('#warna').attr('value',data[0].warna);
                    $('#tahunPembuatan').attr('value',data[0].t183ThnBlnRakit);
                    $('#tahunDEC').attr('value',data[0].t183TglDEC);
                    $('#historyCustomerVehicle').attr('value',data[0].id);
    		    }else{
    		        $('#t183NamaSTNK').attr('value',"");
                    $('#t183AlamatSTNK').attr('value',"");
                    $('#customerVehicle').attr('value',"");
                    $('#fullModelCode').attr('value',"");
                    $('#fullModelCodeBaseModel').attr('value',"");
                    $('#fullModelCodeGear').attr('value',"");
                    $('#warna').attr('value',"");
                    $('#tahunPembuatan').attr('value',"");
                    $('#tahunDEC').attr('value',"");
                    $('#historyCustomerVehicle').attr('value',"");
    		    }
    		},
    		error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(data,textStatus){
   			}
		});
    }

    function report(id){
        $('#idComplaint').attr('value',id);
        $("#btnReport").click();
    }

    $(function() {
        $("dialog").modal({
            show:false
        });

        $("dialog-companyDealer").modal({
            show:false
        });
    });

    function customerComplaintList(){
        reloadComplaintPopUpTable();
        $( "#dialog" ).modal("show");
    }

    function companyDealerList(){
        reloadDealerPenjualTable();
        $('#dealerPembelianLabelDiv').html($('#t921DealerPembelian').val());
        $('#dealerKomplainLabelDiv').html($('#t921DealerDiKomplain').val());
        $('#dealerKomplainTempatLabelDiv').html($('#t921DealerKomplain').val());
        $( "#dialog-companyDealer" ).modal("show");
    }

    var isCharOnly;
    var isNumberKey;
    $(function() {
        isCharOnly = function(e) {
            e = e || event;
            return /[a-zA-Z]/i.test(
                       String.fromCharCode(e.charCode || e.keyCode)
                   ) || !e.charCode && e.keyCode  < 48;
        }

        isNumberKey = function(evt){
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)){
                return false;
            }
            else{
                return true;
            }
        }

        $(".charonly").bind('keypress',isCharOnly);

        $(".numberonly").bind('keypress',isNumberKey);

    });
</g:javascript>

<div class="tabbable control-group" style="width: 1180px">
    <ul class="nav nav-tabs">
        <li id="tab_kendaraan_pelanggan" class="active">
            <a href="#kendaraan_pelanggan" data-toggle="tab">
                Informasi Kendaraan & Pelanggan
            </a>
        </li>
        <li id="tab_keluhan_pelanggan" class="">
            <a href="#keluhan_pelanggan" data-toggle="tab">
                Keluhan Pelanggan
            </a>
        </li>
        <li id="tab_penyelesaian_keluhan" class="">
            <a href="#penyelesaian_keluhan" data-toggle="tab">
                Penyelesaian Keluhan
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="kendaraan_pelanggan">
        <div style="margin-left: 2px" class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921StaMediaKeluhan', 'error')} required">
            <label class="control-label" for="t921StaMediaKeluhan">
                <g:message code="complaint.t921StaMediaKeluhan.label" default="No Polisi" />
                <span class="required-indicator">*</span>
            </label>
            <div class="controls">
                <g:select style="width:50px" id="noPol" name="noPol.id" from="${com.kombos.administrasi.KodeKotaNoPol.createCriteria().list(){eq("staDel","0");order("m116ID")}}" optionValue="${{it.m116ID}}" optionKey="id" required="" value="${(!request.getRequestURI().contains("edit"))?"":complaintInstance?.historyCustomerVehicle.kodeKotaNoPol}" class="many-to-one"/>
                &nbsp;
                <g:textField style="width: 95px" maxlength="5" class="numberonly" name="t183NoPolTengah" id="t183NoPolTengah" required="" value="${(!request.getRequestURI().contains("edit"))?"":complaintInstance?.historyCustomerVehicle.t183NoPolTengah}"/>
                &nbsp;
                %{--<g:field type="text" onkeypress="return validateCharOnly(event);" style="width: 50px" maxlength="3" name="t183NoPolBelakang" id="t183NoPolBelakang" required="" value="${(!request.getRequestURI().contains("edit"))?"":complaintInstance?.historyCustomerVehicle.t183NoPolBelakang}"/>--}%
                <g:field type="text" class="charonly" style="width: 50px" maxlength="3" name="t183NoPolBelakang" id="t183NoPolBelakang" required="" value="${(!request.getRequestURI().contains("edit"))?"":complaintInstance?.historyCustomerVehicle.t183NoPolBelakang}"/>
                &nbsp;
                <g:field type="button" name="findDataCustomer" id="findDataCustomer" class="btn cancel" value="${message(code: 'default.button.complaint.dataCustomer.label', default: 'Data Customer')}"/>
            </div>
        </div>

        <div class="row-fluid">
        <div id="informasiKendaraanForm" class="span5">
            <fieldset>
                <legend>&nbsp;&nbsp;Informasi Kendaraan</legend>
                    <div class="control-group">
                        <label style="text-align: left" class="control-label" for="t183NamaSTNK">
                            <g:message code="complaint.historyCustomerVehicle.t183NamaSTNK.label" default="Nama STNK" />
                        </label>
                        <div class="controls">
                            %{
                                if(request.getRequestURI().contains("edit")){
                            }%
                            <input type="hidden" name="historyCustomerVehicle" id="historyCustomerVehicle" value="${complaintInstance.historyCustomerVehicle.id}"/>
                            %{
                                }else{
                            }%
                            <input type="hidden" name="historyCustomerVehicle" id="historyCustomerVehicle"/>
                            %{
                                }
                            }%
                            <g:textField readonly="" name="t183NamaSTNK" value="${(!request.getRequestURI().contains("edit"))?"":complaintInstance?.historyCustomerVehicle.t183NamaSTNK}"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label style="text-align: left"  class="control-label" for="t183AlamatSTNK">
                            <g:message code="complaint.historyCustomerVehicle.t183AlamatSTNK.label" default="Alamat STNK" />
                        </label>
                        <div class="controls">
                            <g:textField readonly=""  name="t183AlamatSTNK" value="${(!request.getRequestURI().contains("edit"))?"":complaintInstance?.historyCustomerVehicle.t183AlamatSTNK}"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label style="text-align: left"  class="control-label" for="customerVehicle">
                            <g:message code="complaint.historyCustomerVehicle.customerVehicle.label" default="Nomor Rangka" />
                        </label>
                        <div class="controls">
                            <g:textField readonly=""  name="customerVehicle" value="${(!request.getRequestURI().contains("edit"))?"":complaintInstance?.historyCustomerVehicle.customerVehicle.t103VinCode}"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label style="text-align: left"  class="control-label" for="fullModelCode">
                            <g:message code="complaint.historyCustomerVehicle.fullModelCode.label" default="Full Model Code" />
                        </label>
                        <div class="controls">
                            <g:textField readonly=""  name="fullModelCode" value="${(!request.getRequestURI().contains("edit"))?"":complaintInstance?.historyCustomerVehicle.fullModelCode.t110FullModelCode}"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label style="text-align: left"  class="control-label" for="fullModelCodeBaseModel">
                            <g:message code="complaint.historyCustomerVehicle.fullModelCodeBaseModel.label" default="Base Model" />
                        </label>
                        <div class="controls">
                            <g:textField readonly=""  name="fullModelCodeBaseModel" value="${(!request.getRequestURI().contains("edit"))?"":complaintInstance?.historyCustomerVehicle.fullModelCode.baseModel.m102KodeBaseModel}"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label  style="text-align: left" class="control-label" for="fullModelCodeGear">
                            <g:message code="complaint.historyCustomerVehicle.gear.label" default="Gear" />
                        </label>
                        <div class="controls">
                            <g:textField readonly=""  name="fullModelCodeGear" value="${(!request.getRequestURI().contains("edit"))?"":complaintInstance?.historyCustomerVehicle.fullModelCode.gear.m106NamaGear}"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label style="text-align: left"  class="control-label" for="warna">
                            <g:message code="complaint.historyCustomerVehicle.warna.label" default="Warna" />
                        </label>
                        <div class="controls">
                            <g:textField  readonly=""  name="warna" value="${(!request.getRequestURI().contains("edit"))?"":complaintInstance?.historyCustomerVehicle.warna}"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label style="text-align: left"  class="control-label" for="tahunPembuatan">
                            <g:message code="complaint.historyCustomerVehicle.tahunPembuatan.label" default="Tahun Pembuatan" />
                        </label>
                        <div class="controls">
                            <g:textField  readonly="" name="tahunPembuatan" value="${(!request.getRequestURI().contains("edit"))?"":complaintInstance?.historyCustomerVehicle.t183ThnBlnRakit}"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label style="text-align: left"  class="control-label" for="tahunDEC">
                            <g:message code="complaint.historyCustomerVehicle.tahunDEC.label" default="Tahun DEC" />
                        </label>
                        <div class="controls">
                            <g:textField  readonly=""  name="tahunDEC" value="${(!request.getRequestURI().contains("edit"))?"":complaintInstance?.historyCustomerVehicle.t183TglDEC}"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label style="text-align: left"  class="control-label" for="t921Km">
                            <g:message code="complaint.t921Km.label" default="Odometer" />
                        </label>
                        <div class="controls">
                            %{--<g:textField name="t921Km" onkeydown="return isNumberKey(event);" value="${complaintInstance?.t921Km}"/>--}%
                            <g:textField name="t921Km" class="numberonly" value="${complaintInstance?.t921Km}"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label style="text-align: left"  class="control-label" for="t921StaPakaiKendaraan">
                            <g:message code="complaint.t921StaPakaiKendaraan.label" default="Status Pakai Kendaraan" />
                        </label>
                        <div class="controls">
                            <g:radioGroup name="t921StaPakaiKendaraan" id="t921StaPakaiKendaraan" values="['n','u','s']" labels="['New','Used','Sewa']" value="${complaintInstance?.t921StaPakaiKendaraan}" >
                                ${it.radio} ${it.label}
                            </g:radioGroup>
                        </div>
                    </div>
            </fieldset>

            <fieldset>
                <legend>&nbsp;&nbsp;Referensi</legend>
                <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921NoReff', 'error')} required">
                    <label  style="text-align: left"  class="control-label" for="t921NoReff">
                        <g:message code="complaint.t921NoReff.label" default="Nomor Referensi" />
                        <span class="required-indicator">*</span>
                    </label>
                    <div class="controls">
                        <g:textField name="t921NoReff" required="" value="${complaintInstance?.t921NoReff}"/>
                    </div>
                </div>
                <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921TglKasusDiterima', 'error')} required">
                    <label  style="text-align: left"  class="control-label" for="t921TglKasusDiterima">
                        <g:message code="complaint.t921TglKasusDiterima.label" default="Tanggal Kasus Diterima dari TAM" />
                        <span class="required-indicator">*</span>
                    </label>
                    <div class="controls">
                        <ba:datePicker required="true" format="dd/mm/yyyy" name="t921TglKasusDiterima" precision="day"  value="${complaintInstance?.t921TglKasusDiterima}"  />
                    </div>
                </div>
                <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921TglLPKDikirim', 'error')} required">
                    <label  style="text-align: left"  class="control-label" for="t921TglLPKDikirim">
                        <g:message code="complaint.t921TglLPKDikirim.label" default="Tanggal LPK Dikirim ke TAM" />
                        <span class="required-indicator">*</span>
                    </label>
                    <div class="controls">
                        <ba:datePicker required="true" format="dd/mm/yyyy" name="t921TglLPKDikirim" precision="day"  value="${complaintInstance?.t921TglLPKDikirim}"  />
                    </div>
                </div>
                <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921StaMediaKeluhan', 'error')} required">
                    <label  style="text-align: left"  class="control-label" for="t921StaMediaKeluhan">
                        <g:message code="complaint.t921StaMediaKeluhan.label" default="Media Keluhan" />
                        <span class="required-indicator">*</span>
                    </label>
                    <div class="controls">
                        %{--<g:textField name="t921StaMediaKeluhan" required="" value="${complaintInstance?.t921StaMediaKeluhan}"/>--}%
                        <g:select name="t921StaMediaKeluhan" required="" value="${complaintInstance?.t921StaMediaKeluhan}" from="${['Telepon','SMS','Email']}" keys="${['T','S','E']}"/>
                    </div>
                </div>
                <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921AreaKeluhan', 'error')} required">
                    <label  style="text-align: left"  class="control-label" for="t921AreaKeluhan">
                        <g:message code="complaint.t921AreaKeluhan.label" default="Area Keluhan" />
                        <span class="required-indicator">*</span>
                    </label>
                    <div class="controls">
                        %{--<g:textField name="t921AreaKeluhan" required="" value="${complaintInstance?.t921AreaKeluhan}"/>--}%
                        <g:select name="t921AreaKeluhan" required="" value="${complaintInstance?.t921AreaKeluhan}" from="${['Product','Sales','Service & Parts']}" keys="${['1','2','3']}"/>
                    </div>
                </div>
            </fieldset>

            <fieldset>
                <legend>&nbsp;&nbsp;Dealer</legend>
                <g:field type="button" onclick="companyDealerList();" class="btn cancel"
                         name="dealer" id="dealer" value="${message(code: 'default.button.complaint.dealer.label', default: 'Pilih Dealer ...')}" />
                <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921DealerPembelian', 'error')} required">
                    <label  style="text-align: left"  class="control-label" for="t921DealerPembelian">
                        <g:message code="complaint.t921DealerPembelian.label" default="Dealer Pembelian" />
                        <span class="required-indicator">*</span>
                    </label>
                    <div class="controls">
                        <g:textField style="width: 400px" readonly="" name="t921DealerPembelian" required="" value="${complaintInstance?.t921DealerPembelian}"/>
                    </div>
                </div>
                <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921DealerKomplain', 'error')} required">
                    <label  style="text-align: left"  class="control-label" for="t921DealerKomplain">
                        <g:message code="complaint.t921DealerKomplain.label" default="Dealer Tempat Mengkomplain" />
                        <span class="required-indicator">*</span>
                    </label>
                    <div class="controls">
                        <g:textField style="width: 400px" readonly="" name="t921DealerKomplain" required="" value="${complaintInstance?.t921DealerKomplain}"/>
                    </div>
                </div>
                <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921DealerDiKomplain', 'error')} required">
                    <label  style="text-align: left"  class="control-label" for="t921DealerDiKomplain">
                        <g:message code="complaint.t921DealerDiKomplain.label" default="Dealer yang Di Komplain" />
                        <span class="required-indicator">*</span>
                    </label>
                    <div class="controls">
                        <g:textField style="width: 400px" readonly="" name="t921DealerDiKomplain" required="" value="${complaintInstance?.t921DealerDiKomplain}"/>
                    </div>
                </div>
            </fieldset>
        </div>

        <div class="span7 row-fluid">
            <div id="informasiPelangganForm" class="span12 row-fluid">
                <fieldset>
                    <legend>Informasi Pelanggan</legend>
                    <table>
                        <tr>
                            <td>
                                <div class="control-group">
                                    <label style="text-align: left" class="control-label" for="t921Peran">
                                        <g:message code="complaint.t921Peran.label" default="Peran Pelanggan" />
                                    </label>
                                    <div class="controls">
                                        <g:select name="t921Peran" from="${['Pemiilik Mobil']}" />
                                    </div>
                                </div>
                            </td>
                            <td>
                                <label  style="text-align: left"  style="text-align: left" class="control-label" for="t921ProfilPelanggan">
                                    &nbsp;&nbsp;&nbsp;<g:message code="complaint.t921ProfilPelanggan.label" default="Profil Pelanggan" />
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="control-group">
                                    <label style="text-align: left" class="control-label" for="t921NamaPelanggan">
                                        <g:message code="complaint.t921NamaPelanggan.label" default="Nama Pelanggan" />
                                        <span class="required-indicator">*</span>
                                    </label>
                                    <div class="controls">
                                        <g:textField name="t921NamaPelanggan" required="" maxlength="50" value="${complaintInstance?.t921NamaPelanggan}"/>
                                    </div>
                                </div>
                            </td>
                            <td rowspan="6">
                                &nbsp;&nbsp;&nbsp;&nbsp;<g:textArea style="height:220px" name="t921ProfilPelanggan" value="${complaintInstance?.t921ProfilPelanggan}"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="control-group">
                                    <label style="text-align: left" class="control-label" for="t921TelpRumah">
                                        <g:message code="complaint.t921TelpRumah.label" default="Telepon Rumah" />
                                        <span class="required-indicator">*</span>
                                    </label>
                                    <div class="controls">
                                        <g:textField class="numberonly" name="t921TelpRumah" maxlength="12" required="" value="${complaintInstance?.t921TelpRumah}"/>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="control-group">
                                    <label style="text-align: left" class="control-label" for="t921TelpHp">
                                        <g:message code="complaint.t921TelpHp.label" default="Telepon Seluler" />
                                        <span class="required-indicator">*</span>
                                    </label>
                                    <div class="controls">
                                        <g:textField class="numberonly" name="t921TelpHp" maxlength="12 required="" value="${complaintInstance?.t921TelpHp}"/>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="control-group">
                                    <label style="text-align: left" class="control-label" for="t921Fax">
                                        <g:message code="complaint.t921Fax.label" default="Faximile" />
                                        <span class="required-indicator">*</span>
                                    </label>
                                    <div class="controls">
                                        <g:textField class="numberonly" name="t921Fax" maxlength="12 required="" value="${complaintInstance?.t921Fax}"/>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="control-group">
                                    <label  style="text-align: left"  class="control-label" for="t921Email">
                                        <g:message code="complaint.t921Email.label" default="Email" />
                                        <span class="required-indicator">*</span>
                                    </label>
                                    <div class="controls">
                                        <g:field name="t921Email" type="email" required="" value="${complaintInstance?.t921Email}"/>
                                    </div>
                                </div>
                           </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="control-group">
                                    <label  style="text-align: left"  class="control-label" for="t921Alamat">
                                        <g:message code="complaint.t921Alamat.label" default="Alamat" />
                                        <span class="required-indicator">*</span>
                                    </label>
                                    <div class="controls">
                                        <g:textArea name="t921Alamat" required="" value="${complaintInstance?.t921Alamat}"/>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921StaKategoriKeluhan', 'error')} required">
                        <label  style="text-align: left"  class="control-label" for="t921StaKategoriKeluhan">
                            <g:message code="complaint.t921StaKategoriKeluhan.label" default="Kategori Keluhan" />
                            <span class="required-indicator">*</span>
                        </label>
                        <div class="controls">
                            <g:radioGroup required="" name="t921StaKategoriKeluhan" id="t921StaKategoriKeluhan" values="['s','n']" labels="['Serius','Normal']" value="${complaintInstance?.t921StaKategoriKeluhan}" >
                                ${it.radio}&nbsp;&nbsp;${it.label}
                            </g:radioGroup>
                        </div>
                    </div>
                </fieldset>

                <fieldset>
                    <legend><g:message code="complaint.attach.label" default="Attachment File Pendukung"/></legend>
                    <table>
                        <tr>
                            <td style="padding: 5px">
                                <div class="control-group , 'error')} ">
                                    <label  style="text-align: left"  class="control-label" for="t922FileName">
                                        <g:message code="complaint.t922FileName.label" default="Nama File" />
                                    </label>
                                    <div class="controls">
                                        <input type="file" id="t922Gambar" name="t922Gambar" />
                                    </div>
                                </div>
                            </td>
                            <td style="padding: 5px">
                                <g:field style="width:70px;text-align: center" type="button" name="btnUpload" id="btnUpload" class="btn cancel" value="${message(code: 'complaint.button.btnUpload.label', default: 'Upload')}"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <div class="control-group , 'error')} ">
                                    <label  style="text-align: left"  class="control-label" for="t922Ket">
                                        <g:message code="complaint.t922Ket.label" default="Keterangan" />
                                    </label>
                                    <div class="controls">
                                        <g:textField name="t922Ket" value=""/>
                                    </div>
                                </div>
                            </td>
                            <td style="padding: 5px">
                                <g:field style="width:70px;text-align: center" type="button" name="btnReset" id="btnReset" class="btn cancel" value="${message(code: 'complaint.button.btnReset.label', default: 'Reset')}"/>
                            </td>
                        </tr>
                    </table>

                    <div class="span11" id="pathComplainDoc-table">
                        <g:if test="${flash.message}">
                            <div class="message" role="status">
                                ${flash.message}
                            </div>
                        </g:if>
                        <g:render template="dataTablesUpload" />
                    </div>
                    <div>
                        <g:field type="button" style="padding: 5px" class="btn cancel pull-right box-action"
                                 name="btnDelete" id="btnDelete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" />
                    </div>
                </fieldset>


            </div>
        </div>
        </div>
        </div>
        <div class="tab-pane" id="keluhan_pelanggan">
            <fieldset>
                <legend>Keluhan & Permintaan Pelanggan</legend>
                <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921Keluhan', 'error')} required ">
                    <label  style="text-align: left"  class="control-label" for="t921Keluhan">
                        <g:message code="complaint.t921Keluhan.label" default="Keluhan Pelanggan" />
                        <span class="required-indicator">*</span>
                    </label>
                    <div class="controls">
                        <g:textArea style="width:800px" name="t921Keluhan" value="${complaintInstance?.t921Keluhan}" required=""/>
                    </div>
                </div>
                <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921Permintaan', 'error')} ">
                    <label  style="text-align: left"  class="control-label" for="t921Permintaan">
                        <g:message code="complaint.t921Permintaan.label" default="Permintaan Pelanggan" />

                    </label>
                    <div class="controls">
                        <g:textArea style="width:800px" name="t921Permintaan" value="${complaintInstance?.t921Permintaan}"/>
                    </div>
                    <br/>
                    <fieldset>
                        <legend>Latar Belakang Keluhan</legend>
                        <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921LatarBelakangTgl', 'error')} ">
                            <label  style="text-align: left"  class="control-label" for="t921LatarBelakangTgl">
                                <g:message code="complaint.t921LatarBelakangTgl.label" default="Tanggal Latar Belakang Keluhan" />
                                <span class="required-indicator">*</span>
                            </label>
                            <div class="controls">
                                <ba:datePicker required="true" format="dd/mm/yyyy" name="t921LatarBelakangTgl" precision="day"  value="${complaintInstance?.t921LatarBelakangTgl}" default="none" noSelection="['': '']" />
                            </div>
                        </div>
                        <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921LatarBelakangPihakTerlibat', 'error')} ">
                            <label  style="text-align: left"  class="control-label" for="t921LatarBelakangPihakTerlibat">
                                <g:message code="complaint.t921LatarBelakangPihakTerlibat.label" default="Pihak yang Terlibat" />

                            </label>
                            <div class="controls">
                                <g:textArea style="width:800px" name="t921LatarBelakangPihakTerlibat" value="${complaintInstance?.t921LatarBelakangPihakTerlibat}"/>
                            </div>
                        </div>

                        <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921LatarBelakangFakta', 'error')} ">
                            <label  style="text-align: left"  class="control-label" for="t921LatarBelakangFakta">
                                <g:message code="complaint.t921LatarBelakangFakta.label" default="Fakta" />

                            </label>
                            <div class="controls">
                                <g:textArea style="width:800px" name="t921LatarBelakangFakta" value="${complaintInstance?.t921LatarBelakangFakta}"/>
                            </div>
                        </div>
                    </fieldset>
                    <br/>
                    <fieldset>
                        <legend>Penyebab Terjadinya Keluhan</legend>
                        <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921SebabPelanggan', 'error')} ">
                            <label  style="text-align: left"  class="control-label" for="t921SebabPelanggan">
                                <g:message code="complaint.t921SebabPelanggan.label" default="Pelanggan" />

                            </label>
                            <div class="controls">
                                <g:textArea style="width:800px" name="t921SebabPelanggan" value="${complaintInstance?.t921SebabPelanggan}"/>
                            </div>
                        </div>

                        <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921SebabKendaraan', 'error')} ">
                            <label  style="text-align: left"  class="control-label" for="t921SebabKendaraan">
                                <g:message code="complaint.t921SebabKendaraan.label" default="Kendaraan" />

                            </label>
                            <div class="controls">
                                <g:textArea style="width:800px" name="t921SebabKendaraan" value="${complaintInstance?.t921SebabKendaraan}"/>
                            </div>
                        </div>

                        <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921SebabDealer', 'error')} ">
                            <label  style="text-align: left"  class="control-label" for="t921SebabDealer">
                                <g:message code="complaint.t921SebabDealer.label" default="Distributor / Dealer" />

                            </label>
                            <div class="controls">
                                <g:textArea style="width:800px" name="t921SebabDealer" value="${complaintInstance?.t921SebabDealer}"/>
                            </div>
                        </div>
                    </fieldset>
                </div>

            </fieldset>
        </div>
        <div class="tab-pane" id="penyelesaian_keluhan">
            <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921StaPenanganan', 'error')} ">
                <label  style="text-align: left"  class="control-label" for="t921StaPenanganan">
                    <g:message code="complaint.t921StaPenanganan.label" default="Status Penanganan" />
                    <span class="required-indicator">*</span>
                </label>
                <div class="controls">
                    <g:radioGroup required="" name="t921StaPenanganan" id="t921StaPenanganan" values="['n','i','s','c']" labels="['Not Yet Respond','In Process','Solved','Closed']" value="${complaintInstance?.t921StaPenanganan}" >

                        ${it.radio}&nbsp;&nbsp;${it.label}
                    </g:radioGroup>
                </div>
            </div>

            <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921StrategiPenyelesaian', 'error')} ">
                <label  style="text-align: left"  class="control-label" for="t921StrategiPenyelesaian">
                    <g:message code="complaint.t921StrategiPenyelesaian.label" default="Strategi Penyelesaian Keluhan" />

                </label>
                <div class="controls">
                    <g:textArea style="width:800px" name="t921StrategiPenyelesaian" value="${complaintInstance?.t921StrategiPenyelesaian}"/>
                </div>
            </div>

            <br/>

            <fieldset>
                <legend>Tindakan yang Diambil & Alasannya</legend>
                <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921TindakanTgl', 'error')} ">
                    <label  style="text-align: left"  class="control-label" for="t921TindakanTgl">
                        <g:message code="complaint.t921TindakanTgl.label" default="Tanggal Tindakan yang Diambil" />

                    </label>
                    <div class="controls">
                        <ba:datePicker format="dd/mm/yyyy" name="t921TindakanTgl" precision="day"  value="${complaintInstance?.t921TindakanTgl}" default="none" noSelection="['': '']" />
                    </div>
                </div>

                <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921TindakanPihakTerlibat', 'error')} ">
                    <label  style="text-align: left"  class="control-label" for="t921TindakanPihakTerlibat">
                        <g:message code="complaint.t921TindakanPihakTerlibat.label" default="Pihak yang Terlibat" />

                    </label>
                    <div class="controls">
                        <g:textArea style="width:800px" name="t921TindakanPihakTerlibat" value="${complaintInstance?.t921TindakanPihakTerlibat}"/>
                    </div>
                </div>

                <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921TindakanFakta', 'error')} ">
                    <label  style="text-align: left"  class="control-label" for="t921TindakanFakta">
                        <g:message code="complaint.t921TindakanFakta.label" default="Fakta" />

                    </label>
                    <div class="controls">
                        <g:textArea style="width:800px" name="t921TindakanFakta" value="${complaintInstance?.t921TindakanFakta}"/>
                    </div>
                </div>

            </fieldset>

            <br/>

            <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921StaSolusi', 'error')} ">
                <label  style="text-align: left"  class="control-label" for="t921StaSolusi">
                    <g:message code="complaint.t921StaSolusi.label" default="Solusi Penyelesaian" />
                    <span class="required-indicator">*</span>
                </label>
                <div class="controls">
                    <g:radioGroup required="" name="t921StaSolusi" id="t921StaSolusi" values="['w','g','t']" labels="['Warranty','Goodwill','Tidak Ada']" value="${complaintInstance?.t921StaSolusi}" >

                        ${it.radio}&nbsp;&nbsp;${it.label}
                    </g:radioGroup>
                </div>
            </div>

            <fieldset>
                <legend>Kepuasan Pelanggan</legend>
                <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921StaKepuasanPelanggan', 'error')} ">
                    <label  style="text-align: left"  class="control-label" for="t921StaKepuasanPelanggan">
                        <g:message code="complaint.t921StaKepuasanPelanggan.label" default="Kepuasan Pelanggan" />

                    </label>
                    <div class="controls">
                        <g:radioGroup name="t921StaKepuasanPelanggan" id="t921StaKepuasanPelanggan" values="['p','m','t','j']" labels="['Puas','Memahami','Tidak Puas','Tidak Jelas']" value="${complaintInstance?.t921StaKepuasanPelanggan}" >

                            ${it.radio}&nbsp;&nbsp;${it.label}
                        </g:radioGroup>
                    </div>
                </div>

                <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921AlasanKepuasan', 'error')} ">
                    <label  style="text-align: left"  class="control-label" for="t921AlasanKepuasan">
                        <g:message code="complaint.t921AlasanKepuasan.label" default="Alasan Kepuasan" />

                    </label>
                    <div class="controls">
                        <g:textArea style="width:800px" name="t921AlasanKepuasan" value="${complaintInstance?.t921AlasanKepuasan}"/>
                    </div>
                </div>

                <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921TglPenyelesaianKeluhan', 'error')} ">
                    <label  style="text-align: left"  class="control-label" for="t921TglPenyelesaianKeluhan">
                        <g:message code="complaint.t921TglPenyelesaianKeluhan.label" default="Tanggal Penyelesaian Keluhan" />

                    </label>
                    <div class="controls">
                        <ba:datePicker format="dd/mm/yyyy" name="t921TglPenyelesaianKeluhan" precision="day"  value="${complaintInstance?.t921TglPenyelesaianKeluhan}" default="none" noSelection="['': '']" />
                    </div>
                </div>

                <div class="control-group fieldcontain ${hasErrors(bean: complaintInstance, field: 't921StaLangkahPencegahan', 'error')} ">
                    <label  style="text-align: left"  class="control-label" for="t921StaLangkahPencegahan">
                        <g:message code="complaint.t921StaLangkahPencegahan.label" default="Langkah Pencegahan Keluhan Serupa" />

                    </label>
                    <div class="controls">
                        <g:radioGroup name="t921StaLangkahPencegahan" id="t921StaLangkahPencegahan" values="['r','t','a']" labels="['Rencana','Tindakan','Tidak Ada']" value="${complaintInstance?.t921StaLangkahPencegahan}" >

                            ${it.radio}&nbsp;&nbsp;${it.label}
                        </g:radioGroup>
                        <br/><br/>
                        <g:textArea style="width:800px" name="t921TextLangkahPencegahan" value="${complaintInstance?.t921TextLangkahPencegahan}"/>
                    </div>
                </div>
            </fieldset>

        </div>
    </div>
</div>

<div style="align:center">
    <table>
        <tr>
            <td style="padding: 5px">
                <g:field type="button" name="serviceHistory" id="serviceHistory" class="btn cancel" value="${message(code: 'default.button.complaint.dataCustomer.label', default: 'Service History ...')}"/>
            </td>
            <td style="padding: 5px">
                <a class="btn cancel" href="javascript:void(0);"
                   onclick="customerComplaintList();"><g:message
                        code="default.button.customerComplaint.label" default="Customer Complaint List" /></a>
            </td>
            %{
                if(request.getRequestURI().contains("edit")){
            }%
            <td style="padding: 5px">
                %{--<g:field type="button" name="exportToExcel" id="exportToExcel" class="btn cancel" value="${message(code: 'default.button.complaint.dataCustomer.label', default: 'Export To Excel')}"/>--}%
                <a class="btn cancel"  href="javascript:void(0);" onclick="report(${complaintInstance?.id});">Export To Excel</a>
            </td>
            %{
                }
            }%
        </tr>
    </table>
</div>

<div id="dialog" class="modal hide fade in modal-complaint" style="display: none; ">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>
        <h3>Complaint List</h3>
    </div>
    <div class="modal-body">
        <div class="box">
            <div class="dataTables_scroll">
                <div class="span12" id="complaintPopUp-table">
                    <table style="padding-right: 10px">
                        <tr>
                            <td>
                                <label class="control-label" for="t921TglComplain">
                                    <g:message code="complaint.t921TglComplain.label" default="Tanggal" />&nbsp;
                                </label>&nbsp;&nbsp;
                            </td>
                            <td>
                                <div id="filter_t921TglComplain" class="controls">
                                    <ba:datePicker id="search_t921TglComplainPopUp" name="search_t921TglComplainPopUp" precision="day" format="dd/MM/yyyy"  value=""  />
                                    &nbsp;&nbsp;&nbsp;s.d.&nbsp;&nbsp;&nbsp;
                                    <ba:datePicker id="search_t921TglComplainAkhirPopUp" name="search_t921TglComplainAkhirPopUp" precision="day" format="dd/MM/yyyy"  value=""  />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <a class="pull-right box-action" style="display: block;" >
                                    &nbsp;&nbsp;
                                    <i>
                                        <g:field type="button" style="padding: 5px" class="btn cancel pull-right box-action"
                                                 name="clear" id="clear" value="${message(code: 'default.clearsearch.label', default: 'Clear Search')}" />
                                    </i>
                                    &nbsp;&nbsp;
                                </a>
                                <a class="pull-right box-action" style="display: block;" >
                                    &nbsp;&nbsp;
                                    <i>
                                        <g:field type="button" style="padding: 5px" class="btn cancel pull-right box-action"
                                                 name="viewPopUp" id="viewPopUp" value="${message(code: 'default.search.label', default: 'Search')}" />
                                    </i>
                                    &nbsp;&nbsp;
                                </a>
                            </td>
                        </tr>
                    </table>
                    <g:render template="dataTablesComplaint"/>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        %{--<a onclick="addParts();" href="javascript:void(0);" class="btn btn-success" id="add_part_btn">Add Parts</a>--}%
        <a href="#" class="btn" data-dismiss="modal">Close</a>
    </div>
</div>

<div id="dialog-companyDealer" class="modal hide fade in modal-complaint" style="display: none; ">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>
        <h3>Company Dealer List</h3>
    </div>
    <div class="modal-body">
        <div class="box">
            <div class="dataTables_scroll">
                <div class="span12" id="companyDealerPopUp-table">
                    <g:render template="dataTablesCompanyDealer"/>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        %{--<a onclick="addParts();" href="javascript:void(0);" class="btn btn-success" id="add_part_btn">Add Parts</a>--}%
        <a href="#" class="btn" data-dismiss="modal">OK</a>
    </div>
</div>