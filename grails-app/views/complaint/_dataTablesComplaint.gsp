<%@ page import="com.kombos.customercomplaint.Complaint" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="complaintPopUp_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 130px"><g:message code="complaint.t921TglComplain.label" default="Tanggal Complain" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 125px"><g:message code="complaint.t921NoReff.label" default="Nomor Referensi" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 115px"><g:message code="complaint.t921StaMediaKeluhan.label" default="Media Keluhan" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 110px"><g:message code="complaint.t921DealerPembelian.label" default="Main Dealer" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="complaint.t921DealerDiKomplain.label" default="Dealer Yang Di Komplain" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="complaint.t921AreaKeluhan.label" default="Area Keluhan" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="complaint.customerVehicle.label" default="Customer Vehicle" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="complaint.historyCustomerVehicle.label" default="History Customer Vehicle" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="complaint.customer.label" default="Customer" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="complaint.historyCustomer.label" default="History Customer" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="complaint.t921Km.label" default="Odometer" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="complaint.t921StaPakaiKendaraan.label" default="Status Pakai Kendaraan" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="complaint.t921Peran.label" default="Peran Pelanggan" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="complaint.t921NamaPelanggan.label" default="Nama Pelanggan" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="complaint.t921TelpRumah.label" default="Telepon Rumah" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="complaint.t921TelpHp.label" default="Telepon Selular" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="complaint.t921Fax.label" default="Faximile" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="complaint.t921Email.label" default="Email" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="complaint.t921Alamat.label" default="Alamat Pelanggan" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="complaint.t921ProfilPelanggan.label" default="Profil Pelanggan" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="complaint.t921TglKasusDiterima.label" default="Tanggal Kasus Diterima dari TAM" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="complaint.t921TglLPKDikirim.label" default="Tanggal LPK Dikirim ke TAM" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="complaint.t921DealerKomplain.label" default="Dealer Tempat Mengkomplain" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="complaint.t921StaKategoriKeluhan.label" default="Kategori Keluhan" /></div>
        </th>

		</tr>
	</thead>
</table>

<g:javascript>
var complaintPopUpTable;
var reloadComplaintPopUpTable;

function showDetailOnList(id){
    $("#dialog").modal('hide');
    $('#search_id').val(id);
    expandTableLayout();
    $('#search_id').val('');
}

$(function(){
	
	reloadComplaintPopUpTable = function() {
		complaintPopUpTable.fnDraw();
	}


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	complaintPopUpTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	$('#clear').click(function(e){
        $('#search_t921TglComplainPopUp').val("");
        $('#search_t921TglComplainPopUp_day').val("");
        $('#search_t921TglComplainPopUp_month').val("");
        $('#search_t921TglComplainPopUp_year').val("");
        $('#search_t921TglComplainAkhirPopUp').val("");
        $('#search_t921TglComplainAkhirPopUp_day').val("");
        $('#search_t921TglComplainAkhirPopUp_month').val("");
        $('#search_t921TglComplainAkhirPopUp_year').val("");
    });

    $('#viewPopUp').click(function(){
        complaintPopUpTable.fnDraw();
	});


	complaintPopUpTable = $('#complaintPopUp_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "t921TglComplain",
	"mDataProp": "t921TglComplain",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '&nbsp;&nbsp;<a href="#" onclick="showDetailOnList('+row['id']+');">'+data+'</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t921NoReff",
	"mDataProp": "t921NoReff",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t921StaMediaKeluhan",
	"mDataProp": "t921StaMediaKeluhan",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t921DealerPembelian",
	"mDataProp": "t921DealerPembelian",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t921DealerDiKomplain",
	"mDataProp": "t921DealerDiKomplain",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "t921AreaKeluhan",
	"mDataProp": "t921AreaKeluhan",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "customerVehicle",
	"mDataProp": "customerVehicle",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "historyCustomerVehicle",
	"mDataProp": "historyCustomerVehicle",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "customer",
	"mDataProp": "customer",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "historyCustomer",
	"mDataProp": "historyCustomer",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t921Km",
	"mDataProp": "t921Km",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t921StaPakaiKendaraan",
	"mDataProp": "t921StaPakaiKendaraan",
	"aTargets": [11],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t921Peran",
	"mDataProp": "t921Peran",
	"aTargets": [12],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t921NamaPelanggan",
	"mDataProp": "t921NamaPelanggan",
	"aTargets": [13],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t921TelpRumah",
	"mDataProp": "t921TelpRumah",
	"aTargets": [14],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t921TelpHp",
	"mDataProp": "t921TelpHp",
	"aTargets": [15],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t921Fax",
	"mDataProp": "t921Fax",
	"aTargets": [16],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t921Email",
	"mDataProp": "t921Email",
	"aTargets": [17],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t921Alamat",
	"mDataProp": "t921Alamat",
	"aTargets": [18],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t921ProfilPelanggan",
	"mDataProp": "t921ProfilPelanggan",
	"aTargets": [19],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t921TglKasusDiterima",
	"mDataProp": "t921TglKasusDiterima",
	"aTargets": [20],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t921TglLPKDikirim",
	"mDataProp": "t921TglLPKDikirim",
	"aTargets": [21],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t921DealerKomplain",
	"mDataProp": "t921DealerKomplain",
	"aTargets": [22],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t921StaKategoriKeluhan",
	"mDataProp": "t921StaKategoriKeluhan",
	"aTargets": [24],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var customerVehicle = $('#filter_customerVehicle input').val();
						if(customerVehicle){
							aoData.push(
									{"name": 'sCriteria_customerVehicle', "value": customerVehicle}
							);
						}

						var t921ID = $('#filter_t921ID input').val();
						if(t921ID){
							aoData.push(
									{"name": 'sCriteria_t921ID', "value": t921ID}
							);
						}

						var t921TglComplain = $('#search_t921TglComplainPopUp').val();
						var t921TglComplainDay = $('#search_t921TglComplainPopUp_day').val();
						var t921TglComplainMonth = $('#search_t921TglComplainPopUp_month').val();
						var t921TglComplainYear = $('#search_t921TglComplainPopUp_year').val();

						if(t921TglComplain){
							aoData.push(
									{"name": 'sCriteria_t921TglComplain', "value": "date.struct"},
									{"name": 'sCriteria_t921TglComplain_dp', "value": t921TglComplain},
									{"name": 'sCriteria_t921TglComplain_day', "value": t921TglComplainDay},
									{"name": 'sCriteria_t921TglComplain_month', "value": t921TglComplainMonth},
									{"name": 'sCriteria_t921TglComplain_year', "value": t921TglComplainYear}
							);
						}

						var t921TglComplainAkhir = $('#search_t921TglComplainAkhirPopUp').val();
						var t921TglComplainAkhirDay = $('#search_t921TglComplainAkhirPopUp_day').val();
						var t921TglComplainAkhirMonth = $('#search_t921TglComplainAkhirPopUp_month').val();
						var t921TglComplainAkhirYear = $('#search_t921TglComplainAkhirPopUp_year').val();

						if(t921TglComplainAkhir){
							aoData.push(
									{"name": 'sCriteria_t921TglComplainAkhir', "value": "date.struct"},
									{"name": 'sCriteria_t921TglComplainAkhir_dp', "value": t921TglComplainAkhir},
									{"name": 'sCriteria_t921TglComplainAkhir_day', "value": t921TglComplainAkhirDay},
									{"name": 'sCriteria_t921TglComplainAkhir_month', "value": t921TglComplainAkhirMonth},
									{"name": 'sCriteria_t921TglComplainAkhir_year', "value": t921TglComplainAkhirYear}
							);
						}

						var historyCustomerVehicle = $('#filter_historyCustomerVehicle input').val();
						if(historyCustomerVehicle){
							aoData.push(
									{"name": 'sCriteria_historyCustomerVehicle', "value": historyCustomerVehicle}
							);
						}

						var customer = $('#filter_customer input').val();
						if(customer){
							aoData.push(
									{"name": 'sCriteria_customer', "value": customer}
							);
						}

						var historyCustomer = $('#filter_historyCustomer input').val();
						if(historyCustomer){
							aoData.push(
									{"name": 'sCriteria_historyCustomer', "value": historyCustomer}
							);
						}

						var t921Km = $('#filter_t921Km input').val();
						if(t921Km){
							aoData.push(
									{"name": 'sCriteria_t921Km', "value": t921Km}
							);
						}

						var t921StaPakaiKendaraan = $('#filter_t921StaPakaiKendaraan input').val();
						if(t921StaPakaiKendaraan){
							aoData.push(
									{"name": 'sCriteria_t921StaPakaiKendaraan', "value": t921StaPakaiKendaraan}
							);
						}

						var t921Peran = $('#filter_t921Peran input').val();
						if(t921Peran){
							aoData.push(
									{"name": 'sCriteria_t921Peran', "value": t921Peran}
							);
						}

						var t921NamaPelanggan = $('#filter_t921NamaPelanggan input').val();
						if(t921NamaPelanggan){
							aoData.push(
									{"name": 'sCriteria_t921NamaPelanggan', "value": t921NamaPelanggan}
							);
						}

						var t921TelpRumah = $('#filter_t921TelpRumah input').val();
						if(t921TelpRumah){
							aoData.push(
									{"name": 'sCriteria_t921TelpRumah', "value": t921TelpRumah}
							);
						}

						var t921TelpHp = $('#filter_t921TelpHp input').val();
						if(t921TelpHp){
							aoData.push(
									{"name": 'sCriteria_t921TelpHp', "value": t921TelpHp}
							);
						}

						var t921Fax = $('#filter_t921Fax input').val();
						if(t921Fax){
							aoData.push(
									{"name": 'sCriteria_t921Fax', "value": t921Fax}
							);
						}

						var t921Email = $('#filter_t921Email input').val();
						if(t921Email){
							aoData.push(
									{"name": 'sCriteria_t921Email', "value": t921Email}
							);
						}

						var t921Alamat = $('#filter_t921Alamat input').val();
						if(t921Alamat){
							aoData.push(
									{"name": 'sCriteria_t921Alamat', "value": t921Alamat}
							);
						}

						var t921ProfilPelanggan = $('#filter_t921ProfilPelanggan input').val();
						if(t921ProfilPelanggan){
							aoData.push(
									{"name": 'sCriteria_t921ProfilPelanggan', "value": t921ProfilPelanggan}
							);
						}

						var t921NoReff = $('#filter_t921NoReff input').val();
						if(t921NoReff){
							aoData.push(
									{"name": 'sCriteria_t921NoReff', "value": t921NoReff}
							);
						}

						var t921TglKasusDiterima = $('#search_t921TglKasusDiterima').val();
						var t921TglKasusDiterimaDay = $('#search_t921TglKasusDiterima_day').val();
						var t921TglKasusDiterimaMonth = $('#search_t921TglKasusDiterima_month').val();
						var t921TglKasusDiterimaYear = $('#search_t921TglKasusDiterima_year').val();

						if(t921TglKasusDiterima){
							aoData.push(
									{"name": 'sCriteria_t921TglKasusDiterima', "value": "date.struct"},
									{"name": 'sCriteria_t921TglKasusDiterima_dp', "value": t921TglKasusDiterima},
									{"name": 'sCriteria_t921TglKasusDiterima_day', "value": t921TglKasusDiterimaDay},
									{"name": 'sCriteria_t921TglKasusDiterima_month', "value": t921TglKasusDiterimaMonth},
									{"name": 'sCriteria_t921TglKasusDiterima_year', "value": t921TglKasusDiterimaYear}
							);
						}

						var t921TglLPKDikirim = $('#search_t921TglLPKDikirim').val();
						var t921TglLPKDikirimDay = $('#search_t921TglLPKDikirim_day').val();
						var t921TglLPKDikirimMonth = $('#search_t921TglLPKDikirim_month').val();
						var t921TglLPKDikirimYear = $('#search_t921TglLPKDikirim_year').val();

						if(t921TglLPKDikirim){
							aoData.push(
									{"name": 'sCriteria_t921TglLPKDikirim', "value": "date.struct"},
									{"name": 'sCriteria_t921TglLPKDikirim_dp', "value": t921TglLPKDikirim},
									{"name": 'sCriteria_t921TglLPKDikirim_day', "value": t921TglLPKDikirimDay},
									{"name": 'sCriteria_t921TglLPKDikirim_month', "value": t921TglLPKDikirimMonth},
									{"name": 'sCriteria_t921TglLPKDikirim_year', "value": t921TglLPKDikirimYear}
							);
						}

						var t921StaMediaKeluhan = $('#filter_t921StaMediaKeluhan input').val();
						if(t921StaMediaKeluhan){
							aoData.push(
									{"name": 'sCriteria_t921StaMediaKeluhan', "value": t921StaMediaKeluhan}
							);
						}

						var t921AreaKeluhan = $('#filter_t921AreaKeluhan input').val();
						if(t921AreaKeluhan){
							aoData.push(
									{"name": 'sCriteria_t921AreaKeluhan', "value": t921AreaKeluhan}
							);
						}

						var t921DealerPembelian = $('#filter_t921DealerPembelian input').val();
						if(t921DealerPembelian){
							aoData.push(
									{"name": 'sCriteria_t921DealerPembelian', "value": t921DealerPembelian}
							);
						}

						var t921DealerKomplain = $('#filter_t921DealerKomplain input').val();
						if(t921DealerKomplain){
							aoData.push(
									{"name": 'sCriteria_t921DealerKomplain', "value": t921DealerKomplain}
							);
						}

						var t921DealerDiKomplain = $('#filter_t921DealerDiKomplain input').val();
						if(t921DealerDiKomplain){
							aoData.push(
									{"name": 'sCriteria_t921DealerDiKomplain', "value": t921DealerDiKomplain}
							);
						}

						var t921StaKategoriKeluhan = $('#filter_t921StaKategoriKeluhan input').val();
						if(t921StaKategoriKeluhan){
							aoData.push(
									{"name": 'sCriteria_t921StaKategoriKeluhan', "value": t921StaKategoriKeluhan}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
