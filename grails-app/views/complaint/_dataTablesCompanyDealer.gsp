
<%@ page import="com.kombos.administrasi.DealerPenjual" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="dealerPenjual_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover fixed" style="table-layout: fixed; ">
    <col width="800px" />
	<thead>
		<tr>
			<th style="border-bottom: none;padding: 5px;text-align: center; width: 800px;">
				<div><g:message code="dealerPenjual.m091NamaDealer.label" default="Nama Dealer / Workshop" /></div>
			</th>
		</tr>
		<tr>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m091NamaDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 800px;">
					<input style="width: 600px" type="text" name="search_m091NamaDealer" class="search_init" />
				</div>
			</th>
		</tr>
	</thead>
</table>

<table>
    <tr>
        <td style="padding: 5px">
            <div id="dealerPembelianLabelDiv"></div>
        </td>
        <td style="padding: 5px">
            <div id="dealerKomplainTempatLabelDiv"></div>
        </td>
        <td style="padding: 5px">
            <div id="dealerKomplainLabelDiv"></div>
        </td>
    </tr>
    <tr>
        <td style="padding: 5px">
            <g:field onclick="tambahDealer('pembelian');" type="button" class="btn cancel" name="dealerPembelian" id="dealerPembelian"
                     value="${message(code: 'default.button.complaint.dealerPembelian.label', default: 'Dealer Pembelian')}" />
        </td>
        <td style="padding: 5px">
            <g:field onclick="tambahDealer('komplainTempat');" type="button" class="btn cancel" name="dealerKomplainTempat" id="dealerKomplainTempat"
                     value="${message(code: 'default.button.complaint.tempatKomplain.label', default: 'Dealer Tempat Komplain')}" />
        </td>
        <td style="padding: 5px">
            <g:field type="button" onclick="tambahDealer('komplain');" class="btn cancel" name="dealerKomplain" id="dealerKomplain"
                     value="${message(code: 'default.button.complaint.dealerKomplain.label', default: 'Dealer yang Dikomplain')}" />
        </td>
    </tr>
</table>

<g:javascript>
var DealerPenjualTable;
var reloadDealerPenjualTable;

$(function(){
	tambahDealer = function(from){
        var from = from;
//        console.log('masuk tambah dealer');
//        console.log('ini niiihhhhh '+from);
        checkDealer =[];
        $("#companyDealerPopUp-table tbody .row-select").each(function() {
            if(this.checked){
                var id = $(this).next("input:hidden").val();
                checkDealer.push(id);
            }
        });
        if(checkDealer.length<1){
            alert('Anda belum memilih data');
            reloadDealerPenjualTable();
        }
        var checkDealerMap = JSON.stringify(checkDealer);
        $.ajax({
            url:'${request.contextPath}/complaint/tambahDealer',
            type: "POST", // Always use POST when deleting data
            data : { ids: checkDealerMap},
            success : function(data){
                if(from=='pembelian'){
                    $('#t921DealerPembelian').val(data);
                    $('#dealerPembelianLabelDiv').html($('#t921DealerPembelian').val());
                }else if(from=='komplain'){
                    $('#t921DealerDiKomplain').val(data);
                    $('#dealerKomplainLabelDiv').html($('#t921DealerDiKomplain').val());
                }else if(from=='komplainTempat'){
                    $('#t921DealerKomplain').val(data);
                    $('#dealerKomplainTempatLabelDiv').html($('#t921DealerKomplain').val());
                }
                $('#result_search').empty();
                reloadDealerPenjualTable();
            },
            error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
            }
        });
        checkDealer = [];
    }


	reloadDealerPenjualTable = function() {
		DealerPenjualTable.fnDraw();
	}

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	DealerPenjualTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	DealerPenjualTable = $('#dealerPenjual_datatables').dataTable({
		"sScrollX": "850px",
		"bScrollCollapse": true,
		"bAutoWidth" : true,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(controller:"pilihDealer",action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m091NamaDealer",
	"mDataProp": "m091NamaDealer",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data//+<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"850px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m091NamaDealer = $('#filter_m091NamaDealer input').val();
						if(m091NamaDealer){
							aoData.push(
									{"name": 'sCriteria_m091NamaDealer', "value": m091NamaDealer}
							);
						}
	
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
