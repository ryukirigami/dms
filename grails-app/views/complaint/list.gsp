
<%@ page import="com.kombos.customercomplaint.Complaint" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'complaint.label', default: 'Complaint')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>
			var show;
			var edit;
			$(function(){

				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :
							shrinkTableLayout();
							<g:remoteFunction controller="complaint" action="create"
                                              onLoading="jQuery('#spinner').fadeIn(1);"
                                              onSuccess="loadFormInstance('complaint', data, textStatus);"
                                              onComplete="jQuery('#spinner').fadeOut();" />
                            break;
                        case '_DELETE_' :
                            bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
								function(result){
									if(result){
										massDelete('complaint', '${request.contextPath}/complaint/massdelete', reloadComplaintTable);
									}
								});

							break;
				   }
				   return false;
				});

				show = function(id) {
					showInstance('complaint','${request.contextPath}/complaint/show/'+id);
				};

				edit = function(id) {
					editInstance('complaint','${request.contextPath}/complaint/edit/'+id);
				};

				shrinkTableLayout = function(){
                    $("#complaint-table").hide();
                    $("#complaint-form").css("display","block");
                }

                expandTableLayout = function(){
                    try{
                        reloadComplaintTable();
                    }catch(e){}
                    $("#complaint-table").show();
                    $("#complaint-form").css("display","none");
                }

                $("#pilihDealerModal").on("show", function() {
		$("#pilihDealerModal .btn").on("click", function(e) {
			$("#pilihDealerModal").modal('hide');
		});
	});
	$("#pilihDealerModal").on("hide", function() {
		$("#pilihDealerModal a.btn").off("click");
	});

   	loadpilihDealerModal = function(){
        $("#pilihDealerContent").empty();
		$.ajax({type:'POST', url:'${request.contextPath}/pilihDealer',
   			success:function(data,textStatus){
					$("#pilihDealerContent").html(data);
				    $("#pilihDealerModal").modal({
						"backdrop" : "static",
						"keyboard" : true,
						"show" : true
					}).css({'width': '500px','margin-left': function () {return -($(this).width() / 2);}});

   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    }


});
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
    <ul class="nav pull-right">
        <li><a class="pull-right box-action" href="#"
               style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
                    class="icon-plus"></i>&nbsp;&nbsp;
        </a></li>
        <li><a class="pull-right box-action" href="#"
               style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
                    class="icon-remove"></i>&nbsp;&nbsp;
        </a></li>
        <li class="separator"></li>
    </ul>
</div>
<div class="box">
    <div class="span12" id="complaint-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <table style="padding-right: 10px">
            <tr>
                <td>
                    <label class="control-label" for="t921TglComplain">
                        <g:message code="complaint.t921TglComplain.label" default="Tanggal" />&nbsp;
                    </label>&nbsp;&nbsp;
                </td>
                <td>
                    <div id="filter_t921TglComplain" class="controls">
                        <ba:datePicker id="search_t921TglComplain" name="search_t921TglComplain" precision="day" format="dd/MM/yyyy"  value=""  />
                        &nbsp;&nbsp;&nbsp;s.d.&nbsp;&nbsp;&nbsp;
                        <ba:datePicker id="search_t921TglComplainAkhir" name="search_t921TglComplainAkhir" precision="day" format="dd/MM/yyyy"  value=""  />
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <a class="pull-right box-action" style="display: block;" >
                        &nbsp;&nbsp;
                        <i>
                            <g:field type="button" style="padding: 5px" class="btn cancel pull-right box-action"
                                     name="clear" id="clear" value="${message(code: 'default.clearsearch.label', default: 'Clear Search')}" />
                        </i>
                        &nbsp;&nbsp;
                    </a>
                    <a class="pull-right box-action" style="display: block;" >
                        &nbsp;&nbsp;
                        <i>
                            <g:field type="button" style="padding: 5px" class="btn cancel pull-right box-action"
                                     name="view" id="view" value="${message(code: 'default.search.label', default: 'Search')}" />
                        </i>
                        &nbsp;&nbsp;
                    </a>
                </td>
            </tr>
        </table>
        <g:render template="dataTables" />
        <table>
            <tr>
                <td style="padding: 5px">
                    <export:formats formats="['excel']" />
                </td>
                %{--<td style="padding: 5px">--}%
                    %{--<a class="btn btn-primary" href="${request.contextPath}/complaint/show" >View Details Data</a>--}%
                %{--</td>--}%
                %{--<td style="padding: 5px">--}%
                    %{--<a class="btn btn_primary" href="">Service History...</a>--}%
                %{--</td>--}%
            </tr>
        </table>
    </div>
    <div class="span7" id="complaint-form" style="display: none;"></div>
</div>
<div id="pilihDealerModal" class="modal fade">
    <div class="modal-dialog" style="width: 500px;">
        <div class="modal-content" style="width: 500px; height: 800">
            <!-- dialog body -->
            <div class="modal-body" style="max-height: 800px;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="iu-content"></div>
                <div id="pilihDealerContent"/>
            </div>
            <!-- dialog buttons -->
            <div class="modal-footer"><button id='closepilihDealer' type="button" class="btn cancel">OK</button></div>
        </div>
    </div>
</div>
</body>
</html>
