
<%@ page import="com.kombos.parts.Goods" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'goods.label', default: 'View WO Transaction')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
            <g:javascript>
                 var saveForm;
                $(function(){
                    $("#exportData").click(function(e) {
                        var doc = "${doc}";
                        window.location = "${request.contextPath}/woTransaction/exportToExcel";
                    });

             });
            </g:javascript>
	</head>
	<body>
    <div id="jobTable">
        <div class="navbar box-header no-border">
            <span class="pull-left">
                <g:message code="default.list.label" args="[entityName]" />
            </span>
            <ul class="nav pull-right">
                <li></li>
                <li></li>
                <li class="separator"></li>
            </ul>
        </div>
        <div class="box">
            <div class="span12" id="operation-table">
                <fieldset>
                     <table style="padding-right: 10px">
                            <tr>
                                <td style="width: 230px">
                                    <div id="filter_wo" class="controls">
                                        <h3>${doc}</h3>
                                    </div>
                                </td>
                                <td style="width: 230px">
                                    <div id="filter_wo2" class="controls">

                                    </div>
                                </td>
                                <td>
                                    <fieldset class="buttons controls">
                                        &nbsp;&nbsp;&nbsp;
                                        <g:if test="${flash.message}">
                                            ${flash.message}
                                        </g:if>
                                    </fieldset>
                                </td>
                            </tr>
                        </table>
                    <g:javascript>
                        var doGenerate;
                        $(function(){
                            closeUpload = function(){
                                ${flash.message = ""}
                                loadPath("woTransaction/index");
                            }
                        });
                    </g:javascript>
                </fieldset>
                <br>
                    <table id="data" class="display table table-striped table-bordered table-hover dataTable" width="120%" cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px;">
                        <tr>
                            <th>
                                No.
                            </th>
                            <th>
                                Dealer Code
                            </th>
                            <th>
                                Area Code
                            </th>
                            <th>
                                Outlet Code
                            </th>
                            <th>
                                Nomor Wo (PK1)
                            </th>
                            <th>
                                Tanggal WO
                            </th>
                            <th>
                                No Polisi
                            </th>
                            <th>
                                No Vehicle
                            </th>
                            <th>
                                Vehicle Type
                            </th>
                            <th>
                                Is EM
                            </th>
                            <th>
                                WO Type
                            </th>
                            <th>
                                Operation Desc (PK2)
                            </th>
                            <th>
                                Operation Type
                            </th>
                            <th>
                                Flat Rate
                            </th>
                            <th>
                                Warranty
                            </th>

                            </tr>
                        <g:if test="${htmlData}">
                            ${htmlData}
                        </g:if>
                        <g:else>
                            <tr class="odd">
                                <td class="dataTables_empty" valign="top" colspan="15">No data available in table</td>
                            </tr>
                        </g:else>
                    </table>
                <g:field type="button" onclick="closeUpload();" class="btn btn-cancel delete" name="close" id="close" value="${message(code: 'default.button.view.label', default: 'Close')}" />
                <g:field type="button" class="btn btn-primary create" name="export" id="exportData" value="${message(code: 'default.button.view.label', default: 'Export To Excel')}" />
            </div>
        </div>
    </div>
</body>
</html>
