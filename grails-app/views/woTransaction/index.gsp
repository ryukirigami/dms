
<%@ page import="com.kombos.maintable.WoTransaction" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'woTransaction.label', default: 'Wo Transaction')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			$(function(){
                $('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_UPLOAD_' :
							loadPath("woTransaction/uploadData");
							break;
                        case '_GENERATE_' :
							loadPath("woTransaction/generateData");
							break;
				   }
				   return false;
				});
                detailData = function(doc){
                    alert(doc);
                    loadPath("woTransaction/viewData?doc="+doc);
                }
                printWotrx = function(){
                    var tanggal = $("#search_Tanggal").val();
                       checkWoTrx =[];
                        $("#woTransaction-table tbody .row-select").each(function() {
                              if(this.checked){
                                 var nRow = $(this).next("input:hidden").val();
                                    checkWoTrx.push(nRow);
                              }
                        });
                        if(checkWoTrx.length<1 ){
                            alert('Silahkan Pilih Salah satu Data Untuk Dicetak');
                            return;

                        }
                       var docNumber =  JSON.stringify(checkWoTrx);

                       window.location = "${request.contextPath}/woTransaction/printWoTrx?docNumber="+docNumber+"&tanggal="+tanggal;

                }

});
</g:javascript>
	</head>
	<body>

        <div class="navbar box-header no-border">
            <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
            <ul class="nav pull-right">
                <li>
                    <g:if test="${!session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                    <a class="pull-right box-action" href="#"
                       style="display: block;" target="_UPLOAD_">&nbsp;Upload&nbsp;</i>
                    </a>
                    </g:if>
                </li>
                <li class="separator"></li>
                <li>
                    <g:if test="${!session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                        <a class="pull-right box-action" href="#"
                           style="display: block;" target="_GENERATE_">Generate</i>&nbsp;&nbsp;
                        </a>
                    </g:if>
                </li>
                    <li class="separator"></li>
            </ul>
        </div>
	<div class="box">
		<div class="span12" id="woTransaction-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <fieldset>
                <table style="padding-right: 10px">
                    <tr>
                        <td style="width: 130px">
                            <label class="control-label" for="t951Tanggal">
                                <g:message code="auditTrail.wo.label" default="Company Dealer" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="filter_wo" class="controls">
                                <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                    <g:select name="companyDealer" id="companyDealer" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop"  noSelection="${['-':'Semua']}" />
                                </g:if>
                                <g:else>
                                    <g:select name="companyDealer" id="companyDealer" readonly="" disabled="" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                                </g:else>
                            </div>
                        </td>
                    </tr><tr>
                    <td style="width: 130px">
                        <label class="control-label" for="t951Tanggal">
                            <g:message code="auditTrail.tanggal.label" default="Tanggal Upload" />&nbsp;
                        </label>&nbsp;&nbsp;
                    </td>
                    <td>
                        <div id="filter_m777Tgl" class="controls">
                            <ba:datePicker id="search_Tanggal" name="search_Tanggal" precision="day" format="dd/MM/yyyy"  value="${new Date()}" />
                        </div>
                    </td>
                </tr>
                    <tr>
                        <td colspan="3" >
                            <div class="controls" style="right: 0">
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-primary view" name="view" id="view" >View</button>

                                <g:if test="${!session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                    <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-cancel clear" name="clear2" id="clear2" >Clear</button>
                                </g:if><g:else>
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-cancel clear" name="clear" id="clear" >Clear</button>
                                </g:else>
                            </div>
                        </td>
                    </tr>
                </table>

            </fieldset>
            <g:render template="dataTables" />
        </div>
        <button style="width: 130px;height: 30px; border-radius: 5px" class="btn btn-primary print" name="print" id="print" onclick="printWotrx();" >Download *.txt</button>
        <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
            <br><br>
            Ket : *Cabang yang belum kirim : *
            <div id="keterangan"></div>
        </g:if>

    </div>
    </body>
</html>
