
<%@ page import="com.kombos.parts.SupplySlipOwn" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'supplySlipOwn.label', default: 'Permintaan Bebas')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			$(function(){
			
				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :
							loadPath("supplySlipOwn/formAddParts?aksi=Create");
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										// massDelete('supplySlipOwn', '${request.contextPath}/supplySlipOwn/massdelete', reloadSupplySlipOwnTable);
                                        massDelete();
									}
								});                    
							
							break;
				   }    
				   return false;
				});
                massDelete = function() {
                    var recordsToDelete = [];
                    $("#supplySlipOwn-table tbody .row-select").each(function() {
                        if(this.checked){
                            var id = $(this).next("input:hidden").val();
                            recordsToDelete.push(id);
                        }

                    });

                    var json = JSON.stringify(recordsToDelete);

                    $.ajax({
                        url:'${request.contextPath}/supplySlipOwn/massdelete',
                        type: "POST", // Always use POST when deleting data
                        data: { ids: json },
                        success:function(data,textStatus){
							if(data=="not"){
								alert("Slip permintaan bebas tidak dapat dihapus,\n" +
								 "Silahkan menghubungi Admin.")
							}else{
								toastr.success("Slip permintaan bebas berhasil dihapus.");
							}
						},
                        complete: function(xhr, status) {
                            reloadSupplySlipOwnTable();
                        }
                    });
                }
				shrinkTableLayout = function(){
                    $("#supplySlipOwn-table").hide();
                    $("#supplySlipOwn-form").css("display","block");
                }

                expandTableLayout = function(){
                    try{
                        reloadComplaintTable();
                    }catch(e){}
                    $("#supplySlipOwn-table").show();
                    $("#supplySlipOwn-form").css("display","none");
                }

});
        printSupplySlipOwn = function(){
           checkSupplySlipOwn =[];
            $("#supplySlipOwn-table tbody .row-select").each(function() {
                if(this.checked){
                 var nRow = $(this).next("#salesId").val()?$(this).next("#salesId").val():"-"
                    if(nRow!="-"){
                        checkSupplySlipOwn.push(nRow);
                    }
                }
            });
            if(checkSupplySlipOwn.length<1 ){
                alert('Silahkan Pilih Salah Satu No. Permintaan Bebas Untuk Dicetak');
                return;
            }else if(checkSupplySlipOwn.length>1 ){
            	alert('Print satu persatu aja');
                return;
            }
           var idSupplySlipOwn =  JSON.stringify(checkSupplySlipOwn);
           window.location = "${request.contextPath}/supplySlipOwn/printSupplySlipOwn?idSupplySlipOwn="+idSupplySlipOwn;
        }
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="supplySlipOwn-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
            <fieldset class="buttons controls" style="padding-top: 10px;">
                <button id='printSupplySlipOwnData' onclick="printSupplySlipOwn();" type="button" class="btn btn-primary">Print Permintaan Bebas</button>
            </fieldset>
		</div>
		<div class="span11" id="supplySlipOwn-form" style="display: none;"></div>
	</div>
</body>
</html>
