
<%@ page import="com.kombos.parts.SupplySlipOwn" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>


<table id="supplySlipOwn_datatables" cellpadding="0" cellspacing="0" border="0"
       class="display table table-striped table-bordered table-hover" style="table-layout: fixed; ">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="supplySlipOwn.sorNumber.label" default="Nomor Supply Slip" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="supplySlipOwn.deliveryDate.label" default="Tanggal" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="supplySlipOwn.customerName.label" default="Kategori Slip" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="supplySlipOwn.staApprove.label" default="Status Approve" /></div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var supplySlipOwnTable;
var reloadSupplySlipOwnTable;
var updateData;
var show;
$(function(){
    updateData = function(data){
        loadPath("supplySlipOwn/formAddParts?aksi=Update&id="+data);
    }

    show = function(data){
        loadPath("supplySlipOwn/formAddParts?aksi=Show&id="+data);
    }

    var anOpen = [];
	$('#supplySlipOwn_datatables td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = supplySlipOwnTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${request.contextPath}/supplySlipOwn/sublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = supplySlipOwnTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});

  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			supplySlipOwnTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

	reloadSupplySlipOwnTable = function() {
		supplySlipOwnTable.fnDraw();
	}


    supplySlipOwnTable = $('#supplySlipOwn_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"bDestroy": true,
		"aoColumns": [
{
	"sName": "ssNumber",
	"mDataProp": "ssNumber",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
	    if(row['staApprove']=='Approved'){
	        return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" id="salesId" value="'+row['id']+'">&nbsp;&nbsp;<a href="#/supplySlipOwn" onclick="show('+row['id']+');">'+data+'</a>';
	    }else{
		    return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" id="salesId" value="'+row['id']+'">&nbsp;&nbsp;<a href="#/supplySlipOwn" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="updateData('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	    }
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"220px",
	"bVisible": true
}

,

{
	"sName": "ssDate",
	"mDataProp": "ssDate",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200",
	"bVisible": true
}
,

{
	"sName": "categorySlip",
	"mDataProp": "categorySlip",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200",
	"bVisible": true
}

,

{
	"sName": "staApprove",
	"mDataProp": "staApprove",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200",
	"bVisible": true
}

],
    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
    


    $.ajax({ "dataType": 'json',
        "type": "POST",
        "url": sSource,
        "data": aoData ,
        "success": function (json) {
            fnCallback(json);
           },
        "complete": function () {
           }
    });
}
});
});

</g:javascript>
