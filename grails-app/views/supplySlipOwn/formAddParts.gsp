<%--
  Created by IntelliJ IDEA.
  User: Ahmad Fawaz
  Date: 05/12/14
  Time: 16:31
--%>
<%@ page import="com.kombos.parts.CategorySlip; com.kombos.parts.Franc; com.kombos.parts.KlasifikasiGoods; com.kombos.parts.SupplySlipOwn" %>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="supplySlipOwn.formAddParts.label" default="${aksi} Permintaan Bebas" /></title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>
        var doSave;
        var cekStatus = "${supplySlipOwnInstance?.id}";
        $(function(){
            doSave = function(aksi){
                var checkGoods =[];
                var checkDelGoods =[];
                var aoData = []
                $("#goods_datatables tbody .row-select").each(function() {
                    if(this.checked){
                        var id = $(this).next("input:hidden").val();
                        checkGoods.push(id);
                    }else{
                        var idDel = $(this).next("input:hidden").val();
                        checkDelGoods.push(idDel);
                    }
                });

                if(checkGoods.length<1){
                    alert('Anda belum memilih data yang akan ditambahkan');
                }else{
                    for(var a=0 ; a < checkGoods.length;a++){
                        aoData.push(
                            {"name": 'qty_'+checkGoods[a]+'', "value": $('#qty_'+checkGoods[a]+'').val()}
                        );
                    }
                    aoData.push(
                        {"name": 'ids', "value": JSON.stringify(checkGoods)},
                        {"name": 'categorySlips', "value": $('#categorySlip').val()},
                        {"name": 'keterangan', "value": $('#keterangan').val()}
                    )
                    if(aksi=="update"){
                        aoData.push(
                            {"name": 'idsDel', "value": JSON.stringify(checkDelGoods)},
                            {"name": 'idSO', "value": $("#id").val()}
                        )
                    }
                    if(confirm("Anda yakin data akan disimpan?")){
                    $.ajax({
                        url:'${request.contextPath}/supplySlipOwn/'+aksi,
                        type: "POST", // Always use POST when deleting data
                        data : aoData,
                        success : function(data){
                            toastr.success('<div>Data Berhasil disimpan.</div>');
                            loadPath('supplySlipOwn/list');
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            alert('Internal Server Error');
                        }
                    });
                    }
                }

                checkGoods = [];
            }

            $('#customerName').typeahead({
                source: function (query, process) {
                    return $.get('${request.contextPath}/supplySlipOwn/listCustomer', { query: query }, function (data) {
                        return process(data.options);
                    });
                }
            });
        })

        function doAddParts(){
            $("#addPartsContent").empty();
		    $.ajax({
		        type:'POST',
		        url:'${request.contextPath}/supplySlipOwn/addParts',
                success:function(data,textStatus){
                        $("#addPartsContent").html(data);
                        $("#addPartsModal").modal({
                            "backdrop" : "static",
                            "keyboard" : true,
                            "show" : true
                        }).css({'width': '1100px','margin-left': function () {return -($(this).width() / 2);}});

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
   		    });
        }

        function ubahJumlah(param){
            var total = 0;
            var valueParam = param;
            var valQty = $('#qty_'+valueParam).val();
            var valDisc = $('#discount_'+valueParam).val();
            var valHarga = $('#harga_'+valueParam).val();
            var val1 = valQty=="" ? valHarga : valQty*valHarga;
            var val2 = valDisc=="" ? (val1=="" ? valHarga : val1)  : ( val1=="" ? valHarga - valDisc/100*valHarga : val1 - valDisc/100*val1);
            $('#jumlah_'+valueParam+"").val(val2);
            $("#goods_datatables tbody .row-select").each(function() {
                var id = $(this).next("input:hidden").val();
                total = parseInt(total) + parseInt($('#jumlah_'+id+"").val());
            });
            $("#txttotal").val(total);
        }
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="supplySlipOwn.formAddParts.label" default="${aksi} Permintaan Bebas" /></span>
</div>
<div class="box">
    <div class="span12" id="formAddParts-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
            <table>
                <tr>
                    <td style="padding: 10px">
                        <g:message code="supplySlipOwn.kategori.label" default="Kategori Slip" />
                    </td>
                    <td style="padding: 10px">
                        <g:hiddenField id="id" name="id" value="${supplySlipOwnInstance?.id}"/>
                        <g:select id="categorySlip" name="categorySlip" from="${CategorySlip?.createCriteria()?.list{eq("staDel","0");eq("companyDealer",session.userCompanyDealer);order("name")}}" optionValue="name" optionKey="id" required="" value="${supplySlipOwnInstance?.categorySlip?.id}" class="many-to-one"/>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 10px">
                        <g:message code="supplySlipOwn.keterangan.label" default="Keterangan Transaksi" />
                    </td>
                    <td style="padding: 10px">
                        <g:textArea name="keterangan" id="keterangan" value="${supplySlipOwnInstance?.keterangan}" style="resize:none" />
                    </td>
                </tr>
            </table>
            <g:if test="${aksi && aksi!="Show"}">
                <button class="btn pull-left" onclick="doAddParts();">Add Parts</button>
            </g:if>
            <br/>
            <br/>
            <br/>
            <g:render template="dataTablesAdd" />
            <fieldset class="buttons controls">
                <a class="btn cancel" onclick="loadPath('supplySlipOwn/index');">
                    <g:message code="default.button.cancel.label" default="Cancel" />
                </a>
                <g:if test="${aksi=="Create"}">
                    <button class="btn btn-primary" name="save" onclick="doSave('save');" >Create</button>
                </g:if>
                <g:elseif test="${aksi=="Update"}">
                    <button class="btn btn-primary" name="save" onclick="doSave('update');" >Update</button>
                </g:elseif>
            </fieldset>
        </div>
    <div class="span7" id="formAddParts-form" style="display: none;"></div>
</div>
<div id="addPartsModal" class="modal fade">
    <div class="modal-dialog" style="width: 1090px;">
        <div class="modal-content" style="width: 1090px;">
            <div class="modal-body" style="max-height: 800px;">
                <div id="addPartsContent"/>
                <div class="iu-content"></div>

            </div>
        </div>
    </div>
</div>
</body>
</html>
