<%@ page import="com.kombos.baseapp.AppSettingParamService; com.kombos.baseapp.AppSettingParam" %>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<html>
<!--<![endif]-->

<%
    String applicationTitle = ""
    AppSettingParam appSettingParam = AppSettingParam.findByCode(AppSettingParamService.APPLICATION_TITLE)
    if(appSettingParam.value == null){
        applicationTitle = appSettingParam.defaultValue
    }else{
        if(appSettingParam.value.length()==0){
            applicationTitle = appSettingParam.defaultValue
        }else{
            applicationTitle = appSettingParam.value
        }
    }
%>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    %{--<title><g:layoutTitle default="Grails" /></title>--}%
    <title><%= applicationTitle %></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon"
          href="${resource(dir: 'images', file: 'favicon.ico')}"
          type="image/x-icon">
    <link rel="apple-touch-icon"
          href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
    <link rel="apple-touch-icon" sizes="114x114"
          href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
    <link rel="stylesheet"
          href="${resource(dir: 'css', file: 'mobile.css')}" type="text/css">
    <g:layoutHead />
    <r:layoutResources />
    <%--<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/redmond/jquery-ui.css">--%>
    <%--<script src="http://code.jquery.com/jquery-1.9.1.js"></script>--%>
    <%--<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>--%>
    <style>
    .ui-tabs .ui-tabs-panel {
        display: block;
        border-width: 0;
        padding: 1em 1.4em;
        background: none;
        font-size: x-small;
    }

    label, input, button, select, textarea {
        font-size: 10px;
    }

    .form-horizontal .control-group {
        margin-bottom: 5px;
    }
    </style>
</head>
<body class="background-dark">
<g:layoutBody />
<r:layoutResources />
</body>
</html>
