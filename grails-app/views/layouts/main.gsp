<!DOCTYPE html>
<html>
<head>
    <g:layoutHead />
    <r:layoutResources />
</head>
<body class="background-dark">
<g:layoutBody />
<r:layoutResources />
</body>
</html>
