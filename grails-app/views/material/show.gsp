

<%@ page import="com.kombos.administrasi.Material" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'material.label', default: 'Material')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteMaterial;

$(function(){ 
	deleteMaterial=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/material/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadMaterialTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-material" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="material"
			class="table table-bordered table-hover">
			<tbody>

            <g:if test="${materialInstance?.companyDealer}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="companyDealer-label" class="property-label"><g:message
                                code="material.companyDealer.label" default="Company Dealer" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="companyDealer-label">
                        %{--<ba:editableValue
                                bean="${materialInstance}" field="updatedBy"
                                url="${request.contextPath}/Material/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadMaterialTable();" />--}%

                        ${materialInstance?.companyDealer?.encodeAsHTML()}
                    </span></td>

                </tr>
            </g:if>

				
				<g:if test="${materialInstance?.operation}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="operation-label" class="property-label"><g:message
					code="material.operation.label" default="Job" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="operation-label">
						%{--<ba:editableValue
								bean="${materialInstance}" field="operation"
								url="${request.contextPath}/Material/updatefield" type="text"
								title="Enter operation" onsuccess="reloadMaterialTable();" />--}%
							
								${materialInstance?.operation?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${materialInstance?.panelRepair}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="panelRepair-label" class="property-label"><g:message
					code="material.difficultyType.label" default="Tipe Difficult" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="panelRepair-label">
						%{--<ba:editableValue
								bean="${materialInstance}" field="panelRepair"
								url="${request.contextPath}/Material/updatefield" type="text"
								title="Enter panelRepair" onsuccess="reloadMaterialTable();" />--}%
							
								${difficultType}

						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${materialInstance?.staDel}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="staDel-label" class="property-label"><g:message
                                code="material.area.label" default="Area" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="staDel-label">
                        %{--<ba:editableValue
                                bean="${materialInstance}" field="staDel"
                                url="${request.contextPath}/Material/updatefield" type="text"
                                title="Enter staDel" onsuccess="reloadMaterialTable();" />--}%

                        ${area}

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${materialInstance?.goods}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="goods-label" class="property-label"><g:message
					code="material.goods.label" default="Nama Material" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="goods-label">
						%{--<ba:editableValue
								bean="${materialInstance}" field="goods"
								url="${request.contextPath}/Material/updatefield" type="text"
								title="Enter goods" onsuccess="reloadMaterialTable();" />--}%

							${materialInstance?.goods?.m111Nama}

						</span></td>

				</tr>
				</g:if>
			
				<g:if test="${materialInstance?.m047Qty1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m047Qty1-label" class="property-label"><g:message
					code="material.m047Qty1.label" default="Quantity" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m047Qty1-label">
						%{--<ba:editableValue
								bean="${materialInstance}" field="m047Qty1"
								url="${request.contextPath}/Material/updatefield" type="text"
								title="Enter m047Qty1" onsuccess="reloadMaterialTable();" />--}%
							
								<g:fieldValue bean="${materialInstance}" field="m047Qty1"/>
							
						</span></td>
					
				</tr>
				</g:if>



            <g:if test="${materialInstance?.satuan}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="satuan-label" class="property-label"><g:message
                                code="material.satuan.label" default="Satuan" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="satuan-label">
                        %{--<ba:editableValue
                                bean="${materialInstance}" field="satuan"
                                url="${request.contextPath}/Material/updatefield" type="text"
                                title="Enter satuan" onsuccess="reloadMaterialTable();" />--}%

                        ${materialInstance?.satuan?.encodeAsHTML()}

                    </span></td>

                </tr>
            </g:if>


            <g:if test="${materialInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="material.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${materialInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/Material/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadMaterialTable();" />--}%

                        <g:fieldValue bean="${materialInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${materialInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="material.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${materialInstance}" field="dateCreated"
								url="${request.contextPath}/Material/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadMaterialTable();" />--}%
							
								<g:formatDate date="${materialInstance?.dateCreated}"  type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>


            <g:if test="${materialInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="material.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${materialInstance}" field="createdBy"
                                url="${request.contextPath}/Material/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadMaterialTable();" />--}%

                        <g:fieldValue bean="${materialInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${materialInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="material.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${materialInstance}" field="lastUpdated"
								url="${request.contextPath}/Material/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadMaterialTable();" />--}%
							
								<g:formatDate date="${materialInstance?.lastUpdated}"  type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>
			


			
				<g:if test="${materialInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="material.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${materialInstance}" field="updatedBy"
								url="${request.contextPath}/Material/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadMaterialTable();" />--}%
							
								<g:fieldValue bean="${materialInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${materialInstance?.id}"
					update="[success:'material-form',failure:'material-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteMaterial('${materialInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
