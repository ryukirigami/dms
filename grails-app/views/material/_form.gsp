<%@ page import="com.kombos.parts.Satuan; com.kombos.parts.Goods; com.kombos.administrasi.PanelRepair; com.kombos.administrasi.RepairDifficulty; com.kombos.administrasi.Material;com.kombos.administrasi.Operation" %>
<g:javascript>
        function isNumberKey(evt)
        {
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: materialInstance, field: 'operation', 'error')} required">
	<label class="control-label" for="operation">
		<g:message code="material.operation.label" default="Job" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="operation" name="operation.id" from="${Operation.createCriteria().list(){eq("staDel", "0");order("m053NamaOperation", "asc")}}" optionKey="id" required="" value="${materialInstance?.operation?.id}" class="many-to-one"/>
	</div>
</div>
<g:javascript>
    document.getElementById('operation').focus();
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: materialInstance, field: 'panelRepair', 'error')} required">
    <label class="control-label" for="panelRepair">
        <g:message code="material.tipeDifficulty.label" default="Tipe Difficulty" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="tipeDifficulty" name="tipeDifficulty" from="${RepairDifficulty.createCriteria().list(){order("m042Tipe", "asc")}}" optionKey="id" onchange="selectArea()" required="" class="many-to-one"/>
        <g:javascript>
            selectArea();
        </g:javascript>
    </div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: materialInstance, field: 'panelRepair', 'error')} required">
    <label class="control-label" for="panelRepair">
        <g:message code="material.area.label" default="Area" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="area" name="area" from="${PanelRepair.createCriteria().list(){order("m043Area1", "asc")}}" optionKey="id" required="" value="" class="many-to-one"/>
    </div>
</div>



<div class="control-group fieldcontain ${hasErrors(bean: materialInstance, field: 'goods', 'error')} required">
	<label class="control-label" for="goods">
		<g:message code="material.goods.label" default="Nama Material" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="goods" name="goods.id" from="${Goods.createCriteria().list(){eq("staDel", "0");order("m111Nama", "asc")}}" optionKey="id" optionValue="m111Nama" required="" value="${materialInstance?.goods?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: materialInstance, field: 'm047Qty1', 'error')} required">
	<label class="control-label" for="m047Qty1">
		<g:message code="material.m047Qty1.label" default="Quantity" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m047Qty1" value="${fieldValue(bean: materialInstance, field: 'm047Qty1')}" maxlength="12" onkeypress="return isNumberKey(event)" required=""/>
	</div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: materialInstance, field: 'satuan', 'error')} required">
	<label class="control-label" for="satuan">
		<g:message code="material.satuan.label" default="Satuan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="satuan" name="satuan.id" from="${Satuan.createCriteria().list(){eq("staDel", "0");order("m118Satuan1", "asc")}}" optionKey="id" required="" value="${materialInstance?.satuan?.id}" class="many-to-one"/>
	</div>
</div>

