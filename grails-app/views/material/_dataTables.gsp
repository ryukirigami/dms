
<%@ page import="com.kombos.administrasi.Material" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>
<g:javascript>
    function isNumberKey(evt)
    {

        var charCode = (evt.which) ? evt.which : evt.keyCode
        if(charCode == 45)
            return true;

        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function regexTest(comp){
        var value = comp;

      //  var patt = /^(([\d]+\.?[\d]*)?((-\d+\.?)?[\d]*)?)$/ //expected pattern
        var patt = /^(([\d]+\.?[\d]*)?(-(\d+\.?)?[\d]*)?)$/

        var booleanExp = patt.test(value);

        if(booleanExp==false)
          $("#search_panelRepair").val("");

        return booleanExp;
    }


</g:javascript>


<table id="material_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="material.operation.label" default="Job" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="material.tipeDifficulty.label" default="Tipe Difficult" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="material.area.label" default="Area" /></div>
            </th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="material.goods.label" default="Nama Material" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="material.m047Qty1.label" default="Quantity" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="material.satuan.label" default="Satuan" /></div>
			</th>





		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_operation" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_operation" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_tipeDifficulty" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_tipeDifficulty" id="search_tipeDifficulty" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_panelRepair" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_panelRepair" id="search_panelRepair"  onkeyup="regexTest(this.value);"  class="search_init" />
                </div>
            </th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_goods" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_goods" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m047Qty1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m047Qty1" onkeypress="return isNumberKey(event)" class="search_init" />
				</div>
			</th>
	


			<th style="border-top: none;padding: 5px;">
				<div id="filter_satuan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_satuan" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var materialTable;
var reloadMaterialTable;
$(function(){
	
	reloadMaterialTable = function() {
		materialTable.fnDraw();
	}

    var recordsmaterialperpage = [];//new Array();
    var anCountrySelected;
    var jmlRecCountryPerPage=0;
    var id;

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	materialTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	    materialTable = $('#material_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsCountry = $("#material_datatables tbody .row-select");
            var jmlCountryCek = 0;
            var nRow;
            var idRec;
            rsCountry.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsmaterialperpage[idRec]=="1"){
                    jmlCountryCek = jmlCountryCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsmaterialperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecCountryPerPage = rsCountry.length;
            if(jmlCountryCek==jmlRecCountryPerPage && jmlRecCountryPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "operation",
	"mDataProp": "operation",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "panelRepair",
	"mDataProp": "panelRepair",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "staDel",
	"mDataProp": "staDel",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "goods",
	"mDataProp": "goods",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m047Qty1",
	"mDataProp": "m047Qty1",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "satuan",
	"mDataProp": "satuan",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var operation = $('#filter_operation input').val();
						if(operation){
							aoData.push(
									{"name": 'sCriteria_operation', "value": operation}
							);
						}

						var panelRepair = $('#filter_panelRepair input').val();
						if(panelRepair){
							aoData.push(
									{"name": 'sCriteria_panelRepair', "value": panelRepair}
							);
						}

						var tipeDifficulty = $('#filter_tipeDifficulty input').val();
						if(tipeDifficulty){
							aoData.push(
									{"name": 'sCriteria_tipeDifficulty', "value": tipeDifficulty}
							);
						}

						var goods = $('#filter_goods input').val();
						if(goods){
							aoData.push(
									{"name": 'sCriteria_goods', "value": goods}
							);
						}

						var m047Qty1 = $('#filter_m047Qty1 input').val();
						if(m047Qty1){
							aoData.push(
									{"name": 'sCriteria_m047Qty1', "value": m047Qty1}
							);
						}

						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}

						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						var m047Qty2 = $('#filter_m047Qty2 input').val();
						if(m047Qty2){
							aoData.push(
									{"name": 'sCriteria_m047Qty2', "value": m047Qty2}
							);
						}

						var satuan = $('#filter_satuan input').val();
						if(satuan){
							aoData.push(
									{"name": 'sCriteria_satuan', "value": satuan}
							);
						}

						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}

						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
	
	 $('.select-all').click(function(e) {

        $("#material_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordsmaterialperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsmaterialperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#material_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsmaterialperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anCountrySelected = materialTable.$('tr.row_selected');
            if(jmlRecCountryPerPage == anCountrySelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsmaterialperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
