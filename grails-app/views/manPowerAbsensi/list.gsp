
<%@ page import="com.kombos.administrasi.ManPowerAbsensi" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'manPowerAbsensi.label', default: 'Rencana Ketidakhadiran Man Power ')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />

        <g:javascript>
		$(function(){
			$('#namaManPower').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/manPowerAbsensi/kodeList', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});
        </g:javascript>
		<g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/manPowerAbsensi/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/manPowerAbsensi/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#manPowerAbsensi-form').empty();
    	$('#manPowerAbsensi-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#manPowerAbsensi-table").hasClass("span12")){
   			$("#manPowerAbsensi-table").toggleClass("span12 span5");
        }
        $("#manPowerAbsensi-form").css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
   		if($("#manPowerAbsensi-table").hasClass("span5")){
   			$("#manPowerAbsensi-table").toggleClass("span5 span12");
   		}
        $("#manPowerAbsensi-form").css("display","none");
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#manPowerAbsensi-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/manPowerAbsensi/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadManPowerAbsensiTable();
    		}
		});
		
   	}

});
        var checkin = $('#search_t017Tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
        }).on('changeDate', function(ev) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
            checkin.hide();
            $('#search_t017Tanggal2')[0].focus();
        }).data('datepicker');

        var checkout = $('#search_t017Tanggal2').datepicker({
            onRender: function(date) {
                return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');
</g:javascript>

        <g:javascript>
            $(document).ready(function()
            {
                $('#search_staHadir').change(function(){

                    if($('#search_staHadir').val()=='1'){
                        $('#search_t017JamDatang_hour').prop('disabled', false);
                        $('#search_t017JamPulang_hour').prop('disabled', false);
                        $('#search_t017JamDatang_minute').prop('disabled', false);
                        $('#search_t017JamPulang_minute').prop('disabled', false);
                    }else{
                        $('#search_t017JamDatang_hour').prop('disabled', true);
                        $('#search_t017JamPulang_hour').prop('disabled', true);
                        $('#search_t017JamDatang_minute').prop('disabled', true);
                        $('#search_t017JamPulang_minute').prop('disabled', true);
                        $('#search_t017JamPulang_hour').val("0");
                        $('#search_t017JamPulang_minute').val("0");
                        $('#search_t017JamDatang_hour').val("0");
                        $('#search_t017JamDatang_minute').val("0");
                    }
                });

                $('#search_t017JamDatang_hour').change(function(){
                    $('#search_t017JamPulang_hour').val($('#search_t017JamDatang_hour').val());
                    $('#search_t017JamPulang_minute').val($('#search_t017JamDatang_minute').val());
                });

                $('#search_t017JamPulang_hour').change(function(){

                    if( parseInt($('#search_t017JamPulang_hour').val()) < parseInt($('#search_t017JamDatang_hour').val())){
                        $('#search_t017JamPulang_hour').val($('#search_t017JamDatang_hour').val());
                    }


                });
                $('#search_t017JamDatang_minute').change(function(){
                    if( parseInt($('#search_t017JamPulang_hour').val()) == parseInt($('#search_t017JamDatang_hour').val())){
                       $('#search_t017JamPulang_minute').val($('#search_t017JamDatang_minute').val());
                    }
                });
                $('#search_t017JamPulang_minute').change(function(){
                    if( parseInt($('#search_t017JamPulang_hour').val()) == parseInt($('#search_t017JamDatang_hour').val())){
                        $('#search_t017JamDatang_minute').change(function(){
                            $('#search_t017JamPulang_minute').val($('#search_t017JamDatang_minute').val());
                        });
                        if(parseInt($('#search_t017JamDatang_minute').val()) > parseInt($('#search_t017JamPulang_minute').val())){
                            $('#search_t017JamPulang_minute').val($('#search_t017JamDatang_minute').val())
                        }
                    }
                });

            });
        </g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="manPowerAbsensi-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <fieldset>
                    <table style="padding-right: 10px">
                        <tr>
                            <td>
                                <label class="control-label" for="userRole">
                                    <g:message code="auditTrail.role.label" default="Nama Man Power" />&nbsp;
                                </label>&nbsp;&nbsp;
                            </td>
                            <td>
                                <div id="filter_namaManPower" class="controls">
                                    <input type="text" name="search_namaManPower" id="namaManPower" class="search_init .typeahead" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 130px">
                                <label class="control-label" for="t951Tanggal">
                                    <g:message code="auditTrail.tanggal.label" default="Tanggal" />&nbsp;
                                </label>&nbsp;&nbsp;
                            </td>
                            <td>
                                <div id="filter_m777Tgl" class="controls">
                                    <ba:datePicker id="search_t017Tanggal" name="search_t017Tanggal" precision="day" format="dd/MM/yyyy"  value="" />
                                    &nbsp;&nbsp;&nbsp;s.d.&nbsp;&nbsp;&nbsp;
                                    <ba:datePicker id="search_t017Tanggal2" name="search_t017Tanggal2" precision="day" format="dd/MM/yyyy"  value=""  />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label" for="userRole">
                                    <g:message code="auditTrail.role.label" default="Status Kehadiran" />&nbsp;
                                </label>&nbsp;&nbsp;
                            </td>
                            <td>
                                <div id="filter_staHadir" class="controls">
                                    <select name="search_staHadir" id="search_staHadir">
                                        <option value="" disabled selected>Pilih</option>
                                        <option value="1">Ijin</option>
                                        <option value="2">Cuti</option>
                                        <option value="3">Sakit</option>
                                        <option value="4">Alpha</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label" for="userRole">
                                    <g:message code="auditTrail.role.label" default="Jam Datang" />&nbsp;
                                </label>&nbsp;&nbsp;
                            </td>
                            <td>
                                <div id="filter_role" class="controls">
                                    <select id="search_t017JamDatang_hour" name="search_t017JamDatang_hour" style="width: 60px" required="">
                                        %{
                                            for (int i=0;i<24;i++){
                                                if(i<10){
                                                         out.println('<option value="'+i+'">0'+i+'</option>');
                                                 } else {
                                                         out.println('<option value="'+i+'">'+i+'</option>');
                                                 }

                                            }
                                        }%
                                    </select> H :
                                    <select id="search_t017JamDatang_minute" name="search_t017JamDatang_minute" style="width: 60px" required="">
                                        %{
                                            for (int i=0;i<60;i++){
                                                if(i<10){
                                                         out.println('<option value="'+i+'">0'+i+'</option>');
                                                } else {
                                                         out.println('<option value="'+i+'">'+i+'</option>');
                                                 }
                                            }
                                        }%
                                    </select> m
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label" for="userRole">
                                    <g:message code="auditTrail.role.label" default="Jam Pulang" />&nbsp;
                                </label>&nbsp;&nbsp;
                            </td>
                            <td>
                                <div id="filter_JamPulang" class="controls">
                                    <select id="search_t017JamPulang_hour" name="search_t017JamPulang_hour" style="width: 60px" required="">
                                        %{
                                            for (int i=0;i<24;i++){
                                                if(i<10){
                                                        out.println('<option value="'+i+'">0'+i+'</option>');
                                                } else {
                                                        out.println('<option value="'+i+'">'+i+'</option>');
                                                }

                                            }
                                        }%
                                    </select> H :
                                    <select id="search_t017JamPulang_minute" name="search_t017JamPulang_minute" style="width: 60px" required="">
                                        %{
                                            for (int i=0;i<60;i++){
                                                if(i<10){
                                                        out.println('<option value="'+i+'">0'+i+'</option>');
                                                } else {
                                                        out.println('<option value="'+i+'">'+i+'</option>');
                                                }
                                            }
                                        }%
                                    </select> m
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" >
                                <div class="controls" style="right: 0">
                                    <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-primary view" name="view" id="view" >View</button>
                                    <button style="width: 70px;height: 30px; border-radius: 5px" class="btn cancel" name="clear" id="clear" >Clear</button>
                                </div>
                            </td>
                        </tr>
                    </table>
                
            </fieldset>
			<g:render template="dataTables" />
		</div>
		<div class="span7" id="manPowerAbsensi-form" style="display: none;"></div>
	</div>
</body>
</html>
