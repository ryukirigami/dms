<%@ page import="com.kombos.maintable.StaHadir; com.kombos.administrasi.NamaManPower; com.kombos.administrasi.ManPowerAbsensi" %>

<g:javascript>
		$(function(){
			$('#namaManPowers').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/manPowerAbsensi/kodeList', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});
    var checkin = $('#t017Tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
        }).on('changeDate', function(ev) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate());
            checkout.setValue(newDate);
            checkin.hide();
            $('#t017Tanggal2')[0].focus();
        }).data('datepicker');

        var checkout = $('#t017Tanggal2').datepicker({
            onRender: function(date) {
                return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');
</g:javascript>
<g:javascript>
 $(document).ready(function()
         {
             $('#staHadir').change(function(){
             
                if($('#staHadir').val()=='1' || $('#staHadir').val()=='2'){
                	$('#t017JamDatang_hour').prop('disabled', false);
                	$('#t017JamPulang_hour').prop('disabled', false);
                	$('#t017JamDatang_minute').prop('disabled', false);
                	$('#t017JamPulang_minute').prop('disabled', false);
                }else{
                	$('#t017JamDatang_hour').prop('disabled', true);
                	$('#t017JamPulang_hour').prop('disabled', true);
                	$('#t017JamDatang_minute').prop('disabled', true);
                	$('#t017JamPulang_minute').prop('disabled', true);
                    $('#t017JamDatang_hour').val("0");
                    $('#t017JamPulang_hour').val("0");
                    $('#t017JamDatang_minute').val("0");
                    $('#t017JamPulang_minute').val("0");
                }
             });
             $('#t017JamDatang_hour').change(function(){
                 $('#t017JamPulang_hour').val($('#t017JamDatang_hour').val());
                 $('#t017JamPulang_minute').val($('#t017JamDatang_minute').val());
             });

             $('#t017JamPulang_hour').change(function(){

                 if( parseInt($('#t017JamPulang_hour').val()) < parseInt($('#t017JamDatang_hour').val())){
                     $('#t017JamPulang_hour').val($('#t017JamDatang_hour').val());
                 }


             });
             $('#t017JamDatang_minute').change(function(){
                 if( parseInt($('#t017JamPulang_hour').val()) == parseInt($('#t017JamDatang_hour').val())){
                     $('#t017JamPulang_minute').val($('#t017JamDatang_minute').val());
                 }
             });
             $('#t017JamPulang_minute').change(function(){
                 if( parseInt($('#t017JamPulang_hour').val()) == parseInt($('#t017JamDatang_hour').val())){
                     $('#t017JamDatang_minute').change(function(){
                         $('#t017JamPulang_minute').val($('#t017JamDatang_minute').val());
                     });
                     if(parseInt($('#t017JamDatang_minute').val()) > parseInt($('#t017JamPulang_minute').val())){
                         $('#t017JamPulang_minute').val($('#t017JamDatang_minute').val())
                     }
                 }
             });

         });

 $(document).ready(function() {
     if($('#staHadir').val()=='1' || $('#staHadir').val()=='2'){
         $('#t017JamDatang_hour').prop('disabled', false);
         $('#t017JamPulang_hour').prop('disabled', false);
         $('#t017JamDatang_minute').prop('disabled', false);
         $('#t017JamPulang_minute').prop('disabled', false);
     }else{
         $('#t017JamDatang_hour').prop('disabled', true);
         $('#t017JamPulang_hour').prop('disabled', true);
         $('#t017JamDatang_minute').prop('disabled', true);
         $('#t017JamPulang_minute').prop('disabled', true);
         $('#t017JamDatang_hour').val("0");
         $('#t017JamPulang_hour').val("0");
         $('#t017JamDatang_minute').val("0");
         $('#t017JamPulang_minute').val("0");
     }
     if($('#t017Ket').val().length>0){
         $('#hitung').text(250 - $('#t017Ket').val().length);
     }
     $('#t017Ket').keyup(function() {
         var len = this.value.length;
         if (len >= 250) {
             this.value = this.value.substring(0, 250);
             len = 250;
         }
         $('#hitung').text(250 - len);
     });
 });
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: manPowerAbsensiInstance, field: 'namaManPower', 'error')} required">
	<label class="control-label" for="namaManPower">
		<g:message code="manPowerAbsensi.namaManPower.label" default="Nama Man Power" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:select id="namaManPower" name="namaManPower.id" from="${NamaManPower.list()}" optionKey="id" required="" value="${manPowerAbsensiInstance?.namaManPower?.id}" class="many-to-one"/>--}%
        <g:textField name="namaManPower" id="namaManPowers" class="typeahead" maxlength="200" value="${manPowerAbsensiInstance?.namaManPower?.t015NamaLengkap}" autocomplete="off" required="true" />
	</div>
</div>
<g:javascript>
    document.getElementById("namaManPower").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: manPowerAbsensiInstance, field: 't017Tanggal', 'error')} required">
	<label class="control-label" for="t017Tanggal">
        <g:if test="${!manPowerAbsensiInstance?.id}">
            <g:message code="manPowerAbsensi.t017Tanggal.label" default="Tanggal" />
        </g:if>
        <g:else>
		    <g:message code="manPowerAbsensi.t017Tanggal.label" default="Periode Tanggal" />
        </g:else>
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<ba:datePicker name="t017Tanggal" precision="day"  value="${manPowerAbsensiInstance?.t017Tanggal}" format="dd-mm-yyyy" required="true"/>
    <g:if test="${!manPowerAbsensiInstance?.id}">
        s.d
        <ba:datePicker name="t017Tanggal2" precision="day" format="dd-mm-yyyy" required="true"/>
    </g:if>

	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: manPowerAbsensiInstance, field: 'staHadir', 'error')} ">
	<label class="control-label" for="staHadir">
		<g:message code="manPowerAbsensi.staHadir.label" default="Status Kehadiran" /> *
		
	</label>
	<div class="controls">
	<g:select id="staHadir" name="staHadir.id" from="${StaHadir.createCriteria().list {ne("m017NamaStaHadir", "Hadir")}}" optionKey="id" value="${manPowerAbsensiInstance?.staHadir?.id}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: manPowerAbsensiInstance, field: 't017JamDatang', 'error')} ">
	<label class="control-label" for="t017JamDatang">
		<g:message code="manPowerAbsenlosi.t017JamDatang.label" default="Jam Datang" />
		
	</label>
	<div class="controls">
		<select id="t017JamDatang_hour" name="t017JamDatang_hour" style="width: 60px" required="">
                %{
                    for (int i=0;i<24;i++){
                        if(i<10){
                            if(i==manPowerAbsensiInstance?.t017JamDatang?.getHours()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==manPowerAbsensiInstance?.t017JamDatang?.getHours()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }
                    }
                }%
            </select> H :
            <select id="t017JamDatang_minute" name="t017JamDatang_minute" style="width: 60px" required="">
                %{
                    for (int i=0;i<60;i++){
                        if(i<10){
                            if(i==manPowerAbsensiInstance?.t017JamDatang?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==manPowerAbsensiInstance?.t017JamDatang?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }
                    }
                }%
            </select> m
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: manPowerAbsensiInstance, field: 't017JamPulang', 'error')} ">
	<label class="control-label" for="t017JamPulang">
		<g:message code="manPowerAbsensi.t017JamPulang.label" default="Jam Pulang" />
		
	</label>
	<div class="controls">
		<select id="t017JamPulang_hour" name="t017JamPulang_hour" style="width: 60px" required="">
                %{
                    for (int i=0;i<24;i++){
                        if(i<10){
                            if(i==manPowerAbsensiInstance?.t017JamPulang?.getHours()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==manPowerAbsensiInstance?.t017JamPulang?.getHours()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }

                    }
                }%
            </select> H :
            <select id="t017JamPulang_minute" name="t017JamPulang_minute" style="width: 60px" required="">
                %{
                    for (int i=0;i<60;i++){
                        if(i<10){
                            if(i==manPowerAbsensiInstance?.t017JamPulang?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==manPowerAbsensiInstance?.t017JamPulang?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }
                    }
                }%
            </select> m
	</div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: manPowerAbsensiInstance, field: 't017Ket', 'error')} required">
	<label class="control-label" for="t017Ket">
		<g:message code="manPowerAbsensi.t017Ket.label" default="Keterangan" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textArea name="t017Ket" id="t017Ket" value="${manPowerAbsensiInstance?.t017Ket}" required=""/>
        <br/>
        <span id="hitung">250</span> Karakter Tersisa.
	</div>
</div>


