

<%@ page import="com.kombos.administrasi.ManPowerAbsensi" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'manPowerAbsensi.label', default: 'ManPower Rencana Hadir')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteManPowerAbsensi;

$(function(){ 
	deleteManPowerAbsensi=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/manPowerAbsensi/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadManPowerAbsensiTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-manPowerAbsensi" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="manPowerAbsensi"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${manPowerAbsensiInstance?.namaManPower}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="namaManPower-label" class="property-label"><g:message
					code="manPowerAbsensi.namaManPower.label" default="Nama Man Power" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="namaManPower-label">
						%{--<ba:editableValue
								bean="${manPowerAbsensiInstance}" field="namaManPower"
								url="${request.contextPath}/ManPowerAbsensi/updatefield" type="text"
								title="Enter namaManPower" onsuccess="reloadManPowerAbsensiTable();" />--}%
							
								%{--<g:link controller="namaManPower" action="show" id="${manPowerAbsensiInstance?.namaManPower?.id}">${manPowerAbsensiInstance?.namaManPower?.encodeAsHTML()}</g:link>--}%
                        <g:fieldValue bean="${manPowerAbsensiInstance?.namaManPower}" field="t015NamaLengkap"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${manPowerAbsensiInstance?.t017Tanggal}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t017Tanggal-label" class="property-label"><g:message
					code="manPowerAbsensi.t017Tanggal.label" default="Periode Tanggal" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t017Tanggal-label">
						%{--<ba:editableValue
								bean="${manPowerAbsensiInstance}" field="t017Tanggal"
								url="${request.contextPath}/ManPowerAbsensi/updatefield" type="text"
								title="Enter t017Tanggal" onsuccess="reloadManPowerAbsensiTable();" />--}%
							
								<g:formatDate date="${manPowerAbsensiInstance?.t017Tanggal}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${manPowerAbsensiInstance?.staHadir}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staHadir-label" class="property-label"><g:message
					code="manPowerAbsensi.staHadir.label" default="Status Hadir" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staHadir-label">
						%{--<ba:editableValue
								bean="${manPowerAbsensiInstance}" field="staHadir"
								url="${request.contextPath}/ManPowerAbsensi/updatefield" type="text"
								title="Enter staHadir" onsuccess="reloadManPowerAbsensiTable();" />--}%
							
								%{--<g:link controller="staHadir" action="show" id="${manPowerAbsensiInstance?.staHadir?.id}">${manPowerAbsensiInstance?.staHadir?.encodeAsHTML()}</g:link>--}%
                            <g:fieldValue bean="${manPowerAbsensiInstance?.staHadir}" field="m017NamaStaHadir"/>
						</span></td>
					
				</tr>
				</g:if>
			
			
				<g:if test="${manPowerAbsensiInstance?.t017JamDatang}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t017JamDatang-label" class="property-label"><g:message
					code="manPowerAbsensi.t017JamDatang.label" default="Jam Datang" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t017JamDatang-label">
						%{--<ba:editableValue
								bean="${manPowerAbsensiInstance}" field="t017JamDatang"
								url="${request.contextPath}/ManPowerAbsensi/updatefield" type="text"
								title="Enter t017JamDatang" onsuccess="reloadManPowerAbsensiTable();" />--}%
							
								<g:formatDate date="${manPowerAbsensiInstance?.t017JamDatang}"  format="HH:mm"/>
						</span></td>
					
				</tr>
				</g:if>
				<g:if test="${manPowerAbsensiInstance?.t017JamPulang}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t017JamPulang-label" class="property-label"><g:message
					code="manPowerAbsensi.t017JamPulang.label" default="Jam Pulang" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t017JamPulang-label">
						%{--<ba:editableValue
								bean="${manPowerAbsensiInstance}" field="t017JamPulang"
								url="${request.contextPath}/ManPowerAbsensi/updatefield" type="text"
								title="Enter t017JamPulang" onsuccess="reloadManPowerAbsensiTable();" />--}%
							
								<g:formatDate date="${manPowerAbsensiInstance?.t017JamPulang}"  format="HH:mm"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${manPowerAbsensiInstance?.t017Ket}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t017Ket-label" class="property-label"><g:message
					code="manPowerAbsensi.t017Ket.label" default="Ket" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t017Ket-label">
						%{--<ba:editableValue
								bean="${manPowerAbsensiInstance}" field="t017Ket"
								url="${request.contextPath}/ManPowerAbsensi/updatefield" type="text"
								title="Enter t017Ket" onsuccess="reloadManPowerAbsensiTable();" />--}%
							
								<g:fieldValue bean="${manPowerAbsensiInstance}" field="t017Ket"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${manPowerAbsensiInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="manPowerAbsensi.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${manPowerAbsensiInstance}" field="lastUpdProcess"
								url="${request.contextPath}/ManPowerAbsensi/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadManPowerAbsensiTable();" />--}%
							
								<g:fieldValue bean="${manPowerAbsensiInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${manPowerAbsensiInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="manPowerAbsensi.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${manPowerAbsensiInstance}" field="dateCreated"
								url="${request.contextPath}/ManPowerAbsensi/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadManPowerAbsensiTable();" />--}%
							
								<g:formatDate date="${manPowerAbsensiInstance?.dateCreated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>


            <g:if test="${manPowerAbsensiInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="manPowerAbsensi.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${manPowerAbsensiInstance}" field="createdBy"
                                url="${request.contextPath}/ManPowerAbsensi/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadManPowerAbsensiTable();" />--}%

                        <g:fieldValue bean="${manPowerAbsensiInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${manPowerAbsensiInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="manPowerAbsensi.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${manPowerAbsensiInstance}" field="lastUpdated"
								url="${request.contextPath}/ManPowerAbsensi/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadManPowerAbsensiTable();" />--}%
							
								<g:formatDate date="${manPowerAbsensiInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${manPowerAbsensiInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="manPowerAbsensi.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${manPowerAbsensiInstance}" field="updatedBy"
                                url="${request.contextPath}/ManPowerAbsensi/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadManPowerAbsensiTable();" />--}%

                        <g:fieldValue bean="${manPowerAbsensiInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${manPowerAbsensiInstance?.id}"
					update="[success:'manPowerAbsensi-form',failure:'manPowerAbsensi-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteManPowerAbsensi('${manPowerAbsensiInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
