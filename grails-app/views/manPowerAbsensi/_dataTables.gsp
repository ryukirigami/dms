
<%@ page import="com.kombos.administrasi.ManPowerAbsensi" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="manPowerAbsensi_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="manPowerAbsensi.namaManPower.label" default="Nama Man Power" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; width:300px">
				<div><g:message code="manPowerAbsensi.t017Tanggal.label" default="Tanggal " /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="manPowerAbsensi.staHadir.label" default="Status Hadir" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="manPowerAbsensi.t017JamDatang.label" default="Jam Datang" /></div>
			</th>
			

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="manPowerAbsensi.t017JamPulang.label" default="Jam Pulang" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="manPowerAbsensi.t017Ket.label" default="Ket" /></div>
			</th>
		</tr>
		%{--<tr>--}%
		%{----}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_namaManPower" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_namaManPower" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%

			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_t017Tanggal" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >--}%
					%{--<input type="hidden" name="search_t017Tanggal" value="date.struct">--}%
					%{--<input type="hidden" name="search_t017Tanggal_day" id="search_t017Tanggal_day" value="">--}%
					%{--<input type="hidden" name="search_t017Tanggal_month" id="search_t017Tanggal_month" value="">--}%
					%{--<input type="hidden" name="search_t017Tanggal_year" id="search_t017Tanggal_year" value="">--}%
					%{--<input type="text" data-date-format="dd/mm/yyyy" name="search_t017Tanggal_dp" value="" id="search_t017Tanggal" class="search_init">--}%
					%{--<input type="hidden" name="search_t017Tanggal2" value="date.struct">--}%
					%{--<input type="hidden" name="search_t017Tanggal_day2" id="search_t017Tanggal_day" value="">--}%
					%{--<input type="hidden" name="search_t017Tanggal_month2" id="search_t017Tanggal_month" value="">--}%
					%{--<input type="hidden" name="search_t017Tanggal_year2" id="search_t017Tanggal_year" value="">--}%
					%{--<input type="text" data-date-format="dd/mm/yyyy" name="search_t017Tanggal_dp2" value="" id="search_t017Tanggal2" class="search_init">--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_staHadir" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_staHadir" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_t017JamDatang" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_t017JamDatang" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%

			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_t022JamMulai2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_t017JamPulang" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_t017Ket" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_t017Ket" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			 %{----}%

		%{--</tr>--}%
	</thead>
</table>

<g:javascript>
var manPowerAbsensiTable;
var reloadManPowerAbsensiTable;
$(function(){
	
	reloadManPowerAbsensiTable = function() {
		manPowerAbsensiTable.fnDraw();
	}
	var recordsManPowerAbsensiperpage = [];//new Array();
    var anManPowerAbsensiSelected;
    var jmlRecManPowerAbsensiPerPage=0;
    var id;

 $('#clear').click(function(e){
        $('#search_t017Tanggal').val("");
        $('#search_t017Tanggal_day').val("");
        $('#search_t017Tanggal_month').val("");
        $('#search_t017Tanggal_year').val("");
        $('#search_t017Tanggal2').val("");
        $('#search_t017Tanggal2_day').val("");
        $('#search_t017Tanggal2_month').val("");
        $('#search_t017Tanggal2_year').val("");
        $('#filter_namaManPower input').val("");
        $('#filter_staHadir select').val("");
        $('#search_t017JamPulang_hour').val("0");
        $('#search_t017JamPulang_minute').val("0");
        $('#search_t017JamDatang_hour').val("0");
        $('#search_t017JamDatang_minute').val("0");
        manPowerAbsensiTable.fnDraw();

//        reloadAuditTrailTable();
	});
    $('#view').click(function(e){
        e.stopPropagation();
		manPowerAbsensiTable.fnDraw();
//        reloadAuditTrailTable();
	});
$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	manPowerAbsensiTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	manPowerAbsensiTable = $('#manPowerAbsensi_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            //alert( $('li.active').text() );
            var rsManPowerAbsensi = $("#manPowerAbsensi_datatables tbody .row-select");
            var jmlManPowerAbsensiCek = 0;
            var nRow;
            var idRec;
            rsManPowerAbsensi.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsManPowerAbsensiperpage[idRec]=="1"){
                    jmlManPowerAbsensiCek = jmlManPowerAbsensiCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsManPowerAbsensiperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecManPowerAbsensiPerPage = rsManPowerAbsensi.length;
            if(jmlManPowerAbsensiCek==jmlRecManPowerAbsensiPerPage && jmlRecManPowerAbsensiPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "namaManPower",
	"mDataProp": "namaManPower",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "t017Tanggal",
	"mDataProp": "t017Tanggal",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "staHadir",
	"mDataProp": "staHadir",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "t017JamDatang",
	"mDataProp": "t017JamDatang",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "t017JamPulang",
	"mDataProp": "t017JamPulang",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t017Ket",
	"mDataProp": "t017Ket",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

 
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var namaManPower = $('#filter_namaManPower input').val();
						if(namaManPower){
							aoData.push(
									{"name": 'sCriteria_namaManPower', "value": namaManPower}
							);
						}

						var t017Tanggal = $('#search_t017Tanggal').val();
						var t017TanggalDay = $('#search_t017Tanggal_day').val();
						var t017TanggalMonth = $('#search_t017Tanggal_month').val();
						var t017TanggalYear = $('#search_t017Tanggal_year').val();

						var t017Tanggal2 = $('#search_t017Tanggal2').val();
						var t017TanggalDay2 = $('#search_t017Tanggal2_day').val();
						var t017TanggalMonth2 = $('#search_t017Tanggal2_month').val();
						var t017TanggalYear2 = $('#search_t017Tanggal2_year').val();
						
						if(t017Tanggal){
							aoData.push(
									{"name": 'sCriteria_t017Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_t017Tanggal_dp', "value": t017Tanggal},
									{"name": 'sCriteria_t017Tanggal_day', "value": t017TanggalDay},
									{"name": 'sCriteria_t017Tanggal_month', "value": t017TanggalMonth},
									{"name": 'sCriteria_t017Tanggal_year', "value": t017TanggalYear}
							);
						}
	
						
						if(t017Tanggal2){
							aoData.push(
									{"name": 'sCriteria_t017Tanggal2', "value": "date.struct"},
									{"name": 'sCriteria_t017Tanggal2_dp', "value": t017Tanggal2},
									{"name": 'sCriteria_t017Tanggal2_day', "value": t017TanggalDay2},
									{"name": 'sCriteria_t017Tanggal2_month', "value": t017TanggalMonth2},
									{"name": 'sCriteria_t017Tanggal2_year', "value": t017TanggalYear2}
							);
						}
						
						var staHadir = $('#filter_staHadir select').val();
						if(staHadir){
							aoData.push(
									{"name": 'sCriteria_staHadir', "value": staHadir}
							);
						}


						var t017JamDatang = $('#search_t017JamDatang_hour').val() + ":" + $('#search_t017JamDatang_minute').val();

						if(t017JamDatang){
							aoData.push(
									{"name": 'sCriteria_t017JamDatang', "value": t017JamDatang}

							);
						}


                        var t017JamPulang =  $('#search_t017JamPulang_hour').val() + ":" + $('#search_t017JamPulang_minute').val();
                        if(t017JamPulang){
                            aoData.push(
                                    {"name": 'sCriteria_t017JamPulang', "value": t017JamPulang}
                            );
                        }
						var t017Ket = $('#filter_t017Ket input').val();
						if(t017Ket){
							aoData.push(
									{"name": 'sCriteria_t017Ket', "value": t017Ket}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#manPowerAbsensi_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordsManPowerAbsensiperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsManPowerAbsensiperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#manPowerAbsensi_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsManPowerAbsensiperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anManPowerAbsensiSelected = manPowerAbsensiTable.$('tr.row_selected');
            if(jmlRecManPowerAbsensiPerPage == anManPowerAbsensiSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsManPowerAbsensiperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>