<%@ page import="com.kombos.administrasi.KabKota" %>


%{--<div class="control-group fieldcontain ${hasErrors(bean: kabKotaInstance, field: 'm002ID', 'error')} ">--}%
	%{--<label class="control-label" for="m002ID">--}%
		%{--<g:message code="kabKota.m002ID.label" default="ID" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
    <g:if test="${kabKotaInstance?.m002ID}">
        <g:hiddenField name="m002ID" value="${kabKotaInstance?.m002ID}" />
    </g:if>
    <g:else>
        <g:hiddenField name="m002ID" value="${KabKota.last().m002ID.toInteger() + 1}" />
    </g:else>
	%{--</div>--}%
%{--</div>--}%

<div class="control-group fieldcontain ${hasErrors(bean: kabKotaInstance, field: 'provinsi', 'error')} ">
    <label class="control-label" for="provinsi">
        <g:message code="kabKota.provinsi.label" default="Provinsi *" />

    </label>
    <div class="controls">
        <g:select id="provinsi" name="provinsi.id" from="${com.kombos.administrasi.Provinsi.createCriteria().list { eq("staDel","0")}}" optionKey="id" required="" value="${kabKotaInstance?.provinsi?.id}" class="many-to-one"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: kabKotaInstance, field: 'm002NamaKabKota', 'error')} ">
	<label class="control-label" for="m002NamaKabKota">
		<g:message code="kabKota.m002NamaKabKota.label" default="Nama Kab Kota * " />
		
	</label>
	<div class="controls">
	<g:textField name="m002NamaKabKota" value="${kabKotaInstance?.m002NamaKabKota}" required="required" maxlength="45" />
	</div>
</div>

