

<%@ page import="com.kombos.administrasi.KabKota" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'kabKota.label', default: 'KabKota')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteKabKota;

$(function(){ 
	deleteKabKota=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/kabKota/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadKabKotaTable();
   				expandTableLayout('kabKota');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-kabKota" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="kabKota"
			class="table table-bordered table-hover">
			<tbody>

				

				<g:if test="${kabKotaInstance?.m002ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m002ID-label" class="property-label"><g:message
					code="kabKota.m002ID.label" default="ID" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m002ID-label">
						%{--<ba:editableValue
								bean="${kabKotaInstance}" field="m002ID"
								url="${request.contextPath}/KabKota/updatefield" type="text"
								title="Enter m002ID" onsuccess="reloadKabKotaTable();" />--}%
							
								<g:fieldValue bean="${kabKotaInstance}" field="m002ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kabKotaInstance?.m002NamaKabKota}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m002NamaKabKota-label" class="property-label"><g:message
					code="kabKota.m002NamaKabKota.label" default="Nama Kab Kota" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m002NamaKabKota-label">
						%{--<ba:editableValue
								bean="${kabKotaInstance}" field="m002NamaKabKota"
								url="${request.contextPath}/KabKota/updatefield" type="text"
								title="Enter m002NamaKabKota" onsuccess="reloadKabKotaTable();" />--}%
							
								<g:fieldValue bean="${kabKotaInstance}" field="m002NamaKabKota"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kabKotaInstance?.provinsi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="provinsi-label" class="property-label"><g:message
					code="kabKota.provinsi.label" default="Provinsi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="provinsi-label">
						%{--<ba:editableValue
								bean="${kabKotaInstance}" field="provinsi"
								url="${request.contextPath}/KabKota/updatefield" type="text"
								title="Enter provinsi" onsuccess="reloadKabKotaTable();" />--}%
							
								%{--<g:link controller="provinsi" action="show" id="${kabKotaInstance?.provinsi?.id}">${kabKotaInstance?.provinsi?.encodeAsHTML()}</g:link>--}%
                        <g:fieldValue bean="${kabKotaInstance?.provinsi}" field="m001NamaProvinsi"/>
						</span></td>

				</tr>
				</g:if>

            <g:if test="${kabKotaInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="kabKota.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${kabKotaInstance}" field="createdBy"
                                url="${request.contextPath}/KabKota/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadKabKotaTable();" />--}%

                        <g:fieldValue bean="${kabKotaInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${kabKotaInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="kabKota.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${kabKotaInstance}" field="dateCreated"
                                url="${request.contextPath}/KabKota/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadKabKotaTable();" />--}%

                        <g:formatDate date="${kabKotaInstance?.dateCreated}" format="EEEE, dd MMMM yyyy HH:mm"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${kabKotaInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="kabKota.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${kabKotaInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/KabKota/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadKabKotaTable();" />--}%

                        <g:fieldValue bean="${kabKotaInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${kabKotaInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="kabKota.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${kabKotaInstance}" field="lastUpdated"
                                url="${request.contextPath}/KabKota/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadKabKotaTable();" />--}%

                        <g:formatDate date="${kabKotaInstance?.lastUpdated}" format="EEEE, dd MMMM yyyy HH:mm"/>

                    </span></td>

                </tr>
            </g:if>


            <g:if test="${kabKotaInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="kabKota.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${kabKotaInstance}" field="updatedBy"
								url="${request.contextPath}/KabKota/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadKabKotaTable();" />--}%
							
								<g:fieldValue bean="${kabKotaInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('kabKota');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${kabKotaInstance?.id}"
					update="[success:'kabKota-form',failure:'kabKota-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteKabKota('${kabKotaInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
