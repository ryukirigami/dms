<%@ page import="com.kombos.parts.ETA" %>

<r:require modules="baseapplayout, baseapplist" />
<div class="navbar box-header no-border">
    Input ETA PO
</div>
<table class="table table-bordered table-hover">
<tbody>
   <tr>
       <td class="span2" style="text-align: right;">
        <label class="control-label">
            <g:message code="ETA.namaVendor.label" default="Nama Vendor" />

        </label>

      </td>

       <td class="span3">
        <label class="control-label" for="namaVendor" >
            ${namaVendor}
        </label>
    </td>
     </tr>
</div>

<tr>
    <td class="span2" style="text-align: right;">
        <label class="control-label" for="po">
        <g:message code="ETA.idpo.label" default="Nomor PO" />

    </label>
  </td>
    <td class="span3" >
           <g:textField name="nomorPO" id="nomorPO" value="${nomorPO}" readonly="true"/>
     </td>
</tr>

<tr>
    <td class="span2" style="text-align: right;">
        <label class="control-label" for="t165ETA">
        <g:message code="ETA.tanggalPO.label" default="Tanggal PO" />

    </label>
    </td>
    <td class="span3">
        <label class="control-label" for="tglPO" >
            ${tanggalPO}
        </label>
    </td>
</tr>
<tr>
    <td class="span2" style="text-align: right;">
        <label class="control-label" for="kodePart">
        <g:message code="ETA.kodePart.label" default="Kode Part" />

    </label>
    </td>
    <td class="span3">
    <g:textField name="kodeGoods" id="kodeGoods" value="${kodePart}" readonly="true"/>    </td>
</tr>
<tr>
    <td class="span2" style="text-align: right;">
        <label class="control-label" for="namaPart">
        <g:message code="ETA.namaPart.label" default="Nama Part" />

    </label>
    </td>
    <td class="span3">
    <label class="control-label" for="namaPart">
            ${namaPart}
        </label>
    </td>
 </tr>

<tr>
 <td class="span2" style="text-align: right;">
<div class="control-group fieldcontain ${hasErrors(bean: ETAInstance, field: 't165Qty1', 'error')} ">
    <label class="control-label" for="t165Qty1">
        <g:message code="ETA.t165Qty1.label" default="Qty Issue" />

    </label>
</td>
<td class="span3">
<div class="controls">
        <g:field type="number" name="qtyETA" id="qtyETA" />
    </div>
</div>
</td>
</tr>
<tr>
    <td class="span2" style="text-align: right;">
<div class="control-group fieldcontain ${hasErrors(bean: POInstance, field: 't164TglPO', 'error')} ">
    <label class="control-label" for="t164TglPO">
        <g:message code="PO.t164TglPO.label" default="Tgl ETA" />

    </label>
</td>
    <td class="span3">
        <div class="controls">
        <ba:datePicker name="tglETA" id="tglETA" precision="day"  format="yyyy-MM-dd"/>
        H :
        <select id="jamETA" name="jamETA"  style="width: 60px" required="">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){

                        out.println('<option value="'+i+'">0'+i+'</option>');


                    } else {

                        out.println('<option value="'+i+'">'+i+'</option>');

                    }

                }
            }%
        </select>
        <select id="menitETA" name="menitETA" style="width: 60px" required="">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){

                        out.println('<option value="'+i+'">0'+i+'</option>');


                    } else {

                        out.println('<option value="'+i+'">'+i+'</option>');

                    }
                }
            }%
        </select> m
    </div>
</td>
</div>
</tr>
</tbody>
</table>
<div class="modal-footer">
 </div>
<g:field type="button" onclick="addETA();" class="btn btn-primary create" name="tambah" id="tambah" value="${message(code: 'default.button.tambahETA.label', default: 'Tambah ETA')}" />


