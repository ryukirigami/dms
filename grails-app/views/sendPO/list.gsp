
<%@ page import="com.kombos.parts.PO" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'PO.label', default: 'Send PO')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			var searchPO;
			var exportPO;
			var printPO;
			var sendPO;
			var checkPO =[];

			$(function(){


				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('PO');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('PO', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('PO', '${request.contextPath}/PO/massdelete', reloadPOTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('PO','${request.contextPath}/PO/show/'+id);
				};
				
				edit = function(id) {
					editInstance('PO','${request.contextPath}/PO/edit/'+id);
				};



            tambahPO = function(){

               var id;
                $("#PO-table tbody .row-select").each(function() {
                    if(this.checked){
                        id = $(this).next("input:hidden").val();
                        checkPO.push(id);
                    }
                });
                if(checkPO.length<1){
                    alert('Harap pilih PO');
                    return 0;
                }
                else{
                    var checkPOMap = JSON.stringify(checkPO);

                   return checkPOMap;
                }

            checkPO = [];

          };

          exportPO2 = function(){
            var checkPO = tambahPO();
            if(checkPO==0){
                return;
            }else{
                 $.ajax({type:'POST', url:'${request.contextPath}/SendPO/exportPO',
                success:function(){
                        $('#spinner').fadeOut();

                },
                data : {checkedPO : checkPO},
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
            }

          }


           exportPO = function(){
                  checkPO =[];
                   var idBlockStok;
                    $("#PO-table tbody .row-select").each(function() {
                        if(this.checked){
                            idPO = $(this).next("input:hidden").val();
                           checkPO.push(idPO);
                        }
                    });
                    if(checkPO.length<1){
                        alert('Anda belum memilih data yang akan diexport');
                        return;

                    }

                    var idPOs =  JSON.stringify(checkPO);


                    window.location = "${request.contextPath}/sendPO/exportPO?idPOs="+idPOs;

           }


          printPO = function(){
                   checkPO =[];
                   var idBlockStok;
                    $("#PO-table tbody .row-select").each(function() {
                        if(this.checked){
                            idPO = $(this).next("input:hidden").val();
                           checkPO.push(idPO);
                        }
                    });
                    if(checkPO.length<1){
                        alert('Anda belum memilih data yang akan dicetak');
                        return;

                    }

                var idPOs =  JSON.stringify(checkPO);
                var myWindow = window.open("${request.contextPath}/sendPO/printPO?idPOs="+idPOs);


          }

          sendPO = function(){
              checkPO =[];
                   var idPO;
                    $("#PO-table tbody .row-select").each(function() {
                        if(this.checked){
                            idPO = $(this).next("input:hidden").val();
                           checkPO.push(idPO);
                        }
                    });
                    if(checkPO.length<1 || checkPO.length > 1){
                        alert('Pilih satu data yang akan dikirim');
                        return;

                    }

                    var idPOs = JSON.stringify(checkPO);

                   $.ajax({type:'POST', url:'${request.contextPath}/SendPO/sendPOSPLD',
                    success:function(){
                            $('#spinner').fadeOut();
                            toastr.success('<div>PO telah dikirim via email</div>');
                    },
                    data : {idPOs : idPOs},
                    error:function(XMLHttpRequest,textStatus,errorThrown){},
                    complete:function(XMLHttpRequest,textStatus){
                        $('#spinner').fadeOut();


                    }
                });

          }



});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>

	</div>
	<div class="box">
		<div class="span12" id="PO-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <fieldset>
                <table style="padding-right: 10px">
                    <tr>
                        <td style="width: 130px">
                            <label class="control-label" for="tanggalPO">
                                <g:message code="po.tanggal.label" default="Tanggal PO" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="tanggal_invoice" class="controls">
                                %{--<ba:datePicker id="search_tglStart" name="search_tglStart" precision="day" format="dd/MM/yyyy"  value="" />--}%
                                <ba:datePicker id="search_tglStart" name="search_tglStart" precision="day" format="dd-mm-yyyy"  value="" />
                                &nbsp;-&nbsp;
                                <ba:datePicker id="search_tglEnd" name="search_tglEnd" precision="day" format="dd-mm-yyyy"  value="" />
                                %{--<ba:datePicker id="search_tglEnd" name="search_tglEnd" precision="day" format="dd/MM/yyyy"  value="" />--}%
                            </div>
                        </td>
                    </tr>
                </table>

              </fieldset>
              <fieldset>
                  <legend>Kriteria PO</legend>
                <table style="padding-right: 10px" >
                    <tr>
                        <td>
                            <input type="checkbox" id="poBlmDikirim" name="poBlmDikirim" value="1">&nbsp;PO Belum dikirim/diprint &nbsp;
                        </td>
                        <td>
                            <input type="checkbox" id="poSdhDikirim" name="poSdhDikirim" value="1">&nbsp;PO Sudah dikirim/diprint

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="checkbox" id="poETABelumLengkap" name="poETABelumLengkap" value="1">&nbsp;PO dengan ETA Belum Lengkap &nbsp;
                        </td>
                        <td>
                            <input type="checkbox" id="poSPLD" name="poSPLD" value="1">&nbsp;PO SPLD

                        </td>
                    </tr>
                    <tr>
                        <td >
                            <input type="checkbox" id="poETALengkap" name="poETALengkap" value="1">&nbsp;PO dengan ETA Lengkap
                        </td>
                        <td>
                            <input type="checkbox" id="poNonSPLD" name="poNonSPLD" value="1">&nbsp;PO Non SPLD

                        </td>
                    </tr>
                </table>
                <br/>
                <table style="padding-right: 10px">
                    <tr>
                        <td colspan="3" >
                            <div class="controls" style="right: 0">
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn-primary view" name="view" id="view">Search</button>
                            </div>
                        </td>
                    </tr>
                </table>
            </fieldset>
			<g:render template="dataTables" />
            <button style="width: 170px;height: 30px; border-radius: 5px" class="btn-primary add" name="sendEmail" id="sendEmail" onclick="sendPO()">Send Email To SPLD</button>
            <button style="width: 170px;height: 30px; border-radius: 5px" class="btn-primary add" name="exportText" id="exportText" onclick="exportPO()">Export To Txt File</button>
            <button style="width: 170px;height: 30px; border-radius: 5px" class="btn-primary add" name="printPO" id="printPO" onclick="printPO()">Print PO</button>

		</div>
		<div class="span7" id="PO-form" style="display: none;"></div>
	</div>
%{--//woke--}%
</body>
</html>

