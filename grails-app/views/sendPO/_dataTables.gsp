
<%@ page import="com.kombos.parts.PO" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="PO_datatables_${idTable}" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>


        <th></th>
        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="PO.vendor.label" default="Vendor" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="PO.t164NoPO.label" default="No PO" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="PO.t164TglPO.label" default="Tgl PO" /></div>
        </th>



        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="PO.tipeOrder.label" default="Tipe Order" /></div>
        </th>





    </tr>
   </thead>
</table>

<g:javascript>
var POTable;
var reloadPOTable;
$(function(){
	var anOpen = [];

	 $('#view').click(function(e){
        e.stopPropagation();
		POTable.fnDraw();
//        reloadAuditTrailTable();
	});

	$('#clear').click(function(e){

        $('#search_tglStart').val("");
        $('#search_tglEnd').val("");
        $('#search_vendor').val("");
        $('#search_t164NoPO').val("");
        $('#filter_t164TglPO input').val("");
        $('#search_tipeOrder').val("");


        $("#poETABelumLengkap").prop("checked", false);
        $("#poETALengkap").prop("checked", false);
        $("#poSPLD").prop("checked", false);
        $("#poNonSPLD").prop("checked", false);
        $("#poBlmDikirim").prop("checked",false);
        $("#poSdhDikirim").prop("checked",false);


        e.stopPropagation();
		POTable.fnDraw();
//        reloadAuditTrailTable();
	});

	$('#PO_datatables_${idTable} td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = POTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${request.contextPath}/sendPO/sublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = POTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});

  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			POTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

    $('#PO_datatables_${idTable} a.edit').live('click', function (e) {
        e.preventDefault();

        var nRow = $(this).parents('tr')[0];

        if ( nEditing !== null && nEditing != nRow ) {
            restoreRow( POTable, nEditing );
            editRow( POTable, nRow );
            nEditing = nRow;
        }
        else if ( nEditing == nRow && this.innerHTML == "Save" ) {
            saveRow( POTable, nEditing );
            nEditing = null;
        }
        else {
            editRow( POTable, nRow );
            nEditing = nRow;
        }
    } );

	reloadCkpTable = function() {
		POTable.fnDraw();
	}

	reloadPOTable = function() {
		POTable.fnDraw();
	}

	
	$('#search_t164TglPO').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t164TglPO_day').val(newDate.getDate());
			$('#search_t164TglPO_month').val(newDate.getMonth()+1);
			$('#search_t164TglPO_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			POTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	POTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	POTable = $('#PO_datatables_${idTable}').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
               "sName": "vendor",
               "mDataProp": null,
               "sClass": "control center",
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": '<i class="icon-plus"></i>'
}
,
{
	"sName": "vendor",
	"mDataProp": "vendor",
	"aTargets": [13],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"mRender": function ( data, type, row ) {
	    return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['t164NoPO']+'" title="Select this"><input type="hidden" value="'+row['t164NoPO']+'">&nbsp;&nbsp;'+data;
    },
	"bVisible": true
}
,

{
	"sName": "t164NoPO",
	"mDataProp": "t164NoPO",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "t164TglPO",
	"mDataProp": "t164TglPO",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}


,


{
	"sName": "tipeOrder",
	"mDataProp": "tipeOrder",
	"aTargets": [12],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}





],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

	                    var tglStart = $("#search_tglStart").val();
	                    var tglAkhir = $("#search_tglEnd").val();

	               //     var spld = $("#poSPLD").val();

	                    var spld = function(){
	                        if($("#poSPLD").is(':checked')){
	                            return 1;
	                        }else{
	                         return 0;
	                        }
	                        };
	                    var nonSpld =function(){
	                        if($("#poNonSPLD").is(':checked')){
	                            return 1;
	                        }else{
	                         return 0;
	                        }
	                        };

	                     var etaBlmLengkap = function(){
	                        if($("#poETABelumLengkap").is(':checked')){
	                            return 1;
	                        }else{
	                         return 0;
	                        }
	                        };

	                     var etaLengkap = function(){
	                        if($("#poETALengkap").is(':checked')){
	                            return 1;
	                        }else{
	                         return 0;
	                        }
	                        };


                        var blmDikirim = function(){
                             if($("#poBlmDikirim").is(':checked')){
                                return 1;
                             }else{
                                return 0;
                             }
                        }


                          var sdhDikirim = function(){
                            if($("#poSdhDikirim").is(':checked')){
                                return 1;
                            } else{
                                return 0;
                            }
                          }

                        if(blmDikirim){
                            aoData.push(
									{"name": 'search_blmDikirim', "value": blmDikirim}
							);
                        }

                        if(sdhDikirim){
                            aoData.push(
									{"name": 'search_sdhDikirim', "value": sdhDikirim}
							);
                        }


	                    if(tglStart){
							aoData.push(
									{"name": 'search_tglStart', "value": tglStart}
							);
						}

						 if(tglAkhir){
							aoData.push(
									{"name": 'search_tglEnd', "value": tglAkhir}
							);
						}

						 if(etaBlmLengkap){
							aoData.push(
									{"name": 'etaBlmLengkap', "value": etaBlmLengkap}
							);
						}

						if(etaLengkap){
							aoData.push(
									{"name": 'etaLengkap', "value": etaLengkap}
							);
						}

						if(spld){
							aoData.push(
									{"name": 'spld', "value": spld}
							);
						}

						if(nonSpld){
							aoData.push(
									{"name": 'nonSpld', "value": nonSpld}
							);
						}

                        var vendor = $('#filter_vendor input').val();
                            if(vendor){
                                aoData.push(
                                        {"name": 'sCriteria_vendor', "value": vendor}
                             );
						}

						 var nomorPO = $('#filter_t164NoPO input').val();
                            if(nomorPO){
                                aoData.push(
                                        {"name": 'sCriteria_nomorPO', "value": nomorPO}
                             );
						}

						var tanggalPO = $('#filter_t164TglPO input').val();
                            if(tanggalPO){
                          //  alert("tanggal : "+tanggalPO);
                                aoData.push(
                                        {"name": 'sCriteria_tglPO', "value": tanggalPO}
                             );
						}


						var tanggalPO= $('#search_t164TglPO').val();
						var tanggalPODay = $('#search_t164TglPO_day').val();
						var tanggalPOMonth = $('#search_t164TglPO_month').val();
						var tanggalPOYear = $('#search_t164TglPO_year').val();
						var tanggalPODP= $('#search_t164TglPO_dp').val();

						if(tanggalPO){
							aoData.push(
									{"name": 'sCriteria_tanggalPO', "value": "date.struct"},
									{"name": 'sCriteria_tanggalPO_dp', "value": tanggalPODP},
									{"name": 'sCriteria_tanggalPO_day', "value": tanggalPODay},
									{"name": 'sCriteria_tanggalPO_month', "value": tanggalPOMonth},
									{"name": 'sCriteria_tanggalPO_year', "value": tanggalPOYear}
							);
						}


						var tipeOrder = $('#filter_tipeOrder input').val();
                            if(tipeOrder){
                                aoData.push(
                                        {"name": 'sCriteria_tipeOrder', "value": tipeOrder}
                             );
						}


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
