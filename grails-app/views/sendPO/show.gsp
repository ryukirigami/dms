

<%@ page import="com.kombos.parts.PO" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'PO.label', default: 'PO')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deletePO;

$(function(){ 
	deletePO=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/PO/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadPOTable();
   				expandTableLayout('PO');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-PO" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="PO"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${POInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="PO.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${POInstance}" field="createdBy"
								url="${request.contextPath}/PO/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadPOTable();" />--}%
							
								<g:fieldValue bean="${POInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="PO.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${POInstance}" field="dateCreated"
								url="${request.contextPath}/PO/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadPOTable();" />--}%
							
								<g:formatDate date="${POInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POInstance?.goods}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="goods-label" class="property-label"><g:message
					code="PO.goods.label" default="Goods" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="goods-label">
						%{--<ba:editableValue
								bean="${POInstance}" field="goods"
								url="${request.contextPath}/PO/updatefield" type="text"
								title="Enter goods" onsuccess="reloadPOTable();" />--}%
							
								<g:link controller="goods" action="show" id="${POInstance?.goods?.id}">${POInstance?.goods?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="PO.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${POInstance}" field="lastUpdProcess"
								url="${request.contextPath}/PO/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadPOTable();" />--}%
							
								<g:fieldValue bean="${POInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="PO.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${POInstance}" field="lastUpdated"
								url="${request.contextPath}/PO/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadPOTable();" />--}%
							
								<g:formatDate date="${POInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POInstance?.request}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="request-label" class="property-label"><g:message
					code="PO.request.label" default="Request" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="request-label">
						%{--<ba:editableValue
								bean="${POInstance}" field="request"
								url="${request.contextPath}/PO/updatefield" type="text"
								title="Enter request" onsuccess="reloadPOTable();" />--}%
							
								<g:link controller="request" action="show" id="${POInstance?.request?.id}">${POInstance?.request?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="PO.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${POInstance}" field="staDel"
								url="${request.contextPath}/PO/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadPOTable();" />--}%
							
								<g:fieldValue bean="${POInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POInstance?.t164HargaSatuan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t164HargaSatuan-label" class="property-label"><g:message
					code="PO.t164HargaSatuan.label" default="T164 Harga Satuan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t164HargaSatuan-label">
						%{--<ba:editableValue
								bean="${POInstance}" field="t164HargaSatuan"
								url="${request.contextPath}/PO/updatefield" type="text"
								title="Enter t164HargaSatuan" onsuccess="reloadPOTable();" />--}%
							
								<g:fieldValue bean="${POInstance}" field="t164HargaSatuan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POInstance?.t164NoPO}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t164NoPO-label" class="property-label"><g:message
					code="PO.t164NoPO.label" default="T164 No PO" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t164NoPO-label">
						%{--<ba:editableValue
								bean="${POInstance}" field="t164NoPO"
								url="${request.contextPath}/PO/updatefield" type="text"
								title="Enter t164NoPO" onsuccess="reloadPOTable();" />--}%
							
								<g:fieldValue bean="${POInstance}" field="t164NoPO"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POInstance?.t164Qty1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t164Qty1-label" class="property-label"><g:message
					code="PO.t164Qty1.label" default="T164 Qty1" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t164Qty1-label">
						%{--<ba:editableValue
								bean="${POInstance}" field="t164Qty1"
								url="${request.contextPath}/PO/updatefield" type="text"
								title="Enter t164Qty1" onsuccess="reloadPOTable();" />--}%
							
								<g:fieldValue bean="${POInstance}" field="t164Qty1"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POInstance?.t164Qty2}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t164Qty2-label" class="property-label"><g:message
					code="PO.t164Qty2.label" default="T164 Qty2" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t164Qty2-label">
						%{--<ba:editableValue
								bean="${POInstance}" field="t164Qty2"
								url="${request.contextPath}/PO/updatefield" type="text"
								title="Enter t164Qty2" onsuccess="reloadPOTable();" />--}%
							
								<g:fieldValue bean="${POInstance}" field="t164Qty2"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POInstance?.t164StaOpenCancelClose}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t164StaOpenCancelClose-label" class="property-label"><g:message
					code="PO.t164StaOpenCancelClose.label" default="T164 Sta Open Cancel Close" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t164StaOpenCancelClose-label">
						%{--<ba:editableValue
								bean="${POInstance}" field="t164StaOpenCancelClose"
								url="${request.contextPath}/PO/updatefield" type="text"
								title="Enter t164StaOpenCancelClose" onsuccess="reloadPOTable();" />--}%
							
								<g:fieldValue bean="${POInstance}" field="t164StaOpenCancelClose"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POInstance?.t164TglPO}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t164TglPO-label" class="property-label"><g:message
					code="PO.t164TglPO.label" default="T164 Tgl PO" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t164TglPO-label">
						%{--<ba:editableValue
								bean="${POInstance}" field="t164TglPO"
								url="${request.contextPath}/PO/updatefield" type="text"
								title="Enter t164TglPO" onsuccess="reloadPOTable();" />--}%
							
								<g:formatDate date="${POInstance?.t164TglPO}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POInstance?.t164TotalHarga}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t164TotalHarga-label" class="property-label"><g:message
					code="PO.t164TotalHarga.label" default="T164 Total Harga" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t164TotalHarga-label">
						%{--<ba:editableValue
								bean="${POInstance}" field="t164TotalHarga"
								url="${request.contextPath}/PO/updatefield" type="text"
								title="Enter t164TotalHarga" onsuccess="reloadPOTable();" />--}%
							
								<g:fieldValue bean="${POInstance}" field="t164TotalHarga"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POInstance?.t164xNamaDivisi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t164xNamaDivisi-label" class="property-label"><g:message
					code="PO.t164xNamaDivisi.label" default="T164x Nama Divisi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t164xNamaDivisi-label">
						%{--<ba:editableValue
								bean="${POInstance}" field="t164xNamaDivisi"
								url="${request.contextPath}/PO/updatefield" type="text"
								title="Enter t164xNamaDivisi" onsuccess="reloadPOTable();" />--}%
							
								<g:fieldValue bean="${POInstance}" field="t164xNamaDivisi"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POInstance?.t164xNamaUser}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t164xNamaUser-label" class="property-label"><g:message
					code="PO.t164xNamaUser.label" default="T164x Nama User" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t164xNamaUser-label">
						%{--<ba:editableValue
								bean="${POInstance}" field="t164xNamaUser"
								url="${request.contextPath}/PO/updatefield" type="text"
								title="Enter t164xNamaUser" onsuccess="reloadPOTable();" />--}%
							
								<g:fieldValue bean="${POInstance}" field="t164xNamaUser"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="PO.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${POInstance}" field="updatedBy"
								url="${request.contextPath}/PO/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadPOTable();" />--}%
							
								<g:fieldValue bean="${POInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POInstance?.validasiOrder}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="validasiOrder-label" class="property-label"><g:message
					code="PO.validasiOrder.label" default="Validasi Order" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="validasiOrder-label">
						%{--<ba:editableValue
								bean="${POInstance}" field="validasiOrder"
								url="${request.contextPath}/PO/updatefield" type="text"
								title="Enter validasiOrder" onsuccess="reloadPOTable();" />--}%
							
								<g:link controller="validasiOrder" action="show" id="${POInstance?.validasiOrder?.id}">${POInstance?.validasiOrder?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${POInstance?.vendor}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="vendor-label" class="property-label"><g:message
					code="PO.vendor.label" default="Vendor" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="vendor-label">
						%{--<ba:editableValue
								bean="${POInstance}" field="vendor"
								url="${request.contextPath}/PO/updatefield" type="text"
								title="Enter vendor" onsuccess="reloadPOTable();" />--}%
							
								<g:link controller="vendor" action="show" id="${POInstance?.vendor?.id}">${POInstance?.vendor?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('PO');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${POInstance?.id}"
					update="[success:'PO-form',failure:'PO-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deletePO('${POInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
