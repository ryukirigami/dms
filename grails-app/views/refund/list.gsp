<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <meta name="layout" content="main">
        <g:set var="entityName" value="${message(code: 'delivery.refund.label', default: 'Delivery - Refund')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
        <r:require modules="baseapplayout" />
        <g:javascript>

            $(function(){
                var msg='';

                closeData = function() {
					var url = '${request.contextPath}/#/home';
					$(location).attr('href',url);
				};

                $('input[name=outJumlah]').focusout(function(){
                    var jumlah = $('input[name=outJumlah]').val();
                    var param = $('input[name=paramSetting]').val();
                    if((jumlah>0) && (jumlah>param)){
                        $('input[name=metodeRefundSelect]').prop("disabled",false);
                        $(':radio[name="metodeRefundSelect"]:nth(1)').attr('checked',true);
                        $('input[name=metodeRefund]').val('1');
                        clearAllRefund();
                        enabledAllRefund();
                    }else{
                        $('input[name=metodeRefundSelect]').prop("disabled",true);
                        $(':radio[name="metodeRefundSelect"]:nth(0)').attr('checked',true);
                        $('input[name=metodeRefund]').val('0');
                        clearAllRefund();
                        disabledAllRefund();
                    }
                });

                $('input[name=outJumlah]').blur(function(){
                    var jumlah = $('input[name=outJumlah]').val();
                    var param = $('input[name=paramSetting]').val();
                    if((jumlah>0) && (jumlah>param)){
                        $('input[name=metodeRefundSelect]').prop("disabled",false);
                        $(':radio[name="metodeRefundSelect"]:nth(1)').attr('checked',true);
                        $('input[name=metodeRefund]').val('1');
                        clearAllRefund();
                        enabledAllRefund();
                    }else{
                        $('input[name=metodeRefundSelect]').prop("disabled",true);
                        $(':radio[name="metodeRefundSelect"]:nth(0)').attr('checked',true);
                        $('input[name=metodeRefund]').val('0');
                        clearAllRefund();
                        disabledAllRefund();
                    }
                });

                $(':radio[name="metodeRefundSelect"]').change(function() {
                    var metodeRefundSelect = $(this).filter(':checked').val();
                    if(metodeRefundSelect=='1'){
                        $('input[name=metodeRefund]').val('1');
                        clearAllRefund();
                        enabledAllRefund();
                    }else{
                        $('input[name=metodeRefund]').val('0');
                        $('.alasanLainnya').attr('disabled',false);
                        clearAllRefund();
                        disabledAllRefund();
                    }
                    var alasanRefundSelect = $('select[name=alasanRefundSelect] option:selected').val();
                    if(alasanRefundSelect=='0'){
                        $('.alasanLainnya').attr('disabled',false);
                    }
                });

                clearAllRefund = function() {
                    $('#namaBankSelect').val('0');
                    $('input[name=namaBank]').val('');
                    $('input[name=noAccount]').val('');
                    $('input[name=namaAccount]').val('');
                    $('input[name=fcKTP]').attr('checked',false);
                    $('input[name=fcTabungan]').attr('checked',false);
                    $('#alasanRefundSelect').val('0');
                    $('input[name=alasanLainnya]').val('');
                };

                enabledAllRefund = function() {
                    $('#namaBankSelect').prop("disabled",false);
                    $('input[name=namaBank]').prop("readonly",false);
                    $('input[name=noAccount]').prop("readonly",false);
                    $('input[name=namaAccount]').prop("readonly",false);
                    $('input[name=fcKTP]').prop("disabled",false);
                    $('input[name=fcTabungan]').prop("disabled",false);
                };

                disabledAllRefund = function() {
                    $('#namaBankSelect').prop("disabled",true);
                    $('input[name=namaBank]').prop("readonly",true);
                    $('input[name=noAccount]').prop("readonly",true);
                    $('input[name=namaAccount]').prop("readonly",true);
                    $('input[name=fcKTP]').prop("disabled",true);
                    $('input[name=fcTabungan]').prop("disabled",true);
                };

                changeJenisRefund = function() {
                    var jenisRefundSelect = $('select[name=jenisRefundSelect] option:selected').val();
                    $('input[name=jenisRefund]').val(jenisRefundSelect);
                };

                changeAlasanRefund = function() {
                    var alasanRefundSelect = $('select[name=alasanRefundSelect] option:selected').val();
                    if(alasanRefundSelect=='0'){
                        $('input[name=alasanRefund]').val(alasanRefundSelect);
                        $('.alasanLainnya').val('');
                        $('.alasanLainnya').attr('disabled',false);
                        $('.alasanLainnya').focus();
                    }else{
                        $('input[name=alasanRefund]').val(alasanRefundSelect);
                        $('.alasanLainnya').val('');
                        $('.alasanLainnya').attr('disabled',true);
                    }
                };

                changeBank = function() {
                    var namaBankSelect = $('#namaBankSelect').val();
                    $('input[name=namaBank]').val(namaBankSelect);
                };

                searchData = function(){
                    var jenisRefund = $('input[name=jenisRefund]').val();
                    var nomorWO = $('input[name=nomorWO]').val();
                    if(nomorWO){
                        doSearch(jenisRefund, nomorWO);
                    }else{
                        clearDataWorkOrder();
                        clearAllRefund();
                        disabledAllRefund();
                        alert('Nomor WO harus diisi !');
                        return false;
                    }
                };

                doSearch = function(a,b){
                    $("#print").prop("disabled",false);
                    $("#preview").prop("disabled",true);
                    $.ajax({
                        url:'${request.contextPath}/refund/searchData',
                        type: "POST",
                        data : { jenisRefund: a, nomorWO: b },
                        success : function(data){
                            clearDataWorkOrder();
                            if(data){
                                setDataWorkOrder(data);
                                setDataRefund();
                                $('input[name=outJumlah]').focus();
                                if(data.staDone=="1"){
                                    $("#print").prop("disabled",true);
                                    $("#preview").prop("disabled",false);
                                }
                            }else{
                                clearDataWorkOrder();
                                clearAllRefund();
                                disabledAllRefund();
                                alert('Data Tidak Ditemukan');
                                return false;
                            }
                        },
                        error: function() {
                            clearDataWorkOrder();
                            clearAllRefund();
                            disabledAllRefund();
                            alert('Internal Server Error');
                            return false;
                        }
                    });
                };

                isValid = function() {
                    var refundId = $('input[name=outNomorRefund]').val();
                    var amount = $('input[name=outJumlah]').val();
                    var metodeRefund = $('input[name=metodeRefund]').val();
                    var namaBank = $('input[name=namaBank]').val();
                    var noAccount = $('input[name=noAccount]').val();
                    var namaAccount = $('input[name=namaAccount]').val();
                    var fcKTP = $('#fcKTP').is(":checked");
                    var fcTabungan = $('#fcTabungan').is(":checked");
                    var alasanRefund = $('input[name=alasanRefund]').val();
                    var alasanLainnya = $('.alasanLainnya').val();
                    msg='';
                    if(amount=='0' || amount==''){msg += '- Masukkan Jumlah Refund \n';}
                    if(metodeRefund=='1'){
                        if(namaBank==''){msg += '- Silahkan Pilih Nama Bank \n';}
                        if(noAccount==''){msg += '- Masukkan Nomor Account \n';}
                        if(namaAccount==''){msg += '- Masukkan Nama Account \n';}
                        if(!fcKTP){msg += '- Silahkan Pilih Foto Copy KTP \n';}
                        if(!fcTabungan){msg += '- Silahkan Pilih Foto Copy Tabungan \n';}
                    }
                    if(alasanRefund=='0'){
                        if(alasanLainnya==''){msg += '- Masukkan Alasan Refund Lainnya \n';}
                    }
                    return msg=='';
                };

                printData = function() {
                    msg='';
                    if(isValid()){
                        doSave();
                    }else{
                        alert(msg);
                        return false;
                    }
                };

                doSave = function() {
                    var arrData = {};
                    arrData["jenisRefund"] = $('input[name=jenisRefund]').val();
                    arrData["nomorWO"] = $('input[name=nomorWO]').val();
                    arrData["namaCustomer"] = $('input[name=outNamaCustomer]').val();
                    arrData["nopol"] = $('input[name=outNomorPolisi]').val();
                    arrData["jumlah"] = $('input[name=outJumlah]').val();
                    arrData["metodeRefund"] = $('input[name=metodeRefund]').val();
                    arrData["namaBank"] = $('#namaBankSelect').val();
                    arrData["noAccount"] = $('input[name=noAccount]').val();
                    arrData["namaAccount"] = $('input[name=namaAccount]').val();
                    arrData["fcKTP"] = ($('#fcKTP').attr('checked'))?'1':'0';
                    arrData["fcTab"] = ($('#fcTabungan').attr('checked'))?'1':'0';
                    arrData["alasan"] = $('input[name=alasanRefund]').val();
                    arrData["alasanLain"] = $('.alasanLainnya').val();
                    arrData["kasir"] = $('input[name=outKasir]').val();
                    arrData["divisi"] = $('input[name=outDivisi]').val();

                    $.ajax({
                        url: '${request.contextPath}/refund/save',
                        type: 'POST',
                        data: arrData,
                        success : function(data){
                            if(data.err){
                                %{--clearDataWorkOrder();--}%
                                %{--clearAllRefund();--}%
                                %{--disabledAllRefund();--}%
                                %{--$('input[name=outJumlah]').prop("readonly",true);--}%
                                %{--$('input[name=metodeRefundSelect]').prop("disabled",true);--}%
                                %{--$('#alasanRefundSelect').prop("disabled",true);--}%
                                %{--$('input[name=alasanLainnya]').prop("disabled",true);--}%
                                alert(data.err);
                            }else{
                                $('h4.nomorRefundText').text(data.nomorRefund);
                                $('input[name=outNomorRefund]').val(data.nomorRefund);
                                $("#print").prop("disabled",true);
                                $("#preview").prop("disabled",false);
//                                doPrint(data.nomorRefund);
                            }
                        },
                        error: function() {
                            clearDataWorkOrder();
                            clearAllRefund();
                            disabledAllRefund();
                            $('input[name=outJumlah]').prop("readonly",true);
                            $('input[name=metodeRefundSelect]').prop("disabled",true);
                            $('#alasanRefundSelect').prop("disabled",true);
                            $('input[name=alasanLainnya]').prop("disabled",true);
                            alert('Internal Server Error');
                            return false;
                        }
                    });
                };

                doPrint = function() {
                    var refundId = $('input[name=outNomorRefund]').val();
                    %{--if(refNo){--}%
                        %{--var win = window.open("${request.contextPath}/refund/print?nomorRefund="+refNo,'_blank');--}%
                        %{--win.focus();--}%
                    %{--}--}%
                    window.location = "${request.contextPath}/refund/print?nomorRefund="+refundId;
                };

                previewData = function(){
                    var refundId = $('input[name=outNomorRefund]').val();
                    if(refundId){
                        doPreview(refundId);
                    }else{
                        alert('Data tidak ditemukan');
                        return false;
                    }
                };

                doPreview = function(a){
                    $("#refundFormListPreviewContent").empty();
	                $.ajax({
						url: '${request.contextPath}/slipList/previewRefundForm',
						type: 'POST',
						data : { nomorRefund: a },
	                    success: function (data) {
	                        $("#refundFormListPreviewContent").html(data);
	                        $("#refundFormListPreviewModal").modal({
	                            "backdrop": "static",
	                            "keyboard": true,
	                            "show": true
	                        }).css({
								'width': '1200px',
								'margin-left': function () {
	                            	return - ( $(this).width() / 2 );
	                            }
							});
	                    },
	                    error: function () {
	                        alert('Data not found');
                            return false;
	                    },
	                    complete: function () {
	                        $('#spinner').fadeOut();
	                    }
	                });
                };

                setDataRefund = function(){
                    $('input[name=outJumlah]').prop("readonly",false);
                    $('select[name=alasanRefundSelect]').prop("disabled",false);
                    $('.alasanLainnya').attr('disabled',false);
                    $('input[name=metodeRefundSelect]').prop("disabled",false);
                    var jumlah = $('input[name=outJumlah]').val();
                    var param = $('input[name=paramSetting]').val();
                    if((jumlah>0) && (jumlah>param)){
                        $('input[name=metodeRefundSelect]').prop("disabled",false);
                        $(':radio[name="metodeRefundSelect"]:nth(0)').attr('checked',true);
                        $('input[name=metodeRefund]').val('1');
                        clearAllRefund();
                        enabledAllRefund();
                    }else{
                        $('input[name=metodeRefundSelect]').prop("disabled",true);
                        $(':radio[name="metodeRefundSelect"]:nth(0)').attr('checked',true);
                        $('input[name=metodeRefund]').val('0');
                        clearAllRefund();
                        disabledAllRefund();
                    }
                };

                clearDataWorkOrder = function(){
                    $('input[name=outNomorWO]').val('');
                    $('input[name=outNamaCustomer]').val('');
                    $('input[name=outNomorPolisi]').val('');
                    $('input[name=outModel]').val('');
                    $('input[name=outStatusApproval]').val('');
                    $('input[name=outNoKuitansi]').val('');
                    $('input[name=outBookingFee]').val('');
                    $('input[name=outNoInvoice]').val('');
                    $('input[name=outTotalInvoice]').val('');
                    $('input[name=outRefundBF]').val('');
                    $('input[name=outRefundPPH]').val('');
                    $('input[name=outRefundPPN]').val('');
                    $('input[name=outKasir]').val('');
                    $('input[name=outJumlah]').val('');
                    $('input[name=paramSetting]').val('');
                    $('input[name=metodeRefund]').val('');
                    $('h4.nomorRefundText').text('');
                    $('.alasanLainnya').val('');
                };

                setDataWorkOrder = function(data){
                    $('input[name=outNomorWO]').val(data.outNomorWO);
                    $('input[name=outNamaCustomer]').val(data.outNamaCustomer);
                    $('input[name=outNomorPolisi]').val(data.outNomorPolisi);
                    $('input[name=outModel]').val(data.outModel);
                    $('input[name=outStatusApproval]').val(data.outStatusApproval);
                    $('input[name=outNoKuitansi]').val(data.outNoKuitansi);
                    $('input[name=outBookingFee]').val(data.outBookingFee);
                    $('input[name=outNoInvoice]').val(data.outNoInvoice);
                    $('input[name=outTotalInvoice]').val(data.outTotalInvoice);
                    $('input[name=outRefundBF]').val(data.outRefundBF);
                    $('input[name=outRefundPPH]').val(data.outRefundPPH);
                    $('input[name=outRefundPPN]').val(data.outRefundPPN);
                    $('input[name=outKasir]').val(data.outKasir);
                    $('input[name=outDivisi]').val(data.outDivisi);
                    $('input[name=outJumlah]').val(data.outRefundBF);
                    $('input[name=paramSetting]').val(data.outParamSetting);
                    $('input[name=outNomorRefund]').val(data.outNoRefund);
                    $('h4.nomorRefundText').text(data.outNoRefund);
                    if(parseInt(data.outRefundBF)<parseInt(data.outParamSetting)){
                        $('input[name=metodeRefund]').val('0');
                        $(':radio[name="metodeRefundSelect"]:nth(0)').attr('checked',true);
                    }else{
                        $('input[name=metodeRefund]').val('1');
                        $(':radio[name="metodeRefundSelect"]:nth(1)').attr('checked',true);
                    }
                };

                isNumberKey = function(e){
                    var charCode = (e.which) ? e.which : e.keyCode;
                    return !(charCode > 31 && (charCode < 48 || charCode > 57)) || (charCode = 189);
                };

            });
        </g:javascript>
    </head>

    <body>
        <div class="navbar box-header no-border">
            <span class="pull-left">Delivery - Refund</span>
        </div>
        <div>&nbsp;</div>
        <div class="box" style="margin-bottom: 0; padding-bottom: 0;">
            <table width="100%" style="border-spacing: 5px; border-collapse: separate;">
                <tr valign="top">
                    <td width="40%" rowspan="2">
                        <span style="text-decoration: underline;"><g:message code="delivery.refund.left.label" default="Data Work Order"/></span>
                        <div class="box" style="margin-bottom: 0; padding-bottom: 0;">
                            <legend style="font-size: small;">
                                <table width="100%" style="border-spacing: 1px; border-collapse: separate; margin-bottom: 5px;">
                                    <tr>
                                        <td width="30%">Jenis Refund</td>
                                        <td width="70%">
                                            <g:select style="width:100%" name="jenisRefundSelect" id="jenisRefundSelect" onchange="changeJenisRefund();" from="${jenisRefund}" optionKey="tipeKey" optionValue="tipeValue"/>
                                            <input type="hidden" id="jenisRefund" name="jenisRefund" value="0">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30%">Nomor WO</td>
                                        <td width="70%">
                                            <g:textField name="nomorWO" id="nomorWO" style="width:95%" onkeypress="return isNumberKey(event)"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right">
                                            <g:field type="button" style="width: 90px" class="btn btn-primary search" onclick="searchData();"
                                                     name="search" id="search" value="${message(code: 'default.ok.label', default: 'OK')}" />
                                        </td>
                                    </tr>
                                </table>
                            </legend>
                            <table width="100%" border="1" style="margin-bottom: 10px;">
                                <tr style="height: 30px;">
                                    <td width="30%" style="padding-left: 5px;">Nomor WO</td>
                                    <td width="70%" align="center"><g:textField name="outNomorWO" id="outNomorWO" style="margin: 1px; width: 94%" readonly=""/></td>
                                </tr>
                                <tr style="height: 30px;">
                                    <td width="30%" style="padding-left: 5px;">Nama Customer</td>
                                    <td width="70%" align="center"><g:textField name="outNamaCustomer" id="outNamaCustomer" style="margin: 1px; width: 94%" readonly=""/></td>
                                </tr>
                                <tr style="height: 30px;">
                                    <td width="30%" style="padding-left: 5px;">Nomor Polisi</td>
                                    <td width="70%" align="center"><g:textField name="outNomorPolisi" id="outNomorPolisi" style="margin: 1px; width: 94%" readonly=""/></td>
                                </tr>
                                <tr style="height: 30px;">
                                    <td width="30%" style="padding-left: 5px;">Model</td>
                                    <td width="70%" align="center"><g:textField name="outModel" id="outModel" style="margin: 1px; width: 94%" readonly=""/></td>
                                </tr>
                                <tr style="height: 30px;">
                                    <td width="30%" style="padding-left: 5px;">Status Approval Cancel BF Appointment</td>
                                    <td width="70%" align="center"><g:textField name="outStatusApproval" id="outStatusApproval" style="margin: 1px; width: 94%; height: 50px" readonly=""/></td>
                                </tr>
                                <tr style="height: 30px;">
                                    <td width="30%" style="padding-left: 5px;">No. Kuitansi</td>
                                    <td width="70%" align="center"><g:textField name="outNoKuitansi" id="outNoKuitansi" style="margin: 1px; width: 94%" readonly=""/></td>
                                </tr>
                                <tr style="height: 30px;">
                                    <td width="30%" style="padding-left: 5px;">Booking Fee</td>
                                    <td width="70%" align="center"><g:textField name="outBookingFee" id="outBookingFee" style="margin: 1px; width: 94%" readonly=""/></td>
                                </tr>
                                <tr style="height: 30px;">
                                    <td width="30%" style="padding-left: 5px;">No. Invoice</td>
                                    <td width="70%" align="center"><g:textField name="outNoInvoice" id="outNoInvoice" style="margin: 1px; width: 94%" readonly=""/></td>
                                </tr>
                                <tr style="height: 30px;">
                                    <td width="30%" style="padding-left: 5px;">Total Invoice</td>
                                    <td width="70%" align="center"><g:textField name="outTotalInvoice" id="outTotalInvoice" style="margin: 1px; width: 94%" readonly=""/></td>
                                </tr>
                                <tr style="height: 30px;">
                                    <td width="30%" style="padding-left: 5px;">Refund BF</td>
                                    <td width="70%" align="center"><g:textField name="outRefundBF" id="outRefundBF" style="margin: 1px; width: 94%" readonly=""/></td>
                                </tr>
                                <tr style="height: 30px;">
                                    <td width="30%" style="padding-left: 5px;">Refund PPH 23</td>
                                    <td width="70%" align="center"><g:textField name="outRefundPPH" id="outRefundPPH" style="margin: 1px; width: 94%" readonly=""/></td>
                                </tr>
                                <tr style="height: 30px;">
                                    <td width="30%" style="padding-left: 5px;">Refund PPN</td>
                                    <td width="70%" align="center"><g:textField name="outRefundPPN" id="outRefundPPN" style="margin: 1px; width: 94%" readonly=""/></td>
                                </tr>
                                <tr style="height: 30px;">
                                    <td width="30%" style="padding-left: 5px;">Kasir</td>
                                    <td width="70%" align="center">
                                        <g:textField name="outKasir" id="outKasir" style="margin: 1px; width: 94%" readonly=""/>
                                        <g:hiddenField name="outDivisi" id="outDivisi"/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td width="60%">
                        <span style="text-decoration: underline;"><g:message code="delivery.refund.right.top.label" default="Refund"/></span>
                        <div class="box" style="padding-top: 5px; padding-left: 10px; margin-bottom: 10px; padding-bottom: 0;">
                            <table width="100%" border="1" style="margin-bottom: 10px;">
                                <tr style="height: 30px;">
                                    <td width="30%" style="padding-left: 5px;">Jumlah</td>
                                    <td width="70%" align="center"><g:textField name="outJumlah" id="outJumlah" style="margin: 1px; width: 96%; text-align: right" readonly="" onkeypress="return isNumberKey(event)"/></td>
                                </tr>
                                <tr style="height: 30px;">
                                    <td width="30%" style="padding-left: 5px;">Metode Refund</td>
                                    <td width="70%" style="padding-left: 5px;">
                                        <g:radio name="metodeRefundSelect" value="0" disabled="true"/>&nbsp;CASH&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<g:radio name="metodeRefundSelect" value="1" disabled="true"/>&nbsp;TRANSFER
                                        <g:hiddenField name="metodeRefund" id="metodeRefund"/>
                                        <g:hiddenField name="paramSetting" id="paramSetting"/>
                                    </td>
                                </tr>
                                <tr style="height: 30px;">
                                    <td width="30%" style="padding-left: 5px;">Nama Bank</td>
                                    <td width="70%" align="center">
                                        <g:hiddenField name="namaBank" id="namaBank" value=""/>
                                        <g:select name="namaBankSelect" onchange="changeBank();" from="${com.kombos.finance.BankAccountNumber.createCriteria().list {eq("staDel","0");bank{order("m702NamaBank")}}}" id="namaBankSelect" optionValue="${{it.bank.m702NamaBank+" "+it.bank.m702Cabang}}" optionKey="id"
                                                  noSelection="['':'']"/>
                                    </td>
                                </tr>
                                <tr style="height: 30px;">
                                    <td width="30%" style="padding-left: 5px;">No. Account</td>
                                    <td width="70%" align="center"><g:textField name="noAccount" id="noAccount" style="margin: 1px; width: 96%;" readonly=""/></td>
                                </tr>
                                <tr style="height: 30px;">
                                    <td width="30%" style="padding-left: 5px;">Nama Account</td>
                                    <td width="70%" align="center"><g:textField name="namaAccount" id="namaAccount" style="margin: 1px; width: 96%;" readonly=""/></td>
                                </tr>
                                <tr style="height: 30px;">
                                    <td width="30%" style="padding-left: 5px;"><g:checkBox name="fcKTP" id="fcKTP" disabled="true"/>&nbsp;Fotokopi KTP</td>
                                    <td width="70%" style="padding-left: 5px;"><g:checkBox name="fcTabungan" id="fcTabungan" disabled="true"/>&nbsp;Fotokopi Buku Tabungan</td>
                                </tr>
                                <tr style="height: 30px;">
                                    <td width="30%" style="padding-left: 5px;">Alasan Refund</td>
                                    <td width="70%" align="center" style="padding-top: 5px;">
                                        <g:select style="width:98%" name="alasanRefundSelect" id="alasanRefundSelect" onchange="changeAlasanRefund();" from="${alasanRefund}" optionKey="m071ID" optionValue="m071AlasanRefund" noSelection="${['0':'LAINNYA']}" disabled=""/>
                                        <br/>
                                        <g:hiddenField name="alasanRefund" id="alasanRefund" value="0"/>
                                        <g:textArea class="alasanLainnya" name="alasanLainnya" id="alasanLainnya" rows="2" style="width:430px;height:47px;resize:none;" disabled=""/>
                                    </td>
                                </tr>
                            </table>
                            <ul class="nav pull-right">
                                <g:field type="button" style="width: 150px" class="btn btn-primary" onclick="printData();"
                                         name="print" id="print" value="Save Refund" />
                                <g:field type="button" style="width: 150px" class="btn btn-primary" onclick="doPrint();"
                                         name="preview" id="preview" value="Preview Refund Form" />
                            </ul>
                            <table width="100%" border="1" style="margin-bottom: 10px;">
                                <tr style="height: 30px;">
                                    <td width="30%" style="padding-left: 5px;">No. Refund Form</td>
                                    <td width="70%" align="center">
                                        <g:hiddenField name="outNomorRefund" id="outNomorRefund"/>
                                        <h4 class="nomorRefundText"></h4>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr valign="top">
                    <td width="50%">
                        <span style="text-decoration: underline;"><g:message code="delivery.refund.right.bottom.label" default="Cek Data Kuitansi"/></span>
                        <div class="box" style="padding-top: 5px; padding-left: 15px; width:647px;">
                            <table width="100%">
                                <tr valign="top">
                                    <td width="20%" align="right">No Kuitansi *</td>
                                    <td width="60%" align="center"><g:textField name="nomorKuitansi" id="nomorKuitansi" style="width:95%"/></td>
                                    <td width="20%" align="left">
                                        <g:field type="button" style="width: 90px" class="btn btn-primary" onclick="searchKuitansi();"
                                                 name="searchKuitansi" id="searchKuitansi" value="${message(code: 'default.ok.label', default: 'OK')}" />
                                    </td>
                                </tr>
                            </table>
                            <g:render template="kuitansiDataTables"/>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <br/>
        <ul class="nav pull-right">
            <g:field type="button" style="width: 90px" class="btn btn-primary" onclick="closeData();"
                     name="close" id="close" value="${message(code: 'default.close.label', default: 'Close')}" />
        </ul>

        <!-- Start Modal Preview -->
        <div id="refundFormListPreviewModal" class="modal fade">
            <div class="modal-dialog" style="width: 1200px;">
                <div class="modal-content" style="width: 1200px;">
                    <!-- dialog body -->
                    <div class="modal-body" style="max-height: 1200px;">
                        <div id="refundFormListPreviewContent"></div>
                        <div class="iu-content"></div>
                    </div>
                    <!-- dialog buttons -->
                    <div class="modal-footer">
                        <g:field type="button" style="width: 90px" class="btn btn-primary" name="close_modal" id="close_modal"
                                 value="${message(code: 'default.close.label', default: 'Close')}" data-dismiss="modal"/>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Modal Preview -->
    </body>
</html>