<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="kuitansi_datatables_${idTable}" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Tgl Bayar</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">No. WO / Appointment</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 125px;">Customer</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 125px;">Untuk Pembayaran</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Jumlah</div>
        </th>
    </tr>
    </thead>
</table>
<g:javascript>
var kuitansiTable;
var reloadKuitansiTable;

function searchKuitansi() {
    var nomorKuitansi = $('input[name=nomorKuitansi]').val();
    if(nomorKuitansi) {
        reloadKuitansiTable();
    }else{
        alert('Masukkan nomor kuitansi');
        return false;
    }
}

$(function() {
	reloadKuitansiTable = function() {
		kuitansiTable.fnDraw();
	}

	kuitansiTable = $('#kuitansi_datatables_${idTable}').dataTable({
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>>",
		"bFilter": false,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		},
	    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
	    },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "kuitansiDataTablesList")}",
		"aoColumns": [
			//tanggalBayar
			{
				"sName": "tanggalBayar",
				"mDataProp": "tanggalBayar",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			//noWO
			{
				"sName": "noWO",
				"mDataProp": "noWO",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			//customer
			{
				"sName": "customer",
				"mDataProp": "customer",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//pembayaran
			{
				"sName": "pembayaran",
				"mDataProp": "pembayaran",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//jumlah
			{
				"sName": "jumlah",
				"mDataProp": "jumlah",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			}
		],
	    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	        var nomorKuitansi = $('input[name=nomorKuitansi]').val();
            if(nomorKuitansi) {
				aoData.push(
                    {"name": 'nomorKuitansi', "value": nomorKuitansi}
				);
			}
	        $.ajax({ "dataType": 'json',
				"type": "POST",
				"url": sSource,
				"data": aoData ,
				"success": function (json) {
					fnCallback(json);
				},
				"complete": function () {}
			});
	    }
	});

});
</g:javascript>