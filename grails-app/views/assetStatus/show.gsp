

<%@ page import="com.kombos.finance.AssetStatus" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'assetStatus.label', default: 'AssetStatus')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteAssetStatus;

$(function(){ 
	deleteAssetStatus=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/assetStatus/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadAssetStatusTable();
   				expandTableLayout('assetStatus');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-assetStatus" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="assetStatus"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${assetStatusInstance?.assetStatus}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="assetStatus-label" class="property-label"><g:message
					code="assetStatus.assetStatus.label" default="Asset Status" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="assetStatus-label">
						%{--<ba:editableValue
								bean="${assetStatusInstance}" field="assetStatus"
								url="${request.contextPath}/AssetStatus/updatefield" type="text"
								title="Enter assetStatus" onsuccess="reloadAssetStatusTable();" />--}%
							
								<g:fieldValue bean="${assetStatusInstance}" field="assetStatus"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetStatusInstance?.description}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="description-label" class="property-label"><g:message
					code="assetStatus.description.label" default="Description" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="description-label">
						%{--<ba:editableValue
								bean="${assetStatusInstance}" field="description"
								url="${request.contextPath}/AssetStatus/updatefield" type="text"
								title="Enter description" onsuccess="reloadAssetStatusTable();" />--}%
							
								<g:fieldValue bean="${assetStatusInstance}" field="description"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetStatusInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="assetStatus.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${assetStatusInstance}" field="staDel"
								url="${request.contextPath}/AssetStatus/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadAssetStatusTable();" />--}%
							
								<g:fieldValue bean="${assetStatusInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetStatusInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="assetStatus.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${assetStatusInstance}" field="createdBy"
								url="${request.contextPath}/AssetStatus/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadAssetStatusTable();" />--}%
							
								<g:fieldValue bean="${assetStatusInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetStatusInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="assetStatus.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${assetStatusInstance}" field="updatedBy"
								url="${request.contextPath}/AssetStatus/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadAssetStatusTable();" />--}%
							
								<g:fieldValue bean="${assetStatusInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetStatusInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="assetStatus.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${assetStatusInstance}" field="lastUpdProcess"
								url="${request.contextPath}/AssetStatus/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadAssetStatusTable();" />--}%
							
								<g:fieldValue bean="${assetStatusInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetStatusInstance?.asset}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="asset-label" class="property-label"><g:message
					code="assetStatus.asset.label" default="Asset" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="asset-label">
						%{--<ba:editableValue
								bean="${assetStatusInstance}" field="asset"
								url="${request.contextPath}/AssetStatus/updatefield" type="text"
								title="Enter asset" onsuccess="reloadAssetStatusTable();" />--}%
							
								<g:each in="${assetStatusInstance.asset}" var="a">
								<g:link controller="asset" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link>
								</g:each>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetStatusInstance?.assetOpname}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="assetOpname-label" class="property-label"><g:message
					code="assetStatus.assetOpname.label" default="Asset Top Name" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="assetOpname-label">
						%{--<ba:editableValue
								bean="${assetStatusInstance}" field="assetTopName"
								url="${request.contextPath}/AssetStatus/updatefield" type="text"
								title="Enter assetTopName" onsuccess="reloadAssetStatusTable();" />--}%
							
								<g:each in="${assetStatusInstance.assetOpname}" var="a">
								<g:link controller="assetOpname" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link>
								</g:each>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetStatusInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="assetStatus.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${assetStatusInstance}" field="dateCreated"
								url="${request.contextPath}/AssetStatus/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadAssetStatusTable();" />--}%
							
								<g:formatDate date="${assetStatusInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetStatusInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="assetStatus.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${assetStatusInstance}" field="lastUpdated"
								url="${request.contextPath}/AssetStatus/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadAssetStatusTable();" />--}%
							
								<g:formatDate date="${assetStatusInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('assetStatus');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${assetStatusInstance?.id}"
					update="[success:'assetStatus-form',failure:'assetStatus-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteAssetStatus('${assetStatusInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
