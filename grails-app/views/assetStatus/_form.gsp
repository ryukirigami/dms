<%@ page import="com.kombos.finance.AssetStatus" %>



<div class="control-group fieldcontain ${hasErrors(bean: assetStatusInstance, field: 'assetStatus', 'error')} required">
	<label class="control-label" for="assetStatus">
		<g:message code="assetStatus.assetStatus.label" default="Asset Status" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="assetStatus" required="" value="${assetStatusInstance?.assetStatus}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: assetStatusInstance, field: 'description', 'error')} required">
	<label class="control-label" for="description">
		<g:message code="assetStatus.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:textField name="description" required="" value="${assetStatusInstance?.description}"/>--}%
        <g:textArea name="description" required="" value="${assetStatusInstance?.description}" maxlength="256" />
	</div>
</div>

%{--
<div class="control-group fieldcontain ${hasErrors(bean: assetStatusInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="assetStatus.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${assetStatusInstance?.lastUpdProcess}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: assetStatusInstance, field: 'asset', 'error')} ">
	<label class="control-label" for="asset">
		<g:message code="assetStatus.asset.label" default="Asset" />
		
	</label>
	<div class="controls">
	
<ul class="one-to-many">
<g:each in="${assetStatusInstance?.asset?}" var="a">
    <li><g:link controller="asset" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="asset" action="create" params="['assetStatus.id': assetStatusInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'asset.label', default: 'Asset')])}</g:link>
</li>
</ul>

	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: assetStatusInstance, field: 'assetTopName', 'error')} ">
	<label class="control-label" for="assetTopName">
		<g:message code="assetStatus.assetTopName.label" default="Asset Top Name" />
		
	</label>
	<div class="controls">
	
<ul class="one-to-many">
<g:each in="${assetStatusInstance?.assetTopName?}" var="a">
    <li><g:link controller="assetTopName" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="assetTopName" action="create" params="['assetStatus.id': assetStatusInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'assetTopName.label', default: 'AssetTopName')])}</g:link>
</li>
</ul>

	</div>
</div>

--}%
