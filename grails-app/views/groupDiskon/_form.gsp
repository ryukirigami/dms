<%@ page import="com.kombos.parts.Group; com.kombos.parts.GroupDiskon" %>

<script type="text/javascript">
    jQuery(function($) {
        $('.persen').autoNumeric('init',{
            vMin:'0',
            vMax:'100',
            aSep:'',
            mDec: '2'
        });
    });
    var checkin = $('#tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
                checkin.hide();
                $('#tanggal2')[0].focus();
            }).data('datepicker');

    var checkout = $('#tanggal2').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
                checkout.hide();
            }).data('datepicker');
</script>


<div class="control-group fieldcontain ${hasErrors(bean: groupDiskonInstance, field: 'm172TglAwal', 'error')} ">
    <label class="control-label" for="m172TglAwal">
        <g:message code="groupDiskon.m172TglAwal.label" default="Tgl Awal" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <ba:datePicker name="m172TglAwal" id="tanggal" precision="day"  value="${groupDiskonInstance?.m172TglAwal}" format="dd/mm/yyyy" required="true"  />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: groupDiskonInstance, field: 'm172TglAkhir', 'error')} ">
    <label class="control-label" for="m172TglAkhir">
        <g:message code="groupDiskon.m172TglAkhir.label" default="Tgl Akhir" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <ba:datePicker name="m172TglAkhir" id="tanggal2" precision="day"  value="${groupDiskonInstance?.m172TglAkhir}" format="dd/mm/yyyy" required="true" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: groupDiskonInstance, field: 'group', 'error')} ">
	<label class="control-label" for="group">
		<g:message code="groupDiskon.group.label" default="Group" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
    	<g:select id="group" name="group.id" from="${Group.createCriteria().list(){eq("staDel", "0")}}" optionKey="id" value="${groupDiskonInstance?.group?.id}" required="true" class="many-to-one" noSelection="['null': '']"  />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: groupDiskonInstance, field: 'm172PersenDiskon', 'error')} ">
	<label class="control-label" for="m172PersenDiskon">
		<g:message code="groupDiskon.m172PersenDiskon.label" default="Persen Diskon" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
	    <g:textField name="m172PersenDiskon" class="persen" maxlength="3" value="${fieldValue(bean: groupDiskonInstance, field: 'm172PersenDiskon')}" required="true" />%
	</div>
</div>

<g:javascript>
    document.getElementById("m172TglAwal").focus();
</g:javascript>