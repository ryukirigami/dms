<%@ page import="com.kombos.parts.GroupDiskon" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>


<table id="groupDiskon_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="groupDiskon.m172TglAwal.label" default="Tgl Awal" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="groupDiskon.m172TglAkhir.label" default="Tgl Akhir" /></div>
			</th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="groupDiskon.group.label" default="Nama Group" /></div>
            </th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="groupDiskon.m172PersenDiskon.label" default="Persen Diskon" /></div>
			</th>
		</tr>
		<tr>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m172TglAwal" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m172TglAwal" value="date.struct">
					<input type="hidden" name="search_m172TglAwal_day" id="search_m172TglAwal_day" value="">
					<input type="hidden" name="search_m172TglAwal_month" id="search_m172TglAwal_month" value="">
					<input type="hidden" name="search_m172TglAwal_year" id="search_m172TglAwal_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m172TglAwal_dp" value="" id="search_m172TglAwal" class="search_init">
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m172TglAkhir" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m172TglAkhir" value="date.struct">
					<input type="hidden" name="search_m172TglAkhir_day" id="search_m172TglAkhir_day" value="">
					<input type="hidden" name="search_m172TglAkhir_month" id="search_m172TglAkhir_month" value="">
					<input type="hidden" name="search_m172TglAkhir_year" id="search_m172TglAkhir_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m172TglAkhir_dp" value="" id="search_m172TglAkhir" class="search_init">
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_group" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_group" class="search_init" />
                </div>
            </th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m172PersenDiskon" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m172PersenDiskon"  class="persen" maxlength="3" />
				</div>
			</th>

		</tr>
	</thead>
</table>

<g:javascript>
var groupDiskonTable;
var reloadGroupDiskonTable;
$(function(){
	
	reloadGroupDiskonTable = function() {
		groupDiskonTable.fnDraw();
	}

    var recordsGroupDiskonPerPage = [];
    var anGroupDiskonSelected;
    var jmlRecGroupDiskonPerPage=0;
    var id;

	$('#search_m172TglAwal').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m172TglAwal_day').val(newDate.getDate());
			$('#search_m172TglAwal_month').val(newDate.getMonth()+1);
			$('#search_m172TglAwal_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			groupDiskonTable.fnDraw();	
	});

	

	$('#search_m172TglAkhir').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m172TglAkhir_day').val(newDate.getDate());
			$('#search_m172TglAkhir_month').val(newDate.getMonth()+1);
			$('#search_m172TglAkhir_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			groupDiskonTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	groupDiskonTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	groupDiskonTable = $('#groupDiskon_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsGroupDiskon = $("#groupDiskon_datatables tbody .row-select");
            var jmlGroupDiskonCek = 0;
            var nRow;
            var idRec;
            rsGroupDiskon.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsGroupDiskonPerPage[idRec]=="1"){
                    jmlGroupDiskonCek = jmlGroupDiskonCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsGroupDiskonPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecGroupDiskonPerPage = rsGroupDiskon.length;
            if(jmlGroupDiskonCek==jmlRecGroupDiskonPerPage){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [




{
	"sName": "m172TglAwal",
	"mDataProp": "m172TglAwal",
	"aTargets": [0],
		"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},

	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m172TglAkhir",
	"mDataProp": "m172TglAkhir",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "group",
	"mDataProp": "group",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "m172PersenDiskon",
	"mDataProp": "m172PersenDiskon",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}
	
						var group = $('#filter_group input').val();
						if(group){
							aoData.push(
									{"name": 'sCriteria_group', "value": group}
							);
						}

						var m172TglAwal = $('#search_m172TglAwal').val();
						var m172TglAwalDay = $('#search_m172TglAwal_day').val();
						var m172TglAwalMonth = $('#search_m172TglAwal_month').val();
						var m172TglAwalYear = $('#search_m172TglAwal_year').val();
						
						if(m172TglAwal){
							aoData.push(
									{"name": 'sCriteria_m172TglAwal', "value": "date.struct"},
									{"name": 'sCriteria_m172TglAwal_dp', "value": m172TglAwal},
									{"name": 'sCriteria_m172TglAwal_day', "value": m172TglAwalDay},
									{"name": 'sCriteria_m172TglAwal_month', "value": m172TglAwalMonth},
									{"name": 'sCriteria_m172TglAwal_year', "value": m172TglAwalYear}
							);
						}

						var m172TglAkhir = $('#search_m172TglAkhir').val();
						var m172TglAkhirDay = $('#search_m172TglAkhir_day').val();
						var m172TglAkhirMonth = $('#search_m172TglAkhir_month').val();
						var m172TglAkhirYear = $('#search_m172TglAkhir_year').val();
						
						if(m172TglAkhir){
							aoData.push(
									{"name": 'sCriteria_m172TglAkhir', "value": "date.struct"},
									{"name": 'sCriteria_m172TglAkhir_dp', "value": m172TglAkhir},
									{"name": 'sCriteria_m172TglAkhir_day', "value": m172TglAkhirDay},
									{"name": 'sCriteria_m172TglAkhir_month', "value": m172TglAkhirMonth},
									{"name": 'sCriteria_m172TglAkhir_year', "value": m172TglAkhirYear}
							);
						}
	
						var m172PersenDiskon = $('#filter_m172PersenDiskon input').val();
						if(m172PersenDiskon){
							aoData.push(
									{"name": 'sCriteria_m172PersenDiskon', "value": m172PersenDiskon}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#groupDiskon_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsGroupDiskonPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsGroupDiskonPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#groupDiskon_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsGroupDiskonPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anGroupDiskonSelected = groupDiskonTable.$('tr.row_selected');

            if(jmlRecGroupDiskonPerPage == anGroupDiskonSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsGroupDiskonPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>



<script type="text/javascript">
    jQuery(function($) {
        $('.persen').autoNumeric('init',{
            vMin:'0',
            vMax:'100',
            mDec: '2',
            aSep:''
        });
    });
</script>

