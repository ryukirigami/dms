

<%@ page import="com.kombos.parts.GroupDiskon" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'groupDiskon.label', default: 'Discount Per Group')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteGroupDiskon;

$(function(){ 
	deleteGroupDiskon=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/groupDiskon/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadGroupDiskonTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-groupDiskon" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="groupDiskon"
			class="table table-bordered table-hover">
			<tbody>
            <g:if test="${groupDiskonInstance?.m172TglAwal}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m172TglAwal-label" class="property-label"><g:message
                                code="groupDiskon.m172TglAwal.label" default="Tgl Awal" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m172TglAwal-label">
                        %{--<ba:editableValue
                                bean="${groupDiskonInstance}" field="m172TglAwal"
                                url="${request.contextPath}/GroupDiskon/updatefield" type="text"
                                title="Enter m172TglAwal" onsuccess="reloadGroupDiskonTable();" />--}%

                        <g:formatDate date="${groupDiskonInstance?.m172TglAwal}" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${groupDiskonInstance?.m172TglAkhir}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m172TglAkhir-label" class="property-label"><g:message
                                code="groupDiskon.m172TglAkhir.label" default="Tgl Akhir" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m172TglAkhir-label">
                        %{--<ba:editableValue
                                bean="${groupDiskonInstance}" field="m172TglAkhir"
                                url="${request.contextPath}/GroupDiskon/updatefield" type="text"
                                title="Enter m172TglAkhir" onsuccess="reloadGroupDiskonTable();" />--}%

                        <g:formatDate date="${groupDiskonInstance?.m172TglAkhir}" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${groupDiskonInstance?.group}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="group-label" class="property-label"><g:message
					code="groupDiskon.group.label" default="Group" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="group-label">
						%{--<ba:editableValue
								bean="${groupDiskonInstance}" field="group"
								url="${request.contextPath}/GroupDiskon/updatefield" type="text"
								title="Enter group" onsuccess="reloadGroupDiskonTable();" />--}%
							
                        <g:fieldValue bean="${groupDiskonInstance?.group}" field="m181NamaGroup"/>
						</span></td>
					
				</tr>
				</g:if>
			

				<g:if test="${groupDiskonInstance?.m172PersenDiskon}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m172PersenDiskon-label" class="property-label"><g:message
					code="groupDiskon.m172PersenDiskon.label" default="% Diskon" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m172PersenDiskon-label">
						%{--<ba:editableValue
								bean="${groupDiskonInstance}" field="m172PersenDiskon"
								url="${request.contextPath}/GroupDiskon/updatefield" type="text"
								title="Enter m172PersenDiskon" onsuccess="reloadGroupDiskonTable();" />--}%
							
								<g:fieldValue bean="${groupDiskonInstance}" field="m172PersenDiskon"/>%
							
						</span></td>
					
				</tr>
				</g:if>

				<g:if test="${groupDiskonInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="groupDiskon.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${groupDiskonInstance}" field="createdBy"
								url="${request.contextPath}/GroupDiskon/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadGroupDiskonTable();" />--}%
							
								<g:fieldValue bean="${groupDiskonInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${groupDiskonInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="groupDiskon.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${groupDiskonInstance}" field="updatedBy"
								url="${request.contextPath}/GroupDiskon/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadGroupDiskonTable();" />--}%
							
								<g:fieldValue bean="${groupDiskonInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${groupDiskonInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="groupDiskon.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${groupDiskonInstance}" field="lastUpdProcess"
								url="${request.contextPath}/GroupDiskon/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadGroupDiskonTable();" />--}%
							
								<g:fieldValue bean="${groupDiskonInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${groupDiskonInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="groupDiskon.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${groupDiskonInstance}" field="dateCreated"
								url="${request.contextPath}/GroupDiskon/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadGroupDiskonTable();" />--}%
							
								<g:formatDate date="${groupDiskonInstance?.dateCreated}" format="EEEE, dd MMMM yyyy HH:mm"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${groupDiskonInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="groupDiskon.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${groupDiskonInstance}" field="lastUpdated"
								url="${request.contextPath}/GroupDiskon/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadGroupDiskonTable();" />--}%
							
								<g:formatDate date="${groupDiskonInstance?.lastUpdated}" format="EEEE, dd MMMM yyyy HH:mm"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${groupDiskonInstance?.id}"
					update="[success:'groupDiskon-form',failure:'groupDiskon-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteGroupDiskon('${groupDiskonInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
