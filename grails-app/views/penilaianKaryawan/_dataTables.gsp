
<%@ page import="com.kombos.hrd.PenilaianKaryawan" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="penilaianKaryawan_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="penilaianKaryawan.karyawan.label" default="Nama Karyawan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="penilaianKaryawan.periode.label" default="Periode" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="penilaianKaryawan.penilai.label" default="Penilai" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="penilaianKaryawan.jabatan.label" default="Jabatan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="penilaianKaryawan.teknis.label" default="Teknis" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="penilaianKaryawan.logika.label" default="Logika" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="penilaianKaryawan.kecepatanKetelitian.label" default="Kecepatan Ketelitian" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="penilaianKaryawan.loyalitas.label" default="Loyalitas" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="penilaianKaryawan.drive.label" default="Drive" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="penilaianKaryawan.disiplin.label" default="Disiplin" /></div>
			</th>

		</tr>
	</thead>
</table>

<g:javascript>
var penilaianKaryawanTable;
var reloadPenilaianKaryawanTable;
$(function(){
	
	reloadPenilaianKaryawanTable = function() {
		penilaianKaryawanTable.fnDraw();
	}

	$("#btnCari").click(function() {
	    reloadPenilaianKaryawanTable();
    });

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	penilaianKaryawanTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	penilaianKaryawanTable = $('#penilaianKaryawan_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "namaKaryawan",
	"mDataProp": "namaKaryawan",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "periode",
	"mDataProp": "periode",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "penilai",
	"mDataProp": "penilai",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "jabatan",
	"mDataProp": "jabatan",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "teknis",
	"mDataProp": "teknis",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}

,

{
	"sName": "logika",
	"mDataProp": "logika",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}

,

{
	"sName": "kecepatanKetelitian",
	"mDataProp": "kecepatanKetelitian",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "loyalitas",
	"mDataProp": "loyalitas",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}

,

{
	"sName": "drive",
	"mDataProp": "drive",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}

,

{
	"sName": "disiplin",
	"mDataProp": "disiplin",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}
,
{
	"sName": "karyawan",
	"mDataProp": "karyawan",
	"aTargets": [10],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": false
}
,
{
    "sName": "idPenilai",
	"mDataProp": "idPenilai",
	"aTargets": [11],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": false
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var karyawan = $('#sCriteria_namaKaryawan').val();
						if(karyawan){
							aoData.push(
									{"name": 'sCriteria_karyawan', "value": karyawan}
							);
						}
	
						var periode = $('#sCriteria_periode input').val();
						if(periode){
							aoData.push(
									{"name": 'sCriteria_periodeTahun', "value": periode}
							);
						}
	
						var penilai = $('#filter_penilai input').val();
						if(penilai){
							aoData.push(
									{"name": 'sCriteria_penilaiKaryawan', "value": penilai}
							);
						}

                        <g:if test="${session?.userCompanyDealer && session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                            aoData.push(
									{"name": 'ho', "value": true},
									{"name": 'cabang', "value": $("#companyDealer").val()}
							);
                        </g:if>

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
