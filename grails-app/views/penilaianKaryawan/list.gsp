
<%@ page import="com.kombos.hrd.PenilaianKaryawan" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'penilaianKaryawan.label', default: 'PenilaianKaryawan')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
        <style>
        .modal.fade.in {
            left: 25.5% !important;
            width: 1205px !important;
        }
        .modal.fade {
            top: -100%;
        }
        </style>
		<g:javascript>
			var show;
			var edit;
            var oFormService;

			$(function(){

			    oFormService = {
			         fnShowKaryawanDataTable: function() {
                        $("#karyawanModal").modal("show");
                        $("#karyawanModal").fadeIn();
			        },
			        fnHideKaryawanDataTable: function() {
			            $("#karyawanModal").modal("hide");
			        }
			    }

			    oFormService.fnHideKaryawanDataTable();

				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('penilaianKaryawan');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('penilaianKaryawan', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('penilaianKaryawan', '${request.contextPath}/penilaianKaryawan/massdelete', reloadPenilaianKaryawanTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('penilaianKaryawan','${request.contextPath}/penilaianKaryawan/show/'+id);
				};
				
				edit = function(id) {
					editInstance('penilaianKaryawan','${request.contextPath}/penilaianKaryawan/edit/'+id);
				};

});


</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>

    <div class="box" id="box-search">
        <div class="span12">
            <fieldset>
                <table>
                    <g:if test="${session?.userCompanyDealer && session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                        <tr style="display:table-row;">
                            <td style="width: 130px; display:table-cell; padding:5px;">
                                Company Dealer
                            </td>
                            <td>
                                <g:select name="companyDealer" id="companyDealer" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                            </td>
                        </tr>
                    </g:if>
                    <tr style="display:table-row;">
                        <td style="width: 130px; display:table-cell; padding:5px;">
                            <label class="control-label" for="sCriteria_namaKaryawan">Nama Karyawan</label>
                        </td>
                        <td>
                            <input type="hidden" id="sCriteria_idKaryawan">
                            <input type="text" id="sCriteria_namaKaryawan">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 130px; display:table-cell; padding:5px;">
                            <label class="control-label" for="sCriteria_periode">Periode</label>
                        </td>
                        <td >
                            <input type="text" class="input-small" id="sCriteria_periode">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 130px; display:table-cell; padding:5px;">
                            <label class="control-label" for="sCriteria_penilai">Penilai</label>
                        </td>
                        <td>
                            <input type="text" id="sCriteria_penilai">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 130px; display:table-cell; padding:5px;">
                        </td>
                        <td colspan="2">
                            <a id="btnCari" class="btn btn-primary" href="javascript:void(0);">
                                Cari
                            </a>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
    </div>


    <div class="box">
		<div class="span12" id="penilaianKaryawan-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
		</div>
		<div class="span7" id="penilaianKaryawan-form" style="display: none;"></div>
	</div>
</body>
</html>
