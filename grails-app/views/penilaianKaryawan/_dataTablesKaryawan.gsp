<%@ page import="com.kombos.hrd.Karyawan" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="karyawan_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover fixed" style="table-layout: fixed; ">
    <col width="143px" />
    <col width="143px" />
    <col width="143px" />
    <col width="143px" />
    <col width="143px" />
    <col width="143px" />
    <thead>
    <tr>
        <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:142px;">
            NPK
        </th>
        <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:142px;">
            Nama Karyawan
        </th>
        <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:142px;">
            Tanggal Lahir
        </th>
        <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:142px;">
            Alamat
        </th>
        <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:142px;">
            Pekerjaan / Departemen
        </th>
        <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:142px;">
            Status Karyawan
        </th>
    </tr>
    <tr>

        <th style="border-top: none;padding: 0px 0px 5px 0px; width:142px;">

        </th>

        <th style="border-top: none;padding: 0px 0px 5px 0px; width:142px;">

        </th>

        <th style="border-top: none;padding: 0px 0px 5px 0px; width:142px;">

        </th>

        <th style="border-top: none;padding: 0px 0px 5px 0px; width:142px;">

        </th>

        <th style="border-top: none;padding: 0px 0px 5px 0px; width:142px;">

        </th>

        <th style="border-top: none;padding: 0px 0px 5px 0px; width:142px;">

        </th>

    </tr>
    </thead>

</table>

<g:javascript>
var karyawanTable;
var reloadKaryawanTable;
var btnCari2;
$(function(){

    $("#btnPilihKaryawan").click(function() {
        $("#karyawan_datatables tbody").find("tr").each(function() {
            var radio = $(this).find("td:eq(0) input[type='radio']");
            if (radio.is(":checked")) {

                if(btnCari2 == "karyawan"){
                    var id = radio.data("id");
                    var nama = radio.data("nama");
                    //console.log(radio.data("jbt"))
                $("#karyawan_id").val(id);
                $("#karyawan_nama").val(nama);
                }else{
                    var id = radio.data("id");
                    var nama = radio.data("nama");
                    var jabatan = radio.data("jbt");

                $("#penilai_id").val(id);
                $("#penilai_nama").val(nama);
                $("#jabatan_pen").val(jabatan);
                }
                oFormService.fnHideKaryawanDataTable();
            }
        })
    });


	reloadKaryawanTable = function() {
		karyawanTable.fnDraw();
		}

    $("#btnSearchKaryawan").click(function() {
	    reloadKaryawanTable();
    });

	$('#search_tanggalLahir').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tanggalLahir_day').val(newDate.getDate());
			$('#search_tanggalLahir_month').val(newDate.getMonth()+1);
			$('#search_tanggalLahir_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			karyawanTable.fnDraw();
	});



	$('#search_tanggalMasuk').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tanggalMasuk_day').val(newDate.getDate());
			$('#search_tanggalMasuk_month').val(newDate.getMonth()+1);
			$('#search_tanggalMasuk_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			karyawanTable.fnDraw();
	});

	karyawanTable = $('#karyawan_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : true,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : 4, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList", controller: "karyawan")}",
		"aoColumns": [

{
	"sName": "nomorPokokKaryawan",
	"mDataProp": "nomorPokokKaryawan",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return "<input type='radio' data-id='"+row["id"]+"' data-nama='"+row["nama"]+"' data-npk='"+row["nomorPokokKaryawan"]+"' data-jbt='"+row["jabatan"]+"' data-id='"+row["id"]+"' name='radio' class='pull-left row-select' style='position: relative; top: 3px;' aria-label='Row " + row["id"] + "' title='Select this'><input type='hidden' value='"+row["id"]+"'>&nbsp;&nbsp;"+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}

,

{
	"sName": "nama",
	"mDataProp": "nama",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}

,

{
	"sName": "tanggalLahir",
	"mDataProp": "tanggalLahir",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}

,

{
	"sName": "alamat",
	"mDataProp": "alamat",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}

,

{
	"sName": "jabatan",
	"mDataProp": "jabatan",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}

,

{
	"sName": "statusKaryawan",
	"mDataProp": "statusKaryawan",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                        var namaKaryawan = $("#sCriteria_nama").val();

                        if (namaKaryawan) {
                            aoData.push({"name": "sCriteria_nama", "value": namaKaryawan});
                        }


$.ajax({ "dataType": 'json',
    "type": "POST",
    "url": sSource,
    "data": aoData ,
    "success": function (json) {
        fnCallback(json);
       },
    "complete": function () {
       }
});
}
});

});
</g:javascript>



