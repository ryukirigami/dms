

<%@ page import="com.kombos.hrd.PenilaianKaryawan" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'penilaianKaryawan.label', default: 'PenilaianKaryawan')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deletePenilaianKaryawan;

$(function(){ 
	deletePenilaianKaryawan=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/penilaianKaryawan/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadPenilaianKaryawanTable();
   				expandTableLayout('penilaianKaryawan');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-penilaianKaryawan" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="penilaianKaryawan"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${penilaianKaryawanInstance?.karyawan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="karyawan-label" class="property-label"><g:message
					code="penilaianKaryawan.karyawan.label" default="Karyawan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="karyawan-label">
						%{--<ba:editableValue
								bean="${penilaianKaryawanInstance}" field="karyawan"
								url="${request.contextPath}/PenilaianKaryawan/updatefield" type="text"
								title="Enter karyawan" onsuccess="reloadPenilaianKaryawanTable();" />--}%
							
								<g:link controller="karyawan" action="show" id="${penilaianKaryawanInstance?.karyawan?.id}">${penilaianKaryawanInstance?.karyawan?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${penilaianKaryawanInstance?.periode}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="periode-label" class="property-label"><g:message
					code="penilaianKaryawan.periode.label" default="Periode" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="periode-label">
						%{--<ba:editableValue
								bean="${penilaianKaryawanInstance}" field="periode"
								url="${request.contextPath}/PenilaianKaryawan/updatefield" type="text"
								title="Enter periode" onsuccess="reloadPenilaianKaryawanTable();" />--}%
							
								<g:fieldValue bean="${penilaianKaryawanInstance}" field="periode"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${penilaianKaryawanInstance?.penilai}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="penilai-label" class="property-label"><g:message
					code="penilaianKaryawan.penilai.label" default="Penilai" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="penilai-label">
						%{--<ba:editableValue
								bean="${penilaianKaryawanInstance}" field="penilai"
								url="${request.contextPath}/PenilaianKaryawan/updatefield" type="text"
								title="Enter penilai" onsuccess="reloadPenilaianKaryawanTable();" />--}%
							
								<g:fieldValue bean="${penilaianKaryawanInstance}" field="penilai"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${penilaianKaryawanInstance?.jabatan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="jabatan-label" class="property-label"><g:message
					code="penilaianKaryawan.jabatan.label" default="Jabatan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="jabatan-label">
						%{--<ba:editableValue
								bean="${penilaianKaryawanInstance}" field="jabatan"
								url="${request.contextPath}/PenilaianKaryawan/updatefield" type="text"
								title="Enter jabatan" onsuccess="reloadPenilaianKaryawanTable();" />--}%
							
								<g:fieldValue bean="${penilaianKaryawanInstance?.jabatan}" field="m014JabatanManPower"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${penilaianKaryawanInstance?.teknis}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="teknis-label" class="property-label"><g:message
					code="penilaianKaryawan.teknis.label" default="Teknis" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="teknis-label">
						%{--<ba:editableValue
								bean="${penilaianKaryawanInstance}" field="teknis"
								url="${request.contextPath}/PenilaianKaryawan/updatefield" type="text"
								title="Enter teknis" onsuccess="reloadPenilaianKaryawanTable();" />--}%
							
								<g:fieldValue bean="${penilaianKaryawanInstance}" field="teknis"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${penilaianKaryawanInstance?.logika}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="logika-label" class="property-label"><g:message
					code="penilaianKaryawan.logika.label" default="Logika" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="logika-label">
						%{--<ba:editableValue
								bean="${penilaianKaryawanInstance}" field="logika"
								url="${request.contextPath}/PenilaianKaryawan/updatefield" type="text"
								title="Enter logika" onsuccess="reloadPenilaianKaryawanTable();" />--}%
							
								<g:fieldValue bean="${penilaianKaryawanInstance}" field="logika"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${penilaianKaryawanInstance?.kecepatanKetelitian}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kecepatanKetelitian-label" class="property-label"><g:message
					code="penilaianKaryawan.kecepatanKetelitian.label" default="Kecepatan Ketelitian" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kecepatanKetelitian-label">
						%{--<ba:editableValue
								bean="${penilaianKaryawanInstance}" field="kecepatanKetelitian"
								url="${request.contextPath}/PenilaianKaryawan/updatefield" type="text"
								title="Enter kecepatanKetelitian" onsuccess="reloadPenilaianKaryawanTable();" />--}%
							
								<g:fieldValue bean="${penilaianKaryawanInstance}" field="kecepatanKetelitian"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${penilaianKaryawanInstance?.loyalitas}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="loyalitas-label" class="property-label"><g:message
					code="penilaianKaryawan.loyalitas.label" default="Loyalitas" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="loyalitas-label">
						%{--<ba:editableValue
								bean="${penilaianKaryawanInstance}" field="loyalitas"
								url="${request.contextPath}/PenilaianKaryawan/updatefield" type="text"
								title="Enter loyalitas" onsuccess="reloadPenilaianKaryawanTable();" />--}%
							
								<g:fieldValue bean="${penilaianKaryawanInstance}" field="loyalitas"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${penilaianKaryawanInstance?.drive}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="drive-label" class="property-label"><g:message
					code="penilaianKaryawan.drive.label" default="Drive" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="drive-label">
						%{--<ba:editableValue
								bean="${penilaianKaryawanInstance}" field="drive"
								url="${request.contextPath}/PenilaianKaryawan/updatefield" type="text"
								title="Enter drive" onsuccess="reloadPenilaianKaryawanTable();" />--}%
							
								<g:fieldValue bean="${penilaianKaryawanInstance}" field="drive"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${penilaianKaryawanInstance?.disiplin}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="disiplin-label" class="property-label"><g:message
					code="penilaianKaryawan.disiplin.label" default="Disiplin" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="disiplin-label">
						%{--<ba:editableValue
								bean="${penilaianKaryawanInstance}" field="disiplin"
								url="${request.contextPath}/PenilaianKaryawan/updatefield" type="text"
								title="Enter disiplin" onsuccess="reloadPenilaianKaryawanTable();" />--}%
							
								<g:fieldValue bean="${penilaianKaryawanInstance}" field="disiplin"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${penilaianKaryawanInstance?.stadel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="stadel-label" class="property-label"><g:message
					code="penilaianKaryawan.stadel.label" default="Stadel" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="stadel-label">
						%{--<ba:editableValue
								bean="${penilaianKaryawanInstance}" field="stadel"
								url="${request.contextPath}/PenilaianKaryawan/updatefield" type="text"
								title="Enter stadel" onsuccess="reloadPenilaianKaryawanTable();" />--}%
							
								<g:fieldValue bean="${penilaianKaryawanInstance}" field="stadel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${penilaianKaryawanInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="penilaianKaryawan.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${penilaianKaryawanInstance}" field="createdBy"
								url="${request.contextPath}/PenilaianKaryawan/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadPenilaianKaryawanTable();" />--}%
							
								<g:fieldValue bean="${penilaianKaryawanInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${penilaianKaryawanInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="penilaianKaryawan.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${penilaianKaryawanInstance}" field="updatedBy"
								url="${request.contextPath}/PenilaianKaryawan/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadPenilaianKaryawanTable();" />--}%
							
								<g:fieldValue bean="${penilaianKaryawanInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${penilaianKaryawanInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="penilaianKaryawan.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${penilaianKaryawanInstance}" field="lastUpdProcess"
								url="${request.contextPath}/PenilaianKaryawan/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadPenilaianKaryawanTable();" />--}%
							
								<g:fieldValue bean="${penilaianKaryawanInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${penilaianKaryawanInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="penilaianKaryawan.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${penilaianKaryawanInstance}" field="dateCreated"
								url="${request.contextPath}/PenilaianKaryawan/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadPenilaianKaryawanTable();" />--}%
							
								<g:formatDate date="${penilaianKaryawanInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${penilaianKaryawanInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="penilaianKaryawan.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${penilaianKaryawanInstance}" field="lastUpdated"
								url="${request.contextPath}/PenilaianKaryawan/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadPenilaianKaryawanTable();" />--}%
							
								<g:formatDate date="${penilaianKaryawanInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('penilaianKaryawan');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${penilaianKaryawanInstance?.id}"
					update="[success:'penilaianKaryawan-form',failure:'penilaianKaryawan-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deletePenilaianKaryawan('${penilaianKaryawanInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
