<%@ page import="com.kombos.hrd.PenilaianKaryawan" %>

<g:javascript>
        var cek;
		$(function(){

			$("#btnCariKaryawan").click(function() {
			    btnCari2 = "karyawan";
                $("#karyawanModal").modal({
                    "backdrop" : "dynamic",
                    "keyboard" : true,
                    "show" : true
                }).css({'width': '900px','margin-left': function () {return -($(this).width() / 2);}});
//                oFormService.fnShowKaryawanDataTable();
            })

            $("#btnCariPenilai").click(function() {
                btnCari2 = "penilai";
                $("#karyawanModal").modal({
                    "backdrop" : "dynamic",
                    "keyboard" : true,
                    "show" : true
                }).css({'width': '900px','margin-left': function () {return -($(this).width() / 2);}});
//                oFormService.fnShowKaryawanDataTable();
            })

            $("#btnCloseKaryawan").click(function(){
                $("#karyawanModal").modal("hide");
            });

        });

        function isNumberKey(evt)
        {
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

</g:javascript>


<div class="control-group fieldcontain ${hasErrors(bean: penilaianKaryawanInstance, field: 'karyawan', 'error')} required">
	<label class="control-label" for="karyawan">
		<g:message code="penilaianKaryawan.nmKaryawan.label" default="Nama Karyawan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <div class="input-append">
            <g:hiddenField name="karyawan.id" readonly="readonly" id="karyawan_id" value="${penilaianKaryawanInstance?.karyawan?.id}"/>
            <input type="text" value="${penilaianKaryawanInstance?.karyawan?.nama}" name="nmKaryawan" readonly id="karyawan_nama" required="true"/>
            <span class="add-on">

                <g:if test="${!penilaianKaryawanInstance?.karyawan?.nama}">
                <a href="javascript:void(0);" id="btnCariKaryawan">
                    <i class="icon-search"/>
                </a>
                </g:if>

            </span>
        </div>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: penilaianKaryawanInstance, field: 'periode', 'error')} required">
	<label class="control-label" for="periode">
		<g:message code="penilaianKaryawan.periode.label" default="Periode" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="periode" required="true" value="${penilaianKaryawanInstance?.periode}" onkeypress="return isNumberKey(event);" placeholder="2016" maxlength="4" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: penilaianKaryawanInstance, field: 'penilai', 'error')} required">
	<label class="control-label" for="penilai">
		<g:message code="penilaianKaryawan.penilai.label" default="Penilai" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <div class="input-append">
            <g:hiddenField name="penilai.id" readonly="readonly" id="penilai_id" value="${penilaianKaryawanInstance?.penilai?.id}"/>
            <input type="text" value="${penilaianKaryawanInstance?.penilai}" name="nmPenilai" readonly id="penilai_nama" required />
            <span class="add-on">
                <a href="javascript:void(0);" id="btnCariPenilai">
                    <i class="icon-search"/>
                </a>
            </span>
        </div>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: penilaianKaryawanInstance, field: 'jabatan', 'error')} required">
	<label class="control-label" for="jabatan">
		<g:message code="penilaianKaryawan.jabatan.label" default="Jabatan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="jabatanPen" value="${penilaianKaryawanInstance?.jabatan?.m014JabatanManPower}" readonly="true" id="jabatan_pen" required="true" />
	</div>
</div>

<br>
<label style="padding-left: 180px">
    <strong>Aspek Penilaian</strong>
</label>

<div class="control-group fieldcontain ${hasErrors(bean: penilaianKaryawanInstance, field: 'teknis', 'error')} required">
	<label class="control-label" for="teknis">
		<g:message code="penilaianKaryawan.teknis.label" default="Teknis" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:radioGroup name="teknis" id="teknis" value="${penilaianKaryawanInstance?.teknis}"
                      labels="['1','2','3','4','5','6']"
                      values="[1,2,3,4,5,6]">
            ${it.radio} ${it.label}
        </g:radioGroup>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: penilaianKaryawanInstance, field: 'logika', 'error')} required">
	<label class="control-label" for="logika">
		<g:message code="penilaianKaryawan.logika.label" default="Logika" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:radioGroup name="logika" id="logika" value="${penilaianKaryawanInstance?.logika}"
                      labels="['1','2','3','4','5','6']"
                      values="[1,2,3,4,5,6]">
            ${it.radio} ${it.label}
        </g:radioGroup>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: penilaianKaryawanInstance, field: 'kecepatanKetelitian', 'error')} required">
	<label class="control-label" for="kecepatanKetelitian">
		<g:message code="penilaianKaryawan.kecepatanKetelitian.label" default="Kecepatan Ketelitian" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:radioGroup name="kecepatanKetelitian" id="kecepatanKetelitian" value="${penilaianKaryawanInstance?.kecepatanKetelitian}"
                      labels="['1','2','3','4','5','6']"
                      values="[1,2,3,4,5,6]">
            ${it.radio} ${it.label}
        </g:radioGroup>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: penilaianKaryawanInstance, field: 'loyalitas', 'error')} required">
	<label class="control-label" for="loyalitas">
		<g:message code="penilaianKaryawan.loyalitas.label" default="Loyalitas" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:radioGroup name="loyalitas" id="loyalitas" value="${penilaianKaryawanInstance?.loyalitas}"
                      labels="['1','2','3','4','5','6']"
                      values="[1,2,3,4,5,6]">
            ${it.radio} ${it.label}
        </g:radioGroup>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: penilaianKaryawanInstance, field: 'drive', 'error')} required">
	<label class="control-label" for="drive">
		<g:message code="penilaianKaryawan.drive.label" default="Drive" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:radioGroup name="drive" id="drive" value="${penilaianKaryawanInstance?.drive}"
                      labels="['1','2','3','4','5','6']"
                      values="[1,2,3,4,5,6]">
            ${it.radio} ${it.label}
        </g:radioGroup>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: penilaianKaryawanInstance, field: 'disiplin', 'error')} required">
	<label class="control-label" for="disiplin">
		<g:message code="penilaianKaryawan.disiplin.label" default="Disiplin" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:radioGroup name="disiplin" id="disiplin" value="${penilaianKaryawanInstance?.disiplin}"
                      labels="['1','2','3','4','5','6']"
                      values="[1,2,3,4,5,6]">
            ${it.radio} ${it.label}
        </g:radioGroup>
	</div>
</div>

<g:if test="${!params.onDataKaryawan}">

    <!-- Karyawan Modal -->

     <div id="karyawanModal" class="modal fade">
        <div class="modal-dialog" style="width: 1100px;">
            <div class="modal-content" style="width: 1100px;">
                <div class="modal-header">
                    <a class="close" href="javascript:void(0);" data-dismiss="modal">×</a>
                    <h4>Data Karyawan - List</h4>
                </div>
                <!-- dialog body -->
                <div class="modal-body" id="karyawanModal-body" style="max-height: 1100px;">
                    <div class="box">
                        <div class="span12">
                            <fieldset>
                                <table>
                                    <tr style="display:table-row;">
                                        <td style="width: 130px; display:table-cell; padding:5px;">
                                            <label class="control-label" for="sCriteria_nama">Nama Karyawan</label>
                                        </td>
                                        <td style="width: 130px; display:table-cell; padding:5px;">
                                            <input type="text" id="sCriteria_nama">
                                        </td>
                                        <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                            <td style="width: 130px; display:table-cell; padding:5px;">
                                                <g:select name="sCriteria_cabang" id="sCriteria_cabang" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" noSelection="['':'SEMUA CABANG']"/>
                                            </td>
                                        </g:if>
                                        <td >
                                            <a id="btnSearchKaryawan" class="btn btn-primary" href="javascript:void(0);">
                                                Cari
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </div>
                    </div>
                    <g:render template="dataTablesKaryawan" />
                </div>

                <div class="modal-footer">
                    <a href="javascript:void(0);" id="btnCloseKaryawan" class="btn cancel">
                        Tutup
                    </a>

                    <a href="javascript:void(0);" id="btnPilihKaryawan" class="btn btn-primary">
                        Pilih Karyawan
                    </a>
                </div>
            </div>
        %{--</div>--}%
    </div>

</g:if>
