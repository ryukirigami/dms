
<%@ page import="com.kombos.board.JPB" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="JPB_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
        <tr>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" rowspan="3">
                <div>DRIVER</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="34">
                <div>JOB PROGRESS BOARD DRIVER<br><p id="lblTglView" class="lblTglView">${params?.tglHeader}</p></div>
            </th>
        </tr>
        <tr>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>07:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>08:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>09:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>10:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>11:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>12:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>13:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>14:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>15:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>16:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>17:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>18:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>19:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>20:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>21:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>22:00</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>23:00</div>
            </th>

        </tr>
        <tr>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div>&nbsp;</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div> </div>
            </th>
        </tr>

	</thead>
</table>

<g:javascript>
var JPBTable;
var reloadJPBTable;
var getForemanInfo;
var getTeknisiInfo;

var getOnProgressInfo;
$(function(){

	 getForemanInfo = function(id){
    //  $(document).tooltip({ content: "Awesome title!" }).show();

        var content = "nocontent";
            $.ajax({type:'POST', url:'${request.contextPath}/groupManPower/getManPowerInfo/'+id,
                success:function(data,textStatus){
                    if(data){

                       content = data.namaLengkap + " - " + data.jabatan;
                       content = content +"</br>Group : "+data.group;
                       content = content +"</br>Anggota Group : </br>"

                       if(data.anggota){
                            jQuery.each(data.anggota, function (index, value) {
                                content = content + "</br>"+value;
                            });
                       }


                     $("#foreman"+id).tooltip({
                        html : true,
                        title : content,
                        position: 'center right'
                    });

                    }

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){

                }
            });

    }

    getOnProgressInfo = function(idJPB, tag, info){

         var content = "nocontent";

 //        console.log("info : "+info);

        if(info != "BREAK"){
        $.ajax({type:'GET', url:'${request.contextPath}/operation/getOnProgressOperation/',
            data : {bPOrGR : "0", idJPB : idJPB},
   			success:function(data,textStatus){
   				if(data){

   				   content = "No. WO : "+data.noWO;
                   content = content +"</br>No. Polisi : "+data.noPol;
                   content = content +"</br></br>Nama Job : "+data.namaJob;
                   content = content +"</br></br>Level : "+data.level;

                  if(info === "Y"){

                   content = content + "</br><span style='color:red'>JOB STOPPED</span></br>";
                   content = content + "</br><span style='color:red'>Alasan : "+data.alasan+"</span>";

                   }

                  if(info === "W"){

                   content = content + "</br><span style='color:red'>Reschedule Delivery</span></br>";
                   content = content + "</br><span style='color:red'>Alasan : "+data.alasan+"</span>";

                   }

                   if(info === "X"){

                   content = content + "</br><span style='color:red'>Reschedule JPB</span></br>";
                   content = content + "</br><span style='color:red'>Alasan : "+data.alasan+"</span>";

                   }

                   content = content +"</br></br>Current Progress : "+data.currProgress;
                   content = content +"</br>Start : "+data.start;
                   content = content +"</br>Target Finish : "+data.targetFinish;
                   content = content +"</br>Delivery Time : "+data.deliveryTime;


//   				 console.log("tag : "+tag);
  // 				 console.log("idjpb : "+idJPB);

   				 $("#jam"+tag+idJPB).tooltip({
                    html : true,
                    title : content,
                    position: 'center right'
                });

   				}

   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){

   			}
   		});

        }

    }



    getTeknisiInfo = function(idTeknisi, idJPB){
    //  $(document).tooltip({ content: "Awesome title!" }).show();

         var content = "nocontent";
        $.ajax({type:'GET', url:'${request.contextPath}/groupManPower/getTeknisiJPB/',
            data : {id : idTeknisi, idJPB : idJPB},
   			success:function(data,textStatus){
   				if(data){

   				   content = data.namaLengkap + " - " + data.jabatan;
                   content = content +"</br>Group : "+data.group;
                   content = content +"</br>Kepala Group : "+data.kepalaGroup;
                   content = content +"</br></br>";
                   content = content +"</br>Previous Job : "+data.prevJob;
                   content = content +"</br>No. WO : "+data.prevNoWO;
                   content = content +"</br>"+data.prevTime;

                   content = content +"</br></br>Current Job : "+data.currJob;
                   content = content +"</br>No. WO : "+data.currNoWO;
                   content = content +"</br>"+data.currTime;

                   content = content +"</br></br>Next Job : "+data.nextJob;
                   content = content +"</br>No. WO : "+data.nextNoWO;
                   content = content +"</br>"+data.nextTime;




   				 $("#teknisi"+idTeknisi).tooltip({
                    html : true,
                    title : content,
                    position: 'center right'
                });

   				}

   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){

   			}
   		});

    }


	reloadJPBTable = function() {
		JPBTable.fnDraw();
	}

	
	$('#search_t351TglJamApp').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t351TglJamApp_day').val(newDate.getDate());
			$('#search_t351TglJamApp_month').val(newDate.getMonth()+1);
			$('#search_t351TglJamApp_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			JPBTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	JPBTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	JPBTable = $('#JPB_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

               $(nRow).children().each(function(index, td) {
                    if(index >= 3 && index <= 22) {
                        if ($(td).html() === "BREAK") {
                            $(td).css("background-color", "grey");

                        }
                        if ($(td).html() === "V1") {
                            $(td).css("background-color", "#078DC6");
                        }
                        if ($(td).html() === "V") {
                            $(td).css("background-color", "#078DC6");
                        }
                        if ($(td).html() === "W1") {
                             $(td).css("background-color", "#FFDE00");
                        }
                        if ($(td).html() === "W") {
                            $(td).css("background-color", "#FFDE00");
                        }
                        if ($(td).html() === "X1") {
                            $(td).css("background-color", "#06B33A");
                        }
                        if ($(td).html() === "X") {
                            $(td).css("background-color", "#06B33A");
                        }
                        if ($(td).html() === "Y1") {
                            $(td).css("background-color", "#FF3229");
                        }
                        if ($(td).html() === "Y") {
                            $(td).css("background-color", "#FF3229");
                        }
                        if ($(td).html() === "Z1") {
                             $(td).css("background-color", "#ffe2e2");
                        }
                        if ($(td).html() === "Z") {
                            $(td).css("background-color", "#ffe2e2");
                        }
                        $(td).html("");
                    }
                });

			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "driver",
	"mDataProp": "driver",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="foreman'+row['foremanId']+'" onmouseover="getForemanInfo('+row['foremanId']+')" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
	"sWidth":"300px",
	"bVisible": true
}

,
{
	"sName": "jam7",
	"mDataProp": "jam7",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam7'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',7,\''+row['jam7']+'\');" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam730",
	"mDataProp": "jam730",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam73'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',73,\''+row['jam730']+'\');" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam8",
	"mDataProp": "jam8",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam8'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',8,\''+row['jam8']+'\');" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam830",
	"mDataProp": "jam830",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam83'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',83,\''+row['jam830']+'\');" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam9",
	"mDataProp": "jam9",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam9'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',9,\''+row['jam9']+'\');" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam930",
	"mDataProp": "jam930",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam93'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',93,\''+row['jam930']+'\');" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam10",
	"mDataProp": "jam10",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam10'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',10,\''+row['jam10']+'\');" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam1030",
	"mDataProp": "jam1030",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam103'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',103,\''+row['jam1030']+'\')" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
    "bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam11",
	"mDataProp": "jam11",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam11'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',11,\''+row['jam11']+'\')" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
    "bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam1130",
	"mDataProp": "jam1130",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam113'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',113,\''+row['jam1130']+'\')" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
    "bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam12",
	"mDataProp": "jam12",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam12'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',7,\''+row['jam12']+'\')" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
    "bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam1230",
	"mDataProp": "jam1230",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam123'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',123,\''+row['jam1230']+'\');" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam13",
	"mDataProp": "jam13",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam13'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',13,\''+row['jam13']+'\');" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam1330",
	"mDataProp": "jam1330",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam133'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',133,\''+row['jam1330']+'\');" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam14",
	"mDataProp": "jam14",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam14'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',14,\''+row['jam14']+'\');" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam1430",
	"mDataProp": "jam1430",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam143'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',143,\''+row['jam1430']+'\');" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam15",
	"mDataProp": "jam15",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam15'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',15,\''+row['jam15']+'\');" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam1530",
	"mDataProp": "jam1530",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam153'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',153,\''+row['jam1530']+'\');" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jam16",
	"mDataProp": "jam16",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam16'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',16,\''+row['jam16']+'\');" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "jam1630",
	"mDataProp": "jam1630",
//	"mRender": function ( data, type, row ) {
//	    return '<a href="#" id="jam163'+row['jpbId']+'" onmouseover="getOnProgressInfo('+row['jpbId']+',163,\''+row['jam1630']+'\');" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
//	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "jam17",
	"mDataProp": "jam17",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "jam1730",
	"mDataProp": "jam1730",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "jam18",
	"mDataProp": "jam18",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "jam1830",
	"mDataProp": "jam1830",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "jam19",
	"mDataProp": "jam19",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "jam1930",
	"mDataProp": "jam1930",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "jam20",
	"mDataProp": "jam20",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "jam2030",
	"mDataProp": "jam2030",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "jam21",
	"mDataProp": "jam21",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "jam2130",
	"mDataProp": "jam2130",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "jam22",
	"mDataProp": "jam22",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "jam2230",
	"mDataProp": "jam2230",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "jam23",
	"mDataProp": "jam23",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "jam2330",
	"mDataProp": "jam2330",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
            aoData.push(
                    {"name": 'aksi', "value": "input"}
            );

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

    new FixedColumns( JPBTable, {
		"iLeftWidth": 150,
		//"iLeftColumns": 2,
		"fnDrawCallback": function ( left, right ) {
			var that = this, groupVal = null, matches = 0, heights = [], index = -1;

			// Get the heights of the cells and remove redundant ones
			$('tbody tr td', left.body).each( function ( i ) {
				var currVal = this.innerHTML;

				// Reset values on new cell data.
				if (currVal != groupVal) {
					groupVal = currVal;
					index++;
					heights[index] = 0;
					matches = 0;
				} else  {
					matches++;
				}

				heights[ index ] += $(this.parentNode).height();
				if ( currVal == groupVal && matches > 0 ) {
					this.parentNode.parentNode.removeChild(this.parentNode);
				}
			} );

			// Now set the height of the cells which remain, from the summed heights
			$('tbody tr td', left.body).each( function ( i ) {
				that.fnSetRowHeight( this.parentNode, heights[ i ] );
			} );
		}
	} );

});


</g:javascript>


			
