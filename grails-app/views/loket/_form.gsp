<%@ page import="com.kombos.administrasi.Loket" %>
<g:javascript>

    var isNumberKey;
    $(function(){
    isNumberKey = function(evt)
        {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

    })
    $(document).ready(function() {
        if($('#m404Ket').val().length>0){
            $('#hitung').text(50 - $('#m404Ket').val().length);
        }
        $('#m404Ket').keyup(function() {
            var len = this.value.length;
            if (len >= 50) {
                this.value = this.value.substring(0, 50);
                len = 50;
            }
            $('#hitung').text(50 - len);
        });
    });

</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: loketInstance, field: 'm404NoLoket', 'error')} required">
	<label class="control-label" for="m404NoLoket">
		<g:message code="loket.m404NoLoket.label" default="Nomor Loket" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m404NoLoket" maxlength="8" onkeypress="return isNumberKey(event);" value="${loketInstance.m404NoLoket}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: loketInstance, field: 'm404NamaLoket', 'error')} required">
	<label class="control-label" for="m404NamaLoket">
		<g:message code="loket.m404NamaLoket.label" default="Nama Loket" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m404NamaLoket" maxlength="50" required="" value="${loketInstance?.m404NamaLoket}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: loketInstance, field: 'm404Ket', 'error')} required">
	<label class="control-label" for="m404Ket">
		<g:message code="loket.m404Ket.label" default="Keterangan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:textArea name="m404Ket" id="m404Ket" required="" value="${loketInstance?.m404Ket}"/>
        <br/>
        <span id="hitung">50</span> Karakter Tersisa.
	</div>
</div>