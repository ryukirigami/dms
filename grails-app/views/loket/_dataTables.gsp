
<%@ page import="com.kombos.administrasi.Loket" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="loket_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover  table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="loket.m404NoLoket.label" default="Nomor Loket" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="loket.m404NamaLoket.label" default="Nama Loket" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="loket.m404Ket.label" default="Keterangan" /></div>
			</th>



		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m404NoLoket" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m404NoLoket" class="search_init" onkeypress="return isNumberKey(event);"/>
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m404NamaLoket" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m404NamaLoket" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m404Ket" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m404Ket" class="search_init" />
				</div>
			</th>
	


		</tr>
	</thead>
</table>

<g:javascript>
var loketTable;
var reloadLoketTable;
$(function(){
	
	reloadLoketTable = function() {
		loketTable.fnDraw();
	}

	var recordsloketperpage = [];//new Array();
    var anloketSelected;
    var jmlRecloketPerPage=0;
    var id;


	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	loketTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	loketTable = $('#loket_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsloket = $("#loket_datatables tbody .row-select");
            var jmlloketCek = 0;
            var nRow;
            var idRec;
            rsloket.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsloketperpage[idRec]=="1"){
                    jmlloketCek = jmlloketCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsloketperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecloketPerPage = rsloket.length;
            if(jmlloketCek==jmlRecloketPerPage && jmlRecloketPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
        "fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m404NoLoket",
	"mDataProp": "m404NoLoket",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m404NamaLoket",
	"mDataProp": "m404NamaLoket",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m404Ket",
	"mDataProp": "m404Ket",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m404NoLoket = $('#filter_m404NoLoket input').val();
						if(m404NoLoket){
							aoData.push(
									{"name": 'sCriteria_m404NoLoket', "value": m404NoLoket}
							);
						}
	
						var m404NamaLoket = $('#filter_m404NamaLoket input').val();
						if(m404NamaLoket){
							aoData.push(
									{"name": 'sCriteria_m404NamaLoket', "value": m404NamaLoket}
							);
						}
	
						var m404Ket = $('#filter_m404Ket input').val();
						if(m404Ket){
							aoData.push(
									{"name": 'sCriteria_m404Ket', "value": m404Ket}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

	 $('.select-all').click(function(e) {

        $("#loket_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordsloketperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsloketperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#loket_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsloketperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anloketSelected = loketTable.$('tr.row_selected');
            if(jmlRecloketPerPage == anloketSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsloketperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
