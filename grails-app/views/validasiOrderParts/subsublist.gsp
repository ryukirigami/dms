<%@ page import="com.kombos.parts.RequestStatus" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
		<g:javascript>
var vopSubSubTable_${idTable};
var saveHargaSatuan_${idTable};
$(function(){
	saveHargaSatuan_${idTable} = function ( oTable, nRow ){
		var aData = oTable.fnGetData(nRow);
		var hargaSatuanInput = $('#hargaSatuan' + aData['id'], nRow);
		var data = {requestId: aData['id'],
								hargaSatuan: hargaSatuanInput[0].value};
		var status = $('#filter_status input[name=search_status]:checked').val();
						if(status){
							data["sCriteria_status"] = status;
						}
		$.ajax({ "type": "POST",
							"url": "${g.createLink(action: "saveHargaBeli")}",
							"data": data ,
							"success": function (json) {
								if(json.status == 'ok'){
									var parentRow = $("#vop-sub-row-${parentId}").parents('tr')[0];
									var parentTableId = $(parentRow).parents('table').attr('id');
									var parentTable = $('#'+parentTableId).dataTable();
									var total = json.sumTotalPerReferensi.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
									parentTable.fnUpdate(total, parentRow, 4, false );
									var topRow = $("#vop-row-${idTanggal}").parents('tr')[0];
									var topTableId = $(topRow).parents('table').attr('id');
									var topTable = $('#'+topTableId).dataTable();
									var totalS = json.sumTotalPerTanggal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
									topTable.fnUpdate(totalS, topRow, 3, false );
								}
							},
							"complete": function () {
							}
		});
		
	}
	vopSubSubTable_${idTable} = $('#vop_datatables_sub_sub_${idTable}').dataTable({
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "t",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			var aData = vopSubSubTable_${idTable}.fnGetData(nRow);
		        var checkbox = $('.row-select', nRow);
		        if(checked.indexOf(""+aData['id'])>=0){
		              checkbox.attr("checked",true);
		              checkbox.parent().parent().addClass('row_selected');
		        }

		        checkbox.click(function (e) {
            	    var tc = $(this);

                    if(this.checked)
            			tc.parent().parent().addClass('row_selected');
            		else {
            			tc.parent().parent().removeClass('row_selected');
            			var selectAll = $('.select-all');
            			selectAll.removeAttr('checked');
                    }
            	 	e.stopPropagation();
                });
			return nRow;
		},
		"bSort": true,
		"bProcessing": false,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : 1000, //maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubSubList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": ''
},
{
	"sName": "kodePart",
	"mDataProp": "kodePart",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		if(row['status'] === '${RequestStatus.BELUM_VALIDASI.toString()}'){
			return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"> <input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;' + data +
			'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['idPart']+');" title="Edit Klasifikasi Goods">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';

		} else {
			return '&nbsp;&nbsp;' + data;
		}
	},
	"bSortable": false,
	"sWidth":"220px",
	"bVisible": true
},
{
	"sName": "namaPart",
	"mDataProp": "namaPart",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"190px",
	"bVisible": true
},
{
	"sName": "qty",
	"mDataProp": "qty",
	"aTargets": [2],
	"mRender": function ( data, type, row ) {
				            			if(data != null)
				            				return '<span class="pull-right numeric">'+data+'</span>';
				            			else 
				            				return '<span></span>';
								},
	"bSortable": false,
	"sWidth":"40px",
	"bVisible": true
},
{
	"sName": "satuan",
	"mDataProp": "satuan",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"50px",
	"bVisible": true
},
{
	"sName": "vendor",
	"mDataProp": "vendor",
	"sClass": "vendor",
	"aTargets": [4],
	"mRender": function ( data, type, row ) {
		if(row['status'] === '${RequestStatus.BELUM_VALIDASI.toString()}'){
			var vendors = [];
			<g:each in="${com.kombos.parts.Vendor.createCriteria().list { eq("staDel","0"); order("m121Nama","asc")}}">
				vendors.push({id: ${it.id}, label: '${it.m121Nama}', spld: '${it.m121staSPLD}'});
			</g:each>
			
			var vendorCombo = '<select id="vendor'+row['id']+'" name="vendor.id" required=""  class="many-to-one inline-edit vendorId" style="width:180px;" >';
			vendorCombo += '<option value="null">[Pilih vendor...]</option>';
			for(var i = 0; i < vendors.length; i++){

				if(vendors[i].spld == '1'){
					vendorCombo += '<option value="'+vendors[i].id+'">'+vendors[i].label+'</option>';
				} else {
					vendorCombo += '<option class="spld" value="'+vendors[i].id+'">'+vendors[i].label+'</option>';
				}
			}
			vendorCombo += '</select>';
			return vendorCombo;
		} else {
			return data;
		}
	},
	"bSortable": false,
	"sWidth":"180px",
	"bVisible": true
},
{
	"sName": "hargaSatuan",
	"mDataProp": "hargaSatuan",
	"aTargets": [5],
	"mRender": function ( data, type, row ) {
		if(row['status'] === '${RequestStatus.BELUM_VALIDASI.toString()}'){
			return '<input type="text" style="width:100px;" value="'+data+'" class="pull-right inline-edit numeric" id="hargaSatuan'+row['id']+'">';
		} else {
			return '<span class="pull-right numeric">'+data+'</span>';							
		}
	},
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "totalHarga",
	"mDataProp": "totalHarga",
	"aTargets": [6],
	"mRender": function ( data, type, row ) {
				            			if(data != null)
				            				return '<span class="pull-right numeric">'+data+'</span>';
				            			else 
				            				return '<span></span>';
								},
	"bSortable": false,
	"sWidth":"120px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        aoData.push(
									{"name": 'noReferensi', "value": "${noReferensi}"}
						);
						var status = $('#filter_status input[name=search_status]:checked').val();
						if(status){
							aoData.push(
									{"name": 'sCriteria_status', "value": status}
							);
						}
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
								$('#vop_datatables_sub_sub_${idTable}').find('input.numeric').autoNumeric('init',{
									vMin:'0.00',
									vMax:'99999999999999.00',
									aSep:','
								}).keypress(function() {
									var nRow = $(this).parents('tr')[0];
									var aData = vopSubSubTable_${idTable}.fnGetData(nRow);
									var dataString = $(this).val().replace(/,/g,"")
									var total = aData['qty']*dataString;
									total = total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ".00"
									vopSubSubTable_${idTable}.fnUpdate( total, nRow, 9, false );
								}).change(function() {
									var nRow = $(this).parents('tr')[0];
									var aData = vopSubSubTable_${idTable}.fnGetData(nRow);
							    	var dataString = $(this).val().replace(/,/g,"")
									var total = aData['qty']*dataString;
									total = total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ".00"
									vopSubSubTable_${idTable}.fnUpdate( total, nRow, 9, false );
									saveHargaSatuan_${idTable}(vopSubSubTable_${idTable}, nRow);
								}).keypress(checkOnValue)
								.change(checkOnValue);
								
								$('#vop_datatables_sub_sub_${idTable}').find('select')
								.keypress(checkOnValue)
								.change(checkOnValue)
								.click(checkOnValue);

							   },
							"complete": function () {
							   }
						});
		}
	});

});
		</g:javascript>
	</head>
	<body>
	<div class="innerinnerDetails">
<table id="vop_datatables_sub_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
	class="display table table-striped table-bordered table-hover">
    <thead>
        <tr>
           <th></th>
           <th></th>
           <th></th>
			<th style="border-bottom: none; padding: 5px;">
                <div>Kode Part</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Nama Part</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Qty</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Satuan</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Vendor</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Harga Satuan (Rp)</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Total Harga (Rp)</div>
			</th>
		</tr>
	</thead>
	<tbody></tbody>
	</table>
	</div>
</body>
</html>
