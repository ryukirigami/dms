
<%@ page import="com.kombos.parts.Group; com.kombos.parts.Location; com.kombos.parts.SCC; com.kombos.parts.Franc; com.kombos.parts.SOQ" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="title" value="Validasi Order Parts" />
		<g:set var="permohonanId" value="ApprovalOrderParts" />
		<title>${title}</title>
		<r:require modules="baseapplayout" />
		<g:javascript>
	var sendToApproval;
	var sendDirectPO;
	var checkOnValue;	
	var chekNeedApproval;
	var directPO = true;
	var validateEmptyVendor = false;
	$(function(){
	ganti = function(isi){
        $("#vop_datatables_${idTable} tbody .row-select").each(function() {
                var id = $(this).next("input:hidden").val();
                var nRow = $(this).parents('tr')[0];
                $('#vendor' + id, nRow).prop('selectedIndex', isi.selectedIndex);
	    });
	}
	sendDirectPO = function(){		
		var formPermohonanApproval = $('#formPermohonanApproval${permohonanId}Modal').find('form');
		var checkParts = [];
		var nonEditableParts = [];
        $("#vop_datatables_${idTable} tbody .row-select").each(function() {
        if(this.checked){
            var id = $(this).next("input:hidden").val();			
			
			var nRow = $(this).parents('tr')[0];
			var vendorSelect = $('#vendor' + id, nRow);
			var hargaSatuanInput = $('#hargaSatuan' + id, nRow);
            
			if(hargaSatuanInput[0].value && vendorSelect[0].value){
				checkParts.push(id);
				formPermohonanApproval.append('<input type="hidden" name="vendor-'+id + '" value="'+vendorSelect[0].value+'" class="deleteafter">');
				formPermohonanApproval.append('<input type="hidden" name="hargaSatuan-'+id + '" value="'+hargaSatuanInput[0].value+'" class="deleteafter">');
			} else {
				nonEditableParts.push(id);
			}
        }
        });
		if(nonEditableParts.length > 0){
			alert('Part yang sudah request booking fee tidak disertakan lagi.');
		}
        if(checkParts.length < 1){
             alert('Anda belum memilih data yang akan ditambahkan');
        } else {
			$("#requestIds").val(JSON.stringify(checkParts));
		}	
		
		$.ajax({type:'POST',
				url:'${request.contextPath}/validasiOrderParts/requestDirectPO',
				data: formPermohonanApproval.serialize(),
				success:function(data,textStatus){
					toastr.success('<div>' + data.nonapproval + ' PO for SPLD vendor has been created.</div>');
					vopTable.fnDraw();
				},
				error:function(XMLHttpRequest,textStatus,errorThrown){},
				complete:function(XMLHttpRequest,textStatus){
					$('#spinner').fadeOut();
				}
			});
	}
	chekNeedApproval = function(){
	    var checkParts = [];
	    $("#vop_datatables_${idTable} tbody .row-select").each(function() {
            if(this.checked){
                checkParts.push(this);
            }
        });
	    if(checkParts.length > 0){
	        for(var i = 0; i < checkParts.length; i++){
                $this = $(checkParts[i]);
                $this.parent().siblings(".vendor").find('option:selected').each(function() {

                    validateEmptyVendor = validateEmptyVendor | ($(this).val() === 'null')
                    directPO = directPO & $(this).hasClass('spld');
                });
	        }

        if(validateEmptyVendor){
                 alert('Ada vendor yang belum dipilih');
            } else {
                if(directPO){
                    sendDirectPO();
                } else {
                    sendToApproval();
                }
            }
            validateEmptyVendor = false;
            directPO = true;
		} else {
		    alert('Tidak ada data yang dipilih');
		}
	}
	checkOnValue = function() {
		var $this   = $(this);
		var value = $this.val();
		$("select[name='vendor.id']").val(value);
		var nRow = $this.parents('tr')[0];
		var checkInput = $('input[type="checkbox"]', nRow);
		if(value){							 
			checkInput.attr("checked","checked");
		} else {
			checkInput.removeAttr("checked");
		}
	}	

	deleteParts = function() {
		var checkParts = [];
        $("#vop_datatables_${idTable} tbody .row-select").each(function() {
            if(this.checked){
                var id = $(this).next("input:hidden").val();
                checkParts.push(id);
            }
        });

        if(checkParts.length < 1){
             alert('Anda belum memilih data yang akan dihapus');
        } else {
            console.log(checkParts);
            var conf = confirm("Apakah Anda yakin?")
            if(conf){
                $.ajax({type:'POST',
                    url:'${request.contextPath}/validasiOrderParts/deleteParts',
                    data : {dataGoods: JSON.stringify(checkParts)},
                    success : function(data){
                        toastr.success("Sukses dihapus");
                        vopTable.fnDraw();
                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){},
                    complete:function(XMLHttpRequest,textStatus){
                        $('#spinner').fadeOut();
                    }
                });
            }
         }
     }
	sendToApproval = function() {
		var formPermohonanApproval = $('#formPermohonanApproval${permohonanId}Modal').find('form');
		var checkParts = [];
		var nonEditableParts = [];
        $("#vop_datatables_${idTable} tbody .row-select").each(function() {
        if(this.checked){
            var id = $(this).next("input:hidden").val();			
			
			var nRow = $(this).parents('tr')[0];
			var vendorSelect = $('#vendor' + id, nRow);
			var hargaSatuanInput = $('#hargaSatuan' + id, nRow);
            
			if(hargaSatuanInput[0].value && vendorSelect[0].value){
				checkParts.push(id);
				formPermohonanApproval.append('<input type="hidden" name="vendor-'+id + '" value="'+vendorSelect[0].value+'" class="deleteafter">');
				formPermohonanApproval.append('<input type="hidden" name="hargaSatuan-'+id + '" value="'+hargaSatuanInput[0].value+'" class="deleteafter">');
			} else {
				nonEditableParts.push(id);
			}
        }
        });
		if(nonEditableParts.length > 0){
			alert('Part yang sudah request booking fee tidak disertakan lagi.');
		}
        if(checkParts.length < 1){
             alert('Anda belum memilih data yang akan ditambahkan');
        } else {
			$("#requestIds").val(JSON.stringify(checkParts));
			
			$.ajax({type:'POST',
				url:'${request.contextPath}/validasiOrderParts/generateNomorDokumenApprovalOrderParts',
				success:function(data,textStatus){
					$("#t770NoDokumen").val(data.value);
					$("#t770TglJamSend").val(data.tanggal);
				},
				error:function(XMLHttpRequest,textStatus,errorThrown){},
				complete:function(XMLHttpRequest,textStatus){
					$('#spinner').fadeOut();
				}
			});
			
			$("#formPermohonanApproval${permohonanId}Modal").modal({
							"backdrop" : "static",
							"keyboard" : true,
							"show" : true
						}).css({'width': '500px','margin-left': function () {return -($(this).width() / 2);}});
		}			
    };
        $("#btnSave").click(function() {
            var idGoods = $('#idGoods').val()
            var group = $('#group').val()
            var scc = $('#scc').val()
            var franc = $('#franc').val()
            var location = $('#location').val()
            $.ajax({
                url:'${request.contextPath}/validasiOrderParts/saveKlasifikasiGoods',
                type: "POST", // Always use POST when deleting data
                data : {idGoods:idGoods, group:group, scc:scc, franc:franc,location:location},
                success : function(data){
                    toastr.success("SukseS");
                    vopTable.fnDraw();
                },
                error: function(xhr, textStatus, errorThrown) {
                    alert('Internal Server Error');
                }
            });
            oFormService.fnHideVehicleDataTable();
        });

        edit = function(id) {
            $.ajax({type:'POST', url:'${request.contextPath}/validasiOrderParts/findData',
		    data : {id : id},
   			success:function(data,textStatus){
                    $('#kodeGoods').val(data.kodePart);
                    $('#idGoods').val(data.idGoods);
                    $('#namaGoods').val(data.namaPart);
                    $('#group').val(data.group);
                    $('#scc').val(data.scc);
                    $('#franc').val(data.franc);
                    $('#location').val(data.location);
					oFormService.fnShowVehicleDataTable();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){

   			}
   		});

        }
        oFormService = {
            fnShowVehicleDataTable: function() {
                $("#klasifikasiGoodsModal").modal("show");
                $("#klasifikasiGoodsModal").fadeIn();
            },
            fnHideVehicleDataTable: function() {
                $("#klasifikasiGoodsModal").modal("hide");
            }
        }
   	});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left">${title}</span>
		<ul class="nav pull-right">
			
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="vop-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
           	<g:render template="dataTables" />
           	<fieldset class="buttons controls" style="padding-top: 30px;">
					<g:field type="button" onclick="chekNeedApproval();" class="btn btn-primary" name="send-to-approval" id="send-to-approval" value="Send To Approval" />
					<g:field type="button" onclick="deleteParts();" class="btn btn-cancel" name="deleteData" id="deleteData" value="Delete Parts" />
			</fieldset>
            
		</div>
		<div class="span7" id="vop-form" style="display: none;"></div>
	</div>
	<g:render template="formPermohonanApproval" />
    <div id="klasifikasiGoodsModal" class="modal hide">
        <div class="modal-dialog" style="width: 500px;">
            <div class="modal-content" style="width: 500px;">
                <div class="modal-header">
                    <a class="close" href="javascript:void(0);" data-dismiss="modal">×</a>
                    <h6>Edit Klasifikasi Goods</h6>
                </div>
                <!-- dialog body -->
                <div class="modal-body" id="klasifikasiGoodsModal-body" style="max-height: 500px;">
                    <div class="box">
                        <div class="span12">
                            <fieldset>
                                <table>
                                    <tr style="display:table-row;">
                                        <td style="display:table-cell; padding:5px; width: 100px;">
                                            <label class="control-label" for="sCriteria_nama">Kode Goods *</label>
                                        </td>
                                        <td style="display:table-cell; padding:5px;">
                                            <input type="text" id="kodeGoods" style="width: 230px" readonly>
                                            <input type="hidden" id="idGoods" style="width: 230px" readonly>
                                        </td>
                                    </tr>
                                    <tr style="display:table-row;">
                                        <td style="display:table-cell; padding:5px;">
                                            <label class="control-label" for="sCriteria_nama">Nama Goods *</label>
                                        </td>
                                        <td style="display:table-cell; padding:5px;">
                                            <input type="text" id="namaGoods" style="width: 230px" readonly>
                                        </td>
                                    </tr>
                                    <tr style="display:table-row;">
                                        <td style="display:table-cell; padding:5px;">
                                            <label class="control-label" for="sCriteria_nama">Group *</label>
                                        </td>
                                        <td style="display:table-cell; padding:5px;">
                                            <g:select id="group" name="group.id" from="${Group.createCriteria().list(){eq("staDel", "0");order("m181NamaGroup", "asc")}}" optionKey="id" class="many-to-one"/>
                                        </td>
                                    </tr>
                                    <tr style="display:table-row;">
                                        <td style="display:table-cell; padding:5px;">
                                            <label class="control-label" for="sCriteria_nama">Franc *</label>
                                        </td>
                                        <td style="display:table-cell; padding:5px;">
                                            <g:select id="franc" name="franc.id" from="${Franc.createCriteria().list(){order("m117NamaFranc", "asc")}}" optionKey="id" class="many-to-one"/>
                                        </td>
                                    </tr>
                                    <tr style="display:table-row;">
                                        <td style="display:table-cell; padding:5px;">
                                            <label class="control-label" for="sCriteria_nama">Scc *</label>
                                        </td>
                                        <td style="display:table-cell; padding:5px;">
                                            <g:select id="scc" name="scc.id" from="${SCC.createCriteria().list(){eq("staDel", "0");order("m113NamaSCC", "asc")}}" optionKey="id" class="many-to-one"/>
                                        </td>
                                    </tr>
                                    <tr style="display:table-row;">
                                        <td style="display:table-cell; padding:5px;">
                                            <label class="control-label" for="sCriteria_nama">Location *</label>
                                        </td>
                                        <td style="display:table-cell; padding:5px;">
                                            <g:select id="location" name="location.id" from="${Location.createCriteria().list(){eq("staDel", "0");order("m120NamaLocation", "asc")}}" optionKey="id" class="many-to-one"/>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <a href="javascript:void(0);" class="btn cancel" onclick="oFormService.fnHideVehicleDataTable();">
                        Tutup
                    </a>
                    <a href="javascript:void(0);" id="btnSave" class="btn btn-primary">
                        Save
                    </a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
