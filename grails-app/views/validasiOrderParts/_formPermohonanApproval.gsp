<%@ page import="com.kombos.administrasi.KegiatanApproval" %>
<g:javascript>
var approvalOrderPartsSent;
$(function(){ 
	$("#formPermohonanApproval${permohonanId}Modal").on("show", function() {
			//$("#formPermohonanApproval${permohonanId}Modal").on("click", function(e) {
			//	$("#formPermohonanApproval${permohonanId}Modal").modal('hide');
			//});
		});
	$("#formPermohonanApproval${permohonanId}Modal").on("hide", function() {
			            $("#formPermohonanApproval${permohonanId}Modal a.btn").off("click");
		});
	
	approvalOrderPartsSent = function(data){
		$('#formPermohonanApproval${permohonanId}Modal').modal('hide')
		toastr.success('<div>' + data.needapproval + ' Request Order Parts Approval has been sent.</div><div>' + data.nonapproval + ' PO for SPLD vendor has been created.</div>');
		vopTable.fnDraw();
	}
});
</g:javascript>
<div id="formPermohonanApproval${permohonanId}Modal" class="modal hide">
        <div class="modal-dialog">
            <div class="modal-content">
            	<g:formRemote class="form-horizontal" name="create" on404="alert('not found!');" onSuccess="approvalOrderPartsSent(data);" 
             				 url="[controller: 'validasiOrderParts', action:'requestApprovalOrderPartsApproval']">
				<input type="hidden" name="requestIds" id="requestIds" value="">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 500px;">
                    <div id="formPermohonanApproval${permohonanId}Content">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
	                	<div class="iu-content">	                	
							<div class="control-group fieldcontain">
								<label class="control-label" for="kegiatanApproval">Nama Kegiatan</label>
								<div class="controls">
									<g:select name="kegiatanApproval" id="kegiatanApproval" from="${KegiatanApproval.list()}" optionKey="id"  value="${kegiatanApproval.id}" optionValue="m770KegiatanApproval" class="one-to-many" readonly="readonly"/>
								</div>
							</div>
							
							<div class="control-group fieldcontain">
								<label class="control-label" for="t770NoDokumen">
									Nomor Request
								</label>
								<div class="controls">
									<g:field type="text" name="t770NoDokumen" value="" readonly="readonly"/>
								</div>
							</div>
							<div class="control-group fieldcontain">
								<label class="control-label" for="t770TglJamSend">
									Tanggal dan Jam
								</label>
								<div class="controls">
									<g:field type="text" name="t770TglJamSend" id="t770TglJamSend" readonly="readonly"/>
								</div>
							</div>
							<div class="control-group fieldcontain">
								<label class="control-label" for="approver">
									Approver(s)
								</label>
								<div class="controls">
									<g:textArea rows="3" cols="50" maxlength="20" name="approver" value="${approver}" readonly="readonly"/>
								</div>
							</div>
							<div class="control-group fieldcontain">
								<label class="control-label" for="pesan">
									Pesan
								</label>
								<div class="controls">
									<g:textArea rows="4" cols="50" maxlength="300" name="pesan" value="" />
								</div>
							</div>	
									
						</div>	
	                </div>
	            </div>
                <!-- dialog buttons -->
                <div class="modal-footer">
                	<g:submitButton class="btn btn-primary create" name="send" value="${message(code: 'default.button.send.label', default: 'Send')}" />
                	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
            	</g:formRemote>	
            </div>
        </div>
</div>




