<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
		<g:javascript>
var vopSubTable_${idTable};
$(function(){ 
var anOpen = [];
	$('#vop_datatables_sub_${idTable} td.subcontrol').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
  		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = vopSubTable_${idTable}.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST', 
	   			data : oData,
	   			url:'${request.contextPath}/validasiOrderParts/subsublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = vopSubTable_${idTable}.fnOpen(nTr,data,'details');
    				$('div.innerinnerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
    		
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerinnerDetails', $(nTr).next()[0]).slideUp( function () {
    		    $("#vop_datatables_sub_${idTable} tbody .row-select").each(function() {
                            var id = $(this).next("input:hidden").val();
                            alert
                            if(checked.indexOf(""+id)>=0){
                                checked[checked.indexOf(""+id)]="";
                            }
                            if(this.checked){
                                checked.push(""+id);
                            }
                        });
      			vopSubTable_${idTable}.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});
    if(vopSubTable_${idTable})
    	vopSubTable_${idTable}.dataTable().fnDestroy();
vopSubTable_${idTable} = $('#vop_datatables_sub_${idTable}').dataTable({
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "t",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": false,
		"bProcessing": false,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : 1000,//maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "noReferensi",
	"mDataProp": "noReferensi",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '&nbsp;&nbsp;<span id="vop-sub-row-'+row['id']+'">'+data+'</span>';
	},
	"bSortable": false,
	"sWidth":"337px",
	"bVisible": true
},
{
	"sName": "pemohon",
	"mDataProp": "pemohon",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"145px",
	"bVisible": true
},
{
	"sName": "totalHarga",
	"mDataProp": "totalHarga",
	"aTargets": [2],
	"mRender": function ( data, type, row ) {
				            			if(data != null)
				            				return '<span class="pull-right numeric">'+data+'</span>';
				            			else 
				            				return '<span></span>';
								},
	"bSortable": false,
	"sWidth":"190px",
	"bVisible": true
},
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"150px",
	"sDefaultContent": '',
	"bVisible": true
},
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"140px",
	"sDefaultContent": '',
	"bVisible": true
},
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"140px",
	"sDefaultContent": '',
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						aoData.push(
									{"name": 'tanggalRequest', "value": "${tanggalRequest}"},
									{"name": 'idTanggal', "value": "${idTanggal}"}
						);
						var status = $('#filter_status input[name=search_status]:checked').val();
						if(status){
							aoData.push(
									{"name": 'sCriteria_status', "value": status}
							);
						}
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
		</g:javascript>
	</head>
	<body>
<div class="innerDetails">
<table id="vop_datatables_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
	class="display table table-striped table-bordered table-hover">
    <thead>
        <tr>
           <th></th>
           <th></th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Nomor Referensi</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Pemohon</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Total Harga (Rp)</div>
			</th>
			<th style="border-bottom: none; padding: 5px;"/>
			<th style="border-bottom: none; padding: 5px;"/>
			<th style="border-bottom: none; padding: 5px;"/>	
         </tr>
			    </thead>
			    <tbody></tbody>
			</table>
</div>
</body>
</html>
