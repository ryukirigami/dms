
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay" />
<fieldset>
	<table style="padding-right: 10px">
		<tr>
			<td style="width: 130px"><label class="control-label"
				for="search_tanggalFrom">Dari Tanggal&nbsp;
			</label>&nbsp;&nbsp;</td>
			<td>
				<div id="filter_tanggal_from" class="controls">
					<ba:datePicker id="search_tanggalFrom" name="search_tanggalFrom"
						precision="day" format="dd/MM/yyyy" value="" />
				</div>
			</td>
		</tr>
        <tr>
            <td style="width: 130px"><label class="control-label"
                                            for="search_tanggalFrom">Sampai Tanggal&nbsp;
            </label>&nbsp;&nbsp;</td>
            <td>
                <div id="filter_tanggal_to" class="controls">
                    <ba:datePicker id="search_tanggalTo" name="search_tanggalTo"
                                   precision="day" format="dd/MM/yyyy" value="" />
                </div>
            </td>
        </tr>
		<tr>
			<td><label class="control-label" for="search_status">Kriteria&nbsp;
			</label>&nbsp;&nbsp;</td>
			<td>
				<div id="filter_status" class="controls">
					<g:radioGroup name="search_status" values="[2,3]" value="2" labels="['Belum Validasi','Sudah Validasi Belum Approval']">
								${it.radio} <g:message code="${it.label}" />
					</g:radioGroup>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<div class="controls" style="right: 0">
					<button style="width: 70px; height: 30px; border-radius: 5px"
						class="btn btn-primary view" name="view" id="view" onClick="search();">Search</button>
                    <button style="width: 70px; height: 30px; border-radius: 5px"
						class="btn btn-cancel view" name="view" id="clear" onClick="clearAll();">Clear</button>
				</div>
			</td>
		</tr>
	</table>
</fieldset>
<table id="vop_datatables_${idTable}" cellpadding="0" cellspacing="0" border="0"
	class="display table table-striped table-bordered table-hover" style="table-layout: fixed; ">
	<thead>
		<tr>
			<th><input type="checkbox" title="Select all" aria-label="Select all" class="pull-left select-all">
            </th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Tanggal Request</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Jumlah Request</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Total Harga (Rp)</div>
			</th>
			<th style="border-bottom: none; padding: 5px;"/>
			<th style="border-bottom: none; padding: 5px;"/>
			<th style="border-bottom: none; padding: 5px;"/>	
		</tr>
		
	</thead>
</table>

<g:javascript>
var vopTable;
var search;
var checkOnValue;
var checked = [];
$(function(){

	var anOpen = [];
	$('#vop_datatables_${idTable} td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = vopTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST', 
	   			data : oData,
	   			url:'${request.contextPath}/validasiOrderParts/sublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = vopTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
    		
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
    		    $("#vop_datatables_${idTable} tbody .row-select").each(function() {
                            var id = $(this).next("input:hidden").val();
                            alert
                            if(checked.indexOf(""+id)>=0){
                                checked[checked.indexOf(""+id)]="";
                            }
                            if(this.checked){
                                checked.push(""+id);
                            }
                        });
      			vopTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});
    
	search = function() {
		vopTable.fnDraw();
	}

	clearAll = function(){
	    $('#search_tanggalFrom').val('');
	    $('#search_tanggalTo').val('');
	    $("input[name=search_status][value=" + 2 + "]").attr('checked', 'checked');
	    vopTable.fnDraw();
	}

 	vopTable = $('#vop_datatables_${idTable}').dataTable({
		"sScrollX": "99%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "sClass": "control center",
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "tanggalRequest",
	"mDataProp": "tanggalRequest",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '&nbsp;&nbsp;<span id="vop-row-'+row['id']+'">'+data+'</span>';
	},
	"bSortable": false,
	"sWidth":"363px",
	"bVisible": true
},
{
	"sName": "jumlahRequest",
	"mDataProp": "jumlahRequest",
	"aTargets": [1],
	"mRender": function ( data, type, row ) {
				            			if(data != null)
				            				return '<span class="pull-right numeric">'+data+'</span>';
				            			else 
				            				return '<span></span>';
								},
	"bSortable": false,
	"sWidth":"145px",
	"bVisible": true
},
{
	"sName": "totalHarga",
	"mDataProp": "totalHarga",
	"aTargets": [2],
	"mRender": function ( data, type, row ) {
				            			if(data != null)
				            				return '<span class="pull-right numeric">'+data+'</span>';
				            			else 
				            				return '<span></span>';
								},
	"bSortable": false,
	"sWidth":"190px",
	"bVisible": true
},
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"150px",
	"sDefaultContent": '',
	"bVisible": true
},
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"140px",
	"sDefaultContent": '',
	"bVisible": true
},
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"140px",
	"sDefaultContent": '',
	"bVisible": true
}],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                var selectAll = $('.select-all');
                selectAll.removeAttr('checked');
                $("#vop_datatables_${idTable} tbody .row-select").each(function() {
                            var id = $(this).next("input:hidden").val();
                            if(checked.indexOf(""+id)>=0){
                                checked[checked.indexOf(""+id)]="";
                            }
                            if(this.checked){
                                checked.push(""+id);
                            }
                        });

						var tanggalFrom = $('#search_tanggalFrom').val();
						var tanggalFromDay = $('#search_tanggalFrom_day').val();
						var tanggalFromMonth = $('#search_tanggalFrom_month').val();
						var tanggalFromYear = $('#search_tanggalFrom_year').val();
						
						if(tanggalFrom){
							aoData.push(
									{"name": 'sCriteria_tanggalFrom', "value": "date.struct"},
									{"name": 'sCriteria_tanggalFrom_dp', "value": tanggalFrom},
									{"name": 'sCriteria_tanggalFrom_day', "value": tanggalFromDay},
									{"name": 'sCriteria_tanggalFrom_month', "value": tanggalFromMonth},
									{"name": 'sCriteria_tanggalFrom_year', "value": tanggalFromYear}
							);
						}
						
						var tanggalTo = $('#search_tanggalTo').val();
						var tanggalToDay = $('#search_tanggalTo_day').val();
						var tanggalToMonth = $('#search_tanggalTo_month').val();
						var tanggalToYear = $('#search_tanggalTo_year').val();
						
						if(tanggalTo){
							aoData.push(
									{"name": 'sCriteria_tanggalTo', "value": "date.struct"},
									{"name": 'sCriteria_tanggalTo_dp', "value": tanggalTo},
									{"name": 'sCriteria_tanggalTo_day', "value": tanggalToDay},
									{"name": 'sCriteria_tanggalTo_month', "value": tanggalToMonth},
									{"name": 'sCriteria_tanggalTo_year', "value": tanggalToYear}
							);
						}

						var status = $('#filter_status input[name=search_status]:checked').val();
						if(status){
							aoData.push(
									{"name": 'sCriteria_status', "value": status}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});



</g:javascript>



