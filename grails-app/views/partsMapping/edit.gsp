<%@ page import="com.kombos.administrasi.PartsMapping" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'partsMapping.label', default: 'Mapping Full Model Parts')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="edit-partsMapping" class="content scaffold-edit" role="main">
			<legend><g:message code="default.edit.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${partsMappingInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${partsMappingInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form method="post"  enctype="multipart/form-data">--}%


            <form id="partsMapping-update" class="form-horizontal" action="${request.getContextPath()}/partsMapping/update" method="post" onsubmit="updateForm();return false;">
                <fieldset class="form">
                    <g:render template="form"/>
                </fieldset>
                <fieldset class="buttons controls">
                    <g:actionSubmit class="btn btn-primary save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}"
                                    onclick="return cekValidasiNomor();"/>
                    <a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout();"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
                </fieldset>
            </form>
            %{--</g:form>--}%

            <g:javascript>
			var updateForm;
			$(function(){

			function progress(e){
		        if(e.lengthComputable){
		            //kalo mau pake progress bar
		            //$('progress').attr({value:e.loaded,max:e.total});
		        }
		    }

			updateForm = function() {

			var form = new FormData($('#partsMapping-update')[0]);

			$.ajax({
                url:'${request.getContextPath()}/partsMapping/update',
                type: 'POST',
                xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){
                        myXhr.upload.addEventListener('progress',progress, false);
                    }
                    return myXhr;
                },
                //add beforesend handler to validate or something
                //beforeSend: functionname,
                success: function (res) {
     				$('#partsMapping-form').empty();
					$('#partsMapping-form').append(res);
					reloadPartsMappingTable();
                },
                //add error handler for when a error occurs if you want!
                error: function (data, status, e){
					alert(e);
				},
                data: form,
                cache: false,
                contentType: false,
                processData: false
            });

				}
			});
            </g:javascript>
			%{--</g:form>--}%
		</div>
	</body>
</html>
