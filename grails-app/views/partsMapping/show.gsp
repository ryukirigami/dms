

<%@ page import="com.kombos.administrasi.PartsMapping" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'partsMapping.label', default: 'Mapping Full Model Parts')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deletePartsMapping;

$(function(){ 
	deletePartsMapping=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/partsMapping/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadPartsMappingTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-partsMapping" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="partsMapping"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${partsMappingInstance?.fullModelCode}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="fullModelCode-label" class="property-label"><g:message
					code="partsMapping.fullModelCode.label" default="Full Model Code" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="fullModelCode-label">
						%{--<ba:editableValue
								bean="${partsMappingInstance}" field="fullModelCode"
								url="${request.contextPath}/PartsMapping/updatefield" type="text"
								title="Enter fullModelCode" onsuccess="reloadPartsMappingTable();" />--}%
							
								${partsMappingInstance?.fullModelCode?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsMappingInstance?.goods}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="goods-label" class="property-label"><g:message
					code="partsMapping.goods.label" default="Parts" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="goods-label">
						%{--<ba:editableValue
								bean="${partsMappingInstance}" field="goods"
								url="${request.contextPath}/PartsMapping/updatefield" type="text"
								title="Enter goods" onsuccess="reloadPartsMappingTable();" />--}%
							
								${partsMappingInstance?.goods?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsMappingInstance?.t111Jumlah1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t111Jumlah1-label" class="property-label"><g:message
					code="partsMapping.t111Jumlah1.label" default="Jumlah" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t111Jumlah1-label">
						%{--<ba:editableValue
								bean="${partsMappingInstance}" field="t111Jumlah1"
								url="${request.contextPath}/PartsMapping/updatefield" type="text"
								title="Enter t111Jumlah1" onsuccess="reloadPartsMappingTable();" />--}%
							
								<g:fieldValue bean="${partsMappingInstance}" field="t111Jumlah1"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${partsMappingInstance?.satuan}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="satuan-label" class="property-label"><g:message
                                code="partsMapping.satuan.label" default="Satuan" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="satuan-label">
                        %{--<ba:editableValue
                                bean="${partsMappingInstance}" field="satuan"
                                url="${request.contextPath}/PartsMapping/updatefield" type="text"
                                title="Enter satuan" onsuccess="reloadPartsMappingTable();" />--}%

                        ${partsMappingInstance?.satuan?.encodeAsHTML()}

                    </span></td>

                </tr>
            </g:if>
			

				<g:if test="${partsMappingInstance?.foto}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="foto-label" class="property-label"><g:message
					code="partsMapping.foto.label" default="Foto" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="foto-label">
						%{--<ba:editableValue
								bean="${partsMappingInstance}" field="foto"
								url="${request.contextPath}/PartsMapping/updatefield" type="text"
								title="Enter foto" onsuccess="reloadPartsMappingTable();" />--}%
                            <img class="" src="${createLink(controller:'partsMapping', action:'showLogo',params : [id : partsMappingInstance.id, random : new Date().toTimestamp()])}" />

                        </span></td>
					
				</tr>
				</g:if>

				<g:if test="${partsMappingInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="partsMapping.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${partsMappingInstance}" field="lastUpdProcess"
								url="${request.contextPath}/PartsMapping/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadPartsMappingTable();" />--}%
							
								<g:fieldValue bean="${partsMappingInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>



				<g:if test="${partsMappingInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="partsMapping.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${partsMappingInstance}" field="dateCreated"
								url="${request.contextPath}/PartsMapping/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadPartsMappingTable();" />--}%
							
								<g:formatDate date="${partsMappingInstance?.dateCreated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>



            <g:if test="${partsMappingInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="partsMapping.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${partsMappingInstance}" field="createdBy"
                                url="${request.contextPath}/PartsMapping/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadPartsMappingTable();" />--}%

                        <g:fieldValue bean="${partsMappingInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${partsMappingInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="partsMapping.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${partsMappingInstance}" field="lastUpdated"
								url="${request.contextPath}/PartsMapping/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadPartsMappingTable();" />--}%
							
								<g:formatDate date="${partsMappingInstance?.lastUpdated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>



            <g:if test="${partsMappingInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="partsMapping.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${partsMappingInstance}" field="updatedBy"
                                url="${request.contextPath}/PartsMapping/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadPartsMappingTable();" />--}%

                        <g:fieldValue bean="${partsMappingInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${partsMappingInstance?.id}"
					update="[success:'partsMapping-form',failure:'partsMapping-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deletePartsMapping('${partsMappingInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
