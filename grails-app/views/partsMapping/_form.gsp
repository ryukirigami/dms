<%@ page import="com.kombos.parts.Satuan; com.kombos.parts.Goods; com.kombos.administrasi.FullModelCode; com.kombos.administrasi.PartsMapping" %>



<div class="control-group fieldcontain ${hasErrors(bean: partsMappingInstance, field: 'fullModelCode', 'error')} required">
	<label class="control-label" for="fullModelCode">
		<g:message code="partsMapping.fullModelCode.label" default="Full Model Code" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
    <g:hiddenField name="id" id="id"  value="${partsMappingInstance?.id}"/>
	<g:select id="fullModelCode" name="fullModelCode.id" from="${FullModelCode.createCriteria().list(){eq("staDel", "0");order("t110FullModelCode", "asc")}}" optionKey="id" required="" value="${partsMappingInstance?.fullModelCode?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partsMappingInstance, field: 'goods', 'error')} required">
	<label class="control-label" for="goods">
		<g:message code="partsMapping.goods.label" default="Kode Parts" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="goods" name="goods.id" from="${Goods.createCriteria().list(){eq("staDel", "0");order("m111ID", "asc")}}" optionKey="id" required="" value="${partsMappingInstance?.goods?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partsMappingInstance, field: 't111Jumlah1', 'error')} required">
	<label class="control-label" for="t111Jumlah1">
		<g:message code="partsMapping.t111Jumlah1.label" default="Jumlah" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	    <g:textField name="t111Jumlah1" style="width : 117px; text-align: right" maxlength="12" value="${fieldValue(bean: partsMappingInstance, field: 't111Jumlah1')}" onkeypress="return isNumberKey(event)" required=""/>
        <g:select id="satuan" name="satuan.id" style="width : 85px;" from="${Satuan.list()}" optionKey="id" optionValue="m118Satuan1" value="${partsMappingInstance?.satuan?.id}" class="many-to-one" />

    </div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: partsMappingInstance, field: 'foto', 'error')} ">
	<label class="control-label" for="foto">
		<g:message code="partsMapping.foto.label" default="Upload Gambar" />
		
	</label>
	<div class="controls">
	<input type="file" id="foto" name="foto" />
	</div>
</div>


