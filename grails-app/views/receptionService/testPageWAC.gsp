<%--
  Created by IntelliJ IDEA.
  User: pwidodo
  Date: 1/18/14
  Time: 1:01 AM
  To change this template use File | Settings | File Templates.
--%>

<%@ page import="com.kombos.administrasi.CompanyDealer; java.text.SimpleDateFormat; com.kombos.administrasi.BaseModel; com.kombos.administrasi.PriceList" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
        <title>Test Page WAC</title>
        <r:require modules="baseapplayout, core, baseapptheme" />
        <title>Price List</title>
    </head>
    <body>
    <div class="container-fluid" style="margin-left: -100px; margin-top: -30px;">
        <div id="main-content" class="row-fluid">
            <div class="box-header no-border">
                <span class="pull-left">Test Page WAC</span>
            </div>
            </br>
            <div class="box">
                <table class="display table table-striped table-bordered table-hover dataTable" width="100%" cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px; width: 1355px;">
					<tr>
						<td>
							<select id="kodeKotaNoPol" name="kodeKotaNoPol"/>
						</td>
						<td>
							<input type="text" id="noPolTengah" name="noPolTengah"/>
						</td>
						<td>
							<input type="text" id="noPolBelakang" name="noPolBelakang"/>
						</td>
						<td>
							<input type="button" onClick="getReception();" value="Get Reception"/>
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<div id="receptionInstance"/>
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<div id="saveWAC" style="display:none;">
								<form action="${request.contextPath}/receptionService/saveWAC" method="post" enctype="multipart/form-data">
									<table>
										<tr>
											<td>
												<input type="hidden" id="id" name="id"/>
												Catatan : <input type="text" id="catatan" name="catatan"/>	
											</td>	
										</tr>
										<tr>
											<td>
												Bensin : <input type="text" id="bensin" name="bensin"/>	
											</td>	
										</tr>
										<tr>
											<td>
												Gambar WAC : <input type="file" name="gambarWAC" id="gambarWAC" />
											</td>
										</tr>
										<tr>
											<td>
												<table>
													<tr>
														<th>
															No
														</th>
														<th>
															Perlengkapan
														</th>
														<th>
															Baik
														</th>
														<th>
															Rusak
														</th>
														<th>
															Tidak Ada
														</th>
														<th>
															Keterangan
														</th>
													</tr>
													<tr>
														<td>
															1
														</td>
														<td>
															<input type="text" id="itemName1" name="itemName1"/>
														</td>
														<td>
															<input type="text" id="countOk1" name="countOk1"/>
														</td>
														<td>
															<input type="text" id="countNotOk1" name="countNotOk1"/>
														</td>
														<td>
															<input type="text" id="statusNone1" name="statusNone1"/>
														</td>
														<td>
															<input type="text" id="note1" name="note1"/>
														</td>
													</tr>
													<tr>
														<td>
															2
														</td>
														<td>
															<input type="text" id="itemName2" name="itemName2"/>
														</td>
														<td>
															<input type="text" id="countOk2" name="countOk2"/>
														</td>
														<td>
															<input type="text" id="countNotOk2" name="countNotOk2"/>
														</td>
														<td>
															<input type="text" id="statusNone2" name="statusNone2"/>
														</td>
														<td>
															<input type="text" id="note2" name="note2"/>
															<input type="hidden" id="countRow" name="countRow" value="2"/>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td>
												<input type="submit" value="Save WAC"/>
											</td>
										</tr>
									</table>
								</form>
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<div id="printWAC"/>
						</td>
					</tr>
				</table>
            </div>
        </div>
    </div>
	<script type="text/javascript">
		$( document ).ready(function() {
			$.getJSON('${request.contextPath}/receptionService/getKodeKotaNoPol', null, function(data) {
				$("#kodeKotaNoPol option").remove(); // Remove all <option> child tags.
				$.each(data, function(index, item) { // Iterates through a collection
					$("#kodeKotaNoPol").append( // Append an object to the inside of the select box
						$("<option></option>") // Yes you can do this.
							.text(item.m116ID + " (" + item.m116NamaKota + ")")
							.val(item.m116ID)
					);
				});
			});
		});	
		
		function getReception(){
			var kodeKotaNoPol = $('#kodeKotaNoPol').find(":selected").val();
			var noPolTengah = $('#noPolTengah').val();
			var noPolBelakang = $('#noPolBelakang').val();
			var url = '${request.contextPath}/receptionService/getReceptionByNoPol?kodeKotaNoPol='+kodeKotaNoPol+'&noPolTengah='+noPolTengah+'&noPolBelakang='+noPolBelakang+' ';
			
			$.getJSON(url, null, function(data) {
				$('#receptionInstance').html("Class : " + data.class + " </br> Id : " + data.id);
				$('#printWAC').html("<a href='${request.contextPath}/receptionService/printWAC?id="+data.id+"' >Print WAC</a>");
				$('#id').val(data.id);
				$('#saveWAC').show();
			});
		}
	</script>
    </body>
</html>