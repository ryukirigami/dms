
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
<div class="box-header no-border">
    <span class="pull-left">Test Page WAC</span>
</div>
</br>
<div class="box">
    <table width="100%" cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px; width: 1355px;">
        <tr>
            <td>
                <select id="kodeKotaNoPol" name="kodeKotaNoPol"/>
            </td>
            <td>
                <input type="text" id="noPolTengah" name="noPolTengah"/>
            </td>
            <td>
                <input type="text" id="noPolBelakang" name="noPolBelakang"/>
            </td>
            <td>
                <input type="button" onClick="getReception();" value="Get Reception"/>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="receptionInstance"/>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="saveWAC">
                    <form action="${request.contextPath}/receptionService/saveWAC" method="post" enctype="multipart/form-data">
                        <table>
                            <tr>
                                <td>
                                    <input type="hidden" id="id" name="id" value="1"/>
                                    <input type="hidden" id="countRow" name="countRow" />
                                    Catatan : <input type="text" id="catatan" name="catatan"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Bensin : <input type="text" id="bensin" name="bensin"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Gambar WAC : <input type="file" name="gambarWAC" id="gambarWAC" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table id="wacTable">
                                        <tr>
                                            <th>
                                                No
                                            </th>
                                            <th>
                                                Perlengkapan
                                            </th>
                                            <th>
                                                Keadaan Item
                                            </th>
                                            <th>
                                                Keterangan
                                            </th>
                                        </tr>
                                        %{--<tr>--}%
                                        %{--<td>--}%
                                        %{--1--}%
                                        %{--</td>--}%
                                        %{--<td>--}%
                                        %{--<input type="text" id="itemName1" name="itemName1"/>--}%
                                        %{--</td>--}%
                                        %{--<td>--}%
                                        %{--<input type="text" id="countOk1" name="countOk1"/>--}%
                                        %{--</td>--}%
                                        %{--<td>--}%
                                        %{--<input type="text" id="countNotOk1" name="countNotOk1"/>--}%
                                        %{--</td>--}%
                                        %{--<td>--}%
                                        %{--<input type="text" id="statusNone1" name="statusNone1"/>--}%
                                        %{--</td>--}%
                                        %{--<td>--}%
                                        %{--<input type="text" id="note1" name="note1"/>--}%
                                        %{--<input type="hidden" id="countRow" name="countRow" value="1"/>--}%
                                        %{--</td>--}%
                                        %{--</tr>--}%
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="submit" value="Save WAC"/>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="printWAC"/>
            </td>
        </tr>
    </table>
</div>
<script type="text/javascript">
    $( document ).ready(function() {
        %{--$.getJSON('${request.contextPath}/receptionService/getKodeKotaNoPol', null, function(data) {--}%
        %{--console.log(data);--}%
        %{--$("#kodeKotaNoPol option").remove(); // Remove all <option> child tags.--}%
        %{--$.each(data, function(index, item) { // Iterates through a collection--}%
        %{--$("#kodeKotaNoPol").append( // Append an object to the inside of the select box--}%
        %{--$("<option></option>") // Yes you can do this.--}%
        %{--.text(item.m116ID + " (" + item.m116NamaKota + ")")--}%
        %{--.val(item.m116ID)--}%
        %{--);--}%
        %{--});--}%
        %{--});--}%
        $.getJSON('${request.contextPath}/receptionService/getDataAwal', null, function(data) {
            console.log(data);
            $.each(data.nopol, function(index, item) { // Iterates through a collection
                $("#kodeKotaNoPol").append( // Append an object to the inside of the select box
                        $("<option></option>") // Yes you can do this.
                                .text(item.m116ID + " (" + item.m116NamaKota + ")")
                                .val(item.m116ID)
                );
            });
            var idItem=0;
            $.each(data.items, function(index, item) { // Iterates through a collection
                idItem++;
                var newRow = "<tr><td>"+idItem+"</td><td><input type='text' id='itemName"+idItem+"' name='itemName"+idItem+"' value='"+item.nama+"'/></td><td><input type='radio' name='staItem"+idItem+"' value='0'>Baik <input type='radio' name='staItem"+idItem+"' value='1'>Rusak <input type='radio' name='staItem"+idItem+"' value='2'>Tidak Ada</td><td><input type='text' id='note"+idItem+"' name='note"+idItem+"' /></td></tr>";
                $('#wacTable > tbody:last').append(newRow);
            });
            if(idItem>0){
                $('#countRow').val(idItem);
            }
        });
    });

    function getReception(){
        var kodeKotaNoPol = $('#kodeKotaNoPol').find(":selected").val();
        var noPolTengah = $('#noPolTengah').val();
        var noPolBelakang = $('#noPolBelakang').val();
        var url = '${request.contextPath}/receptionService/getReceptionByNoPol?kodeKotaNoPol='+kodeKotaNoPol+'&noPolTengah='+noPolTengah+'&noPolBelakang='+noPolBelakang+' ';

        $.getJSON(url, null, function(data) {
            $('#receptionInstance').html("Class : " + data.class + " </br> Id : " + data.id);
            $('#printWAC').html("<a href='${request.contextPath}/receptionService/printWAC?id="+data.id+"' >Print WAC</a>");
            $('#id').val(data.id);
            $('#saveWAC').show();
        });
    }
</script>

</body>
</html>