
<%@ page import="com.kombos.administrasi.KegiatanApproval; com.kombos.administrasi.NamaManPower; com.kombos.administrasi.Operation; com.kombos.administrasi.NamaProsesBP; com.kombos.parts.Returns" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'returnsAdd.label', default: 'Kurangi Job')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
        return false;
	});


   });
    $(document).ready(function()
    {
        editKurangiJob = function(){
                var nomorWO = $('#nomorWO').val();
                var idJobRcp = $('#idJobRcp').val();
                var pesan = $('#pesan').val();
                 $.ajax({
                    url:'${request.contextPath}/jobInstructionGr/editKurangiJob',
                    type: "POST", // Always use POST when deleting data
                    data : {noWo : nomorWO,pesan:pesan,idJobRcp:idJobRcp},
                    success : function(data){
                        //window.location.href = '#/claim' ;
                        toastr.success('<div>Send To Approval Success</div>');
                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });

        }
    });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
    <ul class="nav pull-right">
        <li></li>
        <li></li>
        <li class="separator"></li>
    </ul>
</div>
<div class="box">
    <fieldset>
        <div class="row-fluid">
                <table style="width: 95%;">
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Nama Kegiatan" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="kegiatan" id="kegiatan" value="${KegiatanApproval.APPROVAL_KURANGI_JOB}" readonly="" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Nomor WO" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="nomorWO" id="nomorWO" value="${noWo}" readonly="" />
                            <g:hiddenField style="width:100%" name="idJobRcp" id="idJobRcp" value="${jobRcp.id}" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Nomor Polisi" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="nomorPolisi" value="${nopol}" readonly="" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Model Kendaraan" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="modelKendaraan" value="${model}" readonly=""/>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Nama Stall" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="modelKendaraan" value="${stall}" readonly=""/>
                        </td>
                    </tr>


                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Job" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="m053NamaOperation" value="${jobRcp?.operation.m053NamaOperation}" readonly=""/>
                        </td>
                    </tr>

                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Tanggal Permohonan" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="modelKendaraan" value="${tanggal}" readonly=""/>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Aprover (s)" />
                        </td>
                        <td>
                            <g:textArea style="width:100%;resize: none" name="Aprover" id="approver" value="${approver}" readonly=""  />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Pesan" />
                        </td>
                        <td>
                            <g:textArea style="width:100%;resize: none" name="pesan" id="pesan"  />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px" colspan="2">
                            <button id='btn1' onclick="editKurangiJob();" type="button" class="btn btn-cancel" style="width: 150px;">Send</button>
                            <button id='btn2' type="button" class="btn btn-cancel" style="width: 150px;">Close</button>
                        </td>
                    </tr>
                </table>
        </div>
    </fieldset>
    </div>
</div>
</body>
</html>

