<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript>
var jobInstructionGrSubSubTable_${idTable};
$(function(){

var anOpen = [];
	$('#jobInstructionGr_datatables_sub_sub_${idTable} td.subcontrol').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
  		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = jobInstructionGrSubSubTable_${idTable}.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST', 
	   			data : oData,
	   			url:'${request.contextPath}/jobInstructionGr/subsubsublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = jobInstructionGrSubSubTable_${idTable}.fnOpen(nTr,data,'details');
    				$('div.innerinnerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
    		
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerinnerDetails', $(nTr).next()[0]).slideUp( function () {
      			jobInstructionGrSubSubTable_${idTable}.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

    if(jobInstructionGrSubSubTable_${idTable})
    	jobInstructionGrSubSubTable_${idTable}.dataTable().fnDestroy();
jobInstructionGrSubSubTable_${idTable} = $('#jobInstructionGr_datatables_sub_sub_${idTable}').dataTable({
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "t",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": false,
		"bProcessing": false,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubSubList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": null,
	"mDataProp": "teknisi",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return ' <input type="hidden" value="'+data+'">&nbsp;&nbsp;' + data;
        },
        "bSortable": false,
        "sWidth":"300px",
        "bVisible": true
    },
    {
        "sName": null,
        "mDataProp": "start",
        "aTargets": [1],
        "bSortable": false,
        "sWidth":"300px",
        "bVisible": true
    },
    {
        "sName": null,
	    "mDataProp": "stop",
        "aTargets": [2],
        "bSortable": false,
        "sWidth":"300px",
        "bVisible": true
    }

    ],
            "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                            aoData.push(
                                       {"name": 't401NoWO', "value": "${t401NoWO}"},
                                       {"name": 'idJob', "value": "${idJob}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
    </g:javascript>
</head>
<body>
<div class="innerinnerDetails">
    <table id="jobInstructionGr_datatables_sub_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
           class="display table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th></th>
            <th></th>
            <th></th>

            <th style="border-bottom: none; padding: 5px;">
                <div>Teknisi</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Start</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Stop</div>
            </th>


        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
</body>
</html>
