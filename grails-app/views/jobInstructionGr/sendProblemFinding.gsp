<%@ page import="com.kombos.parts.Goods; com.kombos.administrasi.Operation; com.kombos.maintable.StatusKendaraan" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'returnsAdd.label', default: 'Send Problem')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;

//	$(document).ready(function(){
//    document.getElementById("durasi").value=0;
//    generate()
//    })
	function Nomor(evt){
				var charCode = (evt.which) ? evt.which : event.keyCode
				if (charCode > 31 && (charCode < 48 || charCode > 57))
				return false;

				return true;
			}
	$(function(){

        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

        $('.box-action').click(function(){
            return false;
        });

        $(function(){
			$('#parts1').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/jobInstructionGr/listKodeParts', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});
        $(function(){
			$('#parts0').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/jobInstructionGr/listNamaParts', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});

   });
    $(document).ready(function()
    {
       $('#staRSA1').click(function(){
            var isChecked = $('#staRSA1').prop('checked');
            if(isChecked==true){
                $('#sta_TL_ATL_NTL1').prop('disabled',false);
                 $('#sta_TL_ATL_NTL2').prop('disabled',true);
                  $('#sta_TL_ATL_NTL3').prop('disabled',true);
            }
        });
         $('#staRSA2').click(function(){
            var isChecked = $('#staRSA2').prop('checked');
            if(isChecked==true){
                $('#sta_TL_ATL_NTL1').prop('disabled',true);
                 $('#sta_TL_ATL_NTL2').prop('disabled',true);
                  $('#sta_TL_ATL_NTL3').prop('disabled',true);
            }
        });

        $('#perluTambahanWaktu0').click(function(){
            var isChecked = $('#perluTambahanWaktu0').prop('checked');

            if(isChecked==true){
                $('#durasi').prop('disabled',true)
                 document.getElementById("durasi").value="";
            }
        });
        $('#perluTambahanWaktu1').click(function(){
            var isChecked = $('#perluTambahanWaktu1').prop('checked');
            if(isChecked==true){
                $('#durasi').prop('disabled',false);
                 document.getElementById("durasi").value=1;

            }
        });

        $('#tambahanJob0').click(function(){
            var isChecked = $('#tambahanJob0').prop('checked');
            if(isChecked==true){
                $('#namaJob').prop('disabled',true);
            }
        });
        $('#tambahanJob1').click(function(){
            var isChecked = $('#tambahanJob1').prop('checked');
            if(isChecked==true){
                $('#namaJob').prop('disabled',false);
            }
        });
        $('#parts1').click(function(){
            $('#parts0').prop('disabled',true);
            $('#partsMaterial1').prop('checked',true);
        });
        $('#parts0').click(function(){
            $('#partsMaterial0').prop('checked',true);
            $('#parts1').prop('disabled',true);
        });
        $('#partsMaterial0').click(function(){
            var isChecked = $('#partsMaterial0').prop('checked');
            if(isChecked==true){
                $('#parts0').prop('disabled',false);
                $('#parts1').prop('disabled',true);
            }
        });
        $('#partsMaterial1').click(function(){
            var isChecked = $('#partsMaterial1').prop('checked');
            if(isChecked==true){
                $('#parts1').prop('disabled',false);
                $('#parts0').prop('disabled',true);
            }
        });

    addParts = function(){
            var valueKode = ""
            var valueNama = ""
            if($('#partsMaterial1').prop('checked')){
                valueKode=$('#parts1').val();
            }else if($('#partsMaterial0').prop('checked')){
                valueNama=$('#parts0').val();
            }
            if(valueNama=="" && valueKode==""){
                alert('Anda belum memilih data yang akan ditambahkan')
            }else{
                $.ajax({
                    url:'${request.contextPath}/jobInstructionGr/getDataGoods',
                    type: "POST",
                    data: { valueKode : valueKode,valueNama:valueNama},
                    success : function(data){
                        if(data.length>0){
                            var checkID =[];
                            $('#sendProblemFinding-table tbody .row-select').each(function() {
                                var id = $(this).next("input:hidden").val();
                                checkID.push(id);
                            });
                            var hasil = true
                            if(checkID.length>0){
                                for(var c=0 ;c < checkID.length;c++){
                                    if(checkID[c]==data[0].kode){
                                        hasil=false
                                        if(hasil==false){
                                        break;
                                        }
                                    }
                                }
                            }
                            if(hasil ==true){
                                orderMaterialTable.fnAddData({
                                    'id': data[0].id,
                                    'kode': data[0].kode,
                                    'nama': data[0].nama,
                                    'qty': 0
                                });
                            }else{
                                alert("Data Sudah Ada")
                            }
                        }
                    }
                });
            }

        }

        deleteParts = function(){
            var checkID =[];
            var a=parseInt(-1);
            $('#sendProblemFinding-table tbody .row-select').each(function() {
                a=parseInt(a)+1;
                if(this.checked){
                    checkID.push(a);
                }
            });

            if(checkID.length<1){
                alert('Anda belum memilih data yang akan dihapus');
            }else{
                for(var b = (checkID.length-1) ; b >= 0 ; b--){
                    orderMaterialTable.fnDeleteRow(parseInt(checkID[b]));
                }
            }
            checkID = [];


        }
         editSendProblemFinding = function(){
            var checkID = [], checkQty = []
            $('#sendProblemFinding-table tbody .row-select').each(function() {
                var id = $(this).next("input:hidden").val();
                var nRow = $(this).parents('tr')[0];
                var aData = orderMaterialTable.fnGetData(nRow);
                var qtyInput = $('#qty_'+aData['id']).val();

                checkID.push(id);
                checkQty.push(qtyInput);
            });

                var ids2 = JSON.stringify(checkID);
                var qtys = JSON.stringify(checkQty);
                var nomorWO = $('#nomorWO').val();
                var jenisProblem = $('input[name="jenisProblem"]:checked').val();
                var catatanTambahan = $('#catatanTambahan').val();
                var butuhRSA = $('input[name="staRSA"]:checked').val();


                var sta_TL_ATL_NTL = $('input[name="sta_TL_ATL_NTL"]:checked').val();

                var bisaDikerjakanSendiri = $('input[name="bisaDikerjakanSendiri"]:checked').val();
                var perluTambahanWaktu = $('input[name="perluTambahanWaktu"]:checked').val();
                var durasi = $('#durasi').val();
                if(durasi==null || durasi=="")
                    durasi="-"

                var butuhTambahanJob = $('input[name="butuhTambahanJob"]:checked').val();
                var namaJob = $('#namaJob').val();
                if(namaJob==null || namaJob==""){
                    namaJob='-'
                }
                var butuhParts = $('input[name="butuhParts"]:checked').val();
                var butuhPauseJob = $('input[name="butuhPauseJob"]:checked').val();
                var statusKendaraan = $('#statusKendaraan').val();
                var actId = $('#actId').val();
                var alert1 = "",alert2 = "";
//                if(sta_TL_ATL_NTL ="" || jenisProblem=="" || butuhRSA=="" || bisaDikerjakanSendiri=="" || perluTambahanWaktu=="" ||
//                   butuhTambahanJob=="")
//                {alert("Data Harap Di Lengkapi")}
                if(document.getElementById("kirimAlert1").checked){alert1 = "1"}else{alert1 = "0"}
                if(document.getElementById("kirimAlert2").checked){alert2 = "1"}else{alert2 = "0"}
                if(jenisProblem!='' && butuhRSA!='' && sta_TL_ATL_NTL!='' && bisaDikerjakanSendiri!='' && perluTambahanWaktu!='' && butuhTambahanJob!='' && butuhPauseJob!='' ){
                    $.ajax({
                        url:'${request.contextPath}/jobInstructionGr/editSendProblemFinding',
                        type: "POST", // Always use POST when deleting data
                        data : {idGoods : ids2, qtys : qtys, noWo : nomorWO, jenisProblem:jenisProblem,catatanTambahan:catatanTambahan, butuhRSA:butuhRSA,
                        sta_TL_ATL_NTL:sta_TL_ATL_NTL, bisaDikerjakanSendiri:bisaDikerjakanSendiri,perluTambahanWaktu:perluTambahanWaktu ,
                        durasi:durasi,butuhTambahanJob:butuhTambahanJob,namaJob:namaJob, butuhParts : butuhParts,butuhPauseJob:butuhPauseJob,
                        statusKendaraan:statusKendaraan,alert1:alert1,alert2:alert2,actId:actId},
                        success : function(data){
                            toastr.success('<div>Kirim Send Problem Succes</div>');
                            expandTableLayout();
                        },
                    error: function(xhr, textStatus, errorThrown) {
                    alert('Internal Server Error');
                    }
                    });
                }else{
                    alert("Data Belum Lengkap")
                }
        }
    });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
    <ul class="nav pull-right">
        <li></li>
        <li></li>
        <li class="separator"></li>
    </ul>
</div>
<div class="box">

    <div class="row-fluid">
        <div id="kiri" class="span5">
            <table style="width: 100%;">
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.t401NoWo.label" default="Nomor WO" />
                    </td>
                    <td>
                        <g:textField style="width:100%" name="nomorWO" id="nomorWO" readonly="" value="${noWo}" />

                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.noPolisi.label" default="Nomor Polisi" />
                    </td>
                    <td>
                        <g:textField style="width:100%" name="nomorPolisi" readonly="" value="${nopol}"/>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.test.label" default="Model Kendaraan" />
                    </td>
                    <td>
                        <g:textField style="width:100%" name="modelKendaraan" readonly="" value="${model}"/>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.test.label" default="Nama Stall" />
                    </td>
                    <td>
                        <g:textField style="width:100%" name="stall" readonly="" value="${stall}" />
                        <g:hiddenField id="actId" name="actId" readonly="" value="${actId}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.test.label" default="Teknisi" />
                    </td>
                    <td>
                        <g:textField style="width:100%" name="teknisi" readonly="" value="${teknisi}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.test.label" default="Job" />
                    </td>
                    <td>
                        <g:textField style="width:100%" name="job" readonly="" value="${job}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.test.label" default="Deskripsi Problem" />
                    </td>
                    <td>
                        <g:textArea style="width:100%;resize: none" name="deskripsiProblem" readonly="" value="${deskripsiProblem}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.test.label" default="Tanggal Problem" />
                    </td>
                    <td>
                        <g:textField style="width:100%" name="job" readonly="" value="${tanggalProblem}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.test.label" default="Jenis Problem" />
                    </td>
                    <td>
                        <input type="radio" name="jenisProblem" id="jenisProblem0" value="1" required="true" />Teknis &nbsp;
                        <input type="radio" name="jenisProblem" id="jenisProblem1" value="0" required="true" />Non-Teknis
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.test.label" default="Catatan Tambahan" />
                    </td>
                    <td>
                        <g:textArea style="width:100%;resize: none" name="catatanTambahan" id="catatanTambahan"  />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.test.label" default="Tanggal Update" />
                    </td>
                    <td>
                        <g:textField style="width:100%" name="namaCustomer" readonly="" value="${tanggalUpdate}" />
                    </td>
                </tr>
            </table>
        </div>

        <div id="kanan" class="span7">
            <table style="width: 100%;">
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.test.label" default="Butuh RSA?" />
                    </td>
                    <td>
                        <input type="radio" name="staRSA" value="1" required="true" id="staRSA1" /> Ya  &nbsp;
                        <input type="radio" name="staRSA" value="0" required="true" id="staRSA2"/> Tidak
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        &nbsp;
                    </td>
                    <td>
                        <input type="radio" name="sta_TL_ATL_NTL" value="1" required="true" id="sta_TL_ATL_NTL1" /> TL  &nbsp;
                        <input type="radio" name="sta_TL_ATL_NTL" value="0" required="true" id="sta_TL_ATL_NTL2" /> ATL &nbsp;
                        <input type="radio" name="sta_TL_ATL_NTL" value="2" required="true" id="sta_TL_ATL_NTL3"/> NTL
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.test.label" default="Bisa Dikerjakan Sendiri" />
                    </td>
                    <td>
                        <input type="radio" name="bisaDikerjakanSendiri" value="1" required="true"   /> Ya  &nbsp;
                        <input type="radio" name="bisaDikerjakanSendiri" value="0" required="true"   /> Tidak
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.test.label" default="Perlu Tambahan Waktu" />
                    </td>
                    <td>
                        <input type="radio" name="perluTambahanWaktu" id="perluTambahanWaktu1" value="1" required="true"   /> Ya  &nbsp;
                        <input type="radio" name="perluTambahanWaktu" id="perluTambahanWaktu0" value="0" required="true"  /> Tidak
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        &nbsp;
                    </td>
                    <td>
                        Durasi : <g:textField style="width:20%" name="durasi" id="durasi" value="" onkeypress="return Nomor(event)" /> Menit
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.test.label" default="Butuh Tambahan Job" />
                    </td>
                    <td>
                        <input type="radio" name="butuhTambahanJob" id="tambahanJob1" value="1" required="true"   /> Ya  &nbsp;
                        <input type="radio" name="butuhTambahanJob" id="tambahanJob0" value="0" required="true"   /> Tidak
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        &nbsp;
                    </td>
                    <td>
                        Nama Job : <g:textField style="width:50%" name="namaJob" id="namaJob" value="" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.test.label" default="Butuh Parts / Material" />
                    </td>
                    <td>
                        <input type="radio" name="butuhParts" id="butuhParts1" value="1" required="true"  /> Ya  &nbsp;
                        <input type="radio" name="butuhParts" id="butuhParts2" value="0" required="true"  /> Tidak
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.test.label" default="Butuh Pause Job" />
                    </td>
                    <td>
                        <input type="radio" name="butuhPauseJob" id="butuhPauseJob1" value="1" required="true"  /> Ya  &nbsp;
                        <input type="radio" name="butuhPauseJob" id="butuhPauseJob2" value="0" required="true"  /> Tidak
                    </td>
                </tr>
                </table>
            <fieldset>
                <legend>Order Parts / Material</legend>

                    <table style="width: 100%;">
                        <tr>
                            <td style="padding: 5px">
                                <input type="radio" name="partsMaterial" id="partsMaterial1" value="1"  required="true" /> Kode  &nbsp;
                                    <g:textField name="parts1" id="parts1" class="typeahead" maxlength="11"  autocomplete="off"/> &nbsp;
                                <input type="radio" name="partsMaterial" id="partsMaterial0" value="0"  required="true" /> Nama  &nbsp;
                                    <g:textField name="parts0" id="parts0" class="typeahead" maxlength="11"  autocomplete="off"/>
                            <g:field type="button" onclick="addParts()" style="padding: 3px;"
                                     class="btn success" name="addPartss" id="addPartss" value="+" />
                            <g:field type="button" onclick="deleteParts()" style="padding: 3px;"
                                     class="btn cancel" name="deletePartss" id="addPartsss" value="delete" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <div class="span12" id="sendProblemFinding-table">
                                     <g:render template="dataTablesParts" />
                                </div>
                            </td>
                        </tr>
                    </table>

            </fieldset>
            <table style="width: 100%;">
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.test.label" default="Status Kendaraan" />
                    </td>
                    <td>
                         <g:select name="statusKendaraan" id="statusKendaraan" from="${StatusKendaraan.createCriteria().list {eq("staDel","0")}}" optionKey="id" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.test.label" default="Kirim Alert Kepada" />
                    </td>
                    <td>
                        <g:checkBox name="kirimAlert1" id="kirimAlert1" value="1" checked="false" /> SA <br>
                        <g:checkBox name="kirimAlert2" id="kirimAlert2" value="2" checked="false" /> Controller
                    </td>
                </tr>
            </table>
            <a class="btn cancel" href="javascript:void(0);" onclick="editSendProblemFinding();">OK</a>
            <a class="btn cancel" href="javascript:void(0);" onclick="expandTableLayout();"><g:message code="default.button.cancel.label" default="Close" /></a>
        </div>
    </div>
</div>
</body>
</html>
