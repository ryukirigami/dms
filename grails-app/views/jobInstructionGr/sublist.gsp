<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript>
var jobInstructionGrSubTable_${idTable};
$(function(){ 
var anOpen = [];
	$('#jobInstructionGr_datatables_sub_${idTable} td.subcontrol').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
  		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = jobInstructionGrSubTable_${idTable}.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST', 
	   			data : oData,
	   			url:'${request.contextPath}/jobInstructionGr/subsublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = jobInstructionGrSubTable_${idTable}.fnOpen(nTr,data,'details');
    				$('div.innerinnerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
    		
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerinnerDetails', $(nTr).next()[0]).slideUp( function () {
      			jobInstructionGrSubTable_${idTable}.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

    if(jobInstructionGrSubTable_${idTable})
    	jobInstructionGrSubTable_${idTable}.dataTable().fnDestroy();
jobInstructionGrSubTable_${idTable} = $('#jobInstructionGr_datatables_sub_${idTable}').dataTable({
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "t",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            if(aData.staTambah == 1 && tambahJob=="Ya"){
                         $(nRow).addClass("background-color-green");
             }

            $(nRow).children().each(function(index, td) {
                if(finalIns=='Sudah Final Inspection' || staCarry=='Ya'){
                        if(index == 1){
                             $(td).click(false);
                        }
                 }
             });
			return nRow;
		 },
		"bSort": false,
		"bProcessing": false,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"11px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": null,
	"mDataProp": "namaJob",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"400px",
	"bVisible": true
}
,

{
	"sName": null,
	"mDataProp": "startJob",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}
,
{
	"sName": null,
	"mDataProp": "stopJob",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": null,
	"mDataProp": "teknisi",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"350px",
	"bVisible": true
},
{
	"sName": null,
	"mDataProp": "foreman",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": null,
	"mDataProp": "t401NoWO",
	"aTargets": [5],
    "mRender": function ( data, type, row ) {
        if(row['staKurangiJob']=="1"){
            return '<input type="button" value="Kurangi Job" class="btn btn-cancel"  disabled="true">';
        }else{
            return '<input type="button" value="Kurangi Job"  class="btn btn-cancel"  onclick="kurangiJob(\''+data+'\','+row['id']+')"> ';
        }
    },
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"400px",
	"bVisible": true
}
,
{
	"sName": null,
	"mDataProp": "t401NoWO",
	"aTargets": [6],
	"mRender": function ( data, type, row ) {
	    if(row['staKurangiJob']=="1"){
            return '<input type="button" value="Add Teknisi" class="btn btn-cancel"  disabled="true">';
        }else{
            return '<input type="button" value="Add Teknisi" onclick="addTeknisi(\''+data+'\')" class="btn btn-cancel">' ;
        }
    },
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"400px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						aoData.push(
									{"name": 't401NoWO', "value": "${t401NoWO}"}

						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
    </g:javascript>
</head>
<body>
<div class="innerDetails">
    <table id="jobInstructionGr_datatables_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
           class="display table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th></th>
            <th></th>
            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="jobInstructionGr.goods.kode.label" default="Nama Job" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="jobInstructionGr.goods.nama.label" default="Start Job" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="jobInstructionGr.goods.lokasi.label" default="Stop Job" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="jobInstructionGr.goods.nama.label" default="Teknisi" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="jobInstructionGr.goods.lokasi.label" default="Foreman" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;" colspan="2">
                <div><g:message code="jobInstructionGr.goods.lokasi.label" default="Action" /></div>
            </th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
</body>
</html>

