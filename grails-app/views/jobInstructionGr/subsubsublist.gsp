<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript>
var jobInstructionGrSubSubSubTable_${idTable};
$(function(){
jobInstructionGrSubSubSubTable_${idTable} = $('#jobInstructionGr_datatables_sub_sub_sub_${idTable}').dataTable({
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "t",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": false,
		"bProcessing": false,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubSubSubList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"15px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"15px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"15px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"15px",
               "sDefaultContent": ''
},
{
	"sName": null,
	"mDataProp": "status",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return ' <input type="hidden" value="'+data+'">&nbsp;&nbsp;' + data;
        },
        "bSortable": false,
        "sWidth":"231px",
        "bVisible": true
    },
    {
        "sName": null,
        "mDataProp": "tanggalStatus",
        "aTargets": [1],
        "bSortable": false,
        "sWidth":"133px",
        "bVisible": true
    },
    {
        "sName": null,
	    "mDataProp": "ket",
        "aTargets": [2],
        "bSortable": false,
        "sWidth":"243px",
        "bVisible": true
    },
    {
        "sName": null,
        "mDataProp": "problem",
        "aTargets": [3],
        "mRender": function ( data, type, row ) {
            if(data=="Teknis"){
                return '<span style="color:red">'+data+'</span>';
            }else{
                return data;
            }
        },
        "bSortable": false,
        "sWidth":"200px",
        "bVisible": true
    } ,
    {
        "sName": null,
        "mDataProp": "aksi",
        "aTargets": [4],
        "mRender": function ( data, type, row ) {
            if(data==2){
                return '<input type="button" value="Send Problems" class="btn btn-cancel" onclick="sendProblemFinding('+row['id']+',\''+row['idJob']+'\',\''+row['teknisi']+'\')"> ';
            }else{
                return data;
            }
        },
        "bSortable": false,
        "sWidth":"275px",
        "bVisible": true
    } ,
    {
        "sName": null,
        "mDataProp": "foremanGanti",
        "aTargets": [5],
        "bSortable": false,
        "sWidth":"200px",
        "bVisible": true
    }

    ],
            "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                            aoData.push(
                                       {"name": 't401NoWO', "value": "${t401NoWO}"},
                                       {"name": 'teknisi', "value": "${teknisi}"},
                                       {"name": 'idJob', "value": "${idJob}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
    </g:javascript>
</head>
<body>
<div class="innerinnerDetails">
    <table id="jobInstructionGr_datatables_sub_sub_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
           class="display table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Status</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Tanggal Status</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Keterangan</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Problem?</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Action</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Foreman Pengganti</div>
            </th>

        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
</body>
</html>
