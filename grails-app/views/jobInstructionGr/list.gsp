<%@ page import="com.kombos.parts.StokOPName" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'jobInstructionGr.label', default: 'Job Instruction')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>
			var finalIns="";
			var tambahJob="";
			var staAmbilWo="";
			var staCarry="";
			var edit;
			var c=document.getElementById("myCanvas");
            var ctx=c.getContext("2d");
            ctx.fillStyle="yellow";
            ctx.fillRect(0,0,60,20);
			var d=document.getElementById("myCanvas2");
            var ctx2=d.getContext("2d");
            ctx2.fillStyle="greenyellow";
            ctx2.fillRect(0,0,60,20);
			$(function(){ 
			
				 //darisini
            $("#jobAddModal").on("show", function() {
                $("#jobAddModal .btn").on("click", function(e) {
                    $("#jobAddModal").modal('hide');
                });
            });
            $("#jobAddModal").on("hide", function() {
                $("#jobAddModal a.btn").off("click");
            });

        addTeknisi = function(id) {
            var nRow = id;
                   $("#jobAddContent").empty();
                    expandTableLayout();
                    $.ajax({type:'POST', url:'${request.contextPath}/jobInstructionGr/addTeknisi',
                    data : {noWo : nRow},
                        success:function(data,textStatus){
                                $("#jobAddContent").html(data);
                                $("#jobAddModal").modal({
                                    "backdrop" : "static",
                                    "keyboard" : true,
                                    "show" : true
                                }).css({'width': '880px','margin-left': function () {return -($(this).width() / 2);}});

                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });

         };

         closeWo = function(){
            checkInspecDuring =[];
            var nRow = "";
            var idCheckJobForTeknisi="";
            $("#jobInstructionGr-table tbody .row-select").each(function() {
                 if(this.checked){
                   nRow = $(this).next("input:hidden").val();

				  checkInspecDuring.push(nRow);
                  }
            });
           if(checkInspecDuring.length<1 || checkInspecDuring.length>1 ){
                alert('Silahkan Pilih Salah satu WO');
                return;

           }else{

                $('#spinner').fadeIn(1);
                $.ajax({type:'POST', url:'${request.contextPath}/WoDetailGr/woJalan',
                    data : {noWo: nRow},
                    success:function(data,textStatus){
                       if(data=="1"){
                            alert("Sudah Close WO Berobat Jalan");
                       }else{
                         shrinkTableLayout();
                         loadForm(data, textStatus);
                       }
                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){},
                    complete:function(XMLHttpRequest,textStatus){
                        $('#spinner').fadeOut();
                    }
                });
           }
         }
         sendProblemFinding = function(id,idJob,teknisi) {
            var nRow = id;
                   shrinkTableLayout();
                    $('#spinner').fadeIn(1);
                    $.ajax({type:'POST', url:'${request.contextPath}/jobInstructionGr/sendProblemFinding',
                        data : {id: id,idJob:idJob,teknisi:teknisi},
                        success:function(data,textStatus){
                            loadForm(data, textStatus);
                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
         };
         kurangiJob = function(id,idRcp) {
            var nRow = id;
            var idRcp = idRcp;
                   $("#jobAddContent").empty();
                    expandTableLayout();
                    $.ajax({type:'POST', url:'${request.contextPath}/jobInstructionGr/kurangiJob',
                    data : {noWo : nRow,idRcp:idRcp},
                        success:function(data,textStatus){
                                $("#jobAddContent").html(data);
                                $("#jobAddModal").modal({
                                    "backdrop" : "static",
                                    "keyboard" : true,
                                    "show" : true
                                }).css({'width': '880px','margin-left': function () {return -($(this).width() / 2);}});

                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
         };

             ambilWO = function() {
                checkInspecDuring =[];
                var nRow = "";
                 var idCheckJobForTeknisi="";
                $("#jobInstructionGr-table tbody .row-select").each(function() {
                     if(this.checked){
                       nRow = $(this).next("input:hidden").val();

                      checkInspecDuring.push(nRow);
                      }
                  });
                   if(checkInspecDuring.length<1 || checkInspecDuring.length>1 ){
                    alert('Silahkan Pilih Salah satu WO');
                    return;
                   }else{
                       $("#jobAddContent").empty();
                        expandTableLayout();
                        $.ajax({type:'POST', url:'${request.contextPath}/jobInstructionGr/ambilWO',
                        data : {noWo : nRow},
                            success:function(data,textStatus){
                                    $("#jobAddContent").html(data);
                                    $("#jobAddModal").modal({
                                        "backdrop" : "static",
                                        "keyboard" : true,
                                        "show" : true
                                    }).css({'width': '880px','margin-left': function () {return -($(this).width() / 2);}});

                            },
                            error:function(XMLHttpRequest,textStatus,errorThrown){},
                            complete:function(XMLHttpRequest,textStatus){
                                $('#spinner').fadeOut();
                            }
                        });
                   }
             };

                loadForm = function(data, textStatus){
                    $('#jobInstructionGr-form').empty();
                    $('#jobInstructionGr-form').append(data);
                }
                shrinkTableLayout = function(){
                    $("#jobInstructionGr-table").hide();
                    $("#jobInstructionGr-form").css("display","block");
                }

                expandTableLayout = function(){
                    $("#jobInstructionGr-table").show();
                    $("#jobInstructionGr-form").css("display","none");
                }
            statusForeman = function(){
                window.location.replace('#/foreman');
                %{--$.ajax({--}%
                    %{--url:'${request.contextPath}/Foreman/list',--}%
                    %{--type: "POST",--}%

                    %{--complete : function (req, err) {--}%
                        %{--$('#main-content').html(req.responseText);--}%
                        %{--$('#spinner').fadeOut();--}%
                        %{--loading = false;--}%
                    %{--}--}%
                %{--});--}%
            }

            woDetail = function(){
                    checkJobInstructionGr =[];
                    var idCheckJobForTeknisi="";
                    var checkID =[];
                    $("#jobInstructionGr-table tbody .row-select").each(function() {
                            if(this.checked){
                            var nRow = $(this).next("input:hidden").val();
                            checkJobInstructionGr.push(nRow);
                            idCheckJobInstructionGr = JSON.stringify(checkJobInstructionGr);
                            window.location.replace('#/WoDetailGr');
                            $.ajax({
                                    url:'${request.contextPath}/woDetailGr/list',
                                    type: "POST",
                                    data: { id: nRow, aksi : 'jobt' },
                                    dataType: "html",
                                    complete : function (req, err) {
                                        $('#main-content').html(req.responseText);
                                        $('#spinner').fadeOut();
                                        loading = false;
                                    }
                                });
                        }
                    });
                    if(checkJobInstructionGr.length<1 || checkJobInstructionGr.length>1 ){
                        alert('Silahkan Pilih Salah satu WO');
                        return;
                    }
            }
});


var checkin = $('#search_t132Tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
        var newDate = new Date(ev.date)
        newDate.setDate(newDate.getDate() + 1);
        checkout.setValue(newDate);
        checkin.hide();
        $('#search_t132Tanggalakhir')[0].focus();
    }).data('datepicker');
    
    var checkout = $('#search_t132Tanggalakhir').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

        return true;
    }
    function filterWO(){
        finalIns = $('#staFinalInspection').val();
        staAmbilWo=$('#staAmbilWO').val();
        staCarry = $('#staCarryOver').val();
        tambahJob=$('#staJobTambah').val();
        if(staAmbilWo=='Belum Ambil WO'){
            $("#btnaAmbilWO").prop('disabled', false);
        }
        if(finalIns=='Belum Final Inspection'){
            $('#kotakFinal').show();
        }
        if(tambahJob=='Ya'){
            $('#kotakTambah').show();
        }
        reloadBPJobInstructionTable();
    }

    function clearForm(){
        finalIns = "";
        tambahJob = "";
        staAmbilWo="";
        $('#search_Tanggal').val('')
        $('#search_Tanggalakhir').val('')
        $('#staJobTambah').val('')
        $('#staAmbilWO').val('')
        $('#staCarryOver').val('')
        $('#staFinalInspection').val('')
        $('#staProblemFinding').val('')
        $("#btnaAmbilWO").prop('disabled', true);
        $('#kotakFinal').hide();
        $('#kotakTambah').hide();
        reloadBPJobInstructionTable();
    }

</g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="jobInstruction.view.label" default="Job Instruction" /></span>
    <ul class="nav pull-right"> </ul>
</div>
<div class="box">
    <div class="span12" id="jobInstructionGr-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <table>
            <tr>
                <td style="padding: 5px">
                    <g:message code="wo.tanggalWO.label" default="Tanggal WO"/>
                </td>
                <td style="padding: 5px">
                <ba:datePicker name="search_Tanggal" precision="day" value="" disable="true" format="dd-MM-yyyy"/>&nbsp;s.d.&nbsp;
                <ba:datePicker name="search_Tanggalakhir" precision="day" value="" disable="true" format="dd-MM-yyyy"/>
                </td>
                <td style="padding: 5px">
                <g:message code="bPJobInstruction.staJobTambah.label" default="Status Job Tambahan"/>
                </td>
                <td style="padding: 5px">
                <g:select name="staJobTambah" id="staJobTambah" from="${['Ya','Tidak']}" noSelection="${['':'Pilih status job tambah']}" />
                </td>
                <td rowspan="3">
                <g:field type="button" onclick="filterWO();" style="padding: 5px; width: 100px"
                class="btn btn-primary create" name="filter" id="filter" value="${message(code: 'default.button.filter.label', default: 'Filter')}" />
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                <g:field type="button" onclick="clearForm();" style="padding: 5px; width: 100px"
                class="btn cancel" name="clear" id="clear" value="${message(code: 'default.button.clear.label', default: 'Clear Filter')}" />
                </td>
            </tr>
            <tr>
                <td style="padding: 5px"><g:message code="bPJobInstruction.staAmbilWO.label" default="Status Ambil WO"/>
                </td>
                <td style="padding: 5px">
                <g:select name="staAmbilWO" style="width:100%" id="staAmbilWO" noSelection="${['':'Pilih status ambil WO']}" from="${['Semua WO','Belum Ambil WO','Sudah Ambil WO','Belum dan Sudah Ambil WO']}" />
                </td>
                <td style="padding: 5px">
                <g:message code="bPJobInstruction.staCarryOver.label" default="Status Carry Over"/>
                </td>
                <td style="padding: 5px">
                <g:select name="staCarryOver" id="staCarryOver" from="${['Ya','Tidak']}" noSelection="${['':'Pilih status carry over']}"  />
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                <g:message code="bPJobInstruction.staProblemFinding.label" default="Status Problem Finding"/>
                </td>
                <td style="padding: 5px">
                <g:select style="width:100%" name="staProblemFinding" id="staProblemFinding" from="${['Teknisi','Non-Teknis']}"  noSelection="${['':'Pilih status problem finding']}" />
                </td>
                <td style="padding: 5px">
                <g:message code="bPJobInstruction.staFinalInspection.label" default="Status Final Inspection"/>
                </td>
                <td style="padding: 5px">
                <g:select name="staFinalInspection" id="staFinalInspection" from="${['Sudah Final Inspection','Belum Final Inspection']}" noSelection="${['':'Pilih status final Inspection']}" />
                </td>
            </tr>
        </table>
        <br/><br/>
        <g:render template="dataTables" />
        <g:field type="button" onclick="statusForeman()" style="padding: 5px;"
                 class="btn cancel" name="staForeman" id="staForeman" value="${message(code: 'wo.staForeman.label', default: 'Lihat Status Foreman')}" />
        <br/>
        <br/>
        <div id="kotakFinal" style="display: none">
            <canvas id="myCanvas" width="60" height="20" style="border:1px solid #c3c3c3;">
                Your browser does not support the HTML5 canvas tag.
            </canvas> = Butuh Final Inspection
        </div>
        <br/>
        <div id="kotakTambah" style="display: none">
            <canvas id="myCanvas2" width="60" height="20" style="border:1px solid #c3c3c3;">
                Your browser does not support the HTML5 canvas tag.
            </canvas> = Job Tambahan
        </div>
        <div id="">

        </div>
        <g:field type="button" style="padding: 5px;" disabled="true" onclick="ambilWO()"
                 class="btn cancel" name="btnaAmbilWO" id="btnaAmbilWO" value="${message(code: 'wo.staAmbilWO.label', default: 'Update Status Ambil WO')}" />
        &nbsp;&nbsp;
        <g:field type="button" onclick="woDetail()" style="padding: 5px;"
                 class="btn cancel" name="woDetail" id="woDetail" value="${message(code: 'wo.woDetail.label', default: 'WO Detail')}" />
        %{--<button id='btn2' onclick="finalInspection();" type="button" class="btn btn-cancel" style="width: 150px;">Final Inspection</button>--}%
        <g:field type="button" onclick="closeWo()" style="padding: 5px;"

                 class="btn cancel" name="woDetail" id="closeWo" value="${message(code: 'wo.woDetail.label', default: 'Close WO Berobat Jalan')}" />
        %{--<g:field type="button" onclick="sendProblemFinding(1)" style="padding: 5px;"--}%
                 %{--class="btn cancel" name="send"  value="${message(code: 'wo.woDetail.label', default: 'Send Prob')}" />--}%
        </div>

    <div class="span7" id="jobInstructionGr-form" style="display: none; width: 1200px;"></div>
</div>
<div id="jobAddModal" class="modal fade"style="width: 800px;">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content" style="width: 800px;">
            <!-- dialog body -->
            <div class="modal-body" style="max-height: 800px; width: 800px;">
                <div id="jobAddContent"/>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="iu-content"></div>
            </div>

        </div>
    </div>
</div>
</body>
</html>
