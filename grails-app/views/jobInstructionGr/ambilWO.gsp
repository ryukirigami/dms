
<%@ page import="com.kombos.administrasi.Operation; com.kombos.administrasi.NamaProsesBP; com.kombos.parts.Returns" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'returnsAdd.label', default: 'Ambil WO')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
        return false;
	});


   });
    $(document).ready(function()
    {


        ubahAmbilWo = function(){
                var nomorWO = $('#nomorWO').val();
                var id = $('#id').val();
                var ambilWO = "";
                var tanggal = $('#tanggal').val();
                if(document.getElementById("ambilWo").checked){ambilWO = "1"}else{ambilWO = "0"}
                  if(ambilWO=="" || ambilWO==null){
                    alert('Anda belum melakukan perubahan');
                 }else{
                    $.ajax({
                        url:'${request.contextPath}/jobInstructionGr/ubahAmbilWo',
                        type: "POST", // Always use POST when deleting data
                         data : {id:id,noWO : nomorWO , updateStaWO : ambilWO},
                        success : function(data){
                            toastr.success('<div>Wo Diambil</div>');
                        },
                    error: function(xhr, textStatus, errorThrown) {
                    alert('Internal Server Error');
                    }
                    });
                }
        }
    });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
    <ul class="nav pull-right">
        <li></li>
        <li></li>
        <li class="separator"></li>
    </ul>
</div>
<div class="box">
    <fieldset>
        <div class="row-fluid">
                <table style="width: 95%;">
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Nomor WO" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="nomorWO" id="nomorWO" value="${noWo}" readonly="" />
                            <g:hiddenField style="width:100%" name="id" id="id" value="${id}" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Nomor Polisi" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="nomorPolisi" value="${nopol}" readonly="" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Model Kendaraan" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="modelKendaraan" value="${model}" readonly=""/>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Nama Stall" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="modelKendaraan" value="${stall}" readonly=""/>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Tanggal WO" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="modelKendaraan" value="${tanggalWO}" readonly=""/>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Status Ambil WO *" />
                        </td>
                        <td>
                           <input type="checkbox" id="ambilWo" name="ambilWo"/> WO Sudah diambil
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Tanggal Update" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="tanggal" id="tanggal" value="${tanggal}" readonly=""/>
                        </td>
                    </tr>

                    <tr>
                        <td style="padding: 5px" colspan="2">
                            <button id='btn1' onclick="ubahAmbilWo();" type="button" class="btn btn-cancel" style="width: 150px;">OK</button>
                            <button id='btn2' type="button" class="btn btn-cancel" style="width: 150px;">Close</button>
                        </td>
                    </tr>
                </table>
        </div>
    </fieldset>
    </div>
</div>
</body>
</html>

