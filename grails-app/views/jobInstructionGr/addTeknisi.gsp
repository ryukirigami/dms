
<%@ page import="com.kombos.reception.Reception; com.kombos.administrasi.NamaManPower; com.kombos.administrasi.Operation; com.kombos.administrasi.NamaProsesBP; com.kombos.parts.Returns" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'returnsAdd.label', default: 'Add Teknisi')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
        return false;
	});


   });
    $(document).ready(function()
    {


        editTeknisi = function(){
                var nomorWO = $('#nomorWO').val();
                var id = $('#id').val();
                var teknisi = $('#teknisi').val();
                var dataKu = $('#job').val();
                var dataKu2 = dataKu.split('.');
                var job = dataKu2[0];
                $.ajax({
                    url:'${request.contextPath}/jobInstructionGr/editTeknisi',
                    type: "POST", // Always use POST when deleting data
                    data : {id:id,noWo : nomorWO,teknisi:teknisi,job:job},
                    success : function(data){
                        //window.location.href = '#/claim' ;
                        toastr.success('<div>Ubah Sukses</div>');
                        reloadBPJobInstructionTable();
                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });

        }
    });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
    <ul class="nav pull-right">
        <li></li>
        <li></li>
        <li class="separator"></li>
    </ul>
</div>
<div class="box">
    <fieldset>
        <div class="row-fluid">
                <table style="width: 95%;">
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Nomor WO" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="nomorWO" id="nomorWO" value="${noWo}" readonly="" />
                            <g:hiddenField style="width:100%" name="id" id="id" value="${id}" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Nomor Polisi" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="nomorPolisi" value="${nopol}" readonly="" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Model Kendaraan" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="modelKendaraan" value="${model}" readonly=""/>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Nama Stall" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="modelKendaraan" value="${stall}" readonly=""/>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Tanggal WO" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="modelKendaraan" value="${tanggalWO}" readonly=""/>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Teknisi" />
                        </td>
                        <td>
                            <g:select name="teknisi" id="teknisi" from="${NamaManPower?.createCriteria()?.list {eq("staDel","0");eq("companyDealer",session?.userCompanyDealer);manPowerDetail{manPower{or{ilike("m014JabatanManPower","%teknisi%");ilike("m014JabatanManPower","%GR%")}}}}}" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Job" />
                        </td>
                        <td>
                            <g:select name="kegiatan" id="job" from="${com.kombos.woinformation.JobRCP.createCriteria().list {eq("staDel","0");eq("reception",Reception.get(id.toLong()))}}" />
                        </td>
                    </tr>

                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Tanggal Tambah" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="modelKendaraan" value="${tanggal}" readonly=""/>
                        </td>
                    </tr>

                    <tr>
                        <td style="padding: 5px" colspan="2">
                            <button id='btn1' onclick="editTeknisi();" type="button" class="btn btn-cancel" style="width: 150px;">OK</button>
                            <button id='btn2' type="button" class="btn btn-cancel" style="width: 150px;">Close</button>
                        </td>
                    </tr>
                </table>
        </div>
    </fieldset>
    </div>
</div>
</body>
</html>

