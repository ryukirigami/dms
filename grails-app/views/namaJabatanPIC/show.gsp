

<%@ page import="com.kombos.maintable.NamaJabatanPIC" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'namaJabatanPIC.label', default: 'Nama Jabatan PIC')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteNamaJabatanPIC;

$(function(){ 
	deleteNamaJabatanPIC=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/namaJabatanPIC/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadNamaJabatanPICTable();
   				expandTableLayout('namaJabatanPIC');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-namaJabatanPIC" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="namaJabatanPIC"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${namaJabatanPICInstance?.id}">
				<tr>
				    <td class="span2" style="text-align: right;">
                        <span id="m093ID-label" class="property-label">
                            <g:message code="namaJabatanPIC.m093ID.label" default="ID" />:
                        </span>
                    </td>
					
                    <td class="span3">
                        <span class="property-value" aria-labelledby="m093ID-label">
                            <g:fieldValue bean="${namaJabatanPICInstance}" field="id"/>
                        </span>
                    </td>
				</tr>
				</g:if>
			
				<g:if test="${namaJabatanPICInstance?.m093NamaJabatan}">
				<tr>
                    <td class="span2" style="text-align: right;">
                        <span id="m093NamaJabatan-label" class="property-label">
                            <g:message code="namaJabatanPIC.m093NamaJabatan.label" default="Nama Jabatan" />:
                        </span>
                    </td>
					
                    <td class="span3">
                        <span class="property-value" aria-labelledby="m093NamaJabatan-label">
                            <g:fieldValue bean="${namaJabatanPICInstance}" field="m093NamaJabatan"/>
                        </span>
                    </td>
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('namaJabatanPIC');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${namaJabatanPICInstance?.id}"
					update="[success:'namaJabatanPIC-form',failure:'namaJabatanPIC-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteNamaJabatanPIC('${namaJabatanPICInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
