<%@ page import="com.kombos.maintable.NamaJabatanPIC" %>
<div class="control-group fieldcontain ${hasErrors(bean: namaJabatanPICInstance, field: 'm093NamaJabatan', 'error')} required">
	<label class="control-label" for="m093NamaJabatan">
		<g:message code="namaJabatanPIC.m093NamaJabatan.label" default="Nama Jabatan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m093NamaJabatan" required="" value="${namaJabatanPICInstance?.m093NamaJabatan}"/>
	</div>
</div>


