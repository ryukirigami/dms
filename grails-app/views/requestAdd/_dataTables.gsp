
<%@ page import="com.kombos.parts.SOQ" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="requestAdd_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hoverfixed" style="table-layout: fixed; width: 900px">
    <col width="100px" />
    <col width="250px" />
    <col width="250px" />
    <col width="150px" />
    <col width="150px" />
    <thead>
    <tr>
        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="requestAdd.t156Tanggal.label" default="No." /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="requestAdd.goods.label" default="Kode Parts" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="requestAdd.goods.label" default="Nama Parts" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="requestAdd.goods.label" default="Harga Jual" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="requestAdd.goods.label" default="Stock Gudang" /></div>
        </th>


    </tr>
    <tr>
        <th style="border-top: none;padding: 5px;"></th>
        <th style="border-top: none;padding: 5px;">
            <div id="filter_goods" style="padding-top: 0px;position:relative; margin-top: 0px;">
             <input type="text" name="search_goods" class="search_init" />
            </div>
        </th>
        <th style="border-top: none;padding: 5px;">
            <div id="filter_goods2" style="padding-top: 0px;position:relative; margin-top: 0px;">
                <input type="text" name="search_goods2" class="search_init" />
            </div>
        </th>
        <th style="border-top: none;padding: 5px;"></th>
        <th style="border-top: none;padding: 5px;"></th>
    </tr>
    </thead>
</table>

<g:javascript>
var requestAddTable;
var reloadrequestAddTable;
$(function(){

	reloadrequestAddTable = function() {
		requestAddTable.fnDraw();
	}

	
	$('#search_t156Tanggal').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t156Tanggal_day').val(newDate.getDate());
			$('#search_t156Tanggal_month').val(newDate.getMonth()+1);
			$('#search_t156Tanggal_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
		//	requestAddTable.fnDraw();
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	requestAddTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	requestAddTable = $('#requestAdd_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "id",
	"mDataProp": "norut",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="#">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="#">&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}

,

{
	"sName": "m111ID",
	"mDataProp": "goods",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"250px",
	"bVisible": true
}
,

{
	"sName": "m111Nama",
	"mDataProp": "goods2",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"250px",
	"bVisible": true
},

{
	"sName": "m111Nama",
	"mDataProp": "hargaJual",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"250px",
	"bVisible": true
},

{
	"sName": "m111Nama",
	"mDataProp": "stockGudang",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"250px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}

						var t156Tanggal = $('#search_t156Tanggal').val();
						var t156TanggalDay = $('#search_t156Tanggal_day').val();
						var t156TanggalMonth = $('#search_t156Tanggal_month').val();
						var t156TanggalYear = $('#search_t156Tanggal_year').val();
						
						if(t156Tanggal){

							aoData.push(
									{"name": 'sCriteria_t156Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_t156Tanggal_dp', "value": t156Tanggal},
									{"name": 'sCriteria_t156Tanggal_day', "value": t156TanggalDay},
									{"name": 'sCriteria_t156Tanggal_month', "value": t156TanggalMonth},
									{"name": 'sCriteria_t156Tanggal_year', "value": t156TanggalYear}
							);
						}
	
						var goods = $('#filter_goods input').val();
						if(goods){
							aoData.push(
									{"name": 'sCriteria_goods', "value": goods}
							);
						}
						var goods2 = $('#filter_goods2 input').val();
						if(goods2){
							aoData.push(
									{"name": 'sCriteria_goods2', "value": goods2}
							);
						}
						
						var exist =[];
						$("#request_datatables tbody .row-select").each(function() {
							var id = $(this).next("input:hidden").val();
							exist.push(id);							
						});
						if(exist.length > 0){
							aoData.push(
										{"name": 'sCriteria_exist', "value": JSON.stringify(exist)}
							);
						}
						

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
