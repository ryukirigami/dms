<%@ page import="com.kombos.parts.Goods; com.kombos.parts.Group; com.kombos.parts.GoodsDiskon" %>
<g:javascript>
    jQuery(function($) {
        $('.persen').autoNumeric('init',{
            vMin:'0',
            vMax:'100',
            aSep:'',
            mDec: '2'
        });
    });
    var checkin = $('#tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
                checkin.hide();
                $('#tanggal2')[0].focus();
            }).data('datepicker');

    var checkout = $('#tanggal2').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
                checkout.hide();
    }).data('datepicker');

    $(function(){
			$('#namaParts').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/goodsDiskon/listParts', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});

    function detailParts(){
                var noGoods = $('#namaParts').val();
                $.ajax({
                    url:'${request.contextPath}/goodsDiskon/detailParts?noGoods='+noGoods,
                    type: "POST", // Always use POST when deleting data
                    success : function(data){
                        $('#idGoods').val("");
                        $('#namaGoods').val("");
                        if(data.hasil=="ada"){
                            $('#idGoods').val(data.id);
                            console.log("idGoods : " + data.id)
                        }
                    }
                })
        }
</g:javascript>



<div class="control-group fieldcontain ${hasErrors(bean: goodsDiskonInstance, field: 'm171TglAwal', 'error')} required">
    <label class="control-label" for="m171TglAwal">
        <g:message code="goodsDiskon.m171TglAwal.label" default="Tgl Awal" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <ba:datePicker name="m171TglAwal" id="tanggal"  precision="day"  value="${goodsDiskonInstance?.m171TglAwal}"  format="dd/mm/yyyy" required="true"  />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: goodsDiskonInstance, field: 'm171TglAkhir', 'error')} required">
    <label class="control-label" for="m171TglAkhir">
        <g:message code="goodsDiskon.m171TglAkhir.label" default="Tgl Akhir" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <ba:datePicker name="m171TglAkhir" id="tanggal2" precision="day"  value="${goodsDiskonInstance?.m171TglAkhir}" format="dd/mm/yyyy" required="true" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: goodsDiskonInstance, field: 'group', 'error')} required">
	<label class="control-label" for="group">
		<g:message code="goodsDiskon.group.label" default="Group" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="group" name="group.id" from="${Group.createCriteria().list(){eq("staDel", "0")}}" optionKey="id"  value="${goodsDiskonInstance?.group?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: goodsDiskonInstance, field: 'goods', 'error')} required">
	<label class="control-label" for="goods">
		<g:message code="goodsDiskon.goods.label" default="Goods" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:textField name="namaParts" id="namaParts" class="typeahead" maxlength="11" value="${goodsDiskonInstance?.goods?.m111ID?goodsDiskonInstance?.goods?.m111ID?.trim()+' || '+goodsDiskonInstance?.goods?.m111Nama:""}" autocomplete="off" onblur="detailParts();"/>
        <g:hiddenField name="goods.id" id="idGoods"  value="${goodsDiskonInstance?.goods?.id}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: goodsDiskonInstance, field: 'm171PersenDiskon', 'error')} required">
	<label class="control-label" for="m171PersenDiskon">
		<g:message code="goodsDiskon.m171PersenDiskon.label" default="% Diskon" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m171PersenDiskon"  class="persen" maxlength="3" value="${fieldValue(bean: goodsDiskonInstance, field: 'm171PersenDiskon')}" required="true" /> %
	</div>
</div>

<g:javascript>
    document.getElementById("m171TglAwal").focus();
</g:javascript>