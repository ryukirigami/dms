
<%@ page import="com.kombos.parts.GoodsDiskon" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="goodsDiskon_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="goodsDiskon.m171TglAwal.label" default="Tgl Awal" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="goodsDiskon.m171TglAkhir.label" default="Tgl Akhir" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="goodsDiskon.group.label" default="Group" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="goodsDiskon.goods.label" default="Goods" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
				<div><g:message code="goodsDiskon.m171PersenDiskon.label" default="Persen Diskon" /></div>
			</th>


		
		</tr>
		<tr>


			<th style="border-top: none;padding: 5px;">
				<div id="filter_m171TglAwal" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m171TglAwal" value="date.struct">
					<input type="hidden" name="search_m171TglAwal_day" id="search_m171TglAwal_day" value="">
					<input type="hidden" name="search_m171TglAwal_month" id="search_m171TglAwal_month" value="">
					<input type="hidden" name="search_m171TglAwal_year" id="search_m171TglAwal_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m171TglAwal_dp" value="" id="search_m171TglAwal" class="search_init">
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m171TglAkhir" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m171TglAkhir" value="date.struct">
					<input type="hidden" name="search_m171TglAkhir_day" id="search_m171TglAkhir_day" value="">
					<input type="hidden" name="search_m171TglAkhir_month" id="search_m171TglAkhir_month" value="">
					<input type="hidden" name="search_m171TglAkhir_year" id="search_m171TglAkhir_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m171TglAkhir_dp" value="" id="search_m171TglAkhir" class="search_init">
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_group" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_group" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_goods" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_goods" class="search_init" />
                </div>
            </th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m171PersenDiskon" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m171PersenDiskon"  class="search_init persen" maxlength="3" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var goodsDiskonTable;
var reloadGoodsDiskonTable;
$(function(){
	
	reloadGoodsDiskonTable = function() {
		goodsDiskonTable.fnDraw();
	}

	var recordsGoodsDiskonPerPage = [];
    var anGoodsDiskonSelected;
    var jmlRecGoodsDiskonPerPage=0;
    var id;
	$('#search_m171TglAwal').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m171TglAwal_day').val(newDate.getDate());
			$('#search_m171TglAwal_month').val(newDate.getMonth()+1);
			$('#search_m171TglAwal_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			goodsDiskonTable.fnDraw();	
	});

	

	$('#search_m171TglAkhir').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m171TglAkhir_day').val(newDate.getDate());
			$('#search_m171TglAkhir_month').val(newDate.getMonth()+1);
			$('#search_m171TglAkhir_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			goodsDiskonTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	goodsDiskonTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	goodsDiskonTable = $('#goodsDiskon_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsGoodsDiskon = $("#goodsDiskon_datatables tbody .row-select");
            var jmlGoodsDiskonCek = 0;
            var nRow;
            var idRec;
            rsGoodsDiskon.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsGoodsDiskonPerPage[idRec]=="1"){
                    jmlGoodsDiskonCek = jmlGoodsDiskonCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsGoodsDiskonPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecGoodsDiskonPerPage = rsGoodsDiskon.length;
            if(jmlGoodsDiskonCek==jmlRecGoodsDiskonPerPage){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m171TglAwal",
	"mDataProp": "m171TglAwal",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m171TglAkhir",
	"mDataProp": "m171TglAkhir",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "group",
	"mDataProp": "group",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "goods",
	"mDataProp": "goods",
	"aTargets": [3],

	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}


,

{
	"sName": "m171PersenDiskon",
	"mDataProp": "m171PersenDiskon",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}
	
						var goods = $('#filter_goods input').val();
						if(goods){
							aoData.push(
									{"name": 'sCriteria_goods', "value": goods}
							);
						}
	
						var group = $('#filter_group input').val();
						if(group){
							aoData.push(
									{"name": 'sCriteria_group', "value": group}
							);
						}

						var m171TglAwal = $('#search_m171TglAwal').val();
						var m171TglAwalDay = $('#search_m171TglAwal_day').val();
						var m171TglAwalMonth = $('#search_m171TglAwal_month').val();
						var m171TglAwalYear = $('#search_m171TglAwal_year').val();
						
						if(m171TglAwal){
							aoData.push(
									{"name": 'sCriteria_m171TglAwal', "value": "date.struct"},
									{"name": 'sCriteria_m171TglAwal_dp', "value": m171TglAwal},
									{"name": 'sCriteria_m171TglAwal_day', "value": m171TglAwalDay},
									{"name": 'sCriteria_m171TglAwal_month', "value": m171TglAwalMonth},
									{"name": 'sCriteria_m171TglAwal_year', "value": m171TglAwalYear}
							);
						}

						var m171TglAkhir = $('#search_m171TglAkhir').val();
						var m171TglAkhirDay = $('#search_m171TglAkhir_day').val();
						var m171TglAkhirMonth = $('#search_m171TglAkhir_month').val();
						var m171TglAkhirYear = $('#search_m171TglAkhir_year').val();
						
						if(m171TglAkhir){
							aoData.push(
									{"name": 'sCriteria_m171TglAkhir', "value": "date.struct"},
									{"name": 'sCriteria_m171TglAkhir_dp', "value": m171TglAkhir},
									{"name": 'sCriteria_m171TglAkhir_day', "value": m171TglAkhirDay},
									{"name": 'sCriteria_m171TglAkhir_month', "value": m171TglAkhirMonth},
									{"name": 'sCriteria_m171TglAkhir_year', "value": m171TglAkhirYear}
							);
						}
	
						var m171PersenDiskon = $('#filter_m171PersenDiskon input').val();
						if(m171PersenDiskon){
							aoData.push(
									{"name": 'sCriteria_m171PersenDiskon', "value": m171PersenDiskon}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#goodsDiskon_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsGoodsDiskonPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsGoodsDiskonPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#goodsDiskon_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsGoodsDiskonPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anGoodsDiskonSelected = goodsDiskonTable.$('tr.row_selected');

            if(jmlRecGoodsDiskonPerPage == anGoodsDiskonSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsGoodsDiskonPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>
<script type="text/javascript">
    jQuery(function($) {
        $('.persen').autoNumeric('init',{
            vMin:'0',
            vMax:'100',
            mDec: '2',
            aSep:''
        });
    });
</script>


			
