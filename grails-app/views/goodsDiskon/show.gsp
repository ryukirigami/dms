

<%@ page import="com.kombos.parts.GoodsDiskon" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'goodsDiskon.label', default: 'Diskon Per Kode')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteGoodsDiskon;

$(function(){ 
	deleteGoodsDiskon=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/goodsDiskon/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadGoodsDiskonTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-goodsDiskon" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="goodsDiskon"
			class="table table-bordered table-hover">
			<tbody>
            <g:if test="${goodsDiskonInstance?.m171TglAwal}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m171TglAwal-label" class="property-label"><g:message
                                code="goodsDiskon.m171TglAwal.label" default="Tgl Awal" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m171TglAwal-label">
                        %{--<ba:editableValue
                                bean="${goodsDiskonInstance}" field="m171TglAwal"
                                url="${request.contextPath}/GoodsDiskon/updatefield" type="text"
                                title="Enter m171TglAwal" onsuccess="reloadGoodsDiskonTable();" />--}%

                        <g:formatDate date="${goodsDiskonInstance?.m171TglAwal}" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${goodsDiskonInstance?.m171TglAkhir}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m171TglAkhir-label" class="property-label"><g:message
                                code="goodsDiskon.m171TglAkhir.label" default="Tgl Akhir" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m171TglAkhir-label">
                        %{--<ba:editableValue
                                bean="${goodsDiskonInstance}" field="m171TglAkhir"
                                url="${request.contextPath}/GoodsDiskon/updatefield" type="text"
                                title="Enter m171TglAkhir" onsuccess="reloadGoodsDiskonTable();" />--}%

                        <g:formatDate date="${goodsDiskonInstance?.m171TglAkhir}" />

                    </span></td>

                </tr>
            </g:if>


				<g:if test="${goodsDiskonInstance?.group}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="group-label" class="property-label"><g:message
					code="goodsDiskon.group.label" default="Group" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="group-label">
						%{--<ba:editableValue
								bean="${goodsDiskonInstance}" field="group"
								url="${request.contextPath}/GoodsDiskon/updatefield" type="text"
								title="Enter group" onsuccess="reloadGoodsDiskonTable();" />--}%

                          <g:fieldValue bean="${goodsDiskonInstance?.group}" field="m181NamaGroup"/>
						</span></td>

				</tr>
				</g:if>

                <g:if test="${goodsDiskonInstance?.goods}">
                    <tr>
                    <td class="span2" style="text-align: right;"><span
                        id="goods-label" class="property-label"><g:message
                        code="goodsDiskon.goods.label" default="Goods" />:</span></td>

                            <td class="span3"><span class="property-value"
                            aria-labelledby="goods-label">
                            %{--<ba:editableValue
                                    bean="${goodsDiskonInstance}" field="goods"
                                    url="${request.contextPath}/GoodsDiskon/updatefield" type="text"
                                    title="Enter goods" onsuccess="reloadGoodsDiskonTable();" />--}%

                            <g:fieldValue bean="${goodsDiskonInstance?.goods}" field="m111Nama"/>
                            </span></td>

                    </tr>
                    </g:if>

				<g:if test="${goodsDiskonInstance?.m171PersenDiskon}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m171PersenDiskon-label" class="property-label"><g:message
					code="goodsDiskon.m171PersenDiskon.label" default="Persen Diskon" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m171PersenDiskon-label">
						%{--<ba:editableValue
								bean="${goodsDiskonInstance}" field="m171PersenDiskon"
								url="${request.contextPath}/GoodsDiskon/updatefield" type="text"
								title="Enter m171PersenDiskon" onsuccess="reloadGoodsDiskonTable();" />--}%
							
								<g:fieldValue bean="${goodsDiskonInstance}" field="m171PersenDiskon"/>%
							
						</span></td>
					
				</tr>
				</g:if>

			
				<g:if test="${goodsDiskonInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="goodsDiskon.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${goodsDiskonInstance}" field="createdBy"
								url="${request.contextPath}/GoodsDiskon/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadGoodsDiskonTable();" />--}%
							
								<g:fieldValue bean="${goodsDiskonInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${goodsDiskonInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="goodsDiskon.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${goodsDiskonInstance}" field="updatedBy"
								url="${request.contextPath}/GoodsDiskon/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadGoodsDiskonTable();" />--}%
							
								<g:fieldValue bean="${goodsDiskonInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${goodsDiskonInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="goodsDiskon.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${goodsDiskonInstance}" field="lastUpdProcess"
								url="${request.contextPath}/GoodsDiskon/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadGoodsDiskonTable();" />--}%
							
								<g:fieldValue bean="${goodsDiskonInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${goodsDiskonInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="goodsDiskon.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${goodsDiskonInstance}" field="dateCreated"
								url="${request.contextPath}/GoodsDiskon/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadGoodsDiskonTable();" />--}%
							
								<g:formatDate date="${goodsDiskonInstance?.dateCreated}" format="EEEE, dd MMMM yyyy HH:mm"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${goodsDiskonInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="goodsDiskon.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${goodsDiskonInstance}" field="lastUpdated"
								url="${request.contextPath}/GoodsDiskon/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadGoodsDiskonTable();" />--}%
							
								<g:formatDate date="${goodsDiskonInstance?.lastUpdated}"  format="EEEE, dd MMMM yyyy HH:mm"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${goodsDiskonInstance?.id}"
					update="[success:'goodsDiskon-form',failure:'goodsDiskon-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteGoodsDiskon('${goodsDiskonInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
