
<%@ page import="com.kombos.maintable.ApprovalT770" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'approvalT770.label', default: 'Approval')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			
			var edit;
			$(function(){ 
			
				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('approvalT770');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('approvalT770', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('approvalT770', '${request.contextPath}/approvalT770/massdelete', reloadApprovalT770Table);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				//show = function(id) {
				//	showInstance('approvalT770','${request.contextPath}/approvalT770/show/'+id);
				//};
				
				edit = function(id) {
					editInstance('approvalT770','${request.contextPath}/approvalT770/edit/'+id);
				};

});
        function openDetail(){
			$("#detailContent").empty();
			$('#spinner').fadeIn(1);
            $.ajax({type:'POST',
//                data: { id : $('#rceptionId').val()},
                url:'${request.contextPath}/approvalT770/detailApproval?idApproval='+$('#id').val(),
                success:function(data,textStatus){
                        $("#detailContent").html(data);
                        $("#detailModal").modal({
                            "backdrop" : "dynamic",
                            "keyboard" : true,
                            "show" : true
                        }).css({'width': '1100px','margin-left': function () {return -($(this).width() / 2);}});
                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
		}

        function saveBillTo(){
            var aoData =  new Array();
		    var recordsGoods = [];
            var recordsJobs = [];
            var recordsDiscJobs = [];
            var recordsDiscParts = [];
            $("#operationBillTo_datatables .row-select").each(function() {
                var id = $(this).next("input:hidden").val();
                recordsJobs.push(id);
                recordsDiscJobs.push($('#discJobs-'+id).val());
            });

            $("#goodsBillTo_datatables .row-select").each(function() {
                var id = $(this).next("input:hidden").val();
                recordsGoods.push(id);
                recordsDiscParts.push($('#discParts-'+id).val())
            });

            var jsonJ = JSON.stringify(recordsJobs);
            var jsonP = JSON.stringify(recordsGoods);
            var jsonDJ = JSON.stringify(recordsDiscJobs);
            var jsonDP = JSON.stringify(recordsDiscParts);
            aoData.push(
                {"name": 'jobs', "value": jsonJ},
                {"name": 'goods', "value": jsonP},
                {"name": 'discJobs', "value": jsonDJ},
                {"name": 'discParts', "value": jsonDP}
            );
            $.ajax({
                url:'${request.contextPath}/editJobParts/editBillTo',
                type: "POST", // Always use POST when deleting data
                data: aoData,
                complete: function(xhr, status) {
                    toastr.success('<div>Data Saved.</div>');
                    $('#closeModalBillto').click();

                },error:function(){
                    alert("Internal Server Error");
                }
            });

        }
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span7" id="approvalT770-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
		</div>
		<div class="span5" id="approvalT770-form">
		</div>
	</div>
    <div id="detailModal" class="modal fade">
        <div class="modal-dialog" style="width: 1100px;">
            <div class="modal-content" style="width: 1100px;">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 500px;">
                    <div id="detailContent"/>
                    <div class="iu-content"></div>

                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a href="javascript:void(0);" id="btnPrintdetail" onclick="saveBillTo();" class="btn btn-primary">
                Update
            </a>
            <a href="javascript:void(0);" data-dismiss="modal" id="closeModalBillto" class="btn cancel">
                Close
            </a>
        </div>
    </div>
</body>
</html>
