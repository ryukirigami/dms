<%@ page import="com.kombos.maintable.ApprovalT770" %>



<div class="control-group fieldcontain ${hasErrors(bean: approvalT770Instance, field: 'kegiatanApproval', 'error')} ">
	<label class="control-label" for="kegiatanApproval">
		<g:message code="approvalT770.kegiatanApproval.label" default="Kegiatan Approval" />
		
	</label>
	<div class="controls">
	<g:select id="kegiatanApproval" name="kegiatanApproval.id" from="${com.kombos.administrasi.KegiatanApproval.list()}" optionKey="id" required="" value="${approvalT770Instance?.kegiatanApproval?.id}" class="many-to-one"/>
	</div>
</div>
<g:javascript>
    document.getElementById("kegiatanApprovalvv").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: approvalT770Instance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="approvalT770.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${approvalT770Instance?.lastUpdProcess}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: approvalT770Instance, field: 't770CurrentLevel', 'error')} ">
	<label class="control-label" for="t770CurrentLevel">
		<g:message code="approvalT770.t770CurrentLevel.label" default="T770 Current Level" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="t770CurrentLevel" value="${approvalT770Instance.t770CurrentLevel}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: approvalT770Instance, field: 't770FK', 'error')} ">
	<label class="control-label" for="t770FK">
		<g:message code="approvalT770.t770FK.label" default="T770 FK" />
		
	</label>
	<div class="controls">
	<g:textField name="t770FK" value="${approvalT770Instance?.t770FK}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: approvalT770Instance, field: 't770JumlahDiajukan', 'error')} ">
	<label class="control-label" for="t770JumlahDiajukan">
		<g:message code="approvalT770.t770JumlahDiajukan.label" default="T770 Jumlah Diajukan" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="t770JumlahDiajukan" value="${approvalT770Instance.t770JumlahDiajukan}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: approvalT770Instance, field: 't770JumlahSeharusnya', 'error')} ">
	<label class="control-label" for="t770JumlahSeharusnya">
		<g:message code="approvalT770.t770JumlahSeharusnya.label" default="T770 Jumlah Seharusnya" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="t770JumlahSeharusnya" value="${approvalT770Instance.t770JumlahSeharusnya}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: approvalT770Instance, field: 't770NoDokumen', 'error')} ">
	<label class="control-label" for="t770NoDokumen">
		<g:message code="approvalT770.t770NoDokumen.label" default="T770 No Dokumen" />
		
	</label>
	<div class="controls">
	<g:textField name="t770NoDokumen" value="${approvalT770Instance?.t770NoDokumen}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: approvalT770Instance, field: 't770Pesan', 'error')} ">
	<label class="control-label" for="t770Pesan">
		<g:message code="approvalT770.t770Pesan.label" default="T770 Pesan" />
		
	</label>
	<div class="controls">
	<g:textField name="t770Pesan" value="${approvalT770Instance?.t770Pesan}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: approvalT770Instance, field: 't770Status', 'error')} ">
	<label class="control-label" for="t770Status">
		<g:message code="approvalT770.t770Status.label" default="T770 Status" />
		
	</label>
	<div class="controls">
	<g:select name="t770Status" from="${com.kombos.parts.StatusApproval?.values()}" keys="${com.kombos.parts.StatusApproval.values()*.name()}" required="" value="${approvalT770Instance?.t770Status?.name()}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: approvalT770Instance, field: 't770TglJamSend', 'error')} ">
	<label class="control-label" for="t770TglJamSend">
		<g:message code="approvalT770.t770TglJamSend.label" default="T770 Tgl Jam Send" />
		
	</label>
	<div class="controls">
	<ba:datePicker name="t770TglJamSend" precision="day" value="${approvalT770Instance?.t770TglJamSend}" format="yyyy-MM-dd"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: approvalT770Instance, field: 't770xNamaUser', 'error')} ">
	<label class="control-label" for="t770xNamaUser">
		<g:message code="approvalT770.t770xNamaUser.label" default="T770x Nama User" />
		
	</label>
	<div class="controls">
	<g:textField name="t770xNamaUser" value="${approvalT770Instance?.t770xNamaUser}" />
	</div>
</div>

