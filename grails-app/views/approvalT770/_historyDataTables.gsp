
<%@ page import="com.kombos.maintable.ApprovalT770" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="approvalHistoryT770_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
			<th style="border-bottom: none;padding: 5px;">
				<div>Approver</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Status</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Tanggal</div>
			</th>
		</tr>
	</thead>
</table>

<g:javascript>
var approvalHistoryT770Table;
var showHistory;
$(function(){

showHistory = function(id) {
		$('#spinner').fadeIn(1);
		$.ajax({type:'POST', url:'${request.contextPath}/approvalT770/showHistory/'+id,
			success:function(data,textStatus){
				//$('#approvalT770-form').html(data);
				$('#waktuApproval_value').html(data.waktuApproval);
				$('#statusKegiatan_value').html(data.statusApproval);
				$('#pesanApprover_value').html(data.pesanApprover);
				$('#historyDetail').show();
			},
			error:function(XMLHttpRequest,textStatus,errorThrown){},
			complete:function(XMLHttpRequest,textStatus){
				$('#spinner').fadeOut();
			}
		});
	};
	
	$('#approvalHistoryT770_datatables').delegate('tr', 'click', function (e) {
		var $this = $(this)
		$this.addClass('row_selected').siblings().removeClass('row_selected');
		showHistory($this.find('input[type=hidden]').val());
    });
	
approvalHistoryT770Table = $('#approvalHistoryT770_datatables').dataTable({
		"sScrollX": "98%",
		"bScrollCollapse": true,
		"bAutoWidth" : true,
		"bPaginate" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : 30, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "historyDatatablesList")}",
		"aoColumns": [

{
	"sName": "approver",
	"mDataProp": "approver",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="hidden" value="'+row['id']+'">'+data;
	},
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"300px",
	"bVisible": true
},
{
	"sName": "status",
	"mDataProp": "status",
	"aTargets": [1],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "tanggal",
	"mDataProp": "tanggal",
	"aTargets": [10],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						aoData.push(
								{'name': 'sCriteria_approval', 'value': '${approvalT770Instance?.id}'}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
								if(json.iTotalDisplayRecords > 0){
									showHistory(json.aaData[0].id);
									$('#approvalHistoryT770_datatables').find('tbody').find('tr:first').addClass('row_selected');
								}
							},
							"complete": function () {
							}
						});
		}			
	});
});
</g:javascript>


			
