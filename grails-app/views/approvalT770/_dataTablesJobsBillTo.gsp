
<%@ page import="com.kombos.administrasi.Operation" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="operationBillTo_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="operation.m053Id.label" default="Kode Job" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="operation.m053NamaOperation.label" default="Nama Job" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="default.discount.label" default="Discount" /></div>
        </th>


    </tr>
    </thead>
</table>

<g:javascript>
var operationBillToTable;
var reloadOperationBillToTable;
$(function(){

	reloadOperationBillToTable = function() {
		operationBillToTable.fnDraw();
	}

    var recordsoperationBillToperpage = [];
    var anOperationSelected;
    var jmlRecOperationPerPage=0;
    var id;

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	operationBillToTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	operationBillToTable = $('#operationBillTo_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsOperation = $("#operationBillTo_datatables tbody .row-select");
            var jmlOperationCek = 0;
            var nRow;
            var idRec;
            rsOperation.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsoperationBillToperpage[idRec]=="1"){
                    jmlOperationCek = jmlOperationCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsoperationBillToperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecOperationPerPage = rsOperation.length;
            if(jmlOperationCek==jmlRecOperationPerPage && jmlRecOperationPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
        "fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesJobsList")}",
		"aoColumns": [

{
	"sName": "m053Id",
	"mDataProp": "m053Id",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m053NamaOperation",
	"mDataProp": "m053NamaOperation",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "discount",
	"mDataProp": "discount",
	"aTargets": [2],
    "mRender": function ( data, type, row ) {
        return '<input id="discJobs-'+row['id']+'" class="inline-edit" type="text" style="width:80px;" value="'+data+'">';
    },
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

            aoData.push(
                {"name": 'noWo', "value": "${receptionInstance?.t401NoWO}"},
                {"name": 'billTo', "value": "${billToInstance?.billTo}"},
                {"name": 'namaBillTo', "value": "${billToInstance?.namaBillTo}"},
                {"name": 'statusBillTo', "value": "${staBD}"}
            );

            $.ajax({ "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData ,
                "success": function (json) {
                    fnCallback(json);
                   },
                "complete": function () {
                   }
            });
		}
	});
	$('.select-all').click(function(e) {
        $("#operationBillTo_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsoperationBillToperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsoperationBillToperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

	$('#operationBillTo_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsoperationBillToperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anOperationSelected = operationBillToTable.$('tr.row_selected');
            if(jmlRecOperationPerPage == anOperationSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsoperationBillToperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>