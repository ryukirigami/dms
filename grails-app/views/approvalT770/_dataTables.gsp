
<%@ page import="com.kombos.maintable.ApprovalT770" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="approvalT770_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="approvalT770.kegiatanApproval.label" default="Nama Kegiatan" /></div>
			</th>
            <th style="border-bottom: none;padding: 5px;">
                <div>
                    <g:message code="approvalT770.nomorNamaDocument.label" default="  Nomor" /><br>
                    <g:message code="approvalT770.nomorNamaDocument.label" default="&lt;Nama Dokumen&gt;" />
                </div>
            </th>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="approvalT770.t770xNamaUser.label" default="Pemohon" /></div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="approvalT770.t770TglJamSend.label" default="Tanggal" /></div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="approvalT770.t770Status.label" default="Status" /></div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Current<br>Level</div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="approvalT770.t770Status.label" default="Durasi /Jam" /></div>
            </th>
		</tr>
		<tr>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_kegiatanApproval" style="padding-top: 0px;position:relative; margin-top: 0px;width: 115%;">
					<input type="text" name="search_kegiatanApproval" class="search_init" />
				</div>
			</th>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t770NoDokumen" style="padding-top: 0px;position:relative; margin-top: 0px;width: 115%;">
					<input type="text" name="search_t770NoDokumen" class="search_init" />
				</div>
			</th>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t770xNamaUser" style="padding-top: 0px;position:relative; margin-top: 0px;width: 110%;">
					<input type="text" name="search_t770xNamaUser" class="search_init" />
				</div>
			</th>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t770TglJamSend" style="padding-top: 0px;position:relative; margin-top: 0px;width: 100%;" >
					<input type="hidden" name="search_t770TglJamSend" value="date.struct">
					<input type="hidden" name="search_t770TglJamSend_day" id="search_t770TglJamSend_day" value="">
					<input type="hidden" name="search_t770TglJamSend_month" id="search_t770TglJamSend_month" value="">
					<input type="hidden" name="search_t770TglJamSend_year" id="search_t770TglJamSend_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t770TglJamSend_dp" value="" id="search_t770TglJamSend" class="search_init">
				</div>
			</th>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t770Status" style="padding-top: 0px;position:relative; margin-top: 0px;width: 120%;">
					<input type="text" name="search_t770Status" class="search_init" />
				</div>
			</th>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t770CurrentLevel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 100%;">
					<input type="text" name="search_t770CurrentLevel" class="search_init" />
				</div>
			</th>
            <th style="border-top: none;padding: 5px;">
                <div id="filter_durasi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 100%;">
                    <input type="text" name="search_durasi" class="search_init" />
                </div>
            </th>
		</tr>
	</thead>
</table>

<g:javascript>
var approvalT770Table;
var reloadApprovalT770Table;
var show;
$(function(){
	show = function(id) {
		$('#spinner').fadeIn(1);
		$.ajax({type:'POST', url:'${request.contextPath}/approvalT770/show/'+id,
			success:function(data,textStatus){
				$('#approvalT770-form').html(data);
			},
			error:function(XMLHttpRequest,textStatus,errorThrown){},
			complete:function(XMLHttpRequest,textStatus){
				$('#spinner').fadeOut();
			}
		});
	};
	
	reloadApprovalT770Table = function() {
		approvalT770Table.fnDraw();
	}

	
	$('#search_t770TglJamSend').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t770TglJamSend_day').val(newDate.getDate());
			$('#search_t770TglJamSend_month').val(newDate.getMonth()+1);
			$('#search_t770TglJamSend_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			approvalT770Table.fnDraw();	
	});

	


$("#approvalT770_datatables th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	approvalT770Table.fnDraw();
		}
	});
	$("#approvalT770_datatables th div input").click(function (e) {
	 	e.stopPropagation();
	});
	
	$('#approvalT770_datatables').delegate('tr', 'click', function (e) {
		var $this = $(this)
		$this.addClass('row_selected').siblings().removeClass('row_selected');
		show($this.find('input[type=hidden]').val());
    });

	approvalT770Table = $('#approvalT770_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : true,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "kegiatanApproval",
	"mDataProp": "kegiatanApproval",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"500px",
	"bVisible": true
},
{
	"sName": "t770NoDokumen",
	"mDataProp": "t770NoDokumen",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"400px",
	"bVisible": true
},
{
	"sName": "t770xNamaUser",
	"mDataProp": "t770xNamaUser",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "t770TglJamSend",
	"mDataProp": "t770TglJamSend",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"90px",
	"bVisible": true
},
{
	"sName": "t770Status",
	"mDataProp": "t770Status",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"133px",
	"bVisible": true
},

{
	"sName": "t770CurrentLevel",
	"mDataProp": "t770CurrentLevel",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"30px",
	"bVisible": true
}
,

{	"sName": "",
	"mDataProp": "durasi",
		"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"133px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                        aoData.push(
                                {"name": 'statusCari', "value": $('#paramsCari').val()}
                        );

						var kegiatanApproval = $('#filter_kegiatanApproval input').val();
						if(kegiatanApproval){
							aoData.push(
									{"name": 'sCriteria_kegiatanApproval', "value": kegiatanApproval}
							);
						}
	
						var t770CurrentLevel = $('#filter_t770CurrentLevel input').val();
						if(t770CurrentLevel){
							aoData.push(
									{"name": 'sCriteria_t770CurrentLevel', "value": t770CurrentLevel}
							);
						}
	
						var t770NoDokumen = $('#filter_t770NoDokumen input').val();
						if(t770NoDokumen){
							aoData.push(
									{"name": 'sCriteria_t770NoDokumen', "value": t770NoDokumen}
							);
						}
	
	
						var t770Status = $('#filter_t770Status input').val();
						if(t770Status){
							aoData.push(
									{"name": 'sCriteria_t770Status', "value": t770Status}
							);
						}

						var t770TglJamSend = $('#search_t770TglJamSend').val();
						var t770TglJamSendDay = $('#search_t770TglJamSend_day').val();
						var t770TglJamSendMonth = $('#search_t770TglJamSend_month').val();
						var t770TglJamSendYear = $('#search_t770TglJamSend_year').val();
						
						if(t770TglJamSend){
							aoData.push(
									{"name": 'sCriteria_t770TglJamSend', "value": "date.struct"},
									{"name": 'sCriteria_t770TglJamSend_dp', "value": t770TglJamSend},
									{"name": 'sCriteria_t770TglJamSend_day', "value": t770TglJamSendDay},
									{"name": 'sCriteria_t770TglJamSend_month', "value": t770TglJamSendMonth},
									{"name": 'sCriteria_t770TglJamSend_year', "value": t770TglJamSendYear}
							);
						}
	
						var t770xNamaUser = $('#filter_t770xNamaUser input').val();
						if(t770xNamaUser){
							aoData.push(
									{"name": 'sCriteria_t770xNamaUser', "value": t770xNamaUser}
							);
						}

						var kegiatanCari = "${kegiatan}";
						if(kegiatanCari){
                            aoData.push(
									{"name": 'sCriteria_kegiatanCari', "value": kegiatanCari}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
								if(json.iTotalDisplayRecords > 0){
									show(json.aaData[0].id);
									$('#approvalT770_datatables').find('tbody').find('tr:first').addClass('row_selected');
								}
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
