<%--
  Created by IntelliJ IDEA.
  User: Ahmad Fawaz
  Date: 10/02/15
  Time: 11:10
--%>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="approval.detail.label" default="Detail Approval" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">

    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="approval.detail.label" default="Detail Approval" /></span>
</div>
<div class="box">
    <div class="span12" id="billToDiscount-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <table>
            <tr>
                <td colspan="2" style="padding: 5px">
                    ${staBD}
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    Nomor WO
                </td>
                <td style="padding: 5px">
                    <g:textField name="noWoBillTo" id="noWoBillTo" value="${receptionInstance?.t401NoWO}" readonly="" />
                </td>
            </tr>
            <g:if test="${staBD=='Bill To'}">
                <tr>
                    <td style="padding: 5px">
                        Jenis Pembayaran
                    </td>
                    <td style="padding: 5px">
                        <g:radioGroup name="jenisBayar" id="jenisBayar" values="['Tunai','Kredit']" labels="['Tunai','Kredit']" value="${billToInstance?.jenisBayar}" readonly="" required="" >
                            ${it.radio} ${it.label}
                        </g:radioGroup>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        Bill To
                    </td>
                    <td style="padding: 5px">
                        <g:select name="from" id="from" from="${["Customer","Asuransi","Dealer"]}" value="${billToInstance?.billTo}" readonly="" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        Bill To Name
                    </td>
                    <td style="padding: 5px">
                        <g:textField name="txtBillTo" id="txtBillTo" readonly="" value="${billToInstance?.namaBillTo}"/>
                    </td>
                </tr>
            </g:if>
        </table>
        <br/>
        <g:render template="dataTablesJobsBillTo" />
        <g:render template="dataTablesPartsBillTo" />
    </div>
    <div class="span7" id="billToDiscount-form" style="display: none;"></div>
</div>
</body>
</html>