<%@ page import="com.kombos.administrasi.KegiatanApproval"%>
<g:javascript>
var approvalSent;
$(function(){ 
	$("#formPersetujuanApprovalModal").on("show", function() {
			$("#requestEditBookingFeeModal .btn").on("click", function(e) {
				$("#requestEditBookingFeeModal").modal('hide');
			});
		});
	$("#formPersetujuanApprovalModal").on("hide", function() {
			            $("#requestEditBookingFeeModal a.btn").off("click");
		});
	
	approvalSent = function(){
		$('#formPersetujuanApprovalModal').modal('hide')
		toastr.success('<div>Permohonan approval berhasil direspon.</div>');
		show(${approvalT770Instance?.id});
	}
});

    function cekStatus(){
        if($('#status').val()=='1'){
            $('#discJasa').attr("disabled",false);
            $('#discPart').attr("disabled",false);
            $('#discMaterial').attr("disabled",false);
            $('#discOli').attr("disabled",false);
        }else{
            $('#discJasa').val('');
            $('#discPart').val('');
            $('#discJasa').attr("disabled",true);
            $('#discPart').attr("disabled",true);
            $('#discMaterial').val('');
            $('#discOli').val('');
            $('#discMaterial').attr("disabled",true);
            $('#discOli').attr("disabled",true);
        }
    }
    $('#discJasa').autoNumeric('init',{
        vMin:'0',
        vMax:'10',
        mDec: null,
        aSep:''
    });

    $('#discPart').autoNumeric('init',{
        vMin:'0',
        vMax:'10',
        mDec: null,
        aSep:''
    });

</g:javascript>
<div id="formPersetujuanApprovalModal" class="modal hide">
	<div class="modal-dialog">
		<div class="modal-content">
			<g:formRemote class="form-horizontal" name="create"
				on404="alert('not found!');" onSuccess="approvalSent();"
				url="[controller: 'approvalT770', action:'respondApproval']">
				<input type="hidden" name="id" id="id" value="${approvalT770Instance?.id}">
				<!-- dialog body -->
				<div class="modal-body" style="max-height: 500px;">
					<div id="formPersetujuanApprovalContent">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<div class="iu-content">
							<div class="control-group fieldcontain">
								<label class="control-label" for="kegiatanApproval">Nama
									Kegiatan</label>
								<div class="controls">
									<g:field type="text" name="kegiatanApproval"
										value="${approvalT770Instance?.kegiatanApproval?.encodeAsHTML()}"
										readonly="readonly" />
								</div>
							</div>
							<div class="control-group fieldcontain">
								<label class="control-label" for="noDokumen">Nomor
									&lt;nama dokumen&gt;</label>
								<div class="controls">
									<g:field type="text" name="noDokumen"
										value="${approvalT770Instance?.t770NoDokumen}"
										readonly="readonly" />
								</div>
							</div>
							<div class="control-group fieldcontain">
								<label class="control-label" for="pesanPemohon">Pesan
									dari pemohon</label>
								<div class="controls">
									<g:textArea rows="4" cols="50" maxlength="20"
										name="pesanPemohon" value="${approvalT770Instance?.t770Pesan}"
										readonly="readonly" />
								</div>
							</div>
							<div class="control-group fieldcontain">
								<label class="control-label" for="t770TglJamSend">Tanggal
									permohonan</label>
								<div class="controls">
									<g:field type="text" name="t770TglJamSend"
										value="${approvalT770Instance?.t770TglJamSend}"
										readonly="readonly" />
								</div>
							</div>
							<div class="control-group fieldcontain">
								<label class="control-label" for="namaApprover">Nama
									approver</label>
								<div class="controls">
									<g:field type="text" name="namaApprover"
										value="${user}"
										readonly="readonly" />
								</div>
							</div>
							<div class="control-group fieldcontain">
								<label class="control-label" for="t770CurrentLevel">Current
									level of approver</label>
								<div class="controls">
									<g:field type="text" name="t770CurrentLevel"
										value="${approvalT770Instance?.t770CurrentLevel + 1} of ${totalLevel}"
										readonly="readonly" />
								</div>
							</div>

							<div class="control-group fieldcontain">
								<label class="control-label" for="status">Status
									kegiatan</label>
								<div class="controls">
									<g:select name="status" id="status" onchange="cekStatus();" keys="${['1', '2']}"
										from="${['APPROVED', 'REJECTED']}" />
								</div>
							</div>
							<div class="control-group fieldcontain">
								<label class="control-label" for="keterangan">Pesan dari approver</label>
								<div class="controls">
									<g:textArea rows="4" cols="50" maxlength="20"
										name="keterangan" value=""
										/>
								</div>
							</div>

						</div>
					</div>
				</div>
				<!-- dialog buttons -->
				<div class="modal-footer">
                <g:if test="${approvalT770Instance?.kegiatanApproval?.m770KegiatanApproval==com.kombos.administrasi.KegiatanApproval.REQUEST_SPECIAL_DISCOUNT}">
                    <button type="button" class="btn btn-primary pull-left" onclick="openDetail();" >Detail</button>
                </g:if>
					<g:submitButton class="btn btn-primary create" name="send"
						value="${message(code: 'default.button.send.label', default: 'Send')}" />
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</g:formRemote>
		</div>
	</div>
</div>




