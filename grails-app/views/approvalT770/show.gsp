<%@ page import="com.kombos.maintable.ApprovalT770" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'approvalT770.label', default: 'Approval')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteApprovalT770;
var approve; //ieu
$(function(){ 
	deleteApprovalT770=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/approvalT770/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadApprovalT770Table();
   				expandTableLayout('approvalT770');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
	
	approve=function(id){
		$("#formPersetujuanApprovalModal").modal({
							"backdrop" : "static",
							"keyboard" : true,
							"show" : true
						}).css({'width': '600px','margin-left': function () {return -($(this).width() / 2);}});
		//$.ajax({type:'POST', url:'${request.contextPath}/approvalT770/approve/',
		//	data: { id: id },
   		//	success:function(data,textStatus){
   		//	},
   		//	error:function(XMLHttpRequest,textStatus,errorThrown){},
   		//	complete:function(XMLHttpRequest,textStatus){
   		//		$('#spinner').fadeOut();
   		//	}
   		//});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-approvalT770" role="main">
		<legend>
			Detail
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<fieldset>
			<legend>History</legend>
			<g:render template="historyDataTables" />
		</fieldset>
		<fieldset id="historyDetail" class="hide">
			<legend>Current Approval</legend>
		<table 
			class="table table-bordered table-hover">
			<tbody>				
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kegiatanApproval-label" class="property-label"><g:message
					code="approvalT770.kegiatanApproval.label" default="Kegiatan Approval" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kegiatanApproval-label">
						${approvalT770Instance?.kegiatanApproval?.encodeAsHTML()}							
						</span></td>
				</tr>
				
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t770NoDokumen-label" class="property-label"><g:message
					code="approvalT770.t770NoDokumen.label" default="Nomor &lt;Nama Dokumen&gt;" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t770NoDokumen-label">
						<g:fieldValue bean="${approvalT770Instance}" field="t770NoDokumen"/>
							
						</span></td>
					
				</tr>
				
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t770TglJamSend-label" class="property-label"><g:message
					code="approvalT770.t770TglJamSend.label" default="Waktu permohonan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t770TglJamSend-label">
							
								<g:formatDate date="${approvalT770Instance?.t770TglJamSend}" />
							
						</span></td>
					
				</tr>
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="waktuApproval" class="property-label">Waktu approval:</span></td>
					
						<td class="span3"><span id="waktuApproval_value" class="property-value"
						aria-labelledby="waktuApproval">				
								
							
						</span></td>
					
				</tr>
				<td class="span2" style="text-align: right;"><span
					id="statusKegiatan" class="property-label">Status kegiatan:</span></td>
					
						<td class="span3"><span id="statusKegiatan_value" class="property-value"
						aria-labelledby="statusKegiatan">				
								
							
						</span></td>
					
				</tr>
				
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t770Pesan-label" class="property-label"><g:message
					code="approvalT770.t770Pesan.label" default="Pesan dari pemohon" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t770Pesan-label">
						<g:fieldValue bean="${approvalT770Instance}" field="t770Pesan"/>
							
						</span></td>
					
				</tr>
				
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="pesanApprover" class="property-label">Pesan dari approver:</span></td>
					
						<td class="span3"><span id="pesanApprover_value" class="property-value"
						aria-labelledby="pesanApprover">
						</span></td>
					
				</tr>
			</tbody>
		</table>
		</fieldset>
		<g:if test="${currentApprover}">
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn" href="javascript:void(0);"
					onclick="approve('${approvalT770Instance?.id}');">Respond</a>		
			</fieldset>
		</g:form>
		</g:if>
	</div>
	<g:render template="formPersetujuanApproval" />
</body>
</html>
