<div id="show-approvalT770" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="approvalT770"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${approvalT770Instance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="approvalT770.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${approvalT770Instance}" field="createdBy"
								url="${request.contextPath}/ApprovalT770/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadApprovalT770Table();" />--}%
							
								<g:fieldValue bean="${approvalT770Instance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${approvalT770Instance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="approvalT770.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${approvalT770Instance}" field="dateCreated"
								url="${request.contextPath}/ApprovalT770/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadApprovalT770Table();" />--}%
							
								<g:formatDate date="${approvalT770Instance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${approvalT770Instance?.kegiatanApproval}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kegiatanApproval-label" class="property-label"><g:message
					code="approvalT770.kegiatanApproval.label" default="Kegiatan Approval" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kegiatanApproval-label">
						%{--<ba:editableValue
								bean="${approvalT770Instance}" field="kegiatanApproval"
								url="${request.contextPath}/ApprovalT770/updatefield" type="text"
								title="Enter kegiatanApproval" onsuccess="reloadApprovalT770Table();" />--}%
							
								<g:link controller="kegiatanApproval" action="show" id="${approvalT770Instance?.kegiatanApproval?.id}">${approvalT770Instance?.kegiatanApproval?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${approvalT770Instance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="approvalT770.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${approvalT770Instance}" field="lastUpdProcess"
								url="${request.contextPath}/ApprovalT770/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadApprovalT770Table();" />--}%
							
								<g:fieldValue bean="${approvalT770Instance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${approvalT770Instance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="approvalT770.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${approvalT770Instance}" field="lastUpdated"
								url="${request.contextPath}/ApprovalT770/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadApprovalT770Table();" />--}%
							
								<g:formatDate date="${approvalT770Instance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${approvalT770Instance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="approvalT770.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${approvalT770Instance}" field="staDel"
								url="${request.contextPath}/ApprovalT770/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadApprovalT770Table();" />--}%
							
								<g:fieldValue bean="${approvalT770Instance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${approvalT770Instance?.t770CurrentLevel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t770CurrentLevel-label" class="property-label"><g:message
					code="approvalT770.t770CurrentLevel.label" default="T770 Current Level" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t770CurrentLevel-label">
						%{--<ba:editableValue
								bean="${approvalT770Instance}" field="t770CurrentLevel"
								url="${request.contextPath}/ApprovalT770/updatefield" type="text"
								title="Enter t770CurrentLevel" onsuccess="reloadApprovalT770Table();" />--}%
							
								<g:fieldValue bean="${approvalT770Instance}" field="t770CurrentLevel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${approvalT770Instance?.t770FK}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t770FK-label" class="property-label"><g:message
					code="approvalT770.t770FK.label" default="T770 FK" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t770FK-label">
						%{--<ba:editableValue
								bean="${approvalT770Instance}" field="t770FK"
								url="${request.contextPath}/ApprovalT770/updatefield" type="text"
								title="Enter t770FK" onsuccess="reloadApprovalT770Table();" />--}%
							
								<g:fieldValue bean="${approvalT770Instance}" field="t770FK"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${approvalT770Instance?.t770JumlahDiajukan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t770JumlahDiajukan-label" class="property-label"><g:message
					code="approvalT770.t770JumlahDiajukan.label" default="T770 Jumlah Diajukan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t770JumlahDiajukan-label">
						%{--<ba:editableValue
								bean="${approvalT770Instance}" field="t770JumlahDiajukan"
								url="${request.contextPath}/ApprovalT770/updatefield" type="text"
								title="Enter t770JumlahDiajukan" onsuccess="reloadApprovalT770Table();" />--}%
							
								<g:fieldValue bean="${approvalT770Instance}" field="t770JumlahDiajukan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${approvalT770Instance?.t770JumlahSeharusnya}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t770JumlahSeharusnya-label" class="property-label"><g:message
					code="approvalT770.t770JumlahSeharusnya.label" default="T770 Jumlah Seharusnya" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t770JumlahSeharusnya-label">
						%{--<ba:editableValue
								bean="${approvalT770Instance}" field="t770JumlahSeharusnya"
								url="${request.contextPath}/ApprovalT770/updatefield" type="text"
								title="Enter t770JumlahSeharusnya" onsuccess="reloadApprovalT770Table();" />--}%
							
								<g:fieldValue bean="${approvalT770Instance}" field="t770JumlahSeharusnya"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${approvalT770Instance?.t770NoDokumen}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t770NoDokumen-label" class="property-label"><g:message
					code="approvalT770.t770NoDokumen.label" default="T770 No Dokumen" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t770NoDokumen-label">
						%{--<ba:editableValue
								bean="${approvalT770Instance}" field="t770NoDokumen"
								url="${request.contextPath}/ApprovalT770/updatefield" type="text"
								title="Enter t770NoDokumen" onsuccess="reloadApprovalT770Table();" />--}%
							
								<g:fieldValue bean="${approvalT770Instance}" field="t770NoDokumen"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${approvalT770Instance?.t770Pesan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t770Pesan-label" class="property-label"><g:message
					code="approvalT770.t770Pesan.label" default="T770 Pesan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t770Pesan-label">
						%{--<ba:editableValue
								bean="${approvalT770Instance}" field="t770Pesan"
								url="${request.contextPath}/ApprovalT770/updatefield" type="text"
								title="Enter t770Pesan" onsuccess="reloadApprovalT770Table();" />--}%
							
								<g:fieldValue bean="${approvalT770Instance}" field="t770Pesan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${approvalT770Instance?.t770Status}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t770Status-label" class="property-label"><g:message
					code="approvalT770.t770Status.label" default="T770 Status" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t770Status-label">
						%{--<ba:editableValue
								bean="${approvalT770Instance}" field="t770Status"
								url="${request.contextPath}/ApprovalT770/updatefield" type="text"
								title="Enter t770Status" onsuccess="reloadApprovalT770Table();" />--}%
							
								<g:fieldValue bean="${approvalT770Instance}" field="t770Status"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${approvalT770Instance?.t770TglJamSend}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t770TglJamSend-label" class="property-label"><g:message
					code="approvalT770.t770TglJamSend.label" default="T770 Tgl Jam Send" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t770TglJamSend-label">
						%{--<ba:editableValue
								bean="${approvalT770Instance}" field="t770TglJamSend"
								url="${request.contextPath}/ApprovalT770/updatefield" type="text"
								title="Enter t770TglJamSend" onsuccess="reloadApprovalT770Table();" />--}%
							
								<g:formatDate date="${approvalT770Instance?.t770TglJamSend}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${approvalT770Instance?.t770xNamaUser}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t770xNamaUser-label" class="property-label"><g:message
					code="approvalT770.t770xNamaUser.label" default="T770x Nama User" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t770xNamaUser-label">
						%{--<ba:editableValue
								bean="${approvalT770Instance}" field="t770xNamaUser"
								url="${request.contextPath}/ApprovalT770/updatefield" type="text"
								title="Enter t770xNamaUser" onsuccess="reloadApprovalT770Table();" />--}%
							
								<g:fieldValue bean="${approvalT770Instance}" field="t770xNamaUser"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${approvalT770Instance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="approvalT770.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${approvalT770Instance}" field="updatedBy"
								url="${request.contextPath}/ApprovalT770/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadApprovalT770Table();" />--}%
							
								<g:fieldValue bean="${approvalT770Instance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('approvalT770');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${approvalT770Instance?.id}"
					update="[success:'approvalT770-form',failure:'approvalT770-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteApprovalT770('${approvalT770Instance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>