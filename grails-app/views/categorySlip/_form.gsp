<%@ page import="com.kombos.parts.CategorySlip" %>

<g:javascript>
    var getNameAccount;
    $(function () {
        $('#accountNumbers').typeahead({
            source: function (query, process) {
                return $.get('${request.contextPath}/franc/listAccount', { query: query }, function (data) {
                    return process(data.options);
                });
            }
        });
        getNameAccount = function(){
            if($('#accountNumbers').val() && $('#accountNumbers').val()!="" && $('#accountNumbers').val().replace(" ","")!=""){
                $.ajax({
                    type: 'POST',
                    url: '${request.contextPath}/categorySlip/namaAccount',
                    data: {accNumber:$("#accountNumbers").val()},
                    success: function (data) {
                        $("#name").val(data);
                    },
                    error: function () {
                        alert("internal server error");
                    }
                });
            }
        }
    });
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: categorySlipInstance, field: 'categoryCode', 'error')} required">
	<label class="control-label" for="categoryCode">
		<g:message code="categorySlip.categoryCode.label" default="Category Code" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="categoryCode" maxlength="12" required="" value="${categorySlipInstance?.categoryCode}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: categorySlipInstance, field: 'accountNumber', 'error')} required">
    <label class="control-label" for="accountNumber">
        <g:message code="categorySlip.accountNumber.label" default="Account Number" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="accountNumbers" id="accountNumbers" maxlength="32" onblur="getNameAccount();" required="" value="${categorySlipInstance?.accountNumber?.accountNumber}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: categorySlipInstance, field: 'name', 'error')} required">
	<label class="control-label" for="name">
		<g:message code="categorySlip.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="name" id="name" maxlength="32" required="" value="${categorySlipInstance?.name}"/>
	</div>
</div>
