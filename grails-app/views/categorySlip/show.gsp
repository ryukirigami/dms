

<%@ page import="com.kombos.parts.CategorySlip" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'categorySlip.label', default: 'Kategory Slip')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteCategorySlip;

$(function(){ 
	deleteCategorySlip=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/categorySlip/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadCategorySlipTable();
   				expandTableLayout('categorySlip');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-categorySlip" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="categorySlip"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${categorySlipInstance?.categoryCode}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="categoryCode-label" class="property-label"><g:message
					code="categorySlip.categoryCode.label" default="Category Code" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="categoryCode-label">
						%{--<ba:editableValue
								bean="${categorySlipInstance}" field="categoryCode"
								url="${request.contextPath}/CategorySlip/updatefield" type="text"
								title="Enter categoryCode" onsuccess="reloadCategorySlipTable();" />--}%
							
								<g:fieldValue bean="${categorySlipInstance}" field="categoryCode"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${categorySlipInstance?.name}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="name-label" class="property-label"><g:message
					code="categorySlip.name.label" default="Name" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="name-label">
						%{--<ba:editableValue
								bean="${categorySlipInstance}" field="name"
								url="${request.contextPath}/CategorySlip/updatefield" type="text"
								title="Enter name" onsuccess="reloadCategorySlipTable();" />--}%
							
								<g:fieldValue bean="${categorySlipInstance}" field="name"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${categorySlipInstance?.accountNumber}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="accountNumber-label" class="property-label"><g:message
					code="categorySlip.accountNumber.label" default="Account Number" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="accountNumber-label">
						%{--<ba:editableValue
								bean="${categorySlipInstance}" field="accountNumber"
								url="${request.contextPath}/CategorySlip/updatefield" type="text"
								title="Enter accountNumber" onsuccess="reloadCategorySlipTable();" />--}%
							
								<g:fieldValue bean="${categorySlipInstance}" field="accountNumber"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${categorySlipInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="categorySlip.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${categorySlipInstance}" field="staDel"
								url="${request.contextPath}/CategorySlip/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadCategorySlipTable();" />--}%
							
								<g:fieldValue bean="${categorySlipInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${categorySlipInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="categorySlip.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${categorySlipInstance}" field="createdBy"
								url="${request.contextPath}/CategorySlip/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadCategorySlipTable();" />--}%
							
								<g:fieldValue bean="${categorySlipInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${categorySlipInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="categorySlip.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${categorySlipInstance}" field="updatedBy"
								url="${request.contextPath}/CategorySlip/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadCategorySlipTable();" />--}%
							
								<g:fieldValue bean="${categorySlipInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${categorySlipInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="categorySlip.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${categorySlipInstance}" field="lastUpdProcess"
								url="${request.contextPath}/CategorySlip/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadCategorySlipTable();" />--}%
							
								<g:fieldValue bean="${categorySlipInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${categorySlipInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="categorySlip.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${categorySlipInstance}" field="dateCreated"
								url="${request.contextPath}/CategorySlip/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadCategorySlipTable();" />--}%
							
								<g:formatDate date="${categorySlipInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${categorySlipInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="categorySlip.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${categorySlipInstance}" field="lastUpdated"
								url="${request.contextPath}/CategorySlip/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadCategorySlipTable();" />--}%
							
								<g:formatDate date="${categorySlipInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('categorySlip');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${categorySlipInstance?.id}"
					update="[success:'categorySlip-form',failure:'categorySlip-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteCategorySlip('${categorySlipInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
