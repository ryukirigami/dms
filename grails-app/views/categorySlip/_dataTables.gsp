
<%@ page import="com.kombos.parts.CategorySlip" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="categorySlip_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="categorySlip.categoryCode.label" default="Kode Kategori Slip" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="categorySlip.accountNumber.label" default="Nomor Akun" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
				<div><g:message code="categorySlip.name.label" default="Nama Kategori Slip" /></div>
			</th>

		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_categoryCode" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_categoryCode" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_accountNumber" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_accountNumber" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
				<div id="filter_name" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_name" class="search_init" />
				</div>
			</th>

		</tr>
	</thead>
</table>

<g:javascript>
var categorySlipTable;
var reloadCategorySlipTable;
$(function(){
	
	reloadCategorySlipTable = function() {
		categorySlipTable.fnDraw();
	}

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	categorySlipTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	categorySlipTable = $('#categorySlip_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "categoryCode",
	"mDataProp": "categoryCode",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "accountNumber",
	"mDataProp": "accountNumber",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "name",
	"mDataProp": "name",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var categoryCode = $('#filter_categoryCode input').val();
						if(categoryCode){
							aoData.push(
									{"name": 'sCriteria_categoryCode', "value": categoryCode}
							);
						}
	
						var name = $('#filter_name input').val();
						if(name){
							aoData.push(
									{"name": 'sCriteria_name', "value": name}
							);
						}
	
						var accountNumber = $('#filter_accountNumber input').val();
						if(accountNumber){
							aoData.push(
									{"name": 'sCriteria_accountNumber', "value": accountNumber}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
