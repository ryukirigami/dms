<%@ page import="com.kombos.administrasi.CompanyDealer" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="baseapplayout" />
<g:javascript disappointmentGrition="head">
	$(function(){
	    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    });

     previewData = function(){
           var tanggal  = $('#tanggal').val();
           window.location = "${request.contextPath}/laporanHass/previewData?tanggal="+tanggal;
    }
     previewTest = function(){
           var tanggal  = $('#tanggal').val();
           window.location = "${request.contextPath}/laporanHass/previewData?tanggal="+tanggal;
    }
</g:javascript>

</head>
<body>
<div class="navbar box-header no-border">
    Laporan Pembukuan - Hasil Aktivitas Service Station
</div><br>
<div class="box">
    <div class="span12" id="appointmentGr-table">
        <div id="form-appointmentGr" class="form-horizontal">
            <fieldset>
                <div class="control-group">
                    <label class="control-label" for="t951Tanggal" style="text-align: left;">
                        Tanggal :
                    </label>
                    <div id="filter_periode2" class="controls">
                        <ba:datePicker format="dd/mm/yyyy" name="tanggal" />
                    </div>
                </div>

            </fieldset>
        </div>
           <g:field type="button" onclick="previewData()" class="btn btn-primary create" name="preview"  value="${message(code: 'default.button.upload.label', default: 'Preview')}" />
           <g:field type="button" onclick="window.location.replace('#/home')" class="btn btn-cancel cancel" name="cancel"  value="${message(code: 'default.button.upload.label', default: 'Close')}" />
           %{--<g:field type="button" onclick="previewTest()" class="btn btn-primary create" name="preview"  value="${message(code: 'default.button.upload.label', default: 'Preview Test')}" />--}%
    </div>
</div>
</body>
</html>
