
<%@ page import="com.kombos.maintable.EFaktur" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'EFaktur.label', default: 'E Faktur - PPN Keluaran')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>
			var show;
			var edit;
			$(function(){

                $('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :
							loadPath("EFaktur2/uploadData");
							break;
						case '_ALL_' :
							loadPath("EFaktur2/list");
							break;
                        case '_DELETE_' :
         		        bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
         			    function(result){
         				if(result){
         					massDeleteNew();
         				}
         			});

         		break;
				   }
				   return false;
				});
                var checkin = $('#search_Tanggal').datepicker({
                    onRender: function(date) {
                        return '';
                    }
                }).on('changeDate', function(ev) {
                            var newDate = new Date(ev.date)
                            newDate.setDate(newDate.getDate());
                            checkout.setValue(newDate);
                            checkin.hide();
                            $('#search_Tanggal2')[0].focus();
                        }).data('datepicker');

                var checkout = $('#search_Tanggal2').datepicker({
                    onRender: function(date) {
                        return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
                    }
                }).on('changeDate', function(ev) {
                            checkout.hide();
                        }).data('datepicker');

                printEfaktur = function(){
                        checkId =[];
                        $("#EFaktur-table tbody .row-select").each(function() {
                              if(this.checked){
                                 var nRow = $(this).next("input:hidden").val();
                                    checkId.push(nRow);
                              }
                        });

                        if(checkId.length<1 ){
                            alert('Silahkan pilih salah satu No. Dokumen untuk dicetak');
                            return;
                        }

                        var docNumber = JSON.stringify(checkId);
                        window.location = "${request.contextPath}/EFaktur2/exportCSV?docNumber="+docNumber;

                }
                massDeleteNew  = function() {
                    var recordsToDelete = [];
                    $("#EFaktur-table tbody .row-select").each(function() {
                        if(this.checked){
                            var id = $(this).next("input:hidden").val();
                            recordsToDelete.push(id);
                        }
                    });

                    var json = JSON.stringify(recordsToDelete);

                    $.ajax({
                        url:'${request.contextPath}/EFaktur2/massDeleteNew',
                        type: "POST", // Always use POST when deleting data
                        data: { ids: json },
                        success:function(data,textStatus){
                          if(data=="ok"){
                            toastr.success("Success Delete data");
                          }else{
                            alert("DATA TIDAK BISA DIHAPUS KARENA SUDAH ADA NOMOR FAKTUR, HARAP HUBUNGI ADMIN!!");
                          }
                        },
                        complete: function(xhr, status) {
                            reloadEFakturTable ();
                        }
                    });

                }
                 $('#print').click(function(e){
                        printEfaktur();
                        e.stopPropagation();
                        EFakturTable.fnDraw();
                    });

                generateNofak = function(docNumber){
                     var conf = confirm("Apakah Anda Yakin ?")
                     if(conf){
                         jQuery("#generate").click(function (event) {
                            jQuery('#spinner').fadeIn(1);
                            $(this).submit();
                            event.preventDefault();
                        });

                         $.ajax({
                                type:'POST',
                                url:'${request.contextPath}/EFaktur2/generateNofak',
                                data : {docNumber : docNumber},
                                error:function(XMLHttpRequest,textStatus,errorThrown){},
                                complete:function(XMLHttpRequest,textStatus){
                                    $('#spinner').fadeOut();
                                    toastr.success("Sukses Generate Nomor Faktur By System");
                                }
                         });
                               toastr.success("Please Waiting....");


                     }
                }



                generateData = function(){

                     var tgl1= $('#search_Tanggal').val();
                     var tgl2= $('#search_Tanggal2').val();
                     var conf = confirm("Apakah Anda Yakin Generate Data ?")
                     if(conf){
                         if(tgl1==null || tgl1=="" || tgl2==null||tgl2==""){
                            alert("Anda Belum memilih Tanggal");
                            return
                         }

                         jQuery("#generate").click(function (event) {
                            jQuery('#spinner').fadeIn(1);
                            $(this).submit();
                            event.preventDefault();
                        });

                         $.ajax({
                                type:'POST',
                                url:'${request.contextPath}/EFaktur2/generateData',
                                data : {tgl1 : tgl1, tgl2 : tgl2},
                                success : function(data){
                                    if(data==""){
                                        toastr.success("Generate Data Selesai");
                                    }else {
                                        alert(data + "\n Silahkan Hubungi Admin");
                                        toastr.error("Generate Data Gagal, " + data);
                                    }
                                },
                                error:function(XMLHttpRequest,textStatus,errorThrown){},
                                complete:function(XMLHttpRequest,textStatus){
                                    $('#spinner').fadeOut();

                                }
                         });
                               toastr.success("Anda Sedang Melakukan Generate Data");


                     }
                }

                showDetail = function(docNumber){
                    $("#detailContent").empty();
                    $.ajax({
                        type:'POST',
                        url:'${request.contextPath}/EFaktur2/showDetail',
                        data : {docNumber : docNumber},
                        success:function(data,textStatus){
                                $("#detailContent").html(data);
                                $("#detailModal").modal({
                                    "backdrop" : "dynamic",
                                    "keyboard" : true,
                                    "show" : true
                                }).css({'width': '1300px','margin-left': function () {return -($(this).width() / 2);}});

                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
                }
                chekNoEfaktur = function(){
//                    $("#detailContent").empty();
                     checkId =[];
                     $("#EFaktur_datatables1 tbody .row-select").each(function() {
                          if(this.checked){
                                var nRow = $(this).next("input:hidden").val();
                                checkId.push(nRow);
//                                alert("nRow"+nRow);
                          }
                     });
                    $.ajax({
                        type:'POST',
                        url:'${request.contextPath}/EFaktur2/chekNoEfaktur?docNumber='+checkId,
                        data : {docNumber : checkId},
                        success:function(data,textStatus){
                                $("#detailContent").html(data);
                                $("#detailModal").modal({
                                    "backdrop" : "dynamic",
                                    "keyboard" : true,
                                    "show" : true
                                }).css({'width': '1300px','margin-left': function () {return -($(this).width() / 2);}});

                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
                }

});
    </g:javascript>
</head>
<body>

<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
    <ul class="nav pull-right">
        <li>
            <g:if test="${!session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                <a class="pull-right box-action" href="#"
                   style="display: block;" target="_CREATE_">&nbsp;&nbsp;Upload Data</i>&nbsp;&nbsp;
                </a>
            </g:if>
        </li>
        <li class="separator"></li>
        <li>
            <a class="pull-right box-action" href="#"
               style="display: block;" target="_ALL_">&nbsp;&nbsp;List All </i>&nbsp;&nbsp;
            </a>
        </li>
        <li><a class="pull-right box-action" href="#"
               style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
                    class="icon-remove"></i>&nbsp;&nbsp;
        </a></li>
    </ul>
</div>

<div class="box">
    <div class="span12" id="EFaktur-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <fieldset>
            <table style="padding-right: 10px">
                <tr>
                    <td style="width: 130px">
                        <label class="control-label" for="t951Tanggal">
                            <g:message code="auditTrail.wo.label" default="Company Dealer" />&nbsp;
                        </label>&nbsp;&nbsp;
                    </td>
                    <td>
                        <div id="filter_wo" class="controls">
                            <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                <g:select name="companyDealer" id="companyDealer" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" noSelection="${['-':'Semua']}" />
                            </g:if>
                            <g:else>
                                <g:select name="companyDealer" id="companyDealer" readonly="" disabled="" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                            </g:else>
                        </div>
                    </td>
                </tr><tr>
                <td style="width: 130px">
                    <label class="control-label" for="t951Tanggal">
                        <g:message code="auditTrail.tanggal.label" default="Tanggal Upload" />&nbsp;
                        <g:message code="auditTrail.tanggal.label" default="(Sesuai Tgl Service Invoice)" />&nbsp;
                    </label>&nbsp;&nbsp;
                </td>
                <td>
                    <div id="filter_m777Tgl" class="controls">
                        <ba:datePicker id="search_Tanggal" name="search_Tanggal" precision="day" format="dd-MM-yyyy"  value="${new Date()}" />
                        &nbsp;&nbsp;&nbsp;s.d.&nbsp;&nbsp;&nbsp;
                        <ba:datePicker id="search_Tanggal2" name="search_Tanggal2" precision="day" format="dd-MM-yyyy"  value="${new Date()}" />
                    </div>
                </td>

            </tr>
            %{--<g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">--}%

            %{--<tr>--}%
                %{--<td style="width: 130px">--}%
                    %{--<label class="control-label" for="t951Tanggal">--}%
                        %{--<g:message code="auditTrail.wo.label" default="Sisa Nomor Faktur : " />&nbsp;--}%
                    %{--</label>--}%
                %{--</td>--}%
                %{--<td>--}%
                    %{--<div id="sisa" class="controls">--}%
                        %{--<table style="width: 60%">--}%
                            %{--<tr style="width: 80%">--}%
                                %{--<td>--}%
                                    %{--<g:textField class="numberonly" disabled="" name="suffix" value="${com.kombos.maintable.NoEfaktur.list().sisa.sum() }" maxlength="3" style="width: 70px; font-size: 30px; height: 50px; border-top-left-radius: 0; border-bottom-left-radius: 0;" required="" />--}%

                                %{--</td>--}%
                            %{--</tr>--}%
                        %{--</table>--}%
                    %{--</div>--}%
                %{--</td>--}%

            %{--</tr>--}%
            %{--</g:if>    --}%
                <tr>
                    <td colspan="3" >
                        <div class="controls" style="right: 0">
                            <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-primary view" name="view" id="view" >Search</button>
                            <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-cancel clear" name="clear" id="clear" >Clear</button>
                            <g:if test="${!session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                <button style="width: 220px;height: 30px; border-radius: 5px" class="btn btn-primary view" name="generate" id="generate" onclick="generateData()" >Generate Data</button>
                            </g:if>
                            <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                %{--<button style="width: 220px;height: 30px; border-radius: 5px" class="btn btn-primary view" name="nofak" id="nofak" onclick="generateNofak()" >Generate Nomor Faktur</button>--}%
                                  %{--ini buat yang make pop up  --}%
                                <button style="width: 220px;height: 30px; border-radius: 5px" class="btn btn-primary view" name="nofak" id="nofak" onclick="chekNoEfaktur()" >Generate Nomor Faktur</button>
                            </g:if>

                        </div>
                    </td>
                </tr>
            </table>

        </fieldset>
        <g:render template="dataTables1" />
    </div>
    <button style="width: 130px;height: 30px; border-radius: 5px" class="btn btn-primary print" name="print" id="print"  >Download *.csv</button>
</div>
<div id="detailModal" class="modal fade">
    <div class="modal-dialog" style="width: 1300px;">
        <div class="modal-content" style="width: 1300px;">
            <div class="modal-body" style="max-height: 520px;">
                <div id="detailContent"/>
                <div class="iu-content"></div>

            </div>
        </div>
    </div>
</div>
</body>
</html>
