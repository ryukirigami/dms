<%@ page import="com.kombos.maintable.EFaktur" %>

<r:require modules="baseapplayout"/>
<g:render template="../menu/maxLineDisplay"/>

<table id="EFaktur_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="100%">
<thead>
<tr>

<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.ALAMAT_LENGKAP.label" default="FK"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.BLOK.label" default="KD_JENIS_TRANSAKSI"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.DISKON.label" default="FG_PENGGANTI"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.DPP.label" default="NOMOR_FAKTUR"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.FG_PENGGANTI.label" default="MASA_PAJAK"/></div>
</th>

<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.FG_UANG_MUKA.label" default="TAHUN_PAJAK"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.FK.label" default="TANGGAL_FAKTUR"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.HARGA_SATUAN.label" default="NPWP"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.HARGA_TOTAL.label" default="NAMA"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.ID_KETERANGAN_TAMBAHAN.label" default="ALAMAT_LENGKAP"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.JALAN.label" default="JUMLAH_DPP"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.LT.label" default="JUMLAH_PPN"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.MASA_PAJAK.label" default="JUMLAH_PPNBM"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.NAMA.label" default="ID_KETERANGAN_TAMBAHAN"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.NAMA2.label" default="FG_UANG_MUKA"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.NAMA_OBJEK.label" default="UANG_MUKA_DPP"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.NOMOR.label" default="UANG_MUKA_PPN"/></div>
</th>

<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.NOMOR_FAKTUR.label" default="UANG_MUKA_PPNBM"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.NOMOR_TELEPON.label" default="REFERENSI"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.NPWP.label" default="LT"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.NPWP2.label" default="NPWP2"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.OF_.label" default="NAMA2"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.PPN.label" default="JALAN"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.PPNBM.label" default="BLOK"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.PROPINSI.label" default="NOMOR"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.REFERENSI.label" default="RT"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.RT.label" default="RW"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.RW.label" default="KECAMATAN"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.TAHUN_PAJAK.label" default="KELURAHAN"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.TANGGAL_FAKTUR.label" default="KABUPATEN"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.TARIF_PPNBM.label" default="PROPINSI"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.UANG_MUKA_DPP.label" default="KODE_POS"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.UANG_MUKA_PPN.label" default="NOMOR_TELEPON"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.UANG_MUKA_PPNBM.label" default="OF_"/></div>
</th>

<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.companyDealer.label" default="KODE_OBJEK"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.docNumber.label" default="NAMA_OBJEK"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.jenisInputan.label" default="HARGA_SATUAN"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.lastUpdProcess.label" default="JUMLAH_BARANG"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.staAprove.label" default="HARGA_TOTAL"/></div>
</th>

<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.staOriRev.label" default="DISKON"/></div>
</th>

<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.tanggal.label" default="DPP"/></div>
</th>

<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.tanggal.label" default="PPN"/></div>
</th>

<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.tanggal.label" default="TARIF_PPNBM"/></div>
</th>

<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.tanggal.label" default="PPNBM"/></div>
</th>

<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.docNumberr.label" default="docNumber"/></div>
</th>


<th style="border-bottom: none;padding: 5px;width: 300px;">
    <div><g:message code="EFaktur.tanggal.label" default="Status Approval"/></div>
</th>

</tr>
<tr>


<th style="border-top: none;padding: 5px;">
    <div id="filter_FK" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_FK" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_KD_JENIS_TRANSAKSI" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_KD_JENIS_TRANSAKSI" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_FG_PENGGANTI" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_FG_PENGGANTI" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_NOMOR_FAKTUR" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_NOMOR_FAKTUR" class="search_init"/>
    </div>
</th>



<th style="border-top: none;padding: 5px;">
    <div id="filter_MASA_PAJAK" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_MASA_PAJAK" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_TAHUN_PAJAK" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_TAHUN_PAJAK" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_TANGGAL_FAKTUR" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
        <input type="hidden" name="search_TANGGAL_FAKTUR" value="date.struct">
        <input type="hidden" name="search_TANGGAL_FAKTUR_day" id="search_TANGGAL_FAKTUR_day" value="">
        <input type="hidden" name="search_TANGGAL_FAKTUR_month" id="search_TANGGAL_FAKTUR_month" value="">
        <input type="hidden" name="search_TANGGAL_FAKTUR_year" id="search_TANGGAL_FAKTUR_year" value="">
        <input type="text" data-date-format="dd-mm-yyyy" name="search_TANGGAL_FAKTUR_dp" value="" id="search_TANGGAL_FAKTUR" class="search_init">
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_NPWP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_NPWP" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_NAMA" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_NAMA" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_ALAMAT_LENGKAP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_ALAMAT_LENGKAP" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_JUMLAH_DPP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_JUMLAH_DPP" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_JUMLAH_PPN" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_JUMLAH_PPN" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_JUMLAH_PPNBM" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_JUMLAH_PPNBM" class="search_init"/>
    </div>
</th>
<th style="border-top: none;padding: 5px;">
    <div id="filter_ID_KETERANGAN_TAMBAHAN" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_ID_KETERANGAN_TAMBAHAN" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_FG_UANG_MUKA" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_FG_UANG_MUKA" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_UANG_MUKA_DPP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_UANG_MUKA_DPP" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_UANG_MUKA_PPN" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_UANG_MUKA_PPN" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_UANG_MUKA_PPNBM" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_UANG_MUKA_PPNBM" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_REFERENSI" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_REFERENSI" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_LT" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_LT" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_NPWP2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_NPWP2" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_NAMA2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_NAMA2" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_JALAN" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_JALAN" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_BLOK" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_BLOK" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_NOMOR" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_NOMOR" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_RT" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_RT" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_RW" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_RW" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_KECAMATAN" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_KECAMATAN" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_KELURAHAN" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_KELURAHAN" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_KABUPATEN" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_KABUPATEN" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_PROPINSI" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_PROPINSI" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_KODE_POS" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_KODE_POS" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_NOMOR_TELEPON" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_NOMOR_TELEPON" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_OF_" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_OF_" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_KODE_OBJEK" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_KODE_OBJEK" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_NAMA_OBJEK" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_NAMA_OBJEK" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_HARGA_SATUAN" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_HARGA_SATUAN" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_JUMLAH_BARANG" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_JUMLAH_BARANG" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_HARGA_TOTAL" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_HARGA_TOTAL" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_DISKON" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_DISKON" class="search_init"/>
        <input type="hidden" name="docNumber" id="docNumber" class="search_init" value="${docNumber}"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_DPP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_DPP" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_PPN" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_PPN" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_TARIF_PPNBM" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_TARIF_PPNBM" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_PPNBM" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_PPNBM" class="search_init"/>
    </div>
</th>
<th style="border-top: none;padding: 5px;">
    <div id="filter_docNumberr" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_docNumberr" class="search_init"/>
    </div>
</th>
<th style="border-top: none;padding: 5px;">

</th>

</tr>
</thead>
    <tfoot>
    <tr>
        <th colspan="40"></th>
        <th>${DPP}</th>
        <th>${PPN}</th>
        <th colspan="5"></th>
    </tr>
    </tfoot>
</table>
<g:javascript>
var EFakturTable;
var reloadEFakturTable;
$(function(){

	reloadEFakturTable = function() {
		EFakturTable.fnDraw();
	}

    $('#search_TANGGAL_FAKTUR').datepicker().on('changeDate', function(ev) {
        var newDate = new Date(ev.date);
        $('#search_TANGGAL_FAKTUR_day').val(newDate.getDate());
        $('#search_TANGGAL_FAKTUR_month').val(newDate.getMonth()+1);
        $('#search_TANGGAL_FAKTUR_year').val(newDate.getFullYear());
        $(this).datepicker('hide');
        EFakturTable.fnDraw();
    });

    var recordsefakturperpage = [];
    var anefakturSelected;
    var jmlRecefakturPerPage=0;
    var id;


	$('#search_tanggal').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tanggal_day').val(newDate.getDate());
			$('#search_tanggal_month').val(newDate.getMonth()+1);
			$('#search_tanggal_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			EFakturTable.fnDraw();
	});




$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	EFakturTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	EFakturTable = $('#EFaktur_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
//jotun
	        "fnDrawCallback": function () {
            var rsEfaktur = $("#EFaktur_datatables tbody .row-select");
            var jmlEfakturCek = 0;
            var nRow;
            var idRec;
            rsEfaktur.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsefakturperpage[idRec]=="1"){
                    jmlEfakturCek = jmlEfakturCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsefakturperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecefakturPerPage = rsEfaktur.length;
            if(jmlEfakturCek==jmlRecefakturPerPage && jmlRecefakturPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },

//jotun
		   "fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			        if ( aData['FK'] != null && aData['FK'] != " " && aData['FK'] != "-")
                    {
                        $('td', nRow).css('background-color', '#faf7f7');
                    }
                    else
                    {
                        $('td', nRow).css('background-color', '#FFFFFF');
                    }
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : 50, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "FK",
	"mDataProp": "FK",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
	    if(data!=null && data!=" " && data!="-"){
		    return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="javascript:void(0);" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	    }else{
	        return '<input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="javascript:void(0);" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	    }
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,
{
	"sName": "KD_JENIS_TRANSAKSI",
	"mDataProp": "KD_JENIS_TRANSAKSI",
	"aTargets": [1],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "FG_PENGGANTI",
	"mDataProp": "FG_PENGGANTI",
	"aTargets": [2],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "NOMOR_FAKTUR",
	"mDataProp": "NOMOR_FAKTUR",
	"aTargets": [3],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "MASA_PAJAK",
	"mDataProp": "MASA_PAJAK",
	"aTargets": [4],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "TAHUN_PAJAK",
	"mDataProp": "TAHUN_PAJAK",
	"aTargets": [5],
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "TANGGAL_FAKTUR",
	"mDataProp": "TANGGAL_FAKTUR",
	"aTargets": [6],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "NPWP",
	"mDataProp": "NPWP",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "NAMA",
	"mDataProp": "NAMA",
	"aTargets": [35],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "ALAMAT_LENGKAP",
	"mDataProp": "ALAMAT_LENGKAP",
	"aTargets": [9],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
//
,

{
	"sName": "JUMLAH_DPP",
	"mDataProp": "JUMLAH_DPP",
	"aTargets": [10],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "JUMLAH_PPN",
	"mDataProp": "JUMLAH_PPN",
	"aTargets": [11],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "JUMLAH_PPNBM",
	"mDataProp": "JUMLAH_PPNBM",
	"aTargets": [12],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "ID_KETERANGAN_TAMBAHAN",
	"mDataProp": "ID_KETERANGAN_TAMBAHAN",
	"aTargets": [13],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "FG_UANG_MUKA",
	"mDataProp": "FG_UANG_MUKA",
	"aTargets": [14],
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "UANG_MUKA_DPP",
	"mDataProp": "UANG_MUKA_DPP",
	"aTargets": [15],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "UANG_MUKA_PPN",
	"mDataProp": "UANG_MUKA_PPN",
	"aTargets": [16],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "UANG_MUKA_PPNBM",
	"mDataProp": "UANG_MUKA_PPNBM",
	"aTargets": [17],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "REFERENSI",
	"mDataProp": "REFERENSI",
	"aTargets": [18],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "LT",
	"mDataProp": "LT",
	"aTargets": [19],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "NPWP2",
	"mDataProp": "NPWP2",
	"aTargets": [20],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "NAMA2",
	"mDataProp": "NAMA2",
	"aTargets": [21],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "JALAN",
	"mDataProp": "JALAN",
	"aTargets": [22],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "BLOK",
	"mDataProp": "BLOK",
	"aTargets": [23],
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "NOMOR",
	"mDataProp": "NOMOR",
	"aTargets": [24],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "RT",
	"mDataProp": "RT",
	"aTargets": [25],
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "RW",
	"mDataProp": "RW",
	"aTargets": [26],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "KECAMATAN",
	"mDataProp": "KECAMATAN",
	"aTargets": [27],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "KELURAHAN",
	"mDataProp": "KELURAHAN",
	"aTargets": [28],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "KABUPATEN",
	"mDataProp": "KABUPATEN",
	"aTargets": [29],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "PROPINSI",
	"mDataProp": "PROPINSI",
	"aTargets": [30],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "KODE_POS",
	"mDataProp": "KODE_POS",
	"aTargets": [31],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "NOMOR_TELEPON",
	"mDataProp": "NOMOR_TELEPON",
	"aTargets": [32],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "OF_",
	"mDataProp": "OF_",
	"aTargets": [33],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "KODE_OBJEK",
	"mDataProp": "KODE_OBJEK",
	"aTargets": [34],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
    "sName": "NAMA_OBJEK",
	"mDataProp": "NAMA_OBJEK",
	"aTargets": [8],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "HARGA_SATUAN",
	"mDataProp": "HARGA_SATUAN",
	"aTargets": [36],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "JUMLAH_BARANG",
	"mDataProp": "JUMLAH_BARANG",
	"aTargets": [37],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "HARGA_TOTAL",
	"mDataProp": "HARGA_TOTAL",
	"aTargets": [38],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "DISKON",
	"mDataProp": "DISKON",
	"aTargets": [39],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "DPP",
	"mDataProp": "DPP",
	"aTargets": [40],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "PPN",
	"mDataProp": "PPN",
	"aTargets": [41],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "TARIF_PPNBM",
	"mDataProp": "TARIF_PPNBM",
	"aTargets": [42],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "PPNBM",
	"mDataProp": "PPNBM",
	"aTargets": [43],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "docNumberr",
	"mDataProp": "docNumberr",
	"aTargets": [43],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,


{
	"sName": "staAprove",
	"mDataProp": "staAprove",
	"aTargets": [43],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						var Tanggal = $('#search_Tanggal').val();
						var TanggalDay = $('#search_Tanggal_day').val();
						var TanggalMonth = $('#search_Tanggal_month').val();
						var TanggalYear = $('#search_Tanggal_year').val();

						var Tanggal2 = $('#search_Tanggal2').val();
						var TanggalDay2 = $('#search_Tanggal2_day').val();
						var TanggalMonth2 = $('#search_Tanggal2_month').val();
						var TanggalYear2 = $('#search_Tanggal2_year').val();

						if(Tanggal){
							aoData.push(
									{"name": 'sCriteria_Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_Tanggal_dp', "value": Tanggal},
									{"name": 'sCriteria_Tanggal_day', "value": TanggalDay},
									{"name": 'sCriteria_Tanggal_month', "value": TanggalMonth},
									{"name": 'sCriteria_Tanggal_year', "value": TanggalYear}
							);
						}


						if(Tanggal2){
							aoData.push(
									{"name": 'sCriteria_Tanggal2', "value": "date.struct"},
									{"name": 'sCriteria_Tanggal2_dp', "value": Tanggal2},
									{"name": 'sCriteria_Tanggal2_day', "value": TanggalDay2},
									{"name": 'sCriteria_Tanggal2_month', "value": TanggalMonth2},
									{"name": 'sCriteria_Tanggal2_year', "value": TanggalYear2}
							);
						}


						var FK = $('#filter_FK input').val();
						if(FK){
							aoData.push(
									{"name": 'sCriteria_FK', "value": FK}
							);
						}


						var KD_JENIS_TRANSAKSI = $('#filter_KD_JENIS_TRANSAKSI input').val();
						if(KD_JENIS_TRANSAKSI){
							aoData.push(
									{"name": 'sCriteria_KD_JENIS_TRANSAKSI', "value": KD_JENIS_TRANSAKSI}
							);
						}

						var FG_PENGGANTI = $('#filter_FG_PENGGANTI input').val();
						if(FG_PENGGANTI){
							aoData.push(
									{"name": 'sCriteria_FG_PENGGANTI', "value": FG_PENGGANTI}
							);
						}

						var NOMOR_FAKTUR = $('#filter_NOMOR_FAKTUR input').val();
						if(NOMOR_FAKTUR){
							aoData.push(
									{"name": 'sCriteria_NOMOR_FAKTUR', "value": NOMOR_FAKTUR}
							);
						}


						var MASA_PAJAK = $('#filter_MASA_PAJAK input').val();
						if(MASA_PAJAK){
							aoData.push(
									{"name": 'sCriteria_MASA_PAJAK', "value": MASA_PAJAK}
							);
						}

						var TAHUN_PAJAK = $('#filter_TAHUN_PAJAK input').val();
						if(TAHUN_PAJAK){
							aoData.push(
									{"name": 'sCriteria_TAHUN_PAJAK', "value": TAHUN_PAJAK}
							);
						}

						var TANGGAL_FAKTUR = $('#search_TANGGAL_FAKTUR').val();
						var TANGGAL_FAKTURDay = $('#search_TANGGAL_FAKTUR_day').val();
						var TANGGAL_FAKTURMonth = $('#search_TANGGAL_FAKTUR_month').val();
						var TANGGAL_FAKTURYear = $('#search_TANGGAL_FAKTUR_year').val();

						if(TANGGAL_FAKTUR){
							aoData.push(
									{"name": 'sCriteria_TANGGAL_FAKTUR', "value": "date.struct"},
									{"name": 'sCriteria_TANGGAL_FAKTUR_dp', "value": TANGGAL_FAKTUR},
									{"name": 'sCriteria_TANGGAL_FAKTUR_day', "value": TANGGAL_FAKTURDay},
									{"name": 'sCriteria_TANGGAL_FAKTUR_month', "value": TANGGAL_FAKTURMonth},
									{"name": 'sCriteria_TANGGAL_FAKTUR_year', "value": TANGGAL_FAKTURYear}
							);
						}


						var NPWP = $('#filter_NPWP input').val();
						if(NPWP){
							aoData.push(
									{"name": 'sCriteria_NPWP', "value": NPWP}
							);
						}

						var NAMA_OBJEK = $('#filter_NAMA_OBJEK input').val();
						if(NAMA_OBJEK){
							aoData.push(
									{"name": 'sCriteria_NAMA_OBJEK', "value": NAMA_OBJEK}
							);
						}

						var ALAMAT_LENGKAP = $('#filter_ALAMAT_LENGKAP input').val();
						if(ALAMAT_LENGKAP){
							aoData.push(
									{"name": 'sCriteria_ALAMAT_LENGKAP', "value": ALAMAT_LENGKAP}
							);
						}

						var JUMLAH_DPP = $('#filter_JUMLAH_DPP input').val();
						if(JUMLAH_DPP){
							aoData.push(
									{"name": 'sCriteria_JUMLAH_DPP', "value": JUMLAH_DPP}
							);
						}

						var JUMLAH_PPN = $('#filter_JUMLAH_PPN input').val();
						if(JUMLAH_PPN){
							aoData.push(
									{"name": 'sCriteria_JUMLAH_PPN', "value": JUMLAH_PPN}
							);
						}

						var JUMLAH_PPNBM = $('#filter_JUMLAH_PPNBM input').val();
						if(JUMLAH_PPNBM){
							aoData.push(
									{"name": 'sCriteria_JUMLAH_PPNBM', "value": JUMLAH_PPNBM}
							);
						}

						var ID_KETERANGAN_TAMBAHAN = $('#filter_ID_KETERANGAN_TAMBAHAN input').val();
						if(ID_KETERANGAN_TAMBAHAN){
							aoData.push(
									{"name": 'sCriteria_ID_KETERANGAN_TAMBAHAN', "value": ID_KETERANGAN_TAMBAHAN}
							);
						}


						var FG_UANG_MUKA = $('#filter_FG_UANG_MUKA input').val();
						if(FG_UANG_MUKA){
							aoData.push(
									{"name": 'sCriteria_FG_UANG_MUKA', "value": FG_UANG_MUKA}
							);
						}

						var UANG_MUKA_DPP = $('#filter_UANG_MUKA_DPP input').val();
						if(UANG_MUKA_DPP){
							aoData.push(
									{"name": 'sCriteria_UANG_MUKA_DPP', "value": UANG_MUKA_DPP}
							);
						}


						var UANG_MUKA_PPN = $('#filter_UANG_MUKA_PPN input').val();
						if(UANG_MUKA_PPN){
							aoData.push(
									{"name": 'sCriteria_UANG_MUKA_PPN', "value": UANG_MUKA_PPN}
							);
						}

						//lanjut dari of_
						var UANG_MUKA_PPNBM = $('#filter_UANG_MUKA_PPNBM input').val();
						if(UANG_MUKA_PPNBM){
							aoData.push(
									{"name": 'sCriteria_UANG_MUKA_PPNBM', "value": UANG_MUKA_PPNBM}
							);
						}

						var REFERENSI = $('#filter_REFERENSI input').val();
						if(REFERENSI){
							aoData.push(
									{"name": 'sCriteria_REFERENSI', "value": REFERENSI}
							);
						}

						var UANG_MUKA_PPNBM = $('#filter_UANG_MUKA_PPNBM input').val();
						if(UANG_MUKA_PPNBM){
							aoData.push(
									{"name": 'sCriteria_UANG_MUKA_PPNBM', "value": UANG_MUKA_PPNBM}
							);
						}

						var LT = $('#filter_LT input').val();
						if(LT){
							aoData.push(
									{"name": 'sCriteria_LT', "value": LT}
							);
						}


						var NPWP2 = $('#filter_NPWP2 input').val();
						if(NPWP2){
							aoData.push(
									{"name": 'sCriteria_NPWP2', "value": NPWP2}
							);
						}

						var NAMA2 = $('#filter_NAMA2 input').val();
						if(NAMA2){
							aoData.push(
									{"name": 'sCriteria_NAMA2', "value": NAMA2}
							);
						}


						var JALAN = $('#filter_JALAN input').val();
						if(JALAN){
							aoData.push(
									{"name": 'sCriteria_JALAN', "value": JALAN}
							);
						}

						var BLOK = $('#filter_BLOK input').val();
						if(BLOK){
							aoData.push(
									{"name": 'sCriteria_BLOK', "value": BLOK}
							);
						}

						var NOMOR = $('#filter_NOMOR input').val();
						if(NOMOR){
							aoData.push(
									{"name": 'sCriteria_NOMOR', "value": NOMOR}
							);
						}


						var RT = $('#filter_RT input').val();
						if(RT){
							aoData.push(
									{"name": 'sCriteria_RT', "value": RT}
							);
						}

						var RW = $('#filter_RW input').val();
						if(RW){
							aoData.push(
									{"name": 'sCriteria_RW', "value": RW}
							);
						}

						var KECAMATAN = $('#filter_KECAMATAN input').val();
						if(KECAMATAN){
							aoData.push(
									{"name": 'sCriteria_KECAMATAN', "value": NAMA}
							);
						}

						var KELURAHAN = $('#filter_KELURAHAN input').val();
						if(KELURAHAN){
							aoData.push(
									{"name": 'sCriteria_KELURAHAN', "value": KELURAHAN}
							);
						}

						var KABUPATEN = $('#filter_KABUPATEN input').val();
						if(KABUPATEN){
							aoData.push(
									{"name": 'sCriteria_KABUPATEN', "value": KABUPATEN}
							);
						}

						var PROPINSI = $('#filter_PROPINSI input').val();
						if(PROPINSI){
							aoData.push(
									{"name": 'sCriteria_PROPINSI', "value": PROPINSI}
							);
						}

						var KODE_POS = $('#filter_KODE_POS input').val();
						if(KODE_POS){
							aoData.push(
									{"name": 'sCriteria_KODE_POS', "value": KODE_POS}
							);
						}
						var docNumber = $('#docNumber').val();
						if(docNumber){
							aoData.push(
									{"name": 'sCriteria_docNumber', "value": docNumber}
							);
						}

						var NOMOR_TELEPON = $('#filter_NOMOR_TELEPON input').val();
						if(NOMOR_TELEPON){
							aoData.push(
									{"name": 'sCriteria_NOMOR_TELEPON', "value": NOMOR_TELEPON}
							);
						}

						var OF_ = $('#filter_OF_ input').val();
						if(OF_){
							aoData.push(
									{"name": 'sCriteria_OF_', "value": OF_}
							);
						}

						var KODE_OBJEK = $('#filter_KODE_OBJEK input').val();
						if(KODE_OBJEK){
							aoData.push(
									{"name": 'sCriteria_KODE_OBJEK', "value": KODE_OBJEK}
							);
						}

						var NAMA = $('#filter_NAMA input').val();
						if(NAMA){
							aoData.push(
									{"name": 'sCriteria_NAMA', "value": NAMA}
							);
						}

						var HARGA_SATUAN = $('#filter_HARGA_SATUAN input').val();
						if(HARGA_SATUAN){
							aoData.push(
									{"name": 'sCriteria_HARGA_SATUAN', "value": HARGA_SATUAN}
							);
						}

						var JUMLAH_BARANG = $('#filter_JUMLAH_BARANG input').val();
						if(JUMLAH_BARANG){
							aoData.push(
									{"name": 'sCriteria_JUMLAH_BARANG', "value": JUMLAH_BARANG}
							);
						}


						var HARGA_TOTAL = $('#filter_HARGA_TOTAL input').val();
						if(HARGA_TOTAL){
							aoData.push(
									{"name": 'sCriteria_HARGA_TOTAL', "value": HARGA_TOTAL}
							);
						}

						var DISKON = $('#filter_DISKON input').val();
						if(DISKON){
							aoData.push(
									{"name": 'sCriteria_DISKON', "value": DISKON}
							);
						}

						var DPP = $('#filter_DPP input').val();
						if(DPP){
							aoData.push(
									{"name": 'sCriteria_DPP', "value": DPP}
							);
						}

//

						var PPN = $('#filter_PPN input').val();
						if(PPN){
							aoData.push(
									{"name": 'sCriteria_PPN', "value": PPN}
							);
						}

						var TARIF_PPNBM = $('#filter_TARIF_PPNBM input').val();
						if(TARIF_PPNBM){
							aoData.push(
									{"name": 'sCriteria_TARIF_PPNBM', "value": TARIF_PPNBM}
							);
						}

						var PPNBM = $('#filter_PPNBM input').val();
						if(PPNBM){
							aoData.push(
									{"name": 'sCriteria_PPNBM', "value": PPNBM}
							);
						}
						var docNumberr = $('#filter_docNumberr input').val();
						if(docNumberr){
							aoData.push(
									{"name": 'sCriteria_docNumberr', "value": docNumberr}
							);
						}


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>


			
