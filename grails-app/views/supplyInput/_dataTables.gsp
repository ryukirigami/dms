
<%@ page import="com.kombos.parts.Claim" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="supply_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;" >
				<div><g:message code="supply.goods.label" default="Kode Warna Vendor" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;" >
                <div><g:message code="supply.goods.label" default="Nama Warna Vendor" /></div>
            </th>


			<th style="border-bottom: none;padding: 5px;"  >
				<div><g:message code="supply.t162Qty1.label" default="Qty (cc)" /></div>
			</th>
        </tr>

	</thead>
</table>

<g:javascript>
var supplyInputTable;
var reloadClaimTable;
$(function(){
	
	var recordsSupplyPerPage = [];
    var anSupplySelected;
    var jmlRecSupplyPerPage=0;
    var id;
    
	reloadSupplyTable = function() {
		supplyInputTable.fnDraw();
	}

	
	$('#search_t162TglSupply').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t162TglClaim_day').val(newDate.getDate());
			$('#search_t162TglSupply_month').val(newDate.getMonth()+1);
			$('#search_t162TglSupply_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			supplyInputTable.fnDraw();
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	supplyInputTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	supplyInputTable = $('#supply_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsSupply = $("#supply_datatables tbody .row-select");
            var jmlSupplyCek = 0;
            var nRow;
            var idRec;
            rsSupply.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsSupplyPerPage[idRec]=="1"){
                    jmlSupplyCek = jmlSupplyCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsSupplyPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecSupplyPerPage = rsSupply.length;
            if(jmlSupplyCek==jmlRecSupplyPerPage && jmlRecSupplyPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"aoColumns": [

{
	"sName": "warna",
	"mDataProp": "kodeWarna",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" checked="true" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data ;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "warna",
	"mDataProp": "namaWarna",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "qty",
	"mDataProp": "qty",
	"aTargets": [2],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        return '<input id="qty-'+row['id']+'" class="inline-edit" type="text" style="width:218px;" value="'+data+'">';
    },
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
	$('.select-all').click(function(e) {

        $("#supply_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsSupplyPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsSupplyPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#supply_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsSupplyPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anSupplySelected = supplyInputTable.$('tr.row_selected');

            if(jmlRecSupplyPerPage == anSupplySelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsSupplyPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
	if(cekStatus=="ubah"){
        var noWo = '${posJob.reception.t401NoWO}';
        var nopos = '${posJob.pos.t418NoPOS}';
        $.ajax({
        url:'${request.contextPath}/supplyInput/getTableData',
    		type: "POST", // Always use POST when deleting data
    		data: { noWo: noWo,nopos:nopos },
    		success : function(data){
    		    $.each(data,function(i,item){
                    supplyInputTable.fnAddData({
                        'id': item.id,
						'kodeWarna': item.kodeWarna,
						'namaWarna': item.namaWarna,
						'qty': item.qty
                    });
                });
            }
		});
    }
});
</g:javascript>


			
