<%@ page import="com.kombos.board.JPB; com.kombos.reception.Reception;" %><!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
    <g:javascript>
		$(function(){
			$('#noWo').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/supplyInput/kodeList', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});
    </g:javascript>
		<g:javascript dissupplyition="head">
        var noUrut = 1;
	var show;
	var cekStatus = "${aksi}";
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){

	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/supply/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#supply-form').empty();
    	$('#supply-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#supply-table").hasClass("span12")){
   			$("#supply-table").toggleClass("span12 span5");
        }
        $("#supply-form").css("display","block");
   	}
   	
   	expandTableLayout = function(){
   		if($("#supply-table").hasClass("span5")){
   			$("#supply-table").toggleClass("span5 span12");
   		}
        $("#supply-form").css("display","none");
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#supply-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/supplyInput/massdelete',
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadClaimTable();
    		}
		});
		
   	}

});

  //darisini
	$("#supplyAddModal").on("show", function() {
		$("#supplyAddModal .btn").on("click", function(e) {
			$("#supplyAddModal").modal('hide');
		});
	});
	$("#supplyAddModal").on("hide", function() {
		$("#supplyAddModal a.btn").off("click");
	});

    loadSupplyAddModal = function(){
    	var noWo = $('#noWo').val();
    	var tanggal = $('#tanggal').val();
    	if(noWo!="" && tanggal!=""){
            $("#supplyAddContent").empty();

            $.ajax({type:'POST', url:'${request.contextPath}/supplyInput/addModal',
                success:function(data,textStatus){
                        $("#supplyAddContent").html(data);
                        $("#supplyAddModal").modal({
                            "backdrop" : "static",
                            "keyboard" : true,
                            "show" : true
                        }).css({'width': '650px','margin-left': function () {return -($(this).width() / 2);}});

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
        }else{
            alert('Data Belum Lengkap');
        }


    }

       tambahReq = function(){
           var checkSupply =[];
            $("#SupplyAdd-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    var nRow = $(this).parents('tr')[0];

					checkSupply.push(nRow);
                }
            });
            if(checkSupply.length<1){
                alert('Anda belum memilih data yang akan ditambahkan');
                loadSupplyAddModal();
            }else {
				for (var i=0;i < checkSupply.length;i++){

					var aData = SupplyAddTable.fnGetData(checkSupply[i]);
					supplyInputTable.fnAddData({
						'id': aData['id'],
						'norut': noUrut,
						'kodeWarna': aData['kodeWarna'],
						'namaWarna': aData['namaWarna'],
						'qty': aData['qty']
						});
					noUrut++;
					$('#qty-'+aData['id']).autoNumeric('init',{
						vMin:'0',
						vMax:'999999999999999999',
						mDec: null,
						aSep:''
					});
				}

			}

      }
      updateSupply = function(){
            var tanggal = $("#tanggal").val();
            var noSupply = $("#noSupply").val();
            var formSupply = $('#supply-table').find('form');
            var qtyNol = false
            var checkSupply =[];
            $("#supply-table tbody .row-select").each(function() {
            if(this.checked){
                var id = $(this).next("input:hidden").val();
                var nRow = $(this).parents('tr')[0];
                var aData = supplyInputTable.fnGetData(nRow);
                var qtyInput = $('#qty-' + aData['id'], nRow);
                if(qtyInput.val()==0 || qtyInput.val()==""){
                   qtyNol = true
                }
                checkSupply.push(id);
                formSupply.append('<input type="hidden" name="qty-'+aData['id'] + '" value="'+qtyInput[0].value+'" class="deleteafter">');
            }
            });
            if(checkSupply.length<1){
                alert('Anda belum memilih data yang akan ditambahkan');
            }else if(tanggal=="" || noSupply==""){
                toastr.error('<div>Data Belum Lengkap</div>');
            }else if(qtyNol == true){
                toastr.error('<div>Quantity Tidak Boleh NOL</div>');
                formSupply.find('.deleteafter').remove();
            }else{

                $("#ids").val(JSON.stringify(checkSupply));
                $.ajax({
                    url:'${request.contextPath}/supplyInput/ubah',
                    type: "POST", // Always use POST when deleting data
                    data : formSupply.serialize(),
                    success : function(data){
                      if(data=='notOk'){
                        toastr.error('<div>Gagal Simpan.</div>');
                      }else{
                        window.location.href = '#/viewOrderCat' ;
                        toastr.success('<div>Succes.</div>');
                        formSupply.find('.deleteafter').remove();
                        $("#ids").val('');
                      }

                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });

                checkSupply = [];
            }
       }
       deleteData = function(){
                var checkGoods =[];
                $("#supply-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    var row = $(this).closest("tr").get(0);
                    supplyInputTable.fnDeleteRow(supplyInputTable.fnGetPosition(row));
                }
                });
      }
        createRequest = function(){
            var tanggal = $("#tanggal").val();
            var noSupply = $("#noSupply").val();
            var formSupply = $('#supply-table').find('form');
            var qtyNol = false
            var checkSupply =[];
            $("#supply-table tbody .row-select").each(function() {
            if(this.checked){
                var id = $(this).next("input:hidden").val();
                var nRow = $(this).parents('tr')[0];
                var aData = supplyInputTable.fnGetData(nRow);
                var qtyInput = $('#qty-' + aData['id'], nRow);
                if(qtyInput.val()==0 || qtyInput.val()==""){
                   qtyNol = true
                }
                checkSupply.push(id);
                formSupply.append('<input type="hidden" name="qty-'+aData['id'] + '" value="'+qtyInput[0].value+'" class="deleteafter">');
            }
            });
            if(checkSupply.length<1){
                alert('Anda belum memilih data yang akan ditambahkan');
            }else if(tanggal=="" || noSupply==""){
                toastr.error('<div>Data Belum Lengkap</div>');
            }else if(qtyNol == true){
                toastr.error('<div>Quantity Tidak Boleh NOL</div>');
                formSupply.find('.deleteafter').remove();
            }else{

                $("#ids").val(JSON.stringify(checkSupply));
                $.ajax({
                    url:'${request.contextPath}/supplyInput/ubah',
                    type: "POST", // Always use POST when deleting data
                    data : formSupply.serialize(),
                    success : function(data){
                      if(data=='notOk'){
                        toastr.error('<div>Gagal Simpan.</div>');
                      }else{
                        window.location.href = '#/viewOrderCat' ;
                        toastr.success('<div>Succes.</div>');
                        formSupply.find('.deleteafter').remove();
                        $("#ids").val('');
                      }

                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });

                checkSupply = [];
            }
        };

</g:javascript>

	</head>
	<body>
    <div class="navbar box-header no-border">
        <span class="pull-left">
            <g:if test="${aksi=='ubah'}">
                Edit Vendor Supply
            </g:if>
            <g:else>
                Input Vendor Supply
            </g:else>
        </span>
        <ul class="nav pull-right">
            <li>&nbsp;</li>
        </ul>
    </div>
    <br>
	<div class="box">
		<div class="span12" id="supply-table">
            <form id="form-supply" class="form-horizontal">
                <input type="hidden" name="ids" id="ids" value="">
                    <fieldset>
                        <div class="control-group">
                            <g:hiddenField name="noWo" value="${posJob.pos.reception?.t401NoWO}" />
                            <g:hiddenField name="nopos" value="${posJob.pos.t418NoPOS}" />
                            <label class="control-label" for="t951Tanggal" style="text-align: left;">
                                Nama Vendor
                            </label>
                            <div id="filter_wo" class="controls">
                                <g:textField name="namaVendor" readonly="readonly" id="namaVendor" value="${posJob.pos.vendorCat?.m191Nama}" autocomplete="off" maxlength="220"  />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="t951Tanggal" style="text-align: left;">
                                Nomor Supply Vendor *
                            </label>
                            <div id="filter_nomor" class="controls">
                                <g:if test="${aksi=='ubah'}">
                                    <g:textField name="noSupply" id="noSupplyEdit" maxlength="20" value="${posJob.pos.t418NoSupply}" />
                                </g:if>
                                <g:else>
                                    <g:textField name="noSupply" id="noSupply" maxlength="20" value="${noSupply}" />
                                </g:else>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="userRole" style="text-align: left;">
                                Tanggal Supply Vendor *
                            </label>
                            <div id="filter_status" class="controls">
                                <ba:datePicker id="tanggal" name="tanggal" precision="day" format="dd-MM-yyyy" value="${posJob.pos.t418TglSupply}" required="true"  />
                            </div>
                         </div>

                        <div class="control-group">
                            <label class="control-label" for="t951Tanggal" style="text-align: left;">
                                Keterangan
                            </label>
                            <div id="filter_nomrs" class="controls">
                                <g:textField name="ket" id="ket" value="${posJob.pos.t418KetSupply}" maxlength="255" />
                            </div>
                        </div>

                    </fieldset>
                   </form>
                    <fieldset style="position: inherit; margin-left: 450px; margin-top: -190px;">
                       <table style="padding: 10px; background-color: #ececec; z-index: -1;">
                           <tr>
                               <td style="padding: 7px">
                                   Nomor Wo
                               </td>
                               <td style="padding: 7px">
                                   : ${posJob.reception?.t401NoWO}
                               </td>
                           </tr>
                           <tr>
                               <td style="padding: 7px">
                                   Tanggal Wo
                               </td>
                               <td style="padding: 7px">
                                   : ${posJob.reception?.t401TanggalWO.format("dd/MM/yyyy")}
                               </td>
                           </tr>
                           <tr>
                               <td style="padding: 7px">
                                   Nomor Polisi
                               </td>
                               <td style="padding: 7px">
                                    :  ${posJob.reception?.historyCustomerVehicle?.kodeKotaNoPol.m116ID}
                                    ${posJob.reception?.historyCustomerVehicle?.t183NoPolTengah}
                                    ${posJob.reception?.historyCustomerVehicle?.t183NoPolBelakang}

                               </td>
                            </tr>
                           <tr>
                               <td style="padding: 7px">
                                   Model
                               </td>
                               <td style="padding: 7px">
                                  : ${posJob.reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel}
                               </td>
                           </tr>
                           <tr>
                               <td style="padding: 7px">
                                   Warna Order
                               </td>
                               <td style="padding: 7px">
                                   : ${posJob.pos?.warna.m092NamaWarna}
                               </td>
                           </tr>
                           <tr>
                               <td style="padding: 7px">
                                   Painting Time
                               </td>
                               <td style="padding: 7px">
                                     :
                                   <%
                                       def jpb = JPB.findByReception(posJob.reception)?.t451TglJamPlan
                                       if(jpb){
                                           jpb += posJob.reception?.t401Proses1 as int
                                           jpb += posJob.reception?.t401Proses2 as int
                                           out.println(jpb.format("dd/MM/yyyy"))
                                       }
                                   %>
                               </td>
                           </tr>
                       </table>
                    </fieldset>
                    <fieldset>
                        <table>
                            <tr>
                                <td>
                                    <button style="width: 170px;height: 30px; border-radius: 5px" class="btn btn-primary" name="view" id="add" onclick="loadSupplyAddModal()">Search Warna</button>
                                </td>
                            </tr>
                        </table>
                    </fieldset><br>
			<g:render template="dataTables" />
            <g:if test="${aksi=='ubah'}">
                <g:field type="button" onclick="updateSupply()" class="btn btn-primary create" name="Edit"  value="${message(code: 'default.button.upload.label', default: 'Update')}" />
            </g:if>
            <g:else>
                <g:field type="button" onclick="createRequest()" class="btn btn-primary create" name="tambahSave"  value="${message(code: 'default.button.upload.label', default: 'Save')}" />
            </g:else>

            <g:field type="button" onclick="window.location.replace('#/viewOrderCat');" class="btn btn-cancel" name="cancel" id="cancel" value="Cancel" />
            <g:field type="button" onclick="deleteData()" class="btn btn-primary create" name="deleteData" id="delData" value="${message(code: 'default.button.upload.label', default: 'Delete')}" />
		</div>
		<div class="span7" id="supply-form" style="display: none;"></div>
	</div>
    <div id="supplyAddModal" class="modal fade">
        <div class="modal-dialog" style="width: 650px;">
            <div class="modal-content" style="width: 650px;">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 650px;">
                    <div id="supplyAddContent"/>
                <div class="iu-content"></div>

                </div>
            </div>
        </div>
    </div>
</body>
</html>

<g:javascript>
    if(cekStatus=="ubah"){
        document.getElementById("noSupplyEdit").focus();
    }else{
        document.getElementById("tanggal").focus();
    }
</g:javascript>