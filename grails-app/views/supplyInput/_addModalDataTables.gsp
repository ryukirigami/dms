
<%@ page import="com.kombos.parts.Claim" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="SupplyAdd_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover fixed table-selectable" style="table-layout: fixed; width: 573px">
    <col width="200px" />
    <col width="373px" />

    <thead>
    <tr>

        <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:200px;">
            <div><input type="checkbox" class="pull-left select-all" aria-label="Select all" title="Select all"/>&nbsp;
                <g:message code="SupplyAdd.goods.label" default="Kode Warna" /></div>
        </th>

        <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:373px;">
            <div><g:message code="SupplyAdd.goods.label" default="Nama Warna" /></div>
        </th>

    </tr>
    <tr>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_kode" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_goods" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_nama" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_goods2" class="search_init" />
            </div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var SupplyAddTable;
var reloadSupplyAddTable;
$(function(){

	reloadSupplyAddTable = function() {
		SupplyAddTable.fnDraw();
	}

 var recordspartsperpage = [];//new Array();
    var anPartsSelected;
    var jmlRecPartsPerPage=0;
    var id;

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	SupplyAddTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});
    SupplyAddTable = $('#SupplyAdd_datatables').dataTable().fnDestroy();
	SupplyAddTable = $('#SupplyAdd_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsParts = $("#SupplyAdd_datatables tbody .row-select");
            var jmlPartsCek = 0;
            var nRow;
            var idRec;
            rsParts.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordspartsperpage[idRec]=="1"){
                    jmlPartsCek = jmlPartsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordspartsperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecPartsPerPage = rsParts.length;
            if(jmlPartsCek == jmlRecPartsPerPage && jmlRecPartsPerPage > 0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "addModalDatatablesList")}",
		"aoColumns": [

{
	"sName": "kodeWarna",
	"mDataProp": "kodeWarna",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp; '+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "namaWarna",
	"mDataProp": "namaWarna",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	

						var kode = $('#filter_kode input').val();
						if(kode){
							aoData.push(
									{"name": 'sCriteria_kode', "value": kode}
							);
						}
						var nama = $('#filter_nama input').val();
						if(nama){
							aoData.push(
									{"name": 'sCriteria_nama', "value": nama}
							);
						}

                        var exist =[];
						$("#supply_datatables tbody .row-select").each(function() {
							var id = $(this).next("input:hidden").val();
							exist.push(id);
						});
						if(exist.length > 0){
							aoData.push(
										{"name": 'sCriteria_exist', "value": JSON.stringify(exist)}
							);
						}
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#SupplyAdd_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordspartsperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordspartsperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#SupplyAdd_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordspartsperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anPartsSelected = SupplyAddTable.$('tr.row_selected');
        } else {
            recordspartsperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
