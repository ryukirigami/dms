
<%@ page import="com.kombos.administrasi.KategoriJob; com.kombos.board.ASB" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout, baseapplist, bootstraptooltip" />
		<g:javascript>
			var show;
			var edit;
            var moveManualASB;
			$(function(){
                moveManualASB = function(){
        			var params;
                    $.post("${request.getContextPath()}/ASBToJPB/moveManual", params, function(data) {
                        if (data){
                            alert("Success");
                            reloadASBtoJPBTable();
                        }
                        else
                            alert("Internal Server Error");
                    });
                }
			         
	$('#search_tglView').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tglView_day').val(newDate.getDate());
			$('#search_tglView_month').val(newDate.getMonth()+1);
			$('#search_tglView_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
	});

   	asbInput = function(){
        var oTable = $('#ASBtoJPB_datatables').dataTable();
        oTable.fnReloadAjax();
        var from = $("#search_tglView").val().split("/");
        var theDate = new Date();
        theDate.setFullYear(from[2], from[1] - 1, from[0]);
        //alert("theDate=" + theDate);
        var m_names = new Array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
        var formattedDate =  theDate.getDate() + " " + m_names[theDate.getMonth()] + " " + theDate.getFullYear();
        document.getElementById("lblTglView").innerHTML = formattedDate;
   	}
});

</g:javascript>
	</head>
	<body>
    <div class="navbar box-header no-border">
   		<span class="pull-left">ASB To JPB</span>
   	</div>

	<div class="box">
        <table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">
            <tr>
            <td style="width: 33%;vertical-align: top;">
                <div class="box">
                    <legend style="font-size: small">View ASB</legend>
                    <table style="width: 100%;border: 0px">
                        <tr>
                            <td>
                                <label class="control-label" for="lbl_rcvDate">
                                    Nama Workshop
                                </label>
                            </td>
                            <td>
                                <div id="lbl_companyDealer" class="controls">
                                    %{--<g:select id="input_companyDealer" name="companyDealer.id" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop")}}" optionKey="id" required="" value="${session?.userCompanyDealer?.id}" class="many-to-one"/>--}%
                                    <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                        <g:select name="companyDealer.id" id="input_companyDealer" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                                    </g:if>
                                    <g:else>
                                        <g:select name="companyDealer.id" id="input_companyDealer" readonly="" disabled="" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                                    </g:else>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label class="control-label" for="lbl_tglView">
                                    Tanggal
                                </label>
                            </td>
                            <td>
                                <div id="lbl_tglView" class="controls">
                                    <div id="filter_tglView" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                                         <input type="hidden" name="search_tglView" value="date.struct">
                                         <input type="hidden" name="search_tglView_day" id="search_tglView_day" value="">
                                         <input type="hidden" name="search_tglView_month" id="search_tglView_month" value="">
                                         <input type="hidden" name="search_tglView_year" id="search_tglView_year" value="">
                                         <input type="text" data-date-format="dd/mm/yyyy" name="search_tglView_dp" value="" id="search_tglView" class="search_init">
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <button class="btn btn-primary" id="buttonView" onclick="asbInput()">View</button>
                            </td>
                        </tr>

                    </table>
                </div>
            </td>
            <td style="width: 33%;vertical-align: top;padding-left: 10px;">
                <button class="btn btn-primary" id="buttonDoMove" onclick="moveManualASB()">Move Manual ASB to JPB</button>
            </td>

            <td style="width: 33%;vertical-align: top;padding-left: 10px;">

            </td>

            </tr>
            <tr>
                <td colspan="3">
                    <div id="ASB-table">
                        <g:if test="${flash.message}">
                            <div class="message" role="status">
                                ${flash.message}
                            </div>
                        </g:if>

                        <g:render template="dataTables" />
                    </div>
                    </td>
                </tr>
            <tr>
                <table>
                    <tr>
                        <%
                            def cariData = KategoriJob.createCriteria().list {order("m055KategoriJob")};
                            cariData.each {
                        %>
                        <td style="padding: 10px;align-content: center">
                            <table class="table table-bordered" style="width: 10%;background-color: ${it?.m055WarnaBord}">
                                <tr><td>&nbsp;&nbsp;</td></tr>
                            </table>
                            <br>${it?.m055KategoriJob}
                        </td>
                        <%
                            }
                        %>
                    </tr>
                </table>
            </tr>
            <tr>
                <td style="width: 33%;vertical-align: top;">
                    <div class="box">
                        <legend style="font-size: small">Daftar Unit Booking Perlu Reschedule</legend>
                        <button class="btn btn-primary" id="btnMoveManual" >Move Manual</button>
                        <button class="btn btn-primary" id="btnMoveAuto" >Move Auto</button>
                    </div>
                </td>
                <td style="width: 33%;vertical-align: top;padding-left: 10px;">
                    <div class="box">
                        <table>
                        <tr>
                            <td style="width: 35%">No Polisi:</td>
                            <td style="width: 65%"></td>
                        </tr>
                        <tr>
                            <td style="width: 35%">No WO:</td>
                            <td style="width: 65%"></td>
                        </tr>
                        <tr>
                            <td style="width: 35%">Delivery Time:</td>
                            <td style="width: 65%"></td>
                        </tr>
                        <tr>
                            <td style="width: 35%">Task List:</td>
                            <td style="width: 65%"></td>
                        </tr>
                    </table>
                    </div>
                </td>

                <td style="width: 33%;vertical-align: top;padding-left: 10px;">
                    <div class="box">
                        <legend style="font-size: small">Daftar Unit Carry Over Perlu Reschedule</legend>
                        <button class="btn btn-primary" id="btnMoveManualCarryOver" >Move Manual</button>
                        <button class="btn btn-primary" id="btnMoveAutoCarryOver" >Move Auto</button>
                    </div>
                </td>

                </tr>
        </table>
	</div>
</body>
</html>
