<%@ page import="com.kombos.hrd.AbsensiKaryawan" %>

<r:require modules="baseapplayout"/>
<g:render template="../menu/maxLineDisplay"/>

<table id="absensiKaryawan_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="absensiKaryawan.absent.label" default="Nomor Karyawan"/></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="absensiKaryawan.attend.label" default="Nama Karyawan"/></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="absensiKaryawan.bulanTahun.label" default="Periode"/></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="absensiKaryawan.karyawan.label" default="Masuk"/></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="absensiKaryawan.lastUpdProcess.label" default="Alpa"/></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="absensiKaryawan.leave.label" default="Ijin"/></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="absensiKaryawan.overtime.label" default="Sakit"/></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="absensiKaryawan.attend.label" default="Cabang"/></div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var absensiKaryawanTable;
var reloadAbsensiKaryawanTable;
$(function(){
	$('#clear').click(function(e){
        $('#bulan').val("");
        $('#tahun').val("");
        $('#namaKaryawan').val("");
        $('#namaKaryawan').val("");
        e.stopPropagation();
        absensiKaryawanTable.fnDraw();
	});
    $('#view').click(function(e){
        e.stopPropagation();
		absensiKaryawanTable.fnDraw();
	});
	reloadAbsensiKaryawanTable = function() {
		absensiKaryawanTable.fnDraw();
	}

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	absensiKaryawanTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	absensiKaryawanTable = $('#absensiKaryawan_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "karyawan",
	"mDataProp": "nomor",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#/absensiKaryawan" onclick="showDetail('+row['id']+');">'+data+'</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "karyawan",
	"mDataProp": "karyawan",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "bulanTahun",
	"mDataProp": "periode",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "attend",
	"mDataProp": "attend",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}

,

{
	"sName": "absent",
	"mDataProp": "absent",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}

,

{
	"sName": "permit",
	"mDataProp": "permit",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}

,

{
	"sName": "leave",
	"mDataProp": "leave",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}
,

{
	"sName": "cabang",
	"mDataProp": "cabang",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var bulan = $('#bulan').val();
						if(bulan){
							aoData.push(
									{"name": 'sCriteria_bulan', "value": bulan}
							);
						}

						var tahun = $('#tahun').val();
						if(tahun){
							aoData.push(
									{"name": 'sCriteria_tahun', "value": tahun}
							);
						}

						var namaKaryawan = $('#namaKaryawan').val();
						if(namaKaryawan){
							aoData.push(
									{"name": 'namaKaryawan', "value": namaKaryawan}
							);
						}
						var sCriteria_dealer = $('#sCriteria_dealer').val();
						if(sCriteria_dealer){
							aoData.push(
									{"name": 'sCriteria_dealer', "value": sCriteria_dealer}
							);
						}
	

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
