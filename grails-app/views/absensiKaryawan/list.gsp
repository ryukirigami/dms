
<%@ page import="com.kombos.hrd.AbsensiKaryawan" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'absensiKaryawan.label', default: 'Absensi Karyawan')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
        <r:require modules="baseapplayout, baseapplist" />
        <style>
        .modal.fade.in {
            left: 25.5% !important;
            width: 1205px !important;
        }
        .modal.fade {
            top: -100%;
        }
        </style>
		<g:javascript>
			var show;
			var edit;
			var oFormService;
			$(function(){ 
			
				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('absensiKaryawan');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('absensiKaryawan', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('absensiKaryawan', '${request.contextPath}/absensiKaryawan/massdelete', reloadAbsensiKaryawanTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('absensiKaryawan','${request.contextPath}/absensiKaryawan/show/'+id);
				    reloadAbsensiKaryawanTable();
				};


            $('#namaKaryawan').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/absensiKaryawan/namaKaryawan', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});

			$("#absensiModal").on("show", function() {
                $("#absensiModal .btn").on("click", function(e) {
                    $("#absensiModal").modal('hide');
                });
            });
            $("#absensiModal").on("hide", function() {
                $("#absensiModal a.btn").off("click");
            });
            showDetail = function(id){
                $("#absensiContent").empty();
                $.ajax({type:'POST', url:'${request.contextPath}/absensiKaryawan/listDetail',
                data : {id : id},
                    success:function(data,textStatus){
                            $("#absensiContent").html(data);
                            $("#absensiModal").modal({
                                "backdrop" : "static",
                                "keyboard" : true,
                                "show" : true
                            }).css({'width': '700px','margin-left': function () {return -($(this).width() / 2);}});
                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){},
                    complete:function(XMLHttpRequest,textStatus){
                        $('#spinner').fadeOut();
                    }
                });
            }

            showUpload = function(){
                $("#uploadContent").empty();
                $.ajax({type:'POST', url:'${request.contextPath}/absensiKaryawan/listUpload',
                    success:function(data,textStatus){
                            $("#uploadContent").html(data);
                            $("#uploadModal").modal({
                                "backdrop" : "static",
                                "keyboard" : true,
                                "show" : true
                            }).css({'width': '1100px','margin-left': function () {return -($(this).width() / 2);}});
                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){},
                    complete:function(XMLHttpRequest,textStatus){
                        $('#spinner').fadeOut();
                    }
                });
            }
            ubah = function(id){
                  $.ajax({type:'POST', url:'${request.contextPath}/absensiKaryawan/editForm?id='+id,
                    success:function(data,textStatus){
                      $("#keteranganEdit").val(data[0].keterangan);
                      $("#jam").val(data[0].jam);
                      $("#menit").val(data[0].menit);
                      $("#statusEdit").val(data[0].masuk);
                      $("#id").val(data[0].id);
                      $("#dialog-edit").modal({
                        "backdrop" : "static",
                        "keyboard" : true,
                        "show" : true
                      }).css({'width': '380px','margin-left': function () {return -($(this).width() / 2);}});
                    }
                  });
            }
            oFormService = {
			         fnShowKaryawanDataTable: function() {
                        $("#karyawanModal").modal("show");
                        $("#karyawanModal").fadeIn();
			        },
			        fnHideKaryawanDataTable: function() {
			            $("#karyawanModal").modal("hide");
			        }
			    }

			    oFormService.fnHideKaryawanDataTable();
});
        </g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#" onclick="showUpload()"
				style="display: block;" > Upload &nbsp;&nbsp;
			</a></li>
            <li><a class="pull-right box-action" href="#"
                   style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
                        class="icon-plus"></i>&nbsp;&nbsp;
            </a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="absensiKaryawan-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <fieldset>
                <table style="padding-right: 10px">
                    <tr>
                        <td style="width: 130px">
                            <label class="control-label" for="t951Tanggal">
                                <g:message code="auditTrail.tanggal.label" default="Periode" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="filter_m777Tgl" class="controls">
                                <select name="bulan" id="bulan" value="${skrg?.format("MM")}" >
                                    <option value="01" ${skrg?.format("MM")=="01" ? "selected" : ""}>Januari</option>
                                    <option value="02" ${skrg?.format("MM")=="02" ? "selected" : ""}>Februari</option>
                                    <option value="03" ${skrg?.format("MM")=="03" ? "selected" : ""}>Maret</option>
                                    <option value="04" ${skrg?.format("MM")=="04" ? "selected" : ""}>April</option>
                                    <option value="05" ${skrg?.format("MM")=="05" ? "selected" : ""}>Mei</option>
                                    <option value="06" ${skrg?.format("MM")=="06" ? "selected" : ""}>Juni</option>
                                    <option value="07" ${skrg?.format("MM")=="07" ? "selected" : ""}>Juli</option>
                                    <option value="08" ${skrg?.format("MM")=="08" ? "selected" : ""}>Agustus</option>
                                    <option value="09" ${skrg?.format("MM")=="09" ? "selected" : ""}>September</option>
                                    <option value="10" ${skrg?.format("MM")=="10" ? "selected" : ""}>Oktober</option>
                                    <option value="11" ${skrg?.format("MM")=="11" ? "selected" : ""}>Nomvember</option>
                                    <option value="12" ${skrg?.format("MM")=="12" ? "selected" : ""}>Desember</option>
                                </select> -

                                <select name="tahun" id="tahun" style="width: 100px">
                                    %{

                                        for(int i=(new Date().format("yyyy").toInteger());i>=2000;i--){
                                            out.print('<option value="'+i+'">'+i+'</option>');
                                        }
                                    }%
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 130px">
                            <label class="control-label" for="t951Tanggal">
                                <g:message code="auditTrail.tanggal.label" default="Nama Karyawan" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <g:textField name="namaKaryawan" id="namaKaryawan" maxlength="20"  class="typeahead" />
                        </td>
                    </tr>
                    <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                    <tr>
                       <td style="width: 130px">
                           <label class="control-label" for="t951Tanggal">
                                <g:message code="auditTrail.tanggal.label" default="Cabang" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <g:select name="sCriteria_dealer" id="sCriteria_dealer" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" noSelection="['':'SEMUA']" />
                        </td>
                    </tr>
                    </g:if>
                    <tr>
                        <td colspan="3" >
                            <div class="controls" style="right: 0">
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-primary" name="view" id="view" >View</button>
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-cancel" name="clear" id="clear" >Clear</button>
                            </div>
                        </td>
                    </tr>
                </table>

            </fieldset>
			<g:render template="dataTables" />
		</div>
		<div class="span7" id="absensiKaryawan-form" style="display: none;"></div>
	</div>

    <div id="absensiModal" class="modal hide">
        <div class="modal-dialog" style="width: 700px;">
            <div class="modal-content" style="width: 700px;">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 700px;">
                    <div id="absensiContent"/>
                    <div class="iu-content"></div>

                </div>
            </div>
        </div>
    </div>

    <div id="uploadModal" class="modal hide">
        <div class="modal-dialog" style="width: 1100px;">
            <div class="modal-content" style="width: 1100px;">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 1100px;">
                    <div id="uploadContent"/>
                    <div class="iu-content"></div>

                </div>
            </div>
        </div>
    </div>
    <div id="dialog-edit" class="modal hide in modal-reception" style="display: none; ">
        <g:render template="edit"/>
    </div>
</body>
</html>
