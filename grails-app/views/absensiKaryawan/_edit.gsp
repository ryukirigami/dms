<%@ page import="com.kombos.hrd.AbsensiKaryawan" %>

<fieldset style="width: 130px; padding-left: 20px;padding-bottom: 20px;">
    <legend>Edit Absensi Detail</legend>
    <table style="padding-left: 10px; ">
        <tr>
            <td style="width: 130px">
                <label class="control-label" for="t951Tanggal">
                    Status
                </label>
            </td>
            <td>
                <label class="control-label" for="t951Tanggal">
                    <g:hiddenField name="id" id="id" />
                    <g:hiddenField name="tgl" id="tgl" />
                    <select name="statusEdit" id="statusEdit">
                        <option value="1">Masuk</option>
                        <option value="3">Istirahat</option>
                        <option value="4">Masuk Istirahat</option>
                        <option value="2">Pulang</option>
                        <option value="5">Ijin</option>
                        <option value="6">Alpa</option>
                        <option value="7">Sakit</option>
                    </select>
                </label>
            </td>
        </tr>
        <tr>
            <td style="width: 130px">
                <label class="control-label" for="t951Tanggal">
                    Jam
                </label>
            </td>
            <td>
                <label class="control-label" for="t951Tanggal">

                    <select id="jam" name="jam" style="width: 60px" >
                        %{
                            for (int i=0;i<24;i++){
                                if(i<10){
                                    out.println('<option value="'+i+'">0'+i+'</option>');
                                } else {
                                    out.println('<option value="'+i+'">'+i+'</option>');
                                }

                            }
                        }%
                    </select> H :
                    <select id="menit" name="menit" style="width: 60px">
                        %{
                            for (int i=0;i<60;i++){
                                if(i<10){
                                    out.println('<option value="'+i+'">0'+i+'</option>');
                                } else {
                                    out.println('<option value="'+i+'">'+i+'</option>');
                                }
                            }
                        }%
                    </select> m
                </label>
            </td>
        </tr>
        <tr>
            <td style="width: 130px">
                <label class="control-label" for="t951Tanggal">
                    Keterangan
                </label>
            </td>
            <td>
                <g:textArea name="keteranganEdit" value="" ></g:textArea>
            </td>
        </tr>
        <tr>
            <td style="width: 130px" colspan="2">
                <button class="btn cancel" data-dismiss="modal" name="close" >Close</button>
                <button class="btn btn-primary" data-dismiss="modal" onclick="save()" name="save">Simpan</button>
            </td>
        </tr>
      </table>
</fieldset>
