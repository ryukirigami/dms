<%@ page import="com.kombos.hrd.AbsensiKaryawan" %>

<r:require modules="baseapplayout"/>
<g:render template="../menu/maxLineDisplay"/>

<table id="absensiKaryawanDetail_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="absensiKaryawanDetail.karyawan.label" default="Masuk"/></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="absensiKaryawanDetail.lastUpdProcess.label" default="Status"/></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="absensiKaryawanDetail.lastUpdProcess.label" default="Keterangan"/></div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var absensiKaryawanDetailTable;
var reloadAbsensiKaryawanTable;
$(function(){
	$('#clear').click(function(e){
        $('#bulan').val("");
        $('#tahun').val("");
        $('#namaKaryawan').val("");
        e.stopPropagation();
        absensiKaryawanDetailTable.fnDraw();
	});
    $('#view').click(function(e){
        e.stopPropagation();
		absensiKaryawanDetailTable.fnDraw();
	});
	reloadAbsensiKaryawanTable = function() {
		absensiKaryawanDetailTable.fnDraw();
	}

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	absensiKaryawanDetailTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	absensiKaryawanDetailTable = $('#absensiKaryawanDetail_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : 5, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesListDetail")}",
		"aoColumns": [


{
	"sName": "masuk",
	"mDataProp": "masuk",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="hidden" value="'+row['id']+'"> '+data+'' +
		 '<a class="pull-right cell-action" href="javascript:void(0);" onclick="hapus('+row['id']+');">&nbsp;&nbsp;<i class="icon-remove"></i>&nbsp;&nbsp;</a>' +
		  '<a class="pull-right cell-action" href="javascript:void(0);" onclick="ubah('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "status",
	"mDataProp": "status",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "keterangan",
	"mDataProp": "ket",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

							aoData.push(
									{"name": 'bulanTahun', "value": "${absensi.bulanTahun}"}
							);


 							aoData.push(
									{"name": 'namaKaryawan', "value": "${absensi.karyawan.nama}"}
							);


						var namaKaryawan = $('#namaKaryawan').val();
						if(namaKaryawan){
							aoData.push(
									{"name": 'namaKaryawan', "value": namaKaryawan}
							);
						}
	

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
