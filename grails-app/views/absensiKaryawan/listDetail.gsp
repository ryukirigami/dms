
<%@ page import="com.kombos.hrd.AbsensiKaryawan" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'absensiKaryawanDetail.label', default: 'AbsensiKaryawan')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			$(function(){
                hapus = function (id){
                    $.ajax({
                        url:'${request.contextPath}/absensiKaryawan/hapusDetail',
                        type: "POST", // Always use POST when deleting data
                        data : {id:id},
                        success : function(data){
                            toastr.success("Hapus Sukses");
		                    absensiKaryawanTable.fnDraw();
                            reloadAbsensiKaryawanTable();
                        }
                    });
                }

                save = function(){
                    var ket = $("#keteranganEdit").val();
                    var jam = $("#jam").val();
                    var menit = $("#menit").val();
                    var tgl = $("#tgl").val();
                    var status = $("#statusEdit").val();
                    var id =  $("#id").val();
                    $.ajax({
                        url:'${request.contextPath}/absensiKaryawan/updateDetail',
                        type: "POST", // Always use POST when deleting data
                        data : {id:id,ket:ket,jam:jam,menit:menit,status:status,tgl:tgl},
                        success : function(data){
                            toastr.success("Ubah Sukses");
		                    absensiKaryawanTable.fnDraw();
                            reloadAbsensiKaryawanTable();
                        }
                    });
                }

            });
        </g:javascript>
	</head>
	<body>

	<div class="box">
		<div class="span12" id="absensiKaryawanDetail-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <fieldset>
                <legend>Absensi Detail</legend>
                <table style="padding-right: 10px">
                    <tr>
                        <td style="width: 130px">
                            <label class="control-label" for="t951Tanggal">
                                Periode
                            </label>
                        </td>
                        <td>
                            <label class="control-label" for="t951Tanggal">
                                ${bulanTahun}
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 130px">
                            <label class="control-label" for="t951Tanggal">
                                Nama Karyawan
                            </label>
                        </td>
                        <td>
                            <label class="control-label" for="t951Tanggal">
                                ${absensi.karyawan.nama}
                            </label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <g:render template="dataTablesDetail" />
		</div>
	</div>
    <button class="btn cancel" data-dismiss="modal" name="close" >Close</button>
</body>
</html>
