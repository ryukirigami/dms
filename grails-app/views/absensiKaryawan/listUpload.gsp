
<%@ page import="com.kombos.hrd.AbsensiKaryawan" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'absensiKaryawanDetail.label', default: 'AbsensiKaryawan')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
        <g:if test="${jsonData}">
            <g:javascript>
                <g:if test="${jmlhDataError && jmlhDataError>0}">
                    $('#save').attr("disabled", true);
                </g:if>
                var saveForm;
                $(function(){

                    function progress(e){
                        if(e.lengthComputable){
                            //kalo mau pake progress bar
                            //$('progress').attr({value:e.loaded,max:e.total});
                        }
                    }

                    saveForm = function() {
                    var sendData = ${jsonData};
                <g:if test="${jsonData}">
                    <g:if test="${jmlhDataError==0}">
                        conf = confirm("Apakah Anda Akan Menyimpan?");
                    </g:if>

                    if(conf){
                        $.ajax({
                            url:'${request.getContextPath()}/absensiKaryawan/saveUpload',
                                type: 'POST',
                                xhr: function() {
                                    var myXhr = $.ajaxSettings.xhr();
                                    if(myXhr.upload){
                                        myXhr.upload.addEventListener('progress',progress, false);
                                    }
                                    return myXhr;
                                },
                                //add beforesend handler to validate or something
                                //beforeSend: functionname,
                                success: function (res) {
                                    reloadAbsensiKaryawanTable();
                                    toastr.success("Succes");
                                },
                                //add error handler for when a error occurs if you want!
                                error: function (data, status, e){
                                    alert(e);
                                },
                                complete: function(xhr, status) {

                                },
                                data: JSON.stringify(sendData),
                                contentType: "application/json; charset=utf-8",
                                traditional: true,
                                cache: false
                            });
                        }
                </g:if>
                <g:else>
                    alert('No File Selected');
                </g:else>
                }
             });
            </g:javascript>
        </g:if>
	</head>
	<body>

	<div id="absensiTable">
		<div class="span12" id="absensiKaryawanDetail-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <fieldset>
                <legend>Upload Absensi</legend>
                <form id="uploadAbsensiKaryawan-save" class="form-vertical" action="${request.getContextPath()}/absensiKaryawan/saveUpload" method="post" onsubmit="submitForm();return false;">
                <table style="padding-right: 10px">
                    <tr>
                        <td colspan="3">
                            <a href="${request.getContextPath()}/formatFileUpload/UploadAbsen.xls" >* File Example Upload</a>
                            <br/><br/>
                        </td>
                    </tr>

                    </tr>
                    <tr>
                        <td style="width: 50px">
                            <label class="control-label" for="t951Tanggal">
                                Upload :
                            </label>
                        </td>
                        <td style="width: 250px">
                            <label class="control-label" for="t951Tanggal">
                                <input type="file" required="" id="fileExcel" name="fileExcel" accept="application/excel|application/vnd.ms-excel" />
                            </label>
                        </td>
                        <td>
                            <label class="control-label" for="t951Tanggal">
                                <g:submitButton class="btn btn-primary create" name="viewUploadData" id="viewUploadData" value="${message(code: 'default.button.view.label', default: 'Upload')}" />
                                <g:field type="button" onclick="saveForm();" data-dismiss="modal" class="btn btn-primary create" name="save" id="save" value="${message(code: 'default.button.upload.label', default: 'Simpan')}" />
                            </label>
                        </td>
                    </tr>
                </table>
               </form>
            </fieldset>
            <br>

            <table class="display table table-striped table-bordered table-hover dataTable" width="100%" cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px; width: 1000px;">
                <tr>
                    <th>
                       Nama
                    </th>
                    <th>
                        Jam Masuk
                    </th>
                    <th>
                        Status
                    </th>

                    <th>
                        Terminal
                    </th>

                </tr>
                <g:if test="${htmlData}">
                    ${htmlData}
                </g:if>
                <g:else>
                    <tr class="odd">
                        <td class="dataTables_empty" valign="top" colspan="5">No data available in table</td>
                    </tr>
                </g:else>
            </table>
		</div>
    <button class="btn cancel" data-dismiss="modal" name="close" >Close</button>
	</div>
</body>
<g:javascript>
                        var submitForm;
                        $(function(){

                            function progress(e){
                                if(e.lengthComputable){
                                    //kalo mau pake progress bar
                                    //$('progress').attr({value:e.loaded,max:e.total});
                                }
                            }

                            submitForm = function() {
//                            stopPropagation();
                                var form = new FormData($('#uploadAbsensiKaryawan-save')[0]);
                                $.ajax({
                                    url:'${request.getContextPath()}/absensiKaryawan/viewUpload',
                                    type: 'POST',
                                    xhr: function() {
                                        var myXhr = $.ajaxSettings.xhr();
                                        if(myXhr.upload){
                                            myXhr.upload.addEventListener('progress',progress, false);
                                        }
                                        return myXhr;
                                    },
                                    //add beforesend handler to validate or something
                                    //beforeSend: functionname,
                                    success: function (res) {
                                        $('#absensiTable').empty();
                                        $('#absensiTable').html(res);
                                        //reloadOperationTable();
                                    },
                                    //add error handler for when a error occurs if you want!
                                    error: function (data, status, e){
                                        alert(e);
                                    },
                                    complete: function(xhr, status) {

                                    },
                                    data: form,
                                    cache: false,
                                    contentType: false,
                                    processData: false
                                });

                            }
                        });
</g:javascript>
</html>
