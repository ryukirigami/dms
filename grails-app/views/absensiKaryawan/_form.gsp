<%@ page import="com.kombos.administrasi.CompanyDealer; com.kombos.hrd.AbsensiKaryawan; com.kombos.hrd.Karyawan" %>


<g:javascript>
    $(function() {
        $("#btnCariKaryawan").click(function() {
            oFormService.fnShowKaryawanDataTable();
        })
    })
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: absensiKaryawanInstance, field: 'karyawan', 'error')} ">
    <label class="control-label" for="karyawan">
        <g:message code="absensiKaryawan.karyawan.label" default="Karyawan"/>

    </label>

    <div class="controls">
        <div class="input-append">
            <g:hiddenField name="karyawan.id" readonly="readonly" id="karyawan_id" value="${historyKaryawanInstance?.karyawan?.id}"/>
            <input type="text" value="${absensiKaryawanInstance?.karyawan?.nama}" readonly id="karyawan_nama"/>
            <span class="add-on">
                <a href="javascript:void(0);" id="btnCariKaryawan">
                    <i class="icon-search"/>
                </a>
            </span>
        </div>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: absensiKaryawanInstance, field: 'bulanTahun', 'error')} ">
    <label class="control-label" for="bulanTahun">
        <g:message code="absensiKaryawan.bulanTahun.label" default="Periode"/>

    </label>

    <div class="controls">
     <select name="bulan" id="bulan" value="${skrg?.format("MM")}" style="width: 100px" >
            <option value="01" ${skrg?.format("MM")=="01" ? "selected" : ""}>Januari</option>
            <option value="02" ${skrg?.format("MM")=="02" ? "selected" : ""}>Februari</option>
            <option value="03" ${skrg?.format("MM")=="03" ? "selected" : ""}>Maret</option>
            <option value="04" ${skrg?.format("MM")=="04" ? "selected" : ""}>April</option>
            <option value="05" ${skrg?.format("MM")=="05" ? "selected" : ""}>Mei</option>
            <option value="06" ${skrg?.format("MM")=="06" ? "selected" : ""}>Juni</option>
            <option value="07" ${skrg?.format("MM")=="07" ? "selected" : ""}>Juli</option>
            <option value="08" ${skrg?.format("MM")=="08" ? "selected" : ""}>Agustus</option>
            <option value="09" ${skrg?.format("MM")=="09" ? "selected" : ""}>September</option>
            <option value="10" ${skrg?.format("MM")=="10" ? "selected" : ""}>Oktober</option>
            <option value="11" ${skrg?.format("MM")=="11" ? "selected" : ""}>Nomvember</option>
            <option value="12" ${skrg?.format("MM")=="12" ? "selected" : ""}>Desember</option>
        </select>





            <select name="tahun" id="tahun" style="width: 100px">
                %{
                    for(int i=(new Date().format("yyyy").toInteger());i>=2000;i--){
                        out.print('<option value="'+i+'">'+i+'</option>');
                    }
                }%
            </select>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: absensiKaryawanInstance, field: 'karyawan', 'error')} ">
    <label class="control-label" for="karyawan">
        <g:message code="absensiKaryawan.karyawan.label" default="Terminal"/>

    </label>

    <div class="controls">


        <g:select id="terminal" name="terminal.id" from="${com.kombos.hrd.Terminal.createCriteria().list {eq("companyDealer",session?.userCompanyDealer)}}" optionKey="id" required=""
                  value="${absensiKaryawanInstance?.karyawan?.id}" class="many-to-one"/>
    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: absensiKaryawanInstance, field: 'leave', 'error')} ">
    <label class="control-label" for="leave">
        <g:message code="absensiKaryawan.leave.label" default="Tanggal"/>

    </label>

    <div class="controls">
        <ba:datePicker name="tanggal" id="tanggal" precision="day" value="${new java.util.Date()}"  format="dd/mm/yyyy" required="true" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: absensiKaryawanInstance, field: 'permit', 'error')} ">
    <label class="control-label" for="permit">
        <g:message code="absensiKaryawan.permit.label" default="Status"/>

    </label>

    <div class="controls">
        <select name="status" id="status">
            <option value="1">Masuk</option>
            <option value="3">Istirahat</option>
            <option value="4">Masuk Istirahat</option>
            <option value="2">Pulang</option>
            <option value="5">Ijin</option>
            <option value="6">Alpa</option>
            <option value="7">Sakit</option>
        </select>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: absensiKaryawanInstance, field: 'overtime', 'error')} ">
    <label class="control-label" for="overtime">
        <g:message code="absensiKaryawan.overtime.label" default="Jam"/>

    </label>

    <div class="controls">

        <select id="jam" name="jam" style="width: 60px" >
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                            if(i==8){
                                out.println('<option value="'+i+'" selected>0'+i+'</option>');
                            }else{
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                    } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                    }

                }
            }%
        </select> H :
        <select id="menit" name="menit" style="width: 60px">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                            out.println('<option value="'+i+'">0'+i+'</option>');
                    } else {
                             out.println('<option value="'+i+'">'+i+'</option>');
                    }
                }
            }%
        </select> m
    </div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: absensiKaryawanInstance, field: 'permit', 'error')} ">
    <label class="control-label" for="permit">
        <g:message code="absensiKaryawan.permit.label" default="Ket"/>

    </label>

    <div class="controls">
       <g:textArea name="keterangan" >-</g:textArea>
    </div>
</div>


    <!-- Karyawan Modal -->

    <div id="karyawanModal" class="modal fade">
        <div class="modal-dialog" style="width: 1100px;">
            <div class="modal-content" style="width: 1100px;">
                <div class="modal-header">
                    <a class="close" href="javascript:void(0);" data-dismiss="modal">×</a>
                    <h4>Data Karyawan - List</h4>
                </div>
                <!-- dialog body -->
                <div class="modal-body" id="karyawanModal-body" style="max-height: 1000px;">
                    <div class="box">
                        <div class="span12">
                            <fieldset>
                                <table>
                                    <tr style="display:table-row;">
                                        <td style="width: 130px; display:table-cell; padding:5px;">
                                            <label class="control-label" for="sCriteria_nama">Nama Karyawan</label>
                                        </td>
                                        <td style="width: 130px; display:table-cell; padding:5px;">
                                            <input type="text" id="sCriteria_nama">
                                        </td>
                                        <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                            <td style="width: 130px; display:table-cell; padding:5px;">
                                                <g:select name="sCriteria_cabang" id="sCriteria_cabang" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" noSelection="['':'SEMUA CABANG']"/>
                                            </td>
                                        </g:if>
                                        <td>
                                            <a id="btnSearchKaryawan" class="btn btn-primary" href="javascript:void(0);">
                                                Cari
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </div>
                    </div>
                    <g:render template="dataTablesKaryawan" />
                </div>

                <div class="modal-footer">
                    <a href="javascript:void(0);" class="btn cancel" data-dismiss="modal">
                        Tutup
                    </a>
                    <a href="javascript:void(0);" id="btnPilihKaryawan" class="btn btn-primary">
                        Pilih Karyawan
                    </a>
                </div>
            </div>
        </div>
    </div>
