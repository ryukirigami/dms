
<%@ page import="com.kombos.parts.StokOPName" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>


<table id="bPProblemFindingList_datatables_${idTable}" cellpadding="0" cellspacing="0" border="0"
       class="display table table-striped table-bordered table-hover table-selectable" style="table-layout: fixed; ">
    <thead>
    <tr>
        <th></th>
        <th style="border-bottom: none;padding: 5px;">
            <g:message code="wo.t401NoWo.label" default="Nomor WO"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="wo.noPolisi.label" default="Nomor Polisi"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="wo.tanggalWO.label" default="Tanggal WO"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="wo.start.label" default="Start"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="wo.stop.label" default="Stop"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="bPProblemFindingList.finalInspection.label" default="Final Inspection"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="bPProblemFindingList.sa.label" default="SA"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="bPProblemFindingList.janjiPenyerahan.label" default="Janji Penyerahan"/>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
var bPProblemFindingListTable;
var reloadBPProblemFindingListTable;
$(function(){
    var anOpen = [];
    $('#bPProblemFindingList_datatables_${idTable} td.control').live('click',function () {
            var nTr = this.parentNode;
            var i = $.inArray( nTr, anOpen );
            if ( i === -1 ) {
                $('i', this).attr( 'class', "icon-minus" );
                var oData = bPProblemFindingListTable.fnGetData(nTr);
                $('#spinner').fadeIn(1);
                $.ajax({type:'POST',
                    data : oData,
                    url:'${request.contextPath}/BPProblemFindingList/sub1list',
                    success:function(data,textStatus){
                        var nDetailsRow = bPProblemFindingListTable.fnOpen(nTr,data,'details');
                        $('div.innerDetails', nDetailsRow).slideDown();
                        anOpen.push( nTr );
                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){},
                    complete:function(XMLHttpRequest,textStatus){
                        $('#spinner').fadeOut();
                    }
                });

            } else {
                $('i', this).attr( 'class', 'icon-plus' );
                $('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
                    bPProblemFindingListTable.fnClose( nTr );
                    anOpen.splice( i, 1 );
                });
            }
        });


	reloadBPProblemFindingListTable = function() {
		bPProblemFindingListTable.fnDraw();
	}


    bPProblemFindingListTable = $('#bPProblemFindingList_datatables_${idTable}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"bDestroy": true,
		"aoColumns": [
{
   "mDataProp": null,
   "sClass": "control center",
   "bSortable": false,
   "sWidth":"10px",
   "sDefaultContent": '<i class="icon-plus"></i>'
}
,
{
	"sName": "t401NoWO",
	"mDataProp": "t401NoWO",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
	    return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"><input type="hidden" name="nomorStok" id="nomorStok" value="'+data+'">&nbsp;&nbsp;'+data;
	},
	"bSortable": false,
	"sWidth":"230px",
	"bVisible": true
}
,
{
	"sName": "noPolisi",
	"mDataProp": "noPolisi",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"95px",
	"bVisible": true
}
,
{
	"sName": "t401TanggalWO",
	"mDataProp": "t401TanggalWO",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"125px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "start",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"135px",
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "stop",
	"aTargets": [4],
	"sWidth":"135px",
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "finalInspection",
	"aTargets": [5],
	"sWidth":"135px",
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "t401NamaSA",
	"mDataProp": "t401NamaSA",
	"aTargets": [6],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}
,
{
	"sName": "t401TglJamJanjiPenyerahan",
	"mDataProp": "t401TglJamJanjiPenyerahan",
	"aTargets": [7],
	"bSortable": false,
	"sWidth":"135px",
	"bVisible": true
}
],
    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
    
                        var Tanggal = $('#search_Tanggal').val();
						var TanggalDay = $('#search_Tanggal_day').val();
						var TanggalMonth = $('#search_Tanggal_month').val();
						var TanggalYear = $('#search_Tanggal_year').val();

	                    var Tanggalakhir = $('#search_Tanggalakhir').val();
                        var TanggalDayakhir = $('#search_Tanggalakhir_day').val();
                        var TanggalMonthakhir = $('#search_Tanggalakhir_month').val();
                        var TanggalYearakhir = $('#search_Tanggalakhir_year').val();


                        if(Tanggal){
                            aoData.push(
                                    {"name": 'sCriteria_Tanggal', "value": "date.struct"},
                                    {"name": 'sCriteria_Tanggal_dp', "value": Tanggal},
                                    {"name": 'sCriteria_Tanggal_day', "value": TanggalDay},
                                    {"name": 'sCriteria_Tanggal_month', "value": TanggalMonth},
                                    {"name": 'sCriteria_Tanggal_year', "value": TanggalYear}
                            );
                        }

                        if(Tanggalakhir){
                            aoData.push(
                                    {"name": 'sCriteria_Tanggalakhir', "value": "date.struct"},
                                    {"name": 'sCriteria_Tanggalakhir_dp', "value": Tanggalakhir},
                                    {"name": 'sCriteria_Tanggalakhir_day', "value": TanggalDayakhir},
                                    {"name": 'sCriteria_Tanggalakhir_month', "value":TanggalMonthakhir},
                                    {"name": 'sCriteria_Tanggalakhir_year', "value": TanggalYearakhir}
                            );
                        }


						var staProblem = $('#staProblem').val();
						if(staProblem){
							aoData.push(
									{"name": 'sCriteria_staProblem', "value": staProblem}
							);
						}

    $.ajax({ "dataType": 'json',
        "type": "POST",
        "url": sSource,
        "data": aoData ,
        "success": function (json) {
            fnCallback(json);
           },
        "complete": function () {
           }
    });
}
});
});

    $('#pencilEdit').click(function(){
        console.log('masuk pencil gan')
    });
</g:javascript>
