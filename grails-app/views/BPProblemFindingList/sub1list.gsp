
<%@ page import="com.kombos.parts.StokOPName" %>

<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
<r:require modules="baseapplayout" />
<g:javascript>
    var bPProblemFindingListSub1Table;
$(function(){
    var anOpen = [];
	$('#bPProblemFindingList_datatables_sub1_${idTable} td.sub1control').live('click',function () {
	console.log('click sub 1')
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
  		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = bPProblemFindingListSub1Table.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST', 
	   			data : oData,
	   			url:'${request.contextPath}/BPProblemFindingList/sub2list',
	   			success:function(data,textStatus){
	   				var nDetailsRow = bPProblemFindingListSub1Table.fnOpen(nTr,data,'details');
    				$('div.inner2Details', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
    		
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.inner2Details', $(nTr).next()[0]).slideUp( function () {
      			bPProblemFindingListSub1Table.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

	bPProblemFindingListSub1Table = $('#bPProblemFindingList_datatables_sub1_${idTable}').dataTable({
		"sScrollX": "1205px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSub1List")}",
		"aoColumns": [
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"10px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "sClass": "sub1control center",
   "bSortable": false,
   "sWidth":"10px",
   "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "operation",
	"mDataProp": "namaJob",
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"1185px",
	"bVisible": true
}
,
{
	"sName": "t401NoWO",
	"mDataProp": "t401NoWO",
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"0px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						aoData.push(
									{"name": 't401NoWO', "value": "${t401NoWO}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>

</head>
    <body>
        <div class="innerDetails">
            <table id="bPProblemFindingList_datatables_sub1_${idTable}" cellpadding="0" cellspacing="0" border="0"
               class="display table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th></th>

                        <th></th>

                        <th style="border-bottom: none;padding: 5px;text-align: center">
                            <div><g:message code="bPProblemFindingList.namaJob.label" default="Nama Job" /></div>
                        </th>
                    </tr>
                </thead>
            </table>

        </div>
    </body>
</html>


			
