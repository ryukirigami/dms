
<%@ page import="com.kombos.customerprofile.FA; com.kombos.administrasi.Operation; com.kombos.parts.Returns" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="wo.respondProblem.label" default="Respond Problem Finding" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var save;
	var loadForm;
	var adaga = '${reception?.t401NoWO}'
    $(function(){

        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

        $('.box-action').click(function(){
            return false;
        });


   });
    $(document).ready(function()
    {
            if(adaga){
                isiData('${reception?.t401NoWO}');
            }

            function isiData(noWo){
//            reloadButuhPartsTable();
            var id=noWo;
            $.ajax({
                url:'${request.contextPath}/BPProblemFindingList/getTableData',
                type: "POST",
                data: { noWO : id },
                success : function(data){
                    if(data.length>0){
                        $.each(data,function(i,item){
                            butuhPartsTable.fnAddData({
                                'kodeParts': item.kodeParts,
                                'namaParts': item.namaParts,
                                'qty': item.qty
                            });
                        });
                    }
                }
            });
        }

         save = function(){
            var noWO = $('#nomorWO').val();
            var respon = $('#responProblem').val();
            if(respon=="" || respon==null){
                alert('Data respon harus di isi')
            }else{
                $.ajax({
                url:'${request.contextPath}/BPProblemFindingList/save',
                type: "POST", // Always use POST when deleting data
                data : {noWO : noWO , respon : respon},
                success : function(data){
                    toastr.success('<div>Data Saved.</div>');
                    expandTableLayout();
                    reloadBPProblemFindingListTable();
                },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });
            }


        }
    });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="wo.respondProblem.label" default="Respond Problem Finding" /></span>
</div>
<div class="box">
    <div class="row-fluid" style="width: 100%">
        <div id="kiri" class="span5">
            <table style="width: 100%;">
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.t401NoWo.label" default="Nomor WO" />
                    </td>
                    <td>
                        <g:textField style="width:100%" name="nomorWO" id="nomorWO" readonly="" value="${reception?.t401NoWO}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.noPolisi.label" default="Nomor Polisi" />
                    </td>
                    <td>
                        <g:textField style="width:100%" name="nomorPolisi" readonly="" value="${reception?.historyCustomerVehicle?.fullNoPol}"/>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.modelKendaraan.label" default="Model Kendaraan" />
                    </td>
                    <td>
                        <g:textField style="width:100%" name="modelKendaraan" readonly="" value="${reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel}"/>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.stall.label" default="Nama Stall" />
                    </td>
                    <td>
                        <g:textField style="width:100%" name="stall" readonly="" value="${jpb?.stall?.m022NamaStall}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.teknisi.label" default="Teknisi" />
                    </td>
                    <td>
                        <g:textField style="width:100%" name="teknisi" readonly="" value="${teknisi}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.foreman.label" default="Foreman" />
                    </td>
                    <td>
                        <g:textField style="width:100%" name="foreman" id="foreman" readonly="" value="${foreman}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.job.label" default="Job" />
                    </td>
                    <td>
                        <g:textArea style="width:100%;resize: none" name="job" readonly="" value="${actual?.operation?.m053NamaOperation}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.proses.label" default="Proses" />
                    </td>
                    <td>
                        <g:textField name="proses" style="width:100%;" readonly="" value="${jpb?.stall?.namaProsesBP?.m190NamaProsesBP}"/>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.catatanProblem.label" default="Catatan Problem" />
                    </td>
                    <td>
                        <g:textArea style="width:100%;resize: none" name="catatanProblem" readonly="" value="${actual?.t452Ket}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.tanggalProblem.label" default="Tanggal Problem" />
                    </td>
                    <td>
                        <g:textField name="tanggalProblem" readonly="" value="${masalah?.t501TglUpdate}" style="width:100%" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.jenisProblem.label" default="Jenis Problem" />
                    </td>
                    <td>
                        <g:textField style="width:100%" name="jenisProblem" readonly="" value="${masalah?.t501staProblemTeknis=="1" ? "Teknis" : "Non Teknis"}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.catatanTambahan.label" default="Catatan Tambahan" />
                    </td>
                    <td>
                        <g:textArea style="width:100%;resize: none" name="catatanTambahan" readonly="" value="${masalah?.t501CatatanTambahan}" />
                    </td>
                </tr>
            </table>
        </div>

        <div id="kanan" class="span7">
            <table style="width: 100%;">
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.statusKendaraan.label" default="Status Kendaraan" />
                    </td>
                    <td>
                        <g:textField name="staKendaraan" style="width:100%" readonly="" value="${progress?.statusKendaraan?.m999NamaStatusKendaraan}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.tambahanWaktu.label" default="Perlu Tambahan Waktu?" />
                    </td>
                    <td>
                        <g:textField name="perluTambahWaktu" style="width:100%" readonly="" value="${masalah?.t501StaBthTambahWkt=="1" ? "Ya" : "Tidak"}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.perluTambahJob.label" default="Perlu Tambahan Job?" />
                    </td>
                    <td>
                        <g:textField name="perluTambahJob" style="width:100%" readonly="" value="${masalah?.t501StaBthJob=="1" ? "Ya" : "Tidak"}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.butuhParts.label" default="Butuh Parts / Material?" />
                    </td>
                    <td>
                        <g:textField name="butuhParts" style="width:100%" readonly="" value="${masalah?.t501StaBthParts=="1" ? "Ya" : "Tidak"}" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br/>
                        <span style="text-decoration: underline;">Parts / Material yang di butuhkan</span>
                        <br/><br/>
                        <g:render template="dataTablesButuhParts"/>
                        <br/>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.butuhCat.label" default="Butuh Cat" />
                    </td>
                    <td>
                        <g:textField name="butuhCat" style="width:100%" readonly="" value="${masalah?.t501StaButuhCat=="1" ? "Ya" : "Tidak"}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.tagihKe.label" default="Tagih Kepada" />
                    </td>
                    <td>
                        <g:textField name="tagihKe" style="width:100%" readonly="" value="${masalah?.t501StaTagihKepada=="1" ? "Bengkel(Vendor)" :"Vendor"}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.namaVendor.label" default="Nama Vendor" />
                    </td>
                    <td>
                        <g:textField name="perluTambahJob" style="width:100%" readonly="" value="${masalah?.vendorCat?.m191Nama}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.hasilCariParts.label" default="Hasil Pencarian Parts yang dibutuhkan" />
                    </td>
                    <td>
                        <g:textArea name="hasilCariParts" style="width:100%;resize: none" readonly="" value="${masalah?.t501HasilDSS}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.responProblem.label" default="Respon Terhadap Problem" />
                    </td>
                    <td>
                        <g:textArea name="responProblem" id="responProblem" style="width:100%;resize: none" value="" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.tanggalUpdate.label" default="Tanggal Update" />
                    </td>
                    <td>
                        <g:textField name="tanggalUpdate" style="width:100%" readonly="" value="${tanggalUpdate}" />
                    </td>
                </tr>
            </table>

        </div>
    </div>
    <div style="margin-left: 100px" >
        <a class="btn cancel" href="javascript:void(0);" onclick="save();">Respond</a>
        <a class="btn cancel" href="javascript:void(0);"onclick="window.location.href ='#/home';"><g:message code="default.button.cancel.label" default="Close" /></a>
    </div>
</div>
</div>
</body>
</html>
