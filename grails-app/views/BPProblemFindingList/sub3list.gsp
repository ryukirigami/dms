
<%@ page import="com.kombos.parts.StokOPName" %>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript>
    var bPProblemFindingListSub3Table;
$(function(){

    var anOpen = [];
    $('#bPProblemFindingList_datatables_sub3_${idTable} td.sub3control').live('click',function () {
        console.log('click sub 3')
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = bPProblemFindingListSub3Table.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${request.contextPath}/BPProblemFindingList/sub4list',
	   			success:function(data,textStatus){
	   				var nDetailsRow = bPProblemFindingListSub3Table.fnOpen(nTr,data,'details');
    				$('div.inner4Details', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});

  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.inner4Details', $(nTr).next()[0]).slideUp( function () {
      			bPProblemFindingListSub3Table.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

	bPProblemFindingListSub3Table = $('#bPProblemFindingList_datatables_sub3_${idTable}').dataTable({
		"sScrollX": "1205px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSub3List")}",
		"aoColumns": [
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"12px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"12px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"12px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "sClass": "sub3control center",
   "bSortable": false,
   "sWidth":"10px",
    "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "",
	"mDataProp": "rolePengirim",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"194px",
	"bVisible": true
}
,

{
	"sName": "",
	"mDataProp": "namaPengirim",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"194px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "tanggalKirim",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"194px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "deskMasalah",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"194px",
	"bVisible": true
}
,

{
	"sName": "",
	"mDataProp": "tanggalSolved",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"194px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "status",
	"aTargets": [5],
	"mRender": function ( data, type, row ) {
	    if(data=="Solved"){
            return '<span style="color:green">' + data + '</span>';
	    }else{
	        return '<span><a style="color:red" href="javascript:void(0);" onclick="if(confirm(\' Are You Sure?\' )){link(\''+row['t401NoWO']+'\',this)}">' + data + '</a></span>';
	    }
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"195px",
	"bVisible": true
}
,
{
	"sName": "t401NoWO",
	"mDataProp": "t401NoWO",
	"aTargets": [6],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"0px",
	"bVisible": false
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						aoData.push(
									{"name": 't401NoWO', "value": "${t401NoWO}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
    </g:javascript>

</head>
<body>
<div class="inner3Details">
    <table id="bPProblemFindingList_datatables_sub3_${idTable}" cellpadding="0" cellspacing="0" border="0"
           class="display table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th></th>

            <th></th>

            <th></th>

            <th></th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPProblemFindingList.roleKirim.label" default="Role Pengirim" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPProblemFindingList.namaKirim.label" default="Nama Pengirim" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPProblemFindingList.tanggalKirim.label" default="Tanggal Kirim" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPProblemFindingList.deskripsiMasalah.label" default="Deskripsi Masalah" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPProblemFindingList.tanggalSolved.label" default="Tanggal Solved" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPProblemFindingList.staProblem.label" default="Status Problem" /></div>
            </th>

        </tr>
        </thead>
    </table>

</div>
</body>
</html>


			
