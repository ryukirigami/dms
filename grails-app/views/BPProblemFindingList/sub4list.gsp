
<%@ page import="com.kombos.parts.StokOPName" %>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript>
    var bPProblemFindingListSub4Table;
$(function(){

	bPProblemFindingListSub4Table = $('#bPProblemFindingList_datatables_sub4_${idTable}').dataTable({
		"sScrollX": "1205px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSub4List")}",
		"aoColumns": [
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"12px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"12px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"11px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"13px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"11px",
   "sDefaultContent": ''
},
{
	"sName": "",
	"mDataProp": "roleRespon",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"231px",
	"bVisible": true
}
,

{
	"sName": "",
	"mDataProp": "namaResponder",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"231px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "tanggalRespon",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"231px",
	"bVisible": true
}
,

{
	"sName": "",
	"mDataProp": "respon",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"231px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "solusi",
	"aTargets": [4],
	"mRender": function ( data, type, row ) {
	    if(data=="Ya"){
            return '<span style="color:green">' + data + '</span>';
	    }else{
	        return '<span><a style="color:black" href="javascript:void(0);" onclick="if(confirm(\' Are You Sure?\' )){link2(\''+row['id']+'\',this)}">' + data + '</a></span>';
	    }
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"231px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						aoData.push(
									{"name": 't401NoWO', "value": "${t401NoWO}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
    </g:javascript>

</head>
<body>
<div class="inner4Details">
    <table id="bPProblemFindingList_datatables_sub4_${idTable}" cellpadding="0" cellspacing="0" border="0"
           class="display table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th></th>

            <th></th>

            <th></th>

            <th></th>

            <th></th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPProblemFindingList.roleRespon.label" default="Role Respon" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPProblemFindingList.namaResponden.label" default="Nama Responden" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPProblemFindingList.tglRespon.label" default="Tanggal Respon" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPProblemFindingList.deskripsiRespon.label" default="Deskripsi Respon" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPProblemFindingList.namaResponden.label" default="Solusi?" /></div>
            </th>
        </tr>
        </thead>
    </table>

</div>
</body>
</html>


			
