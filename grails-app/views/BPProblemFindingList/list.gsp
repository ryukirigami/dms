
<%@ page import="com.kombos.parts.StokOPName" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="bPProblemFindingList.label" default="Body & Paint - Problem Finding List"/></title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>
			var show;
			var edit;
			var WODetail;
			var respondProblem;
			$(function(){

			    link = function(id,itemLink){
                    var nRow = id;
                    $('#spinner').fadeIn(1);
                    $.ajax({type:'POST', url:'${request.contextPath}/BPProblemFindingList/ubah',
                        data: { noWo: nRow},
                        success:function(data,textStatus){
                             $(itemLink).parent().css("color","green");
                             $(itemLink).parent().html(data);
                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
                }
                link2 = function(id,itemLink){
                    var nRow = id;
                    $('#spinner').fadeIn(1);
                    $.ajax({type:'POST', url:'${request.contextPath}/BPProblemFindingList/ubah2',
                        data: { id: nRow},
                        success:function(data,textStatus){
                             $(itemLink).parent().css("color","green");
                             $(itemLink).parent().html(data);
                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });

                }
			    respondProblem = function(){
				    var checkID =[];
                    var id;
                    $('#bPProblemFindingList-table tbody .row-select').each(function() {
                        if(this.checked){
                            var id = $(this).next("input:hidden").val();
                            checkID.push(id);
                        }
                    });

                    if(checkID.length!=1){
                        if(checkID.length<1){
                            alert('Pilih salah satu data');
                        }else{
                            alert('Anda hanya boleh memilih 1 data');
                        }
                        return;
                    }else{
                        var idUbah=checkID[0];
                        shrinkTableLayout();
                        $('#spinner').fadeIn(1);
                        $.ajax({type:'POST', url:'${request.contextPath}/BPProblemFindingList/respondProblem',
                            data : {noWo : idUbah},
                            success:function(data,textStatus){
                                loadForm(data, textStatus);
                            },
                            error:function(XMLHttpRequest,textStatus,errorThrown){},
                            complete:function(XMLHttpRequest,textStatus){
                                $('#spinner').fadeOut();
                            }
                        });
                        }

				};

                shrinkTableLayout = function(){
                    $("#bPProblemFindingList-table").hide()
                    $("#bPProblemFindingList-form").css("display","block");
                }
        
                loadForm = function(data, textStatus){
                    $('#bPProblemFindingList-form').empty();
                    $('#bPProblemFindingList-form').append(data);
                }
        
                expandTableLayout = function(){
                    $("#bPProblemFindingList-table").show()
                    $("#bPProblemFindingList-form").css("display","none");
                }
                
				WODetail = function(){
				    var checkID =[];
                    var id;
                    $('#bPProblemFindingList-table tbody .row-select').each(function() {
                        if(this.checked){
                            var id = $(this).next("input:hidden").val();
                            checkID.push(id);
                        }
                    });

                    if(checkID.length!=1){
                        if(checkID.length<1){
                            alert('Pilih salah satu data untuk melihat WO Detail');
                        }else{
                            alert('Anda hanya boleh memilih 1 data');
                        }
                        return;
                    }else{
                        var idUbah=checkID[0];
                        $.ajax({url: '${request.contextPath}/BPProblemFindingList?idUbah='+idUbah,
                            type:"GET",dataType: "html",
                            complete : function (req, err) {
                                $('#main-content').html(req.responseText);
                                $('#spinner').fadeOut();
                                loading = false;
                            }
                        });
                    }

				};

});

    var checkin = $('#search_Tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
        var newDate = new Date(ev.date)
        newDate.setDate(newDate.getDate() + 1);
        checkout.setValue(newDate);
        checkin.hide();
        $('#search_Tanggalakhir')[0].focus();
    }).data('datepicker');
    
    var checkout = $('#search_Tanggalakhir').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

        return true;
    }

    function clearForm(){
        $('#search_Tanggal').val('')
        $('#search_Tanggalakhir').val('')
        $('#staProblem').val('')
        reloadBPProblemFindingListTable();
    }

    function report(){
        $('#btnReport').click();
    };

    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="bPProblemFindingList.label" default="Body & Paint - Problem Finding List" /></span>
    <ul class="nav pull-right">
    </ul>
</div>
<div class="box">
    <div class="span12" id="bPProblemFindingList-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>

        <table>
            <tr>
                <td style="padding: 5px">
                    <g:message code="wo.tanggalWO.label" default="Tanggal WO"/>
                </td>
                <td style="padding: 5px">
                    <ba:datePicker name="search_Tanggal" precision="day" value="" disable="true" format="dd-MM-yyyy"/>&nbsp;s.d.&nbsp;
                    <ba:datePicker name="search_Tanggalakhir" precision="day" value="" disable="true" format="dd-MM-yyyy"/>
                </td>
                <td style="padding: 5px">
                    <g:message code="bPProblemFindingList.staProblem.label" default="Status Problem Finding"/>
                </td>
                <td style="padding: 5px">
                    <g:select name="staProblem" id="staProblem" from="${['Solved','Not Solved']}" noSelection="${['':'Pilih status problem finding']}" />
                </td>
                <td>&nbsp;&nbsp;
                <g:field type="button" onclick="reloadBPProblemFindingListTable();" style="padding: 5px; width: 100px"
                         class="btn cancel" name="filter" id="filter" value="${message(code: 'default.button.filter.label', default: 'Filter')}" />
                &nbsp;&nbsp;
                <g:field type="button" onclick="clearForm();" style="padding: 5px; width: 100px"
                         class="btn cancel" name="clear" id="clear" value="${message(code: 'default.button.clear.label', default: 'Clear Filter')}" />
                </td>
            </tr>
        </table>
        <br/><br/>
        <g:render template="dataTables" />
        <g:field type="button" onclick="respondProblem();" style="padding: 5px;" disabled="false"
                 class="btn cancel" name="responProblem" id="responProblem" value="${message(code: 'wo.responProblem.label', default: 'Respond Problem')}" />
        <br/>
        <br/>
        <g:field type="button" onclick="return WODetail();" style="padding: 5px;"
                 class="btn cancel" name="woDetail" id="woDetail" value="${message(code: 'wo.woDetail.label', default: 'WO Detail')}" />
        &nbsp;&nbsp;
        <g:field type="button" onclick="" style="padding: 5px;"
                 class="btn cancel" name="runJPB" id="runJPB" value="${message(code: 'wo.runJPB.label', default: 'Run JPB')}" />
        &nbsp;&nbsp;
        <a class="btn cancel"  href="javascript:void(0);" onclick="report();">Export To Excel</a>
        <form action="${request.contextPath}/BPProblemFindingList/report" method="get">
            <div style="display: none;">
                <input type="submit" value="Export To Excel" id="btnReport"/>
            </div>
        </form>
    </div>

    <div class="span11" id="bPProblemFindingList-form" style="display: none;"></div>
</div>
</body>
</html>
