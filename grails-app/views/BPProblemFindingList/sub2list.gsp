
<%@ page import="com.kombos.parts.StokOPName" %>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript>
    var bPProblemFindingListSub2Table;
$(function(){
    var anOpen = [];
	$('#bPProblemFindingList_datatables_sub2_${idTable} td.sub2control').live('click',function () {
	    console.log('click sub 2')
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
  		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = bPProblemFindingListSub2Table.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST', 
	   			data : oData,
	   			url:'${request.contextPath}/BPProblemFindingList/sub3list',
	   			success:function(data,textStatus){
	   				var nDetailsRow = bPProblemFindingListSub2Table.fnOpen(nTr,data,'details');
    				$('div.inner3Details', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
    		
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.inner3Details', $(nTr).next()[0]).slideUp( function () {
      			bPProblemFindingListSub2Table.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

	bPProblemFindingListSub2Table = $('#bPProblemFindingList_datatables_sub2_${idTable}').dataTable({
		"sScrollX": "1205px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSub2List")}",
		"aoColumns": [
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"12px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"12px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "sClass": "sub2control center",
   "bSortable": false,
   "sWidth":"11px",
   "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "",
	"mDataProp": "proses",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"167px",
	"bVisible": true
}
,

{
	"sName": "",
	"mDataProp": "lastProblem",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"167px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "lastRespon",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"167px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "jumlahProblem",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"168px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "foreman",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"168px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "teknisi",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"168px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "status",
	"aTargets": [6],
		"mRender": function ( data, type, row ) {
	    if(data=="Solved"){
            return '<span style="color:green">' + data + '</span>';
	    }else{
	        return '<span><a style="color:red" href="javascript:void(0);" onclick="if(confirm(\' Are You Sure?\' )){link(\''+row['t401NoWO']+'\',this)}">' + data + '</a></span>';
	    }
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"167px",
	"bVisible": true
}
,
{
	"sName": "t401NoWO",
	"mDataProp": "t401NoWO",
	"aTargets": [7],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"0px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						aoData.push(
									{"name": 't401NoWO', "value": "${t401NoWO}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
    </g:javascript>

</head>
<body>
<div class="inner2Details">
    <table id="bPProblemFindingList_datatables_sub2_${idTable}" cellpadding="0" cellspacing="0" border="0"
           class="display table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th></th>

            <th></th>

            <th></th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPProblemFindingList.namaProses.label" default="Nama Proses" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPProblemFindingList.lastProblem.label" default="Last Problem" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPProblemFindingList.lastRespon.label" default="Last Respon" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPProblemFindingList.jumProblem.label" default="Jumlah Problem" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="default.foreman.label" default="Foreman" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="default.teknisi.label" default="Teknisi" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPProblemFindingList.staProblem.label" default="Status Problem" /></div>
            </th>

        </tr>
        </thead>
    </table>

</div>
</body>
</html>


			
