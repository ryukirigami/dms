<%@ page import="com.kombos.hrd.HistoryKaryawan" %>


<g:javascript>
    $(function() {
        $("#btnCariKaryawan").click(function() {
            oFormService.fnShowKaryawanDataTable();
        })

        $('.disable_submit').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });
    })
</g:javascript>

<g:if test="${params.onDataKaryawan}">
    <div class="control-group fieldcontain ${hasErrors(bean: historyKaryawanInstance, field: 'tanggal', 'error')} ">
        <label class="control-label" for="tanggal">
            <g:message code="historyKaryawan.tanggal.label" default="Tanggal" />

        </label>
        <div class="controls">
            <ba:datePicker name="tanggal" precision="day" value="${historyKaryawanInstance?.tanggal}" format="yyyy-MM-dd"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: historyKaryawanInstance, field: 'history', 'error')} ">
        <label class="control-label" for="history">
            <g:message code="historyKaryawan.history.label" default="History" />

        </label>
        <div class="controls">
            <g:select id="history" name="history.id" from="${com.kombos.hrd.History.list()}" optionKey="id" required="" value="${historyKaryawanInstance?.history?.id}" class="many-to-one"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: historyKaryawanInstance, field: 'letterNumber', 'error')} ">
        <label class="control-label" for="letterNumber">
            <g:message code="historyKaryawan.letterNumber.label" default="Letter Number" />

        </label>
        <div class="controls">
            <g:textField name="letterNumber" value="${historyKaryawanInstance?.letterNumber}" />
        </div>
    </div>

</g:if>
%{--tanggal, karyawan, history, letter number, reason, cabang before, cabang after, explanation--}%

<g:if test="${!params.onDataKaryawan}">
    <div class="control-group fieldcontain ${hasErrors(bean: historyKaryawanInstance, field: 'tanggal', 'error')} ">
        <label class="control-label" for="tanggal">
            <g:message code="historyKaryawan.tanggal.label" default="Tanggal" />

        </label>
        <div class="controls">
            <ba:datePicker name="tanggal" precision="day" value="${historyKaryawanInstance?.tanggal}" format="yyyy-MM-dd"/>
        </div>
    </div>


    <div class="control-group fieldcontain ${hasErrors(bean: historyKaryawanInstance, field: 'karyawan', 'error')} ">
        <label class="control-label" for="karyawan">
            <g:message code="historyKaryawan.karyawan.label" default="Karyawan" />

        </label>
        <div class="controls">
            %{--<g:select id="karyawan" name="karyawan.id" from="${com.kombos.hrd.Karyawan.list()}" optionKey="id" optionValue="nama" required="" value="${historyKaryawanInstance?.karyawan?.id}" class="many-to-one"/>--}%
            <div class="input-append">
                <g:hiddenField name="karyawan.id" readonly="readonly" id="karyawan_id" value="${historyKaryawanInstance?.karyawan?.id}"/>
                <input type="text" readonly="readonly" id="karyawan_nama" value="${historyKaryawanInstance?.karyawan?.nama}"/>
                <span class="add-on">
                    <a href="javascript:void(0);" id="btnCariKaryawan">
                        <i class="icon-search"/>
                    </a>
                </span>
            </div>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: historyKaryawanInstance, field: 'history', 'error')} ">
        <label class="control-label" for="history">
            <g:message code="historyKaryawan.history.label" default="History" />

        </label>
        <div class="controls">
            <g:select id="history" name="history.id" from="${com.kombos.hrd.History.list()}" optionKey="id" required="" value="${historyKaryawanInstance?.history?.id}" class="many-to-one"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: historyKaryawanInstance, field: 'letterNumber', 'error')} ">
        <label class="control-label" for="letterNumber">
            <g:message code="historyKaryawan.letterNumber.label" default="Nomor Dokumen" />

        </label>
        <div class="controls">
            <g:textField name="letterNumber" value="${historyKaryawanInstance?.letterNumber}" />
        </div>
    </div>



    <div class="control-group fieldcontain ${hasErrors(bean: historyKaryawanInstance, field: 'reason', 'error')} ">
        <label class="control-label" for="reason">
            <g:message code="historyKaryawan.reason.label" default="Alasan" />

        </label>
        <div class="controls">
            <g:textField name="reason" value="${historyKaryawanInstance?.reason}" />
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: historyKaryawanInstance, field: 'cabangBefore', 'error')} ">
        <label class="control-label" for="cabangBefore">
            <g:message code="historyKaryawan.cabangBefore.label" default="Divisi" />

        </label>
        <div class="controls">
            <g:select id="divisi" name="divisi.id" from="${com.kombos.administrasi.Divisi.list()}" optionKey="id" required="" optionValue="m012NamaDivisi" value="${historyKaryawanInstance?.divisi?.id}" class="many-to-one"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: historyKaryawanInstance, field: 'cabangBefore', 'error')} ">
        <label class="control-label" for="cabangBefore">
            <g:message code="historyKaryawan.cabangBefore.label" default="Cabang Sebelumnya" />

        </label>
        <div class="controls">
            <g:select id="cabangBefore" name="cabangBefore.id" from="${com.kombos.administrasi.CompanyDealer.list()}" optionKey="id" required="" optionValue="m011NamaWorkshop" value="${historyKaryawanInstance?.cabangBefore?.id}" class="many-to-one"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: historyKaryawanInstance, field: 'cabangAfter', 'error')} ">
        <label class="control-label" for="cabangAfter">
            <g:message code="historyKaryawan.cabangAfter.label" default="Cabang Setelahnya" />

        </label>
        <div class="controls">
            <g:select id="cabangAfter" name="cabangAfter.id" from="${com.kombos.administrasi.CompanyDealer.list()}" optionKey="id" required="" optionValue="m011NamaWorkshop" value="${historyKaryawanInstance?.cabangAfter?.id}" class="many-to-one"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: historyKaryawanInstance, field: 'jabatanBefore', 'error')} ">
        <label class="control-label" for="jabatanBefore">
            <g:message code="historyKaryawan.jabatanBefore.label" default="Jabatan Sebelumnya" />

        </label>
        <div class="controls">
            <g:select id="jabatanBefore" name="jabatanBefore.id" from="${com.kombos.administrasi.ManPower.list()}" optionKey="id" required="" optionValue="m014JabatanManPower" value="${historyKaryawanInstance?.jabatanBefore?.id}" class="many-to-one"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: historyKaryawanInstance, field: 'jabatanAfter', 'error')} ">
        <label class="control-label" for="jabatanAfter">
            <g:message code="historyKaryawan.jabatanAfter.label" default="Jabatan Setelahnya" />

        </label>
        <div class="controls">
            <g:select id="jabatanAfter" name="jabatanAfter.id" from="${com.kombos.administrasi.ManPower.list()}" optionKey="id" required="" optionValue="m014JabatanManPower" value="${historyKaryawanInstance?.jabatanAfter?.id}" class="many-to-one"/>
        </div>
    </div>


    <div class="control-group fieldcontain ${hasErrors(bean: historyKaryawanInstance, field: 'explanation', 'error')} ">
        <label class="control-label" for="explanation">
            <g:message code="historyKaryawan.explanation.label" default="Keterangan" />

        </label>
        <div class="controls">
            <g:textField name="explanation" value="${historyKaryawanInstance?.explanation}" />
        </div>
    </div>


    <!-- Karyawan Modal -->

    <div id="karyawanModal" class="modal fade">
        <div class="modal-dialog" style="width: 1200px;">
            <div class="modal-content" style="width: 1200px;">
                <div class="modal-header">
                    <a class="close" href="javascript:void(0);" data-dismiss="modal">×</a>
                    <h4>Data Karyawan - List</h4>
                </div>
                <!-- dialog body -->
                <div class="modal-body" id="karyawanModal-body" style="max-height: 1200px;">
                    <div class="box">
                        <div class="span12">
                            <fieldset>
                                <table>
                                    <tr style="display:table-row;">
                                        <td style="width: 130px; display:table-cell; padding:5px;">
                                            <label class="control-label" for="sCriteria_nama">Nama Karyawan2</label>
                                        </td>
                                        <td style="width: 130px; display:table-cell; padding:5px;">
                                            <input type="text" id="sCriteria_nama" class="disable_submit">
                                        </td>
                                            <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                                <td style="width: 130px; display:table-cell; padding:5px;">
                                                        <g:select name="sCriteria_cabang" id="sCriteria_cabang" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" noSelection="['':'SEMUA CABANG']"/>
                                                </td>
                                            </g:if>
                                        <td >
                                            <a id="btnSearchKaryawan" class="btn btn-primary" href="javascript:void(0);">
                                                Cari
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </div>
                    </div>
                    <g:render template="dataTablesKaryawan" />
                </div>

                <div class="modal-footer">
                    <a href="javascript:void(0);" class="btn cancel"  data-dismiss="modal">
                        Tutup
                    </a>
                    <a href="javascript:void(0);" id="btnPilihKaryawan" class="btn btn-primary">
                        Pilih Karyawan
                    </a>
                </div>
            </div>
        </div>
    </div>


</g:if>

