<%@ page import="com.kombos.hrd.HistoryKaryawan" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'historyKaryawan.label', default: 'HistoryKaryawan')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="create-historyKaryawan" class="content scaffold-create" role="main">
			<legend><g:message code="default.create.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${historyKaryawanInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${historyKaryawanInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form action="save" >--}%
            %{--<g:if test="${params.onDataKaryawan}">
                <g:formRemote class="form-horizontal" name="create" on404="alert('not found!');" update="historyKaryawan-form"
                              url="[controller: 'historyKaryawan', action:'save']">
                    <fieldset class="form">
                        <g:hiddenField name="onDataKaryawan" value="true" />
                        <g:hiddenField name="karyawanId" value="${params.karyawanId}"/>
                        <g:render template="form"/>
                    </fieldset>
                    <fieldset class="buttons controls">
                        <a class="btn cancel" href="javascript:void(0);"
                           onclick="expandTableLayout('historyKaryawan');"><g:message
                                code="default.button.cancel.label" default="Cancel" /></a>
                        <g:submitButton class="btn btn-primary create" name="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                    </fieldset>
                </g:formRemote>
            </g:if>
            <g:else>--}%
                <g:formRemote class="form-horizontal" name="create" on404="alert('not found!');" onSuccess="reloadHistoryKaryawanTable();" update="historyKaryawan-form"
                              url="[controller: 'historyKaryawan', action:'save']">
                    <fieldset class="form">
                        <g:hiddenField name="karyawanId" value="${params.karyawanId}"/>
                        <g:render template="form"/>
                    </fieldset>
                    <fieldset class="buttons controls">
                        <a class="btn cancel" href="javascript:void(0);"
                           onclick="expandTableLayout('historyKaryawan');"><g:message
                                code="default.button.cancel.label" default="Cancel" /></a>
                        <g:submitButton class="btn btn-primary create" name="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                    </fieldset>
                </g:formRemote>

            %{--</g:else>--}%
			%{--</g:form>--}%
		</div>
	</body>
</html>
