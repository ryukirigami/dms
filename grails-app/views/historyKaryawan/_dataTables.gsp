
<%@ page import="com.kombos.administrasi.CompanyDealer; com.kombos.hrd.HistoryKaryawan" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="historyKaryawan_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="historyKaryawan.tanggal.label" default="Tanggal" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="historyKaryawan.karyawan.label" default="Karyawan" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="historyKaryawan.history.label" default="History" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="historyKaryawan.letterNumber.label" default="Nomor Dokumen" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="historyKaryawan.reason.label" default="Alasan" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="historyKaryawan.cabangBefore.label" default="Cabang Sebelumnya" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
				<div><g:message code="historyKaryawan.cabangAfter.label" default="Cabang Setelahnya" /></div>
			</th>



			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="historyKaryawan.explanation.label" default="Keterangan" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="historyKaryawan.explanation.label" default="Approval" /></div>
            </th>

		
		</tr>
		<tr>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_tanggal" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
                    <input type="hidden" name="search_tanggal" value="date.struct">
                    <input type="hidden" name="search_tanggal_day" id="search_tanggal_day" value="">
                    <input type="hidden" name="search_tanggal_month" id="search_tanggal_month" value="">
                    <input type="hidden" name="search_tanggal_year" id="search_tanggal_year" value="">
                    <input type="text" data-date-format="dd/mm/yyyy" name="search_tanggal_dp" value="" id="search_tanggal" class="search_init">
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_karyawan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_karyawan" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_history" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_history" class="search_init" />
                </div>
            </th>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_letterNumber" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_letterNumber" class="search_init" />
                </div>
            </th>



            <th style="border-top: none;padding: 5px;">
                <div id="filter_reason" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_reason" class="search_init" />
                </div>
            </th>



            <th style="border-top: none;padding: 5px;">
                <div id="filter_cabangBefore" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_cabangBefore" class="search_init" />
                </div>
            </th>


            <th style="border-top: none;padding: 5px;">
				<div id="filter_cabangAfter" style="padding-top: 0px;position:relative; margin-top: 0px;width: 185px;">
                    <g:if test="${params.companyDealer.toString().contains("HO")}">
                        <g:select name="search_cabangAfter" id="search_cabangAfter" from="${CompanyDealer.createCriteria().list{eq('staDel','0');order('m011NamaWorkshop')}}" optionKey="id" optionValue="m011NamaWorkshop" style="width: 100%" noSelection="['':'SEMUA']" onchange="reloadHistoryKaryawanTable()" />
                    </g:if>
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_explanation" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_explanation" class="search_init" />
				</div>
			</th>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_approval" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_approval" class="search_init" />
                </div>
            </th>




		</tr>
	</thead>
</table>

<g:javascript>
var historyKaryawanTable;
var reloadHistoryKaryawanTable;
$(function(){
	
	reloadHistoryKaryawanTable = function() {
		historyKaryawanTable.fnDraw();
	}

	
	$('#search_tanggal').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tanggal_day').val(newDate.getDate());
			$('#search_tanggal_month').val(newDate.getMonth()+1);
			$('#search_tanggal_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			historyKaryawanTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	historyKaryawanTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	historyKaryawanTable = $('#historyKaryawan_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [     %{--tanggal, karyawan, history, letter number, reason, cabang before, cabang after, explanation--}%

{
	"sName": "tanggal",
	"mDataProp": "tanggal",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "karyawan",
	"mDataProp": "karyawan",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "history",
	"mDataProp": "history",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "letterNumber",
	"mDataProp": "letterNumber",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "reason",
	"mDataProp": "reason",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "cabangBefore",
	"mDataProp": "cabangBefore",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "cabangAfter",
	"mDataProp": "cabangAfter",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "explanation",
	"mDataProp": "explanation",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "staApproval",
	"mDataProp": "approval",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var cabangAfter = $('#search_cabangAfter').val();
						if(cabangAfter){
							aoData.push(
									{"name": 'sCriteria_cabangAfter', "value": cabangAfter}
							);
						}
	
						var cabangBefore = $('#filter_cabangBefore input').val();
						if(cabangBefore){
							aoData.push(
									{"name": 'sCriteria_cabangBefore', "value": cabangBefore}
							);
						}
	
						var explanation = $('#filter_explanation input').val();
						if(explanation){
							aoData.push(
									{"name": 'sCriteria_explanation', "value": explanation}
							);
						}
	
						var history = $('#filter_history input').val();
						if(history){
							aoData.push(
									{"name": 'sCriteria_history', "value": history}
							);
						}
	
						var karyawan = $('#filter_karyawan input').val();
						if(karyawan){
							aoData.push(
									{"name": 'sCriteria_karyawan', "value": karyawan}
							);
						}

	
						var letterNumber = $('#filter_letterNumber input').val();
						if(letterNumber){
							aoData.push(
									{"name": 'sCriteria_letterNumber', "value": letterNumber}
							);
						}
	
						var reason = $('#filter_reason input').val();
						if(reason){
							aoData.push(
									{"name": 'sCriteria_reason', "value": reason}
							);
						}

						var tanggal = $('#search_tanggal').val();
						var tanggalDay = $('#search_tanggal_day').val();
						var tanggalMonth = $('#search_tanggal_month').val();
						var tanggalYear = $('#search_tanggal_year').val();
						
						if(tanggal){
							aoData.push(
									{"name": 'sCriteria_tanggal', "value": "date.struct"},
									{"name": 'sCriteria_tanggal_dp', "value": tanggal},
									{"name": 'sCriteria_tanggal_day', "value": tanggalDay},
									{"name": 'sCriteria_tanggal_month', "value": tanggalMonth},
									{"name": 'sCriteria_tanggal_year', "value": tanggalYear}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
