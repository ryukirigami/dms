<%@ page import="com.kombos.parts.WarrantyClaim" %>
<r:require module="autoNumeric"/>

<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: warrantyClaimInstance, field: 'goods', 'error')} required">
	<label class="control-label" for="goods">
		<g:message code="warrantyClaim.goods.label" default="Kode Parts" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="goods" name="goods.id" from="${com.kombos.parts.Goods.createCriteria().list(){ eq("staDel", "0"); order("m111ID","asc")}}" optionKey="id" required="" value="${warrantyClaimInstance?.goods?.id}" class="many-to-one"/>
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: warrantyClaimInstance, field: 'm164TglBerlaku', 'error')} required">
    <label class="control-label" for="m164TglBerlaku">
        <g:message code="warrantyClaim.m164TglBerlaku.label" default="Tgl Berlaku" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <ba:datePicker name="m164TglBerlaku" precision="day"  format="dd-mm-yyyy"  value="${warrantyClaimInstance?.m164TglBerlaku}" required="true"  />

    </div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: warrantyClaimInstance, field: 'm164BulanWarranty', 'error')} required">
	<label class="control-label" for="m164BulanWarranty" >
		<g:message code="warrantyClaim.m164BulanWarranty.label" default="Jangka Waktu Warranty (Dalam Bulan)" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m164BulanWarranty" type="number" value="${warrantyClaimInstance.m164BulanWarranty}"  maxlength="2" required=""  onkeypress="return isNumberKey(event)"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: warrantyClaimInstance, field: 'm164KmWarranty', 'error')} required">
	<label class="control-label" for="m164KmWarranty">
		<g:message code="warrantyClaim.m164KmWarranty.label" default="KM" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m164KmWarranty" value="${warrantyClaimInstance.m164KmWarranty}"  required=""  class="angka"/>
	</div>
</div>
<g:javascript>

    $(function(){
        $('.angka').autoNumeric('init',{
            vMin:'0',
            vMax:'1000000',
            mDec: '2',
            aSep:''
        });

    })

</g:javascript>

