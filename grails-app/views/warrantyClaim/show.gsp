

<%@ page import="com.kombos.parts.WarrantyClaim" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'warrantyClaim.label', default: 'Warranty Claim')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteWarrantyClaim;

$(function(){ 
	deleteWarrantyClaim=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/warrantyClaim/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadWarrantyClaimTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-warrantyClaim" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="warrantyClaim"
			class="table table-bordered table-hover">
			<tbody>

            <g:if test="${warrantyClaimInstance?.goods}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="goods-label" class="property-label"><g:message
                                code="warrantyClaim.goods.label" default="Kode Parts" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="goods-label">
                        %{--<ba:editableValue
                                bean="${warrantyClaimInstance}" field="goods"
                                url="${request.contextPath}/WarrantyClaim/updatefield" type="text"
                                title="Enter goods" onsuccess="reloadWarrantyClaimTable();" />--}%

                      ${warrantyClaimInstance?.goods?.encodeAsHTML()}

                    </span></td>

                </tr>
            </g:if>
				<g:if test="${warrantyClaimInstance?.m164TglBerlaku}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m164TglBerlaku-label" class="property-label"><g:message
					code="warrantyClaim.m164TglBerlaku.label" default="Tgl Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m164TglBerlaku-label">
						%{--<ba:editableValue
								bean="${warrantyClaimInstance}" field="m164TglBerlaku"
								url="${request.contextPath}/WarrantyClaim/updatefield" type="text"
								title="Enter m164TglBerlaku" onsuccess="reloadWarrantyClaimTable();" />--}%
							
								<g:formatDate date="${warrantyClaimInstance?.m164TglBerlaku}" />
							
						</span></td>
					
				</tr>
				</g:if>
			

			
				<g:if test="${warrantyClaimInstance?.m164BulanWarranty}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m164BulanWarranty-label" class="property-label"><g:message
					code="warrantyClaim.m164BulanWarranty.label" default="Jangka Waktu Warranty (Dalam Bulan)" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m164BulanWarranty-label">
						%{--<ba:editableValue
								bean="${warrantyClaimInstance}" field="m164BulanWarranty"
								url="${request.contextPath}/WarrantyClaim/updatefield" type="text"
								title="Enter m164BulanWarranty" onsuccess="reloadWarrantyClaimTable();" />--}%
							
								<g:fieldValue bean="${warrantyClaimInstance}" field="m164BulanWarranty"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${warrantyClaimInstance?.m164KmWarranty}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m164KmWarranty-label" class="property-label"><g:message
					code="warrantyClaim.m164KmWarranty.label" default="KM" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m164KmWarranty-label">
						%{--<ba:editableValue
								bean="${warrantyClaimInstance}" field="m164KmWarranty"
								url="${request.contextPath}/WarrantyClaim/updatefield" type="text"
								title="Enter m164KmWarranty" onsuccess="reloadWarrantyClaimTable();" />--}%
							
								<g:fieldValue bean="${warrantyClaimInstance}" field="m164KmWarranty"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${warrantyClaimInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="warrantyClaim.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${warrantyClaimInstance}" field="createdBy"
								url="${request.contextPath}/WarrantyClaim/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadWarrantyClaimTable();" />--}%
							
								<g:fieldValue bean="${warrantyClaimInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${warrantyClaimInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="warrantyClaim.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${warrantyClaimInstance}" field="updatedBy"
								url="${request.contextPath}/WarrantyClaim/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadWarrantyClaimTable();" />--}%
							
								<g:fieldValue bean="${warrantyClaimInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${warrantyClaimInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="warrantyClaim.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${warrantyClaimInstance}" field="lastUpdProcess"
								url="${request.contextPath}/WarrantyClaim/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadWarrantyClaimTable();" />--}%
							
								<g:fieldValue bean="${warrantyClaimInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${warrantyClaimInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="warrantyClaim.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${warrantyClaimInstance}" field="dateCreated"
								url="${request.contextPath}/WarrantyClaim/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadWarrantyClaimTable();" />--}%
							
								<g:formatDate date="${warrantyClaimInstance?.dateCreated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${warrantyClaimInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="warrantyClaim.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${warrantyClaimInstance}" field="lastUpdated"
								url="${request.contextPath}/WarrantyClaim/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadWarrantyClaimTable();" />--}%
							
								<g:formatDate date="${warrantyClaimInstance?.lastUpdated}"  type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${warrantyClaimInstance?.id}"
					update="[success:'warrantyClaim-form',failure:'warrantyClaim-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteWarrantyClaim('${warrantyClaimInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
