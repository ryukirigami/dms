

<%@ page import="com.kombos.administrasi.KegiatanApproval" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'kegiatanApproval.label', default: 'Master Kegiatan Approval')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteKegiatanApproval;

$(function(){ 
	deleteKegiatanApproval=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/kegiatanApproval/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadKegiatanApprovalTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-kegiatanApproval" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="kegiatanApproval"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${kegiatanApprovalInstance?.m770IdApproval}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m770IdApproval-label" class="property-label"><g:message
					code="kegiatanApproval.m770IdApproval.label" default="Kode Kegiatan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m770IdApproval-label">
						%{--<ba:editableValue
								bean="${kegiatanApprovalInstance}" field="m770IdApproval"
								url="${request.contextPath}/KegiatanApproval/updatefield" type="text"
								title="Enter m770IdApproval" onsuccess="reloadKegiatanApprovalTable();" />--}%
							
								<g:fieldValue bean="${kegiatanApprovalInstance}" field="m770IdApproval"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kegiatanApprovalInstance?.m770KegiatanApproval}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m770KegiatanApproval-label" class="property-label"><g:message
					code="kegiatanApproval.m770KegiatanApproval.label" default="Nama Kegiatan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m770KegiatanApproval-label">
						%{--<ba:editableValue
								bean="${kegiatanApprovalInstance}" field="m770KegiatanApproval"
								url="${request.contextPath}/KegiatanApproval/updatefield" type="text"
								title="Enter m770KegiatanApproval" onsuccess="reloadKegiatanApprovalTable();" />--}%
							
								<g:fieldValue bean="${kegiatanApprovalInstance}" field="m770KegiatanApproval"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
            <g:if test="${kegiatanApprovalInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="kegiatanApproval.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${kegiatanApprovalInstance}" field="dateCreated"
                                url="${request.contextPath}/kegiatanApproval/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadkegiatanApprovalTable();" />--}%

                        <g:formatDate date="${kegiatanApprovalInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${kegiatanApprovalInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="kegiatanApproval.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${kegiatanApprovalInstance}" field="createdBy"
                                url="${request.contextPath}/kegiatanApproval/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadkegiatanApprovalTable();" />--}%

                        <g:fieldValue bean="${kegiatanApprovalInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${kegiatanApprovalInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="kegiatanApproval.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${kegiatanApprovalInstance}" field="lastUpdated"
                                url="${request.contextPath}/kegiatanApproval/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadkegiatanApprovalTable();" />--}%

                        <g:formatDate date="${kegiatanApprovalInstance?.lastUpdated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${kegiatanApprovalInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="kegiatanApproval.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${kegiatanApprovalInstance}" field="updatedBy"
                                url="${request.contextPath}/kegiatanApproval/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadkegiatanApprovalTable();" />--}%

                        <g:fieldValue bean="${kegiatanApprovalInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${kegiatanApprovalInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="kegiatanApproval.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${kegiatanApprovalInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/kegiatanApproval/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadkegiatanApprovalTable();" />--}%

                        <g:fieldValue bean="${kegiatanApprovalInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>



			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
                <g:if test="${staRole=="admin"}">

                    <g:remoteLink class="btn btn-primary edit" action="edit"
                                  id="${kegiatanApprovalInstance?.id}"
                                  update="[success:'kegiatanApproval-form',failure:'kegiatanApproval-form']"
                                  on404="alert('not found');">
                        <g:message code="default.button.edit.label" default="Edit" />
                    </g:remoteLink>
                    <ba:confirm id="delete" class="btn cancel"
                                message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
                                onsuccess="deleteKegiatanApproval('${kegiatanApprovalInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
                </g:if>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
