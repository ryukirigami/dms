<%@ page import="com.kombos.administrasi.KegiatanApproval" %>
<script type="text/javascript">
    function numbersonly(e){
        var unicode=e.charCode? e.charCode : e.keyCode
        if (unicode!=8){ //if the key isn't the backspace key (which we should allow)
            if (unicode<48||unicode>57) //if not a number
                return false //disable key press
        }
    }
</script>


<g:if test="${kegiatanApprovalInstance?.m770IdApproval}">
    <div class="control-group fieldcontain ${hasErrors(bean: kegiatanApprovalInstance, field: 'm770IdApproval', 'error')} required">
        <label class="control-label" for="m770IdApproval">
            <g:message code="kegiatanApproval.m770IdApproval.label" default="Kode Kegiatan" />
            <span class="required-indicator">*</span>
        </label>
        <div class="controls">
            ${kegiatanApprovalInstance?.m770IdApproval}
    </div>
</div>
</g:if>

<div class="control-group fieldcontain ${hasErrors(bean: kegiatanApprovalInstance, field: 'm770KegiatanApproval', 'error')} required">
	<label class="control-label" for="m770KegiatanApproval">
		<g:message code="kegiatanApproval.m770KegiatanApproval.label" default="Nama Kegiatan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:textArea name="m770KegiatanApproval" id="m770KegiatanApproval" maxlength="50" required="" value="${kegiatanApprovalInstance?.m770KegiatanApproval}"/>
	</div>
</div>

<g:javascript>
    document.getElementById("m770KegiatanApproval").focus();
</g:javascript>
