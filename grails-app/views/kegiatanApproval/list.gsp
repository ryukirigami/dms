
<%@ page import="com.kombos.administrasi.KegiatanApproval" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'kegiatanApproval.label', default: 'Master Kegiatan Approval')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/kegiatanApproval/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/kegiatanApproval/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#kegiatanApproval-form').empty();
    	$('#kegiatanApproval-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#kegiatanApproval-table").hasClass("span12")){
   			$("#kegiatanApproval-table").toggleClass("span12 span5");
        }
        $("#kegiatanApproval-form").css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
   		if($("#kegiatanApproval-table").hasClass("span5")){
   			$("#kegiatanApproval-table").toggleClass("span5 span12");
   		}
        $("#kegiatanApproval-form").css("display","none");
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#kegiatanApproval-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/kegiatanApproval/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadKegiatanApprovalTable();
    		}
		});
		
   	}

});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
            <g:if test="${staRole=="admin"}">
                <li><a class="pull-right box-action" href="#"
                        style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
            class="icon-plus"></i>&nbsp;&nbsp;
            </a></li>
                <li><a class="pull-right box-action" href="#"
                       style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
                            class="icon-remove"></i>&nbsp;&nbsp;
                </a></li>
            </g:if>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="kegiatanApproval-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
		</div>
		<div class="span7" id="kegiatanApproval-form" style="display: none;"></div>
	</div>
</body>
</html>
