
<%@ page import="com.kombos.administrasi.KegiatanApproval" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="kegiatanApproval_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="kegiatanApproval.m770IdApproval.label" default="Kode Kegiatan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="kegiatanApproval.m770KegiatanApproval.label" default="Nama Kegiatan" /></div>
			</th>


		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m770IdApproval" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m770IdApproval" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m770KegiatanApproval" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m770KegiatanApproval" class="search_init" />
				</div>
			</th>
	
		</tr>
	</thead>
</table>

<g:javascript>
var kegiatanApprovalTable;
var reloadKegiatanApprovalTable;
$(function(){
	
	reloadKegiatanApprovalTable = function() {
		kegiatanApprovalTable.fnDraw();
	}

    var recordskegiatanApprovalperpage = [];
    var anKegiatanApprovalSelected;
    var jmlRecKegiatanApprovalPerPage=0;
    var id;

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	kegiatanApprovalTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	kegiatanApprovalTable = $('#kegiatanApproval_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsKegiatanApproval = $("#kegiatanApproval_datatables tbody .row-select");
            var jmlKegiatanApprovalCek = 0;
            var nRow;
            var idRec;
            rsKegiatanApproval.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordskegiatanApprovalperpage[idRec]=="1"){
                    jmlKegiatanApprovalCek = jmlKegiatanApprovalCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordskegiatanApprovalperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecKegiatanApprovalPerPage = rsKegiatanApproval.length;
            if(jmlKegiatanApprovalCek==jmlRecKegiatanApprovalPerPage && jmlRecKegiatanApprovalPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m770IdApproval",
	"mDataProp": "m770IdApproval",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
        <g:if test="${staRole=="admin"}">
            return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
        </g:if>
        <g:else>
            return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a></i>&nbsp;&nbsp;</a>';
        </g:else>
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"40%",
	"bVisible": true
}

,

{
	"sName": "m770KegiatanApproval",
	"mDataProp": "m770KegiatanApproval",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"60%",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m770IdApproval = $('#filter_m770IdApproval input').val();
						if(m770IdApproval){
							aoData.push(
									{"name": 'sCriteria_m770IdApproval', "value": m770IdApproval}
							);
						}
	
						var m770KegiatanApproval = $('#filter_m770KegiatanApproval input').val();
						if(m770KegiatanApproval){
							aoData.push(
									{"name": 'sCriteria_m770KegiatanApproval', "value": m770KegiatanApproval}
							);
						}
	
						var m770NamaTabel = $('#filter_m770NamaTabel input').val();
						if(m770NamaTabel){
							aoData.push(
									{"name": 'sCriteria_m770NamaTabel', "value": m770NamaTabel}
							);
						}
	
						var m770NamaFk = $('#filter_m770NamaFk input').val();
						if(m770NamaFk){
							aoData.push(
									{"name": 'sCriteria_m770NamaFk', "value": m770NamaFk}
							);
						}
	
						var m770NamaFieldDiupdate = $('#filter_m770NamaFieldDiupdate input').val();
						if(m770NamaFieldDiupdate){
							aoData.push(
									{"name": 'sCriteria_m770NamaFieldDiupdate', "value": m770NamaFieldDiupdate}
							);
						}
	
						var m770NamaFormDetail = $('#filter_m770NamaFormDetail input').val();
						if(m770NamaFormDetail){
							aoData.push(
									{"name": 'sCriteria_m770NamaFormDetail', "value": m770NamaFormDetail}
							);
						}
	
						var m770StaButuhNilai = $('#filter_m770StaButuhNilai input').val();
						if(m770StaButuhNilai){
							aoData.push(
									{"name": 'sCriteria_m770StaButuhNilai', "value": m770StaButuhNilai}
							);
						}
	
						var m770StaDel = $('#filter_m770StaDel input').val();
						if(m770StaDel){
							aoData.push(
									{"name": 'sCriteria_m770StaDel', "value": m770StaDel}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

    $('.select-all').click(function(e) {
        $("#kegiatanApproval_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordskegiatanApprovalperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordskegiatanApprovalperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

	$('#kegiatanApproval_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordskegiatanApprovalperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anKegiatanApprovalSelected = kegiatanApprovalTable.$('tr.row_selected');
            if(jmlRecKegiatanApprovalPerPage == anKegiatanApprovalSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordskegiatanApprovalperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
