
<%@ page import="com.kombos.administrasi.Operation" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="operation_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover fixed table-selectable" style="table-layout: fixed; ">
    <col width="550px" />
    <col width="550px" />
    <thead>
    <tr>
        <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:549px;">
            <div>&nbsp;&nbsp;
                <input type="checkbox" class="select-all" aria-label="Select all" title="Select all"/>&nbsp;&nbsp;
                <g:message code="operation.m053Id.label" default="Kode Job" />
            </div>
        </th>

        <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:549px;">
            <div>&nbsp;&nbsp;
                <g:message code="operation.m053NamaOperation.label" default="Nama Job" />
            </div>
        </th>
    </tr>
    <tr>
        <th style="border-top: none;padding: 0px 0px 5px 0px; width:549px;">
            <div id="filter_m053Id" style="width: 120px;">&nbsp;&nbsp;
                <input type="text" name="search_m053Id" class="search_init" />
            </div>
        </th>
        <th style="border-top: none;padding: 0px 0px 5px 0px; width:549px;">
            <div id="filter_m053NamaOperation" style="width: 120px;">&nbsp;&nbsp;
                <input type="text" name="search_m053NamaOperation" class="search_init" />
            </div>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
var operationTable;
var reloadOperationTable;
$(function(){

	reloadOperationTable = function() {
	    operationTable.fnDraw();
	}
    
    var recordsoperationperpage = [];
    var anOperationSelected;
    var jmlRecOperationPerPage=0;
    var id;


    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	operationTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	operationTable = $('#operation_datatables').dataTable({
		"sScrollX": "1100px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsOperation = $("#operation_datatables tbody .row-select");
            var jmlOperationCek = 0;
            var nRow;
            var idRec;
            rsOperation.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsoperationperpage[idRec]=="1"){
                    jmlOperationCek = jmlOperationCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsoperationperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecOperationPerPage = rsOperation.length;
            if(jmlOperationCek==jmlRecOperationPerPage && jmlRecOperationPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
        "bDestroy" : true,
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m053Id",
	"mDataProp": "m053Id",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
        return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}

,

{
	"sName": "m053NamaOperation",
	"mDataProp": "m053NamaOperation",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

            var m053NamaOperation = $('#filter_m053NamaOperation input').val();
            if(m053NamaOperation){
                aoData.push(
                        {"name": 'sCriteria_m053NamaOperation', "value": m053NamaOperation}
                );
            }

            var m053JobsId = $('#filter_m053Id input').val();
            if(m053JobsId){
                aoData.push(
                        {"name": 'sCriteria_m053Id', "value": m053JobsId}
                );
            }


            var exist =[];
            $("#serviceGratis_datatables tbody .row-select").each(function() {
                var id = $(this).next("input:hidden").val();
                exist.push(id);
            });
            if(exist.length > 0){
                aoData.push(
                            {"name": 'sCriteria_exist', "value": JSON.stringify(exist)}
                );
            }

            $.ajax({ "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData ,
                "success": function (json) {
                    fnCallback(json);
                   },
                "complete": function () {
                   }
            });
		}
	});

    $('.select-all').click(function(e) {
        $("#operation_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsoperationperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsoperationperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

    $('#operation_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsoperationperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anOperationSelected = operationTable.$('tr.row_selected');
            if(jmlRecOperationPerPage == anOperationSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsoperationperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>



