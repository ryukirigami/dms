
<%@ page import="com.kombos.administrasi.Operation" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'operation.label', default: 'Cari Job')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var showJob;
	var loadForm;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

	showJob = function(id) {
    	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/operation/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#operation-form').empty();
    	$('#operation-form').append(data);
   	}
    
    expandTableLayout = function(){
   		if($("#operation-table").hasClass("span5")){
   			$("#operation-table").toggleClass("span5 span12");
   		}
        $("#operation-form").css("display","none");
   	}

});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			%{--<li class="separator"></li>--}%
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="operation-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
			<g:render template="dataTables" />
            <g:field type="button" onclick="tambahServiceGratis();" class="btn btn-primary create" name="tambah" id="tambah" value="${message(code: 'default.button.copyJob.label', default: 'Tambahkan Sebagai Service Gratis')}" />
            <button id='closepilihJob' onclick="reloadServiceGratisTable();" type="button" class="btn btn-primary pull-right">Close</button>
        </div>
	</div>
</body>
</html>
