
<%@ page import="com.kombos.customerprofile.JenisIdCard" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'jenisIdCard.label', default: 'Jenis Id Card')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteJenisIdCard;

$(function(){
	deleteJenisIdCard=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/jenisIdCard/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadJenisIdCardTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-jenisIdCard" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="jenisIdCard"
			class="table table-bordered table-hover">
			<tbody>


				<g:if test="${jenisIdCardInstance?.m060ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m060ID-label" class="property-label"><g:message
					code="jenisIdCard.m060ID.label" default="Kode ID Card" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="m060ID-label">
						%{--<ba:editableValue
								bean="${jenisIdCardInstance}" field="m060ID"
								url="${request.contextPath}/JenisIdCard/updatefield" type="text"
								title="Enter m060ID" onsuccess="reloadJenisIdCardTable();" />--}%

								<g:fieldValue bean="${jenisIdCardInstance}" field="m060ID"/>

						</span></td>

				</tr>
				</g:if>

				<g:if test="${jenisIdCardInstance?.m060JenisIDCard}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m060JenisIDCard-label" class="property-label"><g:message
					code="jenisIdCard.m060JenisIDCard.label" default="Jenis ID Card" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="m060JenisIDCard-label">
						%{--<ba:editableValue
								bean="${jenisIdCardInstance}" field="m060JenisIDCard"
								url="${request.contextPath}/JenisIdCard/updatefield" type="text"
								title="Enter m060JenisIDCard" onsuccess="reloadJenisIdCardTable();" />--}%

								<g:fieldValue bean="${jenisIdCardInstance}" field="m060JenisIDCard"/>

						</span></td>

				</tr>
				</g:if>

				%{--<g:if test="${jenisIdCardInstance?.staDel}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="staDel-label" class="property-label"><g:message--}%
					%{--code="jenisIdCard.staDel.label" default="Sta Del" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="staDel-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${jenisIdCardInstance}" field="staDel"--}%
								%{--url="${request.contextPath}/JenisIdCard/updatefield" type="text"--}%
								%{--title="Enter staDel" onsuccess="reloadJenisIdCardTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${jenisIdCardInstance}" field="staDel"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%

				<g:if test="${jenisIdCardInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="jenisIdCard.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${jenisIdCardInstance}" field="lastUpdProcess"
								url="${request.contextPath}/JenisIdCard/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadJenisIdCardTable();" />--}%

								<g:fieldValue bean="${jenisIdCardInstance}" field="lastUpdProcess"/>

						</span></td>

				</tr>
				</g:if>

				<g:if test="${jenisIdCardInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="jenisIdCard.dateCreated.label" default="Date Created" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${jenisIdCardInstance}" field="dateCreated"
								url="${request.contextPath}/JenisIdCard/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadJenisIdCardTable();" />--}%

								<g:formatDate date="${jenisIdCardInstance?.dateCreated}" type="datetime" style="MEDIUM" />

						</span></td>

				</tr>
				</g:if>

            <g:if test="${jenisIdCardInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="jenisIdCard.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${jenisIdCardInstance}" field="createdBy"
                                url="${request.contextPath}/JenisIdCard/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadJenisIdCardTable();" />--}%

                        <g:fieldValue bean="${jenisIdCardInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

				<g:if test="${jenisIdCardInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="jenisIdCard.lastUpdated.label" default="Last Updated" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${jenisIdCardInstance}" field="lastUpdated"
								url="${request.contextPath}/JenisIdCard/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadJenisIdCardTable();" />--}%

								<g:formatDate date="${jenisIdCardInstance?.lastUpdated}" type="datetime" style="MEDIUM" />

						</span></td>

				</tr>
				</g:if>

            <g:if test="${jenisIdCardInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="jenisIdCard.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${jenisIdCardInstance}" field="updatedBy"
                                url="${request.contextPath}/JenisIdCard/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadJenisIdCardTable();" />--}%

                        <g:fieldValue bean="${jenisIdCardInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${jenisIdCardInstance?.id}"
					update="[success:'jenisIdCard-form',failure:'jenisIdCard-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Anda yakin data akan dihapus?')}"
					onsuccess="deleteJenisIdCard('${jenisIdCardInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
