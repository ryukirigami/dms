<%@ page import="com.kombos.customerprofile.JenisIdCard" %>

<g:if test="${jenisIdCardInstance?.m060ID}">
<div class="control-group fieldcontain ${hasErrors(bean: jenisIdCardInstance, field: 'm060ID', 'error')} required">
	<label class="control-label" for="m060ID">
		<g:message code="jenisIdCard.m060ID.label" default="Kode ID Card" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	${jenisIdCardInstance?.m060ID}
	</div>
</div>
</g:if>

<div class="control-group fieldcontain ${hasErrors(bean: jenisIdCardInstance, field: 'm060JenisIDCard', 'error')} required">
	<label class="control-label" for="m060JenisIDCard">
		<g:message code="jenisIdCard.m060JenisIDCard.label" default="Jenis ID Card" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m060JenisIDCard" maxlength="50" required="" value="${jenisIdCardInstance?.m060JenisIDCard}"/>
	</div>
</div>

<g:javascript>
    document.getElementById("m060JenisIDCard").focus();
</g:javascript>