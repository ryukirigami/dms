<%@ page import="com.kombos.maintable.EFakturKeluaran" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'EFaktur.label', default: 'PPN Keluaran Detail')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>

	<r:require modules="baseapplayout, baseapplist"/>
	<g:javascript disposition="head">
			var show;
			var edit;
			var shrinkTableLayout;
			var expandTableLayout;
			$(function(){
            shrinkTableLayout = function(){
                if($("#EFaktur-table").hasClass("span12")){
                    $("#EFaktur-table").toggleClass("span12 span5");
                }
                $("#EFaktur-form").css("display","block");
            }

            expandTableLayout = function(){
                if($("#EFaktur-table").hasClass("span5")){
                    $("#EFaktur-table").toggleClass("span5 span12");
                }
                $("#EFaktur-form").css("display","none");
            }
            loadForm = function(data, textStatus){
                $('#EFaktur-form').empty();
                $('#EFaktur-form').append(data);
            }
				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :
							shrinkTableLayout();
							<g:remoteFunction action="create"
											  onLoading="jQuery('#spinner').fadeIn(1);"
											  onSuccess="loadFormInstance('EFaktur', data, textStatus);"
											  onComplete="jQuery('#spinner').fadeOut();"/>
		break;
    case '_DELETE_' :
        bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
                                                    function(result){
                                                        if(result){
                                                            massDelete('EFaktur', '${request.contextPath}/EFakturKeluaran/massdelete', reloadEFakturTable);
                                                        }
                                                    });

							break;
				   }
				   return false;
				});

				show = function(id) {
					showInstance('EFaktur','${request.contextPath}/EFakturKeluaran/show/'+id);
				};

				edit = function(id) {
				    shrinkTableLayout();
					$('#spinner').fadeIn(1);
                    $.ajax({type:'POST', url:'${request.contextPath}/EFakturKeluaran/edit/'+id,
                        success:function(data,textStatus){
                            loadForm(data, textStatus);
                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
				};

});

     closeUpload = function(){
		${flash.message = ""}
		loadPath("EFakturKeluaran/index");
     }
	</g:javascript>
</head>

<body>
<div class="navbar box-header no-border">
	<span class="pull-left"><g:message code="default.list.label" args="[entityName]"/></span>
	<ul class="nav pull-right">
		<li><a class="pull-right box-action" href="#"
			   style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
		</a></li>
		<li class="separator"></li>
	</ul>
</div>

<div class="box">
	<div class="span12" id="EFaktur-table">
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<g:render template="dataTables"/>
	</div>

	<div class="span7" id="EFaktur-form" style="display: none;"></div>
	<g:field type="button" onclick="closeUpload();" class="btn btn-cancel delete" name="close" id="close" value="${message(code: 'default.button.view.label', default: 'Close')}" />
</div>
</body>
</html>
