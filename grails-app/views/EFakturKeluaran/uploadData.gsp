<%@ page import="com.kombos.parts.Goods" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'goods.label', default: 'Upload PPN KELUARAN')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:if test="${jsonData}">
        <g:javascript>
            <g:if test="${(jmlhDataError && jmlhDataError>0) || jumJson<=0}">
                $('#save').attr("disabled", true);
            </g:if>
            var saveForm;
            $(function(){
                $("#export").click(function(e) {
                    window.location = "${request.contextPath}/EFakturKeluaran/exportToExcel";
                    });

                    saveForm = function() {
                       var sendData = ${jsonData};
                       var tanggal = $('#tanggalUpload').val();
                       var conf = "";
            <g:if test="${jsonData}">
                <g:if test="${jmlhDataError==0}">
                    conf = confirm("File Akan Disimpan Sesuai Tanggal Upload yang Dipilih, Apakah ingin Lanjut Simpan Data ? ");
                </g:if>
                if(conf){
                    $.ajax({
                        url:'${request.getContextPath()}/EFakturKeluaran/upload',
                                    type: 'POST',

                                    //add beforesend handler to validate or something
                                    //beforeSend: functionname,
                                    success: function (res) {
                                        $('#eFakturTable').empty();
                                        $('#eFakturTable').append(res);
                                    },
                                    //add error handler for when a error occurs if you want!
                                    error: function (data, status, e){
                                        alert(e);
                                    },
                                    complete: function(xhr, status) {

                                    },
                                    data:  {sendData : JSON.stringify(sendData), tanggal:tanggal},
            %{--contentType: "application/json; charset=utf-8",--}%
                traditional: true,
                cache: false
            });
        }
            </g:if>
            <g:else>
                alert('No File Selected');
            </g:else>
            }
     });
        </g:javascript>
    </g:if>
</head>
<body>
<div id="eFakturTable">
    <div class="navbar box-header no-border">
        <span class="pull-left">
            <g:message code="default.list.label" args="[entityName]" />
        </span>
        <ul class="nav pull-right">
            <li></li>
            <li></li>
            <li class="separator"></li>
        </ul>
    </div>
    <div class="box">
        <div class="span12" id="operation-table">
            <fieldset>
                <form id="uploadEFaktur-save" class="form-vertical" action="${request.getContextPath()}/EFakturKeluaran/save" method="post">
                    <table>
                        <tr>
                            <td>
                                <a href="${request.getContextPath()}/formatFileUpload/uploadPPNKeluaran.xls" >* File Example Upload</a>
                                <br/><br/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <fieldset>
                                    <label class="control-label" for="t951Tanggal">
                                        <g:message code="auditTrail.tanggal.label" default="Tanggal Upload" />&nbsp;
                                    </label>&nbsp;&nbsp;
                                </fieldset>
                            <td></td>
                            <td>
                                <div id="filter_m777Tgl" class="controls">
                                    <ba:datePicker id="tanggalUpload" name="tanggalUpload" precision="day" format="dd/MM/yyyy"  value="${tanggal}" />
                                </div>
                            </td>
                        </td>
                        </tr>
                        <tr>
                            <td>
                                <fieldset class="form">
                                    <g:render template="formUpload"/>
                                </fieldset>
                            </td>
                            <td></td>
                            <td>
                                <fieldset class="buttons controls">
                                    <g:field type="button" onclick="submitForm();return false;" class="btn btn-primary create" name="view" id="view" value="${message(code: 'default.button.view.label', default: 'Upload')}" />
                                    <g:field type="button" onclick="saveForm();" class="btn btn-primary create" name="save" id="save" value="${message(code: 'default.button.upload.label', default: 'Simpan')}" />
                                    &nbsp;&nbsp;&nbsp;
                                    <g:if test="${flash.message}">
                                        ${flash.message}
                                    </g:if>
                                    %{--<br/>--}%

                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </form>
                <g:javascript>
    var submitForm;
    $(function(){
        closeUpload = function(){
            ${flash.message = ""}
                    loadPath("EFakturKeluaran/index");
                }

                submitForm = function() {

                    var form = new FormData($('#uploadEFaktur-save')[0]);
                        console.log(form);
                    $.ajax({
                        url:'${request.getContextPath()}/EFakturKeluaran/view',
                                    type: 'POST',
                                    //add beforesend handler to validate or something
                                    //beforeSend: functionname,
                                    success: function (res) {
                                        $('#eFakturTable').empty();
                                        $('#eFakturTable').append(res);

                                    },
                                    //add error handler for when a error occurs if you want!
                                    error: function (data, status, e){
                                        alert(e);
                                    },
                                    complete: function(xhr, status) {

                                    },
                                    data: form,
                                    cache: false,
                                    contentType: false,
                                    processData: false
                                });

                            }
                        });
                </g:javascript>
            </fieldset>
            <br>
            <div style="width: 100%; overflow: auto;">

                <table class="display table table-striped table-bordered table-hover dataTable" width="100%" cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px;" >
                    <thead>
                    <tr>
                        <th style="border-bottom: none;padding: 5px;">
                            <div><g:message code="SCC.m113TglBerlaku.label" default="NO" /></div>
                        </th>
                        <th style="border-bottom: none;padding: 5px;">
                            <div><g:message code="EFaktur.PPNMasukan.label" default="MASA PAJAK" /></div>
                        </th>
                        <th style="border-bottom: none;padding: 5px;">
                            <div><g:message code="EFaktur.PPNMasukan.label" default="TAHUN PAJAK" /></div>
                        </th>
                        <th style="border-bottom: none;padding: 5px;">
                            <div><g:message code="EFaktur.PPNMasukan.label" default="REFERENSI" /></div>
                        </th>
                        <th style="border-bottom: none;padding: 5px;">
                            <div><g:message code="EFaktur.PPNMasukan.label" default="NO. SI" /></div>
                        </th>
                        <th style="border-bottom: none;padding: 5px;">
                            <div><g:message code="EFaktur.PPNMasukan.label" default="TANGGAL SI" /></div>
                        </th>
                        <th style="border-bottom: none;padding: 5px;">
                            <div><g:message code="EFaktur.PPNMasukan.label" default="NAMA CUSTOMER" /></div>
                        </th>
                        <th style="border-bottom: none;padding: 5px;">
                            <div><g:message code="EFaktur.PPNMasukan.label" default="NPWP" /></div>
                        </th>
                        <th style="border-bottom: none;padding: 5px;">
                            <div><g:message code="EFaktur.PPNMasukan.label" default="ALAMAT " /></div>
                        </th>
                        <th style="border-bottom: none;padding: 5px;">
                            <div><g:message code="EFaktur.PPNMasukan.label" default="BIAYA JASA" /></div>
                        </th>
                        <th style="border-bottom: none;padding: 5px;">
                            <div><g:message code="EFaktur.PPNMasukan.label" default="BIAYA PARTS" /></div>
                        </th>
                        <th style="border-bottom: none;padding: 5px;">
                            <div><g:message code="EFaktur.PPNMasukan.label" default="BIAYA OLI" /></div>
                        </th>
                        <th style="border-bottom: none;padding: 5px;">
                            <div><g:message code="EFaktur.PPNMasukan.label" default="BIAYA MATERIAL" /></div>
                        </th>
                        <th style="border-bottom: none;padding: 5px;">
                            <div><g:message code="EFaktur.PPNMasukan.label" default="BIAYA SUBLET" /></div>
                        </th>
                        <th style="border-bottom: none;padding: 5px;">
                            <div><g:message code="EFaktur.PPNMasukan.label" default="BIAYA ADM" /></div>
                        </th>
                        <th style="border-bottom: none;padding: 5px;">
                            <div><g:message code="EFaktur.PPNMasukan.label" default="DISC. JASA" /></div>
                        </th>
                        <th style="border-bottom: none;padding: 5px;">
                            <div><g:message code="EFaktur.PPNMasukan.label" default="DISC. PARTS" /></div>
                        </th>
                        <th style="border-bottom: none;padding: 5px;">
                            <div><g:message code="EFaktur.PPNMasukan.label" default="DISC. OLI" /></div>
                        </th>
                        <th style="border-bottom: none;padding: 5px;">
                            <div><g:message code="EFaktur.PPNMasukan.label" default="DISC. BAHAN" /></div>
                        </th>
                        <th style="border-bottom: none;padding: 5px;">
                            <div><g:message code="EFaktur.PPNMasukan.label" default="DPP" /></div>
                        </th>
                        <th style="border-bottom: none;padding: 5px;">
                            <div><g:message code="EFaktur.PPNMasukan.label" default="PPN" /></div>
                        </th>
                        <th style="border-bottom: none;padding: 5px;">
                            <div><g:message code="EFaktur.PPNMasukan.label" default="TOTAL" /></div>
                        </th>
                    </tr>
                    </thead>
                    <g:if test="${htmlData}">
                        ${htmlData}
                    </g:if>
                    <g:else>
                        <tr class="odd">
                            <td class="dataTables_empty" valign="top" colspan="45">No data available in table</td>
                        </tr>
                    </g:else>
                </table>
            </div>
            <br>
            <g:field type="button" onclick="closeUpload();" class="btn btn-cancel delete" name="close" id="close" value="${message(code: 'default.button.view.label', default: 'Close')}" />
            %{--<g:field type="button" class="btn btn-primary create" name="export" id="export" value="${message(code: 'default.button.view.label', default: 'Export To Excel')}" />--}%
        </div>
        <br>
        <g:if test="${isiEror}">
            <br> Keterangan : <br>
            ${isiEror}
        </g:if>
    </div>
</div>
</body>
</html>
