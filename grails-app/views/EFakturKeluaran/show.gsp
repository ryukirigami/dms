

<%@ page import="com.kombos.maintable.EFakturKeluaran" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'EFakturKeluaran.label', default: 'EFakturKeluaran')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteEFakturKeluaran;

$(function(){ 
	deleteEFakturKeluaran=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/EFakturKeluaran/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadEFakturKeluaranTable();
   				expandTableLayout('EFakturKeluaran');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-EFakturKeluaran" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="EFakturKeluaran"
			class="table table-bordered table-hover">
			<tbody

				<g:if test="${EFakturInstance?.companyDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="companyDealer-label" class="property-label"><g:message
					code="EFakturKeluaran.companyDealer.label" default="Company Dealer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="companyDealer-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="companyDealer"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter companyDealer" onsuccess="reloadEFakturKeluaranTable();" />--}%

								<g:fieldValue bean="${EFakturInstance}" field="companyDealer"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${EFakturInstance?.docNumber}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="docNumber-label" class="property-label"><g:message
					code="EFakturKeluaran.docNumber.label" default="Doc Number" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="docNumber-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="docNumber"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter docNumber" onsuccess="reloadEFakturKeluaranTable();" />--}%
							
								<g:fieldValue bean="${EFakturInstance}" field="docNumber"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${EFakturInstance?.tanggal}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tanggal-label" class="property-label"><g:message
					code="EFakturKeluaran.tanggal.label" default="Tanggal" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tanggal-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="tanggal"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter tanggal" onsuccess="reloadEFakturKeluaranTable();" />--}%
							
								<g:formatDate date="${EFakturInstance?.tanggal}" />
							
						</span></td>
					
				</tr>
				</g:if>

				<g:if test="${EFakturInstance?.NO_FAKTUR}">
				<tr>
					<td class="span2" style="text-align: right;"><span
							id="NO_FAKTUR-label" class="property-label"><g:message
								code="EFakturKeluaran.NO_FAKTUR.label" default="Nomor Faktur" />:</span></td>

					<td class="span3"><span class="property-value"
											aria-labelledby="NO_FAKTUR-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="NO_FAKTUR"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter NO_FAKTUR" onsuccess="reloadEFakturKeluaranTable();" />--}%

						<g:fieldValue bean="${EFakturInstance}" field="NO_FAKTUR"/>

					</span></td>

				</tr>
			</g:if>

				<g:if test="${EFakturInstance?.MASA_PAJAK}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="MASA_PAJAK-label" class="property-label"><g:message
					code="EFakturKeluaran.MASA_PAJAK.label" default="Masa Pajak" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="MASA_PAJAK-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="MASA_PAJAK"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter MASA_PAJAK" onsuccess="reloadEFakturKeluaranTable();" />--}%
							
								<g:fieldValue bean="${EFakturInstance}" field="MASA_PAJAK"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${EFakturInstance?.TAHUN_PAJAK}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="TAHUN_PAJAK-label" class="property-label"><g:message
					code="EFakturKeluaran.TAHUN_PAJAK.label" default="Tahun Pajak" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="TAHUN_PAJAK-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="TAHUN_PAJAK"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter TAHUN_PAJAK" onsuccess="reloadEFakturKeluaranTable();" />--}%
							
								<g:fieldValue bean="${EFakturInstance}" field="TAHUN_PAJAK"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${EFakturInstance?.REFERENSI}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="REFERENSI-label" class="property-label"><g:message
					code="EFakturKeluaran.REFERENSI.label" default="Referensi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="REFERENSI-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="REFERENSI"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter REFERENSI" onsuccess="reloadEFakturKeluaranTable();" />--}%
							
								<g:fieldValue bean="${EFakturInstance}" field="REFERENSI"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${EFakturInstance?.NO_SI}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="NO_SI-label" class="property-label"><g:message
					code="EFakturKeluaran.NO_SI.label" default="NOSI" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="NO_SI-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="NO_SI"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter NO_SI" onsuccess="reloadEFakturKeluaranTable();" />--}%
							
								<g:fieldValue bean="${EFakturInstance}" field="NO_SI"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${EFakturInstance?.TGL_SI}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="TGL_SI-label" class="property-label"><g:message
					code="EFakturKeluaran.TGL_SI.label" default="Tanggal SI" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="TGL_SI-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="TGL_SI"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter TGL_SI" onsuccess="reloadEFakturKeluaranTable();" />--}%
							
								<g:formatDate date="${EFakturInstance?.TGL_SI}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${EFakturInstance?.NAMA_CUSTOMER}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="NAMA_CUSTOMER-label" class="property-label"><g:message
					code="EFakturKeluaran.NAMA_CUSTOMER.label" default="Nama Customer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="NAMA_CUSTOMER-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="NAMA_CUSTOMER"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter NAMA_CUSTOMER" onsuccess="reloadEFakturKeluaranTable();" />--}%
							
								<g:fieldValue bean="${EFakturInstance}" field="NAMA_CUSTOMER"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${EFakturInstance?.NPWP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="NPWP-label" class="property-label"><g:message
					code="EFakturKeluaran.NPWP.label" default="NPWP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="NPWP-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="NPWP"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter NPWP" onsuccess="reloadEFakturKeluaranTable();" />--}%
							
								<g:fieldValue bean="${EFakturInstance}" field="NPWP"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${EFakturInstance?.ALAMAT}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="ALAMAT-label" class="property-label"><g:message
					code="EFakturKeluaran.ALAMAT.label" default="Alamat" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="ALAMAT-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="ALAMAT"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter ALAMAT" onsuccess="reloadEFakturKeluaranTable();" />--}%
							
								<g:fieldValue bean="${EFakturInstance}" field="ALAMAT"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${EFakturInstance?.BIAYA_JASA}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="BIAYA_JASA-label" class="property-label"><g:message
					code="EFakturKeluaran.BIAYA_JASA.label" default="Biaya Jasa" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="BIAYA_JASA-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="BIAYA_JASA"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter BIAYA_JASA" onsuccess="reloadEFakturKeluaranTable();" />--}%
							
								<g:fieldValue bean="${EFakturInstance}" field="BIAYA_JASA"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${EFakturInstance?.BIAYA_PARTS}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="BIAYA_PARTS-label" class="property-label"><g:message
					code="EFakturKeluaran.BIAYA_PARTS.label" default="Biaya Parts" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="BIAYA_PARTS-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="BIAYA_PARTS"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter BIAYA_PARTS" onsuccess="reloadEFakturKeluaranTable();" />--}%
							
								<g:fieldValue bean="${EFakturInstance}" field="BIAYA_PARTS"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${EFakturInstance?.BIAYA_OLI}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="BIAYA_OLI-label" class="property-label"><g:message
					code="EFakturKeluaran.BIAYA_OLI.label" default="Biaya Oli" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="BIAYA_OLI-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="BIAYA_OLI"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter BIAYA_OLI" onsuccess="reloadEFakturKeluaranTable();" />--}%
							
								<g:fieldValue bean="${EFakturInstance}" field="BIAYA_OLI"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${EFakturInstance?.BIAYA_MATERIAL}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="BIAYA_MATERIAL-label" class="property-label"><g:message
					code="EFakturKeluaran.BIAYA_MATERIAL.label" default="Biaya Material" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="BIAYA_MATERIAL-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="BIAYA_MATERIAL"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter BIAYA_MATERIAL" onsuccess="reloadEFakturKeluaranTable();" />--}%
							
								<g:fieldValue bean="${EFakturInstance}" field="BIAYA_MATERIAL"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${EFakturInstance?.BIAYA_SUBLET}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="BIAYA_SUBLET-label" class="property-label"><g:message
					code="EFakturKeluaran.BIAYA_SUBLET.label" default="Biaya Sublet" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="BIAYA_SUBLET-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="BIAYA_SUBLET"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter BIAYA_SUBLET" onsuccess="reloadEFakturKeluaranTable();" />--}%
							
								<g:fieldValue bean="${EFakturInstance}" field="BIAYA_SUBLET"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${EFakturInstance?.BIAYA_ADM}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="BIAYA_ADM-label" class="property-label"><g:message
					code="EFakturKeluaran.BIAYA_ADM.label" default="Biaya Admin" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="BIAYA_ADM-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="BIAYA_ADM"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter BIAYA_ADM" onsuccess="reloadEFakturKeluaranTable();" />--}%
							
								<g:fieldValue bean="${EFakturInstance}" field="BIAYA_ADM"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${EFakturInstance?.DISC_JASA}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="DISC_JASA-label" class="property-label"><g:message
					code="EFakturKeluaran.DISC_JASA.label" default="Diskon Jasa" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="DISC_JASA-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="DISC_JASA"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter DISC_JASA" onsuccess="reloadEFakturKeluaranTable();" />--}%
							
								<g:fieldValue bean="${EFakturInstance}" field="DISC_JASA"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${EFakturInstance?.DISC_PARTS}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="DISC_PARTS-label" class="property-label"><g:message
					code="EFakturKeluaran.DISC_PARTS.label" default="Diskon Parts" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="DISC_PARTS-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="DISC_PARTS"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter DISC_PARTS" onsuccess="reloadEFakturKeluaranTable();" />--}%
							
								<g:fieldValue bean="${EFakturInstance}" field="DISC_PARTS"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${EFakturInstance?.DISC_BAHAN}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="DISC_BAHAN-label" class="property-label"><g:message
					code="EFakturKeluaran.DISC_BAHAN.label" default="Diskon Bahan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="DISC_BAHAN-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="DISC_BAHAN"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter DISC_BAHAN" onsuccess="reloadEFakturKeluaranTable();" />--}%
							
								<g:fieldValue bean="${EFakturInstance}" field="DISC_BAHAN"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${EFakturInstance?.DPP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="DPP-label" class="property-label"><g:message
					code="EFakturKeluaran.DPP.label" default="DPP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="DPP-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="DPP"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter DPP" onsuccess="reloadEFakturKeluaranTable();" />--}%
							
								<g:fieldValue bean="${EFakturInstance}" field="DPP"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${EFakturInstance?.PPN}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="PPN-label" class="property-label"><g:message
					code="EFakturKeluaran.PPN.label" default="PPN" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="PPN-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="PPN"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter PPN" onsuccess="reloadEFakturKeluaranTable();" />--}%
							
								<g:fieldValue bean="${EFakturInstance}" field="PPN"/>
							
						</span></td>
					
				</tr>
				</g:if>

				<g:if test="${EFakturInstance?.TOTAL}">
				<tr>
					<td class="span2" style="text-align: right;"><span
							id="TOTAL-label" class="property-label"><g:message
								code="EFakturKeluaran.TOTAL.label" default="Total" />:</span></td>

					<td class="span3"><span class="property-value"
											aria-labelledby="TOTAL-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="TOTAL"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter TOTAL" onsuccess="reloadEFakturKeluaranTable();" />--}%
								<g:fieldValue bean="${EFakturInstance}" field="TOTAL"/>

					</span></td>

				</tr>
			</g:if>

				<g:if test="${EFakturInstance?.staAprove}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staAprove-label" class="property-label"><g:message
					code="EFakturKeluaran.staAprove.label" default="Sta Aprove" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staAprove-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="staAprove"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter staAprove" onsuccess="reloadEFakturKeluaranTable();" />--}%
								<g:if test="${EFakturInstance.staAprove.equals('0')}">
									<label>Waiting</label>
								</g:if>
								<g:elseif test="${EFakturInstance.staAprove.equals('1')}">
									<label>Approved</label>
								</g:elseif>
								<g:elseif test="${EFakturInstance.staAprove.equals('2')}">
									<label>Rejected</label>
								</g:elseif>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${EFakturInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="EFakturKeluaran.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="createdBy"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadEFakturKeluaranTable();" />--}%
							
								<g:fieldValue bean="${EFakturInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${EFakturInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="EFakturKeluaran.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="updatedBy"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadEFakturKeluaranTable();" />--}%
							
								<g:fieldValue bean="${EFakturInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${EFakturInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="EFakturKeluaran.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="lastUpdProcess"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadEFakturKeluaranTable();" />--}%
							
								<g:fieldValue bean="${EFakturInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${EFakturInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="EFakturKeluaran.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="dateCreated"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadEFakturKeluaranTable();" />--}%
							
								<g:formatDate date="${EFakturInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${EFakturInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="EFakturKeluaran.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${EFakturInstance}" field="lastUpdated"
								url="${request.contextPath}/EFakturKeluaran/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadEFakturKeluaranTable();" />--}%
							
								<g:formatDate date="${EFakturInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('EFakturKeluaran');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${EFakturInstance?.id}"
					update="[success:'EFakturKeluaran-form',failure:'EFakturKeluaran-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteEFakturKeluaran('${EFakturInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
