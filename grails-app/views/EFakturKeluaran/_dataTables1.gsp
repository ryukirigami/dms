
<%@ page import="com.kombos.maintable.EFaktur" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="EFaktur_datatables1" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="EFaktur.dealerCode.label" default="No Dokumen" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="EFaktur.namaFile.label" default="Company Dealer" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="EFaktur.noPol.label" default="Tanggal Upload" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="EFaktur.noPol.label" default="Status" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="EFaktur.noPol.label" default="Download" /></div>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
var EFaktur;
var EF;
var reloadEFakturTable;
$(function(){

	reloadEFakturTable = function() {
		EFakturTable.fnDraw();
	}

    EF = function(docNumber){
         checkId =[];
         $("#EFaktur-table tbody .row-select").each(function() {
              if(this.checked){
                    var nRow = $(this).next("input:hidden").val();
                    checkId.push(nRow);
              }
         });
        loadPath("EFakturKeluaran/list?docNumber="+docNumber);
    }

    $('#clear').click(function(e){
        $('#search_Tanggal').val("");
        $('#search_Tanggal_day').val("");
        $('#search_Tanggal_month').val("");
        $('#search_Tanggal_year').val("");
        $('#search_Tanggal2').val("");
        $('#search_Tanggal2_day').val("");
        $('#search_Tanggal2_month').val("");
        $('#search_Tanggal2_year').val("");
        EFakturTable.fnDraw();
	});
    $('#view').click(function(e){
        e.stopPropagation();
		EFakturTable.fnDraw();
	});


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	EFakturTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	EFakturTable = $('#EFaktur_datatables1').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList1")}",
		"aoColumns": [

{
	"sName": "docNumber",
	"mDataProp": "docNumber",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
            if(row['staAproveId']=="0"){
                if(${session.userAuthority.toUpperCase()?.contains("ADMIN")}){
                    return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['docNumber']+'" title="Select this"><input type="hidden" value="'+row['docNumber']+'">&nbsp;&nbsp;<a href="javascript:void(0)" onclick="EF(\''+data+'\')">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="showDetail(\''+data+'\');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
                }else{
                    return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['docNumber']+'" title="Select this"><input type="hidden" value="'+row['docNumber']+'">&nbsp;&nbsp;'+data;
                }
            }else{
                if(${session.userAuthority.toUpperCase()?.contains("ADMIN")}){
                    return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['docNumber']+'" title="Select this"><input type="hidden" value="'+row['docNumber']+'">&nbsp;&nbsp;<a href="javascript:void(0)" onclick="EF(\''+data+'\')">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="showDetail(\''+data+'\');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
                }else{
                    return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['docNumber']+'" title="Select this"><input type="hidden" value="'+row['docNumber']+'">&nbsp;&nbsp;'+data;
                }

            }

	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "companyDealer",
	"mDataProp": "companyDealer",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "tanggal",
	"mDataProp": "tanggal",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "staAprove",
	"mDataProp": "staAprove",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "staPrint",
	"mDataProp": "staPrint",
	"aTargets": [4],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
           return data=='DOWNLOAD'?'Ya':'-'
    },
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}



],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var companyDealer = $('#companyDealer').val();
						var Tanggal = $('#search_Tanggal').val();
						var TanggalDay = $('#search_Tanggal_day').val();
						var TanggalMonth = $('#search_Tanggal_month').val();
						var TanggalYear = $('#search_Tanggal_year').val();

						var Tanggal2 = $('#search_Tanggal2').val();
						var TanggalDay2 = $('#search_Tanggal2_day').val();
						var TanggalMonth2 = $('#search_Tanggal2_month').val();
						var TanggalYear2 = $('#search_Tanggal2_year').val();
						var noWo = $('#noWo').val();

                        if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
						    );
						}
						if(Tanggal){
							aoData.push(
									{"name": 'sCriteria_Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_Tanggal_dp', "value": Tanggal},
									{"name": 'sCriteria_Tanggal_day', "value": TanggalDay},
									{"name": 'sCriteria_Tanggal_month', "value": TanggalMonth},
									{"name": 'sCriteria_Tanggal_year', "value": TanggalYear}
							);
						}


						if(Tanggal2){
							aoData.push(
									{"name": 'sCriteria_Tanggal2', "value": "date.struct"},
									{"name": 'sCriteria_Tanggal2_dp', "value": Tanggal2},
									{"name": 'sCriteria_Tanggal2_day', "value": TanggalDay2},
									{"name": 'sCriteria_Tanggal2_month', "value": TanggalMonth2},
									{"name": 'sCriteria_Tanggal2_year', "value": TanggalYear2}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>



