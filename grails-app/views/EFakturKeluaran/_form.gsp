<%@ page import="com.kombos.maintable.EFakturKeluaran" %>

<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'Masa Pajak', 'error')} ">
	<label class="control-label" for="MASA_PAJAK">
		<g:message code="EFakturKeluaran.MASA_PAJAK.label" default="Masa Pajak" />

	</label>
	<div class="controls">
	<g:textField name="MASA_PAJAK" value="${EFakturInstance?.MASA_PAJAK}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'TAHUN_PAJAK', 'error')} ">
	<label class="control-label" for="TAHUN_PAJAK">
		<g:message code="EFakturKeluaran.TAHUN_PAJAK.label" default="Tahun Pajak" />

	</label>
	<div class="controls">
	<g:textField name="TAHUN_PAJAK" value="${EFakturInstance?.TAHUN_PAJAK}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'REFERENSI', 'error')} ">
	<label class="control-label" for="REFERENSI">
		<g:message code="EFakturKeluaran.REFERENSI.label" default="Referensi" />

	</label>
	<div class="controls">
	<g:textField name="REFERENSI" value="${EFakturInstance?.REFERENSI}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'NO_SI', 'error')} ">
	<label class="control-label" for="NO_SI">
		<g:message code="EFakturKeluaran.NO_SI.label" default="No. SI" />

	</label>
	<div class="controls">
	<g:textField name="NO_SI" value="${EFakturInstance?.NO_SI}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'TGL_SI', 'error')} ">
	<label class="control-label" for="TGL_SI">
		<g:message code="EFakturKeluaran.TGL_SI.label" default="Tanggal SI" />

	</label>
	<div class="controls">
	<ba:datePicker name="TGL_SI" id="TGL_SI" precision="day"  value="${EFakturInstance?.TGL_SI}" format="dd/mm/yyyy" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'NAMA_CUSTOMER', 'error')} ">
	<label class="control-label" for="NAMA_CUSTOMER">
		<g:message code="EFakturKeluaran.NAMA_CUSTOMER.label" default="Nama Customer" />

	</label>
	<div class="controls">
	<g:textField name="NAMA_CUSTOMER" value="${EFakturInstance?.NAMA_CUSTOMER}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'NPWP', 'error')} ">
	<label class="control-label" for="NPWP">
		<g:message code="EFakturKeluaran.NPWP.label" default="NPWP" />

	</label>
	<div class="controls">
	<g:textField name="NPWP" value="${EFakturInstance?.NPWP}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'ALAMAT', 'error')} ">
	<label class="control-label" for="ALAMAT">
		<g:message code="EFakturKeluaran.ALAMAT.label" default="Alamat" />

	</label>
	<div class="controls">
	<g:textField name="ALAMAT" value="${EFakturInstance?.ALAMAT}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'BIAYA_JASA', 'error')} ">
	<label class="control-label" for="BIAYA_JASA">
		<g:message code="EFakturKeluaran.BIAYA_JASA.label" default="Biaya Jasa" />

	</label>
	<div class="controls">
	<g:textField name="BIAYA_JASA" value="${fieldValue(bean: EFakturInstance, field: 'BIAYA_JASA')}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'BIAYA_PARTS', 'error')} ">
	<label class="control-label" for="BIAYA_PARTS">
		<g:message code="EFakturKeluaran.BIAYA_PARTS.label" default="Biaya Parts" />

	</label>
	<div class="controls">
	<g:textField name="BIAYA_PARTS" value="${fieldValue(bean: EFakturInstance, field: 'BIAYA_PARTS')}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'BIAYA_OLI', 'error')} ">
	<label class="control-label" for="BIAYA_OLI">
		<g:message code="EFakturKeluaran.BIAYA_OLI.label" default="Biaya Oli" />

	</label>
	<div class="controls">
	<g:textField name="BIAYA_OLI" value="${fieldValue(bean: EFakturInstance, field: 'BIAYA_OLI')}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'BIAYA_MATERIAL', 'error')} ">
	<label class="control-label" for="BIAYA_MATERIAL">
		<g:message code="EFakturKeluaran.BIAYA_MATERIAL.label" default="Biaya Material" />

	</label>
	<div class="controls">
	<g:textField name="BIAYA_MATERIAL" value="${fieldValue(bean: EFakturInstance, field: 'BIAYA_MATERIAL')}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'BIAYA_SUBLET', 'error')} ">
	<label class="control-label" for="BIAYA_SUBLET">
		<g:message code="EFakturKeluaran.BIAYA_SUBLET.label" default="Biaya Sublet" />

	</label>
	<div class="controls">
	<g:textField name="BIAYA_SUBLET" value="${fieldValue(bean: EFakturInstance, field: 'BIAYA_SUBLET')}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'BIAYA_ADM', 'error')} ">
	<label class="control-label" for="BIAYA_ADM">
		<g:message code="EFakturKeluaran.BIAYA_ADM.label" default="Biaya Admin" />

	</label>
	<div class="controls">
	<g:textField name="BIAYA_ADM" value="${fieldValue(bean: EFakturInstance, field: 'BIAYA_ADM')}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'DISC_JASA', 'error')} ">
	<label class="control-label" for="DISC_JASA">
		<g:message code="EFakturKeluaran.DISC_JASA.label" default="Diskon Jasa" />

	</label>
	<div class="controls">
	<g:textField name="DISC_JASA" value="${fieldValue(bean: EFakturInstance, field: 'DISC_JASA')}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'DISC_PARTS', 'error')} ">
	<label class="control-label" for="DISC_PARTS">
		<g:message code="EFakturKeluaran.DISC_PARTS.label" default="Diskon Parts" />

	</label>
	<div class="controls">
	<g:textField name="DISC_PARTS" value="${fieldValue(bean: EFakturInstance, field: 'DISC_PARTS')}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'DISC_BAHAN', 'error')} ">
	<label class="control-label" for="DISC_BAHAN">
		<g:message code="EFakturKeluaran.DISC_BAHAN.label" default="Diskon Bahan" />

	</label>
	<div class="controls">
	<g:textField name="DISC_BAHAN" value="${fieldValue(bean: EFakturInstance, field: 'DISC_BAHAN')}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'DPP', 'error')} ">
	<label class="control-label" for="DPP">
		<g:message code="EFakturKeluaran.DPP.label" default="DPP" />

	</label>
	<div class="controls">
	<g:textField name="DPP" value="${fieldValue(bean: EFakturInstance, field: 'DPP')}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'PPN', 'error')} ">
	<label class="control-label" for="PPN">
		<g:message code="EFakturKeluaran.PPN.label" default="PPN" />

	</label>
	<div class="controls">
	<g:textField name="PPN" value="${fieldValue(bean: EFakturInstance, field: 'PPN')}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'Total', 'error')} required">
	<label class="control-label" for="TOTAL">
		<g:message code="EFakturKeluaran.TOTAL.label" default="Total" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="TOTAL" value="${fieldValue(bean: EFakturInstance, field: 'TOTAL')}" required=""/>
	</div>
</div>

