<%@ page import="com.kombos.maintable.Company; com.kombos.hrd.Karyawan; com.kombos.customerprofile.HistoryCustomer; com.kombos.administrasi.VendorAsuransi; com.kombos.parts.Vendor; com.kombos.administrasi.CompanyDealer; com.kombos.parts.Konversi" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title>DATA</title>
    <style>
    #detail-table thead th, #detail-table tbody tr td  {font-size: 10px; white-space: nowrap;vertical-align: middle;}
    #detail-table tbody tr td {text-align: center;}
    #detail-table tbody td input[type="text"] {font-size: 10px; width: 100%; margin-top: 10px;}
    #detail-form {margin-left: 0px;} #btnSearchAccount {position: relative; left: 9px; top: 2px;}
    </style>
    <r:require modules="baseapplayout, baseapplist"/>
    <g:javascript>
        $(function(){
            $("input[type=radio][value='1']").attr('checked', 'checked');
             $("input:checkbox").change(function(){
             var group = ":checkbox[name='"+ $(this).attr("name") + "']";
               if($(this).is(':checked')){
                 $(group).not($(this)).attr("checked",false);
               }
             });

             checkAll = function(ele,val) {
                 var checkboxes = document.getElementsByTagName('input');
                 if (ele.checked) {
                     for (var i = 0; i < checkboxes.length; i++) {
                         if (checkboxes[i].type == 'checkbox' && (checkboxes[i].value != "All2" && checkboxes[i].value != "All1")) {
                             checkboxes[i].checked = false;
                         }
                         if (checkboxes[i].type == 'checkbox' && checkboxes[i].value == val) {
                             checkboxes[i].checked = true;
                         }
                     }
                 } else {
                     for (var i = 0; i < checkboxes.length; i++) {
                         if (checkboxes[i].type == 'checkbox' && (checkboxes[i].value != "All2" && checkboxes[i].value != "All1")) {
                             checkboxes[i].checked = true;
                         }
                         if (checkboxes[i].type == 'checkbox' && checkboxes[i].value == val) {
                             checkboxes[i].checked = false;
                         }
                     }
                 }
             }
            generateNofak = function(){
                var arr = new Array()
                var noFak = ""
                var dataid = ""
                var conf = confirm("Apakah Anda Yakin?")
                if(conf){
                    $("#detail-table tbody .row-select").each(function() {
                        if(this.checked){
                            var fak = this.value
                            var dataid = $(this).next("input:hidden").val()
                            var map = {
                                noFaktur : fak,
                                dataid : dataid
                            }
                            arr.push(map)
                        }
                    });
                    var params = {
                        arr : JSON.stringify(arr)
                    }
                    $.post("${request.getContextPath()}/EFakturKeluaran/generateNofak", params, function(data) {

                    if (!data.error){
                        toastr.success("Sukses Generate Nomor Faktur By System");
                        reloadEFakturTable();
                    }
                    else
                        alert("Internal Server Error");
                    });
                     toastr.success("Please Waite...");
                    console.log(arr);
                }
            }

        });
    </g:javascript>
</head>
<body>

<style type="text/css">
#adaFK{
    background-color: #faf7f7 !important;
}
#noFK{
    background-color: #FFFFFF !important;
}
</style>
<div class="box">
    <div class="span12" id="showDetail-table">
        <div class="row-fluid" style="overflow: auto">
            <table id="detail-table" class="display table table-striped table-bordered table-hover table-selectable fixed">
                <thead>
                <th>No.</th>
                <th><input type="checkbox" class="row-select" name="All" value="All1" onchange="checkAll(this,'1')">&nbsp; Pilihan Generate</th>
                <th>MASA_PAJAK</th>
                <th>TAHUN_PAJAK</th>
                <th>REFERENSI</th>
                <th>NO_SI</th>
                <th>TGL_SI</th>
                <th>NAMA_CUSTOMER</th>
                <th>NPWP</th>
                <th>ALAMAT</th>
                <th>BIAYA_JASA</th>
                <th>BIAYA_PARTS</th>
                <th>BIAYA_OLI</th>
                <th>BIAYA_MATERIAL</th>
                <th>BIAYA_SUBLET</th>
                <th>BIAYA_ADM</th>
                <th>DISC_JASA</th>
                <th>DISC_PARTS</th>
                <th>DISC_OLI</th>
                <th>DISC_BAHAN</th>
                <th>DPP</th>
                <th>PPN</th>
                <th>TOTAL</th>
                </thead>
                <g:if test="${dataFakturDetail}">
                    %{def nom = 0;
                    def id = "'adaFK'";
                    }%
                    <g:each in="${dataFakturDetail}" status="i" var="j" >
                        <tr>
                            %{ nom++;}%
                            <td %{out.print("id=" + id)}%>%{out.print(nom)}%</td>
                            <td %{out.print("id=" + id)}%><input type="checkbox" class="row-select" name="radio_${j.id}" value="1"><input type="hidden" name="data_${j.id}" value="${j.id}"></td>
                            <td %{out.print("id=" + id)}%>${j.getMASA_PAJAK()}</td>
                            <td %{out.print("id=" + id)}%>${j.getTAHUN_PAJAK()}</td>
                            <td %{out.print("id=" + id)}%>${j.getREFERENSI()}</td>
                            <td %{out.print("id=" + id)}%>${j.getNO_SI()}</td>
                            <td %{out.print("id=" + id)}%>${j.getTGL_SI()}</td>
                            <td %{out.print("id=" + id)}%>${j.getNAMA_CUSTOMER()}</td>
                            <td %{out.print("id=" + id)}%>${j.getNPWP()}</td>
                            <td %{out.print("id=" + id)}%>${j.getALAMAT()}</td>
                            <td %{out.print("id=" + id)}%>${j.getBIAYA_JASA()}</td>
                            <td %{out.print("id=" + id)}%>${j.getBIAYA_PARTS()}</td>
                            <td %{out.print("id=" + id)}%>${j.getBIAYA_OLI()}</td>
                            <td %{out.print("id=" + id)}%>${j.getBIAYA_MATERIAL()}</td>
                            <td %{out.print("id=" + id)}%>${j.getBIAYA_SUBLET()}</td>
                            <td %{out.print("id=" + id)}%>${j.getBIAYA_ADM()}</td>
                            <td %{out.print("id=" + id)}%>${j.getDISC_JASA()}</td>
                            <td %{out.print("id=" + id)}%>${j.getDISC_PARTS()}</td>
                            <td %{out.print("id=" + id)}%>${j.getDISC_OLI()}</td>
                            <td %{out.print("id=" + id)}%>${j.getDISC_BAHAN()}</td>
                            <td %{out.print("id=" + id)}%>${j.getDPP()}</td>
                            <td %{out.print("id=" + id)}%>${j.getPPN()}</td>
                            <td %{out.print("id=" + id)}%>${j.getTOTAL()}</td>

                            %{--<td %{out.print("id=" + id)}%>${j.getDPP()}</td/>--}%
                        </tr>
                    </g:each>
                </g:if>
            </table>
        </div>
        <button class="btn btn-primary save" data-dismiss="modal" name="SAVE" id="saveTrxDetail" onclick="generateNofak()" style="margin-top: 13px;">Generate</button>
        <button class="btn cancel" data-dismiss="modal" name="close" id="closeTrxDetail" style="margin-top: 13px;">Close</button>
    </div>
</div>

</body>
</html>