<%@ page import="com.kombos.administrasi.CompanyDealer"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="baseapplayout" />
<g:javascript disappointmentGrition="head">
	$(function(){		
        $('#filter_periode2').hide();
        $('#filter_periode3').hide();
        $('#filter_periode').show();
        $('#bulan1').change(function(){
            $('#bulan2').val($('#bulan1').val());
        });
        $('#bulan2').change(function(){
            if(parseInt($('#bulan1').val()) > parseInt($('#bulan2').val())){
                $('#bulan2').val($('#bulan1').val());
            }
        });
        $('#namaReport').change(function(){        
        	
        });


	    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    });
    var checkin = $('#search_tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
                checkin.hide();
                $('#search_tanggal2')[0].focus();
            }).data('datepicker');

    var checkout = $('#search_tanggal2').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
                checkout.hide();
    }).data('datepicker');

    previewData = function(){
        var namaReport = $('#namaReport').val();
        var workshop = $('#workshop').val();
        var search_tanggal = $('#search_tanggal').val();
        var search_tanggal2 = $('#search_tanggal2').val();
        var bulan1 = $('#bulan1').val();
        var bulan2= $('#bulan2').val();
        var tahun = $('#tahun').val();
        if(namaReport == "" || namaReport == null){
            alert('Harap Isi Data Dengan Lengkap');
            return;
        }
       /*
       window.location = "${request.contextPath}/production_gr/previewData?namaReport="+namaReport+"&workshop="+workshop+"&bulan1="+bulan1+"&bulan2="+bulan2+"&tahun="+tahun+"&tanggal="+search_tanggal+"&tanggal2="+search_tanggal2;
       */
    }


</g:javascript>

</head>
<body>
	<div class="navbar box-header no-border">Report – Production (GR)
	</div>
	<br>
	<div class="box">
		<div class="span12" id="appointmentGr-table">
			<div class="row-fluid form-horizontal">
				<div id="kiri" class="span6">
					<div class="control-group">
						<label class="control-label" for="t951Tanggal"
							style="text-align: left;"> Workshop </label>
						<div id="filter_wo" class="controls">
							%{--<g:select name="workshop" id="workshop"--}%
								%{--from="${CompanyDealer.list()}" optionKey="id"--}%
								%{--optionValue="m011NamaWorkshop" style="width: 44%" />--}%
                            <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                <g:select name="workshop" id="workshop" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                            </g:if>
                            <g:else>
                                <g:select name="workshop" id="workshop" readonly="" disabled="" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                            </g:else>

						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="t951Tanggal"
							style="text-align: left;"> Kategori Workshop </label>
						<div id="filter_workshop" class="controls">
							&nbsp; <b>General Repair</b>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="t951Tanggal"
							style="text-align: left;"> Nama Report </label>
						<div id="filter_nama" class="controls">
							<select name="namaReport" id="namaReport" size="8"
								style="width: 100%; height: 230px; font-size: 12px;">
								<option value="01">01. Goods BP - Goods (Daily)</option>								
								<option value="02">02. Goods BP - Goods (Weekly)</option>								
								<option value="03">03. Goods BP - Goods (Monthly)</option>								
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="t951Tanggal"
							style="text-align: left;"> Periode </label>
						<div id="filter_periode" class="controls">
							<ba:datePicker id="search_tanggal" name="search_tanggal"
								precision="day" format="dd-MM-yyyy" value="${new Date()}" />
							&nbsp;&nbsp;s.d.&nbsp;&nbsp;
							<ba:datePicker id="search_tanggal2" name="search_tanggal2"
								precision="day" format="dd-MM-yyyy" value="${new Date()+1}" />
						</div>
						<div id="filter_periode2" class="controls">
							%{ def listBulan =
							['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
							}% <select name="bulan1" id="bulan1" style="width: 170px">
								%{ for(int i=0;i<12;i++){ out.print('
								<option value="'+(i+1)+'">'+listBulan.get(i)+'</option>'); } }%
							</select> &nbsp; - &nbsp; <select name="bulan2" id="bulan2"
								style="width: 170px"> %{ for(int i=0;i<12;i++){
								out.print('
								<option value="'+(i+1)+'">'+listBulan.get(i)+'</option>'); } }%
							</select> &nbsp; <select name="tahun" id="tahun" style="width: 106px">
								%{ for(def a = (new Date().format("yyyy")).toInteger();a >= 2000
								;a--){ out.print('
								<option value="'+a+'">'+a+'</option>'); } }%

							</select>
						</div>
						<div id="filter_periode3" class="controls">
							<select name="tahun" id="tahun" style="width: 170px"> %{
								for(def a = (new Date().format("yyyy")).toInteger(); a >= 2000;
								a--) { out.print('
								<option value="'+a+'">'+a+'</option>'); } }%
							</select>
						</div>
					</div>					
				</div>				
			</div>
			<g:field type="button" onclick="previewData()"
				class="btn btn-primary create" name="preview"
				value="${message(code: 'default.button.upload.label', default: 'Preview')}" />
			<g:field type="button" onclick="window.location.replace('#/home')"
				class="btn btn-cancel cancel" name="cancel"
				value="${message(code: 'default.button.upload.label', default: 'Close')}" />
		</div>
	</div>
</body>
</html>
