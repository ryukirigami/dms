<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
		<g:javascript>
var POSubTable_${idTable};
$(function(){ 
var anOpen = [];
	$('#PO_datatables_sub_${idTable} td.subcontrol').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
  		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = POSubTable_${idTable}.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST', 
	   			data : oData,
	   			url:'${request.contextPath}/ETAPO/subsublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = POSubTable_${idTable}.fnOpen(nTr,data,'details');
    				$('div.innerinnerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
    		
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerinnerDetails', $(nTr).next()[0]).slideUp( function () {
      			POSubTable_${idTable}.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

    $('#PO_datatables_sub_${idTable} a.edit').live('click', function (e) {
        e.preventDefault();
         
        var nRow = $(this).parents('tr')[0];
         
        if ( nEditing !== null && nEditing != nRow ) {
            restoreRow( POSubTable_${idTable}, nEditing );
            editRow( POSubTable_${idTable}, nRow );
            nEditing = nRow;
        }
        else if ( nEditing == nRow && this.innerHTML == "Save" ) {
            saveRow( POSubTable_${idTable}, nEditing );
            nEditing = null;
        }
        else {
            editRow( POSubTable_${idTable}, nRow );
            nEditing = nRow;
        }
    } );
    if(POSubTable_${idTable})
    	POSubTable_${idTable}.dataTable().fnDestroy();
POSubTable_${idTable} = $('#PO_datatables_sub_${idTable}').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubList")}",
		"aoColumns": [
{
               "sName": "naon",
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": ''
},
{
                "sName": "nomorPO",
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "kodePart",
	"mDataProp": "kodePart",
	"aTargets": [0],

	"bSortable": false,
	"sWidth":"300px",
	"bVisible": true
},
{
	"sName": "namaPart",
	"mDataProp": "namaPart",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"300px",
	"bVisible": true
},
{
	"sName": "qty",
	"mDataProp": "qty",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "qtyIssue",
	"mDataProp": "qtyIssue",
	"bSortable": false,
	"sWidth":"200px",
	"sDefaultContent": '',
	"bVisible": true
},
{
	"sName": "qtySelisih",
	"mDataProp": "qtySelisih",
	"bSortable": false,
	"sWidth":"200px",
	"sDefaultContent": '',
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						aoData.push(
									{"name": 'tanggalRequest', "value": "${tanggalRequest}"}
						);

						aoData.push(
									{"name": 'nomorPO', "value": "${nomorPO}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
		</g:javascript>
	</head>
	<body>
<div class="innerDetails">
<table id="PO_datatables_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
	class="display table table-striped table-bordered table-hover" style="width: 100%">
    <thead>
        <tr>
           <th></th>
            <th></th>
			<th style="border-bottom: none; padding: 5px; width: 200px;">
				<div>Kode Parts</div>
			</th>
			<th style="border-bottom: none; padding: 5px; width: 200px;">
				<div>Nama Parts</div>
			</th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div>Qty</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div>Qty Issue</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div>Qty Selisih</div>
            </th>
         </tr>
			    </thead>
			    <tbody></tbody>
			</table>
</div>
</body>
</html>
