<%@ page import="com.kombos.parts.PO" %>



<div class="control-group fieldcontain ${hasErrors(bean: POInstance, field: 'goods', 'error')} ">
	<label class="control-label" for="goods">
		<g:message code="PO.goods.label" default="Goods" />
		
	</label>
	<div class="controls">
	<g:select id="goods" name="goods.id" from="${com.kombos.parts.Goods.list()}" optionKey="id" required="" value="${POInstance?.goods?.id}" class="many-to-one"/>
	</div>
</div>
<g:javascript>
    document.getElementById("goods").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: POInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="PO.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${POInstance?.lastUpdProcess}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: POInstance, field: 'request', 'error')} ">
	<label class="control-label" for="request">
		<g:message code="PO.request.label" default="Request" />
		
	</label>
	<div class="controls">
	<g:select id="request" name="request.id" from="${com.kombos.parts.Request.list()}" optionKey="id" required="" value="${POInstance?.request?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: POInstance, field: 't164HargaSatuan', 'error')} ">
	<label class="control-label" for="t164HargaSatuan">
		<g:message code="PO.t164HargaSatuan.label" default="T164 Harga Satuan" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="t164HargaSatuan" value="${POInstance.t164HargaSatuan}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: POInstance, field: 't164NoPO', 'error')} ">
	<label class="control-label" for="t164NoPO">
		<g:message code="PO.t164NoPO.label" default="T164 No PO" />
		
	</label>
	<div class="controls">
	<g:textField name="t164NoPO" value="${POInstance?.t164NoPO}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: POInstance, field: 't164Qty1', 'error')} ">
	<label class="control-label" for="t164Qty1">
		<g:message code="PO.t164Qty1.label" default="T164 Qty1" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="t164Qty1" value="${POInstance.t164Qty1}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: POInstance, field: 't164Qty2', 'error')} ">
	<label class="control-label" for="t164Qty2">
		<g:message code="PO.t164Qty2.label" default="T164 Qty2" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="t164Qty2" value="${POInstance.t164Qty2}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: POInstance, field: 't164StaOpenCancelClose', 'error')} ">
	<label class="control-label" for="t164StaOpenCancelClose">
		<g:message code="PO.t164StaOpenCancelClose.label" default="T164 Sta Open Cancel Close" />
		
	</label>
	<div class="controls">
	<g:textField name="t164StaOpenCancelClose" value="${POInstance?.t164StaOpenCancelClose}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: POInstance, field: 't164TglPO', 'error')} ">
	<label class="control-label" for="t164TglPO">
		<g:message code="PO.t164TglPO.label" default="T164 Tgl PO" />
		
	</label>
	<div class="controls">
	<ba:datePicker name="t164TglPO" precision="day" value="${POInstance?.t164TglPO}" format="yyyy-MM-dd"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: POInstance, field: 't164TotalHarga', 'error')} ">
	<label class="control-label" for="t164TotalHarga">
		<g:message code="PO.t164TotalHarga.label" default="T164 Total Harga" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="t164TotalHarga" value="${POInstance.t164TotalHarga}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: POInstance, field: 't164xNamaDivisi', 'error')} ">
	<label class="control-label" for="t164xNamaDivisi">
		<g:message code="PO.t164xNamaDivisi.label" default="T164x Nama Divisi" />
		
	</label>
	<div class="controls">
	<g:textField name="t164xNamaDivisi" value="${POInstance?.t164xNamaDivisi}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: POInstance, field: 't164xNamaUser', 'error')} ">
	<label class="control-label" for="t164xNamaUser">
		<g:message code="PO.t164xNamaUser.label" default="T164x Nama User" />
		
	</label>
	<div class="controls">
	<g:textField name="t164xNamaUser" value="${POInstance?.t164xNamaUser}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: POInstance, field: 'validasiOrder', 'error')} ">
	<label class="control-label" for="validasiOrder">
		<g:message code="PO.validasiOrder.label" default="Validasi Order" />
		
	</label>
	<div class="controls">
	<g:select id="validasiOrder" name="validasiOrder.id" from="${com.kombos.parts.ValidasiOrder.list()}" optionKey="id"value="${POInstance?.validasiOrder?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: POInstance, field: 'vendor', 'error')} ">
	<label class="control-label" for="vendor">
		<g:message code="PO.vendor.label" default="Vendor" />
		
	</label>
	<div class="controls">
	<g:select id="vendor" name="vendor.id" from="${com.kombos.parts.Vendor.list()}" optionKey="id" required="" value="${POInstance?.vendor?.id}" class="many-to-one"/>
	</div>
</div>

