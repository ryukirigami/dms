<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
		<g:javascript>
var POSubSubTable_${idTable};
$(function(){ 
POSubSubTable_${idTable} = $('#PO_datatables_sub_sub_${idTable}').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubSubList")}",
		"fnServerParams": function ( aoData ) {
         aoData.push( { "name": "goods", "value": "${goods}" },{"name":"nomorPO", "value":"${nomorPO}"} );
          },
		"aoColumns": [
{
               "sName": "naon2",
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"15px",
               "sDefaultContent": ''
},
{
               "sName": "naon3",
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"15px",
               "sDefaultContent": ''
},
{
               "sName": "naon3",
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
                "sWidth":"250px",
	            "sDefaultContent": ''
},
{
	"sName": "eta",
	"mDataProp": "eta",
	"aTargets": [0],
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "qtyIssue",
	"mDataProp": "qtyIssue",
	"aTargets": [1],
	"bSortable": false,
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						aoData.push(
									{"name": 'goods', "value": "${goods}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
		</g:javascript>
	</head>
	<body>
	<div class="innerinnerDetails">
<table id="PO_datatables_sub_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
	class="display table table-striped table-bordered table-hover">
    <thead>
        <tr>
           <th></th>
           <th></th>
		    <th></th>
            <th style="border-bottom: none; padding: 5px;">
				<div>ETA</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Qty Issue</div>
			</th>

         </tr>
			    </thead>
			    <tbody></tbody>
			</table>
	</div>
</body>
</html>
