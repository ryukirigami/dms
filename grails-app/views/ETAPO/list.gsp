
<%@ page import="com.kombos.parts.PO" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'PO.label', default: 'ETA PO')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			var tambahEta;
			var loadPopupTambahEta;
			var addETA;
			var searchPO;
			var loadUploadETA;
			$(function(){


			    loadUploadETA = function(){
			         $('#spinner').fadeIn(1);

                    $.ajax({
                        url: '${request.contextPath}/uploadETA',
                        type: "GET",dataType:"html",
                        complete : function (req, err) {
                            $('#main-content').html(req.responseText);
                            $('#spinner').fadeOut();
                        }
                    });

                    window.location = "${request.contextPath}/#/uploadETA/";

			    }
			
				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('PO');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('PO', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('PO', '${request.contextPath}/PO/massdelete', reloadPOTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('PO','${request.contextPath}/PO/show/'+id);
				};
				
				edit = function(id) {
					editInstance('PO','${request.contextPath}/PO/edit/'+id);
				};

			searchPO = function(){

			};


            tambahEta = function(){
               checkETA =[];
               var id;
                $("#PO-table tbody .row-select").each(function() {
                    if(this.checked){
                        id = $(this).next("input:hidden").val();
                        checkETA.push(id);
                    }
                });
                if(checkETA.length<1 || checkETA.length>1){
                    alert('Pilih Satu dan hanya boleh satu yang dipilih');
                    return;
                }
                else{
                    var checkETAMap = JSON.stringify(checkETA);

                    loadPopupTambahEta(id);
                }

            checkETA = [];

          };

        $("#tambahEtaModal").on("show", function() {
            $("#closeTambahEta").on("click", function(e) {
                $("#tambahEtaModal").modal('hide');
            });
        });
        $("#tambahEtaModal").on("hide", function() {
            $("#closeTambahEta").off("click");
        });

      loadPopupTambahEta = function(idPO){
       	$("#tambahEtaContent").empty();
		expandTableLayout();
		$.ajax({type:'POST', url:'${request.contextPath}/ETAPO/tambahETA',
   			success:function(data,textStatus){
					$("#tambahEtaContent").html(data);
				    $("#tambahEtaModal").modal({
						"backdrop" : "false",
						"keyboard" : true,
						"show" : true
					}).css({'width': '600px','margin-left': function () {return -($(this).width() / 2);}});

   			},
   			data : {idPO : idPO},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
   		};

       addETA = function(){



       var qtyETA = $("#qtyETA").val();

       if(qtyETA == "" || qtyETA == "0.00"){
         toastr.error("Qty tidak boleh kosong ataupun 0");
         $("#qtyETA").focus();
         return;
       }

       var konfirmasi = confirm('Apakah anda yakin akan disimpan?');

       if(!konfirmasi){
            return;
       }

        var idPO = $("#nomorPO").val();
        var tglETA = $("#tglETA").val();
        var jamETA = $("#jamETA").val();
        var tglETA_day = $("#tglETA_day").val();
        var tglETA_month = $("#tglETA_month").val();
        var tglETA_year = $("#tglETA_year").val();
        var menitETA = $("#menitETA").val();
        var qtyETA = $("#qtyETA").val();
        var idGoods = $("#idGoods").val();

        $.ajax({type:'POST', url:'${request.contextPath}/ETAPO/saveETA',
                success:function(){
                        toastr.success("Simpan ETA PO Sukses");
                        $("#tambahEtaContent").html("");
                        $("#tambahEtaModal").modal('hide');
                        $('#spinner').fadeOut();
                        reloadPOTable();

                },
                data : {idPO : idPO, tglETA : tglETA, jamETA : jamETA, menitETA : menitETA, qtyETA : qtyETA, idGoods : idGoods, tglETA_day : tglETA_day, tglETA_month: tglETA_month, tglETA_year: tglETA_year},
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
    }


});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>

	</div>
	<div class="box">
		<div class="span12" id="PO-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <fieldset>
                <table style="padding-right: 10px">
                    <tr>
                        <td style="width: 130px">
                            <label class="control-label" for="tanggalPO">
                                <g:message code="po.tanggal.label" default="Tanggal PO" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="tanggal_invoice" class="controls">
                                <ba:datePicker id="search_tglStart" name="search_tglStart" precision="day" format="dd-mm-yyyy"  value="" />
                                &nbsp;-&nbsp;
                                <ba:datePicker id="search_tglEnd" name="search_tglEnd" precision="day" format="dd-mm-yyyy"  value="" />
                            </div>
                        </td>
                    </tr>
                </table>
              </fieldset>
              <fieldset>
                  <legend>Kriteria PO</legend>
                <table style="padding-right: 10px" >
                    <tr>
                        <td>
                            <input type="checkbox" id="poETABelumLengkap" name="poETABelumLengkap" value="1">&nbsp;PO dengan ETA Belum Lengkap &nbsp;
                        </td>
                        <td>
                            <input type="checkbox" id="poSPLD" name="poSPLD" value="1">&nbsp;PO SPLD

                        </td>
                    </tr>
                    <tr>
                        <td >
                            <input type="checkbox" id="poETALengkap" name="poETALengkap" value="1">&nbsp;PO dengan ETA Lengkap
                        </td>
                        <td>
                            <input type="checkbox" id="poNonSPLD" name="poNonSPLD" value="1">&nbsp;PO Non SPLD

                        </td>
                    </tr>
                </table>
                <br/>
                <table style="padding-right: 10px">
                    <tr>
                        <td colspan="3" >
                            <div class="controls" style="right: 0">
                                <button class="btn btn-primary" name="view" id="view">Search</button>
                                <button class="btn btn-primary" name="clear" id="clear">Clear</button>
                            </div>
                        </td>
                    </tr>
                </table>
            </fieldset>
			<g:render template="dataTables" />
            <table>
                <tr>
                    <td>
                        <button class="btn btn-primary" name="add" id="add" onclick="tambahEta()">Input ETA</button> &nbsp;
                    </td>
                    <td>
                        &nbsp; <button class="btn btn-primary" name="btnUploadETA" id="btnUploadETA" onclick="loadUploadETA()">Upload ETA</button>
                    </td>
                </tr>
            </table>
		</div>
		<div class="span7" id="PO-form" style="display: none;"></div>
	</div>
    <div id="tambahEtaModal" class="modal fade">
        <div class="modal-dialog" style="width: 600px;">
            <div class="modal-content" style="width: 600px;">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 600px;">
                    <div id="tambahEtaContent"/>
                    %{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}%
                    <div class="iu-content"></div>

                </div>
                <!-- dialog buttons -->
                <div class="modal-footer">
                <g:field type="button" onclick="addETA();" class="btn btn-primary create" name="tambah" id="tambah" value="${message(code: 'default.button.tambahETA.label', default: 'Save')}" />
                <button id='closeTambahEta' type="button" class="btn btn-primary">Close</button>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
