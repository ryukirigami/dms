<%@ page import="com.kombos.maintable.EFakturMasukan" %>

%{--Baris 1--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'FM', 'error')} ">
    <label class="control-label" for="FM">
        <g:message code="EFaktur.FK.label" default="FM"/>

    </label>

    <div class="controls">
        <g:textField name="FM" value="${EFakturInstance?.FM}"/>
    </div>
</div>

%{--Baris 2--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'KD_JENIS_TRANSAKSI', 'error')} ">
    <label class="control-label" for="KD_JENIS_TRANSAKSI">
        <g:message code="EFaktur.KD_JENIS_TRANSAKSI.label" default="KDJENISTRANSAKSI"/>

    </label>

    <div class="controls">
        <g:textField name="KD_JENIS_TRANSAKSI" value="${EFakturInstance?.KD_JENIS_TRANSAKSI}"/>
    </div>
</div>

%{--Baris 3--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'FG_PENGGANTI', 'error')} ">
    <label class="control-label" for="FG_PENGGANTI">
        <g:message code="EFaktur.FG_PENGGANTI.label" default="FGPENGGANTI"/>

    </label>

    <div class="controls">
        <g:textField name="FG_PENGGANTI" value="${EFakturInstance?.FG_PENGGANTI}"/>
    </div>
</div>

%{--Baris 4--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'NOMOR_FAKTUR', 'error')} ">
    <label class="control-label" for="NOMOR_FAKTUR">
        <g:message code="EFaktur.NOMOR_FAKTUR.label" default="NOMORFAKTUR"/>

    </label>

    <div class="controls">
        <g:textField name="NOMOR_FAKTUR" value="${EFakturInstance?.NOMOR_FAKTUR}"/>
    </div>
</div>

%{--Baris 5--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'MASA_PAJAK', 'error')} ">
    <label class="control-label" for="MASA_PAJAK">
        <g:message code="EFaktur.MASA_PAJAK.label" default="MASAPAJAK"/>

    </label>

    <div class="controls">
        <g:textField name="MASA_PAJAK" value="${EFakturInstance?.MASA_PAJAK}"/>
    </div>
</div>

%{--Baris 6--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'TAHUN_PAJAK', 'error')} ">
    <label class="control-label" for="TAHUN_PAJAK">
        <g:message code="EFaktur.TAHUN_PAJAK.label" default="TAHUNPAJAK"/>

    </label>

    <div class="controls">
        <g:textField name="TAHUN_PAJAK" value="${EFakturInstance?.TAHUN_PAJAK}"/>
    </div>
</div>

%{--Baris 7--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'TANGGAL_FAKTUR', 'error')} ">
    <label class="control-label" for="TANGGAL_FAKTUR">
        <g:message code="EFaktur.TANGGAL_FAKTUR.label" default="TANGGALFAKTUR"/>

    </label>

    <div class="controls">
        <g:textField name="TANGGAL_FAKTUR" value="${EFakturInstance?.TANGGAL_FAKTUR}"/>
    </div>
</div>

%{--Baris 8--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'NPWP', 'error')} ">
    <label class="control-label" for="NPWP">
        <g:message code="EFaktur.NPWP.label" default="NPWP"/>

    </label>

    <div class="controls">
        <g:textField name="NPWP" value="${EFakturInstance?.NPWP}"/>
    </div>
</div>

%{--Baris 9--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'NAMA', 'error')} ">
    <label class="control-label" for="NAMA">
        <g:message code="EFaktur.NAMA_OBJEK.label" default="NAMA"/>

    </label>

    <div class="controls">
        <g:textField name="NAMA" value="${EFakturInstance?.NAMA}"/>
    </div>
</div>

%{--Baris 10--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'ALAMAT_LENGKAP', 'error')} ">
    <label class="control-label" for="ALAMAT_LENGKAP">
        <g:message code="EFaktur.ALAMAT_LENGKAP.label" default="ALAMATLENGKAP"/>

    </label>

    <div class="controls">
        <g:textField name="ALAMAT_LENGKAP" value="${EFakturInstance?.ALAMAT_LENGKAP}"/>
    </div>
</div>

%{--Baris 11--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'JUMLAH_DPP', 'error')} ">
    <label class="control-label" for="JUMLAH_DPP">
        <g:message code="EFaktur.JUMLAH_DPP.label" default="JUMLAHDPP"/>

    </label>

    <div class="controls">
        <g:textField name="JUMLAH_DPP" value="${EFakturInstance?.JUMLAH_DPP}"/>
    </div>
</div>

%{--Baris 12--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'JUMLAH_PPN', 'error')} ">
    <label class="control-label" for="JUMLAH_PPN">
        <g:message code="EFaktur.JUMLAH_PPN.label" default="JUMLAHPPN"/>

    </label>

    <div class="controls">
        <g:textField name="JUMLAH_PPN" value="${EFakturInstance?.JUMLAH_PPN}"/>
    </div>
</div>

%{--Baris 13--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'JUMLAH_PPNBM', 'error')} ">
    <label class="control-label" for="JUMLAH_PPNBM">
        <g:message code="EFaktur.JUMLAH_PPNBM.label" default="JUMLAHPPNBM"/>

    </label>

    <div class="controls">
        <g:textField name="JUMLAH_PPNBM" value="${EFakturInstance?.JUMLAH_PPNBM}"/>
    </div>
</div>

%{--Baris 43--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'IS_CREDITABLE', 'error')} ">
    <label class="control-label" for="IS_CREDITABLE">
        <g:message code="EFaktur.PPNBM.label" default="IS_CREDITABLE"/>

    </label>

    <div class="controls">
        <g:textField name="IS_CREDITABLE" value="${EFakturInstance?.IS_CREDITABLE}"/>
    </div>
</div>
%{--Baris Akhir--}%

