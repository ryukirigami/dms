<%@ page import="com.kombos.maintable.EFaktur" %>

<r:require modules="baseapplayout"/>
<g:render template="../menu/maxLineDisplay"/>

<table id="EFaktur_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="100%">
<thead>
<tr>

<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.ALAMAT_LENGKAP.label" default="FM"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.BLOK.label" default="KD_JENIS_TRANSAKSI"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.DISKON.label" default="FG_PENGGANTI"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.DPP.label" default="NOMOR_FAKTUR"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.FG_PENGGANTI.label" default="MASA_PAJAK"/></div>
</th>

<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.FG_UANG_MUKA.label" default="TAHUN_PAJAK"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.FK.label" default="TANGGAL_FAKTUR"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.HARGA_SATUAN.label" default="NPWP"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.HARGA_TOTAL.label" default="NAMA"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.ID_KETERANGAN_TAMBAHAN.label" default="ALAMAT_LENGKAP"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.JALAN.label" default="JUMLAH_DPP"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.LT.label" default="JUMLAH_PPN"/></div>
</th>

<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.MASA_PAJAK.label" default="JUMLAH_PPNBM"/></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.NAMA.label" default="IS_CREDITABLE"/></div>
</th>

<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="EFaktur.docNumberr.label" default="docNumber"/></div>
</th>


<th style="border-bottom: none;padding: 5px;width: 300px;">
    <div><g:message code="EFaktur.tanggal.label" default="Status Approval"/></div>
</th>

</tr>
<tr>


<th style="border-top: none;padding: 5px;">
    <div id="filter_FM" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_FM" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_KD_JENIS_TRANSAKSI" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_KD_JENIS_TRANSAKSI" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_FG_PENGGANTI" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_FG_PENGGANTI" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_NOMOR_FAKTUR" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_NOMOR_FAKTUR" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_MASA_PAJAK" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_MASA_PAJAK" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_TAHUN_PAJAK" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_TAHUN_PAJAK" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_TANGGAL_FAKTUR" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
        <input type="hidden" name="search_TANGGAL_FAKTUR" value="date.struct">
        <input type="hidden" name="search_TANGGAL_FAKTUR_day" id="search_TANGGAL_FAKTUR_day" value="">
        <input type="hidden" name="search_TANGGAL_FAKTUR_month" id="search_TANGGAL_FAKTUR_month" value="">
        <input type="hidden" name="search_TANGGAL_FAKTUR_year" id="search_TANGGAL_FAKTUR_year" value="">
        <input type="text" data-date-format="dd/mm/yyyy" name="search_TANGGAL_FAKTUR_dp" value="" id="search_TANGGAL_FAKTUR" placeholder="Date from.." class="search_init">

        <input type="hidden" name="search_TANGGAL_FAKTUR_to" value="date.struct">
        <input type="hidden" name="search_TANGGAL_FAKTUR_day_to" id="search_TANGGAL_FAKTUR_day_to" value="">
        <input type="hidden" name="search_TANGGAL_FAKTUR_month_to" id="search_TANGGAL_FAKTUR_month_to" value="">
        <input type="hidden" name="search_TANGGAL_FAKTUR_year_to" id="search_TANGGAL_FAKTUR_year_to" value="">
        <input type="text" data-date-format="dd/mm/yyyy" name="search_TANGGAL_FAKTUR_dp_to" value="" id="search_TANGGAL_FAKTUR_to" style="margin-top: 5px;" placeholder="Date to.." class="search_init">
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_NPWP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_NPWP" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_NAMA" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_NAMA" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_ALAMAT_LENGKAP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_ALAMAT_LENGKAP" class="search_init"/>
        <input type="hidden" name="docNumber" id="docNumber" class="search_init" value="${docNumber}"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_JUMLAH_DPP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_JUMLAH_DPP" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_JUMLAH_PPN" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_JUMLAH_PPN" class="search_init"/>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_JUMLAH_PPNBM" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_JUMLAH_PPNBM" class="search_init"/>
    </div>
</th>
<th style="border-top: none;padding: 5px;">
    <div id="filter_IS_CREDITABLE" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_IS_CREDITABLE" class="search_init"/>
    </div>
</th>
<th style="border-top: none;padding: 5px;">
    <div id="filter_docNumberr" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_docNumberr" class="search_init"/>
    </div>
</th>
<th style="border-top: none;padding: 5px;">
    <div id="filter_staAprove" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <select name="search_staAprove" id="search_staAprove" style="width:100%" class="search_init" onchange="reloadEFakturTable();">
            <option value=""></option>
            <option value="2">Rejected</option>
            <option value="1">Approved</option>
            <option value="0">Wait</option>
        </select>
    </div>
</th>

</tr>
</thead>
</table>
<g:javascript>
var EFakturTable;
var reloadEFakturTable;
$(function(){

	reloadEFakturTable = function() {
		EFakturTable.fnDraw();
	}

$('#search_TANGGAL_FAKTUR').datepicker().on('changeDate', function(ev) {
    var newDate = new Date(ev.date);
    $('#search_TANGGAL_FAKTUR_day').val(newDate.getDate());
    $('#search_TANGGAL_FAKTUR_month').val(newDate.getMonth()+1);
    $('#search_TANGGAL_FAKTUR_year').val(newDate.getFullYear());
    $(this).datepicker('hide');
});

$('#search_TANGGAL_FAKTUR_to').datepicker().on('changeDate', function(ev) {
    var newDate = new Date(ev.date);
    $('#search_TANGGAL_FAKTUR_day_to').val(newDate.getDate());
    $('#search_TANGGAL_FAKTUR_month_to').val(newDate.getMonth()+1);
    $('#search_TANGGAL_FAKTUR_year_to').val(newDate.getFullYear());
    $(this).datepicker('hide');
    EFakturTable.fnDraw();
});

    var recordsefakturperpage = [];
    var anefakturSelected;
    var jmlRecefakturPerPage=0;
    var id;


	$('#search_tanggal').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tanggal_day').val(newDate.getDate());
			$('#search_tanggal_month').val(newDate.getMonth()+1);
			$('#search_tanggal_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			EFakturTable.fnDraw();
	});




$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	EFakturTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	EFakturTable = $('#EFaktur_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
	        "fnDrawCallback": function () {
            var rsEfaktur = $("#EFaktur_datatables tbody .row-select");
            var jmlEfakturCek = 0;
            var nRow;
            var idRec;
            rsEfaktur.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsefakturperpage[idRec]=="1"){
                    jmlEfakturCek = jmlEfakturCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsefakturperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecefakturPerPage = rsEfaktur.length;
            if(jmlEfakturCek==jmlRecefakturPerPage && jmlRecefakturPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },

		   "fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : 30, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "FM",
	"mDataProp": "FM",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
	    if(data!=null && data!=" " && data!="-"){
		    return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	    }else{
	        return '<input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	    }
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,
{
	"sName": "KD_JENIS_TRANSAKSI",
	"mDataProp": "KD_JENIS_TRANSAKSI",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "FG_PENGGANTI",
	"mDataProp": "FG_PENGGANTI",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "NOMOR_FAKTUR",
	"mDataProp": "NOMOR_FAKTUR",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "MASA_PAJAK",
	"mDataProp": "MASA_PAJAK",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "TAHUN_PAJAK",
	"mDataProp": "TAHUN_PAJAK",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "TANGGAL_FAKTUR",
	"mDataProp": "TANGGAL_FAKTUR",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "NPWP",
	"mDataProp": "NPWP",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "NAMA",
	"mDataProp": "NAMA",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "ALAMAT_LENGKAP",
	"mDataProp": "ALAMAT_LENGKAP",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "JUMLAH_DPP",
	"mDataProp": "JUMLAH_DPP",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "JUMLAH_PPN",
	"mDataProp": "JUMLAH_PPN",
	"aTargets": [11],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "JUMLAH_PPNBM",
	"mDataProp": "JUMLAH_PPNBM",
	"aTargets": [12],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "IS_CREDITABLE",
	"mDataProp": "IS_CREDITABLE",
	"aTargets": [13],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "docNumberr",
	"mDataProp": "docNumberr",
	"aTargets": [14],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,


{
	"sName": "staAprove",
	"mDataProp": "staAprove",
	"aTargets": [15],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						var Tanggal = $('#search_Tanggal').val();
						var TanggalDay = $('#search_Tanggal_day').val();
						var TanggalMonth = $('#search_Tanggal_month').val();
						var TanggalYear = $('#search_Tanggal_year').val();

						var Tanggal2 = $('#search_Tanggal2').val();
						var TanggalDay2 = $('#search_Tanggal2_day').val();
						var TanggalMonth2 = $('#search_Tanggal2_month').val();
						var TanggalYear2 = $('#search_Tanggal2_year').val();

						if(Tanggal){
							aoData.push(
									{"name": 'sCriteria_Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_Tanggal_dp', "value": Tanggal},
									{"name": 'sCriteria_Tanggal_day', "value": TanggalDay},
									{"name": 'sCriteria_Tanggal_month', "value": TanggalMonth},
									{"name": 'sCriteria_Tanggal_year', "value": TanggalYear}
							);
						}


						if(Tanggal2){
							aoData.push(
									{"name": 'sCriteria_Tanggal2', "value": "date.struct"},
									{"name": 'sCriteria_Tanggal2_dp', "value": Tanggal2},
									{"name": 'sCriteria_Tanggal2_day', "value": TanggalDay2},
									{"name": 'sCriteria_Tanggal2_month', "value": TanggalMonth2},
									{"name": 'sCriteria_Tanggal2_year', "value": TanggalYear2}
							);
						}

						var KD_JENIS_TRANSAKSI = $('#filter_KD_JENIS_TRANSAKSI input').val();
						if(KD_JENIS_TRANSAKSI){
							aoData.push(
									{"name": 'sCriteria_KD_JENIS_TRANSAKSI', "value": KD_JENIS_TRANSAKSI}
							);
						}

						var FG_PENGGANTI = $('#filter_FG_PENGGANTI input').val();
						if(FG_PENGGANTI){
							aoData.push(
									{"name": 'sCriteria_FG_PENGGANTI', "value": FG_PENGGANTI}
							);
						}

						var NOMOR_FAKTUR = $('#filter_NOMOR_FAKTUR input').val();
						if(NOMOR_FAKTUR){
							aoData.push(
									{"name": 'sCriteria_NOMOR_FAKTUR', "value": NOMOR_FAKTUR}
							);
						}


						var MASA_PAJAK = $('#filter_MASA_PAJAK input').val();
						if(MASA_PAJAK){
							aoData.push(
									{"name": 'sCriteria_MASA_PAJAK', "value": MASA_PAJAK}
							);
						}

						var TAHUN_PAJAK = $('#filter_TAHUN_PAJAK input').val();
						if(TAHUN_PAJAK){
							aoData.push(
									{"name": 'sCriteria_TAHUN_PAJAK', "value": TAHUN_PAJAK}
							);
						}

						var TANGGAL_FAKTUR = $('#search_TANGGAL_FAKTUR').val();
						var TANGGAL_FAKTURDay = $('#search_TANGGAL_FAKTUR_day').val();
						var TANGGAL_FAKTURMonth = $('#search_TANGGAL_FAKTUR_month').val();
						var TANGGAL_FAKTURYear = $('#search_TANGGAL_FAKTUR_year').val();

						if(TANGGAL_FAKTUR){
							aoData.push(
									{"name": 'sCriteria_TANGGAL_FAKTURDate', "value": "date.struct"},
									{"name": 'sCriteria_TANGGAL_FAKTUR_dp', "value": TANGGAL_FAKTUR},
									{"name": 'sCriteria_TANGGAL_FAKTUR_day', "value": TANGGAL_FAKTURDay},
									{"name": 'sCriteria_TANGGAL_FAKTUR_month', "value": TANGGAL_FAKTURMonth},
									{"name": 'sCriteria_TANGGAL_FAKTUR_year', "value": TANGGAL_FAKTURYear}
							);
						}

						var TANGGAL_FAKTURTo = $('#search_TANGGAL_FAKTUR_to').val();
						var TANGGAL_FAKTURDayTo = $('#search_TANGGAL_FAKTUR_day_to').val();
						var TANGGAL_FAKTURMonthTo = $('#search_TANGGAL_FAKTUR_month_to').val();
						var TANGGAL_FAKTURYearTo = $('#search_TANGGAL_FAKTUR_year_to').val();

						if (TANGGAL_FAKTURTo) {
						    aoData.push(
									{"name": 'sCriteria_TANGGAL_FAKTUR_to', "value": "date.struct"},
									{"name": 'sCriteria_TANGGAL_FAKTUR_dp_to', "value": TANGGAL_FAKTURTo},
									{"name": 'sCriteria_TANGGAL_FAKTUR_day_to', "value": TANGGAL_FAKTURDayTo},
									{"name": 'sCriteria_TANGGAL_FAKTUR_month_to', "value": TANGGAL_FAKTURMonthTo},
									{"name": 'sCriteria_TANGGAL_FAKTUR_year_to', "value": TANGGAL_FAKTURYearTo}
							);
						}


						var NPWP = $('#filter_NPWP input').val();
						if(NPWP){
							aoData.push(
									{"name": 'sCriteria_NPWP', "value": NPWP}
							);
						}

						var NAMA = $('#filter_NAMA input').val();
						if(NAMA){
							aoData.push(
									{"name": 'sCriteria_NAMA', "value": NAMA}
							);
						}

						var ALAMAT_LENGKAP = $('#filter_ALAMAT_LENGKAP input').val();
						if(ALAMAT_LENGKAP){
							aoData.push(
									{"name": 'sCriteria_ALAMAT_LENGKAP', "value": ALAMAT_LENGKAP}
							);
						}

						var JUMLAH_DPP = $('#filter_JUMLAH_DPP input').val();
						if(JUMLAH_DPP){
							aoData.push(
									{"name": 'sCriteria_JUMLAH_DPP', "value": JUMLAH_DPP}
							);
						}

						var JUMLAH_PPN = $('#filter_JUMLAH_PPN input').val();
						if(JUMLAH_PPN){
							aoData.push(
									{"name": 'sCriteria_JUMLAH_PPN', "value": JUMLAH_PPN}
							);
						}

						var JUMLAH_PPNBM = $('#filter_JUMLAH_PPNBM input').val();
						if(JUMLAH_PPNBM){
							aoData.push(
									{"name": 'sCriteria_JUMLAH_PPNBM', "value": JUMLAH_PPNBM}
							);
						}

						var IS_CREDITABLE = $('#filter_IS_CREDITABLE input').val();
						if(IS_CREDITABLE){
							aoData.push(
									{"name": 'sCriteria_IS_CREDITABLE', "value": IS_CREDITABLE}
							);
						}
                    	var docNumber = $('#docNumber').val();
						if(docNumber){
							aoData.push(
									{"name": 'sCriteria_docNumber', "value": docNumber}
							);
						}


						var docNumberr = $('#filter_docNumberr input').val();
						if(docNumberr){
							aoData.push(
									{"name": 'sCriteria_docNumberr', "value": docNumberr}
							);
						}
						var staAprove = $('#search_staAprove').val();
						if(staAprove){
							aoData.push(
									{"name": 'sCriteria_staAprove', "value": staAprove}
							);
						}


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>


			
