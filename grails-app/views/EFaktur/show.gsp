<%@ page import="com.kombos.maintable.EFaktur" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="bootstrapeditable"/>
    <g:set var="entityName"
           value="${message(code: 'EFaktur.label', default: 'EFaktur - PPN MASUKAN DETIL')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <g:javascript disposition="head">
var deleteEFaktur;

$(function(){
	deleteEFaktur=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/EFaktur/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadEFakturTable();
   				expandTableLayout('EFaktur');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

    </g:javascript>
</head>

<body>
<div id="show-EFaktur" role="main">
<legend>
    <g:message code="default.show.label" args="[entityName]"/>
</legend>
<g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
</g:if>
<table id="EFaktur"
       class="table table-bordered table-hover">
<tbody>

<g:if test="${EFakturInstance?.docNumber}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="docNumber-label" class="property-label"><g:message
                    code="EFaktur.docNumber.label" default="Doc Number"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="docNumber-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="docNumber"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter docNumber" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="docNumber"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.companyDealer}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="companyDealer-label" class="property-label"><g:message
                    code="EFaktur.companyDealer.label" default="Company Dealer"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="companyDealer-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="companyDealer"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter companyDealer" onsuccess="reloadEFakturTable();" />--}%

            %{--<g:link controller="companyDealer" action="show"--}%
            %{--id="${EFakturInstance?.companyDealer?.id}">${EFakturInstance?.companyDealer?.encodeAsHTML()}</g:link>--}%
            <g:fieldValue bean="${EFakturInstance}" field="companyDealer"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.tanggal}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="tanggal-label" class="property-label"><g:message
                    code="EFaktur.tanggal.label" default="TanggalUpload"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="tanggal-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="tanggal"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter tanggal" onsuccess="reloadEFakturTable();" />--}%

            <g:formatDate date="${EFakturInstance?.tanggal}"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.FM}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="FM-label" class="property-label"><g:message
                    code="EFaktur.FK.label" default="FM"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="FM-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="FK"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter FK" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="FM"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.KD_JENIS_TRANSAKSI}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="KD_JENIS_TRANSAKSI-label" class="property-label"><g:message
                    code="EFaktur.KD_JENIS_TRANSAKSI.label" default="KDJENISTRANSAKSI"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="KD_JENIS_TRANSAKSI-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="KD_JENIS_TRANSAKSI"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter KD_JENIS_TRANSAKSI" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="KD_JENIS_TRANSAKSI"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.FG_PENGGANTI}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="FG_PENGGANTI-label" class="property-label"><g:message
                    code="EFaktur.FG_PENGGANTI.label" default="FGPENGGANTI"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="FG_PENGGANTI-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="FG_PENGGANTI"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter FG_PENGGANTI" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="FG_PENGGANTI"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.NOMOR_FAKTUR}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="NOMOR_FAKTUR-label" class="property-label"><g:message
                    code="EFaktur.NOMOR_FAKTUR.label" default="NOMORFAKTUR"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="NOMOR_FAKTUR-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="NOMOR_FAKTUR"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter NOMOR_FAKTUR" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="NOMOR_FAKTUR"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.MASA_PAJAK}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="MASA_PAJAK-label" class="property-label"><g:message
                    code="EFaktur.MASA_PAJAK.label" default="MASAPAJAK"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="MASA_PAJAK-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="MASA_PAJAK"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter MASA_PAJAK" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="MASA_PAJAK"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.TAHUN_PAJAK}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="TAHUN_PAJAK-label" class="property-label"><g:message
                    code="EFaktur.TAHUN_PAJAK.label" default="TAHUNPAJAK"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="TAHUN_PAJAK-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="TAHUN_PAJAK"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter TAHUN_PAJAK" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="TAHUN_PAJAK"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.TANGGAL_FAKTUR}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="TANGGAL_FAKTUR-label" class="property-label"><g:message
                    code="EFaktur.TANGGAL_FAKTUR.label" default="TANGGALFAKTUR"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="TANGGAL_FAKTUR-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="TANGGAL_FAKTUR"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter TANGGAL_FAKTUR" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="TANGGAL_FAKTUR"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.NPWP}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="NPWP-label" class="property-label"><g:message
                    code="EFaktur.NPWP.label" default="NPWP"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="NPWP-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="NPWP"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter NPWP" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="NPWP"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.NAMA}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="NAMA-label" class="property-label"><g:message
                    code="EFaktur.NAMA.label" default="NAMA"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="NAMA-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="NAMA"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter NAMA" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="NAMA"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.ALAMAT_LENGKAP}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="ALAMAT_LENGKAP-label" class="property-label"><g:message
                    code="EFaktur.ALAMAT_LENGKAP.label" default="ALAMATLENGKAP"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="ALAMAT_LENGKAP-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="ALAMAT_LENGKAP"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter ALAMAT_LENGKAP" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="ALAMAT_LENGKAP"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.JUMLAH_DPP}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="JUMLAH_DPP-label" class="property-label"><g:message
                    code="EFaktur.JUMLAH_DPP.label" default="JUMLAHDPP"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="JUMLAH_DPP-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="JUMLAH_DPP"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter JUMLAH_DPP" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="JUMLAH_DPP"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.JUMLAH_PPN}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="JUMLAH_PPN-label" class="property-label"><g:message
                    code="EFaktur.JUMLAH_PPN.label" default="JUMLAHPPN"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="JUMLAH_PPN-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="JUMLAH_PPN"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter JUMLAH_PPN" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="JUMLAH_PPN"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.JUMLAH_PPNBM}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="JUMLAH_PPNBM-label" class="property-label"><g:message
                    code="EFaktur.JUMLAH_PPNBM.label" default="JUMLAHPPNBM"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="JUMLAH_PPNBM-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="JUMLAH_PPNBM"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter JUMLAH_PPNBM" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="JUMLAH_PPNBM"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.IS_CREDITABLE}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="IS_CREDITABLE-label" class="property-label"><g:message
                    code="EFaktur.IS_CREDITABLE.label" default="IS_CREDITABLE"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="IS_CREDITABLE-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="ID_KETERANGAN_TAMBAHAN"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter ID_KETERANGAN_TAMBAHAN" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="IS_CREDITABLE"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.dateCreated}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="dateCreated-label" class="property-label"><g:message
                    code="EFaktur.dateCreated.label" default="Date Created"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="dateCreated-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="dateCreated"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter dateCreated" onsuccess="reloadEFakturTable();" />--}%

            <g:formatDate date="${EFakturInstance?.dateCreated}"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.lastUpdated}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="lastUpdated-label" class="property-label"><g:message
                    code="EFaktur.lastUpdated.label" default="Last Updated"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="lastUpdated-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="lastUpdated"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter lastUpdated" onsuccess="reloadEFakturTable();" />--}%

            <g:formatDate date="${EFakturInstance?.lastUpdated}"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.lastUpdProcess}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="lastUpdProcess-label" class="property-label"><g:message
                    code="EFaktur.lastUpdProcess.label" default="Last Upd Process"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="lastUpdProcess-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="lastUpdProcess"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter lastUpdProcess" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="lastUpdProcess"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.createdBy}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="createdBy-label" class="property-label"><g:message
                    code="EFaktur.createdBy.label" default="Created By"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="createdBy-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="createdBy"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter createdBy" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="createdBy"/>

        </span></td>

    </tr>
</g:if>



</tbody>
</table>
<g:form class="form-horizontal">
    <fieldset class="buttons controls">
        <a class="btn cancel" href="javascript:void(0);"
           onclick="expandTableLayout('EFaktur');"><g:message
                code="default.button.back.label" default="Close"/></a>
    </fieldset>
</g:form>
</div>
</body>
</html>
