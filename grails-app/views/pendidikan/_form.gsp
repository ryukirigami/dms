<%@ page import="com.kombos.hrd.Pendidikan" %>

<div class="control-group fieldcontain ${hasErrors(bean: pendidikanInstance, field: 'pendidikan', 'error')} ">
    <label class="control-label" for="pendidikan">
        <g:message code="pendidikan.pendidikan.label" default="Pendidikan" />

    </label>
    <div class="controls">
        <g:textField name="pendidikan" value="${pendidikanInstance?.pendidikan}" required="true" maxlength="16" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: pendidikanInstance, field: 'keterangan', 'error')} ">
	<label class="control-label" for="keterangan">
		<g:message code="pendidikan.keterangan.label" default="Keterangan" />
		
	</label>
	<div class="controls">
	<g:textArea rows="5" cols="3" name="keterangan" value="${pendidikanInstance?.keterangan}" maxlength="128" />
	</div>
</div>

<g:javascript>
    $(function() {
        oFormUtils.fnInit();
    })
</g:javascript>



