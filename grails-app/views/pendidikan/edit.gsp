<%@ page import="com.kombos.hrd.Pendidikan" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'pendidikan.label', default: 'Pendidikan')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="edit-pendidikan" class="content scaffold-edit" role="main">
			<legend><g:message code="default.edit.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${pendidikanInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${pendidikanInstance}" var="error">
                    <li>Field '${error.field}' harus unique!</li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form method="post" >--}%
			<g:formRemote class="form-horizontal" name="edit" on404="alert('not found!');" onSuccess="reloadPendidikanTable();" update="pendidikan-form"
				url="[controller: 'pendidikan', action:'update']">
				<g:hiddenField name="id" value="${pendidikanInstance?.id}" />
				<g:hiddenField name="version" value="${pendidikanInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons controls">
					<g:actionSubmit class="btn btn-primary save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                    <a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout('pendidikan');"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
				</fieldset>
			</g:formRemote>
			%{--</g:form>--}%
		</div>
	</body>
</html>
