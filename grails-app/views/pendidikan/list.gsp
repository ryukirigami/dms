
<%@ page import="com.kombos.hrd.Pendidikan" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'pendidikan.label', default: 'Pendidikan')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			var oFormUtils;
			var aoCheckedValues;
			var aoUncheckedValues;
			var bIsCheckAllClicked;
			$(function(){ 

			    oFormUtils = {
			        fnInit: function() {
			            $("#pendidikan").focus();
			            $("<span style='color: red; position: absolute; margin-left: -13px; margin-top: 4px;'>*</span>").insertBefore("input[required='true']");
			        },
			        fnMassDelete: function() {
			            var values = [];
			            var type;// 0: ga check all, 1: check all
			            if (bIsCheckAllClicked) {
                            for (var m in aoUncheckedValues) {
                                if (!aoUncheckedValues[m].value) {
                                    values.push(aoUncheckedValues[m].code);
                                }
                            }
                            type = 1;
			            } else {
                            for (var m in aoCheckedValues) {
                                if (aoCheckedValues[m].value) {
                                    values.push(aoCheckedValues[m].code);
                                }
                            }
                            type = 0;
			            }

                        var json = JSON.stringify(values);
                        $.ajax({
                            url:"${request.contextPath}/pendidikan/massdelete",
                            type: "POST", // Always use POST when deleting data
                            data: { ids: json, type: type },
                            complete: function(xhr, status) {
                                 reloadPendidikanTable();
                                 aoCheckedValues = {}
                            }
                        });
			        }
			    }

				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('pendidikan');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('pendidikan', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
                                        oFormUtils.fnMassDelete();
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('pendidikan','${request.contextPath}/pendidikan/show/'+id);
				};
				
				edit = function(id) {
					editInstance('pendidikan','${request.contextPath}/pendidikan/edit/'+id);
				};

});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="pendidikan-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
		</div>
		<div class="span7" id="pendidikan-form" style="display: none;"></div>
	</div>
</body>
</html>
