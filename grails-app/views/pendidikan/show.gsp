

<%@ page import="com.kombos.hrd.Pendidikan" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'pendidikan.label', default: 'Pendidikan')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deletePendidikan;

$(function(){ 
	deletePendidikan=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/pendidikan/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadPendidikanTable();
   				expandTableLayout('pendidikan');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-pendidikan" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="pendidikan"
			class="table table-bordered table-hover">
			<tbody>
			
				<g:if test="${pendidikanInstance?.pendidikan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="pendidikan-label" class="property-label"><g:message
					code="pendidikan.pendidikan.label" default="Pendidikan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="pendidikan-label">
						%{--<ba:editableValue
								bean="${pendidikanInstance}" field="pendidikan"
								url="${request.contextPath}/Pendidikan/updatefield" type="text"
								title="Enter pendidikan" onsuccess="reloadPendidikanTable();" />--}%
							
								<g:fieldValue bean="${pendidikanInstance}" field="pendidikan"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${pendidikanInstance?.keterangan}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="keterangan-label" class="property-label"><g:message
                                code="pendidikan.keterangan.label" default="Keterangan" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="keterangan-label">
                        %{--<ba:editableValue
                                bean="${pendidikanInstance}" field="keterangan"
                                url="${request.contextPath}/Pendidikan/updatefield" type="text"
                                title="Enter keterangan" onsuccess="reloadPendidikanTable();" />--}%

                        <g:fieldValue bean="${pendidikanInstance}" field="keterangan"/>

                    </span></td>

                </tr>
            </g:if>



            <g:if test="${pendidikanInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="pendidikan.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${pendidikanInstance}" field="createdBy"
                                url="${request.contextPath}/Pendidikan/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadPendidikanTable();" />--}%

                        <g:fieldValue bean="${pendidikanInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${pendidikanInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="pendidikan.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${pendidikanInstance}" field="dateCreated"
                                url="${request.contextPath}/Pendidikan/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadPendidikanTable();" />--}%

                        <g:formatDate date="${pendidikanInstance?.dateCreated}" format="dd/MM/yyyy HH:mm:ss" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${pendidikanInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="pendidikan.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${pendidikanInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/Pendidikan/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadPendidikanTable();" />--}%

                        <g:fieldValue bean="${pendidikanInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${pendidikanInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="pendidikan.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${pendidikanInstance}" field="lastUpdated"
                                url="${request.contextPath}/Pendidikan/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadPendidikanTable();" />--}%

                        <g:formatDate date="${pendidikanInstance?.lastUpdated}" format="dd/MM/yyyy HH:mm:ss" />

                    </span></td>

                </tr>
            </g:if>


				<g:if test="${pendidikanInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="pendidikan.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${pendidikanInstance}" field="updatedBy"
								url="${request.contextPath}/Pendidikan/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadPendidikanTable();" />--}%
							
								<g:fieldValue bean="${pendidikanInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('pendidikan');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${pendidikanInstance?.id}"
					update="[success:'pendidikan-form',failure:'pendidikan-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deletePendidikan('${pendidikanInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
