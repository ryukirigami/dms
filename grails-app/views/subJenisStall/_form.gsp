<%@ page import="com.kombos.administrasi.JenisStall; com.kombos.administrasi.SubJenisStall" %>



<div class="control-group fieldcontain ${hasErrors(bean: subJenisStallInstance, field: 'jenisStall', 'error')} required">
	<label class="control-label" for="jenisStall">
		<g:message code="subJenisStall.jenisStall.label" default="Nama Jenis Stall" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="jenisStall" name="jenisStall.id" from="${JenisStall.createCriteria().list {eq("staDel", "0");order("m021NamaJenisStall", "asc")}}" optionKey="id" required="" value="${subJenisStallInstance?.jenisStall?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: subJenisStallInstance, field: 'm023NamaSubJenisStall', 'error')} required">
	<label class="control-label" for="m023NamaSubJenisStall">
		<g:message code="subJenisStall.m023NamaSubJenisStall.label" default="Nama Sub Jenis Stall" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m023NamaSubJenisStall" maxlength="50" required="" value="${subJenisStallInstance?.m023NamaSubJenisStall}"/>
	</div>
</div>
%{--
<div class="control-group fieldcontain ${hasErrors(bean: subJenisStallInstance, field: 'm023ID', 'error')} ">
	<label class="control-label" for="m023ID">
		<g:message code="subJenisStall.m023ID.label" default="M023 ID" />
		
	</label>
	<div class="controls">
	<g:textField name="m023ID" maxlength="1" value="${subJenisStallInstance?.m023ID}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: subJenisStallInstance, field: 'staDel', 'error')} required">
	<label class="control-label" for="staDel">
		<g:message code="subJenisStall.staDel.label" default="Sta Del" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="staDel" maxlength="1" required="" value="${subJenisStallInstance?.staDel}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: subJenisStallInstance, field: 'createdBy', 'error')} ">
	<label class="control-label" for="createdBy">
		<g:message code="subJenisStall.createdBy.label" default="Created By" />
		
	</label>
	<div class="controls">
	<g:textField name="createdBy" value="${subJenisStallInstance?.createdBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: subJenisStallInstance, field: 'updatedBy', 'error')} ">
	<label class="control-label" for="updatedBy">
		<g:message code="subJenisStall.updatedBy.label" default="Updated By" />
		
	</label>
	<div class="controls">
	<g:textField name="updatedBy" value="${subJenisStallInstance?.updatedBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: subJenisStallInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="subJenisStall.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${subJenisStallInstance?.lastUpdProcess}"/>
	</div>
</div>

--}%