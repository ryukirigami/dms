

<%@ page import="com.kombos.administrasi.SubJenisStall" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'subJenisStall.label', default: 'Sub Jenis Stall')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteSubJenisStall;

$(function(){ 
	deleteSubJenisStall=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/subJenisStall/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadSubJenisStallTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-subJenisStall" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="subJenisStall"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${subJenisStallInstance?.jenisStall}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="jenisStall-label" class="property-label"><g:message
					code="subJenisStall.jenisStall.label" default="Jenis Stall" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="jenisStall-label">
						%{--<ba:editableValue
								bean="${subJenisStallInstance}" field="jenisStall"
								url="${request.contextPath}/SubJenisStall/updatefield" type="text"
								title="Enter jenisStall" onsuccess="reloadSubJenisStallTable();" />--}%
							
								${subJenisStallInstance?.jenisStall?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${subJenisStallInstance?.m023NamaSubJenisStall}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m023NamaSubJenisStall-label" class="property-label"><g:message
					code="subJenisStall.m023NamaSubJenisStall.label" default="M023 Nama Sub Jenis Stall" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m023NamaSubJenisStall-label">
						%{--<ba:editableValue
								bean="${subJenisStallInstance}" field="m023NamaSubJenisStall"
								url="${request.contextPath}/SubJenisStall/updatefield" type="text"
								title="Enter m023NamaSubJenisStall" onsuccess="reloadSubJenisStallTable();" />--}%
							
								<g:fieldValue bean="${subJenisStallInstance}" field="m023NamaSubJenisStall"/>
							
						</span></td>
					
				</tr>
				</g:if>
            %{--
                <g:if test="${subJenisStallInstance?.m023ID}">
                <tr>
                <td class="span2" style="text-align: right;"><span
                    id="m023ID-label" class="property-label"><g:message
                    code="subJenisStall.m023ID.label" default="M023 ID" />:</span></td>

                    <td class="span3"><span class="property-value"
                        aria-labelledby="m023ID-label">
                        %{--<ba:editableValue
                                bean="${subJenisStallInstance}" field="m023ID"
                                url="${request.contextPath}/SubJenisStall/updatefield" type="text"
                                title="Enter m023ID" onsuccess="reloadSubJenisStallTable();" />
							
								<g:fieldValue bean="${subJenisStallInstance}" field="m023ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
            --}%
            %{--
                <g:if test="${subJenisStallInstance?.staDel}">
                <tr>
                <td class="span2" style="text-align: right;"><span
                    id="staDel-label" class="property-label"><g:message
                    code="subJenisStall.staDel.label" default="Sta Del" />:</span></td>

                        <td s="span3"><span class="property-value"
                        aria-labelledby="staDel-label">
                        %{--<ba:editableValue
                                bean="${subJenisStallInstance}" field="staDel"
                                url="${request.contextPath}/SubJenisStall/updatefield" type="text"
                                title="Enter staDel" onsuccess="reloadSubJenisStallTable();" />
							
								<g:fieldValue bean="${subJenisStallInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
            --}%
			
				<g:if test="${subJenisStallInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="subJenisStall.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${subJenisStallInstance}" field="lastUpdProcess"
								url="${request.contextPath}/SubJenisStall/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadSubJenisStallTable();" />--}%
							
								<g:fieldValue bean="${subJenisStallInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${subJenisStallInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="subJenisStall.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${subJenisStallInstance}" field="dateCreated"
								url="${request.contextPath}/SubJenisStall/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadSubJenisStallTable();" />--}%
							
								<g:formatDate date="${subJenisStallInstance?.dateCreated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${subJenisStallInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="subJenisStall.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${subJenisStallInstance}" field="createdBy"
                                url="${request.contextPath}/SubJenisStall/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadSubJenisStallTable();" />--}%

                        <g:fieldValue bean="${subJenisStallInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${subJenisStallInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="subJenisStall.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${subJenisStallInstance}" field="lastUpdated"
								url="${request.contextPath}/SubJenisStall/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadSubJenisStallTable();" />--}%
							
								<g:formatDate date="${subJenisStallInstance?.lastUpdated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${subJenisStallInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="subJenisStall.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${subJenisStallInstance}" field="updatedBy"
                                url="${request.contextPath}/SubJenisStall/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadSubJenisStallTable();" />--}%

                        <g:fieldValue bean="${subJenisStallInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${subJenisStallInstance?.id}"
					update="[success:'subJenisStall-form',failure:'subJenisStall-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteSubJenisStall('${subJenisStallInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
