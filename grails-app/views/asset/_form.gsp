<%@ page import="com.kombos.finance.Asset" %>



<div class="control-group fieldcontain ${hasErrors(bean: assetInstance, field: 'assetCode', 'error')} ">
	<label class="control-label" for="assetCode">
		<g:message code="asset.assetCode.label" default="Kode Asset" />
		
	</label>
	<div class="controls">
        <g:if test="${params.opname}">
                ${fieldValue(bean: assetInstance, field: 'assetCode')}

        </g:if>
        <g:elseif test="${assetInstance.assetCode}">
            <g:textField name="assetCode" value="${assetInstance?.assetCode}"/>
        </g:elseif>
        <g:else>
	        <g:textField name="assetCode" value="${assetInstance?.assetCode}"/>
        </g:else>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: assetInstance, field: 'assetName', 'error')} ">
	<label class="control-label" for="assetName">
		<g:message code="asset.assetName.label" default="Nama Asset" />
	</label>
	<div class="controls">
        <g:if test="${params.opname}">
            ${fieldValue(bean: assetInstance, field: 'assetName')}
        </g:if>
        <g:else>
	        <g:textField name="assetName" value="${assetInstance?.assetName}"  />
        </g:else>
	</div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: assetInstance, field: 'purchaseDate', 'error')} ">
    <label class="control-label" for="purchaseDate">
        <g:message code="asset.purchaseDate.label" default="Tahun Perolehan" />

    </label>
    <div class="controls">
        <g:if test="${params.opname}">
            ${fieldValue(bean: assetInstance, field: 'purchaseDate')}
        </g:if>
        <g:else>
            <g:textField  name="purchaseDate" value="${fieldValue(bean: assetInstance, field: 'purchaseDate')}" />

            %{--<ba:datePicker name="purchaseDate" precision="day" value="${tugasLuarKaryawanInstance?.dateBegin}" format="yyyy-MM-dd"/>--}%
        </g:else>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: assetInstance, field: 'price', 'error')} ">
    <label class="control-label" for="price">
        <g:message code="asset.price.label" default="Nilai Perolehan" />
    </label>
    <div class="controls">
        <g:if test="${params.opname}">
            ${fieldValue(bean: assetInstance, field: 'price')}
        </g:if>
        <g:else>
            <g:textField  name="price" class="numeric" value="${fieldValue(bean: assetInstance, field: 'price')}" />
        </g:else>
    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: assetInstance, field: 'assetStatus', 'error')} ">
	<label class="control-label" for="assetStatus">
		<g:message code="asset.assetStatus.label" default="Asset Status" />
	</label>
	<div class="controls">
	<g:select id="assetStatus" name="assetStatus.id" from="${com.kombos.finance.AssetStatus.list()}" optionKey="id" required="" value="${assetInstance?.assetStatus?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: assetInstance, field: 'assetType', 'error')} ">
	<label class="control-label" for="assetType">
		<g:message code="asset.assetType.label" default="Tipe Asset" />
	</label>
	<div class="controls">
        <g:if test="${params.opname}">
            ${fieldValue(bean: assetInstance, field: 'assetType')}
        </g:if>
        <g:else>
            <g:select id="assetType" name="assetType.id" from="${com.kombos.finance.AssetType.list()}" optionKey="id" required="" value="${assetInstance?.assetType?.id}" class="many-to-one"/>
        </g:else>
	</div>
</div>

<g:if test="${params.opname}">

    <div class="control-group fieldcontain ${hasErrors(bean: assetInstance, field: 'assetType', 'error')} ">
        <label class="control-label" for="tanggalOpname">
            Tanggal Opname
        </label>
        <div class="controls">
            <ba:datePicker format="dd/mm/yyyy" name="tanggalOpname" required="true" />
        </div>
    </div>

</g:if>

<div class="control-group fieldcontain ${hasErrors(bean: assetInstance, field: 'quantity', 'error')} ">
    <label class="control-label" for="quantity">
        <g:message code="asset.quantity.label" default="Jumlah" />

    </label>
    <div class="controls">
        <g:if test="${params.opname}">
            <g:fieldValue field="quantity" bean="${assetInstance}"/>
        </g:if>
        <g:else>
            <g:field type="number" name="quantity" value="${assetInstance.quantity}"  />
        </g:else>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: assetInstance, field: 'location', 'error')} ">
    <label class="control-label" for="location">
        <g:message code="asset.location.label" default="Lokasi" />

    </label>
    <div class="controls">
        <g:textField name="location" value="${assetInstance?.location}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: assetInstance, field: 'user', 'error')} ">
    <label class="control-label" for="user">
        <g:message code="asset.user.label" default="Pengguna" />

    </label>
    <div class="controls">
        <g:textField name="user" value="${assetInstance?.user}" />
    </div>
</div>

<g:if test="${params.opname}">

    <div class="control-group fieldcontain ${hasErrors(bean: assetInstance, field: 'user', 'error')} ">
        <label class="control-label" for="petugasOpname">
            <g:message code="asset.user.label" default="Petugas Opname" />

        </label>
        <div class="controls">
            <g:textField name="petugasOpname" value="" />
        </div>
    </div>

</g:if>

<div class="control-group fieldcontain ${hasErrors(bean: assetInstance, field: 'description', 'error')} ">
	<label class="control-label" for="description">
		<g:message code="asset.description.label" default="Keterangan" />
		
	</label>
	<div class="controls">
        <g:textArea cols="5" rows="3" name="description" value="${assetInstance?.description}" />
	</div>
</div>
