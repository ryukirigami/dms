

<%@ page import="com.kombos.finance.Asset" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'asset.label', default: 'Asset')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteAsset;

$(function(){ 
	deleteAsset=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/asset/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadAssetTable();
   				expandTableLayout('asset');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-asset" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="asset"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${assetInstance?.assetCode}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="assetCode-label" class="property-label"><g:message
					code="asset.assetCode.label" default="Asset Code" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="assetCode-label">
						%{--<ba:editableValue
								bean="${assetInstance}" field="assetCode"
								url="${request.contextPath}/Asset/updatefield" type="text"
								title="Enter assetCode" onsuccess="reloadAssetTable();" />--}%
							
								<g:fieldValue bean="${assetInstance}" field="assetCode"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetInstance?.assetName}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="assetName-label" class="property-label"><g:message
					code="asset.assetName.label" default="Asset Name" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="assetName-label">
						%{--<ba:editableValue
								bean="${assetInstance}" field="assetName"
								url="${request.contextPath}/Asset/updatefield" type="text"
								title="Enter assetName" onsuccess="reloadAssetTable();" />--}%
							
								<g:fieldValue bean="${assetInstance}" field="assetName"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetInstance?.assetOpname}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="assetOpname-label" class="property-label"><g:message
					code="asset.assetOpname.label" default="Asset Opname" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="assetOpname-label">
						%{--<ba:editableValue
								bean="${assetInstance}" field="assetOpname"
								url="${request.contextPath}/Asset/updatefield" type="text"
								title="Enter assetOpname" onsuccess="reloadAssetTable();" />--}%
							
								<g:each in="${assetInstance.assetOpname}" var="a">
								<g:link controller="assetOpname" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link>
								</g:each>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetInstance?.assetStatus}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="assetStatus-label" class="property-label"><g:message
					code="asset.assetStatus.label" default="Asset Status" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="assetStatus-label">
						%{--<ba:editableValue
								bean="${assetInstance}" field="assetStatus"
								url="${request.contextPath}/Asset/updatefield" type="text"
								title="Enter assetStatus" onsuccess="reloadAssetTable();" />--}%
							
								<g:link controller="assetStatus" action="show" id="${assetInstance?.assetStatus?.id}">${assetInstance?.assetStatus?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetInstance?.assetType}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="assetType-label" class="property-label"><g:message
					code="asset.assetType.label" default="Asset Type" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="assetType-label">
						%{--<ba:editableValue
								bean="${assetInstance}" field="assetType"
								url="${request.contextPath}/Asset/updatefield" type="text"
								title="Enter assetType" onsuccess="reloadAssetTable();" />--}%
							
								<g:link controller="assetType" action="show" id="${assetInstance?.assetType?.id}">${assetInstance?.assetType?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="asset.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${assetInstance}" field="createdBy"
								url="${request.contextPath}/Asset/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadAssetTable();" />--}%
							
								<g:fieldValue bean="${assetInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="asset.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${assetInstance}" field="dateCreated"
								url="${request.contextPath}/Asset/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadAssetTable();" />--}%
							
								<g:formatDate date="${assetInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetInstance?.description}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="description-label" class="property-label"><g:message
					code="asset.description.label" default="Description" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="description-label">
						%{--<ba:editableValue
								bean="${assetInstance}" field="description"
								url="${request.contextPath}/Asset/updatefield" type="text"
								title="Enter description" onsuccess="reloadAssetTable();" />--}%
							
								<g:fieldValue bean="${assetInstance}" field="description"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="asset.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${assetInstance}" field="lastUpdProcess"
								url="${request.contextPath}/Asset/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadAssetTable();" />--}%
							
								<g:fieldValue bean="${assetInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="asset.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${assetInstance}" field="lastUpdated"
								url="${request.contextPath}/Asset/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadAssetTable();" />--}%
							
								<g:formatDate date="${assetInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetInstance?.location}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="location-label" class="property-label"><g:message
					code="asset.location.label" default="Location" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="location-label">
						%{--<ba:editableValue
								bean="${assetInstance}" field="location"
								url="${request.contextPath}/Asset/updatefield" type="text"
								title="Enter location" onsuccess="reloadAssetTable();" />--}%
							
								<g:fieldValue bean="${assetInstance}" field="location"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetInstance?.price}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="price-label" class="property-label"><g:message
					code="asset.price.label" default="Price" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="price-label">
						%{--<ba:editableValue
								bean="${assetInstance}" field="price"
								url="${request.contextPath}/Asset/updatefield" type="text"
								title="Enter price" onsuccess="reloadAssetTable();" />--}%
							
								<g:fieldValue bean="${assetInstance}" field="price"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetInstance?.purchaseDate}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="purchaseDate-label" class="property-label"><g:message
					code="asset.purchaseDate.label" default="Tahun Perolehan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="purchaseDate-label">
						%{--<ba:editableValue
								bean="${assetInstance}" field="purchaseDate"
								url="${request.contextPath}/Asset/updatefield" type="text"
								title="Enter purchaseDate" onsuccess="reloadAssetTable();" />--}%
							
                                <g:fieldValue bean="${assetInstance}" field="purchaseDate"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetInstance?.quantity}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="quantity-label" class="property-label"><g:message
					code="asset.quantity.label" default="Quantity" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="quantity-label">
						%{--<ba:editableValue
								bean="${assetInstance}" field="quantity"
								url="${request.contextPath}/Asset/updatefield" type="text"
								title="Enter quantity" onsuccess="reloadAssetTable();" />--}%
							
								<g:fieldValue bean="${assetInstance}" field="quantity"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="asset.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${assetInstance}" field="staDel"
								url="${request.contextPath}/Asset/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadAssetTable();" />--}%
							
								<g:fieldValue bean="${assetInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="asset.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${assetInstance}" field="updatedBy"
								url="${request.contextPath}/Asset/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadAssetTable();" />--}%
							
								<g:fieldValue bean="${assetInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${assetInstance?.user}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="user-label" class="property-label"><g:message
					code="asset.user.label" default="User" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="user-label">
						%{--<ba:editableValue
								bean="${assetInstance}" field="user"
								url="${request.contextPath}/Asset/updatefield" type="text"
								title="Enter user" onsuccess="reloadAssetTable();" />--}%
							
								<g:fieldValue bean="${assetInstance}" field="user"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('asset');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${assetInstance?.id}"
					update="[success:'asset-form',failure:'asset-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteAsset('${assetInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
