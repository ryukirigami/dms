
<%@ page import="com.kombos.finance.Asset" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'asset.label', default: 'Asset')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
        <style>
        #filter_tanggalPembelian_to {margin-top: 10px;}
        </style>
		<r:require modules="baseapplayout, baseapplist, autoNumeric" />
		<g:javascript>
			var show;
			var edit;
			var opname;
			var cancel;
			$(function(){ 
			
				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('asset');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('asset', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('asset', '${request.contextPath}/asset/massdelete', reloadAssetTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('asset','${request.contextPath}/asset/show/'+id);

				};
				
				edit = function(id) {
					editInstance('asset','${request.contextPath}/asset/edit/'+id);
				};

				opname = function(id) {
				    shrinkTableLayout('asset');
				    editInstance('opname', '${request.contextPath}/asset/edit/'+id+'?opname=true');
				}

				cancel = function() {
				    $("#asset-table").fadeIn();
				    $("#opname-form").fadeOut();
				    reloadAssetTable();
				}

});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="asset-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
			<g:render template="dataTables" />
		</div>
		<div class="span7" id="asset-form" style="display: none;"></div>
        <div class="span7" id="opname-form" style="display: none;"></div>
	</div>
</body>
</html>
