
<%@ page import="com.kombos.finance.Asset" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="asset_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="asset.assetCode.label" default="Asset Code" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="asset.assetName.label" default="Asset Name" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="asset.assetStatus.label" default="Kondisi" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="asset.assetType.label" default="Tipe Asset" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="asset.purchaseDate.label" default="Tahun Perolehan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="asset.quantity.label" default="Jumlah" /></div>
			</th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="asset.quantity.label" default="Action" /></div>
            </th>

		</tr>
    <tr>


        <th style="border-top: none;padding: 5px;">
            <div id="filter_asset_code" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_code" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_asset_name" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_nama" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_kondisi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_kondisi" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_tipe_asset" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_tipe_asset" class="search_init" />
            </div>
        </th>


        <th style="border-top: none;padding: 5px;">
            <div id="filter_tahun_perolehan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_tahun_perolehan" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_jumlah" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_jumlah" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
           &nbsp;
        </th>
    </tr>
	</thead>
</table>

<g:javascript>
var assetTable;
var reloadAssetTable;
$(function(){
	
	reloadAssetTable = function() {
		assetTable.fnDraw();
	}

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	assetTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	assetTable = $('#asset_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "assetCode",
	"mDataProp": "assetCode",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "assetName",
	"mDataProp": "assetName",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}



,

{
	"sName": "assetStatus",
	"mDataProp": "assetStatus",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "assetType",
	"mDataProp": "assetType",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "purchaseDate",
	"mDataProp": "purchaseDate",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "quantity",
	"mDataProp": "quantity",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},

{
	"sName": "action",
	"mDataProp": "assetCode",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return "<a href='javascript:void(0);' onclick='opname("+row['id']+");'>Opname</a>";
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}




],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {


						var assetCode = $('#filter_asset_code input').val();
						if(assetCode){
							aoData.push(
									{"name": 'sCriteria_assetCode', "value": assetCode}
							);
						}
	
						var assetName = $('#filter_asset_name input').val();
						if(assetName){
							aoData.push(
									{"name": 'sCriteria_assetName', "value": assetName}
							);
						}

						var kondisi = $('#filter_kondisi input').val();
						if(kondisi){
							aoData.push(
									{"name": 'sCriteria_kondisi', "value": kondisi}
							);
						}

						var assetType = $('#filter_tipe_asset input').val();
						if(assetType){
							aoData.push(
									{"name": 'sCriteria_assetType', "value": assetType}
							);
						}

						var tahunPerolehan = $('#filter_tahun_perolehan input').val();
						if(tahunPerolehan){
							aoData.push(
									{"name": 'sCriteria_tahunPerolehan', "value": tahunPerolehan}
							);
						}

						var jumlah = $('#filter_jumlah input').val();
						if(jumlah){
							aoData.push(
									{"name": 'sCriteria_jumlah', "value": jumlah}
							);
						}



						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
