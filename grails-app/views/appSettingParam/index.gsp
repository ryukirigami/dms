<%@ page import="com.kombos.baseapp.SecurityChecklist; com.kombos.baseapp.AppSettingParam" %>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    %{--<g:set var="entityName" value="${message(code: 'parent.label', default: 'Parent')}" />--}%
    %{--<title><g:message code="default.create.label" args="[entityName]" /></title>--}%

    <script type="text/javascript">
        function openTab(category){
            var idTab = "#"+category;
            var idDiv = "#div-"+category;
            jQuery(".app-setting-tab").removeClass("active");
            jQuery(idTab).addClass("active");

            jQuery(".app-setting-div").hide();
            jQuery(idDiv).show();
        }

        function saveAppSettingParam(category){
            saveValue(category,'saveAppSettingParam');
        }

        function saveSecurityChecklist(category){
            saveValue(category,'saveSecurityChecklist');
        }

        function saveValue(category,action){
            var classCategory = "."+category;
            var kvArray = [];
            var i = 0;
            jQuery(classCategory).each(function() {
                var kv = {};
                kv.code = this.id;
                if(this.type == 'checkbox'){
                    kv.value = this.checked;
                } else{
                    kv.value = this.value;
                }
                kvArray.push(kv);
                i++;
            });

            jQuery.ajax({
                type: 'POST',
                url: '${request.getContextPath()}/appSettingParam/'+action,
                data: {
                    valueInArray : kvArray,
                    valueInArrayLength : i
                },
                async: false,
                success: function(data) {
                    alert(data.message);
                },
                error: function(){
                    alert('Server Response Error, Cannot Get User Permission');
                }
            });
        }

        jQuery(".NUMERIC").keydown(function(event) {
            // Allow: backspace, delete, tab, escape, and enter
            if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
                // Allow: Ctrl+A
                    (event.keyCode == 65 && event.ctrlKey === true) ||
                // Allow: home, end, left, right
                    (event.keyCode >= 35 && event.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            else {
                // Ensure that it is a number and stop the keypress
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault();
                }
            }
        });
    </script>
</head>
<body>
<h3 class="navbar box-header no-border">
<g:message code="app.appSettingParam.label"	default="Application Setting Parameter" />
</h3>
<div class="box">
    <ul class="nav nav-tabs">
        <%
            def listOfAppSettingParams = AppSettingParam.executeQuery('select distinct category from AppSettingParam order by category desc')
            int i = 0
            def category
            listOfAppSettingParams?.each {
                category = ((String)it).replaceAll("\\s+", "")
        %>
        <li id="<%=category%>" class="app-setting-tab <%=i==0?"active":""%>">
            <a href="javascript:openTab('<%=category%>')"><%=it.toString().toUpperCase()%></a>
        </li>
        <%
                i++
            }
        %>
        <%--Adding Security Checklist--%>
        <%
            def listOfSecurityChecklist = SecurityChecklist.executeQuery('select distinct category from SecurityChecklist order by category desc')
            i = 0
            listOfSecurityChecklist?.each {
                category = ((String)it).replaceAll("\\s+", "")
        %>
        <li id="<%=category%>" class="app-setting-tab">
            <a href="javascript:openTab('<%=category%>')"><%=it.toString().toUpperCase()%></a>
        </li>
        <%
            }
        %>
    </ul>
    <%
        def listOfAppSettingParamsByCategory
        i = 0
        listOfAppSettingParams?.each {
            category = ((String)it).replaceAll("\\s+", "")
    %>
    <div id="<%="div-"+category%>" class="app-setting-div" style="<%=i==0?"":"display:none;"%>">
        <form class="form-horizontal" method="post">
            <fieldset class="form">
                <%
                        listOfAppSettingParamsByCategory = AppSettingParam.findAllByCategory(it)
                        listOfAppSettingParamsByCategory?.each {
                %>
                <div class="control-group fieldcontain  ">
                    <label for="<%=it.code%>" class="control-label"><g:message code="app.appSettingParam.${it.code}.label"	default="${it.label}" /></label>
                    <%
                            if(it.listSource){
                    %>
                    <div class="controls">
                        <g:select id="${it.code}" value="${it.value}" name="${it.code}" class="${category}" from="${com.kombos.baseapp.AppSettingParam.getListFormat(it.listSource)}" noSelection="['': '']"/>
                        <i>&nbsp;&nbsp;<g:message code="app.appSettingParam.${it.code}.description"	default="" /></i>
                    </div>
                    <%
                            }else{
                    %>
                    <div class="controls">
                        <input type="text" id="<%=it.code%>" value="<%=it.value%>" name="<%=it.code%>" class="<%=category%> <%=it.type%>">
                        <i>&nbsp;&nbsp;<g:message code="app.appSettingParam.${it.code}.description"	default="" /></i>
                    </div>
                    <%
                            }
                    %>
                </div>
                <%
                        }
                %>
            </fieldset>
            <fieldset class="buttons controls">
                <input type="button" value="Save" class="btn btn-primary save" onclick="javascript:saveAppSettingParam('<%=category%>');">
            </fieldset>
        </form>
    </div>
    <%
            i++
        }
    %>

    <%--Adding Security Checklist list--%>
    <%
        def listOfSecurityChecklistByCategory
        def listOfSecurityChecklistByCategoryChildren
        def query
        def type
        def checked
        def code
    %>

    <%
        listOfSecurityChecklist?.each {
            category = ((String)it).replaceAll("\\s+", "")
    %>
    <div id="<%="div-"+category%>" class="app-setting-div" style="display:none;">
        <form class="form-horizontal" method="post">
            <fieldset class="form">
                <%
                        //                listOfSecurityChecklistByCategory = SecurityChecklist.findAllByCategory(it)
                        listOfSecurityChecklistByCategory = SecurityChecklist.executeQuery("from SecurityChecklist as s where parent is null and category = '"+it+"'")
                        listOfSecurityChecklistByCategory?.each {
                            type = ((String)it.type).equalsIgnoreCase("BOOLEAN")?"checkbox":"text"
                            checked = (type.equalsIgnoreCase("checkbox"))?(it.enable==true?"checked":""):""
                            listOfSecurityChecklistByCategoryChildren = it.children
                            code = it.code
                            if(listOfSecurityChecklistByCategoryChildren.size() != 0){
                %>
                <div class="box">
                <div class="control-group fieldcontain">
                    <legend>
                        <%=it.label%>
                        <input type="<%=type%>" id="<%=it.code%>" <%=checked%> value="" name="<%=it.code%>" class="<%=category%>">
                    </legend>
                </div>
                <%
                        listOfSecurityChecklistByCategoryChildren.each{
                            type = ((String)it.type).equalsIgnoreCase("BOOLEAN")?"checkbox":"text"
                            checked = (type.equalsIgnoreCase("checkbox"))?(it.enable?"checked":""):""
                %>
                <div class="control-group fieldcontain <%=code%>" style="margin-left: 80px;">
                    <label for="<%=it.code%>" class="control-label"><g:message code="app.securityChecklist.${it.code}.label"	default="${it.label}" /></label>
                    <div class="controls">
                        <input type="<%=type%>" id="<%=it.code%>" <%=(type.equalsIgnoreCase("checkbox"))?checked:""%> value="<%=it.value%>" name="<%=it.code%>" class="<%=category%> <%=it.type%>">
                        <i>&nbsp;&nbsp;<g:message code="app.securityChecklist.${it.code}.description"	default="" /></i>
                    </div>
                </div>
                <%
                        }
                %>
                </div>
                <%
                    }else{
                %>
                <div class="control-group fieldcontain">
                    <label for="<%=it.code%>" class="control-label"><g:message code="app.securityChecklist.${it.code}.label"	default="${it.label}" /></label>
                    <div class="controls">
                        <input type="<%=type%>" id="<%=it.code%>" <%=(type.equalsIgnoreCase("checkbox"))?checked:""%> value="<%=it.value%>" name="<%=it.code%>" class="<%=category%> <%=it.type%>">
                        <i>&nbsp;&nbsp;<g:message code="app.securityChecklist.${it.code}.description"	default="" /></i>
                    </div>
                </div>
                <%
                            }
                        }
                %>
            </fieldset>
            <fieldset class="buttons controls">
                <input type="button" value="Save" class="btn btn-primary save" onclick="javascript:saveSecurityChecklist('<%=category%>');">
            </fieldset>
        </form>
    </div>
    <%
        }
    %>
</div>
</body>
</html>
