<%@ page import="com.kombos.parts.PartsAdjust" %>



<div class="control-group fieldcontain ${hasErrors(bean: partsAdjustInstance, field: 't145ID', 'error')} ">
	<label class="control-label" for="t145ID">
		<g:message code="partsAdjust.t145ID.label" default="T145 ID" />
		
	</label>
	<div class="controls">
	<g:textField readonly="true" name="t145ID" maxlength="20" value="${partsAdjustInstance?.t145ID}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partsAdjustInstance, field: 't145TglAdj', 'error')} ">
	<label class="control-label" for="t145TglAdj">
		<g:message code="partsAdjust.t145TglAdj.label" default="T145 Tgl Adj" />
		
	</label>
	<div class="controls">
        <ba:datePicker name="t145TglAdj" precision="day" value="${partsAdjustInstance?.t145TglAdj}" format="dd/MM/yyyy"/>
	</div>
</div>


