

<%@ page import="com.kombos.parts.PartsAdjust" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'partsAdjust.label', default: 'PartsAdjust')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deletePartsAdjust;

$(function(){ 
	deletePartsAdjust=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/partsAdjust/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadPartsAdjustTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-partsAdjust" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="partsAdjust"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${partsAdjustInstance?.t145ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t145ID-label" class="property-label"><g:message
					code="partsAdjust.t145ID.label" default="T145 ID" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t145ID-label">
						%{--<ba:editableValue
								bean="${partsAdjustInstance}" field="t145ID"
								url="${request.contextPath}/PartsAdjust/updatefield" type="text"
								title="Enter t145ID" onsuccess="reloadPartsAdjustTable();" />--}%
							
								<g:fieldValue bean="${partsAdjustInstance}" field="t145ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsAdjustInstance?.t145TglAdj}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t145TglAdj-label" class="property-label"><g:message
					code="partsAdjust.t145TglAdj.label" default="T145 Tgl Adj" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t145TglAdj-label">
						%{--<ba:editableValue
								bean="${partsAdjustInstance}" field="t145TglAdj"
								url="${request.contextPath}/PartsAdjust/updatefield" type="text"
								title="Enter t145TglAdj" onsuccess="reloadPartsAdjustTable();" />--}%
							
								<g:formatDate date="${partsAdjustInstance?.t145TglAdj}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsAdjustInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="partsAdjust.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${partsAdjustInstance}" field="createdBy"
								url="${request.contextPath}/PartsAdjust/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadPartsAdjustTable();" />--}%
							
								<g:fieldValue bean="${partsAdjustInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsAdjustInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="partsAdjust.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${partsAdjustInstance}" field="dateCreated"
								url="${request.contextPath}/PartsAdjust/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadPartsAdjustTable();" />--}%
							
								<g:formatDate date="${partsAdjustInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsAdjustInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="partsAdjust.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${partsAdjustInstance}" field="lastUpdProcess"
								url="${request.contextPath}/PartsAdjust/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadPartsAdjustTable();" />--}%
							
								<g:fieldValue bean="${partsAdjustInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsAdjustInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="partsAdjust.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${partsAdjustInstance}" field="lastUpdated"
								url="${request.contextPath}/PartsAdjust/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadPartsAdjustTable();" />--}%
							
								<g:formatDate date="${partsAdjustInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsAdjustInstance?.t145AlasanUnApprove}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t145AlasanUnApprove-label" class="property-label"><g:message
					code="partsAdjust.t145AlasanUnApprove.label" default="T145 Alasan Un Approve" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t145AlasanUnApprove-label">
						%{--<ba:editableValue
								bean="${partsAdjustInstance}" field="t145AlasanUnApprove"
								url="${request.contextPath}/PartsAdjust/updatefield" type="text"
								title="Enter t145AlasanUnApprove" onsuccess="reloadPartsAdjustTable();" />--}%
							
								<g:fieldValue bean="${partsAdjustInstance}" field="t145AlasanUnApprove"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsAdjustInstance?.t145PetugasAdj}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t145PetugasAdj-label" class="property-label"><g:message
					code="partsAdjust.t145PetugasAdj.label" default="T145 Petugas Adj" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t145PetugasAdj-label">
						%{--<ba:editableValue
								bean="${partsAdjustInstance}" field="t145PetugasAdj"
								url="${request.contextPath}/PartsAdjust/updatefield" type="text"
								title="Enter t145PetugasAdj" onsuccess="reloadPartsAdjustTable();" />--}%
							
								<g:fieldValue bean="${partsAdjustInstance}" field="t145PetugasAdj"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsAdjustInstance?.t145StaDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t145StaDel-label" class="property-label"><g:message
					code="partsAdjust.t145StaDel.label" default="T145 Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t145StaDel-label">
						%{--<ba:editableValue
								bean="${partsAdjustInstance}" field="t145StaDel"
								url="${request.contextPath}/PartsAdjust/updatefield" type="text"
								title="Enter t145StaDel" onsuccess="reloadPartsAdjustTable();" />--}%
							
								<g:fieldValue bean="${partsAdjustInstance}" field="t145StaDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsAdjustInstance?.t145StatusApprove}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t145StatusApprove-label" class="property-label"><g:message
					code="partsAdjust.t145StatusApprove.label" default="T145 Status Approve" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t145StatusApprove-label">
						%{--<ba:editableValue
								bean="${partsAdjustInstance}" field="t145StatusApprove"
								url="${request.contextPath}/PartsAdjust/updatefield" type="text"
								title="Enter t145StatusApprove" onsuccess="reloadPartsAdjustTable();" />--}%
							
								<g:fieldValue bean="${partsAdjustInstance}" field="t145StatusApprove"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsAdjustInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="partsAdjust.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${partsAdjustInstance}" field="updatedBy"
								url="${request.contextPath}/PartsAdjust/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadPartsAdjustTable();" />--}%
							
								<g:fieldValue bean="${partsAdjustInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${partsAdjustInstance?.id}"
					update="[success:'partsAdjust-form',failure:'partsAdjust-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deletePartsAdjust('${partsAdjustInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
