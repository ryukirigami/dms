<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript>
var partsAdjustSubTable;
$(function(){

    if(partsAdjustSubTable)
    	partsAdjustSubTable.dataTable().fnDestroy();
        partsAdjustSubTable = $('#partsAdjust_datatables_sub_${idTable}').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubList")}",
		"aoColumns": [

		{
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"15px",
               "sDefaultContent": ''
},

{
	"sName": "pilih",
	"mDataProp": "pilih",
	"mRender": function ( data, type, row ) {
	    %{--if(data == true){--}%
	        %{--return '<input id="checkBoxJob" checked type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;';--}%
	    %{--}else {--}%
	        return '<input id="checkBoxJob" type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+';'+row['kodePA']+'">&nbsp;&nbsp;';
//	    }
	},
	"bSortable": false,
	"sWidth":"15px",
	"bVisible": true
},
{
	"sName": "kode",
	"mDataProp": "kode",
	"aTargets": [0],
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "nama",
	"mDataProp": "nama",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"250px",
	"bVisible": true
},
{
	"sName": "awal",
	"mDataProp": "awal",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "real",
	"mDataProp": "real",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "alasan",
	"mDataProp": "alasan",
	"aTargets": [4],
	"bSortable": false,
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						aoData.push(
									{"name": 'id', "value": "${params.id}"},
									{"name": 'idTanggal', "value": "${idTanggal}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
    </g:javascript>
</head>
<body>
<div class="innerDetails" style="width: 100%;">
    <table id="partsAdjust_datatables_sub_${idTable}" cellpadding="0" cellspacing="0" border="0" style="width: 100%;"
           class="display table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th></th>
            <th></th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Kode Part</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Nama Part</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Qty Tercatat</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Qty Real</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Alasan Adjusmment</div>
            </th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
</body>
</html>
