<%@ page import="com.kombos.parts.PartsAdjust" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'partsAdjust.label', default: 'PartsAdjust')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <r:require modules="baseapplayout"/>
    <g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :
                window.location.replace('#/inputOnHandAdjustment');
                break;
            case '_DELETE_' :
                bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	window.location.replace('#/inputOnHandAdjustment?id='+id);

    	$('#spinner').fadeIn(1);
        $.ajax({url: '${request.contextPath}/inputOnHandAdjustment?id='+id,type: "GET",dataType: "html",
        complete : function (req, err) {
            $('#main-content').html(req.responseText);
             $('#spinner').fadeOut();
             loading = false;
        }
        });
    };
    
    edit = function(id) {
    	window.location.replace('#/inputOnHandAdjustment?id='+id);

    	$('#spinner').fadeIn(1);
        $.ajax({url: '${request.contextPath}/inputOnHandAdjustment?id='+id,type: "GET",dataType: "html",
        complete : function (req, err) {
            $('#main-content').html(req.responseText);
             $('#spinner').fadeOut();
             loading = false;
        }
        });
    };
    
    loadForm = function(data, textStatus){
		$('#partsAdjust-form').empty();
    	$('#partsAdjust-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#partsAdjust-table").hasClass("span12")){
   			$("#partsAdjust-table").toggleClass("span12 span5");
        }
        $("#partsAdjust-form").css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
   		if($("#partsAdjust-table").hasClass("span5")){
   			$("#partsAdjust-table").toggleClass("span5 span12");
   		}
        $("#partsAdjust-form").css("display","none");
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#partsAdjust-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/partsAdjust/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadPartsAdjustTable();
    		}
		});
		
   	}

    resetCari = function(){
        $('#search_t145TglAdj').val('');
        $('#search_t145TglAdjakhir').val('');
        $('input[name=t145StatusApprove]').attr('checked',false);
        $('#nilaiRadio').val('');
        reloadPartsAdjustTable();
    }

    cekRadio = function(){
        if(document.getElementById('t145StatusApproveSudah').checked) {
          $('#nilaiRadio').val('1');
          console.log('')
        }else if(document.getElementById('t145StatusApproveBelum').checked) {
          $('#nilaiRadio').val('0');
        }
    }

});
    </g:javascript>

    <style>
    div.innerDetails {
        display: block;
    }
    </style>

</head>

<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]"/></span>
    <ul class="nav pull-right">
        <li><a class="pull-right box-action" href="#"
               style="display: hidden;" target="_CREATE_">&nbsp;&nbsp;<i
                    class="icon-plus"></i>&nbsp;&nbsp;
        </a></li>
        <li><a class="pull-right box-action" href="#"
               style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
                    class="icon-remove"></i>&nbsp;&nbsp;
        </a></li>
        <li class="separator"></li>
    </ul>
</div>

<div class="box">
    <div class="span12" id="partsAdjust-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>

        <table>
            <tr>
                <td colspan="2">
                    <legend style="font-size: small">
                        Search Kriteria
                    </legend>
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    <g:message code="partsAdjust.t145TglAdj.label" default="Tanggal Adjusment"/>
                </td>
                <td style="padding: 5px">
                    <ba:datePicker style="width:70px" name="search_t145TglAdj" id="search_t145TglAdj" precision="day" value="" format="dd-MM-yyyy"/>&nbsp;s.d.&nbsp;
                    <ba:datePicker style="width:70px" name="search_t145TglAdjakhir" id="search_t145TglAdjakhir" precision="day" value="" format="dd-MM-yyyy"/>
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    <g:message code="partsAdjust.t145StatusApprove.label" default="Status Adjusment"/>
                </td>
                <td style="padding: 5px">
                    <input type="hidden" name="nilaiRadio" id="nilaiRadio" />
                    <input type="radio" name="t145StatusApprove" id="t145StatusApproveSudah" onclick="cekRadio();" />&nbsp;Sudah Approve
                    <input type="radio" name="t145StatusApprove" id="t145StatusApproveBelum" onclick="cekRadio();" />&nbsp;Belum Approve
                </td>
            </tr>
            <tr>
                <td colspan="2" class="pull-right">
                    %{--<a class="pull-right">--}%
                        %{--&nbsp;&nbsp;--}%
                        %{--<i>--}%
                            %{--<g:field type="button" style="padding: 5px" class="btn cancel"--}%
                                     %{--name="clear" id="clear" value="${message(code: 'default.clearsearch.label', default: 'Clear Search')}" />--}%
                        %{--</i>--}%
                        %{--&nbsp;&nbsp;--}%
                    %{--</a>--}%
                    %{--<a class="pull-right" >--}%
                        %{--&nbsp;&nbsp;--}%
                        %{--<i>--}%
                        <g:field type="button" class="btn btn-primary create" onclick="reloadPartsAdjustTable();"
                                 name="view" id="view" value="${message(code: 'default.search.label', default: 'Search')}" />
                        &nbsp;&nbsp;
                        <g:field type="button" class="btn cancel" onclick="resetCari();"
                                     name="clear" id="clear" value="${message(code: 'default.clearsearch.label', default: 'Clear Search')}" />
                        %{--</i>--}%
                        %{--&nbsp;&nbsp;--}%
                    %{--</a>--}%
                </td>
            </tr>
        </table>
        <br/><br/>
        <g:render template="dataTables"/>
    </div>

    <div class="span7" id="partsAdjust-form" style="display: none;"></div>
</div>
</body>
</html>
