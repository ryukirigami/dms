

<%@ page import="com.kombos.parts.CustomerSales" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'customerSales.label', default: 'CustomerSales')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteCustomerSales;

$(function(){ 
	deleteCustomerSales=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/customerSales/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadCustomerSalesTable();
   				expandTableLayout('customerSales');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-customerSales" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="customerSales"
			class="table table-bordered table-hover">
			<tbody>


            <g:if test="${customerSalesInstance?.customerCode}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="customerCode-label" class="property-label"><g:message
                                code="customerSales.customerCode.label" default="Kode Customer" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="customerCode-label">
                        %{--<ba:editableValue
                                bean="${customerSalesInstance}" field="customerCode"
                                url="${request.contextPath}/CustomerSales/updatefield" type="text"
                                title="Enter customerCode" onsuccess="reloadCustomerSalesTable();" />--}%

                        <g:fieldValue bean="${customerSalesInstance}" field="customerCode"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${customerSalesInstance?.nama}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="nama-label" class="property-label"><g:message
                                code="customerSales.nama.label" default="Nama Customer" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="nama-label">
                        %{--<ba:editableValue
                                bean="${customerSalesInstance}" field="nama"
                                url="${request.contextPath}/CustomerSales/updatefield" type="text"
                                title="Enter nama" onsuccess="reloadCustomerSalesTable();" />--}%

                        <g:fieldValue bean="${customerSalesInstance}" field="nama"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${customerSalesInstance?.phoneNumber}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="phoneNumber-label" class="property-label"><g:message
                                code="customerSales.phoneNumber.label" default="No. Telpon/HP" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="phoneNumber-label">
                        %{--<ba:editableValue
                                bean="${customerSalesInstance}" field="phoneNumber"
                                url="${request.contextPath}/CustomerSales/updatefield" type="text"
                                title="Enter phoneNumber" onsuccess="reloadCustomerSalesTable();" />--}%

                        <g:fieldValue bean="${customerSalesInstance}" field="phoneNumber"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${customerSalesInstance?.alamat}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="alamat-label" class="property-label"><g:message
					code="customerSales.alamat.label" default="Alamat" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="alamat-label">
						%{--<ba:editableValue
								bean="${customerSalesInstance}" field="alamat"
								url="${request.contextPath}/CustomerSales/updatefield" type="text"
								title="Enter alamat" onsuccess="reloadCustomerSalesTable();" />--}%
							
								<g:fieldValue bean="${customerSalesInstance}" field="alamat"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${customerSalesInstance?.kodePos}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="kodePos-label" class="property-label"><g:message
                                code="customerSales.kodePos.label" default="Kode Pos" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="kodePos-label">
                        %{--<ba:editableValue
                                bean="${customerSalesInstance}" field="kodePos"
                                url="${request.contextPath}/CustomerSales/updatefield" type="text"
                                title="Enter kodePos" onsuccess="reloadCustomerSalesTable();" />--}%

                        <g:fieldValue bean="${customerSalesInstance}" field="kodePos"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${customerSalesInstance?.npwp}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="npwp-label" class="property-label"><g:message
                                code="customerSales.npwp.label" default="NPWP" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="npwp-label">
                        %{--<ba:editableValue
                                bean="${customerSalesInstance}" field="npwp"
                                url="${request.contextPath}/CustomerSales/updatefield" type="text"
                                title="Enter npwp" onsuccess="reloadCustomerSalesTable();" />--}%

                        <g:fieldValue bean="${customerSalesInstance}" field="npwp"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${customerSalesInstance?.alamatNpwp}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="alamatNpwp-label" class="property-label"><g:message
					code="customerSales.alamatNpwp.label" default="Alamat NPWP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="alamatNpwp-label">
						%{--<ba:editableValue
								bean="${customerSalesInstance}" field="alamatNpwp"
								url="${request.contextPath}/CustomerSales/updatefield" type="text"
								title="Enter alamatNpwp" onsuccess="reloadCustomerSalesTable();" />--}%
							
								<g:fieldValue bean="${customerSalesInstance}" field="alamatNpwp"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${customerSalesInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="customerSales.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">

                        <g:fieldValue bean="${customerSalesInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${customerSalesInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="customerSales.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">

                        <g:formatDate date="${customerSalesInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${customerSalesInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="customerSales.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">

                        <g:fieldValue bean="${customerSalesInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${customerSalesInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="customerSales.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">

                        <g:formatDate date="${customerSalesInstance?.lastUpdated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${customerSalesInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="customerSales.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">

                        <g:fieldValue bean="${customerSalesInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('customerSales');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${customerSalesInstance?.id}"
					update="[success:'customerSales-form',failure:'customerSales-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteCustomerSales('${customerSalesInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
