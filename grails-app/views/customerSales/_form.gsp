<%@ page import="com.kombos.parts.CustomerSales" %>



<div class="control-group fieldcontain ${hasErrors(bean: customerSalesInstance, field: 'customerCode', 'error')} ">
    <label class="control-label" for="customerCode">
        <g:message code="customerSales.customerCode.label" default="Kode Customer" />

    </label>
    <div class="controls">
        <g:textField name="customerCode" value="${customerSalesInstance?.customerCode}" maxlength="16" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: customerSalesInstance, field: 'nama', 'error')} ">
    <label class="control-label" for="nama">
        <g:message code="customerSales.nama.label" default="Nama Customer" />

    </label>
    <div class="controls">
        <g:textField name="nama" value="${customerSalesInstance?.nama}" maxlength="64"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: customerSalesInstance, field: 'phoneNumber', 'error')} ">
    <label class="control-label" for="phoneNumber">
        <g:message code="customerSales.phoneNumber.label" default="No. Telpon/HP" />

    </label>
    <div class="controls">
        <g:textField name="phoneNumber" value="${customerSalesInstance?.phoneNumber}" maxlength="32" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: customerSalesInstance, field: 'alamat', 'error')} ">
	<label class="control-label" for="alamat">
		<g:message code="customerSales.alamat.label" default="Alamat" />
		
	</label>
	<div class="controls">
	<g:textArea name="alamat" value="${customerSalesInstance?.alamat}" style="resize:none" maxlength="128"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: customerSalesInstance, field: 'kodePos', 'error')} ">
    <label class="control-label" for="kodePos">
        <g:message code="customerSales.kodePos.label" default="Kode Pos" />

    </label>
    <div class="controls">
        <g:textField name="kodePos" value="${customerSalesInstance?.kodePos}" maxlength="6" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: customerSalesInstance, field: 'npwp', 'error')} ">
    <label class="control-label" for="npwp">
        <g:message code="customerSales.npwp.label" default="NPWP" />

    </label>
    <div class="controls">
        <g:textField name="npwp" value="${customerSalesInstance?.npwp}" maxlength="32" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: customerSalesInstance, field: 'alamatNpwp', 'error')} ">
	<label class="control-label" for="alamatNpwp">
		<g:message code="customerSales.alamatNpwp.label" default="Alamat NPWP" />
		
	</label>
	<div class="controls">
	<g:textArea name="alamatNpwp" value="${customerSalesInstance?.alamatNpwp}" style="resize:none" maxlength="128" />
	</div>
</div>