
<%@ page import="com.kombos.parts.CustomerSales" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="customerSales_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="customerSales.customerCode.label" default="Kode Customer" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="customerSales.nama.label" default="Nama Customer" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="customerSales.phoneNumber.label" default="No. Telpon/HP" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerSales.alamat.label" default="Alamat" /></div>
			</th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="customerSales.kodePos.label" default="Kode Pos" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="customerSales.npwp.label" default="NPWP" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerSales.alamatNpwp.label" default="Alamat NPWP" /></div>
			</th>


        </tr>
		<tr>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_customerCode" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_customerCode" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_nama" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_nama" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_phoneNumber" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_phoneNumber" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
				<div id="filter_alamat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_alamat" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_kodePos" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_kodePos" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_npwp" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_npwp" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
				<div id="filter_alamatNpwp" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_alamatNpwp" class="search_init" />
				</div>
			</th>

		</tr>
	</thead>
</table>

<g:javascript>
var customerSalesTable;
var reloadCustomerSalesTable;
$(function(){
	
	reloadCustomerSalesTable = function() {
		customerSalesTable.fnDraw();
	}

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	customerSalesTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	customerSalesTable = $('#customerSales_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "customerCode",
	"mDataProp": "customerCode",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "nama",
	"mDataProp": "nama",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "phoneNumber",
	"mDataProp": "phoneNumber",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "alamat",
	"mDataProp": "alamat",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "kodePos",
	"mDataProp": "kodePos",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "npwp",
	"mDataProp": "npwp",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "alamatNpwp",
	"mDataProp": "alamatNpwp",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var alamat = $('#filter_alamat input').val();
						if(alamat){
							aoData.push(
									{"name": 'sCriteria_alamat', "value": alamat}
							);
						}
	
						var alamatNpwp = $('#filter_alamatNpwp input').val();
						if(alamatNpwp){
							aoData.push(
									{"name": 'sCriteria_alamatNpwp', "value": alamatNpwp}
							);
						}
	
						var customerCode = $('#filter_customerCode input').val();
						if(customerCode){
							aoData.push(
									{"name": 'sCriteria_customerCode', "value": customerCode}
							);
						}
	
						var kodePos = $('#filter_kodePos input').val();
						if(kodePos){
							aoData.push(
									{"name": 'sCriteria_kodePos', "value": kodePos}
							);
						}
	
						var nama = $('#filter_nama input').val();
						if(nama){
							aoData.push(
									{"name": 'sCriteria_nama', "value": nama}
							);
						}
	
						var npwp = $('#filter_npwp input').val();
						if(npwp){
							aoData.push(
									{"name": 'sCriteria_npwp', "value": npwp}
							);
						}
	
						var phoneNumber = $('#filter_phoneNumber input').val();
						if(phoneNumber){
							aoData.push(
									{"name": 'sCriteria_phoneNumber', "value": phoneNumber}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
