<g:javascript>
    var partsTemp = []
    var idJobPart ;
	function searchParts(){
		var kriteriaPencarianParts = $('#kriteriaPencarianParts').find(":selected").val();
		var idText = '#search_'+kriteriaPencarianParts;
		var kataKunciPencarianParts = $('#kataKunciPencarianParts').val();
		if(kataKunciPencarianParts){
			$(idText).val(kataKunciPencarianParts);
			reloadGoodsTable();
			closeDialog('dialog-reception-inputparts');
			openDialog('dialog-reception-searchparts');
		}else{
			alert('Silahkan Isi Kata Kunci Pencarian');		
		}
	}

	function clearPart(){
	    $('.idParts').each(function() {
            $(this).closest("tr").remove();
        });
	}

	function setIdJobPart(id){
	    idJobPart = id;
	}
	function addPartReception(idPart,kodePart,namaPart,satuan,harga){
		var paramAll = idPart+kodePart+namaPart+satuan;
		if(partsTemp.indexOf(paramAll)<0){
            partsTemp.push(paramAll);
            var newRow = "<tr>" +
                             "<td>" +
                                "<input class='idParts' type='hidden' id='"+idPart+"idPart' value='"+idPart+"'/>"+kodePart+"" +
                             "</td>" +
                             "<td>"+namaPart+"</td>" +
                             "<td>" +
                                "<select id='prosesBp"+idPart+"'><option value=''>[dibutuhkan pada proses]</option><option value='1'>Body</option><option value='2'>Preparation</option><option value='3'>Painting</option><option value='4'>Assembly</option><option value='5'>Polishing</option>" +
                                "</select>" +
                             "</td>" +
                             "<td>" +
                                "<input type='text' class='numberonly inputQty'  id='qty"+idPart+"'/>" +
                             "</td>" +
                             "<td>"+satuan+"</td>" +
                             "<td>" +
                                "<span id='harga"+idPart+"'>"+harga+"</span>" +
                             "</td>" +
                          "</tr>";
            $('#tableInputParts > tbody:last').append(newRow);
		}
	}

	function saveParts(){
        var aodataPart =  new Array();
		var idx1 = 1;
		var nameIdPart = '';
		var nameProses = '';
		var nameQty = '';
		$('.idParts').each(
		    function() {
		        var id = this.value;
				nameIdPart = 'good' + idx1;
				nameProses = 'prosesBP' + idx1 ;
                nameQty = 'qty' + idx1;
                aodataPart.push(
					{"name": nameIdPart, "value": this.value},
					{"name": nameProses, "value": $('#prosesBP' + id).val()},
					{"name": nameQty, "value": $('#qty' + id).val()}
				);
				idx1++;
			}
		);
        var cekNull = false;
        $('.inputQty').each(
            function(){
                var cekIsi = this.value.replace(" ","");
                if( cekIsi=="" || cekIsi=="0"){
                    cekNull=true;
                }
            }
        );

		if(idx1==1){
		    alert('Anda belum menambahkan Part');
		    return;
		}

		aodataPart.push(
			{"name": 'countRowPart', "value": idx1},
			{"name": 'idReception', "value": $('#receptionId').val()},
			{"name": 'idJob', "value": idJobPart}
		);
        if(cekNull){
            alert('Masukan jumlah part yang dibutuhkan dan tidak boleh 0');
            return;
        }else{
		$('#spinner').fadeIn(1);
		$.ajax({
			url:'${request.contextPath}/reception/savePartInput',
			type: "POST",
			async : false,
			data: aodataPart,
			success : function(data){
				$('#spinner').fadeOut();
        		reloadjobnPartsTable();
//				reloadJoborderTable();
		        openDialog('dialog-reception-joborder');
                closeDialog('dialog-reception-inputparts');
            },
			error: function(xhr, textStatus, errorThrown) {
				alert('Internal server error');
			}
		});
        }
	}

	function doRequestPart(){
		/*
		var aodataJob =  new Array();

		$.ajax({url: '${request.contextPath}/reception/savePart',
			type: "POST",
			data: aodataJob,
			success: function(data) {
				reloadjobnPartsTable();
			},
			complete : function (req, err) {
				$('#spinner').fadeOut();
				loading = false;
			}
		});
		openDialog('dialog-reception-requestparts');
		*/
		closeDialog('dialog-reception-inputparts');
		openRequestPart();
	}
</g:javascript>
<div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Reception - Input Parts</h3>
</div>
<div class="modal-body">
	<div class="box">
		<table class="display table" width="100%" cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px;">
			<tr>
                <td>
                    Kriteria Pencarian
                    <select id="kriteriaPencarianParts" name="kriteriaPencarianParts">
						<option value="m111ID">Kode Part</option>
						<option value="m111Nama">Nama Part</option>
					</select>
                </td>
            </tr>
            <tr>
                <td>
                    Kata Kunci Pencarian
                    <input type="text" id="kataKunciPencarianParts" name="kataKunciPencarianParts"/>
                </td>
            </tr>
            <tr>
                <td>
                    <input class="btn" type="button" value="Search" onclick="searchParts();"/>
				</td>
			</tr>
		</table>
        <div class="dataTables_scroll">
            <table id="tableInputParts" class="display table table-striped table-bordered table-hover dataTable" width="100%" cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px;">
                <tr>
                    <th>
                        Kode Parts
                    </th>
                    <th>
                        Nama Parts
                    </th>
                    <th>
                        Dibutuhkan pada proses
                    </th>
                    <th>
                        Qty
                    </th>
					<th>
                        Satuan
                    </th>
					<th>
                        Total Harga
                    </th>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="modal-footer">
    <input class="btn" type="button" value="Add Parts" onclick="saveParts();"/>
    <a onclick="doRequestPart();" class="btn btn-success">Request Parts</a>
</div>