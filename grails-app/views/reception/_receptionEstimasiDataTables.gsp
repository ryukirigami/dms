<%@ page import="com.kombos.administrasi.BumperPaintingTime" %>
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="estimasi_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
			<th style="border-bottom: none;padding: 5px;">
				<div>Work Items</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Area (x100cm<sup>2</sup>)</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Repair Difficulty</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Plastic Bumper Painting Time</div>
			</th>
            <th style="border-bottom: none;padding: 5px;">
                <div>Harga Parts</div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div>Harga Jasa</div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div>Harga Total</div>
            </th>
		</tr>
	</thead>
    <tfoot>
    <tr>
        <td colspan="6"><span>TOTAL </span></td>
        <td><span class="pull-right numeric">750000</span></td>
    </tr>
    </tfoot>
</table>

<g:javascript>
var estimasiTable;
var reloadestimasiTable;
$(function(){
	
	reloadestimasiTable = function() {
		estimasiTable.fnDraw();
	}

	estimasiTable = $('#estimasi_datatables').dataTable({
		"sScrollY": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
        "bPaginate" : false,
        "bInfo" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		 bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			var aData = estimasiTable.fnGetData(nRow);
                $.ajax({type:'POST',
                    data : aData,
                    url:'${request.contextPath}/reception/workList',
                    success:function(data,textStatus){
                        var nDetailsRow = estimasiTable.fnOpen(nRow,data,'details');
                        //var innerDetails = $('div.innerDetails', nDetailsRow);
                        //$('div.innerDetails', nDetailsRow).slideDown();
                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){},
                    complete:function(XMLHttpRequest,textStatus){
                        $('#spinner').fadeOut();
                    }
                });
			return nRow;
		},
		"bSort": false,
		"bProcessing": false,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "estimasiDatatablesList")}",
		"aoColumns": [
{
	"sName": "workItem",
	"mDataProp": "workItem",
	"aTargets": [0],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}
,{
	"sName": "area",
	"mDataProp": "area",
	"aTargets": [1],
	"mRender": function ( data, type, row ) {
			var ps = [];
    <g:each in="${com.kombos.administrasi.PanelRepair.list()}">
        ps.push({id: ${it.id}, label: "${it.m043Area1}"});
    </g:each>

    var pCombo = '<select id="panelRepair'+row['id']+'" name="panelRepair.id" class="many-to-one inline-edit" style="width:105px;" >';
pCombo += '<option value="null">[----]</option>';
for(var i = 0; i < ps.length; i++){
pCombo += '<option value="'+ps[i].id+'">'+ps[i].label+'</option>';
}
pCombo += '</select>';
                    return pCombo;

	},
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
},{
	"sName": "repairDifficulty",
	"mDataProp": "repairDifficulty",
	"aTargets": [2],
	"bSearchable": false,
	"mRender": function ( data, type, row ) {
			var ps = [];
            <g:each in="${com.kombos.administrasi.RepairDifficulty.list()}">
                ps.push({id: ${it.id}, label: "${it.m042Tipe}"});
            </g:each>

            var pCombo = '<select id="repairDifficulty'+row['id']+'" name="repairDifficulty.id" class="many-to-one inline-edit" style="width:115px;" >';
        pCombo += '<option value="null">[--Repair Difficulty--]</option>';
        for(var i = 0; i < ps.length; i++){
        pCombo += '<option value="'+ps[i].id+'">'+ps[i].label+'</option>';
        }
        pCombo += '</select>';
                    return pCombo;

	},
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
},{
	"sName": "plasticBumper",
	"mDataProp": "plasticBumper",
	"aTargets": [3],
	"bSearchable": false,
	"mRender": function ( data, type, row ) {
			var ps = [];
    <g:each in="${BumperPaintingTime.list()}">
        ps.push({id: ${it.id}, label: "${it.m0452New}"});
    </g:each>

    var pCombo = '<select id="bumperPaintingTime'+row['id']+'" name="bumperPaintingTime.id" class="many-to-one inline-edit" style="width:115px;" >';
pCombo += '<option value="null">[--Bumper PaintingTime--]</option>';
for(var i = 0; i < ps.length; i++){
pCombo += '<option value="'+ps[i].id+'">'+ps[i].label+'</option>';
}
pCombo += '</select>';
                    return pCombo;

	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},{
	"sName": "hargaParts",
	"mDataProp": "hargaParts",
	"aTargets": [4],
	"mRender": function ( data, type, row ) {
	    return '<span class="pull-right numeric">'+data+'</span>'
	},
	"bSearchable": false,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},{
	"sName": "hargaJasa",
	"mDataProp": "hargaJasa",
	"aTargets": [5],
	"mRender": function ( data, type, row ) {
	    return '<span class="pull-right numeric">'+data+'</span>'
	},
	"bSearchable": false,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},{
	"sName": "hargaTotal",
	"mDataProp": "hargaTotal",
	"aTargets": [6],
	"mRender": function ( data, type, row ) {
	    return '<span class="pull-right numeric">'+data+'</span>'
	},
	"bSearchable": false,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
            aoData.push(
                {"name": 'nowo', "value": nowo}
            )
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
