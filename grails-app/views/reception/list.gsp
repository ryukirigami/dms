
<%@ page import="com.kombos.reception.Reception" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'reception.label', default: 'View Customer Reception')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var show;
	var showDetail;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});
         		break;
       }    
       return false;
	});
	printViewReception = function(){
		var tglReception = "-";
		var tglReceptionTo = "-";
		var cat = "-";
		var keyword = "-";
		if ($("#tanggalReceptionCb").is(":checked")) {
			tglReception = $('#tanggalReception').val();
			tglReceptionTo = $('#tanggalReceptionTo').val();
		}

		if(document.getElementById('kategoriPencarianCb').checked == true){
			cat = $("#kategoriPencarian").val();
			keyword = $("#kataKunci").val();
		}
		var url = "sCriteria_tanggalReception="+tglReception+"&sCriteria_tanggalReceptionTo="+tglReceptionTo+"&cat="+cat+"&keyword="+keyword;
        window.location = "${request.contextPath}/reception/printViewReception?"+url;
    }

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/reception/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/reception/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#reception-form').empty();
    	$('#reception-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#reception-table").hasClass("span12")){
   			$("#reception-table").toggleClass("span12 span5");
        }
        $("#reception-form").css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
   		if($("#reception-table").hasClass("span5")){
   			$("#reception-table").toggleClass("span5 span12");
   		}
        $("#reception-form").css("display","none");
   	}

   	showDetail = function(id){
                    $("#detailContent").empty();
                    $.ajax({
                        type:'POST',
                        url:'${request.contextPath}/reception/viewDetails',
                        data : {id : id},
                        success:function(data,textStatus){
                                $("#detailContent").html(data);
                                $("#detailModal").modal({
                                    "backdrop" : "dynamic",
                                    "keyboard" : true,
                                    "show" : true
                                }).css({'width': '1100px','margin-left': function () {return -($(this).width() / 2);}});

                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
                }
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#reception-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/reception/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadReceptionTable();
    		}
		});
		
   	}

});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<!--
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
		-->
	</div>
	<div class="box">
		<div class="span12" id="reception-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
			<fieldset>
			<legend>Search</legend>
			<table class="table table-bordered" style="width:50%;">
				<tr>
					<td>
						Tanggal Reception		
					</td>
					<td>
						<input type="checkbox" name="tanggalReceptionCb" id="tanggalReceptionCb"/>
						<ba:datePicker format="dd/mm/yyyy"  name="tanggalReception" precision="day"/>
						&nbsp;-&nbsp;
						<ba:datePicker format="dd/mm/yyyy"  name="tanggalReceptionTo" precision="day"/>
					</td>
				</tr>
				<tr>
					<td>
						Kategori Pencarian		
					</td>
					<td>
						<input type="checkbox" name="kategoriPencarianCb" id="kategoriPencarianCb"/>
						<g:select id="kategoriPencarian" name="kategoriPencarian" from="${['Nama Customer', 'Status Invoice','Alamat','No Hp','Kendaraan','Nomor Polisi','Nomor WO','Nama SA']}" keys="${["sCriteria_namaCustomer", "sCriteria_statusInvoice", "sCriteria_alamat","sCriteria_noHp","sCriteria_kendaraan","sCriteria_noPol","sCriteria_noWO","sCriteria_namaSA"]}"/>
					</td>
				</tr>
				<tr>
					<td>
						Kata Kunci		
					</td>
					<td>
						<input type="text" name="kataKunci" id="kataKunci"/>
					</td>
				</tr>
				<tr>
					<td>
						&nbsp;		
					</td>
					<td>
						<input type="button" class="btn cancel" onclick="reloadReceptionTable()" value="Search"/>
					</td>
				</tr>
			</table>
			</fieldset>
			</br>
			<g:render template="dataTables" />
			</br>
			<table style="width:100%;">
				<tr>
					<td style="align:right;">
						<input type="button" class="btn cancel" onclick="javascript:loadPath('reception/');" value="New Reception"/>
						<button type="button" onclick="printViewReception();"  class="btn btn-primary">Export Xls</button>
					</td>
				</tr>
			</table>
		</div>
		<div class="span7" id="reception-form" style="display: none;"></div>
	</div>

	<div id="detailModal" class="modal fade">
		<div class="modal-dialog" style="width: 1090px;">
			<div class="modal-content" style="width: 1090px;">
				<div class="modal-body" style="max-height: 450px;">
					<div id="detailContent"/>
					<div class="iu-content"></div>

				</div>
			</div>
		</div>
	</div>
</body>
</html>
