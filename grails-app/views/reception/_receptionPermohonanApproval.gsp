<%@ page import="com.kombos.administrasi.KegiatanApproval; com.kombos.reception.Reception" %>
<div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Approval - Permohonan Approval</h3>
</div>
<g:javascript>
    $(function(){
        send = function(){
            var noWo = $('#noWoApproval').val();
            var pesan = $('#pesanApproval').val();
            var alasanCancel = $('#alasanCancel').val();
            var kegiatanApproval = $('#kegiatanApproval').val();
            $.ajax({
                    url:'${request.contextPath}/reception/approval',
                    type: "POST", // Always use POST when deleting data
                    data : {noWo : noWo,pesan:pesan,alasanCancel:alasanCancel,kegiatanApproval:kegiatanApproval},
                    success : function(data){
                        window.location.href = '#/home' ;
                        toastr.success('<div>Approval Sudah Dikirim</div>');
                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });
        };

    });
</g:javascript>
<div class="modal-body">
    <div class="box">
        <table class="table">
            <tbody>
            <tr>
				<td>
					Nama Kegiatan
                </td>
                <td>
					<g:select id="kegiatanApproval" name="kegiatanApproval" from="${com.kombos.administrasi.KegiatanApproval.list()}"  value="${KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.CANCEL_RECEPTION).id}" optionKey="id" required="" class="many-to-one"/>
                </td>
            </tr>
            <tr>
				<td>
					No WO
                </td>
                <td>
					<g:textField name="noWo" id="noWoApproval" />
                </td>
            </tr>
			<tr>
				<td>
					Tanggal dan Jam
                </td>
                <td>
					${new Date().format("dd-MM-yyyy HH:mm")}
                </td>
            </tr>
            <tr>
                <td>
                    Alasan Cancel
                </td>
                <td>
                    <g:select id="alasanCancel" name="alasanCancel" from="${com.kombos.maintable.AlasanCancel.list()}" optionKey="id" required="" class="many-to-one"/>
                </td>
            </tr>
			<tr>
				<td>
					Pesan
                </td>
                <td>
					<textarea id="pesanApproval" name="pesanApproval" cols="200" rows="3"></textarea>
                </td>
            </tr>
			</tbody>
        </table>
    </div>
</div>
<div class="modal-footer">
    <a href="#" class="btn" onclick="send()" data-dismiss="modal">Send</a>
	<a href="#" class="btn" data-dismiss="modal">Close</a>
</div>