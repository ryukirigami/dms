<table id="addPart_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover">
    <thead>
    <tr>
       <th style="border-bottom: none;padding: 5px;">
            <div>Kode Parts</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Nama Parts</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Total Harga</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Stok</div>
        </th>

    </tr>
    <tr>
        <th style="border-top: none;padding: 5px;">
            <div id="filter_goods" style="padding-top: 0px;position:relative; margin-top: 0px;width: 105px;">
                <input type="text" name="search_goods" class="search_init" />
            </div>
        </th>
        <th style="border-top: none;padding: 5px;">
            <div id="filter_goods2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_goods2" class="search_init" />
            </div>
        </th>
        <th style="border-top: none;padding: 5px;"/>
        <th style="border-top: none;padding: 5px;"/>
    </tr>
    </thead>
</table>

<g:javascript>
var addPartTable;
var reloadAddPartTable;
$(function(){

	reloadAddPartTable = function() {
		addPartTable.fnDraw();
	}

	$("#addPart_datatables th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	addPartTable.fnDraw();
		}
	});
	$("#addPart_datatables th div input").click(function (e) {
	 	e.stopPropagation();
	});

	addPartTable = $('#addPart_datatables').dataTable({
//		"sScrollX": "100%",
//		"sScrollY": "400px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid't><'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": false,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : 10,// maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "addPartDatatablesList")}",
		"aoColumns": [
{
	"sName": "goods",
	"mDataProp": "goods",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "goods",
	"mDataProp": "goods2",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "totalHarga",
	"mDataProp": "totalHarga",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "stokR",
	"mDataProp": "stokR",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {



						var goods = $('#filter_goods input').val();
						if(goods){
							aoData.push(
									{"name": 'sCriteria_goods', "value": goods}
							);
						}
						var goods2 = $('#filter_goods2 input').val();
						if(goods2){
							aoData.push(
									{"name": 'sCriteria_goods2', "value": goods2}
							);
						}




						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>