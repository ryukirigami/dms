<%--
  Created by IntelliJ IDEA.
  User: KRISTIAN HADI
  Date: 2/22/2017
  Time: 11:27 AM
--%>

<%@ page import="com.kombos.reception.Reception" %>
<%@ page import="com.kombos.parts.Konversi; com.kombos.parts.Franc; com.kombos.parts.KlasifikasiGoods; com.kombos.parts.DeliveryOrder" %>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title>View Details</title>
    <r:require modules="baseapplayout, baseapplist" />
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><b><g:message code="viewDetails.label" default="View Details" /></b></span>
</div>

<div id="detail-form" style="display: block; margin-top: 20px; margin-left: 30px">

    <legend>Detail Job</legend>
    <table id="detail-table" class="table table-bordered">
        <thead>
        <th>No</th>
        <th>Kode Job</th>
        <th>Nama Job</th>
        <th>Harga sebelum discount</th>
        <th>Discount</th>
        <th>Harga sesudah discount</th>
        </thead>
        <tbody>
        <g:if test="${job}">
            <% def konv  =  new Konversi();
                def totalJob = 0;
            %>
            <g:each in="${job}" status="i" var="j" >
                <% totalJob += j?.t402TotalRp;
                %>
                <tr>
                    <td>${i+1}</td>
                    <td>${j?.operation.m053Id}</td>
                    <td>${j?.operation.m053NamaOperation}</td>
                    <td style="text-align: right">${konv.toRupiah(j?.t402HargaRp)}</td>
                    <td style="text-align: right">${konv.toRupiah(j?.t402DiscRp)}</td>
                    <td style="text-align: right">${konv.toRupiah(j?.t402TotalRp)}</td>
                </tr>
            </g:each>
            <tr style="background-color: #F1F0F0">
                <th colspan="5"> Total Jumlah </th>
                <th  style="text-align: right" colspan="5"> ${konv.toRupiah(totalJob)}</th>
            </tr>
        </g:if>
        </tbody>
    </table><br>

    <legend>Detail Part</legend>
    <table id="detail-table2" class="table table-bordered">
<thead>
<th>No</th>
<th>Kode Goods</th>
<th>Nama Goods</th>
<th>Harga Satuan</th>
<th>Qty</th>
<th>Harga sblm discount</th>
<th>Discount</th>
<th>Total harga discount</th>
</thead>
<tbody>
<g:if test="${part}">
    <% def konv  =  new Konversi()
    def totalHarga = 0;
    %>
    <g:each in="${part}" status="i" var="p" >
        <% def harga = p?.t403HargaRp *  p?.t403Jumlah1
        totalHarga += harga;
        %>
         <tr>
             <td width="5">${i+1}</td>
            <td>${p?.goods.m111ID}</td>
            <td>${p?.goods.m111Nama}</td>
            <td style="text-align: right">${konv.toRupiah(p?.t403HargaRp)}</td>
            <td>${p?.t403Jumlah1}</td>
            <td style="text-align: right">${konv.toRupiah(harga)}</td>
            <td style="text-align: right">${konv.toRupiah(p?.t403DiscRp)}</td>
             <td style="text-align: right">${konv.toRupiah(p?.t403TotalRp)}</td>
           </tr>
        </g:each>
    <tr style="background-color: #F1F0F0">
        <th colspan="7"> Total Jumlah </th>
        <th style="text-align: right"colspan="7">${konv.toRupiah(totalHarga)}</th>
    </tr>
    </g:if>
    </tbody>
</table>
</div>
    <button class="btn cancel" data-dismiss="modal" name="close" id="closeTrxDetail" style="margin-top: 13px;">Tutup</button><br><br>
</div>
</body>
</html>
