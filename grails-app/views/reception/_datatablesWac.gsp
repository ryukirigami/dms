<%@ page import="com.kombos.reception.Reception" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="tableHasilWac" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>
        <th style="border-bottom: none;padding: 5px;">
            <div>No</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Perlengkapan</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Baik</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Rusak</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Tidak Ada</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Keterangan</div>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
var tableHasilWac;
var reloadTableHasilWac;
$(function(){

	reloadTableHasilWac = function() {
		tableHasilWac.fnDraw();
	}

	tableHasilWac = $('#tableHasilWac').dataTable({
		"sScrollX": "833px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'p>>",
		bFilter: false,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback":function(oSettings){
			if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
				$(oSettings.nTableWrapper).find('.dataTables_paginate').hide();
			}
		},
		"bSort": false,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesWAC")}",
		"aoColumns": [
{
	"sName": "no",
	"mDataProp": "no",
	"aTargets": [0],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"10px",
	"bVisible": true
},{
	"sName": "itemName",
	"mDataProp": "itemName",
	"aTargets": [1],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
},{
	"sName": "countOk",
	"mDataProp": "countOk",
	"aTargets": [2],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"80px",
	"bVisible": true
},{
	"sName": "countNotOk",
	"mDataProp": "countNotOk",
	"aTargets": [3],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"80px",
	"bVisible": true
},{
	"sName": "statusNone",
	"mDataProp": "statusNone",
	"aTargets": [4],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"80px",
	"bVisible": true
},{
	"sName": "note",
	"mDataProp": "note",
	"aTargets": [5],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"433px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        aoData.push(
                            {"name": 'idReception', "value": $('#receptionId').val()}
                        );
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>