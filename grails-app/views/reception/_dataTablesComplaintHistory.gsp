

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="complaintHistory_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="100%">
    <thead>
    <tr>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="default.tanggal.label" default="Tanggal" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="complaint.keluhan.label" default="Keluhan" /></div>
        </th>


    </tr>
    </thead>
</table>

<g:javascript>
var complaintHistoryTable;
var reloadComplaintHistoryTable;
$(function(){
	
	reloadComplaintHistoryTable = function() {
		complaintHistoryTable.fnDraw();
	}

	complaintHistoryTable = $('#complaintHistory_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesComplaintList")}",
		"aoColumns": [

{
	"sName": "tanggal",
	"mDataProp": "tanggal",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"50%",
	"bVisible": true
}

,

{
	"sName": "keluhan",
	"mDataProp": "keluhan",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
                        aoData.push(
                                {"name": 'idVincode', "value": idVincode}
                        );
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
