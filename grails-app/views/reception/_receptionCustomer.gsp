<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<input type="hidden" id="idCustomerUbah">

<div class="modal-header">
	<a class="close" data-dismiss="modal">×</a>
	<h3>Reception - Ubah Customer</h3>
</div>
<div class="modal-body">
			<div class="span12" id="companyDealerPopUp-table">
				<table id="customerReception_table" cellpadding="10" cellspacing="10"
					   border="0"
					   class="display table table-striped table-bordered table-hover fixed" style="table-layout: fixed; ">
					<col width="450px" />
					<col width="450px" />

					<thead>
					<tr>

						<th style="border-bottom: none;padding: 0px 0px 5px 0px; width:450px;">
							Nama
						</th>
						<th style="border-bottom: none;padding: 0px 0px 5px 0px; width:450px;">
							Alamat
						</th>
					</tr>

					<tr>

						<th style="border-top: none;padding: 0px 0px 5px 0px; width:450px;">
							<div id="filter_nama" style="padding-top: 0px;position:relative; margin-top: 0px;width: 145px;">
								<input type="text" name="nama" class="search_init" />
							</div>
						</th>

						<th style="border-top: none;padding: 0px 0px 5px 0px; width:450px;">
							<div id="filter_alamat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 145px;">
								<input type="text" name="alamat" class="search_init" />
							</div>
						</th>


					</tr>

					</thead>
				</table>

<g:javascript>
var customerReceptionTable;
var reloadcustomerReceptionTable;
var setValues;
$(function(){

	$('#tanggalKwitansi_filter').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#tanggalKwitansi_day').val(newDate.getDate());
			$('#tanggalKwitansi_month').val(newDate.getMonth()+1);
			$('#tanggalKwitansi_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			customerReceptionTable.fnDraw();
	});


    setValues = function(el,noReff) {
        var idReff = noReff;
        $("#idCustomerUbah").val(idReff);
	}

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	customerReceptionTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	customerReceptionTable = $('#customerReception_table').dataTable({
		"sScrollX": "900px",
		"bScrollCollapse": true,
		"bAutoWidth" : true,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "customerDatatables")}",
		"aoColumns": [

{
	"sName": "nama",
	"mDataProp": "nama",
	"aTargets": [1],
	"bSortable": false,
	"bSearchable": true,
	"bVisible": true,
	"mRender": function ( data, type, row ) {
	    return "<input type='radio' onclick='setValues(this,"+row['id']+");' name='nama'>" + data;
	}
},
{
    "sName": "alamat",
	"mDataProp": "alamat",
	"aTargets": [2],
	"bSortable": false,
	"bSearchable": true,
	"sClass": "tglReff",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var nama = $('#filter_nama input').val();

						if(nama){
							aoData.push(
									{"name": 'sCriteria_nama', "value": nama}
							);
						}

						var alamat = $('#filter_alamat input').val();

						if(alamat){
							aoData.push(
									{"name": 'sCriteria_alamat', "value": alamat}
							);
						}


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>
	</div>
</div>
<div class="modal-footer">
	<a onclick="ubahCustomer();" href="javascript:void(0);" class="btn btn-primary create" id="ubah_customer_btn" data-dismiss="modal">Ganti</a>
	<a href="#" class="btn" data-dismiss="modal">Close</a>
</div>