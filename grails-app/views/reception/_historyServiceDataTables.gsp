<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="history_service_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>
        <th style="border-bottom: none;padding: 5px;">
            <div>Tanggal</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Job</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Parts</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Job Suggest</div>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
var historyServiceTable;
var reloadhistoryServiceTable;
$(function(){
	
	reloadhistoryServiceTable = function() {
		historyServiceTable.fnDraw();
	}

	historyServiceTable = $('#history_service_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: false,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
       },
		"bSort": true,
		"bProcessing": false,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : 5, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "historyServiceDatatablesList")}",
		"aoColumns": [
{
	"sName": "tanggal",
	"mDataProp": "tanggal",
	"aTargets": [0],
	"bSearchable": false,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},{
	"sName": "job",
	"mDataProp": "job",
	"aTargets": [1],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
},{
	"sName": "parts",
	"mDataProp": "parts",
	"aTargets": [2],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
},{
	"sName": "jobSuggest",
	"mDataProp": "jobSuggest",
	"aTargets": [3],
	"bSearchable": false,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        aoData.push(
                                {"name": 'idVincode', "value": idVincode}
                        );
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
