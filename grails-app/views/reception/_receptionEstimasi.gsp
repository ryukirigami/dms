<%@ page import="com.kombos.maintable.TipeKerusakan; com.kombos.administrasi.Stall; com.kombos.administrasi.ColorMatching" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
        var progresss;
        var nowo = "${noWo}"
        $(document).ready(function()
        {
            save = function(){
                var noWo = "${noWo}"
                var kerusakan = $("#kerusakan").val();
                var colorKlasifikasi = $("#colorKlasifikasi").val();
                var colorPanel = $("#colorPanel").val();
                var body = $("#body_hour").val();
                var painting = $("#painting_hour").val();
                var preparation = $("#preparation_hour").val();
                var polishing = $("#polishing_hour").val();
                var asembly =$("#assembly_hour").val();
                var total = parseInt($("#body_hour").val()) + parseInt($("#painting_hour").val()) + parseInt($("#preparation_hour").val()) + parseInt($("#polishing_hour").val()) + parseInt($("#assembly_hour").val());
                 $.ajax({
                    url:'${request.contextPath}/reception/saveEstimasi',
                    type: "POST", // Always use POST when deleting data
                    data : {kerusakan:kerusakan,colorKlasifikasi:colorKlasifikasi,colorPanel:colorPanel,noWo:noWo,
                            body:body,painting:painting,preparation:preparation,polishing:polishing,asembly:asembly,total:total
                    },
                    success : function(data){
                        toastr.success("Save Succes");
                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });
            }
            hitung = function(){
                var kerusakan = $("#kerusakan").val();
                var colorKlasifikasi = $("#colorKlasifikasi").val();
                var colorPanel = $("#colorPanel").val();
                 $.ajax({
                    url:'${request.contextPath}/reception/hitungEstimasi',
                    type: "POST", // Always use POST when deleting data
                    data : {kerusakan:kerusakan,colorKlasifikasi:colorKlasifikasi,colorPanel:colorPanel},
                    success : function(data){

                        $("#body_hour").val(data.bodi);
                        $("#painting_hour").val(data.painting);
                        $("#preparation_hour").val(data.preparation);
                        $("#polishing_hour").val(data.polishing);
                        $("#assembly_hour").val(data.asembly);
                        sumTotal();
                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });
            }

            sumTotal = function(){
                var cetakHour = "", cetakMinute="";
                var hour = parseInt($("#body_hour").val()) + parseInt($("#painting_hour").val()) + parseInt($("#preparation_hour").val()) + parseInt($("#polishing_hour").val()) + parseInt($("#assembly_hour").val());
                var minute = parseInt($("#body_minute").val()) + parseInt($("#painting_minute").val()) + parseInt($("#preparation_minute").val()) + parseInt($("#polishing_minute").val()) + parseInt($("#assembly_minute").val());
                if(parseInt(minute)>=60){
                    var hour2 = Math.ceil(parseInt(minute/60));
                    var sisa = parseInt(minute%60);
                    hour += hour2;
                    minute=sisa;
                }
                cetakHour = hour
                cetakMinute = minute
                if(parseInt(hour)<10){
                    cetakHour = "0" + hour
                }
                if(parseInt(minute)<10){
                    cetakMinute = "0" + minute
                }

                var hari = "", jam = "", menit = "";
                if(parseInt(hour)>=8){
                    hari = Math.ceil(parseInt(hour/8));
                    var sisa2 = parseInt(hour%8);
                    hari = hari + " Hari"
                    if(parseInt(sisa2)>=1){
                        jam = sisa2 + " Jam"
                    }
                }else{
                    if(parseInt(hour)>=1){
                        jam = hour + " Jam"
                    }
                }

                if(parseInt(minute)>=1){
                    menit = minute + " Menit"
                }

                $("#total").html("<b>" + cetakHour +":"+ cetakMinute + "</b>");
                $("#hari").html("<b>" + hari +" "+ jam + " "+ menit + "</b>");
            }
            progresss = function(){
                 alert("Sukses");
            }
        });
    </g:javascript>
</head>
<body>

<div class="navbar box-header no-border">
    <span class="pull-left">Reception - Estimasi</span>
</div>
<div class="box">
    <div class="row-fluid">
        <div class="span6 form-horizontal" id="no_pol_search">
            <table class="table table-bordered table-dark table-hover">
                <tbody>
                <tr>
                    <td><span class="property-label">Nomor Polisi</span></td>
                    <td><span class="property-value">${nopol}
                    </span></td>
                    <td><span class="property-label">Tanggal Estimasi</span></td>
                    <td><span class="property-value"> ${tglEstimasi}
                    </span></td>
                </tr>
                <tr>
                    <td><span class="property-label">Model</span></td>
                    <td><span class="property-value">${models}
                    </span></td>
                </tr>
                </tbody>
            </table>
            <div class="span12 form-horizontal" style="margin-left: 0px;">
                <table class="table table-bordered table-dark table-hover">
                    <tbody>
                    <tr>
                        <td style="width: 200px;"><span class="property-label">Tingkat Kerusakan * </span></td>
                        <td><span class="property-value">
                            <g:select name="kerusakan.id" id="kerusakan" from="${TipeKerusakan.createCriteria().list { eq('m401StaDel','0')}}" style="width: 150px;" />
                        </span></td>
                    </tr>
                    <tr>
                        <td style="width: 200px;"><span class="property-label">Jenis Stall * </span></td>
                        <td><span class="property-value">
                            <g:select name="stall.id" from="${Stall.createCriteria().list { eq('staDel','0')}}" style="width: 150px;" />
                            <g:hiddenField name="noWo" id="noWo" value="${noWo}" />
                        </span></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="span4 offset2">
            <table class="table table-bordered table-dark table-hover">
                <caption style="font-size: 10px">Progress Pekerjaan</caption>
                <tbody>
                <tr>
                    <td><span class="property-label">Aksi</span></td>
                    <td><span class="property-value"> <g:radio name="aksi" value="1"/> Clock On &nbsp;
                                                      <g:radio name="aksi" value="2"/> Pause  &nbsp;
                                                      <g:radio name="aksi" value="4"/> Clock Off
                    </span></td>
                </tr>
                <tr>
                    <td><span class="property-label">Catatan</span></td>
                    <td><span class="property-value"> <g:textField name="catatan" id="catatan" />
                    </span></td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: right"><span class="property-label">
                        <g:field type="button" onclick="progresss()" class="btn btn-cancel cancel" name="ok" id="ok" value="OK" />
                    </span></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="dataTables_scroll">
        <div class="span12" id="estimasi-table">
            <g:render template="receptionEstimasiDataTables" />

        </div>
    </div>
    <div class="row-fluid" style="margin-top: -100px;">
        <div class="span6">
            <table class="table table-bordered table-dark table-hover">
                <tbody>
                <tr>
                    <td style="width: 100px;"><span class="property-label">Color Matching * </span></td>
                    <td><span class="property-value">
                        <g:select name="color.id" id="colorKlasifikasi" from="${ColorMatching.list()}" optionId="id" optionValue="${{it.m046Klasifikasi + " Coat"}}" style="width: 100px" />
                        <g:select name="color.id" id="colorPanel" from="${ColorMatching.list()}" optionId="id" optionValue="${{it.m046JmlPanel + " Panel"}}" style="width: 100px" />
                    </span></td>
                    <td><g:field type="button"  class="btn btn-cancel cancel" name="ok" id="ok" onclick="hitung()"  value="Hitung Estimasi" /></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row-fluid" style="margin-top: 0px;">
        <div class="span6">
            <table class="table table-bordered table-dark table-hover">
                <tbody>
                <tr>
                    <td><span class="property-label"><b>Standart Time</b></span></td>
                </tr>
                <tr>
                    <td>
                        <table class="table table-bordered table-dark table-hover">
                            <tbody>
                            <tr>
                                <td><span class="property-label">Body</span></td>
                                <td>
                                    <select id="body_hour" name="body_hour" style="width: 60px;" onchange="sumTotal();">
                                        %{
                                            for (int i=0;i<24;i++){
                                                if(i<10){
                                                        out.println('<option value="'+i+'">0'+i+'</option>');
                                                } else {
                                                        out.println('<option value="'+i+'">'+i+'</option>');
                                                }
                                            }
                                        }%
                                    </select>:
                                    <select id="body_minute" name="body_minute" style="width: 60px;" onchange="sumTotal();">
                                        <g:each var="i" in="${(0..<60)}">
                                            <g:if test="${i < 10}">
                                                    <option value="${i}">0${i}</option>
                                            </g:if>
                                            <g:else>
                                                    <option value="${i}">${i}</option>
                                            </g:else>
                                        </g:each>
                                    </select>

                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><span class="property-label">Preparation</span></td>
                                <td><span class="property-label">
                                    <select id="preparation_hour" name="preparation_hour" style="width: 60px" onchange="sumTotal()" required="required">
                                        <g:each var="i" in="${(0..<24)}">
                                            <g:if test="${i < 10}">
                                                <option value="${i}">0${i}</option>
                                            </g:if>
                                            <g:else>
                                                <option value="${i}">${i}</option>
                                            </g:else>
                                        </g:each>
                                    </select>:
                                    <select id="preparation_minute" name="preparation_minute" style="width: 60px" onchange="sumTotal()" required="required">
                                        <g:each var="i" in="${(0..<60)}">
                                            <g:if test="${i < 10}">
                                                <option value="${i}">0${i}</option>
                                            </g:if>
                                            <g:else>
                                                <option value="${i}">${i}</option>
                                            </g:else>
                                        </g:each>
                                    </select>
                                </span></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><span class="property-label">Painting</span></td>
                                <td><span class="property-label">
                                    <select id="painting_hour" name="painting_hour" style="width: 60px" onchange="sumTotal()" required="required">
                                        <g:each var="i" in="${(0..<24)}">
                                            <g:if test="${i < 10}">
                                                <option value="${i}">0${i}</option>
                                            </g:if>
                                            <g:else>
                                                <option value="${i}">${i}</option>
                                            </g:else>
                                        </g:each>
                                    </select>:
                                    <select id="painting_minute" name="painting_minute" style="width: 60px" onchange="sumTotal()" required="required">
                                        <g:each var="i" in="${(0..<60)}">
                                            <g:if test="${i < 10}">
                                                <option value="${i}">0${i}</option>
                                            </g:if>
                                            <g:else>
                                                <option value="${i}">${i}</option>
                                            </g:else>
                                        </g:each>
                                    </select>
                                </span></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><span class="property-label">Polishing</span></td>
                                <td><span class="property-label">
                                    <select id="polishing_hour" name="polishing_hour" style="width: 60px" onchange="sumTotal()" required="required">
                                        <g:each var="i" in="${(0..<24)}">
                                            <g:if test="${i < 10}">
                                                <option value="${i}">0${i}</option>
                                            </g:if>
                                            <g:else>
                                                <option value="${i}">${i}</option>
                                            </g:else>
                                        </g:each>
                                    </select>:
                                    <select id="polishing_minute" name="polishing_minute" style="width: 60px" onchange="sumTotal()" required="required">
                                        <g:each var="i" in="${(0..<60)}">
                                            <g:if test="${i < 10}">
                                                <option value="${i}">0${i}</option>
                                            </g:if>
                                            <g:else>
                                                <option value="${i}">${i}</option>
                                            </g:else>
                                        </g:each>
                                    </select>
                                </span></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><span class="property-label">Assembly</span></td>
                                <td><span class="property-label">
                                    <select id="assembly_hour" name="assembly_hour" style="width: 60px" onchange="sumTotal()" required="required">
                                    <g:each var="i" in="${(0..<24)}">
                                        <g:if test="${i < 10}">
                                            <option value="${i}">0${i}</option>
                                        </g:if>
                                        <g:else>
                                            <option value="${i}">${i}</option>
                                        </g:else>
                                    </g:each>
                                </select>:
                                    <select id="assembly_minute" name="assembly_minute" style="width: 60px" onchange="sumTotal()" required="required">
                                        <g:each var="i" in="${(0..<60)}">
                                            <g:if test="${i < 10}">
                                                <option value="${i}">0${i}</option>
                                            </g:if>
                                            <g:else>
                                                <option value="${i}">${i}</option>
                                            </g:else>
                                        </g:each>
                                    </select></span></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><span class="property-label"><b>Total</b></span></td>
                                <td><span class="property-label" id="total"></span></td>
                                <td><span class="property-label" id="hari"></span></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <button type="button" class="btn btn-primary" onclick="save()">save</button>
    %{--<g:field type="button"  class="btn btn-primary create" name="tambah" value="${message(code: 'default.button.upload.label', default: 'Print Estimasi')}" />--}%
    <button type="button" class="btn btn-primary">Close</button>
</div>
</body>
</html>
