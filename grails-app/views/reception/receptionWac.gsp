
<%@ page import="com.kombos.reception.Reception;org.apache.commons.codec.binary.Base64" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="wac.label" default="Reception - WAC" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
        $(function(){

        });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="wac.label" default="Reception - WAC" /></span>
</div>
<div class="box">
    <div class="span12" id="wacShow-table">
        <table class="table">
            <tbody>
            <tr>
                <td>
                    <g:radioGroup name="jenisWac" id="jenisWac" values="['1','2']" labels="['WAC GR','WAC BP']" value="${customerInInstance?.tujuanKedatangan?.id}" disabled="" required="" >
                        ${it.radio} ${it.label}
                    </g:radioGroup>
                </td>
                <td style="width: 60%;">
                    &nbsp
                </td>
                <td>
                    <table class="table table-bordered table-dark table-hover">
                        <tbody>
                        <tr>
                            <td><span
                                    class="property-label">Nama Customer</span></td>
                            <td>${customer?.fullNama}</td>
                        </tr>
                        <tr>
                            <td><span
                                    class="property-label">Mobil</span></td>
                            <td>${wacInstance?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel}</td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>

        <table class="table">
            <tbody>
            <tr>
                <td style="width: 55%;">
                    <br/>
                    <div>
                        <g:if test="${wacInstance?.t401GambarWAC}">
                            <img width="100%"
                                 src="data:jpg;base64,${new String(new Base64().encode(wacInstance?.t401GambarWAC), "UTF-8")}"/></br></br>
                        </g:if>
                    </div>
                </td>
                <td style="width: 5%;">
                    Fuel
                    <div id="slider-vertical" style="height:200px;text-align: center">
                        F <input type="range" min="0" max="10" orient="vertical" style="background-color: white"  value="${wacInstance?.t401Bensin}" disabled=""/> E
                    </div>
                </td>
                <td style="width: 40%;">
                    Catatan </br>
                    <g:textArea name="catatanWac" rows="5" style="resize: none;width: 100%" value="${wacInstance?.t401CatatanWAC}" disabled=""/>
                </td>
            </tr>
            </tbody>
        </table>

        <g:render template="datatablesWac"/>
    </div>
</div>
</body>
</html>
