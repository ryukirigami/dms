<%@ page import="com.kombos.administrasi.Serial" %>
<%@ page import="com.kombos.administrasi.Section" %>
<%@ page import="com.kombos.administrasi.Operation" %>
<%@ page import="com.kombos.administrasi.KategoriJob"%>

<g:javascript>
	function saveJob(){
		if(confirm('${message(code: 'default.button.save.confirm.message', default: 'Anda yakin data akan disimpan?')}'))
		{
			var m053StaLift = $('input:radio[name=m053StaLift]:checked').val();
			var m053StaPaket = $('input:radio[name=m053StaPaket]:checked').val();
			var m053StaPaint = $('input:radio[name=m053StaPaint]:checked').val();
			
			var aoDataJob = [];
			aoDataJob.push(
				{"name": 'section.id', "value": $('select[name="section.id"]').val()}
			);		
			aoDataJob.push(
				{"name": 'serial.id', "value": $('select[name="serial.id"]').val()}
			);
			aoDataJob.push(
				{"name": 'm053Id', "value": $('#m053Id').val()}
			);				
			aoDataJob.push(
				{"name": 'm053NamaOperation', "value": $('#m053NamaOperation').val()}
			);		
			aoDataJob.push(
				{"name": 'm053StaLift', "value": m053StaLift}
			);		
			aoDataJob.push(
				{"name": 'm053StaPaket', "value": m053StaPaket}
			);		
			aoDataJob.push(
				{"name": 'kategoriJob.id', "value": $('select[name="kategoriJob.id"]').val()}
			);
			aoDataJob.push(
				{"name": 'm053StaPaint', "value": m053StaPaint}
			);
			aoDataJob.push(
				{"name": 'm053Ket', "value": $('#m053Ket').val()}
			);			
			$.ajax({
				url:'${request.contextPath}/operation/save',
				type: "POST", // Always use POST when deleting data
				data: aoDataJob,
				dataType: 'json',
				success: function(data,textStatus,xhr){
					//reloadoperationTableInput();	
				},
				error:function(XMLHttpRequest,textStatus,errorThrown){
					//reloadoperationTableInput();
				},
				complete:function(data,textStatus){
					reloadoperationTableInput();
				}
			});	
		}
	}
</g:javascript>

<div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Reception - Input Job</h3>
</div>
<div class="modal-body">
    <div class="box">
        
%{--	
		<g:formRemote class="form-horizontal" name="create" on404="alert('not found!');" onSuccess="reloadoperationTableInput();" update="operation-form"
		url="[controller: 'operation', action:'save']">	
--}%		
		<form id="inputJob" class="form-horizontal">
	
			<fieldset class="form">
				<div class="control-group fieldcontain ${hasErrors(bean: operationInstance, field: 'section', 'error')} required">
					<label class="control-label" for="section">
						<g:message code="operation.section.label" default="Nama Section" />
						<span class="required-indicator">*</span>
					</label>
					<div class="controls">
					<g:select noSelection="${['':'Select One...']}" id="section.id" name="section.id" from="${Section.createCriteria().list {eq("staDel", "0");order("m051NamaSection", "asc")}}" optionKey="id" required="" class="many-to-one"/>
					</div>
				</div>

				<div class="control-group fieldcontain ${hasErrors(bean: operationInstance, field: 'serial', 'error')} required">
					<label class="control-label" for="serial">
						<g:message code="operation.serial.label" default="Nama Serial" />
						<span class="required-indicator">*</span>
					</label>
					<div class="controls">
					<g:select id="serial" name="serial.id" id="serial.id" from="${Serial.createCriteria().list {eq("staDel", "0");order("m052NamaSerial", "asc")}}" optionKey="id" required="" class="many-to-one"/>
					</div>
				</div>

				<div class="control-group fieldcontain ${hasErrors(bean: operationInstance, field: 'm053Id', 'error')} required">
					<label class="control-label" for="m053Id">
						<g:message code="operation.m053Id.label" default="Kode Repair" />
						<span class="required-indicator">*</span>
					</label>
					<div class="controls">
					<g:textField name="m053Id" id="m053Id" maxlength="7" required="" />
					</div>
				</div>

				<div class="control-group fieldcontain ${hasErrors(bean: operationInstance, field: 'm053NamaOperation', 'error')} required">
					<label class="control-label" for="m053NamaOperation">
						<g:message code="operation.m053NamaOperation.label" default="Nama Repair" />
						<span class="required-indicator">*</span>
					</label>
					<div class="controls">
					<g:textField name="m053NamaOperation" id="m053NamaOperation" maxlength="50" required="" />
					</div>
				</div>

				<div class="control-group fieldcontain ${hasErrors(bean: operationInstance, field: 'm053StaLift', 'error')} required">
					<label class="control-label" for="m053StaLift">
						<g:message code="operation.m053StaLift.label" default="Butuh Lift" />
						<span class="required-indicator">*</span>
					</label>
					<div class="controls">
					<g:radioGroup name="m053StaLift" required="" values="['1','0']" labels="['Ya','Tidak']" >
						${it.radio} ${it.label}
					</g:radioGroup>
					</div>
				</div>

				<div class="control-group fieldcontain ${hasErrors(bean: operationInstance, field: 'm053StaPaket', 'error')} required">
					<label class="control-label" for="m053StaPaket">
						<g:message code="operation.m053StaPaket.label" default="Paket" />
						<span class="required-indicator">*</span>
					</label>
					<div class="controls">
					<g:radioGroup name="m053StaPaket" required="" values="['1','0']" labels="['Ya','Tidak']" >
						${it.radio} ${it.label}
					</g:radioGroup>
					</div>
				</div>

				<div class="control-group fieldcontain ${hasErrors(bean: operationInstance, field: 'kategoriJob', 'error')} required">
					<label class="control-label" for="kategoriJob">
						<g:message code="operation.kategoriJob.label" default="Kategori Job" />
						<span class="required-indicator">*</span>
					</label>
					<div class="controls">
					<g:select name="kategoriJob.id" id="kategoriJob.id" from="${KategoriJob.list()}" optionKey="id" required="" class="many-to-one"/>
					</div>
				</div>

				<div class="control-group fieldcontain ${hasErrors(bean: operationInstance, field: 'm053StaPaint', 'error')} required">
					<label class="control-label" for="m053StaPaint">
						<g:message code="operation.m053StaPaint.label" default="Paint" />
						<span class="required-indicator">*</span>
					</label>
					<div class="controls">
					<g:radioGroup name="m053StaPaint" required="" values="['1','0']" labels="['Ya','Tidak']" >
						${it.radio} ${it.label}
					</g:radioGroup>
					</div>
				</div>

				<div class="control-group fieldcontain ${hasErrors(bean: operationInstance, field: 'm053Ket', 'error')} required">
					<label class="control-label" for="m053Ket">
						<g:message code="operation.m053Ket.label" default="Keterangan" />
						<span class="required-indicator">*</span>
					</label>
					<div class="controls">
					<g:textArea rows="2" cols="50" required="" name="m053Ket" id="m053Ket" maxlength="255" />
					</div>
				</div>
			</fieldset>
			<fieldset class="buttons controls">
					%{--	
					<g:submitButton class="btn btn-primary create" name="save" value="${message(code: 'default.button.create.label', default: 'Create')}"
									onclick="return confirm('${message(code: 'default.button.save.confirm.message', default: 'Anda yakin data akan disimpan?')}');"/>
					--}%	
					<a onclick="saveJob();" href="javascript:void(0);" class="btn btn-primary create" id="add_part_btn">Create</a>
			</fieldset>
	
		</form>

%{--			
		</g:formRemote>
--}%			
		<div class="dataTables_scroll">
            <div class="span12" id="companyDealerPopUp-table">
                <g:render template="datatablesJobInput"/>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    %{--<a onclick="addParts();" href="javascript:void(0);" class="btn btn-success" id="add_part_btn">Add Parts</a>--}%
    <a href="#" class="btn" data-dismiss="modal">Close</a>
</div>