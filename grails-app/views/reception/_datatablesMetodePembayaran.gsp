<table id="metodePembayaranDatatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>
        <th style="border-bottom: none;padding: 5px;">
            <div>No</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Metode Pembayaran</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Jumlah Pembayaran</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Keterangan</div>
        </th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <td colspan="3"><span>TOTAL </span></td>
        <td><span class="pull-right numeric">10000000</span></td>
    </tr>
    </tfoot>
</table>

<g:javascript>
var metodePembayaranTable;
var reloadmetodePembayaranTable;
$(function(){

	reloadmetodePembayaranTable = function() {
		metodePembayaranTable.fnDraw();
	}

	metodePembayaranTable = $('#metodePembayaranDatatables').dataTable({
		"sScrollX": "833px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'p>>",
		bFilter: false,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			var aData = metodePembayaranTable.fnGetData(nRow);
			$.ajax({type:'POST',
	   			data : aData,
	   			url:'${g.createLink(action: "partList")}',
	   			success:function(data,textStatus){
					var nDetailsRow = metodePembayaranTable.fnOpen(nRow,data,'details');
					//var innerDetails = $('div.innerDetails', nDetailsRow);
					//$('div.innerDetails', nDetailsRow).slideDown();
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
			return nRow;
		},
		"fnDrawCallback":function(oSettings){
			if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
				$(oSettings.nTableWrapper).find('.dataTables_paginate').hide();
			}
		},
		"bSort": true,
		"bProcessing": true,
		"bServerSide": false,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "jobnPartsDatatablesList")}",
		"aoColumns": [
{
	"sName": "namaJob",
	"mDataProp": "namaJob",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['idJob']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": false,
	"bSortable": true,
	"sWidth":"527px",
	"bVisible": true
},{
	"sName": "rate",
	"mDataProp": "rate",
	"aTargets": [1],
	"mRender": function ( data, type, row ) {
		if(data != 'null')
			return '<span class="pull-right numeric">'+data+'</span>';
		else
			return '<span></span>';
	},
	"bSearchable": false,
	"bSortable": true,
	"sWidth":"69px",
	"bVisible": true
},{
	"sName": "statusWarranty",
	"mDataProp": "statusWarranty",
	"aTargets": [2],
	"bSearchable": false,
	"bSortable": true,
	"sWidth":"73px",
	"bVisible": true
},{
	"sName": "nominal",
	"mDataProp": "nominal",
	"aTargets": [3],
	"mRender": function ( data, type, row ) {
		if(data != 'null')
			return '<span class="pull-right numeric">'+data+'</span>';
		else
			return '<span></span>';
	},
	"bSearchable": false,
	"bSortable": true,
	"sWidth":"119px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>