<%@ page import="com.kombos.administrasi.KegiatanApproval; com.kombos.reception.Reception" %>
<div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Approval - Pelanggan Service Punya Tunggakan</h3>
</div>
<g:javascript>
    $(function(){
        sendApprovalPiutang = function(){
            var pesan = $('#pesanApprovalPiutang').val();
            var kegiatanApproval = $('#kegiatanApprovalPiutang').val();
            $.ajax({
                    url:'${request.contextPath}/reception/approvalPiutang',
                    type: "POST", // Always use POST when deleting data
                    data : {pesan:pesan,kegiatanApproval:kegiatanApproval,idVincode:idVincode},
                    success : function(data){
                        toastr.success('<div>Approval Sudah Dikirim</div>');
                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });
        };

    });
</g:javascript>
<div class="modal-body">
    <div class="box">
        <table class="table">
            <tbody>
            <tr>
                <td>
                    Nama Kegiatan *
                </td>
                <td>
                    <g:select id="kegiatanApprovalPiutang" name="kegiatanApprovalPiutang" from="${com.kombos.administrasi.KegiatanApproval.list()}"  value="${KegiatanApproval?.findByM770KegiatanApproval(KegiatanApproval.PELANGGAN_MENUNGGAK)?.id}" optionKey="id" required="" class="many-to-one"/>
                </td>
            </tr>
            <tr>
                <td>
                    Nama Pelanggan
                </td>
                <td>
                    <g:textField name="pelangganApprovalPiutang" id="pelangganApprovalPiutang" readonly="" />
                </td>
            </tr>
            <tr>
                <td>
                    Tanggal dan Jam
                </td>
                <td>
                    ${new Date().format("dd-MM-yyyy HH:mm")}
                </td>
            </tr>
            <tr>
                <td>
                    Pesan
                </td>
                <td>
                    <textarea id="pesanApprovalPiutang" name="pesanApprovalPiutang" cols="200" rows="3"></textarea>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="modal-footer">
    <a href="#" class="btn" onclick="sendApprovalPiutang()" data-dismiss="modal">Send</a>
    <a href="#" class="btn" data-dismiss="modal">Close</a>
</div>