
<%@ page import="com.kombos.reception.Reception" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="reception_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401NoWO.label" default="T401 No WO" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401TanggalWO.label" default="T401 Tanggal WO" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401StaReceptionEstimasiSalesQuotation.label" default="T401 Sta Reception Estimasi Sales Quotation" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.customerIn.label" default="Customer In" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.customerVehicle.label" default="Customer Vehicle" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.historyCustomerVehicle.label" default="History Customer Vehicle" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.operation.label" default="Operation" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.namaManPower.label" default="Nama Man Power" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.customer.label" default="Customer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.historyCustomer.label" default="History Customer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401StaApp.label" default="T401 Sta App" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401NoAppointment.label" default="T401 No Appointment" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401StaKategoriCustUmumSPKAsuransi.label" default="T401 Sta Kategori Cust Umum SPKA suransi" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401BawaSPK.label" default="T401 Bawa SPK" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.sPK.label" default="SPK" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401BawaAsuransi.label" default="T401 Bawa Asuransi" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.sPkAsuransi.label" default="SP k Asuransi" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401TglJamCetakWO.label" default="T401 Tgl Jam Cetak WO" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401StaAmbilWO.label" default="T401 Sta Ambil WO" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401TglJamAmbilWO.label" default="T401 Tgl Jam Ambil WO" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401StaTinggalMobil.label" default="T401 Sta Tinggal Mobil" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401StaTinggalParts.label" default="T401 Sta Tinggal Parts" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401StaCustomerTunggu.label" default="T401 Sta Customer Tunggu" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401StaObatJalan.label" default="T401 Sta Obat Jalan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401TglStaObatJalan.label" default="T401 Tgl Sta Obat Jalan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401StaButuhSPKSebelumProd.label" default="T401 Sta Butuh SPKS ebelum Prod" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401TglJamKirimBerkasAsuransi.label" default="T401 Tgl Jam Kirim Berkas Asuransi" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401TglJamJanjiPenyerahan.label" default="T401 Tgl Jam Janji Penyerahan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401TglJamRencana.label" default="T401 Tgl Jam Rencana" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401TglJamJanjiBayarDP.label" default="T401 Tgl Jam Janji Bayar DP" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401TglJamPenyerahan.label" default="T401 Tgl Jam Penyerahan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401StaOkCancelReSchedule.label" default="T401 Sta Ok Cancel Re Schedule" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401TglJamPenyerahanReSchedule.label" default="T401 Tgl Jam Penyerahan Re Schedule" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401AlasanReSchedule.label" default="T401 Alasan Re Schedule" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401GambarWAC.label" default="T401 Gambar WAC" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401CatatanWAC.label" default="T401 Catatan WAC" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401KmSaatIni.label" default="T401 Km Saat Ini" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401Bensin.label" default="T401 Bensin" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401Catatan.label" default="T401 Catatan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.tipeKerusakan.label" default="Tipe Kerusakan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401StaTPSLine.label" default="T401 Sta TPSL ine" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.colorMatchingKlasifikasi.label" default="Color Matching Klasifikasi" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.colorMatchingJmlPanel.label" default="Color Matching Jml Panel" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401Proses1.label" default="T401 Proses1" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401Proses2.label" default="T401 Proses2" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401Proses3.label" default="T401 Proses3" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401Proses4.label" default="T401 Proses4" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401Proses5.label" default="T401 Proses5" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401TotalProses.label" default="T401 Total Proses" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401HP.label" default="T401 HP" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401HargaRp.label" default="T401 Harga Rp" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401DiscRp.label" default="T401 Disc Rp" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401DiscPersen.label" default="T401 Disc Persen" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401TotalRp.label" default="T401 Total Rp" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401DPRp.label" default="T401 DPR p" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401TagihanRp.label" default="T401 Tagihan Rp" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401SpDiscRp.label" default="T401 Sp Disc Rp" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.alasanCancel.label" default="Alasan Cancel" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401NamaSA.label" default="T401 Nama SA" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401staReminderSBE.label" default="T401sta Reminder SBE" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401TglJamNotifikasi.label" default="T401 Tgl Jam Notifikasi" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401StaPDI.label" default="T401 Sta PDI" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401StaInvoice.label" default="T401 Sta Invoice" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401NamaSAPenerimaBPCenter.label" default="T401 Nama SAP enerima BPC enter" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401NoExplanation.label" default="T401 No Explanation" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401TglJamExplanation.label" default="T401 Tgl Jam Explanation" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401TglJamExplanationSelesai.label" default="T401 Tgl Jam Explanation Selesai" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401NamaSAExplaination.label" default="T401 Nama SAE xplaination" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.stall.label" default="Stall" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401Area.label" default="T401 Area" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.repairDifficulty.label" default="Repair Difficulty" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.bumperPaintngTimeCompanyDealer.label" default="Bumper Paintng Time Company Dealer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.bumperPaintngTimeTglBerlaku.label" default="Bumper Paintng Time Tgl Berlaku" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.bumperPaintngTimeMasterPanel.label" default="Bumper Paintng Time Master Panel" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401xKet.label" default="T401x Ket" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401xNamaUser.label" default="T401x Nama User" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401xDivisi.label" default="T401x Divisi" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401StaDel.label" default="T401 Sta Del" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.createdBy.label" default="Created By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.updatedBy.label" default="Updated By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>

		
		</tr>
	</thead>
</table>

<g:javascript>
var receptionTable;
var reloadReceptionTable;
$(function(){
	
	reloadReceptionTable = function() {
		receptionTable.fnDraw();
	}
	
	$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	receptionTable.fnDraw();
		}
	});
	
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	receptionTable = $('#reception_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

	{
		"sName": "t401NoWO",
		"mDataProp": "t401NoWO",
		"aTargets": [0],
		"mRender": function ( data, type, row ) {
			return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
		},
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"300px",
		"bVisible": true
	}

	,

	{
		"sName": "t401TanggalWO",
		"mDataProp": "t401TanggalWO",
		"aTargets": [1],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401StaReceptionEstimasiSalesQuotation",
		"mDataProp": "t401StaReceptionEstimasiSalesQuotation",
		"aTargets": [2],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "customerIn",
		"mDataProp": "customerIn",
		"aTargets": [3],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "customerVehicle",
		"mDataProp": "customerVehicle",
		"aTargets": [4],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "historyCustomerVehicle",
		"mDataProp": "historyCustomerVehicle",
		"aTargets": [5],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "operation",
		"mDataProp": "operation",
		"aTargets": [6],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "namaManPower",
		"mDataProp": "namaManPower",
		"aTargets": [7],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "customer",
		"mDataProp": "customer",
		"aTargets": [8],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "historyCustomer",
		"mDataProp": "historyCustomer",
		"aTargets": [9],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401StaApp",
		"mDataProp": "t401StaApp",
		"aTargets": [10],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401NoAppointment",
		"mDataProp": "t401NoAppointment",
		"aTargets": [11],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401StaKategoriCustUmumSPKAsuransi",
		"mDataProp": "t401StaKategoriCustUmumSPKAsuransi",
		"aTargets": [12],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401BawaSPK",
		"mDataProp": "t401BawaSPK",
		"aTargets": [13],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "sPK",
		"mDataProp": "sPK",
		"aTargets": [14],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401BawaAsuransi",
		"mDataProp": "t401BawaAsuransi",
		"aTargets": [15],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "sPkAsuransi",
		"mDataProp": "sPkAsuransi",
		"aTargets": [16],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401TglJamCetakWO",
		"mDataProp": "t401TglJamCetakWO",
		"aTargets": [17],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401StaAmbilWO",
		"mDataProp": "t401StaAmbilWO",
		"aTargets": [18],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401TglJamAmbilWO",
		"mDataProp": "t401TglJamAmbilWO",
		"aTargets": [19],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401StaTinggalMobil",
		"mDataProp": "t401StaTinggalMobil",
		"aTargets": [21],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401StaTinggalParts",
		"mDataProp": "t401StaTinggalParts",
		"aTargets": [22],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401StaCustomerTunggu",
		"mDataProp": "t401StaCustomerTunggu",
		"aTargets": [23],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401StaObatJalan",
		"mDataProp": "t401StaObatJalan",
		"aTargets": [24],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401TglStaObatJalan",
		"mDataProp": "t401TglStaObatJalan",
		"aTargets": [25],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401StaButuhSPKSebelumProd",
		"mDataProp": "t401StaButuhSPKSebelumProd",
		"aTargets": [26],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401TglJamKirimBerkasAsuransi",
		"mDataProp": "t401TglJamKirimBerkasAsuransi",
		"aTargets": [27],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401TglJamJanjiPenyerahan",
		"mDataProp": "t401TglJamJanjiPenyerahan",
		"aTargets": [28],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401TglJamRencana",
		"mDataProp": "t401TglJamRencana",
		"aTargets": [29],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401TglJamJanjiBayarDP",
		"mDataProp": "t401TglJamJanjiBayarDP",
		"aTargets": [30],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401TglJamPenyerahan",
		"mDataProp": "t401TglJamPenyerahan",
		"aTargets": [31],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401StaOkCancelReSchedule",
		"mDataProp": "t401StaOkCancelReSchedule",
		"aTargets": [32],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401TglJamPenyerahanReSchedule",
		"mDataProp": "t401TglJamPenyerahanReSchedule",
		"aTargets": [33],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401AlasanReSchedule",
		"mDataProp": "t401AlasanReSchedule",
		"aTargets": [34],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401GambarWAC",
		"mDataProp": "t401GambarWAC",
		"aTargets": [35],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401CatatanWAC",
		"mDataProp": "t401CatatanWAC",
		"aTargets": [36],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401KmSaatIni",
		"mDataProp": "t401KmSaatIni",
		"aTargets": [37],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401Bensin",
		"mDataProp": "t401Bensin",
		"aTargets": [38],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401Catatan",
		"mDataProp": "t401Catatan",
		"aTargets": [39],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "tipeKerusakan",
		"mDataProp": "tipeKerusakan",
		"aTargets": [40],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401StaTPSLine",
		"mDataProp": "t401StaTPSLine",
		"aTargets": [41],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "colorMatchingKlasifikasi",
		"mDataProp": "colorMatchingKlasifikasi",
		"aTargets": [42],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "colorMatchingJmlPanel",
		"mDataProp": "colorMatchingJmlPanel",
		"aTargets": [43],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401Proses1",
		"mDataProp": "t401Proses1",
		"aTargets": [44],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401Proses2",
		"mDataProp": "t401Proses2",
		"aTargets": [45],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401Proses3",
		"mDataProp": "t401Proses3",
		"aTargets": [46],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401Proses4",
		"mDataProp": "t401Proses4",
		"aTargets": [47],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401Proses5",
		"mDataProp": "t401Proses5",
		"aTargets": [48],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401TotalProses",
		"mDataProp": "t401TotalProses",
		"aTargets": [49],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401HP",
		"mDataProp": "t401HP",
		"aTargets": [50],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401HargaRp",
		"mDataProp": "t401HargaRp",
		"aTargets": [51],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401DiscRp",
		"mDataProp": "t401DiscRp",
		"aTargets": [52],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401DiscPersen",
		"mDataProp": "t401DiscPersen",
		"aTargets": [53],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401TotalRp",
		"mDataProp": "t401TotalRp",
		"aTargets": [54],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401DPRp",
		"mDataProp": "t401DPRp",
		"aTargets": [55],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401TagihanRp",
		"mDataProp": "t401TagihanRp",
		"aTargets": [56],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401SpDiscRp",
		"mDataProp": "t401SpDiscRp",
		"aTargets": [57],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "alasanCancel",
		"mDataProp": "alasanCancel",
		"aTargets": [58],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401NamaSA",
		"mDataProp": "t401NamaSA",
		"aTargets": [59],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401staReminderSBE",
		"mDataProp": "t401staReminderSBE",
		"aTargets": [60],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401TglJamNotifikasi",
		"mDataProp": "t401TglJamNotifikasi",
		"aTargets": [61],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401StaPDI",
		"mDataProp": "t401StaPDI",
		"aTargets": [62],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401StaInvoice",
		"mDataProp": "t401StaInvoice",
		"aTargets": [63],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401NamaSAPenerimaBPCenter",
		"mDataProp": "t401NamaSAPenerimaBPCenter",
		"aTargets": [64],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401NoExplanation",
		"mDataProp": "t401NoExplanation",
		"aTargets": [65],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401TglJamExplanation",
		"mDataProp": "t401TglJamExplanation",
		"aTargets": [66],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401TglJamExplanationSelesai",
		"mDataProp": "t401TglJamExplanationSelesai",
		"aTargets": [67],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401NamaSAExplaination",
		"mDataProp": "t401NamaSAExplaination",
		"aTargets": [68],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "stall",
		"mDataProp": "stall",
		"aTargets": [69],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401Area",
		"mDataProp": "t401Area",
		"aTargets": [70],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "repairDifficulty",
		"mDataProp": "repairDifficulty",
		"aTargets": [71],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "bumperPaintngTimeCompanyDealer",
		"mDataProp": "bumperPaintngTimeCompanyDealer",
		"aTargets": [72],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "bumperPaintngTimeTglBerlaku",
		"mDataProp": "bumperPaintngTimeTglBerlaku",
		"aTargets": [73],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "bumperPaintngTimeMasterPanel",
		"mDataProp": "bumperPaintngTimeMasterPanel",
		"aTargets": [74],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401xKet",
		"mDataProp": "t401xKet",
		"aTargets": [75],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401xNamaUser",
		"mDataProp": "t401xNamaUser",
		"aTargets": [76],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401xDivisi",
		"mDataProp": "t401xDivisi",
		"aTargets": [77],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401StaDel",
		"mDataProp": "t401StaDel",
		"aTargets": [79],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "createdBy",
		"mDataProp": "createdBy",
		"aTargets": [80],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "updatedBy",
		"mDataProp": "updatedBy",
		"aTargets": [81],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "lastUpdProcess",
		"mDataProp": "lastUpdProcess",
		"aTargets": [82],
		"bSearchable": true,
		"bSortable": true,
		"sWidth":"200px",
		"bVisible": true
	}

	],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var t401NoWO = $('#filter_t401NoWO input').val();
						if(t401NoWO){
							aoData.push(
									{"name": 'sCriteria_t401NoWO', "value": t401NoWO}
							);
						}

						var t401TanggalWO = $('#search_t401TanggalWO').val();
						var t401TanggalWODay = $('#search_t401TanggalWO_day').val();
						var t401TanggalWOMonth = $('#search_t401TanggalWO_month').val();
						var t401TanggalWOYear = $('#search_t401TanggalWO_year').val();
						
						if(t401TanggalWO){
							aoData.push(
									{"name": 'sCriteria_t401TanggalWO', "value": "date.struct"},
									{"name": 'sCriteria_t401TanggalWO_dp', "value": t401TanggalWO},
									{"name": 'sCriteria_t401TanggalWO_day', "value": t401TanggalWODay},
									{"name": 'sCriteria_t401TanggalWO_month', "value": t401TanggalWOMonth},
									{"name": 'sCriteria_t401TanggalWO_year', "value": t401TanggalWOYear}
							);
						}
	
						var t401StaReceptionEstimasiSalesQuotation = $('#filter_t401StaReceptionEstimasiSalesQuotation input').val();
						if(t401StaReceptionEstimasiSalesQuotation){
							aoData.push(
									{"name": 'sCriteria_t401StaReceptionEstimasiSalesQuotation', "value": t401StaReceptionEstimasiSalesQuotation}
							);
						}
	
						var customerIn = $('#filter_customerIn input').val();
						if(customerIn){
							aoData.push(
									{"name": 'sCriteria_customerIn', "value": customerIn}
							);
						}
	
						var customerVehicle = $('#filter_customerVehicle input').val();
						if(customerVehicle){
							aoData.push(
									{"name": 'sCriteria_customerVehicle', "value": customerVehicle}
							);
						}
	
						var historyCustomerVehicle = $('#filter_historyCustomerVehicle input').val();
						if(historyCustomerVehicle){
							aoData.push(
									{"name": 'sCriteria_historyCustomerVehicle', "value": historyCustomerVehicle}
							);
						}
	
						var operation = $('#filter_operation input').val();
						if(operation){
							aoData.push(
									{"name": 'sCriteria_operation', "value": operation}
							);
						}
	
						var namaManPower = $('#filter_namaManPower input').val();
						if(namaManPower){
							aoData.push(
									{"name": 'sCriteria_namaManPower', "value": namaManPower}
							);
						}
	
						var customer = $('#filter_customer input').val();
						if(customer){
							aoData.push(
									{"name": 'sCriteria_customer', "value": customer}
							);
						}
	
						var historyCustomer = $('#filter_historyCustomer input').val();
						if(historyCustomer){
							aoData.push(
									{"name": 'sCriteria_historyCustomer', "value": historyCustomer}
							);
						}
	
						var t401StaApp = $('#filter_t401StaApp input').val();
						if(t401StaApp){
							aoData.push(
									{"name": 'sCriteria_t401StaApp', "value": t401StaApp}
							);
						}
	
						var t401NoAppointment = $('#filter_t401NoAppointment input').val();
						if(t401NoAppointment){
							aoData.push(
									{"name": 'sCriteria_t401NoAppointment', "value": t401NoAppointment}
							);
						}
	
						var t401StaKategoriCustUmumSPKAsuransi = $('#filter_t401StaKategoriCustUmumSPKAsuransi input').val();
						if(t401StaKategoriCustUmumSPKAsuransi){
							aoData.push(
									{"name": 'sCriteria_t401StaKategoriCustUmumSPKAsuransi', "value": t401StaKategoriCustUmumSPKAsuransi}
							);
						}
	
						var t401BawaSPK = $('#filter_t401BawaSPK input').val();
						if(t401BawaSPK){
							aoData.push(
									{"name": 'sCriteria_t401BawaSPK', "value": t401BawaSPK}
							);
						}
	
						var sPK = $('#filter_sPK input').val();
						if(sPK){
							aoData.push(
									{"name": 'sCriteria_sPK', "value": sPK}
							);
						}
	
						var t401BawaAsuransi = $('#filter_t401BawaAsuransi input').val();
						if(t401BawaAsuransi){
							aoData.push(
									{"name": 'sCriteria_t401BawaAsuransi', "value": t401BawaAsuransi}
							);
						}
	
						var sPkAsuransi = $('#filter_sPkAsuransi input').val();
						if(sPkAsuransi){
							aoData.push(
									{"name": 'sCriteria_sPkAsuransi', "value": sPkAsuransi}
							);
						}

						var t401TglJamCetakWO = $('#search_t401TglJamCetakWO').val();
						var t401TglJamCetakWODay = $('#search_t401TglJamCetakWO_day').val();
						var t401TglJamCetakWOMonth = $('#search_t401TglJamCetakWO_month').val();
						var t401TglJamCetakWOYear = $('#search_t401TglJamCetakWO_year').val();
						
						if(t401TglJamCetakWO){
							aoData.push(
									{"name": 'sCriteria_t401TglJamCetakWO', "value": "date.struct"},
									{"name": 'sCriteria_t401TglJamCetakWO_dp', "value": t401TglJamCetakWO},
									{"name": 'sCriteria_t401TglJamCetakWO_day', "value": t401TglJamCetakWODay},
									{"name": 'sCriteria_t401TglJamCetakWO_month', "value": t401TglJamCetakWOMonth},
									{"name": 'sCriteria_t401TglJamCetakWO_year', "value": t401TglJamCetakWOYear}
							);
						}
	
						var t401StaAmbilWO = $('#filter_t401StaAmbilWO input').val();
						if(t401StaAmbilWO){
							aoData.push(
									{"name": 'sCriteria_t401StaAmbilWO', "value": t401StaAmbilWO}
							);
						}

						var t401TglJamAmbilWO = $('#search_t401TglJamAmbilWO').val();
						var t401TglJamAmbilWODay = $('#search_t401TglJamAmbilWO_day').val();
						var t401TglJamAmbilWOMonth = $('#search_t401TglJamAmbilWO_month').val();
						var t401TglJamAmbilWOYear = $('#search_t401TglJamAmbilWO_year').val();
						
						if(t401TglJamAmbilWO){
							aoData.push(
									{"name": 'sCriteria_t401TglJamAmbilWO', "value": "date.struct"},
									{"name": 'sCriteria_t401TglJamAmbilWO_dp', "value": t401TglJamAmbilWO},
									{"name": 'sCriteria_t401TglJamAmbilWO_day', "value": t401TglJamAmbilWODay},
									{"name": 'sCriteria_t401TglJamAmbilWO_month', "value": t401TglJamAmbilWOMonth},
									{"name": 'sCriteria_t401TglJamAmbilWO_year', "value": t401TglJamAmbilWOYear}
							);
						}
	
						var t401PermintaanCust = $('#filter_t401PermintaanCust input').val();
						if(t401PermintaanCust){
							aoData.push(
									{"name": 'sCriteria_t401PermintaanCust', "value": t401PermintaanCust}
							);
						}
	
						var t401StaTinggalMobil = $('#filter_t401StaTinggalMobil input').val();
						if(t401StaTinggalMobil){
							aoData.push(
									{"name": 'sCriteria_t401StaTinggalMobil', "value": t401StaTinggalMobil}
							);
						}
	
						var t401StaTinggalParts = $('#filter_t401StaTinggalParts input').val();
						if(t401StaTinggalParts){
							aoData.push(
									{"name": 'sCriteria_t401StaTinggalParts', "value": t401StaTinggalParts}
							);
						}
	
						var t401StaCustomerTunggu = $('#filter_t401StaCustomerTunggu input').val();
						if(t401StaCustomerTunggu){
							aoData.push(
									{"name": 'sCriteria_t401StaCustomerTunggu', "value": t401StaCustomerTunggu}
							);
						}
	
						var t401StaObatJalan = $('#filter_t401StaObatJalan input').val();
						if(t401StaObatJalan){
							aoData.push(
									{"name": 'sCriteria_t401StaObatJalan', "value": t401StaObatJalan}
							);
						}

						var t401TglStaObatJalan = $('#search_t401TglStaObatJalan').val();
						var t401TglStaObatJalanDay = $('#search_t401TglStaObatJalan_day').val();
						var t401TglStaObatJalanMonth = $('#search_t401TglStaObatJalan_month').val();
						var t401TglStaObatJalanYear = $('#search_t401TglStaObatJalan_year').val();
						
						if(t401TglStaObatJalan){
							aoData.push(
									{"name": 'sCriteria_t401TglStaObatJalan', "value": "date.struct"},
									{"name": 'sCriteria_t401TglStaObatJalan_dp', "value": t401TglStaObatJalan},
									{"name": 'sCriteria_t401TglStaObatJalan_day', "value": t401TglStaObatJalanDay},
									{"name": 'sCriteria_t401TglStaObatJalan_month', "value": t401TglStaObatJalanMonth},
									{"name": 'sCriteria_t401TglStaObatJalan_year', "value": t401TglStaObatJalanYear}
							);
						}
	
						var t401StaButuhSPKSebelumProd = $('#filter_t401StaButuhSPKSebelumProd input').val();
						if(t401StaButuhSPKSebelumProd){
							aoData.push(
									{"name": 'sCriteria_t401StaButuhSPKSebelumProd', "value": t401StaButuhSPKSebelumProd}
							);
						}

						var t401TglJamKirimBerkasAsuransi = $('#search_t401TglJamKirimBerkasAsuransi').val();
						var t401TglJamKirimBerkasAsuransiDay = $('#search_t401TglJamKirimBerkasAsuransi_day').val();
						var t401TglJamKirimBerkasAsuransiMonth = $('#search_t401TglJamKirimBerkasAsuransi_month').val();
						var t401TglJamKirimBerkasAsuransiYear = $('#search_t401TglJamKirimBerkasAsuransi_year').val();
						
						if(t401TglJamKirimBerkasAsuransi){
							aoData.push(
									{"name": 'sCriteria_t401TglJamKirimBerkasAsuransi', "value": "date.struct"},
									{"name": 'sCriteria_t401TglJamKirimBerkasAsuransi_dp', "value": t401TglJamKirimBerkasAsuransi},
									{"name": 'sCriteria_t401TglJamKirimBerkasAsuransi_day', "value": t401TglJamKirimBerkasAsuransiDay},
									{"name": 'sCriteria_t401TglJamKirimBerkasAsuransi_month', "value": t401TglJamKirimBerkasAsuransiMonth},
									{"name": 'sCriteria_t401TglJamKirimBerkasAsuransi_year', "value": t401TglJamKirimBerkasAsuransiYear}
							);
						}

						var t401TglJamJanjiPenyerahan = $('#search_t401TglJamJanjiPenyerahan').val();
						var t401TglJamJanjiPenyerahanDay = $('#search_t401TglJamJanjiPenyerahan_day').val();
						var t401TglJamJanjiPenyerahanMonth = $('#search_t401TglJamJanjiPenyerahan_month').val();
						var t401TglJamJanjiPenyerahanYear = $('#search_t401TglJamJanjiPenyerahan_year').val();
						
						if(t401TglJamJanjiPenyerahan){
							aoData.push(
									{"name": 'sCriteria_t401TglJamJanjiPenyerahan', "value": "date.struct"},
									{"name": 'sCriteria_t401TglJamJanjiPenyerahan_dp', "value": t401TglJamJanjiPenyerahan},
									{"name": 'sCriteria_t401TglJamJanjiPenyerahan_day', "value": t401TglJamJanjiPenyerahanDay},
									{"name": 'sCriteria_t401TglJamJanjiPenyerahan_month', "value": t401TglJamJanjiPenyerahanMonth},
									{"name": 'sCriteria_t401TglJamJanjiPenyerahan_year', "value": t401TglJamJanjiPenyerahanYear}
							);
						}

						var t401TglJamRencana = $('#search_t401TglJamRencana').val();
						var t401TglJamRencanaDay = $('#search_t401TglJamRencana_day').val();
						var t401TglJamRencanaMonth = $('#search_t401TglJamRencana_month').val();
						var t401TglJamRencanaYear = $('#search_t401TglJamRencana_year').val();
						
						if(t401TglJamRencana){
							aoData.push(
									{"name": 'sCriteria_t401TglJamRencana', "value": "date.struct"},
									{"name": 'sCriteria_t401TglJamRencana_dp', "value": t401TglJamRencana},
									{"name": 'sCriteria_t401TglJamRencana_day', "value": t401TglJamRencanaDay},
									{"name": 'sCriteria_t401TglJamRencana_month', "value": t401TglJamRencanaMonth},
									{"name": 'sCriteria_t401TglJamRencana_year', "value": t401TglJamRencanaYear}
							);
						}

						var t401TglJamJanjiBayarDP = $('#search_t401TglJamJanjiBayarDP').val();
						var t401TglJamJanjiBayarDPDay = $('#search_t401TglJamJanjiBayarDP_day').val();
						var t401TglJamJanjiBayarDPMonth = $('#search_t401TglJamJanjiBayarDP_month').val();
						var t401TglJamJanjiBayarDPYear = $('#search_t401TglJamJanjiBayarDP_year').val();
						
						if(t401TglJamJanjiBayarDP){
							aoData.push(
									{"name": 'sCriteria_t401TglJamJanjiBayarDP', "value": "date.struct"},
									{"name": 'sCriteria_t401TglJamJanjiBayarDP_dp', "value": t401TglJamJanjiBayarDP},
									{"name": 'sCriteria_t401TglJamJanjiBayarDP_day', "value": t401TglJamJanjiBayarDPDay},
									{"name": 'sCriteria_t401TglJamJanjiBayarDP_month', "value": t401TglJamJanjiBayarDPMonth},
									{"name": 'sCriteria_t401TglJamJanjiBayarDP_year', "value": t401TglJamJanjiBayarDPYear}
							);
						}

						var t401TglJamPenyerahan = $('#search_t401TglJamPenyerahan').val();
						var t401TglJamPenyerahanDay = $('#search_t401TglJamPenyerahan_day').val();
						var t401TglJamPenyerahanMonth = $('#search_t401TglJamPenyerahan_month').val();
						var t401TglJamPenyerahanYear = $('#search_t401TglJamPenyerahan_year').val();
						
						if(t401TglJamPenyerahan){
							aoData.push(
									{"name": 'sCriteria_t401TglJamPenyerahan', "value": "date.struct"},
									{"name": 'sCriteria_t401TglJamPenyerahan_dp', "value": t401TglJamPenyerahan},
									{"name": 'sCriteria_t401TglJamPenyerahan_day', "value": t401TglJamPenyerahanDay},
									{"name": 'sCriteria_t401TglJamPenyerahan_month', "value": t401TglJamPenyerahanMonth},
									{"name": 'sCriteria_t401TglJamPenyerahan_year', "value": t401TglJamPenyerahanYear}
							);
						}
	
						var t401StaOkCancelReSchedule = $('#filter_t401StaOkCancelReSchedule input').val();
						if(t401StaOkCancelReSchedule){
							aoData.push(
									{"name": 'sCriteria_t401StaOkCancelReSchedule', "value": t401StaOkCancelReSchedule}
							);
						}

						var t401TglJamPenyerahanReSchedule = $('#search_t401TglJamPenyerahanReSchedule').val();
						var t401TglJamPenyerahanReScheduleDay = $('#search_t401TglJamPenyerahanReSchedule_day').val();
						var t401TglJamPenyerahanReScheduleMonth = $('#search_t401TglJamPenyerahanReSchedule_month').val();
						var t401TglJamPenyerahanReScheduleYear = $('#search_t401TglJamPenyerahanReSchedule_year').val();
						
						if(t401TglJamPenyerahanReSchedule){
							aoData.push(
									{"name": 'sCriteria_t401TglJamPenyerahanReSchedule', "value": "date.struct"},
									{"name": 'sCriteria_t401TglJamPenyerahanReSchedule_dp', "value": t401TglJamPenyerahanReSchedule},
									{"name": 'sCriteria_t401TglJamPenyerahanReSchedule_day', "value": t401TglJamPenyerahanReScheduleDay},
									{"name": 'sCriteria_t401TglJamPenyerahanReSchedule_month', "value": t401TglJamPenyerahanReScheduleMonth},
									{"name": 'sCriteria_t401TglJamPenyerahanReSchedule_year', "value": t401TglJamPenyerahanReScheduleYear}
							);
						}
	
						var t401AlasanReSchedule = $('#filter_t401AlasanReSchedule input').val();
						if(t401AlasanReSchedule){
							aoData.push(
									{"name": 'sCriteria_t401AlasanReSchedule', "value": t401AlasanReSchedule}
							);
						}
	
						var t401GambarWAC = $('#filter_t401GambarWAC input').val();
						if(t401GambarWAC){
							aoData.push(
									{"name": 'sCriteria_t401GambarWAC', "value": t401GambarWAC}
							);
						}
	
						var t401CatatanWAC = $('#filter_t401CatatanWAC input').val();
						if(t401CatatanWAC){
							aoData.push(
									{"name": 'sCriteria_t401CatatanWAC', "value": t401CatatanWAC}
							);
						}
	
						var t401KmSaatIni = $('#filter_t401KmSaatIni input').val();
						if(t401KmSaatIni){
							aoData.push(
									{"name": 'sCriteria_t401KmSaatIni', "value": t401KmSaatIni}
							);
						}
	
						var t401Bensin = $('#filter_t401Bensin input').val();
						if(t401Bensin){
							aoData.push(
									{"name": 'sCriteria_t401Bensin', "value": t401Bensin}
							);
						}
	
						var t401Catatan = $('#filter_t401Catatan input').val();
						if(t401Catatan){
							aoData.push(
									{"name": 'sCriteria_t401Catatan', "value": t401Catatan}
							);
						}
	
						var tipeKerusakan = $('#filter_tipeKerusakan input').val();
						if(tipeKerusakan){
							aoData.push(
									{"name": 'sCriteria_tipeKerusakan', "value": tipeKerusakan}
							);
						}
	
						var t401StaTPSLine = $('#filter_t401StaTPSLine input').val();
						if(t401StaTPSLine){
							aoData.push(
									{"name": 'sCriteria_t401StaTPSLine', "value": t401StaTPSLine}
							);
						}
	
						var colorMatchingKlasifikasi = $('#filter_colorMatchingKlasifikasi input').val();
						if(colorMatchingKlasifikasi){
							aoData.push(
									{"name": 'sCriteria_colorMatchingKlasifikasi', "value": colorMatchingKlasifikasi}
							);
						}
	
						var colorMatchingJmlPanel = $('#filter_colorMatchingJmlPanel input').val();
						if(colorMatchingJmlPanel){
							aoData.push(
									{"name": 'sCriteria_colorMatchingJmlPanel', "value": colorMatchingJmlPanel}
							);
						}
	
						var t401Proses1 = $('#filter_t401Proses1 input').val();
						if(t401Proses1){
							aoData.push(
									{"name": 'sCriteria_t401Proses1', "value": t401Proses1}
							);
						}
	
						var t401Proses2 = $('#filter_t401Proses2 input').val();
						if(t401Proses2){
							aoData.push(
									{"name": 'sCriteria_t401Proses2', "value": t401Proses2}
							);
						}
	
						var t401Proses3 = $('#filter_t401Proses3 input').val();
						if(t401Proses3){
							aoData.push(
									{"name": 'sCriteria_t401Proses3', "value": t401Proses3}
							);
						}
	
						var t401Proses4 = $('#filter_t401Proses4 input').val();
						if(t401Proses4){
							aoData.push(
									{"name": 'sCriteria_t401Proses4', "value": t401Proses4}
							);
						}
	
						var t401Proses5 = $('#filter_t401Proses5 input').val();
						if(t401Proses5){
							aoData.push(
									{"name": 'sCriteria_t401Proses5', "value": t401Proses5}
							);
						}
	
						var t401TotalProses = $('#filter_t401TotalProses input').val();
						if(t401TotalProses){
							aoData.push(
									{"name": 'sCriteria_t401TotalProses', "value": t401TotalProses}
							);
						}
	
						var t401HP = $('#filter_t401HP input').val();
						if(t401HP){
							aoData.push(
									{"name": 'sCriteria_t401HP', "value": t401HP}
							);
						}
	
						var t401HargaRp = $('#filter_t401HargaRp input').val();
						if(t401HargaRp){
							aoData.push(
									{"name": 'sCriteria_t401HargaRp', "value": t401HargaRp}
							);
						}
	
						var t401DiscRp = $('#filter_t401DiscRp input').val();
						if(t401DiscRp){
							aoData.push(
									{"name": 'sCriteria_t401DiscRp', "value": t401DiscRp}
							);
						}
	
						var t401DiscPersen = $('#filter_t401DiscPersen input').val();
						if(t401DiscPersen){
							aoData.push(
									{"name": 'sCriteria_t401DiscPersen', "value": t401DiscPersen}
							);
						}
	
						var t401TotalRp = $('#filter_t401TotalRp input').val();
						if(t401TotalRp){
							aoData.push(
									{"name": 'sCriteria_t401TotalRp', "value": t401TotalRp}
							);
						}
	
						var t401DPRp = $('#filter_t401DPRp input').val();
						if(t401DPRp){
							aoData.push(
									{"name": 'sCriteria_t401DPRp', "value": t401DPRp}
							);
						}
	
						var t401TagihanRp = $('#filter_t401TagihanRp input').val();
						if(t401TagihanRp){
							aoData.push(
									{"name": 'sCriteria_t401TagihanRp', "value": t401TagihanRp}
							);
						}
	
						var t401SpDiscRp = $('#filter_t401SpDiscRp input').val();
						if(t401SpDiscRp){
							aoData.push(
									{"name": 'sCriteria_t401SpDiscRp', "value": t401SpDiscRp}
							);
						}
	
						var alasanCancel = $('#filter_alasanCancel input').val();
						if(alasanCancel){
							aoData.push(
									{"name": 'sCriteria_alasanCancel', "value": alasanCancel}
							);
						}
	
						var t401NamaSA = $('#filter_t401NamaSA input').val();
						if(t401NamaSA){
							aoData.push(
									{"name": 'sCriteria_t401NamaSA', "value": t401NamaSA}
							);
						}
	
						var t401staReminderSBE = $('#filter_t401staReminderSBE input').val();
						if(t401staReminderSBE){
							aoData.push(
									{"name": 'sCriteria_t401staReminderSBE', "value": t401staReminderSBE}
							);
						}

						var t401TglJamNotifikasi = $('#search_t401TglJamNotifikasi').val();
						var t401TglJamNotifikasiDay = $('#search_t401TglJamNotifikasi_day').val();
						var t401TglJamNotifikasiMonth = $('#search_t401TglJamNotifikasi_month').val();
						var t401TglJamNotifikasiYear = $('#search_t401TglJamNotifikasi_year').val();
						
						if(t401TglJamNotifikasi){
							aoData.push(
									{"name": 'sCriteria_t401TglJamNotifikasi', "value": "date.struct"},
									{"name": 'sCriteria_t401TglJamNotifikasi_dp', "value": t401TglJamNotifikasi},
									{"name": 'sCriteria_t401TglJamNotifikasi_day', "value": t401TglJamNotifikasiDay},
									{"name": 'sCriteria_t401TglJamNotifikasi_month', "value": t401TglJamNotifikasiMonth},
									{"name": 'sCriteria_t401TglJamNotifikasi_year', "value": t401TglJamNotifikasiYear}
							);
						}
	
						var t401StaPDI = $('#filter_t401StaPDI input').val();
						if(t401StaPDI){
							aoData.push(
									{"name": 'sCriteria_t401StaPDI', "value": t401StaPDI}
							);
						}
	
						var t401StaInvoice = $('#filter_t401StaInvoice input').val();
						if(t401StaInvoice){
							aoData.push(
									{"name": 'sCriteria_t401StaInvoice', "value": t401StaInvoice}
							);
						}
	
						var t401NamaSAPenerimaBPCenter = $('#filter_t401NamaSAPenerimaBPCenter input').val();
						if(t401NamaSAPenerimaBPCenter){
							aoData.push(
									{"name": 'sCriteria_t401NamaSAPenerimaBPCenter', "value": t401NamaSAPenerimaBPCenter}
							);
						}
	
						var t401NoExplanation = $('#filter_t401NoExplanation input').val();
						if(t401NoExplanation){
							aoData.push(
									{"name": 'sCriteria_t401NoExplanation', "value": t401NoExplanation}
							);
						}

						var t401TglJamExplanation = $('#search_t401TglJamExplanation').val();
						var t401TglJamExplanationDay = $('#search_t401TglJamExplanation_day').val();
						var t401TglJamExplanationMonth = $('#search_t401TglJamExplanation_month').val();
						var t401TglJamExplanationYear = $('#search_t401TglJamExplanation_year').val();
						
						if(t401TglJamExplanation){
							aoData.push(
									{"name": 'sCriteria_t401TglJamExplanation', "value": "date.struct"},
									{"name": 'sCriteria_t401TglJamExplanation_dp', "value": t401TglJamExplanation},
									{"name": 'sCriteria_t401TglJamExplanation_day', "value": t401TglJamExplanationDay},
									{"name": 'sCriteria_t401TglJamExplanation_month', "value": t401TglJamExplanationMonth},
									{"name": 'sCriteria_t401TglJamExplanation_year', "value": t401TglJamExplanationYear}
							);
						}

						var t401TglJamExplanationSelesai = $('#search_t401TglJamExplanationSelesai').val();
						var t401TglJamExplanationSelesaiDay = $('#search_t401TglJamExplanationSelesai_day').val();
						var t401TglJamExplanationSelesaiMonth = $('#search_t401TglJamExplanationSelesai_month').val();
						var t401TglJamExplanationSelesaiYear = $('#search_t401TglJamExplanationSelesai_year').val();
						
						if(t401TglJamExplanationSelesai){
							aoData.push(
									{"name": 'sCriteria_t401TglJamExplanationSelesai', "value": "date.struct"},
									{"name": 'sCriteria_t401TglJamExplanationSelesai_dp', "value": t401TglJamExplanationSelesai},
									{"name": 'sCriteria_t401TglJamExplanationSelesai_day', "value": t401TglJamExplanationSelesaiDay},
									{"name": 'sCriteria_t401TglJamExplanationSelesai_month', "value": t401TglJamExplanationSelesaiMonth},
									{"name": 'sCriteria_t401TglJamExplanationSelesai_year', "value": t401TglJamExplanationSelesaiYear}
							);
						}
	
						var t401NamaSAExplaination = $('#filter_t401NamaSAExplaination input').val();
						if(t401NamaSAExplaination){
							aoData.push(
									{"name": 'sCriteria_t401NamaSAExplaination', "value": t401NamaSAExplaination}
							);
						}
	
						var stall = $('#filter_stall input').val();
						if(stall){
							aoData.push(
									{"name": 'sCriteria_stall', "value": stall}
							);
						}
	
						var t401Area = $('#filter_t401Area input').val();
						if(t401Area){
							aoData.push(
									{"name": 'sCriteria_t401Area', "value": t401Area}
							);
						}
	
						var repairDifficulty = $('#filter_repairDifficulty input').val();
						if(repairDifficulty){
							aoData.push(
									{"name": 'sCriteria_repairDifficulty', "value": repairDifficulty}
							);
						}
	
						var bumperPaintngTimeCompanyDealer = $('#filter_bumperPaintngTimeCompanyDealer input').val();
						if(bumperPaintngTimeCompanyDealer){
							aoData.push(
									{"name": 'sCriteria_bumperPaintngTimeCompanyDealer', "value": bumperPaintngTimeCompanyDealer}
							);
						}
	
						var bumperPaintngTimeTglBerlaku = $('#filter_bumperPaintngTimeTglBerlaku input').val();
						if(bumperPaintngTimeTglBerlaku){
							aoData.push(
									{"name": 'sCriteria_bumperPaintngTimeTglBerlaku', "value": bumperPaintngTimeTglBerlaku}
							);
						}
	
						var bumperPaintngTimeMasterPanel = $('#filter_bumperPaintngTimeMasterPanel input').val();
						if(bumperPaintngTimeMasterPanel){
							aoData.push(
									{"name": 'sCriteria_bumperPaintngTimeMasterPanel', "value": bumperPaintngTimeMasterPanel}
							);
						}
	
						var t401xKet = $('#filter_t401xKet input').val();
						if(t401xKet){
							aoData.push(
									{"name": 'sCriteria_t401xKet', "value": t401xKet}
							);
						}
	
						var t401xNamaUser = $('#filter_t401xNamaUser input').val();
						if(t401xNamaUser){
							aoData.push(
									{"name": 'sCriteria_t401xNamaUser', "value": t401xNamaUser}
							);
						}
	
						var t401xDivisi = $('#filter_t401xDivisi input').val();
						if(t401xDivisi){
							aoData.push(
									{"name": 'sCriteria_t401xDivisi', "value": t401xDivisi}
							);
						}
	
						var jpb = $('#filter_jpb input').val();
						if(jpb){
							aoData.push(
									{"name": 'sCriteria_jpb', "value": jpb}
							);
						}
	
						var t401StaDel = $('#filter_t401StaDel input').val();
						if(t401StaDel){
							aoData.push(
									{"name": 'sCriteria_t401StaDel', "value": t401StaDel}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
