<%@ page import="com.kombos.maintable.TipeKerusakan; com.kombos.reception.Reception" %>
<%@ page import="com.kombos.administrasi.KategoriJob"%>
<%@ page import="com.kombos.administrasi.CompanyDealer"%>
<%@ page import="com.kombos.administrasi.Operation" %>

<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'reception.label', default: 'Reception')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
        var alamatPemilik = "";
        var telpPemilik = "";
        var mobilPemilik = "";
        var isCharOnly;
        var isNumberKey;
        var dataCustomer;
		var savePartApp;
		var gatePass;
		var idVincode = '-1';
		var idJobTempPart='-1';
		var selectAllCheckbox;
        var unselectAllCheckbox;
		var openRequestPart;
		var openEstimasi;
		var showAppSummary;
		var showPrediagnose;
		var staValidate = 0;
		var openCustomer;
		var nilaiSpk = -1;
		var showApprovalSPK;
		var staNewEdit = "";
		var isValidated = false;
		var showApprovalPiutang;
        var countJpb = 0;
        var doDetail;
        var showAproval;
        var cekCustomerIn;
		$(function(){
		    cekCustomerIn = function(){
                var noDepan = $('#kodeKota').val();
                var noTengah = $('#nomorTengah').val();
                var noBelakang = $('#nomorBelakang').val();
                var dataUrl = '${request.contextPath}/reception/cekCustomerIn';
                $.ajax({
                    url: dataUrl,
                    type: "POST",
                    data: {kodeKotaNoPol : noDepan,noPolTengah : noTengah, noPolBelakang : noBelakang},
                    success: function(data) {
                        if(data=="ada"){
                            doDetail();
                        }else{
                            alert("Belum Customer In");
                        }
                    }
                 });
		    }
            doDetail = function(){
                var noDepan = $('#kodeKota').val();
                var noTengah = $('#nomorTengah').val();
                var noBelakang = $('#nomorBelakang').val();
                var dataUrl = '${request.contextPath}/reception/'+staNewEdit
                $.ajax({
                    url: dataUrl,
                    type: "POST",
                    data: {kodeKotaNoPol : noDepan,noPolTengah : noTengah, noPolBelakang : noBelakang,idReception : $('#receptionId').val()},
                    success: function(data) {
                        var kosong = false;
                        if(data[0].hasil=="ada"){
                            $('#lblTgglWO').show();
                            $('#lblNoWO').show();
                            if(staNewEdit=="getReception"){
                                initBeforeSave();
                                initAfterSave();
                                initButtonPrintWoBpGr(data[0].idReception);
                            }else{
                                isValidated = true;
                            }
                            staValidate=1;
                            document.getElementById('lblfa').innerHTML = data[0].fa
                            document.getElementById('lbltpss').innerHTML = data[0].tpss
                            document.getElementById('lblfu').innerHTML = data[0].fu
                            document.getElementById('lblpks').innerHTML = data[0].pks
                            document.getElementById('lblpemakai').innerHTML = data[0].pemakai
                            document.getElementById('lbldriver').innerHTML = data[0].driver
                            document.getElementById('lblmobil').innerHTML = data[0].mobil
                            document.getElementById('lblbooking').innerHTML = data[0].staBooking
                            document.getElementById('lbltglbookingfee').innerHTML = data[0].tanggalBayar
                            document.getElementById('lbltgljanjidatang').innerHTML = data[0].tanggalDatang
                            document.getElementById('lbltgljanjiservice').innerHTML = data[0].tanggalMulai
                            document.getElementById('lbltgldelivery').innerHTML = data[0].tanggalDelivery
                            $('#txtJobSuggest').val(data[0].jobSuggest);
                            alamatPemilik = data[0].alamat;
                            telpPemilik = data[0].telp;
                            mobilPemilik = data[0].mobil;
                            countJpb = data[0].jpb
                            if(data[0].idReception){
                                $('#receptionId').val(data[0].idReception);
        //                                    initAll(data[0].idReception);
                            }
                            if(data[0].tgglReception){
                                $('#receptionTggl').val(data[0].tgglReception);
                                $('#lblTgglWO').text(data[0].tgglReception);
                            }
                            if(data[0].noWO){
                                $('#receptionNoWO').val(data[0].noWO);
                                $('#lblNoWO').text( data[0].noWO);
                            }
                            if(data[0].staJDPower=="0"){
                                $('#lbljdpower').show();
                            }
                            if(data[0].staTwc && data[0].staTwc=="1"){
                                $('#staTwc').attr('checked',true)
                            }
                            if(data[0].kmSekarang ){
                                $('#i_km').val(data[0].kmSekarang)
                            }
                            if(data[0].keluhan && data[0].keluhan.length>0 ){
                                var keluhan = data[0].keluhan;
                                countKeluhan = 1;
                                for(var i = 0;i< keluhan.length ; i++){
                                    var yaTidak = (keluhan[i][1]=='1' ? 'Ya' : 'Tidak');
                                    var KP = (keluhan[i][2]);
                                    var newRow = "<tr>" +
                                            "<td><input id='countKeluhan"+countKeluhan+"' type='hidden' value='"+countKeluhan+"'/><input id='keluhan"+countKeluhan+"' type='hidden' value='"+keluhan[i][0]+"'/><input id='staDiagnose"+countKeluhan+"' type='hidden' value='"+keluhan[i][1]+"'/><input id='staKP"+countKeluhan+"' type='hidden' value='"+keluhan[i][2]+"'/>" +
                                            "<input type='checkbox' class='idKeluhan' />"+countKeluhan+"</td>" +
                                            "<td>"+keluhan[i][0]+"</td>" +
                                            "<td>"+yaTidak+"</td>" +
                                            "<td>"+KP+"</td>" +
                                           "</tr>";
			                        $('#tableKeluhan > tbody:last').append(newRow);
			                        countKeluhan++;
                                }
                                $('#i_km').val(data[0].kmSekarang)
                            }
                            if(data[0].kategori ){
                                $('#kategoriJob').val(data[0].kategori)
                            }
                            if(data[0].statusWarranty ){
                                $('#staWarranty').val(data[0].statusWarranty)
                            }
//                            if(data[0].tipeKerusakanBP ){
//                                $('#tipeKerusakanBP').val(data[0].tipeKerusakanBP)
//                            }
                            if(data[0].isBpGr){
                                $("input[name=jenisApp][value="+data[0].isBpGr+"]").attr('checked', 'checked');
                                if(data[0].isBpGr=="BP"){
                                    $('#SpkBp').show();
                                    $('#TipeBp').show();
                                    if(data[0].noSpk){
                                        $("#noSpk").val(data[0].noSpk);
                                    }
                                    if(data[0].tipeRusakBP){
                                        $("#tipeKerusakanBP").val(data[0].tipeRusakBP)
                                    }
                                }
                            }
                            if(data[0].staAsuransi!="N"){
                                var staTemp=data[0].staAsuransi
                                if(staTemp.substring(staTemp.indexOf("-")+1,staTemp.length)=="1"){
                                    $('#btnAsuransi').show();
                                    $('#save_reception').attr("disabled",true);
                                }
                            }
                            if(data[0].staPiutang && data[0].staPiutang=="PY"){
                                $("#notifPiutang").show();
                                if(data[0].staApp && data[0].staApp!="${com.kombos.parts.StatusApproval.APPROVED}"){
                                    $("#lblstaapproved").show()
                                    $("#approvalPiutang").hide();
                                }else{
                                    if(data[0].staApp && data[0].staApp!="${com.kombos.parts.StatusApproval.REJECTED}"){
                                        $("#lblstarejected").text("REJECTED");
                                        $("#lblstarejected").show();
                                    }else if(data[0].staApp && data[0].staApp!="${com.kombos.parts.StatusApproval.WAIT_FOR_APPROVAL}"){
                                        $("#lblstarejected").text("WAIT FOR APPROVAL");
                                        $("#lblstarejected").show();
                                    }
                                    $('#save_reception').attr("disabled",true);
                                }
                            }
                            idVincode=data[0].idCV;
                            nilaiSpk=data[0].spk;
                            reloadhistoryServiceTable();
                            reloadjobnPartsTable();
                            reloadComplaintHistoryTable();
                        }else if(data[0].hasil=="replaced"){
                            kosong = true;
                            alert('Nomor polisi sudah tidak berlaku');
                        }else if(data[0].hasil=="nothing"){
                            kosong = true;
                            alert('Data dengan nomor polisi tersebut tidak ditemukan');
                        }else if(data[0].hasil=="noreception"){
                            kosong = true;
                            alert('Belum ada data reception untuk nomor polisi tersebut');
                        }
                        else if(data[0].hasil=="closed"){
                            kosong = true;
                            alert('Sudah ada data invoice untuk nomor wo tersebut. Detail data bisa di lihat di menu edit job and parts');
                        }
                        if(kosong){
                            idVincode='-1';
                            $('#lblfooternominal').text('');
                            $('#lblfooterrate').text('');
                            $('#divSpk').hide();
                            $('#receptionId').val('${receptionInstance?.id}');
                            $('#receptionTggl').val('${receptionInstance?.dateCreated ? receptionInstance?.dateCreated.format("dd MMMM yyyy") : new java.util.Date().format("dd MMMM yyyy")}');
                            $('#receptionNoWO').val('${receptionInstance?.t401NoWO}');
                            document.getElementById('lblTgglWO').innerHTML=$('#receptionTggl').val();
                            document.getElementById('lblNoWO').innerHTML = $('#receptionNoWO').val()
                            document.getElementById('lblfa').innerHTML = ""
                            document.getElementById('lbltpss').innerHTML = ""
                            document.getElementById('lblfu').innerHTML = ""
                            document.getElementById('lblpks').innerHTML = ""
                            document.getElementById('lblpemakai').innerHTML = ""
                            document.getElementById('lbldriver').innerHTML = ""
                            document.getElementById('lblmobil').innerHTML = ""
                            document.getElementById('lblbooking').innerHTML = ""
                            document.getElementById('lbltglbookingfee').innerHTML = ""
                            document.getElementById('lbltgljanjidatang').innerHTML = ""
                            document.getElementById('lbltgljanjiservice').innerHTML = ""
                            document.getElementById('lbltgldelivery').innerHTML = ""
                            $('#i_km').val('');
                            $('#kategoriJob').val('');
                            $('#staWarranty').val('');
                            $('#tipeKerusakanBP').val('');
                            $('#lbljdpower').hide();
                            reloadhistoryServiceTable();
                            reloadComplaintHistoryTable();
                            reloadjobnPartsTable();
                        }
                    },
                    error: function (data, status, e){
                        alert(e);
                    }
                });
            }
		    <g:if test="${vehicle?.t183NoPolTengah}">
                $("#lblAksiReception").text("New Reception");
                staNewEdit = "getNewReception";
                $('#new_reception').hide();
                $('#edit_reception').hide();
                $('#reset').show();
                initBeforeSave();
                doDetail();
		    </g:if>

			savePartApp = function() {
                var partparams = {};
                partparams['idReception'] =  $('#receptionId').val();
                partparams['idJob'] =  idJobTempPart;

		        var checkPart =[];
                var id = 1;
                $("#inputPart_datatables tbody .row-select").each(function() {
                    if(this.checked){
                        var nRow = $(this).parents('tr')[0];
                        var aData = inputPartTable.fnGetData(nRow);
                        partparams['good' + id] = aData['id'];
                        partparams['prosesBP' + id] = $('#prosesBP' + aData['id']).val();
                        partparams['qty' + id] = $('#qty' + aData['id']).val();
                        checkPart.push(id);
                       id++;
                    }
                });

                partparams['countRowPart'] = id;
                $.ajax({url: '${request.contextPath}/reception/savePartInput',
					type: "POST",
					data: partparams,
					success: function(data) {
				        toastr.success('Data parts berhasil ditambahkan');
				        $('#appointmentInputPartModal').modal('hide');
				        reloadjobnPartsTable();
					},
					complete : function (req, err) {
						$('#spinner').fadeOut();
						loading = false;
					}
				});
		    }

            var nomorBelakang = $('#nomorBelakang');
            nomorBelakang.bind('keypress', inputNoPolisi).keyup(function() {
               var val = $(this).val()
               $(this).val(val.toUpperCase())
            });
            function inputNoPolisi(e){
                var code = (e.keyCode ? e.keyCode : e.which);
            }


            isCharOnly = function(e) {
                e = e || event;
                return /[a-zA-Z-]/i.test(
                        String.fromCharCode(e.charCode || e.keyCode)
                ) || !e.charCode && e.keyCode  < 48;
            }

            isNumberKey = function(evt){
                var charCode = (evt.which) ? evt.which : evt.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57)){
                    return false;
                }
                else{
                    return true;
                }
            }

            $(".charonly").bind('keypress',isCharOnly);

            $(".numberonly").bind('keypress',isNumberKey);

            jpbGr = function(){
                var kodeKota=$('#kodeKota').val();
                var noPolTengah=$('#nomorTengah').val();
                var noPolBelakang=$('#nomorBelakang').val();
                var noWo = $('#lblNoWO').text();
                window.location.replace('#/inputJPBGR');
                $.ajax({
                        url:'${request.contextPath}/JPBGRInput/list?staReception=1&noWo='+noWo,
                         type:"GET",dataType: "html",
                         complete : function (req, err) {
                            $('#main-content').html(req.responseText);
                            $('#spinner').fadeOut();
                            loading = false;
                         }
                    });
            }

            jpbBp = function(){
                var kodeKota=$('#kodeKota').val();
                var noPolTengah=$('#nomorTengah').val();
                var noPolBelakang=$('#nomorBelakang').val();
                var noWo = $('#lblNoWO').text();
                window.location.replace('#/InputJpBBp');
                $.ajax({
                        url:'${request.contextPath}/JPBBPInput/list?staReception=1&noWo='+noWo,
                         type:"GET",dataType: "html",
                         complete : function (req, err) {
                            $('#main-content').html(req.responseText);
                            $('#spinner').fadeOut();
                            loading = false;
                         }
                    });
            }

            dataCustomer = function(){
                var kodeKota=$('#kodeKota').val();
                var noPolTengah=$('#nomorTengah').val();
                var noPolBelakang=$('#nomorBelakang').val();

                if(kodeKota=="" || kodeKota==null || noPolBelakang=="" || noPolBelakang==null || noPolTengah=="" || noPolTengah==null){
                    alert('Anda belum memasukan nomor polisi secara lengkap')
                }else{
                    $('#spinner').fadeIn(1);
                    var notNull = false;
                    var idKirim = []
                    $.ajax({
                        url:'${request.contextPath}/reception/findNoPol',
                        type: "POST",
                        data: { kode: kodeKota , tengah : noPolTengah , belakang : noPolBelakang },
                        success : function(data){
                            if(data.length>2){
                                notNull = true;
                                idKirim.push(data[data.length-1].id)
                                idKirim.push(data[0].id)
                            }else{
                                alert('\t\t\tData customer untuk nomor polisi tersebut belum ada. \nSilahkan isi data customer di menu Customer Profile => Master Customer => Customer');
                            }
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            alert('Internal server error');
                        },
                        complete:function(XMLHttpRequest,textStatus){
                            openCustomer(notNull,idKirim);
                        }
                    });
                }
            }

            openCustomer = function(notNull,idKirim){
                if(notNull==true){
                    window.location.href='#/customer'
                    $('#spinner').fadeIn(1);
                    $.ajax({
                        url: '${request.contextPath}/customer',
                        data: { idCustomer: idKirim[0] , idVehicle : idKirim[1] },
                        type: "POST",dataType:"html",
                        complete : function (req, err) {
                            $('#main-content').html(req.responseText);
                            $('#spinner').fadeOut();
                        }
                    });
                }
            }

			gatePass = function(){
				window.location.replace('#/gatePassT800');
				$('#spinner').fadeIn(1);
				$.ajax({
					url: '${request.contextPath}/gatePassT800',
					/*
					data: {
						idCustomer: data[data.length-1].id ,
						idVehicle : data[0].id
						},
					*/
					type: "GET",dataType:"html",
					complete : function (req, err) {
						$('#main-content').html(req.responseText);
						$('#spinner').fadeOut();
						shrinkTableLayout('gatePassT800');
						<g:remoteFunction controller="gatePassT800" action="create"
							onLoading="jQuery('#spinner').fadeIn(1);"
							onSuccess="loadFormInstance('gatePassT800', data, textStatus);"
							onComplete="jQuery('#spinner').fadeOut();" />
					}
				});
            }

			selectAllCheckbox = function(className){
				$('.'+className).prop('checked', true);
			}

			unselectAllCheckbox = function(className){
				$('.'+className).prop('checked', false);
			}

			openRequestPart = function(){
			        $("#requestPartContent").empty();
                    $.ajax({type:'POST',
                        url:'${request.contextPath}/reception/requestPart',
                        success:function(data,textStatus){
                                $("#requestPartContent").html(data);
                                $("#receptionRequestPartModal").modal({
                                    "backdrop" : "static",
                                    "keyboard" : true,
                                    "show" : true
                                }).css({'width': '1200px','margin-left': function () {return -($(this).width() / 2);}});

                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
            }
            //darisini
            $("#estimasiModal").on("show", function() {
                $("#estimasiModal .btn-primary").on("click", function(e) {
                    $("#estimasiModal").modal('hide');
                });
            });

            openEstimasi = function(){
                    var noWosss = $('#lblNoWO').text();
                    var kodeKota=$('#kodeKota').val();
                    var noPolTengah=$('#nomorTengah').val();
                    var noPolBelakang=$('#nomorBelakang').val();

                    if(kodeKota=="" || kodeKota==null || noPolBelakang=="" || noPolBelakang==null || noPolTengah=="" || noPolTengah==null){
                        alert('Anda belum memasukan nomor polisi secara lengkap')
                    }else{
                        $("#estimasiContent").empty();
                        $.ajax({type:'POST',
                            data: { kode: kodeKota , tengah : noPolTengah , belakang : noPolBelakang, noWo : noWosss},
                            url:'${request.contextPath}/reception/showEstimasi',
                            success:function(data,textStatus){
                                    $("#estimasiContent").html(data);
                                    $("#estimasiModal").modal({
                                        "backdrop" : "static",
                                        "keyboard" : true,
                                        "show" : true
                                    }).css({'width': '1200px','margin-left': function () {return -($(this).width() / 2);}});

                            },
                            error:function(XMLHttpRequest,textStatus,errorThrown){},
                            complete:function(XMLHttpRequest,textStatus){
                                $('#spinner').fadeOut();
                            }
                        });
                    }
            }

            showPrediagnose = function(){
              var km = $('#i_km').val();
              var kodeKota=$('#kodeKota option:selected').text();
              var noPolTengah=$('#nomorTengah').val();
              var noPolBelakang=$('#nomorBelakang').val();

              $('#lblnopoldiagnose').text(kodeKota+" "+ noPolTengah +" "+ noPolBelakang);
              $('#lblkmdiagnose').text(km);
              $('#lblmobildiagnose').text(mobilPemilik)
              $('#keluhanCust').text($('#inputKeluhan').val())
              openDialog('dialog-reception-prediagnose');
            }

            showAproval = function() {
                var noWo = $('#lblNoWO').text();
                $('#noWoApproval').val(noWo);
                openDialog('dialog-reception-permohonan-approval');
            }

            showAppSummary = function() {
                $("#summaryNamaCustomer").text("");
                $("#summaryAlamat").text("");
                $("#summaryTelepon").text("");
                $("#summaryMobil").text("");
                $("#summaryNamaCustomer").text($("#lblpemakai").text());
                $("#summaryAlamat").text(alamatPemilik);
                $("#summaryTelepon").text(telpPemilik);
                $("#summaryMobil").text(mobilPemilik);
                $("#tglBayarBooking").text($("#lbltglbookingfee").text());
                $("#tgljanjiDatang").text($("#lbltgljanjidatang").text());
                var noWo = $('#lblNoWO').text();
                $('#noWo').val(noWo);
                initSummary(noWo);
                openDialog('dialog-reception-summary');
             }

            showApprovalSPK = function() {
                if(staValidate==0){
                    alert('Validasi terlebih dahulu nomor polisi dengan menekan tombol OK');
                    return
                }
                var noWo = $('#lblNoWO').text();
                $('#noWoApprovalSPK').val(noWo);
                openDialog('dialog-reception-permohonan-approval-spk');
            }
            showApprovalPiutang = function() {
                if(staValidate==0){
                    alert('Validasi terlebih dahulu nomor polisi dengan menekan tombol OK');
                    return
                }
                var nama = $('#lblpemakai').text();
                $('#pelangganApprovalPiutang').val(nama);
                openDialog('dialog-reception-permohonan-approval-piutang');
            }
            $('#receptionId').val('${receptionInstance?.id}');
    });

    function newReception(){
        $("#lblAksiReception").text("New Reception");
        $("#lblneedbookingfee").hide();
        staNewEdit = "getNewReception";
        $('#new_reception').hide();
        $('#edit_reception').hide();
        $('#reset').show();
        initBeforeSave();
    }

    function editReception(){
        $("#lblAksiReception").text("Edit Reception");
        staNewEdit = "getReception";
        $('#new_reception').hide();
        $('#edit_reception').hide();
        $('#reset').show();
    }

    function openDialog(divModal){
        var depan = $('#kodeKota').val();
        var tengah=$('#nomorTengah').val();
        var belakang=$('#nomorBelakang').val();
        if(divModal=="dialog-reception-jobkeluhan"){
            var pemilik = $('#lblpemakai').text();
            if(depan=="" || tengah==""){
                alert('Data nomor polisi belum lengkap')
                return
            }
            if(staValidate==0){
                alert('Validasi terlebih dahulu nomor polisi dengan menekan tombol enter pada kolom nomor polisi belakang');
                return
            }
            if(idVincode=="-1"){
                alert('Nomor polisi tidak dikenal');
                return
            }
            openJobKeluhan();
        }else if(divModal=="dialog-reception-parts"){
            var length=0;
            var idJobTemp;
            $("#job_n_parts_datatables tbody .row-select").each(function() {
                if(this.checked){
                    length++;
                    idJobTemp = $(this).next("input:hidden").val();
                }
            });
            if(length==0){
                alert('Pilih satu job yang akan ditambahkan part');
                return;
            }
            if(length>1){
                alert('Anda hanya dapat memilih satu job');
                return;
            }
            idJobTempPart = idJobTemp;
        }
        else if(divModal=="dialog-reception-prediagnose"){
            var km = $('#i_km').val()
	        if(depan=="" || tengah==""){
                alert('Data nomor polisi belum lengkap')
                return
            }
            if(km==""){
                alert('Masukan KM sekarang terlebih dahulu');
                return
            }

        }
        $( "#"+divModal ).modal("show");
    }

    function closeDialog(divModal){
        $( "#"+divModal ).modal("hide");
    }

    function initButtonPreDiagnose(id){
        $('#btnPrintPrediagnose').show();
        $('#printPrediagnose').html("<a id='hrefPrintPreDiagnose' href='${request.contextPath}/reception/printPrediagnosis?id="+id+"' >Print Prediagnose</a>");
		}

	function initButtonPrintWoBpGr(id){
	        $('.btnPrintWo').show();
			$('#printWoBpGr').html("<a id='hrefPrintWoBp' href='${request.contextPath}/reception/printWoBp?id="+id+"' >Print WO BP</a><a id='hrefPrintWoGr' href='${request.contextPath}/reception/printWoGr?id="+id+"' >Print WO GR</a>");
		}

    $('#SpkBp').hide()
    $('#TipeBp').hide()
    function cekJenis(){
            if($('input:radio[name=jenisApp]:nth(0)').is(':checked')){
                cekRadio=0;
                $('#btncekJpbGr').attr("disabled",false)
                $('#btncekJpbBp').attr("disabled",true)
                $('#print_wo_gr').attr("disabled",false)
                $('#print_wo_bp').attr("disabled",true)
                $('#input_estimasi_bp').attr("disabled",true)
                $('#SpkBp').hide()
                $('#TipeBp').hide()

            }else{
                cekRadio=1;
                $('#btncekJpbGr').attr("disabled",true)
                $('#btncekJpbBp').attr("disabled",false)
                $('#print_wo_gr').attr("disabled",true)
                $('#print_wo_bp').attr("disabled",false)
                $('#input_estimasi_bp').attr("disabled",false)
                $('#SpkBp').show()
                $('#TipeBp').show()
            }
    }

	function saveReception(){
	        var jum = $('#lblfooternominal').text();
	        var jumJam = $('#lblfooterrate').text();
	        var kmGanti = $('#i_km').val();
	        var dataSave = 0;
            $("#job_n_parts_datatables tbody .row-select").each(function() {
                dataSave++;
            });

            if(dataSave<=0){
                alert('Belum ada job yang diinputkan');
                return;
            }
            var noDepan = $('#kodeKota').val();
            var noTengah = $('#nomorTengah').val();
            var noBelakang = $('#nomorBelakang').val();
		    var receptionID = $('#receptionId').val();
		    var spk = $("#noSpk").val();
		    var tipeRusakBP = $('#tipeKerusakanBP').val();
		    if($('input:radio[name=jenisApp]:nth(1)').is(':checked') && tipeRusakBP=="" ){
               cekRadio=1;
               alert("Pilih Tingkat Kerusakan BP")
               return;
		    }
                $.ajax({
                    url:'${request.getContextPath()}/reception/saveReception',
                    type: 'POST',
                    data : {idReception : receptionID,kodeKotaNoPol : noDepan,noPolTengah : noTengah, noPolBelakang : noBelakang,jumlah : jum,jumlahJam : jumJam, spk : spk, tipeRusakBP:tipeRusakBP,kmGanti:kmGanti},
                    success:function(data,textStatus){
                        if(data=="ok"){
                            initButtonPrintWoBpGr(receptionID);
                            if(staNewEdit=="getNewReception"){
                                initAfterSave();
                            }
                            toastr.success('Data berhasil disimpan');
                        }else if(data=="nocustomerin"){
                            toastr.error('Tidak ada data customer in untuk nomor polisi tersebut');
                        }else{
                            toastr.error('Nomor Polisi Salah');
                        }
                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){alert('Internal Server Error')},
                    complete:function(XMLHttpRequest,textStatus){
                        $('#spinner').fadeOut();
                    }
                });
    }

    function initBeforeSave(){
        $('#input_job_dan_keluhan').show();
        $('#input_parts').show();
        $('#btnDelete2').show();
        $('#save_reception').show();
    }

    function initAfterSave(){
        $('#request_parts').show();
        $('#input_estimasi_bp').show();
        $('#btncekJpbGr').show();
        $('#btncekJpbBp').show();
        $('#summary_reception').show();
        $('#cancel_reception').show();
        $('#estimasi_reception').show();
        $('#gate_pass').show();
        $('#walk_around_check').show();
        $('#print_pre_diagnose').show();
        $('#print_wo_bp').show();
        $('#print_wo_gr').show();
    }

	function initSummary(id){

			$.ajax({
				url:'${request.getContextPath()}/reception/getDataJobSummary?noWo='+id,
				type: 'POST',
				success: function (res) {

					$('#tableJobSummaryReception').find("tr:gt(0)").remove();
					$('#tableJobSummaryReception').append(res);
				},
				error: function (data, status, e){
					alert(e);
				},
				complete: function(xhr, status) {

				},
				cache: false,
				contentType: false,
				processData: false
			});

			$.ajax({
				url:'${request.getContextPath()}/reception/getDataKeluhanSummary?noWo='+id,
				type: 'POST',
				success: function (res) {
					$('#keluhanSummary').val(res);
				},
				error: function (data, status, e){
					alert(e);
				},
				complete: function(xhr, status) {

				},
				cache: false,
				contentType: false,
				processData: false
			});

			$.ajax({
				url:'${request.getContextPath()}/reception/getDataPembayaran?noWo='+id,
				type: 'POST',
				success: function (res) {
					$('#tablePembayaranSummary').find("tr:gt(0)").remove();
					$('#tablePembayaranSummary').append(res);
				},
				error: function (data, status, e){
					alert(e);
				},
				complete: function(xhr, status) {

				},
				cache: false,
				contentType: false,
				processData: false
			});
		}

	function openWac(){
			$("#wacContent").empty();
            $.ajax({type:'POST',
                url:'${request.contextPath}/reception/receptionWac?id='+$('#receptionId').val(),
                success:function(data,textStatus){
                        $("#wacContent").html(data);
                        $("#wacModal").modal({
                            "backdrop" : "static",
                            "keyboard" : true,
                            "show" : true
                        }).css({'width': '1200px','margin-left': function () {return -($(this).width() / 2);}});
                        reloadTableHasilWac();
                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
		}

	$("#nomorBelakang").bind('keypress', function(e) {
        cekJob = [];
        $('#btnAsuransi').hide();
        var noDepan = $('#kodeKota').val();
        var noTengah = $('#nomorTengah').val();
        var noBelakang = $('#nomorBelakang').val();
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code == 13 && isValidated==false) {
            if(staNewEdit==""){
                alert('Pilih new atau edit')
                return
            }

            e.preventDefault();
            e.stopPropagation();
            cekCustomerIn();

        }
    });


    function doDelete(){
            var jobDelete = [];
            var partsDelete = []
            $("#job_n_parts_datatables tbody .row-select").each(function() {
                if(this.checked){
                    var idDelete = $(this).next("input:hidden").val()
                    if(idDelete.indexOf("#")>-1){
                        idDelete = idDelete.substring(0,idDelete.indexOf("#"))
                        partsDelete.push(idDelete);
                    }else{
                        jobDelete.push(idDelete);
                    }
                }
            });
            if(jobDelete.length<1 && partsDelete.length<1){
                alert('Anda belum memilih data yang akan dihapus.')
                return
            }
            var json = JSON.stringify(jobDelete);
            var json2 = JSON.stringify(partsDelete);
            $.ajax({
                url:'${request.contextPath}/reception/massdelete',
                type: "POST", // Always use POST when deleting data
                data: { jobs: json, parts : json2 },
                complete: function(xhr, status) {
                    reloadjobnPartsTable();
                }
            });

        }

    function doConfirm(){
            $.ajax({
                url:'${request.contextPath}/reception/confirm',
                type: "POST", // Always use POST when deleting data
                data: { id : $('#receptionId').val() },
                success: function(data) {
                    toastr.success('Insurance Cofirmed');
                    $('#btnAsuransi').hide();
                    $('#save_reception').attr("disabled",false);
                },error:function(){
                    alert('failed to confirm');
                }
            });

        }

    function cekSPK(count){
            if(nilaiSpk!=-1){
                $('#lblspk').text('Butuh Approval SPK (Estimasi biaya melebihi batas SPK sebesar Rp. '+nilaiSpk+')');
                $('#divSpk').show()
            }
        }

	function printWoBp(){
	        if(countJpb<=0){
	            alert("Belum ada data jpb untuk wo tersebut");
	            return
	        }
			var href = $('#hrefPrintWoBp').attr('href');
			window.location.href = href;
		}

    function printWoGr(){
	        if(countJpb<=0){
	            alert("Belum ada data jpb untuk wo tersebut");
	            return
	        }
			var href = $('#hrefPrintWoGr').attr('href');
			window.location.href = href;
		}

    function printWAC(){
            window.location = "${request.contextPath}/receptionService/printWAC?id="+$('#receptionId').val();
        }

    function estimasiServis(){
        window.location = "${request.contextPath}/reception/previewEstimasi?id="+$('#receptionId').val();
    }

    function saveChangeData(id){
        $('#spinner').fadeIn(1);
        $.ajax({
                url:'${request.contextPath}/reception/changeValue',
                type: "POST", // Always use POST when deleting data
                data: { id : id , nilai : $("#"+id).val() },
                success: function(data) {
                    $('#spinner').fadeOut();
                },error:function(){
                    alert('Internal Server Error');
                }
            });
    }
    function ubahCustomer(){
           if($("#idCustomerUbah").val()!="" || $("#idCustomerUbah").val()!=null){
               $.ajax({
                    url:'${request.contextPath}/reception/ubahCustomer',
                    type: "POST", // Always use POST when deleting data
                    data: { id : $('#receptionId').val(),idCustomer : $("#idCustomerUbah").val() },
                    success: function(data) {
                        toastr.success('Succes');
                        doDetail();
                    },error:function(){
                        alert('failed to confirm');
                    }
                });
           }
    }
    function cekNomorSPK(){
        var noDepan = $('#kodeKota').val();
        var noTengah = $('#nomorTengah').val();
        var noBelakang = $('#nomorBelakang').val();
		var receptionID = $('#receptionId').val();
		var noSpk = $('#noSpk').val();

        $('#spinner').fadeIn(1);
        $.ajax({
                url:'${request.contextPath}/reception/cekNoSpk',
                type: "POST", // Always use POST when deleting data
                data: { idReception : receptionID,kodeKotaNoPol : noDepan,noPolTengah : noTengah, noPolBelakang : noBelakang, noSpk : noSpk },
                success: function(data) {
                    $('#spinner').fadeOut();
                    if(data == "OK"){
                        toastr.success("Spk Masih Berlaku")
                        $('#input_estimasi_bp').show()
                        $('#save_reception').attr("disabled",false);
                    }else{
                        toastr.error("Spk tidak ditemukan atau sudah tidak berlaku")
                    }
                },error:function(){
                    alert('Internal Server Error');
                }
        });
    }

            function doReset(){
                $('#lblAksiReception').text("Reception");
                loadPath('reception');
            }

            function saveHarga(id,dari,tes){
                var harga = $('#'+dari+'-'+id).val();
                var params;
                params = {
                    id: id,
                    harga: tes,
                    dari : dari
                }
                $.post("${request.getContextPath()}/salesQuotation/updateHarga", params, function(data) {
                    if (data){
                        reloadjobnPartsTable();
                    }else{
                        alert("Internal Server Error");
                    }
                });
            }
		</g:javascript>
        <style>
        body .modal-reception {
            width: 1020px;
            margin-left: -510px;
        }

        .datepicker{z-index:1151;}
        </style>
	</head>
	<body>

	<g:render template="../menu/maxLineDisplay"/>

    <div id="dialog-reception-jobkeluhan" class="modal hide fade in modal-reception" style="display: none; ">
        <g:render template="receptionJobKeluhan"/>
    </div>
	
    <div id="dialog-reception-permohonan-approval-spk" class="modal hide fade in modal-reception" style="display: none; ">
        <g:render template="approvalSPK"/>
    </div>

	<div id="dialog-reception-permohonan-approval-piutang" class="modal hide fade in modal-reception" style="display: none; ">
        <g:render template="approvalPiutang"/>
    </div>

	<div id="dialog-reception-searchparts" class="modal hide fade in modal-reception" style="display: none; ">
        <g:render template="receptionSearchParts"/>
    </div>
	
    <div id="dialog-reception-inputjob" class="modal hide fade in modal-reception" style="display: none; ">
        <g:render template="receptionInputJob"/>
    </div>

    <div id="dialog-reception-customer" class="modal hide fade in modal-reception" style="display: none; ">
        <g:render template="receptionCustomer"/>
    </div>
	
	<div id="dialog-reception-prediagnose" class="modal hide fade in modal-reception" style="display: none; ">
        <g:render template="receptionPrediagnose"/>
    </div>
	
	<div id="dialog-reception-parts" class="modal hide fade in modal-reception" style="display: none; ">
        <g:render template="receptionParts"/>
    </div>
	
	<div id="dialog-reception-summary" class="modal hide fade in modal-reception" style="display: none; ">
        <g:render template="receptionSummary"/>
    </div>
    
	<div id="dialog-reception-cancel" class="modal hide fade in modal-reception" style="display: none; ">
        <g:render template="receptionCancel"/>
    </div>
	
	<div id="dialog-reception-permohonan-approval" class="modal hide fade in modal-reception" style="display: none; ">
        <g:render template="receptionPermohonanApproval"/>
    </div>
	
	<div id="dialog-reception-searchjob" class="modal hide fade in modal-reception" style="display: none; ">
        <g:render template="receptionSearchJob"/>
    </div>
	
	<div id="dialog-reception-inputparts" class="modal hide fade in modal-reception" style="display: none; ">
        <g:render template="receptionInputParts"/>
    </div>
	
	<div id="dialog-reception-joborder" class="modal hide fade in modal-reception" style="display: none; ">
        <g:render template="receptionJobOrder"/>
    </div>
	
	<div id="dialog-reception-paketjob" class="modal hide fade in modal-reception" style="display: none; ">
        <g:render template="receptionPaketJob"/>
    </div>

    <g:render template="formPermohonanApprovalCancelBookingFee"/>
    <g:render template="formPermohonanApprovalEditBookingFee"/>

	<div id="receptionRequestPartModal" class="modal hide">
		<div class="modal-dialog" style="width: 1200px;">
			<div class="modal-content" style="width: 1200px;">
				<!-- dialog body -->
				<div class="modal-body" style="max-height: 1200px;">
					<div id="requestPartContent"/>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
					<div class="iu-content"></div>

				</div>
				<!-- dialog buttons -->
				<div class="modal-footer"><button id='closeRequestPart' type="button" class="btn btn-primary" data-dismiss="modal">Close</button></div>
			</div>
		</div>
	</div>

    <div id="CancelEditBookingFeeModal" class="modal hide fade in">
        <div class="modal-dialog" style="width: 100%;">
            <div class="modal-content" style="width: 100%;">
                %{--<!-- dialog body --><button type="button" class="close" data-dismiss="modal">&times;</button>--}%
                <div class="modal-body" style="max-height: 100%;">

                    <div id="cancelEditBookingFeeContent"/>

                    <div class="iu-content"></div>

                </div>
                <!-- dialog buttons -->
                <div class="modal-footer"><button id='closeCancelEditBookingFee' type="button" class="btn btn-primary" data-dismiss="modal">Close</button></div>
            </div>
        </div>
    </div>

    <div id="estimasiModal" class="modal fade">
        <div class="modal-dialog" style="width: 1200px;">
            <div class="modal-content" style="width: 1200px; overflow-y: auto">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 500px;">
                    <div id="estimasiContent"/>
                    <div class="iu-content"></div>
                </div>
            </div>
        </div>
    </div>

    <div id="wacModal" class="modal fade">
        <div class="modal-dialog" style="width: 1200px;">
            <div class="modal-content" style="width: 1200px; overflow-y: auto">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 500px;">
                    <div id="wacContent"/>
                    <div class="iu-content"></div>

                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a href="javascript:void(0);" id="btnPrintWac" onclick="printWAC();" class="btn btn-primary">
                Print WAC
            </a>
            <a href="javascript:void(0);" onclick="closeDialog('wacModal');" class="btn cancel">
                Close
            </a>
        </div>
    </div>

        <div class="navbar box-header no-border">
            <span class="pull-left" id="lblAksiReception">Reception</span>
        </div>
        <div id="appointment" class="box">
            <div class="row-fluid">
                <div class="span6 form-horizontal" id="no_pol_search">
                    <g:field type="button" onclick="newReception();" class="btn" name="new_reception" id="new_reception" value="New" />
                    <g:field type="button" onclick="editReception();" class="btn" name="edit_reception" id="edit_reception" value="Edit" />
                    <g:field type="button" style="display:none" onclick="doReset();" class="btn" name="reset" id="reset" value="Reset" />
                    <div class="control-group"><label class="control-label" for="kodeKota" style="font-size: 20px; height: 50px; text-align: left;line-height: 50px;">Nomor Polisi
                        <span class="required-indicator">*</span>
                        </label>
                        <div class="controls" style="margin-left: 100px;">
                            <g:select style="width: 85px; font-size: 30px; height: 61px; border-radius: 0;" id="kodeKota" name="kodeKota" from="${com.kombos.administrasi.KodeKotaNoPol.createCriteria().list(){eq("staDel","0");order("m116ID")}}" optionValue="m116ID" optionKey="id" required="" value="${vehicle?.kodeKotaNoPol?.id}" class="many-to-one"/>
                            <g:textField class="numberonly" name="nomorTengah" id="nomorTengah" maxlength="5" value="${vehicle?.t183NoPolTengah}" style="width: 95px; font-size: 30px; height: 50px; border-radius: 0;"/>
                            <g:textField class="charonly" name="nomorBelakang" id="nomorBelakang" maxlength="3" value="${vehicle?.t183NoPolBelakang}" style="width: 70px; font-size: 30px; height: 50px; border-top-left-radius: 0; border-bottom-left-radius: 0;"/>
                            <input type="hidden" id="receptionId" name="receptionId" />
							<input type="hidden" id="receptionNoWO" name="receptionNoWO" />
							<input type="hidden" id="receptionTggl" name="receptionTggl" />
						</div>
                    </div>
                    <br/>
                    <div id="notifPiutang" style="display: none" >
                        <span style="font-size: 19px;color: red">Pelanggan masih memiliki utang</span>
                        <span style="font-size: 19px;color: red" id="lblapprovalPiutang"><br/>Butuh Approval untuk melanjutkan</span>
                        <br/>
                        <span style="font-size: 19px;color: greenyellow;display: none" id="lblstaapproved">Approved</span>
                        <span style="font-size: 19px;color: red;display: none" id="lblstarejected"></span>
                        <br/>
                        <g:field type="button" class="btn" name="approvalPiutang" id="approvalPiutang" onclick="showApprovalPiutang();" value="Approval Pelanggan Menunggak" />
                    </div>
                    <span id="lblneedbookingfee" style="color: red;font-size: 14px;display: none">Terdapat Part Wajib DP</span>
                </div>
                <div class="span4 offset2" id="tgl_appointment">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>
                                <table class="table table-bordered table-dark table-hover">
                                    <tbody>
                                    <tr>
                                        <td style="width: 100px">
                                            <span class="property-label">
                                                <label id="lblbooking"></label>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a class="btn cancel" href="javascript:void(0);" onclick="dataCustomer();" >Data Customer</a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td>
                                <table class="table table-bordered table-dark table-hover">
                                    <tbody>
                                    <tr>
                                        <td><span
                                                class="property-label">Tanggal Reception</span></td>
                                        <td><span id="lblTgglWO" style="display: none" class="property-value">
                                            ${receptionInstance?.dateCreated ? receptionInstance?.dateCreated.format("dd MMMM yyyy") : new java.util.Date().format("dd MMMM yyyy") }
                                        </span></td>
                                    </tr>
                                    <tr>
                                        <td><span
                                                class="property-label">No. Reception</span></td>
                                        <td><span id="lblNoWO" style="display: none" class="property-value">
                                            ${receptionInstance?.t401NoWO}
                                        </span></td>
                                    </tr>
                                    <tr>
                                        <td><span
                                                class="property-label">Jenis Pekerjaan</span></td>
                                        <td>
                                            <g:radioGroup name="jenisApp" id="jenisApp" values="['GR','BP']" labels="['GR','BP']" required="" onclick="cekJenis()" >
                                                ${it.radio} ${it.label}
                                            </g:radioGroup>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span
                                                class="property-label">Service Point?</span></td>
                                        <td>
                                            <g:checkBox name="staTwc" id="staTwc" /> Ya
                                        </td>
                                    </tr>
                                    <tr id="SpkBp">
                                        <td><span
                                                class="property-label">Nomor SPK</span></td>
                                        <td>
                                            <g:textField name="noSpk" id="noSpk" value="" required="" size="10" onblur="cekNomorSPK();" />
                                        </td>
                                    </tr>
                                    <tr id="TipeBp">
                                        <td>
                                            <span class="property-label">Tingkat Kerusakan *</span></td>
                                        <td>
                                            <g:select name="tipeKerusakanBP" id="tipeKerusakanBP" from="${TipeKerusakan.createCriteria().list {order("m401NamaTipeKerusakan")}}" optionKey="id" noSelection="['':'Pilih Tingkat Kerusakan BP']"  optionValue="m401NamaTipeKerusakan" />
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row-fluid" style="margin-top: 30px;">
                <div class="span4">
                    <span style="font-size: 30px;color: red;display: none" id="lbljdpower" class="property-value">
                        <b>JD POWER</b>
                    </span>
                <br/>
                <table class="table table-bordered table-dark table-hover">
                <tbody>
                    <tr>
                        <td style="width: 35%"><span
                            class="property-label">FA</span></td>
                        <td style="width: 65%"><span class="property-value">
                            <label id="lblfa"></label>
                        </span></td>
                    </tr>
                    <tr>
                        <td><span
                            class="property-label">TPSS / SCC</span></td>
                        <td><span class="property-value">
                            <label id="lbltpss"></label>
                        </span></td>
                    </tr>
                    <tr>
                        <td><span
                            class="property-label">FOLLOW UP</span></td>
                        <td><span class="property-value">
                            <label id="lblfu"></label>
                        </span></td>
                    </tr>
                    <tr>
                        <td><span
                            class="property-label">PKS</span></td>
                        <td><span class="property-value">
                            <label id="lblpks"></label>
                        </span></td>
                    </tr>
                </tbody>
                </table>
                <table class="table table-bordered table-dark table-hover">
                <tbody>
                    <tr>
                        <td style="width: 35%"><span
                            class="property-label">Nama Pemakai</span></td>
                        <td style="width: 65%"><span class="property-value">
                            <label id="lblpemakai"></label>
                            <a class="pull-right cell-action" href="javascript:void(0);" onclick="openDialog('dialog-reception-customer')">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>
                        </span></td>
                    </tr>
                    <tr>
                        <td><span
                            class="property-label">Nama Driver</span></td>
                        <td><span class="property-value">
                            <label id="lbldriver"></label>
                        </span></td>
                    </tr>
                    <tr>
                        <td><span
                            class="property-label">Mobil</span></td>
                        <td><span class="property-value">
                            <label id="lblmobil"></label>
                        </span></td>
                    </tr>
                </tbody>
                </table>
                </div>
                <div class="span8" id="appointment_table">
                <fieldset class="buttons controls" style="">
                        <g:field type="button" style="display:none;" onclick="openDialog('dialog-reception-jobkeluhan');" class="btn" name="input_job_dan_keluhan" id="input_job_dan_keluhan" value="Input Job & Keluhan" />
                        <g:field type="button" style="display:none;" onclick="openDialog('dialog-reception-parts');" class="btn" name="input_parts" id="input_parts" value="Input Parts" />
                        <input type="button" style="display:none;" value="Delete" id="btnDelete2" class="btn" onclick="doDelete();">
                        <input type="button" style="display:none;" style="display: none" value="Approve Asuransi" id="btnAsuransi" class="btn" onclick="doConfirm();">
                </fieldset>
                    <g:render template="jobnPartsDataTables" />
                    <fieldset class="buttons controls" style="">
                        <input type="button" style="display:none;" value="Request Parts" id="request_parts" class="btn" onclick="openRequestPart();">
                    </fieldset>
                </div>
                <div class="span8 offset4" id="appointment_buttons">
                <fieldset class="buttons controls" style="padding-top: 10px;">
                        <input type="button" style="display:none;" onclick="openEstimasi()" class="btn" name="input_estimasi_bp" id="input_estimasi_bp" value="Input Estimasi [BP]" />
                         <g:field type="button" style="display:none;" class="btn" onclick="jpbGr()" id="btncekJpbGr" name="cekJpbGr" value="Cek JPB GR"/>
                         <g:field type="button" style="display:none;" class="btn" onclick="jpbBp()" id="btncekJpbBp" name="cekJpbBp" value="Cek JPB BP"/>
						<g:field type="button" style="display:none;" onclick="showAppSummary()" class="btn" name="summary_reception" id="summary_reception" value="Summary Reception" />
                        <g:field type="button" style="display:none;" onclick="showAproval()" class="btn" name="cancel_reception" id="cancel_reception" value="Cancel Reception" />
                        <g:field type="button" style="display:none;" onclick="estimasiServis()" class="btn" name="estimasi_reception" id="estimasi_reception" value="Estimasi Service" />
                </fieldset>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span8" id="history_service">
                <span class="table-label">History Service</span>
                <g:render template="historyServiceDataTables" />
                <br/><br/>
                <span class="table-label">History Complaint</span>
                <g:render template="dataTablesComplaintHistory" />
                </div>
                <div class="span4" id="tgl_detail">
                <span>&nbsp;</span>
                <div id="divSpk" style="display: none">
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 80%">
                                <span id="lblspk" style="color: red">
                                </span>
                            </td>
                            <td style="width: 20%">
                                <input type="button" value=". . ." id="btnApprovalSpk" onclick="showApprovalSPK();" class="btn" />
                            </td>
                        </tr>
                    </table>
                </div>
                    <br/><br/>
                <table class="table table-bordered table-dark table-hover">
                <tbody>
                    <tr>
                        <td style="width: 45%"><span
                            class="property-label">Tgl/Jam Bayar Booking Fee</span></td>
                        <td style="width: 55%"><span class="property-value">
                            <label id="lbltglbookingfee"></label>
                        </span></td>
                    </tr>
                    <tr>
                        <td><span
                            class="property-label">Tgl/Jam Janji Datang</span></td>
                        <td><span class="property-value">
                            <label id="lbltgljanjidatang"></label>
                        </span></td>
                    </tr>
                    <tr>
                        <td><span
                            class="property-label">Tgl/Jam Mulai Service</span></td>
                        <td><span class="property-value">
                            <label id="lbltgljanjiservice"></label>
                        </span></td>
                    </tr>
                    <tr>
                        <td><span
                            class="property-label">Tgl/Jam Delivery</span></td>
                        <td><span class="property-value">
                            <label id="lbltgldelivery"></label>
                        </span></td>
                    </tr>
                </tbody>
                </table>
                </div>
            </div>
            <div class="row-fluid" id="buttons">
				<div id="printWoBpGr" style="display:none;"/>
                <fieldset class="buttons controls pull-right" style="padding-top: 10px;">
                        <g:field type="button" style="display:none;" onclick="gatePass();" class="btn" name="gate_pass" id="gate_pass" value="Gate Pass" />
                        <g:field type="button" style="display:none;" onclick="openWac();" class="btn" name="walk_around_check" id="walk_around_check" value="WALK AROUND CHECK [WAC]" />
                        <g:field type="button" style="display:none;" onclick="showPrediagnose()" class="btn" name="print_pre_diagnose" id="print_pre_diagnose" value="Print Pre-Diagnose" />
                        <g:field style="display:none;" type="button" onclick="printWoBp();" class="btn btnPrintWo" name="print_wo_bp" id="print_wo_bp" value="Print WO BP" />
                        <g:field style="display:none;" type="button" onclick="printWoGr();" class="btn btnPrintWo" name="print_wo_gr" id="print_wo_gr" value="Print WO GR" />
                        <g:field type="button" style="display:none;" onclick="saveReception();" class="btn" name="save_reception" id="save_reception" value="Save" />
                </fieldset>
            </div>
        </div>
    </body>
</html>
