<%@ page import="com.kombos.reception.Reception" %>



<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401NoWO', 'error')} required">
	<label class="control-label" for="t401NoWO">
		<g:message code="reception.t401NoWO.label" default="T401 No WO" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401NoWO" maxlength="20" required="" value="${receptionInstance?.t401NoWO}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401TanggalWO', 'error')} required">
	<label class="control-label" for="t401TanggalWO">
		<g:message code="reception.t401TanggalWO.label" default="T401 Tanggal WO" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:datePicker name="t401TanggalWO" precision="day"  value="${receptionInstance?.t401TanggalWO}"  />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401StaReceptionEstimasiSalesQuotation', 'error')} required">
	<label class="control-label" for="t401StaReceptionEstimasiSalesQuotation">
		<g:message code="reception.t401StaReceptionEstimasiSalesQuotation.label" default="T401 Sta Reception Estimasi Sales Quotation" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401StaReceptionEstimasiSalesQuotation" maxlength="1" required="" value="${receptionInstance?.t401StaReceptionEstimasiSalesQuotation}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 'customerIn', 'error')} required">
	<label class="control-label" for="customerIn">
		<g:message code="reception.customerIn.label" default="Customer In" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="customerIn" name="customerIn.id" from="${com.kombos.reception.CustomerIn.list()}" optionKey="id" required="" value="${receptionInstance?.customerIn?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 'customerVehicle', 'error')} required">
	<label class="control-label" for="customerVehicle">
		<g:message code="reception.customerVehicle.label" default="Customer Vehicle" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="customerVehicle" name="customerVehicle.id" from="${com.kombos.customerprofile.CustomerVehicle.list()}" optionKey="id" required="" value="${receptionInstance?.customerVehicle?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 'historyCustomerVehicle', 'error')} required">
	<label class="control-label" for="historyCustomerVehicle">
		<g:message code="reception.historyCustomerVehicle.label" default="History Customer Vehicle" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="historyCustomerVehicle" name="historyCustomerVehicle.id" from="${com.kombos.customerprofile.HistoryCustomerVehicle.list()}" optionKey="id" required="" value="${receptionInstance?.historyCustomerVehicle?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 'operation', 'error')} ">
	<label class="control-label" for="operation">
		<g:message code="reception.operation.label" default="Operation" />
		
	</label>
	<div class="controls">
	<g:select id="operation" name="operation.id" from="${com.kombos.administrasi.Operation.list()}" optionKey="id" value="${receptionInstance?.operation?.id}" class="many-to-one" noSelection="['null': '']"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 'namaManPower', 'error')} ">
	<label class="control-label" for="namaManPower">
		<g:message code="reception.namaManPower.label" default="Nama Man Power" />
		
	</label>
	<div class="controls">
	<g:select id="namaManPower" name="namaManPower.id" from="${com.kombos.administrasi.NamaManPower.list()}" optionKey="id" value="${receptionInstance?.namaManPower?.id}" class="many-to-one" noSelection="['null': '']"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 'customer', 'error')} required">
	<label class="control-label" for="customer">
		<g:message code="reception.customer.label" default="Customer" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="customer" name="customer.id" from="${com.kombos.maintable.Customer.list()}" optionKey="id" required="" value="${receptionInstance?.customer?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 'historyCustomer', 'error')} required">
	<label class="control-label" for="historyCustomer">
		<g:message code="reception.historyCustomer.label" default="History Customer" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="historyCustomer" name="historyCustomer.id" from="${com.kombos.customerprofile.HistoryCustomer.list()}" optionKey="id" required="" value="${receptionInstance?.historyCustomer?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401StaApp', 'error')} required">
	<label class="control-label" for="t401StaApp">
		<g:message code="reception.t401StaApp.label" default="T401 Sta App" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401StaApp" maxlength="1" required="" value="${receptionInstance?.t401StaApp}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401NoAppointment', 'error')} required">
	<label class="control-label" for="t401NoAppointment">
		<g:message code="reception.t401NoAppointment.label" default="T401 No Appointment" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401NoAppointment" maxlength="20" required="" value="${receptionInstance?.t401NoAppointment}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401StaKategoriCustUmumSPKAsuransi', 'error')} required">
	<label class="control-label" for="t401StaKategoriCustUmumSPKAsuransi">
		<g:message code="reception.t401StaKategoriCustUmumSPKAsuransi.label" default="T401 Sta Kategori Cust Umum SPKA suransi" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401StaKategoriCustUmumSPKAsuransi" maxlength="1" required="" value="${receptionInstance?.t401StaKategoriCustUmumSPKAsuransi}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401BawaSPK', 'error')} required">
	<label class="control-label" for="t401BawaSPK">
		<g:message code="reception.t401BawaSPK.label" default="T401 Bawa SPK" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401BawaSPK" maxlength="1" required="" value="${receptionInstance?.t401BawaSPK}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 'sPK', 'error')} required">
	<label class="control-label" for="sPK">
		<g:message code="reception.sPK.label" default="SPK" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="sPK" name="sPK.id" from="${com.kombos.customerprofile.SPK.list()}" optionKey="id" required="" value="${receptionInstance?.sPK?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401BawaAsuransi', 'error')} required">
	<label class="control-label" for="t401BawaAsuransi">
		<g:message code="reception.t401BawaAsuransi.label" default="T401 Bawa Asuransi" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401BawaAsuransi" maxlength="1" required="" value="${receptionInstance?.t401BawaAsuransi}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 'sPkAsuransi', 'error')} required">
	<label class="control-label" for="sPkAsuransi">
		<g:message code="reception.sPkAsuransi.label" default="SP k Asuransi" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="sPkAsuransi" name="sPkAsuransi.id" from="${com.kombos.customerprofile.SPKAsuransi.list()}" optionKey="id" required="" value="${receptionInstance?.sPkAsuransi?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401TglJamCetakWO', 'error')} required">
	<label class="control-label" for="t401TglJamCetakWO">
		<g:message code="reception.t401TglJamCetakWO.label" default="T401 Tgl Jam Cetak WO" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:datePicker name="t401TglJamCetakWO" precision="day"  value="${receptionInstance?.t401TglJamCetakWO}"  />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401StaAmbilWO', 'error')} required">
	<label class="control-label" for="t401StaAmbilWO">
		<g:message code="reception.t401StaAmbilWO.label" default="T401 Sta Ambil WO" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401StaAmbilWO" maxlength="1" required="" value="${receptionInstance?.t401StaAmbilWO}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401TglJamAmbilWO', 'error')} required">
	<label class="control-label" for="t401TglJamAmbilWO">
		<g:message code="reception.t401TglJamAmbilWO.label" default="T401 Tgl Jam Ambil WO" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:datePicker name="t401TglJamAmbilWO" precision="day"  value="${receptionInstance?.t401TglJamAmbilWO}"  />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401PermintaanCust', 'error')} required">
	<label class="control-label" for="t401PermintaanCust">
		<g:message code="reception.t401PermintaanCust.label" default="T401 Permintaan Cust" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401PermintaanCust" maxlength="16" required="" value="${receptionInstance?.t401PermintaanCust}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401StaTinggalMobil', 'error')} required">
	<label class="control-label" for="t401StaTinggalMobil">
		<g:message code="reception.t401StaTinggalMobil.label" default="T401 Sta Tinggal Mobil" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401StaTinggalMobil" maxlength="1" required="" value="${receptionInstance?.t401StaTinggalMobil}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401StaTinggalParts', 'error')} required">
	<label class="control-label" for="t401StaTinggalParts">
		<g:message code="reception.t401StaTinggalParts.label" default="T401 Sta Tinggal Parts" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401StaTinggalParts" maxlength="1" required="" value="${receptionInstance?.t401StaTinggalParts}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401StaCustomerTunggu', 'error')} required">
	<label class="control-label" for="t401StaCustomerTunggu">
		<g:message code="reception.t401StaCustomerTunggu.label" default="T401 Sta Customer Tunggu" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401StaCustomerTunggu" maxlength="1" required="" value="${receptionInstance?.t401StaCustomerTunggu}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401StaObatJalan', 'error')} required">
	<label class="control-label" for="t401StaObatJalan">
		<g:message code="reception.t401StaObatJalan.label" default="T401 Sta Obat Jalan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401StaObatJalan" maxlength="1" required="" value="${receptionInstance?.t401StaObatJalan}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401TglStaObatJalan', 'error')} required">
	<label class="control-label" for="t401TglStaObatJalan">
		<g:message code="reception.t401TglStaObatJalan.label" default="T401 Tgl Sta Obat Jalan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:datePicker name="t401TglStaObatJalan" precision="day"  value="${receptionInstance?.t401TglStaObatJalan}"  />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401StaButuhSPKSebelumProd', 'error')} required">
	<label class="control-label" for="t401StaButuhSPKSebelumProd">
		<g:message code="reception.t401StaButuhSPKSebelumProd.label" default="T401 Sta Butuh SPKS ebelum Prod" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401StaButuhSPKSebelumProd" maxlength="1" required="" value="${receptionInstance?.t401StaButuhSPKSebelumProd}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401TglJamKirimBerkasAsuransi', 'error')} required">
	<label class="control-label" for="t401TglJamKirimBerkasAsuransi">
		<g:message code="reception.t401TglJamKirimBerkasAsuransi.label" default="T401 Tgl Jam Kirim Berkas Asuransi" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:datePicker name="t401TglJamKirimBerkasAsuransi" precision="day"  value="${receptionInstance?.t401TglJamKirimBerkasAsuransi}"  />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401TglJamJanjiPenyerahan', 'error')} required">
	<label class="control-label" for="t401TglJamJanjiPenyerahan">
		<g:message code="reception.t401TglJamJanjiPenyerahan.label" default="T401 Tgl Jam Janji Penyerahan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:datePicker name="t401TglJamJanjiPenyerahan" precision="day"  value="${receptionInstance?.t401TglJamJanjiPenyerahan}"  />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401TglJamRencana', 'error')} required">
	<label class="control-label" for="t401TglJamRencana">
		<g:message code="reception.t401TglJamRencana.label" default="T401 Tgl Jam Rencana" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:datePicker name="t401TglJamRencana" precision="day"  value="${receptionInstance?.t401TglJamRencana}"  />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401TglJamJanjiBayarDP', 'error')} required">
	<label class="control-label" for="t401TglJamJanjiBayarDP">
		<g:message code="reception.t401TglJamJanjiBayarDP.label" default="T401 Tgl Jam Janji Bayar DP" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:datePicker name="t401TglJamJanjiBayarDP" precision="day"  value="${receptionInstance?.t401TglJamJanjiBayarDP}"  />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401TglJamPenyerahan', 'error')} required">
	<label class="control-label" for="t401TglJamPenyerahan">
		<g:message code="reception.t401TglJamPenyerahan.label" default="T401 Tgl Jam Penyerahan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:datePicker name="t401TglJamPenyerahan" precision="day"  value="${receptionInstance?.t401TglJamPenyerahan}"  />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401StaOkCancelReSchedule', 'error')} required">
	<label class="control-label" for="t401StaOkCancelReSchedule">
		<g:message code="reception.t401StaOkCancelReSchedule.label" default="T401 Sta Ok Cancel Re Schedule" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401StaOkCancelReSchedule" maxlength="1" required="" value="${receptionInstance?.t401StaOkCancelReSchedule}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401TglJamPenyerahanReSchedule', 'error')} required">
	<label class="control-label" for="t401TglJamPenyerahanReSchedule">
		<g:message code="reception.t401TglJamPenyerahanReSchedule.label" default="T401 Tgl Jam Penyerahan Re Schedule" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:datePicker name="t401TglJamPenyerahanReSchedule" precision="day"  value="${receptionInstance?.t401TglJamPenyerahanReSchedule}"  />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401AlasanReSchedule', 'error')} required">
	<label class="control-label" for="t401AlasanReSchedule">
		<g:message code="reception.t401AlasanReSchedule.label" default="T401 Alasan Re Schedule" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401AlasanReSchedule" maxlength="50" required="" value="${receptionInstance?.t401AlasanReSchedule}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401GambarWAC', 'error')} required">
	<label class="control-label" for="t401GambarWAC">
		<g:message code="reception.t401GambarWAC.label" default="T401 Gambar WAC" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<input type="file" id="t401GambarWAC" name="t401GambarWAC" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401CatatanWAC', 'error')} required">
	<label class="control-label" for="t401CatatanWAC">
		<g:message code="reception.t401CatatanWAC.label" default="T401 Catatan WAC" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401CatatanWAC" maxlength="16" required="" value="${receptionInstance?.t401CatatanWAC}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401KmSaatIni', 'error')} required">
	<label class="control-label" for="t401KmSaatIni">
		<g:message code="reception.t401KmSaatIni.label" default="T401 Km Saat Ini" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field name="t401KmSaatIni" type="number" value="${receptionInstance.t401KmSaatIni}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401Bensin', 'error')} required">
	<label class="control-label" for="t401Bensin">
		<g:message code="reception.t401Bensin.label" default="T401 Bensin" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field name="t401Bensin" type="number" value="${receptionInstance.t401Bensin}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401Catatan', 'error')} required">
	<label class="control-label" for="t401Catatan">
		<g:message code="reception.t401Catatan.label" default="T401 Catatan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401Catatan" maxlength="50" required="" value="${receptionInstance?.t401Catatan}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 'tipeKerusakan', 'error')} required">
	<label class="control-label" for="tipeKerusakan">
		<g:message code="reception.tipeKerusakan.label" default="Tipe Kerusakan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="tipeKerusakan" name="tipeKerusakan.id" from="${com.kombos.maintable.TipeKerusakan.list()}" optionKey="id" required="" value="${receptionInstance?.tipeKerusakan?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401StaTPSLine', 'error')} required">
	<label class="control-label" for="t401StaTPSLine">
		<g:message code="reception.t401StaTPSLine.label" default="T401 Sta TPSL ine" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401StaTPSLine" maxlength="1" required="" value="${receptionInstance?.t401StaTPSLine}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 'colorMatchingKlasifikasi', 'error')} required">
	<label class="control-label" for="colorMatchingKlasifikasi">
		<g:message code="reception.colorMatchingKlasifikasi.label" default="Color Matching Klasifikasi" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="colorMatchingKlasifikasi" name="colorMatchingKlasifikasi.id" from="${com.kombos.administrasi.ColorMatching.list()}" optionKey="id" required="" value="${receptionInstance?.colorMatchingKlasifikasi?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 'colorMatchingJmlPanel', 'error')} required">
	<label class="control-label" for="colorMatchingJmlPanel">
		<g:message code="reception.colorMatchingJmlPanel.label" default="Color Matching Jml Panel" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="colorMatchingJmlPanel" name="colorMatchingJmlPanel.id" from="${com.kombos.administrasi.ColorMatching.list()}" optionKey="id" required="" value="${receptionInstance?.colorMatchingJmlPanel?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401Proses1', 'error')} required">
	<label class="control-label" for="t401Proses1">
		<g:message code="reception.t401Proses1.label" default="T401 Proses1" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field name="t401Proses1" value="${fieldValue(bean: receptionInstance, field: 't401Proses1')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401Proses2', 'error')} required">
	<label class="control-label" for="t401Proses2">
		<g:message code="reception.t401Proses2.label" default="T401 Proses2" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field name="t401Proses2" value="${fieldValue(bean: receptionInstance, field: 't401Proses2')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401Proses3', 'error')} required">
	<label class="control-label" for="t401Proses3">
		<g:message code="reception.t401Proses3.label" default="T401 Proses3" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field name="t401Proses3" value="${fieldValue(bean: receptionInstance, field: 't401Proses3')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401Proses4', 'error')} required">
	<label class="control-label" for="t401Proses4">
		<g:message code="reception.t401Proses4.label" default="T401 Proses4" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field name="t401Proses4" value="${fieldValue(bean: receptionInstance, field: 't401Proses4')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401Proses5', 'error')} required">
	<label class="control-label" for="t401Proses5">
		<g:message code="reception.t401Proses5.label" default="T401 Proses5" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field name="t401Proses5" value="${fieldValue(bean: receptionInstance, field: 't401Proses5')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401TotalProses', 'error')} required">
	<label class="control-label" for="t401TotalProses">
		<g:message code="reception.t401TotalProses.label" default="T401 Total Proses" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field name="t401TotalProses" value="${fieldValue(bean: receptionInstance, field: 't401TotalProses')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401HP', 'error')} required">
	<label class="control-label" for="t401HP">
		<g:message code="reception.t401HP.label" default="T401 HP" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401HP" maxlength="50" required="" value="${receptionInstance?.t401HP}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401HargaRp', 'error')} required">
	<label class="control-label" for="t401HargaRp">
		<g:message code="reception.t401HargaRp.label" default="T401 Harga Rp" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field name="t401HargaRp" value="${fieldValue(bean: receptionInstance, field: 't401HargaRp')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401DiscRp', 'error')} required">
	<label class="control-label" for="t401DiscRp">
		<g:message code="reception.t401DiscRp.label" default="T401 Disc Rp" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field name="t401DiscRp" value="${fieldValue(bean: receptionInstance, field: 't401DiscRp')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401DiscPersen', 'error')} required">
	<label class="control-label" for="t401DiscPersen">
		<g:message code="reception.t401DiscPersen.label" default="T401 Disc Persen" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field name="t401DiscPersen" value="${fieldValue(bean: receptionInstance, field: 't401DiscPersen')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401TotalRp', 'error')} required">
	<label class="control-label" for="t401TotalRp">
		<g:message code="reception.t401TotalRp.label" default="T401 Total Rp" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field name="t401TotalRp" value="${fieldValue(bean: receptionInstance, field: 't401TotalRp')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401DPRp', 'error')} required">
	<label class="control-label" for="t401DPRp">
		<g:message code="reception.t401DPRp.label" default="T401 DPR p" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field name="t401DPRp" value="${fieldValue(bean: receptionInstance, field: 't401DPRp')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401TagihanRp', 'error')} required">
	<label class="control-label" for="t401TagihanRp">
		<g:message code="reception.t401TagihanRp.label" default="T401 Tagihan Rp" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field name="t401TagihanRp" value="${fieldValue(bean: receptionInstance, field: 't401TagihanRp')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401SpDiscRp', 'error')} required">
	<label class="control-label" for="t401SpDiscRp">
		<g:message code="reception.t401SpDiscRp.label" default="T401 Sp Disc Rp" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field name="t401SpDiscRp" value="${fieldValue(bean: receptionInstance, field: 't401SpDiscRp')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 'alasanCancel', 'error')} required">
	<label class="control-label" for="alasanCancel">
		<g:message code="reception.alasanCancel.label" default="Alasan Cancel" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="alasanCancel" name="alasanCancel.id" from="${com.kombos.maintable.AlasanCancel.list()}" optionKey="id" required="" value="${receptionInstance?.alasanCancel?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401NamaSA', 'error')} required">
	<label class="control-label" for="t401NamaSA">
		<g:message code="reception.t401NamaSA.label" default="T401 Nama SA" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401NamaSA" maxlength="20" required="" value="${receptionInstance?.t401NamaSA}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401staReminderSBE', 'error')} required">
	<label class="control-label" for="t401staReminderSBE">
		<g:message code="reception.t401staReminderSBE.label" default="T401sta Reminder SBE" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401staReminderSBE" maxlength="1" required="" value="${receptionInstance?.t401staReminderSBE}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401TglJamNotifikasi', 'error')} required">
	<label class="control-label" for="t401TglJamNotifikasi">
		<g:message code="reception.t401TglJamNotifikasi.label" default="T401 Tgl Jam Notifikasi" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:datePicker name="t401TglJamNotifikasi" precision="day"  value="${receptionInstance?.t401TglJamNotifikasi}"  />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401StaPDI', 'error')} required">
	<label class="control-label" for="t401StaPDI">
		<g:message code="reception.t401StaPDI.label" default="T401 Sta PDI" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401StaPDI" maxlength="1" required="" value="${receptionInstance?.t401StaPDI}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401StaInvoice', 'error')} required">
	<label class="control-label" for="t401StaInvoice">
		<g:message code="reception.t401StaInvoice.label" default="T401 Sta Invoice" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401StaInvoice" maxlength="1" required="" value="${receptionInstance?.t401StaInvoice}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401NamaSAPenerimaBPCenter', 'error')} required">
	<label class="control-label" for="t401NamaSAPenerimaBPCenter">
		<g:message code="reception.t401NamaSAPenerimaBPCenter.label" default="T401 Nama SAP enerima BPC enter" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401NamaSAPenerimaBPCenter" maxlength="50" required="" value="${receptionInstance?.t401NamaSAPenerimaBPCenter}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401NoExplanation', 'error')} required">
	<label class="control-label" for="t401NoExplanation">
		<g:message code="reception.t401NoExplanation.label" default="T401 No Explanation" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401NoExplanation" maxlength="20" required="" value="${receptionInstance?.t401NoExplanation}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401TglJamExplanation', 'error')} required">
	<label class="control-label" for="t401TglJamExplanation">
		<g:message code="reception.t401TglJamExplanation.label" default="T401 Tgl Jam Explanation" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:datePicker name="t401TglJamExplanation" precision="day"  value="${receptionInstance?.t401TglJamExplanation}"  />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401TglJamExplanationSelesai', 'error')} required">
	<label class="control-label" for="t401TglJamExplanationSelesai">
		<g:message code="reception.t401TglJamExplanationSelesai.label" default="T401 Tgl Jam Explanation Selesai" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:datePicker name="t401TglJamExplanationSelesai" precision="day"  value="${receptionInstance?.t401TglJamExplanationSelesai}"  />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401NamaSAExplaination', 'error')} required">
	<label class="control-label" for="t401NamaSAExplaination">
		<g:message code="reception.t401NamaSAExplaination.label" default="T401 Nama SAE xplaination" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401NamaSAExplaination" maxlength="50" required="" value="${receptionInstance?.t401NamaSAExplaination}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 'stall', 'error')} required">
	<label class="control-label" for="stall">
		<g:message code="reception.stall.label" default="Stall" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="stall" name="stall.id" from="${com.kombos.administrasi.Stall.list()}" optionKey="id" required="" value="${receptionInstance?.stall?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401Area', 'error')} required">
	<label class="control-label" for="t401Area">
		<g:message code="reception.t401Area.label" default="T401 Area" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401Area" maxlength="4" required="" value="${receptionInstance?.t401Area}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 'repairDifficulty', 'error')} required">
	<label class="control-label" for="repairDifficulty">
		<g:message code="reception.repairDifficulty.label" default="Repair Difficulty" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="repairDifficulty" name="repairDifficulty.id" from="${com.kombos.administrasi.RepairDifficulty.list()}" optionKey="id" required="" value="${receptionInstance?.repairDifficulty?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 'bumperPaintngTimeCompanyDealer', 'error')} required">
	<label class="control-label" for="bumperPaintngTimeCompanyDealer">
		<g:message code="reception.bumperPaintngTimeCompanyDealer.label" default="Bumper Paintng Time Company Dealer" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="bumperPaintngTimeCompanyDealer" name="bumperPaintngTimeCompanyDealer.id" from="${com.kombos.administrasi.BumperPaintingTime.list()}" optionKey="id" required="" value="${receptionInstance?.bumperPaintngTimeCompanyDealer?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 'bumperPaintngTimeTglBerlaku', 'error')} required">
	<label class="control-label" for="bumperPaintngTimeTglBerlaku">
		<g:message code="reception.bumperPaintngTimeTglBerlaku.label" default="Bumper Paintng Time Tgl Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="bumperPaintngTimeTglBerlaku" name="bumperPaintngTimeTglBerlaku.id" from="${com.kombos.administrasi.BumperPaintingTime.list()}" optionKey="id" required="" value="${receptionInstance?.bumperPaintngTimeTglBerlaku?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 'bumperPaintngTimeMasterPanel', 'error')} required">
	<label class="control-label" for="bumperPaintngTimeMasterPanel">
		<g:message code="reception.bumperPaintngTimeMasterPanel.label" default="Bumper Paintng Time Master Panel" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="bumperPaintngTimeMasterPanel" name="bumperPaintngTimeMasterPanel.id" from="${com.kombos.administrasi.BumperPaintingTime.list()}" optionKey="id" required="" value="${receptionInstance?.bumperPaintngTimeMasterPanel?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401xKet', 'error')} required">
	<label class="control-label" for="t401xKet">
		<g:message code="reception.t401xKet.label" default="T401x Ket" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401xKet" maxlength="50" required="" value="${receptionInstance?.t401xKet}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401xNamaUser', 'error')} required">
	<label class="control-label" for="t401xNamaUser">
		<g:message code="reception.t401xNamaUser.label" default="T401x Nama User" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401xNamaUser" maxlength="20" required="" value="${receptionInstance?.t401xNamaUser}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 't401xDivisi', 'error')} required">
	<label class="control-label" for="t401xDivisi">
		<g:message code="reception.t401xDivisi.label" default="T401x Divisi" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t401xDivisi" maxlength="50" required="" value="${receptionInstance?.t401xDivisi}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 'jpb', 'error')} ">
	<label class="control-label" for="jpb">
		<g:message code="reception.jpb.label" default="Jpb" />
		
	</label>
	<div class="controls">
	<g:select id="jpb" name="jpb.id" from="${com.kombos.board.JPB.list()}" optionKey="id" value="${receptionInstance?.jpb?.id}" class="many-to-one" noSelection="['null': '']"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 'staDel', 'error')} required">
	<label class="control-label" for="staDel">
		<g:message code="reception.staDel.label" default="T401 Sta Del" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="staDel" maxlength="1" required="" value="${receptionInstance?.staDel}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 'createdBy', 'error')} ">
	<label class="control-label" for="createdBy">
		<g:message code="reception.createdBy.label" default="Created By" />
		
	</label>
	<div class="controls">
	<g:textField name="createdBy" value="${receptionInstance?.createdBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 'updatedBy', 'error')} ">
	<label class="control-label" for="updatedBy">
		<g:message code="reception.updatedBy.label" default="Updated By" />
		
	</label>
	<div class="controls">
	<g:textField name="updatedBy" value="${receptionInstance?.updatedBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: receptionInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="reception.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${receptionInstance?.lastUpdProcess}"/>
	</div>
</div>

