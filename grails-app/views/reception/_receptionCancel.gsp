<div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Alasan Cancel Reception</h3>
</div>
<div class="modal-body">
    <div class="box">
        <table class="table">
            <tbody>
            <tr>
				<td>
					Alasan Cancel
                </td>
                <td>
					<g:select id="alasanCancel" name="alasanCancel" from="${com.kombos.maintable.AlasanCancel.list()}" optionKey="id" required="" class="many-to-one"/>	
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="modal-footer">
    <a href="javascript:openDialog('dialog-reception-permohonan-approval');" class="btn">Yes</a>
	<a href="#" class="btn" data-dismiss="modal">No</a>
	<a href="#" class="btn" data-dismiss="modal">Cancel</a>
</div>