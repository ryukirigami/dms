<%@ page import="com.kombos.administrasi.KegiatanApproval; com.kombos.reception.Reception" %>
<div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Approval - Permohonan Approval</h3>
</div>
<g:javascript>
    $(function(){
        sendApprovalSPK = function(){
            var noWo = $('#noWoApprovalSPK').val();
            var pesan = $('#pesanApprovalSPK').val();
            var kegiatanApproval = $('#kegiatanApprovalSPK').val();
            $.ajax({
                    url:'${request.contextPath}/reception/approvalSPK',
                    type: "POST", // Always use POST when deleting data
                    data : {noWo : noWo,pesan:pesan,kegiatanApproval:kegiatanApproval},
                    success : function(data){
                        toastr.success('<div>Approval Sudah Dikirim</div>');
                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });
        };

    });
</g:javascript>
<div class="modal-body">
    <div class="box">
        <table class="table">
            <tbody>
            <tr>
                <td>
                    Nama Kegiatan *
                </td>
                <td>
                    <g:select id="kegiatanApprovalSPK" name="kegiatanApprovalSPK" from="${com.kombos.administrasi.KegiatanApproval.list()}"  value="${KegiatanApproval?.findByM770KegiatanApproval(KegiatanApproval.SPK_OVER_LIMIT)?.id}" optionKey="id" required="" class="many-to-one"/>
                </td>
            </tr>
            <tr>
                <td>
                    Nomor WO
                </td>
                <td>
                    <g:textField name="noWoApprovalSPK" id="noWoApprovalSPK" readonly="" />
                </td>
            </tr>
            <tr>
                <td>
                    Tanggal dan Jam
                </td>
                <td>
                    ${new Date().format("dd-MM-yyyy HH:mm")}
                </td>
            </tr>
            <tr>
                <td>
                    Pesan
                </td>
                <td>
                    <textarea id="pesanApprovalSPK" name="pesanApprovalSPK" cols="200" rows="3"></textarea>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="modal-footer">
    <a href="#" class="btn" onclick="sendApprovalSPK()" data-dismiss="modal">Send</a>
    <a href="#" class="btn" data-dismiss="modal">Close</a>
</div>