
<%@ page import="com.kombos.parts.Vendor; com.kombos.parts.Invoice" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'invoice.label', default: 'Invoice')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massAction();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/poInvoice/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/poInvoice/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#invoice-form').empty();
    	$('#invoice-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#invoice-table").hasClass("span12")){
   			$("#invoice-table").toggleClass("span12 span5");
        }
        $("#invoice-form").css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
   		if($("#invoice-table").hasClass("span5")){
   			$("#invoice-table").toggleClass("span5 span12");
   		}
        $("#invoice-form").css("display","none");
   	}
   	
   	massAction = function(sta) {
   		var recordsToDelete = [];
		$("#invoice-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/poInvoice/massAction?sta='+sta,
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadInvoiceTable();
    		}
		});

		$('input[name=checkSelect]').attr("checked",false);
   	}

});

    function disableCheck(from,to){
        if($('#'+from+'').prop("checked")){
             $('#'+to+'').attr("disabled",true)
        }else{
             $('#'+to+'').attr("disabled",false)
        }
    }

    function selectAll(){
        if($('input[name=checkSelect]').is(':checked')){
            $(".row-select").attr("checked",true);
        }else {
            $(".row-select").attr("checked",false);
        }
    }

    function clearInput(){
        $("#search_t156Tanggal_start").val("");
        $("#search_t156Tanggal_end").val("");
        $("#staSPLD1").attr("checked",false);
        $("#staSPLD2").attr("checked",false);
        $("#staSPLD3").attr("checked",false);
        $("#staSPLD4").attr("checked",false);
        $("#staSPLD1").attr("disabled",false);
        $("#staSPLD2").attr("disabled",false);
        $("#staSPLD3").attr("disabled",false);
        $("#staSPLD4").attr("disabled",false);
        searchInvoice();
    }
        printPartsReconsile = function(){
           checkPartsReconsile =[];
            $("#invoice-table tbody .row-select").each(function() {
                 var nRow = this.value?this.value:'-'
                    if(nRow!="-"){
                        checkPartsReconsile.push(nRow);
                    }
            });

           var idPartsReconsile =  JSON.stringify(checkPartsReconsile);
           window.location = "${request.contextPath}/poInvoice/printPartsReconsile?idPartsReconsile="+idPartsReconsile;
        }
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			%{--<li><a class="pull-right box-action" href="#"--}%
				%{--style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i--}%
					%{--class="icon-plus"></i>&nbsp;&nbsp;--}%
			%{--</a></li>--}%
			%{--<li><a class="pull-right box-action" href="#"--}%
				%{--style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i--}%
					%{--class="icon-remove"></i>&nbsp;&nbsp;--}%
			%{--</a></li>--}%
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="invoice-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <fieldset>
                <table style="padding-right: 10px">
                    <tr>
                        <td style="width: 130px">
                            <label class="control-label" for="tanggal_invoice">
                                <g:message code="invoice.tanggal.label" default="Tanggal Invoice" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="tanggal_invoice" class="controls">
                                <ba:datePicker id="search_t156Tanggal_start" name="search_t156Tanggal_start" precision="day" format="dd/MM/yyyy"  value="" />
                                &nbsp;-&nbsp;
                                <ba:datePicker id="search_t156Tanggal_end" name="search_t156Tanggal_end" precision="day" format="dd/MM/yyyy"  value="" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label" for="kelengkapanKriteria">
                                <g:message code="invoice.kelengkapanKriteria.label" default="Kelengkapan Kriteria" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="kelengkapanKriteria" class="controls">
                                <table>
                                    <tr>
                                        <td>
                                            <input type="checkbox" onclick="disableCheck('staSPLD1','staSPLD2');" name="staSPLD1" id="staSPLD1" value="1">SPLD&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <input type="checkbox" onclick="disableCheck('staSPLD2','staSPLD1');" name="staSPLD2" id="staSPLD2" value="0">Non SPLD
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="checkbox" onclick="disableCheck('staSPLD3','staSPLD4');" name="staSPLD3" id="staSPLD3" value="1">PO dengan invoice belum lengkap&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <input type="checkbox" onclick="disableCheck('staSPLD4','staSPLD3');" name="staSPLD4" id="staSPLD4" value="0">PO dengan invoice lengkap
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" >
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" >
                            <div class="controls" style="right: 0">
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-primary view" name="view" id="view" onclick="searchInvoice();" >Search</button>
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-cancel clear" name="clear" id="clear" onclick="clearInput();">Clear</button>
                            </div>
                        </td>
                    </tr>
                </table>
            </fieldset>
			<g:render template="dataTables" />
            <fieldset class="buttons controls" style="padding-top: 10px;">
                <button id='printPartsReconsileData' onclick="printPartsReconsile();" type="button" class="btn btn-primary">Print Reconsile</button>
            </fieldset>
		</div>
		<div class="span7" id="invoice-form" style="display: none;"></div>
	</div>
</body>
</html>
