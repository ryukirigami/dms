<%@ page import="com.kombos.administrasi.NamaProsesBP; com.kombos.administrasi.Lift; com.kombos.administrasi.SubJenisStall; com.kombos.administrasi.JenisStall; com.kombos.administrasi.CompanyDealer; com.kombos.administrasi.Stall" %>
<g:javascript>
    var cek='${stallInstance?.m022StaTersedia}'
    if(cek!=''){
        if(cek=='1'){
            document.getElementById('m022TglJamAwal').disabled = true;
            document.getElementById('m022TglJamAwal_hour').disabled = true;
            document.getElementById('m022TglJamAwal_minute').disabled = true;
            document.getElementById('m022TglJamAkhir').disabled = true;
            document.getElementById('m022TglJamAkhir_hour').disabled = true;
            document.getElementById('m022TglJamAkhir_minute').disabled = true;
        }
    }
    function enabledDisabledTglJamAwalAkhir(val)
    {
        if(val=="1"){
            document.getElementById('m022TglJamAwal').disabled = true;
            document.getElementById('m022TglJamAwal_hour').disabled = true;
            document.getElementById('m022TglJamAwal_minute').disabled = true;
            document.getElementById('m022TglJamAkhir').disabled = true;
            document.getElementById('m022TglJamAkhir_hour').disabled = true;
            document.getElementById('m022TglJamAkhir_minute').disabled = true;
        } else{
            document.getElementById('m022TglJamAwal').disabled = false;
            document.getElementById('m022TglJamAwal_hour').disabled = false;
            document.getElementById('m022TglJamAwal_minute').disabled = false;
            document.getElementById('m022TglJamAkhir').disabled = false;
            document.getElementById('m022TglJamAkhir_hour').disabled = false;
            document.getElementById('m022TglJamAkhir_minute').disabled = false;
        }
    }
    $(document).ready(function(){
        $("#m022TglJamAwal_hour").change(function(){
            if(parseInt($('#m022TglJamAkhir_hour').val()) <= parseInt($('#m022TglJamAwal_hour').val())
                && parseInt($('#m022TglJamAwal_minute').val()) == '59' ){
                $("#m022TglJamAkhir_hour").val(parseInt($('#m022TglJamAwal_hour').val())+1);
            }
            else if(parseInt($("#m022TglJamAkhir_hour").val()) == parseInt($("#m022TglJamAwal_hour").val())
                && parseInt($("#m022TglJamAkhir_minute").val()) < parseInt($("#m022TglJamAwal_minute").val())){
                $('#m022TglJamAkhir_minute').val(parseInt($('#m022TglJamAwal_minute').val())+1);
            }
            else if(parseInt($("#m022TglJamAkhir_hour").val()) < parseInt($("#m022TglJamAwal_hour").val())){
                $("#m022TglJamAkhir_hour").val($("#m022TglJamAwal_hour").val());
                $('#m022TglJamAkhir_minute').val(parseInt($('#m022TglJamAwal_minute').val())+1);
            }
        });
        $("#m022TglJamAkhir_hour").change(function(){
            if(parseInt($('#m022TglJamAkhir_hour').val()) <= parseInt($('#m022TglJamAwal_hour').val())
                && parseInt($('#m022TglJamAwal_minute').val()) == '59' ){
                $("#m022TglJamAkhir_hour").val(parseInt($('#m022TglJamAwal_hour').val())+1);
            }
            else if(parseInt($("#m022TglJamAkhir_hour").val()) == parseInt($("#m022TglJamAwal_hour").val())
                && parseInt($("#m022TglJamAkhir_minute").val()) < parseInt($("#m022TglJamAwal_minute").val())){
                $('#m022TglJamAkhir_minute').val(parseInt($('#m022TglJamAwal_minute').val())+1);
            }
            else if(parseInt($("#m022TglJamAkhir_hour").val()) < parseInt($("#m022TglJamAwal_hour").val())){
                $("#m022TglJamAkhir_hour").val($("#m022TglJamAwal_hour").val());
                $('#m022TglJamAkhir_minute').val(parseInt($('#m022TglJamAwal_minute').val())+1);
            }
        });
    });
    function cekJenis(subJenisStall){
        var jenis = $('#jenisStall').val();
        //alert(jenis)BP-BODY & PAINT/4102/;
        if(document.getElementsByName("jenisStall.id")[0].selectedIndex==0 || document.getElementsByName("jenisStall.id")[0].selectedIndex==1){
           $("#namaProsesBP").show();
           $("#labelProses").show();
        }else if(document.getElementsByName("jenisStall.id")[0].selectedIndex==6){
            (document.getElementsByName("subJenisStall.id")[0].selectedIndex==13).hide();
            alert('THS')
        }else{
             $("#namaProsesBP").hide();
            $("#labelProses").hide();
        }

    }

</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: stallInstance, field: 'jenisStall', 'error')} required">
	<label class="control-label" for="jenisStall">
		<g:message code="stall.jenisStall.label" default="Jenis Stall" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="jenisStall" name="jenisStall.id" noSelection="['':'Pilih Jenis Stall']" from="${JenisStall.createCriteria().list {eq("staDel", "0");order("m021NamaJenisStall", "asc")}}" optionKey="id" required="" value="${stallInstance?.jenisStall?.id}" class="many-to-one" onchange="cekJenis('subJenisStall',this.value)"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stallInstance, field: 'subJenisStall', 'error')} required">
	<label class="control-label" for="subJenisStall">
		<g:message code="stall.subJenisStall.label" default="Sub Jenis Stall" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:select id="subJenisStall" name="subJenisStall.id" noSelection="['':'Pilih Sub Jenis Stall']" from="${SubJenisStall.createCriteria().list {eq("staDel", "0");order("m023NamaSubJenisStall", "asc")}}" optionKey="id" required="" value="${stallInstance?.subJenisStall?.id}" class="many-to-one"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stallInstance, field: 'm022NamaStall', 'error')} required">
	<label class="control-label" for="m022NamaStall">
		<g:message code="stall.m022NamaStall.label" default="Nama Stall" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m022NamaStall" required="" value="${stallInstance?.m022NamaStall}" maxlength="50"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stallInstance, field: 'm022Singkat', 'error')} required">
	<label class="control-label" for="m022Singkat">
		<g:message code="stall.m022Singkat.label" default="Initial Stall" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m022Singkat" required="" value="${stallInstance?.m022Singkat}" maxlength="25"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stallInstance, field: 'm022StaTersedia', 'error')} required">
	<label class="control-label" for="m022StaTersedia">
		<g:message code="stall.m022StaTersedia.label" default="Status Tersedia" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
    <g:radioGroup name="m022StaTersedia" id="m022StaTersedia" onclick="enabledDisabledTglJamAwalAkhir(this.value)" values="['1','0']" labels="['Ya','Tidak']" value="${stallInstance?.m022StaTersedia}" required="">
        ${it.radio} ${it.label}
    </g:radioGroup>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stallInstance, field: 'm022TglJamAwal', 'error')} required">
	<label class="control-label" for="m022TglJamAwal">
		<g:message code="stall.m022TglJamAwal.label" default="Tgl Jam Awal" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	    <ba:datePicker id="m022TglJamAwal" name="m022TglJamAwal" precision="minute" value="${stallInstance?.m022TglJamAwal}" format="dd/mm/yyyy" required="true"/>
        <select id="m022TglJamAwal_hour" name="m022TglJamAwal_hour" style="width: 60px" required="">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==stallInstance?.m022TglJamAwal?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==stallInstance?.m022TglJamAwal?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m022TglJamAwal_minute" name="m022TglJamAwal_minute" style="width: 60px" required="">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==stallInstance?.m022TglJamAwal?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==stallInstance?.m022TglJamAwal?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stallInstance, field: 'm022TglJamAkhir', 'error')} required">
	<label class="control-label" for="m022TglJamAkhir">
		<g:message code="stall.m022TglJamAkhir.label" default="Tgl Jam Akhir" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <ba:datePicker name="m022TglJamAkhir" precision="minute" value="${stallInstance?.m022TglJamAkhir}" format="dd/mm/yyyy" required="true"/>
        <select id="m022TglJamAkhir_hour" name="m022TglJamAkhir_hour" style="width: 60px" required="">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==stallInstance?.m022TglJamAkhir?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==stallInstance?.m022TglJamAkhir?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m022TglJamAkhir_minute" name="m022TglJamAkhir_minute" style="width: 60px" required="">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==stallInstance?.m022TglJamAkhir?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==stallInstance?.m022TglJamAkhir?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stallInstance, field: 'lift', 'error')} required">
	<label class="control-label" for="lift">
		<g:message code="stall.lift.label" default="Jenis Lift" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="lift" name="lift.id" from="${Lift.list()}" optionKey="id" required="" value="${stallInstance?.lift?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stallInstance, field: 'm022StaTPSLine', 'error')} required">
	<label class="control-label" for="m022StaTPSLine">
		<g:message code="stall.m022StaTPSLine.label" default="TPS Line" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
    <g:radioGroup name="m022StaTPSLine" values="['1','0']" labels="['Ya','Tidak']" value="${stallInstance?.m022StaTPSLine}" required="">
        ${it.radio} ${it.label}
    </g:radioGroup>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stallInstance, field: 'm022StaTHS', 'error')} required">
	<label class="control-label" for="m022StaTHS">
		<g:message code="stall.m022StaTHS.label" default="THS?" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
    <g:radioGroup name="m022StaTHS" values="['0','1']" labels="['Ya','Tidak']" value="${stallInstance?.m022StaTHS}" required="">
        ${it.radio} ${it.label}
    </g:radioGroup>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stallInstance, field: 'namaProsesBP', 'error')} ">
	<label id="labelProses" class="control-label" for="namaProsesBP">
		<g:message code="stall.namaProsesBP.label" default="Proses" />
	</label>

	<div class="controls">
	<g:select id="namaProsesBP" name="namaProsesBP.id" noSelection="['-1':'Pilih Proses']" from="${NamaProsesBP.list()}" optionKey="id"  value="${stallInstance?.namaProsesBP?.id}" class="many-to-one"  />
    </div>
</div>
<g:javascript>
    document.getElementById('jenisStall').focus();
</g:javascript>