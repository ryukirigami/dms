
<%@ page import="com.kombos.administrasi.Stall" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="stall_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="stall.companyDealer.label" default="Company Dealer" /></div>--}%
			%{--</th>--}%


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stall.jenisStall.label" default="Jenis Stall" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stall.subJenisStall.label" default="Sub Jenis Stall" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stall.m022NamaStall.label" default="Nama Stall" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stall.m022Singkat.label" default="Initial Stall" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stall.m022StaTersedia.label" default="Status Tersedia" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stall.m022TglJamAwal.label" default="M022 Tgl Jam Awal" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stall.m022TglJamAkhir.label" default="M022 Tgl Jam Akhir" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stall.lift.label" default="Jenis Lift" /></div>
			</th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="stall.namaProsesBP.label" default="Proses" /></div>
            </th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stall.m022StaTPSLine.label" default="TPS Line" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stall.staDel.label" default="Sta Del" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stall.createdBy.label" default="Created By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stall.updatedBy.label" default="Updated By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stall.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stall.m022StallID.label" default="Stall ID" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stall.m022ID.label" default="ID" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_companyDealer" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_jenisStall" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_jenisStall" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_subJenisStall" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_subJenisStall" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m022NamaStall" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m022NamaStall" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m022Singkat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m022Singkat" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m022StaTersedia" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m022StaTersedia" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m022TglJamAwal" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m022TglJamAwal" value="date.struct">
					<input type="hidden" name="search_m022TglJamAwal_day" id="search_m022TglJamAwal_day" value="">
					<input type="hidden" name="search_m022TglJamAwal_month" id="search_m022TglJamAwal_month" value="">
					<input type="hidden" name="search_m022TglJamAwal_year" id="search_m022TglJamAwal_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m022TglJamAwal_dp" value="" id="search_m022TglJamAwal" class="search_init">
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m022TglJamAkhir" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m022TglJamAkhir" value="date.struct">
					<input type="hidden" name="search_m022TglJamAkhir_day" id="search_m022TglJamAkhir_day" value="">
					<input type="hidden" name="search_m022TglJamAkhir_month" id="search_m022TglJamAkhir_month" value="">
					<input type="hidden" name="search_m022TglJamAkhir_year" id="search_m022TglJamAkhir_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m022TglJamAkhir_dp" value="" id="search_m022TglJamAkhir" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_lift" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lift" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_namaProsesBP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_namaProsesBP" class="search_init" />
                </div>
            </th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m022StaTPSLine" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<!--<input type="text" name="search_m022StaTPSLine" class="search_init" />-->
                    <select name="search_m022StaTPSLine" id="search_m022StaTPSLine" style="width:100%" class="search_init" onchange="reloadStallTable();">
                        <option value=""></option>
                        <option value="1">Ya</option>
                        <option value="0">Tidak</option>
                    </select>
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_staDel" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_createdBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_updatedBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m022StallID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m022StallID" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m022ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m022ID" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var stallTable;
var reloadStallTable;
$(function(){
	
	reloadStallTable = function() {
		stallTable.fnDraw();
	}

	var recordsStallPerPage = [];
    var anStallSelected;
    var jmlRecStallPerPage=0;
    var id;
    
	$('#search_m022TglJamAwal').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m022TglJamAwal_day').val(newDate.getDate());
			$('#search_m022TglJamAwal_month').val(newDate.getMonth()+1);
			$('#search_m022TglJamAwal_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			stallTable.fnDraw();	
	});

	

	$('#search_m022TglJamAkhir').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m022TglJamAkhir_day').val(newDate.getDate());
			$('#search_m022TglJamAkhir_month').val(newDate.getMonth()+1);
			$('#search_m022TglJamAkhir_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			stallTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	stallTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	stallTable = $('#stall_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsStall = $("#stall_datatables tbody .row-select");
            var jmlStallCek = 0;
            var nRow;
            var idRec;
            rsStall.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsStallPerPage[idRec]=="1"){
                    jmlStallCek = jmlStallCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsStallPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecStallPerPage = rsStall.length;
            if(jmlStallCek==jmlRecStallPerPage && jmlRecStallPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
//
//{
//	"sName": "companyDealer",
//	"mDataProp": "companyDealer",
//	"aTargets": [0],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"300px",
//	"bVisible": false
//}
//
//,

{
	"sName": "jenisStall",
	"mDataProp": "jenisStall",
	"aTargets": [1],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "subJenisStall",
	"mDataProp": "subJenisStall",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m022NamaStall",
	"mDataProp": "m022NamaStall",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m022Singkat",
	"mDataProp": "m022Singkat",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m022StaTersedia",
	"mDataProp": "m022StaTersedia",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "m022TglJamAwal",
	"mDataProp": "m022TglJamAwal",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "m022TglJamAkhir",
	"mDataProp": "m022TglJamAkhir",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "lift",
	"mDataProp": "lift",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "namaProsesBP",
	"mDataProp": "namaProsesBP",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m022StaTPSLine",
	"mDataProp": "m022StaTPSLine",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "staDel",
	"mDataProp": "staDel",
	"aTargets": [11],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "createdBy",
	"mDataProp": "createdBy",
	"aTargets": [12],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "updatedBy",
	"mDataProp": "updatedBy",
	"aTargets": [13],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [14],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "m022StallID",
	"mDataProp": "m022StallID",
	"aTargets": [15],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "m022ID",
	"mDataProp": "m022ID",
	"aTargets": [16],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}
	
						var jenisStall = $('#filter_jenisStall input').val();
						if(jenisStall){
							aoData.push(
									{"name": 'sCriteria_jenisStall', "value": jenisStall}
							);
						}
	
						var subJenisStall = $('#filter_subJenisStall input').val();
						if(subJenisStall){
							aoData.push(
									{"name": 'sCriteria_subJenisStall', "value": subJenisStall}
							);
						}
	
						var m022NamaStall = $('#filter_m022NamaStall input').val();
						if(m022NamaStall){
							aoData.push(
									{"name": 'sCriteria_m022NamaStall', "value": m022NamaStall}
							);
						}
	
						var m022Singkat = $('#filter_m022Singkat input').val();
						if(m022Singkat){
							aoData.push(
									{"name": 'sCriteria_m022Singkat', "value": m022Singkat}
							);
						}
	
						var m022StaTersedia = $('#filter_m022StaTersedia input').val();
						if(m022StaTersedia){
							aoData.push(
									{"name": 'sCriteria_m022StaTersedia', "value": m022StaTersedia}
							);
						}

						var m022TglJamAwal = $('#search_m022TglJamAwal').val();
						var m022TglJamAwalDay = $('#search_m022TglJamAwal_day').val();
						var m022TglJamAwalMonth = $('#search_m022TglJamAwal_month').val();
						var m022TglJamAwalYear = $('#search_m022TglJamAwal_year').val();
						
						if(m022TglJamAwal){
							aoData.push(
									{"name": 'sCriteria_m022TglJamAwal', "value": "date.struct"},
									{"name": 'sCriteria_m022TglJamAwal_dp', "value": m022TglJamAwal},
									{"name": 'sCriteria_m022TglJamAwal_day', "value": m022TglJamAwalDay},
									{"name": 'sCriteria_m022TglJamAwal_month', "value": m022TglJamAwalMonth},
									{"name": 'sCriteria_m022TglJamAwal_year', "value": m022TglJamAwalYear}
							);
						}

						var m022TglJamAkhir = $('#search_m022TglJamAkhir').val();
						var m022TglJamAkhirDay = $('#search_m022TglJamAkhir_day').val();
						var m022TglJamAkhirMonth = $('#search_m022TglJamAkhir_month').val();
						var m022TglJamAkhirYear = $('#search_m022TglJamAkhir_year').val();
						
						if(m022TglJamAkhir){
							aoData.push(
									{"name": 'sCriteria_m022TglJamAkhir', "value": "date.struct"},
									{"name": 'sCriteria_m022TglJamAkhir_dp', "value": m022TglJamAkhir},
									{"name": 'sCriteria_m022TglJamAkhir_day', "value": m022TglJamAkhirDay},
									{"name": 'sCriteria_m022TglJamAkhir_month', "value": m022TglJamAkhirMonth},
									{"name": 'sCriteria_m022TglJamAkhir_year', "value": m022TglJamAkhirYear}
							);
						}
	
						var lift = $('#filter_lift input').val();
						if(lift){
							aoData.push(
									{"name": 'sCriteria_lift', "value": lift}
							);
						}
	
						var m022StaTPSLine = $('#search_m022StaTPSLine').val();
						if(m022StaTPSLine){
							aoData.push(
									{"name": 'sCriteria_m022StaTPSLine', "value": m022StaTPSLine}
							);
						}
	
						var namaProsesBP = $('#filter_namaProsesBP input').val();
						if(namaProsesBP){
							aoData.push(
									{"name": 'sCriteria_namaProsesBP', "value": namaProsesBP}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}
	
						var m022StallID = $('#filter_m022StallID input').val();
						if(m022StallID){
							aoData.push(
									{"name": 'sCriteria_m022StallID', "value": m022StallID}
							);
						}
	
						var m022ID = $('#filter_m022ID input').val();
						if(m022ID){
							aoData.push(
									{"name": 'sCriteria_m022ID', "value": m022ID}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

	$('.select-all').click(function(e) {

        $("#stall_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsStallPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsStallPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#stall_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsStallPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anStallSelected = stallTable.$('tr.row_selected');
            if(jmlRecStallPerPage == anStallSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsStallPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
