<%@ page import="com.kombos.administrasi.TarifPerJam" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>
<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</g:javascript>
<table id="tarifPerJam_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="tarifPerJam.companyDealer.label" default="Company Dealer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="tarifPerJam.kategori.label" default="Kategori" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="tarifPerJam.t152TMT.label" default="Tanggal Berlaku" /></div>
			</th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="tarifPerJam.t152TarifPerjamGR.label" default="Tarif Per Jam GR" /></div>
            </th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="tarifPerJam.t152TarifPerjamSB.label" default="Tarif Per Jam SB" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="tarifPerJam.t152TarifPerjamDealer.label" default="Tarif Per Jam TAM" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="tarifPerJam.t152TarifPerjamSBI.label" default="Tarif Per Jam SBI" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="tarifPerJam.t152TarifPerjamPDS.label" default="Tarif Per Jam PDS" /></div>
            </th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="tarifPerJam.t152TarifPerjamSPO.label" default="Tarif Per Jam SPOORING" /></div>
			</th>
		</tr>
		<tr>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_companyDealer" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_kategori" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_kategori" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t152TMT" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_t152TMT" value="date.struct">
					<input type="hidden" name="search_t152TMT_day" id="search_t152TMT_day" value="">
					<input type="hidden" name="search_t152TMT_month" id="search_t152TMT_month" value="">
					<input type="hidden" name="search_t152TMT_year" id="search_t152TMT_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t152TMT_dp" value="" id="search_t152TMT" class="search_init">
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t152TarifPerjamGR" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" onkeypress="return isNumberKey(event)" name="search_t152TarifPerjamGR" class="search_init" />
                </div>
            </th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t152TarifPerjamSB" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" onkeypress="return isNumberKey(event)" name="search_t152TarifPerjamSB" class="search_init" />
				</div>
			</th>
	

	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t152TarifPerjamDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" onkeypress="return isNumberKey(event)" name="search_t152TarifPerjamDealer" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t152TarifPerjamSBI" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" onkeypress="return isNumberKey(event)" name="search_t152TarifPerjamSBI" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t152TarifPerjamPDS" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" onkeypress="return isNumberKey(event)" name="search_t152TarifPerjamPDS" class="search_init" />
                </div>
            </th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t152TarifPerjamSPO" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" onkeypress="return isNumberKey(event)" name="search_t152TarifPerjamSPO" class="search_init" />
				</div>
			</th>
        </tr>
	</thead>
</table>

<g:javascript>
var TarifPerJamTable;
var reloadTarifPerJamTable;
$(function(){
	
	reloadTarifPerJamTable = function() {
		TarifPerJamTable.fnDraw();
	}

	var recordstarifPerJamperpage = [];//new Array();
    var antarifPerJamSelected;
    var jmlRectarifPerJamPerPage=0;
    var id;

	$('#search_t152TMT').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t152TMT_day').val(newDate.getDate());
			$('#search_t152TMT_month').val(newDate.getMonth()+1);
			$('#search_t152TMT_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			TarifPerJamTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	TarifPerJamTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	TarifPerJamTable = $('#tarifPerJam_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rstarifPerJam = $("#tarifPerJam_datatables tbody .row-select");
            var jmltarifPerJamCek = 0;
            var nRow;
            var idRec;
            rstarifPerJam.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordstarifPerJamperpage[idRec]=="1"){
                    jmltarifPerJamCek = jmltarifPerJamCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordstarifPerJamperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRectarifPerJamPerPage = rstarifPerJam.length;
            if(jmltarifPerJamCek==jmlRectarifPerJamPerPage && jmlRectarifPerJamPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
	"sName": "companyDealer",
	"mDataProp": "companyDealer",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "kategori",
	"mDataProp": "kategori",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t152TMT",
	"mDataProp": "t152TMT",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "t152TarifPerjamGR",
	"mDataProp": "t152TarifPerjamGR",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "t152TarifPerjamSB",
	"mDataProp": "t152TarifPerjamSB",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t152TarifPerjamDealer",
	"mDataProp": "t152TarifPerjamDealer",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t152TarifPerjamSBI",
	"mDataProp": "t152TarifPerjamSBI",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "t152TarifPerjamPDS",
	"mDataProp": "t152TarifPerjamPDS",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "t152TarifPerjamSPO",
	"mDataProp": "t152TarifPerjamSPO",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}
	
						var kategori = $('#filter_kategori input').val();
						if(kategori){
							aoData.push(
									{"name": 'sCriteria_kategori', "value": kategori}
							);
						}

						var t152TMT = $('#search_t152TMT').val();
						var t152TMTDay = $('#search_t152TMT_day').val();
						var t152TMTMonth = $('#search_t152TMT_month').val();
						var t152TMTYear = $('#search_t152TMT_year').val();
						
						if(t152TMT){
							aoData.push(
									{"name": 'sCriteria_t152TMT', "value": "date.struct"},
									{"name": 'sCriteria_t152TMT_dp', "value": t152TMT},
									{"name": 'sCriteria_t152TMT_day', "value": t152TMTDay},
									{"name": 'sCriteria_t152TMT_month', "value": t152TMTMonth},
									{"name": 'sCriteria_t152TMT_year', "value": t152TMTYear}
							);
						}
	
						var t152TarifPerjamSB = $('#filter_t152TarifPerjamSB input').val();
						if(t152TarifPerjamSB){
							aoData.push(
									{"name": 'sCriteria_t152TarifPerjamSB', "value": t152TarifPerjamSB}
							);
						}
	
						var t152TarifPerjamGR = $('#filter_t152TarifPerjamGR input').val();
						if(t152TarifPerjamGR){
							aoData.push(
									{"name": 'sCriteria_t152TarifPerjamGR', "value": t152TarifPerjamGR}
							);
						}
	
						var t152TarifPerjamDealer = $('#filter_t152TarifPerjamDealer input').val();
						if(t152TarifPerjamDealer){
							aoData.push(
									{"name": 'sCriteria_t152TarifPerjamDealer', "value": t152TarifPerjamDealer}
							);
						}
	
                        var t152TarifPerjamSBI = $('#filter_t152TarifPerjamSBI input').val();
						if(t152TarifPerjamSBI){
							aoData.push(
									{"name": 'sCriteria_t152TarifPerjamSBI', "value": t152TarifPerjamSBI}
							);
						}

                        var t152TarifPerjamPDS = $('#filter_t152TarifPerjamPDS input').val();
						if(t152TarifPerjamPDS){
							aoData.push(
									{"name": 'sCriteria_t152TarifPerjamPDS', "value": t152TarifPerjamPDS}
							);
						}

						var t152TarifPerjamSPO = $('#filter_t152TarifPerjamSPO input').val();
						if(t152TarifPerjamSPO){
							aoData.push(
									{"name": 'sCriteria_t152TarifPerjamSPO', "value": t152TarifPerjamSPO}
							);
						}


						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

	  $('.select-all').click(function(e) {

        $("#tarifPerJam_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordstarifPerJamperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordstarifPerJamperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#tarifPerJam_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordstarifPerJamperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            antarifPerJamSelected = tarifPerJamTable.$('tr.row_selected');
            if(jmlRectarifPerJamPerPage == antarifPerJamSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordstarifPerJamperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>