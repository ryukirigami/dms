

<%@ page import="com.kombos.administrasi.TarifPerJam" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'tarifPerJam.label', default: 'Tarif Per Jam')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteTarifPerJam;

$(function(){ 
	deleteTarifPerJam=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/tarifPerJam/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadTarifPerJamTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-tarifPerJam" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="tarifPerJam"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${tarifPerJamInstance?.companyDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="companyDealer-label" class="property-label"><g:message
					code="tarifPerJam.companyDealer.label" default="Company Dealer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="companyDealer-label">
						%{--<ba:editableValue
								bean="${tarifPerJamInstance}" field="companyDealer"
								url="${request.contextPath}/TarifPerJam/updatefield" type="text"
								title="Enter companyDealer" onsuccess="reloadTarifPerJamTable();" />--}%
							
								${tarifPerJamInstance?.companyDealer?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tarifPerJamInstance?.kategori}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kategori-label" class="property-label"><g:message
					code="tarifPerJam.kategori.label" default="Kategori" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kategori-label">
						%{--<ba:editableValue
								bean="${tarifPerJamInstance}" field="kategori"
								url="${request.contextPath}/TarifPerJam/updatefield" type="text"
								title="Enter kategori" onsuccess="reloadTarifPerJamTable();" />--}%
							
								${tarifPerJamInstance?.kategori?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tarifPerJamInstance?.t152TMT}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t152TMT-label" class="property-label"><g:message
					code="tarifPerJam.t152TMT.label" default="Tanggal Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t152TMT-label">
						%{--<ba:editableValue
								bean="${tarifPerJamInstance}" field="t152TMT"
								url="${request.contextPath}/TarifPerJam/updatefield" type="text"
								title="Enter t152TMT" onsuccess="reloadTarifPerJamTable();" />--}%
							
								<g:formatDate date="${tarifPerJamInstance?.t152TMT}" />
							
						</span></td>
					
				</tr>
				</g:if>
                <g:if test="${tarifPerJamInstance?.t152TarifPerjamGR}">
                    <tr>
                        <td class="span2" style="text-align: right;"><span
                                id="t152TarifPerjamGR-label" class="property-label"><g:message
                                    code="tarifPerJam.t152TarifPerjamGR.label" default="Tarif Per Jam GR" />:</span></td>

                        <td class="span3"><span class="property-value"
                                                aria-labelledby="t152TarifPerjamGR-label">
                            %{--<ba:editableValue
                                    bean="${tarifPerJamInstance}" field="t152TarifPerjamGR"
                                    url="${request.contextPath}/TarifPerJam/updatefield" type="text"
                                    title="Enter t152TarifPerjamGR" onsuccess="reloadTarifPerJamTable();" />--}%

                            <g:fieldValue bean="${tarifPerJamInstance}" field="t152TarifPerjamGR"/>

                        </span></td>

                    </tr>
                </g:if>

				<g:if test="${tarifPerJamInstance?.t152TarifPerjamSB}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t152TarifPerjamSB-label" class="property-label"><g:message
					code="tarifPerJam.t152TarifPerjamSB.label" default="Tarif Per Jam SB" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t152TarifPerjamSB-label">
						%{--<ba:editableValue
								bean="${tarifPerJamInstance}" field="t152TarifPerjamSB"
								url="${request.contextPath}/TarifPerJam/updatefield" type="text"
								title="Enter t152TarifPerjamSB" onsuccess="reloadTarifPerJamTable();" />--}%
							
								<g:fieldValue bean="${tarifPerJamInstance}" field="t152TarifPerjamSB"/>
							
						</span></td>
					
				</tr>
				</g:if>
			

			
				<g:if test="${tarifPerJamInstance?.t152TarifPerjamBP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t152TarifPerjamBP-label" class="property-label"><g:message
					code="tarifPerJam.t152TarifPerjamBP.label" default="Tarif Per Jam BP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t152TarifPerjamBP-label">
						%{--<ba:editableValue
								bean="${tarifPerJamInstance}" field="t152TarifPerjamBP"
								url="${request.contextPath}/TarifPerJam/updatefield" type="text"
								title="Enter t152TarifPerjamBP" onsuccess="reloadTarifPerJamTable();" />--}%
							
								<g:fieldValue bean="${tarifPerJamInstance}" field="t152TarifPerjamBP"/>
							
						</span></td>
					
				</tr>
				</g:if>

				<g:if test="${tarifPerJamInstance?.t152TarifPerjamSBI}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t152TarifPerjamSBI-label" class="property-label"><g:message
					code="tarifPerJam.t152TarifPerjamSBI.label" default="Tarif Per SBI" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="t152TarifPerjamSBI-label">
						%{--<ba:editableValue
								bean="${tarifPerJamInstance}" field="t152TarifPerjamBP"
								url="${request.contextPath}/TarifPerJam/updatefield" type="text"
								title="Enter t152TarifPerjamBP" onsuccess="reloadTarifPerJamTable();" />--}%

								<g:fieldValue bean="${tarifPerJamInstance}" field="t152TarifPerjamSBI"/>

						</span></td>

				</tr>
				</g:if>

				<g:if test="${tarifPerJamInstance?.t152TarifPerjamPDS}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t152TarifPerjamPDS-label" class="property-label"><g:message
					code="tarifPerJam.t152TarifPerjamPDS.label" default="Tarif Per Jam PDS" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="t152TarifPerjamPDS-label">
						%{--<ba:editableValue
								bean="${tarifPerJamInstance}" field="t152TarifPerjamBP"
								url="${request.contextPath}/TarifPerJam/updatefield" type="text"
								title="Enter t152TarifPerjamBP" onsuccess="reloadTarifPerJamTable();" />--}%

								<g:fieldValue bean="${tarifPerJamInstance}" field="t152TarifPerjamPDS"/>

						</span></td>

				</tr>

				</g:if><g:if test="${tarifPerJamInstance?.t152TarifPerjamSPO}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t152TarifPerjamSPO-label" class="property-label"><g:message
					code="tarifPerJam.t152TarifPerjamSPO.label" default="Tarif Per Jam SPOORING" />:</span></td>

						<td class="span3"><span class="property-value" aria-labelledby="t152TarifPerjamSPO-label">

								<g:fieldValue bean="${tarifPerJamInstance}" field="t152TarifPerjamSPO"/>

						</span></td>

				</tr>
				</g:if>

				<g:if test="${tarifPerJamInstance?.t152TarifPerjamDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t152TarifPerjamDealer-label" class="property-label"><g:message
					code="tarifPerJam.t152TarifPerjamDealer.label" default="Tarif Per Jam TAM" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="t152TarifPerjamDealer-label">
						%{--<ba:editableValue
								bean="${tarifPerJamInstance}" field="t152TarifPerjamBP"
								url="${request.contextPath}/TarifPerJam/updatefield" type="text"
								title="Enter t152TarifPerjamBP" onsuccess="reloadTarifPerJamTable();" />--}%

								<g:fieldValue bean="${tarifPerJamInstance}" field="t152TarifPerjamDealer"/>

						</span></td>

				</tr>
				</g:if>

            <g:if test="${tarifPerJamInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="dealerPenjual.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${tarifPerJamInstance}" field="dateCreated"
                                url="${request.contextPath}/dealerPenjual/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloaddealerPenjualTable();" />--}%

                        <g:formatDate date="${tarifPerJamInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${tarifPerJamInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="dealerPenjual.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${tarifPerJamInstance}" field="createdBy"
                                url="${request.contextPath}/dealerPenjual/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloaddealerPenjualTable();" />--}%

                        <g:fieldValue bean="${tarifPerJamInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${tarifPerJamInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="dealerPenjual.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${tarifPerJamInstance}" field="lastUpdated"
                                url="${request.contextPath}/dealerPenjual/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloaddealerPenjualTable();" />--}%

                        <g:formatDate date="${tarifPerJamInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${tarifPerJamInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="dealerPenjual.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${tarifPerJamInstance}" field="updatedBy"
                                url="${request.contextPath}/dealerPenjual/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloaddealerPenjualTable();" />--}%

                        <g:fieldValue bean="${tarifPerJamInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${tarifPerJamInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="dealerPenjual.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${tarifPerJamInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/dealerPenjual/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloaddealerPenjualTable();" />--}%

                        <g:fieldValue bean="${tarifPerJamInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>




            </tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${tarifPerJamInstance?.id}"
					update="[success:'tarifPerJam-form',failure:'tarifPerJam-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteTarifPerJam('${tarifPerJamInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
