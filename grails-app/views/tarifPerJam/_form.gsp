<%@ page import="com.kombos.administrasi.KategoriKendaraan; com.kombos.administrasi.Kategori; com.kombos.administrasi.CompanyDealer; com.kombos.administrasi.TarifPerJam" %>
<r:require modules="autoNumeric" />
<g:javascript>
    $(function(){
        $('.auto').autoNumeric('init',{
            aSep:','
        });
    });
</g:javascript>

%{--<div class="control-group fieldcontain ${hasErrors(bean: tarifPerJamInstance, field: 'companyDealer', 'error')} required">--}%
	%{--<label class="control-label" for="companyDealer">--}%
		%{--<g:message code="tarifPerJam.companyDealer.label" default="Company Dealer" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:select id="companyDealer" name="companyDealer.id" from="${CompanyDealer.createCriteria().list(){eq("staDel", "0"); order("m011NamaWorkshop", "asc")}}" optionKey="id" required="" value="${tarifPerJamInstance?.companyDealer?.id}" class="many-to-one"/>--}%
	%{--</div>--}%
%{--</div>--}%

<div class="control-group fieldcontain ${hasErrors(bean: tarifPerJamInstance, field: 'companyDealer', 'error')} required">
    <label class="control-label" for="companyDealer">
        <g:message code="tarifPerJam.companyDealer.label" default="Company Dealer" />
        <span class="required-indicator">*</span>
    </label>
    <div id="filter_company" class="controls">
        <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
            <g:select name="companyDealer.id" id="companyDealer" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${tarifPerJamInstance?.companyDealer ? tarifPerJamInstance?.companyDealer?.id : session?.userCompanyDealer?.id}" />
        </g:if>
        <g:else>
            <g:hiddenField name="companyDealer.id" id="companyDealer" value="${tarifPerJamInstance?.companyDealer ? tarifPerJamInstance?.companyDealer?.id : session?.userCompanyDealer?.id}" />
            <g:textField name="txtCompany" id="txtCompany" readonly="" value="${tarifPerJamInstance?.companyDealer ? tarifPerJamInstance?.companyDealer?.m011NamaWorkshop : session?.userCompanyDealer?.m011NamaWorkshop}" />
        </g:else>
    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: tarifPerJamInstance, field: 'kategori', 'error')} required">
	<label class="control-label" for="kategori">
		<g:message code="tarifPerJam.kategori.label" default="Kategori" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="kategori" name="kategori.id" from="${KategoriKendaraan.createCriteria().list(){eq("staDel", "0");order("m101NamaKategori", "asc")}}" optionKey="id" required="" value="${tarifPerJamInstance?.kategori?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: tarifPerJamInstance, field: 't152TMT', 'error')} required">
	<label class="control-label" for="t152TMT">
		<g:message code="tarifPerJam.t152TMT.label" default="Tanggal Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">

    <ba:datePicker  name="t152TMT" precision="day"  value="${tarifPerJamInstance?.t152TMT}" format="yyyy-MM-dd" required="true"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: tarifPerJamInstance, field: 't152TarifPerjamGR', 'error')} required">
    <label class="control-label" for="t152TarifPerjamGR">
        <g:message code="tarifPerJam.t152TarifPerjamGR.label" default="Tarif Per Jam GR" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="t152TarifPerjamGR" class="auto" maxlength="12" value="${fieldValue(bean: tarifPerJamInstance, field: 't152TarifPerjamGR')}" required=""/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: tarifPerJamInstance, field: 't152TarifPerjamSB', 'error')} required">
	<label class="control-label" for="t152TarifPerjamSB">
		<g:message code="tarifPerJam.t152TarifPerjamSB.label" default="Tarif Per Jam SB" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t152TarifPerjamSB" class="auto" maxlength="12" value="${fieldValue(bean: tarifPerJamInstance, field: 't152TarifPerjamSB')}" required=""/>
	</div>
</div>



<div class="control-group fieldcontain ${hasErrors(bean: tarifPerJamInstance, field: 't152TarifPerjamBP', 'error')} required">
	<label class="control-label" for="t152TarifPerjamBP">
		<g:message code="tarifPerJam.t152TarifPerjamBP.label" default="Tarif Per Jam BP" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t152TarifPerjamBP" class="auto" maxlength="12"  value="${fieldValue(bean: tarifPerJamInstance, field: 't152TarifPerjamBP')}"  required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: tarifPerJamInstance, field: 't152TarifPerjamSBI', 'error')} required">
    <label class="control-label" for="t152TarifPerjamSBI">
        <g:message code="tarifPerJam.t152TarifPerjamSBI.label" default="Tarif Per Jam SBI" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="t152TarifPerjamSBI" class="auto" maxlength="12"  value="${fieldValue(bean: tarifPerJamInstance, field: 't152TarifPerjamSBI')}"  required=""/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: tarifPerJamInstance, field: 't152TarifPerjamPDS', 'error')} required">
    <label class="control-label" for="t152TarifPerjamPDS">
        <g:message code="tarifPerJam.t152TarifPerjamPDS.label" default="Tarif Per Jam PDS" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="t152TarifPerjamPDS" class="auto" maxlength="12"  value="${fieldValue(bean: tarifPerJamInstance, field: 't152TarifPerjamPDS')}"  required=""/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: tarifPerJamInstance, field: 't152TarifPerjamSPO', 'error')} required">
    <label class="control-label" for="t152TarifPerjamSPO">
        <g:message code="tarifPerJam.t152TarifPerjamSPO.label" default="Tarif Per Jam SPOORING" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="t152TarifPerjamSPO" class="auto" maxlength="12"  value="${fieldValue(bean: tarifPerJamInstance, field: 't152TarifPerjamSPO')}"  required=""/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: tarifPerJamInstance, field: 't152TarifPerjamDealer', 'error')} required">
    <label class="control-label" for="t152TarifPerjamDealer">
        <g:message code="tarifPerJam.t152TarifPerjamDealer.label" default="Tarif Per Jam TAM" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="t152TarifPerjamDealer" class="auto" maxlength="12"  value="${fieldValue(bean: tarifPerJamInstance, field: 't152TarifPerjamDealer')}"  required=""/>
    </div>
</div>

<g:javascript>
    document.getElementById("companyDealer").focus();
</g:javascript>