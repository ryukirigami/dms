
<%@ page import="com.kombos.administrasi.HistoryPrint" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>



<table id="historyPrint_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="historyPrint.t951Tanggal.label" default="Tanggal" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="historyPrint.userProfile.label" default="Nama User" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="historyPrint.t951NoDokumen.label" default="No Dokumen" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
				<div><g:message code="historyPrint.namaDokumen.label" default="Nama Dokumen" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="historyPrint.t951PrintCounter.label" default="Print Counter" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="historyPrint.t951Alasan.label" default="Alasan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="historyPrint.createdBy.label" default="Created By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="historyPrint.updatedBy.label" default="Updated By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="historyPrint.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>

		
		</tr>
	</thead>
</table>

<g:javascript>
var historyPrintTable;
var reloadHistoryPrintTable;
$(function(){
	
	reloadHistoryPrintTable = function() {
		historyPrintTable.fnDraw();
	}

	
$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	historyPrintTable.fnDraw();
		}
	});
	$('#clear').click(function(e){
        $('#search_t951Tanggal').val("");
        $('#search_t951Tanggal_day').val("");
        $('#search_t951Tanggal_month').val("");
        $('#search_t951Tanggal_year').val("");
        $('#search_t951TanggalAkhir').val("");
        $('#search_t951TanggalAkhir_day').val("");
        $('#search_t951TanggalAkhir_month').val("");
        $('#search_t951TanggalAkhir_year').val("");
        $('#filter_userProfile input').val("");
        $('#filter_role select').val("");
        $('#filter_namaDokumen select').val("");
//        reloadAuditTrailTable();
	});
	$('#view').click(function(e){
	    if($('#search_t951Tanggal').val()!="" && $('#search_t951TanggalAkhir').val()!=""){
            var t951Tanggal = $('#search_t951Tanggal').val().split("/");
            var t951TanggalAkhir = $('#search_t951TanggalAkhir').val().split("/");
            var date1 = new Date(t951Tanggal[0], t951Tanggal[1]-1, t951Tanggal[2]);
            var date2 = new Date(t951TanggalAkhir[0], t951TanggalAkhir[1]-1, t951TanggalAkhir[2]);
            if(date2 < date1){
                alert('Tanggal akhir harus setelah tanggal awal');
                return;
            }
	    }
        e.stopPropagation();
		historyPrintTable.fnDraw();
//        reloadAuditTrailTable();
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	historyPrintTable = $('#historyPrint_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "t951Tanggal",
	"mDataProp": "t951Tanggal",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true

}

,


{
	"sName": "userProfile",
	"mDataProp": "userProfile",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "t951NoDokumen",
	"mDataProp": "t951NoDokumen",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "namaDokumen",
	"mDataProp": "namaDokumen",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,


{
	"sName": "t951PrintCounter",
	"mDataProp": "t951PrintCounter",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,



{
	"sName": "t951Alasan",
	"mDataProp": "t951Alasan",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "createdBy",
	"mDataProp": "createdBy",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "updatedBy",
	"mDataProp": "updatedBy",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
        
                        var t951Tanggal = $('#search_t951Tanggal').val();
						var t951TanggalDay = $('#search_t951Tanggal_day').val();
						var t951TanggalMonth = $('#search_t951Tanggal_month').val();
						var t951TanggalYear = $('#search_t951Tanggal_year').val();
						
						if(t951Tanggal){
							aoData.push(
									{"name": 'sCriteria_t951Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_t951Tanggal_dp', "value": t951Tanggal},
									{"name": 'sCriteria_t951Tanggal_day', "value": t951TanggalDay},
									{"name": 'sCriteria_t951Tanggal_month', "value": t951TanggalMonth},
									{"name": 'sCriteria_t951Tanggal_year', "value": t951TanggalYear}
							);
						}

                        var t951TanggalAkhir = $('#search_t951TanggalAkhir').val();
                        var t951TanggalDayakhir = $('#search_t951TanggalAkhir_day').val();
                        var t951TanggalMonthakhir = $('#search_t951TanggalAkhir_month').val();
                        var t951TanggalYearakhir = $('#search_t951TanggalAkhir_year').val();
                        
                        if(t951TanggalAkhir){
                            aoData.push(
                                    {"name": 'sCriteria_t951TanggalAkhir', "value": "date.struct"},
                                    {"name": 'sCriteria_t951TanggalAkhir_dp', "value": t951TanggalAkhir},
                                    {"name": 'sCriteria_t951TanggalAkhir_day', "value": t951TanggalDayakhir},
                                    {"name": 'sCriteria_t951TanggalAkhir_month', "value":t951TanggalMonthakhir},
                                    {"name": 'sCriteria_t951TanggalAkhir_year', "value": t951TanggalYearakhir}
                            );
                        }

	                    var userProfile = $('#filter_userProfile input').val();
						if(userProfile){
							aoData.push(
									{"name": 'sCriteria_userProfile', "value": userProfile}
							);
						}
						var role = $('#filter_role select').val();
                        if(role){
                            aoData.push(
                                            {"name": 'sCriteria_role', "value": role}
                            );
                        }
						var namaDokumen = $('#filter_namaDokumen select').val();
						if(namaDokumen){
							aoData.push(
									{"name": 'sCriteria_namaDokumen', "value": namaDokumen}
							);
						}
	
						var t951NoDokumen = $('#filter_t951NoDokumen input').val();
						if(t951NoDokumen){
							aoData.push(
									{"name": 'sCriteria_t951NoDokumen', "value": t951NoDokumen}
							);
						}
	
						var t951PrintCounter = $('#filter_t951PrintCounter input').val();
						if(t951PrintCounter){
							aoData.push(
									{"name": 'sCriteria_t951PrintCounter', "value": t951PrintCounter}
							);
						}

						var t951Alasan = $('#filter_t951Alasan input').val();
						if(t951Alasan){
							aoData.push(
									{"name": 'sCriteria_t951Alasan', "value": t951Alasan}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>
