
<%@ page import="com.kombos.baseapp.sec.shiro.Role; com.kombos.administrasi.NamaDokumen; com.kombos.administrasi.UserProfile; com.kombos.administrasi.UserRole; com.kombos.administrasi.HistoryPrint" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'historyPrint.label', default: 'Print History')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
   	$('#userProfile').typeahead({
        source: function (query, process) {
            return $.get('${request.contextPath}/historyPrint/getUser?roles='+$('#userRole').val(), { query: query }, function (data) {
                return process(data.options);
            });
        }
    });

});

    var checkin = $('#search_t951Tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
        var newDate = new Date(ev.date)
        newDate.setDate(newDate.getDate() + 1);
        checkout.setValue(newDate);
        checkin.hide();
        $('#search_t951TanggalAkhir')[0].focus();
    }).data('datepicker');
    
    var checkout = $('#search_t951TanggalAkhir').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

    var roleTemp="";
    function changeRole(){
        var roles = $('#userRole').val();
        if(roles==-1){
            $('#userProfile').val('');
            $('#userProfile').attr("disabled",true);
        }else{
            $('#userProfile').attr("disabled",false);
            if(roleTemp==""){
                roleTemp=roles;
            }else{
                if(roles!=roleTemp){
                    $('#userProfile').val('');
                    roleTemp=roles;
                }
            }
        }
    }

</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="historyPrint-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
			<fieldset>
                <table style="padding-right: 10px">
                    <tr>
                        <td>
                            <label class="control-label" for="t951Tanggal">
                                <g:message code="historyPrint.tanggal.label" default="Tanggal" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="filter_t951Tanggal" class="controls">
                                <ba:datePicker id="search_t951Tanggal" name="search_t951Tanggal" precision="day" format="dd/MM/yyyy"  value=""  />
                                &nbsp;&nbsp;&nbsp;s.d.&nbsp;&nbsp;&nbsp;
                                <ba:datePicker id="search_t951TanggalAkhir" name="search_t951TanggalAkhir" precision="day" format="dd/MM/yyyy"  value=""  />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label" for="userRole">
                                <g:message code="historyPrint.role.label" default="Role" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>

                            <div id="filter_role" class="controls">
                                <g:select style="width:500px" id="userRole" name="userRole" onchange="changeRole();" from="${Role.createCriteria().list(){eq("staDel","0");order("authority")}}" optionKey="id" value="" class="many-to-one" noSelection="['-1':'Pilih Role']"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label" for="userProfile">
                                <g:message code="historyPrint.userProfile.label" default="User Profile" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="filter_userProfile" class="controls">
                                <g:textField style="width:480px;"  name="userProfile" id="userProfile" disabled=""  class="typeahead" value="" autocomplete="off"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label" for="namaDokumen">
                                <g:message code="historyPrint.namaDokumen.label" default="Nama Dokumen" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="filter_namaDokumen" class="controls">
                                <g:select style="width:500px; align-items:flex-end;align-content: flex-end;align-self:flex-end" id="namaDokumen" name="namaDokumen.id" from="${NamaDokumen.createCriteria().list(){eq("staDel","0");order("m007NamaDokumen")}}" optionKey="id" value="" class="many-to-one"  noSelection="['':'Pilih Nama Dokumen']"/>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" >
                            <div class="controls" style="right: 0">
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn-primary view" name="view" id="view" >View</button>
                                &nbsp;&nbsp;
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn-primary editable-cancel" name="clear" id="clear" >Clear</button>
                            </div>
                        </td>
                    </tr>
                </table>
            </fieldset>
			<g:render template="dataTables" />
		</div>
		<div class="span7" id="historyPrint-form" style="display: none;"></div>
	</div>
</body>
</html>
