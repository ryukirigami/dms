<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript>
var inspectionDuringGrSubTable_${idTable};
var inspectionDuringGrSubTable2_${idTable};
$(function(){ 
var anOpen = [];
	$('#inspectionDuringGr_datatables_sub_${idTable} td.subcontrol').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
  		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = inspectionDuringGrSubTable_${idTable}.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST', 
	   			data : oData,
	   			url:'${request.contextPath}/inspectionDuringGr/subsublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = inspectionDuringGrSubTable_${idTable}.fnOpen(nTr,data,'details');
    				$('div.innerinnerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
    		
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerinnerDetails', $(nTr).next()[0]).slideUp( function () {
      			inspectionDuringGrSubTable_${idTable}.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

    $('#inspectionDuringGr_datatables_sub_${idTable} a.edit').live('click', function (e) {
        e.preventDefault();
         
        var nRow = $(this).parents('tr')[0];
         
        if ( nEditing !== null && nEditing != nRow ) {
            restoreRow( inspectionDuringGrSubTable_${idTable}, nEditing );
            editRow( inspectionDuringGrSubTable_${idTable}, nRow );
            nEditing = nRow;
        }
        else if ( nEditing == nRow && this.innerHTML == "Save" ) {
            saveRow( inspectionDuringGrSubTable_${idTable}, nEditing );
            nEditing = null;
        }
        else {
            editRow( inspectionDuringGrSubTable_${idTable}, nRow );
            nEditing = nRow;
        }
    } );
    if(inspectionDuringGrSubTable_${idTable})
    	inspectionDuringGrSubTable_${idTable}.dataTable().fnDestroy();
    inspectionDuringGrSubTable_${idTable} = $('#inspectionDuringGr_datatables_sub_${idTable}').dataTable({
		"sScrollX": "1195px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "reception",
	"mDataProp": "operation",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select2" aria-label="Row '+data+'" title="Select this"> <input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;' + data;
	},
	"bSortable": false,
	"sWidth":"600px",
	"bVisible": true
},
{
	"sName": "reception",
	"mDataProp": "foreman",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"500px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						aoData.push(
									{"name": 'noWo', "value": "${noWo}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	inspectionDuringGrSubTable2_${idTable} = $('#inspectionDuringGr2_datatables_sub_${idTable}').dataTable({
		"sScrollX": "1195px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubList2")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"15px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"15px",
               "sDefaultContent": ''
},
{
	"sName": "t508Keterangan",
	"mDataProp": "FI",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return ' <input type="hidden" value="'+data+'">&nbsp;&nbsp;' + data;
	},
	"bSortable": false,
	"sWidth":"300px",
	"bVisible": true
},
{
	"sName": "t508TglJamMulai",
	"mDataProp": "t508TglJamMulai",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "t508TglJamSelesai",
	"mDataProp": "t508TglJamSelesai",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "t508StaInspection",
	"mDataProp": "t508StaInspection",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"300px",
	"bVisible": true
},
{
	"sName": "t508Keterangan",
	"mDataProp": "t508Keterangan",
	"aTargets": [4],
	"bSortable": false,
	"sWidth":"400px",
	"bVisible": true
},
{
	"sName": "t508RedoKeProses_M190_ID",
	"mDataProp": "t508RedoKeProses_M190_ID",
	"aTargets": [5],
	"bSortable": false,
	"sWidth":"400px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						aoData.push(
									{"name": 'noWo', "value": "${noWo}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
    </g:javascript>
</head>
<body>
<div class="innerDetails">
    <table id="inspectionDuringGr_datatables_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
           class="display table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th></th>
            <th></th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div>Job</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 550px;">
                <div>Foreman</div>
            </th>

        </tr>
        </thead>
        <tbody></tbody>
    </table>
    <table id="inspectionDuringGr2_datatables_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
           class="display table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th></th>
            <th></th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div>Final Inspection</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Start</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Stop</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Lolos FI?</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Alasan</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Redo ke Job</div>
            </th>

        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
</body>
</html>
