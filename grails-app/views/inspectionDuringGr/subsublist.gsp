<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript>
var inspectionDuringGrSubTable_${idTable};
$(function(){
inspectionDuringGrSubTable_${idTable} = $('#inspectionDuringGr_datatables_sub_sub_${idTable}').dataTable({
		"sScrollX": "1198px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubSubList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": ''
},
{
	"sName": "t506M053_JobID_Redo",
	"mDataProp": "IDR",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;' + data ;
    %{--return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';--}%
        },
        "bSortable": false,
        "sWidth":"241px",
        "bVisible": true
    },
    {
        "sName": "t506TglJamMulai",
	    "mDataProp": "t506TglJamMulai",
        "aTargets": [1],
        "bSortable": false,
        "sWidth":"183px",
        "bVisible": true
    },
    {
        "sName": "t506TglJamSelesai",
        "mDataProp": "t506TglJamSelesai",
        "aTargets": [2],
        "bSortable": false,
        "sWidth":"183px",
        "bVisible": true
    },
    {
        "sName": "t506StaIDR",
        "mDataProp": "t506StaIDR",
        "aTargets": [3],
        "bSortable": false,
        "sWidth":"89px",
        "bVisible": true
    },
    {
        "sName": "t506AlasanIDR",
        "mDataProp": "t506AlasanIDR",
        "aTargets": [4],
        "bSortable": false,
        "sWidth":"150px",
        "bVisible": true
    },
    {
        "sName": "t506RedoKeProses_M190_ID",
        "mDataProp": "t506RedoKeProses_M190_ID",
        "aTargets": [5],
        "bSortable": false,
        "sWidth":"183px",
        "bVisible": true
    }

    ],
            "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                            aoData.push(
                                        {"name": 'noWo', "value": "${noWo}"},
                                        {"name": 'idJob', "value": "${idJob}"}

						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
    </g:javascript>
</head>
<body>
<div class="innerinnerDetails">
    <table id="inspectionDuringGr_datatables_sub_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
           class="display table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Inspect During Repair</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Start</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Stop</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Lolos IDR?</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Alasan</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Redo ke Job</div>
            </th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
</body>
</html>
