
<%@ page import="com.kombos.administrasi.Operation; com.kombos.administrasi.NamaProsesBP; com.kombos.parts.Returns" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'returnsAdd.label', default: 'Inspection During')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
        return false;
	});


   });
    $(document).ready(function()
    {
        $('#staRedo0').click(function(){
            var isChecked = $('#staRedo0').prop('checked');
            if(isChecked==true){
                $('#redoKeJob').prop('disabled',true);
            }
        });
        $('#staRedo1').click(function(){
            var isChecked = $('#staRedo1').prop('checked');
            if(isChecked==true){
                $('#redoKeJob').prop('disabled',false);
            }
        });

        editInspect = function(){
                var nomorWO = $('#nomorWO').val();
                var idJob = $('#idJob').val();
                var staIDR = $('input[name="staIDR"]:checked').val();
                var alasan = $('#alasan').val();
                var staRedo = $('input[name="staRedo"]:checked').val();
                var redoKeJob = $('#redoKeJob').val();
                var jamMulai = $('#jamMulai').val();
                $.ajax({
                    url:'${request.contextPath}/inspectionDuringGr/editInspect',
                    type: "POST", // Always use POST when deleting data
                    data : {idJob:idJob,noWo : nomorWO,staIDR : staIDR,alasan : alasan,staRedo : staRedo,
                    redoKeJob:redoKeJob,jamMulai:jamMulai},
                    success : function(data){
                        //window.location.href = '#/claim' ;
                        toastr.success('<div>Add Inspection During Repair Succes</div>');
                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });

        }
    });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
    <ul class="nav pull-right">
        <li></li>
        <li></li>
        <li class="separator"></li>
    </ul>
</div>
<div class="box">
    <fieldset>
        <div class="row-fluid">
                <table style="width: 95%;">
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Nomor WO" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="nomorWO" id="nomorWO" value="${noWo}" readonly="" />
                            <g:hiddenField style="width:100%" name="id" id="idJob" value="${idJob}" />
                            <g:hiddenField style="width:100%" name="jamMulai" id="jamMulai" value="${jamMulai}" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Nomor Polisi" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="nomorPolisi" value="${nopol}" readonly="" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Model Kendaraan" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="modelKendaraan" value="${model}" readonly=""/>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Nama Stall" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="modelKendaraan" value="${stall}" readonly=""/>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Teknisi"  />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="modelKendaraan" value="${nama}" readonly=""/>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Tanggal Update" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="modelKendaraan" value="${tanggal}" readonly=""/>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Status IDR" />
                        </td>
                        <td>
                            <input type="radio" name="staIDR" value="1" required="true"  ${staIDR=='1'?'checked':''} />OK  &nbsp;
                            <input type="radio" name="staIDR" value="0" required="true"   ${staIDR=='0'?'checked':''}  />Not OK
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Alasan Status IDR" />
                        </td>
                        <td>
                            <g:textArea style="width:100%;resize: none" name="alasan" id="alasan" value="${alasan}"  />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Perlu Redo?" />
                        </td>
                        <td>
                            <input type="radio" name="staRedo" id="staRedo1" value="1"  ${staRedo=='1'?'checked':''} required="true"/> Ya &nbsp;
                            <input type="radio" name="staRedo" id="staRedo0" value="0"  ${staRedo=='0'?'checked':''} required="true"/> Tidak
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Redo Ke Job" />
                        </td>
                        <td>
                            <g:select name="kegiatan" id="redoKeJob" from="${jobRcp}" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px" colspan="2">
                            <button id='btn1' onclick="editInspect();" type="button" class="btn btn-cancel" style="width: 150px;">Send</button>
                            <button id='btn2' type="button" class="btn btn-cancel" style="width: 150px;">Close</button>
                        </td>
                    </tr>
                </table>
        </div>
    </fieldset>
    </div>
</div>
</body>
</html>

