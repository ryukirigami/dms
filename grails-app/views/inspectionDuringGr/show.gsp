
<%@ page import="com.kombos.parts.Returns" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'returns.label', default: 'Returns')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteReturns;

$(function(){ 
	deleteReturns=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/returns/delete/',
			data: { id: id },
   			success:function(data,textStatus){
  				reloadReturnsTable();
   				expandTableLayout('returns');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-returns" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="returns"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${returnsInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="returns.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${returnsInstance}" field="createdBy"
								url="${request.contextPath}/Returns/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadReturnsTable();" />--}%
							
								<g:fieldValue bean="${returnsInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${returnsInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="returns.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${returnsInstance}" field="dateCreated"
								url="${request.contextPath}/Returns/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadReturnsTable();" />--}%
							
								<g:formatDate date="${returnsInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${returnsInstance?.goods}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="goods-label" class="property-label"><g:message
					code="returns.goods.label" default="Goods" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="goods-label">
						%{--<ba:editableValue
								bean="${returnsInstance}" field="goods"
								url="${request.contextPath}/Returns/updatefield" type="text"
								title="Enter goods" onsuccess="reloadReturnsTable();" />--}%
							
								%{--<g:link controller="#" action="#" id="${returnsInstance?.goods?.id}">${returnsInstance?.goods?.encodeAsHTML()}</g:link>--}%
                                <g:fieldValue bean="${returnsInstance?.goods}" field="m111Nama"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${returnsInstance?.goodsReceive}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="goodsReceive-label" class="property-label"><g:message
					code="returns.goodsReceive.label" default="Goods Receive" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="goodsReceive-label">
						%{--<ba:editableValue
								bean="${returnsInstance}" field="goodsReceive"
								url="${request.contextPath}/Returns/updatefield" type="text"
								title="Enter goodsReceive" onsuccess="reloadReturnsTable();" />--}%
							
								%{--<g:link controller="goodsReceive" action="show" id="${returnsInstance?.goodsReceive?.id}">${returnsInstance?.goodsReceive?.encodeAsHTML()}</g:link>--}%
                            <g:fieldValue bean="${returnsInstance?.goodsReceive}" field="t167ID"/>
						</span></td>
					
				</tr>
				</g:if>
			

				<g:if test="${returnsInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="returns.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${returnsInstance}" field="lastUpdProcess"
								url="${request.contextPath}/Returns/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadReturnsTable();" />--}%
							
								<g:fieldValue bean="${returnsInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${returnsInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="returns.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${returnsInstance}" field="lastUpdated"
								url="${request.contextPath}/Returns/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadReturnsTable();" />--}%
							
								<g:formatDate date="${returnsInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>

				<g:if test="${returnsInstance?.t172ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t172ID-label" class="property-label"><g:message
					code="returns.t172ID.label" default="Nomor Return" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t172ID-label">
						%{--<ba:editableValue
								bean="${returnsInstance}" field="t172ID"
								url="${request.contextPath}/Returns/updatefield" type="text"
								title="Enter t172ID" onsuccess="reloadReturnsTable();" />--}%
							
								<g:fieldValue bean="${returnsInstance}" field="t172ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
			

				<g:if test="${returnsInstance?.t172PetugasReturn}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t172PetugasReturn-label" class="property-label"><g:message
					code="returns.t172PetugasReturn.label" default="Petugas Return" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t172PetugasReturn-label">
						%{--<ba:editableValue
								bean="${returnsInstance}" field="t172PetugasReturn"
								url="${request.contextPath}/Returns/updatefield" type="text"
								title="Enter t172PetugasReturn" onsuccess="reloadReturnsTable();" />--}%
							
								<g:fieldValue bean="${returnsInstance}" field="t172PetugasReturn"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${returnsInstance?.t172Qty1Return}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t172Qty1Return-label" class="property-label"><g:message
					code="returns.t172Qty1Return.label" default="Qty1 Return" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t172Qty1Return-label">
						%{--<ba:editableValue
								bean="${returnsInstance}" field="t172Qty1Return"
								url="${request.contextPath}/Returns/updatefield" type="text"
								title="Enter t172Qty1Return" onsuccess="reloadReturnsTable();" />--}%
							
								<g:fieldValue bean="${returnsInstance}" field="t172Qty1Return"/>
							
						</span></td>
					
				</tr>
				</g:if>
			

			
				<g:if test="${returnsInstance?.t172TglJamReturn}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t172TglJamReturn-label" class="property-label"><g:message
					code="returns.t172TglJamReturn.label" default="Tgl Return" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t172TglJamReturn-label">
						%{--<ba:editableValue
								bean="${returnsInstance}" field="t172TglJamReturn"
								url="${request.contextPath}/Returns/updatefield" type="text"
								title="Enter t172TglJamReturn" onsuccess="reloadReturnsTable();" />--}%
							
								<g:formatDate date="${returnsInstance?.t172TglJamReturn}" />
							
						</span></td>
					
				</tr>
				</g:if>
			

			
				<g:if test="${returnsInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="returns.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${returnsInstance}" field="updatedBy"
								url="${request.contextPath}/Returns/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadReturnsTable();" />--}%
							
								<g:fieldValue bean="${returnsInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${returnsInstance?.vendor}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="vendor-label" class="property-label"><g:message
					code="returns.vendor.label" default="Vendor" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="vendor-label">
						%{--<ba:editableValue
								bean="${returnsInstance}" field="vendor"
								url="${request.contextPath}/Returns/updatefield" type="text"
								title="Enter vendor" onsuccess="reloadReturnsTable();" />--}%
							
								%{--<g:link controller="vendor" action="show" id="${returnsInstance?.vendor?.id}">${returnsInstance?.vendor?.encodeAsHTML()}</g:link>--}%
                                    <g:fieldValue bean="${returnsInstance?.vendor}" field="m121Nama"/>
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('returns');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${returnsInstance?.id}"
					update="[success:'returns-form',failure:'returns-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteReturns('${returnsInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
