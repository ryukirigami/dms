
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay" />

<table id="inspectionDuringGr_datatables_${idTable}" cellpadding="0" cellspacing="0" border="0"
       class="display table table-striped table-bordered table-hover" style="table-layout: fixed; ">
    <thead>
    <tr>
        <th style="border-bottom: none;width: 15px;"></th>

        <th style="border-bottom: none; padding: 5px;">
            <div>Nomor Wo</div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div>Nomor Polisi</div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div>Tanggal Wo</div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div>SA</div>
        </th>

    </tr>

    <tr>

        <th style="border-top: none; width: 15px;">
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_noWO" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_noWO" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_noPol" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_noPol" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;"></th>

        <th style="border-top: none;padding: 5px;"></th>
    </tr>

    </thead>
</table>

<g:javascript>
var inspectionDuringGrTable;
var reloadInspectionDuringGrTable;
$(function(){

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	inspectionDuringGrTable.fnDraw();
		}
	});

reloadInspectionDuringGrTable = function() {
		inspectionDuringGrTable.fnDraw();
	}
 $('#clear').click(function(e){
        $('#search_t017Tanggal').val("");
        $('#search_t017Tanggal_day').val("");
        $('#search_t017Tanggal_month').val("");
        $('#search_t017Tanggal_year').val("");
        $('#search_t017Tanggal2').val("");
        $('#search_t017Tanggal2_day').val("");
        $('#search_t017Tanggal2_month').val("");
        $('#search_t017Tanggal2_year').val("");

	});
    $('#view').click(function(e){
        e.stopPropagation();
		inspectionDuringGrTable.fnDraw();
	});
	var anOpen = [];
	$('#inspectionDuringGr_datatables_${idTable} td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = inspectionDuringGrTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST', 
	   			data : oData,
	   			url:'${request.contextPath}/inspectionDuringGr/sublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = inspectionDuringGrTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
    		
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			inspectionDuringGrTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});
	
    $('#inspectionDuringGr_datatables_${idTable} a.edit').live('click', function (e) {
        e.preventDefault();
         
        var nRow = $(this).parents('tr')[0];
         
        if ( nEditing !== null && nEditing != nRow ) {
            restoreRow( inspectionDuringGrTable, nEditing );
            editRow( inspectionDuringGrTable, nRow );
            nEditing = nRow;
        }
        else if ( nEditing == nRow && this.innerHTML == "Save" ) {
            saveRow( inspectionDuringGrTable, nEditing );
            nEditing = null;
        }
        else {
            editRow( inspectionDuringGrTable, nRow );
            nEditing = nRow;
        }
    } );
    
	reloadInspectionDuringGrTable = function() {
		inspectionDuringGrTable.fnDraw();
	}

 	inspectionDuringGrTable = $('#inspectionDuringGr_datatables_${idTable}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "sClass": "control center",
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "reception",
	"mDataProp": "noWo",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"> <input type="hidden" value="'+data+'">&nbsp;&nbsp;' + data;
	},
	"bSortable": false,
	"sWidth":"320px",
	"bVisible": true
},
{
	"sName": "reception",
	"mDataProp": "noPol",
	"aTargets": [1] ,
	"bSortable": false,
	"sWidth":"300px",
	"bVisible": true
},
{
	"sName": "reception",
	"mDataProp": "tglWo",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "reception",
	"mDataProp": "SA",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"300px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                        var noWO = $('#filter_noWO input').val();
						if(noWO){
							aoData.push(
									{"name": 'sCriteria_noWO', "value": noWO}
							);
						}

                        var noPol = $('#filter_noPol input').val();
						if(noPol){
							aoData.push(
									{"name": 'sCriteria_noPol', "value": noPol}
							);
						}

                        var cekTglWo = $('#cekTglWo').prop('checked');
                        if(cekTglWo){
                          aoData.push(
									{"name": 'cekTglWo', "value": cekTglWo}
							);
                        }

                        var cekRedoJob = $('#cekRedoJob').prop('checked');
                        if(cekRedoJob){
                          aoData.push(
									{"name": 'cekRedoJob', "value": cekRedoJob}
							);
                        }

                        var cekInspec = $('#cekInspec').prop('checked');
                        if(cekInspec){
                          aoData.push(
									{"name": 'cekInspec', "value": cekInspec}
							);
                        }

                        var cekLolosIdr = $('#cekLolosIdr').prop('checked');
                        if(cekLolosIdr){
                          aoData.push(
									{"name": 'cekLolosIdr', "value": cekLolosIdr}
							);
                        }

                        var cekFinalIns = $('#cekFinalIns').prop('checked');
                        if(cekFinalIns){
                          aoData.push(
									{"name": 'cekFinalIns', "value": cekFinalIns}
							);
                        }

                        var cekLolosFI = $('#cekLolosFI').prop('checked');
                        if(cekLolosFI){
                          aoData.push(
									{"name": 'cekLolosFI', "value": cekLolosFI}
							);
                        }

                        var cekSA = $('#cekSA').prop('checked');
                        if(cekSA){
                          aoData.push(
									{"name": 'cekSA', "value": cekSA}
							);
                        }

                        var cekForeman = $('#cekForeman').prop('checked');
                        if(cekForeman){
                          aoData.push(
									{"name": 'cekForeman', "value": cekForeman}
							);
                        }

                        var t017Tanggal = $('#search_t017Tanggal').val();
						var t017TanggalDay = $('#search_t017Tanggal_day').val();
						var t017TanggalMonth = $('#search_t017Tanggal_month').val();
						var t017TanggalYear = $('#search_t017Tanggal_year').val();

						var t017Tanggal2 = $('#search_t017Tanggal2').val();
						var t017TanggalDay2 = $('#search_t017Tanggal2_day').val();
						var t017TanggalMonth2 = $('#search_t017Tanggal2_month').val();
						var t017TanggalYear2 = $('#search_t017Tanggal2_year').val();

						if(t017Tanggal){
							aoData.push(
									{"name": 'sCriteria_t017Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_t017Tanggal_dp', "value": t017Tanggal},
									{"name": 'sCriteria_t017Tanggal_day', "value": t017TanggalDay},
									{"name": 'sCriteria_t017Tanggal_month', "value": t017TanggalMonth},
									{"name": 'sCriteria_t017Tanggal_year', "value": t017TanggalYear}
							);
						}


						if(t017Tanggal2){
							aoData.push(
									{"name": 'sCriteria_t017Tanggal2', "value": "date.struct"},
									{"name": 'sCriteria_t017Tanggal2_dp', "value": t017Tanggal2},
									{"name": 'sCriteria_t017Tanggal2_day', "value": t017TanggalDay2},
									{"name": 'sCriteria_t017Tanggal2_month', "value": t017TanggalMonth2},
									{"name": 'sCriteria_t017Tanggal2_year', "value": t017TanggalYear2}
							);
						}
	                   var staRedo = $('#staRedo').val();
                        if(staRedo){
                          aoData.push(
									{"name": 'staRedo', "value": staRedo}
							);
                        }

                        var lolosIDR = $('#lolosIDR').val();
                        if(lolosIDR){
                          aoData.push(
									{"name": 'lolosIDR', "value": lolosIDR}
							);
                        }

                        var inspect = $('#inspect').val();
                        if(inspect){
                          aoData.push(
									{"name": 'inspect', "value": inspect}
							);
                        }

                        var lolosFI = $('#lolosFI').val();
                        if(lolosFI){
                          aoData.push(
									{"name": 'lolosFI', "value": lolosFI}
							);
                        }

                        var final = $('#final').val();
                        if(final){
                          aoData.push(
									{"name": 'final', "value": final}
							);
                        }

                        var sa = $('#sa').val();
                        if(sa){
                          aoData.push(
									{"name": 'sa', "value": sa}
							);
                        }

                        var foreman = $('#foreman').val();
                        if(foreman){
                          aoData.push(
									{"name": 'foreman', "value": foreman}
							);
                        }
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});



</g:javascript>



