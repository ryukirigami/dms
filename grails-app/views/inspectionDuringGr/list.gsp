<%@ page import="com.kombos.hrd.Karyawan; com.kombos.administrasi.NamaManPower; com.kombos.reception.Reception; com.kombos.parts.Returns" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="title" value="Inspection During Repair & Inspection List" />
    <title>${title}</title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;
	var printReturns;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){

	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
                                   onLoading="jQuery('#spinner').fadeIn(1);"
                                   onSuccess="loadForm(data, textStatus);"
                                   onComplete="jQuery('#spinner').fadeOut();" />
        break;
    case '_DELETE_' :
        bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});

         		break;
       }
       return false;
	});
        
        //darisini
            $("#inspecAddModal").on("show", function() {
                $("#inspecAddModal .btn").on("click", function(e) {
                    $("#inspecAddModal").modal('hide');
                });
            });
            $("#inspecAddModal").on("hide", function() {
                $("#inspecAddModal a.btn").off("click");
            });
        inspecDuring = function() {
            checkInspecDuring =[];
            var nRow = "";
             var idCheckJobForTeknisi="";
            $("#inspecDuring-table tbody .row-select2").each(function() {
                 if(this.checked){
                   nRow = $(this).next("input:hidden").val();
				  checkInspecDuring.push(nRow);
                  }
              });
               if(checkInspecDuring.length<1 || checkInspecDuring.length>1 ){
                    alert('Silahkan Pilih Salah satu Operation');
                    return;
               }else{

                   $("#inspecAddContent").empty();
                    expandTableLayout();
                    $.ajax({type:'POST', url:'${request.contextPath}/inspectionDuringGr/inspecDuring',
                    data : {idJob : nRow},
                        success:function(data,textStatus){
                                $("#inspecAddContent").html(data);
                                $("#inspecAddModal").modal({
                                    "backdrop" : "static",
                                    "keyboard" : true,
                                    "show" : true
                                }).css({'width': '800px','margin-left': function () {return -($(this).width() / 2);}});

                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
               }
         };


    finalInspection = function() {
            checkInspecDuring =[];
            var nRow = "";

            var idCheckJobForTeknisi="";
            $("#inspecDuring-table tbody .row-select").each(function() {
                 if(this.checked){
                   nRow = $(this).next("input:hidden").val();

				  checkInspecDuring.push(nRow);
                  }
              });
               if(checkInspecDuring.length<1 || checkInspecDuring.length>1 ){

                    alert('Silahkan Pilih Salah satu WO');
                    return;

               }else{
                    console.log("nRow=" + nRow);
                    shrinkTableLayout();
                    $('#spinner').fadeIn(1);
                    $.ajax({type:'POST', url:'${request.contextPath}/woDetailGr/finalInspection',
                    data : {noWo : nRow},
                        success:function(data,textStatus){
                            loadForm(data, textStatus);
                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
            }
        };
    loadForm = function(data, textStatus){
		$('#inspecDuring-form').empty();
    	$('#inspecDuring-form').append(data);
   	}

    shrinkTableLayout = function(){
    	$("#inspecDuring-table").hide()
        $("#inspecDuring-form").css("display","block");
   	}

   	expandTableLayout = function(){
        $("#inspecDuring-table").show()
        $("#inspecDuring-form").css("display","none");
   	}

    woDetail = function(){
           checkInspecDuring =[];
            var idCheckJobForTeknisi="";

            $("#inspecDuring-table tbody .row-select").each(function() {
                 if(this.checked){
                    var nRow = $(this).next("input:hidden").val();
					checkInspecDuring.push(nRow);
                    idCheckInspecDuring =  JSON.stringify(checkInspecDuring);
                     window.location.replace('#/WoDetailGr');
					    $.ajax({
                            url:'${request.contextPath}/WoDetailGr/list',
                            type: "POST",
                            data: { id: nRow, aksi : 'inspect' },
                            dataType: "html",
                            complete : function (req, err) {
                                $('#main-content').html(req.responseText);
                                $('#spinner').fadeOut();
                                loading = false;
                            }
                        });
                }
            });
            if(checkInspecDuring.length<1 || checkInspecDuring.length>1 ){
                alert('Silahkan Pilih Salah satu WO');
                return;

            }

    }

});
        var checkin = $('#search_t017Tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
        }).on('changeDate', function(ev) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
            checkin.hide();
            $('#search_t017Tanggal2')[0].focus();
        }).data('datepicker');

        var checkout = $('#search_t017Tanggal2').datepicker({
            onRender: function(date) {
                return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');




    function clearForm(){

        document.getElementById("cekRedoJob").checked = false;
       document.getElementById("cekTglWo").checked = false;
       document.getElementById("cekInspec").checked = false;
       document.getElementById("cekLolosIdr").checked = false;
       document.getElementById("cekFinalIns").checked = false;
       document.getElementById("cekLolosFI").checked = false;
       document.getElementById("cekSA").checked = false;
   $('#cekSA').val('')
     $('#cekForeman').val('')
       document.getElementById("cekForeman").checked = false;

        reloadInspectionDuringGrTable();
    }
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left">${title}</span>
    <ul class="nav pull-right">

    </ul>
</div>
<div class="box">
    <div class="span12" id="inspecDuring-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <fieldset>
            <table style="padding-right: 10px">
                <tr>
                    <td style="width: 130px">
                           <g:message code="auditTrail.tanggal.label" default="Tanggal WO" /> &nbsp;
                    </td>
                    <td style="width: 30px;">
                        <g:checkBox name="cekTglWo" id="cekTglWo" /> &nbsp;
                    </td>
                    <td>
                        <div id="filter_m777Tgl" class="controls">
                            <ba:datePicker id="search_t017Tanggal" name="search_t017Tanggal" precision="day" format="dd/MM/yyyy"  value="" />
                           &nbsp;s.d.&nbsp;
                            <ba:datePicker id="search_t017Tanggal2" name="search_t017Tanggal2" precision="day" format="dd/MM/yyyy"  value=""  />
                        </div>
                    </td>
                    <td style="width: 130px; padding-left:30px;">
                            <g:message code="auditTrail.tanggal.label" default="Redo Job" />  &nbsp;
                    </td>
                    <td style="width: 30px;">
                        <g:checkBox name="cekRedoJob" id="cekRedoJob" />  &nbsp;
                    </td>
                    <td>
                        <div id="filter_redo" class="controls">
                            <select name="status" id="staRedo" >
                                <option value="1">Ada Redo Job</option>
                                <option value="0">Tidak Ada Redo Job</option>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 130px">

                            <g:message code="auditTrail.tanggal.label" default="Inspection During" /> &nbsp;

                    </td>
                    <td>
                        <g:checkBox name="cekInspec" id="cekInspec" />
                    </td>
                    <td>
                        <div id="filter_inspec" class="controls">
                            <select name="inspect" id="inspect" style="width: 100%">
                                <option value="1">Sudah Inspection During Repair</option>
                                <option value="0">Belum Inspection During Repair</option>
                            </select>
                        </div>
                    </td>
                    <td style="width: 130px; padding-left:30px;">

                            <g:message code="auditTrail.tanggal.label" default="Lolos IDR" /> &nbsp;

                    </td>
                    <td>
                        <g:checkBox name="cekLolosIdr" id="cekLolosIdr" />
                    </td>
                    <td>
                        <div id="filter_lolos" class="controls">
                            <select name="lolosIDR" id="lolosIDR">
                                <option value="1">Lolos IDR</option>
                                <option value="0">Tidak Lolos IDR</option>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 130px">

                            <g:message code="auditTrail.tanggal.label" default="Final Inspection" /> &nbsp;

                    </td>
                    <td>
                        <g:checkBox name="cekFinalIns" id="cekFinalIns" />
                    </td>
                    <td>
                        <div id="filter_final" class="controls">
                            <select name="final" id="final" style="width: 100%">>
                                <option value="1">Sudah Final Inspection</option>
                                <option value="0">Belum Final Inspection</option>
                            </select>
                        </div>
                    </td>
                    <td style="width: 130px; padding-left:30px;">

                            <g:message code="auditTrail.tanggal.label" default="Lolos FI" /> &nbsp;

                    </td>
                    <td>
                        <g:checkBox name="cekLolosFI" id="cekLolosFI" />
                    </td>
                    <td>
                        <div id="filter_lolosFI" class="controls">
                            <select name="lolosFI" id="lolosFI">
                                <option value="1">Lolos FI</option>
                                <option value="0">Tidak Lolos FI</option>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 130px">
                            <g:message code="auditTrail.tanggal.label" default="SA" /> &nbsp;
                    </td>
                    <td>
                        <g:checkBox name="cekSA" id="cekSA" />
                    </td>
                    <td>
                        %{--<g:select name="sa" from="${Reception.list()}" id="sa" optionValue="t401NamaSA" noSelection="['-':'( Semua )']" style="width:100%;" />--}%
                        <g:select style="width:100%" name="sa" id="sa" noSelection="['':'Pilih SA']" from="${Karyawan.createCriteria().list { eq("staDel","0")jabatan{eq("m014Inisial","SAGR")}}}" optionValue="nama" />
                    </td>
                    <td style="width: 130px; padding-left:30px;">
                            <g:message code="auditTrail.tanggal.label" default="Foreman" /> &nbsp;
                    <td>
                        <g:checkBox name="cekForeman" id="cekForeman" />
                    </td>
                    <td>
                        <div id="filter_status" class="controls">
                            <g:select name="foreman" id="foreman"  noSelection="['':'( Semua ) ']" from="${Karyawan.createCriteria().list  {eq("staDel","0") jabatan{eq("m014Inisial","FOGR")}}}" optionValue="nama" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" >
                        <div class="controls" style="right: 0">
                            <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-cancel" name="view" id="view" >Filter</button>
                            <button style="width: 110px;height: 30px; border-radius: 5px" class="btn btn-cancel" name="clear" id="clear" onclick="clearForm()">Clear Filter</button>

                        </div>
                    </td>
                </tr>
            </table>
        </fieldset>
        <g:render template="dataTables" />
        <fieldset class="buttons controls" style="padding-top: 10px;">
            <button id='wodetail' onclick="woDetail();" type="button" class="btn btn-cancel" style="width: 150px;">WO Details</button>
            <button id='btn' onclick="inspecDuring();" type="button" class="btn btn-cancel" style="width: 200px;">Inspection During Repair</button>
            <button id='btn2' onclick="finalInspection();" type="button" class="btn btn-cancel" style="width: 150px;">Final Inspection</button>
        </fieldset>

    </div>
    <div class="span7" id="inspecDuring-form" style="display: none; width:95%;"></div>
</div>
<div id="inspecAddModal" class="modal fade"style="width: 750px;">
    <div class="modal-dialog" style="width: 750px;">
        <div class="modal-content" style="width: 750px;">
            <!-- dialog body -->
            <div class="modal-body" style="max-height: 800px; width: 750px;">
                <div id="inspecAddContent"/>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="iu-content"></div>
            </div>

        </div>
    </div>
</div>
</body>
</html>


