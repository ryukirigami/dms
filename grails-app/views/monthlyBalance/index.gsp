<%--
  Created by IntelliJ IDEA.
  User: Ahmad Fawaz
  Date: 25/02/15
  Time: 13:56
--%>

<%@ page import="com.kombos.finance.MonthlyBalance" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="montlyBalance.label" default="Monthly Balance" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
        $(function(){
            $('#accountNumber').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/franc/listAccount', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
        })

        function doSearch(){
            var peBulan = $("#bulanPeriode").val();
            var peTahun = $("#tahunPeriode").val();
            $("#staAwal").val("tidak");
            if(peBulan=="" || peTahun==""){
                alert("Data belum lengkap")
                return
            }
            $.ajax({
                    url:'${request.contextPath}/monthlyBalance/getJumlah',
                    type: "POST", // Always use POST when deleting data
                    data: { bulan: peBulan,tahun : peTahun},
                    success : function(data,item){
                        document.getElementById('saldoAwal').innerHTML = data[0].startingBalance;
                        document.getElementById('debitMutasi').innerHTML = data[0].debetMutation;
                        document.getElementById('kreditMutasi').innerHTML = data[0].creditMutation;
                        document.getElementById('saldoAkhir').innerHTML = data[0].endingBalance;
                    }
            });
            reloadMonthlyBalanceTable();
        }

        function doRemove(){
            var peBulan = $("#bulanPeriode").val();
            var peTahun = $("#tahunPeriode").val();
            var cek = confirm("Apakah anda yakin akan melakukan proses ini?")
            if(cek){
                $.ajax({
                    url:'${request.contextPath}/monthlyBalance/doRemove',
                    type: "POST", // Always use POST when deleting data
                    data: { bulan: peBulan,tahun : peTahun},
                    success : function(data){
                        if(data=="ok"){
                            alert("Hapus Monthly Balance Selesai");
                            doSearch();
                        }else{
                            alert("Proses Gagal, Monthly Balance bulan sebelumnya belum ada");
                        }
                    },
                    error: function(xhr, status) {
                        alert("Internal server error");
                    }
                });
            }
        }
        function doProses(){
            var peBulan = $("#bulanPeriode").val();
            var peTahun = $("#tahunPeriode").val();
            var akun = $("#accountNumber").val();
            if(peBulan=="" || peTahun==""){
                alert("Data belum lengkap")
                return
            }

            var cek = confirm("Apakah anda yakin akan melakukan proses ini?")
            if(cek){
                $.ajax({
                    url:'${request.contextPath}/monthlyBalance/doSave',
                    type: "POST", // Always use POST when deleting data
                    data: { bulan: peBulan,tahun : peTahun, akun : akun },
                    success : function(data){
                        if(data=="ok"){
                            alert("Proses Monthly Balance Selesai");
                        }else{
                            alert("Proses Gagal, Monthly Balance bulan sebelumnya belum ada");
                        }
                    },
                    error: function(xhr, status) {
                        alert("Internal server error");
                    }
                });
            }
        }
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="montlyBalance.label" default="Monthly Balance" /></span>
</div>
<div class="box">
    <div class="span12" id="monthlyBalance-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <table style="margin-left: 30px">
            <tr>
                <td style="padding-right: 10px">Periode</td>
                <td>
                    <select name="bulanPeriode" id="bulanPeriode" value="${skrg?.format("MM")}" >
                        <option value="01" ${skrg?.format("MM")=="01" ? "selected" : ""}>Januari</option>
                        <option value="02" ${skrg?.format("MM")=="02" ? "selected" : ""}>Februari</option>
                        <option value="03" ${skrg?.format("MM")=="03" ? "selected" : ""}>Maret</option>
                        <option value="04" ${skrg?.format("MM")=="04" ? "selected" : ""}>April</option>
                        <option value="05" ${skrg?.format("MM")=="05" ? "selected" : ""}>Mei</option>
                        <option value="06" ${skrg?.format("MM")=="06" ? "selected" : ""}>Juni</option>
                        <option value="07" ${skrg?.format("MM")=="07" ? "selected" : ""}>Juli</option>
                        <option value="08" ${skrg?.format("MM")=="08" ? "selected" : ""}>Agustus</option>
                        <option value="09" ${skrg?.format("MM")=="09" ? "selected" : ""}>September</option>
                        <option value="10" ${skrg?.format("MM")=="10" ? "selected" : ""}>Oktober</option>
                        <option value="11" ${skrg?.format("MM")=="11" ? "selected" : ""}>Nomvember</option>
                        <option value="12" ${skrg?.format("MM")=="12" ? "selected" : ""}>Desember</option>
                    </select> -
                    <g:textField name="tahunPeriode" id="tahunPeriode" value="${skrg.format("yyyy")}" maxlength="4" />
                </td>
            </tr>
            <tr>
                <td style="padding-right: 10px">Nomor Akun</td>
                <td>
                    <g:textField name="accountNumber" id="accountNumber" style="width:97%" class="typeahead" autocomplete="off"/>
                    <g:hiddenField name="staAwal" id="staAwal" value="ya" />
                </td>
            </tr>
            <tr>
                <td style="padding-right: 10px">SubType</td>
                <td>
                    <g:select id="subType" name="subType" style="width: 97%" from="${com.kombos.finance.SubType.createCriteria().list {eq("staDel","0");order("description")}}" noSelection="['':'--Pilih SubType--']" optionKey="id" optionValue="description" class="many-to-one"/>
                </td>
            </tr>
            <tr>
                <td style="padding-right: 10px">Subledger</td>
                <td>
                    <g:textField name="subledger" id="subledger" style="width:100%" class="typeahead" autocomplete="off"/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <button class="btn btn-primary" onclick="doProses();">Proses</button>
                    <button class="btn btn-primary" onclick="doSearch();">View</button>
                    <button class="btn btn-cancel" onclick="doRemove();">Remove</button>
                </td>
            </tr>
        </table>
        <br/>
        <g:render template="dataTables" />
        %{--<g:render template="dataTables" />--}%
    </div>
    <div class="span7" id="monthlyBalance-form" style="display: none;"></div>
</div>
</body>
</html>