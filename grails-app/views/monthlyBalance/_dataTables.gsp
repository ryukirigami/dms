
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="monthlyBalance_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="monthlyBalance.periode.label" default="Periode" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="monthlyBalance.accountNumber.label" default="Nomor Akun" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="monthlyBalance.accountName.label" default="Nama Akun" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="monthlyBalance.startingBalance.label" default="Saldo Awal" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="monthlyBalance.debetMutation.label" default="Mutasi Debet" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="monthlyBalance.creditMutation.label" default="Mutasi Kredit" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="monthlyBalance.endingBalance.label" default="Saldo Akhir" /></div>
        </th>

    </tr>
    </thead>
    <tfoot>
            <tr>
                <th style="border-bottom: none;padding: 5px;" />
                <th style="border-bottom: none;padding: 5px;" />
                <th style="border-bottom: none;padding: 5px;" />
                <th><label class="pull-right" id="saldoAwal"></label></th>
                <th><label class="pull-right" id="debitMutasi"></label></th>
                <th><label class="pull-right" id="kreditMutasi"></label></th>
                <th><label class="pull-right" id="saldoAkhir"></label></th>
            </tr>
    </tfoot>
</table>

<g:javascript>
var MonthlyBalanceTable;
var reloadMonthlyBalanceTable;
$(function(){
	
	reloadMonthlyBalanceTable = function() {
		MonthlyBalanceTable.fnDraw();
	}

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	MonthlyBalanceTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	MonthlyBalanceTable = $('#monthlyBalance_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
        "fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },

		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : 1000, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "periode",
	"mDataProp": "periode",
	"aTargets": [1],
//        "mRender": function ( data, type, row ) {
//		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
//	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"80px",
	"bVisible": true
}

,

{
	"sName": "accountNumber",
	"mDataProp": "accountNumber",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}

,

{
	"sName": "accountName",
	"mDataProp": "accountName",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"450px",
	"bVisible": true
}

,

{
	"sName": "startingBalance",
	"mDataProp": "startingBalance",
	"aTargets": [4],
    "mRender": function ( data, type, row ) {
		return '<span class="pull-right">'+data+'</span>';
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
}

,

{
	"sName": "debetMutation",
	"mDataProp": "debetMutation",
	"aTargets": [5],
    "mRender": function ( data, type, row ) {
		return '<span class="pull-right">'+data+'</span>';
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
}

,

{
	"sName": "creditMutation",
	"mDataProp": "creditMutation",
	"aTargets": [6],
    "mRender": function ( data, type, row ) {
		return '<span class="pull-right">'+data+'</span>';
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
}

,

{
	"sName": "endingBalance",
	"mDataProp": "endingBalance",
	"aTargets": [7],
    "mRender": function ( data, type, row ) {
		return '<span class="pull-right sum">'+data+'</span>';
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        var periodeBulan = $("#bulanPeriode").val();
                        var periodeTahun = $("#tahunPeriode").val();
                        var akun = $("#accountNumber").val();
                        var subType = $("#subType").val();
                        var subledger = $("#subledger").val();

                        if(periodeBulan){
							aoData.push(
									{"name": 'periodeBulan', "value": periodeBulan}
							);
						}

						if(periodeTahun){
							aoData.push(
									{"name": 'periodeTahun', "value": periodeTahun}
							);
						}

						if(akun){
							aoData.push(
									{"name": 'akun', "value": akun}
							);
						}

                        if(subType){
							aoData.push(
									{"name": 'subType', "value": subType}
							);
						}

                        if(subledger){
							aoData.push(
									{"name": 'subledger', "value": subledger}
							);
						}

                        aoData.push(
                            {"name": 'staAwal', "value": $('#staAwal').val()}
                        );

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

});
</g:javascript>