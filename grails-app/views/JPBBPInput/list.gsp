
<%@ page import="com.kombos.board.JPB" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'JPB.label', default: 'JPB BP')}" />
		%{--<title><g:message code="default.list.label" args="[entityName]" /></title>--}%
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript disposition="head">
			var show;
			var edit;
			var getTeknisiInfo;
            var getForemanInfo;
            var getTeknisiInfoMonth;
            var getForemanInfoMonth;
            var getTeknisiInfoWeek;
            var getForemanInfoWeek;
            var getOnProgressInfo;
            var appId = "${idApp}";
            var receptId = "${idRecept}";
			$(function(){
                var staApp = "${staApp}";
                getForemanInfo = function(id){
                    var content = "nocontent";
                        $.ajax({type:'POST', url:'${request.contextPath}/groupManPower/getManPowerInfo/'+id,
                            success:function(data,textStatus){
                                if(data){

                                   content = data.namaLengkap + " - " + data.jabatan;
                                   content = content +"</br>Group : "+data.group;
                                   content = content +"</br>Anggota Group : </br>"

                                   if(data.anggota){
                                        jQuery.each(data.anggota, function (index, value) {
                                            content = content + "</br>"+value;
                                        });
                                   }


                                 $("#foreman"+id).tooltip({
                                    html : true,
                                    title : content,
                                    position: 'center right'
                                });

                                }

                            },
                            error:function(XMLHttpRequest,textStatus,errorThrown){},
                            complete:function(XMLHttpRequest,textStatus){

                            }
                });

    }

                  getTeknisiInfo = function(idTeknisi, idJPB){
    //  $(document).tooltip({ content: "Awesome title!" }).show();

         var content = "nocontent";
        $.ajax({type:'GET', url:'${request.contextPath}/groupManPower/getTeknisiJPB/',
            data : {id : idTeknisi, idJPB : idJPB},
   			success:function(data,textStatus){
   				if(data){

   				   content = data.namaLengkap + " - " + data.jabatan;
                   content = content +"</br>Group : "+data.group;
                   content = content +"</br>Kepala Group : "+data.kepalaGroup;
                   content = content +"</br></br>";
                   content = content +"</br>Previous Job : "+data.prevJob;
                   content = content +"</br>No. WO : "+data.prevNoWO;
                   content = content +"</br>"+data.prevTime;

                   content = content +"</br></br>Current Job : "+data.currJob;
                   content = content +"</br>No. WO : "+data.currNoWO;
                   content = content +"</br>"+data.currTime;

                   content = content +"</br></br>Next Job : "+data.nextJob;
                   content = content +"</br>No. WO : "+data.nextNoWO;
                   content = content +"</br>"+data.nextTime;




   				 $("#teknisi"+idTeknisi).tooltip({
                    html : true,
                    title : content,
                    position: 'center right'
                });

   				}

   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){

   			}
   		});

    }
        closeBillto = function (){
            loadPath('editJobParts/list?nowo='+receptId);
        }
     getForemanInfoMonth = function(id){
    //  $(document).tooltip({ content: "Awesome title!" }).show();

        var content = "nocontent";
            $.ajax({type:'POST', url:'${request.contextPath}/groupManPower/getManPowerInfo/'+id,
                success:function(data,textStatus){
                    if(data){

                       content = data.namaLengkap + " - " + data.jabatan;
                       content = content +"</br>Group : "+data.group;
                       content = content +"</br>Anggota Group : </br>"

                       if(data.anggota){
                            jQuery.each(data.anggota, function (index, value) {
                                content = content + "</br>"+value;
                            });
                       }


                     $("#foremanMonth"+id).tooltip({
                        html : true,
                        title : content,
                        position: 'center right'
                    });

                    }

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){

                }
            });

    }

          getTeknisiInfoMonth = function(idTeknisi, idJPB){
                     var content = "nocontent";
                    $.ajax({type:'GET', url:'${request.contextPath}/groupManPower/getTeknisiJPB/',
                        data : {id : idTeknisi, idJPB : idJPB},
                        success:function(data,textStatus){
                            if(data){

                               content = data.namaLengkap + " - " + data.jabatan;
                               content = content +"</br>Group : "+data.group;
                               content = content +"</br>Kepala Group : "+data.kepalaGroup;
                               content = content +"</br></br>";
                               content = content +"</br>Previous Job : "+data.prevJob;
                               content = content +"</br>No. WO : "+data.prevNoWO;
                               content = content +"</br>"+data.prevTime;

                               content = content +"</br></br>Current Job : "+data.currJob;
                               content = content +"</br>No. WO : "+data.currNoWO;
                               content = content +"</br>"+data.currTime;

                               content = content +"</br></br>Next Job : "+data.nextJob;
                               content = content +"</br>No. WO : "+data.nextNoWO;
                               content = content +"</br>"+data.nextTime;




                             $("#teknisiMonth"+idTeknisi).tooltip({
                                html : true,
                                title : content,
                                position: 'center right'
                            });

                            }

   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){

   			}
   		});

    }

              getForemanInfoWeek = function(id){
                    var content = "nocontent";
                        $.ajax({type:'POST', url:'${request.contextPath}/groupManPower/getManPowerInfo/'+id,
                            success:function(data,textStatus){
                                if(data){

                                   content = data.namaLengkap + " - " + data.jabatan;
                                   content = content +"</br>Group : "+data.group;
                                   content = content +"</br>Anggota Group : </br>"

                                   if(data.anggota){
                                        jQuery.each(data.anggota, function (index, value) {
                                            content = content + "</br>"+value;
                                        });
                                   }


                                 $("#foremanWeek"+id).tooltip({
                                    html : true,
                                    title : content,
                                    position: 'center right'
                                });

                                }

                            },
                            error:function(XMLHttpRequest,textStatus,errorThrown){},
                            complete:function(XMLHttpRequest,textStatus){

                            }
                        });

                }

                  getTeknisiInfoWeek = function(idTeknisi, idJPB){
                    //  $(document).tooltip({ content: "Awesome title!" }).show();

                     var content = "nocontent";
                     $.ajax({type:'GET', url:'${request.contextPath}/groupManPower/getTeknisiJPB/',
                    data : {id : idTeknisi, idJPB : idJPB},
                    success:function(data,textStatus){
                        if(data){

                           content = data.namaLengkap + " - " + data.jabatan;
                           content = content +"</br>Group : "+data.group;
                           content = content +"</br>Kepala Group : "+data.kepalaGroup;
                           content = content +"</br></br>";
                           content = content +"</br>Previous Job : "+data.prevJob;
                           content = content +"</br>No. WO : "+data.prevNoWO;
                           content = content +"</br>"+data.prevTime;

                           content = content +"</br></br>Current Job : "+data.currJob;
                           content = content +"</br>No. WO : "+data.currNoWO;
                           content = content +"</br>"+data.currTime;

                           content = content +"</br></br>Next Job : "+data.nextJob;
                           content = content +"</br>No. WO : "+data.nextNoWO;
                           content = content +"</br>"+data.nextTime;

                         $("#teknisiWeek"+idTeknisi).tooltip({
                            html : true,
                            title : content,
                            position: 'center right'
                        });

                        }

                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){},
                    complete:function(XMLHttpRequest,textStatus){

                    }
                });

    }


       getOnProgressInfo = function(idJPB, tag){

         var content = "nocontent";
        $.ajax({type:'GET', url:'${request.contextPath}/operation/getOnProgressOperation/',
            data : {bPOrGR : "0", idJPB : idJPB},
   			success:function(data,textStatus){
   				if(data){

   				   content = "No. WO : "+data.noWO;
                   content = content +"</br>No. Polisi : "+data.noPol;
                   content = content +"</br></br>Nama Job : "+data.namaJob;
                   content = content +"</br></br>Level : "+data.level;
                   content = content +"</br></br>Current Progress : "+data.currProgress;
                   content = content +"</br>Start : "+data.start;
                   content = content +"</br>Target Finish : "+data.targetFinish;
                   content = content +"</br>Delivery Time : "+data.deliveryTime;

   				 $("#jam"+tag+idJPB).tooltip({
                    html : true,
                    title : content,
                    position: 'center right'
                });

   				}

   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){

   			}
   		});

    }

			    $('#bp_per_hari').load('${request.contextPath}/JPBBPInput/perDay?staApp='+staApp+'&appointmentId='+appId+'&idReception='+receptId);

                $('#tab_bp_per_hari').click(function() {
                    $('#bp_per_hari').load('${request.contextPath}/JPBBPInput/perDay?staApp='+staApp+'&appointmentId='+appId+'&idReception='+receptId);
                });

                $('#tab_bp_per_minggu').click(function() {
                    $('#bp_per_minggu').load('${request.contextPath}/JPBBPInput/perWeek?staApp='+staApp+'&appointmentId='+appId+'&idReception='+receptId);
                });

                $('#tab_bp_per_bulan').click(function() {
                    $('#bp_per_bulan').load('${request.contextPath}/JPBBPInput/perMonth?staApp='+staApp+'&appointmentId='+appId+'&idReception='+receptId);
                });

                var reloadAll = function(){
                    var table = $('#JPB_datatables');
                    if(table.length > 0){
                        asbView();
                        setTimeout(function(){reloadAll()}, 10000);
                    } else {
                        var tableW = $('#JPB_datatablesPerWeek');
                        if(tableW.length > 0){
                            asbViewW();
                            setTimeout(function(){reloadAll()}, 10000);
                        } else {
                            var tableM = $('#JPB_datatablesPerMonth');
                            if(tableM.length > 0){
                                jpbView();
                                setTimeout(function(){reloadAll()}, 10000);
                            }
                        }
                    }
                }
                setTimeout(function(){reloadAll()}, 10000);

            });

</g:javascript>
	</head>
	<body>
    <div class="navbar box-header no-border">
   		<span class="pull-left">Job Progress Board (JPB) Body & Paint</span>
   	</div>

	<div class="box">
        <div class="tabbable control-group">
            <ul class="nav nav-tabs">
                <li id="tab_bp_per_hari" class="active">
                    <a href="#bp_per_hari" data-toggle="tab">Per Hari</a>
                </li>
                <li id="tab_bp_per_minggu" class="">
                    <a href="#bp_per_minggu" data-toggle="tab">Per Minggu</a>
                </li>
                <li id="tab_bp_per_bulan" class="">
                    <a href="#bp_per_bulan" data-toggle="tab">Per Bulan</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="bp_per_hari" />
                <div class="tab-pane" id="bp_per_minggu" />
                <div class="tab-pane" id="bp_per_bulan" />
            </div>
        </div>
        <g:if test="${idRecept}">
            <div class="control-group">
                <g:field type="button" onclick="closeBillto();" class="btn cancel" name="close" id="close" value="Edit JOB Parts" />
            </div>
        </g:if>
    </div>
</body>
</html>
