
<%@ page import="com.kombos.board.JPB" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'JPB.label', default: 'JPB GR')}" />
		%{--<title><g:message code="default.list.label" args="[entityName]" /></title>--}%
		<r:require modules="baseapplayout, baseapplist" />
        <style>
        .dss-selected {
            background-color: #FF0000 !important;
        }
        </style>
		<g:javascript>
			var show;
			var edit;
			var asbView;
			var saveDss;
			$(function(){
			save = function(){
                var idApp = "${params.appointmentId}";
			    var tglRencana = $('#input_rcvDate').val();
			    var jamRencana = $('#input_rcvHour').val() + ":" + $('#input_rcvMinute').val();
			    var tglPenyerahan = $('#input_penyerahan').val();
			    var jamPenyerahan = $('#input_penyerahanHour').val()+":"+$('#input_penyerahanMinute').val();
                $.ajax({url: '${request.contextPath}/JPBBPInput/saveAppointment',
					type: "POST",
					data: {tglRencana:tglRencana,tglPenyerahan:tglPenyerahan,idApp:idApp,jamRencana:jamRencana,jamPenyerahan:jamPenyerahan},
					success: function(data) {
					    reloadJPBTable();
					},
					complete : function (req, err) {
						$('#spinner').fadeOut();
						loading = false;
					}
				});
			}
			         
	$('#search_tglView').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tglView_day').val(newDate.getDate());
			$('#search_tglView_month').val(newDate.getMonth()+1);
			$('#search_tglView_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
	});

				show = function(id) {
					showInstance('JPB','${request.contextPath}/JPB/show/'+id);
				};
				
				edit = function(id) {
					editInstance('JPB','${request.contextPath}/JPB/edit/'+id);
				};

   	asbView = function(){
        var oTable = $('#JPBBPInput_datatables').dataTable();
        oTable.fnReloadAjax();
        var from = $("#search_tglView").val().split("/");
        var theDate = new Date();
        theDate.setFullYear(from[2], from[1] - 1, from[0]);
        //alert("theDate=" + theDate);
        var m_names = new Array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
        var formattedDate =  theDate.getDate() + " " + m_names[theDate.getMonth()] + " " + theDate.getFullYear();
        document.getElementById("lblTglView").innerHTML = formattedDate;
        console.log($("#search_tglView").val());
   	}

   	saveDss = function(){
        $first = $('.dss-selected').first();
        jamDatang = $first.data("jam");
        var from = $("#search_tglView").val();
        menitDatang = $first.data("menit");
        $last = $('.dss-selected').last();
        jamPenyerahan = $last.data("jam");
        menitPenyerahan = $last.data("menit");
        stallid = $first.data("stallid");
        var tgglAwal = $("#input_rcvDateWeek").val();
        var jamAwal = $("#input_rcvHourWeek").val();
        var menitAwal = $("#input_rcvMinuiteWeek").val();
        var tgglAkhir = $("#input_penyerahanWeek").val();
        var jamAkhir = $("#input_penyerahanWeekHour").val();
        var menitAkhir = $("#input_penyerahanWeekMinuite").val();
        $.ajax({url: '${request.contextPath}/JPBBPInput/saveJPBPerday',
            type: "POST",
            data: {tgglAwal:tgglAwal,
            jamAwal:jamAwal,
            menitAwal:menitAwal,
            tglSet:from,
            jamDatang:jamDatang,
            menitDatang: menitDatang,
            jamPenyerahan: jamPenyerahan,
            menitPenyerahan: menitPenyerahan,
            tgglAkhir:tgglAkhir,
            jamAkhir:jamAkhir,
            menitAKhir:menitAkhir,
            idApp:appId,
            idReception:receptId,
            stall:stallid
            },
            success: function(data) {
               alert("SUKSES");
               var oTable = $('#JPBBPInput_datatables').dataTable();
                oTable.fnReloadAjax();
            },
            complete : function (req, err) {
                $('#spinner').fadeOut();
                loading = false;
            }
        });
   	}

   	doDeleteJPB = function(){
                var params;
                params = {
                    noWO : "${params.idReception}",
                    plotDays : $("#search_tglView").val()
                }
                $.post("${request.getContextPath()}/JPBBPInput/deleteJPB", params, function(data) {
                    if (data)
                    {
                        if(data=="ok")
                        {
                            alert("Success");
                            reloadJPBTable();
                        }
                        else
                        {
                            alert("Gagal, terjadi kesalahan")
                        }
                    }
                    else
                        alert("Internal Server Error");
                });
            }
});

</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border"></div>
	<div class="box">
        <table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">
            <tr>
                <td style="width: 63%;vertical-align: top;">
                    <div class="box">
                    <legend style="font-size: small">Input JPB</legend>
                    <table style="width: 100%;border: 0px">
                        <tr>
                            <td>
                                <label class="control-label" for="lbl_rcvDate">
                                    Tgl/Jam Janji Datang
                                </label>
                            </td>
                            <td><g:checkBox name="checkJanjiDatang"/></td>
                            <td>
                                <div id="lbl_rcvDate" class="controls">
                                    <ba:datePicker id="input_rcvDateWeek" name="input_rcvDateWeek" precision="day" format="dd-MM-yyyy"  value="${params.mulai}"  />
                                    <select id="input_rcvHourWeek" name="input_rcvHourWeek" style="width: 60px" required="">
                                        %{
                                            for (int i=0;i<24;i++){
                                                if(i<10){
                                                        out.println('<option value="'+i+'">0'+i+'</option>');
                                                } else {
                                                        out.println('<option value="'+i+'">'+i+'</option>');
                                                }
                                            }
                                        }%
                                    </select> :
                                    <select id="input_rcvMinuiteWeek" name="input_rcvMinuiteWeek" style="width: 60px" required="">
                                        %{
                                            for (int i=0;i<60;i++){
                                                if(i<10){
                                                        out.println('<option value="'+i+'">0'+i+'</option>');
                                                } else {
                                                        out.println('<option value="'+i+'">'+i+'</option>');
                                                }
                                            }
                                        }%
                                    </select>
                                </div>
                            </td>
                            <td>
                                <div class="ico-apply">&nbsp;&nbsp;&nbsp;</div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                            <td>
                                <div class="ico-refresh">&nbsp;&nbsp;&nbsp;</div>
                            </td>
                        </tr>
                        <tr>
                            <td>Status Mobil</td>
                            <td colspan="2">
                                <div class="controls">
                                    <g:radioGroup name="statusMobil" values="['W','L','P']" value="${JPBInstance?.t351StaOkCancelReSchedule}" labels="['Ditunggu','Mobil Ditinggal','Parts Ditinggal']">
                                        ${it.radio} <g:message code="${it.label}" />
                                    </g:radioGroup>
                                </div>
                            </td>
                            <td>
                                <div class="ico-warning">&nbsp;&nbsp;&nbsp;</div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                            <td>
                                <div class="ico-cancel">&nbsp;&nbsp;&nbsp;</div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label" for="lbl_penyerahan">
                                    Tgl/Jam Janji Penyerahan
                                </label>
                            </td>
                            <td><g:checkBox name="checkJanjiPenyerahan"/></td>
                            <td style="padding-top: 10px;">
                                <div id="lbl_penyerahan" class="controls">
                                    <ba:datePicker id="input_penyerahanWeek" name="input_penyerahanWeek" precision="day" format="dd-MM-yyyy"  value="${params.selesai}"  />
                                    <select id="input_penyerahanWeekHour" name="input_penyerahanWeekHour" style="width: 60px" required="">
                                        %{
                                            for (int i=0;i<24;i++){
                                                if(i<10){
                                                        out.println('<option value="'+i+'">0'+i+'</option>');
                                                } else {
                                                        out.println('<option value="'+i+'">'+i+'</option>');
                                                }
                                            }
                                        }%
                                    </select> :
                                    <select id="input_penyerahanWeekMinuite" name="input_penyerahanWeekMinuite" style="width: 60px" required="">
                                        %{
                                            for (int i=0;i<60;i++){
                                                if(i<10){
                                                        out.println('<option value="'+i+'">0'+i+'</option>');
                                                } else {
                                                        out.println('<option value="'+i+'">'+i+'</option>');
                                                }
                                            }
                                        }%
                                    </select>
                                </div>
                            </td>
                            <td>
                                <div class="ico-question">&nbsp;&nbsp;&nbsp;</div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: right; padding-right: 75px;">
                                <g:if test="${params.idReception}">
                                    <button class="btn btn-primary pull-left" id="buttonReset" onclick="doDeleteJPB();">Clear JPB</button>
                                </g:if>
                                <button class="btn btn-primary" id="buttonSave" onclick="reloadReminderTable();">Search</button>
                                <button class="btn btn-primary" id="buttonSaveDss" onclick="saveDss();">Save</button>
                            </td>
                            <td></td>
                        </tr>
                    </table>

                    </div>
                </td>

                <td style="width: 3%;vertical-align: top;padding-left: 10px;">

                </td>

                <td style="width: 33%;vertical-align: top;padding-left: 10px;">
                    <div class="box">
                        <legend style="font-size: small">View JPB</legend>
                        <table style="width: 100%;border: 0px">
                            <tr>
                                <td>
                                    <label class="control-label" for="lbl_rcvDate">
                                        Nama Workshop
                                    </label>
                                </td>
                                <td colspan="2">
                                    <div id="lbl_companyDealer" class="controls">
                                        <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                            <g:select name="companyDealer.id" id="input_companyDealer" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                                        </g:if>
                                        <g:else>
                                            <g:select name="companyDealer.id" id="input_companyDealer" readonly="" disabled="" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                                        </g:else>
                                        %{--<g:select id="input_companyDealer" disabled="" name="companyDealer.id" from="${com.kombos.administrasi.CompanyDealer.list()}" optionKey="id" value="${session?.userCompanyDealer?.id}" required="" class="many-to-one" style="margin-top: 5px"/>--}%
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label class="control-label" for="lbl_tglView">
                                        Tanggal
                                    </label>
                                </td>
                                <td>
                                    <div id="lbl_tglView" class="controls">
                                        <div id="filter_tglView" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                                             <input type="hidden" name="search_tglView" value="date.struct">
                                             <input type="hidden" name="search_tglView_day" id="search_tglView_day" value="">
                                             <input type="hidden" name="search_tglView_month" id="search_tglView_month" value="">
                                             <input type="hidden" name="search_tglView_year" id="search_tglView_year" value="">
                                             <input type="text" data-date-format="dd/mm/yyyy" name="search_tglView_dp" value="${new java.util.Date().format('dd/MM/yyyy')}" id="search_tglView" class="search_init">
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <button class="btn btn-primary" id="buttonView" onclick="asbView()">View</button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>

            </tr>
            <tr>
                <td colspan="3">
                    <div id="JPB-table">
                        <g:if test="${flash.message}">
                            <div class="message" role="status">
                                ${flash.message}
                            </div>
                        </g:if>

                        <g:render template="dataTables" />
                    </div>
                    </td>
                </tr>
        </table>
	</div>
</body>
</html>
