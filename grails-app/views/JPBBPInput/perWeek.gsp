
<%@ page import="com.kombos.board.JPB" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'JPB.label', default: 'JPB GR')}" />
		%{--<title><g:message code="default.list.label" args="[entityName]" /></title>--}%
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			$(function(){ 
			         
	$('#search_tglViewWeek').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tglViewWeek_day').val(newDate.getDate());
			$('#search_tglViewWeek_month').val(newDate.getMonth()+1);
			$('#search_tglViewWeek_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
//			var oTable = $('#JPB_datatables').dataTable();
//            oTable.fnReloadAjax();
	});

   	asbView = function(){
        var oTable = $('#JPB_datatables').dataTable();
        oTable.fnReloadAjax();
        var from = $("#search_tglViewWeek").val().split("/");
        var theDate = new Date();
        theDate.setFullYear(from[2], from[1] - 1, from[0]);
        //alert("theDate=" + theDate);
        var m_names = new Array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
        var formattedDate =  m_names[theDate.getMonth()] + " " + theDate.getFullYear();
        document.getElementById("lblTglView").innerHTML = formattedDate;
        //Sunday is 0, Monday is 1, and so on
        var day = theDate.getDay();

        var dayMon, dayTue, dayWed, dayThu, dayFri, daySat, daySun;
        console.log("day=" + day + " dayMon=" + dayMon);
        if (day == 1) {
            dayMon = day; dayTue = day + 1; dayWed = day + 2; dayThu = day + 3;
            dayFri = day + 4; daySat = day + 5; daySun = day + 6;
        } else if (day == 2) {
            dayMon = day - 1; dayTue = day; dayWed = day + 1; dayThu = day + 2;
            dayFri = day + 3; daySat = day + 4; daySun = day + 5;
        } else if (day == 3) {
            dayMon = day - 2; dayTue = day - 1; dayWed = day; dayThu = day + 1;
            dayFri = day + 2; daySat = day + 3; daySun = day + 4;
        } else if (day == 4) {
            dayMon = day - 3; dayTue = day - 2; dayWed = day - 1; dayThu = day;
            dayFri = day + 1; daySat = day + 2; daySun = day + 3;
        } else if (day == 5) {
            dayMon = day - 4; dayTue = day - 3; dayWed = day - 2; dayThu = day - 1;
            dayFri = day; daySat = day + 1; daySun = day + 2;
        } else if (day == 6) {
            dayMon = day - 5; dayTue = day - 4; dayWed = day - 3; dayThu = day - 2;
            dayFri = day - 1; daySat = day; daySun = day + 1;
        } else {
            dayMon = day - 6; dayTue = day - 5; dayWed = day - 4; dayThu = day - 3;
            dayFri = day - 2; daySat = day - 1; daySun = day;
        }
        console.log("day=" + day + " dayMon=" + dayMon);
        document.getElementById("lblSenin").innerHTML = dayMon;
        document.getElementById("lblSelasa").innerHTML = dayTue;
        document.getElementById("lblRabu").innerHTML = dayWed;
        document.getElementById("lblKamis").innerHTML = dayThu;
        document.getElementById("lblJumat").innerHTML = dayFri;
        document.getElementById("lblSabtu").innerHTML = daySat;
        document.getElementById("lblMinggu").innerHTML = daySun;
   	}
});

</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		%{--<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>--}%
		%{--<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>--}%
	</div>

	<div class="box">
        <table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">
            <tr>
            <td style="width: 53%;vertical-align: top;">

                <div class="box">
                    <legend style="font-size: small">Input JPB</legend>
                    <table style="width: 100%;border: 0px">
                        <tr>
                            <td>
                                <label class="control-label" for="lbl_rcvDate">
                                    Tgl/Jam Janji Datang
                                </label>
                            </td>
                            <td><g:checkBox name="checkJanjiDatang"/></td>
                            <td>
                                <div id="lbl_rcvDate" class="controls">
                                    <ba:datePicker id="input_rcvDateWeek" name="input_rcvDateWeek" precision="day" format="dd-MM-yyyy"  value=""  />
                                    <select id="input_rcvHourWeek" name="input_rcvHourWeek" style="width: 60px" required="">
                                        %{
                                            for (int i=0;i<24;i++){
                                                if(i<10){
                                                        out.println('<option value="'+i+'">0'+i+'</option>');
                                                } else {
                                                        out.println('<option value="'+i+'">'+i+'</option>');
                                                }
                                            }
                                        }%
                                    </select> :
                                    <select id="input_rcvMinuiteWeek" name="input_rcvMinuiteWeek" style="width: 60px" required="">
                                        %{
                                            for (int i=0;i<60;i++){
                                                if(i<10){
                                                        out.println('<option value="'+i+'">0'+i+'</option>');
                                                } else {
                                                        out.println('<option value="'+i+'">'+i+'</option>');
                                                }
                                            }
                                        }%
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Status Mobil</td>
                            <td colspan="2">
                                <div class="controls">
                                    <g:radioGroup name="statusMobil" values="['W','L', 'P']" value="${JPBInstance?.t351StaOkCancelReSchedule}" labels="['Ditunggu','Mobil Ditinggal', 'Parts Ditinggal']">
                                        ${it.radio} <g:message code="${it.label}" />
                                    </g:radioGroup>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label" for="lbl_penyerahan">
                                    Tgl/Jam Janji Penyerahan
                                </label>
                            </td>
                            <td><g:checkBox name="checkJanjiPenyerahan"/></td>
                            <td style="padding-top: 10px;">
                                <div id="lbl_penyerahan" class="controls">
                                    <ba:datePicker id="input_penyerahanWeek" name="input_penyerahanWeek" precision="day" format="dd-MM-yyyy"  value=""  />
                                    <select id="input_penyerahanWeekHour" name="input_penyerahanWeekHour" style="width: 60px" required="">
                                        %{
                                            for (int i=0;i<24;i++){
                                                if(i<10){
                                                        out.println('<option value="'+i+'">0'+i+'</option>');
                                                } else {
                                                        out.println('<option value="'+i+'">'+i+'</option>');
                                                }
                                            }
                                        }%
                                    </select> :
                                    <select id="input_penyerahanWeekMinuite" name="input_penyerahanWeekMinuite" style="width: 60px" required="">
                                        %{
                                            for (int i=0;i<60;i++){
                                                if(i<10){
                                                        out.println('<option value="'+i+'">0'+i+'</option>');
                                                } else {
                                                        out.println('<option value="'+i+'">'+i+'</option>');
                                                }
                                            }
                                        }%
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: right; padding-right: 120px;"><button class="btn btn-primary" id="buttonSave" onclick="save();">Search</button></td>
                        </tr>
                    </table>
                </div>

            </td>
            <td style="width: 13%;vertical-align: top;padding-left: 10px;">

            </td>

            <td style="width: 33%;vertical-align: top;padding-left: 10px;">
                <div class="box">
                    <legend style="font-size: small">View JPB</legend>
                    <table style="width: 100%;border: 0px">
                        <tr>
                            <td></td>
                            <td style="width: 135px;"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label" for="lbl_rcvDate">
                                    Nama Workshop
                                </label>
                            </td>
                            <td colspan="2">
                                <div id="lbl_companyDealer" class="controls">
                                    <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                        <g:select name="companyDealer.id" id="input_companyDealer" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                                    </g:if>
                                    <g:else>
                                        <g:select name="companyDealer.id" id="input_companyDealer" readonly="" disabled="" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                                    </g:else>
                                    %{--<g:select id="input_companyDealer" name="companyDealer.id" from="${com.kombos.administrasi.CompanyDealer.list()}" optionKey="id" required="" class="many-to-one"/>--}%
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label class="control-label" for="lbl_tglView">
                                    Tanggal
                                </label>
                            </td>
                            <td>
                                <div id="lbl_tglView" class="controls">
                                    <div id="filter_tglView" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                                         <input type="hidden" name="search_tglViewWeek" value="date.struct">
                                         <input type="hidden" name="search_tglViewWeek_day" id="search_tglViewWeek_day" value="">
                                         <input type="hidden" name="search_tglViewWeek_month" id="search_tglViewWeek_month" value="">
                                         <input type="hidden" name="search_tglViewWeek_year" id="search_tglViewWeek_year" value="">
                                         <input type="text" data-date-format="dd/mm/yyyy" name="search_tglViewWeek_dp" value="" id="search_tglViewWeek" class="search_init">
                                    </div>
                                </div>
                            </td>
                            <td>
                                <button class="btn btn-primary" id="buttonView" onclick="clickView()">View</button>
                            </td>
                        </tr>

                    </table>
                </div>

            </td>

            </tr>
            <tr>
                <td colspan="3">
                    <div id="JPB-table">
                        <g:if test="${flash.message}">
                            <div class="message" role="status">
                                ${flash.message}
                            </div>
                        </g:if>

                        <g:render template="dataTablesPerWeek" />
                    </div>
                    </td>
                </tr>
        </table>
	</div>
</body>
</html>
