
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="binning_datatables_${idTable}" cellpadding="0" cellspacing="0" border="0"
       class="display table table-striped table-bordered table-hover table-selectable fixed" style="table-layout: fixed; ">
    <col width="25px" />
    <col width="400px" />
    <col width="300px" />
    <col width="500px" />
	<thead>
		<tr>
            <th></th>
			<th style="border-bottom: none;padding: 5px;" class="center">
				<div class="center"><input type="checkbox" class="pull-left select-all" aria-label="Select all" title="Select all"/>&nbsp;&nbsp;Nomor Binning</div>
			</th>
            <th style="border-bottom: none;padding: 5px;" class="center">
                <div class="center">Tanggal Binning</div>
            </th>
            <th style="border-bottom: none;padding: 5px;" class="center">
                <div class="center">Petugas Binning</div>
            </th>
		</tr>
	</thead>
</table>

<g:javascript>
var binningTable;
var reloadBinningTable;
var shrinkTableLayout;
var expandTableLayout;
var isSelected;

$(function(){
    var recordspartsperpage = [];//new Array();
    var anPartsSelected;
    var jmlRecPartsPerPage=0;
    var id;
	var anOpen = [];

    $('#binning_datatables_${idTable} td.control').live('click',function () {

        var nTr = this.parentNode;
        console.log("nTr=" + nTr );
        var i = $.inArray( nTr, anOpen );

        if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
            var noBinningInner = $(this).closest('tr').find("input:hidden").val();
            var sname = "binning-row-"  + noBinningInner;
            console.log("sname=" + sname);
            if ($('#' + sname).is(':checked')) {
                console.log("is checked");
                isSelected = "true";
            } else {
                console.log("is unchecked");
                isSelected = "false";
            }

    		var oData = binningTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${request.contextPath}/binning/sublist?isSelected=' + isSelected,
	   			success:function(data,textStatus){
	   				var nDetailsRow = binningTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );

	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});

  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			binningTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});
    $('input[id^=binning-row-]').change(function(){
        if($(this).is(':checked')){
            console.log('Checked');
        } else {
            console.log('Not checked');
        }
        console.log("this.name()=" + this.name());
    });

	reloadBinningTable = function() {

		binningTable.fnDraw();
	}

 	binningTable = $('#binning_datatables_${idTable}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsParts = $("#binning_datatables_${idTable} tbody .row-select");
            var jmlPartsCek = 0;
            var nRow;
            var idRec;
            rsParts.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordspartsperpage[idRec]=="1"){
                    jmlPartsCek = jmlPartsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordspartsperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecPartsPerPage = rsParts.length;
            if(jmlPartsCek == jmlRecPartsPerPage && jmlRecPartsPerPage > 0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "sClass": "control center",
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "nomorBinning",
	"mDataProp": "nomorBinning",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
	    %{--return '<input id="binning-row-'+row['id']+'" type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"> ' +--}%
	     %{--'<input type="hidden" value="'+row['id']+'"><input type="hidden" value="'+ data +'"><input type="hidden" id="checkbox-'+row['id']+'" >&nbsp;&nbsp;<a href="javascript:void(0);" onclick="edit(' + row['id'] + ');">' + data + '</a>';--}%
        return '<input id="binning-row-'+row['id']+'" type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"> ' +
        '<input type="hidden" value="'+row['id']+'"><input type="hidden" value="'+ data +'"><input type="hidden" id="checkbox-'+row['id']+'" >&nbsp;&nbsp;' + data;
	},
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "tanggalBinning",
	"mDataProp": "tanggalBinning",
	"aTargets": [1],
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "petugasBinning",
	"mDataProp": "petugasBinning",
	"aTargets": [2],
	"bSortable": false,
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                               
             var search_noBinning = $('#search_noBinning').val();

            if(search_noBinning){
                aoData.push(
                        {"name": 'sCriteria_noBinning', "value": search_noBinning}
                );
            }

            var tglFrom = $('#search_tanggal').val();

            if(tglFrom){
                aoData.push(
                        {"name": 'sCriteria_tglFrom', "value": tglFrom}
                );
            }

            var tglTo = $('#search_tanggalAkhir').val();

            if(tglTo){
                aoData.push(
                        {"name": 'sCriteria_tglTo', "value": tglTo}
                );
            }
            var criteriaParts = $('input[name^=criteriaParts]:checked').val();
            console.log("binning criteriaParts=" + criteriaParts);
            if(criteriaParts){
                aoData.push(
                        {"name": 'criteriaParts', "value": criteriaParts}
                );
            }
                                                 
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});

    $('.select-all').click(function(e) {
        $("#binning_datatables_${idTable} tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                $('.row-select2').prop('checked', true);
                $('.row-select').prop('checked', true);
                $('.sel-all2').prop('checked', true);
                var nRow = $('.row-select2').parent().parent();//.addClass('row_selected');
                nRow.addClass('row_selected');

                recordspartsperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                $('.row-select2').prop('checked', false);
                $('.row-select').prop('checked', false);
                $('.sel-all2').prop('checked', false);
                var nRow = $('.row-select2').parent().parent();//.addClass('row_selected');
                nRow.removeClass('row_selected');

                recordspartsperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#binning_datatables_${idTable} tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        console.log("id=" + id);
        var noBinning = $(this).find('.row-select').next("input:hidden").next("input:hidden").val();
        console.log("noBinning=" + noBinning);
        if($(this).find('.row-select').is(":checked")){
            recordspartsperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anPartsSelected = binningTable.$('tr.row_selected');
            console.log("jmlRecPartsPerPage=" + jmlRecPartsPerPage + " anPartsSelected=" + anPartsSelected);
            if(jmlRecPartsPerPage == anPartsSelected.length){
                $('.select-all').attr('checked', true);
            }

            $('.sel-all2').each(function() {
                    var noBinningInner = $(this).next("input:hidden").val();
                    //console.log("select-all2 noBinning=" + noBinning + " noBinningInner=" + noBinningInner);
                    if (noBinning == noBinningInner) {
                        $(this).attr('checked', true);

                        var nRow = $(this).parent().parent().parent();//.addClass('row_selected');
                        console.log("select-all2 nRow.html()=" + nRow.html());
                        nRow.addClass('row_selected');
                    }
                });
            $('table[id^=binning_datatables_sub_] tbody .row-select2').each(function() {
                var noBinningInner = $(this).next("input:hidden").next("input:hidden").val();
                //console.log("row-select2 noBinning=" + noBinning + " noBinningInner=" + noBinningInner);
                if (noBinning == noBinningInner) {
                    $(this).attr('checked', true);

                    var nRow = $(this).parent().parent();//.addClass('row_selected');
                    console.log("row-select2 nRow.html()=" + nRow.html());
                    nRow.addClass('row_selected');
                }
            });

        } else {
            recordspartsperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');

            $('.sel-all2').each(function() {
                var noBinningInner = $(this).next("input:hidden").val();
                //console.log("select-all2 noBinning=" + noBinning + " noBinningInner=" + noBinningInner);
                if (noBinning == noBinningInner) {
                    $(this).attr('checked', false);

                    var nRow = $(this).parent().parent().parent();//.addClass('row_selected');
                    nRow.removeClass('row_selected');
                }
            });
            $('table[id^=binning_datatables_sub_] tbody .row-select2').each(function() {
                var noBinningInner = $(this).next("input:hidden").next("input:hidden").val();
                //console.log("row-select2 noBinning=" + noBinning + " noBinningInner=" + noBinningInner);
                if (noBinning == noBinningInner) {
                    $(this).attr('checked', false);

                    var nRow = $(this).parent().parent();//.addClass('row_selected');
                    nRow.removeClass('row_selected');
                }
            });
        }

    }
    );


});
</g:javascript>
