<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
        <style>
            table.fixed { table-layout:fixed; }
            table.fixed td { overflow: hidden; }
            .center {
                text-align:center;
            }
        </style>
		<g:javascript>
var binningSubTable_${idTableSub};
$(function(){
    var recordspartsperpage = [];//new Array();
    var anPartsSelected;
    var jmlRecPartsPerPage=0;
    var id;
    var isSelected;
var anOpen = [];
	$('#binning_datatables_sub_${idTableSub} a.edit').live('click', function (e) {
        e.preventDefault();

        var nRow = $(this).parents('tr')[0];

        if ( nEditing !== null && nEditing != nRow ) {
            restoreRow( binningSubTable_${idTableSub}, nEditing );
            editRow( binningSubTable_${idTableSub}, nRow );
            nEditing = nRow;
        }
        else if ( nEditing == nRow && this.innerHTML == "Save" ) {
            saveRow( binningSubTable_${idTableSub}, nEditing );
            nEditing = null;
        }
        else {
            editRow( binningSubTable_${idTableSub}, nRow );
            nEditing = nRow;
        }
    } );
    if(binningSubTable_${idTableSub})
    	binningSubTable_${idTableSub}.dataTable().fnDestroy();
binningSubTable_${idTableSub} = $('#binning_datatables_sub_${idTableSub}').dataTable({
		"sScrollX": "1398px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsParts = $("#binning_datatables_sub_${idTableSub} tbody .row-select2");
            var jmlPartsCek = 0;
            var nRow;
            var idRec;
            rsParts.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                console.log("nRow=" + nRow);
                console.log("idRec=" + idRec);
                console.log("recordspartsperpage[idRec]=" + recordspartsperpage[idRec]);

                if(this.checked || recordspartsperpage[idRec]=="1"){
                    jmlPartsCek = jmlPartsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(!this.checked || recordspartsperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecPartsPerPage = rsParts.length;
            %{--if(jmlPartsCek == jmlRecPartsPerPage && jmlRecPartsPerPage > 0){
                $('.select-all2_${idTableSub}').attr('checked', true);
            } else {
                $('.select-all2_${idTableSub}').attr('checked', false);
            }--}%
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sDefaultContent": ''
},
{
	"sName": "kodeParts",
	"mDataProp": "kodeParts",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
	    return '<input id="binning-sub-row-'+row['id']+'" type="checkbox" class="pull-left row-select2" ${isSelected=='true'?'checked':''} > ' +
	     '<input type="hidden" value="'+row['id']+'"><input type="hidden" value="${nomorBinning}">&nbsp;&nbsp;' + data;
	},
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "namaParts",
	"mDataProp": "namaParts",
	"aTargets": [1],
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "noReff",
	"mDataProp": "noReff",
	"aTargets": [2],
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "tglJamReceive",
	"mDataProp": "tglJamReceive",
	"aTargets": [3],
	"bSortable": false,
	"bVisible": true
},
{
    "sName": "nomorPO",
	"mDataProp": "nomorPO",
	"bSortable": false,
	"sDefaultContent": '',
	"bVisible": true
},
{
    "sName": "tanggalPO",
	"mDataProp": "tanggalPO",
	"bSortable": false,
	"sDefaultContent": '',
	"bVisible": true
},
{
    "sName": "nomorInvoice",
	"mDataProp": "nomorInvoice",
	"bSortable": false,
	"sDefaultContent": '',
	"bVisible": true
},
{
    "sName": "tglInvoice",
	"mDataProp": "tglInvoice",
	"bSortable": false,
	"sDefaultContent": '',
	"bVisible": true
},
{
    "sName": "qtyInvoice",
	"mDataProp": "qtyInvoice",
	"bSortable": false,
	"sDefaultContent": '',
	"bVisible": true
},
{
    "sName": "qtyReceive1",
	"mDataProp": "qtyReceive1",
	"bSortable": false,
	"sDefaultContent": '',
	"bVisible": true
},
{
    "sName": "qtyRusak1",
	"mDataProp": "qtyRusak1",
	"bSortable": false,
	"sDefaultContent": '',
	"bVisible": true
},
{
    "sName": "qtySalah1",
	"mDataProp": "qtySalah1",
	"bSortable": false,
	"sDefaultContent": '',
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						aoData.push(
							{"name": 'nomorBinning', "value": "${nomorBinning}"},
							{"name": 'tglBinning', "value": "${tglBinning}"},
							{"name": 'petugasBinning', "value": "${petugasBinning}"},
							{"name": 'isSelected', "value": "${isSelected}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
    %{--$('.select-all2_${idTableSub}').click(function(e) {
        $("#binning_datatables_sub_${idTableSub} tbody .row-select2").each(function() {
            console.log(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordspartsperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordspartsperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });--}%
    $('.select-all2_${idTableSub}').click(function(e) {
        var tw = $(this).parents(".dataTables_wrapper");
        console.log("tw=" + tw);
        console.log("idTableSub" + ${idTableSub} + " this.checked=" + this.checked);
        var tc = tw.find('#binning_datatables_sub_${idTableSub} tbody').find('.row-select2').attr('checked', this.checked);
        console.log("tc.html()=" + tc.html());
        if(this.checked)
            tc.parent().parent().addClass(' row_selected ');
        else
            tc.parent().parent().removeClass('row_selected');
        console.log("tc.html()=" + tc.html());
        console.log("tc.parent().parent().html()=" + tc.parent().parent().html());
        e.stopPropagation();
    });

	$('#binning_datatables_sub_${idTableSub} tbody tr').live('click', function () {

        id = $(this).find('.row-select2').next("input:hidden").val();
        if($(this).find('.row-select2').is(":checked")){
            recordspartsperpage[id] = "1";
            $(this).find('.row-select2').parent().parent().addClass('row_selected');
            anPartsSelected = binningSubTable_${idTableSub}.$('tr.row_selected');
            if(jmlRecPartsPerPage == anPartsSelected.length){
                $('.select-all2_${idTableSub}').attr('checked', true);
            }
        } else {
            recordspartsperpage[id] = "0";
            $('.select-all2_${idTableSub}').attr('checked', false);
            $(this).find('.row-select2').parent().parent().removeClass('row_selected');
        }
    });

    var checkbox_col2 = $(".table-selectable").find("thead").find("tr").first().find("th").first().find("div");
    if(checkbox_col2 && checkbox_col2.html()){
           if(checkbox_col2.html().search('type="checkbox"')==-1){
               checkbox_col2.prepend('<input type="checkbox" class="pull-left select-all2_${idTableSub} sel-all2" aria-label="Select all" title="Select all"/>&nbsp;&nbsp;')
           }

    }


});
		</g:javascript>
	</head>
	<body>
<div class="innerDetails">
<table id="binning_datatables_sub_${idTableSub}" cellpadding="0" cellspacing="0" border="0"
	class="display table table-striped table-bordered table-hover table-selectable fixed">
    <col width="24px" />
    <col width="25px" />
    <col width="150px" />
    <col width="150px" />
    <col width="150px" />
    <col width="150px" />
    <col width="150px" />
    <col width="100px" />
    <col width="75px" />
    <col width="125px" />
    <col width="100px" />
    <col width="100px" />
    <col width="100px" />
    <col width="98px" />
    <thead>
        <tr>
           <th rowspan="2"></th>
           <th rowspan="2"></th>
			<th style="border-bottom: none; padding: 5px; width: 200px;" rowspan="2">
				<div class="center"><input type="checkbox" class="pull-left select-all2_${idTableSub} sel-all2" aria-label="Select all" title="Select all" ${isSelected=='true'?'checked':''} />
                    <input type="hidden" value="${nomorBinning}">&nbsp;&nbsp;Kode Part</div>
			</th>
			<th style="border-bottom: none; padding: 5px; width: 200px;" rowspan="2">
				<div class="center">Nama Part</div>
			</th>
			<th style="border-bottom: none; padding: 5px; width: 200px;" rowspan="2">
				<div class="center">No Reff</div>
			</th>
            <th style="border-bottom: none; padding: 5px; width: 200px;" rowspan="2">
				<div class="center">Tanggal Dan Jam Receive</div>
			</th>
            <th style="border-bottom: none; padding: 5px; width: 200px;" rowspan="2">
                <div class="center">Nomor PO</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 200px;" rowspan="2">
                <div class="center">Tanggal PO</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 200px;" rowspan="2">
                <div class="center">Nomor Invoice</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 200px;" rowspan="2">
                <div class="center">Tanggal Invoice</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 200px;" colspan="4">
                <div class="center">Qty</div>
            </th>
         </tr>
        <tr>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div class="center">Invoice</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div class="center">Receive</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div class="center">Rusak</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div class="center">Salah</div>
            </th>
         </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
</body>
</html>
