
<%@ page import="com.kombos.parts.Binning" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="title" value="Parts Binning" />
		<title>${title}</title>
		<r:require modules="baseapplayout" />
        <style>
            table.fixed { table-layout:fixed; }
            table.fixed td { overflow: hidden; }
            .center {
                text-align:center;
            }
         </style>
		<g:javascript>
	var orderParts;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	var printBinningSlip;
	$(function(){ 

    $("#binning-table").css("margin-left","-0px");
    $("#binning-table").css("margin-right","0px");
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :
                shrinkTableLayout();
                <g:remoteFunction controller="inputBinning" action="index"
                        onLoading="jQuery('#spinner').fadeIn(1);"
                        onSuccess="loadForm(data, textStatus);"
                        onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
         			function(result){
         				if(result){
         					massDeleteBinning();
         				}
         			});

         		break;
       }
       return false;
	});

    massDeleteBinning = function() {
        var recordsToDelete = [];
        var binningDetailsToDelete = [];
        $('#binning_datatables_${idTable} tbody .row-select').each(function() {
            if(this.checked){
                var id = $(this).next("input:hidden").val();
                recordsToDelete.push(id);
            }
        });

        $('table[id^=binning_datatables_sub_] tbody .row-select2').each(function() {
            console.log("this=" + this);

            if(this.checked){
                var id = $(this).next("input:hidden").val();
                var binningNumber = $(this).next("input:hidden").next("input:hidden").val();
                console.log("id=" + id + " binningNumber=" + binningNumber);
                binningDetailsToDelete.push(id);
            }
        });
        console.log("recordsToDelete=" + recordsToDelete);
        console.log("binningDetailsToDelete=" + binningDetailsToDelete);
        var json = JSON.stringify(recordsToDelete);
        var jsonDetail = JSON.stringify(binningDetailsToDelete);
        console.log("json=" + json);
        console.log("jsonDetail=" + jsonDetail);
        if (recordsToDelete.length > 0) {
            $.ajax({
                url:'${request.contextPath}/binning/massdeleteBinning',
                type: "POST", // Always use POST when deleting data
                data: { ids: json },
                complete: function(xhr, status) {
                    binningTable.fnDraw();
                     toastr.success('<div>Binning Telah Dihapus.</div>');

                }
            });
        } else if (binningDetailsToDelete.length > 0) {
            $.ajax({
                url:'${request.contextPath}/binning/massdeleteBinningDetail',
                type: "POST", // Always use POST when deleting data
                data: { ids: jsonDetail },
                complete: function(xhr, status) {
                    binningTable.fnDraw();
                     toastr.success('<div>Binning Detail Telah Dihapus.</div>');
                }
            });
        }


    }


     onClearSearch = function () {
        $('input[name=criteriaParts]:radio').removeAttr('checked');
         $('#search_tanggal').val("");
         $('#search_tanggalAkhir').val("");
         $('#search_noBinning').val("");
        reloadBinningTable();
    }

   	onSearch = function(){
       reloadBinningTable();
   	}

    edit = function(id){
        console.log("didalam edit");
        shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/inputBinning/index?1=1',
   		    data : { id: id},
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
        $(".row-select").each(function() {
            this.checked = false
        });
    }

    shrinkTableLayout = function(){
        $("#binning-table").hide();
        console.log("afterHide");
        $("#binning-form").css("display","block");
        $("#binning-form").css("margin-left","-40px");
        $("#binning-form").css("margin-right","40px");
   	}

    loadForm = function(data, textStatus){
		$('#binning-form').empty();
    	$('#binning-form').append(data);
   	}

   	expandTableLayout = function(){
   	    var oTable = $('#binning_datatables_${idTable}').dataTable();
        oTable.fnReloadAjax();
   		$("#binning-table").show();
   		$("#binning-form").css("display","none");
   	}

    printBinningSlip = function(){
//        window.location = "${request.contextPath}/binning/printBinningSlip";
        console.log("inside printBinningSlip");
        var binningToPrint = [];
        var binningDetailsToPrint = [];
        $('#binning_datatables_${idTable} tbody .row-select').each(function() {
            if(this.checked){
                var id = $(this).next("input:hidden").val();
                binningToPrint.push(id);
            }
        });
        $('table[id^=binning_datatables_sub_] tbody .row-select2').each(function() {
            if(this.checked){
                var id = $(this).next("input:hidden").val();
                binningDetailsToPrint.push(id);
            }
        });
        console.log("binningToPrint=" + binningToPrint);
        console.log("binningDetailsToPrint=" + binningDetailsToPrint);
        var json = JSON.stringify(binningToPrint);
        var jsonDetail = JSON.stringify(binningDetailsToPrint);

        console.log("json=" + json);
        console.log("jsonDetail=" + jsonDetail);
        if (binningToPrint.length > 0) {
            var idBinnings =  JSON.stringify(binningToPrint);
            console.log("idBinnings=" + idBinnings);
            window.location = "${request.contextPath}/binning/printBinningSlip?idBinnings="+idBinnings;
        } else if (binningDetailsToPrint.length > 0) {
            var idBinningDetails =  JSON.stringify(binningDetailsToPrint);
            console.log("idBinningDetails=" + idBinningDetails);
            window.location = "${request.contextPath}/binning/printBinningSlip?idBinningDetails="+idBinningDetails;
        }
    }

    	var checkin = $('#search_tanggal').datepicker({
            onRender: function(date) {
                return '';
            }
        }).on('changeDate', function(ev) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate());
            checkout.setValue(newDate);
            checkin.hide();
            $('#search_tanggalAkhir')[0].focus();
        }).data('datepicker');

        var checkout = $('#search_tanggalAkhir').datepicker({
            onRender: function(date) {
                return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');
   	});
</g:javascript>
	</head>
	<body>
    <div class="span12" id="binning-table" >
    <div class="box" >
        <legend style="font-size: small">Search Criteria</legend>
        <table style="width: 50%;border: 0px">
            <tr>
                <td style="width: 130px">
                    <label class="control-label" for="lbl_rcvDate">
                        No Binning
                    </label>
                </td>
                <td>
                    <div id="lbl_noBinn" class="controls">
                        <g:textField name="search_noBinning" id="search_noBinning" />
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width: 130px">
                    <label class="control-label" for="lbl_rcvDate">
                        Tanggal Binning
                    </label>
                </td>
                <td>
                    <div id="lbl_rcvDate" class="controls">
                        <ba:datePicker id="search_tanggal" name="search_tanggal" precision="day" format="dd/MM/yyyy"  value="${new Date()}" /> S.d
                        <ba:datePicker id="search_tanggalAkhir" name="search_tanggalAkhir" precision="day" format="dd/MM/yyyy" style="display: none" value="${new Date()}" />
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="control-label" for="lbl_criteriaParts">
                        Kriteria Parts
                    </label>
                </td>
                <td colspan="2">
                    <div id="lbl_criteriaParts" class="controls">
                        <g:radioGroup name="criteriaParts" values="['0','1']" labels="['Parts Belum Stock In','Parts Sudah Stock In']">
                            ${it.radio} <g:message code="${it.label}" />
                        </g:radioGroup>
                    </div>
                </td>
            </tr>
        </table>
        <fieldset class="buttons controls">
            <button class="btn btn-primary" onclick="onSearch();">Search</button>
            <button class="btn btn-cancel" onclick="onClearSearch();">Clear Search</button>
        </fieldset>
    </div>

    <div class="navbar box-header no-border" >
        <span class="pull-left">Parts Binning</span>
        <ul class="nav pull-right">
          <li><a class="pull-right box-action" href="#"
              style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
                  class="icon-plus"></i>&nbsp;&nbsp;
          </a></li>
          %{--<li><a class="pull-right box-action" href="#"--}%
              %{--style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i--}%
                  %{--class="icon-remove"></i>&nbsp;&nbsp;--}%
          %{--</a></li>--}%
        </ul>
    </div>

    <div class="box" style="margin-left: -0px; margin-right: 0px;">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        %{--<legend style="font-size: small">Parts Binning</legend>--}%

        <g:render template="dataTables" />
        <div class="modal-footer">
            <button class="btn btn-primary" data-dismiss="modal" onclick="printBinningSlip()">Print Binning Slip</button>
        </div>
    </div>
    </div>

    <div class="span12" id="binning-form" style="display: none;"></div>


</body>
</html>
