

<%@ page import="com.kombos.administrasi.VendorCat" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'vendorCat.label', default: 'Vendor Cat')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteVendorCat;

$(function(){ 
	deleteVendorCat=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/vendorCat/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadVendorCatTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-vendorCat" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="vendorCat"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${vendorCatInstance?.m191ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m191ID-label" class="property-label"><g:message
					code="vendorCat.m191ID.label" default="Kode Vendor" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m191ID-label">
						%{--<ba:editableValue
								bean="${vendorCatInstance}" field="m191ID"
								url="${request.contextPath}/VendorCat/updatefield" type="text"
								title="Enter m191ID" onsuccess="reloadVendorCatTable();" />--}%
							
								<g:fieldValue bean="${vendorCatInstance}" field="m191ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vendorCatInstance?.m191Nama}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m191Nama-label" class="property-label"><g:message
					code="vendorCat.m191Nama.label" default="Nama Vendor" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m191Nama-label">
						%{--<ba:editableValue
								bean="${vendorCatInstance}" field="m191Nama"
								url="${request.contextPath}/VendorCat/updatefield" type="text"
								title="Enter m191Nama" onsuccess="reloadVendorCatTable();" />--}%
							
								<g:fieldValue bean="${vendorCatInstance}" field="m191Nama"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vendorCatInstance?.m191PIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m191PIC-label" class="property-label"><g:message
					code="vendorCat.m191PIC.label" default="PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m191PIC-label">
						%{--<ba:editableValue
								bean="${vendorCatInstance}" field="m191PIC"
								url="${request.contextPath}/VendorCat/updatefield" type="text"
								title="Enter m191PIC" onsuccess="reloadVendorCatTable();" />--}%
							
								<g:fieldValue bean="${vendorCatInstance}" field="m191PIC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vendorCatInstance?.m191Alamat}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m191Alamat-label" class="property-label"><g:message
					code="vendorCat.m191Alamat.label" default="Alamat" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m191Alamat-label">
						%{--<ba:editableValue
								bean="${vendorCatInstance}" field="m191Alamat"
								url="${request.contextPath}/VendorCat/updatefield" type="text"
								title="Enter m191Alamat" onsuccess="reloadVendorCatTable();" />--}%
							
								<g:fieldValue bean="${vendorCatInstance}" field="m191Alamat"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vendorCatInstance?.m191Telp}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m191Telp-label" class="property-label"><g:message
					code="vendorCat.m191Telp.label" default="Telp" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m191Telp-label">
						%{--<ba:editableValue
								bean="${vendorCatInstance}" field="m191Telp"
								url="${request.contextPath}/VendorCat/updatefield" type="text"
								title="Enter m191Telp" onsuccess="reloadVendorCatTable();" />--}%
							
								<g:fieldValue bean="${vendorCatInstance}" field="m191Telp"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vendorCatInstance?.m191Email}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m191Email-label" class="property-label"><g:message
					code="vendorCat.m191Email.label" default="Email" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m191Email-label">
						%{--<ba:editableValue
								bean="${vendorCatInstance}" field="m191Email"
								url="${request.contextPath}/VendorCat/updatefield" type="text"
								title="Enter m191Email" onsuccess="reloadVendorCatTable();" />--}%
							
								<g:fieldValue bean="${vendorCatInstance}" field="m191Email"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vendorCatInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="vendorCat.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${vendorCatInstance}" field="createdBy"
								url="${request.contextPath}/VendorCat/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadVendorCatTable();" />--}%
							
								<g:fieldValue bean="${vendorCatInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vendorCatInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="vendorCat.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${vendorCatInstance}" field="updatedBy"
								url="${request.contextPath}/VendorCat/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadVendorCatTable();" />--}%
							
								<g:fieldValue bean="${vendorCatInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				

            %{--<g:if test="${vendorCatInstance?.staDel}">
            <tr>
            <td class="span2" style="text-align: right;"><span
                id="staDel-label" class="property-label"><g:message
                code="vendorCat.staDel.label" default="Sta Del" />:</span></td>

                    <td class="span3"><span class="property-value"
                    aria-labelledby="staDel-label">
                    <ba:editableValue
                            bean="${vendorCatInstance}" field="staDel"
                            url="${request.contextPath}/VendorCat/updatefield" type="text"
                            title="Enter staDel" onsuccess="reloadVendorCatTable();" />
							
								<g:fieldValue bean="${vendorCatInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>--}%
			
				<g:if test="${vendorCatInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="vendorCat.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${vendorCatInstance}" field="dateCreated"
								url="${request.contextPath}/VendorCat/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadVendorCatTable();" />--}%
							
								%{--<g:formatDate date="${vendorCatInstance?.dateCreated}" />--}%
                            <g:formatDate date="${vendorCatInstance?.dateCreated}"  type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

				<g:if test="${vendorCatInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="vendorCat.lastUpdated.label" default="Last Updated" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${vendorCatInstance}" field="lastUpdated"
								url="${request.contextPath}/VendorCat/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadVendorCatTable();" />--}%

								%{--<g:formatDate date="${vendorCatInstance?.lastUpdated}" />--}%
                            <g:formatDate date="${vendorCatInstance?.lastUpdated}"  type="datetime" style="MEDIUM"/>
						</span></td>

				</tr>
				</g:if>
				
				<g:if test="${vendorCatInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="vendorCat.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${vendorCatInstance}" field="lastUpdProcess"
								url="${request.contextPath}/VendorCat/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadVendorCatTable();" />--}%
							
								<g:fieldValue bean="${vendorCatInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${vendorCatInstance?.id}"
					update="[success:'vendorCat-form',failure:'vendorCat-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteVendorCat('${vendorCatInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
