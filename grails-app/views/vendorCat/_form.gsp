<%@ page import="com.kombos.administrasi.VendorCat" %>
<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode != 45  && charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    $(document).ready(function() {
        if($('#m191Alamat').val().length>0){
            $('#hitung').text(255 - $('#m191Alamat').val().length);
        }

        $('#m191Alamat').keyup(function() {
            var len = this.value.length;
            if (len >= 255) {
                this.value = this.value.substring(0, 255);
                len = 255;
            }
            $('#hitung').text(255 - len);
        });
    });
</g:javascript>


<g:if test="${vendorCatInstance?.m191ID}">
<div class="control-group fieldcontain ${hasErrors(bean: vendorCatInstance, field: 'm191ID', 'error')} required">
	<label class="control-label" for="m191ID">
		<g:message code="vendorCat.m191ID.label" default="Kode Vendor" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        ${vendorCatInstance.m191ID}
	%{--<g:field name="m191ID" type="number" maxlenght="10" value="${vendorCatInstance.m191ID}" required=""/>--}%
	</div>
</div>

</g:if>
<div class="control-group fieldcontain ${hasErrors(bean: vendorCatInstance, field: 'm191Nama', 'error')} required">
	<label class="control-label" for="m191Nama">
		<g:message code="vendorCat.m191Nama.label" default="Nama Vendor" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m191Nama" id="m191Nama" maxlength="50" required="" value="${vendorCatInstance?.m191Nama}"/>
	</div>
</div>
<g:javascript>
    document.getElementById('m191Nama').focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: vendorCatInstance, field: 'm191PIC', 'error')} required">
	<label class="control-label" for="m191PIC">
		<g:message code="vendorCat.m191PIC.label" default="PIC" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m191PIC" maxlength="50" required="" value="${vendorCatInstance?.m191PIC}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorCatInstance, field: 'm191Alamat', 'error')} required">
	<label class="control-label" for="m191Alamat">
		<g:message code="vendorCat.m191Alamat.label" default="Alamat" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textArea name="m191Alamat" id="m191Alamat" required="" value="${vendorCatInstance?.m191Alamat}"/>
        <br/>
        <span id="hitung">255</span> Karakter Tersisa.
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorCatInstance, field: 'm191Telp', 'error')} required">
	<label class="control-label" for="m191Telp">
		<g:message code="vendorCat.m191Telp.label" default="Telp" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m191Telp" maxlength="50" onkeypress="return isNumberKey(event)" required="" value="${vendorCatInstance?.m191Telp}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorCatInstance, field: 'm191Email', 'error')} required">
	<label class="control-label" for="m191Email">
		<g:message code="vendorCat.m191Email.label" default="Email" />
		%{--<span class="required-indicator">*</span>--}%
	</label>
	<div class="controls">
	%{--<g:textField name="m191Email" maxlength="50" required="" value="${vendorCatInstance?.m191Email}"/>--}%
    <g:field name="m191Email" type="email" maxlength="50" value="${vendorCatInstance?.m191Email}"/>
	</div>
</div>
%{--
<div class="control-group fieldcontain ${hasErrors(bean: vendorCatInstance, field: 'createdBy', 'error')} ">
	<label class="control-label" for="createdBy">
		<g:message code="vendorCat.createdBy.label" default="Created By" />
		
	</label>
	<div class="controls">
	<g:textField name="createdBy" value="${vendorCatInstance?.createdBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorCatInstance, field: 'updatedBy', 'error')} ">
	<label class="control-label" for="updatedBy">
		<g:message code="vendorCat.updatedBy.label" default="Updated By" />
		
	</label>
	<div class="controls">
	<g:textField name="updatedBy" value="${vendorCatInstance?.updatedBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorCatInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="vendorCat.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${vendorCatInstance?.lastUpdProcess}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorCatInstance, field: 'staDel', 'error')} ">
	<label class="control-label" for="staDel">
		<g:message code="vendorCat.staDel.label" default="Sta Del" />
		
	</label>
	<div class="controls">
	<g:textField name="staDel" maxlength="1" value="${vendorCatInstance?.staDel}"/>
	</div>
</div>
--}%
