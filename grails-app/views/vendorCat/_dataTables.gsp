
<%@ page import="com.kombos.administrasi.VendorCat" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</g:javascript>

<table id="vendorCat_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vendorCat.m191ID.label" default="Kode Vendor" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vendorCat.m191Nama.label" default="Nama Vendor" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vendorCat.m191PIC.label" default="PIC" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vendorCat.m191Alamat.label" default="Alamat" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vendorCat.m191Telp.label" default="Telp" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vendorCat.m191Email.label" default="Email" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vendorCat.createdBy.label" default="Created By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vendorCat.updatedBy.label" default="Updated By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vendorCat.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vendorCat.staDel.label" default="Sta Del" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m191ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m191ID" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m191Nama" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m191Nama" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m191PIC" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m191PIC" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m191Alamat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m191Alamat" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m191Telp" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m191Telp" onkeypress="return isNumberKey(event)" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m191Email" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m191Email" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_createdBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_updatedBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_staDel" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var VendorCatTable;
var reloadVendorCatTable;
$(function(){
	
	reloadVendorCatTable = function() {
		VendorCatTable.fnDraw();
	}

        var recordsVendorCatperpage = [];
        var anVendorCatSelected;
        var jmlRecVendorCatPerPage=0;
        var id;


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	VendorCatTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	VendorCatTable = $('#vendorCat_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            //alert( $('li.active').text() );
            var rsVendorCat = $("#vendorCat_datatables tbody .row-select");
            var jmlVendorCatCek = 0;
            var idRec;
            var nRow;
            rsVendorCat.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsVendorCatperpage[idRec]=="1"){
                    jmlVendorCatCek = jmlVendorCatCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsVendorCatperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecVendorCatPerPage = rsVendorCat.length;
            if(jmlVendorCatCek==jmlRecVendorCatPerPage && jmlRecVendorCatPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
        "fnInitComplete": function () {
            this.fnAdjustColumnSizing(true);
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            return nRow;
        },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m191ID",
	"mDataProp": "m191ID",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m191Nama",
	"mDataProp": "m191Nama",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m191PIC",
	"mDataProp": "m191PIC",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m191Alamat",
	"mDataProp": "m191Alamat",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m191Telp",
	"mDataProp": "m191Telp",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m191Email",
	"mDataProp": "m191Email",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "createdBy",
	"mDataProp": "createdBy",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "updatedBy",
	"mDataProp": "updatedBy",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "staDel",
	"mDataProp": "staDel",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m191ID = $('#filter_m191ID input').val();
						if(m191ID){
							aoData.push(
									{"name": 'sCriteria_m191ID', "value": m191ID}
							);
						}
	
						var m191Nama = $('#filter_m191Nama input').val();
						if(m191Nama){
							aoData.push(
									{"name": 'sCriteria_m191Nama', "value": m191Nama}
							);
						}
	
						var m191PIC = $('#filter_m191PIC input').val();
						if(m191PIC){
							aoData.push(
									{"name": 'sCriteria_m191PIC', "value": m191PIC}
							);
						}
	
						var m191Alamat = $('#filter_m191Alamat input').val();
						if(m191Alamat){
							aoData.push(
									{"name": 'sCriteria_m191Alamat', "value": m191Alamat}
							);
						}
	
						var m191Telp = $('#filter_m191Telp input').val();
						if(m191Telp){
							aoData.push(
									{"name": 'sCriteria_m191Telp', "value": m191Telp}
							);
						}
	
						var m191Email = $('#filter_m191Email input').val();
						if(m191Email){
							aoData.push(
									{"name": 'sCriteria_m191Email', "value": m191Email}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

	     $('.select-all').click(function(e) {
            $("#vendorCat_datatables tbody .row-select").each(function() {
                if(this.checked){
                    recordsVendorCatperpage[$(this).next("input:hidden").val()] = "1";
                } else {
                    recordsVendorCatperpage[$(this).next("input:hidden").val()] = "0";
                }
            });
        });

        $('#vendorCat_datatables tbody tr').live('click', function () {
            id = $(this).find('.row-select').next("input:hidden").val();
            if($(this).find('.row-select').is(":checked")){
                recordsVendorCatperpage[id] = "1";
                $(this).find('.row-select').parent().parent().addClass('row_selected');
                anVendorCatSelected = VendorCatTable.$('tr.row_selected');
                if(jmlRecVendorCatPerPage == anVendorCatSelected.length){
                    $('.select-all').attr('checked', true);
                }
            } else {
                recordsVendorCatperpage[id] = "0";
                $('.select-all').attr('checked', false);
                $(this).find('.row-select').parent().parent().removeClass('row_selected');
            }
        });

});
</g:javascript>


			
