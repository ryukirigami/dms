<%@ page import="com.kombos.finance.BankReconciliation" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'bankReconciliation.label', default: 'BankReconciliation')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="create-bankReconciliation" class="content scaffold-create" role="main">
			<legend><g:message code="default.create.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${bankReconciliationInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${bankReconciliationInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form action="save" >--}%
			<g:formRemote class="form-horizontal" name="create" on404="alert('not found!');" onSuccess="reloadBankReconciliationTable();" update="bankReconciliation-form"
              url="[controller: 'bankReconciliation', action:'save']">	
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons controls" style="margin-left: -32px;margin-top: 10px;">
					<a id="btnCancel" class="btn cancel" href="javascript:void(0);"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
					%{--<g:submitButton class="btn btn-primary create" name="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />--}%
                    <a class="btn btn-primary create" id="btnSave">Save</a>
				</fieldset>
			</g:formRemote>
			%{--</g:form>--}%
		</div>
    <g:javascript>
        var deleteRow;
        $(function() {

            deleteRow = function(el) {
                var _target = $(el).data("target");

                $(el).parent().parent().parent().remove();

                performCalculation(_target);
            }

            $(".numeric").autoNumeric("init", {
                vMin: '-999999999.99'
            });

            performCalculation("detail");
            performCalculation("detail2");

            $("#btnCancel").click(function() {
                $("#bankReconciliation-form").fadeOut();
                $("#bankReconciliation-table").fadeIn("fast");
            });
            $("#btnSave").click(function() {

                $("#btnHitungSelisih").click();

                var bank                 = $("#bank").val();
                var date                 = $("#reconciliationDate_gabisa").val();
                var selisih              = $("#selisih").autoNumeric("get");
                var saldoAwalBank        = $("#saldoBankPerRekonsiliasi").autoNumeric("get");
                var saldoAkhirBank       = $("#saldoAkhir_Bank").autoNumeric("get");
                var saldoAwalPerusahaan  = $("#saldoPerusahaanPerRekonsiliasi").autoNumeric("get");
                var saldoAkhirPerusahaan = $("#saldoAkhir_Perusahaan").autoNumeric("get");
                var data = {};

                if (bank && date) {
                    data = {
                        bank: bank,
                        reconciliationDate: date,
                        selisih: selisih,
                        saldoAwalBank: saldoAwalBank,
                        saldoAkhirBank: saldoAkhirBank,
                        saldoAwalPerusahaan: saldoAwalPerusahaan,
                        saldoAkhirPerusahaan: saldoAkhirPerusahaan,
                        bankDetails: JSON.stringify(getDetailsFromTable("detail")),
                        perusahaanDetails: JSON.stringify(getDetailsFromTable("detail2"))
                    };

                    $.post("${request.contextPath}/bankReconciliation/save", data, function(data) {
                        //success
                        $("#bankReconciliation-form").fadeOut();
                        $("#bankReconciliation-table").fadeIn();
                        reloadBankReconciliationTable();
                    })
                } else {
                    alert("data belum lengkap. mohon lengkapi terlebih dahulu untuk melanjutkan");
                }

            });

        });

        function getDetailsFromTable(tblName) {
            var json = [];
            $("#" + tblName + "-table tbody").find("tr").each(function(k,v) {
                var map        = {};
                var id         = $(this).parent().data("id") ? $(this).parent().data("id") : null;
                var keterangan = $(this).find("td input#keterangan").val();
                var debet      = $(this).find("td input#debet").autoNumeric("get");
                var kredit     = $(this).find("td input#kredit").autoNumeric("get");

                map = {
                    id: id,
                    description: keterangan,
                    debitAmount: debet,
                    creditAmount: kredit
                }
                json.push(map);
            });

            return json;
        }
    </g:javascript>
	</body>
</html>
