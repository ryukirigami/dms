
<%@ page import="com.kombos.finance.BankReconciliation" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="bankReconciliation_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bankReconciliation.reconciliationDate.label" default="Tgl Reconciliation" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
				<div><g:message code="bankReconciliation.bank.label" default="Bank" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="bankReconciliation.saldoAwalBank.label" default="Saldo Awal Bank" /></div>
			</th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bankReconciliation.saldoAkhirBank.label" default="Saldo Akhir Bank" /></div>
            </th>


        </tr>
	</thead>
</table>

<g:javascript>
var bankReconciliationTable;
var reloadBankReconciliationTable;
$(function(){
	
	reloadBankReconciliationTable = function() {
		bankReconciliationTable.fnDraw();
	}

	
	$('#search_reconciliationDate').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_reconciliationDate_day').val(newDate.getDate());
			$('#search_reconciliationDate_month').val(newDate.getMonth()+1);
			$('#search_reconciliationDate_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			/*bankReconciliationTable.fnDraw();*/
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	bankReconciliationTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	bankReconciliationTable = $('#bankReconciliation_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "reconciliationDate",
	"mDataProp": "reconciliationDate",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true,
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	}
}

,

{
	"sName": "bank",
	"mDataProp": "bank",
	"aTargets": [0],

	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "saldoAwalBank",
	"mDataProp": "saldoAwalBank",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "saldoAkhirBank",
	"mDataProp": "saldoAkhirBank",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var bank = $('#filter_bank').val();
						if(bank){
							aoData.push(
									{"name": 'sCriteria_bank', "value": bank}
							);
						}


						var bulan = $('#bulanCari').val();
						if(bulan){
							aoData.push(
									{"name": 'bulan', "value": bulan}
							);
						}


						var tahun = $('#tahunCari').val();
						if(tahun){
							aoData.push(
									{"name": 'tahun', "value": tahun}
							);
						}


						var saldoAkhirBank = $('#filter_saldoAkhirBank input').val();
						if(saldoAkhirBank){
							aoData.push(
									{"name": 'sCriteria_saldoAkhirBank', "value": saldoAkhirBank}
							);
						}
	

	
						var saldoAwalBank = $('#filter_saldoAwalBank input').val();
						if(saldoAwalBank){
							aoData.push(
									{"name": 'sCriteria_saldoAwalBank', "value": saldoAwalBank}
							);
						}
	

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
