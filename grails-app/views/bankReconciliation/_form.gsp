<%@ page import="com.kombos.finance.BankReconciliation" %>

<g:javascript>
    $(function() {
        $("#reconciliationDate_gabisa").datepicker({
            format: "dd/mm/yyyy"
        })
    });
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: bankReconciliationInstance, field: 'bank', 'error')} ">
    <label class="control-label" for="bank">
        <g:message code="bankReconciliation.bank.label" default="Nama Bank"/>

    </label>

    <div class="controls">
        <g:select id="bank" name="bank.id" from="${com.kombos.administrasi.Bank.list()}" optionKey="id" required=""
                  value="${bankReconciliationInstance?.bank?.id}" class="many-to-one"/>
    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: bankReconciliationInstance, field: 'reconciliationDate', 'error')} ">
    <label class="control-label" for="reconciliationDate">
        <g:message code="bankReconciliation.reconciliationDate.label" default="Bulan Tahun Rekonsiliasi"/>

    </label>

    <div class="controls">
        <input id="reconciliationDate_gabisa" name="reconciliationDate" type="text" value="${bankReconciliationInstance.reconciliationDate?new java.text.SimpleDateFormat("dd/MM/yyyy").format(bankReconciliationInstance?.reconciliationDate) : ""}">
    </div>
</div>

<div class="span12" id="detail-form" style="display: block; margin-top: 20px; margin-left: -33px;">
    <legend>Saldo <strong>Bank</strong> per tanggal rekonsiliasi</legend>
    <a href="javascript:void(0);" class="btn btn-default" style="margin: 0 0 13px 0;" onclick="addRow('detail', 'saldoBankPerRekonsiliasi');"><i class="icon-plus"></i> Tambah</a>
    <div class="pull-right">
        <input type="text" value="${bankReconciliationInstance.saldoAwalBank}" class="numeric" id="saldoBankPerRekonsiliasi" >
    </div>
    <table id="detail-table" class="table table-bordered">
        <thead>
        <th>No</th>
        <th>Keterangan</th>
        <th>Debet</th>
        <th>Kredit</th>
        <th>Jumlah</th>
        <th>Aktifitas</th>
        </thead>
        <tbody>
        <g:if test="${bankReconciliationInstance.bankReconciliationDetail}">
            <% def counts = 1 %>
            <g:each in="${bankReconciliationInstance.bankReconciliationDetail}" status="i" var="detail">
                <g:if test="${detail.reconTransactionType.equals("B")}">
                    <tr data-row="${counts}" data-id="${detail.id}">
                        <td>
                            <span id="nomorUrut">${counts}</span>
                        </td>
                        <td>
                            <input id="keterangan" type="text" style="width: 96%;" value="${detail.description}">
                        </td>
                        <td>
                            <input id="debet" class="numeric" type="text" style="width: 100px;" value="${detail.debitAmount}">
                        </td>
                        <td>
                            <input id="kredit" class="numeric" type="text" style="width: 100px;" value="${detail.creditAmount}">
                        </td>
                        <td>
                            <input id="jumlah" class="numeric" type="text" style="width: 100px;" readonly value="${detail.debitAmount + detail.creditAmount}">
                        </td>
                        <td>
                            %{--<a href="javascript:void(0);">
                                <i id="btnCheckOut" class="icon-check"></i>
                            </a>--}%
                            <a href="javascript:void(0);">
                                <i onclick="deleteRow(this);" data-target="detail" data-row="${i+1}" class="icon-trash"></i>
                            </a>
                        </td>
                    </tr>
                </g:if>
                <% counts++ %>
            </g:each>
        </g:if>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="2">Total Transaksi Bank yang belum dibukukan</td>
                <td><input type="text" id="totalDebetBank"  class="numeric" style="width: 100px; font-size: 11px;" value="0" readonly></td>
                <td><input type="text" id="totalKreditBank" class="numeric" style="width: 100px; font-size: 11px;" value="0" readonly></td>
                <td><input type="text" id="totalJumlahBank" class="numeric" style="width: 100px; font-size: 11px;" value="0" readonly></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="4">Saldo Akhir Bank</td>
                <td><input type="text" id="saldoAkhir_Bank" class="numeric" style="width: 100px; font-size: 11px;" value="${bankReconciliationInstance?.saldoAkhirBank}" readonly></td>
                <td></td>
            </tr>
        </tfoot>
    </table>
</div>

<div class="span12" id="detail2-form" style="display: block; margin-top: 20px; margin-left: -33px;">
    <legend>Saldo <strong>Perusahaan</strong> per tanggal rekonsiliasi</legend>
    <a href="javascript:void(0);" class="btn btn-default" style="margin: 0 0 13px 0;" onclick="addRow('detail2', 'saldoPerusahaanPerRekonsiliasi');"><i class="icon-plus"></i> Tambah</a>
    <div class="pull-right">
        <input type="text" class="numeric" value="${bankReconciliationInstance.saldoAwalPerusahaan}" id="saldoPerusahaanPerRekonsiliasi" >
    </div>
    <table id="detail2-table" class="table table-bordered">
        <thead>
        <th>No</th>
        <th>Keterangan</th>
        <th>Debet</th>
        <th>Kredit</th>
        <th>Jumlah</th>
        <th>Aktifitas</th>
        </thead>
        <tbody>
            <g:if test="${bankReconciliationInstance.bankReconciliationDetail}">
                <% int count = 1 %>
                <g:each in="${bankReconciliationInstance.bankReconciliationDetail}" status="y" var="detail">
                    <g:if test="${detail.reconTransactionType.equals("P")}">
                        <tr data-row="${y}" data-id="${detail.id}">
                             <td>
                                 <span id="nomorUrut">${count}</span>
                             </td>
                             <td>
                                 <input id="keterangan" type="text" style="width: 96%;" value="${detail.description}">
                             </td>
                            <td>
                                <input id="debet" type="text" class="numeric" style="width: 100px;" value="${detail.debitAmount}">
                            </td>
                            <td>
                                <input id="kredit" type="text" class="numeric" style="width: 100px;" value="${detail.creditAmount}">
                            </td>
                            <td>
                                <input id="jumlah" type="text" class="numeric" style="width: 100px;" readonly value="${detail.debitAmount + detail.creditAmount}">
                            </td>
                            <td>
                                %{--<a href="javascript:void(0);">
                                    <i id="btnCheckOut" class="icon-check"></i>
                                </a>--}%
                                <a href="javascript:void(0);">
                                    <i onclick="deleteRow(this);" data-target="detail2" data-row="${y}" class="icon-trash"></i>
                                </a>
                            </td>
                        </tr>
                    </g:if>
                    <% count++ %>
                </g:each>
            </g:if>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="2">Total Transaksi Perusahaan yang belum dibukukan</td>
            <td><input type="text" id="totalDebetPerusahaan" class="numeric"  style="width: 100px; font-size: 11px;" value="0" readonly></td>
            <td><input type="text" id="totalKreditPerusahaan" class="numeric" style="width: 100px; font-size: 11px;" value="0" readonly></td>
            <td><input type="text" id="totalJumlahPerusahaan" class="numeric" style="width: 100px; font-size: 11px;" value="0" readonly></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="4">Saldo Akhir Perusahaan</td>
            <td><input class="numeric" type="text" id="saldoAkhir_Perusahaan" style="width: 100px; font-size: 11px;" value="${bankReconciliationInstance?.saldoAkhirPerusahaan}" readonly></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="4">Selisih</td>
            <td><input type="text" id="selisih" value="${bankReconciliationInstance?.selisih}" class="numeric" style="width: 100px; font-size: 11px;" value="0" readonly ></td>
            <td><a href="javascript:void(0);" id="btnHitungSelisih">Hitung Selisih</a> </td>
        </tr>
        </tfoot>
    </table>
</div>

<g:javascript>
    $(function() {
        /*$(".numeric").autoNumeric("init", {
            vMin: '-999999999.99'
        });
        performCalculation("detail");
        performCalculation("detail2");*/
        $("#detail-table tbody").on("focusout", "tr td input#debet", function(evt) {
            performCalculation("detail");
        });
        $("#detail-table tbody").on("focusout", "tr td input#kredit", function(evt) {
            performCalculation("detail");
        });

        $("#detail-table tbody").on("click", "tr td a i#btnCheckOut", function(e) {
            performCheckOut(this, "saldoBankPerRekonsiliasi", "Bank");
        })

        $("#detail2-table tbody").on("focusout", "tr td input#debet", function(evt) {
            performCalculation("detail2", "saldoPerusahaanPerRekonsiliasi");
        });

        $("#detail2-table tbody").on("focusout", "tr td input#kredit", function(evt) {
            performCalculation("detail2", "saldoPerusahaanPerRekonsiliasi");
        });

        $("#detail2-table tbody").on("click", "tr td a i#btnCheckOut", function(e) {
            performCheckOut(this, "saldoPerusahaanPerRekonsiliasi", "Perusahaan");
        })

        $("#btnHitungSelisih").click(function() {
            var iSaldoBank       = new Number($("#saldoAkhir_Bank").autoNumeric("get"));
            var iSaldoPerusahaan = new Number($("#saldoAkhir_Perusahaan").autoNumeric("get"));
            var iSelisih         = iSaldoPerusahaan - iSaldoBank;

            $("#selisih").autoNumeric("set", iSelisih);
        });

        $("#saldoPerusahaanPerRekonsiliasi").focusout(function() {
            performCalculation("detail2");
        });

        $("#saldoBankPerRekonsiliasi").focusout(function() {
            performCalculation("detail");
        });

    })
</g:javascript>