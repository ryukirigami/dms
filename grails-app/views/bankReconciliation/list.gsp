
<%@ page import="com.kombos.finance.BankReconciliation" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'bankReconciliation.label', default: 'BankReconciliation')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
        <style>
        table thead th, table tbody tr td  {font-size: 10px; white-space: nowrap;vertical-align: middle; text-align: center;}
        table.table td {text-align: center;}
        table tbody tr td {text-align: center;}
        table tbody td input[type="text"] {font-size: 10px; width: 100%;}
        #detail-form {margin-left: 0px;} #btnSearchAccount {position: relative; left: 9px; top: 2px;}
        </style>
		<r:require modules="baseapplayout, baseapplist, autoNumeric" />
		<g:javascript>
			var show;
			var edit;
			var deleteRow;
			var addDetailRow;
			var saldoBank = new Number(0);
			var saldoPerusahaan = new Number(0);
            var previewData;
			$(function(){
			    previewData = function(){
                        var namaReport = "REKONSILIASI_BANK";
                        var bulan = $('#bulanPeriode').val();
                        var tahun = $('#tahunPeriode').val();
                        window.location = "${request.contextPath}/financeReport/previewData?namaReport="+namaReport+"&bulan="+bulan+"&tahun="+tahun;
                }

                $("#cari").click(function() {
                    var bln = $("#bulanCari").val();
                    var thn = $("#tahunCari").val();
                    if((bln && thn=="") || (thn && bln=="")){
                        alert("Data bulan dan tahun harus lengkap");
                        return;
                    }
                    reloadBankReconciliationTable();
                });



				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :
						     var table = $('#bankReconciliation-table');
		                    table.fadeOut();
                            $('#bankReconciliation-form').css("display","block");
                            $('#bankReconciliation-table').css("display","block");
                            <g:remoteFunction action="create"
                                              onLoading="jQuery('#spinner').fadeIn(1);"
                                              onSuccess="loadFormInstance('bankReconciliation', data, textStatus);"
                                              onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
								function(result){
									if(result){
										massDelete('bankReconciliation', '${request.contextPath}/bankReconciliation/massdelete', reloadBankReconciliationTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('bankReconciliation','${request.contextPath}/bankReconciliation/show/'+id);
				};
				
				edit = function(id) {
					editInstance('bankReconciliation','${request.contextPath}/bankReconciliation/edit/'+id);
					 var table = $('#bankReconciliation-table');
		                    table.fadeOut();
                            $('#bankReconciliation-form').css("display","block");
                            $('#bankReconciliation-table').css("display","block");
            %{--<g:remoteFunction action="create"
                              onLoading="jQuery('#spinner').fadeIn(1);"
                              onSuccess="loadFormInstance('bankReconciliation', data, textStatus);"
                              onComplete="jQuery('#spinner').fadeOut();" />--}%
				};

            });

             function addRow(tblName, saldoInputName) {
                var mRow  = getLastRow( tblName );
                var count = new Number(mRow.count) + 1;

                $("#"+ tblName +"-table tbody").append(drawRowColumn(tblName, saldoInputName, count));
                $(".numeric").autoNumeric("init", {
                    vMin: '-999999999.99'
                });
             }

            function getLastRow(tblName) {
                if ($("#"+ tblName +"-table tbody").html().length <= 22)
                    return {
                        count: 0,
                        hasRow: false
                    }
                return {
                    count: $("#"+ tblName +"-table tbody tr:last").data("row"),
                    hasRow: true
                }
            }

            function drawRowColumn(tblName, saldoInputName, iCount) {
                return  "<tr data-row='"+iCount+"'>" +
                        "<td ><span id='nomorUrut'>"+iCount+"</span></td>" +
                        "<td><input type='text' id='keterangan' style='width: 96%;'></td>" +
                        "<td><input type='text' id='debet' class='numeric'  value='0' style='width: 100px;'></td>" +
                        "<td><input type='text' id='kredit' class='numeric' value='0' style='width: 100px;'></td>" +
                        "<td><input type='text' id='jumlah' class='numeric' style='width: 100px;' readonly></td>" +
                        "<td><a href='javascript:void(0);'><i id='btnDeleteDetail' onclick='deleteRow(this);' data-target='"+tblName+"' class='icon-trash' style='margin-left: 10px;'></i></a></td>" +
                        "</tr>";
            }

            function performCalculation(tblName) {
                var totalDebet  = new Number(0);
                var totalKredit = new Number(0);
                var jumlahSemua = new Number(0);
                var saldoAkhir  = new Number(0);
                var saldoAwal   = new Number(0);
                var elTd_debet  = null;
                var elTd_kredit = null;
                var elTd_jumlah = null;
                var elTd_saldo  = null;

                if (tblName === "detail") {
                    /*totalDebet  = $("#totalDebetBank").autoNumeric("get");
                    totalCredit = $("#totalKreditBank").autoNumeric("get");*/
                    elTd_debet  = $("#totalDebetBank");
                    elTd_kredit = $("#totalKreditBank");
                    elTd_jumlah = $("#totalJumlahBank");
                    elTd_saldo  = $("#saldoAkhir_Bank");
                    saldoAwal   = new Number($("#saldoBankPerRekonsiliasi").autoNumeric("get"));
                } else {
                    /*totalDebet  = $("#totalDebetPerusahaan").autoNumeric("get");
                    totalCredit = $("#totalKreditPerusahaan").autoNumeric("get");*/
                    elTd_debet  = $("#totalDebetPerusahaan");
                    elTd_kredit = $("#totalKreditPerusahaan");
                    elTd_saldo  = $("#saldoAkhir_Perusahaan");
                    elTd_jumlah = $("#totalJumlahPerusahaan");
                    saldoAwal   = new Number($("#saldoPerusahaanPerRekonsiliasi").autoNumeric("get"));
                }

                if ($("#"+ tblName +"-table tbody").find("tr").length < 1) {
                    elTd_debet.autoNumeric("set", totalDebet);
                    elTd_kredit.autoNumeric("set", totalKredit);
                    elTd_saldo.autoNumeric("set", saldoAkhir);
                    elTd_jumlah.autoNumeric("set", jumlahSemua);
                } else {
                    $("#"+ tblName +"-table tbody").find("tr").each(function() {
                    var sDebet    = $(this).find("td input#debet").autoNumeric("get");
                    var sKredit   = $(this).find("td input#kredit").autoNumeric("get");
                    var iDebet    = new Number(sDebet);
                    var iKredit   = new Number(sKredit)
                    var tempTotal = new Number(0);

                    tempTotal += iDebet + iKredit;

                    $(this).find("td input#jumlah").autoNumeric("set", tempTotal);

                    totalDebet  += iDebet;
                    totalKredit += iKredit;
                    jumlahSemua = totalDebet - totalKredit;
                    saldoAkhir  = jumlahSemua + saldoAwal;

                    elTd_debet.autoNumeric("set", totalDebet);
                    elTd_kredit.autoNumeric("set", totalKredit);
                    elTd_saldo.autoNumeric("set", saldoAkhir);
                    elTd_jumlah.autoNumeric("set", jumlahSemua);

                });
                }
            }

            function performCheckOut(el, saldoName, type) {
                 var sTotalJumlah = $("#totalJumlah"+type).val();
                 var sJumlah      = $(el).parent().parent().parent().find("td input#jumlah").val();
                 var iTotalJumlah = new Number(sTotalJumlah);
                 var iJumlah      = new Number(sJumlah);

                 iTotalJumlah += iJumlah;

                 if (!isNaN(iTotalJumlah)) {
                    /*alert(iTotalJumlah);*/
                      $("#totalJumlah" + type).val(iTotalJumlah);

                      var saldo = new Number($("#" + saldoName).val());
                      saldo += iTotalJumlah;

                      $("#saldoAkhir_" + type).val(saldo);
                      $(el).hide();
                 }
            }

</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="bankReconciliation-table">

			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

            <div class="span8">
                <form class="form-horizontal" action="">
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label" for="filter_bank">
                                Nama Bank
                            </label>

                            <div class="controls">
                                <g:textField name="filter_bank"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="search_reconciliationDate">
                                Bulan Tahun Rekonsiliasi
                            </label>

                            <div class="controls">
                                <select name="bulanCari" id="bulanCari" value="${skrg?.format("MM")}" >
                                    <option value="01" ${skrg?.format("MM")=="01" ? "selected" : ""}>Januari</option>
                                    <option value="02" ${skrg?.format("MM")=="02" ? "selected" : ""}>Februari</option>
                                    <option value="03" ${skrg?.format("MM")=="03" ? "selected" : ""}>Maret</option>
                                    <option value="04" ${skrg?.format("MM")=="04" ? "selected" : ""}>April</option>
                                    <option value="05" ${skrg?.format("MM")=="05" ? "selected" : ""}>Mei</option>
                                    <option value="06" ${skrg?.format("MM")=="06" ? "selected" : ""}>Juni</option>
                                    <option value="07" ${skrg?.format("MM")=="07" ? "selected" : ""}>Juli</option>
                                    <option value="08" ${skrg?.format("MM")=="08" ? "selected" : ""}>Agustus</option>
                                    <option value="09" ${skrg?.format("MM")=="09" ? "selected" : ""}>September</option>
                                    <option value="10" ${skrg?.format("MM")=="10" ? "selected" : ""}>Oktober</option>
                                    <option value="11" ${skrg?.format("MM")=="11" ? "selected" : ""}>Nomvember</option>
                                    <option value="12" ${skrg?.format("MM")=="12" ? "selected" : ""}>Desember</option>
                                </select> -
                                <g:textField name="tahunCari" id="tahunCari" value="${skrg.format("yyyy")}" maxlength="4" />
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="buttons controls">
                        %{--<a class="btn cancel" href="javascript:void(0);"
                           onclick="expandTableLayout('bankReconciliation');"><g:message
                                code="default.button.cancel.label" default="Cancel" /></a>--}%
                        <a class="btn btn-primary create" id="cari">Cari </a>
                    </fieldset>
                </form>
            </div>

			<g:render template="dataTables" />
            <div class="box">
                <legend>
                    Report
                </legend>
                <table>
                    <tr>
                        <td style="padding-right: 10px">Periode</td>
                        <td>
                            <select name="bulanPeriode" id="bulanPeriode" value="${skrg?.format("MM")}" >
                                <option value="01" ${skrg?.format("MM")=="01" ? "selected" : ""}>Januari</option>
                                <option value="02" ${skrg?.format("MM")=="02" ? "selected" : ""}>Februari</option>
                                <option value="03" ${skrg?.format("MM")=="03" ? "selected" : ""}>Maret</option>
                                <option value="04" ${skrg?.format("MM")=="04" ? "selected" : ""}>April</option>
                                <option value="05" ${skrg?.format("MM")=="05" ? "selected" : ""}>Mei</option>
                                <option value="06" ${skrg?.format("MM")=="06" ? "selected" : ""}>Juni</option>
                                <option value="07" ${skrg?.format("MM")=="07" ? "selected" : ""}>Juli</option>
                                <option value="08" ${skrg?.format("MM")=="08" ? "selected" : ""}>Agustus</option>
                                <option value="09" ${skrg?.format("MM")=="09" ? "selected" : ""}>September</option>
                                <option value="10" ${skrg?.format("MM")=="10" ? "selected" : ""}>Oktober</option>
                                <option value="11" ${skrg?.format("MM")=="11" ? "selected" : ""}>Nomvember</option>
                                <option value="12" ${skrg?.format("MM")=="12" ? "selected" : ""}>Desember</option>
                            </select> -
                        <g:textField name="tahunPeriode" id="tahunPeriode" value="${skrg.format("yyyy")}" maxlength="4" width="60px" />
                        </td>
                    </tr>
                </table>
                <g:field type="button" onclick="previewData()"  class="btn btn-primary create" name="preview"  value="${message(code: 'default.button.upload.label', default: 'Preview')}" />
            </div>
		</div>
		<div class="span12" id="bankReconciliation-form" style="display: none;"></div>
	</div>
</body>
</html>
