

<%@ page import="com.kombos.finance.BankReconciliation" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'bankReconciliation.label', default: 'BankReconciliation')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteBankReconciliation;

$(function(){ 
	deleteBankReconciliation=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/bankReconciliation/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadBankReconciliationTable();
   				expandTableLayout('bankReconciliation');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-bankReconciliation" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="bankReconciliation"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${bankReconciliationInstance?.bank}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="bank-label" class="property-label"><g:message
					code="bankReconciliation.bank.label" default="Bank" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="bank-label">
						%{--<ba:editableValue
								bean="${bankReconciliationInstance}" field="bank"
								url="${request.contextPath}/BankReconciliation/updatefield" type="text"
								title="Enter bank" onsuccess="reloadBankReconciliationTable();" />--}%
							
								<g:link controller="bank" action="show" id="${bankReconciliationInstance?.bank?.id}">${bankReconciliationInstance?.bank?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bankReconciliationInstance?.bankReconciliationDetail}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="bankReconciliationDetail-label" class="property-label"><g:message
					code="bankReconciliation.bankReconciliationDetail.label" default="Bank Reconciliation Detail" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="bankReconciliationDetail-label">
						%{--<ba:editableValue
								bean="${bankReconciliationInstance}" field="bankReconciliationDetail"
								url="${request.contextPath}/BankReconciliation/updatefield" type="text"
								title="Enter bankReconciliationDetail" onsuccess="reloadBankReconciliationTable();" />--}%
							
								<g:each in="${bankReconciliationInstance.bankReconciliationDetail}" var="b">
								<g:link controller="bankReconciliationDetail" action="show" id="${b.id}">${b?.encodeAsHTML()}</g:link>
								</g:each>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bankReconciliationInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="bankReconciliation.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${bankReconciliationInstance}" field="createdBy"
								url="${request.contextPath}/BankReconciliation/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadBankReconciliationTable();" />--}%
							
								<g:fieldValue bean="${bankReconciliationInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bankReconciliationInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="bankReconciliation.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${bankReconciliationInstance}" field="dateCreated"
								url="${request.contextPath}/BankReconciliation/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadBankReconciliationTable();" />--}%
							
								<g:formatDate date="${bankReconciliationInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bankReconciliationInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="bankReconciliation.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${bankReconciliationInstance}" field="lastUpdProcess"
								url="${request.contextPath}/BankReconciliation/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadBankReconciliationTable();" />--}%
							
								<g:fieldValue bean="${bankReconciliationInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bankReconciliationInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="bankReconciliation.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${bankReconciliationInstance}" field="lastUpdated"
								url="${request.contextPath}/BankReconciliation/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadBankReconciliationTable();" />--}%
							
								<g:formatDate date="${bankReconciliationInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bankReconciliationInstance?.perusahaan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="perusahaan-label" class="property-label"><g:message
					code="bankReconciliation.perusahaan.label" default="Perusahaan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="perusahaan-label">
						%{--<ba:editableValue
								bean="${bankReconciliationInstance}" field="perusahaan"
								url="${request.contextPath}/BankReconciliation/updatefield" type="text"
								title="Enter perusahaan" onsuccess="reloadBankReconciliationTable();" />--}%
							
								<g:fieldValue bean="${bankReconciliationInstance}" field="perusahaan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bankReconciliationInstance?.reconciliationDate}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="reconciliationDate-label" class="property-label"><g:message
					code="bankReconciliation.reconciliationDate.label" default="Reconciliation Date" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="reconciliationDate-label">
						%{--<ba:editableValue
								bean="${bankReconciliationInstance}" field="reconciliationDate"
								url="${request.contextPath}/BankReconciliation/updatefield" type="text"
								title="Enter reconciliationDate" onsuccess="reloadBankReconciliationTable();" />--}%
							
								<g:formatDate date="${bankReconciliationInstance?.reconciliationDate}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bankReconciliationInstance?.saldoAkhirBank}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="saldoAkhirBank-label" class="property-label"><g:message
					code="bankReconciliation.saldoAkhirBank.label" default="Saldo Akhir Bank" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="saldoAkhirBank-label">
						%{--<ba:editableValue
								bean="${bankReconciliationInstance}" field="saldoAkhirBank"
								url="${request.contextPath}/BankReconciliation/updatefield" type="text"
								title="Enter saldoAkhirBank" onsuccess="reloadBankReconciliationTable();" />--}%
							
								<g:fieldValue bean="${bankReconciliationInstance}" field="saldoAkhirBank"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bankReconciliationInstance?.saldoAkhirPerusahaan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="saldoAkhirPerusahaan-label" class="property-label"><g:message
					code="bankReconciliation.saldoAkhirPerusahaan.label" default="Saldo Akhir Perusahaan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="saldoAkhirPerusahaan-label">
						%{--<ba:editableValue
								bean="${bankReconciliationInstance}" field="saldoAkhirPerusahaan"
								url="${request.contextPath}/BankReconciliation/updatefield" type="text"
								title="Enter saldoAkhirPerusahaan" onsuccess="reloadBankReconciliationTable();" />--}%
							
								<g:fieldValue bean="${bankReconciliationInstance}" field="saldoAkhirPerusahaan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bankReconciliationInstance?.saldoAwalBank}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="saldoAwalBank-label" class="property-label"><g:message
					code="bankReconciliation.saldoAwalBank.label" default="Saldo Awal Bank" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="saldoAwalBank-label">
						%{--<ba:editableValue
								bean="${bankReconciliationInstance}" field="saldoAwalBank"
								url="${request.contextPath}/BankReconciliation/updatefield" type="text"
								title="Enter saldoAwalBank" onsuccess="reloadBankReconciliationTable();" />--}%
							
								<g:fieldValue bean="${bankReconciliationInstance}" field="saldoAwalBank"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bankReconciliationInstance?.saldoAwalPerusahaan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="saldoAwalPerusahaan-label" class="property-label"><g:message
					code="bankReconciliation.saldoAwalPerusahaan.label" default="Saldo Awal Perusahaan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="saldoAwalPerusahaan-label">
						%{--<ba:editableValue
								bean="${bankReconciliationInstance}" field="saldoAwalPerusahaan"
								url="${request.contextPath}/BankReconciliation/updatefield" type="text"
								title="Enter saldoAwalPerusahaan" onsuccess="reloadBankReconciliationTable();" />--}%
							
								<g:fieldValue bean="${bankReconciliationInstance}" field="saldoAwalPerusahaan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bankReconciliationInstance?.selisih}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="selisih-label" class="property-label"><g:message
					code="bankReconciliation.selisih.label" default="Selisih" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="selisih-label">
						%{--<ba:editableValue
								bean="${bankReconciliationInstance}" field="selisih"
								url="${request.contextPath}/BankReconciliation/updatefield" type="text"
								title="Enter selisih" onsuccess="reloadBankReconciliationTable();" />--}%
							
								<g:fieldValue bean="${bankReconciliationInstance}" field="selisih"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bankReconciliationInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="bankReconciliation.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${bankReconciliationInstance}" field="staDel"
								url="${request.contextPath}/BankReconciliation/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadBankReconciliationTable();" />--}%
							
								<g:fieldValue bean="${bankReconciliationInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bankReconciliationInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="bankReconciliation.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${bankReconciliationInstance}" field="updatedBy"
								url="${request.contextPath}/BankReconciliation/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadBankReconciliationTable();" />--}%
							
								<g:fieldValue bean="${bankReconciliationInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('bankReconciliation');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${bankReconciliationInstance?.id}"
					update="[success:'bankReconciliation-form',failure:'bankReconciliation-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteBankReconciliation('${bankReconciliationInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
