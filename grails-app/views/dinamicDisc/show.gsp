

<%@ page import="com.kombos.administrasi.DinamicDisc" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'dinamicDisc.label', default: 'Dinamic Discount')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteDinamicDisc;

$(function(){ 
	deleteDinamicDisc=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/dinamicDisc/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadDinamicDiscTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-dinamicDisc" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="dinamicDisc"
			class="table table-bordered table-hover">
			<tbody>

				<g:if test="${dinamicDiscInstance?.m020TMT}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m020TMT-label" class="property-label"><g:message
					code="dinamicDisc.m020TMT.label" default="Tanggal Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m020TMT-label">
						%{--<ba:editableValue
								bean="${dinamicDiscInstance}" field="m020TMT"
								url="${request.contextPath}/DinamicDisc/updatefield" type="text"
								title="Enter m020TMT" onsuccess="reloadDinamicDiscTable();" />--}%
							
								<g:formatDate date="${dinamicDiscInstance?.m020TMT}" />
							
						</span></td>
					
				</tr>
				</g:if>
				<g:if test="${dinamicDiscInstance?.m020BookingSBEProg}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m020BookingSBEProg-label" class="property-label"><g:message
					code="dinamicDisc.m020BookingSBEProg.label" default="Min SBE Progressive Disc Booking" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m020BookingSBEProg-label">
						%{--<ba:editableValue
								bean="${dinamicDiscInstance}" field="m020BookingSBEProg"
								url="${request.contextPath}/DinamicDisc/updatefield" type="text"
								title="Enter m020BookingSBEProg" onsuccess="reloadDinamicDiscTable();" />--}%
							
								<g:fieldValue bean="${dinamicDiscInstance}" field="m020BookingSBEProg"/>
							
						</span></td>
					
				</tr>
				</g:if>
				<g:if test="${dinamicDiscInstance?.m020BookingSBEMax}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m020BookingSBEMax-label" class="property-label"><g:message
					code="dinamicDisc.m020BookingSBEMax.label" default="Max SBE Progressive Disc Booking" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m020BookingSBEMax-label">
						%{--<ba:editableValue
								bean="${dinamicDiscInstance}" field="m020BookingSBEMax"
								url="${request.contextPath}/DinamicDisc/updatefield" type="text"
								title="Enter m020BookingSBEMax" onsuccess="reloadDinamicDiscTable();" />--}%
							
								<g:fieldValue bean="${dinamicDiscInstance}" field="m020BookingSBEMax"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
					<g:if test="${dinamicDiscInstance?.m020NonBookingSBEProg}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m020NonBookingSBEProg-label" class="property-label"><g:message
					code="dinamicDisc.m020NonBookingSBEProg.label" default="Min SBE Progressive Disc Non Booking" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m020NonBookingSBEProg-label">
						%{--<ba:editableValue
								bean="${dinamicDiscInstance}" field="m020NonBookingSBEProg"
								url="${request.contextPath}/DinamicDisc/updatefield" type="text"
								title="Enter m020NonBookingSBEProg" onsuccess="reloadDinamicDiscTable();" />--}%
							
								<g:fieldValue bean="${dinamicDiscInstance}" field="m020NonBookingSBEProg"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			
				<g:if test="${dinamicDiscInstance?.m020NonBookingSBEMax}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m020NonBookingSBEMax-label" class="property-label"><g:message
					code="dinamicDisc.m020NonBookingSBEMax.label" default="Max SBE Progressive Disc Non Booking" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m020NonBookingSBEMax-label">
						%{--<ba:editableValue
								bean="${dinamicDiscInstance}" field="m020NonBookingSBEMax"
								url="${request.contextPath}/DinamicDisc/updatefield" type="text"
								title="Enter m020NonBookingSBEMax" onsuccess="reloadDinamicDiscTable();" />--}%
							
								<g:fieldValue bean="${dinamicDiscInstance}" field="m020NonBookingSBEMax"/>
							
						</span></td>
					
				</tr>
				</g:if>


            <g:if test="${dinamicDiscInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="dinamicDiscInstance.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${goodsInstance}" field="createdBy"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadGoodsTable();" />--}%

                        <g:fieldValue bean="${dinamicDiscInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${dinamicDiscInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="dinamicDiscInstance.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${goodsInstance}" field="updatedBy"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadGoodsTable();" />--}%

                        <g:fieldValue bean="${dinamicDiscInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${dinamicDiscInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="dinamicDiscInstance.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${goodsInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadGoodsTable();" />--}%

                        <g:fieldValue bean="${dinamicDiscInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${dinamicDiscInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="dinamicDiscInstance.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${goodsInstance}" field="dateCreated"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadGoodsTable();" />--}%

                        <g:formatDate date="${dinamicDiscInstance?.dateCreated}" format="EEEE, dd MMMM yyyy HH:mm" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${dinamicDiscInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="dinamicDiscInstance.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${goodsInstance}" field="lastUpdated"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadGoodsTable();" />--}%

                        <g:formatDate date="${dinamicDiscInstance?.lastUpdated}" format="EEEE, dd MMMM yyyy HH:mm"/>

                    </span></td>

                </tr>
            </g:if>




            </tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${dinamicDiscInstance?.id}"
					update="[success:'dinamicDisc-form',failure:'dinamicDisc-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteDinamicDisc('${dinamicDiscInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
