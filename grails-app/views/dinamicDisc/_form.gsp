<%@ page import="com.kombos.administrasi.DinamicDisc" %>
<g:javascript>
    $(function(){
        $('.percentage').autoNumeric('init',{
            vMin:'0',
            vMax:'100',
            mDec: '2',
            aSep:''
        });
    });
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: dinamicDiscInstance, field: 'm020TMT', 'error')} ">
	<label class="control-label" for="m020TMT">
		<g:message code="dinamicDisc.m020TMT.label" default="Tanggal Berlaku" /><span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
	<ba:datePicker name="m020TMT" precision="day" value="${dinamicDiscInstance?.m020TMT}" format="dd-MM-yyyy" required="true"/>
	</div>
</div>
<g:javascript>
    document.getElementById("m020TMT").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: dinamicDiscInstance, field: 'm020BookingSBEProg', 'error')} ">
	<label class="control-label" for="m020BookingSBEProg">
		<g:message code="dinamicDisc.m020BookingSBEProg.label" default="Min SBE Progressive Disc Booking" />
		
	</label>
	<div class="controls">
        <g:textField class="percentage" name="m020BookingSBEProg" value="${dinamicDiscInstance.m020BookingSBEProg}"/>
	%{--<g:field type="number" name="m020BookingSBEProg" value="${dinamicDiscInstance.m020BookingSBEProg}" />--}%
	&nbsp;%
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: dinamicDiscInstance, field: 'm020BookingSBEMax', 'error')} ">
	<label class="control-label" for="m020BookingSBEMax">
		<g:message code="dinamicDisc.m020BookingSBEMax.label" default="Max SBE Progressive Disc Booking" />
		
	</label>
	<div class="controls">
        <g:textField class="percentage" name="m020BookingSBEMax" value="${dinamicDiscInstance.m020BookingSBEMax}"/>
	%{--<g:field type="number" name="m020BookingSBEMax" value="${dinamicDiscInstance.m020BookingSBEMax}" />--}%
	&nbsp;%
	</div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: dinamicDiscInstance, field: 'm020NonBookingSBEProg', 'error')} ">
	<label class="control-label" for="m020NonBookingSBEProg">
		<g:message code="dinamicDisc.m020NonBookingSBEProg.label" default="Min SBE Progressive Disc Non Booking" />
		
	</label>
	<div class="controls">
        <g:textField class="percentage" name="m020NonBookingSBEProg" value="${dinamicDiscInstance.m020NonBookingSBEProg}"/>
	%{--<g:field type="number" name="m020NonBookingSBEProg" value="${dinamicDiscInstance.m020NonBookingSBEProg}" />--}%
	&nbsp;%
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: dinamicDiscInstance, field: 'm020NonBookingSBEMax', 'error')} ">
	<label class="control-label" for="m020NonBookingSBEMax">
		<g:message code="dinamicDisc.m020NonBookingSBEMax.label" default="Max SBE Progressive Disc Non Booking" />
		
	</label>
	<div class="controls">
        <g:textField class="percentage" name="m020NonBookingSBEMax" value="${dinamicDiscInstance.m020NonBookingSBEMax}"/>
	%{--<g:field type="number" name="m020NonBookingSBEMax" value="${dinamicDiscInstance.m020NonBookingSBEMax}" />--}%
	&nbsp;%
	</div>
</div>






