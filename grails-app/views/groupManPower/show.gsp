

<%@ page import="com.kombos.administrasi.GroupManPower" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'groupManPower.label', default: 'Group Man Power')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteGroupManPower;

$(function(){ 
	deleteGroupManPower=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/groupManPower/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadGroupManPowerTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-groupManPower" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="groupManPower"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${groupManPowerInstance?.t021Group}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t021Group-label" class="property-label"><g:message
					code="groupManPower.t021Group.label" default="Kode Group" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t021Group-label">
						%{--<ba:editableValue
								bean="${groupManPowerInstance}" field="t021Group"
								url="${request.contextPath}/GroupManPower/updatefield" type="text"
								title="Enter t021Group" onsuccess="reloadGroupManPowerTable();" />--}%
							
								<g:fieldValue bean="${groupManPowerInstance}" field="t021Group"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${groupManPowerInstance?.namaManPowerForeman}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="namaManPowerForeman-label" class="property-label"><g:message
					code="groupManPower.namaManPowerForeman.label" default="Nama Man Power" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="namaManPowerForeman-label">
						%{--<ba:editableValue
								bean="${groupManPowerInstance}" field="namaManPowerForeman"
								url="${request.contextPath}/GroupManPower/updatefield" type="text"
								title="Enter namaManPowerForeman" onsuccess="reloadGroupManPowerTable();" />--}%
                        <g:fieldValue bean="${groupManPowerInstance?.namaManPowerForeman}" field="t015NamaLengkap"/>

						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${groupManPowerInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="jenisStall.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${groupManPowerInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadJenisStallTable();" />--}%

                        <g:fieldValue bean="${groupManPowerInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
			
				 	<g:if test="${groupManPowerInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="jenisStall.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${groupManPowerInstance}" field="dateCreated"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:formatDate date="${groupManPowerInstance?.dateCreated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${groupManPowerInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="jenisStall.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${groupManPowerInstance}" field="createdBy"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:fieldValue bean="${groupManPowerInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${groupManPowerInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="jenisStall.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${groupManPowerInstance}" field="lastUpdated"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:formatDate date="${groupManPowerInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${groupManPowerInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="jenisStall.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${groupManPowerInstance}" field="updatedBy"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:fieldValue bean="${groupManPowerInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${groupManPowerInstance?.id}"
					update="[success:'groupManPower-form',failure:'groupManPower-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteGroupManPower('${groupManPowerInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
