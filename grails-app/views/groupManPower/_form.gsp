<%@ page import="com.kombos.administrasi.ManPowerDetail; com.kombos.administrasi.ManPower; com.kombos.administrasi.NamaManPower; com.kombos.administrasi.GroupManPower" %>
<g:javascript>
        var addMember;
        var doDelete;
        var idMemberList = [];
        var jumlah = 0;
		$(function(){
			$('#t021Group').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/groupManPower/kodeList', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});

			addMember = function(aksi){
                var id = $("#anggotaGroup").val();
                if(idMemberList.indexOf(id)<0){
                    if(id==""){
                        alert("Anda belum memilih data yang akan ditambahkan")
                        return
                    }
                    idMemberList.push(id);
                    jumlah=jumlah+1;
                    $.ajax({
                        url:'${request.contextPath}/groupManPower/setSession',
                        type: "POST", // Always use POST when deleting data
                        data: { id: id }
                    });
                    var nama = $("#anggotaGroup option[value='"+id+"']").text();
                    ListTeknisiTable.fnAddData({
                        'namaManPower': nama,
                        'id': id,
                        'noUrut' : jumlah-1
                    });
                }
            }

			doDelete = function(noUrut){
                var recordsToDelete = [];
                var ind =0;
                var noDelete = [];
                $("#listTeknisi_datatables .row-select").each(function() {
                    var id = $(this).next("input:hidden").val();
                    if(this.checked){
                        recordsToDelete.push(ind);
                        var index = idMemberList.indexOf(id);
                        if (index > -1) {
                            idMemberList.splice(index, 1);
                        }
                    }else{
                        noDelete.push(id);
                    }
                    ind++;
                });
                if(recordsToDelete.length>0){
                    console.log(recordsToDelete)
                    console.log(recordsToDelete.length)
                    for(var ax=(recordsToDelete.length-1);ax>=0;ax--){
                        ListTeknisiTable.fnDeleteRow(parseInt(recordsToDelete[ax]));
                    }
                    var json = JSON.stringify(noDelete);
                    $.ajax({
                        url:'${request.contextPath}/groupManPower/doDelete',
                        type: "POST", // Always use POST when deleting data
                        data: { idExist: json }
                    });
                }
            }
		});
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: groupManPowerInstance, field: 't021Group', 'error')} ">
	<label class="control-label" for="t021Group">
		<g:message code="groupManPower.t021Group.label" default="Kode Group" />
		
	</label>
	<div class="controls">
	
	<g:textField name="t021Group" id="t021Group"  class="typeahead" maxlength="10" value="${groupManPowerInstance?.t021Group}" autocomplete="off"/>

	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: groupManPowerInstance, field: 'namaManPowerForeman', 'error')} required">
	<label class="control-label" for="namaManPowerForeman">
		<g:message code="groupManPower.namaManPowerForeman.label" default="Nama Foreman" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select noSelection="['':'Pilih Foreman']" id="namaManPowerForeman" name="namaManPowerForeman.id" from="${com.kombos.administrasi.NamaManPower.createCriteria().list {eq("companyDealer",session.userCompanyDealer);manPowerDetail{manPower{ilike("m014JabatanManPower","%foreman%")}};eq("staDel","0");order("t015NamaLengkap")}}" optionKey="id" optionValue="t015NamaLengkap" class="many-to-one"/>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="namaManPowerForeman">
		<g:message code="groupManPower.anggota.label" default="Anggota Group" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	    <g:select noSelection="['':'Pilih Anggota Group']" id="anggotaGroup" name="anggotaGroup" from="${com.kombos.administrasi.NamaManPower.createCriteria().list {eq("companyDealer",session.userCompanyDealer);manPowerDetail{manPower{ilike("m014JabatanManPower","%teknisi%")}};eq("staDel","0");order("t015NamaLengkap")}}" optionKey="id" optionValue="t015NamaLengkap" class="many-to-one"/>
        <g:if test="${groupManPowerInstance?.id}">
            <a class="btn cancel" href="javascript:void(0);"  onclick="addMember('edit');">
                Tambah
            </a>
        </g:if>
        <g:else>
            <a class="btn cancel" href="javascript:void(0);" onclick="addMember('create');" >
                Tambah
            </a>
        </g:else>
    </div>
</div>

<div class="control-group">
    <br>
    <span style="font-size: 14px"><b>Anggota Group</b></span>
    <a class="btn cancel" href="javascript:void(0);" onclick="doDelete();" >
        Delete
    </a>
    <br/>
    <br/>
    <g:render template="dataTablesTeknisi" />
</div>

<g:javascript>
        document.getElementById("t021Group").focus();
        <g:if test="${groupManPowerInstance?.namaManPowers && groupManPowerInstance?.namaManPowers.size()>0}">
            var a = 0;
            <g:each in="${groupManPowerInstance?.namaManPowers}">
                a++;
                ListTeknisiTable.fnAddData({
                    'namaManPower': "${it.t015NamaLengkap}",
                    'id': ${it.id},
                    'noUrut' : a
                });
            </g:each>
        </g:if>
</g:javascript>