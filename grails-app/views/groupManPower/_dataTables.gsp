
<%@ page import="com.kombos.administrasi.GroupManPower" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="groupManPower_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="groupManPower.t021Group.label" default="Kode Group" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="groupManPower.namaManPower.label" default="Nama Foreman" /></div>
			</th>


			
		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t021Group" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t021Group" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_namaManPower" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_namaManPower" class="search_init" />
				</div>
			</th>
	
			 
		</tr>
	</thead>
</table>

<g:javascript>
var GroupManPowerTable;
var reloadGroupManPowerTable;
$(function(){
	
	reloadGroupManPowerTable = function() {
		GroupManPowerTable.fnDraw();
	}

	var recordsGroupManPowerperpage = [];//new Array();
    var anGroupManPowerSelected;
    var jmlRecGroupManPowerPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	GroupManPowerTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	GroupManPowerTable = $('#groupManPower_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            //alert( $('li.active').text() );
            var rsGroupManPower = $("#groupManPower_datatables tbody .row-select");
            var jmlGroupManPowerCek = 0;
            var nRow;
            var idRec;
            rsGroupManPower.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsGroupManPowerperpage[idRec]=="1"){
                    jmlGroupManPowerCek = jmlGroupManPowerCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsGroupManPowerperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecGroupManPowerPerPage = rsGroupManPower.length;
            if(jmlGroupManPowerCek==jmlRecGroupManPowerPerPage && jmlRecGroupManPowerPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "t021Group",
	"mDataProp": "t021Group",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "namaManPower",
	"mDataProp": "namaManPower",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

 

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var t021Group = $('#filter_t021Group input').val();
						if(t021Group){
							aoData.push(
									{"name": 'sCriteria_t021Group', "value": t021Group}
							);
						}
	
						var namaManPower = $('#filter_namaManPower input').val();
						if(namaManPower){
							aoData.push(
									{"name": 'sCriteria_namaManPower', "value": namaManPower}
							);
						}
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#groupManPower_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordsGroupManPowerperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsGroupManPowerperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#groupManPower_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsGroupManPowerperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anGroupManPowerSelected = GroupManPowerTable.$('tr.row_selected');
            if(jmlRecGroupManPowerPerPage == anGroupManPowerSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsGroupManPowerperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
