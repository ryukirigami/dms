
<%@ page import="com.kombos.parts.StockIN" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="requestAdd_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover fixed table-selectable" style="table-layout: fixed; width: 2120px; ">
        <col width="80px" />
    	<col width="200px" />
        <col width="200px" />
        <col width="200px" />
        <col width="200px" />
        <col width="200px" />
        <col width="200px" />
        <col width="200px" />
        <col width="200px" />
        <col width="150px" />
        <col width="150px" />
        <col width="150px" />
        <col width="150px" />
    <thead>
    <tr>
        <th style="border-bottom: none; padding: 5px 10px 7px 7px; ">
            <div><input type="checkbox" class="pull-left select-all sel-all2" aria-label="Select all" title="Select all"/>&nbsp;&nbsp;No. </div>
        </th>

        <th style="border-bottom: none; padding: 5px 10px 7px 7px; text-align:center; ">
            <div class="center">Kode Part</div>
        </th>

        <th style="border-bottom: none; padding: 5px 10px 7px 7px; text-align:center; ">
            <div class="center">Nama Part</div>
        </th>

        <th style="border-bottom: none; padding: 5px 10px 7px 7px; text-align:center; ">
            <div class="center">Nomor PO</div>
        </th>
        <th style="border-bottom: none; padding: 5px 10px 7px 7px; text-align:center; ">
            <div class="center">Tanggal PO</div>
        </th>
        <th style="border-bottom: none; padding: 5px 10px 7px 7px; text-align:center; ">
            <div class="center">Nomor Invoice</div>
        </th>
        <th style="border-bottom: none; padding: 5px 10px 7px 7px; text-align:center; ">
            <div class="center">Tanggal Invoice</div>
        </th>

        <th style="border-bottom: none; padding: 5px 10px 7px 7px; text-align:center; ">
            <div class="center">Nomor Binning</div>
        </th>

        <th style="border-bottom: none; padding: 5px 10px 7px 7px; text-align:center; ">
            <div class="center">Tanggal Binning</div>
        </th>
        <th style="border-bottom: none; padding: 5px 10px 7px 7px; text-align:center; ">
            <div class="center">Qty Receive</div>
        </th>
        <th style="border-bottom: none; padding: 5px 10px 7px 7px; text-align:center; ">
            <div class="center">Qty Binning</div>
        </th>
        <th style="border-bottom: none; padding: 5px 10px 7px 7px; text-align:center; ">
            <div class="center">Qty Rusak</div>
        </th>
        <th style="border-bottom: none; padding: 5px 10px 7px 7px; text-align:center; ">
            <div class="center">Qty Salah</div>
        </th>
    </tr>
    <tr>
        <th style="border-top: none; padding: 0px 10px 7px 7px;">
            <div id="filter_urut" style="padding-top: 0px;position:relative; margin-top: 0px;">
             &nbsp;
            </div>
        </th>
        <th style="border-top: none; padding: 0px 10px 7px 7px;">
            <div id="filter_kodePart" style="padding-top: 0px;position:relative; margin-top: 0px;">
             <input type="text" name="search_kodePart" class="search_init" />
            </div>
        </th>
        <th style="border-top: none; padding: 0px 10px 7px 7px;">
            <div id="filter_namaPart" style="padding-top: 0px;position:relative; margin-top: 0px;">
                <input type="text" name="search_namaPart" class="search_init" />
            </div>
        </th>
        <th style="border-top: none; padding: 0px 10px 7px 7px;">
            <div id="filter_nomorPO" style="padding-top: 0px;position:relative; margin-top: 0px;">
                <input type="text" name="search_nomorPO" class="search_init" />
            </div>
        </th>
        <th style="border-top: none; padding: 0px 10px 7px 7px;">
            <div id="filter_tglPO" style="padding-top: 0px;position:relative; margin-top: 0px;">

                <input type="hidden" name="search_tglPO" value="date.struct">
                <input type="hidden" name="search_tglPO_day" id="search_tglPO_day" value="">
                <input type="hidden" name="search_tglPO_month" id="search_tglPO_month" value="">
                <input type="hidden" name="search_tglPO_year" id="search_tglPO_year" value="">
                <input type="text" data-date-format="dd/mm/yyyy" name="search_tglPO_dp" value="" id="search_tglPO" class="search_init">

            </div>
        </th>
        <th style="border-top: none; padding: 0px 10px 7px 7px;">
            <div id="filter_nomorInvoice" style="padding-top: 0px;position:relative; margin-top: 0px;">
                <input type="text" name="search_nomorInvoice" class="search_init" />
            </div>
        </th>
        <th style="border-top: none; padding: 0px 10px 7px 7px;">
            <div id="filter_tglInvoice" style="padding-top: 0px;position:relative; margin-top: 0px;">

                <input type="hidden" name="search_tglInvoice" value="date.struct">
                <input type="hidden" name="search_tglInvoice_day" id="search_tglInvoice_day" value="">
                <input type="hidden" name="search_tglInvoice_month" id="search_tglInvoice_month" value="">
                <input type="hidden" name="search_tglInvoice_year" id="search_tglInvoice_year" value="">
                <input type="text" data-date-format="dd/mm/yyyy" name="search_tglInvoice_dp" value="" id="search_tglInvoice" class="search_init">

            </div>
        </th>
        <th style="border-top: none; padding: 0px 10px 7px 7px;">
            <div id="filter_nomorBinning" style="padding-top: 0px;position:relative; margin-top: 0px;">
                <input type="text" name="search_nomorBinning" class="search_init" />
            </div>
        </th>
        <th style="border-top: none; padding: 0px 10px 7px 7px;">
            <div id="filter_tglBinning" style="padding-top: 0px;position:relative; margin-top: 0px;">

                <input type="hidden" name="search_tglBinning" value="date.struct">
                <input type="hidden" name="search_tglBinning_day" id="search_tglBinning_day" value="">
                <input type="hidden" name="search_tglBinning_month" id="search_tglBinning_month" value="">
                <input type="hidden" name="search_tglBinning_year" id="search_tglBinning_year" value="">
                <input type="text" data-date-format="dd/mm/yyyy" name="search_tglBinning_dp" value="" id="search_tglBinning" class="search_init">
            </div>
        </th>
        <th style="border-top: none; padding: 0px 10px 7px 7px;">
            <div id="filter_qReceive" style="padding-top: 0px;position:relative; margin-top: 0px;">
                <input type="text" name="search_qReceive" class="search_init" />
            </div>
        </th>
        <th style="border-top: none; padding: 0px 10px 7px 7px;">
            <div id="filter_qBinning" style="padding-top: 0px;position:relative; margin-top: 0px;">
                <input type="text" name="search_qBinning" class="search_init" />
            </div>
        </th>
        <th style="border-top: none; padding: 0px 10px 7px 7px;">
            <div id="filter_qRusak" style="padding-top: 0px;position:relative; margin-top: 0px;">
                <input type="text" name="search_qRusak" class="search_init" />
            </div>
        </th>
        <th style="border-top: none; padding: 0px 10px 7px 7px;">
            <div id="filter_qSalah" style="padding-top: 0px;position:relative; margin-top: 0px;">
                <input type="text" name="search_qSalah" class="search_init" />
            </div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var requestAddTable;
var reloadrequestAddTable;
$(function(){

	reloadrequestAddTable = function() {
		requestAddTable.fnDraw();
	}
    var recordspartsperpage = [];//new Array();
    var anPartsSelected;
    var jmlRecPartsPerPage=0;
    var id;

	$('#search_tglPO').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tglPO_day').val(newDate.getDate());
			$('#search_tglPO_month').val(newDate.getMonth()+1);
			$('#search_tglPO_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
            var oTable = $('#requestAdd_datatables').dataTable();
            oTable.fnReloadAjax();
            console.log("after oTable.fnReloadAjax()");
	});

	$('#search_tglInvoice').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tglInvoice_day').val(newDate.getDate());
			$('#search_tglInvoice_month').val(newDate.getMonth()+1);
			$('#search_tglInvoice_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
            var oTable = $('#requestAdd_datatables').dataTable();
            oTable.fnReloadAjax();
            console.log("after oTable.fnReloadAjax()");
	});

	$('#search_tglBinning').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tglBinning_day').val(newDate.getDate());
			$('#search_tglBinning_month').val(newDate.getMonth()+1);
			$('#search_tglBinning_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
            var oTable = $('#requestAdd_datatables').dataTable();
            oTable.fnReloadAjax();
            console.log("after oTable.fnReloadAjax()");
	});


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	requestAddTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

 	requestAddTable = $('#requestAdd_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsParts = $("#requestAdd_datatables  tbody .row-select");
            var jmlPartsCek = 0;
            var nRow;
            var idRec;
            rsParts.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordspartsperpage[idRec]=="1"){
                    jmlPartsCek = jmlPartsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordspartsperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecPartsPerPage = rsParts.length;
            if(jmlPartsCek == jmlRecPartsPerPage && jmlRecPartsPerPage > 0){
                $('.sel-all2').attr('checked', true);
            } else {
                $('.sel-all2').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "addModalDatatablesList")}",
		"aoColumns": [

{
	"sName": "id",
	"mDataProp": "norut",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"80px",
	"bVisible": true
}

,

{
	"sName": "kodePart",
	"mDataProp": "kodePart",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "namaPart",
	"mDataProp": "namaPart",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "nomorPO",
	"mDataProp": "nomorPO",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "tglPO",
	"mDataProp": "tglPO",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "nomorInvoice",
	"mDataProp": "nomorInvoice",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "tglInvoice",
	"mDataProp": "tglInvoice",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "nomorBinning",
	"mDataProp": "nomorBinning",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "tglJamBinning",
	"mDataProp": "tglJamBinning",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "qReceive",
	"mDataProp": "qReceive",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}
,

{
	"sName": "qBinning",
	"mDataProp": "qBinning",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}
,

{
	"sName": "qRusak",
	"mDataProp": "qRusak",
	"aTargets": [11],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}
,

{
	"sName": "qSalah",
	"mDataProp": "qSalah",
	"aTargets": [12],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var kodePart = $('#filter_kodePart input').val();
						if(kodePart){
							aoData.push(
									{"name": 'sCriteria_kodeParts', "value": kodePart}
							);
						}
						var namaPart = $('#filter_namaPart input').val();
						if(namaPart){
							aoData.push(
									{"name": 'sCriteria_namaParts', "value": namaPart}
							);
						}
						var nomorPO = $('#filter_nomorPO input').val();
						if(nomorPO){
							aoData.push(
									{"name": 'sCriteria_nomorPO', "value": nomorPO}
							);
						}

						var nomorInvoice = $('#filter_nomorInvoice input').val();
						if(nomorInvoice){
							aoData.push(
									{"name": 'sCriteria_nomorInvoice', "value": nomorInvoice}
							);
						}

						var qReceive = $('#filter_qReceive input').val();
						if(qReceive){
							aoData.push(
									{"name": 'sCriteria_qReceive', "value": qReceive}
							);
						}
						var nomorBinning = $('#filter_nomorBinning input').val();
						if(nomorBinning){
							aoData.push(
									{"name": 'sCriteria_nomorBinning', "value": nomorBinning}
							);
						}

						var qBinning = $('#filter_qBinning input').val();
						if(qBinning){
							aoData.push(
									{"name": 'sCriteria_qBinning', "value": qBinning}
							);
						}
						var qRusak = $('#filter_qRusak input').val();
						if(qRusak){
							aoData.push(
									{"name": 'sCriteria_qRusak', "value": qRusak}
							);
						}
						var qSalah = $('#filter_qSalah input').val();
						if(qSalah){
							aoData.push(
									{"name": 'sCriteria_qSalah', "value": qSalah}
							);
						}
						var exist =[];
                        var arrayLength = recordsPartsAll.length;
                        for (var i = 0; i < arrayLength; i++) {
                            console.log("i=" + i + ", recordsPartsAll[i]=" + recordsPartsAll[i]);
                            //Do something
                            exist.push(recordsPartsAll[i]);
                        }
						if(exist.length > 0){
							aoData.push(
										{"name": 'sCriteria_exist', "value": JSON.stringify(exist)}
							);
						}

						var tglPO = $('#search_tglPO').val();
						var tglPODay = $('#search_tglPO_day').val();
						var tglPOMonth = $('#search_tglPO_month').val();
						var tglPOYear = $('#search_tglPO_year').val();

						if(tglPO){

							aoData.push(
									{"name": 'sCriteria_tglPO', "value": "date.struct"},
									{"name": 'sCriteria_tglPO_dp', "value": tglPO},
									{"name": 'sCriteria_tglPO_day', "value": tglPODay},
									{"name": 'sCriteria_tglPO_month', "value": tglPOMonth},
									{"name": 'sCriteria_tglPO_year', "value": tglPOYear}
							);
						}

						var tglInvoice = $('#search_tglInvoice').val();
						var tglInvoiceDay = $('#search_tglInvoice_day').val();
						var tglInvoiceMonth = $('#search_tglInvoice_month').val();
						var tglInvoiceYear = $('#search_tglInvoice_year').val();

						if(tglInvoice){

							aoData.push(
									{"name": 'sCriteria_tglInvoice', "value": "date.struct"},
									{"name": 'sCriteria_tglInvoice_dp', "value": tglInvoice},
									{"name": 'sCriteria_tglInvoice_day', "value": tglInvoiceDay},
									{"name": 'sCriteria_tglInvoice_month', "value": tglInvoiceMonth},
									{"name": 'sCriteria_tglInvoice_year', "value": tglInvoiceYear}
							);
						}

						var tglBinning = $('#search_tglBinning').val();
						var tglBinningDay = $('#search_tglBinning_day').val();
						var tglBinningMonth = $('#search_tglBinning_month').val();
						var tglBinningYear = $('#search_tglBinning_year').val();

						if(tglBinning){

							aoData.push(
									{"name": 'sCriteria_tglBinning', "value": "date.struct"},
									{"name": 'sCriteria_tglBinning_dp', "value": tglBinning},
									{"name": 'sCriteria_tglBinning_day', "value": tglBinningDay},
									{"name": 'sCriteria_tglBinning_month', "value": tglBinningMonth},
									{"name": 'sCriteria_tglBinning_year', "value": tglBinningYear}
							);
						}
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
    $('.sel-all2').click(function(e) {

        $("#requestAdd_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordspartsperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordspartsperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#requestAdd_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordspartsperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anPartsSelected = requestAddTable.$('tr.row_selected');
            if(jmlRecPartsPerPage == anPartsSelected.length){
                $('.sel-all2').attr('checked', true);
            }
        } else {
            recordspartsperpage[id] = "0";
            $('.sel-all2').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>



