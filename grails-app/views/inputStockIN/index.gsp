
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
<head>
<meta name="layout" content="main">
<title>Input StockIN</title>
<r:require modules="baseapplayout"/>

<g:javascript disposition="head">
    var recordsPartsAll = [];   //untuk menyimpan dari modal
    var noUrut = 1;
	var checkOnValue;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;

        $(function () {

	$("#requestAddModal").on("show", function() {
		$("#closeAddPart").on("click", function(e) {
			$("#requestAddModal").modal('hide');
		});
	});

	$("#requestAddModal").on("hide", function() {
		$("#requestAddModal a.btn").off("click");
	});

    loadRequestAddModal = function(){
		$("#requestAddContent").empty();
		$.ajax({type:'POST', url:'${request.contextPath}/inputStockIN/addModal',
   			success:function(data,textStatus){
					$("#requestAddContent").html(data);
				    $("#requestAddModal").modal({
						"backdrop" : "static",
						"keyboard" : true,
						"show" : true
					}).css({'width': '1200px','margin-left': function () {return -($(this).width() / 2);}});

   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
		}

       tambahReq = function(){
            var checkGoods =[];
            $("#requestAdd-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    var nRow = $(this).parents('tr')[0];
                    var aData = requestAddTable.fnGetData(nRow);
                    var code = aData['kodePart'];
                    var isNotYet = true;
                    $('#parts_datatables tbody .row-select').each(function() {
                        var nRowExisting = $(this).parents('tr')[0];
                        var aDataExisting = partsTable.fnGetData(nRowExisting);
                        var codeExisting = aDataExisting['partsCode'];
                        if (code === codeExisting) {
                            isNotYet = false;
                        }
                    });
                    if (isNotYet) {
                        checkGoods.push(nRow);
                        recordsPartsAll.push(id);
                    }
                }
            });
            console.log("checkGoods=" + checkGoods + " checkGoods.length=" + checkGoods.length);
            if(checkGoods.length<1){
                alert('Anda belum memilih data yang akan ditambahkan');
                loadRequestAddModal();
            } else {
				for (var i=0;i < checkGoods.length;i++){
					var aData = requestAddTable.fnGetData(checkGoods[i]);
					partsTable.fnAddData({
					    'id': aData['id'],
                        'norut': noUrut,
                        'kodePart': aData['kodePart'],
                        'namaPart': aData['namaPart'],
                        'tglJamBinning': aData['tglJamBinning'],
                        'nomorBinning': aData['nomorBinning'],
                        'qReceive': aData['qReceive'],
                        'qBinning': aData['qBinning'],
                        'qRusak': aData['qRusak'],
                        'qSalah': aData['qSalah'],
                        'qStockIN': aData['qReceive'],
                        'qLokasiRak': aData['qLokasiRak'],
                        'qLokasiRakRusakSalah': '0'
						});
					noUrut++;

					$('#qStockIN_'+aData['id']).autoNumeric('init',{
                        vMin:'0',
                        vMax:'999999999999999999',
                        mDec: '2',
                        aSep:''
                    }).keypress(checkOnValue)
                    .change(checkOnValue);

					$('#qLokasiRak_'+aData['id']).autoNumeric('init',{
                        vMin:'0',
                        vMax:'999999999999999999',
                        mDec: '2',
                        aSep:''
                    }).keypress(checkOnValue)
                    .change(checkOnValue);

					$('#qLokasiRakRusakSalah_'+aData['id']).autoNumeric('init',{
                        vMin:'0',
                        vMax:'999999999999999999',
                        mDec: '2',
                        aSep:''
                    }).keypress(checkOnValue)
                    .change(checkOnValue);
				}
                        var rowNum = checkGoods.pop();
                       requestAddTable.fnDeleteRow(rowNum);
			}
      }

             onDeleteParts = function(){
                var checkedRows =[];
                $('#parts_datatables tbody .row-select').each(function() {
                    if(this.checked){
                        var row = $(this).closest('tr');
                        var nRow = row[0];
                        console.log("nRow=" + nRow);
                        checkedRows.push(nRow);
                    }
                });
                for (var i=checkedRows.length-1; i >= 0 ;i--){
                    var rowNum = checkedRows.pop();
                    partsTable.fnDeleteRow(rowNum);
                }
             }

             onSaveData = function(){
                 var input_rcvDate = $("#input_rcvDate").val();
                 var stockIN_id = $("#stockIN_id").val();
                 console.log("stockIN_id=" + stockIN_id);
                 if (input_rcvDate) {
                    if (confirm('${message(code: 'default.button.save.confirm.message', default: 'Anda yakin data akan disimpan?')}'))
                      {
                         try{
                             var checkJob =[];
                             var qStockIN = "";
                             var rak = "";
                             var rakRusak = "";
                                 $("#parts_datatables tbody .row-select").each(function() {
                                     if(this.checked){
                                         var id = $(this).next("input:hidden").val();
                                         checkJob.push(id);
                                         qStockIN += "&qStockIN_"+id+"="+document.getElementById("qStockIN_"+id).value;
//                                         rak += "&rak"+id+"="+document.getElementById("rak_"+id).value;
                                         rakRusak += "&rakRusak"+id+"="+document.getElementById("rakRusak_"+id).value;
                                     }
                                 });

                                 if(checkJob.length == 0){
                                     alert('Anda belum memilih data yang akan disimpan');
                                     return;
                                 }
                                console.log(rak)
                                console.log(rakRusak)
                                 var checkBoxVendor = JSON.stringify(checkJob);
                                 $.ajax({
                                     url:'${request.contextPath}/inputStockIN/save?1=1'+qStockIN+""+rakRusak,
                                     type: "POST", // Always use POST when deleting data
                                     data : { ids: checkBoxVendor, input_rcvDate:input_rcvDate, stockIN_id: stockIN_id,
                                        qStockIN:qStockIN,rakRusak:rakRusak},
                                     success : function(data){
                                         if(data.eror == "eror"){
                                            alert("Error.. Data Sudah Ada");
                                         }else{
                                            toastr.success('Save success');
                                         }
                                        shrinkTableLayout();
                                        console.log("after shrinkTableLayout()");
                                        $('#spinner').fadeIn(1);
                                        console.log("after spinner fadeIn");

                                        $.ajax({type:'POST', url:'${request.contextPath}/stockIN/',
                                            success:function(data,textStatus){
                                                //loadForm(data, textStatus);
                                                $('#stockIN-form').empty();
                                                $('#stockIN-form').append(data);
                                            },
                                            error:function(XMLHttpRequest,textStatus,errorThrown){
                                                console.log("on error save, textStatus=" + textStatus + ", errorThrown=" + errorThrown);
                                            },
                                            complete:function(XMLHttpRequest,textStatus){
                                                $('#spinner').fadeOut();
                                            }
                                        });
                                     },
                                     error: function(xhr, textStatus, errorThrown) {
                                         alert('Internal Server Error');
                                         partsTable.fnClearTable();
                                     }
                                 });

                             checkJob = [];
                         }catch (e){
                             alert(e)
                         }
                     }
                  } else {
                    if (!input_rcvDate) {
                        alert("Silahkan memilih Tanggal Stock In");
                     }
                  }
              }

    shrinkTableLayout = function(){
        $("#input-stockIN-table").hide();
        console.log("afterHide");
        $("#input-stockIN-form").css("display","block");
   	}

    loadForm = function(data, textStatus){
        console.log($('inside loadForm'));
        console.log($('#stockIN-form'));
        console.log($('#stockIN-form').html());
		$('#stockIN-form').empty();
		console.log("loadForm data=" + data);
		console.log("loadForm textStatus=" + textStatus);
    	$('#stockIN-form').append(data);
    	console.log("after loadForm");
   	}

    loadFormAfterInsert = function(data, textStatus){
        $("#stockIN-table").show();
        $("#stockIN-form").css("display","none");
    }

    cancelling = function() {
        shrinkTableLayout();
        $('#spinner').fadeIn(1);
        $.ajax({type:'POST', url:'${request.contextPath}/stockIN/',
            success:function(data,textStatus){
                //loadForm(data, textStatus);
                $('#stockIN-form').empty();
                $('#stockIN-form').append(data);
            },
            error:function(XMLHttpRequest,textStatus,errorThrown){},
            complete:function(XMLHttpRequest,textStatus){
                $('#spinner').fadeOut();
            }
        });
    }

 });
</g:javascript>

</head>

<body>
<div class="span12" id="input-stockIN-table">
<fieldset class="form">

    <div class="box">
        <form id="form-request" class="form-horizontal">
            <legend style="font-size: small">Input </legend>
            <fieldset>
            <g:hiddenField name="stockIN_id" id="stockIN_id" value="${stockINInstance?.id}" />
            <div class="control-group">
                <label class="control-label" for="input_rcvDate" style="text-align: left;">
                    Tanggal Stock In <span class="required-indicator">*</span>
                </label>
                <div id="tglStockIn" class="controls">
                    <ba:datePicker id="input_rcvDate" name="input_rcvDate" precision="day" format="dd-MM-yyyy"  value="${tgl}"  />
                </div>
            </div>
            </fieldset>
        </form>
        <fieldset class="buttons controls">
            <button class="btn btn-primary" onclick="loadRequestAddModal();">Add Parts</button>
        </fieldset>
    </div>

    <div class="box">
        <legend style="font-size: small">Stock In</legend>

        <div id="goodsReceive_table">
            <g:render template="partsDataTables"/>
        </div>
        <a class="btn cancel" href="javascript:void(0);" onclick="onDeleteParts();">
            <g:message code="default.button.delete.label" default="Delete" />
        </a>

        <button class="btn btn-primary create" onclick="onSaveData();">Save</button>

        <button class="btn cancel" onclick="cancelling();">Cancel</button>
    </div>
</fieldset>

<div id="requestAddModal" class="modal fade">
    <div class="modal-dialog" style="width: 1200px;">
        <div class="modal-content" style="width: 1200px;">
            <!-- dialog body -->
            <div class="modal-body" style="max-height: 550px;">
                <div id="requestAddContent"/>
            </div>
            </div>
    </div>
</div>
</div>

<div class="span12" id="input-stockIN-form" style="display: none;margin-left: 0;"></div>
</body>
</html>