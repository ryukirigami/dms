<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="parts_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover fixed "
       width="100%">
    <thead>
    <tr>
        <th style="border-bottom: none;padding: 5px; text-align:center" rowspan="2">
            <div><input type="checkbox" style=" width: 13px;
                            height: 13px;
                            padding: 0;
                            margin:0;
                            vertical-align: bottom;
                            position: relative;
                            top: -1px;
                            *overflow: hidden;" class="pull-left select-all" />Kode Parts</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align:center" rowspan="2">
            <div class="center">Nama Parts</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align:center" rowspan="2">
            <div class="center">Tanggal Dan Jam Binning</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align:center" rowspan="2">
            <div class="center">Nomor Binning</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align:center" colspan="7">
            <div class="center">Qty</div>
        </th>
    </tr>
    <tr>
        <th style="border-bottom: none;padding: 5px; text-align:center">
            <div class="center">Receive</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align:center">
            <div class="center">Binning</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align:center">
            <div class="center">Rusak</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align:center">
            <div class="center">Salah</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align:center">
            <div class="center">Stock In</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align:center">
            <div class="center">Lokasi/Rak</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align:center">
            <div class="center">Lokasi/Rak (Rusak/Salah)</div>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
var partsTable;
var reloadpartsTable;
$(function(){

    reloadpartsTable = function() {
		partsTable.fnDraw();
	}
    var recordspartsperpage = [];//new Array();
    var anPartsSelected;
    var jmlRecPartsPerPage=0;
    var id;

	partsTable = $('#parts_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsParts = $("#parts_datatables tbody .row-select");
            var jmlPartsCek = 0;
            var nRow;
            var idRec;
            rsParts.each(function() {
                idRec = $(this).next("input:hidden").val();
                recordsPartsAll.push(idRec);
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordspartsperpage[idRec]=="1"){
                    jmlPartsCek = jmlPartsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordspartsperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecPartsPerPage = rsParts.length;
            if(jmlPartsCek == jmlRecPartsPerPage && jmlRecPartsPerPage > 0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },

		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "inputStockINDatatablesList")}",
		"aoColumns": [
{
	"sName": "kodePart",
	"mDataProp": "kodePart",
	"aTargets": [0],
    "mRender": function ( data, type, row ) {
        return '<input type="checkbox" style="width: 13px; height: 13px; padding: 0; margin:0; vertical-align: bottom; position: relative; top: -1px; *overflow: hidden;" ' +
                        'class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
    },
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "namaPart",
	"mDataProp": "namaPart",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}

,

{
	"sName": "tglJamBinning",
	"mDataProp": "tglJamBinning",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}
,

{
	"sName": "nomorBinning",
	"mDataProp": "nomorBinning",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"90px",
	"bVisible": true
}

,

{
	"sName": "qReceive",
	"mDataProp": "qReceive",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"50px",
	"bVisible": true
}
,


{
	"sName": "qBinning",
	"mDataProp": "qBinning",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"50px",
	"bVisible": true
}
,

{
	"sName": "qRusak",
	"mDataProp": "qRusak",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"50px",
	"bVisible": true
}
,
{
	"sName": "qSalah",
	"mDataProp": "qSalah",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"50px",
	"bVisible": true
}
,

{
	"sName": "qStockIN",
	"mDataProp": "qStockIN",
	"aTargets": [8],
    "mRender": function ( data, type, row ) {
    			return '<input id="qStockIN_'+row['id']+'" class="inline-edit" type="text" style="width:70px;" value="'+data+'">';
    		},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"70px",
	"bVisible": true
}

,

{
	"sName": "qLokasiRak",
	"mDataProp": "qLokasiRak",
	"aTargets": [9],
	"bSearchable": true,
	%{--"mRender": function ( data, type, row ) {--}%
    			%{--var raks = [];--}%
                    %{--<g:each in="${com.kombos.parts.Location.list()}">--}%
                        %{--raks.push({id: "${it.id}", label: "${it.m120NamaLocation}"});--}%
                    %{--</g:each>--}%

                    %{--var rakCombo = '<select id="rak_'+row['id']+'" name="rak.id" class="many-to-one inline-edit" style="width:105px;" >';--}%
                    %{--rakCombo += '<option value="0">[- Rak -]</option>';--}%
                    %{--for(var i = 0; i < raks.length; i++){--}%
                    %{--if(data === raks[i].id)--}%
                    %{--rakCombo += '<option value="'+raks[i].id+'" selected="selected">'+raks[i].label+'</option>';--}%
                    %{--else--}%
                    %{--rakCombo += '<option value="'+raks[i].id+'">'+raks[i].label+'</option>';--}%
                    %{--}--}%
                    %{--rakCombo += '</select>';--}%

              %{--return rakCombo;--}%

    		%{--},--}%
	"bSortable": false,
	"sWidth":"250px",
	"bVisible": true
}
,

{
	"sName": "qLokasiRakRusakSalah",
	"mDataProp": "qLokasiRakRusakSalah",
	"aTargets": [10],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
    			var raks = [];
                <g:each in="${com.kombos.parts.Location.list()}">
                    raks.push({id: "${it.id}", label: "${it.m120NamaLocation}"});
                </g:each>

                var rakCombo = '<select id="rakRusak_'+row['id']+'" name="rak.id" class="many-to-one inline-edit" style="width:105px;" >';
                rakCombo += '<option value="0">[- Rak -]</option>';
                for(var i = 0; i < raks.length; i++){
                if(data === raks[i].id)
                rakCombo += '<option value="'+raks[i].id+'" selected="selected">'+raks[i].label+'</option>';
                else
                rakCombo += '<option value="'+raks[i].id+'">'+raks[i].label+'</option>';
                }
                rakCombo += '</select>';

              return rakCombo;

    		},
	"bSortable": false,
	"sWidth":"250px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
            aoData.push(
                {"name": 'stockINId', "value": "${stockINInstance?.id}"}
            );
            $.ajax({ "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData ,
                "success": function (json) {
                    fnCallback(json);
                   },
                "complete": function () {
                   }
            });
		}
	});
    $('.select-all').click(function(e) {

        $("#parts_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordspartsperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordspartsperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#parts_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordspartsperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anPartsSelected = partsTable.$('tr.row_selected');
            if(jmlRecPartsPerPage == anPartsSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordspartsperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});

</g:javascript>



