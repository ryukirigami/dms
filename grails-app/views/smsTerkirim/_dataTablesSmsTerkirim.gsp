
<%@ page import="com.kombos.reception.Reception" %>

<r:require modules="baseapplayout" />

<g:render template="../menu/maxLineDisplay"/>

<table id="smsTerkirimHistory_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="WO.jobOnly.label" default="Tanggal SMS" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="WO.jobOnly.label" default="SA" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="WO.teknisi.label" default="Nomor Polisi" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="WO.rate.label" default="Nomor HP" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="WO.rate2.label" default="Nomor WO" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="WO.rate2.label" default="Status SMS" /></div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
    var smsTerkirimHistoryTable;
    var reloadSmsTerkirimHistoryTable;
    $(function(){

        reloadSmsTerkirimHistoryTable = function() {
            smsTerkirimHistoryTable.fnClearTable();
            smsTerkirimHistoryTable.fnDraw();
        }

        smsTerkirimHistoryTable = $('#smsTerkirimHistory_datatables').dataTable({
            "sScrollX": "100%",
            "bScrollCollapse": true,
            "bAutoWidth" : false,
            "bPaginate" : false,
            "sInfo" : "",
            "sInfoEmpty" : "",
            "sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
            bFilter: true,
            "bStateSave": false,
            'sPaginationType': 'bootstrap',
            "fnInitComplete": function () {
                this.fnAdjustColumnSizing(true);
            },
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                return nRow;
            },
            "bSort": true,
            "bDestroy" : true,
            "iDisplayLength" : maxLineDisplay,
            "aoColumns": [

                {
                    "sName": "",
                    "mDataProp": "tanggalSms",
                    "aTargets": [0],
                    "bSearchable": true,
                    "bSortable": false,
                    "sWidth":"120px",
                    "bVisible": true
                }

                ,

                {
                    "sName": "",
                    "mDataProp": "SA",
                    "aTargets": [1],
                    "bSearchable": true,
                    "bSortable": false,
                    "sWidth":"120px",
                    "bVisible": true
                }


                ,

                {
                    "sName": "",
                    "mDataProp": "nopol",
                    "aTargets": [2],
                    "bSearchable": true,
                    "bSortable": false,
                    "sWidth":"150px",
                    "bVisible": true
                }

                ,

                {
                    "sName": "",
                    "mDataProp": "noHp",
                    "aTargets": [3],
                    "bSearchable": true,
                    "bSortable": false,
                    "sWidth":"120px",
                    "bVisible": true
                }

                ,

                {
                    "sName": "",
                    "mDataProp": "noWo",
                    "aTargets": [4],
                    "bSearchable": false,
                    "bSortable": false,
                    "sWidth":"120px",
                    "bVisible": true
                },

                {
                    "sName": "",
                    "mDataProp": "statusSms",
                    "aTargets": [5],
                    "bSearchable": false,
                    "bSortable": false,
                    "sWidth":"120px",
                    "bVisible": true
                }

            ]

        });


    });
</g:javascript>

