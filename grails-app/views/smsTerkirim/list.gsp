
<%@ page import="com.kombos.administrasi.KodeKotaNoPol" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'smsTerkirim.label', default: 'SMS Terkirim')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>
                loadForm = function(data, textStatus){
                    $('#smsTerkirim-form').empty();
                    $('#smsTerkirim-form').append(data);
                }
                shrinkTableLayout = function(){
                    $("#smsTerkirim-table").hide();
                    $("#smsTerkirim-form").css("display","block");
                }

                expandTableLayout = function(){
                    $("#smsTerkirim-table").show();
                    $("#smsTerkirim-form").css("display","none");
                }

     function details(){
        var id = ""
        checkSmsTerkirim =[];
        $("#smsTerkirim-table tbody .row-select").each(function() {
             if(this.checked){
               id = $(this).next("input:hidden").val();
               checkSmsTerkirim .push(id);
              }
          });
           if(checkSmsTerkirim .length<1 || checkSmsTerkirim .length>1 ){
                alert('Silahkan Pilih Salah satu');
                return;
           }else{
                reloadSmsTerkirimHistoryTable();
                $.ajax({
                    url:'${request.contextPath}/smsTerkirim/getSmsTerkirimHistory',
                    type: "POST",data : {nomorWo : id},
                    success : function(data){
                         jQuery.each(data, function (index, value) {
                           smsTerkirimHistoryTable.fnAddData({
                                'tanggalSms':value.tanggalSms,
                                'SA': value.SA,
                                'nopol': value.nopol,
                                'noHp': value.noHp,
                                'noWo': value.noWo,
                                 'statusSms' : value.statusSms
                            });
                          });
                    }
                });
           }
    }
    var checkin = $('#search_TanggalSms').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
        var newDate = new Date(ev.date)
        newDate.setDate(newDate.getDate() + 1);
        checkout.setValue(newDate);
        checkin.hide();
        $('#search_TanggalSmsAkhir')[0].focus();
    }).data('datepicker');
    
    var checkout = $('#search_TanggalSmsAkhir').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

    var checkin2 = $('#search_TanggalService').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
                var newDate2 = new Date(ev.date)
                newDate2.setDate(newDate2.getDate() + 1);
                checkout2.setValue(newDate2);
                checkin2.hide();
                $('#search_TanggalServiceAkhir')[0].focus();
    }).data('datepicker');

    var checkout2 = $('#search_TanggalServiceAkhir').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin2.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
                checkout2.hide();
    }).data('datepicker');

    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

        return true;
    }

    function smsFir(){
        window.location.replace('#/smsJawabanFirCustomer')
        $('#spinner').fadeIn(1);
        $.ajax({
            url: '${request.contextPath}/smsJawabanFirCustomer',
            type: "GET",dataType:"html", 
            complete : function (req, err) {
                $('#main-content').html(req.responseText);
                $('#spinner').fadeOut();
            }
        });
    }
                $(document).ready(function() {
                    $("#nopol3").keyup(function(e) {
                        var isi = $(e.target).val();
                        $(e.target).val(isi.toUpperCase());
                    });
                });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="smsTerkirim.view.label" default="SMS Terkirim" /></span>
    <ul class="nav pull-right">

    </ul>
</div>
<div class="box">
    <div class="span12" id="smsTerkirim-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>

        <table>
            <tr>
                <td style="padding: 5px">
                    <g:message code="wo.tanggalWOs.label" default="Tanggal SMS"/>
                </td>
                <td style="padding: 5px">
                    <ba:datePicker name="search_TanggalSms" id="search_TanggalSms" precision="day" value="" disable="true" format="dd-MM-yyyy"/>&nbsp;s.d.&nbsp;
                    <ba:datePicker name="search_TanggalSmsAkhir" id="search_TanggalSmsAkhir" precision="day" value="" disable="true" format="dd-MM-yyyy"/>
                </td>
                <td style="padding: 5px">
                    <g:message code="smsTerkirim.staJobTambah.label" default="Nomor Polisi"/>
                </td>
                <td style="padding: 5px">
                    <g:select name="nopol1" id="nopol1" from="${KodeKotaNoPol.createCriteria().list {eq("staDel",'0')}}"  optionKey="id"  style="width: 70px;"/>
                    <g:textField name="nopol2" id="nopol2" style="width: 70px" maxlength="5" onkeypress="return isNumberKey(event)" />
                    <g:textField name="nopol3" id="nopol3" style="width: 40px" maxlength="3" />
                </td>
                <td style="padding: 5px">
                    <g:message code="smsTerkirim.staProblemFinding.label" default="Inisial SA"/>
                </td>
                <td style="padding: 5px; width: 200px;">
                    <g:select style="width:100%" name="inisialSA" id="inisialSA" from="${realisasiFu}"  noSelection="${['':'Semua']}" />
                </td>

            </tr>
            <tr>
                <td style="padding: 5px">
                    <g:message code="wo.tanggalWOs.label" default="Tanggal Service"/>
                </td>
                <td style="padding: 5px">
                    <ba:datePicker name="search_TanggalService" id="search_TanggalService" precision="day" value="" disable="true" format="dd-MM-yyyy"/>&nbsp;s.d.&nbsp;
                    <ba:datePicker name="search_TanggalServiceAkhir" id="search_TanggalServiceAkhir" precision="day" value="" disable="true" format="dd-MM-yyyy"/>
                </td>

                <td style="padding: 5px">
                    <g:message code="smsTerkirim.staJobTambah.label" default="Nomor HP"/>
                </td>
                <td style="padding: 5px">
                    <g:textField name="noHp" id="noHp" style="width: 200px" maxlength="15" onkeypress="return isNumberKey(event)" />
                </td>
                <td rowspan="2" colspan="2" >
                    <div class="controls" style="margin-left: 70px">
                        <button style="width: 100px;height: 30px; border-radius: 5px" class="btn-primary view" name="view" id="view" >Filter</button>
                        <button style="width: 100px;height: 30px; border-radius: 5px" class="btn cancel" name="clear" id="clear" >Clear</button>
                    </div>
                </td>
            </tr>
            <tr>

                <td style="padding: 5px">
                    <g:message code="smsTerkirim.staFinalInspection.label" default="Status Balasan"/>
                </td>
                <td style="padding: 5px">
                    <select name="statusBalasan" id="statusBalasan" style="width: 100%">
                        <option value="">Semua</option>
                        <option value="1">Sudah Dibalas</option>
                        <option value="0">Belum Dibalas</option>
                    </select>
                </td>
                <td style="padding: 5px">
                    <g:message code="smsTerkirim.staCarryOver.label" default="Nomor WO"/>
                </td>
                <td style="padding: 5px">
                    <g:textField name="nomorWo" id="nomorWo" style="width: 200px"/>
                </td>
            </tr>
        </table>
        <br/>

        <fieldset>
            <legend>Sms Terkirim List</legend>
            <div class="controls" style="right: 0">
                <button style="width: 200px;height: 30px; border-radius: 5px" class="btn cancel" name="jawabanFir" id="jawabanFir" onclick="smsFir()" >Lihat SMS Jawaban FIR</button>
                <button style="width: 120px;height: 30px; border-radius: 5px" class="btn cancel" name="details" id="details" onclick="details()"  >Details SMS</button>
            </div>
            <g:render template="dataTables" />
        </fieldset><br/><br/><br/>
        <fieldset>
            <legend>Sms Terkirim History</legend>
            <g:render template="dataTablesSmsTerkirim" />
        </fieldset>
    </div>

    <div class="span7" id="smsTerkirim-form" style="display: none; width: 1200px;"></div>
</div>
</body>
</html>
