<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>SecurityChecklist</title>
    <script type="text/javascript">

        function initSecurityChecklistGrid(){
            jQuery("#SecurityChecklist-Grid").jqGrid({
                url: '${request.getContextPath()}/securityChecklist/getList',
                editurl: '${request.getContextPath()}/securityChecklist/edit',
                datatype: 'json',
                mtype: 'POST',
                colNames:[
                    
                    '<g:message code="app.securityChecklist.approvalStatus.label" default="ApprovalStatus"/>',  
                    '<g:message code="app.securityChecklist.category.label" default="Category"/>',  
                    '<g:message code="app.securityChecklist.code.label" default="Code"/>',  
                    '<g:message code="app.securityChecklist.defaultValue.label" default="DefaultValue"/>',  
                    '<g:message code="app.securityChecklist.enable.label" default="Enable"/>',  
                    '<g:message code="app.securityChecklist.label.label" default="Label"/>',  
                    '<g:message code="app.securityChecklist.type.label" default="Type"/>',  
                    '<g:message code="app.securityChecklist.value.label" default="Value"/>'  

                ],
                colModel :[
                    
                    {hidden:true, name:'approvalStatus', index:'approvalStatus',width:200,editable:true,edittype:'text',editrules:{required:true},searchoptions:{sopt:['eq','ne','cn','nc']}}  ,
                    {name:'category', index:'category',width:200,editable:true,edittype:'text',editrules:{required:true},searchoptions:{sopt:['eq','ne','cn','nc']}}  ,   
                    {name:'code', index:'code',width:200,editable:true,edittype:'text',editrules:{required:true},searchoptions:{sopt:['eq','ne','cn','nc']}}  ,   
                    {name:'defaultValue', index:'defaultValue',width:200,editable:true,edittype:'text',editrules:{required:true},searchoptions:{sopt:['eq','ne','cn','nc']}}  ,   
                    {name:'enable', index:'enable',width:200,editable:true,edittype:'text',editrules:{required:true},searchoptions:{sopt:['eq','ne','cn','nc']}}  ,   
                    {name:'label', index:'label',width:200,editable:true,edittype:'text',editrules:{required:true},searchoptions:{sopt:['eq','ne','cn','nc']}}  ,   
                    {name:'type', index:'type',width:200,editable:true,edittype:'text',editrules:{required:true},searchoptions:{sopt:['eq','ne','cn','nc']}}  ,   
                    {name:'value', index:'value',width:200,editable:true,edittype:'text',editrules:{required:true},searchoptions:{sopt:['eq','ne','cn','nc']}}   
                ],
                height: 'auto',
                pager: '#SecurityChecklist-Pager',
                rowNum:1000,
                rowList:[10,20,30],
                viewrecords: true,
                sortable: true,
                gridview: true,
                forceFit: true,
                loadui: 'block',
                rownumbers: true,
                caption: 'SecurityChecklist',
                onSelectRow: function(rowid){
                    var rd = jQuery("#SecurityChecklist-Grid").jqGrid('getRowData', rowid);
                    //TODO
                },
                gridComplete: function(){
                    try{
                        jQuery("#cData").click();
                        jQuery(".SecurityChecklist-Gridghead_0").addClass('ui-state-default');
                        setPermission('${request.getContextPath()}/securityChecklist','SecurityChecklist-Grid');
                    }catch(e){
                    }
                },
                sortname: 'id',
                sortorder: 'asc',
                grouping:true,
                groupingView : {
                    groupField : ['category'],
                    groupDataSorted : true,
                    groupColumnShow : false,
                    groupCollapse : true
                },
                beforeRequest: function(){
                    checkSession();
                }
            });

            jQuery('#SecurityChecklist-Grid').jqGrid(
                'navGrid','#SecurityChecklist-Pager',
                {edit:true,add:true,del:true,view:true}, // options
                {
                    afterSubmit: afterSubmitForm,
                    beforeShowForm:function(formid) {
                        checkSession();
                    }
                }, // edit options
                {
                    afterSubmit: afterSubmitForm,
                    beforeShowForm:function(formid) {
                        checkSession();
                    }
                }, // add options
                {
                    afterSubmit: afterSubmitForm,
                    onclickSubmit: function(params, posdata){
                        var id = jQuery("#SecurityChecklist-Grid").jqGrid('getGridParam','selrow');
                        var rd = jQuery("#SecurityChecklist-Grid").jqGrid('getRowData', id);
                        return {code : rd.code};
                    },
                    beforeShowForm:function(formid) {
                        checkSession();
                    }
                }, // del options
                {
                    multipleSearch:true, showOnLoad:false,
                    onInitializeSearch : function(form_id) {
                        jQuery(".vdata",form_id).keypress(function(e){
                            var code = (e.keyCode ? e.keyCode : e.which);
                            if(code == 13) { //Enter keycode
                                e.preventDefault();
                                jQuery(".ui-search").click();
                            }
                        });
                    },
                    beforeShowForm:function(formid) {
                        checkSession();
                    }
                }, // search options
                {} // view options
            );
        }

        function afterSubmitForm(response, postdata){
            var res = jQuery.parseJSON(response.responseText);
            alert(res.message);
            return [true,""];
        }

        jQuery(function(){
            initSecurityChecklistGrid();
        });

    </script>
</head>
<body>

<table id="SecurityChecklist-Grid"></table>
<div id="SecurityChecklist-Pager"></div>

</body>
</html>