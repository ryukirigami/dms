<%@ page import="com.kombos.administrasi.CompanyDealer; com.kombos.administrasi.PersenPwc" %>
<script type="text/javascript">
    jQuery(function($) {
        $('.auto').autoNumeric('init');
    });
</script>



<div class="control-group fieldcontain ${hasErrors(bean: persenPwcInstance, field: 'companyDealer', 'error')} required">
	<label class="control-label" for="companyDealer">
		<g:message code="persenPwc.companyDealer.label" default="Company Dealer" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="companyDealer" name="companyDealer.id" from="${CompanyDealer.list()}" optionKey="id" required="" value="${persenPwcInstance?.companyDealer?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: persenPwcInstance, field: 'm008TanggalBerlaku', 'error')} required">
	<label class="control-label" for="m008TanggalBerlaku">
		<g:message code="persenPwc.m008TanggalBerlaku.label" default="Tanggal Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<ba:datePicker name="m008TanggalBerlaku" precision="day" value="${persenPwcInstance?.m008TanggalBerlaku}" format="dd-MM-yyyy" required="true"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: persenPwcInstance, field: 'm008PersenPwc', 'error')} required">
	<label class="control-label" for="m008PersenPwc">
		<g:message code="persenPwc.m008PersenPwc.label" default="Persen PWC" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField style="width:40px" data-v-min="0" data-v-max="100" class="auto" maxlength="3" name="m008PersenPwc" value="${fieldValue(bean: persenPwcInstance, field: 'm008PersenPwc')}" required=""/>&nbsp;%
	</div>
</div>

<g:javascript>
    document.getElementById("companyDealer").focus();
</g:javascript>