
<%@ page import="com.kombos.administrasi.PersenPwc" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>
<script type="text/javascript">
    jQuery(function($) {
        $('.cek').autoNumeric('init');
    });
</script>
<table id="persenPwc_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="persenPwc.companyDealer.label" default="Company Dealer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="persenPwc.m008TanggalBerlaku.label" default="Tanggal Berlaku" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="persenPwc.m008PersenPwc.label" default="Persen PWC" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_companyDealer" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m008TanggalBerlaku" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m008TanggalBerlaku" value="date.struct">
					<input type="hidden" name="search_m008TanggalBerlaku_day" id="search_m008TanggalBerlaku_day" value="">
					<input type="hidden" name="search_m008TanggalBerlaku_month" id="search_m008TanggalBerlaku_month" value="">
					<input type="hidden" name="search_m008TanggalBerlaku_year" id="search_m008TanggalBerlaku_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m008TanggalBerlaku_dp" value="" id="search_m008TanggalBerlaku" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m008PersenPwc" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m008PersenPwc" class="search_init cek" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var PersenPwcTable;
var reloadPersenPwcTable;
$(function(){
	
	reloadPersenPwcTable = function() {
		PersenPwcTable.fnDraw();
	}

	
	$('#search_m008TanggalBerlaku').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m008TanggalBerlaku_day').val(newDate.getDate());
			$('#search_m008TanggalBerlaku_month').val(newDate.getMonth()+1);
			$('#search_m008TanggalBerlaku_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			PersenPwcTable.fnDraw();	
	});

	var recordsPersenPwcperpage = [];
    var anPersenPwcSelected;
    var jmlRecPersenPwcPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	PersenPwcTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	PersenPwcTable = $('#persenPwc_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            //alert( $('li.active').text() );
            var rsPersenPwc = $("#persenPwc_datatables tbody .row-select");
            var jmlPersenPwcCek = 0;
            var idRec;
            var nRow;
            rsPersenPwc.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsPersenPwcperpage[idRec]=="1"){
                    jmlPersenPwcCek = jmlPersenPwcCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsPersenPwcperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecPersenPwcPerPage = rsPersenPwc.length;
            if(jmlPersenPwcCek==jmlRecPersenPwcPerPage && jmlRecPersenPwcPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : 2,//maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "companyDealer",
	"mDataProp": "companyDealer",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m008TanggalBerlaku",
	"mDataProp": "m008TanggalBerlaku",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m008PersenPwc",
	"mDataProp": "m008PersenPwc",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}

						var m008TanggalBerlaku = $('#search_m008TanggalBerlaku').val();
						var m008TanggalBerlakuDay = $('#search_m008TanggalBerlaku_day').val();
						var m008TanggalBerlakuMonth = $('#search_m008TanggalBerlaku_month').val();
						var m008TanggalBerlakuYear = $('#search_m008TanggalBerlaku_year').val();
						
						if(m008TanggalBerlaku){
							aoData.push(
									{"name": 'sCriteria_m008TanggalBerlaku', "value": "date.struct"},
									{"name": 'sCriteria_m008TanggalBerlaku_dp', "value": m008TanggalBerlaku},
									{"name": 'sCriteria_m008TanggalBerlaku_day', "value": m008TanggalBerlakuDay},
									{"name": 'sCriteria_m008TanggalBerlaku_month', "value": m008TanggalBerlakuMonth},
									{"name": 'sCriteria_m008TanggalBerlaku_year', "value": m008TanggalBerlakuYear}
							);
						}
	
						var m008PersenPwc = $('#filter_m008PersenPwc input').val();
						if(m008PersenPwc){
							aoData.push(
									{"name": 'sCriteria_m008PersenPwc', "value": m008PersenPwc}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

    $('.select-all').click(function(e) {
        $("#persenPwc_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsPersenPwcperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsPersenPwcperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

	$('#persenPwc_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsPersenPwcperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anPersenPwcSelected = PersenPwcTable.$('tr.row_selected');
            if(jmlRecPersenPwcPerPage == anPersenPwcSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsPersenPwcperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });


});
</g:javascript>


			
