

<%@ page import="com.kombos.administrasi.PersenPwc" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'persenPwc.label', default: 'Persen PWC')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deletePersenPwc;

$(function(){ 
	deletePersenPwc=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/persenPwc/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadPersenPwcTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-persenPwc" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="persenPwc"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${persenPwcInstance?.companyDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="companyDealer-label" class="property-label"><g:message
					code="persenPwc.companyDealer.label" default="Company Dealer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="companyDealer-label">
						%{--<ba:editableValue
								bean="${persenPwcInstance}" field="companyDealer"
								url="${request.contextPath}/PersenPwc/updatefield" type="text"
								title="Enter companyDealer" onsuccess="reloadPersenPwcTable();" />--}%
							
								${persenPwcInstance?.companyDealer?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${persenPwcInstance?.m008TanggalBerlaku}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m008TanggalBerlaku-label" class="property-label"><g:message
					code="persenPwc.m008TanggalBerlaku.label" default="Tanggal Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m008TanggalBerlaku-label">
						%{--<ba:editableValue
								bean="${persenPwcInstance}" field="m008TanggalBerlaku"
								url="${request.contextPath}/PersenPwc/updatefield" type="text"
								title="Enter m008TanggalBerlaku" onsuccess="reloadPersenPwcTable();" />--}%
							
								<g:formatDate date="${persenPwcInstance?.m008TanggalBerlaku}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${persenPwcInstance?.m008PersenPwc}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m008PersenPwc-label" class="property-label"><g:message
					code="persenPwc.m008PersenPwc.label" default="Persen PWC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m008PersenPwc-label">
						%{--<ba:editableValue
								bean="${persenPwcInstance}" field="m008PersenPwc"
								url="${request.contextPath}/PersenPwc/updatefield" type="text"
								title="Enter m008PersenPwc" onsuccess="reloadPersenPwcTable();" />--}%
							
								<g:fieldValue bean="${persenPwcInstance}" field="m008PersenPwc"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${persenPwcInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="persenPwc.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${persenPwcInstance}" field="dateCreated"
                                url="${request.contextPath}/persenPwc/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadpersenPwcTable();" />--}%

                        <g:formatDate date="${persenPwcInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${persenPwcInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="persenPwc.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${persenPwcInstance}" field="createdBy"
                                url="${request.contextPath}/persenPwc/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadpersenPwcTable();" />--}%

                        <g:fieldValue bean="${persenPwcInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${persenPwcInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="persenPwc.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${persenPwcInstance}" field="lastUpdated"
                                url="${request.contextPath}/persenPwc/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadpersenPwcTable();" />--}%

                        <g:formatDate date="${persenPwcInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${persenPwcInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="persenPwc.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${persenPwcInstance}" field="updatedBy"
                                url="${request.contextPath}/persenPwc/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadpersenPwcTable();" />--}%

                        <g:fieldValue bean="${persenPwcInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${persenPwcInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="persenPwc.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${persenPwcInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/persenPwc/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadpersenPwcTable();" />--}%

                        <g:fieldValue bean="${persenPwcInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${persenPwcInstance?.id}"
					update="[success:'persenPwc-form',failure:'persenPwc-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deletePersenPwc('${persenPwcInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
