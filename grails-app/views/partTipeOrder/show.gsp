<%@ page import="com.kombos.parts.PartTipeOrder" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="bootstrapeditable" />
    <g:set var="entityName"
           value="${message(code: 'partTipeOrder.label', default: 'Tipe Order')}" />
    <title><g:message code="default.show.label" args="[entityName]" /></title>
    <g:javascript disposition="head">
var deletePartTipeOrder;

$(function(){ 
	deletePartTipeOrder=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/partTipeOrder/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadPartTipeOrderTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

    </g:javascript>
</head>
<body>
<div id="show-partTipeOrder" role="main">
<legend>
    <g:message code="default.show.label" args="[entityName]" />
</legend>
<g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
</g:if>
<table id="partTipeOrder"
       class="table table-bordered table-hover">
<tbody>


<g:if test="${partTipeOrderInstance?.m112TglBerlaku}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="m112TglBerlaku-label" class="property-label"><g:message
                    code="partTipeOrder.m112TglBerlaku.label" default="Tanggal Berlaku" />:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="m112TglBerlaku-label">
            %{--<ba:editableValue
                    bean="${partTipeOrderInstance}" field="m112TglBerlaku"
                    url="${request.contextPath}/PartTipeOrder/updatefield" type="text"
                    title="Enter m112TglBerlaku" onsuccess="reloadPartTipeOrderTable();" />--}%

            <g:formatDate date="${partTipeOrderInstance?.m112TglBerlaku}" />

        </span></td>

    </tr>
</g:if>

<g:if test="${partTipeOrderInstance?.vendor}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="vendor-label" class="property-label"><g:message
                    code="partTipeOrder.vendor.label" default="Default DEPO/SPLD" />:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="vendor-label">
            %{--<ba:editableValue
                    bean="${partTipeOrderInstance}" field="vendor"
                    url="${request.contextPath}/PartTipeOrder/updatefield" type="text"
                    title="Enter vendor" onsuccess="reloadPartTipeOrderTable();" />--}%

            ${partTipeOrderInstance?.vendor?.encodeAsHTML()}

        </span></td>

    </tr>
</g:if>

<g:if test="${partTipeOrderInstance?.m112TipeOrder}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="m112TipeOrder-label" class="property-label"><g:message
                    code="partTipeOrder.m112TipeOrder.label" default="Tipe Order" />:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="m112TipeOrder-label">
            %{--<ba:editableValue
                    bean="${partTipeOrderInstance}" field="m112TipeOrder"
                    url="${request.contextPath}/PartTipeOrder/updatefield" type="text"
                    title="Enter m112TipeOrder" onsuccess="reloadPartTipeOrderTable();" />--}%

            <g:if test="${partTipeOrderInstance.m112TipeOrder == 'Realtime/Emergency'}">
                <g:message code="partTipeOrder.m112TipeOrder.label" default="Realtime/Emergency" />
            </g:if>
            <g:if test="${partTipeOrderInstance.m112TipeOrder == 'Batch Routine/Reguler'}">
                <g:message code="partTipeOrder.m112TipeOrder.label" default="Batch Routine/Reguler" />
            </g:if>
            <g:if test="${partTipeOrderInstance.m112TipeOrder == 'Booking'}">
                <g:message code="partTipeOrder.m112TipeOrder.label" default="Booking" />
            </g:if>

        </span></td>

    </tr>
</g:if>

<g:if test="${partTipeOrderInstance?.m112JamLamaDatang1}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="m112JamLamaDatang1-label" class="property-label"><g:message
                    code="partTipeOrder.m112JamLamaDatang1.label" default="Lead Time Supply" />:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="m112JamLamaDatang1-label">
            %{--<ba:editableValue
                    bean="${partTipeOrderInstance}" field="m112JamLamaDatang1"
                    url="${request.contextPath}/PartTipeOrder/updatefield" type="text"
                    title="Enter m112JamLamaDatang1" onsuccess="reloadPartTipeOrderTable();" />--}%

            <g:fieldValue bean="${partTipeOrderInstance}" field="m112JamLamaDatang1"/>&nbsp; -

            <g:fieldValue bean="${partTipeOrderInstance}" field="m112JamLamaDatang2"/>&nbsp;Jam


        </span></td>

    </tr>
</g:if>

%{--<g:if test="${partTipeOrderInstance?.m112JamLamaDatang2}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="m112JamLamaDatang2-label" class="property-label"><g:message--}%
                    %{--code="partTipeOrder.m112JamLamaDatang2.label" default="Lead Time Supply 2" />:</span></td>--}%

        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="m112JamLamaDatang2-label">--}%
            %{--<ba:editableValue--}%
                    %{--bean="${partTipeOrderInstance}" field="m112JamLamaDatang2"--}%
                    %{--url="${request.contextPath}/PartTipeOrder/updatefield" type="text"--}%
                    %{--title="Enter m112JamLamaDatang2" onsuccess="reloadPartTipeOrderTable();" />--}%

            %{--<g:fieldValue bean="${partTipeOrderInstance}" field="m112JamLamaDatang2"/>--}%


        %{--</span></td>--}%

    %{--</tr>--}%
%{--</g:if>--}%

<g:if test="${partTipeOrderInstance?.m112CutoffTime1}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="m112CutoffTime1-label" class="property-label"><g:message
                    code="partTipeOrder.m112CutoffTime1.label" default="Cut Off Time 1" />:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="m112CutoffTime1-label">
            %{--<ba:editableValue
                    bean="${partTipeOrderInstance}" field="m112CutoffTime1"
                    url="${request.contextPath}/PartTipeOrder/updatefield" type="text"
                    title="Enter m112CutoffTime1" onsuccess="reloadPartTipeOrderTable();" />

                    <g:formatDate date="${partTipeOrderInstance?.m112CutoffTime1}" />--}%
            %{--${partTipeOrderInstance?.m112CutoffTime1}--}%
            <g:formatDate date="${partTipeOrderInstance?.m112CutoffTime1}" format="HH:mm"/>
        </span></td>

    </tr>
</g:if>

<g:if test="${partTipeOrderInstance?.m112CutoffTime2}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="m112CutoffTime2-label" class="property-label"><g:message
                    code="partTipeOrder.m112CutoffTime2.label" default="Cut Off Time 2" />:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="m112CutoffTime2-label">
            %{--<ba:editableValue
                    bean="${partTipeOrderInstance}" field="m112CutoffTime2"
                    url="${request.contextPath}/PartTipeOrder/updatefield" type="text"
                    title="Enter m112CutoffTime2" onsuccess="reloadPartTipeOrderTable();" />

                    <g:formatDate date="${partTipeOrderInstance?.m112CutoffTime2}" />--}%
            <g:formatDate date="${partTipeOrderInstance?.m112CutoffTime2}" format="HH:mm"/>
        </span></td>

    </tr>
</g:if>

<g:if test="${partTipeOrderInstance?.m112CutoffTime3}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="m112CutoffTime3-label" class="property-label"><g:message
                    code="partTipeOrder.m112CutoffTime3.label" default="Cut Off Time 3" />:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="m112CutoffTime3-label">
            %{--<ba:editableValue
                    bean="${partTipeOrderInstance}" field="m112CutoffTime3"
                    url="${request.contextPath}/PartTipeOrder/updatefield" type="text"
                    title="Enter m112CutoffTime3" onsuccess="reloadPartTipeOrderTable();" />

                    <g:formatDate date="${partTipeOrderInstance?.m112CutoffTime3}" />--}%
            <g:formatDate date="${partTipeOrderInstance?.m112CutoffTime3}" format="HH:mm"/>
        </span></td>

    </tr>
</g:if>

<g:if test="${partTipeOrderInstance?.m112CutoffTime4}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="m112CutoffTime4-label" class="property-label"><g:message
                    code="partTipeOrder.m112CutoffTime4.label" default="Cut Off Time 4" />:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="m112CutoffTime4-label">
            %{--<ba:editableValue
                    bean="${partTipeOrderInstance}" field="m112CutoffTime4"
                    url="${request.contextPath}/PartTipeOrder/updatefield" type="text"
                    title="Enter m112CutoffTime4" onsuccess="reloadPartTipeOrderTable();" />

                    <g:formatDate date="${partTipeOrderInstance?.m112CutoffTime4}" />--}%
            <g:formatDate date="${partTipeOrderInstance?.m112CutoffTime4}" format="HH:mm"/>
        </span></td>

    </tr>
</g:if>

<g:if test="${partTipeOrderInstance?.m112CutoffTime5}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="m112CutoffTime5-label" class="property-label"><g:message
                    code="partTipeOrder.m112CutoffTime5.label" default="Cut Off Time 5" />:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="m112CutoffTime5-label">
            %{--<ba:editableValue
                    bean="${partTipeOrderInstance}" field="m112CutoffTime5"
                    url="${request.contextPath}/PartTipeOrder/updatefield" type="text"
                    title="Enter m112CutoffTime5" onsuccess="reloadPartTipeOrderTable();" />

                    <g:formatDate date="${partTipeOrderInstance?.m112CutoffTime5}" />--}%
            <g:formatDate date="${partTipeOrderInstance?.m112CutoffTime5}" format="HH:mm"/>
        </span></td>

    </tr>
</g:if>

<g:if test="${partTipeOrderInstance?.m112CutoffTime6}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="m112CutoffTime6-label" class="property-label"><g:message
                    code="partTipeOrder.m112CutoffTime6.label" default="Cut Off Time 6" />:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="m112CutoffTime6-label">
            %{--<ba:editableValue
                    bean="${partTipeOrderInstance}" field="m112CutoffTime6"
                    url="${request.contextPath}/PartTipeOrder/updatefield" type="text"
                    title="Enter m112CutoffTime6" onsuccess="reloadPartTipeOrderTable();" />

                    <g:formatDate date="${partTipeOrderInstance?.m112CutoffTime6}" />--}%
            <g:formatDate date="${partTipeOrderInstance?.m112CutoffTime6}" format="HH:mm"/>
        </span></td>

    </tr>
</g:if>

<g:if test="${partTipeOrderInstance?.m112CutoffTime7}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="m112CutoffTime7-label" class="property-label"><g:message
                    code="partTipeOrder.m112CutoffTime7.label" default="Cut Off Time 7" />:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="m112CutoffTime7-label">
            %{--<ba:editableValue
                    bean="${partTipeOrderInstance}" field="m112CutoffTime7"
                    url="${request.contextPath}/PartTipeOrder/updatefield" type="text"
                    title="Enter m112CutoffTime7" onsuccess="reloadPartTipeOrderTable();" />

                    <g:formatDate date="${partTipeOrderInstance?.m112CutoffTime7}" />--}%
            <g:formatDate date="${partTipeOrderInstance?.m112CutoffTime7}" format="HH:mm"/>
        </span></td>

    </tr>
</g:if>

<g:if test="${partTipeOrderInstance?.m112CutoffTime8}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="m112CutoffTime8-label" class="property-label"><g:message
                    code="partTipeOrder.m112CutoffTime8.label" default="Cut Off Time 8" />:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="m112CutoffTime8-label">
            %{--<ba:editableValue
                    bean="${partTipeOrderInstance}" field="m112CutoffTime8"
                    url="${request.contextPath}/PartTipeOrder/updatefield" type="text"
                    title="Enter m112CutoffTime8" onsuccess="reloadPartTipeOrderTable();" />

                    <g:formatDate date="${partTipeOrderInstance?.m112CutoffTime8}" />--}%
            <g:formatDate date="${partTipeOrderInstance?.m112CutoffTime8}" format="HH:mm"/>
        </span></td>

    </tr>
</g:if>

%{--<g:if test="${partTipeOrderInstance?.staDel}">--}%
%{--<tr>--}%
%{--<td class="span2" style="text-align: right;"><span--}%
%{--id="staDel-label" class="property-label"><g:message--}%
%{--code="partTipeOrder.staDel.label" default="Sta Del" />:</span></td>--}%
%{----}%
%{--<td class="span3"><span class="property-value"--}%
%{--aria-labelledby="staDel-label">--}%
%{--<ba:editableValue--}%
%{--bean="${partTipeOrderInstance}" field="staDel"--}%
%{--url="${request.contextPath}/PartTipeOrder/updatefield" type="text"--}%
%{--title="Enter staDel" onsuccess="reloadPartTipeOrderTable();" />--}%
%{----}%
%{--<g:fieldValue bean="${partTipeOrderInstance}" field="staDel"/>--}%
%{----}%
%{--</span></td>--}%
%{----}%
%{--</tr>--}%
%{--</g:if>--}%
%{----}%
%{--<g:if test="${partTipeOrderInstance?.m112ID}">--}%
%{--<tr>--}%
%{--<td class="span2" style="text-align: right;"><span--}%
%{--id="m112ID-label" class="property-label"><g:message--}%
%{--code="partTipeOrder.m112ID.label" default="M112 ID" />:</span></td>--}%
%{----}%
%{--<td class="span3"><span class="property-value"--}%
%{--aria-labelledby="m112ID-label">--}%
%{--<ba:editableValue--}%
%{--bean="${partTipeOrderInstance}" field="m112ID"--}%
%{--url="${request.contextPath}/PartTipeOrder/updatefield" type="text"--}%
%{--title="Enter m112ID" onsuccess="reloadPartTipeOrderTable();" />--}%
%{----}%
%{--<g:fieldValue bean="${partTipeOrderInstance}" field="m112ID"/>--}%
%{----}%
%{--</span></td>--}%
%{----}%
%{--</tr>--}%
%{--</g:if>--}%
%{----}%
%{--<g:if test="${partTipeOrderInstance?.m112StaCutOffTime}">--}%
%{--<tr>--}%
%{--<td class="span2" style="text-align: right;"><span--}%
%{--id="m112StaCutOffTime-label" class="property-label"><g:message--}%
%{--code="partTipeOrder.m112StaCutOffTime.label" default="M112 Sta Cut Off Time" />:</span></td>--}%
%{----}%
%{--<td class="span3"><span class="property-value"--}%
%{--aria-labelledby="m112StaCutOffTime-label">--}%
%{--<ba:editableValue--}%
%{--bean="${partTipeOrderInstance}" field="m112StaCutOffTime"--}%
%{--url="${request.contextPath}/PartTipeOrder/updatefield" type="text"--}%
%{--title="Enter m112StaCutOffTime" onsuccess="reloadPartTipeOrderTable();" />--}%
%{----}%
%{--<g:formatDate date="${partTipeOrderInstance?.m112StaCutOffTime}" />--}%
%{----}%
%{--</span></td>--}%
%{----}%
%{--</tr>--}%
%{--</g:if>--}%

<g:if test="${partTipeOrderInstance?.lastUpdProcess}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="lastUpdProcess-label" class="property-label"><g:message
                    code="partTipeOrder.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="lastUpdProcess-label">
            %{--<ba:editableValue
                    bean="${partTipeOrderInstance}" field="lastUpdProcess"
                    url="${request.contextPath}/PartTipeOrder/updatefield" type="text"
                    title="Enter lastUpdProcess" onsuccess="reloadPartTipeOrderTable();" />--}%

            <g:fieldValue bean="${partTipeOrderInstance}" field="lastUpdProcess"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${partTipeOrderInstance?.dateCreated}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="dateCreated-label" class="property-label"><g:message
                    code="partTipeOrder.dateCreated.label" default="Date Created" />:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="dateCreated-label">
            %{--<ba:editableValue
                    bean="${partTipeOrderInstance}" field="dateCreated"
                    url="${request.contextPath}/PartTipeOrder/updatefield" type="text"
                    title="Enter dateCreated" onsuccess="reloadPartTipeOrderTable();" />--}%

            <g:formatDate date="${partTipeOrderInstance?.dateCreated}" type="datetime" style="MEDIUM" />

        </span></td>

    </tr>
</g:if>

<g:if test="${partTipeOrderInstance?.createdBy}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="createdBy-label" class="property-label"><g:message
                    code="partTipeOrder.createdBy.label" default="Created By" />:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="createdBy-label">
            %{--<ba:editableValue
                    bean="${partTipeOrderInstance}" field="createdBy"
                    url="${request.contextPath}/PartTipeOrder/updatefield" type="text"
                    title="Enter createdBy" onsuccess="reloadPartTipeOrderTable();" />--}%

            <g:fieldValue bean="${partTipeOrderInstance}" field="createdBy"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${partTipeOrderInstance?.lastUpdated}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="lastUpdated-label" class="property-label"><g:message
                    code="partTipeOrder.lastUpdated.label" default="Last Updated" />:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="lastUpdated-label">
            %{--<ba:editableValue
                    bean="${partTipeOrderInstance}" field="lastUpdated"
                    url="${request.contextPath}/PartTipeOrder/updatefield" type="text"
                    title="Enter lastUpdated" onsuccess="reloadPartTipeOrderTable();" />--}%

            <g:formatDate date="${partTipeOrderInstance?.lastUpdated}" type="datetime" style="MEDIUM" />

        </span></td>

    </tr>
</g:if>

<g:if test="${partTipeOrderInstance?.updatedBy}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="updatedBy-label" class="property-label"><g:message
                    code="partTipeOrder.updatedBy.label" default="Updated By" />:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="updatedBy-label">
            %{--<ba:editableValue
                    bean="${partTipeOrderInstance}" field="updatedBy"
                    url="${request.contextPath}/PartTipeOrder/updatefield" type="text"
                    title="Enter updatedBy" onsuccess="reloadPartTipeOrderTable();" />--}%

            <g:fieldValue bean="${partTipeOrderInstance}" field="updatedBy"/>

        </span></td>

    </tr>
</g:if>

</tbody>
</table>
<g:form class="form-horizontal">
    <fieldset class="buttons controls">
        <a class="btn cancel" href="javascript:void(0);"
           onclick="expandTableLayout();"><g:message
                code="default.button.close.label" default="Close" /></a>
        <g:remoteLink class="btn btn-primary edit" action="edit"
                      id="${partTipeOrderInstance?.id}"
                      update="[success:'partTipeOrder-form',failure:'partTipeOrder-form']"
                      on404="alert('not found');">
            <g:message code="default.button.edit.label" default="Edit" />
        </g:remoteLink>
        <ba:confirm id="delete" class="btn cancel"
                    message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
                    onsuccess="deletePartTipeOrder('${partTipeOrderInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
    </fieldset>
</g:form>
</div>
</body>
</html>
