<%@ page import="com.kombos.parts.PartTipeOrder;com.kombos.parts.Vendor" %>
<r:require modules="autoNumeric" />
<g:javascript src="../js/parttipeorder.js"/>
<g:javascript>
    <g:if test="${partTipeOrderInstance.id}">
        var data = "${partTipeOrderInstance.m112TipeOrder}"
        var dataEdit = "${partTipeOrderInstance.m112JamLamaDatang1}"
        var dataEdit2 = "${partTipeOrderInstance.m112JamLamaDatang2}"
        setValLeadTimeSupply(data,dataEdit)
        setLeadTimeSupply2(dataEdit,dataEdit2)
    </g:if>
<g:if test="${partTipeOrderInstance.m112CutoffTime1!=null}">
    $('#m112CutoffTime1_check').attr({checked:true, disabled:false});
    $('#m112CutoffTime1_hour').prop('disabled', false);
    $('#m112CutoffTime1_minute').prop('disabled', false);
</g:if>
<g:if test="${partTipeOrderInstance.m112CutoffTime2!=null}">
    $('#m112CutoffTime2_check').attr({checked:true, disabled:false});
    $('#m112CutoffTime2_hour').prop('disabled', false);
    $('#m112CutoffTime2_minute').prop('disabled', false);
</g:if>
<g:if test="${partTipeOrderInstance.m112CutoffTime3!=null}">
    $('#m112CutoffTime3_check').attr({checked:true, disabled:false});
    $('#m112CutoffTime3_hour').prop('disabled', false);
    $('#m112CutoffTime3_minute').prop('disabled', false);
</g:if>
<g:if test="${partTipeOrderInstance.m112CutoffTime4!=null}">
    $('#m112CutoffTime4_check').attr({checked:true, disabled:false});
    $('#m112CutoffTime4_hour').prop('disabled', false);
    $('#m112CutoffTime4_minute').prop('disabled', false);
</g:if>
<g:if test="${partTipeOrderInstance.m112CutoffTime5!=null}">
    $('#m112CutoffTime5_check').attr({checked:true, disabled:false});
    $('#m112CutoffTime5_hour').prop('disabled', false);
    $('#m112CutoffTime5_minute').prop('disabled', false);
</g:if>
<g:if test="${partTipeOrderInstance.m112CutoffTime6!=null}">
    $('#m112CutoffTime6_check').attr({checked:true, disabled:false});
    $('#m112CutoffTime6_hour').prop('disabled', false);
    $('#m112CutoffTime6_minute').prop('disabled', false);
</g:if>
<g:if test="${partTipeOrderInstance.m112CutoffTime7!=null}">
    $('#m112CutoffTime7_check').attr({checked:true, disabled:false});
    $('#m112CutoffTime7_hour').prop('disabled', false);
    $('#m112CutoffTime7_minute').prop('disabled', false);
</g:if>
<g:if test="${partTipeOrderInstance.m112CutoffTime8!=null}">
    $('#m112CutoffTime8_check').attr({checked:true, disabled:false});
    $('#m112CutoffTime8_hour').prop('disabled', false);
    $('#m112CutoffTime8_minute').prop('disabled', false);
</g:if>
</g:javascript>


<div class="control-group fieldcontain ${hasErrors(bean: partTipeOrderInstance, field: 'm112TglBerlaku', 'error')} required">
    <label class="control-label" for="m112TglBerlaku">
        <g:message code="partTipeOrder.m112TglBerlaku.label" default="Tanggal Berlaku"/>
        <span class="required-indicator">*</span>
    </label>

    <div class="controls">
        <ba:datePicker required="true" name="m112TglBerlaku" precision="day" value="${partTipeOrderInstance?.m112TglBerlaku}" format="dd/MM/yyyy"/>
    </div>
</div>
<g:javascript>
    document.getElementById("m112TglBerlaku").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: partTipeOrderInstance, field: 'vendor', 'error')} required">
    <label class="control-label" for="vendor">
        <g:message code="partTipeOrder.vendor.label" default="Default DEPO/SPLD"/>
        <span class="required-indicator">*</span>
    </label>

    <div class="controls">
        <g:select id="vendor" name="vendor.id" required="" from="${Vendor.createCriteria().list(){eq("staDel", "0");order("m121Nama","asc")}}" optionKey="id"
                  value="${partTipeOrderInstance?.vendor?.id}" class="many-to-one" noSelection="['': '']"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partTipeOrderInstance, field: 'm112TipeOrder', 'error')} required">
    <label class="control-label" for="m112TipeOrder">
        <g:message code="partTipeOrder.m112TipeOrder.label" default="Tipe Order"/>
        <span class="required-indicator">*</span>
    </label>

    <div class="controls">
        <g:radioGroup name="m112TipeOrder" id="m112TipeOrder" values="['Realtime/Emergency','Batch Routine/Reguler','Booking']" labels="['Realtime/Emergency','Batch Routine/Reguler','Booking']" onclick="setValLeadTimeSupply(this.value,null)" value="${partTipeOrderInstance?.m112TipeOrder}" required="">
            ${it.radio} ${it.label}
        </g:radioGroup>
        %{--<g:textField name="m112TipeOrder" maxlength="1" required="" value="${partTipeOrderInstance?.m112TipeOrder}"/>--}%
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partTipeOrderInstance, field: 'm112JamLamaDatang1', 'error')} required">
    <label class="control-label" for="m112JamLamaDatang1">
        <g:message code="partTipeOrder.m112JamLamaDatang1.label" default="Lead Time Supply (Jam)"/>
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <select id="m112JamLamaDatang1" name="m112JamLamaDatang1" onchange="setLeadTimeSupply2(document.getElementById('m112JamLamaDatang1').value,null)" required="" style="width: 60px">
            <option value=""></option>
        </select>
        <select id="m112JamLamaDatang2" name="m112JamLamaDatang2" required="" style="width: 60px">
            <option value=""></option>
        </select>
        %{--<g:field name="m112JamLamaDatang1" type="number" value="${partTipeOrderInstance.m112JamLamaDatang1}" required=""/>--}%
        %{--<g:textField style="width: 75px" name="m112JamLamaDatang1" id="m112JamLamaDatang1" class="auto" value="${fieldValue(bean : partTipeOrderInstance, field : 'm112JamLamaDatang1') }" required=""/>--}%
        %{--&nbsp;&nbsp;s.d.&nbsp;&nbsp;--}%
        %{--<g:textField style="width: 75px"  name="m112JamLamaDatang2" id="m112JamLamaDatang2" class="auto" value="${fieldValue(bean : partTipeOrderInstance, field : 'm112JamLamaDatang2')}" required=""/>--}%
    </div>
</div>

<fieldset>
    <legend>Cut Off Time</legend>

<div class="control-group fieldcontain ${hasErrors(bean: partTipeOrderInstance, field: 'm112CutoffTime1', 'error')}">
    <label class="control-label" for="m112CutoffTime1">
        <g:message code="partTipeOrder.m112CutoffTime1.label" default="Cut Off Time 1" />

    </label>
    <div class="controls">
        <g:checkBox name="m112CutoffTime1_check" id="m112CutoffTime1_check" />
        %{--<g:datePicker name="m112CutoffTime1" precision="minute"  value="${partTipeOrderInstance?.m112CutoffTime1}"  />--}%
        <select id="m112CutoffTime1_hour" disabled="true" name="m112CutoffTime1_hour" style="width: 60px" >
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==partTipeOrderInstance?.m112CutoffTime1?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==partTipeOrderInstance?.m112CutoffTime1?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m112CutoffTime1_minute" disabled="true" name="m112CutoffTime1_minute" style="width: 60px">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==partTipeOrderInstance?.m112CutoffTime1?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==partTipeOrderInstance?.m112CutoffTime1?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partTipeOrderInstance, field: 'm112CutoffTime2', 'error')} ">
    <label class="control-label" for="m112CutoffTime2">
        <g:message code="partTipeOrder.m112CutoffTime2.label" default="Cut Off Time 2" />

    </label>
    <div class="controls">
        <g:checkBox name="m112CutoffTime2_check" id="m112CutoffTime2_check" disabled="true"/>
        %{--<g:datePicker name="m112CutoffTime2" precision="minute"  value="${partTipeOrderInstance?.m112CutoffTime2}"  />--}%
        <select id="m112CutoffTime2_hour" disabled="true" name="m112CutoffTime2_hour" style="width: 60px">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==partTipeOrderInstance?.m112CutoffTime2?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==partTipeOrderInstance?.m112CutoffTime2?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m112CutoffTime2_minute" disabled="true" name="m112CutoffTime2_minute" style="width: 60px">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==partTipeOrderInstance?.m112CutoffTime2?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==partTipeOrderInstance?.m112CutoffTime2?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partTipeOrderInstance, field: 'm112CutoffTime3', 'error')}">
    <label class="control-label" for="m112CutoffTime3">
        <g:message code="partTipeOrder.m112CutoffTime3.label" default="Cut Off Time 3" />

    </label>
    <div class="controls">
        <g:checkBox name="m112CutoffTime3_check" id="m112CutoffTime3_check" disabled="true"/>
        %{--<g:datePicker name="m112CutoffTime3" precision="minute"  value="${partTipeOrderInstance?.m112CutoffTime3}"  />--}%
        <select id="m112CutoffTime3_hour" disabled="true" name="m112CutoffTime3_hour" style="width: 60px">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==partTipeOrderInstance?.m112CutoffTime3?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==partTipeOrderInstance?.m112CutoffTime3?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m112CutoffTime3_minute" disabled="true" name="m112CutoffTime3_minute" style="width: 60px">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==partTipeOrderInstance?.m112CutoffTime3?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==partTipeOrderInstance?.m112CutoffTime3?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partTipeOrderInstance, field: 'm112CutoffTime4', 'error')}">
    <label class="control-label" for="m112CutoffTime4">
        <g:message code="partTipeOrder.m112CutoffTime4.label" default="Cut Off Time 4" />

    </label>
    <div class="controls">
        <g:checkBox name="m112CutoffTime4_check" id="m112CutoffTime4_check" disabled="true"/>
        %{--<g:datePicker name="m112CutoffTime4" precision="minute"  value="${partTipeOrderInstance?.m112CutoffTime4}"  />--}%
        <select id="m112CutoffTime4_hour" disabled="true" name="m112CutoffTime4_hour" style="width: 60px">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==partTipeOrderInstance?.m112CutoffTime4?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==partTipeOrderInstance?.m112CutoffTime4?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m112CutoffTime4_minute" disabled="true" name="m112CutoffTime4_minute" style="width: 60px">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==partTipeOrderInstance?.m112CutoffTime4?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==partTipeOrderInstance?.m112CutoffTime4?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partTipeOrderInstance, field: 'm112CutoffTime5', 'error')}">
    <label class="control-label" for="m112CutoffTime5">
        <g:message code="partTipeOrder.m112CutoffTime5.label" default="Cut Off Time 5" />

    </label>
    <div class="controls">
        <g:checkBox name="m112CutoffTime5_check" id="m112CutoffTime5_check" disabled="true"/>
        %{--<g:datePicker name="m112CutoffTime5" precision="minute"  value="${partTipeOrderInstance?.m112CutoffTime5}"  />--}%
        <select id="m112CutoffTime5_hour" disabled="true" name="m112CutoffTime5_hour" style="width: 60px">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==partTipeOrderInstance?.m112CutoffTime5?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==partTipeOrderInstance?.m112CutoffTime5?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m112CutoffTime5_minute" disabled="true" name="m112CutoffTime5_minute" style="width: 60px">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==partTipeOrderInstance?.m112CutoffTime5?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==partTipeOrderInstance?.m112CutoffTime5?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partTipeOrderInstance, field: 'm112CutoffTime6', 'error')}">
    <label class="control-label" for="m112CutoffTime6">
        <g:message code="partTipeOrder.m112CutoffTime6.label" default="Cut Off Time 6" />

    </label>
    <div class="controls">
        <g:checkBox name="m112CutoffTime6_check" id="m112CutoffTime6_check" disabled="true"/>
        %{--<g:datePicker name="m112CutoffTime6" precision="minute"  value="${partTipeOrderInstance?.m112CutoffTime6}"  />--}%
        <select id="m112CutoffTime6_hour" disabled="true" name="m112CutoffTime6_hour" style="width: 60px">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==partTipeOrderInstance?.m112CutoffTime6?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==partTipeOrderInstance?.m112CutoffTime6?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m112CutoffTime6_minute" disabled="true" name="m112CutoffTime6_minute" style="width: 60px">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==partTipeOrderInstance?.m112CutoffTime6?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==partTipeOrderInstance?.m112CutoffTime6?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partTipeOrderInstance, field: 'm112CutoffTime7', 'error')}">
    <label class="control-label" for="m112CutoffTime7">
        <g:message code="partTipeOrder.m112CutoffTime7.label" default="Cut Off Time 7" />

    </label>
    <div class="controls">
        <g:checkBox name="m112CutoffTime7_check" id="m112CutoffTime7_check" disabled="true"/>
        %{--<g:datePicker name="m112CutoffTime7" precision="minute"  value="${partTipeOrderInstance?.m112CutoffTime7}"  />--}%
        <select id="m112CutoffTime7_hour" disabled="true" name="m112CutoffTime7_hour" style="width: 60px">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==partTipeOrderInstance?.m112CutoffTime7?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==partTipeOrderInstance?.m112CutoffTime7?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m112CutoffTime7_minute" disabled="true" name="m112CutoffTime7_minute" style="width: 60px">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==partTipeOrderInstance?.m112CutoffTime7?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==partTipeOrderInstance?.m112CutoffTime7?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partTipeOrderInstance, field: 'm112CutoffTime8', 'error')}">
    <label class="control-label" for="m112CutoffTime8">
        <g:message code="partTipeOrder.m112CutoffTime8.label" default="Cut Off Time 8" />

    </label>
    <div class="controls">
        <g:checkBox name="m112CutoffTime8_check" id="m112CutoffTime8_check" disabled="true"/>
        %{--<g:datePicker name="m112CutoffTime8" precision="minute"  value="${partTipeOrderInstance?.m112CutoffTime8}"  />--}%
        <select id="m112CutoffTime8_hour" disabled="true" name="m112CutoffTime8_hour" style="width: 60px">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==partTipeOrderInstance?.m112CutoffTime8?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==partTipeOrderInstance?.m112CutoffTime8?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m112CutoffTime8_minute" disabled="true" name="m112CutoffTime8_minute" style="width: 60px">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==partTipeOrderInstance?.m112CutoffTime8?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==partTipeOrderInstance?.m112CutoffTime8?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
    </div>
</div>
</fieldset>

%{--<div class="control-group fieldcontain ${hasErrors(bean: partTipeOrderInstance, field: 'staDel', 'error')} required">--}%
    %{--<label class="control-label" for="staDel">--}%
        %{--<g:message code="partTipeOrder.staDel.label" default="Sta Del"/>--}%
        %{--<span class="required-indicator">*</span>--}%
    %{--</label>--}%

    %{--<div class="controls">--}%
        %{--<g:textField name="staDel" maxlength="1" required="" value="${partTipeOrderInstance?.staDel}"/>--}%
    %{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: partTipeOrderInstance, field: 'm112ID', 'error')} ">--}%
    %{--<label class="control-label" for="m112ID">--}%
        %{--<g:message code="partTipeOrder.m112ID.label" default="M112 ID"/>--}%

    %{--</label>--}%

    %{--<div class="controls">--}%
        %{--<g:textField name="m112ID" maxlength="6" value="${partTipeOrderInstance?.m112ID}"/>--}%
    %{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: partTipeOrderInstance, field: 'm112StaCutOffTime', 'error')} ">--}%
    %{--<label class="control-label" for="m112StaCutOffTime">--}%
        %{--<g:message code="partTipeOrder.m112StaCutOffTime.label" default="M112 Sta Cut Off Time"/>--}%

    %{--</label>--}%

    %{--<div class="controls">--}%
        %{--<g:datePicker name="m112StaCutOffTime" precision="minute" value="${partTipeOrderInstance?.m112StaCutOffTime}"--}%
                      %{--default="none" noSelection="['': '']"/>--}%
    %{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: partTipeOrderInstance, field: 'createdBy', 'error')} ">--}%
    %{--<label class="control-label" for="createdBy">--}%
        %{--<g:message code="partTipeOrder.createdBy.label" default="Created By"/>--}%

    %{--</label>--}%

    %{--<div class="controls">--}%
        %{--<g:textField name="createdBy" value="${partTipeOrderInstance?.createdBy}"/>--}%
    %{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: partTipeOrderInstance, field: 'updatedBy', 'error')} ">--}%
    %{--<label class="control-label" for="updatedBy">--}%
        %{--<g:message code="partTipeOrder.updatedBy.label" default="Updated By"/>--}%

    %{--</label>--}%

    %{--<div class="controls">--}%
        %{--<g:textField name="updatedBy" value="${partTipeOrderInstance?.updatedBy}"/>--}%
    %{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: partTipeOrderInstance, field: 'lastUpdProcess', 'error')} ">--}%
    %{--<label class="control-label" for="lastUpdProcess">--}%
        %{--<g:message code="partTipeOrder.lastUpdProcess.label" default="Last Upd Process"/>--}%

    %{--</label>--}%

    %{--<div class="controls">--}%
        %{--<g:textField name="lastUpdProcess" value="${partTipeOrderInstance?.lastUpdProcess}"/>--}%
    %{--</div>--}%
%{--</div>--}%

