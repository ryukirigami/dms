
<%@ page import="com.kombos.parts.PartTipeOrder" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</g:javascript>

<table id="partTipeOrder_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="100%">
<thead>
<tr>

    <th style="border-bottom: none;padding: 5px; vertical-align:bottom;" rowspan="2">
        <div><g:message code="partTipeOrder.m112TglBerlaku.label" default="Tanggal Berlaku" /></div>
    </th>


    <th style="border-bottom: none;padding: 5px; vertical-align:bottom;" rowspan="2">
        <div><g:message code="partTipeOrder.vendor.label" default="Default DEPO/SPLD" /></div>
    </th>


    <th style="border-bottom: none;padding: 5px; vertical-align:bottom;" rowspan="2">
        <div><g:message code="partTipeOrder.m112TipeOrder.label" default="Tipe Order" /></div>
    </th>


    <th style="border-bottom: none;padding: 5px; vertical-align:bottom;" rowspan="2">
        <div><g:message code="partTipeOrder.m112JamLamaDatang1.label" default="Lead Time Supply (Jam)" /></div>
    </th>


    <th style="border-bottom: none;padding: 5px; text-align: center" colspan="8">
        <div><g:message code="partTipeOrder.cutOffTime.label" default="Cut Off Time" /></div>
    </th>

</tr>
<tr>


    <th style="border-bottom: none;padding: 5px; text-align: center">
        <div><g:message code="partTipeOrder.m112CutoffTime1.label" default="1" /></div>
    </th>


    <th style="border-bottom: none;padding: 5px; text-align: center">
        <div><g:message code="partTipeOrder.m112CutoffTime2.label" default="2" /></div>
    </th>


    <th style="border-bottom: none;padding: 5px; text-align: center">
        <div><g:message code="partTipeOrder.m112CutoffTime3.label" default="3" /></div>
    </th>


    <th style="border-bottom: none;padding: 5px; text-align: center">
        <div><g:message code="partTipeOrder.m112CutoffTime4.label" default="4" /></div>
    </th>


    <th style="border-bottom: none;padding: 5px; text-align: center">
        <div><g:message code="partTipeOrder.m112CutoffTime5.label" default="5" /></div>
    </th>


    <th style="border-bottom: none;padding: 5px; text-align: center">
        <div><g:message code="partTipeOrder.m112CutoffTime6.label" default="6" /></div>
    </th>


    <th style="border-bottom: none;padding: 5px; text-align: center">
        <div><g:message code="partTipeOrder.m112CutoffTime7.label" default="7" /></div>
    </th>


    <th style="border-bottom: none;padding: 5px; text-align: center">
        <div><g:message code="partTipeOrder.m112CutoffTime8.label" default="8" /></div>
    </th>


    %{--<th style="border-bottom: none;padding: 5px;">--}%
    %{--<div><g:message code="partTipeOrder.staDel.label" default="Sta Del" /></div>--}%
    %{--</th>--}%


    %{--<th style="border-bottom: none;padding: 5px;">--}%
    %{--<div><g:message code="partTipeOrder.m112ID.label" default="M112 ID" /></div>--}%
    %{--</th>--}%


    %{--<th style="border-bottom: none;padding: 5px;">--}%
    %{--<div><g:message code="partTipeOrder.m112StaCutOffTime.label" default="M112 Sta Cut Off Time" /></div>--}%
    %{--</th>--}%


    %{--<th style="border-bottom: none;padding: 5px;">--}%
    %{--<div><g:message code="partTipeOrder.createdBy.label" default="Created By" /></div>--}%
    %{--</th>--}%


    %{--<th style="border-bottom: none;padding: 5px;">--}%
    %{--<div><g:message code="partTipeOrder.updatedBy.label" default="Updated By" /></div>--}%
    %{--</th>--}%


    %{--<th style="border-bottom: none;padding: 5px;">--}%
    %{--<div><g:message code="partTipeOrder.lastUpdProcess.label" default="Last Upd Process" /></div>--}%
    %{--</th>--}%


</tr>
<tr>


    <th style="border-top: none;padding: 5px;">
        <div id="filter_m112TglBerlaku" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
            <input type="hidden" name="search_m112TglBerlaku" value="date.struct">
            <input type="hidden" name="search_m112TglBerlaku_day" id="search_m112TglBerlaku_day" value="">
            <input type="hidden" name="search_m112TglBerlaku_month" id="search_m112TglBerlaku_month" value="">
            <input type="hidden" name="search_m112TglBerlaku_year" id="search_m112TglBerlaku_year" value="">
            <input type="text" data-date-format="dd/mm/yyyy" name="search_m112TglBerlaku_dp" value="" id="search_m112TglBerlaku" class="search_init">
        </div>
    </th>

    <th style="border-top: none;padding: 5px;">
        <div id="filter_vendor" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
            <input type="text" name="search_vendor" class="search_init" />
        </div>
    </th>

    <th style="border-top: none;padding: 5px;">
        <div id="filter_m112TipeOrder" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">

            <select name="search_m112TipeOrder" id="search_m112TipeOrder" style="width:100%" onchange="reloadPartTipeOrderTable();">
                <option value="">All</option>
                <option value="Realtime/Emergency">Realtime/Emergency</option>
                <option value="Batch Routine/Reguler">Batch Routine/Reguler</option>
                <option value="Booking">Booking</option>
            </select>
        </div>
    </th>

    %{--<th style="border-top: none;padding: 5px;">--}%
    %{--<div id="filter_m112TipeOrder" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
    %{--<input type="text" name="search_m112TipeOrder" class="search_init" />--}%
    %{--</div>--}%
    %{--</th>--}%

    <th style="border-top: none;padding: 5px;">
        <div id="filter_m112JamLamaDatang" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
            <input maxlength="2" type="text" id="search_m112JamLamaDatang" name="search_m112JamLamaDatang" class="search_init" onkeypress="return isNumberKey(event)"/>
            %{--<input type="text" name="search_m112JamLamaDatang1" class="search_init" />--}%
        </div>
    </th>

    %{--<th style="border-top: none;padding: 5px;">--}%
    %{--<div id="filter_m112JamLamaDatang2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
    %{--<input type="text" name="search_m112JamLamaDatang2" class="search_init" />--}%
    %{--</div>--}%
    %{--</th>--}%

    <th style="border-top: none;padding: 5px;">
        <div id="filter_m112CutoffTime1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
            <input type="text" name="search_m112CutoffTime1" class="search_init" />
        </div>
    </th>

    <th style="border-top: none;padding: 5px;">
        <div id="filter_m112CutoffTime2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
            <input type="text" name="search_m112CutoffTime2" class="search_init" />
        </div>
    </th>

    <th style="border-top: none;padding: 5px;">
        <div id="filter_m112CutoffTime3" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
            <input type="text" name="search_m112CutoffTime3" class="search_init" />
        </div>
    </th>

    <th style="border-top: none;padding: 5px;">
        <div id="filter_m112CutoffTime4" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
            <input type="text" name="search_m112CutoffTime4" class="search_init" />
        </div>
    </th>

    <th style="border-top: none;padding: 5px;">
        <div id="filter_m112CutoffTime5" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
            <input type="text" name="search_m112CutoffTime5" class="search_init" />
        </div>
    </th>

    <th style="border-top: none;padding: 5px;">
        <div id="filter_m112CutoffTime6" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
            <input type="text" name="search_m112CutoffTime6" class="search_init" />
        </div>
    </th>

    <th style="border-top: none;padding: 5px;">
        <div id="filter_m112CutoffTime7" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
            <input type="text" name="search_m112CutoffTime7" class="search_init" />
        </div>
    </th>

    <th style="border-top: none;padding: 5px;">
        <div id="filter_m112CutoffTime8" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
            <input type="text" name="search_m112CutoffTime8" class="search_init" />
        </div>
    </th>

    %{--<th style="border-top: none;padding: 5px;">--}%
    %{--<div id="filter_staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
    %{--<input type="text" name="search_staDel" class="search_init" />--}%
    %{--</div>--}%
    %{--</th>--}%
    %{----}%
    %{--<th style="border-top: none;padding: 5px;">--}%
    %{--<div id="filter_m112ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
    %{--<input type="text" name="search_m112ID" class="search_init" />--}%
    %{--</div>--}%
    %{--</th>--}%
    %{----}%
    %{--<th style="border-top: none;padding: 5px;">--}%
    %{--<div id="filter_m112StaCutOffTime" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
    %{--<input type="text" name="search_m112StaCutOffTime" class="search_init" />--}%
    %{--</div>--}%
    %{--</th>--}%
    %{----}%
    %{--<th style="border-top: none;padding: 5px;">--}%
    %{--<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
    %{--<input type="text" name="search_createdBy" class="search_init" />--}%
    %{--</div>--}%
    %{--</th>--}%
    %{----}%
    %{--<th style="border-top: none;padding: 5px;">--}%
    %{--<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
    %{--<input type="text" name="search_updatedBy" class="search_init" />--}%
    %{--</div>--}%
    %{--</th>--}%
    %{----}%
    %{--<th style="border-top: none;padding: 5px;">--}%
    %{--<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
    %{--<input type="text" name="search_lastUpdProcess" class="search_init" />--}%
    %{--</div>--}%
    %{--</th>--}%


</tr>
</thead>
</table>

<g:javascript>
var partTipeOrderTable;
var reloadPartTipeOrderTable;
$(function(){
	
	reloadPartTipeOrderTable = function() {
		partTipeOrderTable.fnDraw();
	}

    var recordsPartTipeOrderPerPage = [];
    var anPartTipeOrderSelected;
    var jmlRecPartTipeOrderPerPage=0;
    var id;
	
	$('#search_m112TglBerlaku').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m112TglBerlaku_day').val(newDate.getDate());
			$('#search_m112TglBerlaku_month').val(newDate.getMonth()+1);
			$('#search_m112TglBerlaku_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			partTipeOrderTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	partTipeOrderTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	partTipeOrderTable = $('#partTipeOrder_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsPartTipeOrder = $("#partTipeOrder_datatables tbody .row-select");
            var jmlPartTipeOrderCek = 0;
            var nRow;
            var idRec;
            rsPartTipeOrder.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsPartTipeOrderPerPage[idRec]=="1"){
                    jmlPartTipeOrderCek = jmlPartTipeOrderCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsPartTipeOrderPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecPartTipeOrderPerPage = rsPartTipeOrder.length;
            if(jmlPartTipeOrderCek==jmlRecPartTipeOrderPerPage && jmlRecPartTipeOrderPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m112TglBerlaku",
	"mDataProp": "m112TglBerlaku",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "vendor",
	"mDataProp": "vendor",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m112TipeOrder",
	"mDataProp": "m112TipeOrder",
	"aTargets": [2],
	"mRender": function ( data, type, row ) {
            if(data=="Realtime/Emergency"){
            return "Realtime/Emergency";
            }
            if(data=="Batch Routine/Reguler"){
            return "Batch Routine/Reguler";
            }
            if(data=="Booking"){
            return "Booking";
            }
            },
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m112JamLamaDatang1",
	"mDataProp": "m112JamLamaDatang1",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
//
//{
//	"sName": "m112JamLamaDatang2",
//	"mDataProp": "m112JamLamaDatang2",
//	"aTargets": [4],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,

{
	"sName": "m112CutoffTime1",
	"mDataProp": "m112CutoffTime1",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m112CutoffTime2",
	"mDataProp": "m112CutoffTime2",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m112CutoffTime3",
	"mDataProp": "m112CutoffTime3",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m112CutoffTime4",
	"mDataProp": "m112CutoffTime4",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m112CutoffTime5",
	"mDataProp": "m112CutoffTime5",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m112CutoffTime6",
	"mDataProp": "m112CutoffTime6",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m112CutoffTime7",
	"mDataProp": "m112CutoffTime7",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m112CutoffTime8",
	"mDataProp": "m112CutoffTime8",
	"aTargets": [11],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
//
//,
//
//{
//	"sName": "staDel",
//	"mDataProp": "staDel",
//	"aTargets": [13],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "m112ID",
//	"mDataProp": "m112ID",
//	"aTargets": [14],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "m112StaCutOffTime",
//	"mDataProp": "m112StaCutOffTime",
//	"aTargets": [15],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "createdBy",
//	"mDataProp": "createdBy",
//	"aTargets": [16],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "updatedBy",
//	"mDataProp": "updatedBy",
//	"aTargets": [17],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "lastUpdProcess",
//	"mDataProp": "lastUpdProcess",
//	"aTargets": [18],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var m112TglBerlaku = $('#search_m112TglBerlaku').val();
						var m112TglBerlakuDay = $('#search_m112TglBerlaku_day').val();
						var m112TglBerlakuMonth = $('#search_m112TglBerlaku_month').val();
						var m112TglBerlakuYear = $('#search_m112TglBerlaku_year').val();
						
						if(m112TglBerlaku){
							aoData.push(
									{"name": 'sCriteria_m112TglBerlaku', "value": "date.struct"},
									{"name": 'sCriteria_m112TglBerlaku_dp', "value": m112TglBerlaku},
									{"name": 'sCriteria_m112TglBerlaku_day', "value": m112TglBerlakuDay},
									{"name": 'sCriteria_m112TglBerlaku_month', "value": m112TglBerlakuMonth},
									{"name": 'sCriteria_m112TglBerlaku_year', "value": m112TglBerlakuYear}
							);
						}
	
						var vendor = $('#filter_vendor input').val();
						if(vendor){
							aoData.push(
									{"name": 'sCriteria_vendor', "value": vendor}
							);
						}
	
						var m112TipeOrder = $('#search_m112TipeOrder').val();
						if(m112TipeOrder){
							aoData.push(
									{"name": 'sCriteria_m112TipeOrder', "value": m112TipeOrder}
							);
						}
	
						var m112JamLamaDatang = $('#filter_m112JamLamaDatang input').val();
						if(m112JamLamaDatang){//1 && m112JamLamaDatang2){
						aoData.push(
                            {"name": 'sCriteria_m112JamLamaDatang', "value": m112JamLamaDatang}
                        //,
                        //{"name": 'sCriteria_m112JamLamaDatang2', "value": m112JamLamaDatang2}
                        );
                        }
	
						var m112CutoffTime1 = $('#filter_m112CutoffTime1 input').val();
						if(m112CutoffTime1){
							aoData.push(
									{"name": 'sCriteria_m112CutoffTime1', "value": m112CutoffTime1}
							);
						}
	
						var m112CutoffTime2 = $('#filter_m112CutoffTime2 input').val();
						if(m112CutoffTime2){
							aoData.push(
									{"name": 'sCriteria_m112CutoffTime2', "value": m112CutoffTime2}
							);
						}
	
						var m112CutoffTime3 = $('#filter_m112CutoffTime3 input').val();
						if(m112CutoffTime3){
							aoData.push(
									{"name": 'sCriteria_m112CutoffTime3', "value": m112CutoffTime3}
							);
						}
	
						var m112CutoffTime4 = $('#filter_m112CutoffTime4 input').val();
						if(m112CutoffTime4){
							aoData.push(
									{"name": 'sCriteria_m112CutoffTime4', "value": m112CutoffTime4}
							);
						}
	
						var m112CutoffTime5 = $('#filter_m112CutoffTime5 input').val();
						if(m112CutoffTime5){
							aoData.push(
									{"name": 'sCriteria_m112CutoffTime5', "value": m112CutoffTime5}
							);
						}
	
						var m112CutoffTime6 = $('#filter_m112CutoffTime6 input').val();
						if(m112CutoffTime6){
							aoData.push(
									{"name": 'sCriteria_m112CutoffTime6', "value": m112CutoffTime6}
							);
						}
	
						var m112CutoffTime7 = $('#filter_m112CutoffTime7 input').val();
						if(m112CutoffTime7){
							aoData.push(
									{"name": 'sCriteria_m112CutoffTime7', "value": m112CutoffTime7}
							);
						}
	
						var m112CutoffTime8 = $('#filter_m112CutoffTime8 input').val();
						if(m112CutoffTime8){
							aoData.push(
									{"name": 'sCriteria_m112CutoffTime8', "value": m112CutoffTime8}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var m112ID = $('#filter_m112ID input').val();
						if(m112ID){
							aoData.push(
									{"name": 'sCriteria_m112ID', "value": m112ID}
							);
						}
	
						var m112StaCutOffTime = $('#filter_m112StaCutOffTime input').val();
						if(m112StaCutOffTime){
							aoData.push(
									{"name": 'sCriteria_m112StaCutOffTime', "value": m112StaCutOffTime}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
	$('.select-all').click(function(e) {

        $("#partTipeOrder_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsPartTipeOrderPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsPartTipeOrderPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#partTipeOrder_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsPartTipeOrderPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anPartTipeOrderSelected = partTipeOrderTable.$('tr.row_selected');
            if(jmlRecPartTipeOrderPerPage == anPartTipeOrderSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsPartTipeOrderPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
