<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<title>Formula Management</title>
<r:require modules="baseapplayout" />
<g:javascript>
   var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	var filterCustomCriteria;
	$(function(){ 
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/UserActivity/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/UserActivity/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#useractivity-form').empty();
    	$('#useractivity-form').append(data);
   	}
    
    shrinkTableLayout = function(){
   		if($("#useractivity-table").hasClass('span12')){
   			$("#useractivity-table").toggleClass('span12 span5');
   		}
        $("#useractivity-form").css("display","block");
       	shrinkUserActivityTable();
   	}
   	
   	expandTableLayout = function(){
   		if($("#useractivity-table").hasClass('span5')){
   			$("#useractivity-table").toggleClass('span5 span12');
   		}
        $("#useractivity-form").css("display","none");
       	expandUserActivityTable(); 
   	}
   	
   	filterCustomCriteria = function(){
    	var oTable = $('#useractivityDatatables_datatable').dataTable();    	
		
    	oTable.fnDraw();
    }
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#useractivity-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/UserActivity/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadUserActivityTable();
    		}
		});
		
   	}

});
</g:javascript>
</head>
<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"> User Access Log </span>
		<ul class="nav pull-right">
			
		</ul>
	</div>
	<div class="box" style="padding-left: 0;">
		<div id="useractivity-table" class="span12" >
				<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
			<div id="criteria-placeholder" class="container-fluid span5">
				<div class="navbar box-header no-border">
					<span class="pull-left">Criteria:</span>
				</div>
				<div class="box no-border non-collapsible">
					<div id="custom-criteria" class="control-group criteria">
						
						<div class="controls row-fluid"><span id="criteriaField0" class="criteriaField span3 row">User: </span><input type="text" id="criteriaUser" name="criteriaValue0" value="" class="criteriaValue span4 row"></div>
						<div class="controls row-fluid"><span id="criteriaField1" class="criteriaField span3 row">Menu: </span><input type="text" id="criteriaMenu" name="criteriaValue1" value="" class="criteriaValue span4 row"></div>
						<div class="controls row-fluid"><span id="criteriaField2" class="criteriaField span3 row">Controller: </span><input type="text" id="criteriaController" name="criteriaValue2" value="" class="criteriaValue span4 row"></div>
						<div class="controls row-fluid"><span id="criteriaField3" class="criteriaField span3 row">Action: </span><input type="text" id="criteriaAction" name="criteriaValue3" value="" class="criteriaValue span4 row"></div>
						<div class="controls row-fluid"><span id="criteriaField4" class="criteriaField span3 row">From: </span><input type="text" id="criteriaFrom" name="criteriaValue4" value="" class="criteriaValue span4 row" data-date-format="dd-mm-yyyy"></div>
						<div class="controls row-fluid"><span id="criteriaField5" class="criteriaField span3 row">To: </span><input type="text" id="criteriaTo" name="criteriaValue5" value="" class="criteriaValue span4 row" data-date-format="dd-mm-yyyy"></div>
						<g:javascript>
							$('#criteriaFrom').datepicker();
							$('#criteriaTo').datepicker();
						</g:javascript>
					</div>
					<div class="control-group">
						 <a class="btn create" href="javascript:void(0);" onclick="filterCustomCriteria();">Search</a>
					</div>
				</div>
			</div>
			<div id="useractivity-table-table" class="span11">
				<table id="useractivityDatatables_datatable" cellpadding="0"
					cellspacing="0" border="0"
					class="display table table-striped table-bordered table-hover"
					width="100%">
					<thead>
						<tr>
							<th>ID</th>
							<th>User</th>
							<th>Menu</th>
							<th>Controller</th>
							<th>Action</th>
							<th>Access Time</th>
						</tr>
					</thead>
					<g:render template="../menu/maxLineDisplay" />
					<g:javascript>
					var shrinkUserActivityTable;
                	var reloadUserActivityTable;
                	var expandUserActivityTable;
                	jQuery(function () {
                	 var oTable = $('#useractivityDatatables_datatable').dataTable({
                	    "bAutoWidth" : false,
                        "bPaginate" : true,
                        "bSort" : false,
                        "sInfo" : "",
                        "sInfoEmpty" : "",

                        "sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
                        bFilter: false,
                        "bStateSave": false,
                        'sPaginationType': 'bootstrap',
                        "fnInitComplete": function () {
                        },
                        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        },
                        "bSort": true,
                        "bProcessing": true,
                        "bServerSide": true,
                        "sServerMethod": "POST",
                        "iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
                       
                        "sAjaxSource": "${request.getContextPath()}/UserActivity/getList",
						
                        "aoColumns": [
                        
				            {   "sName": "pkid",
				            	"mDataProp": "pkid",
				            	"aTargets": [0],
				                "bSearchable": true,
				                "bSortable": true,                
				                "bVisible": false
				            } ,
				            {   "sName": "user_id",
				            	"mDataProp": "user_id",
				            	"aTargets": [1],
				                "bSearchable": true,
				                "bSortable": true,                
				                "bVisible": true,
								"sWidth":"20%"
				            } ,
				            {   "sName": "menu_id",
				            	"mDataProp": "menu_id",
				            	"aTargets": [2],
				                "bSearchable": true,
				                "bSortable": true,                
				                "bVisible": true,
				                "sWidth":"16%"
				            } ,
				             {  "sName": "controller",
				             	"mDataProp": "controller",
				            	"aTargets": [3],
				                "bSearchable": true,
				                "bSortable": true,                
				                "bVisible": true,
				                "sWidth":"16%"
				            } ,
				             {   "sName": "action",
				             	"mDataProp": "action",
				            	"aTargets": [4],
				                "bSearchable": true,
				                "bSortable": true,                
				                "bVisible": true,
				                "sWidth":"16%"
				            } ,
				             {   "sName": "access_time",
				             	"mDataProp": "access_time",
				            	"aTargets": [5],
				                "bSearchable": true,
				                "bSortable": true,                
				                "bVisible": true,
				                "sWidth":"16%"
				            } 
						
                        ],
                        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
								if($('#criteriaUser').val()){
									aoData.push({ "name": 'sCriteriaUser', "value": $('#criteriaUser').val() });
								}
								if($('#criteriaMenu').val()){
									aoData.push({ "name": 'sCriteriaMenu', "value": $('#criteriaMenu').val() });
								}
								if($('#criteriaController').val()){
									aoData.push({ "name": 'sCriteriaController', "value": $('#criteriaController').val() });
								}
								if($('#criteriaAction').val()){
									aoData.push({ "name": 'sCriteriaAction', "value": $('#criteriaAction').val() });
								}
								if($('#criteriaFrom').val()){
									aoData.push({ "name": 'sCriteriaFrom', "value": $('#criteriaFrom').val() });
								}
								if($('#criteriaTo').val()){
									aoData.push({ "name": 'sCriteriaTo', "value": $('#criteriaTo').val() });
								}
						        
						        $.ajax({ "dataType": 'json', 
						        	"type": "POST", 
						        	"url": sSource, 
						        	"data": aoData , 
						        	"success": function (json) {
						        		fnCallback(json);
						        	}
						        });
					       	}
                	 });
                	   
                	 //  new FixedColumns(oTable);   
                	 
                    shrinkUserActivityTable = function(){
                        var oTable = $('#useractivityDatatables_datatable').dataTable();
                        oTable.fnSetColumnVis( 2, false );
                        oTable.fnSetColumnVis( 3, false );
                        oTable.fnSetColumnVis( 4, false );
                    }

                    expandUserActivityTable = function(){
                        var oTable = $('#useractivityDatatables_datatable').dataTable();
                        oTable.fnSetColumnVis( 2, true );
                        oTable.fnSetColumnVis( 3, true );
                        oTable.fnSetColumnVis( 4, true );
                    }

                    reloadUserActivityTable = function(){
                        var oTable = $('#useractivityDatatables_datatable').dataTable();
                        oTable.fnReloadAjax();
                    }                   

                    $('#useractivityDatatables_datatable tbody tr a').live('click', function (e) {
                        e.stopPropagation();
                    } );  	
				    
				    });
                	
                	
                	</g:javascript>
				</table>
			</div>
		</div>
		<div class="span7" id="useractivity-form" style="display: none;"></div>
	</div>
</body>
</html>
