<%@ page import="com.kombos.baseapp.UserActivity" %>



<div class="control-group fieldcontain ${hasErrors(bean: userActivityInstance, field: 'accessTime', 'error')} ">
	<label class="control-label" for="accessTime">
		<g:message code="userActivity.accessTime.label" default="Access Time" />
		
	</label>
	<div class="controls">
	<g:datePicker name="accessTime" precision="day" value="${userActivityInstance?.accessTime}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userActivityInstance, field: 'action', 'error')} ">
	<label class="control-label" for="action">
		<g:message code="userActivity.action.label" default="Action" />
		
	</label>
	<div class="controls">
	<g:textField name="action" value="${userActivityInstance?.action}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userActivityInstance, field: 'controller', 'error')} ">
	<label class="control-label" for="controller">
		<g:message code="userActivity.controller.label" default="Controller" />
		
	</label>
	<div class="controls">
	<g:textField name="controller" value="${userActivityInstance?.controller}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userActivityInstance, field: 'menu', 'error')} ">
	<label class="control-label" for="menu">
		<g:message code="userActivity.menu.label" default="Menu" />
		
	</label>
	<div class="controls">
	<g:select id="menu" name="menu.id" from="${com.kombos.baseapp.menu.Menu.list()}" optionKey="id" required="" value="${userActivityInstance?.menu?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userActivityInstance, field: 'user', 'error')} ">
	<label class="control-label" for="user">
		<g:message code="userActivity.user.label" default="User" />
		
	</label>
	<div class="controls">
	<g:select id="user" name="user.id" from="${com.kombos.baseapp.sec.shiro.User.list()}" optionKey="id" required="" value="${userActivityInstance?.user?.id}" class="many-to-one"/>
	</div>
</div>

