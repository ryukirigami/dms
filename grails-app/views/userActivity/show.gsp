

<%@ page import="com.kombos.baseapp.UserActivity" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'userActivity.label', default: 'UserActivity')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteUserActivity;

$(function(){ 
	deleteUserActivity=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/userActivity/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadUserActivityTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-userActivity" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="userActivity"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${userActivityInstance?.accessTime}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="accessTime-label" class="property-label"><g:message
					code="userActivity.accessTime.label" default="Access Time" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="accessTime-label">
						%{--<ba:editableValue
								bean="${userActivityInstance}" field="accessTime"
								url="${request.contextPath}/UserActivity/updatefield" type="text"
								title="Enter accessTime" onsuccess="reloadUserActivityTable();" />--}%
							
								<g:formatDate date="${userActivityInstance?.accessTime}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${userActivityInstance?.action}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="action-label" class="property-label"><g:message
					code="userActivity.action.label" default="Action" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="action-label">
						%{--<ba:editableValue
								bean="${userActivityInstance}" field="action"
								url="${request.contextPath}/UserActivity/updatefield" type="text"
								title="Enter action" onsuccess="reloadUserActivityTable();" />--}%
							
								<g:fieldValue bean="${userActivityInstance}" field="action"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${userActivityInstance?.controller}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="controller-label" class="property-label"><g:message
					code="userActivity.controller.label" default="Controller" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="controller-label">
						%{--<ba:editableValue
								bean="${userActivityInstance}" field="controller"
								url="${request.contextPath}/UserActivity/updatefield" type="text"
								title="Enter controller" onsuccess="reloadUserActivityTable();" />--}%
							
								<g:fieldValue bean="${userActivityInstance}" field="controller"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${userActivityInstance?.menu}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="menu-label" class="property-label"><g:message
					code="userActivity.menu.label" default="Menu" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="menu-label">
						%{--<ba:editableValue
								bean="${userActivityInstance}" field="menu"
								url="${request.contextPath}/UserActivity/updatefield" type="text"
								title="Enter menu" onsuccess="reloadUserActivityTable();" />--}%
							
								<g:link controller="menu" action="show" id="${userActivityInstance?.menu?.id}">${userActivityInstance?.menu?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${userActivityInstance?.user}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="user-label" class="property-label"><g:message
					code="userActivity.user.label" default="User" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="user-label">
						%{--<ba:editableValue
								bean="${userActivityInstance}" field="user"
								url="${request.contextPath}/UserActivity/updatefield" type="text"
								title="Enter user" onsuccess="reloadUserActivityTable();" />--}%
							
								<g:link controller="user" action="show" id="${userActivityInstance?.user?.id}">${userActivityInstance?.user?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${userActivityInstance?.id}"
					update="[success:'userActivity-form',failure:'userActivity-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteUserActivity('${userActivityInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
