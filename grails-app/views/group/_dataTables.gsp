
<%@ page import="com.kombos.parts.Group" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="group_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="group.m181TglBerlaku.label" default="Tanggal Penetapan" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="group.m181ID.label" default="Kode Group" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="group.m181NamaGroup.label" default="Nama Group" /></div>
            </th>

		
		</tr>
		<tr>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_m181TglBerlaku" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
                    <input type="hidden" name="search_m181TglBerlaku" value="date.struct">
                    <input type="hidden" name="search_m181TglBerlaku_day" id="search_m181TglBerlaku_day" value="">
                    <input type="hidden" name="search_m181TglBerlaku_month" id="search_m181TglBerlaku_month" value="">
                    <input type="hidden" name="search_m181TglBerlaku_year" id="search_m181TglBerlaku_year" value="">
                    <input type="text" data-date-format="dd/mm/yyyy" name="search_m181TglBerlaku_dp" value="" id="search_m181TglBerlaku" class="search_init">
                </div>
            </th>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_m181ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_m181ID" class="search_init" />
                </div>
            </th>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_m181NamaGroup" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_m181NamaGroup" class="search_init" />
                </div>
            </th>

		</tr>
	</thead>
</table>

<g:javascript>
var groupTable;
var reloadGroupTable;
$(function(){
	
	reloadGroupTable = function() {
		groupTable.fnDraw();
	}

	var recordsGroupPerPage = [];
    var anGroupSelected;
    var jmlRecGroupPerPage=0;
    var id;
    
	$('#search_m181TglBerlaku').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m181TglBerlaku_day').val(newDate.getDate());
			$('#search_m181TglBerlaku_month').val(newDate.getMonth()+1);
			$('#search_m181TglBerlaku_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			groupTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	groupTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	groupTable = $('#group_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsGroup = $("#group_datatables tbody .row-select");
            var jmlGroupCek = 0;
            var nRow;
            var idRec;
            rsGroup.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsGroupPerPage[idRec]=="1"){
                    jmlGroupCek = jmlGroupCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsGroupPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecGroupPerPage = rsGroup.length;
            if(jmlGroupCek==jmlRecGroupPerPage && jmlRecGroupPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m181TglBerlaku",
	"mDataProp": "m181TglBerlaku",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m181ID",
	"mDataProp": "m181ID",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m181NamaGroup",
	"mDataProp": "m181NamaGroup",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m181TglBerlaku = $('#search_m181TglBerlaku').val();
						var m181TglBerlakuDay = $('#search_m181TglBerlaku_day').val();
						var m181TglBerlakuMonth = $('#search_m181TglBerlaku_month').val();
						var m181TglBerlakuYear = $('#search_m181TglBerlaku_year').val();

						if(m181TglBerlaku){
							aoData.push(
									{"name": 'sCriteria_m181TglBerlaku', "value": "date.struct"},
									{"name": 'sCriteria_m181TglBerlaku_dp', "value": m181TglBerlaku},
									{"name": 'sCriteria_m181TglBerlaku_day', "value": m181TglBerlakuDay},
									{"name": 'sCriteria_m181TglBerlaku_month', "value": m181TglBerlakuMonth},
									{"name": 'sCriteria_m181TglBerlaku_year', "value": m181TglBerlakuYear}
							);
						}

						var m181ID = $('#filter_m181ID input').val();
						if(m181ID){
							aoData.push(
									{"name": 'sCriteria_m181ID', "value": m181ID}
							);
						}
	
						var m181NamaGroup = $('#filter_m181NamaGroup input').val();
						if(m181NamaGroup){
							aoData.push(
									{"name": 'sCriteria_m181NamaGroup', "value": m181NamaGroup}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#group_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsGroupPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsGroupPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#group_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsGroupPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anGroupSelected = groupTable.$('tr.row_selected');

            if(jmlRecGroupPerPage == anGroupSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsGroupPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
