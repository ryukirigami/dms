

<%@ page import="com.kombos.parts.Group" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'group.label', default: 'Group')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteGroup;

$(function(){ 
	deleteGroup=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/group/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadGroupTable();
   				expandTableLayout('group');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-group" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="group"
			class="table table-bordered table-hover">
			<tbody>


            <g:if test="${groupInstance?.m181TglBerlaku}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m181TglBerlaku-label" class="property-label"><g:message
                                code="group.m181TglBerlaku.label" default="Tanggal Penetapan" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m181TglBerlaku-label">
                        %{--<ba:editableValue
                                bean="${groupInstance}" field="m181TglBerlaku"
                                url="${request.contextPath}/Group/updatefield" type="text"
                                title="Enter m181TglBerlaku" onsuccess="reloadGroupTable();" />--}%

                        <g:formatDate date="${groupInstance?.m181TglBerlaku}" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${groupInstance?.m181ID}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m181ID-label" class="property-label"><g:message
                                code="group.m181ID.label" default="Kode Group" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m181ID-label">
                        %{--<ba:editableValue
                                bean="${groupInstance}" field="m181ID"
                                url="${request.contextPath}/Group/updatefield" type="text"
                                title="Enter m181ID" onsuccess="reloadGroupTable();" />--}%

                        <g:fieldValue bean="${groupInstance}" field="m181ID"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${groupInstance?.m181NamaGroup}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m181NamaGroup-label" class="property-label"><g:message
                                code="group.m181NamaGroup.label" default="Nama Group" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m181NamaGroup-label">
                        %{--<ba:editableValue
                                bean="${groupInstance}" field="m181NamaGroup"
                                url="${request.contextPath}/Group/updatefield" type="text"
                                title="Enter m181NamaGroup" onsuccess="reloadGroupTable();" />--}%

                        <g:fieldValue bean="${groupInstance}" field="m181NamaGroup"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${groupInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="group.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${groupInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/Group/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadGroupTable();" />--}%

                        <g:fieldValue bean="${groupInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

				<g:if test="${groupInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="group.dateCreated.label" default="Date Created" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${groupInstance}" field="dateCreated"
								url="${request.contextPath}/Group/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadGroupTable();" />--}%

								<g:formatDate date="${groupInstance?.dateCreated}" type="datetime" style="MEDIUM"/>

						</span></td>

				</tr>
				</g:if>


            <g:if test="${groupInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="group.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${groupInstance}" field="createdBy"
                                url="${request.contextPath}/Group/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadGroupTable();" />--}%

                        <g:fieldValue bean="${groupInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${groupInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="group.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${groupInstance}" field="lastUpdated"
								url="${request.contextPath}/Group/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadGroupTable();" />--}%
							
								<g:formatDate date="${groupInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${groupInstance?.staDel}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="staDel-label" class="property-label"><g:message--}%
					%{--code="group.staDel.label" default="Sta Del" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="staDel-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${groupInstance}" field="staDel"--}%
								%{--url="${request.contextPath}/Group/updatefield" type="text"--}%
								%{--title="Enter staDel" onsuccess="reloadGroupTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${groupInstance}" field="staDel"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			
				<g:if test="${groupInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="group.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${groupInstance}" field="updatedBy"
								url="${request.contextPath}/Group/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadGroupTable();" />--}%
							
								<g:fieldValue bean="${groupInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('group');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${groupInstance?.id}"
					update="[success:'group-form',failure:'group-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteGroup('${groupInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
