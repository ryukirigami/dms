
<%@ page import="com.kombos.parts.Group" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'group.label', default: 'Group')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			var loadForm;
	        var shrinkTableLayout;
	        var expandTableLayout;
			$(function(){ 
			
				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('group');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('group', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('group', '${request.contextPath}/group/massdelete', reloadGroupTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('group','${request.contextPath}/group/show/'+id);
				};
				
				edit = function(id) {
					editInstance('group','${request.contextPath}/group/edit/'+id);
				};

				loadForm = function(data, textStatus){
		$('#group-form').empty();
    	$('#group-form').append(data);
   	}

    shrinkTableLayout = function(){
    	if($("#group-table").hasClass("span12")){
   			$("#group-table").toggleClass("span12 span5");
        }
        $("#group-form").css("display","block");
   	}

   	expandTableLayout = function(){
   		if($("#group-table").hasClass("span5")){
   			$("#group-table").toggleClass("span5 span12");
   		}
        $("#group-form").css("display","none");
   	}

});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="group-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
		</div>
		<div class="span7" id="group-form" style="display: none;"></div>
	</div>
</body>
</html>
