<%@ page import="com.kombos.parts.Group" %>


<div class="control-group fieldcontain ${hasErrors(bean: groupInstance, field: 'm181TglBerlaku', 'error')} ">
    <label class="control-label" for="m181TglBerlaku">
        <g:message code="group.m181TglBerlaku.label" default="Tanggal Penetapan" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <ba:datePicker name="m181TglBerlaku" precision="day" value="${groupInstance?.m181TglBerlaku}" format="dd-MM-yyyy" required="true"/>
    </div>
</div>

<g:if test="${groupInstance?.m181ID}">
<div class="control-group fieldcontain ${hasErrors(bean: groupInstance, field: 'm181ID', 'error')} ">
	<label class="control-label" for="m181ID">
		<g:message code="group.m181ID.label" default="Kode Group" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
	${groupInstance?.m181ID}
	</div>
</div>
</g:if>

<div class="control-group fieldcontain ${hasErrors(bean: groupInstance, field: 'm181NamaGroup', 'error')} ">
	<label class="control-label" for="m181NamaGroup">
		<g:message code="group.m181NamaGroup.label" default="Nama Group" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m181NamaGroup" maxlength="20" value="${groupInstance?.m181NamaGroup}" required="" />
	</div>
</div>

<g:javascript>
    document.getElementById("m181TglBerlaku").focus();
</g:javascript>


