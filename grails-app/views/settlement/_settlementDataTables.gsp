<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="settlement_datatables_${idTable}" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Tgl Bayar</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">No. Invoice</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Tipe</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Nomor WO</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 125px;">Customer</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Metode Bayar</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">No. WBS</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Nama Bank</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">No. Kartu</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Bunga</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Jumlah</div>
        </th>
    </tr>
    </thead>
</table>
<g:javascript>
var settlementTable;
var settlementKuitansiTable;

function searchData() {
    reloadSettlementTable();
}

$(function() {
	reloadSettlementTable = function() {
		settlementTable.fnDraw();
	}

	settlementTable = $('#settlement_datatables_${idTable}').dataTable({
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		},
	    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            return nRow;
	    },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "settlementDataTablesList")}",
		"aoColumns": [
			//tanggalBayar
			{
				"sName": "tanggalBayar",
				"mDataProp": "tanggalBayar",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			//noInvoice
			{
				"sName": "noInvoice",
				"mDataProp": "noInvoice",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			//tipe
			{
				"sName": "tipe",
				"mDataProp": "tipe",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			//nomorWO
			{
				"sName": "nomorWO",
				"mDataProp": "nomorWO",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			//customer
			{
				"sName": "customer",
				"mDataProp": "customer",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//metodeBayar
			{
				"sName": "metodeBayar",
				"mDataProp": "metodeBayar",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			//noWBS
			{
				"sName": "noWBS",
				"mDataProp": "noWBS",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			//namaBank
			{
				"sName": "namaBank",
				"mDataProp": "namaBank",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			//noKartu
			{
				"sName": "noKartu",
				"mDataProp": "noKartu",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			//bunga
			{
				"sName": "bunga",
				"mDataProp": "bunga",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			//jumlah
			{
				"sName": "jumlah",
				"mDataProp": "jumlah",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			}
		],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                aoData.push(
                        {"name": 'idInv', "value": $("#idInvoice").val()}

                );

                $.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>



