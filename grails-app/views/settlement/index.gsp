<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'delivery.settlement.label', default: 'Delivery - Settlement')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout,autoNumeric" />
    <g:javascript>
        var searchData;
        var saveDataSettlement;
        var metode=-1;
        var keyword=-1;
        var jumBayar = -1;
        $(function () {
            var msg = '';

            $(".numeric").autoNumeric("init", {
                vMin: '-999999999.99',
                vMax: '99999999999.99'
            });

            saveDataSettlement = function(){
                var aoData = []
                var param1 = $('#outNomorWO').val();
                var outJenisInvoice = $('#outJenisInvoice').val();
                var param2 = $('#totalUang').autoNumeric("get");
                var param3 = $('#nomorWBS').val();
                var param4 = $('#namaBank').val();
                var param5 = $('#mesinEDC').val();
                var param6 = $('#nomorKartu').val();
                var param7 = $('#noBuktiTransfer').val();
                var param8 = $('#outPph23').val();
                var param9 = $('#outPpn').val();
                var param10 = $('#cppn').val();
                var param11 = $('#cpph23').val();
                var param12 = $('#metodeBayarSelect').val();
                var jumKembali = $('#kembalikan').autoNumeric("get");

                aoData.push(
                        {"name": 'nowo', "value": param1},
                        {"name": 'uang', "value": param2},
                        {"name": 'wbs', "value": param3},
                        {"name": 'bank', "value": param4},
                        {"name": 'mesin', "value": param5},
                        {"name": 'nokartu', "value": param6},
                        {"name": 'buktitrf', "value": param7},
                        {"name": 'pph', "value": param8},
                        {"name": 'ppn', "value": param9},
                        {"name": 'stapph', "value": param10},
                        {"name": 'stappn', "value": param11},
                        {"name": 'metodebayar', "value": param12},
                        {"name": 'idCari', "value": $("#idCari").val()},
                        {"name": 'metodeCari', "value": $("#metodeCari").val()},
                        {"name": 'jumKembali', "value": jumKembali}
                );
                var find = ' ';
                var re = new RegExp(find, 'g');

                if(param1=="" || param1==null || param2.replace(re,"")=="" || param2=="" || param2==null){
                    alert('Data belum lengkap')
                    return
                }
                if(outJenisInvoice=="K"){
                    alert("Ini merupakan Invoice KREDIT, Harap Melakukan Pembayaran di Menu Kasir");
                    return
                }
                var wajibBayar = $('#totalBayar').autoNumeric("get");
                if(parseFloat(wajibBayar)<=0){
                    alert('Pembayaran sudah lunas')
                    return
                }
                var isFull = true;
                var sTambahan = '';
                if(param12==2 && (param4==0 || !param7)){
                    if(param4==0){sTambahan = sTambahan+'-Bank\n';}
                    if(!param7){sTambahan = sTambahan+'-No. Bukti Trf-\n';}
                    isFull = false;
                }

                if((param12==3 || param12==5) && (param4==0 || param5==0|| !param6)){
                    if(param4==0){sTambahan = sTambahan+'-Bank\n';}
                    if(param5==0){sTambahan = sTambahan+'-Mesin EDC\n';}
                    if(!param6){sTambahan = sTambahan+'-Nomor Kartu\n';}
                    isFull = false;
                }

                if(!isFull){
                    alert('Harap Lengkapi Data Berikut :\n'+sTambahan);
                    return
                }
                var c = confirm("Apakah Anda Yakin?");
                if(c){
                    $.ajax({
                        url:'${request.contextPath}/settlement/save',
                        type: "POST",
                        data : aoData,
                        success : function(data){
                            if(data == "ok"){
                                clearSettlementData();
                                toastr.success('Data berhasil disimpan');
                                reloadSettlementTable();
                                jumlahSudahBayar();
                            }else{
                                console.log(data)
                                toastr.info('Data settlement sudah ada!!!');
                            }

                        },
                        error: function() {
                            alert('Internal Server Error');
                        }
                    });
                }
            }

            jumlahSudahBayar = function(){
                var idInv = $("#idInvoice").val();
                $.ajax({
                    url:'${request.contextPath}/settlement/jumlahSudahBayar?idInv='+idInv,
                    type: "POST",
                        success : function(data){
                        $('#totalPembayaran').val(data);
                        calc();
                        if(parseFloat($('#totalBayar').autoNumeric("get")) > 0){
                            document.getElementById("statusSettlement").style.color = "red";
                            $('#statusSettlement').val('KURANG BAYAR');
                        }else{
                            document.getElementById("statusSettlement").style.color = "blue";
                            $('#gate').attr('disabled',false);
                            $('#statusSettlement').val('LUNAS');
                            $("#printKW").show();
                        }
                        if(parseFloat($('#outTotalInvoice').val()) < parseFloat($('#outBookingFee').val())){
                            $('#refund').attr("disabled", false);
                            $('#outRefundBookingFee').val(parseFloat($('#outBookingFee').val()) - parseFloat($('#outTotalInvoice').val()));
                        }
                    },
                    error: function() {
                        alert('Internal Server Error');
                    }
                });
            }
            //fungsi Enter
            $("#nomorInvoice").bind('keypress', function(e) {
                var code = (e.keyCode ? e.keyCode : e.which);
                    if(code == 13) {
                            searchData();
                    }
                });

            searchData = function(){
                $("#printKW").hide();
                msg = '';
                metode=-1;
                keyword=-1;
                var settleBy = $('input:radio[name=settleBy]:checked').val();
                if(!settleBy){msg += '- Silahkan pilih Metode Settlement \n'}
                var invoice = $('input[name=nomorInvoice]').val();
                if(!invoice){msg += '- Silahkan input Nomor Invoice \n'}
                if(msg != ''){
                    alert(msg);
                    reloadSettlementTable();
                    return false;
                }else{
                    metode=settleBy;
                    keyword=invoice;
                    doSearch(settleBy,invoice);
                }
            };

            doSearch = function(a,b){
                $.ajax({
                    url:'${request.contextPath}/settlement/searchData',
                    type: "POST",
                    data : { metodeSettlement: a, nomorInvoice: b },
                    success : function(data){
                        clearAllData();
                        clearSettlementData();
                        if(data.status=='ok'){
                            setAllData(data.detail);
                            if(data.detail.outJenisInvoice=="K"){
                                alert("Ini merupakan Invoice KREDIT, Harap Melakukan Pembayaran di Menu Kasir");
                            }
                            $('select[name=metodeBayarSelect]').prop("disabled",false);
                            reloadSettlementTable();
                            cekPKS();
                        }else{
                            clearSettlementData();
                            $('input[name=metodeBayar]').val(0);
                            $('#metodeBayarSelect option:first-child').attr("selected", "selected");
                            $('select[name=metodeBayarSelect]').prop("disabled",true);
                            disableMetodePayment();
                            cekPKS();
                            alert(data.message);
                            return false;
                        }
                    },
                    error: function() {
                        clearSettlementData();
                        $('input[name=metodeBayar]').val(0);
                        $('#metodeBayarSelect option:first-child').attr("selected", "selected");
                        $('select[name=metodeBayarSelect]').prop("disabled",true);
                        disableMetodePayment();
                        cekPKS();
                        alert('Internal Server Error');
                        return false;
                    }
                });
            };

            clearAllData = function(){
                $('input[name=outNomorWO]').val(null);
                $('input[name=outJenisService]').val(null);
                $('input[name=outNamaCustomer]').val(null);
                $('input[name=outNomorPolisi]').val(null);
                $('input[name=outModel]').val(null);
                $('input[name=outCustomerPKS]').val(null);
                $('input[name=outJenisInvoice]').val(null);
                $('input[name=outJumlahOR]').val(null);
                $('input[name=outJasaLabor]').val(null);
                $('input[name=outSublet]').val(null);
                $('input[name=outParts]').val(null);
                $('input[name=outPpnTax]').val(null);
                $('input[name=outMaterai]').val(null);
                $('input[name=outTotalInvoice]').val(null);
//                $('input[name=outKasir]').val(null);
                $('input[name=outNoInv]').val(null);
                $('input[name=outDivisi]').val(null);
                $('input[name=outBookingFee]').val(null);
                $('input[name=outRefundBookingFee]').val(null);
                $('input[name=outDiscountGoodwill]').val(null);
                $('input[name=outPph23]').val(null);
                $('input[name=outPpn]').val(null);
                $('input[name=totalPembayaran]').val(null);
                $('input[name=totalUang]').val(null);
                $('input[name=kembalikan]').val(null);
                $('#nomorWBS').val();
            };

            clearSettlementData = function(){
                $('input[name=nomorWBS]').val(null);
                $('input[name=namaBank]').val(0);
                $('#namaBankSelect option:first-child').attr("selected", "selected");
                $('input[name=mesinEDC]').val(0);
                $('input[name=totalUang]').val(0);
                $('input[name=kembalikan]').val(0);
                $('#mesinEDCSelect option:first-child').attr("selected", "selected");
                $('input[name=nomorKartu]').val(null);
                $('input[name=noBuktiTransfer]').val(null);
            };

            setAllData = function(data){
                $('input[name=outNomorWO]').val(data.outNomorWO);
                $('input[name=outJenisService]').val(data.outJenisService);
                $('input[name=outNamaCustomer]').val(data.outNamaCustomer);
                $('input[name=outNomorPolisi]').val(data.outNomorPolisi);
                $('input[name=outModel]').val(data.outModel);
                $('input[name=outCustomerPKS]').val(data.outCustomerPKS);
                $('input[name=outJenisInvoice]').val(data.outJenisInvoice);
                $('input[name=outJumlahOR]').val(data.outJumlahOR);
                $('input[name=outJasaLabor]').val(data.outJasaLabor);
                $('input[name=outSublet]').val(data.outSublet);
                $('input[name=outParts]').val(data.outParts);
                $('input[name=outPpnTax]').val(data.outPpnTax);
                $('input[name=outMaterai]').val(data.outMaterai);
                $('input[name=outTotalInvoice]').val(data.outTotalInvoice);
//                $('input[name=outKasir]').val(data.outKasir);
                $('input[name=outDivisi]').val(data.outDivisi);
                $('input[name=outBookingFee]').val(data.outBookingFee);
                $('input[name=outRefundBookingFee]').val(data.outRefundBF);
                $('input[name=outDiscountGoodwill]').val(data.outDiscountGW);
                $('input[name=outPph23]').val(data.outPph23);
                $('input[name=outPpn]').val(data.outPpn);
                $('input[name=outNoInv]').val(data.outNoInvoice);
                $('input[name=totalPembayaran]').val(data.outTotal);
                $('#idCari').val(data.idCari);
                $('#idInvoice').val(data.idInvoice);
                $('#metodeCari').val(data.metodeCari);
                jumlahSudahBayar();
            };

            disableMetodePayment = function(){
                $('input[name=nomorWBS]').prop("readonly",true);
                $('select[name=namaBankSelect]').prop("disabled",true);
                $('select[name=mesinEDCSelect]').prop("disabled",true);
                $('input[name=nomorKartu]').prop("readonly",true);
                $('input[name=noBuktiTransfer]').prop("readonly",true);
            };

            cekPKS = function(){
                var isPKS = $('input[name=outCustomerPKS]').val();
                if(isPKS == "Ya"){
                    $("#pph23").attr("disabled", false);
                    $("#ppn").attr("disabled", false);
                }else{
                    $("#pph23").attr("disabled", true);
                    $("#ppn").attr("disabled", true);
                }
            };

            calc = function(){
                var find = ',';
                var re = new RegExp(find, 'g');
                var a = $('input[name=outTotalInvoice]').val();
                var x = a.indexOf(",")>-1 ? a.replace(re,"") : a
                a = x;
                var b = $('input[name=outBookingFee]').val();
                var c = $('#pph23').val() ? $('#pph23').val() : 0;
                var d = $('#ppn').val() ? $('#ppn').val() : 0;
                var e = $('input[name=totalPembayaran]').val();
                var z = e.indexOf(",")>-1 ? e.replace(re,"") : e
                e = z;
                var f = new Number(parseFloat(a) - (parseFloat(b) + parseFloat(c) + parseFloat(d) + parseFloat(e)));
                $('#totalBayar').autoNumeric("set", f);
            };

            changeMetodeBayar = function() {
                var metodeBayarSelect = $('select[name=metodeBayarSelect] option:selected').val();
                $('input[name=metodeBayar]').val(metodeBayarSelect);
                if(metodeBayarSelect==1){ //Cash
                    clearSettlementData();
                    $('input[name=nomorWBS]').prop("readonly",true);
                    $('select[name=namaBankSelect]').prop("disabled",true);
                    $('select[name=mesinEDCSelect]').prop("disabled",true);
                    $('input[name=nomorKartu]').prop("readonly",true);
                    $('input[name=noBuktiTransfer]').prop("readonly",true);
                    cekPKS();
                }else if(metodeBayarSelect==2){ //Transfer
                    clearSettlementData();
                    $('input[name=nomorWBS]').prop("readonly",true);
                    $('select[name=namaBankSelect]').prop("disabled",false);
                    $('select[name=mesinEDCSelect]').prop("disabled",true);
                    $('input[name=nomorKartu]').prop("readonly",true);
                    $('input[name=noBuktiTransfer]').prop("readonly",false);
                    cekPKS();
                }else if(metodeBayarSelect==3){ //DEBIT
                    clearSettlementData();
                    $('input[name=nomorWBS]').prop("readonly",true);
                    $('select[name=namaBankSelect]').prop("disabled",false);
                    $('select[name=mesinEDCSelect]').prop("disabled",false);
                    $('input[name=nomorKartu]').prop("readonly",false);
                    $('input[name=noBuktiTransfer]').prop("readonly",true);
                    cekPKS();
                }else if(metodeBayarSelect==4){ //WBS
                    clearSettlementData();
                    $('input[name=nomorWBS]').prop("readonly",false);
                    $('select[name=namaBankSelect]').prop("disabled",true);
                    $('select[name=mesinEDCSelect]').prop("disabled",true);
                    $('input[name=nomorKartu]').prop("readonly",true);
                    $('input[name=noBuktiTransfer]').prop("readonly",true);
                    cekPKS();
                }else if(metodeBayarSelect==5){ //Credit Card
                    clearSettlementData();
                    $('input[name=nomorWBS]').prop("readonly",true);
                    $('select[name=namaBankSelect]').prop("disabled",false);
                    $('select[name=mesinEDCSelect]').prop("disabled",false);
                    $('input[name=nomorKartu]').prop("readonly",false);
                    $('input[name=noBuktiTransfer]').prop("readonly",true);
                    cekPKS();
                }else if(metodeBayarSelect==6){ //Third Party
                    clearSettlementData();
                    $('input[name=nomorWBS]').prop("readonly",true);
                    $('select[name=namaBankSelect]').prop("disabled",true);
                    $('select[name=mesinEDCSelect]').prop("disabled",true);
                    $('input[name=nomorKartu]').prop("readonly",true);
                    $('input[name=noBuktiTransfer]').prop("readonly",true);
                    var tobar = new Number($("#totalBayar").autoNumeric("get"));
                    $('#totalUang').autoNumeric("set", tobar);
                    cekPKS();
                }else{
                    clearSettlementData();
                    $('input[name=nomorWBS]').prop("readonly",true);
                    $('select[name=namaBankSelect]').prop("disabled",true);
                    $('select[name=mesinEDCSelect]').prop("disabled",true);
                    $('input[name=nomorKartu]').prop("readonly",true);
                    $('input[name=noBuktiTransfer]').prop("readonly",true);
                    cekPKS();
                }
            };

            changeBank = function() {
                var namaBankSelect = $('select[name=namaBankSelect] option:selected').val();
                var mesinEDCSelect = $('select[name=mesinEDCSelect] option:selected').val();
                $('input[name=namaBank]').val(namaBankSelect);
                if(namaBankSelect!="0" && mesinEDCSelect!="0"){
                    getRate(namaBankSelect, mesinEDCSelect);
                }else{
                    $('input[name=outBankCharge]').val(0);
                }
            };

            changeMesinEDC = function() {
                var mesinEDCSelect = $('select[name=mesinEDCSelect] option:selected').val();
                var namaBankSelect = $('select[name=namaBankSelect] option:selected').val();
                $('input[name=mesinEDC]').val(mesinEDCSelect);
                if(mesinEDCSelect!="0" && namaBankSelect!="0"){
                    getRate(namaBankSelect, mesinEDCSelect);
                }else{
                    $('input[name=outBankCharge]').val(0);
                }
            };

            getRate = function(bank,edc) {
                $.ajax({
                    url: '${request.contextPath}/settlement/getRate',
                    type: 'POST',
                    data: { namaBank: bank, mesinEDC: edc },
                    success : function(data){
                        $('input[name=bankCharge]').val(data.m703RatePersen);
                    },
                    error: function() {
                        alert('Can not gate rate');
                        $('input[name=bankCharge]').val(0);
                    }
                });
            };

            calculator = function(){
                var totalBayar = new Number($('#totalBayar').autoNumeric("get"));
                if(!totalBayar)totalBayar=new Number(0);
                var totalUang = new Number($('#totalUang').autoNumeric("get"));
                if(!totalUang)totalUang=new Number(0);
                if(totalUang > totalBayar){
                    var kembalikan = new Number(totalUang - totalBayar);
                    $('#kembalikan').autoNumeric("set",kembalikan)
                }else{
                    $('#kembalikan').autoNumeric("set",new Number(0));
                    $('#totalUang').autoNumeric("set",totalBayar);
                }
            };

        });

        function hitungKembalian(){
            var bayar = $('#totalUang').autoNumeric("get");
            var wajibbayar = $('#totalBayar').autoNumeric("get");
            if(parseFloat(bayar)>parseFloat(wajibbayar)){
                $('#kembalikan').autoNumeric("set",new Number(parseFloat(bayar)-parseFloat(wajibbayar)));
            }else{
                $('#kembalikan').val('');
            }
        }

        function doPrint(){
            var id = $("#idInvoice").val();
            window.location = "${request.contextPath}/settlement/printKW?id="+id;
        }
    </g:javascript>
</head>

<body>
    <div class="navbar box-header no-border">
        <span class="pull-left">${message(code: 'delivery.settlement.label', default: 'Delivery - Settlement')}</span>
    </div>
    <div>&nbsp;</div>
    <div class="box" style="margin-bottom: 0; padding-bottom: 0;">
        <table width="100%" style="border-spacing: 5px; border-collapse: separate;">
            <tr valign="top">
                <td width="30%">
                    <span style="text-decoration: underline;"><g:message code="delivery.settlement.left.label" default="Data Invoice"/></span>
                    <div class="box" style="margin-bottom: 0; padding-bottom: 0;">
                        <legend style="font-size: small;">
                            <table width="100%" style="border-spacing: 1px; border-collapse: separate; margin-bottom: 5px;">
                                <tr>
                                    <td width="35%">Settle By No</td>
                                    <td width="60%">
                                        <g:radioGroup name="settleBy"
                                                      labels="['WO','Invoice']"
                                                      values="[1,2]">
                                            ${it.radio} ${it.label}&nbsp;&nbsp;&nbsp;
                                        </g:radioGroup>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="30%">Nomor WO/Invoice</td>
                                    <td width="70%">
                                        <g:textField name="nomorInvoice" id="nomorInvoice" style="width:95%"/>
                                        <g:hiddenField name="idCari" id="idCari" />
                                        <g:hiddenField name="idInvoice" id="idInvoice" />
                                        <g:hiddenField name="metodeCari" id="metodeCari" />
                                        <g:hiddenField name="outJenisInvoice" id="outJenisInvoice" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="right">
                                        <g:field type="button" style="width: 90px" class="btn btn-primary search" onclick="searchData();"
                                                 name="search" id="search" value="${message(code: 'default.ok.label', default: 'OK')}" />
                                    </td>
                                </tr>
                            </table>
                        </legend>
                        <table width="100%" border="1" style="margin-bottom: 10px;">
                            <tr style="height: 30px;">
                                <td width="35%" style="padding-left: 5px;">Nomor WO</td>
                                <td width="65%" align="center">
                                    <g:textField name="outNomorWO" id="outNomorWO" style="margin: 1px; width: 90%" readonly=""/>
                                </td>
                            </tr>
                            <tr style="height: 30px;">
                                <td width="35%" style="padding-left: 5px;">Nomor Invoice</td>
                                <td width="65%" align="center">
                                    <g:textField name="outNoInv" id="outNoInv" style="margin: 1px; width: 90%" readonly=""/>
                                </td>
                            </tr>
                            <tr style="height: 30px;">
                                <td width="35%" style="padding-left: 5px;">Jenis Service</td>
                                <td width="65%" align="center">
                                    <g:textField name="outJenisService" id="outJenisService" style="margin: 1px; width: 90%" readonly=""/>
                                </td>
                            </tr>
                            <tr style="height: 30px;">
                                <td width="35%" style="padding-left: 5px;">Nama Customer</td>
                                <td width="65%" align="center">
                                    <g:textField name="outNamaCustomer" id="outNamaCustomer" style="margin: 1px; width: 90%" readonly=""/>
                                </td>
                            </tr>
                            <tr style="height: 30px;">
                                <td width="35%" style="padding-left: 5px;">Nomor Polisi</td>
                                <td width="65%" align="center">
                                    <g:textField name="outNomorPolisi" id="outNomorPolisi" style="margin: 1px; width: 90%" readonly=""/>
                                </td>
                            </tr>
                            <tr style="height: 30px;">
                                <td width="35%" style="padding-left: 5px;">Model</td>
                                <td width="65%" align="center">
                                    <g:textField name="outModel" id="outModel" style="margin: 1px; width: 90%" readonly=""/>
                                </td>
                            </tr>
                            <tr style="height: 30px;">
                                <td width="35%" style="padding-left: 5px;">Customer PKS ?</td>
                                <td width="65%" align="center">
                                    <g:textField name="outCustomerPKS" id="outCustomerPKS" style="margin: 1px; width: 90%" readonly=""/>
                                </td>
                            </tr>
                            <tr style="height: 30px;display: none" id="jmlBayarPKS">
                                <td width="35%" style="padding-left: 5px;">Saldo PKS</td>
                                <td width="65%" align="center">
                                    <g:checkBox name="staCekPKS" id="staCekPKS" />
                                    <g:textField name="saldoPKS" id="saldoPKS" style="margin: 1px; width: 81%" readonly=""/>
                                </td>
                            </tr>
                            <tr style="height: 30px;">
                                <td width="35%" style="padding-left: 5px;">Total Invoice (A)</td>
                                <td width="65%" align="center">
                                    <g:textField name="outTotalInvoice" id="outTotalInvoice" style="margin: 1px; width: 90%" readonly=""/>
                                </td>
                            </tr>
                            %{--<tr style="height: 30px;border-bottom: 1">--}%
                                %{--<td width="35%" style="padding-left: 5px;">Kasir</td>--}%
                                %{--<td width="65%" align="center">--}%
                                    %{--<g:textField name="outKasir" id="outKasir" style="margin: 1px; width: 90%" readonly=""/>--}%
                                %{--</td>--}%
                            %{--</tr>--}%
                        </table>
                    </div>
                </td>
                <td width="70%">
                    <span style="text-decoration: underline;"><g:message code="delivery.settlement.right.top.label" default="Invoice Settlement"/></span>
                    <div class="box" style="padding-top: 0px; padding-left: 15px; padding-bottom: 0px;">
                        <table width="100%" style="border-spacing: 5px; border-collapse: separate;">
                            <tr valign="top">
                                <td width="50%">
                                    <table width="100%" border="1" style="margin-bottom: 10px;">
                                        <tr style="height: 30px;">
                                            <td width="40%" style="padding-left: 5px;">Metode Bayar</td>
                                            <td width="60%" valign="midle" align="center">
                                                <g:select style="margin: 1px;" name="metodeBayarSelect" id="metodeBayarSelect" onchange="changeMetodeBayar();" from="${metodeBayar}" optionKey="id" optionValue="m701MetodeBayar" disabled=""/>
                                                <input type="hidden" id="metodeBayar" name="metodeBayar" value="0">
                                            </td>
                                        </tr>
                                        <tr style="height: 30px;">
                                            <td width="40%" style="padding-left: 5px;">Nomor WBS</td>
                                            <td width="60%" align="center">
                                                <g:textField name="nomorWBS" id="nomorWBS" style="margin: 1px;" readonly=""/>
                                            </td>
                                        </tr>
                                        <tr style="height: 30px;">
                                            <td width="40%" style="padding-left: 5px;">Nama Bank</td>
                                            <td width="60%" align="center">
                                                <g:hiddenField name="namaBank" id="namaBank" value="0"/>
                                                <g:select style="margin: 1px;" name="namaBankSelect" id="namaBankSelect" onchange="changeBank();" from="${com.kombos.finance.BankAccountNumber.createCriteria().list {eq("staDel","0");eq("companyDealer",session?.userCompanyDealer);bank{order("m702NamaBank")}}}" optionKey="id" optionValue="${{it.bank.m702NamaBank + '-' + it.bank.m702Cabang}}" noSelection="${['':'Silahkan Pilih']}" disabled=""/>
                                            </td>
                                        </tr>
                                        <tr style="height: 30px;">
                                            <td width="40%" style="padding-left: 5px;">Mesin EDC</td>
                                            <td width="60%" align="center">
                                                <input type="hidden" id="mesinEDC" name="mesinEDC" value="0">
                                                <g:select style="margin: 1px;" name="mesinEDCSelect" id="mesinEDCSelect" onchange="changeMesinEDC();" from="${mesinEDC}" optionKey="id" optionValue="m704NamaMesinEdc" disabled="" noSelection="${['0':'Please Select']}"/>
                                            </td>
                                        </tr>
                                        <tr style="height: 30px;">
                                            <td width="40%" style="padding-left: 5px;">Nomor Kartu</td>
                                            <td width="60%" align="center">
                                                <g:textField name="nomorKartu" id="nomorKartu" style="margin: 1px;" readonly=""/>
                                            </td>
                                        </tr>
                                        <tr style="height: 30px;">
                                            <td width="40%" style="padding-left: 5px;">No Bukti Transfer</td>
                                            <td width="60%" align="center">
                                                <g:textField name="noBuktiTransfer" id="noBuktiTransfer" style="margin: 1px;" readonly=""/>
                                            </td>
                                        </tr>
                                        <tr style="height: 30px;">
                                            <td width="40%" style="padding-left: 5px;">Bank Charge</td>
                                            <td width="60%" align="center">
                                                <g:textField name="bankCharge" id="bankCharge" style="margin: 1px; width: 50px;" readonly="" value="0"/>%
                                            </td>
                                        </tr>
                                        <tr style="height: 30px;">
                                            <td width="40%" style="padding-left: 5px;">Booking Fee (B)</td>
                                            <td width="60%" align="center">
                                                <g:textField name="outBookingFee" id="outBookingFee" style="margin: 1px;" readonly=""/>
                                            </td>
                                        </tr>
                                        <tr style="height: 30px;">
                                            <td width="40%" style="padding-left: 5px;">
                                                <g:checkBox name="pph23" id="cpph23" value="" style="margin: 1px;" disabled=""/>&nbsp;Pph 23 (C)
                                                <input type="hidden" id="outPph23" name="outPph23" value="0">
                                            </td>
                                            <td width="60%" align="center">
                                                <g:textField name="pph23" id="pph23" style="margin: 1px;" readonly="" value="0"/>
                                            </td>
                                        </tr>
                                        <tr style="height: 30px;">
                                            <td width="40%" style="padding-left: 5px;">
                                                <g:checkBox name="ppn" id="cppn" value="" style="margin: 1px;" disabled=""/>&nbsp;PPN (D)
                                                <input type="hidden" id="outPpn" name="outPpn" value="0">
                                            </td>
                                            <td width="60%" align="center">
                                                <g:textField name="ppn" id="ppn" style="margin: 1px;" readonly="" value="0"/>
                                            </td>
                                        </tr>
                                        <tr style="height: 30px;">
                                            <td width="40%" style="padding-left: 5px;">Total Bayar (F) * </td>
                                            <td width="60%" align="center">
                                                <g:textField name="totalBayar" id="totalBayar" class="numeric" readonly="" style="margin: 1px;" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="50%">
                                    <span style="text-decoration: underline;">Kalkulator</span>
                                    <table width="100%" border="1">
                                        <tr>
                                            <td width="50%" style="padding-left: 5px;">Total Uang</td>
                                            <td width="50%">
                                                <g:textField name="totalUang" id="totalUang" class="numeric" onkeyup="hitungKembalian();" style="margin: 1px;"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="50%" style="padding-left: 5px;">Kembalikan</td>
                                            <td width="50%">
                                                <g:textField name="kembalikan" id="kembalikan" class="numeric" style="margin: 1px;" readonly=""/>
                                            </td>
                                        </tr>
                                    </table>
                                    <br/>
                                    <table width="100%">
                                        <tr style="height: 30px;">
                                            <td width="40%" style="border: 1px #000000 solid; padding-left: 5px;">REFUND BOOKING FEE</td>
                                            <td width="40%" style="border: 1px #000000 solid">
                                                <g:textField name="outRefundBookingFee" id="outRefundBookingFee" style="margin: 1px; width: 89%;" readonly=""/>
                                            </td>
                                            <td width="20%" style="padding-left: 5px;">
                                                <g:field type="button" style="width: 50px" class="btn btn-primary" onclick="window.location.href='#/refund';"
                                                         name="refund" id="refund" value="${message(code: 'default.refund.label', default: 'Refund')}" disabled="true"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr style="height: 30px;">
                                            <td width="40%" style="border: 1px #000000 solid; padding-left: 5px;">DISCOUNT GOODWILL</td>
                                            <td width="40%" style="border: 1px #000000 solid">
                                                <g:textField name="outDiscountGoodwill" id="outDiscountGoodwill" style="margin: 1px; width: 89%;" readonly=""/>
                                            </td>
                                            <td width="20%">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <g:field type="button" style="width: 120px" class="btn btn-primary" onclick="saveDataSettlement();"
                                                         name="save" id="save" value="${message(code: 'default.save.settlement.label', default: 'Save Settlement')}" />
                                                <g:field type="button" style="width: 120px; display: none" class="btn btn-primary" onclick="doPrint();"
                                                         name="printKW" id="printKW" value="${message(code: 'cetak.kwitansi.label', default: 'Cetak Kwitansi')}" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr style="height: 110px;">
                                            <td colspan="3" valign="bottom">
                                                <h4>F = A - (B + C + D + E)</h4>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" width="100%">
                                    <span style="text-decoration: underline;">Detail Pembayaran</span>
                                    <div class="box" style="padding-top: 0px; padding-left: 15px; padding-right: 15px; width: 720px; margin-bottom: 0px;">
                                        <g:render template="settlementDataTables"/>
                                        <table width="100%" border="1">
                                            <tr style="height:30px;">
                                                <td width="70%" style="padding-left: 5px;">Total Detail Pembayaran (E)</td>
                                                <td width="30%">
                                                    <g:textField name="totalPembayaran" id="totalPembayaran" style="margin: 1px;" readonly="" value="0"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" width="100%">
                                    <table width="100%">
                                        <tr style="height: 30px;">
                                            <td width="30%" style="border: 1px solid #000000; padding-left: 5px;">STATUS SETTLEMENT</td>
                                            <td width="50%" style="border: 1px solid #000000;" align="center">
                                                <g:textField name="statusSettlement" id="statusSettlement" style="margin: 1px; width: 95%;" readonly=""/>
                                            </td>
                                            <td width="20%" style="padding-left: 5px;">
                                                <g:field type="button" style="width: 120px" class="btn btn-primary" disabled="" onclick="window.location.href='#/gatePassT800';"
                                                         name="gate" id="gate" value="${message(code: 'default.gate.label', default: 'Gate Pass')}" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <br/>
    <ul class="nav pull-right">
        <g:field type="button" style="width: 90px" class="btn btn-primary" onclick="closeData();"
                 name="close" id="close" value="${message(code: 'default.close.label', default: 'Close')}" />
    </ul>
</body>
</html>