
<%@ page import="com.kombos.parts.SOQ" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'SOQ.label', default: 'SOQ')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/SOQ/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/SOQ/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#SOQ-form').empty();
    	$('#SOQ-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#SOQ-table").hasClass("span12")){
   			$("#SOQ-table").toggleClass("span12 span5");
        }
        $("#SOQ-form").css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
   		if($("#SOQ-table").hasClass("span5")){
   			$("#SOQ-table").toggleClass("span5 span12");
   		}
        $("#SOQ-form").css("display","none");
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#SOQ-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/SOQ/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadSOQTable();
    		}
		});
		
   	}
    tambahReq2 = function(){
        checkJob =[];
        $("#SOQ-table tbody .row-select").each(function() {
        if(this.checked){
            var id = $(this).next("input:hidden").val();
            checkJob.push(id);
        }
        });
        if(checkJob.length<1){
            alert('Anda belum memilih data yang akan ditambahkan');
             reloadSOQTable();
        }
        var checkJobMap = JSON.stringify(checkJob);

        $.ajax({
            url:'${request.contextPath}/SOQ/tambah',
            type: "POST", // Always use POST when deleting data
            data : { ids: checkJobMap },
            success : function(data){
				$('#result_search').empty();
                reloadSOQTable();
				toastr.success('<div>Request Parts telah dibuat.</div>');
            },
			error: function(xhr, textStatus, errorThrown) {
			alert('Internal Server Error');
			}
        });

        checkJob = [];

            }
      });
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			%{--<li><a class="pull-right box-action" href="#"--}%
				%{--style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i--}%
					%{--class="icon-plus"></i>&nbsp;&nbsp;--}%
			%{--</a></li>--}%
			%{--<li><a class="pull-right box-action" href="#"--}%
				%{--style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i--}%
					%{--class="icon-remove"></i>&nbsp;&nbsp;--}%
			%{--</a></li>--}%
			%{--<li class="separator"></li>--}%
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="SOQ-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <fieldset>
                <table style="padding-right: 10px">
                    <tr>
                        <td style="width: 130px">
                            <label class="control-label" for="t951Tanggal">
                                <g:message code="auditTrail.tanggal.label" default="Tanggal" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="filter_t156Tanggal" class="controls">
                                <ba:datePicker id="search_t156Tanggal" name="search_t156Tanggal" precision="day" format="dd/MM/yyyy"  value="${new Date()}" />

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label" for="userRole">
                                <g:message code="auditTrail.role.label" default="Kriteria Parts" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="filter_status" class="controls">
                                %{--<g:radioGroup name="search_status" id="search_status" values="['','0','1']" labels="['Semua Parts','Belum Parts','Sudah Parts']" >--}%
                                    %{--${it.radio} ${it.label}--}%
                                %{--</g:radioGroup>--}%
                                <input type="radio" name="search_status" id="search_status1" value="" />Semua Parts &nbsp;
                            <input type="radio" name="search_status" id="search_status2"  value="1" />Sudah Request &nbsp;
                                <input type="radio" name="search_status" id="search_status3" value="0" />Belum Request &nbsp;
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" >
                            <div class="controls" style="right: 0">
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn-primary view" name="view" id="view" >Search</button>
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn-primary clear" name="clear" id="clear" >Clear</button>
                            </div>
                        </td>
                    </tr>
                </table>

            </fieldset>
			<g:render template="dataTables" />
            <g:field type="button" onclick="tambahReq2();" class="btn btn-primary create" name="tambah" id="tambah" value="${message(code: 'default.button.upload.label', default: 'Request Parts')}" />
		</div>
		<div class="span7" id="SOQ-form" style="display: none;"></div>
	</div>
</body>
</html>
