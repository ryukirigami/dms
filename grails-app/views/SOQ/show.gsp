

<%@ page import="com.kombos.parts.SOQ" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'SOQ.label', default: 'SOQ')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteSOQ;

$(function(){ 
	deleteSOQ=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/SOQ/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadSOQTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-SOQ" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="SOQ"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${SOQInstance?.companyDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="companyDealer-label" class="property-label"><g:message
					code="SOQ.companyDealer.label" default="Company Dealer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="companyDealer-label">
						%{--<ba:editableValue
								bean="${SOQInstance}" field="companyDealer"
								url="${request.contextPath}/SOQ/updatefield" type="text"
								title="Enter companyDealer" onsuccess="reloadSOQTable();" />--}%
							
								<g:link controller="companyDealer" action="show" id="${SOQInstance?.companyDealer?.id}">${SOQInstance?.companyDealer?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${SOQInstance?.t156Tanggal}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t156Tanggal-label" class="property-label"><g:message
					code="SOQ.t156Tanggal.label" default="T156 Tanggal" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t156Tanggal-label">
						%{--<ba:editableValue
								bean="${SOQInstance}" field="t156Tanggal"
								url="${request.contextPath}/SOQ/updatefield" type="text"
								title="Enter t156Tanggal" onsuccess="reloadSOQTable();" />--}%
							
								<g:formatDate date="${SOQInstance?.t156Tanggal}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${SOQInstance?.goods}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="goods-label" class="property-label"><g:message
					code="SOQ.goods.label" default="Goods" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="goods-label">
						%{--<ba:editableValue
								bean="${SOQInstance}" field="goods"
								url="${request.contextPath}/SOQ/updatefield" type="text"
								title="Enter goods" onsuccess="reloadSOQTable();" />--}%
							
								<g:link controller="goods" action="show" id="${SOQInstance?.goods?.id}">${SOQInstance?.goods?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${SOQInstance?.t156Qty1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t156Qty1-label" class="property-label"><g:message
					code="SOQ.t156Qty1.label" default="T156 Qty1" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t156Qty1-label">
						%{--<ba:editableValue
								bean="${SOQInstance}" field="t156Qty1"
								url="${request.contextPath}/SOQ/updatefield" type="text"
								title="Enter t156Qty1" onsuccess="reloadSOQTable();" />--}%
							
								<g:fieldValue bean="${SOQInstance}" field="t156Qty1"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${SOQInstance?.t156Qty2}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t156Qty2-label" class="property-label"><g:message
					code="SOQ.t156Qty2.label" default="T156 Qty2" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t156Qty2-label">
						%{--<ba:editableValue
								bean="${SOQInstance}" field="t156Qty2"
								url="${request.contextPath}/SOQ/updatefield" type="text"
								title="Enter t156Qty2" onsuccess="reloadSOQTable();" />--}%
							
								<g:fieldValue bean="${SOQInstance}" field="t156Qty2"/>
							
						</span></td>
					
				</tr>
				</g:if>
            <g:if test="${SOQInstance?.satuan1}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="satuan1-label" class="property-label"><g:message
                                code="SOQ.satuan1.label" default="satuan1" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="satuan1-label">
                        %{--<ba:editableValue
                                bean="${SOQInstance}" field="goods"
                                url="${request.contextPath}/SOQ/updatefield" type="text"
                                title="Enter goods" onsuccess="reloadSOQTable();" />--}%

                        <g:link controller="satuan1" action="show" id="${SOQInstance?.satuan1?.id}">${SOQInstance?.satuan1?.encodeAsHTML()}</g:link>

                    </span></td>

                </tr>
            </g:if>
            <g:if test="${SOQInstance?.satuan2}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="satuan2-label" class="property-label"><g:message
                                code="SOQ.satuan2.label" default="satuan2" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="satuan2-label">
                        %{--<ba:editableValue
                                bean="${SOQInstance}" field="goods"
                                url="${request.contextPath}/SOQ/updatefield" type="text"
                                title="Enter goods" onsuccess="reloadSOQTable();" />--}%

                        <g:link controller="satuan2" action="show" id="${SOQInstance?.satuan2?.id}">${SOQInstance?.satuan2?.encodeAsHTML()}</g:link>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${SOQInstance?.statusParts}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m703StaPersenRp-label" class="property-label"><g:message
                                code="SOQ.statusParts.label" default="Status Parts" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="statusParts-label">
                        %{--<ba:editableValue
                                bean="${rateBankInstance}" field="m703StaPersenRp"
                                url="${request.contextPath}/RateBank/updatefield" type="text"
                                title="Enter m703StaPersenRp" onsuccess="reloadRateBankTable();" />

                                <g:fieldValue bean="${rateBankInstance}" field="m703StaPersenRp"/> --}%
                        <g:if test="${SOQInstance.statusParts == '0'}">
                            <g:message code="SOQ.statusParts.label.belum" default="Belum Parts" />
                        </g:if>
                        <g:else>
                            <g:message code="SOQ.statusParts.label.Sudah" default="Sudah Parts" />
                        </g:else>
                    </span></td>

                </tr>
            </g:if>
				<g:if test="${SOQInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="SOQ.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${SOQInstance}" field="createdBy"
								url="${request.contextPath}/SOQ/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadSOQTable();" />--}%
							
								<g:fieldValue bean="${SOQInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${SOQInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="SOQ.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${SOQInstance}" field="updatedBy"
								url="${request.contextPath}/SOQ/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadSOQTable();" />--}%
							
								<g:fieldValue bean="${SOQInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${SOQInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="SOQ.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${SOQInstance}" field="lastUpdProcess"
								url="${request.contextPath}/SOQ/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadSOQTable();" />--}%
							
								<g:fieldValue bean="${SOQInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${SOQInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="SOQ.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${SOQInstance}" field="dateCreated"
								url="${request.contextPath}/SOQ/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadSOQTable();" />--}%
							
								<g:formatDate date="${SOQInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${SOQInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="SOQ.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${SOQInstance}" field="lastUpdated"
								url="${request.contextPath}/SOQ/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadSOQTable();" />--}%
							
								<g:formatDate date="${SOQInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${SOQInstance?.id}"
					update="[success:'SOQ-form',failure:'SOQ-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteSOQ('${SOQInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
