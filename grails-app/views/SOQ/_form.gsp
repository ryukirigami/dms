<%@ page import="com.kombos.parts.SOQ" %>

<g:javascript>
    function Nomor(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</g:javascript>


<div class="control-group fieldcontain ${hasErrors(bean: SOQInstance, field: 't156Tanggal', 'error')} ">
	<label class="control-label" for="t156Tanggal">
		<g:message code="SOQ.t156Tanggal.label" default="Tanggal" />
		
	</label>
	<div class="controls">
	<ba:datePicker name="t156Tanggal" precision="day"  value="${SOQInstance?.t156Tanggal}" default="none" noSelection="['': '']" required="" format="dd/MM/yyyy" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: SOQInstance, field: 'goods', 'error')} ">
	<label class="control-label" for="goods">
		<g:message code="SOQ.goods.label" default="Goods" />
		
	</label>
	<div class="controls">
        <g:if test="${aksi=="ubah"}">
            <g:textField name="goods.id" value="${SOQInstance?.goods?.id}" readonly="" />
        </g:if>
        <g:else>
            <g:select id="goods" name="goods.id" from="${com.kombos.parts.Goods.list()}" optionKey="id" value="${SOQInstance?.goods?.id}" class="many-to-one" noSelection="['null': '']"/>

        </g:else>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: SOQInstance, field: 't156Qty1', 'error')} ">
	<label class="control-label" for="t156Qty1">
		<g:message code="SOQ.t156Qty1.label" default="Jumlah 1" />
		
	</label>
	<div class="controls">
	<g:textField name="t156Qty1" value="${fieldValue(bean: SOQInstance, field: 't156Qty1')}" onkeypress="return Nomor(event)"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: SOQInstance, field: 't156Qty2', 'error')} ">
	<label class="control-label" for="t156Qty2">
		<g:message code="SOQ.t156Qty2.label" default="Jumlah 2" />
		
	</label>
	<div class="controls">
	<g:textField name="t156Qty2" value="${fieldValue(bean: SOQInstance, field: 't156Qty2')}" onkeypress="return Nomor(event)"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: SOQInstance, field: 'satuan1', 'error')} ">
    <label class="control-label" for="satuan1">
        <g:message code="SOQ.satuan1.label" default="Satuan 1" />

    </label>
    <div class="controls">
        <g:select id="satuan1" name="satuan1.id" from="${com.kombos.parts.Satuan.list()}" optionKey="id" value="${SOQInstance?.satuan1?.id}" class="many-to-one" noSelection="['null': '']"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: SOQInstance, field: 'satuan2', 'error')} ">
    <label class="control-label" for="satuan2">
        <g:message code="SOQ.goods.label" default="Satuan 2" />

    </label>
    <div class="controls">
        <g:select id="satuan2" name="satuan2.id" from="${com.kombos.parts.Satuan.list()}" optionKey="id" value="${SOQInstance?.satuan2?.id}" class="many-to-one" noSelection="['null': '']"/>
    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: SOQInstance, field: 'statusParts', 'error')} ">
    <label class="control-label" for="statusParts">
        <g:message code="SOQ.statusParts.label" default="Kriteria Parts" />

    </label>
    <div class="controls">
        <g:radioGroup name="statusParts" id="statusParts" values="['0','1']" labels="['Belum Parts','Sudah Parts']" value="${SOQInstance?.statusParts}" >
            ${it.radio} ${it.label}
        </g:radioGroup>
    </div>
</div>