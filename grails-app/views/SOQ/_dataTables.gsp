
<%@ page import="com.kombos.parts.SOQ" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="SOQ_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="SOQ.t156Tanggal.label" default="Id" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="SOQ.goods.label" default="Kode Parts" /></div>
            </th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="SOQ.goods2.label" default="Nama Parts" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="SOQ.t156Qty1.label" default="Jumlah" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="SOQ.t156Satuan.label" default="Satuan" /></div>
			</th>

		
		</tr>
		%{--<tr>--}%
		%{----}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_companyDealer" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%

			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_t156Tanggal" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >--}%
					%{--<input type="hidden" name="search_t156Tanggal" value="date.struct">--}%
					%{--<input type="hidden" name="search_t156Tanggal_day" id="search_t156Tanggal_day" value="">--}%
					%{--<input type="hidden" name="search_t156Tanggal_month" id="search_t156Tanggal_month" value="">--}%
					%{--<input type="hidden" name="search_t156Tanggal_year" id="search_t156Tanggal_year" value="">--}%
					%{--<input type="text" data-date-format="dd/mm/yyyy" name="search_t156Tanggal_dp" value="" id="search_t156Tanggal" class="search_init">--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_goods" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_goods" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_t156Qty1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_t156Qty1" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_t156Qty2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_t156Qty2" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_createdBy" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_updatedBy" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_lastUpdProcess" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%


		%{--</tr>--}%
	</thead>
</table>

<g:javascript>
var SOQTable;
var reloadSOQTable;
var checked = [];
$(function(){

 $('#clear').click(function(e){
        $('#search_t156Tanggal').val("");
        $('#search_t156Tanggal_day').val("");
        $('#search_t156Tanggal_month').val("");
        $('#search_t156Tanggal_year').val("");
        $('#search_t156Tanggal2').val("");
        $('#search_t156Tanggal2_day').val("");
        $('#search_t156Tanggal2_month').val("");
        $('#search_t156Tanggal2_year').val("");
        $("#search_status1").prop("checked", false);
        $("#search_status2").prop("checked", false);
        $("#search_status3").prop("checked", false);

        reloadSOQTable();
	});
	function getCheckedRadioId(name) {
         var elements = document.getElementsByName(name);

         for (var i=0, len=elements.length; i<len; ++i){
                if (elements[i].checked)
                    return elements[i].value;
         }

    }
    $('#view').click(function(e){
        e.stopPropagation();
        checked = [];
	    $('.row-select').attr("checked",false);
		SOQTable.fnDraw();

//        reloadAuditTrailTable();
	});
	reloadSOQTable = function() {
	    checked = [];
	    $('.row-select').attr("checked",false);
		SOQTable.fnDraw();
	}

	
	$('#search_t156Tanggal').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t156Tanggal_day').val(newDate.getDate());
			$('#search_t156Tanggal_month').val(newDate.getMonth()+1);
			$('#search_t156Tanggal_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
		//	SOQTable.fnDraw();
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	SOQTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	SOQTable = $('#SOQ_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			var aData = SOQTable.fnGetData(nRow);
		        var checkbox = $('.row-select', nRow);
		        if(checked.indexOf(""+aData['id'])>=0){
		              checkbox.attr("checked",true);
		              checkbox.parent().parent().addClass('row_selected');
		        }

		        checkbox.click(function (e) {
            	    var tc = $(this);

                    if(this.checked)
            			tc.parent().parent().addClass('row_selected');
            		else {
            			tc.parent().parent().removeClass('row_selected');
            			var selectAll = $('.select-all');
            			selectAll.removeAttr('checked');
                    }
            	 	e.stopPropagation();
                });
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "id",
	"mDataProp": "norut",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="#">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="#">&nbsp;&nbsp; &nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"80px",
	"bVisible": true
}

,

{
	"sName": "goods",
	"mDataProp": "goods",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "goods",
	"mDataProp": "goods2",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t156Qty1",
	"mDataProp": "t156Qty1",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "goods",
	"mDataProp": "satuan1",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                var selectAll = $('.select-all');
                selectAll.removeAttr('checked');
                $("#SOQ_datatables tbody .row-select").each(function() {
                            var id = $(this).next("input:hidden").val();
                            if(checked.indexOf(""+id)>=0){
                                checked[checked.indexOf(""+id)]="";
                            }
                            if(this.checked){
                                checked.push(""+id);
                            }
                        });
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}

						var t156Tanggal = $('#search_t156Tanggal').val();
						var t156TanggalDay = $('#search_t156Tanggal_day').val();
						var t156TanggalMonth = $('#search_t156Tanggal_month').val();
						var t156TanggalYear = $('#search_t156Tanggal_year').val();
						
						if(t156Tanggal){

							aoData.push(
									{"name": 'sCriteria_t156Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_t156Tanggal_dp', "value": t156Tanggal},
									{"name": 'sCriteria_t156Tanggal_day', "value": t156TanggalDay},
									{"name": 'sCriteria_t156Tanggal_month', "value": t156TanggalMonth},
									{"name": 'sCriteria_t156Tanggal_year', "value": t156TanggalYear}
							);
						}
	
						var goods = $('#filter_goods input').val();
						if(goods){
							aoData.push(
									{"name": 'sCriteria_goods', "value": goods}
							);
						}
	
						var t156Qty1 = $('#filter_t156Qty1 input').val();
						if(t156Qty1){
							aoData.push(
									{"name": 'sCriteria_t156Qty1', "value": t156Qty1}
							);
						}
	
						var t156Qty2 = $('#filter_t156Qty2 input').val();
						if(t156Qty2){
							aoData.push(
									{"name": 'sCriteria_t156Qty2', "value": t156Qty2}
							);
						}

						var statusParts = getCheckedRadioId('search_status')

						if(statusParts){

							aoData.push(
									{"name": 'sCriteria_status', "value": statusParts}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
