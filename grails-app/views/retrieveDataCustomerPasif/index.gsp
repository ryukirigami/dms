<%--
  Created by IntelliJ IDEA.
  User: Mohammad Fauzi
  Date: 3/3/14
  Time: 2:44 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>

<html>

<head>
    <meta name="layout" content="main">
    <title>MRS General Parameter</title>
    <r:require modules="baseapplayout"/>


    <g:javascript disposition="head">
        $(function () {

            jQuery("#buttonSave").click(function (event) {
                jQuery('#spinner').fadeIn(1);
                $(this).submit();
                event.preventDefault();
            });

            jQuery("#buttonClose").click(function (event) {
                event.preventDefault(); //to stop the default loading
            });

            completeProcess = function () {

            }

            doResponse = function (data) {
                jQuery('#spinner').fadeOut();
                if (data.status == 'OK') {
                    alert('Retrieve Data Customer Pasif telah dilakukan\n'+data.hasil);
                    if(data.hasil!='Data record tidak ditemukan'){
                        window.location.replace('#/reminder');
                    }
                } else {
                    alert('Save fail\n' + data.error);
                }

            }
        });
    </g:javascript>
</head>

<body>
<g:formRemote name="form" id="form" on404="alert('not found!')" update="updateMe"
              method="post"
              onComplete="completeProcess()"
              onSuccess="doResponse(data)"
              url="[controller: 'retrieveDataCustomerPasif', action: 'doSave']">
    <fieldset class="form">

        <div class="box">
            <h3>Tanggal Terakhir Service</h3>
            <table style="width: 100%;border: 0px">
                <tr>
                    <td>
                        <label class="control-label" for="start">
                            Tanggal Terakhir Service
                            <span class="required-indicator">*</span>
                        </label>
                    </td>
                    <td>
                        <ba:datePicker name="start" precision="day" value="${new Date()-30}" format="dd/MM/yyyy"/>  s.d
                        <ba:datePicker name="end" precision="day" value="${new Date()}" format="dd/MM/yyyy"/>
                    </td>
                </tr>

            </table>

        </div>

    </fieldset>


    <fieldset class="buttons controls">

        %{--<button class="btn btn-primary" id="buttonEdit" onclick="onEdit()">Edit</button>--}%
        <button class="btn btn-primary" id="buttonSave">Retreive</button>
        %{--<button class="btn btn-primary" id="buttonCancel" onclick="onCancel()">Cancel</button>--}%

        <button class="btn btn-primary" id="buttonClose" onclick="window.location.replace('#/home');">Close</button>

    </fieldset>
</g:formRemote>

</body>
</html>