<%@ page import="com.kombos.administrasi.CompanyDealer" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
	<style>
	.form-horizontal input + .help-block, .form-horizontal select + .help-block, .form-horizontal textarea + .help-block, .form-horizontal .uneditable-input + .help-block, .form-horizontal .input-prepend + .help-block, .form-horizontal .input-append + .help-block {
		font-size: 10px !important;
		font-style: italic !important;
		margin-top: 6px !important;
	}
	.modal.fade.in {
		left: 25.5% !important;
		width: 1205px !important;
	}
	.modal.fade {
		top: -100%;
	}
	</style>
<r:require modules="baseapplayout" />
<g:javascript disappointmentGrition="head">
	$(function(){
		$("#btnCariGoods").click(function() {
            oFormService.fnShowGoodsDataTable();
        })
        oFormService = {
            fnShowGoodsDataTable: function() {
                $("#goodsModal").modal("show");
                $("#goodsModal").fadeIn();
            },
            fnHideGoodsDataTable: function() {
                $("#goodsModal").modal("hide");
            }
        }
				
        $('#bulan1').change(function(){
            $('#bulan2').val($('#bulan1').val());
        });
        $('#bulan2').change(function(){
            if(parseInt($('#bulan1').val()) > parseInt($('#bulan2').val())){
                $('#bulan2').val($('#bulan1').val());
            }
        });
		
		$('#filter_periode').show();
		$('#filter_periode2').hide();
		$('#filterNoWo').hide();
		$('#filterKodeParts').hide();
		$('#filterKodeVendor').hide();
		$('#filterKodeOH').hide();
		$('#filterKodeDisposal').hide();
		$('#filterKodeReserved').hide();
		$('#filterKodeBlocked').hide();
		$('#filterKodeWip').hide();
		$('#filterStockMovementPerCode').hide();
			
        $('#namaReport').change(function(){
			$('#filter_periode').hide();
			$('#filter_periode2').hide();
			$('#filterNoWo').hide();
			$('#filterKodeParts').hide();
			$('#filterKodeVendor').hide();
			$('#filterKodeOH').hide();
			$('#filterKodeDisposal').hide();
			$('#filterKodeReserved').hide();
			$('#filterKodeBlocked').hide();
			$('#filterKodeWip').hide();
			
             if($('#namaReport').val()=="01"){
                $('#filter_periode').show();                
				$('#filterNoWo').hide();
				$('#filterStockMovementPerCode').hide();
             }else if($('#namaReport').val()=="02"){                 
                 $('#filter_periode').show();
				 $('#filterKodeParts').show();				
				 $('#filterStockMovementPerCode').hide();
			 }else if($('#namaReport').val()=="03"){                 
                 $('#filter_periode').show();	
				$('#filterStockMovementPerCode').hide();				 
             }else if($('#namaReport').val()=="035"){
                 $('#filter_periode').show();
				$('#filterStockMovementPerCode').hide();
             }else if($('#namaReport').val()=="04"){
                 $('#filter_periode').show();
				 $('#filterKodeVendor').hide();
				 $('#filterStockMovementPerCode').hide();
             }else if($('#namaReport').val()=="05"){
                $('#filter_periode').show();
				$('#filterKodeVendor').show();			
				$('#filterStockMovementPerCode').hide();				
             }else if($('#namaReport').val()=="06"){
                $('#filter_periode').show();
				$('#filterKodeOH').show();				
				$('#filterStockMovementPerCode').hide();
             }else if($('#namaReport').val()=="07"){
                $('#filter_periode').show();
				$('#filterKodeDisposal').show();		
				$('#filterStockMovementPerCode').hide();				
             }
			 else if($('#namaReport').val()=="08"){
                $('#filter_periode').show();
				// $('#filterKodeOH').show();
				// $('#filterKodeDisposal').show();
				// $('#filterKodeReserved').show();
				// $('#filterKodeBlocked').show();
				// $('#filterKodeWip').show();
				// $('#filterStockMovementPerCode').hide();
             }else if($('#namaReport').val()=="09" || $('#namaReport').val()=="16"){
                $('#filter_periode').show();
				$('#filterKodeVendor').show();				
				$('#filterStockMovementPerCode').hide();
             }
			 else if($('#namaReport').val()=="10"){
                $('#filter_periode').show();				
             }else if($('#namaReport').val()=="11" || $('#namaReport').val()=="12" || $('#namaReport').val()=="13" || $('#namaReport').val()=="14" || $('#namaReport').val()=="15"){
				$('#filter_periode').show();				
				$('#filter_periode2').hide();
				$('#filterNoWo').hide();
				$('#filterKodeParts').hide();
				$('#filterKodeVendor').hide();
				$('#filterKodeOH').hide();
				$('#filterKodeDisposal').hide();
				$('#filterKodeReserved').hide();
				$('#filterKodeBlocked').hide();
				$('#filterKodeWip').hide();
				if($('#namaReport').val() == "12"){
					$('#filterStockMovementPerCode').show();
				} else {
					$('#filterStockMovementPerCode').hide();
				}
			 }
			 
         });


	    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    });
    var checkin = $('#search_tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate());
                checkout.setValue(newDate);
                checkin.hide();
                $('#search_tanggal2')[0].focus();
            }).data('datepicker');

    var checkout = $('#search_tanggal2').datepicker({
        onRender: function(date) {
            return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
                checkout.hide();
    }).data('datepicker');

     previewData = function(){
		var namaReport = $('#namaReport').val();
		var workshop = $('#workshop').val();
		var search_tanggal = $('#search_tanggal').val();
		var search_tanggal2 = $('#search_tanggal2').val();
		var bulan1 = $('#bulan1').val();
		var bulan2= $('#bulan2').val();
		var tahun = $('#tahun').val();
		var ceklis = "";
		var nomorWo = $('#nomorWo').val();
		var kodePart = $('#kodePart').val();
		var kodeVendor = $('#kodeVendor').val();
		var kodeOH = $('#kodeOH').val();
		var kodeDisposal = $('#kodeDisposal').val();
		var kodeReserved = $('#kodeReserved').val();
		var kodeBlocked = $('#kodeBlocked').val();
		var kodeWip = $('#kodeWip').val();
		var kodePart = $('#kodePart').val();
		var goods_id = $('#goods_id').val();
		var namaPart = $('#namaPart').val();
        var format = $('input[name="formatReport"]:checked').val();
		var url ="";
	   if(namaReport == "" || namaReport == null){
			alert('Harap Isi Data Dengan Lengkap');
			return;
	   }
		if (namaReport == "01"){
			url = "${request.contextPath}/partSalesReport/previewData?namaReport="+namaReport+"&workshop="+workshop+"&bulan1="+bulan1+"&bulan2="+bulan2+"&tahun="+tahun+"&tanggal="+search_tanggal+"&tanggal2="+search_tanggal2+"&format="+format;
		} else if (namaReport == "02") {
			url = "${request.contextPath}/partSalesReport/previewData?namaReport="+namaReport+"&workshop="+workshop+"&bulan1="+bulan1+"&bulan2="+bulan2+"&tahun="+tahun+"&tanggal="+search_tanggal+"&tanggal2="+search_tanggal2+"&goods_id="+goods_id+"&format="+format;
		} else if (namaReport == "035") {
			url = "${request.contextPath}/partSalesReport/previewData?namaReport="+namaReport+"&workshop="+workshop+"&bulan1="+bulan1+"&bulan2="+bulan2+"&tahun="+tahun+"&tanggal="+search_tanggal+"&tanggal2="+search_tanggal2+"&format="+format;
		}else if (namaReport == "03") {
			url = "${request.contextPath}/partSalesReport/previewData?namaReport="+namaReport+"&workshop="+workshop+"&bulan1="+bulan1+"&bulan2="+bulan2+"&tahun="+tahun+"&tanggal="+search_tanggal+"&tanggal2="+search_tanggal2+"&format="+format;
		} else if (namaReport == "04") {
			url = "${request.contextPath}/partSalesReport/previewData?namaReport="+namaReport+"&workshop="+workshop+"&bulan1="+bulan1+"&bulan2="+bulan2+"&tahun="+tahun+"&tanggal="+search_tanggal+"&tanggal2="+search_tanggal2+"&format="+format;
		} else if (namaReport == "05") {
			url = "${request.contextPath}/partSalesReport/previewData?namaReport="+namaReport+"&workshop="+workshop+"&bulan1="+bulan1+"&bulan2="+bulan2+"&tahun="+tahun+"&tanggal="+search_tanggal+"&tanggal2="+search_tanggal2+"&kodeVendor="+kodeVendor+"&format="+format;
		} else if (namaReport == "06") {
			url = "${request.contextPath}/partSalesReport/previewData?namaReport="+namaReport+"&workshop="+workshop+"&bulan1="+bulan1+"&bulan2="+bulan2+"&tahun="+tahun+"&tanggal="+search_tanggal+"&tanggal2="+search_tanggal2+"&kodeOH="+kodeOH+"&format="+format;
		} else if (namaReport == "07") {
			url = "${request.contextPath}/partSalesReport/previewData?namaReport="+namaReport+"&workshop="+workshop+"&bulan1="+bulan1+"&bulan2="+bulan2+"&tahun="+tahun+"&tanggal="+search_tanggal+"&tanggal2="+search_tanggal2+"&kodeDisposal="+kodeDisposal+"&format="+format;
		} else if (namaReport == "08") {
			url = "${request.contextPath}/partSalesReport/previewData?namaReport="+namaReport+"&workshop="+workshop+"&bulan1="+bulan1+"&bulan2="+bulan2+"&tahun="+tahun+"&tanggal="+search_tanggal+"&tanggal2="+search_tanggal2+"&kodeOH="+kodeOH+"&kodeReserved="+kodeReserved+"&kodeWip="+kodeWip+"&kodeBlocked="+kodeBlocked+"&kodeDisposal="+kodeDisposal+"&format="+format;
		} else if (namaReport == "09") {
			url = "${request.contextPath}/partSalesReport/previewData?namaReport="+namaReport+"&workshop="+workshop+"&bulan1="+bulan1+"&bulan2="+bulan2+"&tahun="+tahun+"&tanggal="+search_tanggal+"&tanggal2="+search_tanggal2+"&kodeVendor="+kodeVendor+"&format="+format;
		} else if (namaReport == "10" || namaReport == "16" || namaReport == "11" || namaReport == "13" || namaReport == "14" || namaReport == "15") {
			url = "${request.contextPath}/partSalesReport/previewDataManual?namaReport="+namaReport+"&workshop="+workshop+"&bulan1="+bulan1+"&bulan2="+bulan2+"&tahun="+tahun+"&tanggal="+search_tanggal+"&tanggal2="+search_tanggal2+"&kodeVendor="+kodeVendor+"&format="+format;
		} else if (namaReport == "12"){
			url = "${request.contextPath}/partSalesReport/previewDataManual?namaReport="+namaReport+"&workshop="+workshop+"&bulan1="+bulan1+"&bulan2="+bulan2+"&tahun="+tahun+"&tanggal="+search_tanggal+"&tanggal2="+search_tanggal2+"&kodeVendor="+kodeVendor+"&kodePart="+kodePart+"&namaPart="+namaPart+"&format="+format;
		}		
		window.location = url

    }
     previewTest = function(){
            var namaReport = $('#namaReport').val();
            var workshop = $('#workshop').val();
            var search_tanggal = $('#search_tanggal').val();
            var search_tanggal2 = $('#search_tanggal2').val();
            var bulan1 = $('#bulan1').val();
            var bulan2= $('#bulan2').val();
            var tahun = $('#tahun').val();
            var ceklis = "";
			var nomorWo = $('#nomorWo').val();
			if(namaReport == '01' && (nomorWo == "" || nomorWo == null)){
			    alert('Nomor WO belum terisi');
                return;
			}
            if(document.getElementById("detail").checked){ceklis = "1"}else{ceklis = "0"}
            if(namaReport == "" || namaReport == null){
                alert('Harap Isi Data Dengan Lengkap');
                return;
            }
           window.location = "${request.contextPath}/partSalesReport/previewTest?namaReport="+namaReport+"&workshop="+workshop+"&bulan1="+bulan1+"&bulan2="+bulan2+"&tahun="+tahun+"&tanggal="+search_tanggal+"&tanggal2="+search_tanggal2+"&nomorWo="+nomorWo;
    }
</g:javascript>

</head>
<body>
<div class="navbar box-header no-border">
    Kriteria Report -> Part Sales
</div><br>
<div class="box">
    <div class="span12" id="partSalesReport-table">
        <div id="form-partSalesReport" class="form-horizontal">
            <fieldset>
                <div class="control-group">
                    <label class="control-label" for="filter_periode" style="text-align: left;">
                        Format Report
                    </label>
                    <div id="filter_format" class="controls">
                        <g:radioGroup name="formatReport" id="formatReport" values="['x','p']" labels="['Excel','PDF']" value="x" required="" >
                            ${it.radio} ${it.label}
                        </g:radioGroup>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="filter_periode" style="text-align: left;">
                        Workshop
                    </label>
                    <div id="filter_company" class="controls">
                        <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                            <g:select name="workshop" id="workshop" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                        </g:if>
                        <g:else>
                            <g:select name="workshop" id="workshop" readonly="" disabled="" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                        </g:else>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="t951Tanggal" style="text-align: left;">
                        Periode
                    </label>
                    <div id="filter_periode" class="controls">
                        <ba:datePicker id="search_tanggal" name="search_tanggal" precision="day" format="dd-MM-yyyy"  value="${new Date()}" />
                        &nbsp;&nbsp;s.d.&nbsp;&nbsp;
                        <ba:datePicker id="search_tanggal2" name="search_tanggal2" precision="day" format="dd-MM-yyyy"  value="${new Date()+1}"  />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="t951Tanggal" style="text-align: left;">
                        Nama Report
                    </label>
                    <div id="filter_nama" class="controls">
                        <select name="namaReport" id="namaReport" size="16" style="width: 44%; font-size: 12px;">
                            <option value="01">Part Sales Per Parts</option>
							<option value="02">Transaksi Part Per Kode Parts</option>
							<option value="16">Transaksi Part Per Vendor</option>
							<option value="035">Penjualan Tunai / Langsung</option>
                            <option value="03">Part Sales</option>
							<option value="04">Part Receiving</option>
							<option value="05">Part Return</option>
							<option value="06">Part On Hand Adjustment</option>
							<option value="07">Part Disposal</option>
							<option value="08">Stock Report</option>
							<option value="10">Data Of Parts Transaction</option>
							<option value="11">Stock Movement Report</option>
							<option value="12">Stock Movement Per Kode Report</option>
							<option value="13">WIP Unit Report</option>
							<option value="14">Stock Taking</option>
							<option value="15">Lampiran BA Stock Taking</option>
                        </select>
                    </div>
                </div>
                <div class="control-group">

					<div id="filterNoWo">	
						<label class="control-label" for="t951Tanggal" style="text-align: left;">
							Nomor Wo
						</label>
						<div id="filter_kode1" class="controls">
							<input name="nomorWo" id="nomorWo" size="8" style="width: 44%; font-size: 12px;" />
						</div>
					</div>
					<div id="filterKodeParts">	
						<label class="control-label" for="t951Tanggal" style="text-align: left;">
							Kode Part
						</label>
						<div id="filter_kode2" class="controls">
							<g:textField name="goods.name" id="goods_nama" readonly="" style="width: 40%"/>
							<span class="add-on">
								<a href="javascript:void(0);" id="btnCariGoods">
									<i class="icon-search"/>
								</a>
							</span>
							<g:hiddenField name="goods.id" id="goods_id" />

						</div>
					</div>
					<div id="filterKodeVendor">	
						<label class="control-label" for="t951Tanggal" style="text-align: left;">
							Kode/Nama Vendor
						</label>
						<div id="filter_kode3" class="controls">
							<input name="kodeVendor" id="kodeVendor" size="8" style="width: 44%; font-size: 12px;" />
						</div>
					</div>
					<div id="filterStockMovementPerCode">	
						<label class="control-label" for="kodeParts" style="text-align: left;">
							Kode Part
						</label>
						<div id="filter_kode4" class="controls">
							<input name="kodePart" id="kodePart" size="8" style="width: 44%; font-size: 12px;" />
						</div>
						<br/>
						<label class="control-label" for="kodeParts" style="text-align: left;">
							Nama Part
						</label>
						<div id="filter_kode5" class="controls">
							<input name="namaPart" id="namaPart" size="8" style="width: 44%; font-size: 12px;" />
						</div>
					</div>					
					<div id="filterKodeOH">	
						<label class="control-label" for="t951Tanggal" style="text-align: left;">
							Kode OnHands Adjustment
						</label>
						<div id="filter_kode6" class="controls">
							<input name="kodeOH" id="kodeOH" size="8" style="width: 44%; font-size: 12px;" />
						</div>
					</div>
					<div id="filterKodeDisposal">	
						<label class="control-label" for="t951Tanggal" style="text-align: left;">
							Nomor Disposal
						</label>
						<div id="filter_kode7" class="controls">
							<input name="kodeDisposal" id="kodeDisposal" size="8" style="width: 44%; font-size: 12px;" />
						</div>
					</div>
					<div id="filterKodeReserved">	
						<label class="control-label" for="t951Tanggal" style="text-align: left;">
							Nomor Reserved
						</label>
						<div id="filter_kode8" class="controls">
							<input name="kodeReserved" id="kodeReserved" size="8" style="width: 44%; font-size: 12px;" />
						</div>
					</div>
					<div id="filterKodeBlocked">	
						<label class="control-label" for="t951Tanggal" style="text-align: left;">
							Nomor Blocked
						</label>
						<div id="filter_kode9" class="controls">
							<input name="kodeBlocked" id="kodeBlocked" size="8" style="width: 44%; font-size: 12px;" />
						</div>
					</div>
					<div id="filterKodeWip">	
						<label class="control-label" for="t951Tanggal" style="text-align: left;">
							Nomor WIP
						</label>
						<div id="filter_wip" class="controls">
							<input name="kodeWip" id="kodeWip" size="8" style="width: 44%; font-size: 12px;" />
						</div>
					</div>
					
                    <div id="filter_periode2" class="controls">
                        %{
                            def listBulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
                        }%
                        <select name="bulan1" id="bulan1" style="width: 170px">
                            %{
                                for(int i=0;i<12;i++){
                                    out.print('<option value="'+(i+1)+'">'+listBulan.get(i)+'</option>');
                                }
                            }%
                        </select> &nbsp; - &nbsp;
                        <select name="bulan2" id="bulan2" style="width: 170px">
                            %{
                                for(int i=0;i<12;i++){
                                    out.print('<option value="'+(i+1)+'">'+listBulan.get(i)+'</option>');
                                }
                            }%
                        </select> &nbsp;
                       <select name="tahun" id="tahun" style="width: 106px">
                           %{
                            for(def a = (new Date().format("yyyy")).toInteger();a >= 2000 ;a--){
                                out.print('<option value="'+a+'">'+a+'</option>');
                            }
                           }%

                       </select>
                    </div>
                </div>

            </fieldset>
        </div>
           <g:field type="button" onclick="previewData()" class="btn btn-primary create" name="preview"  value="${message(code: 'default.button.upload.label', default: 'Preview')}" />
           <g:field type="button" onclick="window.location.replace('#/home')" class="btn btn-cancel cancel" name="cancel"  value="${message(code: 'default.button.upload.label', default: 'Close')}" />
    </div>
</div>
<div id="goodsModal" class="modal fade">
	<div class="modal-dialog" style="width: 1200px;">
		<div class="modal-content" style="width: 1200px;">
			<div class="modal-header">
				<a class="close" href="javascript:void(0);" data-dismiss="modal">×</a>
				<h4>Data Goods - List</h4>
			</div>
			<!-- dialog body -->
			<div class="modal-body" id="goodsModal-body" style="max-height: 1200px;">
				<g:render template="dataTablesGoods" />
			</div>

			<div class="modal-footer">
				<a href="javascript:void(0);" class="btn cancel" onclick="oFormService.fnHideGoodsDataTable();">
					Tutup
				</a>
				<a href="javascript:void(0);" id="btnPilihGoods" class="btn btn-primary">
					Pilih Goods
				</a>
			</div>
		</div>
	</div>
</div>
</body>
</html>
