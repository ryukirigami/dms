<%@ page import="com.kombos.parts.Goods" %>
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="goods_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.m111ID.label" default="Kode Goods" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.m111Nama.label" default="Nama Goods" /></div>
        </th>
    </tr>
    <tr>
        <th style="border-top: none;padding: 5px;">
            <div id="filter_m111ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_m111ID" id="search_m111ID" class="search_init" />
            </div>
        </th>
        <th style="border-top: none;padding: 5px;">
            <div id="filter_m111Nama" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_m111Nama" id="search_m111Nama" class="search_init" />
            </div>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
var goodsTable;
var reloadGoodsTable;
$(function(){
    $("#btnPilihGoods").click(function() {
        $("#goods_datatables tbody").find("tr").each(function() {
            var radio = $(this).find("td:eq(0) input[type='radio']");
            if (radio.is(":checked")) {
                var id = radio.data("id");
                var nama = radio.data("nama");
                var kode = radio.data("npk");
                $("#goods_id").val(id);
                $("#goods_nama").val(kode + " || " + nama);
                oFormService.fnHideGoodsDataTable();
            }
        })
        reloadGoodsTable();
    });

	reloadGoodsTable = function() {
		goodsTable.fnDraw();
		}

  $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	reloadGoodsTable();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	goodsTable = $('#goods_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : 10, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList", controller: "goods")}",
		"aoColumns": [

{
	"sName": "m111ID",
	"mDataProp": "m111ID",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return "<input type='radio' data-id='"+row["id"]+"' data-nama='"+row["m111Nama"]+"' data-npk='"+row["m111ID"]+"' name='radio' class='pull-left row-select' style='position: relative; top: 3px;' aria-label='Row " + row["id"] + "' title='Select this'><input type='hidden' value='"+row["id"]+"'>&nbsp;&nbsp;"+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}

,

{
	"sName": "m111Nama",
	"mDataProp": "m111Nama",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                        var kode = $("#search_m111ID").val();

                        if (kode) {
                            aoData.push({"name": "sCriteria_m111ID", "value": kode});
                        }
                        var nama = $("#search_m111Nama").val();

                        if (nama) {
                            aoData.push({"name": "sCriteria_m111Nama", "value": nama});
                        }

                    $.ajax({ "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData ,
                        "success": function (json) {
                            fnCallback(json);
                           },
                        "complete": function () {
                           }
                    });
        }
});

});
</g:javascript>



