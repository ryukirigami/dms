
<%@ page import="com.kombos.customerprofile.HistoryCustomer" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>


<table id="historyCustomer_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>


        <th style="border-bottom: none;padding: 5px;width: 40%;">
            <div><g:message code="historyCustomer.fullNama.label" default="fullNama" /></div>
        </th>


        %{--<th style="border-bottom: none;padding: 5px;">--}%
            %{--<div><g:message code="historyCustomer.t182NamaBelakang.label" default="T182 Nama Belakang" /></div>--}%
        %{--</th>--}%


        <th style="border-bottom: none;padding: 5px;width: 40%;">
            <div><g:message code="historyCustomer.t182Alamat.label" default="T182 Alamat" /></div>
        </th>

        %{--/Pekerjaan/--}%
            %{--<th style="border-bottom: none;padding: 5px;">--}%
                %{--<div><g:message code="historyCustomer.t182Pekerjaan.label" default="Pekerjaan" /></div>--}%
            %{--</th>--}%
            %{--//--}%

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="historyCustomer.t182NoTelpRumah.label" default="T182 No Telp Rumah" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="historyCustomer.t182NoTelpKantor.label" default="T182 No Telp Kantor" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="historyCustomer.t182NoHp.label" default="T182 No Hp" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="historyCustomer.company.label" default="Company" /></div>
        </th>

		</tr>
		<tr>

            %{--<th style="border-top: none;padding: 5px;">--}%
                %{--<div id="filter_t182NamaDepan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
                    %{--<input type="text" name="search_t182NamaDepan" class="search_init" />--}%
                %{--</div>--}%
            %{--</th>--}%
            <th style="border-top: none;padding: 5px;width: 40%;">
                <div id="filter_fullNama" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_fullNama" class="search_init" />
                </div>
            </th>


            %{--<th style="border-top: none;padding: 5px;">--}%
                %{--<div id="filter_t182NamaBelakang" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
                    %{--<input type="text" name="search_t182NamaBelakang" class="search_init" />--}%
                %{--</div>--}%
            %{--</th>--}%


            <th style="border-top: none;padding: 5px;width: 40%;">
                <div id="filter_t182Alamat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t182Alamat" class="search_init" />
                </div>
            </th>


            %{--/Pekerjaan/--}%
            %{--<th style="border-top: none;padding: 5px;">--}%
                %{--<div id="filter_t182Pekerjaan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
                    %{--<input type="text" name="search_t182Pekerjaan" class="search_init" />--}%
                %{--</div>--}%
            %{--</th>--}%
            %{--//--}%

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t182NoTelpRumah" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" onkeyup="checkNumber(this);" name="search_t182NoTelpRumah" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t182NoTelpKantor" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" onkeyup="checkNumber(this);" name="search_t182NoTelpKantor" class="search_init" />
                </div>
            </th>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_t182NoHp" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" onkeyup="checkNumber(this);" name="search_t182NoHp" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_company" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_company" class="search_init" />
                </div>
            </th>

		</tr>
	</thead>
</table>

<g:javascript>
var historyCustomerTable;
var reloadHistoryCustomerTable;
$(function(){
	
	reloadHistoryCustomerTable = function() {
		historyCustomerTable.fnDraw();
	}

	var recordsHistoryCustomerPerPage = [];//new Array();
    var anHistoryCustomerSelected;
    var jmlRecHistoryCustomerPerPage=0;
    var id;

	
	$('#search_t182TglLahir').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t182TglLahir_day').val(newDate.getDate());
			$('#search_t182TglLahir_month').val(newDate.getMonth()+1);
			$('#search_t182TglLahir_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			historyCustomerTable.fnDraw();	
	});

	

	$('#search_t182TglTransaksi').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t182TglTransaksi_day').val(newDate.getDate());
			$('#search_t182TglTransaksi_month').val(newDate.getMonth()+1);
			$('#search_t182TglTransaksi_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			historyCustomerTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	historyCustomerTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	historyCustomerTable = $('#historyCustomer_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsHistoryCustomer = $("#historyCustomer_datatables tbody .row-select");
            var jmlHistoryCustomerCek = 0;
            var nRow;
            var idRec;
            rsHistoryCustomer.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsHistoryCustomerPerPage[idRec]=="1"){
                    jmlHistoryCustomerCek = jmlHistoryCustomerCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsHistoryCustomerPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecHistoryCustomerPerPage = rsHistoryCustomer.length;
            if(jmlHistoryCustomerCek==jmlRecHistoryCustomerPerPage && jmlRecHistoryCustomerPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [



{

	"sName": "fullNama",
	"mDataProp": "fullNama",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true


//	"sName": "t182NamaDepan",
//	"mDataProp": "t182NamaDepan",
//	"aTargets": [0],
//	"mRender": function ( data, type, row ) {
//		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
//	},
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
}

,

//{
//	"sName": "t182NamaBelakang",
//	"mDataProp": "t182NamaBelakang",
//	"aTargets": [1],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,


{
	"sName": "t182Alamat",
	"mDataProp": "t182Alamat",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

//,
//
//{
//	"sName": "t182Pekerjaan",
//	"mDataProp": "t182Pekerjaan",
//	"aTargets": [3],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}

,


{
	"sName": "t182NoTelpRumah",
	"mDataProp": "t182NoTelpRumah",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"135px",
	"bVisible": true
}

,

{
	"sName": "t182NoTelpKantor",
	"mDataProp": "t182NoTelpKantor",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"135px",
	"bVisible": true
}

,

{
	"sName": "t182NoHp",
	"mDataProp": "t182NoHp",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"135px",
	"bVisible": true
}

,

{
	"sName": "company",
	"mDataProp": "company",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var customer = $('#filter_customer input').val();
						if(customer){
							aoData.push(
									{"name": 'sCriteria_customer', "value": customer}
							);
						}
	
						var t182ID = $('#filter_t182ID input').val();
						if(t182ID){
							aoData.push(
									{"name": 'sCriteria_t182ID', "value": t182ID}
							);
						}
	
						var company = $('#filter_company input').val();
						if(company){
							aoData.push(
									{"name": 'sCriteria_company', "value": company}
							);
						}

						var asuransi = $('#filter_asuransi input').val();
						if(asuransi){
							aoData.push(
									{"name": 'sCriteria_asuransi', "value": asuransi}
							);
						}

						var leasing = $('#filter_leasing input').val();
						if(leasing){
							aoData.push(
									{"name": 'sCriteria_leasing', "value": leasing}
							);
						}


						var fullNama = $('#filter_fullNama input').val();
						if(fullNama){
							aoData.push(
									{"name": 'sCriteria_fullNama', "value": fullNama}
							);
						}

//						var t182NamaDepan = $('#filter_t182NamaDepan input').val();
//						if(t182NamaDepan){
//							aoData.push(
//									{"name": 'sCriteria_t182NamaDepan', "value": t182NamaDepan}
//							);
//						}

//						var t182NamaBelakang = $('#filter_t182NamaBelakang input').val();
//						if(t182NamaBelakang){
//							aoData.push(
//									{"name": 'sCriteria_t182NamaBelakang', "value": t182NamaBelakang}
//							);
//						}
	
						var t182NoTelpRumah = $('#filter_t182NoTelpRumah input').val();
						if(t182NoTelpRumah){
							aoData.push(
									{"name": 'sCriteria_t182NoTelpRumah', "value": t182NoTelpRumah}
							);
						}
	
						var t182NoTelpKantor = $('#filter_t182NoTelpKantor input').val();
						if(t182NoTelpKantor){
							aoData.push(
									{"name": 'sCriteria_t182NoTelpKantor', "value": t182NoTelpKantor}
							);
						}
	
						var t182NoFax = $('#filter_t182NoFax input').val();
						if(t182NoFax){
							aoData.push(
									{"name": 'sCriteria_t182NoFax', "value": t182NoFax}
							);
						}
	
						var t182NoHp = $('#filter_t182NoHp input').val();
						if(t182NoHp){
							aoData.push(
									{"name": 'sCriteria_t182NoHp', "value": t182NoHp}
							);
						}
	
						var t182Email = $('#filter_t182Email input').val();
						if(t182Email){
							aoData.push(
									{"name": 'sCriteria_t182Email', "value": t182Email}
							);
						}
	
						var t182Alamat = $('#filter_t182Alamat input').val();
						if(t182Alamat){
							aoData.push(
									{"name": 'sCriteria_t182Alamat', "value": t182Alamat}
							);
						}
	
						var t182AlamatNPWP = $('#filter_t182AlamatNPWP input').val();
						if(t182AlamatNPWP){
							aoData.push(
									{"name": 'sCriteria_t182AlamatNPWP', "value": t182AlamatNPWP}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
$('.select-all').click(function(e) {

        $("#historyCustomer_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsHistoryCustomerPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsHistoryCustomerPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#historyCustomer_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsHistoryCustomerPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anHistoryCustomerSelected = historyCustomerTable.$('tr.row_selected');
            if(jmlRecHistoryCustomerPerPage == anHistoryCustomerSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsHistoryCustomerPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
