<%@ page import="org.apache.commons.codec.binary.Base64; com.kombos.customerprofile.HistoryCustomer" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="bootstrapeditable"/>
    <g:set var="entityName"
           value="${message(code: 'historyCustomer.label', default: 'History Customer')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <g:javascript disposition="head">
var deleteHistoryCustomer;

$(function(){ 
	deleteHistoryCustomer=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/historyCustomer/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadHistoryCustomerTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

    </g:javascript>
</head>


%{--<body>--}%

%{--<div id="main_tabs">--}%
%{--<div id="main_tabs-1">--}%

%{--<div id="show-historyCustomer" role="main">--}%
%{--<legend>--}%
    %{--<g:message code="default.show.label" args="[entityName]"/>--}%
%{--</legend>--}%
%{--<g:if test="${flash.message}">--}%
    %{--<div class="message" role="status">${flash.message}</div>--}%
%{--</g:if>--}%
%{--<table id="historyCustomer"--}%
       %{--class="table table-bordered table-hover">--}%
%{--<tbody>--}%

%{--<g:if test="${historyCustomerInstance?.t182ID}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="t182ID-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.t182ID.label" default="T182 ID"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t182ID-label">--}%
            %{--<g:fieldValue bean="${historyCustomerInstance}" field="t182ID"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.company}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="company-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.company.label" default="Company"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="company-label">--}%
            %{--<g:link controller="company" action="show"--}%
                    %{--id="${historyCustomerInstance?.company?.id}">${historyCustomerInstance?.company?.encodeAsHTML()}</g:link>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.peranCustomer}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="peranCustomer-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.peranCustomer.label" default="Peran Customer"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="peranCustomer-label">--}%
            %{--<g:link controller="peranCustomer" action="show"--}%
                    %{--id="${historyCustomerInstance?.peranCustomer?.id}">${historyCustomerInstance?.peranCustomer?.encodeAsHTML()}</g:link>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.t182GelarD}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;">--}%
            %{--<span id="t182GelarD-label" class="property-label"><g:message code="historyCustomer.t182GelarD.label" default="T182 Gelar D"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t182GelarD-label">--}%
            %{--<g:fieldValue bean="${historyCustomerInstance}" field="t182GelarD"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.t182NamaDepan}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="t182NamaDepan-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.t182NamaDepan.label" default="T182 Nama Depan"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t182NamaDepan-label">--}%
            %{--<g:fieldValue bean="${historyCustomerInstance}" field="t182NamaDepan"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.t182NamaBelakang}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="t182NamaBelakang-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.t182NamaBelakang.label" default="T182 Nama Belakang"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t182NamaBelakang-label">--}%
            %{--<g:fieldValue bean="${historyCustomerInstance}" field="t182NamaBelakang"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.t182GelarB}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="t182GelarB-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.t182GelarB.label" default="T182 Gelar B"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t182GelarB-label">--}%
            %{--<g:fieldValue bean="${historyCustomerInstance}" field="t182GelarB"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.t182JenisKelamin}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="t182JenisKelamin-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.t182JenisKelamin.label" default="T182 Jenis Kelamin"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t182JenisKelamin-label">--}%
            %{--<g:if test="${historyCustomerInstance?.t182JenisKelamin.equals("1")}">--}%
                %{--Laki-laki--}%
            %{--</g:if>--}%
            %{--<g:else>--}%
                %{--Perempuan--}%
            %{--</g:else>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.t182TglLahir}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="t182TglLahir-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.t182TglLahir.label" default="T182 Tgl Lahir"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t182TglLahir-label">--}%
            %{--<g:formatDate date="${historyCustomerInstance?.t182TglLahir}"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.jenisCustomer}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="jenisCustomer-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.jenisCustomer.label" default="Jenis Customer"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="jenisCustomer-label">--}%
            %{--<g:fieldValue bean="${historyCustomerInstance}" field="jenisCustomer"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.t182NPWP}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="t182NPWP-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.t182NPWP.label" default="T182 NPWP"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t182NPWP-label">--}%
            %{--<g:fieldValue bean="${historyCustomerInstance}" field="t182NPWP"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.t182NoFakturPajakStd}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="t182NoFakturPajakStd-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.t182NoFakturPajakStd.label" default="T182 No Faktur Pajak Std"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t182NoFakturPajakStd-label">--}%
            %{--<g:fieldValue bean="${historyCustomerInstance}" field="t182NoFakturPajakStd"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.jenisIdCard}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="jenisIdCard-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.jenisIdCard.label" default="Jenis Id Card"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="jenisIdCard-label">--}%
            %{--<g:link controller="jenisIdCard" action="show"--}%
                    %{--id="${historyCustomerInstance?.jenisIdCard?.id}">${historyCustomerInstance?.jenisIdCard?.encodeAsHTML()}</g:link>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.t182NomorIDCard}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="t182NomorIDCard-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.t182NomorIDCard.label" default="T182 Nomor IDC ard"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t182NomorIDCard-label">--}%
            %{--<g:fieldValue bean="${historyCustomerInstance}" field="t182NomorIDCard"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.agama}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="agama-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.agama.label" default="Agama"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="agama-label">--}%
            %{--<g:link controller="agama" action="show"--}%
                    %{--id="${historyCustomerInstance?.agama?.id}">${historyCustomerInstance?.agama?.encodeAsHTML()}</g:link>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.nikah}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="nikah-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.nikah.label" default="Nikah"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="nikah-label">--}%
            %{--<g:link controller="nikah" action="show"--}%
                    %{--id="${historyCustomerInstance?.nikah?.id}">${historyCustomerInstance?.nikah?.encodeAsHTML()}</g:link>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.t182NoTelpRumah}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="t182NoTelpRumah-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.t182NoTelpRumah.label" default="T182 No Telp Rumah"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t182NoTelpRumah-label">--}%
            %{--<g:fieldValue bean="${historyCustomerInstance}" field="t182NoTelpRumah"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.t182NoTelpKantor}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="t182NoTelpKantor-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.t182NoTelpKantor.label" default="T182 No Telp Kantor"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t182NoTelpKantor-label">--}%
            %{--<g:fieldValue bean="${historyCustomerInstance}" field="t182NoTelpKantor"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.t182NoFax}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="t182NoFax-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.t182NoFax.label" default="T182 No Fax"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t182NoFax-label">--}%
            %{--<g:fieldValue bean="${historyCustomerInstance}" field="t182NoFax"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.t182NoHp}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="t182NoHp-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.t182NoHp.label" default="T182 No Hp"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t182NoHp-label">--}%
            %{--<g:fieldValue bean="${historyCustomerInstance}" field="t182NoHp"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.t182Email}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="t182Email-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.t182Email.label" default="T182 Email"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t182Email-label">--}%
            %{--<g:fieldValue bean="${historyCustomerInstance}" field="t182Email"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.t182StaTerimaMRS}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="t182StaTerimaMRS-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.t182StaTerimaMRS.label" default="T182 Sta Terima MRS"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t182StaTerimaMRS-label">--}%
            %{--<g:if test="${historyCustomerInstance?.t182StaTerimaMRS.equals('1')}">Ya</g:if>--}%
            %{--<g:else>Tidak</g:else>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.t182Alamat}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="t182Alamat-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.t182Alamat.label" default="T182 Alamat"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t182Alamat-label">--}%
            %{--<g:fieldValue bean="${historyCustomerInstance}" field="t182Alamat"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.t182RT}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="t182RT-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.t182RT.label" default="T182 RT"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t182RT-label">--}%
            %{--<g:fieldValue bean="${historyCustomerInstance}" field="t182RT"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.t182RW}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="t182RW-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.t182RW.label" default="T182 RW"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t182RW-label">--}%
            %{--<g:fieldValue bean="${historyCustomerInstance}" field="t182RW"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.kelurahan}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="kelurahan-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.kelurahan.label" default="Kelurahan"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="kelurahan-label">--}%
            %{--<g:link controller="kelurahan" action="show"--}%
                    %{--id="${historyCustomerInstance?.kelurahan?.id}">${historyCustomerInstance?.kelurahan?.encodeAsHTML()}</g:link>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.kecamatan}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="kecamatan-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.kecamatan.label" default="Kecamatan"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="kecamatan-label">--}%
            %{--<g:link controller="kecamatan" action="show"--}%
                    %{--id="${historyCustomerInstance?.kecamatan?.id}">${historyCustomerInstance?.kecamatan?.encodeAsHTML()}</g:link>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.kabKota}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="kabKota-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.kabKota.label" default="Kab Kota"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="kabKota-label">--}%
            %{--<g:link controller="kabKota" action="show"--}%
                    %{--id="${historyCustomerInstance?.kabKota?.id}">${historyCustomerInstance?.kabKota?.encodeAsHTML()}</g:link>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.provinsi}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="provinsi-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.provinsi.label" default="Provinsi"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="provinsi-label">--}%
            %{--<g:link controller="provinsi" action="show"--}%
                    %{--id="${historyCustomerInstance?.provinsi?.id}">${historyCustomerInstance?.provinsi?.encodeAsHTML()}</g:link>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.t182KodePos}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="t182KodePos-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.t182KodePos.label" default="T182 Kode Pos"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t182KodePos-label">--}%
            %{--<g:fieldValue bean="${historyCustomerInstance}" field="t182KodePos"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.t182Foto}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="t182Foto-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.t182Foto.label" default="T182 Foto"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t182Foto-label">--}%
            %{--<img width="200px" src="data:jpg;base64,${new String(new Base64().encode(historyCustomerInstance.t182Foto), "UTF-8")}"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.t182AlamatNPWP}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="t182AlamatNPWP-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.t182AlamatNPWP.label" default="T182 Alamat NPWP"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t182AlamatNPWP-label">--}%
            %{--<g:fieldValue bean="${historyCustomerInstance}" field="t182AlamatNPWP"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.t182RTNPWP}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="t182RTNPWP-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.t182RTNPWP.label" default="T182 RTNPWP"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t182RTNPWP-label">--}%
            %{--<g:fieldValue bean="${historyCustomerInstance}" field="t182RTNPWP"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.t182RWNPWP}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="t182RWNPWP-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.t182RWNPWP.label" default="T182 RWNPWP"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t182RWNPWP-label">--}%
            %{--<g:fieldValue bean="${historyCustomerInstance}" field="t182RWNPWP"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.kelurahan2}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="kelurahan2-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.kelurahan2.label" default="Kelurahan2"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="kelurahan2-label">--}%
            %{--<g:link controller="kelurahan" action="show"--}%
                    %{--id="${historyCustomerInstance?.kelurahan2?.id}">${historyCustomerInstance?.kelurahan2?.encodeAsHTML()}</g:link>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.kecamatan2}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="kecamatan2-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.kecamatan2.label" default="Kecamatan2"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="kecamatan2-label">--}%
            %{--<g:link controller="kecamatan" action="show"--}%
                    %{--id="${historyCustomerInstance?.kecamatan2?.id}">${historyCustomerInstance?.kecamatan2?.encodeAsHTML()}</g:link>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.kabKota2}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="kabKota2-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.kabKota2.label" default="Kab Kota2"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="kabKota2-label">--}%
            %{--<g:link controller="kabKota" action="show"--}%
                    %{--id="${historyCustomerInstance?.kabKota2?.id}">${historyCustomerInstance?.kabKota2?.encodeAsHTML()}</g:link>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.provinsi2}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="provinsi2-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.provinsi2.label" default="Provinsi2"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="provinsi2-label">--}%
            %{--<g:link controller="provinsi" action="show"--}%
                    %{--id="${historyCustomerInstance?.provinsi2?.id}">${historyCustomerInstance?.provinsi2?.encodeAsHTML()}</g:link>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.t182KodePosNPWP}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="t182KodePosNPWP-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.t182KodePosNPWP.label" default="T182 Kode Pos NPWP"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t182KodePosNPWP-label">--}%
            %{--<g:fieldValue bean="${historyCustomerInstance}" field="t182KodePosNPWP"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%


%{--<g:if test="${historyCustomerInstance?.t182WebUserName}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="t182WebUserName-label" class="property-label"><g:message--}%
                    %{--code="historyCustomer.t182WebUserName.label" default="T182 Web User Name"/>:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="t182WebUserName-label">--}%
            %{--<g:fieldValue bean="${historyCustomerInstance}" field="t182WebUserName"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.customer?.lastUpdProcess}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="lastUpdProcess-label" class="property-label"><g:message--}%
                    %{--code="agama.lastUpdProcess.label" default="Last Upd Process" />:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="lastUpdProcess-label">--}%
            %{--<g:fieldValue bean="${historyCustomerInstance?.customer}" field="lastUpdProcess"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.customer?.dateCreated}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="dateCreated-label" class="property-label"><g:message--}%
                    %{--code="agama.dateCreated.label" default="Date Created" />:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="dateCreated-label">--}%
            %{--<g:formatDate date="${historyCustomerInstance?.customer?.dateCreated}" type="datetime" style="MEDIUM" />--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.customer?.createdBy}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="createdBy-label" class="property-label"><g:message--}%
                    %{--code="agama.createdBy.label" default="Created By" />:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="createdBy-label">--}%
            %{--<g:fieldValue bean="${historyCustomerInstance?.customer}" field="createdBy"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.customer?.lastUpdated}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="lastUpdated-label" class="property-label"><g:message--}%
                    %{--code="agama.lastUpdated.label" default="Last Updated" />:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="lastUpdated-label">--}%
            %{--<g:formatDate date="${historyCustomerInstance?.customer?.lastUpdated}" type="datetime" style="MEDIUM" />--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--<g:if test="${historyCustomerInstance?.customer?.updatedBy}">--}%
    %{--<tr>--}%
        %{--<td class="span2" style="text-align: right;"><span--}%
                %{--id="updatedBy-label" class="property-label"><g:message--}%
                    %{--code="agama.updatedBy.label" default="Updated By" />:</span></td>--}%
        %{--<td class="span3"><span class="property-value"--}%
                                %{--aria-labelledby="updatedBy-label">--}%
            %{--<g:fieldValue bean="${historyCustomerInstance?.customer}" field="updatedBy"/>--}%
        %{--</span></td>--}%
    %{--</tr>--}%
%{--</g:if>--}%

%{--</tbody>--}%
%{--</table>--}%

%{--</div>--}%

%{--</div>--}%
%{--</div>--}%

%{--<script>--}%
    %{--$(function () {--}%
        %{--$("#main_tabs").tabs();--}%
    %{--});--}%
%{--</script>--}%

%{--</body>--}%
<script>
    $(function () {

        $("#profile_tabs").tabs();
        $("#alamat_tabs").tabs();
        $("#account_web").tabs();
        $("#foto_profile_tabs").tabs();
        $("#customer_cars_tabs").tabs();
        $("#kontak_profile_tabs").tabs();
        $("#identitas_profile_tabs").tabs();
        $("#lain_profile_tabs").tabs();
        $("#tpss_survey_tab").tabs();
        $("#history_retention_tabs").tabs();
        $("#history_kepemilikan_mobil_tabs").tabs();
        $("#customer_list_tabs").tabs();
    });
</script>
<table style="width: 100%;padding-left: 5px;padding-right: 5px;">
<tr style="vertical-align: top;">
<td style="width: 33%;padding:5px;">
<div id="profile_tabs">
    <legend style="font-size: small">Profile</legend>
    <div id="profile_tabs-1">

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182ID', 'error')} ">
            <label class="control-label" for="t182ID">
                <g:message code="historyCustomer.t182ID.label" default="Kode Customer"/>&nbsp;:&nbsp;
                <g:fieldValue bean="${historyCustomerInstance}" field="t182ID"/>
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182GelarD', 'error')} ">
            <label class="control-label" for="t182GelarD">
                <g:message code="historyCustomer.t182GelarD.label" default="Gelar di depan"/>&nbsp;:&nbsp;
                <g:fieldValue bean="${historyCustomerInstance}" field="t182GelarD"/>
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NamaDepan', 'error')} ">
            <label class="control-label" for="t182NamaDepan">
                <g:message code="historyCustomer.t182NamaDepan.label" default="Nama Depan"/>&nbsp;:&nbsp;
                <g:fieldValue bean="${historyCustomerInstance}" field="t182NamaDepan"/>
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NamaBelakang', 'error')} ">
            <label class="control-label" for="t182NamaBelakang">
                <g:message code="historyCustomer.t182NamaBelakang.label" default="Nama Belakang"/>&nbsp;:&nbsp;
                <g:fieldValue bean="${historyCustomerInstance}" field="t182NamaBelakang"/>
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182GelarB', 'error')} ">
            <label class="control-label" for="t182GelarB">
                <g:message code="historyCustomer.t182GelarB.label" default="Gelar di belakang"/>&nbsp;:&nbsp;
                <g:fieldValue bean="${historyCustomerInstance}" field="t182GelarB"/>
            </label>
        </div>

        %{--/Pekerjaan/--}%
        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182Pekerjaan', 'error')} ">
            <label class="control-label" for="t182Pekerjaan">
                <g:message code="historyCustomer.t182Pekerjaan.label" default="Pekerjaan"/>&nbsp;:&nbsp;
                <g:fieldValue bean="${historyCustomerInstance}" field="t182Pekerjaan"/>
            </label>
        </div>
        %{--//--}%

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182JenisKelamin', 'error')} ">
            <label class="control-label" for="t182JenisKelamin">
                <g:message code="historyCustomer.t182JenisKelamin.label" default="Jenis Kelamin"/>&nbsp;:&nbsp;
                <g:if test="${historyCustomerInstance?.t182JenisKelamin.equals("1")}">
                    Laki-laki
                </g:if>
                <g:else>
                    Perempuan
                </g:else>
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182TglLahir', 'error')} ">
            <label class="control-label" for="t182TglLahir">
                <g:message code="historyCustomer.t182TglLahir.label" default="Tgl. Lahir"/>&nbsp;:&nbsp;
                <g:formatDate date="${historyCustomerInstance?.t182TglLahir}"/>
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'jenisCustomer', 'error')} ">
            <label class="control-label" for="jenisCustomer">
                <g:message code="historyCustomer.jenisCustomer.label" default="Jenis Customer"/>&nbsp;:&nbsp;
                <g:fieldValue bean="${historyCustomerInstance}" field="jenisCustomer"/>
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'company', 'error')} ">
            <label class="control-label" for="company">
                <g:message code="historyCustomer.company.label" default="Company"/>&nbsp;:&nbsp;
                ${historyCustomerInstance?.company?.encodeAsHTML()}
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'peranCustomer', 'error')} ">
            <label class="control-label" for="peranCustomer">
                <g:message code="historyCustomer.peranCustomer.label" default="Peran Customer"/>&nbsp;:&nbsp;
                ${historyCustomerInstance?.peranCustomer?.encodeAsHTML()}
            </label>
        </div>
    </div>
</div>

<br>

<div id="alamat_tabs">
    <ul>
        <li><a href="#alamat_tabs-1">Alamat Korespodensi</a></li>
        <li><a href="#alamat_tabs-2">Alamat NPWP</a></li>
    </ul>

    <div id="alamat_tabs-1">
        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182Alamat', 'error')} ">
            <label class="control-label" for="t182Alamat">
                <g:message code="historyCustomer.t182Alamat.label" default="T182 Alamat"/>&nbsp;:&nbsp;
                <g:fieldValue bean="${historyCustomerInstance}" field="t182Alamat"/>
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182RT', 'error')} ">
            <label class="control-label" for="t182RT">
                <g:message code="historyCustomer.t182RT.label" default="T182 RT"/>&nbsp;:&nbsp;
                <g:fieldValue bean="${historyCustomerInstance}" field="t182RT"/>
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182RW', 'error')} ">
            <label class="control-label" for="t182RW">
                <g:message code="historyCustomer.t182RW.label" default="T182 RW"/>&nbsp;:&nbsp;
                <g:fieldValue bean="${historyCustomerInstance}" field="t182RW"/>
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'provinsi', 'error')} ">
            <label class="control-label" for="provinsi">
                <g:message code="historyCustomer.provinsi.label" default="Provinsi"/>&nbsp;:&nbsp;
                ${historyCustomerInstance?.provinsi?.encodeAsHTML()}
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'kabKota', 'error')} ">
            <label class="control-label" for="kabKota">
                <g:message code="historyCustomer.kabKota.label" default="Kab Kota"/>&nbsp;:&nbsp;
                ${historyCustomerInstance?.kabKota?.encodeAsHTML()}
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'kecamatan', 'error')} ">
            <label class="control-label" for="kecamatan">
                <g:message code="historyCustomer.kecamatan.label" default="Kecamatan"/>&nbsp;:&nbsp;
                ${historyCustomerInstance?.kecamatan?.encodeAsHTML()}
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'kelurahan', 'error')} ">
            <label class="control-label" for="kelurahan">
                <g:message code="historyCustomer.kelurahan.label" default="Kelurahan"/>&nbsp;:&nbsp;
                ${historyCustomerInstance?.kelurahan?.encodeAsHTML()}
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182KodePos', 'error')} ">
            <label class="control-label" for="t182KodePos">
                <g:message code="historyCustomer.t182KodePos.label" default="T182 Kode Pos"/>&nbsp;:&nbsp;
                <g:fieldValue bean="${historyCustomerInstance}" field="t182KodePos"/>
            </label>
        </div>
    </div>

    <div id="alamat_tabs-2">
        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182AlamatNPWP', 'error')} ">
            <label class="control-label" for="t182AlamatNPWP">
                <g:message code="historyCustomer.t182AlamatNPWP.label" default="T182 Alamat NPWP"/>&nbsp;:&nbsp;
                <g:fieldValue bean="${historyCustomerInstance}" field="t182AlamatNPWP"/>
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182RTNPWP', 'error')} ">
            <label class="control-label" for="t182RTNPWP">
                <g:message code="historyCustomer.t182RTNPWP.label" default="T182 RTNPWP"/>&nbsp;:&nbsp;
                <g:fieldValue bean="${historyCustomerInstance}" field="t182RTNPWP"/>
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182RWNPWP', 'error')} ">
            <label class="control-label" for="t182RWNPWP">
                <g:message code="historyCustomer.t182RWNPWP.label" default="T182 RWNPWP"/>&nbsp;:&nbsp;
                <g:fieldValue bean="${historyCustomerInstance}" field="t182RWNPWP"/>
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'provinsi2', 'error')} ">
            <label class="control-label" for="provinsi2">
                <g:message code="historyCustomer.provinsi2.label" default="Provinsi2"/>&nbsp;:&nbsp;
                ${historyCustomerInstance?.provinsi2?.encodeAsHTML()}
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'kabKota2', 'error')} ">
            <label class="control-label" for="kabKota2">
                <g:message code="historyCustomer.kabKota2.label" default="Kab Kota2"/>&nbsp;:&nbsp;
                ${historyCustomerInstance?.kabKota2?.encodeAsHTML()}
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'kecamatan2', 'error')} ">
            <label class="control-label" for="kecamatan2">
                <g:message code="historyCustomer.kecamatan2.label" default="Kecamatan2"/>&nbsp;:&nbsp;
                ${historyCustomerInstance?.kecamatan2?.encodeAsHTML()}
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'kelurahan2', 'error')} ">
            <label class="control-label" for="kelurahan2">
                <g:message code="historyCustomer.kelurahan2.label" default="Kelurahan2"/>&nbsp;:&nbsp;
                ${historyCustomerInstance?.kelurahan2?.encodeAsHTML()}
            </label>
        </div>


        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182KodePosNPWP', 'error')} ">
            <label class="control-label" for="t182KodePosNPWP">
                <g:message code="historyCustomer.t182KodePosNPWP.label" default="T182 Kode Pos NPWP"/>&nbsp;:&nbsp;
                <g:fieldValue bean="${historyCustomerInstance}" field="t182KodePosNPWP"/>
            </label>
        </div>
    </div>
</div>

<br>

<div id="account_web">
    <legend style="font-size: small">Account Web Hosting</legend>
    <div id="account_web_tabs-1">
        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182WebUserName', 'error')} ">
            <label class="control-label" for="t182WebUserName">
                <g:message code="historyCustomer.t182WebUserName.label" default="T182 Web User Name"/>&nbsp;:&nbsp;
                <g:fieldValue bean="${historyCustomerInstance}" field="t182WebUserName"/>
            </label>
        </div>
    </div>
</div>

</td>
<td style="width: 33%;padding:5px;">

<div id="foto_profile_tabs">
    <legend style="font-size: small">Foto</legend>

    <div id="foto_profile_tabs-1">
        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182WebUserName', 'error')} ">
            <label class="control-label" for="t182Foto">
                <g:message code="historyCustomer.t182Foto.label" default="T182 Foto"/>&nbsp;:&nbsp;
                <g:if test="${historyCustomerInstance?.t182Foto}">
                <img width="200px" src="data:jpg;base64,${new String(new Base64().encode(historyCustomerInstance.t182Foto), "UTF-8")}"/>
                </g:if>
            </label>
        </div>


    </div>
</div>


<br>

<div id="kontak_profile_tabs">
    <legend style="font-size: small">Kontak</legend>

    <div id="kontak_profile_tabs-1">

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NoTelpRumah', 'error')} ">
            <label class="control-label" for="t182NoTelpRumah">
                <g:message code="historyCustomer.t182NoTelpRumah.label" default="T182 No Telp Rumah"/>&nbsp;:&nbsp;
                <g:fieldValue bean="${historyCustomerInstance}" field="t182NoTelpRumah"/>
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NoTelpKantor', 'error')} ">
            <label class="control-label" for="t182NoTelpKantor">
                <g:message code="historyCustomer.t182NoTelpKantor.label" default="T182 No Telp Kantor"/>&nbsp;:&nbsp;
                <g:fieldValue bean="${historyCustomerInstance}" field="t182NoTelpKantor"/>
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NoFax', 'error')} ">
            <label class="control-label" for="t182NoFax">
                <g:message code="historyCustomer.t182NoFax.label" default="T182 No Fax"/>&nbsp;:&nbsp;
                <g:fieldValue bean="${historyCustomerInstance}" field="t182NoFax"/>
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NoHp', 'error')} ">
            <label class="control-label" for="t182NoHp">
                <g:message code="historyCustomer.t182NoHp.label" default="T182 No Hp"/>&nbsp;:&nbsp;
                <g:fieldValue bean="${historyCustomerInstance}" field="t182NoHp"/>
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182Email', 'error')} ">
            <label class="control-label" for="t182Email">
                <g:message code="historyCustomer.t182Email.label" default="T182 Email"/>&nbsp;:&nbsp;
                <g:fieldValue bean="${historyCustomerInstance}" field="t182Email"/>
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182StaTerimaMRS', 'error')} ">
            <label class="control-label" for="t182StaTerimaMRS">
                <g:message code="historyCustomer.t182StaTerimaMRS.label" default="T182 Sta Terima MRS"/>&nbsp;:&nbsp;
                <g:if test="${historyCustomerInstance?.t182StaTerimaMRS.equals('1')}">Ya</g:if>
                <g:else>Tidak</g:else>
            </label>
            </div>
        </div>
    </div>
</div>


<br>

<div id="identitas_profile_tabs">
    <legend style="font-size: small">Identitas</legend>
    <div id="identitas_profile_tabs-1">
        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'jenisIdCard', 'error')} ">
            <label class="control-label" for="jenisIdCard">
                <g:message code="historyCustomer.jenisIdCard.label" default="Jenis Id Card"/>&nbsp;:&nbsp;
                ${historyCustomerInstance?.jenisIdCard?.encodeAsHTML()}
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NomorIDCard', 'error')} ">
            <label class="control-label" for="t182NomorIDCard">
                <g:message code="historyCustomer.t182NomorIDCard.label" default="T182 Nomor IDC ard"/>&nbsp;:&nbsp;
                <g:fieldValue bean="${historyCustomerInstance}" field="t182NomorIDCard"/>
            </label>
        </div>


        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NPWP', 'error')} ">
            <label class="control-label" for="t182NPWP">
                <g:message code="historyCustomer.t182NPWP.label" default="T182 NPWP"/>&nbsp;:&nbsp;
                <g:fieldValue bean="${historyCustomerInstance}" field="t182NPWP"/>
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 't182NoFakturPajakStd', 'error')} ">
            <label class="control-label" for="t182NoFakturPajakStd">
                <g:message code="historyCustomer.t182NoFakturPajakStd.label" default="T182 No Faktur Pajak Std"/>&nbsp;:&nbsp;
                <g:fieldValue bean="${historyCustomerInstance}" field="t182NoFakturPajakStd"/>
            </label>
        </div>
    </div>
</div>


<br>

<div id="lain_profile_tabs">
    <legend style="font-size: small">Lain-lain</legend>
    <div id="lain_profile_tabs-1">
        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'agama', 'error')} ">
            <label class="control-label" for="agama">
                <g:message code="historyCustomer.agama.label" default="Agama"/>&nbsp;:&nbsp;
                ${historyCustomerInstance?.agama?.encodeAsHTML()}
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'nikah', 'error')} ">
            <label class="control-label" for="nikah">
                <g:message code="historyCustomer.nikah.label" default="Nikah"/>&nbsp;:&nbsp;
                ${historyCustomerInstance?.nikah?.encodeAsHTML()}
            </label>
        </div>

        <div class="control-group fieldcontain ${hasErrors(bean: historyCustomerInstance, field: 'hobbies', 'error')} ">
            <label class="control-label" for="hobbies">
                <g:message code="historyCustomer.hobbies.label" default="Hobby"/>&nbsp;:&nbsp;
                ${historyCustomerInstance?.hobbies?.encodeAsHTML()}
            </label>
        </div>
    </div>
</div>

</td>
<td style="width: 33%;padding:5px;">
    <div id="customer_cars_tabs">
        <legend style="font-size: small">Customer Cars</legend>
        <div id="customer_cars_tabs-1">
            <g:render template="customerCarsDataTables"/>
        </div>
    </div>
    <br>

    <div id="customer_list_tabs">
        <legend style="font-size: small">Customer List</legend>
        <div id="customer_list_tabs-1">
            <g:render template="customerListDataTables"/>
        </div>
    </div>
    <br>

    <div id="history_kepemilikan_mobil_tabs">
        <legend style="font-size: small">History Kepemilikan Mobil</legend>
        <div id="history_kepemilikan_mobil_tabs-1">
            <g:render template="historyKepemilikanMobilDataTables"/>
        </div>
    </div>
    <br>

    <div id="history_retention_tabs">
        <legend style="font-size: small">History Retention</legend>
        <div id="history_retention_tabs-1">
            <div id="tpss_survey_tab">
                <ul>
                    <li><a href="#tpss_survey_tab-1">TPSS Survey</a></li>
                    <li><a href="#tpss_survey_tab-2">MRS Birthday</a></li>
                    <li><a href="#tpss_survey_tab-3">MRS STNK</a></li>
                </ul>

                <div id="tpss_survey_tab-1">
                    <g:render template="surveyListDataTables"/>
                </div>

                <div id="tpss_survey_tab-2">

                </div>

                <div id="tpss_survey_tab-3">

                </div>
            </div>
        </div>
    </div>

</td>
</tr>
</table>
<g:form class="form-horizontal">
    <fieldset class="buttons controls">
        <a class="btn cancel" href="javascript:void(0);"
           onclick="expandTableLayout();"><g:message
                code="default.button.cancel.label" default="Cancel"/></a>
        <g:remoteLink class="btn btn-primary edit" action="edit"
                      id="${(historyCustomerInstance?.id == null ? -1 : historyCustomerInstance?.id)}"
                      update="[success: 'historyCustomer-form', failure: 'historyCustomer-form']"
                      on404="alert('not found');">
            <g:message code="default.button.edit.label" default="Edit"/>
        </g:remoteLink>
        <ba:confirm id="delete" class="btn cancel"
                    message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
                    onsuccess="deleteHistoryCustomer('${(historyCustomerInstance?.id == null ? -1 : historyCustomerInstance?.id)}')"
                    label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
    </fieldset>
    <br>
    <br>
    <br>
</g:form>
</html>
