
<%@ page import="com.kombos.customerprofile.HistoryCustomer" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'historyCustomer.label', default: 'HistoryCustomer')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <r:require modules="baseapplayout" />
    <link href="${request.contextPath}/custom_library/jquery-ui-1.9.2.custom/css/smoothness/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <script src="${request.contextPath}/custom_library/jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.js"></script>
    <style>
    .ui-tabs .ui-tabs-panel {
        display: block;
        border-width: 0;
        padding: 1em 1.4em;
        background: none;
        font-size: x-small;
    }

    label, input, button, select, textarea {
        font-size: 10px;
    }

    .form-horizontal .control-group {
        margin-bottom: 5px;
    }

    #spinner{
        z-index: 1000;
    }
    </style>
    <g:javascript>
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;

	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

	$('.pull-right .box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
        <g:remoteFunction action="create"
                          onLoading="jQuery('#spinner').fadeIn(1);"
                          onSuccess="loadForm(data, textStatus);"
                          onComplete="jQuery('#spinner').fadeOut();" />
        break;
    case '_DELETE_' :
        bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/historyCustomer/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/historyCustomer/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#historyCustomer-form').empty();
    	$('#historyCustomer-form').append(data);
   	}
    
    shrinkTableLayout = function(){
        $("#historyCustomer-table").hide();
        $("#historyCustomer-form").css("display","block");
   	}
   	
   	expandTableLayout = function(){
   		$("#historyCustomer-table").show();
   		$("#historyCustomer-form").css("display","none");
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#historyCustomer-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/historyCustomer/massdelete',
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadHistoryCustomerTable();
    		}
		});
		
   	}

});
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]"/></span>
    <ul class="nav pull-right">
        <li><a class="pull-right box-action" href="#"
               style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
                    class="icon-plus"></i>&nbsp;&nbsp;
        </a></li>
        <li><a class="pull-right box-action" href="#"
               style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
                    class="icon-remove"></i>&nbsp;&nbsp;
        </a></li>
        <li class="separator"></li>
    </ul>
</div>
<div class="box">
    <div class="span12" id="historyCustomer-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>

        <g:render template="dataTables" />
    </div>
    <div class="span12" id="historyCustomer-form" style="display: none;margin-left: 0;"></div>
</div>
</body>
</html>
