<%@ page import="com.kombos.customerprofile.HistoryCustomer" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'historyCustomer.label', default: 'HistoryCustomer')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>

</head>

<body>
<div id="main_tabs">
    <div id="main_tabs-1">

        <div id="create-historyCustomer" class="content scaffold-create" role="main">
        <legend><g:message code="default.create.label" args="[entityName]" /></legend>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${historyCustomerInstance}">
                <ul class="errors" role="alert">
                    <g:eachError bean="${historyCustomerInstance}" var="error">
                        <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                error="${error}"/></li>
                    </g:eachError>
                </ul>
            </g:hasErrors>
                <g:formRemote class="form-horizontal" name="create" on404="alert('not found!');"
                              onLoading="jQuery('#spinner').fadeIn(1);"
                              onComplete="setTimeout(function(){jQuery('#spinner').fadeOut();},1000);"
                          onSuccess="reloadHistoryCustomerTable();" update="historyCustomer-form"

                          url="[controller: 'historyCustomer', action: 'save']">
                <fieldset class="form">
                    <g:render template="form"/>
                </fieldset>
                <fieldset class="buttons controls">
                    <a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout();"><g:message
                            code="default.button.cancel.label" default="Cancel"/></a>
                    <g:submitButton class="btn btn-primary create" name="save" onclick="return checkPassword()"
                                    value="${message(code: 'default.button.create.label', default: 'Create')}"/>
                </fieldset>
            </g:formRemote>
            <br>
            <br>
            <br>
        </div>

    </div>
</div>

<script>
    $(function () {
        $("#main_tabs").tabs();
    });
</script>

</body>
</html>
