

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="historyCustomerVehicle_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover fixed table-selectable"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="historyCustomerVehicle.nopol.label" default="Nomor Polisi" /></div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="historyCustomerVehicle.customerVehicle.label" default="Vin Code" /></div>
        </th>

    </tr>

    <tr>
        <th style="border-top: none;padding: 5px;">
            <div id="filter_nopol" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_nopol" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_customerVehicle" style="padding-top: 0px;position:relative; margin-top: 0px;width: 105px;">
                <input type="text" name="search_customerVehicle" class="search_init" />
            </div>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
var historyCustomerVehicleTable;
var reloadHistoryCustomerVehicleTable;
$(function(){
	
	reloadHistoryCustomerVehicleTable = function() {
		historyCustomerVehicleTable.fnDraw();
        historyCustomerVehicleTable.fnReloadAjax();
	}
                 
    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	historyCustomerVehicleTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});
	historyCustomerVehicleTable = $('#historyCustomerVehicle_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "mappingVehicleDataTables")}",
		"aoColumns": [

{
	"sName": "nopol",
	"mDataProp": "nopol",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "customerVehicle",
	"mDataProp": "customerVehicle",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"170px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        var historyCustomerId = $('#historyCustomerIdTemp').val();
                        if(historyCustomerId){
                            aoData.push(
									{"name": 'historyCustomerId', "value": historyCustomerId}
						    );
                        }

						var nopol = $('#filter_nopol input').val();
						if(nopol){
							aoData.push(
									{"name": 'sCriteria_nopol', "value": nopol}
							);
						}
	
						var customerVehicle = $('#filter_customerVehicle input').val();
						if(customerVehicle){
							aoData.push(
									{"name": 'sCriteria_customerVehicle', "value": customerVehicle}
							);
						}


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>
