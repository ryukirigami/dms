
<%@ page import="com.kombos.parts.Satuan; com.kombos.parts.Goods" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="sales_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="sales.m111ID.label" default="Nama Customer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="sales.m111Nama.label" default="Alamat" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="sales.satuan.label" default="Nomor HP" /></div>
			</th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="sales.satuan.label" default="Kendaraan" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="sales.satuan.label" default="Nomor Polisi" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="sales.satuan.label" default="Tanggal Estimasi" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="sales.satuan.label" default="Total Estimasi" /></div>
            </th>
		</tr>
	</thead>
</table>

<g:javascript>
var salesTable;
var reloadSalesTable;
$(function(){
	
	reloadSalesTable = function() {
		salesTable.fnDraw();
	}

	var recordsSalesPerPage = [];
    var anSalesSelected;
    var jmlRecSalesPerPage=0;
    var id;

    $('#view').click(function(e){
        salesTable.fnDraw();
	});

	$('#clear').click(function(e){
	    $('#search_Tanggal').val('');
	    $('#search_Tanggalakhir').val('');
	    $('#kriteria').val('');
	    $('#kunci').val('');
        salesTable.fnDraw();
	});

	salesTable = $('#sales_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsSales = $("#sales_datatables tbody .row-select");
            var jmlSalesCek = 0;
            var nRow;
            var idRec;
            rsSales.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsSalesPerPage[idRec]=="1"){
                    jmlSalesCek = jmlSalesCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsSalesPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecSalesPerPage = rsSales.length;
            if(jmlSalesCek==jmlRecSalesPerPage && jmlRecSalesPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "namaCustomer",
	"mDataProp": "namaCustomer",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"> '+
		    '<input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;

	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "alamat",
	"mDataProp": "alamat",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "nomorHp",
	"mDataProp": "nomorHp",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}
,

{
	"sName": "historyCustomerVehicle",
	"mDataProp": "kendaraan",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "historyCustomerVehicle",
	"mDataProp": "noPol",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "t401TglJamCetakWO",
	"mDataProp": "tglEstimasi",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "totalDPRp",
	"mDataProp": "totalEstimasi",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
					    var Tanggal = $('#search_Tanggal').val();
						var TanggalDay = $('#search_Tanggal_day').val();
						var TanggalMonth = $('#search_Tanggal_month').val();
						var TanggalYear = $('#search_Tanggal_year').val();

	                    var Tanggalakhir = $('#search_Tanggalakhir').val();
                        var TanggalDayakhir = $('#search_Tanggalakhir_day').val();
                        var TanggalMonthakhir = $('#search_Tanggalakhir_month').val();
                        var TanggalYearakhir = $('#search_Tanggalakhir_year').val();


                        if(Tanggal){
                            aoData.push(
                                    {"name": 'sCriteria_Tanggal', "value": "date.struct"},
                                    {"name": 'sCriteria_Tanggal_dp', "value": Tanggal},
                                    {"name": 'sCriteria_Tanggal_day', "value": TanggalDay},
                                    {"name": 'sCriteria_Tanggal_month', "value": TanggalMonth},
                                    {"name": 'sCriteria_Tanggal_year', "value": TanggalYear}
                            );
                        }

                        if(Tanggalakhir){
                            aoData.push(
                                    {"name": 'sCriteria_Tanggalakhir', "value": "date.struct"},
                                    {"name": 'sCriteria_Tanggalakhir_dp', "value": Tanggalakhir},
                                    {"name": 'sCriteria_Tanggalakhir_day', "value": TanggalDayakhir},
                                    {"name": 'sCriteria_Tanggalakhir_month', "value":TanggalMonthakhir},
                                    {"name": 'sCriteria_Tanggalakhir_year', "value": TanggalYearakhir}
                            );
                        }

						var kriteria = $('#kriteria').val();
						if(kriteria){
							aoData.push(
									{"name": 'sCriteria_kriteria', "value": kriteria}
							);
						}

						var kunci = $('#kunci').val();
						if(kunci){
							aoData.push(
									{"name": 'sCriteria_kunci', "value": kunci}
							);
						}


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

	$('.select-all').click(function(e) {

        $("#sales_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsSalesPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsSalesPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#sales_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsSalesPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anSalesSelected = salesTable.$('tr.row_selected');

            if(jmlRecSalesPerPage == anSalesSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsSalesPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>