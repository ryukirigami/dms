
<%@ page import="com.kombos.parts.Goods" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'sales.label', default: 'View Sales Quotation')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/sales/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
        $('#spinner').fadeIn(1);
        window.location.replace('#/salesQuotation')
        $.ajax({url: '${request.contextPath}/salesQuotation/' + id,
            type:"GET",dataType: "html",
            complete : function (req, err) {
                $('#main-content').html(req.responseText);
                $('#spinner').fadeOut();
                loading = false;
            }
        });
            %{--shrinkTableLayout();
            window.location.replace('#/salesQuotation/' + id);
            $('#spinner').fadeIn(1);
            $.ajax({type:'POST', url:'${request.contextPath}/salesQuotation/'+id,
                success:function(data,textStatus){
                    loadForm(data, textStatus);
                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });--}%
    };
    
    loadForm = function(data, textStatus){
		$('#sales-form').empty();
    	$('#sales-form').append(data);
   	}

    shrinkTableLayout = function(){
        $("#sales-table").hide();
        $("#sales-form").css("display","block");
    }
    /*
    shrinkTableLayout = function(){
    	if($("#sales-table").hasClass("span12")){
   			$("#sales-table").toggleClass("span12 span5");
        }
        $("#sales-form").css("display","block");
   	}
   	*/
   	expandTableLayout = function(){
   		if($("#sales-table").hasClass("span5")){
   			$("#sales-table").toggleClass("span5 span12");
   		}
        $("#sales-form").css("display","none");
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#sales-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/sales/massdelete',
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadSalesTable();
    		}
		});
		
   	}

});
var checkin = $('#search_Tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
        var newDate = new Date(ev.date)
        newDate.setDate(newDate.getDate() + 1);
        checkout.setValue(newDate);
        checkin.hide();
        $('#search_Tanggalakhir')[0].focus();
    }).data('datepicker');

    var checkout = $('#search_Tanggalakhir').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="sales-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <table>
                <tr>
                    <td style="padding: 5px">
                        <label class="control-label" for="t951Tanggal">
                            <g:message code="sss.tanggalWO.label" default="Tanggal Estimasi"/>
                        </label>
                    </td>
                    <td style="padding: 5px">
                        <ba:datePicker name="search_Tanggal" id="search_Tanggal" precision="day" value="" disable="true" format="dd-MM-yyyy"/>&nbsp;s.d.&nbsp;
                        <ba:datePicker name="search_Tanggalakhir" id="search_Tanggalakhir" precision="day" value="" disable="true" format="dd-MM-yyyy"/>
                    </td>

                </tr>
                <tr>
                    <td style="padding: 5px">
                        <label class="control-label" for="t951Tanggal">
                            <g:message code="bPJobInstruction.staAmbilWO.label" default="Kriteria Pencarian"/>
                        </label>
                    </td>
                    <td style="padding: 5px">
                        <select name="kriteria" id="kriteria" style="width: 100%">
                            <option value="">Pilih</option>
                            <option value="s_customer" >Nama Customer</option>
                            <option value="s_alamat" >Alamat</option>
                            <option value="s_noHp" >Nomor Hp</option>
                            <option value="s_kendaraan" >Kendaraan</option>
                            <option value="s_nopol" >Nomor Polisi</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <label class="control-label" for="t951Tanggal">
                            <g:message code="bPJobInstruction.staProblemFinding.label" default="Kata Kunci"/>
                        </label>
                    </td>
                    <td style="padding: 5px">
                        <g:textField name="kunci" id="kunci" style="width: 97%" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3" >
                        <div class="controls" style="right: 0">
                            <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-primary" name="view" id="view" >Search</button>
                            <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-cancel clear" name="clear" id="clear" >Clear</button>
                        </div>
                    </td>
                </tr>
            </table>

			<g:render template="dataTables" />
		</div>
		<div class="span7" id="sales-form" style="display: none;"></div>
	</div>
</body>
</html>
