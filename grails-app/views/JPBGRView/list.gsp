
<%@ page import="com.kombos.administrasi.KategoriJob; com.kombos.board.JPB" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="JPB" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			var jpbView;
			$(function(){ 

            $('#search_tglView').datepicker().on('changeDate', function(ev) {
                    var newDate = new Date(ev.date);
                    $('#search_tglView_day').val(newDate.getDate());
                    $('#search_tglView_month').val(newDate.getMonth()+1);
                    $('#search_tglView_year').val(newDate.getFullYear());
                    $(this).datepicker('hide');
        //			var oTable = $('#JPBGRView_datatables').dataTable();
        //            oTable.fnReloadAjax();
            });
				jpbView = function(){
                    var oTable = $('#JPBGRView_datatables').dataTable();
                    oTable.fnReloadAjax();
                    var from = $("#search_tglView").val().split("/");
                    var theDate = new Date();
                    theDate.setFullYear(from[2], from[1] - 1, from[0]);
                    //alert("theDate=" + theDate);
                    var m_names = new Array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
                    var formattedDate =  theDate.getDate() + " " + m_names[theDate.getMonth()] + " " + theDate.getFullYear();
                    document.getElementById("lblTglView").innerHTML = formattedDate;
                }

   	            var reloadAll = function(){
                    var table = $('#JPBGRView_datatables');
                    if(table.length > 0){
                        jpbInput();
                        setTimeout(function(){reloadAll()}, 10000);
                    }
                }
//                setTimeout(function(){reloadAll()}, 10000);

});
</g:javascript>
	</head>
	<body>
    <div class="navbar box-header no-border">
   		<span class="pull-left">Job Progress Board (JPB) General Repair</span>
   	</div>

	<div class="box">

        <table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">
            <tr>
                <td style="width: 43%;vertical-align: top;">

                    <div class="box">
                        <legend style="font-size: small">View JPB GR</legend>

                        <table style="width: 100%;border: 0px">
                            <tr>
                                <td>
                                    <label for="lbl_companyDealer" style="width: 100px">
                                        Nama Workshop
                                    </label>
                                </td>
                                <td colspan="2">
                                    <div id="lbl_companyDealer" style="margin-bottom: 10px;">
                                        %{--<g:select id="input_companyDealer" disabled="" name="companyDealer.id" from="${com.kombos.administrasi.CompanyDealer.list()}" optionKey="id" value="${session?.userCompanyDealer?.id}" required="" class="many-to-one" style="margin-top: 5px"/>--}%
                                        <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                            <g:select name="companyDealer.id" id="input_companyDealer" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                                        </g:if>
                                        <g:else>
                                            <g:select name="companyDealer.id" id="input_companyDealer" readonly="" disabled="" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                                        </g:else>
                                    </div>
                                </td>
                                <td>
                                    <div class="ico-apply">&nbsp;&nbsp;&nbsp;</div>
                                </td>
                                <td>
                                    Status OK
                                </td>
                                <td>
                                    <div class="ico-refresh">&nbsp;&nbsp;&nbsp;</div>
                                </td>
                                <td>
                                    Status reschedule JPB
                                </td>
                                <td>
                                    <div class="ico-warning">&nbsp;&nbsp;&nbsp;</div>
                                </td>
                                <td>
                                    Status delay
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label for="lbl_tglView" style="width: 100px">
                                        Tanggal
                                    </label>
                                </td>
                                <td>
                                    <div id="lbl_tglView" style="margin-bottom: 10px;">
                                        <div id="filter_tglView" >
                                             <input type="hidden" name="search_tglView" value="date.struct">
                                             <input type="hidden" name="search_tglView_day" id="search_tglView_day" value="">
                                             <input type="hidden" name="search_tglView_month" id="search_tglView_month" value="">
                                             <input type="hidden" name="search_tglView_year" id="search_tglView_year" value="">
                                             <input type="text" data-date-format="dd/mm/yyyy" name="search_tglView_dp" value="" id="search_tglView" class="search_init" style="width: 120px;">
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <button class="btn btn-primary" id="buttonView" onclick="jpbView()" style="width: 120px; margin-bottom: 10px; margin-left: -50px;" >View</button>
                                </td>
                                <td>
                                    <div class="ico-cancel">&nbsp;&nbsp;&nbsp;</div>
                                </td>
                                <td>
                                    Status job stopped
                                </td>
                                <td>
                                    <div class="ico-question">&nbsp;&nbsp;&nbsp;</div>
                                </td>
                                <td>
                                    Status reschedule delivery
                                </td>
                            </tr>
                        </table>
                   </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div id="JPB-table">
                        <g:if test="${flash.message}">
                            <div class="message" role="status">
                                ${flash.message}
                            </div>
                        </g:if>

                        <g:render template="dataTables" />
                    </div>
                </td>
            </tr>
            <tr>
                <table style="max-width: 100%">
                    <tr>
                        <%
                            def cariData = KategoriJob.createCriteria().list {order("m055KategoriJob")};
                            cariData.each {
                        %>
                        <td style="padding: 10px;align-content: center">
                            <table class="table table-bordered" style="width: 10%;background-color: ${it?.m055WarnaBord}">
                                <tr><td>&nbsp;&nbsp;</td></tr>
                            </table>
                            <br>${it?.m055KategoriJob}
                        </td>
                        <%
                            }
                        %>
                        <td style="padding: 10px">
                            <table class="table table-bordered" style="width: 10%;background-color: #fabb88">
                                <tr><td>&nbsp;&nbsp;</td></tr>
                            </table>
                            <br/>Actual Time
                        </td>
                    </tr>
                </table>
            </tr>
        </table>
	</div>
</body>
</html>
