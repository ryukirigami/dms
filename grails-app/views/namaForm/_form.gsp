<%@ page import="com.kombos.administrasi.NamaForm" %>

<div class="control-group fieldcontain ${hasErrors(bean: namaFormInstance, field: 't004NamaAlias', 'error')} required">
	<label class="control-label" for="t004NamaAlias">
		<g:message code="namaForm.t004NamaAlias.label" default="Nama Alias" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t004NamaAlias" maxlength="50" required="" value="${namaFormInstance?.t004NamaAlias}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: namaFormInstance, field: 't004StaAuditTrail', 'error')} required">
	<label class="control-label" for="t004StaAuditTrail">
		<g:message code="namaForm.t004StaAuditTrail.label" default="Audit Trail?" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
    <g:radioGroup values="['1','0']" labels="['Ya','Tidak']" name="t004StaAuditTrail" value="${namaFormInstance.t004StaAuditTrail==null?'0':namaFormInstance.t004StaAuditTrail}">
        ${it.label} ${it.radio}
    </g:radioGroup>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: namaFormInstance, field: 't004StaPerformanceLog', 'error')} required">
	<label class="control-label" for="t004StaPerformanceLog">
		<g:message code="namaForm.t004StaPerformanceLog.label" default="Performance Log?" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
    <g:radioGroup values="['1','0']" labels="['Ya','Tidak']" name="t004StaPerformanceLog" value="${namaFormInstance.t004StaPerformanceLog==null?'0':namaFormInstance.t004StaPerformanceLog}">
        ${it.label} ${it.radio}
    </g:radioGroup>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: namaFormInstance, field: 't004StaFreeze', 'error')} required">
	<label class="control-label" for="t004StaFreeze">
		<g:message code="namaForm.t004StaFreeze.label" default="Freeze ketika Stock Opname?" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
    <g:radioGroup values="['1','0']" labels="['Ya','Tidak']" name="t004StaFreeze" value="${namaFormInstance.t004StaFreeze==null?'0':namaFormInstance.t004StaFreeze}">
        ${it.label} ${it.radio}
    </g:radioGroup>
    </div>
</div>

