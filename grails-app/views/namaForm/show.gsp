

<%@ page import="com.kombos.administrasi.NamaForm" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'namaForm.label', default: 'Nama Form')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteNamaForm;

$(function(){ 
	deleteNamaForm=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/namaForm/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadNamaFormTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-namaForm" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="namaForm"
			class="table table-bordered table-hover">
			<tbody>
			
				<g:if test="${namaFormInstance?.t004NamaObjek}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t004NamaObjek-label" class="property-label"><g:message
					code="namaForm.t004NamaObjek.label" default="Nama Objek" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t004NamaObjek-label">
						%{--<ba:editableValue
								bean="${namaFormInstance}" field="t004NamaObjek"
								url="${request.contextPath}/NamaForm/updatefield" type="text"
								title="Enter t004NamaObjek" onsuccess="reloadNamaFormTable();" />--}%
							
								<g:fieldValue bean="${namaFormInstance}" field="t004NamaObjek"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${namaFormInstance?.t004NamaAlias}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t004NamaAlias-label" class="property-label"><g:message
					code="namaForm.t004NamaAlias.label" default="Nama Alias" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t004NamaAlias-label">
						%{--<ba:editableValue
								bean="${namaFormInstance}" field="t004NamaAlias"
								url="${request.contextPath}/NamaForm/updatefield" type="text"
								title="Enter t004NamaAlias" onsuccess="reloadNamaFormTable();" />--}%
							
								<g:fieldValue bean="${namaFormInstance}" field="t004NamaAlias"/>

						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${namaFormInstance?.t004JenisFR}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t004JenisFR-label" class="property-label"><g:message
					code="namaForm.t004JenisFR.label" default="Form / Report" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t004JenisFR-label">
						%{--<ba:editableValue
								bean="${namaFormInstance}" field="t004JenisFR"
								url="${request.contextPath}/NamaForm/updatefield" type="text"
								title="Enter t004JenisFR" onsuccess="reloadNamaFormTable();" />--}%
							
								%{--<g:fieldValue bean="${namaFormInstance}" field="t004JenisFR"/>--}%
                        ${namaFormInstance.t004JenisFR=='F'?"Form":"Report"}
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${namaFormInstance?.t004StaAuditTrail}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t004StaAuditTrail-label" class="property-label"><g:message
					code="namaForm.t004StaAuditTrail.label" default="Audit Trail?" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t004StaAuditTrail-label">
						%{--<ba:editableValue
								bean="${namaFormInstance}" field="t004StaAuditTrail"
								url="${request.contextPath}/NamaForm/updatefield" type="text"
								title="Enter t004StaAuditTrail" onsuccess="reloadNamaFormTable();" />--}%
							
								%{--<g:fieldValue bean="${namaFormInstance}" field="t004StaAuditTrail"/>--}%
                                ${namaFormInstance.t004StaAuditTrail=='1'?"Ya":"Tidak"}
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${namaFormInstance?.t004StaPerformanceLog}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t004StaPerformanceLog-label" class="property-label"><g:message
					code="namaForm.t004StaPerformanceLog.label" default="Performance Log?" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t004StaPerformanceLog-label">
						%{--<ba:editableValue
								bean="${namaFormInstance}" field="t004StaPerformanceLog"
								url="${request.contextPath}/NamaForm/updatefield" type="text"
								title="Enter t004StaPerformanceLog" onsuccess="reloadNamaFormTable();" />--}%
							
								%{--<g:fieldValue bean="${namaFormInstance}" field="t004StaPerformanceLog"/>--}%
                        ${namaFormInstance.t004StaPerformanceLog=='1'?"Ya":"Tidak"}
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${namaFormInstance?.t004StaFreeze}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t004StaFreeze-label" class="property-label"><g:message
					code="namaForm.t004StaFreeze.label" default="Freeze Ketika Stock Opname" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t004StaFreeze-label">
						%{--<ba:editableValue
								bean="${namaFormInstance}" field="t004StaFreeze"
								url="${request.contextPath}/NamaForm/updatefield" type="text"
								title="Enter t004StaFreeze" onsuccess="reloadNamaFormTable();" />--}%
							
								%{--<g:fieldValue bean="${namaFormInstance}" field="t004StaFreeze"/>--}%
                        ${namaFormInstance.t004StaFreeze=='1'?"Ya":"Tidak"}
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${namaFormInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="namaForm.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${namaFormInstance}" field="dateCreated"
                                url="${request.contextPath}/NamaForm/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadNamaFormTable();" />--}%

                        <g:formatDate date="${namaFormInstance?.dateCreated}" format="dd MMMM yyyy , hh:mm" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${namaFormInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="namaForm.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${namaFormInstance}" field="createdBy"
								url="${request.contextPath}/NamaForm/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadNamaFormTable();" />--}%
							
								<g:fieldValue bean="${namaFormInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
            <g:if test="${namaFormInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="namaForm.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${namaFormInstance}" field="lastUpdated"
                                url="${request.contextPath}/NamaForm/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadNamaFormTable();" />--}%

                        <g:formatDate date="${namaFormInstance?.lastUpdated}" format="dd MMMM yyyy , hh:mm"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${namaFormInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="namaForm.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${namaFormInstance}" field="updatedBy"
								url="${request.contextPath}/NamaForm/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadNamaFormTable();" />--}%
							
								<g:fieldValue bean="${namaFormInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${namaFormInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="namaForm.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${namaFormInstance}" field="lastUpdProcess"
								url="${request.contextPath}/NamaForm/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadNamaFormTable();" />--}%

								<g:fieldValue bean="${namaFormInstance}" field="lastUpdProcess"/>

						</span></td>

				</tr>
				</g:if>



			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${namaFormInstance?.id}"
					update="[success:'namaForm-form',failure:'namaForm-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
