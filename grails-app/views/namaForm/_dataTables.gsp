
<%@ page import="com.kombos.administrasi.NamaForm" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="namaForm_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="namaForm.t004NamaObjek.label" default="Nama Objek" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="namaForm.t004NamaAlias.label" default="Nama Alias" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="namaForm.t004JenisFR.label" default="Form / Report" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="namaForm.t004StaAuditTrail.label" default="Audit Trail" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="namaForm.t004StaPerformanceLog.label" default="Performance Log" /></div>
			</th>

		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t004NamaObjek" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t004NamaObjek" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t004NamaAlias" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t004NamaAlias" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t004JenisFR" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<select name="search_t004JenisFR" onchange="reloadNamaFormTable();">
                        <option value=""></option>
                        <option value="F"> Form</option>
                        <option value="R"> Report</option>
					</select>
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t004StaAuditTrail" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <select name="search_t004StaAuditTrail" onchange="reloadNamaFormTable();">
                        <option value=""></option>
                        <option value="1"> Ya</option>
                        <option value="0"> Tidak</option>
                    </select>
                </div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t004StaPerformanceLog" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <select name="search_t004StaPerformanceLog" onchange="reloadNamaFormTable();">
                        <option value=""></option>
                        <option value="1"> Ya</option>
                        <option value="0"> Tidak</option>
                    </select>
				</div>
			</th>
	
		</tr>
	</thead>
</table>

<g:javascript>
var namaFormTable;
var reloadNamaFormTable;
$(function(){
	
	reloadNamaFormTable = function() {
		namaFormTable.fnDraw();
	}

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	namaFormTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	namaFormTable = $('#namaForm_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "t004NamaObjek",
	"mDataProp": "t004NamaObjek",
	"aTargets": [1],
		"mRender": function ( data, type, row ) {
		return '<input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t004NamaAlias",
	"mDataProp": "t004NamaAlias",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t004JenisFR",
	"mDataProp": "t004JenisFR",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t004StaAuditTrail",
	"mDataProp": "t004StaAuditTrail",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t004StaPerformanceLog",
	"mDataProp": "t004StaPerformanceLog",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var t004NamaObjek = $('#filter_t004NamaObjek input').val();
						if(t004NamaObjek){
							aoData.push(
									{"name": 'sCriteria_t004NamaObjek', "value": t004NamaObjek}
							);
						}
	
						var t004NamaAlias = $('#filter_t004NamaAlias input').val();
						if(t004NamaAlias){
							aoData.push(
									{"name": 'sCriteria_t004NamaAlias', "value": t004NamaAlias}
							);
						}
	
						var t004JenisFR = $('#filter_t004JenisFR select').val();
						if(t004JenisFR){
							aoData.push(
									{"name": 'sCriteria_t004JenisFR', "value": t004JenisFR}
							);
						}
	
						var t004StaAuditTrail = $('#filter_t004StaAuditTrail select').val();
						if(t004StaAuditTrail){
							aoData.push(
									{"name": 'sCriteria_t004StaAuditTrail', "value": t004StaAuditTrail}
							);
						}
	
						var t004StaPerformanceLog = $('#filter_t004StaPerformanceLog select').val();
						if(t004StaPerformanceLog){
							aoData.push(
									{"name": 'sCriteria_t004StaPerformanceLog', "value": t004StaPerformanceLog}
							);
						}
	
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
