<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="dcv_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
			<th style="border-bottom: none;padding: 5px;">
				<div>VinCode</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Nomor Polisi</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Model</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Nama STNK</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Tanggal DEC</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Tanggal Awal</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Tanggal Akhir</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Diskon Part(%)</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Diskon Jasa(%)</div>
			</th>
		</tr>
	</thead>
</table>

<g:javascript>
var dcvTable;
var reloadDcvTable;
$(function(){
	
	reloadDcvTable = function() {
		dcvTable.fnDraw();
	}

	dcvTable = $('#dcv_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "dcvDatatablesList")}",
		"aoColumns": [

{
	"sName": "vinCode",
	"mDataProp": "vinCode",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data+'';
	},
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
},
{
	"sName": "nomorPolisi",
	"mDataProp": "nomorPolisi",
	"aTargets": [1],
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "model",
	"mDataProp": "model",
	"aTargets": [2],
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "namaSTNK",
	"mDataProp": "namaSTNK",
	"aTargets": [3],
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "tglDEC",
	"mDataProp": "tglDEC",
	"aTargets": [4],
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "tglAwal",
	"mDataProp": "tglAwal",
	"aTargets": [5],
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "tglAkhir",
	"mDataProp": "tglAkhir",
	"aTargets": [6],
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "diskonPart",
	"mDataProp": "diskonPart",
	"aTargets": [7],
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "diskonJasa",
	"mDataProp": "diskonJasa",
	"aTargets": [8],
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						var criterias = $('#custom-criteria div').length;
						
						var c = 0;
				    	for (var i=0; i < criterias; i++) {
							if($('#criteriaValue'+i).val()){
								aoData.push( 
									{"name": 'sCriteriaField'+i, "value": $('#criteriaField'+i).val()},
									{"name": 'sCriteriaOperator'+i, "value":  $('#criteriaOperator'+i).val()},
									{"name": 'sCriteriaValue'+i, "value": $('#criteriaValue'+i).val()}
									);
									c++;
							} else if($('#criteriaValueFrom'+i).val() && $('#criteriaValueTo'+i).val()){
								
								aoData.push( 
									{"name": 'sCriteriaField'+i, "value": $('#criteriaField'+i).val()},
									{"name": 'sCriteriaOperator'+i, "value":  $('#criteriaOperator'+i).val()},
									{"name": 'sCriteriaValueFrom'+i, "value": $('#criteriaValueFrom'+i).val()},
									{"name": 'sCriteriaValueTo'+i, "value": $('#criteriaValueTo'+i).val()}
									);
									c++;
							}
						}
						aoData.push({ "name": 'sCriterias', "value": c });
								
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
