
<%@ page import="com.kombos.administrasi.GeneralParameter" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="generalParameter_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.companyDealer.label" default="Company Dealer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.createdBy.label" default="Created By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000Aging11.label" default="M000 Aging11" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000Aging12.label" default="M000 Aging12" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000Aging21.label" default="M000 Aging21" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000Aging22.label" default="M000 Aging22" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000Aging31.label" default="M000 Aging31" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000Aging32.label" default="M000 Aging32" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000Aging41.label" default="M000 Aging41" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000Aging42.label" default="M000 Aging42" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000Aging51.label" default="M000 Aging51" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000Aging52.label" default="M000 Aging52" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000AlamatFTP.label" default="M000 Alamat FTP" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000AlamatPWC.label" default="M000 Alamat PWC" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000AlamatTWC.label" default="M000 Alamat TWC" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000BufferDeliveryTimeBP.label" default="M000 Buffer Delivery Time BP" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000BufferDeliveryTimeGR.label" default="M000 Buffer Delivery Time GR" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000BulanWarningSertifikat.label" default="M000 Bulan Warning Sertifikat" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000DefaultMasaBerlakuAccount.label" default="M000 Default Masa Berlaku Account" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000DefaultPUK.label" default="M000 Default PUK" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000DefaultUserPass.label" default="M000 Default User Pass" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000DisableConcurrentLogin.label" default="M000 Disable Concurrent Login" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000FormatEmail.label" default="M000 Format Email" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000FormatSMSHMinus.label" default="M000 Format SMSHM inus" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000FormatSMSJMinus.label" default="M000 Format SMSJM inus" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000FormatSMSNotifikasi.label" default="M000 Format SMSN otifikasi" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000FormatSMSService.label" default="M000 Format SMSS ervice" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000HMinParameterReserved.label" default="M000 HM in Parameter Reserved" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000HMinusCekDokAsuransi.label" default="M000 HM inus Cek Dok Asuransi" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000HMinusFee.label" default="M000 HM inus Fee" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000HMinusReminderEmail.label" default="M000 HM inus Reminder Email" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000HMinusReminderSMS.label" default="M000 HM inus Reminder SMS" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000HMinusReminderSTNK.label" default="M000 HM inus Reminder STNK" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000HMinusSMSAppointment.label" default="M000 HM inus SMSA ppointment" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000HMinusTglReminderDM.label" default="M000 HM inus Tgl Reminder DM" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000HPlusCancelBookingFee.label" default="M000 HP lus Cancel Booking Fee" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000HPlusUndanganFA.label" default="M000 HP lus Undangan FA" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000InvoiceFooter.label" default="M000 Invoice Footer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000JMinusSMSAppointment.label" default="M000 JM inus SMSA ppointment" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000JPlusSMSTerlambat.label" default="M000 JP lus SMST erlambat" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000JamKirimEmail.label" default="M000 Jam Kirim Email" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000JamKirimSMS.label" default="M000 Jam Kirim SMS" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000JamKirimSMSSTNK.label" default="M000 Jam Kirim SMSSTNK" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000JmlMaksimalKesalahan.label" default="M000 Jml Maksimal Kesalahan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000JmlMaxInvitation.label" default="M000 Jml Max Invitation" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000JmlMaxItemWAC.label" default="M000 Jml Max Item WAC" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000JmlMaxKirimSMSFollowUp.label" default="M000 Jml Max Kirim SMSF ollow Up" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000JmlSKipCustomer.label" default="M000 Jml SK ip Customer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000JmlToleransiAsuransiBP.label" default="M000 Jml Toleransi Asuransi BP" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000KonfirmasiHariSebelum.label" default="M000 Konfirmasi Hari Sebelum" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000KonfirmasiJamSebelum.label" default="M000 Konfirmasi Jam Sebelum" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000KonfirmasiTerlambat.label" default="M000 Konfirmasi Terlambat" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000LamaCuciMobil.label" default="M000 Lama Cuci Mobil" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000LamaWaktuMinCuci.label" default="M000 Lama Waktu Min Cuci" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000MaksBayarCashRefund.label" default="M000 Maks Bayar Cash Refund" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000MaxPasswordExpired.label" default="M000 Max Password Expired" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000MaxPersenKembaliBookingFee.label" default="M000 Max Persen Kembali Booking Fee" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000MaxRecordPerPage.label" default="M000 Max Record Per Page" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000MinLastPassTdkBolehDigunakan.label" default="M000 Min Last Pass Tdk Boleh Digunakan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000NPWPPWC.label" default="M000 NPWPPWC" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000NPWPTWC.label" default="M000 NPWPTWC" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000NamaHostOrIPAddress.label" default="M000 Nama Host Or IPA ddress" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000NamaPWC.label" default="M000 Nama PWC" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000NamaTWC.label" default="M000 Nama TWC" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000NominalMaterai.label" default="M000 Nominal Materai" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000Password.label" default="M000 Password" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000PenguncianPassword.label" default="M000 Penguncian Password" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000PersenKenaikanJobSublet.label" default="M000 Persen Kenaikan Job Sublet" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000PersenMaxGunakanMaterial.label" default="M000 Persen Max Gunakan Material" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000PersenPPN.label" default="M000 Persen PPN" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000PersenPPh.label" default="M000 Persen PP h" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000PersenPainting.label" default="M000 Persen Painting" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000PersenPelepasanPartBP.label" default="M000 Persen Pelepasan Part BP" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000PersenPemasanganPartBP.label" default="M000 Persen Pemasangan Part BP" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000PersenPemborong.label" default="M000 Persen Pemborong" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000PersenPoleshing.label" default="M000 Persen Poleshing" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000PersenPreparation.label" default="M000 Persen Preparation" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000PersenWorkshop.label" default="M000 Persen Workshop" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000Person1.label" default="M000 Person1" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000Person2.label" default="M000 Person2" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000Person3.label" default="M000 Person3" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000Person4.label" default="M000 Person4" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000Person5.label" default="M000 Person5" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000Port.label" default="M000 Port" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000PreExpiredAlert.label" default="M000 Pre Expired Alert" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000PreparationTime.label" default="M000 Preparation Time" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000ProgressDisc.label" default="M000 Progress Disc" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000ReminderDMKm1.label" default="M000 Reminder DMK m1" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000ReminderDMKm2.label" default="M000 Reminder DMK m2" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000ReminderDMOp1.label" default="M000 Reminder DMO p1" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000ReminderDMOp2.label" default="M000 Reminder DMO p2" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000ReminderEmailKm1.label" default="M000 Reminder Email Km1" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000ReminderEmailKm2.label" default="M000 Reminder Email Km2" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000ReminderEmailOp1.label" default="M000 Reminder Email Op1" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000ReminderEmailOp2.label" default="M000 Reminder Email Op2" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000ReminderSMSKm1.label" default="M000 Reminder SMSK m1" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000ReminderSMSKm2.label" default="M000 Reminder SMSK m2" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000ReminderSMSOp1.label" default="M000 Reminder SMSO p1" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000ReminderSMSOp2.label" default="M000 Reminder SMSO p2" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000RunningTextInformation.label" default="M000 Running Text Information" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000SBEDisc.label" default="M000 SBED isc" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000SOHeader.label" default="M000 SOH eader" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000SettingRPP.label" default="M000 Setting RPP" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000StaAktifCall.label" default="M000 Sta Aktif Call" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000StaAktifDM.label" default="M000 Sta Aktif DM" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000StaAktifEmail.label" default="M000 Sta Aktif Email" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000StaAktifSMS.label" default="M000 Sta Aktif SMS" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000StaDel.label" default="M000 Sta Del" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000StaEnablePartTambahanWeb.label" default="M000 Sta Enable Part Tambahan Web" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000StaLihatDataBengkelLain.label" default="M000 Sta Lihat Data Bengkel Lain" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000StaSalon.label" default="M000 Sta Salon" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000StaWaktuAkhir.label" default="M000 Sta Waktu Akhir" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000Tax.label" default="M000 Tax" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000TextCatatanDM.label" default="M000 Text Catatan DM" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000TglJamWaktuAkhir.label" default="M000 Tgl Jam Waktu Akhir" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000ToleransiAmbilWO.label" default="M000 Toleransi Ambil WO" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000ToleransiIDRPerJob.label" default="M000 Toleransi IDRP er Job" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000ToleransiIDRPerProses.label" default="M000 Toleransi IDRP er Proses" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000UserName.label" default="M000 User Name" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000VersionAplikasi.label" default="M000 Version Aplikasi" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000WaktuNotifikasiTargetClockOffJob.label" default="M000 Waktu Notifikasi Target Clock Off Job" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000WaktuSessionLog.label" default="M000 Waktu Session Log" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000mMinusFee.label" default="M000m Minus Fee" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000staJenisJamKerja.label" default="M000sta Jenis Jam Kerja" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.m000staVendorCatByCc.label" default="M000sta Vendor Cat By Cc" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="generalParameter.updatedBy.label" default="Updated By" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_companyDealer" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_createdBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000Aging11" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000Aging11" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000Aging12" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000Aging12" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000Aging21" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000Aging21" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000Aging22" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000Aging22" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000Aging31" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000Aging31" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000Aging32" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000Aging32" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000Aging41" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000Aging41" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000Aging42" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000Aging42" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000Aging51" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000Aging51" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000Aging52" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000Aging52" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000AlamatFTP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000AlamatFTP" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000AlamatPWC" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000AlamatPWC" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000AlamatTWC" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000AlamatTWC" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000BufferDeliveryTimeBP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m000BufferDeliveryTimeBP" value="date.struct">
					<input type="hidden" name="search_m000BufferDeliveryTimeBP_day" id="search_m000BufferDeliveryTimeBP_day" value="">
					<input type="hidden" name="search_m000BufferDeliveryTimeBP_month" id="search_m000BufferDeliveryTimeBP_month" value="">
					<input type="hidden" name="search_m000BufferDeliveryTimeBP_year" id="search_m000BufferDeliveryTimeBP_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m000BufferDeliveryTimeBP_dp" value="" id="search_m000BufferDeliveryTimeBP" class="search_init">
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000BufferDeliveryTimeGR" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m000BufferDeliveryTimeGR" value="date.struct">
					<input type="hidden" name="search_m000BufferDeliveryTimeGR_day" id="search_m000BufferDeliveryTimeGR_day" value="">
					<input type="hidden" name="search_m000BufferDeliveryTimeGR_month" id="search_m000BufferDeliveryTimeGR_month" value="">
					<input type="hidden" name="search_m000BufferDeliveryTimeGR_year" id="search_m000BufferDeliveryTimeGR_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m000BufferDeliveryTimeGR_dp" value="" id="search_m000BufferDeliveryTimeGR" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000BulanWarningSertifikat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000BulanWarningSertifikat" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000DefaultMasaBerlakuAccount" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000DefaultMasaBerlakuAccount" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000DefaultPUK" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000DefaultPUK" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000DefaultUserPass" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000DefaultUserPass" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000DisableConcurrentLogin" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000DisableConcurrentLogin" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000FormatEmail" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000FormatEmail" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000FormatSMSHMinus" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000FormatSMSHMinus" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000FormatSMSJMinus" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000FormatSMSJMinus" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000FormatSMSNotifikasi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000FormatSMSNotifikasi" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000FormatSMSService" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000FormatSMSService" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000HMinParameterReserved" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000HMinParameterReserved" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000HMinusCekDokAsuransi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000HMinusCekDokAsuransi" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000HMinusFee" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000HMinusFee" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000HMinusReminderEmail" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000HMinusReminderEmail" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000HMinusReminderSMS" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000HMinusReminderSMS" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000HMinusReminderSTNK" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000HMinusReminderSTNK" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000HMinusSMSAppointment" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000HMinusSMSAppointment" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000HMinusTglReminderDM" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000HMinusTglReminderDM" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000HPlusCancelBookingFee" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000HPlusCancelBookingFee" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000HPlusUndanganFA" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000HPlusUndanganFA" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000InvoiceFooter" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000InvoiceFooter" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000JMinusSMSAppointment" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000JMinusSMSAppointment" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000JPlusSMSTerlambat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000JPlusSMSTerlambat" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000JamKirimEmail" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m000JamKirimEmail" value="date.struct">
					<input type="hidden" name="search_m000JamKirimEmail_day" id="search_m000JamKirimEmail_day" value="">
					<input type="hidden" name="search_m000JamKirimEmail_month" id="search_m000JamKirimEmail_month" value="">
					<input type="hidden" name="search_m000JamKirimEmail_year" id="search_m000JamKirimEmail_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m000JamKirimEmail_dp" value="" id="search_m000JamKirimEmail" class="search_init">
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000JamKirimSMS" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m000JamKirimSMS" value="date.struct">
					<input type="hidden" name="search_m000JamKirimSMS_day" id="search_m000JamKirimSMS_day" value="">
					<input type="hidden" name="search_m000JamKirimSMS_month" id="search_m000JamKirimSMS_month" value="">
					<input type="hidden" name="search_m000JamKirimSMS_year" id="search_m000JamKirimSMS_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m000JamKirimSMS_dp" value="" id="search_m000JamKirimSMS" class="search_init">
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000JamKirimSMSSTNK" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m000JamKirimSMSSTNK" value="date.struct">
					<input type="hidden" name="search_m000JamKirimSMSSTNK_day" id="search_m000JamKirimSMSSTNK_day" value="">
					<input type="hidden" name="search_m000JamKirimSMSSTNK_month" id="search_m000JamKirimSMSSTNK_month" value="">
					<input type="hidden" name="search_m000JamKirimSMSSTNK_year" id="search_m000JamKirimSMSSTNK_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m000JamKirimSMSSTNK_dp" value="" id="search_m000JamKirimSMSSTNK" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000JmlMaksimalKesalahan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000JmlMaksimalKesalahan" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000JmlMaxInvitation" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000JmlMaxInvitation" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000JmlMaxItemWAC" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000JmlMaxItemWAC" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000JmlMaxKirimSMSFollowUp" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000JmlMaxKirimSMSFollowUp" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000JmlSKipCustomer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000JmlSKipCustomer" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000JmlToleransiAsuransiBP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000JmlToleransiAsuransiBP" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000KonfirmasiHariSebelum" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000KonfirmasiHariSebelum" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000KonfirmasiJamSebelum" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000KonfirmasiJamSebelum" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000KonfirmasiTerlambat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000KonfirmasiTerlambat" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000LamaCuciMobil" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000LamaCuciMobil" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000LamaWaktuMinCuci" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000LamaWaktuMinCuci" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000MaksBayarCashRefund" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000MaksBayarCashRefund" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000MaxPasswordExpired" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000MaxPasswordExpired" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000MaxPersenKembaliBookingFee" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000MaxPersenKembaliBookingFee" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000MaxRecordPerPage" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000MaxRecordPerPage" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000MinLastPassTdkBolehDigunakan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000MinLastPassTdkBolehDigunakan" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000NPWPPWC" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000NPWPPWC" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000NPWPTWC" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000NPWPTWC" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000NamaHostOrIPAddress" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000NamaHostOrIPAddress" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000NamaPWC" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000NamaPWC" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000NamaTWC" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000NamaTWC" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000NominalMaterai" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000NominalMaterai" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000Password" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000Password" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000PenguncianPassword" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m000PenguncianPassword" value="date.struct">
					<input type="hidden" name="search_m000PenguncianPassword_day" id="search_m000PenguncianPassword_day" value="">
					<input type="hidden" name="search_m000PenguncianPassword_month" id="search_m000PenguncianPassword_month" value="">
					<input type="hidden" name="search_m000PenguncianPassword_year" id="search_m000PenguncianPassword_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m000PenguncianPassword_dp" value="" id="search_m000PenguncianPassword" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000PersenKenaikanJobSublet" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000PersenKenaikanJobSublet" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000PersenMaxGunakanMaterial" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000PersenMaxGunakanMaterial" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000PersenPPN" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000PersenPPN" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000PersenPPh" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000PersenPPh" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000PersenPainting" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000PersenPainting" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000PersenPelepasanPartBP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000PersenPelepasanPartBP" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000PersenPemasanganPartBP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000PersenPemasanganPartBP" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000PersenPemborong" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000PersenPemborong" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000PersenPoleshing" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000PersenPoleshing" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000PersenPreparation" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000PersenPreparation" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000PersenWorkshop" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000PersenWorkshop" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000Person1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000Person1" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000Person2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000Person2" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000Person3" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000Person3" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000Person4" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000Person4" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000Person5" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000Person5" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000Port" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000Port" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000PreExpiredAlert" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000PreExpiredAlert" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000PreparationTime" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000PreparationTime" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000ProgressDisc" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000ProgressDisc" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000ReminderDMKm1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000ReminderDMKm1" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000ReminderDMKm2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000ReminderDMKm2" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000ReminderDMOp1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000ReminderDMOp1" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000ReminderDMOp2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000ReminderDMOp2" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000ReminderEmailKm1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000ReminderEmailKm1" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000ReminderEmailKm2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000ReminderEmailKm2" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000ReminderEmailOp1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000ReminderEmailOp1" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000ReminderEmailOp2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000ReminderEmailOp2" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000ReminderSMSKm1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000ReminderSMSKm1" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000ReminderSMSKm2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000ReminderSMSKm2" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000ReminderSMSOp1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000ReminderSMSOp1" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000ReminderSMSOp2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000ReminderSMSOp2" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000RunningTextInformation" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000RunningTextInformation" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000SBEDisc" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000SBEDisc" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000SOHeader" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000SOHeader" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000SettingRPP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000SettingRPP" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000StaAktifCall" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000StaAktifCall" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000StaAktifDM" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000StaAktifDM" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000StaAktifEmail" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000StaAktifEmail" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000StaAktifSMS" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000StaAktifSMS" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000StaDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000StaDel" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000StaEnablePartTambahanWeb" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000StaEnablePartTambahanWeb" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000StaLihatDataBengkelLain" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000StaLihatDataBengkelLain" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000StaSalon" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000StaSalon" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000StaWaktuAkhir" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000StaWaktuAkhir" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000Tax" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000Tax" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000TextCatatanDM" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000TextCatatanDM" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000TglJamWaktuAkhir" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m000TglJamWaktuAkhir" value="date.struct">
					<input type="hidden" name="search_m000TglJamWaktuAkhir_day" id="search_m000TglJamWaktuAkhir_day" value="">
					<input type="hidden" name="search_m000TglJamWaktuAkhir_month" id="search_m000TglJamWaktuAkhir_month" value="">
					<input type="hidden" name="search_m000TglJamWaktuAkhir_year" id="search_m000TglJamWaktuAkhir_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m000TglJamWaktuAkhir_dp" value="" id="search_m000TglJamWaktuAkhir" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000ToleransiAmbilWO" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000ToleransiAmbilWO" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000ToleransiIDRPerJob" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000ToleransiIDRPerJob" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000ToleransiIDRPerProses" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000ToleransiIDRPerProses" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000UserName" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000UserName" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000VersionAplikasi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000VersionAplikasi" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000WaktuNotifikasiTargetClockOffJob" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m000WaktuNotifikasiTargetClockOffJob" value="date.struct">
					<input type="hidden" name="search_m000WaktuNotifikasiTargetClockOffJob_day" id="search_m000WaktuNotifikasiTargetClockOffJob_day" value="">
					<input type="hidden" name="search_m000WaktuNotifikasiTargetClockOffJob_month" id="search_m000WaktuNotifikasiTargetClockOffJob_month" value="">
					<input type="hidden" name="search_m000WaktuNotifikasiTargetClockOffJob_year" id="search_m000WaktuNotifikasiTargetClockOffJob_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m000WaktuNotifikasiTargetClockOffJob_dp" value="" id="search_m000WaktuNotifikasiTargetClockOffJob" class="search_init">
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000WaktuSessionLog" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m000WaktuSessionLog" value="date.struct">
					<input type="hidden" name="search_m000WaktuSessionLog_day" id="search_m000WaktuSessionLog_day" value="">
					<input type="hidden" name="search_m000WaktuSessionLog_month" id="search_m000WaktuSessionLog_month" value="">
					<input type="hidden" name="search_m000WaktuSessionLog_year" id="search_m000WaktuSessionLog_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m000WaktuSessionLog_dp" value="" id="search_m000WaktuSessionLog" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000mMinusFee" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000mMinusFee" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000staJenisJamKerja" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000staJenisJamKerja" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m000staVendorCatByCc" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m000staVendorCatByCc" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_updatedBy" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var generalParameterTable;
var reloadGeneralParameterTable;
$(function(){
	
	reloadGeneralParameterTable = function() {
		generalParameterTable.fnDraw();
	}

	
	$('#search_m000BufferDeliveryTimeBP').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m000BufferDeliveryTimeBP_day').val(newDate.getDate());
			$('#search_m000BufferDeliveryTimeBP_month').val(newDate.getMonth()+1);
			$('#search_m000BufferDeliveryTimeBP_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			generalParameterTable.fnDraw();	
	});

	

	$('#search_m000BufferDeliveryTimeGR').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m000BufferDeliveryTimeGR_day').val(newDate.getDate());
			$('#search_m000BufferDeliveryTimeGR_month').val(newDate.getMonth()+1);
			$('#search_m000BufferDeliveryTimeGR_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			generalParameterTable.fnDraw();	
	});

	

	$('#search_m000JamKirimEmail').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m000JamKirimEmail_day').val(newDate.getDate());
			$('#search_m000JamKirimEmail_month').val(newDate.getMonth()+1);
			$('#search_m000JamKirimEmail_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			generalParameterTable.fnDraw();	
	});

	

	$('#search_m000JamKirimSMS').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m000JamKirimSMS_day').val(newDate.getDate());
			$('#search_m000JamKirimSMS_month').val(newDate.getMonth()+1);
			$('#search_m000JamKirimSMS_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			generalParameterTable.fnDraw();	
	});

	

	$('#search_m000JamKirimSMSSTNK').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m000JamKirimSMSSTNK_day').val(newDate.getDate());
			$('#search_m000JamKirimSMSSTNK_month').val(newDate.getMonth()+1);
			$('#search_m000JamKirimSMSSTNK_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			generalParameterTable.fnDraw();	
	});

	

	$('#search_m000PenguncianPassword').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m000PenguncianPassword_day').val(newDate.getDate());
			$('#search_m000PenguncianPassword_month').val(newDate.getMonth()+1);
			$('#search_m000PenguncianPassword_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			generalParameterTable.fnDraw();	
	});

	

	$('#search_m000TglJamWaktuAkhir').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m000TglJamWaktuAkhir_day').val(newDate.getDate());
			$('#search_m000TglJamWaktuAkhir_month').val(newDate.getMonth()+1);
			$('#search_m000TglJamWaktuAkhir_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			generalParameterTable.fnDraw();	
	});

	

	$('#search_m000WaktuNotifikasiTargetClockOffJob').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m000WaktuNotifikasiTargetClockOffJob_day').val(newDate.getDate());
			$('#search_m000WaktuNotifikasiTargetClockOffJob_month').val(newDate.getMonth()+1);
			$('#search_m000WaktuNotifikasiTargetClockOffJob_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			generalParameterTable.fnDraw();	
	});

	

	$('#search_m000WaktuSessionLog').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m000WaktuSessionLog_day').val(newDate.getDate());
			$('#search_m000WaktuSessionLog_month').val(newDate.getMonth()+1);
			$('#search_m000WaktuSessionLog_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			generalParameterTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	generalParameterTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	generalParameterTable = $('#generalParameter_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "companyDealer",
	"mDataProp": "companyDealer",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "createdBy",
	"mDataProp": "createdBy",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000Aging11",
	"mDataProp": "m000Aging11",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000Aging12",
	"mDataProp": "m000Aging12",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000Aging21",
	"mDataProp": "m000Aging21",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000Aging22",
	"mDataProp": "m000Aging22",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000Aging31",
	"mDataProp": "m000Aging31",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000Aging32",
	"mDataProp": "m000Aging32",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000Aging41",
	"mDataProp": "m000Aging41",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000Aging42",
	"mDataProp": "m000Aging42",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000Aging51",
	"mDataProp": "m000Aging51",
	"aTargets": [11],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000Aging52",
	"mDataProp": "m000Aging52",
	"aTargets": [12],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000AlamatFTP",
	"mDataProp": "m000AlamatFTP",
	"aTargets": [13],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000AlamatPWC",
	"mDataProp": "m000AlamatPWC",
	"aTargets": [14],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000AlamatTWC",
	"mDataProp": "m000AlamatTWC",
	"aTargets": [15],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000BufferDeliveryTimeBP",
	"mDataProp": "m000BufferDeliveryTimeBP",
	"aTargets": [16],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000BufferDeliveryTimeGR",
	"mDataProp": "m000BufferDeliveryTimeGR",
	"aTargets": [17],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000BulanWarningSertifikat",
	"mDataProp": "m000BulanWarningSertifikat",
	"aTargets": [18],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000DefaultMasaBerlakuAccount",
	"mDataProp": "m000DefaultMasaBerlakuAccount",
	"aTargets": [19],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000DefaultPUK",
	"mDataProp": "m000DefaultPUK",
	"aTargets": [20],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000DefaultUserPass",
	"mDataProp": "m000DefaultUserPass",
	"aTargets": [21],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000DisableConcurrentLogin",
	"mDataProp": "m000DisableConcurrentLogin",
	"aTargets": [22],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000FormatEmail",
	"mDataProp": "m000FormatEmail",
	"aTargets": [23],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000FormatSMSHMinus",
	"mDataProp": "m000FormatSMSHMinus",
	"aTargets": [24],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000FormatSMSJMinus",
	"mDataProp": "m000FormatSMSJMinus",
	"aTargets": [25],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000FormatSMSNotifikasi",
	"mDataProp": "m000FormatSMSNotifikasi",
	"aTargets": [26],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000FormatSMSService",
	"mDataProp": "m000FormatSMSService",
	"aTargets": [27],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000HMinParameterReserved",
	"mDataProp": "m000HMinParameterReserved",
	"aTargets": [28],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000HMinusCekDokAsuransi",
	"mDataProp": "m000HMinusCekDokAsuransi",
	"aTargets": [29],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000HMinusFee",
	"mDataProp": "m000HMinusFee",
	"aTargets": [30],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000HMinusReminderEmail",
	"mDataProp": "m000HMinusReminderEmail",
	"aTargets": [31],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000HMinusReminderSMS",
	"mDataProp": "m000HMinusReminderSMS",
	"aTargets": [32],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000HMinusReminderSTNK",
	"mDataProp": "m000HMinusReminderSTNK",
	"aTargets": [33],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000HMinusSMSAppointment",
	"mDataProp": "m000HMinusSMSAppointment",
	"aTargets": [34],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000HMinusTglReminderDM",
	"mDataProp": "m000HMinusTglReminderDM",
	"aTargets": [35],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000HPlusCancelBookingFee",
	"mDataProp": "m000HPlusCancelBookingFee",
	"aTargets": [36],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000HPlusUndanganFA",
	"mDataProp": "m000HPlusUndanganFA",
	"aTargets": [37],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000InvoiceFooter",
	"mDataProp": "m000InvoiceFooter",
	"aTargets": [38],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000JMinusSMSAppointment",
	"mDataProp": "m000JMinusSMSAppointment",
	"aTargets": [39],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000JPlusSMSTerlambat",
	"mDataProp": "m000JPlusSMSTerlambat",
	"aTargets": [40],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000JamKirimEmail",
	"mDataProp": "m000JamKirimEmail",
	"aTargets": [41],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000JamKirimSMS",
	"mDataProp": "m000JamKirimSMS",
	"aTargets": [42],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000JamKirimSMSSTNK",
	"mDataProp": "m000JamKirimSMSSTNK",
	"aTargets": [43],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000JmlMaksimalKesalahan",
	"mDataProp": "m000JmlMaksimalKesalahan",
	"aTargets": [44],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000JmlMaxInvitation",
	"mDataProp": "m000JmlMaxInvitation",
	"aTargets": [45],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000JmlMaxItemWAC",
	"mDataProp": "m000JmlMaxItemWAC",
	"aTargets": [46],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000JmlMaxKirimSMSFollowUp",
	"mDataProp": "m000JmlMaxKirimSMSFollowUp",
	"aTargets": [47],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000JmlSKipCustomer",
	"mDataProp": "m000JmlSKipCustomer",
	"aTargets": [48],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000JmlToleransiAsuransiBP",
	"mDataProp": "m000JmlToleransiAsuransiBP",
	"aTargets": [49],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000KonfirmasiHariSebelum",
	"mDataProp": "m000KonfirmasiHariSebelum",
	"aTargets": [50],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000KonfirmasiJamSebelum",
	"mDataProp": "m000KonfirmasiJamSebelum",
	"aTargets": [51],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000KonfirmasiTerlambat",
	"mDataProp": "m000KonfirmasiTerlambat",
	"aTargets": [52],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000LamaCuciMobil",
	"mDataProp": "m000LamaCuciMobil",
	"aTargets": [53],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000LamaWaktuMinCuci",
	"mDataProp": "m000LamaWaktuMinCuci",
	"aTargets": [54],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000MaksBayarCashRefund",
	"mDataProp": "m000MaksBayarCashRefund",
	"aTargets": [55],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000MaxPasswordExpired",
	"mDataProp": "m000MaxPasswordExpired",
	"aTargets": [56],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000MaxPersenKembaliBookingFee",
	"mDataProp": "m000MaxPersenKembaliBookingFee",
	"aTargets": [57],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000MaxRecordPerPage",
	"mDataProp": "m000MaxRecordPerPage",
	"aTargets": [58],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000MinLastPassTdkBolehDigunakan",
	"mDataProp": "m000MinLastPassTdkBolehDigunakan",
	"aTargets": [59],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000NPWPPWC",
	"mDataProp": "m000NPWPPWC",
	"aTargets": [60],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000NPWPTWC",
	"mDataProp": "m000NPWPTWC",
	"aTargets": [61],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000NamaHostOrIPAddress",
	"mDataProp": "m000NamaHostOrIPAddress",
	"aTargets": [62],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000NamaPWC",
	"mDataProp": "m000NamaPWC",
	"aTargets": [63],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000NamaTWC",
	"mDataProp": "m000NamaTWC",
	"aTargets": [64],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000NominalMaterai",
	"mDataProp": "m000NominalMaterai",
	"aTargets": [65],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000Password",
	"mDataProp": "m000Password",
	"aTargets": [66],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000PenguncianPassword",
	"mDataProp": "m000PenguncianPassword",
	"aTargets": [67],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000PersenKenaikanJobSublet",
	"mDataProp": "m000PersenKenaikanJobSublet",
	"aTargets": [68],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000PersenMaxGunakanMaterial",
	"mDataProp": "m000PersenMaxGunakanMaterial",
	"aTargets": [69],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000PersenPPN",
	"mDataProp": "m000PersenPPN",
	"aTargets": [70],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000PersenPPh",
	"mDataProp": "m000PersenPPh",
	"aTargets": [71],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000PersenPainting",
	"mDataProp": "m000PersenPainting",
	"aTargets": [72],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000PersenPelepasanPartBP",
	"mDataProp": "m000PersenPelepasanPartBP",
	"aTargets": [73],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000PersenPemasanganPartBP",
	"mDataProp": "m000PersenPemasanganPartBP",
	"aTargets": [74],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000PersenPemborong",
	"mDataProp": "m000PersenPemborong",
	"aTargets": [75],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000PersenPoleshing",
	"mDataProp": "m000PersenPoleshing",
	"aTargets": [76],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000PersenPreparation",
	"mDataProp": "m000PersenPreparation",
	"aTargets": [77],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000PersenWorkshop",
	"mDataProp": "m000PersenWorkshop",
	"aTargets": [78],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000Person1",
	"mDataProp": "m000Person1",
	"aTargets": [79],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000Person2",
	"mDataProp": "m000Person2",
	"aTargets": [80],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000Person3",
	"mDataProp": "m000Person3",
	"aTargets": [81],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000Person4",
	"mDataProp": "m000Person4",
	"aTargets": [82],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000Person5",
	"mDataProp": "m000Person5",
	"aTargets": [83],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000Port",
	"mDataProp": "m000Port",
	"aTargets": [84],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000PreExpiredAlert",
	"mDataProp": "m000PreExpiredAlert",
	"aTargets": [85],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000PreparationTime",
	"mDataProp": "m000PreparationTime",
	"aTargets": [86],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000ProgressDisc",
	"mDataProp": "m000ProgressDisc",
	"aTargets": [87],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000ReminderDMKm1",
	"mDataProp": "m000ReminderDMKm1",
	"aTargets": [88],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000ReminderDMKm2",
	"mDataProp": "m000ReminderDMKm2",
	"aTargets": [89],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000ReminderDMOp1",
	"mDataProp": "m000ReminderDMOp1",
	"aTargets": [90],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000ReminderDMOp2",
	"mDataProp": "m000ReminderDMOp2",
	"aTargets": [91],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000ReminderEmailKm1",
	"mDataProp": "m000ReminderEmailKm1",
	"aTargets": [92],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000ReminderEmailKm2",
	"mDataProp": "m000ReminderEmailKm2",
	"aTargets": [93],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000ReminderEmailOp1",
	"mDataProp": "m000ReminderEmailOp1",
	"aTargets": [94],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000ReminderEmailOp2",
	"mDataProp": "m000ReminderEmailOp2",
	"aTargets": [95],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000ReminderSMSKm1",
	"mDataProp": "m000ReminderSMSKm1",
	"aTargets": [96],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000ReminderSMSKm2",
	"mDataProp": "m000ReminderSMSKm2",
	"aTargets": [97],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000ReminderSMSOp1",
	"mDataProp": "m000ReminderSMSOp1",
	"aTargets": [98],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000ReminderSMSOp2",
	"mDataProp": "m000ReminderSMSOp2",
	"aTargets": [99],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000RunningTextInformation",
	"mDataProp": "m000RunningTextInformation",
	"aTargets": [100],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000SBEDisc",
	"mDataProp": "m000SBEDisc",
	"aTargets": [101],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000SOHeader",
	"mDataProp": "m000SOHeader",
	"aTargets": [102],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000SettingRPP",
	"mDataProp": "m000SettingRPP",
	"aTargets": [103],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000StaAktifCall",
	"mDataProp": "m000StaAktifCall",
	"aTargets": [104],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000StaAktifDM",
	"mDataProp": "m000StaAktifDM",
	"aTargets": [105],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000StaAktifEmail",
	"mDataProp": "m000StaAktifEmail",
	"aTargets": [106],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000StaAktifSMS",
	"mDataProp": "m000StaAktifSMS",
	"aTargets": [107],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000StaDel",
	"mDataProp": "m000StaDel",
	"aTargets": [108],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000StaEnablePartTambahanWeb",
	"mDataProp": "m000StaEnablePartTambahanWeb",
	"aTargets": [109],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000StaLihatDataBengkelLain",
	"mDataProp": "m000StaLihatDataBengkelLain",
	"aTargets": [110],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000StaSalon",
	"mDataProp": "m000StaSalon",
	"aTargets": [111],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000StaWaktuAkhir",
	"mDataProp": "m000StaWaktuAkhir",
	"aTargets": [112],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000Tax",
	"mDataProp": "m000Tax",
	"aTargets": [113],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000TextCatatanDM",
	"mDataProp": "m000TextCatatanDM",
	"aTargets": [114],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000TglJamWaktuAkhir",
	"mDataProp": "m000TglJamWaktuAkhir",
	"aTargets": [115],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000ToleransiAmbilWO",
	"mDataProp": "m000ToleransiAmbilWO",
	"aTargets": [116],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000ToleransiIDRPerJob",
	"mDataProp": "m000ToleransiIDRPerJob",
	"aTargets": [117],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000ToleransiIDRPerProses",
	"mDataProp": "m000ToleransiIDRPerProses",
	"aTargets": [118],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000UserName",
	"mDataProp": "m000UserName",
	"aTargets": [119],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000VersionAplikasi",
	"mDataProp": "m000VersionAplikasi",
	"aTargets": [120],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000WaktuNotifikasiTargetClockOffJob",
	"mDataProp": "m000WaktuNotifikasiTargetClockOffJob",
	"aTargets": [121],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000WaktuSessionLog",
	"mDataProp": "m000WaktuSessionLog",
	"aTargets": [122],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000mMinusFee",
	"mDataProp": "m000mMinusFee",
	"aTargets": [123],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000staJenisJamKerja",
	"mDataProp": "m000staJenisJamKerja",
	"aTargets": [124],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m000staVendorCatByCc",
	"mDataProp": "m000staVendorCatByCc",
	"aTargets": [125],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "updatedBy",
	"mDataProp": "updatedBy",
	"aTargets": [126],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}
	
						var m000Aging11 = $('#filter_m000Aging11 input').val();
						if(m000Aging11){
							aoData.push(
									{"name": 'sCriteria_m000Aging11', "value": m000Aging11}
							);
						}
	
						var m000Aging12 = $('#filter_m000Aging12 input').val();
						if(m000Aging12){
							aoData.push(
									{"name": 'sCriteria_m000Aging12', "value": m000Aging12}
							);
						}
	
						var m000Aging21 = $('#filter_m000Aging21 input').val();
						if(m000Aging21){
							aoData.push(
									{"name": 'sCriteria_m000Aging21', "value": m000Aging21}
							);
						}
	
						var m000Aging22 = $('#filter_m000Aging22 input').val();
						if(m000Aging22){
							aoData.push(
									{"name": 'sCriteria_m000Aging22', "value": m000Aging22}
							);
						}
	
						var m000Aging31 = $('#filter_m000Aging31 input').val();
						if(m000Aging31){
							aoData.push(
									{"name": 'sCriteria_m000Aging31', "value": m000Aging31}
							);
						}
	
						var m000Aging32 = $('#filter_m000Aging32 input').val();
						if(m000Aging32){
							aoData.push(
									{"name": 'sCriteria_m000Aging32', "value": m000Aging32}
							);
						}
	
						var m000Aging41 = $('#filter_m000Aging41 input').val();
						if(m000Aging41){
							aoData.push(
									{"name": 'sCriteria_m000Aging41', "value": m000Aging41}
							);
						}
	
						var m000Aging42 = $('#filter_m000Aging42 input').val();
						if(m000Aging42){
							aoData.push(
									{"name": 'sCriteria_m000Aging42', "value": m000Aging42}
							);
						}
	
						var m000Aging51 = $('#filter_m000Aging51 input').val();
						if(m000Aging51){
							aoData.push(
									{"name": 'sCriteria_m000Aging51', "value": m000Aging51}
							);
						}
	
						var m000Aging52 = $('#filter_m000Aging52 input').val();
						if(m000Aging52){
							aoData.push(
									{"name": 'sCriteria_m000Aging52', "value": m000Aging52}
							);
						}
	
						var m000AlamatFTP = $('#filter_m000AlamatFTP input').val();
						if(m000AlamatFTP){
							aoData.push(
									{"name": 'sCriteria_m000AlamatFTP', "value": m000AlamatFTP}
							);
						}
	
						var m000AlamatPWC = $('#filter_m000AlamatPWC input').val();
						if(m000AlamatPWC){
							aoData.push(
									{"name": 'sCriteria_m000AlamatPWC', "value": m000AlamatPWC}
							);
						}
	
						var m000AlamatTWC = $('#filter_m000AlamatTWC input').val();
						if(m000AlamatTWC){
							aoData.push(
									{"name": 'sCriteria_m000AlamatTWC', "value": m000AlamatTWC}
							);
						}

						var m000BufferDeliveryTimeBP = $('#search_m000BufferDeliveryTimeBP').val();
						var m000BufferDeliveryTimeBPDay = $('#search_m000BufferDeliveryTimeBP_day').val();
						var m000BufferDeliveryTimeBPMonth = $('#search_m000BufferDeliveryTimeBP_month').val();
						var m000BufferDeliveryTimeBPYear = $('#search_m000BufferDeliveryTimeBP_year').val();
						
						if(m000BufferDeliveryTimeBP){
							aoData.push(
									{"name": 'sCriteria_m000BufferDeliveryTimeBP', "value": "date.struct"},
									{"name": 'sCriteria_m000BufferDeliveryTimeBP_dp', "value": m000BufferDeliveryTimeBP},
									{"name": 'sCriteria_m000BufferDeliveryTimeBP_day', "value": m000BufferDeliveryTimeBPDay},
									{"name": 'sCriteria_m000BufferDeliveryTimeBP_month', "value": m000BufferDeliveryTimeBPMonth},
									{"name": 'sCriteria_m000BufferDeliveryTimeBP_year', "value": m000BufferDeliveryTimeBPYear}
							);
						}

						var m000BufferDeliveryTimeGR = $('#search_m000BufferDeliveryTimeGR').val();
						var m000BufferDeliveryTimeGRDay = $('#search_m000BufferDeliveryTimeGR_day').val();
						var m000BufferDeliveryTimeGRMonth = $('#search_m000BufferDeliveryTimeGR_month').val();
						var m000BufferDeliveryTimeGRYear = $('#search_m000BufferDeliveryTimeGR_year').val();
						
						if(m000BufferDeliveryTimeGR){
							aoData.push(
									{"name": 'sCriteria_m000BufferDeliveryTimeGR', "value": "date.struct"},
									{"name": 'sCriteria_m000BufferDeliveryTimeGR_dp', "value": m000BufferDeliveryTimeGR},
									{"name": 'sCriteria_m000BufferDeliveryTimeGR_day', "value": m000BufferDeliveryTimeGRDay},
									{"name": 'sCriteria_m000BufferDeliveryTimeGR_month', "value": m000BufferDeliveryTimeGRMonth},
									{"name": 'sCriteria_m000BufferDeliveryTimeGR_year', "value": m000BufferDeliveryTimeGRYear}
							);
						}
	
						var m000BulanWarningSertifikat = $('#filter_m000BulanWarningSertifikat input').val();
						if(m000BulanWarningSertifikat){
							aoData.push(
									{"name": 'sCriteria_m000BulanWarningSertifikat', "value": m000BulanWarningSertifikat}
							);
						}
	
						var m000DefaultMasaBerlakuAccount = $('#filter_m000DefaultMasaBerlakuAccount input').val();
						if(m000DefaultMasaBerlakuAccount){
							aoData.push(
									{"name": 'sCriteria_m000DefaultMasaBerlakuAccount', "value": m000DefaultMasaBerlakuAccount}
							);
						}
	
						var m000DefaultPUK = $('#filter_m000DefaultPUK input').val();
						if(m000DefaultPUK){
							aoData.push(
									{"name": 'sCriteria_m000DefaultPUK', "value": m000DefaultPUK}
							);
						}
	
						var m000DefaultUserPass = $('#filter_m000DefaultUserPass input').val();
						if(m000DefaultUserPass){
							aoData.push(
									{"name": 'sCriteria_m000DefaultUserPass', "value": m000DefaultUserPass}
							);
						}
	
						var m000DisableConcurrentLogin = $('#filter_m000DisableConcurrentLogin input').val();
						if(m000DisableConcurrentLogin){
							aoData.push(
									{"name": 'sCriteria_m000DisableConcurrentLogin', "value": m000DisableConcurrentLogin}
							);
						}
	
						var m000FormatEmail = $('#filter_m000FormatEmail input').val();
						if(m000FormatEmail){
							aoData.push(
									{"name": 'sCriteria_m000FormatEmail', "value": m000FormatEmail}
							);
						}
	
						var m000FormatSMSHMinus = $('#filter_m000FormatSMSHMinus input').val();
						if(m000FormatSMSHMinus){
							aoData.push(
									{"name": 'sCriteria_m000FormatSMSHMinus', "value": m000FormatSMSHMinus}
							);
						}
	
						var m000FormatSMSJMinus = $('#filter_m000FormatSMSJMinus input').val();
						if(m000FormatSMSJMinus){
							aoData.push(
									{"name": 'sCriteria_m000FormatSMSJMinus', "value": m000FormatSMSJMinus}
							);
						}
	
						var m000FormatSMSNotifikasi = $('#filter_m000FormatSMSNotifikasi input').val();
						if(m000FormatSMSNotifikasi){
							aoData.push(
									{"name": 'sCriteria_m000FormatSMSNotifikasi', "value": m000FormatSMSNotifikasi}
							);
						}
	
						var m000FormatSMSService = $('#filter_m000FormatSMSService input').val();
						if(m000FormatSMSService){
							aoData.push(
									{"name": 'sCriteria_m000FormatSMSService', "value": m000FormatSMSService}
							);
						}
	
						var m000HMinParameterReserved = $('#filter_m000HMinParameterReserved input').val();
						if(m000HMinParameterReserved){
							aoData.push(
									{"name": 'sCriteria_m000HMinParameterReserved', "value": m000HMinParameterReserved}
							);
						}
	
						var m000HMinusCekDokAsuransi = $('#filter_m000HMinusCekDokAsuransi input').val();
						if(m000HMinusCekDokAsuransi){
							aoData.push(
									{"name": 'sCriteria_m000HMinusCekDokAsuransi', "value": m000HMinusCekDokAsuransi}
							);
						}
	
						var m000HMinusFee = $('#filter_m000HMinusFee input').val();
						if(m000HMinusFee){
							aoData.push(
									{"name": 'sCriteria_m000HMinusFee', "value": m000HMinusFee}
							);
						}
	
						var m000HMinusReminderEmail = $('#filter_m000HMinusReminderEmail input').val();
						if(m000HMinusReminderEmail){
							aoData.push(
									{"name": 'sCriteria_m000HMinusReminderEmail', "value": m000HMinusReminderEmail}
							);
						}
	
						var m000HMinusReminderSMS = $('#filter_m000HMinusReminderSMS input').val();
						if(m000HMinusReminderSMS){
							aoData.push(
									{"name": 'sCriteria_m000HMinusReminderSMS', "value": m000HMinusReminderSMS}
							);
						}
	
						var m000HMinusReminderSTNK = $('#filter_m000HMinusReminderSTNK input').val();
						if(m000HMinusReminderSTNK){
							aoData.push(
									{"name": 'sCriteria_m000HMinusReminderSTNK', "value": m000HMinusReminderSTNK}
							);
						}
	
						var m000HMinusSMSAppointment = $('#filter_m000HMinusSMSAppointment input').val();
						if(m000HMinusSMSAppointment){
							aoData.push(
									{"name": 'sCriteria_m000HMinusSMSAppointment', "value": m000HMinusSMSAppointment}
							);
						}
	
						var m000HMinusTglReminderDM = $('#filter_m000HMinusTglReminderDM input').val();
						if(m000HMinusTglReminderDM){
							aoData.push(
									{"name": 'sCriteria_m000HMinusTglReminderDM', "value": m000HMinusTglReminderDM}
							);
						}
	
						var m000HPlusCancelBookingFee = $('#filter_m000HPlusCancelBookingFee input').val();
						if(m000HPlusCancelBookingFee){
							aoData.push(
									{"name": 'sCriteria_m000HPlusCancelBookingFee', "value": m000HPlusCancelBookingFee}
							);
						}
	
						var m000HPlusUndanganFA = $('#filter_m000HPlusUndanganFA input').val();
						if(m000HPlusUndanganFA){
							aoData.push(
									{"name": 'sCriteria_m000HPlusUndanganFA', "value": m000HPlusUndanganFA}
							);
						}
	
						var m000InvoiceFooter = $('#filter_m000InvoiceFooter input').val();
						if(m000InvoiceFooter){
							aoData.push(
									{"name": 'sCriteria_m000InvoiceFooter', "value": m000InvoiceFooter}
							);
						}
	
						var m000JMinusSMSAppointment = $('#filter_m000JMinusSMSAppointment input').val();
						if(m000JMinusSMSAppointment){
							aoData.push(
									{"name": 'sCriteria_m000JMinusSMSAppointment', "value": m000JMinusSMSAppointment}
							);
						}
	
						var m000JPlusSMSTerlambat = $('#filter_m000JPlusSMSTerlambat input').val();
						if(m000JPlusSMSTerlambat){
							aoData.push(
									{"name": 'sCriteria_m000JPlusSMSTerlambat', "value": m000JPlusSMSTerlambat}
							);
						}

						var m000JamKirimEmail = $('#search_m000JamKirimEmail').val();
						var m000JamKirimEmailDay = $('#search_m000JamKirimEmail_day').val();
						var m000JamKirimEmailMonth = $('#search_m000JamKirimEmail_month').val();
						var m000JamKirimEmailYear = $('#search_m000JamKirimEmail_year').val();
						
						if(m000JamKirimEmail){
							aoData.push(
									{"name": 'sCriteria_m000JamKirimEmail', "value": "date.struct"},
									{"name": 'sCriteria_m000JamKirimEmail_dp', "value": m000JamKirimEmail},
									{"name": 'sCriteria_m000JamKirimEmail_day', "value": m000JamKirimEmailDay},
									{"name": 'sCriteria_m000JamKirimEmail_month', "value": m000JamKirimEmailMonth},
									{"name": 'sCriteria_m000JamKirimEmail_year', "value": m000JamKirimEmailYear}
							);
						}

						var m000JamKirimSMS = $('#search_m000JamKirimSMS').val();
						var m000JamKirimSMSDay = $('#search_m000JamKirimSMS_day').val();
						var m000JamKirimSMSMonth = $('#search_m000JamKirimSMS_month').val();
						var m000JamKirimSMSYear = $('#search_m000JamKirimSMS_year').val();
						
						if(m000JamKirimSMS){
							aoData.push(
									{"name": 'sCriteria_m000JamKirimSMS', "value": "date.struct"},
									{"name": 'sCriteria_m000JamKirimSMS_dp', "value": m000JamKirimSMS},
									{"name": 'sCriteria_m000JamKirimSMS_day', "value": m000JamKirimSMSDay},
									{"name": 'sCriteria_m000JamKirimSMS_month', "value": m000JamKirimSMSMonth},
									{"name": 'sCriteria_m000JamKirimSMS_year', "value": m000JamKirimSMSYear}
							);
						}

						var m000JamKirimSMSSTNK = $('#search_m000JamKirimSMSSTNK').val();
						var m000JamKirimSMSSTNKDay = $('#search_m000JamKirimSMSSTNK_day').val();
						var m000JamKirimSMSSTNKMonth = $('#search_m000JamKirimSMSSTNK_month').val();
						var m000JamKirimSMSSTNKYear = $('#search_m000JamKirimSMSSTNK_year').val();
						
						if(m000JamKirimSMSSTNK){
							aoData.push(
									{"name": 'sCriteria_m000JamKirimSMSSTNK', "value": "date.struct"},
									{"name": 'sCriteria_m000JamKirimSMSSTNK_dp', "value": m000JamKirimSMSSTNK},
									{"name": 'sCriteria_m000JamKirimSMSSTNK_day', "value": m000JamKirimSMSSTNKDay},
									{"name": 'sCriteria_m000JamKirimSMSSTNK_month', "value": m000JamKirimSMSSTNKMonth},
									{"name": 'sCriteria_m000JamKirimSMSSTNK_year', "value": m000JamKirimSMSSTNKYear}
							);
						}
	
						var m000JmlMaksimalKesalahan = $('#filter_m000JmlMaksimalKesalahan input').val();
						if(m000JmlMaksimalKesalahan){
							aoData.push(
									{"name": 'sCriteria_m000JmlMaksimalKesalahan', "value": m000JmlMaksimalKesalahan}
							);
						}
	
						var m000JmlMaxInvitation = $('#filter_m000JmlMaxInvitation input').val();
						if(m000JmlMaxInvitation){
							aoData.push(
									{"name": 'sCriteria_m000JmlMaxInvitation', "value": m000JmlMaxInvitation}
							);
						}
	
						var m000JmlMaxItemWAC = $('#filter_m000JmlMaxItemWAC input').val();
						if(m000JmlMaxItemWAC){
							aoData.push(
									{"name": 'sCriteria_m000JmlMaxItemWAC', "value": m000JmlMaxItemWAC}
							);
						}
	
						var m000JmlMaxKirimSMSFollowUp = $('#filter_m000JmlMaxKirimSMSFollowUp input').val();
						if(m000JmlMaxKirimSMSFollowUp){
							aoData.push(
									{"name": 'sCriteria_m000JmlMaxKirimSMSFollowUp', "value": m000JmlMaxKirimSMSFollowUp}
							);
						}
	
						var m000JmlSKipCustomer = $('#filter_m000JmlSKipCustomer input').val();
						if(m000JmlSKipCustomer){
							aoData.push(
									{"name": 'sCriteria_m000JmlSKipCustomer', "value": m000JmlSKipCustomer}
							);
						}
	
						var m000JmlToleransiAsuransiBP = $('#filter_m000JmlToleransiAsuransiBP input').val();
						if(m000JmlToleransiAsuransiBP){
							aoData.push(
									{"name": 'sCriteria_m000JmlToleransiAsuransiBP', "value": m000JmlToleransiAsuransiBP}
							);
						}
	
						var m000KonfirmasiHariSebelum = $('#filter_m000KonfirmasiHariSebelum input').val();
						if(m000KonfirmasiHariSebelum){
							aoData.push(
									{"name": 'sCriteria_m000KonfirmasiHariSebelum', "value": m000KonfirmasiHariSebelum}
							);
						}
	
						var m000KonfirmasiJamSebelum = $('#filter_m000KonfirmasiJamSebelum input').val();
						if(m000KonfirmasiJamSebelum){
							aoData.push(
									{"name": 'sCriteria_m000KonfirmasiJamSebelum', "value": m000KonfirmasiJamSebelum}
							);
						}
	
						var m000KonfirmasiTerlambat = $('#filter_m000KonfirmasiTerlambat input').val();
						if(m000KonfirmasiTerlambat){
							aoData.push(
									{"name": 'sCriteria_m000KonfirmasiTerlambat', "value": m000KonfirmasiTerlambat}
							);
						}
	
						var m000LamaCuciMobil = $('#filter_m000LamaCuciMobil input').val();
						if(m000LamaCuciMobil){
							aoData.push(
									{"name": 'sCriteria_m000LamaCuciMobil', "value": m000LamaCuciMobil}
							);
						}
	
						var m000LamaWaktuMinCuci = $('#filter_m000LamaWaktuMinCuci input').val();
						if(m000LamaWaktuMinCuci){
							aoData.push(
									{"name": 'sCriteria_m000LamaWaktuMinCuci', "value": m000LamaWaktuMinCuci}
							);
						}
	
						var m000MaksBayarCashRefund = $('#filter_m000MaksBayarCashRefund input').val();
						if(m000MaksBayarCashRefund){
							aoData.push(
									{"name": 'sCriteria_m000MaksBayarCashRefund', "value": m000MaksBayarCashRefund}
							);
						}
	
						var m000MaxPasswordExpired = $('#filter_m000MaxPasswordExpired input').val();
						if(m000MaxPasswordExpired){
							aoData.push(
									{"name": 'sCriteria_m000MaxPasswordExpired', "value": m000MaxPasswordExpired}
							);
						}
	
						var m000MaxPersenKembaliBookingFee = $('#filter_m000MaxPersenKembaliBookingFee input').val();
						if(m000MaxPersenKembaliBookingFee){
							aoData.push(
									{"name": 'sCriteria_m000MaxPersenKembaliBookingFee', "value": m000MaxPersenKembaliBookingFee}
							);
						}
	
						var m000MaxRecordPerPage = $('#filter_m000MaxRecordPerPage input').val();
						if(m000MaxRecordPerPage){
							aoData.push(
									{"name": 'sCriteria_m000MaxRecordPerPage', "value": m000MaxRecordPerPage}
							);
						}
	
						var m000MinLastPassTdkBolehDigunakan = $('#filter_m000MinLastPassTdkBolehDigunakan input').val();
						if(m000MinLastPassTdkBolehDigunakan){
							aoData.push(
									{"name": 'sCriteria_m000MinLastPassTdkBolehDigunakan', "value": m000MinLastPassTdkBolehDigunakan}
							);
						}
	
						var m000NPWPPWC = $('#filter_m000NPWPPWC input').val();
						if(m000NPWPPWC){
							aoData.push(
									{"name": 'sCriteria_m000NPWPPWC', "value": m000NPWPPWC}
							);
						}
	
						var m000NPWPTWC = $('#filter_m000NPWPTWC input').val();
						if(m000NPWPTWC){
							aoData.push(
									{"name": 'sCriteria_m000NPWPTWC', "value": m000NPWPTWC}
							);
						}
	
						var m000NamaHostOrIPAddress = $('#filter_m000NamaHostOrIPAddress input').val();
						if(m000NamaHostOrIPAddress){
							aoData.push(
									{"name": 'sCriteria_m000NamaHostOrIPAddress', "value": m000NamaHostOrIPAddress}
							);
						}
	
						var m000NamaPWC = $('#filter_m000NamaPWC input').val();
						if(m000NamaPWC){
							aoData.push(
									{"name": 'sCriteria_m000NamaPWC', "value": m000NamaPWC}
							);
						}
	
						var m000NamaTWC = $('#filter_m000NamaTWC input').val();
						if(m000NamaTWC){
							aoData.push(
									{"name": 'sCriteria_m000NamaTWC', "value": m000NamaTWC}
							);
						}
	
						var m000NominalMaterai = $('#filter_m000NominalMaterai input').val();
						if(m000NominalMaterai){
							aoData.push(
									{"name": 'sCriteria_m000NominalMaterai', "value": m000NominalMaterai}
							);
						}
	
						var m000Password = $('#filter_m000Password input').val();
						if(m000Password){
							aoData.push(
									{"name": 'sCriteria_m000Password', "value": m000Password}
							);
						}

						var m000PenguncianPassword = $('#search_m000PenguncianPassword').val();
						var m000PenguncianPasswordDay = $('#search_m000PenguncianPassword_day').val();
						var m000PenguncianPasswordMonth = $('#search_m000PenguncianPassword_month').val();
						var m000PenguncianPasswordYear = $('#search_m000PenguncianPassword_year').val();
						
						if(m000PenguncianPassword){
							aoData.push(
									{"name": 'sCriteria_m000PenguncianPassword', "value": "date.struct"},
									{"name": 'sCriteria_m000PenguncianPassword_dp', "value": m000PenguncianPassword},
									{"name": 'sCriteria_m000PenguncianPassword_day', "value": m000PenguncianPasswordDay},
									{"name": 'sCriteria_m000PenguncianPassword_month', "value": m000PenguncianPasswordMonth},
									{"name": 'sCriteria_m000PenguncianPassword_year', "value": m000PenguncianPasswordYear}
							);
						}
	
						var m000PersenKenaikanJobSublet = $('#filter_m000PersenKenaikanJobSublet input').val();
						if(m000PersenKenaikanJobSublet){
							aoData.push(
									{"name": 'sCriteria_m000PersenKenaikanJobSublet', "value": m000PersenKenaikanJobSublet}
							);
						}
	
						var m000PersenMaxGunakanMaterial = $('#filter_m000PersenMaxGunakanMaterial input').val();
						if(m000PersenMaxGunakanMaterial){
							aoData.push(
									{"name": 'sCriteria_m000PersenMaxGunakanMaterial', "value": m000PersenMaxGunakanMaterial}
							);
						}
	
						var m000PersenPPN = $('#filter_m000PersenPPN input').val();
						if(m000PersenPPN){
							aoData.push(
									{"name": 'sCriteria_m000PersenPPN', "value": m000PersenPPN}
							);
						}
	
						var m000PersenPPh = $('#filter_m000PersenPPh input').val();
						if(m000PersenPPh){
							aoData.push(
									{"name": 'sCriteria_m000PersenPPh', "value": m000PersenPPh}
							);
						}
	
						var m000PersenPainting = $('#filter_m000PersenPainting input').val();
						if(m000PersenPainting){
							aoData.push(
									{"name": 'sCriteria_m000PersenPainting', "value": m000PersenPainting}
							);
						}
	
						var m000PersenPelepasanPartBP = $('#filter_m000PersenPelepasanPartBP input').val();
						if(m000PersenPelepasanPartBP){
							aoData.push(
									{"name": 'sCriteria_m000PersenPelepasanPartBP', "value": m000PersenPelepasanPartBP}
							);
						}
	
						var m000PersenPemasanganPartBP = $('#filter_m000PersenPemasanganPartBP input').val();
						if(m000PersenPemasanganPartBP){
							aoData.push(
									{"name": 'sCriteria_m000PersenPemasanganPartBP', "value": m000PersenPemasanganPartBP}
							);
						}
	
						var m000PersenPemborong = $('#filter_m000PersenPemborong input').val();
						if(m000PersenPemborong){
							aoData.push(
									{"name": 'sCriteria_m000PersenPemborong', "value": m000PersenPemborong}
							);
						}
	
						var m000PersenPoleshing = $('#filter_m000PersenPoleshing input').val();
						if(m000PersenPoleshing){
							aoData.push(
									{"name": 'sCriteria_m000PersenPoleshing', "value": m000PersenPoleshing}
							);
						}
	
						var m000PersenPreparation = $('#filter_m000PersenPreparation input').val();
						if(m000PersenPreparation){
							aoData.push(
									{"name": 'sCriteria_m000PersenPreparation', "value": m000PersenPreparation}
							);
						}
	
						var m000PersenWorkshop = $('#filter_m000PersenWorkshop input').val();
						if(m000PersenWorkshop){
							aoData.push(
									{"name": 'sCriteria_m000PersenWorkshop', "value": m000PersenWorkshop}
							);
						}
	
						var m000Person1 = $('#filter_m000Person1 input').val();
						if(m000Person1){
							aoData.push(
									{"name": 'sCriteria_m000Person1', "value": m000Person1}
							);
						}
	
						var m000Person2 = $('#filter_m000Person2 input').val();
						if(m000Person2){
							aoData.push(
									{"name": 'sCriteria_m000Person2', "value": m000Person2}
							);
						}
	
						var m000Person3 = $('#filter_m000Person3 input').val();
						if(m000Person3){
							aoData.push(
									{"name": 'sCriteria_m000Person3', "value": m000Person3}
							);
						}
	
						var m000Person4 = $('#filter_m000Person4 input').val();
						if(m000Person4){
							aoData.push(
									{"name": 'sCriteria_m000Person4', "value": m000Person4}
							);
						}
	
						var m000Person5 = $('#filter_m000Person5 input').val();
						if(m000Person5){
							aoData.push(
									{"name": 'sCriteria_m000Person5', "value": m000Person5}
							);
						}
	
						var m000Port = $('#filter_m000Port input').val();
						if(m000Port){
							aoData.push(
									{"name": 'sCriteria_m000Port', "value": m000Port}
							);
						}
	
						var m000PreExpiredAlert = $('#filter_m000PreExpiredAlert input').val();
						if(m000PreExpiredAlert){
							aoData.push(
									{"name": 'sCriteria_m000PreExpiredAlert', "value": m000PreExpiredAlert}
							);
						}
	
						var m000PreparationTime = $('#filter_m000PreparationTime input').val();
						if(m000PreparationTime){
							aoData.push(
									{"name": 'sCriteria_m000PreparationTime', "value": m000PreparationTime}
							);
						}
	
						var m000ProgressDisc = $('#filter_m000ProgressDisc input').val();
						if(m000ProgressDisc){
							aoData.push(
									{"name": 'sCriteria_m000ProgressDisc', "value": m000ProgressDisc}
							);
						}
	
						var m000ReminderDMKm1 = $('#filter_m000ReminderDMKm1 input').val();
						if(m000ReminderDMKm1){
							aoData.push(
									{"name": 'sCriteria_m000ReminderDMKm1', "value": m000ReminderDMKm1}
							);
						}
	
						var m000ReminderDMKm2 = $('#filter_m000ReminderDMKm2 input').val();
						if(m000ReminderDMKm2){
							aoData.push(
									{"name": 'sCriteria_m000ReminderDMKm2', "value": m000ReminderDMKm2}
							);
						}
	
						var m000ReminderDMOp1 = $('#filter_m000ReminderDMOp1 input').val();
						if(m000ReminderDMOp1){
							aoData.push(
									{"name": 'sCriteria_m000ReminderDMOp1', "value": m000ReminderDMOp1}
							);
						}
	
						var m000ReminderDMOp2 = $('#filter_m000ReminderDMOp2 input').val();
						if(m000ReminderDMOp2){
							aoData.push(
									{"name": 'sCriteria_m000ReminderDMOp2', "value": m000ReminderDMOp2}
							);
						}
	
						var m000ReminderEmailKm1 = $('#filter_m000ReminderEmailKm1 input').val();
						if(m000ReminderEmailKm1){
							aoData.push(
									{"name": 'sCriteria_m000ReminderEmailKm1', "value": m000ReminderEmailKm1}
							);
						}
	
						var m000ReminderEmailKm2 = $('#filter_m000ReminderEmailKm2 input').val();
						if(m000ReminderEmailKm2){
							aoData.push(
									{"name": 'sCriteria_m000ReminderEmailKm2', "value": m000ReminderEmailKm2}
							);
						}
	
						var m000ReminderEmailOp1 = $('#filter_m000ReminderEmailOp1 input').val();
						if(m000ReminderEmailOp1){
							aoData.push(
									{"name": 'sCriteria_m000ReminderEmailOp1', "value": m000ReminderEmailOp1}
							);
						}
	
						var m000ReminderEmailOp2 = $('#filter_m000ReminderEmailOp2 input').val();
						if(m000ReminderEmailOp2){
							aoData.push(
									{"name": 'sCriteria_m000ReminderEmailOp2', "value": m000ReminderEmailOp2}
							);
						}
	
						var m000ReminderSMSKm1 = $('#filter_m000ReminderSMSKm1 input').val();
						if(m000ReminderSMSKm1){
							aoData.push(
									{"name": 'sCriteria_m000ReminderSMSKm1', "value": m000ReminderSMSKm1}
							);
						}
	
						var m000ReminderSMSKm2 = $('#filter_m000ReminderSMSKm2 input').val();
						if(m000ReminderSMSKm2){
							aoData.push(
									{"name": 'sCriteria_m000ReminderSMSKm2', "value": m000ReminderSMSKm2}
							);
						}
	
						var m000ReminderSMSOp1 = $('#filter_m000ReminderSMSOp1 input').val();
						if(m000ReminderSMSOp1){
							aoData.push(
									{"name": 'sCriteria_m000ReminderSMSOp1', "value": m000ReminderSMSOp1}
							);
						}
	
						var m000ReminderSMSOp2 = $('#filter_m000ReminderSMSOp2 input').val();
						if(m000ReminderSMSOp2){
							aoData.push(
									{"name": 'sCriteria_m000ReminderSMSOp2', "value": m000ReminderSMSOp2}
							);
						}
	
						var m000RunningTextInformation = $('#filter_m000RunningTextInformation input').val();
						if(m000RunningTextInformation){
							aoData.push(
									{"name": 'sCriteria_m000RunningTextInformation', "value": m000RunningTextInformation}
							);
						}
	
						var m000SBEDisc = $('#filter_m000SBEDisc input').val();
						if(m000SBEDisc){
							aoData.push(
									{"name": 'sCriteria_m000SBEDisc', "value": m000SBEDisc}
							);
						}
	
						var m000SOHeader = $('#filter_m000SOHeader input').val();
						if(m000SOHeader){
							aoData.push(
									{"name": 'sCriteria_m000SOHeader', "value": m000SOHeader}
							);
						}
	
						var m000SettingRPP = $('#filter_m000SettingRPP input').val();
						if(m000SettingRPP){
							aoData.push(
									{"name": 'sCriteria_m000SettingRPP', "value": m000SettingRPP}
							);
						}
	
						var m000StaAktifCall = $('#filter_m000StaAktifCall input').val();
						if(m000StaAktifCall){
							aoData.push(
									{"name": 'sCriteria_m000StaAktifCall', "value": m000StaAktifCall}
							);
						}
	
						var m000StaAktifDM = $('#filter_m000StaAktifDM input').val();
						if(m000StaAktifDM){
							aoData.push(
									{"name": 'sCriteria_m000StaAktifDM', "value": m000StaAktifDM}
							);
						}
	
						var m000StaAktifEmail = $('#filter_m000StaAktifEmail input').val();
						if(m000StaAktifEmail){
							aoData.push(
									{"name": 'sCriteria_m000StaAktifEmail', "value": m000StaAktifEmail}
							);
						}
	
						var m000StaAktifSMS = $('#filter_m000StaAktifSMS input').val();
						if(m000StaAktifSMS){
							aoData.push(
									{"name": 'sCriteria_m000StaAktifSMS', "value": m000StaAktifSMS}
							);
						}
	
						var m000StaDel = $('#filter_m000StaDel input').val();
						if(m000StaDel){
							aoData.push(
									{"name": 'sCriteria_m000StaDel', "value": m000StaDel}
							);
						}
	
						var m000StaEnablePartTambahanWeb = $('#filter_m000StaEnablePartTambahanWeb input').val();
						if(m000StaEnablePartTambahanWeb){
							aoData.push(
									{"name": 'sCriteria_m000StaEnablePartTambahanWeb', "value": m000StaEnablePartTambahanWeb}
							);
						}
	
						var m000StaLihatDataBengkelLain = $('#filter_m000StaLihatDataBengkelLain input').val();
						if(m000StaLihatDataBengkelLain){
							aoData.push(
									{"name": 'sCriteria_m000StaLihatDataBengkelLain', "value": m000StaLihatDataBengkelLain}
							);
						}
	
						var m000StaSalon = $('#filter_m000StaSalon input').val();
						if(m000StaSalon){
							aoData.push(
									{"name": 'sCriteria_m000StaSalon', "value": m000StaSalon}
							);
						}
	
						var m000StaWaktuAkhir = $('#filter_m000StaWaktuAkhir input').val();
						if(m000StaWaktuAkhir){
							aoData.push(
									{"name": 'sCriteria_m000StaWaktuAkhir', "value": m000StaWaktuAkhir}
							);
						}
	
						var m000Tax = $('#filter_m000Tax input').val();
						if(m000Tax){
							aoData.push(
									{"name": 'sCriteria_m000Tax', "value": m000Tax}
							);
						}
	
						var m000TextCatatanDM = $('#filter_m000TextCatatanDM input').val();
						if(m000TextCatatanDM){
							aoData.push(
									{"name": 'sCriteria_m000TextCatatanDM', "value": m000TextCatatanDM}
							);
						}

						var m000TglJamWaktuAkhir = $('#search_m000TglJamWaktuAkhir').val();
						var m000TglJamWaktuAkhirDay = $('#search_m000TglJamWaktuAkhir_day').val();
						var m000TglJamWaktuAkhirMonth = $('#search_m000TglJamWaktuAkhir_month').val();
						var m000TglJamWaktuAkhirYear = $('#search_m000TglJamWaktuAkhir_year').val();
						
						if(m000TglJamWaktuAkhir){
							aoData.push(
									{"name": 'sCriteria_m000TglJamWaktuAkhir', "value": "date.struct"},
									{"name": 'sCriteria_m000TglJamWaktuAkhir_dp', "value": m000TglJamWaktuAkhir},
									{"name": 'sCriteria_m000TglJamWaktuAkhir_day', "value": m000TglJamWaktuAkhirDay},
									{"name": 'sCriteria_m000TglJamWaktuAkhir_month', "value": m000TglJamWaktuAkhirMonth},
									{"name": 'sCriteria_m000TglJamWaktuAkhir_year', "value": m000TglJamWaktuAkhirYear}
							);
						}
	
						var m000ToleransiAmbilWO = $('#filter_m000ToleransiAmbilWO input').val();
						if(m000ToleransiAmbilWO){
							aoData.push(
									{"name": 'sCriteria_m000ToleransiAmbilWO', "value": m000ToleransiAmbilWO}
							);
						}
	
						var m000ToleransiIDRPerJob = $('#filter_m000ToleransiIDRPerJob input').val();
						if(m000ToleransiIDRPerJob){
							aoData.push(
									{"name": 'sCriteria_m000ToleransiIDRPerJob', "value": m000ToleransiIDRPerJob}
							);
						}
	
						var m000ToleransiIDRPerProses = $('#filter_m000ToleransiIDRPerProses input').val();
						if(m000ToleransiIDRPerProses){
							aoData.push(
									{"name": 'sCriteria_m000ToleransiIDRPerProses', "value": m000ToleransiIDRPerProses}
							);
						}
	
						var m000UserName = $('#filter_m000UserName input').val();
						if(m000UserName){
							aoData.push(
									{"name": 'sCriteria_m000UserName', "value": m000UserName}
							);
						}
	
						var m000VersionAplikasi = $('#filter_m000VersionAplikasi input').val();
						if(m000VersionAplikasi){
							aoData.push(
									{"name": 'sCriteria_m000VersionAplikasi', "value": m000VersionAplikasi}
							);
						}

						var m000WaktuNotifikasiTargetClockOffJob = $('#search_m000WaktuNotifikasiTargetClockOffJob').val();
						var m000WaktuNotifikasiTargetClockOffJobDay = $('#search_m000WaktuNotifikasiTargetClockOffJob_day').val();
						var m000WaktuNotifikasiTargetClockOffJobMonth = $('#search_m000WaktuNotifikasiTargetClockOffJob_month').val();
						var m000WaktuNotifikasiTargetClockOffJobYear = $('#search_m000WaktuNotifikasiTargetClockOffJob_year').val();
						
						if(m000WaktuNotifikasiTargetClockOffJob){
							aoData.push(
									{"name": 'sCriteria_m000WaktuNotifikasiTargetClockOffJob', "value": "date.struct"},
									{"name": 'sCriteria_m000WaktuNotifikasiTargetClockOffJob_dp', "value": m000WaktuNotifikasiTargetClockOffJob},
									{"name": 'sCriteria_m000WaktuNotifikasiTargetClockOffJob_day', "value": m000WaktuNotifikasiTargetClockOffJobDay},
									{"name": 'sCriteria_m000WaktuNotifikasiTargetClockOffJob_month', "value": m000WaktuNotifikasiTargetClockOffJobMonth},
									{"name": 'sCriteria_m000WaktuNotifikasiTargetClockOffJob_year', "value": m000WaktuNotifikasiTargetClockOffJobYear}
							);
						}

						var m000WaktuSessionLog = $('#search_m000WaktuSessionLog').val();
						var m000WaktuSessionLogDay = $('#search_m000WaktuSessionLog_day').val();
						var m000WaktuSessionLogMonth = $('#search_m000WaktuSessionLog_month').val();
						var m000WaktuSessionLogYear = $('#search_m000WaktuSessionLog_year').val();
						
						if(m000WaktuSessionLog){
							aoData.push(
									{"name": 'sCriteria_m000WaktuSessionLog', "value": "date.struct"},
									{"name": 'sCriteria_m000WaktuSessionLog_dp', "value": m000WaktuSessionLog},
									{"name": 'sCriteria_m000WaktuSessionLog_day', "value": m000WaktuSessionLogDay},
									{"name": 'sCriteria_m000WaktuSessionLog_month', "value": m000WaktuSessionLogMonth},
									{"name": 'sCriteria_m000WaktuSessionLog_year', "value": m000WaktuSessionLogYear}
							);
						}
	
						var m000mMinusFee = $('#filter_m000mMinusFee input').val();
						if(m000mMinusFee){
							aoData.push(
									{"name": 'sCriteria_m000mMinusFee', "value": m000mMinusFee}
							);
						}
	
						var m000staJenisJamKerja = $('#filter_m000staJenisJamKerja input').val();
						if(m000staJenisJamKerja){
							aoData.push(
									{"name": 'sCriteria_m000staJenisJamKerja', "value": m000staJenisJamKerja}
							);
						}
	
						var m000staVendorCatByCc = $('#filter_m000staVendorCatByCc input').val();
						if(m000staVendorCatByCc){
							aoData.push(
									{"name": 'sCriteria_m000staVendorCatByCc', "value": m000staVendorCatByCc}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
