<%@ page import="com.kombos.administrasi.GeneralParameter" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'generalParameter.label', default: 'GeneralParameter')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="edit-generalParameter-delivery" class="content scaffold-edit general-parameter" role="main">
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${generalParameterInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${generalParameterInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:javascript>
			var updateStatus;
			$(function(){ 
				updateStatus = function(data){
                    $("#version").val(data.version);
					toastr.success('<div>'+data.message+'</div>');
				};
                $('.percentage').autoNumeric('init',{
                    vMin:'0',
                    vMax:'100',
                    mDec: '2',
                    aSep:''
                });
                $('.rupiah').autoNumeric('init',{
                    vMin:'0',
                    vMax:'1000000000000',
                    mDec: null,
                    aSep:''
                });
			});
			</g:javascript>
			<g:formRemote class="form-horizontal" name="edit" on404="alert('not found!');" onSuccess="updateStatus(data);"
				url="[controller: 'generalParameter', action:'update']">
				<g:hiddenField name="id" value="${generalParameterInstance?.id}" />
				<g:hiddenField name="version" value="${generalParameterInstance?.version}" />
				<fieldset class="form">
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000BufferDeliveryTimeGR', 'error')} ">
						<label class="control-label" for="m000BufferDeliveryTimeGR">
							<g:message code="generalParameter.m000BufferDeliveryTimeGR.label" default="Buffer Delivery time GR" /><span class="required-indicator">*</span>
							
						</label>
						<div class="controls">
						<select id="m000BufferDeliveryTimeGR_hour" name="m000BufferDeliveryTimeGR_hour" style="width: 60px" required="">
							<g:each var="i" in="${ (0..<24) }">
							    <g:if test="${i < 10}">
							    	<g:if test="${i==generalParameterInstance?.m000BufferDeliveryTimeGR?.getHours()}">
							    		<option value="${i}" selected="">0${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">0${i}</option>
							    	</g:else>
							    </g:if>
							    <g:else>
							    	<g:if test="${i==generalParameterInstance?.m000BufferDeliveryTimeGR?.getHours()}">
							    		<option value="${i}" selected="">${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">${i}</option>
							    	</g:else>
							    </g:else>
							</g:each>
				        </select> H :
				        <select id="m000BufferDeliveryTimeGR_minute" name="m000BufferDeliveryTimeGR_minute" style="width: 60px" required="">
				           <g:each var="i" in="${ (0..<60) }">
				           		<g:if test="${i < 10}">
							    	<g:if test="${i==generalParameterInstance?.m000BufferDeliveryTimeGR?.getMinutes()}">
							    		<option value="${i}" selected="">0${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">0${i}</option>
							    	</g:else>
							    </g:if>
							    <g:else>
							    	<g:if test="${i==generalParameterInstance?.m000BufferDeliveryTimeGR?.getMinutes()}">
							    		<option value="${i}" selected="">${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">${i}</option>
							    	</g:else>
							    </g:else>
							 </g:each>
				        </select> m
						&nbsp;setelah proses produksi
						</div>
					</div>
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000BufferDeliveryTimeBP', 'error')} ">
						<label class="control-label" for="m000BufferDeliveryTimeBP">
							<g:message code="generalParameter.m000BufferDeliveryTimeBP.label" default="Buffer Delivery time BP" /><span class="required-indicator">*</span>
							
						</label>
						<div class="controls">
						<select id="m000BufferDeliveryTimeBP_hour" name="m000BufferDeliveryTimeBP_hour" style="width: 60px" required="">
							<g:each var="i" in="${ (0..<24) }">
							    <g:if test="${i < 10}">
							    	<g:if test="${i==generalParameterInstance?.m000BufferDeliveryTimeBP?.getHours()}">
							    		<option value="${i}" selected="">0${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">0${i}</option>
							    	</g:else>
							    </g:if>
							    <g:else>
							    	<g:if test="${i==generalParameterInstance?.m000BufferDeliveryTimeBP?.getHours()}">
							    		<option value="${i}" selected="">${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">${i}</option>
							    	</g:else>
							    </g:else>
							</g:each>
				        </select> H :
				        <select id="m000BufferDeliveryTimeBP_minute" name="m000BufferDeliveryTimeBP_minute" style="width: 60px" required="">
				           <g:each var="i" in="${ (0..<60) }">
				           		<g:if test="${i < 10}">
							    	<g:if test="${i==generalParameterInstance?.m000BufferDeliveryTimeBP?.getMinutes()}">
							    		<option value="${i}" selected="">0${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">0${i}</option>
							    	</g:else>
							    </g:if>
							    <g:else>
							    	<g:if test="${i==generalParameterInstance?.m000BufferDeliveryTimeBP?.getMinutes()}">
							    		<option value="${i}" selected="">${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">${i}</option>
							    	</g:else>
							    </g:else>
							 </g:each>
				        </select> m
						&nbsp;setelah proses produksi
						</div>
					</div>
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000MaksBayarCashRefund', 'error')} ">
						<label class="control-label" for="m000MaksBayarCashRefund">
							<g:message code="generalParameter.m000MaksBayarCashRefund.label" default="Maks. Cash Refund" /><span class="required-indicator">*</span>
							
						</label>
						<div class="controls">
						<g:textField class="rupiah" name="m000MaksBayarCashRefund" value="${generalParameterInstance.m000MaksBayarCashRefund}" required=""/>
						</div>
					</div>
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000Tax', 'error')} ">
						<label class="control-label" for="m000Tax">
							<g:message code="generalParameter.m000Tax.label" default="PPN/Tax" /><span class="required-indicator">*</span>
							
						</label>
						<div class="controls">
						<g:textField class="percentage" name="m000Tax" value="${generalParameterInstance.m000Tax}" required=""/>
						&nbsp;%
						</div>
					</div>
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000PersenPPh', 'error')} ">
						<label class="control-label" for="m000PersenPPh">
							<g:message code="generalParameter.m000PersenPPh.label" default="PPh23" /><span class="required-indicator">*</span>
							
						</label>
						<div class="controls">
						<g:textField class="percentage" name="m000PersenPPh" value="${generalParameterInstance.m000PersenPPh}" required=""/>
						&nbsp;%
						</div>
					</div>
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000FormatSMSNotifikasi', 'error')} ">
						<label class="control-label" for="m000FormatSMSNotifikasi">
							<g:message code="generalParameter.m000FormatSMSNotifikasi.label" default="Format SMS Notifikasi Penyerahan" /><span class="required-indicator">*</span>
							
						</label>
						<div class="controls">
						<span class="span3">
						<g:textArea rows="5" cols="50" name="m000FormatSMSNotifikasi" value="${generalParameterInstance?.m000FormatSMSNotifikasi}" required="" maxlength="160"/>
						</span>
						<span class="span6 offset2">
							<span id="appointment-workshop">&lt;Workshop&gt;</span>&nbsp;|&nbsp;
							<span id="appointment-cabang">&lt;Cabang&gt;</span>&nbsp;|&nbsp;
							<span id="appointment-nama-customer">&lt;NamaCustomer&gt;</span>&nbsp;|&nbsp;
							<span id="appointment-jenis-sb">&lt;JenisSB&gt;</span>&nbsp;|&nbsp;
							<span id="appointment-tanggal-appointment">&lt;TanggalAppointment&gt;</span>&nbsp;|&nbsp;
							<span id="appointment-jam-appoinment">&lt;JamAppointment&gt;</span>&nbsp;|&nbsp;
							<span id="appointment-base-model">&lt;BaseModel&gt;</span>&nbsp;|&nbsp;
							<span id="appointment-nopol">&lt;Nopol&gt;</span>&nbsp;|&nbsp;
							<span id="appointment-km">&lt;Km&gt;</span>
						</span>
						</div>
					</div>
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000InvoiceFooter', 'error')} ">
						<label class="control-label" for="m000InvoiceFooter">
							<g:message code="generalParameter.m000InvoiceFooter.label" default="Footer Text Invoice Untuk Account Bank Pembayaran metode transfer " /><span class="required-indicator">*</span>
							
						</label>
						<div class="controls">
						<g:textArea rows="5" cols="50" name="m000InvoiceFooter" value="${generalParameterInstance?.m000InvoiceFooter}" required="" maxlength="160"/>
						</div>
					</div>
					<fieldset>
						<legend>Tujuan Invoice TWC</legend>
						<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000NamaTWC', 'error')} ">
							<label class="control-label" for="m000NamaTWC">
								<g:message code="generalParameter.m000NamaTWC.label" default="Nama" /><span class="required-indicator">*</span>
								
							</label>
							<div class="controls">
							<g:textField name="m000NamaTWC" value="${generalParameterInstance?.m000NamaTWC}" required="" maxlength="50"/>
							</div>
						</div>
						<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000AlamatTWC', 'error')} ">
							<label class="control-label" for="m000AlamatTWC">
								<g:message code="generalParameter.m000AlamatTWC.label" default="Alamat" /><span class="required-indicator">*</span>
								
							</label>
							<div class="controls">
							<g:textArea name="m000AlamatTWC" value="${generalParameterInstance?.m000AlamatTWC}" required="" maxlength="50"/>
							</div>
						</div>
						<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000NPWPTWC', 'error')} ">
							<label class="control-label" for="m000NPWPTWC">
								<g:message code="generalParameter.m000NPWPTWC.label" default="NPWP" />
								
							</label>
							<div class="controls">
							<g:textField name="m000NPWPTWC" value="${generalParameterInstance?.m000NPWPTWC}" maxlength="50"/>
							</div>
						</div>
					</fieldset>
					<fieldset>
						<legend>Tujuan Invoice PWC</legend>
						<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000NamaPWC', 'error')} ">
							<label class="control-label" for="m000NamaPWC">
								<g:message code="generalParameter.m000NamaPWC.label" default="Nama" /><span class="required-indicator">*</span>
								
							</label>
							<div class="controls">
							<g:textField name="m000NamaPWC" value="${generalParameterInstance?.m000NamaPWC}" required="" maxlength="50"/>
							</div>
						</div>
						<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000AlamatPWC', 'error')} ">
							<label class="control-label" for="m000AlamatPWC">
								<g:message code="generalParameter.m000AlamatPWC.label" default="Alamat" /><span class="required-indicator">*</span>
								
							</label>
							<div class="controls">
							<g:textArea name="m000AlamatPWC" value="${generalParameterInstance?.m000AlamatPWC}" required="" maxlength="50"/>
							</div>
						</div>
						<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000NPWPPWC', 'error')} ">
							<label class="control-label" for="m000NPWPPWC">
								<g:message code="generalParameter.m000NPWPPWC.label" default="NPWP" />
								
							</label>
							<div class="controls">
							<g:textField name="m000NPWPPWC" value="${generalParameterInstance?.m000NPWPPWC}" maxlength="50"/>
							</div>
						</div>
					</fieldset>
					<fieldset>
						<legend>&nbsp;</legend>
						<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000ToleransiDeliveryGR', 'error')} ">
							<label class="control-label" for="m000ToleransiDeliveryGR">
								<g:message code="generalParameter.m000ToleransiDeliveryGR.label" default="Waktu toleransi keterlambatan delivery GR" /><span class="required-indicator">*</span>
								
							</label>
							<div class="controls">
							<select id="m000ToleransiDeliveryGR_hour" name="m000ToleransiDeliveryGR_hour" style="width: 60px" required="">
							<g:each var="i" in="${ (0..<24) }">
							    <g:if test="${i < 10}">
							    	<g:if test="${i==generalParameterInstance?.m000ToleransiDeliveryGR?.getHours()}">
							    		<option value="${i}" selected="">0${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">0${i}</option>
							    	</g:else>
							    </g:if>
							    <g:else>
							    	<g:if test="${i==generalParameterInstance?.m000ToleransiDeliveryGR?.getHours()}">
							    		<option value="${i}" selected="">${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">${i}</option>
							    	</g:else>
							    </g:else>
							</g:each>
				        </select> H :
				        <select id="m000ToleransiDeliveryGR_minute" name="m000ToleransiDeliveryGR_minute" style="width: 60px" required="">
				           <g:each var="i" in="${ (0..<60) }">
				           		<g:if test="${i < 10}">
							    	<g:if test="${i==generalParameterInstance?.m000ToleransiDeliveryGR?.getMinutes()}">
							    		<option value="${i}" selected="">0${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">0${i}</option>
							    	</g:else>
							    </g:if>
							    <g:else>
							    	<g:if test="${i==generalParameterInstance?.m000ToleransiDeliveryGR?.getMinutes()}">
							    		<option value="${i}" selected="">${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">${i}</option>
							    	</g:else>
							    </g:else>
							 </g:each>
				        </select> m
							</div>
						</div>
						<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000ToleransiDeliveryBP', 'error')} ">
							<label class="control-label" for="m000ToleransiDeliveryBP">
								<g:message code="generalParameter.m000ToleransiDeliveryBP.label" default="Waktu toleransi keterlambatan delivery BP" /><span class="required-indicator">*</span>
								
							</label>
							<div class="controls">
							<select id="m000ToleransiDeliveryBP_hour" name="m000ToleransiDeliveryBP_hour" style="width: 60px" required="">
							<g:each var="i" in="${ (0..<24) }">
							    <g:if test="${i < 10}">
							    	<g:if test="${i==generalParameterInstance?.m000ToleransiDeliveryBP?.getHours()}">
							    		<option value="${i}" selected="">0${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">0${i}</option>
							    	</g:else>
							    </g:if>
							    <g:else>
							    	<g:if test="${i==generalParameterInstance?.m000ToleransiDeliveryBP?.getHours()}">
							    		<option value="${i}" selected="">${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">${i}</option>
							    	</g:else>
							    </g:else>
							</g:each>
				        </select> H :
				        <select id="m000ToleransiDeliveryBP_minute" name="m000ToleransiDeliveryBP_minute" style="width: 60px" required="">
				           <g:each var="i" in="${ (0..<60) }">
				           		<g:if test="${i < 10}">
							    	<g:if test="${i==generalParameterInstance?.m000ToleransiDeliveryBP?.getMinutes()}">
							    		<option value="${i}" selected="">0${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">0${i}</option>
							    	</g:else>
							    </g:if>
							    <g:else>
							    	<g:if test="${i==generalParameterInstance?.m000ToleransiDeliveryBP?.getMinutes()}">
							    		<option value="${i}" selected="">${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">${i}</option>
							    	</g:else>
							    </g:else>
							 </g:each>
				        </select> m
							</div>
						</div>
					</fieldset>


				</fieldset>
				<fieldset class="buttons controls">
					<g:actionSubmit class="btn btn-primary save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
				</fieldset>
			</g:formRemote>
		</div>
	</body>
</html>
