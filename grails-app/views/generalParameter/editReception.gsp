<%@ page import="com.kombos.administrasi.GeneralParameter" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'generalParameter.label', default: 'GeneralParameter')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="edit-generalParameter-reception" class="content scaffold-edit general-parameter" role="main">
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${generalParameterInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${generalParameterInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:javascript>
			var updateStatus;
			$(function(){ 
				updateStatus = function(data){
                    $("#version").val(data.version);
					toastr.success('<div>'+data.message+'</div>');
				};
                $('#m000PersenKenaikanJobSublet').autoNumeric('init',{
                    vMin:'0',
                    vMax:'100',
                    mDec: '2',
                    aSep:''
                });

                $('.gpnumber').autoNumeric('init',{
                    vMin:'0',
                    vMax:'9999',
                    mDec: null,
                    aSep:''
                });
			});
			</g:javascript>
			<g:formRemote class="form-horizontal" name="edit" on404="alert('not found!');" onSuccess="updateStatus(data);" 
				url="[controller: 'generalParameter', action:'update']">
				<g:hiddenField name="id" value="${generalParameterInstance?.id}" />
				<g:hiddenField name="version" value="${generalParameterInstance?.version}" />
				<fieldset class="form">
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000StaSalon', 'error')} ">
	<label class="control-label" for="m000StaSalon">
		<g:message code="generalParameter.m000StaSalon.label" default="Status Stall Salon" />
	</label>
	<div class="controls">
		<g:radioGroup name="m000StaSalon" values="[1,0]" value="${generalParameterInstance?.m000StaSalon}" labels="['Ya','Tidak']">
			${it.radio} <g:message code="${it.label}" />
		</g:radioGroup>
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000JmlSKipCustomer', 'error')} ">
	<label class="control-label" for="m000JmlSKipCustomer">
		<g:message code="generalParameter.m000JmlSKipCustomer.label" default="Jumlah skip nomor antrian jika saat dipanggil Customer tidak ada" /><span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
        <g:textField class="gpnumber" name="m000JmlSKipCustomer" value="${generalParameterInstance?.m000JmlSKipCustomer}" required=""/>
	%{--<g:field type="number" onkeypress="return isNumberKey(event)" min="0" max="9999" onKeyDown="if(this.value.length==4) return false;" name="m000JmlSKipCustomer" value="${generalParameterInstance.m000JmlSKipCustomer}" required=""/>--}%
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000PersenKenaikanJobSublet', 'error')} ">
	<label class="control-label" for="m000PersenKenaikanJobSublet">
		<g:message code="generalParameter.m000PersenKenaikanJobSublet.label" default="Persen Kenaikan Harga Job Sublet" /><span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m000PersenKenaikanJobSublet" value="${generalParameterInstance.m000PersenKenaikanJobSublet}" required=""/>
    %{--<g:field type="number" name="m000PersenKenaikanJobSublet" onkeypress="return isNumberKey(event)" min="0" max="100" value="${generalParameterInstance.m000PersenKenaikanJobSublet}" required=""/>--}%
	&nbsp;%
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000SOHeader', 'error')} ">
	<label class="control-label" for="m000SOHeader">
		<g:message code="generalParameter.m000SOHeader.label" default="Service Order Header Text" />
		
	</label>
	<div class="controls">
	<g:textArea rows="5" cols="50" maxlength="1000" name="m000SOHeader" value="${generalParameterInstance?.m000SOHeader}" maxlength="255"/>
	</div>
</div>
				</fieldset>
				<fieldset class="buttons controls">
					<g:actionSubmit class="btn btn-primary save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
				</fieldset>
			</g:formRemote>
		</div>
	</body>
</html>
