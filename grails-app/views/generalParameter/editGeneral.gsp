<%@ page import="com.kombos.administrasi.GeneralParameter" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'generalParameter.label', default: 'GeneralParameter')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="edit-generalParameter-general" class="content scaffold-edit general-parameter" role="main">
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${generalParameterInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${generalParameterInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:javascript>
			var updateStatus;
			$(function(){ 
				updateStatus = function(data){
				    $("#version").val(data.version);
					toastr.success('<div>'+data.message+'</div>');
				};
                $('#m000Port').autoNumeric('init',{
                    vMin:'0',
                    vMax:'32767',
                    mDec: null,
                    aSep:''
                });
                $('.gpnumber999').autoNumeric('init',{
                    vMin:'0',
                    vMax:'999',
                    mDec: null,
                    aSep:''
                });
                $('#m000StaWaktuAkhir').change(function() {
                    if(this.checked) {
                        $('#m000TglJamWaktuAkhir').attr("required", "required");
                        $('#m000TglJamWaktuAkhir_hour').attr("required", "required");
                        $('#m000TglJamWaktuAkhir_minute').attr("required", "required");
                    }else{
                        $('#m000TglJamWaktuAkhir').removeAttr("required");
                        $('#m000TglJamWaktuAkhir_hour').removeAttr("required");
                        $('#m000TglJamWaktuAkhir_minute').removeAttr("required");
                    }
                });
                if($('#m000StaWaktuAkhir').checked){
                    $('#m000TglJamWaktuAkhir').attr("required", "required");
                    $('#m000TglJamWaktuAkhir_hour').attr("required", "required");
                    $('#m000TglJamWaktuAkhir_minute').attr("required", "required");
                }else{
                    $('#m000TglJamWaktuAkhir').removeAttr("required");
                    $('#m000TglJamWaktuAkhir_hour').removeAttr("required");
                    $('#m000TglJamWaktuAkhir_minute').removeAttr("required");
                }
			});
			</g:javascript>
			<g:formRemote class="form-horizontal" name="edit" on404="alert('not found!');" onSuccess="updateStatus(data);" url="[controller: 'generalParameter', action:'update']">
				<g:hiddenField name="id" value="${generalParameterInstance?.id}" />
				<g:hiddenField name="version" value="${generalParameterInstance?.version}" />
				<fieldset class="form">
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000MaxRecordPerPage', 'error')} required">
						<label class="control-label" for="m000MaxRecordPerPage">
							<g:message code="generalParameter.m000MaxRecordPerPage.label" default="Max Record Per Page" /><span class="required-indicator">*</span>
						</label>
						<div class="controls">
                            <g:textField class="gpnumber999" name="m000MaxRecordPerPage" value="${generalParameterInstance?.m000MaxRecordPerPage}" required=""/>&nbsp;Record
						%{--<g:field type="number" onkeypress="return isNumberKey(event)" max="999" name="m000MaxRecordPerPage" value="${generalParameterInstance.m000MaxRecordPerPage}" required=""/> &nbsp;Record--}%
						</div>
					</div>
					<fieldset>
					<legend>Margin Dokumen</legend>
					<g:if test="${marginList.size() > 0}">
						<g:hiddenField name="margin" value="true" />
					</g:if>
					<g:each var="margin" in="${marginList}">
					    <div class="control-group fieldcontain required">
							<label class="control-label" for="margin-${margin.id}">
								${margin.namaDokumen.m007NamaDokumen}
							</label>
							<div class="controls">
                                <g:textField class="gpnumber999" name="margin-${margin.id}" value="${margin.m018MarginAtas}"/>
							%{--<g:field type="number" onkeypress="return isNumberKey(event)" max="999" name="margin-${margin.id}" value="${margin.m018MarginAtas}"/>--}%
							</div>
						</div>
					</g:each>
					</fieldset>
					<fieldset>
						<legend>SMTP Mail Setting</legend>
						<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000NamaHostOrIPAddress', 'error')} required">
							<label class="control-label" for="m000NamaHostOrIPAddress">
								<g:message code="generalParameter.m000NamaHostOrIPAddress.label" default="Nama Host/IP Address" /><span class="required-indicator">*</span>
								
							</label>
							<div class="controls">
							<g:textField name="m000NamaHostOrIPAddress" value="${generalParameterInstance?.m000NamaHostOrIPAddress}" maxlength="50" required=""/>
							</div>
						</div>
						<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000Port', 'error')} required">
							<label class="control-label" for="m000Port">
								<g:message code="generalParameter.m000Port.label" default="Port" /><span class="required-indicator">*</span>
								
							</label>
							<div class="controls">
							<g:textField name="m000Port" value="${generalParameterInstance?.m000Port}" required=""/>
							</div>
						</div>
						<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000UserName', 'error')} required">
							<label class="control-label" for="m000UserName">
								<g:message code="generalParameter.m000UserName.label" default="UserName" /><span class="required-indicator">*</span>
								
							</label>
							<div class="controls">
							<g:textField name="m000UserName" value="${generalParameterInstance?.m000UserName}" required="" maxlength="50"/>
							</div>
						</div>
						<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000Password', 'error')} required">
							<label class="control-label" for="m000Password">
								<g:message code="generalParameter.m000Password.label" default="Password" /><span class="required-indicator">*</span>
								
							</label>
							<div class="controls">
							<g:textField name="m000Password" value="${generalParameterInstance?.m000Password}" required="" maxlength="50"/>
							</div>
						</div>
					</fieldset>
					<fieldset>
						<legend>Running Text Information</legend>	
						<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000RunningTextInformation', 'error')} ">
							<label class="control-label" for="m000RunningTextInformation">
								<g:message code="generalParameter.m000RunningTextInformation.label" default="Running Text" />
								
							</label>
							<div class="controls">
							<g:textArea name="m000RunningTextInformation" value="${generalParameterInstance?.m000RunningTextInformation}" maxlength="255"/>
							</div>
						</div>
						<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000TglJamWaktuAkhir', 'error')} ">
							<label class="control-label" for="m000TglJamWaktuAkhir">
								<g:message code="generalParameter.m000TglJamWaktuAkhir.label" default="Waktu Akhir" />
								
							</label>
							<div class="controls">
                            <g:checkBox name="m000StaWaktuAkhir" value="${'1' == generalParameterInstance?.m000StaWaktuAkhir}"></g:checkBox>

							<ba:datePicker name="m000TglJamWaktuAkhir" precision="day" value="${generalParameterInstance?.m000TglJamWaktuAkhir}" format="dd/MM/yyyy"/>
							<select id="m000TglJamWaktuAkhir_hour" name="m000TglJamWaktuAkhir_hour" style="width: 60px" >
							<g:each var="i" in="${ (0..<24) }">
							    <g:if test="${i < 10}">
							    	<g:if test="${i==generalParameterInstance?.m000TglJamWaktuAkhir?.getHours()}">
							    		<option value="${i}" selected="">0${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">0${i}</option>
							    	</g:else>
							    </g:if>
							    <g:else>
							    	<g:if test="${i==generalParameterInstance?.m000TglJamWaktuAkhir?.getHours()}">
							    		<option value="${i}" selected="">${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">${i}</option>
							    	</g:else>
							    </g:else>
							</g:each>
				        </select> H :
				        <select id="m000TglJamWaktuAkhir_minute" name="m000TglJamWaktuAkhir_minute" style="width: 60px">
				           <g:each var="i" in="${ (0..<60) }">
				           		<g:if test="${i < 10}">
							    	<g:if test="${i==generalParameterInstance?.m000TglJamWaktuAkhir?.getMinutes()}">
							    		<option value="${i}" selected="">0${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">0${i}</option>
							    	</g:else>
							    </g:if>
							    <g:else>
							    	<g:if test="${i==generalParameterInstance?.m000TglJamWaktuAkhir?.getMinutes()}">
							    		<option value="${i}" selected="">${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">${i}</option>
							    	</g:else>
							    </g:else>
							 </g:each>
				        </select> m
							</div>
							</div>
					</fieldset>
				</fieldset>
				<fieldset class="buttons controls">
					<g:actionSubmit class="btn btn-primary save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
				</fieldset>
			</g:formRemote>
		</div>
	</body>
</html>
