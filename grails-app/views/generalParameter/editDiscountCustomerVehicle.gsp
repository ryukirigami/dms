er <%@ page import="com.kombos.administrasi.GeneralParameter" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'generalParameter.label', default: 'GeneralParameter')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
		<style>
		#edit-discount-customer-vehicle input[type=text], #edit-discount-customer-vehicle input[type=number]{
			width: 136px;
		}
		</style>
	</head>
	<body>
		<div id="edit-generalParameter-dcv" class="span12 content scaffold-edit general-parameter" role="main" style="margin-left: 0;">
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${generalParameterInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${generalParameterInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:javascript>
				var addCustomCriteria;
				var deleteCustomCriteria;
				var filterCustomCriteria;
				var resetCustomCriteria;
				var setDiscountCustVehicle;
				var updateOperator;
				var updateValue;
				$(function(){
					updateOperator = function(e, id) { 
						var $e = $(e);
						var selectedClass = $e.find('option:selected').attr("class");
						var operator = $e.next();
						operator.empty();
						if(selectedClass === 'string'){
						<g:each in="${['=', 'LIKE']}" var="op">
				    	operator.append('<option value="${op}">${op}</option>');
				    	</g:each>
						} else if(selectedClass === 'date'){
						<g:each in="${['=', 'BETWEEN']}" var="op">
							operator.append('<option value="${op}">${op}</option>');
				    	</g:each>
						}
						var value = operator.next();
							value.remove();
							$e.parent().append('<span><input type="text"	style="min-height: 30px;" class="criteriaValue span2 row" value="" name="criteriaValue'+id+'" id="criteriaValue'+id+'" data-date-format="dd/mm/yyyy"></span>');
							if(selectedClass === 'date'){
								$('#criteriaValue'+id).datepicker().on('changeDate', function(ev) {
									$(this).datepicker('hide');}).data('datepicker');
							}
					};
					updateValue = function(e, id) { 
						var $e = $(e);
						var op = $e.val();
						if(op === 'BETWEEN'){
							var value = $e.next();
							value.remove();
							$e.parent()
								.append('<span><input type="text"	style="min-height: 30px;" class="criteriaValue span2 row" value="" name="criteriaValueFrom'+id+'" id="criteriaValueFrom'+id+'" data-date-format="dd/mm/yyyy">&nbsp;AND&nbsp;<input type="text"	style="min-height: 30px;" class="criteriaValue span2 row" value="" name="criteriaValueTo'+id+'" id="criteriaValueTo'+id+'" data-date-format="dd/mm/yyyy"></span>');
								var tglFrom_dp = $('#criteriaValueFrom'+id).datepicker().on('changeDate', function(ev) {
									$(this).datepicker('hide');}).data('datepicker');
								$('#criteriaValueTo'+id).datepicker({
										onRender: function(date) {
											return date.valueOf() < tglFrom_dp.date.valueOf() ? 'disabled' : '';
										}
									}).on('changeDate', function(ev) {
									$(this).datepicker('hide');}).data('datepicker');
						} else {
							var value = $e.next();
							var field = $e.previous();
							var selectedClass = field.find('option:selected').attr("class");
							value.remove();
							$e.parent().append('<span><input type="text"	style="min-height: 30px;" class="criteriaValue span2 row" value="" name="criteriaValue'+id+'" id="criteriaValue'+id+'" data-date-format="dd/mm/yyyy"></span>');
							if(selectedClass === 'date'){
								$('#criteriaValue'+id).datepicker().on('changeDate', function(ev) {
									$(this).datepicker('hide');}).data('datepicker');
							}
						}					
					};
					var tglAwal_dp = $('#tglAwal').datepicker()
						.on('changeDate', function(ev) {
							var newDate = new Date(ev.date);
							$('#tglAwal_day').val(newDate.getDate());
							$('#tglAwal_month').val(newDate.getMonth()+1);
							$('#tglAwal_year').val(newDate.getFullYear());
							
							if (ev.date.valueOf() > tglAkhir_dp.date.valueOf()) {
								var newDate = new Date(ev.date)
								newDate.setDate(newDate.getDate() + 1);
								tglAkhir_dp.setValue(newDate);
							}
							$(this).datepicker('hide');}).data('datepicker');
					var tglAkhir_dp = $('#tglAkhir').datepicker({
							onRender: function(date) {
								return date.valueOf() < tglAwal_dp.date.valueOf() ? 'disabled' : '';
							}
						})
						.on('changeDate', function(ev) {
							var newDate = new Date(ev.date);
							$('#tglAkhir_day').val(newDate.getDate());
							$('#tglAkhir_month').val(newDate.getMonth()+1);
							$('#tglAkhir_year').val(newDate.getFullYear());
							$(this).datepicker('hide');}).data('datepicker');
							
					$('#diskonPart').autoNumeric('init',{
						vMin:'0.00',
						vMax:'100.00',
						aSep:''
					});
					$('#diskonJasa').autoNumeric('init',{
						vMin:'0.00',
						vMax:'100.00',
						aSep:''
					});
							
					addCustomCriteria = function(){
		    			var c = $('#custom-criteria div').length;
		    			$('#custom-criteria').append('<div class="controls row-fluid" />');
		    			$('#custom-criteria div:last-child').append('<select class="criteriaField span5" name="criteriaField'+c+'" id="criteriaField'+c+'" onchange="updateOperator(this, '+c+');"/>');
		    			<g:each in="${searchCriterias}" var="col" status="idx">
		    				$('#custom-criteria div:last-child .criteriaField').append('<option	value="${col.field}" class="${col.type}">${col.label}</option>');
		    			</g:each>
				    	$('#custom-criteria div:last-child').append('<select class="criteriaOperator span2" name="criteriaOperator'+c+'" id="criteriaOperator'+c+'" onchange="updateValue(this, '+c+');"/>');
				    	<g:each in="${['=', 'BETWEEN']}" var="op">
				    	$('#custom-criteria div:last-child .criteriaOperator').append('<option value="${op}">${op}</option>');
				    	</g:each>
				    	$('#custom-criteria div:last-child').append('<span><input type="text"	style="min-height: 30px;" class="criteriaValue span2 row" value="" name="criteriaValue'+c+'" id="criteriaValue'+c+'" data-date-format="dd/mm/yyyy"></span>');
				   		
				   		$(".criteriaValue").bind('keypress', function(e) {
							var code = (e.keyCode ? e.keyCode : e.which);
							if(code == 13) { //Enter keycode
								filterCustomCriteria();
							}	
						}); 
						
						$('#criteriaValue'+c).datepicker().data('datepicker');
		   			}
		    
				    deleteCustomCriteria = function(){
				    	$('#custom-criteria div:last-child').remove();
				    }
				    
				    filterCustomCriteria = function(){
				    	//expandTableLayout();
				    
				    	//var oTable = $('#formDatatables_datatable').dataTable();    	
						if(dcvTable)
				    		dcvTable.fnDraw();
				    }
				    
				    resetCustomCriteria = function(){
				    	$('.criteriaValue').val('');
				    }
					
					setDiscountCustVehicle = function(){
						var checkVehicle =[];
						$("#dcv-table tbody .row-select").each(function() {
							if(this.checked){
								var id = $(this).next("input:hidden").val();
								checkVehicle.push(id);
							}
						});
						
						if(checkVehicle.length<1){
								alert('Anda belum memilih data yang akan ditambahkan');
						}
										
						$("#edit-discount-customer-vehicle #ids").val(JSON.stringify(checkVehicle));
						
						var formParams = $("#edit-discount-customer-vehicle").serialize();
						
						$.ajax({
							url:'${request.contextPath}/generalParameter/setDiscountCustVehicle',
							type: "POST", 
							data : formParams,
							success : function(data){
								if(dcvTable)
									dcvTable.fnDraw();
							},
							error: function(xhr, textStatus, errorThrown) {
									alert('Internal Server Error');
							}
					    });
					}					
	    		});
			</g:javascript>
			<div id="criteria-placeholder" class="container-fluid span12">
				<div class="navbar box-header no-border">
					<span class="pull-left param">Criteria:&nbsp;</span>
				</div>
				<div class="box no-border non-collapsible">
					<div class="control-group">						
						<a class="btn create" href="javascript:void(0);" onclick="addCustomCriteria();">Add Filter</a> 
						<a class="btn create" href="javascript:void(0);" onclick="deleteCustomCriteria();">Delete Filter</a>
						<a class="btn create" href="javascript:void(0);" onclick="filterCustomCriteria();">Search</a>
					</div>
					<div id="custom-criteria" class="control-group criteria"></div>
					<a class="btn create" href="javascript:void(0);" onclick="resetCustomCriteria();">Clear Filter</a>
				</div>
			</div>
			
			<div class="span12" id="dcv-table" style="margin-left: 0;">			
				<g:render template="dcvDataTables" />
			</div>
			<br/>
			<br/>
			<fieldset id="dcv-form" class="buttons controls" style="margin-left: 0; padding-top: 25px;">	
				<g:form name="edit-discount-customer-vehicle" on404="alert('not found!');" onSuccess="reloadGeneralParameterTable();" update="generalParameter-form"
					url="[controller: 'generalParameter', action:'update']">
					<input type="hidden" name="ids" id="ids" value="">
					<fieldset class="form">
					<div
						id="input-tanggal" class="span6" style="margin-left: 0;">
						<label class="control-label" for="tglAwal"> Masa Berlaku Diskon	
						</label>
						<div class="controls">
							<input type="hidden" value="date.struct" name="tglAwal"><input type="hidden" value="" id="tglAwal_day" name="tglAwal_day"><input type="hidden" value="" id="tglAwal_month" name="tglAwal_month"><input type="hidden" value="" id="tglAwal_year" name="tglAwal_year"><input type="text"  id="tglAwal" value="" name="tglAwal_dp" data-date-format="yyyy-mm-dd">
							s.d.
							<input type="hidden" value="date.struct" name="tglAkhir"><input type="hidden" value="" id="tglAkhir_day" name="tglAkhir_day"><input type="hidden" value="" id="tglAkhir_month" name="tglAkhir_month"><input type="hidden" value="" id="tglAkhir_year" name="tglAkhir_year"><input type="text"  id="tglAkhir" value="" name="tglAkhir_dp" data-date-format="yyyy-mm-dd">
						</div>
					</div>	
					<div
						class="span3" style="margin-left: 0;">
						<label class="control-label" for="diskonPart">Diskon Parts	
						</label>
						<div class="controls">
							<g:field type="number" name="diskonPart" value="" />&nbsp;%
						</div>
					</div>		
					<div
						class="span3" style="margin-left: 0;">
						<label class="control-label" for="diskonJasa">Diskon Jasa	
						</label>
						<div class="controls">
							<g:field type="number" name="diskonJasa" value="" />&nbsp;%
						</div>
					</div>			
					</fieldset>
					<fieldset class="buttons controls">
						<a class="btn cancel" href="javascript:void(0);"
	                       onclick="expandTableLayout();"><g:message
	                            code="default.button.cancel.label" default="Cancel" /></a>
						<g:field type="button" onclick="setDiscountCustVehicle();" class="btn btn-primary create" name="set-discount" id="set-discount" value="Set Discount" />
						<g:actionSubmit class="btn delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" formnovalidate="" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
					</fieldset>
				</g:form>
			</fieldset>	
		</div>
	</body>
</html>
