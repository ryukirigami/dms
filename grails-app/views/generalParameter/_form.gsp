<%@ page import="com.kombos.administrasi.CompanyDealer; com.kombos.administrasi.GeneralParameter" %>



<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'companyDealer', 'error')} ">
	<label class="control-label" for="companyDealer">
		<g:message code="generalParameter.companyDealer.label" default="Company Dealer" />
		
	</label>
	<div class="controls">
	<g:select id="companyDealer" name="companyDealer.id" from="${CompanyDealer.list()}" optionKey="id" required="" value="${generalParameterInstance?.companyDealer?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'createdBy', 'error')} ">
	<label class="control-label" for="createdBy">
		<g:message code="generalParameter.createdBy.label" default="Created By" />
		
	</label>
	<div class="controls">
	<g:textField name="createdBy" value="${generalParameterInstance?.createdBy}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="generalParameter.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${generalParameterInstance?.lastUpdProcess}" />
	</div>
</div>



<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000AlamatFTP', 'error')} ">
	<label class="control-label" for="m000AlamatFTP">
		<g:message code="generalParameter.m000AlamatFTP.label" default="M000 Alamat FTP" />
		
	</label>
	<div class="controls">
	<g:textField name="m000AlamatFTP" value="${generalParameterInstance?.m000AlamatFTP}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000AlamatPWC', 'error')} ">
	<label class="control-label" for="m000AlamatPWC">
		<g:message code="generalParameter.m000AlamatPWC.label" default="M000 Alamat PWC" />
		
	</label>
	<div class="controls">
	<g:textField name="m000AlamatPWC" value="${generalParameterInstance?.m000AlamatPWC}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000AlamatTWC', 'error')} ">
	<label class="control-label" for="m000AlamatTWC">
		<g:message code="generalParameter.m000AlamatTWC.label" default="M000 Alamat TWC" />
		
	</label>
	<div class="controls">
	<g:textField name="m000AlamatTWC" value="${generalParameterInstance?.m000AlamatTWC}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000BufferDeliveryTimeBP', 'error')} ">
	<label class="control-label" for="m000BufferDeliveryTimeBP">
		<g:message code="generalParameter.m000BufferDeliveryTimeBP.label" default="M000 Buffer Delivery Time BP" />
		
	</label>
	<div class="controls">
	<ba:datePicker name="m000BufferDeliveryTimeBP" precision="day" value="${generalParameterInstance?.m000BufferDeliveryTimeBP}" format="yyyy-MM-dd"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000BufferDeliveryTimeGR', 'error')} ">
	<label class="control-label" for="m000BufferDeliveryTimeGR">
		<g:message code="generalParameter.m000BufferDeliveryTimeGR.label" default="M000 Buffer Delivery Time GR" />
		
	</label>
	<div class="controls">
	<ba:datePicker name="m000BufferDeliveryTimeGR" precision="day" value="${generalParameterInstance?.m000BufferDeliveryTimeGR}" format="yyyy-MM-dd"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000BulanWarningSertifikat', 'error')} ">
	<label class="control-label" for="m000BulanWarningSertifikat">
		<g:message code="generalParameter.m000BulanWarningSertifikat.label" default="M000 Bulan Warning Sertifikat" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000BulanWarningSertifikat" value="${generalParameterInstance.m000BulanWarningSertifikat}" />
	</div>
</div>



<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000DefaultPUK', 'error')} ">
	<label class="control-label" for="m000DefaultPUK">
		<g:message code="generalParameter.m000DefaultPUK.label" default="M000 Default PUK" />
		
	</label>
	<div class="controls">
	<g:textField name="m000DefaultPUK" value="${generalParameterInstance?.m000DefaultPUK}" />
	</div>
</div>





<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000FormatEmail', 'error')} ">
	<label class="control-label" for="m000FormatEmail">
		<g:message code="generalParameter.m000FormatEmail.label" default="M000 Format Email" />
		
	</label>
	<div class="controls">
	<g:textField name="m000FormatEmail" value="${generalParameterInstance?.m000FormatEmail}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000FormatSMSHMinus', 'error')} ">
	<label class="control-label" for="m000FormatSMSHMinus">
		<g:message code="generalParameter.m000FormatSMSHMinus.label" default="M000 Format SMSHM inus" />
		
	</label>
	<div class="controls">
	<g:textField name="m000FormatSMSHMinus" value="${generalParameterInstance?.m000FormatSMSHMinus}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000FormatSMSJMinus', 'error')} ">
	<label class="control-label" for="m000FormatSMSJMinus">
		<g:message code="generalParameter.m000FormatSMSJMinus.label" default="M000 Format SMSJM inus" />
		
	</label>
	<div class="controls">
	<g:textField name="m000FormatSMSJMinus" value="${generalParameterInstance?.m000FormatSMSJMinus}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000FormatSMSNotifikasi', 'error')} ">
	<label class="control-label" for="m000FormatSMSNotifikasi">
		<g:message code="generalParameter.m000FormatSMSNotifikasi.label" default="M000 Format SMSN otifikasi" />
		
	</label>
	<div class="controls">
	<g:textField name="m000FormatSMSNotifikasi" value="${generalParameterInstance?.m000FormatSMSNotifikasi}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000FormatSMSService', 'error')} ">
	<label class="control-label" for="m000FormatSMSService">
		<g:message code="generalParameter.m000FormatSMSService.label" default="M000 Format SMSS ervice" />
		
	</label>
	<div class="controls">
	<g:textField name="m000FormatSMSService" value="${generalParameterInstance?.m000FormatSMSService}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000HMinParameterReserved', 'error')} ">
	<label class="control-label" for="m000HMinParameterReserved">
		<g:message code="generalParameter.m000HMinParameterReserved.label" default="M000 HM in Parameter Reserved" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000HMinParameterReserved" value="${generalParameterInstance.m000HMinParameterReserved}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000HMinusCekDokAsuransi', 'error')} ">
	<label class="control-label" for="m000HMinusCekDokAsuransi">
		<g:message code="generalParameter.m000HMinusCekDokAsuransi.label" default="M000 HM inus Cek Dok Asuransi" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000HMinusCekDokAsuransi" value="${generalParameterInstance.m000HMinusCekDokAsuransi}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000HMinusFee', 'error')} ">
	<label class="control-label" for="m000HMinusFee">
		<g:message code="generalParameter.m000HMinusFee.label" default="M000 HM inus Fee" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000HMinusFee" value="${generalParameterInstance.m000HMinusFee}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000HMinusReminderEmail', 'error')} ">
	<label class="control-label" for="m000HMinusReminderEmail">
		<g:message code="generalParameter.m000HMinusReminderEmail.label" default="M000 HM inus Reminder Email" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000HMinusReminderEmail" value="${generalParameterInstance.m000HMinusReminderEmail}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000HMinusReminderSMS', 'error')} ">
	<label class="control-label" for="m000HMinusReminderSMS">
		<g:message code="generalParameter.m000HMinusReminderSMS.label" default="M000 HM inus Reminder SMS" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000HMinusReminderSMS" value="${generalParameterInstance.m000HMinusReminderSMS}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000HMinusReminderSTNK', 'error')} ">
	<label class="control-label" for="m000HMinusReminderSTNK">
		<g:message code="generalParameter.m000HMinusReminderSTNK.label" default="M000 HM inus Reminder STNK" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000HMinusReminderSTNK" value="${generalParameterInstance.m000HMinusReminderSTNK}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000HMinusSMSAppointment', 'error')} ">
	<label class="control-label" for="m000HMinusSMSAppointment">
		<g:message code="generalParameter.m000HMinusSMSAppointment.label" default="M000 HM inus SMSA ppointment" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000HMinusSMSAppointment" value="${generalParameterInstance.m000HMinusSMSAppointment}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000HMinusTglReminderDM', 'error')} ">
	<label class="control-label" for="m000HMinusTglReminderDM">
		<g:message code="generalParameter.m000HMinusTglReminderDM.label" default="M000 HM inus Tgl Reminder DM" />
		
	</label>
	<div class="controls">
	<g:textField name="m000HMinusTglReminderDM" value="${generalParameterInstance?.m000HMinusTglReminderDM}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000HPlusCancelBookingFee', 'error')} ">
	<label class="control-label" for="m000HPlusCancelBookingFee">
		<g:message code="generalParameter.m000HPlusCancelBookingFee.label" default="M000 HP lus Cancel Booking Fee" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000HPlusCancelBookingFee" value="${generalParameterInstance.m000HPlusCancelBookingFee}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000HPlusUndanganFA', 'error')} ">
	<label class="control-label" for="m000HPlusUndanganFA">
		<g:message code="generalParameter.m000HPlusUndanganFA.label" default="M000 HP lus Undangan FA" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000HPlusUndanganFA" value="${generalParameterInstance.m000HPlusUndanganFA}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000InvoiceFooter', 'error')} ">
	<label class="control-label" for="m000InvoiceFooter">
		<g:message code="generalParameter.m000InvoiceFooter.label" default="M000 Invoice Footer" />
		
	</label>
	<div class="controls">
	<g:textField name="m000InvoiceFooter" value="${generalParameterInstance?.m000InvoiceFooter}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000JMinusSMSAppointment', 'error')} ">
	<label class="control-label" for="m000JMinusSMSAppointment">
		<g:message code="generalParameter.m000JMinusSMSAppointment.label" default="M000 JM inus SMSA ppointment" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000JMinusSMSAppointment" value="${generalParameterInstance.m000JMinusSMSAppointment}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000JPlusSMSTerlambat', 'error')} ">
	<label class="control-label" for="m000JPlusSMSTerlambat">
		<g:message code="generalParameter.m000JPlusSMSTerlambat.label" default="M000 JP lus SMST erlambat" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000JPlusSMSTerlambat" value="${generalParameterInstance.m000JPlusSMSTerlambat}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000JamKirimEmail', 'error')} ">
	<label class="control-label" for="m000JamKirimEmail">
		<g:message code="generalParameter.m000JamKirimEmail.label" default="M000 Jam Kirim Email" />
		
	</label>
	<div class="controls">
	<ba:datePicker name="m000JamKirimEmail" precision="day" value="${generalParameterInstance?.m000JamKirimEmail}" format="yyyy-MM-dd"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000JamKirimSMS', 'error')} ">
	<label class="control-label" for="m000JamKirimSMS">
		<g:message code="generalParameter.m000JamKirimSMS.label" default="M000 Jam Kirim SMS" />
		
	</label>
	<div class="controls">
	<ba:datePicker name="m000JamKirimSMS" precision="day" value="${generalParameterInstance?.m000JamKirimSMS}" format="yyyy-MM-dd"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000JamKirimSMSSTNK', 'error')} ">
	<label class="control-label" for="m000JamKirimSMSSTNK">
		<g:message code="generalParameter.m000JamKirimSMSSTNK.label" default="M000 Jam Kirim SMSSTNK" />
		
	</label>
	<div class="controls">
	<ba:datePicker name="m000JamKirimSMSSTNK" precision="day" value="${generalParameterInstance?.m000JamKirimSMSSTNK}" format="yyyy-MM-dd"/>
	</div>
</div>



<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000JmlMaxInvitation', 'error')} ">
	<label class="control-label" for="m000JmlMaxInvitation">
		<g:message code="generalParameter.m000JmlMaxInvitation.label" default="M000 Jml Max Invitation" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000JmlMaxInvitation" value="${generalParameterInstance.m000JmlMaxInvitation}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000JmlMaxItemWAC', 'error')} ">
	<label class="control-label" for="m000JmlMaxItemWAC">
		<g:message code="generalParameter.m000JmlMaxItemWAC.label" default="M000 Jml Max Item WAC" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000JmlMaxItemWAC" value="${generalParameterInstance.m000JmlMaxItemWAC}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000JmlMaxKirimSMSFollowUp', 'error')} ">
	<label class="control-label" for="m000JmlMaxKirimSMSFollowUp">
		<g:message code="generalParameter.m000JmlMaxKirimSMSFollowUp.label" default="M000 Jml Max Kirim SMSF ollow Up" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000JmlMaxKirimSMSFollowUp" value="${generalParameterInstance.m000JmlMaxKirimSMSFollowUp}" />
	</div>
</div>



<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000JmlToleransiAsuransiBP', 'error')} ">
	<label class="control-label" for="m000JmlToleransiAsuransiBP">
		<g:message code="generalParameter.m000JmlToleransiAsuransiBP.label" default="M000 Jml Toleransi Asuransi BP" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000JmlToleransiAsuransiBP" value="${generalParameterInstance.m000JmlToleransiAsuransiBP}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000KonfirmasiHariSebelum', 'error')} ">
	<label class="control-label" for="m000KonfirmasiHariSebelum">
		<g:message code="generalParameter.m000KonfirmasiHariSebelum.label" default="M000 Konfirmasi Hari Sebelum" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000KonfirmasiHariSebelum" value="${generalParameterInstance.m000KonfirmasiHariSebelum}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000KonfirmasiJamSebelum', 'error')} ">
	<label class="control-label" for="m000KonfirmasiJamSebelum">
		<g:message code="generalParameter.m000KonfirmasiJamSebelum.label" default="M000 Konfirmasi Jam Sebelum" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000KonfirmasiJamSebelum" value="${generalParameterInstance.m000KonfirmasiJamSebelum}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000KonfirmasiTerlambat', 'error')} ">
	<label class="control-label" for="m000KonfirmasiTerlambat">
		<g:message code="generalParameter.m000KonfirmasiTerlambat.label" default="M000 Konfirmasi Terlambat" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000KonfirmasiTerlambat" value="${generalParameterInstance.m000KonfirmasiTerlambat}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000LamaCuciMobil', 'error')} ">
	<label class="control-label" for="m000LamaCuciMobil">
		<g:message code="generalParameter.m000LamaCuciMobil.label" default="M000 Lama Cuci Mobil" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000LamaCuciMobil" value="${generalParameterInstance.m000LamaCuciMobil}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000LamaWaktuMinCuci', 'error')} ">
	<label class="control-label" for="m000LamaWaktuMinCuci">
		<g:message code="generalParameter.m000LamaWaktuMinCuci.label" default="M000 Lama Waktu Min Cuci" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000LamaWaktuMinCuci" value="${generalParameterInstance.m000LamaWaktuMinCuci}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000MaksBayarCashRefund', 'error')} ">
	<label class="control-label" for="m000MaksBayarCashRefund">
		<g:message code="generalParameter.m000MaksBayarCashRefund.label" default="M000 Maks Bayar Cash Refund" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000MaksBayarCashRefund" value="${generalParameterInstance.m000MaksBayarCashRefund}" />
	</div>
</div>



<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000MaxPersenKembaliBookingFee', 'error')} ">
	<label class="control-label" for="m000MaxPersenKembaliBookingFee">
		<g:message code="generalParameter.m000MaxPersenKembaliBookingFee.label" default="M000 Max Persen Kembali Booking Fee" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000MaxPersenKembaliBookingFee" value="${generalParameterInstance.m000MaxPersenKembaliBookingFee}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000MaxRecordPerPage', 'error')} ">
	<label class="control-label" for="m000MaxRecordPerPage">
		<g:message code="generalParameter.m000MaxRecordPerPage.label" default="M000 Max Record Per Page" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000MaxRecordPerPage" value="${generalParameterInstance.m000MaxRecordPerPage}" />
	</div>
</div>

x

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000NPWPPWC', 'error')} ">
	<label class="control-label" for="m000NPWPPWC">
		<g:message code="generalParameter.m000NPWPPWC.label" default="M000 NPWPPWC" />
		
	</label>
	<div class="controls">
	<g:textField name="m000NPWPPWC" value="${generalParameterInstance?.m000NPWPPWC}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000NPWPTWC', 'error')} ">
	<label class="control-label" for="m000NPWPTWC">
		<g:message code="generalParameter.m000NPWPTWC.label" default="M000 NPWPTWC" />
		
	</label>
	<div class="controls">
	<g:textField name="m000NPWPTWC" value="${generalParameterInstance?.m000NPWPTWC}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000NamaHostOrIPAddress', 'error')} ">
	<label class="control-label" for="m000NamaHostOrIPAddress">
		<g:message code="generalParameter.m000NamaHostOrIPAddress.label" default="M000 Nama Host Or IPA ddress" />
		
	</label>
	<div class="controls">
	<g:textField name="m000NamaHostOrIPAddress" value="${generalParameterInstance?.m000NamaHostOrIPAddress}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000NamaPWC', 'error')} ">
	<label class="control-label" for="m000NamaPWC">
		<g:message code="generalParameter.m000NamaPWC.label" default="M000 Nama PWC" />
		
	</label>
	<div class="controls">
	<g:textField name="m000NamaPWC" value="${generalParameterInstance?.m000NamaPWC}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000NamaTWC', 'error')} ">
	<label class="control-label" for="m000NamaTWC">
		<g:message code="generalParameter.m000NamaTWC.label" default="M000 Nama TWC" />
		
	</label>
	<div class="controls">
	<g:textField name="m000NamaTWC" value="${generalParameterInstance?.m000NamaTWC}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000NominalMaterai', 'error')} ">
	<label class="control-label" for="m000NominalMaterai">
		<g:message code="generalParameter.m000NominalMaterai.label" default="M000 Nominal Materai" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000NominalMaterai" value="${generalParameterInstance.m000NominalMaterai}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000Password', 'error')} ">
	<label class="control-label" for="m000Password">
		<g:message code="generalParameter.m000Password.label" default="M000 Password" />
		
	</label>
	<div class="controls">
	<g:textField name="m000Password" value="${generalParameterInstance?.m000Password}" />
	</div>
</div>





<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000PersenMaxGunakanMaterial', 'error')} ">
	<label class="control-label" for="m000PersenMaxGunakanMaterial">
		<g:message code="generalParameter.m000PersenMaxGunakanMaterial.label" default="M000 Persen Max Gunakan Material" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000PersenMaxGunakanMaterial" value="${generalParameterInstance.m000PersenMaxGunakanMaterial}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000PersenPPN', 'error')} ">
	<label class="control-label" for="m000PersenPPN">
		<g:message code="generalParameter.m000PersenPPN.label" default="M000 Persen PPN" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000PersenPPN" value="${generalParameterInstance.m000PersenPPN}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000PersenPPh', 'error')} ">
	<label class="control-label" for="m000PersenPPh">
		<g:message code="generalParameter.m000PersenPPh.label" default="M000 Persen PP h" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000PersenPPh" value="${generalParameterInstance.m000PersenPPh}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000PersenPainting', 'error')} ">
	<label class="control-label" for="m000PersenPainting">
		<g:message code="generalParameter.m000PersenPainting.label" default="M000 Persen Painting" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000PersenPainting" value="${generalParameterInstance.m000PersenPainting}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000PersenPelepasanPartBP', 'error')} ">
	<label class="control-label" for="m000PersenPelepasanPartBP">
		<g:message code="generalParameter.m000PersenPelepasanPartBP.label" default="M000 Persen Pelepasan Part BP" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000PersenPelepasanPartBP" value="${generalParameterInstance.m000PersenPelepasanPartBP}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000PersenPemasanganPartBP', 'error')} ">
	<label class="control-label" for="m000PersenPemasanganPartBP">
		<g:message code="generalParameter.m000PersenPemasanganPartBP.label" default="M000 Persen Pemasangan Part BP" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000PersenPemasanganPartBP" value="${generalParameterInstance.m000PersenPemasanganPartBP}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000PersenPemborong', 'error')} ">
	<label class="control-label" for="m000PersenPemborong">
		<g:message code="generalParameter.m000PersenPemborong.label" default="M000 Persen Pemborong" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000PersenPemborong" value="${generalParameterInstance.m000PersenPemborong}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000PersenPoleshing', 'error')} ">
	<label class="control-label" for="m000PersenPoleshing">
		<g:message code="generalParameter.m000PersenPoleshing.label" default="M000 Persen Poleshing" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000PersenPoleshing" value="${generalParameterInstance.m000PersenPoleshing}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000PersenPreparation', 'error')} ">
	<label class="control-label" for="m000PersenPreparation">
		<g:message code="generalParameter.m000PersenPreparation.label" default="M000 Persen Preparation" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000PersenPreparation" value="${generalParameterInstance.m000PersenPreparation}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000PersenWorkshop', 'error')} ">
	<label class="control-label" for="m000PersenWorkshop">
		<g:message code="generalParameter.m000PersenWorkshop.label" default="M000 Persen Workshop" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000PersenWorkshop" value="${generalParameterInstance.m000PersenWorkshop}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000Person1', 'error')} ">
	<label class="control-label" for="m000Person1">
		<g:message code="generalParameter.m000Person1.label" default="M000 Person1" />
		
	</label>
	<div class="controls">
	<g:textField name="m000Person1" value="${generalParameterInstance?.m000Person1}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000Person2', 'error')} ">
	<label class="control-label" for="m000Person2">
		<g:message code="generalParameter.m000Person2.label" default="M000 Person2" />
		
	</label>
	<div class="controls">
	<g:textField name="m000Person2" value="${generalParameterInstance?.m000Person2}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000Person3', 'error')} ">
	<label class="control-label" for="m000Person3">
		<g:message code="generalParameter.m000Person3.label" default="M000 Person3" />
		
	</label>
	<div class="controls">
	<g:textField name="m000Person3" value="${generalParameterInstance?.m000Person3}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000Person4', 'error')} ">
	<label class="control-label" for="m000Person4">
		<g:message code="generalParameter.m000Person4.label" default="M000 Person4" />
		
	</label>
	<div class="controls">
	<g:textField name="m000Person4" value="${generalParameterInstance?.m000Person4}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000Person5', 'error')} ">
	<label class="control-label" for="m000Person5">
		<g:message code="generalParameter.m000Person5.label" default="M000 Person5" />
		
	</label>
	<div class="controls">
	<g:textField name="m000Person5" value="${generalParameterInstance?.m000Person5}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000Port', 'error')} ">
	<label class="control-label" for="m000Port">
		<g:message code="generalParameter.m000Port.label" default="M000 Port" />
		
	</label>
	<div class="controls">
	<g:textField name="m000Port" value="${generalParameterInstance?.m000Port}" />
	</div>
</div>



<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000PreparationTime', 'error')} ">
	<label class="control-label" for="m000PreparationTime">
		<g:message code="generalParameter.m000PreparationTime.label" default="M000 Preparation Time" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000PreparationTime" value="${generalParameterInstance.m000PreparationTime}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000ProgressDisc', 'error')} ">
	<label class="control-label" for="m000ProgressDisc">
		<g:message code="generalParameter.m000ProgressDisc.label" default="M000 Progress Disc" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000ProgressDisc" value="${generalParameterInstance.m000ProgressDisc}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000ReminderDMKm1', 'error')} ">
	<label class="control-label" for="m000ReminderDMKm1">
		<g:message code="generalParameter.m000ReminderDMKm1.label" default="M000 Reminder DMK m1" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000ReminderDMKm1" value="${generalParameterInstance.m000ReminderDMKm1}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000ReminderDMKm2', 'error')} ">
	<label class="control-label" for="m000ReminderDMKm2">
		<g:message code="generalParameter.m000ReminderDMKm2.label" default="M000 Reminder DMK m2" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000ReminderDMKm2" value="${generalParameterInstance.m000ReminderDMKm2}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000ReminderDMOp1', 'error')} ">
	<label class="control-label" for="m000ReminderDMOp1">
		<g:message code="generalParameter.m000ReminderDMOp1.label" default="M000 Reminder DMO p1" />
		
	</label>
	<div class="controls">
	<g:textField name="m000ReminderDMOp1" value="${generalParameterInstance?.m000ReminderDMOp1}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000ReminderDMOp2', 'error')} ">
	<label class="control-label" for="m000ReminderDMOp2">
		<g:message code="generalParameter.m000ReminderDMOp2.label" default="M000 Reminder DMO p2" />
		
	</label>
	<div class="controls">
	<g:textField name="m000ReminderDMOp2" value="${generalParameterInstance?.m000ReminderDMOp2}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000ReminderEmailKm1', 'error')} ">
	<label class="control-label" for="m000ReminderEmailKm1">
		<g:message code="generalParameter.m000ReminderEmailKm1.label" default="M000 Reminder Email Km1" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000ReminderEmailKm1" value="${generalParameterInstance.m000ReminderEmailKm1}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000ReminderEmailKm2', 'error')} ">
	<label class="control-label" for="m000ReminderEmailKm2">
		<g:message code="generalParameter.m000ReminderEmailKm2.label" default="M000 Reminder Email Km2" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000ReminderEmailKm2" value="${generalParameterInstance.m000ReminderEmailKm2}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000ReminderEmailOp1', 'error')} ">
	<label class="control-label" for="m000ReminderEmailOp1">
		<g:message code="generalParameter.m000ReminderEmailOp1.label" default="M000 Reminder Email Op1" />
		
	</label>
	<div class="controls">
	<g:textField name="m000ReminderEmailOp1" value="${generalParameterInstance?.m000ReminderEmailOp1}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000ReminderEmailOp2', 'error')} ">
	<label class="control-label" for="m000ReminderEmailOp2">
		<g:message code="generalParameter.m000ReminderEmailOp2.label" default="M000 Reminder Email Op2" />
		
	</label>
	<div class="controls">
	<g:textField name="m000ReminderEmailOp2" value="${generalParameterInstance?.m000ReminderEmailOp2}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000ReminderSMSKm1', 'error')} ">
	<label class="control-label" for="m000ReminderSMSKm1">
		<g:message code="generalParameter.m000ReminderSMSKm1.label" default="M000 Reminder SMSK m1" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000ReminderSMSKm1" value="${generalParameterInstance.m000ReminderSMSKm1}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000ReminderSMSKm2', 'error')} ">
	<label class="control-label" for="m000ReminderSMSKm2">
		<g:message code="generalParameter.m000ReminderSMSKm2.label" default="M000 Reminder SMSK m2" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000ReminderSMSKm2" value="${generalParameterInstance.m000ReminderSMSKm2}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000ReminderSMSOp1', 'error')} ">
	<label class="control-label" for="m000ReminderSMSOp1">
		<g:message code="generalParameter.m000ReminderSMSOp1.label" default="M000 Reminder SMSO p1" />
		
	</label>
	<div class="controls">
	<g:textField name="m000ReminderSMSOp1" value="${generalParameterInstance?.m000ReminderSMSOp1}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000ReminderSMSOp2', 'error')} ">
	<label class="control-label" for="m000ReminderSMSOp2">
		<g:message code="generalParameter.m000ReminderSMSOp2.label" default="M000 Reminder SMSO p2" />
		
	</label>
	<div class="controls">
	<g:textField name="m000ReminderSMSOp2" value="${generalParameterInstance?.m000ReminderSMSOp2}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000RunningTextInformation', 'error')} ">
	<label class="control-label" for="m000RunningTextInformation">
		<g:message code="generalParameter.m000RunningTextInformation.label" default="M000 Running Text Information" />
		
	</label>
	<div class="controls">
	<g:textField name="m000RunningTextInformation" value="${generalParameterInstance?.m000RunningTextInformation}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000SBEDisc', 'error')} ">
	<label class="control-label" for="m000SBEDisc">
		<g:message code="generalParameter.m000SBEDisc.label" default="M000 SBED isc" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000SBEDisc" value="${generalParameterInstance.m000SBEDisc}" />
	</div>
</div>



<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000SettingRPP', 'error')} ">
	<label class="control-label" for="m000SettingRPP">
		<g:message code="generalParameter.m000SettingRPP.label" default="M000 Setting RPP" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000SettingRPP" value="${generalParameterInstance.m000SettingRPP}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000StaAktifCall', 'error')} ">
	<label class="control-label" for="m000StaAktifCall">
		<g:message code="generalParameter.m000StaAktifCall.label" default="M000 Sta Aktif Call" />
		
	</label>
	<div class="controls">
	<g:textField name="m000StaAktifCall" value="${generalParameterInstance?.m000StaAktifCall}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000StaAktifDM', 'error')} ">
	<label class="control-label" for="m000StaAktifDM">
		<g:message code="generalParameter.m000StaAktifDM.label" default="M000 Sta Aktif DM" />
		
	</label>
	<div class="controls">
	<g:textField name="m000StaAktifDM" value="${generalParameterInstance?.m000StaAktifDM}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000StaAktifEmail', 'error')} ">
	<label class="control-label" for="m000StaAktifEmail">
		<g:message code="generalParameter.m000StaAktifEmail.label" default="M000 Sta Aktif Email" />
		
	</label>
	<div class="controls">
	<g:textField name="m000StaAktifEmail" value="${generalParameterInstance?.m000StaAktifEmail}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000StaAktifSMS', 'error')} ">
	<label class="control-label" for="m000StaAktifSMS">
		<g:message code="generalParameter.m000StaAktifSMS.label" default="M000 Sta Aktif SMS" />
		
	</label>
	<div class="controls">
	<g:textField name="m000StaAktifSMS" value="${generalParameterInstance?.m000StaAktifSMS}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000StaDel', 'error')} ">
	<label class="control-label" for="m000StaDel">
		<g:message code="generalParameter.m000StaDel.label" default="M000 Sta Del" />
		
	</label>
	<div class="controls">
	<g:textField name="m000StaDel" value="${generalParameterInstance?.m000StaDel}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000StaLihatDataBengkelLain', 'error')} ">
	<label class="control-label" for="m000StaLihatDataBengkelLain">
		<g:message code="generalParameter.m000StaLihatDataBengkelLain.label" default="M000 Sta Lihat Data Bengkel Lain" />
		
	</label>
	<div class="controls">
	<g:textField name="m000StaLihatDataBengkelLain" value="${generalParameterInstance?.m000StaLihatDataBengkelLain}" />
	</div>
</div>



<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000StaWaktuAkhir', 'error')} ">
	<label class="control-label" for="m000StaWaktuAkhir">
		<g:message code="generalParameter.m000StaWaktuAkhir.label" default="M000 Sta Waktu Akhir" />
		
	</label>
	<div class="controls">
	<g:textField name="m000StaWaktuAkhir" value="${generalParameterInstance?.m000StaWaktuAkhir}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000Tax', 'error')} ">
	<label class="control-label" for="m000Tax">
		<g:message code="generalParameter.m000Tax.label" default="M000 Tax" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000Tax" value="${generalParameterInstance.m000Tax}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000TextCatatanDM', 'error')} ">
	<label class="control-label" for="m000TextCatatanDM">
		<g:message code="generalParameter.m000TextCatatanDM.label" default="M000 Text Catatan DM" />
		
	</label>
	<div class="controls">
	<g:textField name="m000TextCatatanDM" value="${generalParameterInstance?.m000TextCatatanDM}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000TglJamWaktuAkhir', 'error')} ">
	<label class="control-label" for="m000TglJamWaktuAkhir">
		<g:message code="generalParameter.m000TglJamWaktuAkhir.label" default="M000 Tgl Jam Waktu Akhir" />
		
	</label>
	<div class="controls">
	<ba:datePicker name="m000TglJamWaktuAkhir" precision="day" value="${generalParameterInstance?.m000TglJamWaktuAkhir}" format="yyyy-MM-dd"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000ToleransiAmbilWO', 'error')} ">
	<label class="control-label" for="m000ToleransiAmbilWO">
		<g:message code="generalParameter.m000ToleransiAmbilWO.label" default="M000 Toleransi Ambil WO" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000ToleransiAmbilWO" value="${generalParameterInstance.m000ToleransiAmbilWO}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000ToleransiDeliveryBP', 'error')} ">
	<label class="control-label" for="m000ToleransiDeliveryBP">
		<g:message code="generalParameter.m000ToleransiDeliveryBP.label" default="M000 Toleransi Delivery BP" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000ToleransiDeliveryBP" value="${generalParameterInstance.m000ToleransiDeliveryBP}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000ToleransiDeliveryGR', 'error')} ">
	<label class="control-label" for="m000ToleransiDeliveryGR">
		<g:message code="generalParameter.m000ToleransiDeliveryGR.label" default="M000 Toleransi Delivery GR" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000ToleransiDeliveryGR" value="${generalParameterInstance.m000ToleransiDeliveryGR}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000ToleransiIDRPerJob', 'error')} ">
	<label class="control-label" for="m000ToleransiIDRPerJob">
		<g:message code="generalParameter.m000ToleransiIDRPerJob.label" default="M000 Toleransi IDRP er Job" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000ToleransiIDRPerJob" value="${generalParameterInstance.m000ToleransiIDRPerJob}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000ToleransiIDRPerProses', 'error')} ">
	<label class="control-label" for="m000ToleransiIDRPerProses">
		<g:message code="generalParameter.m000ToleransiIDRPerProses.label" default="M000 Toleransi IDRP er Proses" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000ToleransiIDRPerProses" value="${generalParameterInstance.m000ToleransiIDRPerProses}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000UserName', 'error')} ">
	<label class="control-label" for="m000UserName">
		<g:message code="generalParameter.m000UserName.label" default="M000 User Name" />
		
	</label>
	<div class="controls">
	<g:textField name="m000UserName" value="${generalParameterInstance?.m000UserName}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000VersionAplikasi', 'error')} ">
	<label class="control-label" for="m000VersionAplikasi">
		<g:message code="generalParameter.m000VersionAplikasi.label" default="M000 Version Aplikasi" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000VersionAplikasi" value="${generalParameterInstance.m000VersionAplikasi}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000WaktuNotifikasiTargetClockOffJob', 'error')} ">
	<label class="control-label" for="m000WaktuNotifikasiTargetClockOffJob">
		<g:message code="generalParameter.m000WaktuNotifikasiTargetClockOffJob.label" default="M000 Waktu Notifikasi Target Clock Off Job" />
		
	</label>
	<div class="controls">
	<ba:datePicker name="m000WaktuNotifikasiTargetClockOffJob" precision="day" value="${generalParameterInstance?.m000WaktuNotifikasiTargetClockOffJob}" format="yyyy-MM-dd"/>
	</div>
</div>



<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000mMinusFee', 'error')} ">
	<label class="control-label" for="m000mMinusFee">
		<g:message code="generalParameter.m000mMinusFee.label" default="M000m Minus Fee" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="m000mMinusFee" value="${generalParameterInstance.m000mMinusFee}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000staJenisJamKerja', 'error')} ">
	<label class="control-label" for="m000staJenisJamKerja">
		<g:message code="generalParameter.m000staJenisJamKerja.label" default="M000sta Jenis Jam Kerja" />
		
	</label>
	<div class="controls">
	<g:textField name="m000staJenisJamKerja" value="${generalParameterInstance?.m000staJenisJamKerja}" />
	</div>
</div>



<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'updatedBy', 'error')} ">
	<label class="control-label" for="updatedBy">
		<g:message code="generalParameter.updatedBy.label" default="Updated By" />
		
	</label>
	<div class="controls">
	<g:textField name="updatedBy" value="${generalParameterInstance?.updatedBy}" />
	</div>
</div>

