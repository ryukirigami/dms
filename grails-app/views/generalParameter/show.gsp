

<%@ page import="com.kombos.administrasi.GeneralParameter" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'generalParameter.label', default: 'GeneralParameter')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteGeneralParameter;

$(function(){ 
	deleteGeneralParameter=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/generalParameter/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadGeneralParameterTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-generalParameter" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="generalParameter"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${generalParameterInstance?.companyDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="companyDealer-label" class="property-label"><g:message
					code="generalParameter.companyDealer.label" default="Company Dealer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="companyDealer-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="companyDealer"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter companyDealer" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:link controller="companyDealer" action="show" id="${generalParameterInstance?.companyDealer?.id}">${generalParameterInstance?.companyDealer?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="generalParameter.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="createdBy"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="generalParameter.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="dateCreated"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:formatDate date="${generalParameterInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="generalParameter.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="lastUpdProcess"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="generalParameter.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="lastUpdated"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:formatDate date="${generalParameterInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000Aging11}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000Aging11-label" class="property-label"><g:message
					code="generalParameter.m000Aging11.label" default="M000 Aging11" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000Aging11-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000Aging11"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000Aging11" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000Aging11"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000Aging12}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000Aging12-label" class="property-label"><g:message
					code="generalParameter.m000Aging12.label" default="M000 Aging12" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000Aging12-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000Aging12"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000Aging12" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000Aging12"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000Aging21}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000Aging21-label" class="property-label"><g:message
					code="generalParameter.m000Aging21.label" default="M000 Aging21" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000Aging21-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000Aging21"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000Aging21" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000Aging21"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000Aging22}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000Aging22-label" class="property-label"><g:message
					code="generalParameter.m000Aging22.label" default="M000 Aging22" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000Aging22-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000Aging22"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000Aging22" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000Aging22"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000Aging31}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000Aging31-label" class="property-label"><g:message
					code="generalParameter.m000Aging31.label" default="M000 Aging31" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000Aging31-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000Aging31"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000Aging31" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000Aging31"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000Aging32}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000Aging32-label" class="property-label"><g:message
					code="generalParameter.m000Aging32.label" default="M000 Aging32" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000Aging32-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000Aging32"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000Aging32" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000Aging32"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000Aging41}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000Aging41-label" class="property-label"><g:message
					code="generalParameter.m000Aging41.label" default="M000 Aging41" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000Aging41-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000Aging41"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000Aging41" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000Aging41"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000Aging42}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000Aging42-label" class="property-label"><g:message
					code="generalParameter.m000Aging42.label" default="M000 Aging42" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000Aging42-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000Aging42"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000Aging42" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000Aging42"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000Aging51}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000Aging51-label" class="property-label"><g:message
					code="generalParameter.m000Aging51.label" default="M000 Aging51" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000Aging51-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000Aging51"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000Aging51" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000Aging51"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000Aging52}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000Aging52-label" class="property-label"><g:message
					code="generalParameter.m000Aging52.label" default="M000 Aging52" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000Aging52-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000Aging52"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000Aging52" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000Aging52"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000AlamatFTP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000AlamatFTP-label" class="property-label"><g:message
					code="generalParameter.m000AlamatFTP.label" default="M000 Alamat FTP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000AlamatFTP-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000AlamatFTP"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000AlamatFTP" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000AlamatFTP"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000AlamatPWC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000AlamatPWC-label" class="property-label"><g:message
					code="generalParameter.m000AlamatPWC.label" default="M000 Alamat PWC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000AlamatPWC-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000AlamatPWC"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000AlamatPWC" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000AlamatPWC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000AlamatTWC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000AlamatTWC-label" class="property-label"><g:message
					code="generalParameter.m000AlamatTWC.label" default="M000 Alamat TWC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000AlamatTWC-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000AlamatTWC"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000AlamatTWC" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000AlamatTWC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000BufferDeliveryTimeBP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000BufferDeliveryTimeBP-label" class="property-label"><g:message
					code="generalParameter.m000BufferDeliveryTimeBP.label" default="M000 Buffer Delivery Time BP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000BufferDeliveryTimeBP-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000BufferDeliveryTimeBP"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000BufferDeliveryTimeBP" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:formatDate date="${generalParameterInstance?.m000BufferDeliveryTimeBP}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000BufferDeliveryTimeGR}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000BufferDeliveryTimeGR-label" class="property-label"><g:message
					code="generalParameter.m000BufferDeliveryTimeGR.label" default="M000 Buffer Delivery Time GR" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000BufferDeliveryTimeGR-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000BufferDeliveryTimeGR"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000BufferDeliveryTimeGR" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:formatDate date="${generalParameterInstance?.m000BufferDeliveryTimeGR}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000BulanWarningSertifikat}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000BulanWarningSertifikat-label" class="property-label"><g:message
					code="generalParameter.m000BulanWarningSertifikat.label" default="M000 Bulan Warning Sertifikat" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000BulanWarningSertifikat-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000BulanWarningSertifikat"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000BulanWarningSertifikat" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000BulanWarningSertifikat"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000DefaultMasaBerlakuAccount}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000DefaultMasaBerlakuAccount-label" class="property-label"><g:message
					code="generalParameter.m000DefaultMasaBerlakuAccount.label" default="M000 Default Masa Berlaku Account" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000DefaultMasaBerlakuAccount-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000DefaultMasaBerlakuAccount"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000DefaultMasaBerlakuAccount" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000DefaultMasaBerlakuAccount"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000DefaultPUK}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000DefaultPUK-label" class="property-label"><g:message
					code="generalParameter.m000DefaultPUK.label" default="M000 Default PUK" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000DefaultPUK-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000DefaultPUK"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000DefaultPUK" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000DefaultPUK"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000DefaultUserPass}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000DefaultUserPass-label" class="property-label"><g:message
					code="generalParameter.m000DefaultUserPass.label" default="M000 Default User Pass" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000DefaultUserPass-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000DefaultUserPass"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000DefaultUserPass" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000DefaultUserPass"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000DisableConcurrentLogin}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000DisableConcurrentLogin-label" class="property-label"><g:message
					code="generalParameter.m000DisableConcurrentLogin.label" default="M000 Disable Concurrent Login" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000DisableConcurrentLogin-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000DisableConcurrentLogin"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000DisableConcurrentLogin" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000DisableConcurrentLogin"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000FormatEmail}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000FormatEmail-label" class="property-label"><g:message
					code="generalParameter.m000FormatEmail.label" default="M000 Format Email" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000FormatEmail-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000FormatEmail"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000FormatEmail" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000FormatEmail"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000FormatSMSHMinus}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000FormatSMSHMinus-label" class="property-label"><g:message
					code="generalParameter.m000FormatSMSHMinus.label" default="M000 Format SMSHM inus" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000FormatSMSHMinus-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000FormatSMSHMinus"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000FormatSMSHMinus" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000FormatSMSHMinus"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000FormatSMSJMinus}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000FormatSMSJMinus-label" class="property-label"><g:message
					code="generalParameter.m000FormatSMSJMinus.label" default="M000 Format SMSJM inus" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000FormatSMSJMinus-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000FormatSMSJMinus"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000FormatSMSJMinus" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000FormatSMSJMinus"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000FormatSMSNotifikasi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000FormatSMSNotifikasi-label" class="property-label"><g:message
					code="generalParameter.m000FormatSMSNotifikasi.label" default="M000 Format SMSN otifikasi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000FormatSMSNotifikasi-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000FormatSMSNotifikasi"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000FormatSMSNotifikasi" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000FormatSMSNotifikasi"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000FormatSMSService}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000FormatSMSService-label" class="property-label"><g:message
					code="generalParameter.m000FormatSMSService.label" default="M000 Format SMSS ervice" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000FormatSMSService-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000FormatSMSService"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000FormatSMSService" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000FormatSMSService"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000HMinParameterReserved}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000HMinParameterReserved-label" class="property-label"><g:message
					code="generalParameter.m000HMinParameterReserved.label" default="M000 HM in Parameter Reserved" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000HMinParameterReserved-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000HMinParameterReserved"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000HMinParameterReserved" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000HMinParameterReserved"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000HMinusCekDokAsuransi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000HMinusCekDokAsuransi-label" class="property-label"><g:message
					code="generalParameter.m000HMinusCekDokAsuransi.label" default="M000 HM inus Cek Dok Asuransi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000HMinusCekDokAsuransi-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000HMinusCekDokAsuransi"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000HMinusCekDokAsuransi" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000HMinusCekDokAsuransi"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000HMinusFee}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000HMinusFee-label" class="property-label"><g:message
					code="generalParameter.m000HMinusFee.label" default="M000 HM inus Fee" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000HMinusFee-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000HMinusFee"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000HMinusFee" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000HMinusFee"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000HMinusReminderEmail}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000HMinusReminderEmail-label" class="property-label"><g:message
					code="generalParameter.m000HMinusReminderEmail.label" default="M000 HM inus Reminder Email" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000HMinusReminderEmail-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000HMinusReminderEmail"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000HMinusReminderEmail" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000HMinusReminderEmail"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000HMinusReminderSMS}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000HMinusReminderSMS-label" class="property-label"><g:message
					code="generalParameter.m000HMinusReminderSMS.label" default="M000 HM inus Reminder SMS" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000HMinusReminderSMS-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000HMinusReminderSMS"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000HMinusReminderSMS" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000HMinusReminderSMS"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000HMinusReminderSTNK}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000HMinusReminderSTNK-label" class="property-label"><g:message
					code="generalParameter.m000HMinusReminderSTNK.label" default="M000 HM inus Reminder STNK" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000HMinusReminderSTNK-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000HMinusReminderSTNK"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000HMinusReminderSTNK" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000HMinusReminderSTNK"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000HMinusSMSAppointment}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000HMinusSMSAppointment-label" class="property-label"><g:message
					code="generalParameter.m000HMinusSMSAppointment.label" default="M000 HM inus SMSA ppointment" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000HMinusSMSAppointment-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000HMinusSMSAppointment"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000HMinusSMSAppointment" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000HMinusSMSAppointment"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000HMinusTglReminderDM}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000HMinusTglReminderDM-label" class="property-label"><g:message
					code="generalParameter.m000HMinusTglReminderDM.label" default="M000 HM inus Tgl Reminder DM" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000HMinusTglReminderDM-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000HMinusTglReminderDM"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000HMinusTglReminderDM" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000HMinusTglReminderDM"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000HPlusCancelBookingFee}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000HPlusCancelBookingFee-label" class="property-label"><g:message
					code="generalParameter.m000HPlusCancelBookingFee.label" default="M000 HP lus Cancel Booking Fee" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000HPlusCancelBookingFee-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000HPlusCancelBookingFee"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000HPlusCancelBookingFee" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000HPlusCancelBookingFee"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000HPlusUndanganFA}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000HPlusUndanganFA-label" class="property-label"><g:message
					code="generalParameter.m000HPlusUndanganFA.label" default="M000 HP lus Undangan FA" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000HPlusUndanganFA-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000HPlusUndanganFA"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000HPlusUndanganFA" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000HPlusUndanganFA"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000InvoiceFooter}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000InvoiceFooter-label" class="property-label"><g:message
					code="generalParameter.m000InvoiceFooter.label" default="M000 Invoice Footer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000InvoiceFooter-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000InvoiceFooter"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000InvoiceFooter" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000InvoiceFooter"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000JMinusSMSAppointment}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000JMinusSMSAppointment-label" class="property-label"><g:message
					code="generalParameter.m000JMinusSMSAppointment.label" default="M000 JM inus SMSA ppointment" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000JMinusSMSAppointment-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000JMinusSMSAppointment"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000JMinusSMSAppointment" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000JMinusSMSAppointment"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000JPlusSMSTerlambat}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000JPlusSMSTerlambat-label" class="property-label"><g:message
					code="generalParameter.m000JPlusSMSTerlambat.label" default="M000 JP lus SMST erlambat" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000JPlusSMSTerlambat-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000JPlusSMSTerlambat"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000JPlusSMSTerlambat" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000JPlusSMSTerlambat"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000JamKirimEmail}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000JamKirimEmail-label" class="property-label"><g:message
					code="generalParameter.m000JamKirimEmail.label" default="M000 Jam Kirim Email" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000JamKirimEmail-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000JamKirimEmail"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000JamKirimEmail" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:formatDate date="${generalParameterInstance?.m000JamKirimEmail}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000JamKirimSMS}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000JamKirimSMS-label" class="property-label"><g:message
					code="generalParameter.m000JamKirimSMS.label" default="M000 Jam Kirim SMS" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000JamKirimSMS-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000JamKirimSMS"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000JamKirimSMS" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:formatDate date="${generalParameterInstance?.m000JamKirimSMS}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000JamKirimSMSSTNK}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000JamKirimSMSSTNK-label" class="property-label"><g:message
					code="generalParameter.m000JamKirimSMSSTNK.label" default="M000 Jam Kirim SMSSTNK" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000JamKirimSMSSTNK-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000JamKirimSMSSTNK"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000JamKirimSMSSTNK" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:formatDate date="${generalParameterInstance?.m000JamKirimSMSSTNK}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000JmlMaksimalKesalahan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000JmlMaksimalKesalahan-label" class="property-label"><g:message
					code="generalParameter.m000JmlMaksimalKesalahan.label" default="M000 Jml Maksimal Kesalahan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000JmlMaksimalKesalahan-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000JmlMaksimalKesalahan"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000JmlMaksimalKesalahan" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000JmlMaksimalKesalahan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000JmlMaxInvitation}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000JmlMaxInvitation-label" class="property-label"><g:message
					code="generalParameter.m000JmlMaxInvitation.label" default="M000 Jml Max Invitation" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000JmlMaxInvitation-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000JmlMaxInvitation"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000JmlMaxInvitation" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000JmlMaxInvitation"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000JmlMaxItemWAC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000JmlMaxItemWAC-label" class="property-label"><g:message
					code="generalParameter.m000JmlMaxItemWAC.label" default="M000 Jml Max Item WAC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000JmlMaxItemWAC-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000JmlMaxItemWAC"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000JmlMaxItemWAC" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000JmlMaxItemWAC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000JmlMaxKirimSMSFollowUp}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000JmlMaxKirimSMSFollowUp-label" class="property-label"><g:message
					code="generalParameter.m000JmlMaxKirimSMSFollowUp.label" default="M000 Jml Max Kirim SMSF ollow Up" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000JmlMaxKirimSMSFollowUp-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000JmlMaxKirimSMSFollowUp"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000JmlMaxKirimSMSFollowUp" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000JmlMaxKirimSMSFollowUp"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000JmlSKipCustomer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000JmlSKipCustomer-label" class="property-label"><g:message
					code="generalParameter.m000JmlSKipCustomer.label" default="M000 Jml SK ip Customer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000JmlSKipCustomer-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000JmlSKipCustomer"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000JmlSKipCustomer" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000JmlSKipCustomer"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000JmlToleransiAsuransiBP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000JmlToleransiAsuransiBP-label" class="property-label"><g:message
					code="generalParameter.m000JmlToleransiAsuransiBP.label" default="M000 Jml Toleransi Asuransi BP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000JmlToleransiAsuransiBP-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000JmlToleransiAsuransiBP"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000JmlToleransiAsuransiBP" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000JmlToleransiAsuransiBP"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000KonfirmasiHariSebelum}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000KonfirmasiHariSebelum-label" class="property-label"><g:message
					code="generalParameter.m000KonfirmasiHariSebelum.label" default="M000 Konfirmasi Hari Sebelum" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000KonfirmasiHariSebelum-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000KonfirmasiHariSebelum"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000KonfirmasiHariSebelum" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000KonfirmasiHariSebelum"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000KonfirmasiJamSebelum}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000KonfirmasiJamSebelum-label" class="property-label"><g:message
					code="generalParameter.m000KonfirmasiJamSebelum.label" default="M000 Konfirmasi Jam Sebelum" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000KonfirmasiJamSebelum-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000KonfirmasiJamSebelum"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000KonfirmasiJamSebelum" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000KonfirmasiJamSebelum"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000KonfirmasiTerlambat}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000KonfirmasiTerlambat-label" class="property-label"><g:message
					code="generalParameter.m000KonfirmasiTerlambat.label" default="M000 Konfirmasi Terlambat" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000KonfirmasiTerlambat-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000KonfirmasiTerlambat"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000KonfirmasiTerlambat" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000KonfirmasiTerlambat"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000LamaCuciMobil}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000LamaCuciMobil-label" class="property-label"><g:message
					code="generalParameter.m000LamaCuciMobil.label" default="M000 Lama Cuci Mobil" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000LamaCuciMobil-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000LamaCuciMobil"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000LamaCuciMobil" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000LamaCuciMobil"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000LamaWaktuMinCuci}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000LamaWaktuMinCuci-label" class="property-label"><g:message
					code="generalParameter.m000LamaWaktuMinCuci.label" default="M000 Lama Waktu Min Cuci" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000LamaWaktuMinCuci-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000LamaWaktuMinCuci"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000LamaWaktuMinCuci" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000LamaWaktuMinCuci"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000MaksBayarCashRefund}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000MaksBayarCashRefund-label" class="property-label"><g:message
					code="generalParameter.m000MaksBayarCashRefund.label" default="M000 Maks Bayar Cash Refund" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000MaksBayarCashRefund-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000MaksBayarCashRefund"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000MaksBayarCashRefund" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000MaksBayarCashRefund"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000MaxPasswordExpired}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000MaxPasswordExpired-label" class="property-label"><g:message
					code="generalParameter.m000MaxPasswordExpired.label" default="M000 Max Password Expired" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000MaxPasswordExpired-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000MaxPasswordExpired"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000MaxPasswordExpired" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000MaxPasswordExpired"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000MaxPersenKembaliBookingFee}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000MaxPersenKembaliBookingFee-label" class="property-label"><g:message
					code="generalParameter.m000MaxPersenKembaliBookingFee.label" default="M000 Max Persen Kembali Booking Fee" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000MaxPersenKembaliBookingFee-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000MaxPersenKembaliBookingFee"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000MaxPersenKembaliBookingFee" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000MaxPersenKembaliBookingFee"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000MaxRecordPerPage}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000MaxRecordPerPage-label" class="property-label"><g:message
					code="generalParameter.m000MaxRecordPerPage.label" default="M000 Max Record Per Page" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000MaxRecordPerPage-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000MaxRecordPerPage"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000MaxRecordPerPage" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000MaxRecordPerPage"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000MinLastPassTdkBolehDigunakan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000MinLastPassTdkBolehDigunakan-label" class="property-label"><g:message
					code="generalParameter.m000MinLastPassTdkBolehDigunakan.label" default="M000 Min Last Pass Tdk Boleh Digunakan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000MinLastPassTdkBolehDigunakan-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000MinLastPassTdkBolehDigunakan"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000MinLastPassTdkBolehDigunakan" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000MinLastPassTdkBolehDigunakan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000NPWPPWC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000NPWPPWC-label" class="property-label"><g:message
					code="generalParameter.m000NPWPPWC.label" default="M000 NPWPPWC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000NPWPPWC-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000NPWPPWC"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000NPWPPWC" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000NPWPPWC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000NPWPTWC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000NPWPTWC-label" class="property-label"><g:message
					code="generalParameter.m000NPWPTWC.label" default="M000 NPWPTWC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000NPWPTWC-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000NPWPTWC"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000NPWPTWC" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000NPWPTWC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000NamaHostOrIPAddress}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000NamaHostOrIPAddress-label" class="property-label"><g:message
					code="generalParameter.m000NamaHostOrIPAddress.label" default="M000 Nama Host Or IPA ddress" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000NamaHostOrIPAddress-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000NamaHostOrIPAddress"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000NamaHostOrIPAddress" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000NamaHostOrIPAddress"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000NamaPWC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000NamaPWC-label" class="property-label"><g:message
					code="generalParameter.m000NamaPWC.label" default="M000 Nama PWC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000NamaPWC-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000NamaPWC"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000NamaPWC" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000NamaPWC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000NamaTWC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000NamaTWC-label" class="property-label"><g:message
					code="generalParameter.m000NamaTWC.label" default="M000 Nama TWC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000NamaTWC-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000NamaTWC"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000NamaTWC" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000NamaTWC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000NominalMaterai}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000NominalMaterai-label" class="property-label"><g:message
					code="generalParameter.m000NominalMaterai.label" default="M000 Nominal Materai" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000NominalMaterai-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000NominalMaterai"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000NominalMaterai" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000NominalMaterai"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000Password}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000Password-label" class="property-label"><g:message
					code="generalParameter.m000Password.label" default="M000 Password" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000Password-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000Password"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000Password" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000Password"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000PenguncianPassword}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000PenguncianPassword-label" class="property-label"><g:message
					code="generalParameter.m000PenguncianPassword.label" default="M000 Penguncian Password" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000PenguncianPassword-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000PenguncianPassword"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000PenguncianPassword" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:formatDate date="${generalParameterInstance?.m000PenguncianPassword}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000PersenKenaikanJobSublet}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000PersenKenaikanJobSublet-label" class="property-label"><g:message
					code="generalParameter.m000PersenKenaikanJobSublet.label" default="M000 Persen Kenaikan Job Sublet" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000PersenKenaikanJobSublet-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000PersenKenaikanJobSublet"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000PersenKenaikanJobSublet" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000PersenKenaikanJobSublet"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000PersenMaxGunakanMaterial}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000PersenMaxGunakanMaterial-label" class="property-label"><g:message
					code="generalParameter.m000PersenMaxGunakanMaterial.label" default="M000 Persen Max Gunakan Material" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000PersenMaxGunakanMaterial-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000PersenMaxGunakanMaterial"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000PersenMaxGunakanMaterial" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000PersenMaxGunakanMaterial"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000PersenPPN}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000PersenPPN-label" class="property-label"><g:message
					code="generalParameter.m000PersenPPN.label" default="M000 Persen PPN" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000PersenPPN-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000PersenPPN"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000PersenPPN" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000PersenPPN"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000PersenPPh}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000PersenPPh-label" class="property-label"><g:message
					code="generalParameter.m000PersenPPh.label" default="M000 Persen PP h" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000PersenPPh-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000PersenPPh"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000PersenPPh" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000PersenPPh"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000PersenPainting}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000PersenPainting-label" class="property-label"><g:message
					code="generalParameter.m000PersenPainting.label" default="M000 Persen Painting" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000PersenPainting-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000PersenPainting"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000PersenPainting" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000PersenPainting"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000PersenPelepasanPartBP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000PersenPelepasanPartBP-label" class="property-label"><g:message
					code="generalParameter.m000PersenPelepasanPartBP.label" default="M000 Persen Pelepasan Part BP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000PersenPelepasanPartBP-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000PersenPelepasanPartBP"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000PersenPelepasanPartBP" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000PersenPelepasanPartBP"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000PersenPemasanganPartBP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000PersenPemasanganPartBP-label" class="property-label"><g:message
					code="generalParameter.m000PersenPemasanganPartBP.label" default="M000 Persen Pemasangan Part BP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000PersenPemasanganPartBP-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000PersenPemasanganPartBP"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000PersenPemasanganPartBP" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000PersenPemasanganPartBP"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000PersenPemborong}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000PersenPemborong-label" class="property-label"><g:message
					code="generalParameter.m000PersenPemborong.label" default="M000 Persen Pemborong" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000PersenPemborong-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000PersenPemborong"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000PersenPemborong" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000PersenPemborong"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000PersenPoleshing}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000PersenPoleshing-label" class="property-label"><g:message
					code="generalParameter.m000PersenPoleshing.label" default="M000 Persen Poleshing" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000PersenPoleshing-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000PersenPoleshing"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000PersenPoleshing" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000PersenPoleshing"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000PersenPreparation}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000PersenPreparation-label" class="property-label"><g:message
					code="generalParameter.m000PersenPreparation.label" default="M000 Persen Preparation" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000PersenPreparation-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000PersenPreparation"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000PersenPreparation" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000PersenPreparation"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000PersenWorkshop}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000PersenWorkshop-label" class="property-label"><g:message
					code="generalParameter.m000PersenWorkshop.label" default="M000 Persen Workshop" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000PersenWorkshop-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000PersenWorkshop"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000PersenWorkshop" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000PersenWorkshop"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000Person1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000Person1-label" class="property-label"><g:message
					code="generalParameter.m000Person1.label" default="M000 Person1" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000Person1-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000Person1"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000Person1" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000Person1"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000Person2}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000Person2-label" class="property-label"><g:message
					code="generalParameter.m000Person2.label" default="M000 Person2" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000Person2-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000Person2"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000Person2" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000Person2"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000Person3}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000Person3-label" class="property-label"><g:message
					code="generalParameter.m000Person3.label" default="M000 Person3" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000Person3-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000Person3"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000Person3" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000Person3"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000Person4}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000Person4-label" class="property-label"><g:message
					code="generalParameter.m000Person4.label" default="M000 Person4" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000Person4-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000Person4"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000Person4" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000Person4"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000Person5}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000Person5-label" class="property-label"><g:message
					code="generalParameter.m000Person5.label" default="M000 Person5" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000Person5-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000Person5"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000Person5" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000Person5"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000Port}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000Port-label" class="property-label"><g:message
					code="generalParameter.m000Port.label" default="M000 Port" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000Port-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000Port"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000Port" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000Port"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000PreExpiredAlert}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000PreExpiredAlert-label" class="property-label"><g:message
					code="generalParameter.m000PreExpiredAlert.label" default="M000 Pre Expired Alert" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000PreExpiredAlert-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000PreExpiredAlert"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000PreExpiredAlert" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000PreExpiredAlert"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000PreparationTime}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000PreparationTime-label" class="property-label"><g:message
					code="generalParameter.m000PreparationTime.label" default="M000 Preparation Time" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000PreparationTime-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000PreparationTime"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000PreparationTime" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000PreparationTime"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000ProgressDisc}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000ProgressDisc-label" class="property-label"><g:message
					code="generalParameter.m000ProgressDisc.label" default="M000 Progress Disc" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000ProgressDisc-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000ProgressDisc"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000ProgressDisc" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000ProgressDisc"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000ReminderDMKm1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000ReminderDMKm1-label" class="property-label"><g:message
					code="generalParameter.m000ReminderDMKm1.label" default="M000 Reminder DMK m1" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000ReminderDMKm1-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000ReminderDMKm1"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000ReminderDMKm1" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000ReminderDMKm1"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000ReminderDMKm2}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000ReminderDMKm2-label" class="property-label"><g:message
					code="generalParameter.m000ReminderDMKm2.label" default="M000 Reminder DMK m2" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000ReminderDMKm2-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000ReminderDMKm2"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000ReminderDMKm2" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000ReminderDMKm2"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000ReminderDMOp1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000ReminderDMOp1-label" class="property-label"><g:message
					code="generalParameter.m000ReminderDMOp1.label" default="M000 Reminder DMO p1" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000ReminderDMOp1-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000ReminderDMOp1"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000ReminderDMOp1" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000ReminderDMOp1"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000ReminderDMOp2}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000ReminderDMOp2-label" class="property-label"><g:message
					code="generalParameter.m000ReminderDMOp2.label" default="M000 Reminder DMO p2" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000ReminderDMOp2-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000ReminderDMOp2"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000ReminderDMOp2" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000ReminderDMOp2"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000ReminderEmailKm1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000ReminderEmailKm1-label" class="property-label"><g:message
					code="generalParameter.m000ReminderEmailKm1.label" default="M000 Reminder Email Km1" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000ReminderEmailKm1-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000ReminderEmailKm1"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000ReminderEmailKm1" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000ReminderEmailKm1"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000ReminderEmailKm2}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000ReminderEmailKm2-label" class="property-label"><g:message
					code="generalParameter.m000ReminderEmailKm2.label" default="M000 Reminder Email Km2" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000ReminderEmailKm2-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000ReminderEmailKm2"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000ReminderEmailKm2" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000ReminderEmailKm2"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000ReminderEmailOp1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000ReminderEmailOp1-label" class="property-label"><g:message
					code="generalParameter.m000ReminderEmailOp1.label" default="M000 Reminder Email Op1" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000ReminderEmailOp1-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000ReminderEmailOp1"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000ReminderEmailOp1" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000ReminderEmailOp1"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000ReminderEmailOp2}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000ReminderEmailOp2-label" class="property-label"><g:message
					code="generalParameter.m000ReminderEmailOp2.label" default="M000 Reminder Email Op2" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000ReminderEmailOp2-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000ReminderEmailOp2"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000ReminderEmailOp2" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000ReminderEmailOp2"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000ReminderSMSKm1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000ReminderSMSKm1-label" class="property-label"><g:message
					code="generalParameter.m000ReminderSMSKm1.label" default="M000 Reminder SMSK m1" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000ReminderSMSKm1-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000ReminderSMSKm1"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000ReminderSMSKm1" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000ReminderSMSKm1"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000ReminderSMSKm2}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000ReminderSMSKm2-label" class="property-label"><g:message
					code="generalParameter.m000ReminderSMSKm2.label" default="M000 Reminder SMSK m2" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000ReminderSMSKm2-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000ReminderSMSKm2"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000ReminderSMSKm2" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000ReminderSMSKm2"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000ReminderSMSOp1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000ReminderSMSOp1-label" class="property-label"><g:message
					code="generalParameter.m000ReminderSMSOp1.label" default="M000 Reminder SMSO p1" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000ReminderSMSOp1-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000ReminderSMSOp1"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000ReminderSMSOp1" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000ReminderSMSOp1"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000ReminderSMSOp2}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000ReminderSMSOp2-label" class="property-label"><g:message
					code="generalParameter.m000ReminderSMSOp2.label" default="M000 Reminder SMSO p2" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000ReminderSMSOp2-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000ReminderSMSOp2"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000ReminderSMSOp2" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000ReminderSMSOp2"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000RunningTextInformation}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000RunningTextInformation-label" class="property-label"><g:message
					code="generalParameter.m000RunningTextInformation.label" default="M000 Running Text Information" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000RunningTextInformation-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000RunningTextInformation"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000RunningTextInformation" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000RunningTextInformation"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000SBEDisc}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000SBEDisc-label" class="property-label"><g:message
					code="generalParameter.m000SBEDisc.label" default="M000 SBED isc" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000SBEDisc-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000SBEDisc"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000SBEDisc" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000SBEDisc"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000SOHeader}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000SOHeader-label" class="property-label"><g:message
					code="generalParameter.m000SOHeader.label" default="M000 SOH eader" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000SOHeader-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000SOHeader"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000SOHeader" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000SOHeader"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000SettingRPP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000SettingRPP-label" class="property-label"><g:message
					code="generalParameter.m000SettingRPP.label" default="M000 Setting RPP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000SettingRPP-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000SettingRPP"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000SettingRPP" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000SettingRPP"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000StaAktifCall}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000StaAktifCall-label" class="property-label"><g:message
					code="generalParameter.m000StaAktifCall.label" default="M000 Sta Aktif Call" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000StaAktifCall-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000StaAktifCall"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000StaAktifCall" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000StaAktifCall"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000StaAktifDM}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000StaAktifDM-label" class="property-label"><g:message
					code="generalParameter.m000StaAktifDM.label" default="M000 Sta Aktif DM" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000StaAktifDM-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000StaAktifDM"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000StaAktifDM" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000StaAktifDM"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000StaAktifEmail}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000StaAktifEmail-label" class="property-label"><g:message
					code="generalParameter.m000StaAktifEmail.label" default="M000 Sta Aktif Email" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000StaAktifEmail-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000StaAktifEmail"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000StaAktifEmail" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000StaAktifEmail"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000StaAktifSMS}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000StaAktifSMS-label" class="property-label"><g:message
					code="generalParameter.m000StaAktifSMS.label" default="M000 Sta Aktif SMS" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000StaAktifSMS-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000StaAktifSMS"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000StaAktifSMS" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000StaAktifSMS"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000StaDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000StaDel-label" class="property-label"><g:message
					code="generalParameter.m000StaDel.label" default="M000 Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000StaDel-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000StaDel"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000StaDel" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000StaDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000StaEnablePartTambahanWeb}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000StaEnablePartTambahanWeb-label" class="property-label"><g:message
					code="generalParameter.m000StaEnablePartTambahanWeb.label" default="M000 Sta Enable Part Tambahan Web" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000StaEnablePartTambahanWeb-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000StaEnablePartTambahanWeb"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000StaEnablePartTambahanWeb" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000StaEnablePartTambahanWeb"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000StaLihatDataBengkelLain}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000StaLihatDataBengkelLain-label" class="property-label"><g:message
					code="generalParameter.m000StaLihatDataBengkelLain.label" default="M000 Sta Lihat Data Bengkel Lain" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000StaLihatDataBengkelLain-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000StaLihatDataBengkelLain"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000StaLihatDataBengkelLain" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000StaLihatDataBengkelLain"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000StaSalon}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000StaSalon-label" class="property-label"><g:message
					code="generalParameter.m000StaSalon.label" default="M000 Sta Salon" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000StaSalon-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000StaSalon"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000StaSalon" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000StaSalon"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000StaWaktuAkhir}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000StaWaktuAkhir-label" class="property-label"><g:message
					code="generalParameter.m000StaWaktuAkhir.label" default="M000 Sta Waktu Akhir" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000StaWaktuAkhir-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000StaWaktuAkhir"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000StaWaktuAkhir" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000StaWaktuAkhir"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000Tax}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000Tax-label" class="property-label"><g:message
					code="generalParameter.m000Tax.label" default="M000 Tax" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000Tax-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000Tax"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000Tax" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000Tax"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000TextCatatanDM}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000TextCatatanDM-label" class="property-label"><g:message
					code="generalParameter.m000TextCatatanDM.label" default="M000 Text Catatan DM" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000TextCatatanDM-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000TextCatatanDM"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000TextCatatanDM" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000TextCatatanDM"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000TglJamWaktuAkhir}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000TglJamWaktuAkhir-label" class="property-label"><g:message
					code="generalParameter.m000TglJamWaktuAkhir.label" default="M000 Tgl Jam Waktu Akhir" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000TglJamWaktuAkhir-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000TglJamWaktuAkhir"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000TglJamWaktuAkhir" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:formatDate date="${generalParameterInstance?.m000TglJamWaktuAkhir}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000ToleransiAmbilWO}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000ToleransiAmbilWO-label" class="property-label"><g:message
					code="generalParameter.m000ToleransiAmbilWO.label" default="M000 Toleransi Ambil WO" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000ToleransiAmbilWO-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000ToleransiAmbilWO"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000ToleransiAmbilWO" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000ToleransiAmbilWO"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000ToleransiIDRPerJob}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000ToleransiIDRPerJob-label" class="property-label"><g:message
					code="generalParameter.m000ToleransiIDRPerJob.label" default="M000 Toleransi IDRP er Job" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000ToleransiIDRPerJob-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000ToleransiIDRPerJob"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000ToleransiIDRPerJob" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000ToleransiIDRPerJob"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000ToleransiIDRPerProses}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000ToleransiIDRPerProses-label" class="property-label"><g:message
					code="generalParameter.m000ToleransiIDRPerProses.label" default="M000 Toleransi IDRP er Proses" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000ToleransiIDRPerProses-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000ToleransiIDRPerProses"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000ToleransiIDRPerProses" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000ToleransiIDRPerProses"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000UserName}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000UserName-label" class="property-label"><g:message
					code="generalParameter.m000UserName.label" default="M000 User Name" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000UserName-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000UserName"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000UserName" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000UserName"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000VersionAplikasi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000VersionAplikasi-label" class="property-label"><g:message
					code="generalParameter.m000VersionAplikasi.label" default="M000 Version Aplikasi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000VersionAplikasi-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000VersionAplikasi"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000VersionAplikasi" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000VersionAplikasi"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000WaktuNotifikasiTargetClockOffJob}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000WaktuNotifikasiTargetClockOffJob-label" class="property-label"><g:message
					code="generalParameter.m000WaktuNotifikasiTargetClockOffJob.label" default="M000 Waktu Notifikasi Target Clock Off Job" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000WaktuNotifikasiTargetClockOffJob-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000WaktuNotifikasiTargetClockOffJob"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000WaktuNotifikasiTargetClockOffJob" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:formatDate date="${generalParameterInstance?.m000WaktuNotifikasiTargetClockOffJob}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000WaktuSessionLog}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000WaktuSessionLog-label" class="property-label"><g:message
					code="generalParameter.m000WaktuSessionLog.label" default="M000 Waktu Session Log" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000WaktuSessionLog-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000WaktuSessionLog"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000WaktuSessionLog" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:formatDate date="${generalParameterInstance?.m000WaktuSessionLog}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000mMinusFee}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000mMinusFee-label" class="property-label"><g:message
					code="generalParameter.m000mMinusFee.label" default="M000m Minus Fee" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000mMinusFee-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000mMinusFee"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000mMinusFee" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000mMinusFee"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000staJenisJamKerja}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000staJenisJamKerja-label" class="property-label"><g:message
					code="generalParameter.m000staJenisJamKerja.label" default="M000sta Jenis Jam Kerja" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000staJenisJamKerja-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000staJenisJamKerja"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000staJenisJamKerja" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000staJenisJamKerja"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.m000staVendorCatByCc}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m000staVendorCatByCc-label" class="property-label"><g:message
					code="generalParameter.m000staVendorCatByCc.label" default="M000sta Vendor Cat By Cc" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m000staVendorCatByCc-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="m000staVendorCatByCc"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter m000staVendorCatByCc" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="m000staVendorCatByCc"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${generalParameterInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="generalParameter.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${generalParameterInstance}" field="updatedBy"
								url="${request.contextPath}/GeneralParameter/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadGeneralParameterTable();" />--}%
							
								<g:fieldValue bean="${generalParameterInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${generalParameterInstance?.id}"
					update="[success:'generalParameter-form',failure:'generalParameter-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteGeneralParameter('${generalParameterInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
