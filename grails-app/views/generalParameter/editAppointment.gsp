<%@ page import="com.kombos.administrasi.GeneralParameter" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'generalParameter.label', default: 'GeneralParameter')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="edit-generalParameter-appointment" class="content scaffold-edit general-parameter" role="main">
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${generalParameterInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${generalParameterInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:javascript>
			var updateStatus;
			$(function(){ 
				updateStatus = function(data){
					$("#version").val(data.version);
                    toastr.success('<div>'+data.message+'</div>');
				};
                $('#m000MaxPersenKembaliBookingFee').autoNumeric('init',{
                    vMin:'0',
                    vMax:'100',
                    mDec: '2',
                    aSep:''
                });
                $('.gpnumber').autoNumeric('init',{
                    vMin:'0',
                    vMax:'9999',
                    mDec: null,
                    aSep:''
                });
			});
			</g:javascript>
			<g:formRemote class="form-horizontal" name="edit" on404="alert('not found!');" onSuccess="updateStatus(data);" 
				url="[controller: 'generalParameter', action:'update']">
				<g:hiddenField name="id" value="${generalParameterInstance?.id}" />
				<g:hiddenField name="version" value="${generalParameterInstance?.version}" />
				<fieldset class="form">
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000KonfirmasiHariSebelum', 'error')} required">
						<label class="control-label" for="m000KonfirmasiHariSebelum">
							<g:message code="generalParameter.m000KonfirmasiHariSebelum.label" default="Konfirmasi 1" /><span class="required-indicator">*</span>
							
						</label>
						<div class="controls">
                            <g:textField class="gpnumber" name="m000KonfirmasiHariSebelum" value="${generalParameterInstance?.m000KonfirmasiHariSebelum}" required=""/>
						%{--<g:field style="width: 132px" type="number" onkeypress="return isNumberKey(event)" max="9999" onKeyDown="if(this.value.length==4) return false;" name="m000KonfirmasiHariSebelum" value="${generalParameterInstance.m000KonfirmasiHariSebelum}" required=""/>--}%
						&nbsp;hari sebelum janji datang
						</div>
					</div>
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000KonfirmasiJamSebelum', 'error')} required">
						<label class="control-label" for="m000KonfirmasiJamSebelum">
							<g:message code="generalParameter.m000KonfirmasiJamSebelum.label" default="Konfirmasi 2" /><span class="required-indicator">*</span>
							
						</label>
						<div class="controls">
						<select id="m000KonfirmasiJamSebelum_hour" name="m000KonfirmasiJamSebelum_hour" style="width: 60px" required="">
							<g:each var="i" in="${ (0..<24) }">
							    <g:if test="${i < 10}">
							    	<g:if test="${i==generalParameterInstance?.m000KonfirmasiJamSebelum?.getHours()}">
							    		<option value="${i}" selected="">0${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">0${i}</option>
							    	</g:else>
							    </g:if>
							    <g:else>
							    	<g:if test="${i==generalParameterInstance?.m000KonfirmasiJamSebelum?.getHours()}">
							    		<option value="${i}" selected="">${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">${i}</option>
							    	</g:else>
							    </g:else>
							</g:each>
				        </select> H :
				        <select id="m000KonfirmasiJamSebelum_minute" name="m000KonfirmasiJamSebelum_minute" style="width: 60px" required="">
				           <g:each var="i" in="${ (0..<60) }">
				           		<g:if test="${i < 10}">
							    	<g:if test="${i==generalParameterInstance?.m000KonfirmasiJamSebelum?.getMinutes()}">
							    		<option value="${i}" selected="">0${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">0${i}</option>
							    	</g:else>
							    </g:if>
							    <g:else>
							    	<g:if test="${i==generalParameterInstance?.m000KonfirmasiJamSebelum?.getMinutes()}">
							    		<option value="${i}" selected="">${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">${i}</option>
							    	</g:else>
							    </g:else>
							 </g:each>
				        </select> m
						&nbsp;sebelum janji datang
						</div>
					</div>

					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000KonfirmasiTerlambat', 'error')} required">
						<label class="control-label" for="m000KonfirmasiTerlambat">
							<g:message code="generalParameter.m000KonfirmasiTerlambat.label" default="Konfirmasi Terlambat" /><span class="required-indicator">*</span>
						</label>
						<div class="controls">
						<select id="m000KonfirmasiTerlambat_hour" name="m000KonfirmasiTerlambat_hour" style="width: 60px" required="">
							<g:each var="i" in="${ (0..<24) }">
							    <g:if test="${i < 10}">
							    	<g:if test="${i==generalParameterInstance?.m000KonfirmasiTerlambat?.getHours()}">
							    		<option value="${i}" selected="">0${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">0${i}</option>
							    	</g:else>
							    </g:if>
							    <g:else>
							    	<g:if test="${i==generalParameterInstance?.m000KonfirmasiTerlambat?.getHours()}">
							    		<option value="${i}" selected="">${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">${i}</option>
							    	</g:else>
							    </g:else>
							</g:each>
				        </select> H :
				        <select id="m000KonfirmasiTerlambat_minute" name="m000KonfirmasiTerlambat_minute" style="width: 60px" required="">
				           <g:each var="i" in="${ (0..<60) }">
				           		<g:if test="${i < 10}">
							    	<g:if test="${i==generalParameterInstance?.m000KonfirmasiTerlambat?.getMinutes()}">
							    		<option value="${i}" selected="">0${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">0${i}</option>
							    	</g:else>
							    </g:if>
							    <g:else>
							    	<g:if test="${i==generalParameterInstance?.m000KonfirmasiTerlambat?.getMinutes()}">
							    		<option value="${i}" selected="">${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">${i}</option>
							    	</g:else>
							    </g:else>
							 </g:each>
				        </select> m
						&nbsp;setelah janji datang
						</div>
					</div>
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000HMinusSMSAppointment', 'error')} required">
						<label class="control-label" for="m000HMinusSMSAppointment">
							<g:message code="generalParameter.m000HMinusSMSAppointment.label" default="Alert Booking Fee 1" /><span class="required-indicator">*</span>
							
						</label>
						<div class="controls">
                            <g:textField class="gpnumber" name="m000HMinusSMSAppointment" value="${generalParameterInstance?.m000HMinusSMSAppointment}" required=""/>
						%{--<g:field style="width: 132px" type="number" onkeypress="return isNumberKey(event)" max="9999" onKeyDown="if(this.value.length==4) return false;" name="m000HMinusSMSAppointment" value="${generalParameterInstance.m000HMinusSMSAppointment}" required=""/>--}%
						&nbsp;hari sebelum janji pembayaran Booking Fee
						</div>
					</div>
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000JMinusSMSAppointment', 'error')} required">
						<label class="control-label" for="m000JMinusSMSAppointment">
							<g:message code="generalParameter.m000JMinusSMSAppointment.label" default="Alert Booking Fee 2" /><span class="required-indicator">*</span>
							
						</label>
						<div class="controls">
						<select id="m000JMinusSMSAppointment_hour" name="m000JMinusSMSAppointment_hour" style="width: 60px" required="">
							<g:each var="i" in="${ (0..<24) }">
							    <g:if test="${i < 10}">
							    	<g:if test="${i==generalParameterInstance?.m000JMinusSMSAppointment?.getHours()}">
							    		<option value="${i}" selected="">0${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">0${i}</option>
							    	</g:else>
							    </g:if>
							    <g:else>
							    	<g:if test="${i==generalParameterInstance?.m000JMinusSMSAppointment?.getHours()}">
							    		<option value="${i}" selected="">${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">${i}</option>
							    	</g:else>
							    </g:else>
							</g:each>
				        </select> H :
				        <select id="m000JMinusSMSAppointment_minute" name="m000JMinusSMSAppointment_minute" style="width: 60px" required="">
				           <g:each var="i" in="${ (0..<60) }">
				           		<g:if test="${i < 10}">
							    	<g:if test="${i==generalParameterInstance?.m000JMinusSMSAppointment?.getMinutes()}">
							    		<option value="${i}" selected="">0${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">0${i}</option>
							    	</g:else>
							    </g:if>
							    <g:else>
							    	<g:if test="${i==generalParameterInstance?.m000JMinusSMSAppointment?.getMinutes()}">
							    		<option value="${i}" selected="">${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">${i}</option>
							    	</g:else>
							    </g:else>
							 </g:each>
				        </select> m
						&nbsp;jam sebelum janji pembayaran Booking Fee
						</div>
					</div>
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000JPlusSMSTerlambat', 'error')} required">
						<label class="control-label" for="m000JPlusSMSTerlambat">
							<g:message code="generalParameter.m000JPlusSMSTerlambat.label" default="Alert Booking Fee 3" /><span class="required-indicator">*</span>
							
						</label>
						<div class="controls">
						<select id="m000JPlusSMSTerlambat_hour" name="m000JPlusSMSTerlambat_hour" style="width: 60px" required="">
							<g:each var="i" in="${ (0..<24) }">
							    <g:if test="${i < 10}">
							    	<g:if test="${i==generalParameterInstance?.m000JPlusSMSTerlambat?.getHours()}">
							    		<option value="${i}" selected="">0${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">0${i}</option>
							    	</g:else>
							    </g:if>
							    <g:else>
							    	<g:if test="${i==generalParameterInstance?.m000JPlusSMSTerlambat?.getHours()}">
							    		<option value="${i}" selected="">${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">${i}</option>
							    	</g:else>
							    </g:else>
							</g:each>
				        </select> H :
				        <select id="m000JPlusSMSTerlambat_minute" name="m000JPlusSMSTerlambat_minute" style="width: 60px" required="">
				           <g:each var="i" in="${ (0..<60) }">
				           		<g:if test="${i < 10}">
							    	<g:if test="${i==generalParameterInstance?.m000JPlusSMSTerlambat?.getMinutes()}">
							    		<option value="${i}" selected="">0${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">0${i}</option>
							    	</g:else>
							    </g:if>
							    <g:else>
							    	<g:if test="${i==generalParameterInstance?.m000JPlusSMSTerlambat?.getMinutes()}">
							    		<option value="${i}" selected="">${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">${i}</option>
							    	</g:else>
							    </g:else>
							 </g:each>
				        </select> m
						&nbsp;jam setelah janji pembayaran Booking Fee
						</div>
					</div>
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000MaxPersenKembaliBookingFee', 'error')} required">
						<label class="control-label" for="m000MaxPersenKembaliBookingFee">
							<g:message code="generalParameter.m000MaxPersenKembaliBookingFee.label" default="Max Pengembalian Booking Fee" /><span class="required-indicator">*</span>
							
						</label>
						<div class="controls">
                            <g:textField name="m000MaxPersenKembaliBookingFee" value="${generalParameterInstance?.m000MaxPersenKembaliBookingFee}" required=""/>
						%{--<g:field type="number" onkeypress="return isNumberKey(event)" min="0" max="100" name="m000MaxPersenKembaliBookingFee" value="${generalParameterInstance.m000MaxPersenKembaliBookingFee}" required=""/>&nbsp;%--}%
						</div>
					</div>
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000FormatSMSHMinus', 'error')} ">
						<label class="control-label" for="m000FormatSMSHMinus">
							<g:message code="generalParameter.m000FormatSMSHMinus.label" default="Format SMS Konfirmasi H-" /><span class="required-indicator">*</span>
							
						</label>
						<div class="controls">
						<span class="span3">
						<g:textArea rows="5" cols="50" maxlength="160" name="m000FormatSMSHMinus" value="${generalParameterInstance?.m000FormatSMSHMinus}" required=""/>
						</span>
						<span class="span6 offset2">
							<span id="appointment-workshop">&lt;Workshop&gt;</span>&nbsp;|&nbsp;
							<span id="appointment-cabang">&lt;Cabang&gt;</span>&nbsp;|&nbsp;
							<span id="appointment-nama-customer">&lt;NamaCustomer&gt;</span>&nbsp;|&nbsp;
							<span id="appointment-jenis-sb">&lt;JenisSB&gt;</span>&nbsp;|&nbsp;
							<span id="appointment-tanggal-appointment">&lt;TanggalAppointment&gt;</span>&nbsp;|&nbsp;
							<span id="appointment-jam-appoinment">&lt;JamAppointment&gt;</span>&nbsp;|&nbsp;
							<span id="appointment-base-model">&lt;BaseModel&gt;</span>&nbsp;|&nbsp;
							<span id="appointment-nopol">&lt;Nopol&gt;</span>&nbsp;|&nbsp;
							<span id="appointment-km">&lt;Km&gt;</span>
						</span>
						</div>
						
					</div>

					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000FormatSMSJMinus', 'error')} ">
						<label class="control-label" for="m000FormatSMSJMinus">
							<g:message code="generalParameter.m000FormatSMSJMinus.label" default="Format SMS Konfirmasi J-" /><span class="required-indicator">*</span>
							
						</label>
						<div class="controls">
						<g:textArea rows="5" cols="50" maxlength="160" name="m000FormatSMSJMinus" value="${generalParameterInstance?.m000FormatSMSJMinus}" required=""/>
						</div>
					</div>
				</fieldset>
				<fieldset class="buttons controls">
					<g:actionSubmit class="btn btn-primary save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
				</fieldset>
			</g:formRemote>
		</div>
	</body>
</html>
