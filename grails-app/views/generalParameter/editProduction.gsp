<%@ page import="com.kombos.administrasi.GeneralParameter" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'generalParameter.label', default: 'GeneralParameter')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="edit-generalParameter-production" class="content scaffold-edit general-parameter" role="main">
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${generalParameterInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${generalParameterInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:javascript>
			var updateStatus;
			var submitForm;
			$(function(){ 
				updateStatus = function(data){
                    $("#version").val(data.version);
					toastr.success('<div>'+data.message+'</div>');
				};
				 $('.gpnumber').autoNumeric('init',{
                    vMin:'0',
                    vMax:'9999',
                    mDec: null,
                    aSep:''
                });
                $('.percentage').autoNumeric('init',{
                    vMin:'0',
                    vMax:'100',
                    mDec: '2',
                    aSep:''
                });

                submitForm = function(){
                        if($('#formEditProduction').valid()){
                            $.ajax({
                                type:'POST',
                                data:jQuery('#formEditProduction').serialize(),
                                url:'${request.getContextPath()}/generalParameter/update',
                                success:function(data,textStatus){
                                    updateStatus(data);
                                },
                                error:function(XMLHttpRequest,textStatus,errorThrown){}});
                        }
                    }

                    $.validator.addMethod("oneHundred", function (value, element, param) {
                        var $element = $(element)
                            , $ratiopair;

                        if (typeof(param) === "string") {
                            $ratiopair = $(param);
                        } else {
                            $ratiopair = $("#" + $element.data("ratiopair"));
                        }

                        if (this.settings.onfocusout) {
                            $ratiopair.off(".validate-oneHundred").on("blur.validate-oneHundred", function () {
                                $element.valid();
                            });
                        }
                        return (parseFloat(value) + parseFloat($ratiopair.val()) == 100.0);
                          return true;
                    }, "Jumlah presentase harus 100%.");

                    $.validator.addClassRules({
                        oneHundred: {
                            oneHundred: true
                        }
                    });
			});
			</g:javascript>
        <form id="formEditProduction" class="form-horizontal" action="${request.getContextPath()}/generalParameter/update" method="post" onsubmit="submitForm();;return false;">
			%{--<g:formRemote class="form-horizontal" name="edit" on404="alert('not found!');" onSuccess="updateStatus(data);" --}%
				%{--url="[controller: 'generalParameter', action:'update']">--}%
				<g:hiddenField name="id" value="${generalParameterInstance?.id}" />
				<g:hiddenField name="version" value="${generalParameterInstance?.version}" />
				<fieldset class="form">
<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000staVendorCatByCc', 'error')} ">
	<label class="control-label" for="m000staVendorCatByCc">
		<g:message code="generalParameter.m000staVendorCatByCc.label" default="Status Vendor Cat By Cc" /><span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
		<g:radioGroup name="m000staVendorCatByCc" values="[1,0]" value="${generalParameterInstance?.m000staVendorCatByCc}" labels="['Ya','Tidak']" required="">
			${it.radio} <g:message code="${it.label}" />
		</g:radioGroup>
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000SBEDisc', 'error')} ">
	<label class="control-label" for="m000SBEDisc">
		<g:message code="generalParameter.m000SBEDisc.label" default="SBE Discount" /><span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
	<g:textField class="percentage" name="m000SBEDisc" value="${generalParameterInstance.m000SBEDisc}" required=""/>
	&nbsp;%
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000ProgressDisc', 'error')} ">
	<label class="control-label" for="m000ProgressDisc">
		<g:message code="generalParameter.m000ProgressDisc.label" default="SBE Progressive Discount" /><span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
	<g:textField class="percentage" name="m000ProgressDisc" value="${generalParameterInstance.m000ProgressDisc}" required=""/>
	&nbsp;%
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000HMinParameterReserved', 'error')} ">
	<label class="control-label" for="m000HMinParameterReserved">
		<g:message code="generalParameter.m000HMinParameterReserved.label" default="Parameter Reserved Part" /><span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
        <g:textField class="gpnumber" name="m000HMinParameterReserved" value="${generalParameterInstance?.m000HMinParameterReserved}" required=""/>
	%{--<g:field type="number" onkeypress="return isNumberKey(event)" min="0" max="9999" onKeyDown="if(this.value.length==4) return false;" name="m000HMinParameterReserved" value="${generalParameterInstance.m000HMinParameterReserved}" required=""/>--}%
	&nbsp;hari sebelum production
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000PersenPelepasanPartBP', 'error')} ">
	<label class="control-label" for="m000PersenPelepasanPartBP">
		<g:message code="generalParameter.m000PersenPelepasanPartBP.label" default="% Pelepasan Parts" /><span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
	<g:textField class="percentage" name="m000PersenPelepasanPartBP" value="${generalParameterInstance.m000PersenPelepasanPartBP}" required=""/>
	&nbsp;%
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000PersenPemasanganPartBP', 'error')} ">
	<label class="control-label" for="m000PersenPemasanganPartBP">
		<g:message code="generalParameter.m000PersenPemasanganPartBP.label" default="% Pemasangan Parts" /><span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
	<g:textField class="percentage" name="m000PersenPemasanganPartBP" value="${generalParameterInstance.m000PersenPemasanganPartBP}" required=""/>
	&nbsp;%
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000PreparationTime', 'error')} ">
	<label class="control-label" for="m000PreparationTime">
		<g:message code="generalParameter.m000PreparationTime.label" default="Preparation Time" /><span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
        <g:textField class="gpnumber" name="m000PreparationTime" value="${generalParameterInstance?.m000PreparationTime}" required=""/>
	%{--<g:field type="number" onkeypress="return isNumberKey(event)" min="0" max="9999" onKeyDown="if(this.value.length==4) return false;" name="m000PreparationTime" value="${generalParameterInstance.m000PreparationTime}" required=""/>--}%
	&nbsp;Jam
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000PersenPreparation', 'error')} ">
	<label class="control-label" for="m000PersenPreparation">
		<g:message code="generalParameter.m000PersenPreparation.label" default="% Preparation" /><span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
	<g:textField class="percentage" name="m000PersenPreparation" value="${generalParameterInstance.m000PersenPreparation}" required=""/>
	&nbsp;%
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000PersenPainting', 'error')} ">
	<label class="control-label" for="m000PersenPainting">
		<g:message code="generalParameter.m000PersenPainting.label" default="% Painting" /><span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
	<g:textField class="percentage" name="m000PersenPainting" value="${generalParameterInstance.m000PersenPainting}" required=""/>
	&nbsp;%
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000PersenPoleshing', 'error')} ">
	<label class="control-label" for="m000PersenPoleshing">
		<g:message code="generalParameter.m000PersenPoleshing.label" default="% Polishing" /> <span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
	<g:textField class="percentage" name="m000PersenPoleshing" value="${generalParameterInstance.m000PersenPoleshing}" required=""/>
	&nbsp;%
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000PersenPemborong', 'error')} ">
	<label class="control-label" for="m000PersenPemborong">
		<g:message code="generalParameter.m000PersenPemborong.label" default="Pemborong : Workshop" /><span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
	<g:textField class="percentage" name="m000PersenPemborong" value="${generalParameterInstance.m000PersenPemborong}" required=""/> :
	<g:textField class="percentage oneHundred" data-ratiopair="m000PersenPemborong" name="m000PersenWorkshop" value="${generalParameterInstance.m000PersenWorkshop}" required=""/>
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000PersenMaxGunakanMaterial', 'error')} ">
	<label class="control-label" for="m000PersenMaxGunakanMaterial">
		<g:message code="generalParameter.m000PersenMaxGunakanMaterial.label" default="% Maks. Penggunaan Material" /><span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
	<g:textField class="percentage" name="m000PersenMaxGunakanMaterial" value="${generalParameterInstance.m000PersenMaxGunakanMaterial}" required=""/>
	&nbsp;%
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000LamaCuciMobil', 'error')} ">
	<label class="control-label" for="m000LamaCuciMobil">
		<g:message code="generalParameter.m000LamaCuciMobil.label" default="Lama Waktu Cuci" /><span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
        <g:textField class="gpnumber" name="m000LamaCuciMobil" value="${generalParameterInstance?.m000LamaCuciMobil}" required=""/>
	%{--<g:field type="number" onkeypress="return isNumberKey(event)" min="0" max="9999" onKeyDown="if(this.value.length==4) return false;" name="m000LamaCuciMobil" value="${generalParameterInstance.m000LamaCuciMobil}" required=""/>--}%
	&nbsp;menit
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000LamaWaktuMinCuci', 'error')} ">
	<label class="control-label" for="m000LamaWaktuMinCuci">
		<g:message code="generalParameter.m000LamaWaktuMinCuci.label" default="Waktu Min. Clock Off Proses Cuci" /><span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
        <g:textField class="gpnumber" name="m000LamaWaktuMinCuci" value="${generalParameterInstance?.m000LamaWaktuMinCuci}" required=""/>
	%{--<g:field type="number" onkeypress="return isNumberKey(event)" min="0" max="9999" onKeyDown="if(this.value.length==4) return false;" name="m000LamaWaktuMinCuci" value="${generalParameterInstance.m000LamaWaktuMinCuci}" required=""/>--}%
	&nbsp;menit
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000WaktuNotifikasiTargetClockOffJob', 'error')} ">
	<label class="control-label" for="m000WaktuNotifikasiTargetClockOffJob">
		<g:message code="generalParameter.m000WaktuNotifikasiTargetClockOffJob.label" default="Waktu Notifikasi Target Clock Off Job" />
		
	</label>
	<div class="controls">
        <g:hiddenField name="m000WaktuNotifikasiTargetClockOffJob_year" value="${generalParameterInstance?.m000WaktuNotifikasiTargetClockOffJob?.getYear()}" />
        <g:hiddenField name="m000WaktuNotifikasiTargetClockOffJob_month" value="${generalParameterInstance?.m000WaktuNotifikasiTargetClockOffJob?.getMonth()}" />
        <g:hiddenField name="m000WaktuNotifikasiTargetClockOffJob_date" value="${generalParameterInstance?.m000WaktuNotifikasiTargetClockOffJob?.getDate()}" />
	<select id="m000WaktuNotifikasiTargetClockOffJob_hour" name="m000WaktuNotifikasiTargetClockOffJob_hour" style="width: 60px" >
							<g:each var="i" in="${ (0..<24) }">
							    <g:if test="${i < 10}">
							    	<g:if test="${i==generalParameterInstance?.m000WaktuNotifikasiTargetClockOffJob?.getHours()}">
							    		<option value="${i}" selected="">0${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">0${i}</option>
							    	</g:else>
							    </g:if>
							    <g:else>
							    	<g:if test="${i==generalParameterInstance?.m000WaktuNotifikasiTargetClockOffJob?.getHours()}">
							    		<option value="${i}" selected="">${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">${i}</option>
							    	</g:else>
							    </g:else>
							</g:each>
				        </select> H :
				        <select id="m000WaktuNotifikasiTargetClockOffJob_minute" name="m000WaktuNotifikasiTargetClockOffJob_minute" style="width: 60px" >
				           <g:each var="i" in="${ (0..<60) }">
				           		<g:if test="${i < 10}">
							    	<g:if test="${i==generalParameterInstance?.m000WaktuNotifikasiTargetClockOffJob?.getMinutes()}">
							    		<option value="${i}" selected="">0${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">0${i}</option>
							    	</g:else>
							    </g:if>
							    <g:else>
							    	<g:if test="${i==generalParameterInstance?.m000WaktuNotifikasiTargetClockOffJob?.getMinutes()}">
							    		<option value="${i}" selected="">${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">${i}</option>
							    	</g:else>
							    </g:else>
							 </g:each>
				        </select> m
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000ToleransiAmbilWO', 'error')} ">
	<label class="control-label" for="m000ToleransiAmbilWO">
		<g:message code="generalParameter.m000ToleransiAmbilWO.label" default="Toleransi Waktu untuk menampilkan Alert Pengambilan WO sebelum Jam Mulai Production" /><span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
        <g:hiddenField name="m000ToleransiAmbilWO_year" value="${generalParameterInstance?.m000ToleransiAmbilWO?.getYear()}" />
        <g:hiddenField name="m000ToleransiAmbilWO_month" value="${generalParameterInstance?.m000ToleransiAmbilWO?.getMonth()}" />
        <g:hiddenField name="m000ToleransiAmbilWO_date" value="${generalParameterInstance?.m000ToleransiAmbilWO?.getDate()}" />

        <select id="m000ToleransiAmbilWO_hour" name="m000ToleransiAmbilWO_hour" style="width: 60px" required="">
							<g:each var="i" in="${ (0..<24) }">
							    <g:if test="${i < 10}">
							    	<g:if test="${i==generalParameterInstance?.m000ToleransiAmbilWO?.getHours()}">
							    		<option value="${i}" selected="">0${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">0${i}</option>
							    	</g:else>
							    </g:if>
							    <g:else>
							    	<g:if test="${i==generalParameterInstance?.m000ToleransiAmbilWO?.getHours()}">
							    		<option value="${i}" selected="">${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">${i}</option>
							    	</g:else>
							    </g:else>
							</g:each>
				        </select> H :
				        <select id="m000ToleransiAmbilWO_minute" name="m000ToleransiAmbilWO_minute" style="width: 60px" required="">
				           <g:each var="i" in="${ (0..<60) }">
				           		<g:if test="${i < 10}">
							    	<g:if test="${i==generalParameterInstance?.m000ToleransiAmbilWO?.getMinutes()}">
							    		<option value="${i}" selected="">0${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">0${i}</option>
							    	</g:else>
							    </g:if>
							    <g:else>
							    	<g:if test="${i==generalParameterInstance?.m000ToleransiAmbilWO?.getMinutes()}">
							    		<option value="${i}" selected="">${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">${i}</option>
							    	</g:else>
							    </g:else>
							 </g:each>
				        </select> m :
						 <select id="m000ToleransiAmbilWO_second" name="m000ToleransiAmbilWO_second" style="width: 60px" required="">
				           <g:each var="i" in="${ (0..<60) }">
				           		<g:if test="${i < 10}">
							    	<g:if test="${i==generalParameterInstance?.m000ToleransiAmbilWO?.getSeconds()}">
							    		<option value="${i}" selected="">0${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">0${i}</option>
							    	</g:else>
							    </g:if>
							    <g:else>
							    	<g:if test="${i==generalParameterInstance?.m000ToleransiAmbilWO?.getSeconds()}">
							    		<option value="${i}" selected="">${i}</option>
							    	</g:if>
							    	<g:else>
							    		<option value="${i}">${i}</option>
							    	</g:else>
							    </g:else>
							 </g:each>
				        </select> s
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000ToleransiIDRPerProses', 'error')} ">
	<label class="control-label" for="m000ToleransiIDRPerProses">
		<g:message code="generalParameter.m000ToleransiIDRPerProses.label" default="Persentase toleransi waktu sebelum waktu target selesai untuk Inspection During Repair per proses" /><span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
	<g:textField class="percentage" name="m000ToleransiIDRPerProses" value="${generalParameterInstance.m000ToleransiIDRPerProses}" required=""/>
	&nbsp;% (Body & Paint)
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000ToleransiIDRPerJob', 'error')} ">
	<label class="control-label" for="m000ToleransiIDRPerJob">
		<g:message code="generalParameter.m000ToleransiIDRPerJob.label" default="Persentase toleransi waktu sebelum waktu target selesai untuk Inspection During Repair per job" /><span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
	<g:textField class="percentage" name="m000ToleransiIDRPerJob" value="${generalParameterInstance.m000ToleransiIDRPerJob}" required=""/>
	&nbsp;% (General Repair)
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000ToleransiProsesBP', 'error')} ">
	<label class="control-label" for="m000ToleransiProsesBP">
		<g:message code="generalParameter.m000ToleransiProsesBP.label" default="Toleransi waktu keterlambatan proses BP" /><span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
	%{--<g:field type="number" name="m000ToleransiProsesBP" value="${generalParameterInstance.m000ToleransiProsesBP}" />--}%
    <g:hiddenField name="m000ToleransiProsesBP_year" value="${generalParameterInstance?.m000ToleransiProsesBP?.getYear()}" />
    <g:hiddenField name="m000ToleransiProsesBP_month" value="${generalParameterInstance?.m000ToleransiProsesBP?.getMonth()}" />
    <g:hiddenField name="m000ToleransiProsesBP_date" value="${generalParameterInstance?.m000ToleransiProsesBP?.getDate()}" />

        <select id="m000ToleransiProsesBP_hour" name="m000ToleransiProsesBP_hour" style="width: 60px" required="">
            <g:each var="i" in="${ (0..<24) }">
                <g:if test="${i < 10}">
                    <g:if test="${i==generalParameterInstance?.m000ToleransiProsesBP?.getHours()}">
                        <option value="${i}" selected="">0${i}</option>
                    </g:if>
                    <g:else>
                        <option value="${i}">0${i}</option>
                    </g:else>
                </g:if>
                <g:else>
                    <g:if test="${i==generalParameterInstance?.m000ToleransiProsesBP?.getHours()}">
                        <option value="${i}" selected="">${i}</option>
                    </g:if>
                    <g:else>
                        <option value="${i}">${i}</option>
                    </g:else>
                </g:else>
            </g:each>
        </select> H :
        <select id="m000ToleransiProsesBP_minute" name="m000ToleransiProsesBP_minute" style="width: 60px" required="">
            <g:each var="i" in="${ (0..<60) }">
                <g:if test="${i < 10}">
                    <g:if test="${i==generalParameterInstance?.m000ToleransiProsesBP?.getMinutes()}">
                        <option value="${i}" selected="">0${i}</option>
                    </g:if>
                    <g:else>
                        <option value="${i}">0${i}</option>
                    </g:else>
                </g:if>
                <g:else>
                    <g:if test="${i==generalParameterInstance?.m000ToleransiProsesBP?.getMinutes()}">
                        <option value="${i}" selected="">${i}</option>
                    </g:if>
                    <g:else>
                        <option value="${i}">${i}</option>
                    </g:else>
                </g:else>
            </g:each>
        </select> m :
        <select id="m000ToleransiProsesBP_second" name="m000ToleransiProsesBP_second" style="width: 60px" required="">
            <g:each var="i" in="${ (0..<60) }">
                <g:if test="${i < 10}">
                    <g:if test="${i==generalParameterInstance?.m000ToleransiProsesBP?.getSeconds()}">
                        <option value="${i}" selected="">0${i}</option>
                    </g:if>
                    <g:else>
                        <option value="${i}">0${i}</option>
                    </g:else>
                </g:if>
                <g:else>
                    <g:if test="${i==generalParameterInstance?.m000ToleransiProsesBP?.getSeconds()}">
                        <option value="${i}" selected="">${i}</option>
                    </g:if>
                    <g:else>
                        <option value="${i}">${i}</option>
                    </g:else>
                </g:else>
            </g:each>
        </select> s
    </div>


</div>
				</fieldset>
				<fieldset class="buttons controls">
					<g:actionSubmit class="btn btn-primary save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
				</fieldset>
			%{--</g:formRemote>--}%
            </form>
		</div>
	</body>
</html>
