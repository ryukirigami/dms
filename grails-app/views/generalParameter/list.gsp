<%@ page import="com.kombos.administrasi.GeneralParameter"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<title>General Parameter</title>
<r:require modules="baseapplayout" />
<g:javascript>
			var gp_man_power_loaded = false;
			var gp_appointment_loaded = false;
			var gp_reception_loaded = false;
			var gp_production_loaded = false;
			var gp_delivery_loaded = false;
			var gp_security_loaded = false;
			var gp_static_discount_loaded = false;
			var gp_dinamic_discount_loaded = false;
			var gp_discount_customer_vehicle_loaded = false;
			var gp_web_loaded = false;
			var gp_follow_up_loaded = false;
			var gp_report_loaded = false;
			var gp_refund_loaded = false;
			var gp_materai_loaded = false;
			function isNumberKey(evt)
            {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }
			$(function(){
			    function loadGP(id, url){
			        $('#spinner').fadeIn();
                    $.ajax({url: url,
                        type: "GET",
                        dataType: "html",
                        complete : function (req, err) {
                            $(id).append(req.responseText);
                            $('#spinner').fadeOut();
                        }
                    });
			    }
			    loadGP('#gp_general', '${request.contextPath}/generalParameter/editGeneral');

				$('#tab_gp_general').click(function() {
				    clearAllTab();
					loadGP('#gp_general', '${request.contextPath}/generalParameter/editGeneral');
				});
				
				$('#tab_gp_man_power').click(function() {
				    clearAllTab();
					loadGP('#gp_man_power', '${request.contextPath}/generalParameter/editManPower');
				});
				$('#tab_gp_appointment').click(function() {
				    clearAllTab();
					 loadGP('#gp_appointment', '${request.contextPath}/generalParameter/editAppointment');
				});
				$('#tab_gp_reception').click(function() {
				    clearAllTab();
					loadGP('#gp_reception', '${request.contextPath}/generalParameter/editReception');
				});
				$('#tab_gp_production').click(function() {
				    clearAllTab();
				    loadGP('#gp_production', '${request.contextPath}/generalParameter/editProduction');
				});
				$('#tab_gp_delivery').click(function() {
				    clearAllTab();
				    loadGP('#gp_delivery', '${request.contextPath}/generalParameter/editDelivery');
				});
				$('#tab_gp_security').click(function() {
				    clearAllTab();
				    loadGP('#gp_security', '${request.contextPath}/generalParameter/editSecurity');
				});
				$('#tab_gp_static_discount').click(function() {
				    clearAllTab();
				    loadGP('#gp_static_discount', '${request.contextPath}/staticDisc');
				});
				$('#tab_gp_dinamic_discount').click(function() {
				    clearAllTab();
				    loadGP('#gp_dinamic_discount', '${request.contextPath}/dinamicDisc');
				});
				$('#tab_gp_discount_customer_vehicle').click(function() {
				    clearAllTab();
				    loadGP('#gp_discount_customer_vehicle', '${request.contextPath}/generalParameter/editDiscountCustomerVehicle');
				});
				$('#tab_gp_web').click(function() {
				    clearAllTab();
				    loadGP('#gp_web', '${request.contextPath}/generalParameter/editWeb');
				});
				$('#tab_gp_follow_up').click(function() {
				    clearAllTab();
				    loadGP('#gp_follow_up', '${request.contextPath}/generalParameter/editFollowUp');
				});
				$('#tab_gp_report').click(function() {
				    clearAllTab();
				    loadGP('#gp_report', '${request.contextPath}/generalParameter/editReport');
				});
				$('#tab_gp_refund').click(function() {
				    clearAllTab();
				    loadGP('#gp_refund', '${request.contextPath}/alasanRefund');
				});
				$('#tab_gp_materai').click(function() {
				    clearAllTab();
				    loadGP('#gp_materai', '${request.contextPath}/materai');
				});
				$('#tab_gp_biaya_administrasi').click(function() {
				    clearAllTab();
				    loadGP('#gp_biayaAdministrasi', '${request.contextPath}/biayaAdministrasi');
				});
				$('#tab_gp_nama_dokumen').click(function() {
					clearAllTab();
					loadGP('#gp_nama_dokumen', '${request.contextPath}/namaDokumen');
				});

				function clearAllTab(){
				    $('#gp_general').empty();
				    $('#gp_man_power').empty();
				    $('#gp_appointment').empty();
				    $('#gp_reception').empty();
				    $('#gp_production').empty();
				    $('#gp_delivery').empty();
				    $('#gp_security').empty();
				    $('#gp_static_discount').empty();
				    $('#gp_dinamic_discount').empty();
				    $('#gp_discount_customer_vehicle').empty();
				    $('#gp_web').empty();
				    $('#gp_follow_up').empty();
				    $('#gp_report').empty();
				    $('#gp_refund').empty();
				    $('#gp_materai').empty();
				    $('#gp_biayaAdministrasi').empty();
				    $('#gp_nama_dokumen').empty();
				}

			});
		</g:javascript>
</head>
<body>
	<div class="navbar box-header no-border">
		<span class="pull-left">General Parameter</span>
	</div>
	<div class="box">
		<div class="tabbable tabs-left">
			<ul class="nav nav-tabs span3" style="">
				<li id="tab_gp_general" class="active"><a href="#gp_general" data-toggle="tab">General</a></li>
				<li id="tab_gp_man_power" class=""><a href="#gp_man_power" data-toggle="tab">Man Power</a></li>
				<li id="tab_gp_appointment" class=""><a href="#gp_appointment" data-toggle="tab">Appointment</a></li>
				<li id="tab_gp_reception" class=""><a href="#gp_reception" data-toggle="tab">Reception</a></li>
				<li id="tab_gp_production" class=""><a href="#gp_production" data-toggle="tab">Production</a></li>
				<li id="tab_gp_delivery" class=""><a href="#gp_delivery" data-toggle="tab">Delivery</a></li>
				<li id="tab_gp_security" class=""><a href="#gp_security" data-toggle="tab">Security</a></li>
				<li id="tab_gp_static_discount" class=""><a href="#gp_static_discount" data-toggle="tab">Static Discount</a></li>
				<li id="tab_gp_dinamic_discount" class=""><a href="#gp_dinamic_discount" data-toggle="tab">Dinamic Discount</a></li>
				<li id="tab_gp_discount_customer_vehicle" class=""><a href="#gp_discount_customer_vehicle" data-toggle="tab">Discount Customer Vehicle</a></li>
				<li id="tab_gp_web" class=""><a href="#gp_web" data-toggle="tab">Web</a></li>
				<li id="tab_gp_follow_up" class=""><a href="#gp_follow_up" data-toggle="tab">Follow Up</a></li>
				<li id="tab_gp_report" class=""><a href="#gp_report" data-toggle="tab">Report</a></li>
				<li id="tab_gp_refund" class=""><a href="#gp_refund" data-toggle="tab">Refund</a></li>
				<li id="tab_gp_materai" class=""><a href="#gp_materai" data-toggle="tab">Materai</a></li>
				<li id="tab_gp_biaya_administrasi" class=""><a href="#gp_biayaAdministrasi" data-toggle="tab">Biaya Administrasi</a></li>
				<li id="tab_gp_nama_dokumen" class=""><a href="#gp_nama_dokumen" data-toggle="tab">Nama Dokumen</a></li>
			</ul>
			<div class="tab-content span8"
									style="">
				<div class="tab-pane active" id="gp_general"/>
				<div class="tab-pane" id="gp_man_power" />
				<div class="tab-pane" id="gp_appointment" />
				<div class="tab-pane" id="gp_reception" />
				<div class="tab-pane" id="gp_production" />
				<div class="tab-pane" id="gp_delivery" />
				<div class="tab-pane" id="gp_security" />
				<div class="tab-pane" id="gp_static_discount" />
				<div class="tab-pane" id="gp_dinamic_discount" />
				<div class="tab-pane" id="gp_discount_customer_vehicle" />
				<div class="tab-pane" id="gp_web" />
				<div class="tab-pane" id="gp_follow_up" />
				<div class="tab-pane" id="gp_report" />
				<div class="tab-pane" id="gp_refund" />
				<div class="tab-pane" id="gp_materai" />
				<div class="tab-pane" id="gp_biayaAdministrasi" />
				<div class="tab-pane" id="gp_nama_dokumen" />
			</div>
		</div>
	</div>
</body>
</html>
