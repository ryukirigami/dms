<%@ page import="com.kombos.administrasi.GeneralParameter" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'generalParameter.label', default: 'GeneralParameter')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="edit-generalParameter-manpower" class="content scaffold-edit general-parameter" role="main">
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${generalParameterInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${generalParameterInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:javascript>
			var updateStatus;
            var submitForm;
			$(function(){
			    $('.gpnumber').autoNumeric('init',{
                    vMin:'0',
                    vMax:'9999',
                    mDec: null,
                    aSep:''
                });

				updateStatus = function(data){
                    $("#version").val(data.version);
					toastr.success('<div>'+data.message+'</div>');
				};

                submitForm = function(){
                        if($('#formEditManPower').valid()){
                            $.ajax({
                                type:'POST',
                                data:jQuery('#formEditManPower').serialize(),
                                url:'${request.getContextPath()}/generalParameter/update',
                                success:function(data,textStatus){
                                    updateStatus(data);
                                },
                                error:function(XMLHttpRequest,textStatus,errorThrown){}});
                        }
                    }

                    $.validator.addMethod("noDoubleAssign", function (value, element) {
                        var $element = $(element)
                            , $check, res = false;

                        $check = $("." + $element.data("check"));
                        $check.each(function() {
                              $this =  $(this);
                              if($element.attr('id') != $this.attr('id')){
                                if (value == $this.val())
                                    res = true;
                              }
                         });

                         if(value == 'null'){
                            res = false;
                         }

//                        if (this.settings.onfocusout) {
//                            $min.off(".validate-greaterThan").on("blur.validate-greaterThan", function () {
//                                $element.valid();
//                            });
//                        }
                        return !res;
                    }, "User tidak boleh diassign 2 kali.");

                    $.validator.addClassRules({
                        checkAssign: {
                            noDoubleAssign: true
                        }
                    });
			});
			</g:javascript>
        <form id="formEditManPower" class="form-horizontal" action="${request.getContextPath()}/generalParameter/update" method="post" onsubmit="submitForm();return false;">
			%{--<g:formRemote class="form-horizontal" name="edit" on404="alert('not found!');" onSuccess="updateStatus(data);" --}%
				%{--url="[controller: 'generalParameter', action:'update']">--}%
				<g:hiddenField name="id" value="${generalParameterInstance?.id}" />
				<g:hiddenField name="version" value="${generalParameterInstance?.version}" />
				<fieldset class="form">
					<fieldset>
					<legend>Tipe Jam Kerja</legend>
					
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000staJenisJamKerjaSenin', 'error')} required">
						<label class="control-label" for="m000staJenisJamKerja">
						Senin	
						</label>
						<div class="controls">
						<g:select name="m000staJenisJamKerjaSenin" from="${jenisJamKerjaList}" optionKey="id" 
							                		noSelection="['null': '[Select Jenis Jam Kerja]']"
							                		value="${generalParameterInstance?.m000staJenisJamKerjaSenin}"/>
						</div>
					</div>
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000staJenisJamKerjaSelasa', 'error')} required">
						<label class="control-label" for="m000staJenisJamKerjaSelasa">
						Selasa
						</label>
						<div class="controls">
						<g:select name="m000staJenisJamKerjaSelasa" from="${jenisJamKerjaList}" optionKey="id" 
							                		noSelection="['null': '[Select Jenis Jam Kerja]']"
							                		value="${generalParameterInstance?.m000staJenisJamKerjaSelasa}"/>
						</div>
					</div>
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000staJenisJamKerjaRabu', 'error')} required">
						<label class="control-label" for="m000staJenisJamKerjaRabu">
						Rabu
						</label>
						<div class="controls">
						<g:select name="m000staJenisJamKerjaRabu" from="${jenisJamKerjaList}" optionKey="id" 
							                		noSelection="['null': '[Select Jenis Jam Kerja]']"
							                		value="${generalParameterInstance?.m000staJenisJamKerjaRabu}"/>
						</div>
					</div>
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000staJenisJamKerjaKamis', 'error')} required">
						<label class="control-label" for="m000staJenisJamKerjaKamis">
						Kamis
						</label>
						<div class="controls">
						<g:select name="m000staJenisJamKerjaKamis" from="${jenisJamKerjaList}" optionKey="id" 
							                		noSelection="['null': '[Select Jenis Jam Kerja]']"
							                		value="${generalParameterInstance?.m000staJenisJamKerjaKamis}"/>
						</div>
					</div>
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000staJenisJamKerjaJumat', 'error')} required">
						<label class="control-label" for="m000staJenisJamKerjaJumat">
						Jumat	
						</label>
						<div class="controls">
						<g:select name="m000staJenisJamKerjaJumat" from="${jenisJamKerjaList}" optionKey="id" 
							                		noSelection="['null': '[Select Jenis Jam Kerja]']"
							                		value="${generalParameterInstance?.m000staJenisJamKerjaJumat}"/>
						</div>
					</div>
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000staJenisJamKerjaSabtu', 'error')} required">
						<label class="control-label" for="m000staJenisJamKerjaSabtu">
						Sabtu
						</label>
						<div class="controls">
						<g:select name="m000staJenisJamKerjaSabtu" from="${jenisJamKerjaList}" optionKey="id" 
							                		noSelection="['null': '[Select Jenis Jam Kerja]']"
							                		value="${generalParameterInstance?.m000staJenisJamKerjaSabtu}"/>
						</div>
					</div>
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000staJenisJamKerjaMinggu', 'error')} required">
						<label class="control-label" for="m000staJenisJamKerjaMinggu">
						Minggu
						</label>
						<div class="controls">
						<g:select name="m000staJenisJamKerjaMinggu" from="${jenisJamKerjaList}" optionKey="id" 
							                		noSelection="['null': '[Select Jenis Jam Kerja]']"
							                		value="${generalParameterInstance?.m000staJenisJamKerjaMinggu}"/>
						</div>
					</div>
					</fieldset>
					<fieldset>
					<legend></legend>
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000BulanWarningSertifikat', 'error')} ">
						<label class="control-label" for="m000BulanWarningSertifikat">
							<g:message code="generalParameter.m000BulanWarningSertifikat.label" default="Warning Jatuh Tempo Sertifikat" /><span class="required-indicator">*</span>
							
						</label>
						<div class="controls">
                            <g:textField class="gpnumber" name="m000BulanWarningSertifikat" value="${generalParameterInstance?.m000BulanWarningSertifikat}" required=""/>&nbsp;Bulan Sebelumnya
                        	%{--<g:field type="number" onkeypress="return isNumberKey(event)" min="0" max="9999" onKeyDown="if(this.value.length==4) return false;" name="m000BulanWarningSertifikat" value="${generalParameterInstance.m000BulanWarningSertifikat}" required=""/>&nbsp;Bulan Sebelumnya--}%
						</div>
					</div>
					</fieldset>
					<fieldset>
					<legend>Warning dikirim kepada:</legend>
					
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000Person1', 'error')} ">
						<label class="control-label" for="m000Person1">
							<g:message code="generalParameter.m000Person1.label" default="Person 1" />
							
						</label>
						<div class="controls">
						<g:select name="m000Person1" class="checkAssign" data-check="checkAssign" from="${userList}" optionKey="username" optionValue="label"
							                		noSelection="['null': '[Select User Profile]']"
							                		value="${generalParameterInstance?.m000Person1}"/>
						</div>
					</div>
					
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000Person2', 'error')} ">
						<label class="control-label" for="m000Person2">
							<g:message code="generalParameter.m000Person2.label" default="Person 2" />
							
						</label>
						<div class="controls">
						<g:select name="m000Person2" class="checkAssign" data-check="checkAssign" from="${userList}" optionKey="username" optionValue="label"
							                		noSelection="['null': '[Select User Profile]']"
							                		value="${generalParameterInstance?.m000Person2}"/>
						</div>
					</div>
					
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000Person3', 'error')} ">
						<label class="control-label" for="m000Person3">
							<g:message code="generalParameter.m000Person3.label" default="Person 3" />
							
						</label>
						<div class="controls">
						<g:select name="m000Person3" class="checkAssign" data-check="checkAssign" from="${userList}" optionKey="username" optionValue="label"
							                		noSelection="['null': '[Select User Profile]']"
							                		value="${generalParameterInstance?.m000Person3}"/>
						</div>
					</div>
					
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000Person4', 'error')} ">
						<label class="control-label" for="m000Person4">
							<g:message code="generalParameter.m000Person4.label" default="Person 4" />
							
						</label>
						<div class="controls">
						<g:select name="m000Person4" class="checkAssign" data-check="checkAssign" from="${userList}" optionKey="username" optionValue="label"
							                		noSelection="['null': '[Select User Profile]']"
							                		value="${generalParameterInstance?.m000Person4}"/>
						</div>
					</div>
					
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000Person5', 'error')} ">
						<label class="control-label" for="m000Person5">
							<g:message code="generalParameter.m000Person5.label" default="Person 5" />
							
						</label>
						<div class="controls">
						<g:select name="m000Person5" class="checkAssign" data-check="checkAssign" from="${userList}" optionKey="username" optionValue="label"
							                		noSelection="['null': '[Select User Profile]']"
							                		value="${generalParameterInstance?.m000Person5}"/>
						</div>
					</div>
					</fieldset>
				</fieldset>
				<fieldset class="buttons controls">
					<g:actionSubmit class="btn btn-primary save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
				</fieldset>
            </form>
			%{--</g:formRemote>--}%
		</div>
	</body>
</html>
