
<%@ page import="com.kombos.administrasi.Materai" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="materai_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="materai.createdBy.label" default="Created By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="materai.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="materai.m705Nilai1.label" default="M705 Nilai1" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="materai.m705Nilai2.label" default="M705 Nilai2" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="materai.m705NilaiMaterai.label" default="M705 Nilai Materai" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="materai.m705TglBerlaku.label" default="M705 Tgl Berlaku" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="materai.updatedBy.label" default="Updated By" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_createdBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m705Nilai1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m705Nilai1" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m705Nilai2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m705Nilai2" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m705NilaiMaterai" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m705NilaiMaterai" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m705TglBerlaku" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m705TglBerlaku" value="date.struct">
					<input type="hidden" name="search_m705TglBerlaku_day" id="search_m705TglBerlaku_day" value="">
					<input type="hidden" name="search_m705TglBerlaku_month" id="search_m705TglBerlaku_month" value="">
					<input type="hidden" name="search_m705TglBerlaku_year" id="search_m705TglBerlaku_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m705TglBerlaku_dp" value="" id="search_m705TglBerlaku" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_updatedBy" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var materaiTable;
var reloadMateraiTable;
$(function(){
	
	reloadMateraiTable = function() {
		materaiTable.fnDraw();
	}

	
	$('#search_m705TglBerlaku').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m705TglBerlaku_day').val(newDate.getDate());
			$('#search_m705TglBerlaku_month').val(newDate.getMonth()+1);
			$('#search_m705TglBerlaku_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			materaiTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	materaiTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	materaiTable = $('#materai_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesMateraiList")}",
		"aoColumns": [

{
	"sName": "createdBy",
	"mDataProp": "createdBy",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m705Nilai1",
	"mDataProp": "m705Nilai1",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m705Nilai2",
	"mDataProp": "m705Nilai2",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m705NilaiMaterai",
	"mDataProp": "m705NilaiMaterai",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m705TglBerlaku",
	"mDataProp": "m705TglBerlaku",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "updatedBy",
	"mDataProp": "updatedBy",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}
	
						var m705Nilai1 = $('#filter_m705Nilai1 input').val();
						if(m705Nilai1){
							aoData.push(
									{"name": 'sCriteria_m705Nilai1', "value": m705Nilai1}
							);
						}
	
						var m705Nilai2 = $('#filter_m705Nilai2 input').val();
						if(m705Nilai2){
							aoData.push(
									{"name": 'sCriteria_m705Nilai2', "value": m705Nilai2}
							);
						}
	
						var m705NilaiMaterai = $('#filter_m705NilaiMaterai input').val();
						if(m705NilaiMaterai){
							aoData.push(
									{"name": 'sCriteria_m705NilaiMaterai', "value": m705NilaiMaterai}
							);
						}

						var m705TglBerlaku = $('#search_m705TglBerlaku').val();
						var m705TglBerlakuDay = $('#search_m705TglBerlaku_day').val();
						var m705TglBerlakuMonth = $('#search_m705TglBerlaku_month').val();
						var m705TglBerlakuYear = $('#search_m705TglBerlaku_year').val();
						
						if(m705TglBerlaku){
							aoData.push(
									{"name": 'sCriteria_m705TglBerlaku', "value": "date.struct"},
									{"name": 'sCriteria_m705TglBerlaku_dp', "value": m705TglBerlaku},
									{"name": 'sCriteria_m705TglBerlaku_day', "value": m705TglBerlakuDay},
									{"name": 'sCriteria_m705TglBerlaku_month', "value": m705TglBerlakuMonth},
									{"name": 'sCriteria_m705TglBerlaku_year', "value": m705TglBerlakuYear}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
