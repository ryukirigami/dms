<%@ page import="com.kombos.administrasi.GeneralParameter" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'generalParameter.label', default: 'GeneralParameter')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="edit-generalParameter-report" class="content scaffold-edit general-parameter" role="main">
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${generalParameterInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${generalParameterInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:javascript>
			var updateStatus;
            var submitForm;
			$(function(){ 
				updateStatus = function(data){
                    $("#version").val(data.version);
					toastr.success('<div>'+data.message+'</div>');
				};
                submitForm = function(){
                        if($('#formEditReport').valid()){
                            $.ajax({
                                type:'POST',
                                data:jQuery('#formEditReport').serialize(),
                                url:'${request.getContextPath()}/generalParameter/update',
                                success:function(data,textStatus){
                                    updateStatus(data);
                                },
                                error:function(XMLHttpRequest,textStatus,errorThrown){}});
                        }
                    }

                    $.validator.addMethod("greaterThan", function (value, element, param) {
                        var $element = $(element)
                            , $min;

                        if (typeof(param) === "string") {
                            $min = $(param);
                        } else {
                            $min = $("#" + $element.data("min"));
                        }

                        if (this.settings.onfocusout) {
                            $min.off(".validate-greaterThan").on("blur.validate-greaterThan", function () {
                                $element.valid();
                            });
                        }
                        return parseInt(value) > parseInt($min.val());
                    }, "Kolom akhir < kolom awal.");

                    $.validator.addMethod("greaterThan2", function (value, element, param) {
                        var $element = $(element)
                            , $min;

                        if (typeof(param) === "string") {
                            $min = $(param);
                        } else {
                            $min = $("#" + $element.data("min"));
                        }

                        if (this.settings.onfocusout) {
                            $min.off(".validate-greaterThan2").on("blur.validate-greaterThan2", function () {
                                $element.valid();
                            });
                        }
                        return parseInt(value) >= parseInt($min.val());
                    }, "Kolom awal < atau = kolom akhir sebelumnya.");


                    $.validator.addClassRules({
                        greaterThan: {
                            greaterThan: true
                        },
                        greaterThan2: {
                            greaterThan2: true
                        }
                    });
                     $('.gpnumber').autoNumeric('init',{
                        vMin:'0',
                        vMax:'9999',
                        mDec: null,
                        aSep:''
                    });
			});
			</g:javascript>
            <form id="formEditReport" class="form-horizontal" action="${request.getContextPath()}/generalParameter/update" method="post" onsubmit="submitForm();;return false;">
			%{--<g:formRemote class="form-horizontal" name="edit" on404="alert('not found!');" onSuccess="updateStatus(data);"--}%
				%{--url="[controller: 'generalParameter', action:'update']">--}%
				<g:hiddenField name="id" value="${generalParameterInstance?.id}" />
				<g:hiddenField name="version" value="${generalParameterInstance?.version}" />
				<fieldset class="form">
					<legend>Aging</legend>
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000Aging11', 'error')} ">
						<label class="control-label" for="m000Aging11">
							<g:message code="generalParameter.m000Aging11.label" default="Kolom 1" /><span class="required-indicator">*</span>
						</label>
						<div class="controls">
                            <span class="pull-left"><g:textField class="gpnumber" name="m000Aging11" value="${generalParameterInstance?.m000Aging11}" required=""/></span>
                                %{--<g:field type="number" name="m000Aging11" onkeypress="return isNumberKey(event)" min="0" max="1000000" value="${generalParameterInstance.m000Aging11}" required=""/></span>--}%
                            <span class="pull-left">&nbsp;s.d.&nbsp;</span>
                            <span class="pull-left"><g:textField class="gpnumber greaterThan" data-min="m000Aging11" name="m000Aging12" value="${generalParameterInstance?.m000Aging12}" required=""/></span>
                                %{--<g:field type="number" class="greaterThan" data-min="m000Aging11" name="m000Aging12" onkeypress="return isNumberKey(event)" min="0" max="1000000" value="${generalParameterInstance.m000Aging12}" required=""/></span>--}%
                        </div>
					</div>

					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000Aging21', 'error')} ">
						<label class="control-label" for="m000Aging21">
							<g:message code="generalParameter.m000Aging21.label" default="Kolom 2" /><span class="required-indicator">*</span>
						</label>
						<div class="controls">
                            <span class="pull-left"><g:textField class="gpnumber greaterThan2" data-min="m000Aging12" name="m000Aging21" value="${generalParameterInstance?.m000Aging21}" required=""/></span>
                                %{--<g:field type="number" name="m000Aging21" class="greaterThan2" data-min="m000Aging12" onkeypress="return isNumberKey(event)" min="0" max="1000000" value="${generalParameterInstance.m000Aging21}" required=""/></span>--}%
                            <span class="pull-left">&nbsp;s.d.&nbsp;</span>
                            <span class="pull-left"><g:textField class="gpnumber greaterThan" data-min="m000Aging21" name="m000Aging22" value="${generalParameterInstance?.m000Aging22}" required=""/></span>
                                %{--<g:field type="number" name="m000Aging22" class="greaterThan" data-min="m000Aging21" onkeypress="return isNumberKey(event)" min="0" max="1000000" value="${generalParameterInstance.m000Aging22}" required=""/></span>--}%
                        </div>
					</div>

					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000Aging31', 'error')} ">
						<label class="control-label" for="m000Aging31">
							<g:message code="generalParameter.m000Aging31.label" default="Kolom 3" /><span class="required-indicator">*</span>
						</label>
						<div class="controls">
                            <span class="pull-left"><g:textField class="gpnumber greaterThan2" data-min="m000Aging22" name="m000Aging31" value="${generalParameterInstance?.m000Aging31}" required=""/></span>
                                %{--<g:field type="number" name="m000Aging31" class="greaterThan2" data-min="m000Aging22" onkeypress="return isNumberKey(event)" min="0" max="1000000" value="${generalParameterInstance.m000Aging31}" required=""/></span>--}%
                            <span class="pull-left">&nbsp;s.d.&nbsp;</span>
                            <span class="pull-left"><g:textField class="gpnumber greaterThan" data-min="m000Aging31" name="m000Aging32" value="${generalParameterInstance?.m000Aging32}" required=""/></span>
                                %{--<g:field type="number" name="m000Aging32" class="greaterThan" data-min="m000Aging31" onkeypress="return isNumberKey(event)" min="0" max="1000000" value="${generalParameterInstance.m000Aging32}" required=""/></span>--}%
						</div>
					</div>

					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000Aging41', 'error')} ">
						<label class="control-label" for="m000Aging41">
							<g:message code="generalParameter.m000Aging41.label" default="Kolom 4" /><span class="required-indicator">*</span>
						</label>
						<div class="controls">
                            <span class="pull-left"><g:textField class="gpnumber greaterThan2" data-min="m000Aging32" name="m000Aging41" value="${generalParameterInstance?.m000Aging41}" required=""/></span>
                                %{--<g:field type="number" name="m000Aging41" class="greaterThan2" data-min="m000Aging32" onkeypress="return isNumberKey(event)" min="0" max="1000000" value="${generalParameterInstance.m000Aging41}" required=""/></span>--}%
                            <span class="pull-left">&nbsp;s.d.&nbsp;</span>
                            <span class="pull-left"><g:textField class="gpnumber greaterThan" data-min="m000Aging41" name="m000Aging42" value="${generalParameterInstance?.m000Aging42}" required=""/></span>
                                %{--<g:field type="number" name="m000Aging42" class="greaterThan" data-min="m000Aging41" onkeypress="return isNumberKey(event)" min="0" max="1000000" value="${generalParameterInstance.m000Aging42}" required=""/></span>--}%
						</div>
					</div>

					

					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000Aging51', 'error')} ">
						<label class="control-label" for="m000Aging51">
							<g:message code="generalParameter.m000Aging51.label" default="Kolom 5" /><span class="required-indicator">*</span>
						</label>
						<div class="controls">
                            <span class="pull-left"><g:textField class="gpnumber greaterThan2" data-min="m000Aging42" name="m000Aging51" value="${generalParameterInstance?.m000Aging51}" required=""/></span>
                                %{--<g:field type="number" name="m000Aging51" class="greaterThan2" data-min="m000Aging42" onkeypress="return isNumberKey(event)" min="0" max="1000000" value="${generalParameterInstance.m000Aging51}" required=""/></span>--}%
                            <span class="pull-left">&nbsp;s.d.&nbsp;</span>
                            <span class="pull-left"><g:textField class="gpnumber greaterThan" data-min="m000Aging51" name="m000Aging52" value="${generalParameterInstance?.m000Aging52}" required=""/></span>
                                %{--<g:field type="number" name="m000Aging52" class="greaterThan" data-min="m000Aging51" onkeypress="return isNumberKey(event)" min="0" max="1000000" value="${generalParameterInstance.m000Aging52}" required=""/></span>--}%
						</div>
					</div>
				</fieldset>
				<fieldset class="buttons controls">
					<g:actionSubmit class="btn btn-primary save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
				</fieldset>
            </form>
			%{--</g:formRemote>--}%
		</div>
	</body>
</html>
