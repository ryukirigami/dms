<%@ page import="com.kombos.administrasi.GeneralParameter" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'generalParameter.label', default: 'GeneralParameter')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="edit-generalParameter-followup" class="content scaffold-edit general-parameter" role="main">
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${generalParameterInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${generalParameterInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:javascript>
			var updateStatus;
			$(function(){ 
				updateStatus = function(data){
                    $("#version").val(data.version);
					toastr.success('<div>'+data.message+'</div>');
				};
                $('.gpnumber').autoNumeric('init',{
                    vMin:'0',
                    vMax:'9999',
                    mDec: null,
                    aSep:''
                });
			});
			</g:javascript>
			
			<g:formRemote class="form-horizontal" name="edit" on404="alert('not found!');" onSuccess="updateStatus(data);"
				url="[controller: 'generalParameter', action:'update']">
				<g:hiddenField name="id" value="${generalParameterInstance?.id}" />
				<g:hiddenField name="version" value="${generalParameterInstance?.version}" />
				<fieldset class="form">
					<div class="control-group fieldcontain ${hasErrors(bean: generalParameterInstance, field: 'm000JmlMaxKirimSMSFollowUp', 'error')} ">
					<label class="control-label" for="m000JmlMaxKirimSMSFollowUp">
						<g:message code="generalParameter.m000JmlMaxKirimSMSFollowUp.label" default="Jumlah Maks. Pengiriman SMS Customer Follow Up" /><span class="required-indicator">*</span>
						
					</label>
					<div class="controls">
                        <g:textField class="gpnumber" name="m000JmlMaxKirimSMSFollowUp" value="${generalParameterInstance?.m000JmlMaxKirimSMSFollowUp}" required=""/>
					%{--<g:field type="number" name="m000JmlMaxKirimSMSFollowUp" onkeypress="return isNumberKey(event)" min="0" max="9999" onKeyDown="if(this.value.length==4) return false;" value="${generalParameterInstance.m000JmlMaxKirimSMSFollowUp}" required=""/>--}%
					</div>
				</div>
				</fieldset>
				<fieldset class="buttons controls">
					<g:actionSubmit class="btn btn-primary save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
				</fieldset>
			</g:formRemote>
		</div>
	</body>
</html>
