
<%@ page import="com.kombos.hrd.CertificationType" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'certificationType.label', default: 'CertificationType')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
        <style>
        .form-horizontal input + .help-block, .form-horizontal select + .help-block, .form-horizontal textarea + .help-block, .form-horizontal .uneditable-input + .help-block, .form-horizontal .input-prepend + .help-block, .form-horizontal .input-append + .help-block {
            font-size: 10px !important;
            font-style: italic !important;
            margin-top: 6px !important;
        }
        </style>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			$(function(){ 
			
				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('certificationType');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('certificationType', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('certificationType', '${request.contextPath}/certificationType/massdelete', reloadCertificationTypeTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('certificationType','${request.contextPath}/certificationType/show/'+id);
				};
				
				edit = function(id) {
					editInstance('certificationType','${request.contextPath}/certificationType/edit/'+id);
				};

});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left">Tipe Sertifikasi - List</span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="certificationType-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
		</div>
		<div class="span7" id="certificationType-form" style="display: none;padding: 10px; box-shadow: 0px 1px 2px gray;"></div>
	</div>
</body>
</html>
