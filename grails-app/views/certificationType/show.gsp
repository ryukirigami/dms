

<%@ page import="com.kombos.hrd.CertificationType" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'certificationType.label', default: 'CertificationType')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteCertificationType;

$(function(){ 
	deleteCertificationType=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/certificationType/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadCertificationTypeTable();
   				expandTableLayout('certificationType');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-certificationType" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="certificationType"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${certificationTypeInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="certificationType.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${certificationTypeInstance}" field="createdBy"
								url="${request.contextPath}/CertificationType/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadCertificationTypeTable();" />--}%
							
								<g:fieldValue bean="${certificationTypeInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${certificationTypeInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="certificationType.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${certificationTypeInstance}" field="dateCreated"
								url="${request.contextPath}/CertificationType/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadCertificationTypeTable();" />--}%
							
								<g:formatDate date="${certificationTypeInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${certificationTypeInstance?.keterangan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="keterangan-label" class="property-label"><g:message
					code="certificationType.keterangan.label" default="Keterangan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="keterangan-label">
						%{--<ba:editableValue
								bean="${certificationTypeInstance}" field="keterangan"
								url="${request.contextPath}/CertificationType/updatefield" type="text"
								title="Enter keterangan" onsuccess="reloadCertificationTypeTable();" />--}%
							
								<g:fieldValue bean="${certificationTypeInstance}" field="keterangan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${certificationTypeInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="certificationType.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${certificationTypeInstance}" field="lastUpdProcess"
								url="${request.contextPath}/CertificationType/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadCertificationTypeTable();" />--}%
							
								<g:fieldValue bean="${certificationTypeInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${certificationTypeInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="certificationType.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${certificationTypeInstance}" field="lastUpdated"
								url="${request.contextPath}/CertificationType/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadCertificationTypeTable();" />--}%
							
								<g:formatDate date="${certificationTypeInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${certificationTypeInstance?.namaSertifikasi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="namaSertifikasi-label" class="property-label"><g:message
					code="certificationType.namaSertifikasi.label" default="Nama Sertifikasi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="namaSertifikasi-label">
						%{--<ba:editableValue
								bean="${certificationTypeInstance}" field="namaSertifikasi"
								url="${request.contextPath}/CertificationType/updatefield" type="text"
								title="Enter namaSertifikasi" onsuccess="reloadCertificationTypeTable();" />--}%
							
								<g:fieldValue bean="${certificationTypeInstance}" field="namaSertifikasi"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${certificationTypeInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="certificationType.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${certificationTypeInstance}" field="staDel"
								url="${request.contextPath}/CertificationType/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadCertificationTypeTable();" />--}%
							
								<g:fieldValue bean="${certificationTypeInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${certificationTypeInstance?.tipeSertifikasi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tipeSertifikasi-label" class="property-label"><g:message
					code="certificationType.tipeSertifikasi.label" default="Tipe Sertifikasi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tipeSertifikasi-label">
						%{--<ba:editableValue
								bean="${certificationTypeInstance}" field="tipeSertifikasi"
								url="${request.contextPath}/CertificationType/updatefield" type="text"
								title="Enter tipeSertifikasi" onsuccess="reloadCertificationTypeTable();" />--}%
							
								<g:fieldValue bean="${certificationTypeInstance}" field="tipeSertifikasi"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${certificationTypeInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="certificationType.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${certificationTypeInstance}" field="updatedBy"
								url="${request.contextPath}/CertificationType/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadCertificationTypeTable();" />--}%
							
								<g:fieldValue bean="${certificationTypeInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('certificationType');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${certificationTypeInstance?.id}"
					update="[success:'certificationType-form',failure:'certificationType-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteCertificationType('${certificationTypeInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
