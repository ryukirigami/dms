<%@ page import="com.kombos.hrd.CertificationType" %>

<g:javascript>
    $(function() {
        $("#namaSertifikasi").focus();
    })
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: certificationTypeInstance, field: 'namaSertifikasi', 'error')} ">
	<label class="control-label" for="namaSertifikasi">
		<g:message code="certificationType.namaSertifikasi.label" default="Nama Sertifikasi" />
		
	</label>
	<div class="controls">
	<g:textField name="namaSertifikasi" value="${certificationTypeInstance?.namaSertifikasi}" />
        <span class="help-block">
            <g:fieldError field="namaSertifikasi" bean="${certificationTypeInstance}"/>
        </span>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: certificationTypeInstance, field: 'tipeSertifikasi', 'error')} ">
	<label class="control-label" for="tipeSertifikasi">
		<g:message code="certificationType.tipeSertifikasi.label" default="Tipe Sertifikasi" />
		
	</label>
	<div class="controls">
	<g:textField name="tipeSertifikasi" value="${certificationTypeInstance?.tipeSertifikasi}" />
        <span class="help-block">
            <g:fieldError field="tipeSertifikasi" bean="${certificationTypeInstance}"/>
        </span>
	</div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: certificationTypeInstance, field: 'keterangan', 'error')} ">
    <label class="control-label" for="keterangan">
        <g:message code="certificationType.keterangan.label" default="Keterangan" />

    </label>
    <div class="controls">
        <g:textField name="keterangan" value="${certificationTypeInstance?.keterangan}" />
        <span class="help-block">
            <g:fieldError field="keterangan" bean="${certificationTypeInstance}"/>
        </span>
    </div>
</div>