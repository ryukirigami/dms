<%@ page import="com.kombos.administrasi.Kecamatan; com.kombos.administrasi.KabKota; com.kombos.administrasi.Provinsi; com.kombos.administrasi.Kelurahan" %>
<g:javascript>
    var createProvinsi;
    var createKecamatan;
    var createKabKota;
    var saveProvinsi;
    var saveKabKota;
    var saveKecamatan;
    var cancelProvinsi;
    var cancelKabKota;
    var cancelKecamatan;
    var disableEdit;
    var saveProvinsi;
    var saveKabKota;
    var saveKecamatan;
    var updateDeleteProvinsi;
    var updateDeleteKabKota;
    var updateDeleteKecamatan;
    var selectEditKabupaten;
    var selectEditKecamatan;


    $(function(){
        createProvinsi = function(){
            $("#inputProvinsi").show();
            $("#saveProvinsiBtn").show();
            $("#createProvinsiBtn").hide();
            $("#inputKecamatan").hide();
            $("#cancelProvinsiBtn").show();
            $("#inputKabKota").hide();
            $("#provinsi").hide();
            $("#kabkota").show();
            $("#kabkota").attr("disabled","true");
            $("#kecamatan").show();
            $("#kecamatan").attr("disabled","false");
            $("#m004NamaKelurahan").attr("disabled","false");
            $("#saveKecBtn").hide();
            $("#createKecBtn").show();
            $("#cancelKecBtn").hide();
            $("#createKabKotaBtn").show();
            $("#saveKabKotaBtn").hide();
            $("#cancelKabKotaBtn").hide();
            $("#saveProvinsiBtn").show();

        }

        createKabKota = function(){
            $("#inputKabKota").show();
            $("#kabkota").hide();
            $("#createKabKotaBtn").hide();
            $("#inputProvinsi").hide();
            $("#inputKecamatan").hide();
            $("#cancelKabKotaBtn").show();
            $("#provinsi").show();
            $("#provinsi").attr("disabled","true");
            $("#kecamatan").show();
            $("#kecamatan").attr("disabled","false");
            $("#saveProvinsiBtn").hide();
            $("#createProvinsiBtn").show();
            $("#cancelProvinsiBtn").hide();
            $("#createKecBtn").show();
            $("#saveKecBtn").hide();
            $("#cancelKecBtn").hide();
            $("#m004NamaKelurahan").attr("disabled","false");
            $("#saveKabKotaBtn").show();
        }

        createKecamatan = function(){
            $("#inputKecamatan").show();
            $("#kecamatan").hide();
            $("#inputKabKota").hide();
            $("#inputProvinsi").hide();
            $("#createKecBtn").hide();
            $("#cancelKecBtn").show();
            $("#provinsi").show();
            $("#provinsi").attr("disabled","true");
            $("#kabkota").show();
            $("#kabkota").attr("disabled","false");
            $("#m004NamaKelurahan").attr("disabled","false");
            $("#saveProvinsiBtn").hide();
            $("#createProvinsiBtn").show();
            $("#cancelProvinsiBtn").hide();
            $("#createKabKotaBtn").show();
            $("#saveKabKotaBtn").hide();
            $("#cancelKabKotaBtn").hide();
            $("#saveKecBtn").show();

        }

        cancelProvinsi = function(){
            $("#inputProvinsi").hide();
            disableEdit();
            $("#createProvinsiBtn").show();
            $("#cancelProvinsiBtn").hide();
            $("#saveProvinsiBtn").hide();

        }

        cancelKabKota = function(){
            $("#inputKabKota").hide();
            disableEdit();
            $("#createKabKotaBtn").show();
            $("#cancelKabKotaBtn").hide();
            $("#saveKabKotaBtn").hide();

        }

        cancelKecamatan = function(){
            $("#inputKecamatan").hide();
            disableEdit();
            $("#createKecBtn").show();
            $("#cancelKecBtn").hide();
            $("#saveKecBtn").hide();

        }

        disableEdit = function(){
            $("#provinsi").show();
            $("#kabkota").show();
            $("#kecamatan").show();
            $("#m004NamaKelurahan").show();

            $("#provinsi").prop("disabled", null);
            $("#kabkota").prop("disabled", null);
            $("#kecamatan").prop("disabled", null);
            $("#m004NamaKelurahan").prop("disabled", null);

        }


        saveProvinsi = function(){

            var isiProvinsi = $('#inputProvinsi').val();
            if(isiProvinsi == ""){
                alert("Nama Provinsi Tidak boleh kosong");
                return;
            }



           $.ajax({type:'POST', url:'${request.contextPath}/kelurahan/insertProvinsi',
                data : {namaProvinsi : isiProvinsi},
                success:function(data,textStatus){
                    if(data){

                       if(data.error === "duplicate"){
                            alert("Data Duplicate, Silakan input yang belum ada");
                            $('#inputProvinsi').val("");
                            $("#inputProvinsi").focus();

                       }else{
                          $('#inputProvinsi').val("");
                          $('#provinsi').append("<option value='" + data.id + "'>" + data.namaProvinsi + "</option>");
                          toastr.success("Provinsi Berhasil disimpan");
                           cancelProvinsi();

                       }

                     }

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
   		    });
        };

       saveKabKota = function(){
           var idProvinsi = $('#provinsi option:selected').val();
           var isiKabKota = $('#inputKabKota').val();

           if(isiKabKota == ""){
            alert("Nama Kab/Kota tidak boleh kosong");
            return;
           }


           $.ajax({type:'POST', url:'${request.contextPath}/kelurahan/insertKabKota',
                data : {namaKabKota : $("#inputKabKota").val(), idProv : idProvinsi},
                success:function(data,textStatus){
                    if(data){
                        if(data.error === "duplicate"){
                            alert("data dupliclate, Silakan input data yang belum ada");
                             $("#inputKabKota").val("");
                             $("#inputKabKota").focus();

                        }else{
                                $("#inputKabKota").val("");
                                 $('#kabKota').append("<option value='" + data.id + "'>" + data.namaKabKota + "</option>");
                                  cancelKabKota();
                                  selectKabupaten();
                                toastr.success("Kabupaten/Kota Berhasil disimpan");

                        }
                     }

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
   		    });
        };

        saveKecamatan = function(){
           var idProvinsi = $('#provinsi option:selected').val();
           var idKabKota = $('#kabkota option:selected').val();
           var isiKecamatan = $("#inputKecamatan").val();
           if(isiKecamatan == ""){
                alert("Input Kecamatan tidak boleh kosong");
                return;
           }
           $.ajax({type:'POST', url:'${request.contextPath}/kelurahan/insertKecamatan',
                data : {namaKecamatan : isiKecamatan, idProv : idProvinsi, idKabKota : idKabKota},
                success:function(data,textStatus){
                    if(data){
                        if(data.error == "duplicate"){
                            alert("Data duplicate, Silakan data yang belum ada");
                             $("#inputKecamatan").val("");
                             $("#inputKecamatan").focus();

                        }else{
                             $("#inputKecamatan").val("");
                            $('#kecamatan').append("<option value='" + data.id + "'>" + data.namaKecamatan + "</option>");
                            cancelKecamatan();
                            selectKecamatan();
                             toastr.success("Kecamatan Berhasil disimpan");

                        }

                     }

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
   		    });
        };



        selectKecamatan = function() {
                var idKab = $('#kabkota option:selected').val();
              if(idKab!= undefined){
                  jQuery.getJSON('${request.contextPath}/kelurahan/getKecamatans?idKab='+idKab, function (data) {
                    if (data) {
                        $('#kecamatan').empty();
                        jQuery.each(data, function (index, value) {
                            $('#kecamatan').append("<option value='" + value.id + "'>" + value.namaKecamatan + "</option>");
                        });
                    }
                });

              }else{
                $('#kecamatan').empty();
              }

     };


         selectEditKabupaten = function () {

                     var select_element = $('#kabkota');
                      $('option:not(:selected)', select_element).remove();

                     jQuery.getJSON('${request.contextPath}/kelurahan/getKabupatensEdit?idKelurahan='+'${kelurahanInstance?.id}', function (data) {
                       //     $('#kabkota').empty();
                            if (data) {
                                jQuery.each(data, function (index, value) {
                                    $('#kabkota').append("<option value='" + value.id + "'>" + value.namaKabupaten + "</option>");
                                });
                            }

                            selectEditKecamatan();
                        });


        };


        selectEditKecamatan = function() {
           //     var idKab = $('#kabkota option:selected').val();
                  var select_element = $('#kecamatan');
                   $('option:not(:selected)', select_element).remove();


                  jQuery.getJSON('${request.contextPath}/kelurahan/getKecamatansEdit?idKelurahan='+'${kelurahanInstance?.id}', function (data) {
                    if (data) {
                        jQuery.each(data, function (index, value) {
                            $('#kecamatan').append("<option value='" + value.id + "'>" + value.namaKecamatan + "</option>");
                        });
                    }
                });



     };
        selectKabupaten = function () {
                var idProp = $('#provinsi option:selected').val();
                if(idProp != undefined){
                    jQuery.getJSON('${request.contextPath}/kelurahan/getKabupatens?idProp='+idProp, function (data) {
                            $('#kabkota').empty();
                            if (data) {
                                jQuery.each(data, function (index, value) {
                                    $('#kabkota').append("<option value='" + value.id + "'>" + value.namaKabupaten + "</option>");
                                });
                            }

                            selectKecamatan();
                        });

                }else{
                     $('#kabkota').empty();
                    $('#kecamatan').empty();

                }

        };




    });



</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: kelurahanInstance, field: 'provinsi', 'error')} required">
	<label class="control-label" for="provinsi">
		<g:message code="kelurahan.provinsi.label" default="Nama Provinsi" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="provinsi" name="provinsi.id" from="${Provinsi.createCriteria().list {order("m001NamaProvinsi","asc")}}" optionKey="id" required=""  onchange="selectKabupaten()" value="${kelurahanInstance?.provinsi?.id}" class="many-to-one"/>
    <g:textField name="inputProvinsi" maxlength="20" />
    <g:field type="button" onclick="createProvinsi();" class="btn btn-primary create" name="createProvinsiBtn" id="createProvinsiBtn" value="${message(code: 'default.button.add.label', default: '+')}"/>
    <g:field type="button"  onclick="saveProvinsi();" class="btn btn-primary create" name="saveProvinsiBtn" id="saveProvinsiBtn" value="${message(code: 'default.button.save.label', default: 'Simpan')}"/>
    <g:field type="button"   onclick="cancelProvinsi();" class="btn btn-primary create" name="cancelProvinsiBtn" id="cancelProvinsiBtn" value="${message(code: 'default.button.cancel.label', default: 'Cancel')}"/>
    <g:field type="button"   onclick="updateProvinsi();" class="btn btn-primary create" name="updateProvinsiBtn" id="updateProvinsiBtn" value="${message(code: 'default.button.update.label', default: 'Update')}"/>
    <g:field type="button"   onclick="deleteProvinsi();" class="btn btn-primary create" name="editProvinsiBtn" id="editProvinsiBtn" value="${message(code: 'default.button.editLokasi.label', default: 'u')}"/>
    <g:field type="button"   onclick="deleteProvinsi();" class="btn btn-primary create" name="deleteProvinsiBtn" id="deleteProvinsiBtn" value="${message(code: 'default.button.deleteLokasi.label', default: 'x')}"/>

    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: kelurahanInstance, field: 'kabkota', 'error')} required">
	<label class="control-label" for="kabkota">
		<g:message code="kelurahan.kabkota.label" default="Nama Kabkota" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
    <g:select id="kabkota" name="kabkota.id" from="${KabKota.list()}" optionKey="id" required="" value="${kelurahanInstance?.kabkota?.id}" onchange="selectKecamatan();"  class="many-to-one"/>
    <g:textField name="inputKabKota" maxlength="20" />
    <g:field type="button" onclick="createKabKota();" class="btn btn-primary create" name="createKabKotaBtn" id="createKabKotaBtn" value="${message(code: 'default.button.add.label', default: '+')}"/>
    <g:field type="button"  onclick="saveKabKota();" class="btn btn-primary create" name="saveKabKotaBtn" id="saveKabKotaBtn" value="${message(code: 'default.button.save.label', default: 'Simpan')}"/>
    <g:field type="button"  hidden="true" onclick="cancelKabKota();" class="btn btn-primary create" name="cancelKabKotaBtn" id="cancelKabKotaBtn" value="${message(code: 'default.button.cancel.label', default: 'Cancel')}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: kelurahanInstance, field: 'kecamatan', 'error')} required">
	<label class="control-label" for="kecamatan">
		<g:message code="kelurahan.kecamatan.label" default="Nama Kecamatan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="kecamatan" name="kecamatan.id" from="${Kecamatan.list()}" optionKey="id" required="" value="${kelurahanInstance?.kecamatan?.id}"  class="many-to-one"/>
    <g:textField name="inputKecamatan" maxlength="20"/>
    <g:field type="button" onclick="createKecamatan();" class="btn btn-primary create" name="createKecBtn" id="createKecBtn" value="${message(code: 'default.button.add.label', default: '+')}"/>
    <g:field type="button"  onclick="saveKecamatan();" class="btn btn-primary create" name="saveKecBtn" id="saveKecBtn" value="${message(code: 'default.button.save.label', default: 'Simpan')}"/>
    <g:field type="button"  hidden="true" onclick="cancelKecamatan();" class="btn btn-primary create" name="cancelKecBtn" id="cancelKecBtn" value="${message(code: 'default.button.cancel.label', default: 'Cancel')}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: kelurahanInstance, field: 'm004NamaKelurahan', 'error')} required">
    <label class="control-label" for="m004NamaKelurahan">
        <g:message code="kelurahan.m004NamaKelurahan.label" default="Nama Kelurahan" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="m004NamaKelurahan" id="m004NamaKelurahan" maxlength="40" required="" value="${kelurahanInstance?.m004NamaKelurahan}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: kelurahanInstance, field: 'm004KodePos', 'error')} required">
    <label class="control-label" for="m004KodePos">
        <g:message code="kelurahan.m004NamaKelurahan.label" default="Kode Pos" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="m004KodePos" id="m004KodePos" maxlength="5" required="" value="${kelurahanInstance?.m004KodePos}"/>
    </div>
</div>

<g:if test="${kelurahanInstance?.m004ID}">
    <g:hiddenField name="m004ID" value="${kelurahanInstance?.m004ID}" />
</g:if>
<g:else>
    <g:hiddenField name="m004ID" value="${Kelurahan.last().m004ID.toInteger() + 1}" />
</g:else>
%{--
<div class="control-group fieldcontain ${hasErrors(bean: kelurahanInstance, field: 'm004ID', 'error')} ">
	<label class="control-label" for="m004ID">
		<g:message code="kelurahan.m004ID.label" default="M004 ID" />
		
	</label>
	<div class="controls">
	<g:textField name="m004ID" maxlength="3" value="${kelurahanInstance?.m004ID}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: kelurahanInstance, field: 'staDel', 'error')} required">
	<label class="control-label" for="staDel">
		<g:message code="kelurahan.staDel.label" default="Sta Del" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="staDel" required="" value="${kelurahanInstance?.staDel}"/>
	</div>
</div>
--}%
<g:javascript>
    document.getElementById("m004NamaKelurahan").focus();

    $(function(){
        $("#inputProvinsi").hide();
        $("#inputKabKota").hide();
        $("#inputKecamatan").hide();
        $("#saveProvinsiBtn").hide();
        $("#saveKabKotaBtn").hide();
        $("#saveKecBtn").hide();
        $("#cancelProvinsiBtn").hide();
        $("#cancelKabKotaBtn").hide();
        $("#cancelKecBtn").hide();
        $("#updateProvinsiBtn").hide();
        $("#updateKabKotaBtn").hide();
        $("#updateKecBtn").hide();
        $("#editProvinsiBtn").hide();
        $("#deleteProvinsiBtn").hide();



    });

</g:javascript>
<g:if test="${kelurahanInstance}">
    <g:javascript>
        selectEditKabupaten();
    </g:javascript>
</g:if>
<g:else>
    <g:javascript>
        selectKabupaten();
    </g:javascript>
</g:else>

