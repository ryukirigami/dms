

<%@ page import="com.kombos.administrasi.Kelurahan" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'kelurahan.label', default: 'Lokasi')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteKelurahan;

$(function(){ 
	deleteKelurahan=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/kelurahan/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadKelurahanTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-kelurahan" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="kelurahan"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${kelurahanInstance?.provinsi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="provinsi-label" class="property-label"><g:message
					code="kelurahan.provinsi.label" default="Nama Provinsi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="provinsi-label">
						%{--<ba:editableValue
								bean="${kelurahanInstance}" field="provinsi"
								url="${request.contextPath}/Kelurahan/updatefield" type="text"
								title="Enter provinsi" onsuccess="reloadKelurahanTable();" />--}%
							
								${kelurahanInstance?.provinsi?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kelurahanInstance?.kabkota}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kabkota-label" class="property-label"><g:message
					code="kelurahan.kabkota.label" default="Nama Kabkota" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kabkota-label">
						%{--<ba:editableValue
								bean="${kelurahanInstance}" field="kabkota"
								url="${request.contextPath}/Kelurahan/updatefield" type="text"
								title="Enter kabkota" onsuccess="reloadKelurahanTable();" />--}%
							
								${kelurahanInstance?.kabkota?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kelurahanInstance?.kecamatan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kecamatan-label" class="property-label"><g:message
					code="kelurahan.kecamatan.label" default="Nama Kecamatan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kecamatan-label">
						%{--<ba:editableValue
								bean="${kelurahanInstance}" field="kecamatan"
								url="${request.contextPath}/Kelurahan/updatefield" type="text"
								title="Enter kecamatan" onsuccess="reloadKelurahanTable();" />--}%
							
								${kelurahanInstance?.kecamatan?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kelurahanInstance?.m004NamaKelurahan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m004NamaKelurahan-label" class="property-label"><g:message
					code="kelurahan.m004NamaKelurahan.label" default="Nama Kelurahan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m004NamaKelurahan-label">
						%{--<ba:editableValue
								bean="${kelurahanInstance}" field="m004NamaKelurahan"
								url="${request.contextPath}/Kelurahan/updatefield" type="text"
								title="Enter m004NamaKelurahan" onsuccess="reloadKelurahanTable();" />--}%
							
								<g:fieldValue bean="${kelurahanInstance}" field="m004NamaKelurahan"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${kelurahanInstance?.m004KodePos}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m004KodePos-label" class="property-label"><g:message
                                code="kelurahan.m004NamaKelurahan.label" default="Kode Pos" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m004KodePos-label">
                        %{--<ba:editableValue
                                bean="${kelurahanInstance}" field="m004NamaKelurahan"
                                url="${request.contextPath}/Kelurahan/updatefield" type="text"
                                title="Enter m004NamaKelurahan" onsuccess="reloadKelurahanTable();" />--}%

                        <g:fieldValue bean="${kelurahanInstance}" field="m004KodePos"/>

                    </span></td>

                </tr>
            </g:if>
<!--			
				<g:if test="${kelurahanInstance?.m004ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m004ID-label" class="property-label"><g:message
					code="kelurahan.m004ID.label" default="M004 ID" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m004ID-label">
						%{--<ba:editableValue
								bean="${kelurahanInstance}" field="m004ID"
								url="${request.contextPath}/Kelurahan/updatefield" type="text"
								title="Enter m004ID" onsuccess="reloadKelurahanTable();" />--}%
							
								<g:fieldValue bean="${kelurahanInstance}" field="m004ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kelurahanInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="kelurahan.staDel.label" default="M004 Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${kelurahanInstance}" field="staDel"
								url="${request.contextPath}/Kelurahan/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadKelurahanTable();" />--}%
							
								<g:fieldValue bean="${kelurahanInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
-->
            <g:if test="${kelurahanInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="goods.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${kelurahanInstance}" field="createdBy"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadGoodsTable();" />--}%

                        <g:fieldValue bean="${kelurahanInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
            <g:if test="${kelurahanInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="goods.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${kelurahanInstance}" field="updatedBy"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadGoodsTable();" />--}%

                        <g:fieldValue bean="${kelurahanInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
            <g:if test="${kelurahanInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="goods.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${kelurahanInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadGoodsTable();" />--}%

                        <g:fieldValue bean="${kelurahanInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${kelurahanInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="goods.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${kelurahanInstance}" field="dateCreated"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadGoodsTable();" />--}%

                        <g:formatDate date="${kelurahanInstance?.dateCreated}"  type="datetime" style="MEDIUM"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${kelurahanInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="goods.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${kelurahanInstance}" field="lastUpdated"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadGoodsTable();" />--}%

                        <g:formatDate date="${kelurahanInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>

                    </span></td>

                </tr>
            </g:if>

            </tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${kelurahanInstance?.id}"
					update="[success:'kelurahan-form',failure:'kelurahan-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteKelurahan('${kelurahanInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
