
<%@ page import="com.kombos.administrasi.Kelurahan" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="kelurahan_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover  table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="kelurahan.provinsi.label" default="Nama Provinsi" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="kelurahan.kabkota.label" default="Nama Kabkota" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="kelurahan.kecamatan.label" default="Nama Kecamatan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="kelurahan.m004NamaKelurahan.label" default="Nama Kelurahan" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="kelurahan.m004KodePos.label" default="Kode Pos" /></div>
            </th>

%{--
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="kelurahan.m004ID.label" default="M004 ID" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="kelurahan.staDel.label" default="M004 Sta Del" /></div>
			</th>
--}%
		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_provinsi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_provinsi" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_kabkota" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_kabkota" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_kecamatan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_kecamatan" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m004NamaKelurahan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m004NamaKelurahan" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_m004KodePos" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_m004KodePos" class="search_init" />
                </div>
            </th>
%{--	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m004ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m004ID" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_staDel" class="search_init" />
				</div>
			</th>
--}%

		</tr>
	</thead>
</table>

<g:javascript>
var KelurahanTable;
var reloadKelurahanTable;
$(function(){
	
	reloadKelurahanTable = function() {
		KelurahanTable.fnDraw();
	}

	var recordskelurahanperpage = [];//new Array();
    var ankelurahanSelected;
    var jmlReckelurahanPerPage=0;
    var id;


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	KelurahanTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	KelurahanTable = $('#kelurahan_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rskelurahan = $("#kelurahan_datatables tbody .row-select");
            var jmlkelurahanCek = 0;
            var nRow;
            var idRec;
            rskelurahan.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordskelurahanperpage[idRec]=="1"){
                    jmlkelurahanCek = jmlkelurahanCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordskelurahanperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlReckelurahanPerPage = rskelurahan.length;
            if(jmlkelurahanCek==jmlReckelurahanPerPage && jmlReckelurahanPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "provinsi",
	"mDataProp": "provinsi",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "kabkota",
	"mDataProp": "kabkota",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "kecamatan",
	"mDataProp": "kecamatan",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m004NamaKelurahan",
	"mDataProp": "m004NamaKelurahan",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "m004KodePos",
	"mDataProp": "m004KodePos",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m004ID",
	"mDataProp": "m004ID",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "staDel",
	"mDataProp": "staDel",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var provinsi = $('#filter_provinsi input').val();
						if(provinsi){
							aoData.push(
									{"name": 'sCriteria_provinsi', "value": provinsi}
							);
						}
	
						var kabkota = $('#filter_kabkota input').val();
						if(kabkota){
							aoData.push(
									{"name": 'sCriteria_kabkota', "value": kabkota}
							);
						}
	
						var kecamatan = $('#filter_kecamatan input').val();
						if(kecamatan){
							aoData.push(
									{"name": 'sCriteria_kecamatan', "value": kecamatan}
							);
						}
	
						var m004NamaKelurahan = $('#filter_m004NamaKelurahan input').val();
						if(m004NamaKelurahan){
							aoData.push(
									{"name": 'sCriteria_m004NamaKelurahan', "value": m004NamaKelurahan}
							);
						}

						var m004KodePos = $('#filter_m004KodePos input').val();
						if(m004KodePos){
							aoData.push(
									{"name": 'sCriteria_m004KodePos', "value": m004KodePos}
							);
						}
	
						var m004ID = $('#filter_m004ID input').val();
						if(m004ID){
							aoData.push(
									{"name": 'sCriteria_m004ID', "value": m004ID}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

	$('.select-all').click(function(e) {

        $("#kelurahan_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordskelurahanperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordskelurahanperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#kelurahan_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordskelurahanperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            ankelurahanSelected = KelurahanTable.$('tr.row_selected');
            if(jmlReckelurahanPerPage == ankelurahanSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordskelurahanperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
