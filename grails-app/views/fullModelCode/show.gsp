

<%@ page import="com.kombos.administrasi.FullModelCode" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'fullModelCode.label', default: 'Full Model')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteFullModelCode;

$(function(){ 
	deleteFullModelCode=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/fullModelCode/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadFullModelCodeTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-fullModelCode" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="fullModelCode"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${fullModelCodeInstance?.kategoriKendaraan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kategoriKendaraan-label" class="property-label"><g:message
					code="fullModelCode.kategoriKendaraan.label" default="Kategori" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kategoriKendaraan-label">
						%{--<ba:editableValue
								bean="${fullModelCodeInstance}" field="kategoriKendaraan"
								url="${request.contextPath}/FullModelCode/updatefield" type="text"
								title="Enter kategoriKendaraan" onsuccess="reloadFullModelCodeTable();" />--}%
							
								${fullModelCodeInstance?.kategoriKendaraan?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${fullModelCodeInstance?.baseModel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="baseModel-label" class="property-label"><g:message
					code="fullModelCode.baseModel.label" default="Base Model" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="baseModel-label">
						%{--<ba:editableValue
								bean="${fullModelCodeInstance}" field="baseModel"
								url="${request.contextPath}/FullModelCode/updatefield" type="text"
								title="Enter baseModel" onsuccess="reloadFullModelCodeTable();" />--}%
							
								${fullModelCodeInstance?.baseModel?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${fullModelCodeInstance?.stir}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="stir-label" class="property-label"><g:message
					code="fullModelCode.stir.label" default="Stir" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="stir-label">
						%{--<ba:editableValue
								bean="${fullModelCodeInstance}" field="stir"
								url="${request.contextPath}/FullModelCode/updatefield" type="text"
								title="Enter stir" onsuccess="reloadFullModelCodeTable();" />--}%
							
								${fullModelCodeInstance?.stir?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${fullModelCodeInstance?.modelName}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="modelName-label" class="property-label"><g:message
					code="fullModelCode.modelName.label" default="Model" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="modelName-label">
						%{--<ba:editableValue
								bean="${fullModelCodeInstance}" field="modelName"
								url="${request.contextPath}/FullModelCode/updatefield" type="text"
								title="Enter modelName" onsuccess="reloadFullModelCodeTable();" />--}%
							
								${fullModelCodeInstance?.modelName?.encodeAsHTML()}

						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${fullModelCodeInstance?.bodyType}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="bodyType-label" class="property-label"><g:message
					code="fullModelCode.bodyType.label" default="Body Type" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="bodyType-label">
						%{--<ba:editableValue
								bean="${fullModelCodeInstance}" field="bodyType"
								url="${request.contextPath}/FullModelCode/updatefield" type="text"
								title="Enter bodyType" onsuccess="reloadFullModelCodeTable();" />--}%
							
								${fullModelCodeInstance?.bodyType?.m105KodeBodyType?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${fullModelCodeInstance?.gear}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="gear-label" class="property-label"><g:message
					code="fullModelCode.gear.label" default="Gear" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="gear-label">
						%{--<ba:editableValue
								bean="${fullModelCodeInstance}" field="gear"
								url="${request.contextPath}/FullModelCode/updatefield" type="text"
								title="Enter gear" onsuccess="reloadFullModelCodeTable();" />--}%
							
								${fullModelCodeInstance?.gear?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${fullModelCodeInstance?.grade}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="grade-label" class="property-label"><g:message
					code="fullModelCode.grade.label" default="Grade" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="grade-label">
						%{--<ba:editableValue
								bean="${fullModelCodeInstance}" field="grade"
								url="${request.contextPath}/FullModelCode/updatefield" type="text"
								title="Enter grade" onsuccess="reloadFullModelCodeTable();" />--}%
							
								${fullModelCodeInstance?.grade?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${fullModelCodeInstance?.engine}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="engine-label" class="property-label"><g:message
					code="fullModelCode.engine.label" default="Engine" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="engine-label">
						%{--<ba:editableValue
								bean="${fullModelCodeInstance}" field="engine"
								url="${request.contextPath}/FullModelCode/updatefield" type="text"
								title="Enter engine" onsuccess="reloadFullModelCodeTable();" />--}%
							
								${fullModelCodeInstance?.engine?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${fullModelCodeInstance?.country}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="country-label" class="property-label"><g:message
					code="fullModelCode.country.label" default="Country" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="country-label">
						%{--<ba:editableValue
								bean="${fullModelCodeInstance}" field="country"
								url="${request.contextPath}/FullModelCode/updatefield" type="text"
								title="Enter country" onsuccess="reloadFullModelCodeTable();" />--}%
							
								${fullModelCodeInstance?.country?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${fullModelCodeInstance?.formOfVehicle}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="formOfVehicle-label" class="property-label"><g:message
					code="fullModelCode.formOfVehicle.label" default="Form Of Vehicle" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="formOfVehicle-label">
						%{--<ba:editableValue
								bean="${fullModelCodeInstance}" field="formOfVehicle"
								url="${request.contextPath}/FullModelCode/updatefield" type="text"
								title="Enter formOfVehicle" onsuccess="reloadFullModelCodeTable();" />--}%
							
								${fullModelCodeInstance?.formOfVehicle?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${fullModelCodeInstance?.t110FullModelCode}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t110FullModelCode-label" class="property-label"><g:message
                                code="fullModelCode.t110FullModelCode.label" default="Full Model" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t110FullModelCode-label">
                        %{--<ba:editableValue
                                bean="${fullModelCodeInstance}" field="t110FullModelCode"
                                url="${request.contextPath}/FullModelCode/updatefield" type="text"
                                title="Enter t110FullModelCode" onsuccess="reloadFullModelCodeTable();" />--}%

                        <g:fieldValue bean="${fullModelCodeInstance}" field="t110FullModelCode"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${fullModelCodeInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="fullModelCode.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${fullModelCodeInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/fullModelCode/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadfullModelCodeTable();" />--}%

                        <g:fieldValue bean="${fullModelCodeInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${fullModelCodeInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="fullModelCode.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${fullModelCodeInstance}" field="dateCreated"
                                url="${request.contextPath}/fullModelCode/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadfullModelCodeTable();" />--}%

                        <g:formatDate date="${fullModelCodeInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${fullModelCodeInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="fullModelCode.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${fullModelCodeInstance}" field="createdBy"
                                url="${request.contextPath}/fullModelCode/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadfullModelCodeTable();" />--}%

                        <g:fieldValue bean="${fullModelCodeInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${fullModelCodeInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="fullModelCode.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${fullModelCodeInstance}" field="lastUpdated"
                                url="${request.contextPath}/fullModelCode/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadfullModelCodeTable();" />--}%

                        <g:formatDate date="${fullModelCodeInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${fullModelCodeInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="fullModelCode.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${fullModelCodeInstance}" field="updatedBy"
                                url="${request.contextPath}/fullModelCode/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadfullModelCodeTable();" />--}%

                        <g:fieldValue bean="${fullModelCodeInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>


<!--			
				<g:if test="${fullModelCodeInstance?.staImport}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staImport-label" class="property-label"><g:message
					code="fullModelCode.staImport.label" default="Sta Import" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staImport-label">
						%{--<ba:editableValue
								bean="${fullModelCodeInstance}" field="staImport"
								url="${request.contextPath}/FullModelCode/updatefield" type="text"
								title="Enter staImport" onsuccess="reloadFullModelCodeTable();" />--}%
							
								<g:link controller="staImport" action="show" id="${fullModelCodeInstance?.staImport?.id}">${fullModelCodeInstance?.staImport?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			

			
				<g:if test="${fullModelCodeInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="fullModelCode.staDel.label" default="T110 Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${fullModelCodeInstance}" field="staDel"
								url="${request.contextPath}/FullModelCode/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadFullModelCodeTable();" />--}%
							
								<g:fieldValue bean="${fullModelCodeInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
-->			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${fullModelCodeInstance?.id}"
					update="[success:'fullModelCode-form',failure:'fullModelCode-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteFullModelCode('${fullModelCodeInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
