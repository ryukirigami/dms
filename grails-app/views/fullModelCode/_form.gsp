<%@ page import="com.kombos.administrasi.FormOfVehicle; com.kombos.administrasi.Country; com.kombos.administrasi.Engine; com.kombos.administrasi.Grade; com.kombos.administrasi.Gear; com.kombos.administrasi.BodyType; com.kombos.administrasi.ModelName; com.kombos.administrasi.Stir; com.kombos.administrasi.BaseModel; com.kombos.administrasi.KategoriKendaraan; com.kombos.administrasi.FullModelCode" %>

<g:javascript>
    var selectBaseModel;
    var selectEngine;
    var selectModelName;
    var selectBodyType;
    var selectGear;
    var selectGrade;

			$(function(){

			selectBaseModel = function () {
			var idKategoriKendaraan = $('#kategoriKendaraan option:selected').val();
			    if(idKategoriKendaraan != undefined){
                    jQuery.getJSON('${request.contextPath}/bodyType/getBaseModels?idKategoriKendaraan='+idKategoriKendaraan, function (data) {
                            $('#baseModel').empty();
                            if (data) {
                                jQuery.each(data, function (index, value) {
                                    $('#baseModel').append("<option value='" + value.id + "'>" + value.kodeBaseModel + "</option>");
                                });
                            }
                            selectModelName();
                        });
                }else{
                    $('#baseModel').empty();
                    $('#modelName').empty();
                    $('#bodyType').empty();
                    $('#gear').empty();
                    $('#grade').empty();
                     $('#engine').empty();
                }
            };

			selectModelName = function () {
			var idBaseModel = $('#baseModel option:selected').val();
			    if(idBaseModel != undefined){
                    jQuery.getJSON('${request.contextPath}/bodyType/getModelNames?idBaseModel='+idBaseModel, function (data) {
                            $('#modelName').empty();
                            if (data) {
                                jQuery.each(data, function (index, value) {
                                    $('#modelName').append("<option value='" + value.id + "'>" + value.kodeModelName + "</option>");
                                });
                            }
                            selectBodyType();
                        });
                }else{
                     $('#modelName').empty();
                     $('#bodyType').empty();
                     $('#gear').empty();
                     $('#grade').empty();
                     $('#engine').empty();
                }
            };

			selectBodyType = function () {
			var idModelName = $('#modelName').val();
			    if(idModelName != undefined){
                    jQuery.getJSON('${request.contextPath}/bodyType/getBodyTypes?idModelName='+idModelName, function (data) {
                            $('#bodyType').empty();
                            if (data) {
                                jQuery.each(data, function (index, value) {
                                    $('#bodyType').append("<option value='" + value.id + "'>" + value.kodeBodyType + "</option>");
                                });
                            }
                            selectGear();
                        });
                }else{
                     $('#bodyType').empty();
                     $('#gear').empty();
                     $('#grade').empty();
                     $('#engine').empty();
                }
            };

            selectGear = function() {
            var idBodyType = $('#bodyType').val();
                if (idBodyType != undefined){
                    jQuery.getJSON('${request.contextPath}/bodyType/getGears?idBodyType='+idBodyType, function (data) {
                            $('#gear').empty();
                            if (data) {
                                jQuery.each(data, function (index, value) {
                                    $('#gear').append("<option value='" + value.id + "'>" + value.kodeGear + "</option>");
                                });
                            }
                            selectGrade();
                        });
                }else{
                     $('#gear').empty();
                     $('#grade').empty();
                     $('#engine').empty();
                }
            };

            selectGrade = function(){
            var idGear = $('#gear').val();
                if (idGear != undefined){
                    jQuery.getJSON('${request.contextPath}/bodyType/getGrades?idGear='+idGear, function (data) {
                            $('#grade').empty();
                            if (data) {
                                jQuery.each(data, function (index, value) {
                                    $('#grade').append("<option value='" + value.id + "'>" + value.kodeGrade + "</option>");
                                });
                            }
                            selectEngine();
                        });
                }else{
                     $('#grade').empty();
                     $('#engine').empty();
                }
            };

            selectEngine = function(){
            var idGrade = $('#grade').val();
                if (idGrade != undefined){
                    jQuery.getJSON('${request.contextPath}/bodyType/getEngines?idGrade='+idGrade, function (data) {
                            $('#engine').empty();
                            if (data) {
                                jQuery.each(data, function (index, value) {
                                    $('#engine').append("<option value='" + value.id + "'>" + value.kodeEngine + "</option>");
                                });
                            }
                        });
                }else{
                     $('#engine').empty();
                }
            };
    });
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: fullModelCodeInstance, field: 'kategoriKendaraan', 'error')} required">
	<label class="control-label" for="kategoriKendaraan">
		<g:message code="fullModelCode.kategoriKendaraan.label" default="Kategori Kendaraan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="kategoriKendaraan" noSelection="['':'Pilih Kategori']" onchange="selectBaseModel();" name="kategoriKendaraan.id" from="${KategoriKendaraan.createCriteria().list(){eq("staDel","0");order("m101KodeKategori","asc")}}" optionKey="id" optionValue="m101KodeKategori" required="" value="${fullModelCodeInstance?.kategoriKendaraan?.id}" class="many-to-one"/>
	</div>
</div>
<g:javascript>
    document.getElementById("kategoriKendaraan").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: fullModelCodeInstance, field: 'baseModel', 'error')} required">
	<label class="control-label" for="baseModel">
		<g:message code="fullModelCode.baseModel.label" default="Base Model" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="baseModel" noSelection="['':'Pilih Base Model']" onchange="selectModelName();" name="baseModel.id" from="${BaseModel.createCriteria().list(){eq("staDel","0");order("m102KodeBaseModel","asc")}}" optionKey="id" optionValue="m102KodeBaseModel" required="" value="${fullModelCodeInstance?.baseModel?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: fullModelCodeInstance, field: 'stir', 'error')} required">
	<label class="control-label" for="stir">
		<g:message code="fullModelCode.stir.label" default="Stir" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="stir" noSelection="['':'Pilih Stir']" name="stir.id" from="${Stir.createCriteria().list(){eq("staDel","0");order("m103KodeStir","asc")}}" optionKey="id" required="" value="${fullModelCodeInstance?.stir?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: fullModelCodeInstance, field: 'modelName', 'error')} required">
	<label class="control-label" for="modelName">
		<g:message code="fullModelCode.modelName.label" default="Model Name" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="modelName" noSelection="['':'Pilih Model Name']" onchange="selectBodyType();" name="modelName.id" from="${ModelName.createCriteria().list(){eq("staDel","0");order("m104KodeModelName","asc")}}" optionKey="id" required="" value="${fullModelCodeInstance?.modelName?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: fullModelCodeInstance, field: 'bodyType', 'error')} required">
	<label class="control-label" for="bodyType">
		<g:message code="fullModelCode.bodyType.label" default="Body Type" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="bodyType" noSelection="['':'Pilih Body Type']" onchange="selectGear();" name="bodyType.id" from="${BodyType.createCriteria().list(){eq("staDel","0");order("m105KodeBodyType","asc")}}" optionValue="m105KodeBodyType" optionKey="id" required="" value="${fullModelCodeInstance?.bodyType?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: fullModelCodeInstance, field: 'gear', 'error')} required">
	<label class="control-label" for="gear">
		<g:message code="fullModelCode.gear.label" default="Gear" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="gear" noSelection="['':'Pilih Gear']" onchange="selectGrade();" name="gear.id" from="${Gear.createCriteria().list(){eq("staDel","0");order("m106KodeGear","asc")}}" optionKey="id" required="" value="${fullModelCodeInstance?.gear?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: fullModelCodeInstance, field: 'grade', 'error')} required">
	<label class="control-label" for="grade">
		<g:message code="fullModelCode.grade.label" default="Grade" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="grade" noSelection="['':'Pilih Grade']" onchange="selectEngine();" name="grade.id" from="${Grade.createCriteria().list(){eq("staDel","0");order("m107KodeGrade","asc")}}" optionKey="id" required="" value="${fullModelCodeInstance?.grade?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: fullModelCodeInstance, field: 'engine', 'error')} required">
	<label class="control-label" for="engine">
		<g:message code="fullModelCode.engine.label" default="Engine" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="engine" noSelection="['':'Pilih Engine']" name="engine.id" from="${Engine.createCriteria().list(){eq("staDel","0");order("m108KodeEngine","asc")}}" optionKey="id" required="" value="${fullModelCodeInstance?.engine?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: fullModelCodeInstance, field: 'country', 'error')} required">
	<label class="control-label" for="country">
		<g:message code="fullModelCode.country.label" default="Country" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="country" noSelection="['':'Pilih Country']" name="country.id" from="${Country.createCriteria().list(){eq("staDel","0");order("m109KodeNegara","asc")}}" optionKey="id" required="" value="${fullModelCodeInstance?.country?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: fullModelCodeInstance, field: 'formOfVehicle', 'error')} required">
	<label class="control-label" for="formOfVehicle">
		<g:message code="fullModelCode.formOfVehicle.label" default="Form Of Vehicle" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="formOfVehicle" noSelection="['':'Pilih Form']" name="formOfVehicle.id" from="${FormOfVehicle.createCriteria().list(){eq("staDel","0");order("m114KodeFoV","asc")}}" optionKey="id" required="" value="${fullModelCodeInstance?.formOfVehicle?.id}" class="many-to-one"/>
	</div>
</div>


%{--
<div class="control-group fieldcontain ${hasErrors(bean: fullModelCodeInstance, field: 'staImport', 'error')} required">
	<label class="control-label" for="staImport">
		<g:message code="fullModelCode.staImport.label" default="Sta Import" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="staImport" name="staImport.id" from="${com.kombos.customerprofile.StaImport.list()}" optionKey="id" required="" value="${fullModelCodeInstance?.staImport?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: fullModelCodeInstance, field: 't110FullModelCode', 'error')} required">
	<label class="control-label" for="t110FullModelCode">
		<g:message code="fullModelCode.t110FullModelCode.label" default="Full Model" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t110FullModelCode" required="" value="${fullModelCodeInstance?.t110FullModelCode}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: fullModelCodeInstance, field: 'staDel', 'error')} required">
	<label class="control-label" for="staDel">
		<g:message code="fullModelCode.staDel.label" default="T110 Sta Del" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="staDel" required="" value="${fullModelCodeInstance?.staDel}"/>
	</div>
</div>
--}%
