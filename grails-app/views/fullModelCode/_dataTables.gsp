
<%@ page import="com.kombos.administrasi.FullModelCode" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="fullModelCode_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="fullModelCode.kategoriKendaraan.label" default="Kategori" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="fullModelCode.baseModel.label" default="Base Model" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="fullModelCode.stir.label" default="Stir" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="fullModelCode.modelName.label" default="Model" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="fullModelCode.bodyType.label" default="Body Type" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="fullModelCode.gear.label" default="Gear" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="fullModelCode.grade.label" default="Grade" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="fullModelCode.engine.label" default="Engine" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="fullModelCode.country.label" default="Country" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="fullModelCode.formOfVehicle.label" default="Form Of Vehicle" /></div>
			</th>
			
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="fullModelCode.t110FullModelCode.label" default="Full Model" /></div>
			</th>

%{--
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="fullModelCode.staImport.label" default="Sta Import" /></div>
			</th>


			


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="fullModelCode.staDel.label" default="T110 Sta Del" /></div>
			</th>
--}%
		
		</tr>
		<tr>


			<th style="border-top: none;padding: 5px;">
				<div id="filter_kategoriKendaraan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" id="search_kategoriKendaraan" name="search_kategoriKendaraan" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_baseModel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_baseModel" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_stir" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_stir" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_modelName" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_modelName" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_bodyType" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_bodyType" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_gear" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_gear" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_grade" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_grade" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_engine" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_engine" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_country" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_country" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_formOfVehicle" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_formOfVehicle" class="search_init" />
				</div>
			</th>
			
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t110FullModelCode" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t110FullModelCode" class="search_init" />
				</div>
			</th>
%{--	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_staImport" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_staImport" class="search_init" />
				</div>
			</th>
	
			
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_staDel" class="search_init" />
				</div>
			</th>
--}%

		</tr>
	</thead>
</table>

<g:javascript>
var fullModelCodeTable;
var reloadFullModelCodeTable;
$(function(){
	reloadFullModelCodeTable = function() {
		fullModelCodeTable.fnDraw();
	}

	var recordsFullModelCodeperpage = [];
    var anFullModelCodeSelected;
    var jmlRecFullModelCodePerPage=0;
    var id;


    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	fullModelCodeTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	fullModelCodeTable = $('#fullModelCode_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            //alert( $('li.active').text() );
            var rsFullModelCode = $("#fullModelCode_datatables tbody .row-select");
            var jmlFullModelCodeCek = 0;
            var idRec;
            var nRow;
            rsFullModelCode.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsFullModelCodeperpage[idRec]=="1"){
                    jmlFullModelCodeCek = jmlFullModelCodeCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsFullModelCodeperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecFullModelCodePerPage = rsFullModelCode.length;
            if(jmlFullModelCodeCek==jmlRecFullModelCodePerPage && jmlRecFullModelCodePerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "kategoriKendaraan",
	"mDataProp": "kategoriKendaraan",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "baseModel",
	"mDataProp": "baseModel",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "stir",
	"mDataProp": "stir",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "modelName",
	"mDataProp": "modelName",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "bodyType",
	"mDataProp": "bodyType",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "gear",
	"mDataProp": "gear",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "grade",
	"mDataProp": "grade",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "engine",
	"mDataProp": "engine",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "country",
	"mDataProp": "country",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "formOfVehicle",
	"mDataProp": "formOfVehicle",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t110FullModelCode",
	"mDataProp": "t110FullModelCode",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "staDel",
	"mDataProp": "staDel",
	"aTargets": [11],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var kategoriKendaraan = $('#search_kategoriKendaraan').val();
						if(kategoriKendaraan){
							aoData.push(
									{"name": 'sCriteria_kategoriKendaraan', "value": kategoriKendaraan}
							);
						}
	
						var baseModel = $('#filter_baseModel input').val();
						if(baseModel){
							aoData.push(
									{"name": 'sCriteria_baseModel', "value": baseModel}
							);
						}
	
						var stir = $('#filter_stir input').val();
						if(stir){
							aoData.push(
									{"name": 'sCriteria_stir', "value": stir}
							);
						}
	
						var modelName = $('#filter_modelName input').val();
						if(modelName){
							aoData.push(
									{"name": 'sCriteria_modelName', "value": modelName}
							);
						}
	
						var bodyType = $('#filter_bodyType input').val();
						if(bodyType){
							aoData.push(
									{"name": 'sCriteria_bodyType', "value": bodyType}
							);
						}
	
						var gear = $('#filter_gear input').val();
						if(gear){
							aoData.push(
									{"name": 'sCriteria_gear', "value": gear}
							);
						}
	
						var grade = $('#filter_grade input').val();
						if(grade){
							aoData.push(
									{"name": 'sCriteria_grade', "value": grade}
							);
						}
	
						var engine = $('#filter_engine input').val();
						if(engine){
							aoData.push(
									{"name": 'sCriteria_engine', "value": engine}
							);
						}
	
						var country = $('#filter_country input').val();
						if(country){
							aoData.push(
									{"name": 'sCriteria_country', "value": country}
							);
						}
	
						var formOfVehicle = $('#filter_formOfVehicle input').val();
						if(formOfVehicle){
							aoData.push(
									{"name": 'sCriteria_formOfVehicle', "value": formOfVehicle}
							);
						}
	
						var staImport = $('#filter_staImport input').val();
						if(staImport){
							aoData.push(
									{"name": 'sCriteria_staImport', "value": staImport}
							);
						}
	
						var t110FullModelCode = $('#filter_t110FullModelCode input').val();
						if(t110FullModelCode){
							aoData.push(
									{"name": 'sCriteria_t110FullModelCode', "value": t110FullModelCode}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

    $('.select-all').click(function(e) {
        $("#fullModelCode_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsFullModelCodeperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsFullModelCodeperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

	$('#fullModelCode_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsFullModelCodeperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anFullModelCodeSelected = fullModelCodeTable.$('tr.row_selected');
            if(jmlRecFullModelCodePerPage == anFullModelCodeSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsFullModelCodeperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
