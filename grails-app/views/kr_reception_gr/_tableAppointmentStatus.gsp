<table cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="100%">
    <tr>
    	<td>
    		<input type="checkbox" id="selectAllAppointmentStatus" onclick="selectAll(this);"/>
    	</td>
    </tr>
    <tr>
    	<td>
    		<input type="checkbox" name="appointmentStatus" value="BOOKING"/>&nbsp;BOOKING
    	</td>
    </tr>
    <tr>
    	<td>
    		<input type="checkbox" name="appointmentStatus" value="NON BOOKING"/>&nbsp;NON BOOKING
    	</td>
    </tr>
</table>
<g:javascript>
function selectAll(e){
	if(e.checked){
		$("input[name='appointmentStatus']").each(function (){
			$(this).attr("checked", "checked");
		});
	} else {
		$("input[name='appointmentStatus']").each(function (){
			$(this).removeAttr("checked");
		});
	}	
}
</g:javascript>