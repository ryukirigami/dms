<%@ page import="com.kombos.administrasi.CompanyDealer"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="baseapplayout" />
<g:javascript disappointmentGrition="head">
	$(function(){	
		$('#kanan').show();	
		$('#periodePart').show();
        $('#filter_periode2').hide();
        $('#filter_periode3').hide();
        $('#filter_periode').show();
        $('#arrivalTime').hide();
        $('#delayTime').hide();
        $('#detail').attr('disabled',false);
        $('#bulan1').change(function(){
            $('#bulan2').val($('#bulan1').val());
        });
        $('#bulan2').change(function(){
            if(parseInt($('#bulan1').val()) > parseInt($('#bulan2').val())){
                $('#bulan2').val($('#bulan1').val());
            }
        });
        $('#tahun2').change(function(){
             $('#tahun').val($('#tahun2').val());
        });
        $('#tahun').change(function(){
             $('#tahun2').val($('#tahun').val());
        });
        $('#namaReport').change(function(){        
        	if($('#namaReport').val() == "01"){
        		$('#kanan').show();
        		$('#periodePart').show();
        		$('#filter_periode').show();
        		$('#filter_periode2').hide();
        		$('#filter_periode3').hide();	
        		$('#arrivalTime').hide();
        		$('#delayTime').hide();
        		$('#detail').attr('disabled',false);
        	} else
        	if($('#namaReport').val() == "02"){
        		$('#kanan').show();
        		$('#periodePart').show();
        		$('#filter_periode').hide();
        		$('#filter_periode2').show();
        		$('#filter_periode3').hide();	
        		$('#arrivalTime').hide();
        		$('#delayTime').hide();
        		$('#detail').attr('disabled',false);
        	} else
        	if($('#namaReport').val() == "03"){
        		$('#kanan').show();
        		$('#periodePart').show();
        		$('#filter_periode').hide();
        		$('#filter_periode2').hide();
        		$('#filter_periode3').show();	
        		$('#arrivalTime').hide();
        		$('#delayTime').hide();
        		$('#detail').attr('disabled',false);
        	} else
        	if($('#namaReport').val() == "04"){
        		$('#kanan').hide();
        		$('#periodePart').show();   
				$('#filter_periode').show();
        		$('#filter_periode2').hide();
        		$('#filter_periode3').hide();	
        		$('#arrivalTime').show();
        		$('#delayTime').show();
        		$('#reportDetail').show();
        	} else
        	if($('#namaReport').val() == "05"){
        		$('#kanan').hide();
        		$('#periodePart').show();
        		$('#filter_periode').show();
        		$('#filter_periode2').hide();
        		$('#filter_periode3').hide();	
        		$('#arrivalTime').hide();
        		$('#delayTime').hide();
        		$('#reportDetail').show();
        	} else
        	if($('#namaReport').val() == "06"){
        		$('#kanan').hide();
        		$('#periodePart').show();
        		$('#filter_periode').hide();
        		$('#filter_periode2').show();
        		$('#filter_periode3').hide();	
        		$('#arrivalTime').hide();
        		$('#delayTime').hide();
        		$('#reportDetail').show();
        	}

        });


	    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    });
    var checkin = $('#search_tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
                checkin.hide();
                $('#search_tanggal2')[0].focus();
            }).data('datepicker');

    var checkout = $('#search_tanggal2').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
                checkout.hide();
    }).data('datepicker');

    previewData = function(){
        var namaReport = $('#namaReport').val();
        var workshop = $('#workshop').val();
        var search_tanggal = $('#search_tanggal').val();
        var search_tanggal2 = $('#search_tanggal2').val();
        var bulan1 = $('#bulan1').val();
        var bulan2= $('#bulan2').val();
        var tahun = $('#tahun').val();
        var format = $('input[name="formatReport"]:checked').val();
        var ceklis = "";
		var sa = "";
		var jenisPekerjaan = "";
		var appointmentStatus = "";
        if(document.getElementById("detail").checked){ceklis = "1"}else{ceklis = "0"}
        if(namaReport == "" || namaReport == null){
            alert('Harap Isi Data Dengan Lengkap');
            return;
        }				
		
		$("input[name='sa']").each(function (){
			if ($(this).attr("checked")) {
				sa += $(this).val() + ',';
			}			
		});
		sa = sa.substr(0, sa.length - 1);		
		
		$("input[name='jenisPekerjaan']").each(function (){
			if ($(this).attr("checked")) {
				jenisPekerjaan += $(this).val() + ',';
			}			
		});
		jenisPekerjaan = jenisPekerjaan.substr(0, jenisPekerjaan.length - 1);		
		
		$("input[name='appointmentStatus']").each(function (){
			if ($(this).attr("checked")) {
				appointmentStatus += $(this).val() + ',';
			}			
		});
		appointmentStatus = appointmentStatus.substr(0, appointmentStatus.length - 1);		
		
		var arrivalStart = $("input[name='arrivalTimeStartHour']").val() + ':' + $("input[name='arrivalTimeStartMinute']").val();
		var arrivalTo = $("input[name='arrivalTimeToHour']").val() + ':' + $("input[name='arrivalTimeToMinute']").val();
		
		var delayStart = $("input[name='delayTimeStartMinute']").val() + ':' + $("input[name='delayTimeStartSecond']").val();
		var delayTo = $("input[name='delayTimeToMinute']").val() + ':' + $("input[name='delayTimeToSecond']").val();
		
		window.location = "${request.contextPath}/kr_reception_gr/previewData?namaReport="+namaReport+"&workshop="+workshop+"&bulan1="+bulan1+"&bulan2="+bulan2+"&tahun="+tahun+
				"&tanggal="+search_tanggal+"&tanggal2="+search_tanggal2+"&detail="+ceklis+"&sa="+sa+"&jenisPekerjaan="+jenisPekerjaan+
				"&appointmentStatus="+appointmentStatus+"&arrivalStart="+arrivalStart+"&arrivalTo="+arrivalTo+
				"&delayStart="+delayStart+"&delayTo="+delayTo+"&format="+format;
    }


</g:javascript>

</head>
<body>
	<div class="navbar box-header no-border">Report – Reception (GR)
	</div>
	<br>
	<div class="box">
		<div class="span12" id="appointmentGr-table">
			<div class="row-fluid form-horizontal">
				<div id="kiri" class="span6">
                    <div class="control-group">
                        <label class="control-label" for="filter_periode" style="text-align: left;">
                            Format Report
                        </label>
                        <div id="filter_format" class="controls">
                            <g:radioGroup name="formatReport" id="formatReport" values="['x','p']" labels="['Excel','PDF']" value="x" required="" >
                                ${it.radio} ${it.label}
                            </g:radioGroup>
                        </div>
                    </div>
					<div class="control-group">
						<label class="control-label" for="t951Tanggal"
							style="text-align: left;"> Workshop </label>
						<div id="filter_wo" class="controls">
                            <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                <g:select name="workshop" id="workshop" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                            </g:if>
                            <g:else>
                                <g:select name="workshop" id="workshop" readonly="" disabled="" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                            </g:else>
							%{--<g:select name="workshop" id="workshop"--}%
								%{--from="${CompanyDealer.list()}" optionKey="id"--}%
								%{--optionValue="m011NamaWorkshop" style="width: 44%" />--}%
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="t951Tanggal"
							style="text-align: left;"> Kategori Workshop </label>
						<div id="filter_workshop" class="controls">
							&nbsp; <b>General Repair</b>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="t951Tanggal"
							style="text-align: left;"> Nama Report </label>
						<div id="filter_nama" class="controls">
							<select name="namaReport" id="namaReport" size="8"
								style="width: 100%; height: 230px; font-size: 12px;">
								<option value="01">01. Reception GR - Reception Lead Time (Daily)</option>				
								<option value="02">02. Reception GR - Reception Lead Time (Monthly)</option>
								<option value="03">03. Reception GR - Reception Lead Time (Yearly)</option>
								<option value="04">04. Reception GR - Customer In</option>
								<option value="05">05. Reception GR - Potential Lost Sales (Daily)</option>
								<option value="06">06. Reception GR - Potential Lost Sales (Monthly)</option>			
							</select>
						</div>
					</div>
					<div class="control-group" id="periodePart">
						<label class="control-label" for="t951Tanggal"
							style="text-align: left;"> Periode </label>
						<div id="filter_periode" class="controls">
							<ba:datePicker id="search_tanggal" name="search_tanggal"
								precision="day" format="dd-MM-yyyy" value="${new Date()}" />
							&nbsp;&nbsp;s.d.&nbsp;&nbsp;
							<ba:datePicker id="search_tanggal2" name="search_tanggal2"
								precision="day" format="dd-MM-yyyy" value="${new Date()+1}" />
						</div>
						<div id="filter_periode2" class="controls">
							%{ def listBulan =
							['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
							}% <select name="bulan1" id="bulan1" style="width: 170px">
								%{ for(int i=0;i<12;i++){ out.print('
								<option value="'+(i+1)+'">'+listBulan.get(i)+'</option>'); } }%
							</select> &nbsp; - &nbsp; <select name="bulan2" id="bulan2"
								style="width: 170px"> %{ for(int i=0;i<12;i++){
								out.print('
								<option value="'+(i+1)+'">'+listBulan.get(i)+'</option>'); } }%
							</select> &nbsp; <select name="tahun" id="tahun" style="width: 106px">
								%{ for(def a = (new Date().format("yyyy")).toInteger();a >= 2000
								;a--){ out.print('
								<option value="'+a+'">'+a+'</option>'); } }%

							</select>
						</div>
						<div id="filter_periode3" class="controls">
							<select name="tahun" id="tahun2" style="width: 170px"> %{
								for(def a = (new Date().format("yyyy")).toInteger(); a >= 2000;
								a--) { out.print('
								<option value="'+a+'">'+a+'</option>'); } }%
							</select>
						</div>
						<div class="control-group" id="reportDetail">
							<label class="control-label"
								style="text-align: left;"> Report Detail </label>
							<div id="filter_detail" class="controls">
								<input type="checkbox" name="detail" id="detail" value="1" /> Ya
							</div>
						</div>						
					</div>	
					<div class="control-group" id="arrivalTime">
						<label class="control-label" style="text-align: left;">Arrival Time</label>
						<input type="text" name="arrivalTimeStartHour" style="width:20px" value="00" maxlength="2"/>:<input type="text" name="arrivalTimeStartMinute" style="width:20px" value="00" maxlength="2"/>&nbsp;To
						<input type="text" name="arrivalTimeToHour" style="width:20px" value="00" maxlength="2"/>:<input type="text" name="arrivalTimeToMinute" style="width:20px" value="00" maxlength="2"/>&nbsp;						
					</div>				
					<div class="control-group" id="delayTime">
						<label class="control-label" style="text-align: left;">Delay Time</label>
						<input type="text" name="delayTimeStartMinute" style="width:20px" value="00" maxlength="2"/>:<input type="text" name="delayTimeStartSecond" style="width:20px" value="00" maxlength="2"/>&nbsp;To
						<input type="text" name="delayTimeToMinute" style="width:20px" value="00" maxlength="2"/>:<input type="text" name="delayTimeToSecond" style="width:20px" value="00" maxlength="2"/>
					</div>				
				</div>				
				<div id="kanan" class="span6">
					<div class="control-group">
						<label class="control-label"
							style="text-align: left;"> Appointment Status </label>
						<div class="controls">
							<g:render template="tableAppointmentStatus" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="workshop"
							style="text-align: left;"> Service Advisor </label>
						<div class="controls">
							<g:render template="tableSA" />
						</div>
					</div>
					<div class="control-group">	
						<label class="control-label" style="text-align:left;">
						Jenis Pekerjaan
						</label>
						<div class="controls">
							<g:render template="tableJenisPekerjaan"></g:render>
						</div>
					</div>
				</div>				
			</div>
			
			<g:field type="button" onclick="previewData()"
				class="btn btn-primary create" name="preview"
				value="${message(code: 'default.button.upload.label', default: 'Preview')}" />
			<g:field type="button" onclick="window.location.replace('#/home')"
				class="btn btn-cancel cancel" name="cancel"
				value="${message(code: 'default.button.upload.label', default: 'Close')}" />
		</div>
	</div>
</body>
</html>

