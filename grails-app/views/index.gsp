<%@ page import="com.kombos.baseapp.AppSettingParamService; com.kombos.baseapp.AppSettingParam; com.kombos.administrasi.SearchArea; com.kombos.administrasi.SubArea; com.kombos.administrasi.Kategori" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="mainIndex" />
<r:require modules="baseapp,autoNumeric,jqueryvalidate,jquerynumber,path,jqueryhashchange" />
%{--<style>--}%
%{--.scrollable-menu {--}%
    %{--height: auto;--}%
    %{--max-height: 400px;--}%
    %{--overflow-x: hidden;--}%
%{--}--}%
%{--</style>--}%
<g:javascript>
	var showGlobalSearch = false;
	toastr.options = {
			"positionClass": 'toast-bottom-left',
			"onclick": null,
  			"fadeIn": 300,
  			"fadeOut": 1000,
  			"timeOut": 5000,
  			"extendedTimeOut": 1000
		}; 
	function clearFilter(){
		 $('.search_init').val('');
		var oTable = $.fn.dataTable.fnTables(true);
        if ( oTable.length > 0 ) {
          $(oTable).dataTable().fnFilterClear();
          $(oTable).dataTable().fnDraw();
        }
        return false;
    }
    
    Path.map("#/home").to(function(){
            loadPath("menu/home");
    });
    
    Path.root("#/home");

    $(document).ready(function(){
			$('body').bind('ajaxSuccess',function(event,request,settings){
				if (request.getResponseHeader('REQUIRES_AUTH') === '1'){
				   window.location = '${request.contextPath}';
				};
			});
            
            Path.listen();
            loadMenu();


            /* Menu bar fixing */
            $('#menu-navbar').mouseenter(function() {
                $(this).css({
                    height: "60px",
                    overflowY: "visible"
                });
            });

            $('#menu-navbar').mouseleave(function() {
                $(this).css({
                height: "30px",
                    overflowY: "hidden"
                });
            })
            
<%--            $(window).hashchange( function(){--%>
<%--				--%>
<%--			})--%>
    });

	function toggleGlobalSearch(){
		if(showGlobalSearch) {
			$('#search-navbar').hide();
			$('#menu-navbar').css("top","41px");
			$('#content-layout').css("margin-top","-30px");
			showGlobalSearch = false;
			
		} else {
			$('#search-navbar').show();
			$('#menu-navbar').css("top","90px");
			$('#content-layout').css("margin-top","19px");
			showGlobalSearch = true;
		}
		
	}
    $('#spinner').ajaxSend(function(event, jqxhr, settings){

            if(settings.url!="${request.contextPath}/auth/checkSession" && settings.url!="${request.contextPath}/Task/tasksCount"){
				$(this).fadeIn();
			}
		}).ajaxComplete(function(){
			$(this).fadeOut();
	});
	
	var loading = false;
	var mainInterval ;
	
	$('.usermenu').click(function(event){
    	event.stopPropagation();
	 });
	
	$('.baseapp-menu').click(function(){
		if(!loading){
				loading = true;
				checkSession();
				switch($(this).attr('target')){
		         	case '_REDIRECT_' :
		         		//window.location = $(this).attr('link');
		         		break;
		           	case '_LOAD_' :
		                loadContent($(this));
		            	break;
		            case '_NEW_WINDOW_' :
		               //	window.open($(this).attr('link'));
		                break;
		       	}
		}
        return false;
	});
	
    function loadChangePassword(){
        $('#main-content').empty();
        $('#spinner').fadeIn(1);
        $.ajax({url: '${request.contextPath}/user/changePassword',type: "GET",data: {_menucode: 'MENU_USER_CHANGE_PASSWORD'},dataType: "html",
                complete : function (req, err) {
                    $('#main-content').append(req.responseText);
                     $('#spinner').fadeOut();
                   }
        });
    }
	
	function loadContent(node){
<%
    com.kombos.baseapp.sec.shiro.User user = com.kombos.baseapp.sec.shiro.User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString())
    if(user){
        if(user.status?.equals(com.kombos.baseapp.sec.shiro.User.STATUS_PASSWORD_RESET) || user.status?.equals(com.kombos.baseapp.sec.shiro.User.STATUS_NEW_USER)){
%>
loadChangePassword();
<%
    }else{
%>
if (mainInterval)
    window.clearInterval(mainInterval);
	//Path.dispatch('#/'+node.attr('link'));
alert(node.attr('link'));
window.location.replace('#/'+node.attr('link'));
<%
        }
    }
%>
}

function loadPath(path, menucode){
<%
    if(user){
        if(user.status?.equals(com.kombos.baseapp.sec.shiro.User.STATUS_PASSWORD_RESET) || user.status?.equals(com.kombos.baseapp.sec.shiro.User.STATUS_NEW_USER)){
%>
loadChangePassword();
<%
    }else{
%>
if (mainInterval)
    window.clearInterval(mainInterval);
if(menucode){
	//$('#main-content').empty();
$('#spinner').fadeIn(1);
$.ajax({url: '${request.contextPath}/' + path,type: "GET",data: {_menucode: menucode},dataType: "html",
complete : function (req, err) {
$('#main-content').html(req.responseText);
 $('#spinner').fadeOut();
 loading = false;
}
});
} else {
//$('#main-content').empty();
$('#spinner').fadeIn(1);
$.ajax({url: '${request.contextPath}/' + path,type: "GET",dataType: "html",
complete : function (req, err) {
$('#main-content').html(req.responseText);
 $('#spinner').fadeOut();
 loading = false;
}
});
}
<%
        }
    }
%>
}

function loadController(controller){
  	if (mainInterval)
   		window.clearInterval(mainInterval);
	$('#main-content').empty();
	$('#spinner').fadeIn(1);
	$.ajax({url: "${request.getContextPath()}/"+controller,type: "POST",dataType: "html",
        	complete : function (req, err) {
            	$('#main-content').append(req.responseText);
            	 $('#spinner').fadeOut();
           	}
        });
}
   	
<%--   	function checkSession(){--%>
<%--        var result = false;--%>
<%--        $.ajax({--%>
<%--        	type: 'POST',--%>
<%--            url: '${request.getContextPath()}/auth/checkSession',--%>
<%--            async: false,--%>
<%--            success: function(data) {--%>
<%--            	if(data.loggedin){--%>
<%--            		result = true;--%>
<%--                } else {--%>
<%--                	alert('<g:message code="app.banner.logged.session.expired" />');--%>
<%--                	window.location.replace("${request.getContextPath()}/auth/login?automaticlogout=true&username=${org.apache.shiro.SecurityUtils.subject.principal.toString()}");--%>
<%--               	}--%>
<%--          	},--%>
<%--            error: function(){--%>
<%--            	alert('Server Response Error');--%>
<%--            }--%>
<%--       	});--%>
<%--       	return result;--%>
<%--    }--%>

    var pollCheckSession = function(){
    	 $.ajax({
        	type: 'POST',
            url: '${request.getContextPath()}/auth/checkSession',
            async: false,
            success: function(data) {
            	if(!data.loggedin){
            		alert('<g:message code="app.banner.logged.session.expired" />');
                	window.location.replace("${request.getContextPath()}/auth/login?automaticlogout=true&username=${org.apache.shiro.SecurityUtils.subject.principal.toString()}");
               	} else {
               		setTimeout(pollCheckSession,10000);
               	}
          	},
            error: function(){
            	//alert('Server Response Error');
            }
      	});
<%--    	--%>
	}
	
//	pollCheckSession();
	
<%--	var checkTasks = function(){--%>
<%--		$.ajax({--%>
<%--        	type: 'POST',--%>
<%--            url: '${request.getContextPath()}/Task/tasksCount',--%>
<%--            async: true,--%>
<%--            success: function(data) {--%>
<%--            	var total = data.unassignedTasksCount + data.myTasksCount;--%>
<%--            	$("#tasksCount").html(total);--%>
<%--            	setTimeout(checkTasks,10000);--%>
<%--          	},--%>
<%--            error: function(){--%>
<%--                //alert('Server Response Error');--%>
<%--            }--%>
<%--       	});--%>
<%--	}--%>

	var loadMenu = function(){
		var ul = $("#menu-navbar>ul");
		ul.empty();
		ul.append('<li class="dropdown"><a href="#/home" class="usermenu"><i class="icon-home"></i></a></li>');
		ul.append('<li class="separator"></li>');
		$.ajax({
        	type: 'GET',
            url: '${request.getContextPath()}/menu/loadMenu',
            success: function(data) {
            	for (var i = 0; i < data.length; i++) { 
    				renderMenu(data[i]);
				}      
				if(window.location.hash) {
            		window.location.replace('#/home');
				}      
			},
            error: function(){
            }
       	});
	}
	
	var renderMenu = function(menu){
		//alert('render menu ' + menu.linkController+'/'+menu.linkAction)
		if(!menu.parent){
			var ul = $("#menu-navbar>ul");
			ul.append('<li class="dropdown"><a href="#" class="usermenu"><span style="left: 0px" class="'+menu.iconClass+'"></span>'+menu.label+'</a><ul id="menu-'+menu.id+'-list" class="dropdown-menu"></ul></li>');
			ul.append('<li class="separator"></li>');           		
		} else {			
			var ul = $("#menu-"+menu.parent.id+"-list");
			if(menu.children.length > 0){
				ul.append('<li class="dropdown-submenu"><a href="#" class="usermenu"><span style="left: 0px" class="'+menu.iconClass+'"></span>'+menu.label+'</a><ul id="menu-'+menu.id+'-list" class="dropdown-menu"></ul></li>');
			} else {
				if(menu.linkAction) {
					ul.append('<li><a href="#/'+menu.linkController+'/'+menu.linkAction+'" class="usermenu"><span style="left: 0px" class="'+menu.iconClass+'"></span>'+menu.label+'</a></li>');

					Path.map('#/'+menu.linkController+'/'+menu.linkAction).to(function(){
							loadPath(menu.linkController+'/'+menu.linkAction,menu.menuCode);
						});
				} else {
					if(menu.targetMode=="_NEW_WINDOW_"){
    					ul.append('<li><a target="_blank" href="'+menu.url+'" class="usermenu"><span style="left: 0px" class="'+menu.iconClass+'"></span>'+menu.label+'</a></li>');
					}else{
                        ul.append('<li><a href="#/'+menu.linkController+'" class="usermenu"><span style="left: 0px" class="'+menu.iconClass+'"></span>'+menu.label+'</a></li>');
                        Path.map('#/'+menu.linkController).to(function(){
							loadPath(menu.linkController,menu.menuCode);
						});
					}
				}
			}
		}
	}
	
	//checkTasks();
	
	$('#tasksNotification a').click(function(){
		$.ajax({
        	type: 'POST',
            url: '${request.getContextPath()}/task/tasksNotification',
            async: false,
            success: function(data) {
            	$("#tasksNotification .tasks").html(data)
          	},
            error: function(){
                //alert('Server Response Error');
            }
       	});




	});
	
	var showTaskForm = function(id) {
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/task/getForm?taskId='+id,
   			success:function(data,textStatus){
   				$('#main-content').html(data);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };

<%--<%--%>
<%--    if(user){--%>
<%--        if(user.status?.equals(com.kombos.baseapp.sec.shiro.User.STATUS_PASSWORD_RESET) || user.status?.equals(com.kombos.baseapp.sec.shiro.User.STATUS_NEW_USER)){--%>
<%--%>--%>
<%--loadChangePassword();--%>
<%--<%--%>
<%--        }--%>
<%--    }--%>
<%--%>--%>

$('#logout').click(function(e){
            e.preventDefault();
             bootbox.confirm('${message(code: 'default.button.logout.confirm.message', default: 'Are you sure?')}',
         			function(result){
         				if(result){
         					window.location = '${request.contextPath}/auth/signOut';
         				}else{
         					e.preventDefault();
         				}
         			});
         			
         		
		});
$('#changepassword').click(function(e){
            e.preventDefault();
            loadChangePassword();
         		
		});
$('.carousel').carousel({
  	interval: 2000
})
</g:javascript>

</head>
<body>
<!-- Main navigation bar -->
<div id="main-navbar" class="navbar navbar-fixed-top">
    <div class="navbar-inner-lbus">
        <div class="container-fluid">
            <a href="#/home" class="logo"><img
                    src="${resource(dir: 'images',file: 'Hasjrat_logo.png')}" alt="logo"></a>
            <span class="logo title">Dealer Management System</span>
            <ul class="nav pull-right">
            		<li> <a href="javascript:void(0);" onclick="toggleGlobalSearch();"> <span
                        class="icon-search" style="left: 0px"></span></a></li>
<%--                <li id="tasksNotification" class="dropdown"><a href="#" data-toggle="dropdown"--%>
<%--                                                               class="dropdown-toggle usermenu"><i	class="icon-tasks icon-white"></i> <span--%>
<%--                            class="label label-warning" id="tasksCount">0</span>--%>
<%--                </a>--%>
<%--                    <ul class="dropdown-menu tasks">--%>
<%--                    </ul>--%>
<%--                </li>--%>

                <%
                    def user1 = com.kombos.baseapp.sec.shiro.User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString())
                    def permission = ""
                    user1.getRoles().each {
                        permission = permission + it.permissions
                    }

                    permission = permission + user1.permissions
                %>

 				<g:if test="${adminMenus.size() > 0}">
                <li class="separator"></li>
                
               
                <li class="dropdown"><a 
                                        class="usermenu" href="javascript:void(0);">
                    <span><g:message code="app.administrations.label"
                                     default="Administrations" /></span>
                </a>
                    <ul id="menu-admin" class="dropdown-menu">
                        <g:each in="${adminMenus}" status="i" var="adminMenu">
                           <shiro:hasPermission permission="${adminMenu.linkController}:index">
                            <li><a href="#/${adminMenu.linkController}"><span></span>
                                <g:message code="app.menu.${adminMenu.linkController}.label"
                                           default="${adminMenu.label}" /></a></li>
                            <g:javascript>
                            	Path.map("#/${adminMenu.linkController}").to(function(){loadPath("${adminMenu.linkController}", "${adminMenu.menuCode}");});
                            </g:javascript>
                            <g:if test="${adminMenu.divider}">
                                <li class="divider"></li>
                            </g:if>
                           </shiro:hasPermission>
                        </g:each>
                    </ul></li>
                </g:if>
                <li class="separator"></li>
                <li class="dropdown"><a data-toggle="dropdown"
                                        class="dropdown-toggle usermenu" href="#"> <img
                            src="${resource(dir: 'images',file: 'avatar.png')}" alt="Avatar">
                    <span>
                        %{--&nbsp;&nbsp;${org.apache.shiro.SecurityUtils.subject.principal}--}%
                        <%
                            def user = com.kombos.baseapp.sec.shiro.User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString())
                            def appSettingParam = com.kombos.baseapp.AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.USERS_DISPLAY_FORMAT)
                            def defaultUserDisplay = true
                            if(user){
                                def appSettingParamDateFormat = com.kombos.baseapp.AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
                                def appSettingParamTimeFormat = com.kombos.baseapp.AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.TIME_FORMAT)
                                String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
                                String timeFormat = appSettingParamTimeFormat.value?appSettingParamTimeFormat.value:appSettingParamTimeFormat.defaultValue
                                String pattern = dateFormat + " " + timeFormat
                                def sdf = new java.text.SimpleDateFormat(pattern)
                                if(appSettingParam){
                                    if(appSettingParam.value){
                                        if(appSettingParam.value.equalsIgnoreCase("username")){
                                            defaultUserDisplay = false
                        %>
                        &nbsp;&nbsp;<%=user.username%>
                        <%
                            } else if(appSettingParam.value.equalsIgnoreCase("username, lastLoggedIn")){
                                defaultUserDisplay = false
                        %>
                        &nbsp;&nbsp;<%=user.username +", "+ sdf.format(user.lastLoggedIn)%>
                        <%
                                        }
                                    }
                                }
                            }

                            if(defaultUserDisplay){
                        %>
                        &nbsp;&nbsp;${org.apache.shiro.SecurityUtils.subject.principal}
                        <%
                            }
                        %>
                    </span>
                </a>
                    <ul class="dropdown-menu">
                        <%--							<li><a href="#">Profile</a></li>--%>
                        <%--							<li><a href="#">Settings</a></li>--%>
                        <%--							<li class="divider"></li>--%>
                        <li><a href="#" id="changepassword">Change Password</a></li>
                        <li><a href="#" id="logout">Logout</a></li>
                    </ul></li>
            </ul>
        </div>
    </div>
</div>
<!-- / Main navigation bar -->
<!-- Global search bar -->
<div id="search-navbar" class="navbar search-navbar-fixed-top" style="display:none;">
	<form id="global-search-form" class="navbar-form" role="search" style="float: right; margin-right: 200px;">
		<div class="search-form-control">
			<label class="control-label" for="area">
			Area:&nbsp;
			</label>
			<div class="controls dropdown">
        		<g:select id="search_SearchArea" name="search_SearchArea" from="${SearchArea.list()}" optionKey="id" optionValue="m081NamaArea" class="search-combo search-input"/>
    		</div>
    	</div>
    	<div class="search-form-control">
			<label class="control-label" for="subarea">
			Sub Area:&nbsp;
			</label>
			<div class="controls dropdown">
        		<g:select id="search_SubArea" name="search_SubArea" from="${[]}" class="search-combo search-input"></g:select>
    		</div>
    	</div>
    	<div class="search-form-control">
			<label class="control-label" for="kategori">
			Kategori:&nbsp;
			</label>
			<div class="controls dropdown">
        		<g:select id="search_Kategori" name="search_Kategori" from="${[]}" class="search-combo search-input"></g:select>
    		</div>
    	</div>
    	
    	<div class="search-form-control">
    		<label class="control-label" for="searchkey">Kata kunci:&nbsp;			
			</label>
			<input name="searchkey" class="search-query search-input" type="search" >
		</div>
		
		<div class="search-form-control" style="padding-left: 5px; ">
    		<label class="control-label" for="searchkey">&nbsp;			
			</label>
			<a class="btn btn-primary search-input search-button" href="javascript:void(0);"
                       onclick="void(0);">
            	<g:message code="default.button.search.label" default="Cari" />
            </a>
            <a class="btn search-input search-button" href="javascript:void(0);"
                       onclick="void(0);">
            	<g:message code="default.button.reset.label" default="Reset" />
            </a>
		</div>
	</form>
</div>
<!-- / Global search bar -->
<!-- Sub navigation bar -->
<div id="menu-navbar" class="navbar menu-navbar-fixed-top" style="top: 41px;">	
	<ul class="nav" style="transition-property: -moz-transform; transform-origin: 0px 0px 0px; transform: translate(0px, 0px);">
    	<li class="dropdown"><a href="#/home" class="usermenu"><i class="icon-home"></i></a></li>
    	<li class="separator"></li>
<%--        <g:each in="${menus}" status="i" var="menu">--%>
<%--            <ba:menu menu="${menu}" />--%>
<%--           	<g:if test="${i < menus.size() - 1}">--%>
<%--           		<li class="separator"></li>--%>
<%--           	</g:if>            --%>
<%--        </g:each>--%>
    </ul>
</div>
<!-- / Sub navigation bar -->

<!-- Sub navigation bar -->
<div id="info-bar" class="navbar info-bar-fixed-bottom">	
	<div id="this-carousel-id" class="carousel slide"><!-- class of slide for animation -->
  <div class="carousel-inner">
    <div class="item active info"><!-- class of active since it's the first item -->
    	${runningText}
    </div>
    <div class="item info"><!-- class of active since it's the first item -->
    	${runningText}
    </div>
  </div><!-- /.carousel-inner -->
  <!--  Next and Previous controls below
        href values must reference the id for this carousel -->
<%--    <a class="carousel-control left" href="#this-carousel-id" data-slide="prev">&lsaquo;</a>--%>
<%--    <a class="carousel-control right" href="#this-carousel-id" data-slide="next">&rsaquo;</a>--%>
</div><!-- /.carousel -->
</div>
<!-- / Sub navigation bar -->

<!-- Left navigation panel -->
%{--
    <div id="left-panel0"
        style="height: 100%; position: fixed; top: 0px; left: 0px;">
        <div id="left-panel-content" style="overflow: hidden;">
            <ul
                style="transition-property: -moz-transform; transform-origin: 0px 0px 0px; transform: translate(0px, 0px);">
                <g:each in="${menus}" status="i" var="menu">
                    <g:if test="${menu.children}">
                    <li><a data-toggle="collapse"
                    class="accordion-toggle collapsed" href="#${menu.label}-list"> <span
                        class="icon-table" style="left: 0px"></span>${menu.label}<b
                        class="caret"></b></a>
                    <ul class="secondary collapse" id="${menu.label}-list"
                        style="height: 0px;">
                        <g:each in="${menu.children}" status="j" var="childmenu">
                        <li><a href="javascript:void(0);" class="baseapp-menu"	target="_LOAD_" link="${request.contextPath}/${childmenu.linkController}">
                            <span class="${childmenu.iconClass}"></span>
                            <g:message code="app.menu.${childmenu.linkController}.label"	default="${childmenu.label}" />
                        </a></li>
                        </g:each>
                    </ul></li>
                    </g:if>
                    <g:else>
                        <li><a href="javascript:void(0);" class="baseapp-menu"	target="_LOAD_" link="${request.contextPath}/${menu.linkController}">
                            <span class="${menu.iconClass}"></span>
                            <g:message code="app.menu.${menu.linkController}.label"	default="${menu.label}" />
                        </a></li>
                    </g:else>
                </g:each>
            </ul>
        </div>
        <div class="icon-caret-down"></div>
        <div class="icon-caret-up"></div>
    </div>
    --}%
<!-- / Left navigation panel -->

<!-- Page content -->
<div id="content-layout" class="container-fluid" style="margin-top: -30px;">
    <div>
        <hr>
        %{--
              <ul class="breadcrumb">
                  <li><a href="#">Home</a> <span class="divider">/</span></li>
                  <li><a href="#">Dashboard</a></li>
              </ul>
              <hr>
              --}%
    </div>

    <div id="spinner" class="spinner">
        <img src="${resource(dir:'images',file:'spinner.gif')}" alt="${message(code:'spinner.alt',default:'Loading...')}" />
    </div>



    <div class="row-fluid" id="main-content">
        <h3 class="box-header">&nbsp;</h3>
        <div class="box">
            %{--<div class="span3">${haha}</div>--}%
            %{--<div class="span6">Column 2</div>--}%
            %{--<div class="span3">Column 3</div>--}%
            <g:render template="welcomePage"/>
        </div>
    </div>

    <!-- Page footer -->
    <%
        def appVersion
        appSettingParam = AppSettingParam.findByCode(AppSettingParamService.APPLICATION_VERSION)
        if(appSettingParam.value == null){
            appVersion = appSettingParam.defaultValue
        }else{
            if(appSettingParam.value.length()==0){
                appVersion = appSettingParam.defaultValue
            }else{
                appVersion = appSettingParam.value
            }
        }
    %>
    <div id="main-footer">
        <g:message code="app.copyright" default="" /><br/>
        <span><%=appVersion%></span>
    </div>
    <!-- / Page footer -->
</div>
<!-- / Page content -->

</body>
</html>
