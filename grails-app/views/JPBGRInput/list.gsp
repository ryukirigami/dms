
<%@ page import="com.kombos.board.JPB" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'JPB.label', default: 'JPB GR')}" />
		%{--<title><g:message code="default.list.label" args="[entityName]" /></title>--}%
		<r:require modules="baseapplayout, baseapplist, bootstraptooltip" />
        <style>
        .dss-selected {
            background-color: #FF0000 !important;
        }
        </style>
		<g:javascript>
			var show;
			var edit;
			var idApp = "${params.appointmentId}";
			var noWo = "${params.noWo}";
			var saveJPB;
			var dss;
			var tanggalDss = [];
            var jamStart = [];
			var jamStop = [];
			var arrStall = [];
			var pesans = "${pesan}\n${pesan2}\n${pesan3}";
            var jumJPB = "${countJPB}";

			$(function(){
			 $(".checkJanji").click(function() {
                selectedBox = this.id;

                $(".checkJanji").each(function() {
                    if ( this.id == selectedBox )
                    {
                        this.checked = true;
                    }
                    else
                    {
                        this.checked = false;
                    };
                });
            });
			dss = function(){
                tanggalDss = []
                jamStart = []
                jamStop = []
                arrStall = []
                var tglRencana = $('#input_rcvDate').val();

			    var jamRencana = $('#input_rcvHour').val() + ":" + $('#input_rcvMinute').val();
			    var tglPenyerahan = $('#input_penyerahan').val();
			    var jamPenyerahan = $('#input_penyerahanHour').val()+":"+$('#input_penyerahanMinute').val();
			    var checkJanjiDatang = $('#checkJanjiDatang').attr('checked');
			    var checkJanjiPenyerahan = $('#checkJanjiPenyerahan').attr('checked');
			    if(!checkJanjiDatang && !checkJanjiPenyerahan){
			        alert("Data belum lengkap");
			        return;
			    }


			    if((checkJanjiDatang && !tglRencana) || (checkJanjiPenyerahan && !tglPenyerahan)){
                    alert("Data belum lengkap");
			        return;
			    }
                $.ajax({url: '${request.contextPath}/JPBGRInput/dssAppointment',
					type: "POST",
					data: {tglRencana:tglRencana,
					tglPenyerahan:tglPenyerahan,
					idApp:idApp,
					noWo: noWo,
					jamRencana:jamRencana,
					jamPenyerahan:jamPenyerahan,
					checkJanjiDatang: checkJanjiDatang,
					checkJanjiPenyerahan: checkJanjiPenyerahan,
					aksi: "input",
					jenis : "GR"
					},
					success: function(data) {
					var slots;
					    if(data.status == 'found'){
                            $('input[name=statusSave][value=dss]').attr('checked', true);
					        if(data.jumData > 1){
                                for(var a=0; a < data.jumData; a++){
                                    slots = data.slot[a]
                                    tanggalDss.push(data.tanggalDSS[a])
                                    arrStall.push(data.stallId[a])
                                    for(var i = 0; i < slots.length; i++){
                                        if(a==0){
                                            $('.'+slots[i]+'[data-stallId="'+data.stallId[a]+'"]').addClass("dss-selected");
                                        }
                                        if(i==0){
                                            jamStart.push(slots[i]);
                                        }
                                        if(i+1==slots.length){
                                            jamStop.push(slots[i])
                                        }
                                    }
					            }
					        }
					        else{
					            slots = data.slot[0]
					            tanggalDss.push(data.tanggalDSS[0]);
					            jamStart.push(slots[0]);
					            jamStop.push(slots[slots.length-1]);
                                arrStall.push(data.stallId[0])
                                for(var j = 0; j < slots.length; j++){
                                    $('.'+slots[j]+'[data-stallId="'+data.stallId[0]+'"]').addClass("dss-selected");
                                }
					        }
					    }
//					    reloadASBTable();
					},
					complete : function (req, err) {
						$('#spinner').fadeOut();
						loading = false;
					}
				});
             }

            closeBillto = function (){
                loadPath('editJobParts/list?nowo='+noWo);
           }

            saveJPB = function(){
                $first = $('.dss-selected').first();
                jamDatang = $first.data("jam");
                menitDatang = $first.data("menit");
                $last = $('.dss-selected').last();
                penyerahans = $last.data("jam");
                menitPenyerahan = $last.data("menit");
                stallid = $first.data("stallid");
                var saveBy = $('input[name="statusSave"]:checked').val();
                var tglRencana = $('#input_rcvDate').val();
			    var jamRencana = $('#input_rcvHour').val() + ":" + $('#input_rcvMinute').val();
			    var tglPenyerahan = $('#input_penyerahan').val();
			    var jamPenyerahan = $('#input_penyerahanHour').val()+":"+$('#input_penyerahanMinute').val();
			    var checkJanjiDatang = $('#checkJanjiDatang').attr('checked');
			    var checkJanjiPenyerahan = $('#checkJanjiPenyerahan').attr('checked');

                //Validasi Tanggal Receive
                if (tglRencana == null || tglRencana == ""){
                    alert('Tanggal Janji Datang belum lengkap!');
                    return
                }
                //Validasi Jam Receive
                if ($('#input_rcvHour').val()=="null" || $('#input_rcvMinute').val()=="null"){
                    alert('Jam janji datang belum lengkap!');
                    return
                }

                //Validasi Tanggal Penyerahan
                if (tglPenyerahan == null || tglPenyerahan == ""){
                    alert('Tanggal Janji Penyerahan masih Kosong!');
                    return
                }
                //Validasi Jam Penyerahan
                if ($('#input_penyerahanHour').val()=="null" || $('#input_penyerahanMinute').val()=="null"){
                    alert('Jam janji penyerahan belum lengkap!');
                    return
                }


			    // if(tglPenyerahan instanceof Date){
			    //     alert('salah');
                 //    return
			    // }

			    if(checkJanjiPenyerahan == "checked"){
			       tanggal = tglPenyerahan;
			    } else if(checkJanjiDatang == "checked"){
			       tanggal = tglRencana;
			    }
                if(!tanggal){
			        tanggal = $('#search_tglView').val();
			    }
                // if(!tglRencana){
                //     alert('Anda belum memilih metode simpan');
                //     return
                // }

                var jsonT = JSON.stringify(tanggalDss);
                var jsonSa = JSON.stringify(jamStart);
                var jsonSo = JSON.stringify(jamStop);
                var jsonS = JSON.stringify(arrStall);

                if(!saveBy){
                    alert('Anda belum memilih metode simpan');
                    return
                }
			    if(saveBy=="own"){
			        if(!stallid){
			            alert("Belum ada data terpilih");
			            return
			        }
			        jsonT = $("#search_tglView").val() ? $("#search_tglView").val() : "${new java.util.Date()?.format("dd/MM/yyyy")}";
                    jsonSa = jamDatang+":"+menitDatang
                    jsonSo = penyerahans+":"+menitPenyerahan
                    jsonS = stallid

			    }else{
			        if(!tglRencana || !tglPenyerahan){
                        alert("Data Belum lengkap");
                        return;
                    }
			    }

			    var pesanConfirm = jumJPB>0 ? pesans : "Apakah anda yakin?";
                var apaka = confirm(pesanConfirm);
                if(apaka){
                    $.ajax({url: '${request.contextPath}/JPBGRInput/saveJPB',
                        type: "POST",
                        data: {
                            tanggal:jsonT,
                            idApp:idApp,
                            noWo: noWo,
                            jamRencana:jsonSa,
                            jamPenyerahan:jsonSo,
                            planStartDate:tglRencana,
                            planStartHour:jamRencana,
                            planDeliverDate:tglPenyerahan,
                            planDeliverHour:jamPenyerahan,
                            stallid: jsonS,
                            saveBy:saveBy
                        },
                        success: function(data) {
                           var oTable = $('#JPBGRInput_datatables').dataTable();
                            oTable.fnReloadAjax();
                        },
                        complete : function (req, err) {
                            $('#spinner').fadeOut();
                            loading = false;
                        }
                    });
                }
            }

	$('#search_tglView').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tglView_day').val(newDate.getDate());
			$('#search_tglView_month').val(newDate.getMonth()+1);
			$('#search_tglView_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
//			var oTable = $('#JPBGRInput_datatables').dataTable();
//            oTable.fnReloadAjax();
	});

	$('#search_tglView3Hari').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tglView3Hari_day').val(newDate.getDate());
			$('#search_tglView3Hari_month').val(newDate.getMonth()+1);
			$('#search_tglView3Hari_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
//			var oTable = $('#JPBGRInput_datatables').dataTable();
//            oTable.fnReloadAjax();
	});
				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('JPB');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('JPB', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('JPB', '${request.contextPath}/JPB/massdelete', reloadJPBInputTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('JPB','${request.contextPath}/JPB/show/'+id);
				};
				
				edit = function(id) {
					editInstance('JPB','${request.contextPath}/JPB/edit/'+id);
				};

   	jpbInput = function(){
        var oTable = $('#JPBGRInput_datatables').dataTable();
        oTable.fnReloadAjax();
        var from = $("#search_tglView").val().split("/");
        var theDate = new Date();
        theDate.setFullYear(from[2], from[1] - 1, from[0]);
        //alert("theDate=" + theDate);
        var m_names = new Array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
        var formattedDate =  theDate.getDate() + " " + m_names[theDate.getMonth()] + " " + theDate.getFullYear();
        document.getElementById("lblTglView").innerHTML = formattedDate;
   	}

   	 var reloadAll = function(){
                    var table = $('#JPBGRInput_datatables');
                    if(table.length > 0){
                        jpbInput();
                        setTimeout(function(){reloadAll()}, 10000);
                    }
                }
//                setTimeout(function(){reloadAll()}, 10000);
});

            function doDeleteJPB(){
                var params;
                params = {
                    noWO : noWo
                }
                $.post("${request.getContextPath()}/JPBGRInput/deleteJPB", params, function(data) {
                    if (data){
                        if(data=="ok"){
                            pesans = "";
                            jumJPB = 0;
                            $("#buttonReset").hide();
                            alert("Success");
                            reloadJPBInputTable();
                        }else{
                            alert("Gagal, terjadi kesalahan")
                        }
                    }
                    else
                        alert("Internal Server Error");
                });
            }
</g:javascript>
	</head>
	<body>
    <div class="navbar box-header no-border">
   		<span class="pull-left">Job Progress Board (JPB) General Repair</span>
   	</div>

	<div class="box">
        <table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">
            <tr>
            <td style="width: 45%;vertical-align: top;">

                <div class="box">
                    <legend style="font-size: small">Input JPB</legend>
                    <table style="width: 100%;border: 0px">
                        <tr>
                            <td>
                                <label class="control-label" for="lbl_rcvDate">
                                    Tgl/Jam Janji Datang <span>*</span>
                                </label>
                            </td>
                            <td><g:checkBox name="checkJanjiDatang" class="checkJanji"/></td>
                            <td>
                                <div id="lbl_rcvDate" class="controls">
                                    <ba:datePicker id="input_rcvDate" name="input_rcvDate" precision="day" format="dd-MM-yyyy" value="" style="width: 180px"/>

                                    <select id="input_rcvHour" name="input_rcvHour" style="width: 60px" required="">
                                    %{
                                        out.println('<option value=null>Jam</option>');
                                        for (int i=0;i<24;i++){
                                            if(i<10){
                                                out.println('<option value="0'+i+'">0'+i+'</option>');
                                            } else {
                                                out.println('<option value="'+i+'">'+i+'</option>');
                                            }
                                        }
                                    }%
                                    </select> :
                                    <select id="input_rcvMinute" name="input_rcvMinuite" style="width: 60px" required="">
                                    %{
                                        out.println('<option value=null>Menit</option>');
                                        for (int i=0;i<60;i++){
                                            if(i<10){
                                                out.println('<option value="0'+i+'">0'+i+'</option>');
                                            } else {
                                                out.println('<option value="'+i+'">'+i+'</option>');
                                            }
                                        }
                                    }%
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Status Mobil</td>
                            <td colspan="2">
                                <div class="controls">
                                    <g:radioGroup name="statusMobil" values="['W','L']" value="${JPBInstance?.t351StaOkCancelReSchedule}" labels="['Ditunggu','Ditinggal']">
                                        ${it.radio} <g:message code="${it.label}" />
                                    </g:radioGroup>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label" for="lbl_penyerahan">
                                    Tgl/Jam Janji Penyerahan <span>*</span>
                                </label>
                            </td>
                            <td><g:checkBox name="checkJanjiPenyerahan" class="checkJanji"/></td>
                            <td>
                                <div id="lbl_penyerahan" class="controls">
                                    <ba:datePicker id="input_penyerahan" name="input_penyerahan" precision="day" format="dd-MM-yyyy"  value="" style="width: 180px" />
                                    <select id="input_penyerahanHour" name="input_penyerahanHour" style="width: 60px" required="">
                                    %{
                                        out.println('<option value=null>Jam</option>');
                                        for (int i=0;i<24;i++){
                                            if(i<10){
                                                out.println('<option value="0'+i+'">0'+i+'</option>');
                                            } else {
                                                out.println('<option value="'+i+'">'+i+'</option>');
                                            }
                                        }
                                    }%
                                    </select> :
                                    <select id="input_penyerahanMinute" name="input_penyerahanMinuite" style="width: 60px" required="">
                                    %{
                                        out.println('<option value=null>Menit</option>');
                                        for (int i=0;i<60;i++){
                                            if(i<10){
                                                out.println('<option value="0'+i+'">0'+i+'</option>');
                                            } else {
                                                out.println('<option value="'+i+'">'+i+'</option>');
                                            }
                                        }
                                    }%
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Save By?
                            </td>
                            <td colspan="2">
                                <div class="controls">
                                    <g:radioGroup name="statusSave" id="statusSave" values="['dss','own']" labels="['DSS','Manual']">
                                        ${it.radio} <g:message code="${it.label}" />
                                    </g:radioGroup>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: right">
                                <g:if test="${params.noWo && countJPB>0}">
                                    <button class="btn btn-primary pull-left" id="buttonReset" onclick="doDeleteJPB();">Clear JPB</button>
                                </g:if>
                                <button class="btn btn-primary" id="buttonSearch" onclick="dss();">Search</button>
                                <button class="btn btn-primary" id="buttonSave" onclick="saveJPB();">Save</button>
                            </td>
                        </tr>
                    </table>
                </div>

            </td>
            <td style="width: 22%;vertical-align: top;padding-left: 10px;">

            </td>

            <td style="width: 33%;vertical-align: top;padding-left: 10px;">
                <div class="box">
                    <legend style="font-size: small">View JPB</legend>
                    <table style="width: 100%;border: 0px">
                        <tr>
                            <td>
                                <label class="control-label" for="lbl_rcvDate">
                                    Nama Workshop
                                </label>
                            </td>
                            <td>
                                <div id="lbl_companyDealer" class="controls">
                                    <g:select id="input_companyDealer" readonly="" disabled="" name="companyDealer.id" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list{eq("staDel","0");order("m011NamaWorkshop")}}" value="${session?.userCompanyDealer?.id}" optionKey="id" required="" class="many-to-one"/>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label class="control-label" for="lbl_tglView">
                                    Tanggal
                                </label>
                            </td>
                            <td>
                                <div id="lbl_tglView" class="controls">
                                    <div id="filter_tglView" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                                         <input type="hidden" name="search_tglView" value="date.struct">
                                         <input type="hidden" name="search_tglView_day" id="search_tglView_day" value="">
                                         <input type="hidden" name="search_tglView_month" id="search_tglView_month" value="">
                                         <input type="hidden" name="search_tglView_year" id="search_tglView_year" value="">
                                         <input type="text" data-date-format="dd/mm/yyyy" name="search_tglView_dp" value="${new java.util.Date().format("dd/MM/yyyy")}" id="search_tglView" class="search_init">
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <button class="btn btn-primary" id="buttonView" onclick="jpbInput()">View</button>
                            </td>
                        </tr>

                    </table>
                </div>

            </td>

            </tr>
            <tr>
                <td colspan="3">
                    <div id="JPB-table">
                        <g:if test="${flash.message}">
                            <div class="message" role="status">
                                ${flash.message}
                            </div>
                        </g:if>
                        <g:render template="dataTables" />
                    </div>
                    <g:if test="${params.noWo}">
                        <div class="control-group">
                            <g:field type="button" onclick="closeBillto();" class="btn cancel" name="close" id="close" value="Edit JOB Parts" />
                        </div>
                    </g:if>
                    </td>
                </tr>
        </table>
	</div>
</body>
</html>
