<%--
  Created by IntelliJ IDEA.
  User: Ahmad Fawaz
  Date: 27/02/15
  Time: 10:37
--%>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript disappointmentGrition="head">
    var previewData;
    var changeReport;
	$(function(){
	    <g:if test="${!isTeknisi}">
            $("#search_tanggalAkhir").hide();
	    </g:if>

        previewData = function(){
                var namaReport = $("#namaReport").val();
                var tanggal = $('#search_tanggal').val();
                var tanggalAkhir = $('#search_tanggalAkhir').val();
                var companyDealer = $('#companyDealer').val();
                var format = $('input[name="formatReport"]:checked').val();
                var bulan = $('#bulanCari').val();
                window.location = "${request.contextPath}/financeReport/previewData?namaReport="+namaReport+"&tanggal="+tanggal+"&tanggalAkhir="+tanggalAkhir+"&companyDealer="+companyDealer+"&format="+format+"&bulan="+bulan;
        }

        changeReport = function(){
            var namaReport = $("#namaReport").val();
            if(namaReport=="PENCAPAIAN_HASIL_MEKANIK" || namaReport=="REKAP_PENCAPAIAN_HASIL_MEKANIK"){
                $('#search_tanggal').show();
                $('#search_tanggalAkhir').show();
                $('#bulanCari').hide();
            }else if(namaReport=="DAFTAR_PENJELASAN_UMUR_PIUTANG" || namaReport=="DAFTAR_UMUR_PIUTANG"){
                $('#bulanCari').show();
                $('#search_tanggalAkhir').hide();
                $('#search_tanggal').hide();
            }
            else{
                $('#search_tanggal').show();
                $('#bulanCari').hide();
                $('#search_tanggalAkhir').hide();
            }

        }
        
        var checkin = $('#search_tanggal').datepicker({
            onRender: function(date) {
                return '';
            }
        }).on('changeDate', function(ev) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate());
            checkout.setValue(newDate);
            checkin.hide();
            $('#search_tanggalAkhir')[0].focus();
        }).data('datepicker');
        
        var checkout = $('#search_tanggalAkhir').datepicker({
            onRender: function(date) {
                return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');
	});
    </g:javascript>

</head>
<body>
<div class="navbar box-header no-border">
    Laporan Keuangan
</div><br>
<div class="box">
    <div class="span12" id="appointmentGr-table">
        <div id="form-appointmentGr" class="form-horizontal">
            <fieldset>
                <div class="control-group">
                    <label class="control-label" for="filter_periode" style="text-align: left;">
                        Format Report
                    </label>
                    <div id="filter_format" class="controls">
                        <g:radioGroup name="formatReport" id="formatReport" values="['x','p']" labels="['Excel','PDF']" value="x" required="" >
                            ${it.radio} ${it.label}
                        </g:radioGroup>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="filter_periode" style="text-align: left;">
                        Company Dealer
                    </label>
                    <div id="filter_company" class="controls">
                        <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                            <g:select name="companyDealer" id="companyDealer" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                        </g:if>
                        <g:else>
                            <g:select name="companyDealer" id="companyDealer" readonly="" disabled="" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                        </g:else>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="filter_periode" style="text-align: left;">
                        Tanggal
                    </label>
                    <div id="filter_periode" class="controls">
                        <ba:datePicker id="search_tanggal" name="search_tanggal" precision="day" format="dd-MM-yyyy"  value="${new Date()}" />
                        <ba:datePicker id="search_tanggalAkhir" name="search_tanggalAkhir" precision="day" format="dd-MM-yyyy" style="display: none" value="${new Date()}" />
                        <select name="bulanCari" id="bulanCari" style="font-size: 12px; display: none">
                            <option value="1" ${bulanCari==1 ? "selected" : ""}>Januari</option>
                            <option value="2" ${bulanCari==2 ? "selected" : ""}>Februari</option>
                            <option value="3" ${bulanCari==3 ? "selected" : ""}>Maret</option>
                            <option value="4" ${bulanCari==4 ? "selected" : ""}>April</option>
                            <option value="5" ${bulanCari==5 ? "selected" : ""}>Mei</option>
                            <option value="6" ${bulanCari==6 ? "selected" : ""}>Juni</option>
                            <option value="7" ${bulanCari==7 ? "selected" : ""}>Juli</option>
                            <option value="8" ${bulanCari==8 ? "selected" : ""}>Agustus</option>
                            <option value="9" ${bulanCari==9 ? "selected" : ""}>September</option>
                            <option value="10" ${bulanCari==10 ? "selected" : ""}>Oktober</option>
                            <option value="11" ${bulanCari==11 ? "selected" : ""}>November</option>
                            <option value="12" ${bulanCari==12 ? "selected" : ""}>Desember</option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="filter_nama" style="text-align: left;">
                        Nama Report
                    </label>
                    <div id="filter_nama" class="controls">
                        <select name="namaReport" id="namaReport" size="${!isTeknisi ? 24 : 4}" style="width: 44%; font-size: 12px;" onchange="changeReport();">
                            <g:if test="${!isTeknisi}">
                                <option value="LHKB" ${!isTeknisi?'selected':'disabled'}>LHKB</option>
                                <option value="REGISTER_SERVICE_ORDER" >Register Service Order</option>
                                <option value="ANALISA_KAS" >Analisa KAS</option>
                                <option value="DAFTAR_PENAGIHAN" >Daftar Penagihan</option>
                                <option value="REGISTRASI_FAKTUR_PAJAK" >Register Faktur Pajak</option>
                            %{--<option value="REGISTRASI_KWITANSI">Register Kwitansi (kwitansi panjar, OR, dll)</option>--}%
                                <option value="REGISTRASI_CEK_BG" >Register Cek / BG</option>
                                <option value="DAFTAR_UMUR_PIUTANG" >Daftar Umur Piutang</option>
                                <option value="DAFTAR_PENJELASAN_UMUR_PIUTANG" >Daftar Penjelasan Umur Piutang</option>
                                <option value="PERINCIAN_TRANSAKSI_KAS" >Perincian Transaksi Kas</option>
                                <option value="PERINCIAN_TRANSAKSI_BANK" >Perincian Transaksi Bank</option>
                            </g:if>
                            <option value="REKAP_PENCAPAIAN_HASIL_MEKANIK" ${!isTeknisi?'':'selected'}>Rekap Pencapaian Mekanik</option>
                            <option value="PENCAPAIAN_HASIL_MEKANIK">Pencapaian Hasil Mekanik (Detail)</option>
                            <g:if test="${!isTeknisi}">
                                <option value="BUKTI_KAS_KELUAR_HUTANG" >Bukti Kas Keluar Pembayaran Hutang Usaha</option>
                                <option value="BUKTI_KAS_KELUAR_REFUND" >Bukti Kas Keluar Pengembalian DP</option>
                                <option value="BUKTI_KAS_KELUAR_OTHER" >Bukti Kas Keluar Pengeluaran Operasional dan lain lain</option>
                                <option value="BUKTI_KAS_MASUK_TUNAI" >Bukti Kas Masuk Pendapatan Tunai</option>
                                <option value="BUKTI_KAS_MASUK_PIUTANG" >Bukti Kas Masuk Penerimaan Piutang</option>
                                <option value="BUKTI_KAS_MASUK_OTHER" >Bukti Kas Masuk Penerimaan lain-lain</option>
                                <option value="BUKTI_BANK_KELUAR_HUTANG" >Bukti Bank Keluar Pembayaran Hutang Usaha</option>
                                <option value="BUKTI_BANK_KELUAR_REFUND" >Bukti Bank Keluar Pengembalian DP</option>
                                <option value="BUKTI_BANK_KELUAR_OTHER" >Bukti Bank Keluar Pengeluaran Operasional dan lain lain</option>
                                <option value="BUKTI_BANK_MASUK_TUNAI" >Bukti Bank Masuk Pendapatan Tunai</option>
                                <option value="BUKTI_BANK_MASUK_PIUTANG" >Bukti Bank Masuk Penerimaan Piutang</option>
                                <option value="BUKTI_BANK_MASUK_OTHER" >Bukti Bank Masuk Penerimaan lain-lain</option>
                            </g:if>
                        </select>
                    </div>
                </div>
            </fieldset>
        </div>
        <g:field type="button" onclick="previewData()" class="btn btn-primary create" name="preview"  value="${message(code: 'default.button.upload.label', default: 'Preview')}" />
        <g:field type="button" onclick="window.location.replace('#/home')" class="btn btn-cancel cancel" name="cancel"  value="${message(code: 'default.button.upload.label', default: 'Close')}" />
    </div>
</div>
</body>
</html>
