<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="title" value="View Material Per Wo" />
		<title>${title}</title>
		<r:require modules="baseapplayout" />
        <g:javascript disposition="head">
	var show;
	var loadForm;
	var noUrut = 1;
	var printClaim;
	var printClaimCont;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){


	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
                                   onLoading="jQuery('#spinner').fadeIn(1);"
                                   onSuccess="loadForm(data, textStatus);"
                                   onComplete="jQuery('#spinner').fadeOut();" />
            break;
        case '_DELETE_' :
            bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});

         		break;
       }
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/mos/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };


    loadForm = function(data, textStatus){
		$('#mos-form').empty();
    	$('#mos-form').append(data);
   	}

    shrinkTableLayout = function(){
    	$("#mos-table").hide()
        $("#mos-form").css("display","block");
   	}

   	expandTableLayout = function(){
        $("#mos-table").show()
        $("#mos-form").css("display","none");
   	}

   	massDelete = function() {
   		var recordsToDelete = [];
		$("#mos-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});

		var json = JSON.stringify(recordsToDelete);

		$.ajax({
    		url:'${request.contextPath}/viewMaterialPerWo/massdelete',
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		succes : function(data){
    		  toastr.success('<div>Delete Data Succes<div>')
    		},
    		complete: function(xhr, status) {
        		reloadMosTable();
    		}
		});

   	}

});
     var checkin = $('#search_t017Tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
        var newDate = new Date(ev.date)
        newDate.setDate(newDate.getDate() + 1);
        checkout.setValue(newDate);
        checkin.hide();
        $('#search_t017Tanggal2')[0].focus();
    }).data('datepicker');

    var checkout = $('#search_t017Tanggal2').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

    edit = function(id) {
        $('#spinner').fadeIn(1);
        window.location.replace('#/editMos')
        $.ajax({url: '${request.contextPath}/mosInput?noMos='+id,
            type:"GET",dataType: "html",
            complete : function (req, err) {
                $('#main-content').html(req.responseText);
                $('#spinner').fadeOut();
                loading = false;
            }
        });
    };
    klik = function(){

        $('#spinner').fadeIn(1);
        $.ajax({
            url: '${request.contextPath}/mosInput',
            type: "GET",dataType:"html",
            complete : function (req, err) {
                $('#main-content').html(req.responseText);
                $('#spinner').fadeOut();
            }
        });
    }
    printMos = function(){
           checkMos =[];
           var nowo = false
            $("#mos-table tbody .row-select").each(function() {
                 if(this.checked){
                   var nRow = $(this).next("#noWo").val()?$(this).next("#noWo").val():"-"
                    if(nRow!="-"){
                        checkMos.push(nRow);
                        nowo = true;
                    }
                }
            });
            if(checkMos.length<1 ){
                alert('Silahkan Pilih tanda centang yang ada di Nomor WO');
                return;
            }else if(nowo==false){
                alert('Silahkan Pilih tanda centang yang ada di Nomor WO');
                return;
            }
           var idMos =  JSON.stringify(checkMos);
           window.location = "${request.contextPath}/viewMaterialPerWo/printMos?idMos="+idMos;
    }
 </g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left">${title}</span>
        <ul class="nav pull-right">

            <li><a class="pull-right" href="#/mosInput" onclick="klik()" style="display: block;" >&nbsp;&nbsp;<i class="icon-plus"></i>&nbsp;&nbsp;
            <li><a class="pull-right box-action" href="#"
                   style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
                        class="icon-remove"></i>&nbsp;&nbsp;
            </a></li>
            <li class="separator"></li>
        </ul>
	</div>
	<div class="box">
		<div class="span12" id="mos-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <fieldset>
                <table style="padding-right: 10px">
                    <tr>
                        <td style="width: 130px">
                            <label class="control-label" for="t951Tanggal">
                                <g:message code="auditTrail.tanggal.label" default="Kriteria Pencarian" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="filter_kriteria" class="controls">
                                <select name="kriteria" id="kriteria">
                                    <option value="" disabled selected>Pilih</option>
                                    <option value="s_noWo" >Nomor Wo</option>
                                    <option value="s_noPol" >No Polisi</option>
                                    <option value="s_SA" >Nama SA</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 130px">
                            <label class="control-label" for="t951Tanggal">
                                <g:message code="auditTrail.tanggal.label" default="Kata Kunci" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="filter_kataKunci" class="controls">
                                    <g:textField name="kunci" id="kunci" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" >
                            <div class="controls" style="right: 0">
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-primary" name="view" id="view" >View</button>
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-cancel clear" name="clear" id="clear" >Clear</button>
                            </div>
                        </td>
                    </tr>
                </table>

            </fieldset>
           	<g:render template="dataTables" />
            
           	<fieldset class="buttons controls" style="padding-top: 10px;">
                %{--<button id='ubahData' onclick="ubahMos();" type="button" class="btn btn-primary">Ubah Data</button>--}%
                <button id='printMosData' onclick="printMos();" type="button" class="btn btn-primary">Print Mos</button>
			</fieldset>
            
		</div>
		<div class="span7" id="mos-form" style="display: none;"></div>
	</div>
</body>
</html>
