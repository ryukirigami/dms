<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
		<g:javascript>
var mosSubSubTable_${idTable};
$(function(){ 
mosSubSubTable_${idTable} = $('#mos_datatables_sub_sub_${idTable}').dataTable({
		"bAutoWidth" : false,
		"bPaginate" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "t",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": false,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubSubList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"6px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"5px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"6px",
               "sDefaultContent": ''
},
{
	"sName": "goods",
	"mDataProp": "kodeMaterial",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"> <input type="hidden" value="'+row['id']+'">&nbsp;&nbsp; ' + data ;
	},
	"bSortable": false,
	"sWidth":"167px",
	"bVisible": true
},
{
	"sName": "goods",
	"mDataProp": "namaMaterial",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"212px",
	"bVisible": true
},
{
	"sName": "t417Qty1",
	"mDataProp": "t417Qty1",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "goods",
	"mDataProp": "satuan",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"100px",
	"sDefaultContent": '',
	"bVisible": true
},
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"100px",
	"sDefaultContent": '',
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						aoData.push(
									{"name": 'noMos', "value": "${noMos}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
		</g:javascript>
	</head>
	<body>
	<div class="innerinnerDetails">
<table id="mos_datatables_sub_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
	class="display table table-striped table-bordered table-hover">
    <thead>
        <tr>
           <th></th>
           <th></th>
           <th></th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Kode Material</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Nama Material</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Qty</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Satuan</div>
			</th>
			<th style="border-bottom: none; padding: 5px;" />
			<th style="border-bottom: none; padding: 5px;" />

        </tr>
			    </thead>
			    <tbody></tbody>
			</table>
	</div>
</body>
</html>
