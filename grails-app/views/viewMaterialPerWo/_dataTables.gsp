
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay" />

<table id="mos_datatables_${idTable}" cellpadding="0" cellspacing="0" border="0"
	class="display table table-striped table-bordered table-hover table-selectable" style="table-layout: fixed; ">
	<thead>
		<tr>
            <th style="vertical-align: middle;">
                <div style="height: 10px"> </div>
            </th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Nomor WO</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Tanggal WO</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Nomor Polisi</div>
			</th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Nama SA</div>
            </th>

        </tr>
		
	</thead>
</table>

<g:javascript>
var mosTable;
var reloadMosTable;
$(function(){

reloadMosTable = function() {
		mosTable.fnDraw();
	}
    $('#clear').click(function(e){
        $('#kriteria').val("");
        $('#kunci').val("");
        mosTable.fnDraw();
	});
    $('#view').click(function(e){
        e.stopPropagation();
		mosTable.fnDraw();
	});
	var anOpen = [];
	$('#mos_datatables_${idTable} td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = mosTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST', 
	   			data : oData,
	   			url:'${request.contextPath}/viewMaterialPerWo/sublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = mosTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
    		
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			mosTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});
	
    $('#mos_datatables_${idTable} a.edit').live('click', function (e) {
        e.preventDefault();
         
        var nRow = $(this).parents('tr')[0];
         
        if ( nEditing !== null && nEditing != nRow ) {
            restoreRow( mosTable, nEditing );
            editRow( mosTable, nRow );
            nEditing = nRow;
        }
        else if ( nEditing == nRow && this.innerHTML == "Save" ) {
            saveRow( mosTable, nEditing );
            nEditing = null;
        }
        else {
            editRow( mosTable, nRow );
            nEditing = nRow;
        }
    } );
    
	reloadMosTable = function() {
		mosTable.fnDraw();
	}

 	mosTable = $('#mos_datatables_${idTable}').dataTable({

		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "sClass": "control center",
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "t401NoWO",
	"mDataProp": "t401NoWO",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"> <input type="hidden" id="noWo" value="'+row['t401NoWO']+'">&nbsp;&nbsp;' + data+ ' <a class="pull-right cell-action" href="javascript:void(0);" onclick="edit(\''+row['t401NoWO']+'\');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": false,
	"sWidth":"250px",
	"bVisible": true
},
{
	"sName": "t401TanggalWO",
	"mDataProp": "t401TanggalWO",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"250px",
	"bVisible": true
},
{
	"sName": "historyCustomerVehicle",
	"mDataProp": "noPol",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"250px",
	"bVisible": true
},
{
	"sName": "t401NamaSA",
	"mDataProp": "t401NamaSA",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"250px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        var kriteria = $('#kriteria').val();

						var kataKunci = $('#kunci').val();

						if(kriteria){
							aoData.push(
									{"name": 'kriteria', "value": kriteria}
							);
						}


						if(kataKunci){
							aoData.push(
									{"name": 'kataKunci', "value": kataKunci}
							);
						}
	
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});



</g:javascript>



