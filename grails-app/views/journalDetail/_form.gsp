<%@ page import="com.kombos.finance.JournalDetail" %>



<div class="control-group fieldcontain ${hasErrors(bean: journalDetailInstance, field: 'accountNumber', 'error')} ">
	<label class="control-label" for="accountNumber">
		<g:message code="journalDetail.accountNumber.label" default="Account Number" />
		
	</label>
	<div class="controls">
	<g:select id="accountNumber" name="accountNumber.id" from="${com.kombos.finance.AccountNumber.list()}" optionKey="id" required="" value="${journalDetailInstance?.accountNumber?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: journalDetailInstance, field: 'creditAmount', 'error')} ">
	<label class="control-label" for="creditAmount">
		<g:message code="journalDetail.creditAmount.label" default="Credit Amount" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="creditAmount" value="${journalDetailInstance.creditAmount}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: journalDetailInstance, field: 'debitAmount', 'error')} ">
	<label class="control-label" for="debitAmount">
		<g:message code="journalDetail.debitAmount.label" default="Debit Amount" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="debitAmount" value="${journalDetailInstance.debitAmount}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: journalDetailInstance, field: 'journal', 'error')} ">
	<label class="control-label" for="journal">
		<g:message code="journalDetail.journal.label" default="Journal" />
		
	</label>
	<div class="controls">
	<g:select id="journal" name="journal.id" from="${com.kombos.finance.Journal.list()}" optionKey="id" required="" value="${journalDetailInstance?.journal?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: journalDetailInstance, field: 'journalTransactionType', 'error')} ">
	<label class="control-label" for="journalTransactionType">
		<g:message code="journalDetail.journalTransactionType.label" default="Journal Transaction Type" />
		
	</label>
	<div class="controls">
	<g:textField name="journalTransactionType" value="${journalDetailInstance?.journalTransactionType}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: journalDetailInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="journalDetail.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${journalDetailInstance?.lastUpdProcess}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: journalDetailInstance, field: 'subLedger', 'error')} ">
	<label class="control-label" for="subLedger">
		<g:message code="journalDetail.subLedger.label" default="Sub Ledger" />
		
	</label>
	<div class="controls">
	<g:textField name="subLedger" value="${journalDetailInstance?.subLedger}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: journalDetailInstance, field: 'subType', 'error')} ">
	<label class="control-label" for="subType">
		<g:message code="journalDetail.subType.label" default="Sub Type" />
		
	</label>
	<div class="controls">
	<g:select id="subType" name="subType.id" from="${com.kombos.finance.SubType.list()}" optionKey="id" required="" value="${journalDetailInstance?.subType?.id}" class="many-to-one"/>
	</div>
</div>

