
<%@ page import="com.kombos.finance.JournalDetail" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="journalDetail_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="journalDetail.accountNumber.label" default="Account Number" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="journalDetail.creditAmount.label" default="Credit Amount" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="journalDetail.debitAmount.label" default="Debit Amount" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="journalDetail.journal.label" default="Journal" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="journalDetail.journalTransactionType.label" default="Journal Transaction Type" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="journalDetail.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="journalDetail.subLedger.label" default="Sub Ledger" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="journalDetail.subType.label" default="Sub Type" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_accountNumber" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_accountNumber" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_creditAmount" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_creditAmount" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_debitAmount" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_debitAmount" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_journal" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_journal" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_journalTransactionType" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_journalTransactionType" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_subLedger" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_subLedger" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_subType" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_subType" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var journalDetailTable;
var reloadJournalDetailTable;
$(function(){
	
	reloadJournalDetailTable = function() {
		journalDetailTable.fnDraw();
	}

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	journalDetailTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	journalDetailTable = $('#journalDetail_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "accountNumber",
	"mDataProp": "accountNumber",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "creditAmount",
	"mDataProp": "creditAmount",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "debitAmount",
	"mDataProp": "debitAmount",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "journal",
	"mDataProp": "journal",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "journalTransactionType",
	"mDataProp": "journalTransactionType",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "subLedger",
	"mDataProp": "subLedger",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "subType",
	"mDataProp": "subType",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var accountNumber = $('#filter_accountNumber input').val();
						if(accountNumber){
							aoData.push(
									{"name": 'sCriteria_accountNumber', "value": accountNumber}
							);
						}
	
						var creditAmount = $('#filter_creditAmount input').val();
						if(creditAmount){
							aoData.push(
									{"name": 'sCriteria_creditAmount', "value": creditAmount}
							);
						}
	
						var debitAmount = $('#filter_debitAmount input').val();
						if(debitAmount){
							aoData.push(
									{"name": 'sCriteria_debitAmount', "value": debitAmount}
							);
						}
	
						var journal = $('#filter_journal input').val();
						if(journal){
							aoData.push(
									{"name": 'sCriteria_journal', "value": journal}
							);
						}
	
						var journalTransactionType = $('#filter_journalTransactionType input').val();
						if(journalTransactionType){
							aoData.push(
									{"name": 'sCriteria_journalTransactionType', "value": journalTransactionType}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}
	
						var subLedger = $('#filter_subLedger input').val();
						if(subLedger){
							aoData.push(
									{"name": 'sCriteria_subLedger', "value": subLedger}
							);
						}
	
						var subType = $('#filter_subType input').val();
						if(subType){
							aoData.push(
									{"name": 'sCriteria_subType', "value": subType}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
