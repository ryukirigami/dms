

<%@ page import="com.kombos.finance.JournalDetail" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'journalDetail.label', default: 'JournalDetail')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteJournalDetail;

$(function(){ 
	deleteJournalDetail=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/journalDetail/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadJournalDetailTable();
   				expandTableLayout('journalDetail');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-journalDetail" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="journalDetail"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${journalDetailInstance?.accountNumber}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="accountNumber-label" class="property-label"><g:message
					code="journalDetail.accountNumber.label" default="Account Number" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="accountNumber-label">
						%{--<ba:editableValue
								bean="${journalDetailInstance}" field="accountNumber"
								url="${request.contextPath}/JournalDetail/updatefield" type="text"
								title="Enter accountNumber" onsuccess="reloadJournalDetailTable();" />--}%
							
								<g:link controller="accountNumber" action="show" id="${journalDetailInstance?.accountNumber?.id}">${journalDetailInstance?.accountNumber?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalDetailInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="journalDetail.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${journalDetailInstance}" field="createdBy"
								url="${request.contextPath}/JournalDetail/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadJournalDetailTable();" />--}%
							
								<g:fieldValue bean="${journalDetailInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalDetailInstance?.creditAmount}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="creditAmount-label" class="property-label"><g:message
					code="journalDetail.creditAmount.label" default="Credit Amount" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="creditAmount-label">
						%{--<ba:editableValue
								bean="${journalDetailInstance}" field="creditAmount"
								url="${request.contextPath}/JournalDetail/updatefield" type="text"
								title="Enter creditAmount" onsuccess="reloadJournalDetailTable();" />--}%
							
								<g:fieldValue bean="${journalDetailInstance}" field="creditAmount"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalDetailInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="journalDetail.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${journalDetailInstance}" field="dateCreated"
								url="${request.contextPath}/JournalDetail/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadJournalDetailTable();" />--}%
							
								<g:formatDate date="${journalDetailInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalDetailInstance?.debitAmount}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="debitAmount-label" class="property-label"><g:message
					code="journalDetail.debitAmount.label" default="Debit Amount" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="debitAmount-label">
						%{--<ba:editableValue
								bean="${journalDetailInstance}" field="debitAmount"
								url="${request.contextPath}/JournalDetail/updatefield" type="text"
								title="Enter debitAmount" onsuccess="reloadJournalDetailTable();" />--}%
							
								<g:fieldValue bean="${journalDetailInstance}" field="debitAmount"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalDetailInstance?.journal}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="journal-label" class="property-label"><g:message
					code="journalDetail.journal.label" default="Journal" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="journal-label">
						%{--<ba:editableValue
								bean="${journalDetailInstance}" field="journal"
								url="${request.contextPath}/JournalDetail/updatefield" type="text"
								title="Enter journal" onsuccess="reloadJournalDetailTable();" />--}%
							
								<g:link controller="journal" action="show" id="${journalDetailInstance?.journal?.id}">${journalDetailInstance?.journal?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalDetailInstance?.journalTransactionType}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="journalTransactionType-label" class="property-label"><g:message
					code="journalDetail.journalTransactionType.label" default="Journal Transaction Type" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="journalTransactionType-label">
						%{--<ba:editableValue
								bean="${journalDetailInstance}" field="journalTransactionType"
								url="${request.contextPath}/JournalDetail/updatefield" type="text"
								title="Enter journalTransactionType" onsuccess="reloadJournalDetailTable();" />--}%
							
								<g:fieldValue bean="${journalDetailInstance}" field="journalTransactionType"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalDetailInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="journalDetail.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${journalDetailInstance}" field="lastUpdProcess"
								url="${request.contextPath}/JournalDetail/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadJournalDetailTable();" />--}%
							
								<g:fieldValue bean="${journalDetailInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalDetailInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="journalDetail.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${journalDetailInstance}" field="lastUpdated"
								url="${request.contextPath}/JournalDetail/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadJournalDetailTable();" />--}%
							
								<g:formatDate date="${journalDetailInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalDetailInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="journalDetail.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${journalDetailInstance}" field="staDel"
								url="${request.contextPath}/JournalDetail/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadJournalDetailTable();" />--}%
							
								<g:fieldValue bean="${journalDetailInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalDetailInstance?.subLedger}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="subLedger-label" class="property-label"><g:message
					code="journalDetail.subLedger.label" default="Sub Ledger" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="subLedger-label">
						%{--<ba:editableValue
								bean="${journalDetailInstance}" field="subLedger"
								url="${request.contextPath}/JournalDetail/updatefield" type="text"
								title="Enter subLedger" onsuccess="reloadJournalDetailTable();" />--}%
							
								<g:fieldValue bean="${journalDetailInstance}" field="subLedger"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalDetailInstance?.subType}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="subType-label" class="property-label"><g:message
					code="journalDetail.subType.label" default="Sub Type" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="subType-label">
						%{--<ba:editableValue
								bean="${journalDetailInstance}" field="subType"
								url="${request.contextPath}/JournalDetail/updatefield" type="text"
								title="Enter subType" onsuccess="reloadJournalDetailTable();" />--}%
							
								<g:link controller="subType" action="show" id="${journalDetailInstance?.subType?.id}">${journalDetailInstance?.subType?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalDetailInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="journalDetail.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${journalDetailInstance}" field="updatedBy"
								url="${request.contextPath}/JournalDetail/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadJournalDetailTable();" />--}%
							
								<g:fieldValue bean="${journalDetailInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('journalDetail');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${journalDetailInstance?.id}"
					update="[success:'journalDetail-form',failure:'journalDetail-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteJournalDetail('${journalDetailInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
