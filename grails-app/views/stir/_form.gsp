<%@ page import="com.kombos.administrasi.Stir" %>





<div class="control-group fieldcontain ${hasErrors(bean: stirInstance, field: 'm103KodeStir', 'error')} required">
	<label class="control-label" for="m103KodeStir">
		<g:message code="stir.m103KodeStir.label" default="Kode Stir" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m103KodeStir" id="m103KodeStir" maxlength="2" required="" value="${stirInstance?.m103KodeStir}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stirInstance, field: 'm103NamaStir', 'error')} required">
	<label class="control-label" for="m103NamaStir">
		<g:message code="stir.m103NamaStir.label" default="Nama Stir" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m103NamaStir" maxlength="20" required="" value="${stirInstance?.m103NamaStir}"/>
	</div>
</div>

<g:javascript>
    document.getElementById('m103KodeStir').focus();
</g:javascript>