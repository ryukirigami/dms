
<%@ page import="com.kombos.administrasi.Stir" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="stir_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div>
                  <g:message code="stir.m103KodeStir.label" default="KODE STIR" />
                </div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div>
                  <g:message code="stir.m103NamaStir.label" default="NAMA STIR" />
                </div>
			</th>

                        
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m103KodeStir" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m103KodeStir" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m103NamaStir" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m103NamaStir" class="search_init" />
				</div>
			</th>
	

		</tr>
	</thead>
</table>

<g:javascript>
var StirTable;
var reloadStirTable;
$(function(){
	
	reloadStirTable = function() {
		StirTable.fnDraw();
	}

    var recordsstirperpage = [];
    var anStirSelected;
    var jmlRecStirPerPage=0;
    var id;



$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	StirTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	StirTable = $('#stir_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsStir = $("#stir_datatables tbody .row-select");
            var jmlStirCek = 0;
            var nRow;
            var idRec;
            rsStir.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsstirperpage[idRec]=="1"){
                    jmlStirCek = jmlStirCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsstirperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecStirPerPage = rsStir.length;
            if(jmlStirCek==jmlRecStirPerPage && jmlRecStirPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [



{
	"sName": "m103KodeStir",
	"mDataProp": "m103KodeStir",
	"aTargets": [1],
        "mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m103NamaStir",
	"mDataProp": "m103NamaStir",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m103ID = $('#filter_m103ID input').val();
						if(m103ID){
							aoData.push(
									{"name": 'sCriteria_m103ID', "value": m103ID}
							);
						}
	
						var m103KodeStir = $('#filter_m103KodeStir input').val();
						if(m103KodeStir){
							aoData.push(
									{"name": 'sCriteria_m103KodeStir', "value": m103KodeStir}
							);
						}
	
						var m103NamaStir = $('#filter_m103NamaStir input').val();
						if(m103NamaStir){
							aoData.push(
									{"name": 'sCriteria_m103NamaStir', "value": m103NamaStir}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
    $('.select-all').click(function(e) {
        $("#stir_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsstirperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsstirperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

	$('#stir_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsstirperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anStirSelected = StirTable.$('tr.row_selected');
            if(jmlRecStirPerPage == anStirSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsstirperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
