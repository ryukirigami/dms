
<%@ page import="com.kombos.administrasi.Operation; com.kombos.administrasi.NamaManPower; com.kombos.administrasi.ManPowerAbsensi;com.kombos.administrasi.Stall" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<title><g:message code="bPTeknisi.label" default="Body & Paint - Teknisi"/></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var closeAbsensi;
	var save;
	$(function(){
    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;

       }    
       return false;
	});

    
   loadForm = function(data, textStatus){
		$('#bPRevisiRateTeknisi-form').empty();
    	$('#bPRevisiRateTeknisi-form').append(data);
   	}

    shrinkTableLayout = function(){
    	$("#bPRevisiRateTeknisi-table").hide()
        $("#bPRevisiRateTeknisi-form").css("display","block");
   	}
            $('input:radio[name=aksi]').click(function(){

                if($('input[name="aksi"]:checked').val()==5){
                    $("#alasan1").prop('disabled', false);
                    $("#alasan2").prop('disabled', false);
                }else{
                    document.getElementById("alasan1").checked=false;
                    document.getElementById("alasan2").checked=false;
                    $("#alasan1").prop('disabled', true);
                    $("#alasan2").prop('disabled', true);
                }
             });

        save  = function() {
            var formTeknisi = $('#bPRevisiRateTeknisi-table').find('form');
   		    $.ajax({
                url:'${request.contextPath}/BPRevisiRateTeknisi/save',
                type: "POST", // Always use POST when deleting data
                data : formTeknisi.serialize(),
                success : function(data){
                    toastr.success('<div>Data berhasil disimpan.</div>');
                },
                error: function(xhr, textStatus, errorThrown) {
                    toastr.error('<div>Data gagal disimpan.</div>');
                }
        });

    };


});
    function cekNumber(evt){
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57)){
            if(charCode == 46){
                return true
            }
            return false;
        }
        return true;
    }

</g:javascript>


	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="bPTeknisi.label" default="Body & Paint - Revisi Rate Teknisi"/></span>
		<ul class="nav pull-right">

		</ul>
	</div>
	<div class="box">
		<div class="span12" id="bPRevisiRateTeknisi-table">
            <fieldset style="padding: 10px;">
                <form id="form-teknisi" style="padding: 10px;">
                <table style="padding: 10px;margin-left: 180px">
                    <g:hiddenField name="id" value="${id}" />
                    <tr>
                        <td style="width: 150px;">Nomor WO</td>
                        <td><g:textField name="noWo" readonly="true" value="${noWo}" /> </td>
                    </tr>
                    <tr>
                        <td>Nomor Polisi</td>
                        <td><g:textField name="nopol" readonly="true" value="${nopol}" /> </td>
                    </tr>
                    <tr>
                        <td>Model Kendaraan</td>
                        <td><g:textField name="model" readonly="true" value="${model}" /> </td>
                    </tr>
                    <tr>
                        <td>Nama Stall</td>
                        <td><g:select name="stall" from="${Stall.createCriteria().list() {eq("staDel","0")}}" optionValue="m022NamaStall" value="${stall}"/></td>
                    </tr>
                    <tr>
                        <td>Tanggal WO</td>
                        <td>
                            <g:textField name="tanggalWO" id="tanggalWO" value="${tanggalWO}" readonly="" />
                        </td>
                    </tr>
                    <tr>
                        <td>Job *</td>
                        <td>
                            <g:select name="job" from="${Operation.createCriteria().list() {eq("staDel","0")}}" optionValue="m053NamaOperation" value=""/>
                        </td>
                    </tr>
                    <tr>
                        <td>Proses</td>
                        <td>
                            <g:textField name="proses" id="proses" readonly="true" value="${proses}" />
                        </td>
                    </tr>
                    <tr>
                        <td>Inisial dan Nama Teknisi *</td>
                        <td>
                            <g:select name="namaTeknisi" from="${NamaManPower.createCriteria().list() {eq("staDel","0")}}" optionValue="${{it?.manPowerDetail?.m015Inisial+" - "+it.t015NamaLengkap}}" value=""/>
                        </td>
                    </tr>
                    <tr>
                        <td>Rate Teknisi</td>
                        <td>
                            <g:textField name="rateTeknisi" id="rateTeknisi" onkeypress="return cekNumber(event);" value="${rate}" />
                        </td>
                    </tr>
                    <tr>
                        <td>Tanggal Revisi</td>
                        <td>
                            <g:textField name="tanggalRevisi" id="tanggalRevisi" readonly="" value="${tanggalRevisi}" />
                        </td>
                    </tr>
                </table>
                <g:hiddenField name="id" id="id" value="${id}" />
               </form>
            </fieldset>
            <fieldset class="buttons controls" style="padding-top: 10px;margin-left: 280px">
                <button id='btn' onclick="save();" style="width: 100px" type="button" class="btn btn-primary">Ok</button>
                <button id='btn2' style="width: 100px" onclick="" type="button" class="btn cancel">Close</button>
            </fieldset>
		</div>
		<div class="span7" id="bPRevisiRateTeknisi-form" style="display: none;"></div>

	</div>
</body>
</html>
