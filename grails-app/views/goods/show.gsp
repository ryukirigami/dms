

<%@ page import="com.kombos.parts.Goods" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'goods.label', default: 'Master Goods')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteGoods;

$(function(){ 
	deleteGoods=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/goods/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadGoodsTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-goods" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="goods"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${goodsInstance?.m111ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m111ID-label" class="property-label"><g:message
					code="goods.m111ID.label" default="Kode Goods" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m111ID-label">
						%{--<ba:editableValue
								bean="${goodsInstance}" field="m111ID"
								url="${request.contextPath}/Goods/updatefield" type="text"
								title="Enter m111ID" onsuccess="reloadGoodsTable();" />--}%
							
								<g:fieldValue bean="${goodsInstance}" field="m111ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${goodsInstance?.m111Nama}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m111Nama-label" class="property-label"><g:message
					code="goods.m111Nama.label" default="Nama Goods" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m111Nama-label">
						%{--<ba:editableValue
								bean="${goodsInstance}" field="m111Nama"
								url="${request.contextPath}/Goods/updatefield" type="text"
								title="Enter m111Nama" onsuccess="reloadGoodsTable();" />--}%
							
								<g:fieldValue bean="${goodsInstance}" field="m111Nama"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${goodsInstance?.satuan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="satuan-label" class="property-label"><g:message
					code="goods.satuan.label" default="Satuan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="satuan-label">
						%{--<ba:editableValue
								bean="${goodsInstance}" field="satuan"
								url="${request.contextPath}/Goods/updatefield" type="text"
								title="Enter satuan" onsuccess="reloadGoodsTable();" />--}%
                            <g:fieldValue bean="${goodsInstance?.satuan}" field="m118Satuan1"/>
						</span></td>
					
				</tr>
				</g:if>

				<g:if test="${goodsInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="goods.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${goodsInstance}" field="createdBy"
								url="${request.contextPath}/Goods/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadGoodsTable();" />--}%
							
								<g:fieldValue bean="${goodsInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${goodsInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="goods.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${goodsInstance}" field="updatedBy"
								url="${request.contextPath}/Goods/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadGoodsTable();" />--}%
							
								<g:fieldValue bean="${goodsInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${goodsInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="goods.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${goodsInstance}" field="lastUpdProcess"
								url="${request.contextPath}/Goods/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadGoodsTable();" />--}%
							
								<g:fieldValue bean="${goodsInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${goodsInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="goods.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${goodsInstance}" field="dateCreated"
								url="${request.contextPath}/Goods/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadGoodsTable();" />--}%
							
								<g:formatDate date="${goodsInstance?.dateCreated}" format="EEEE, dd MMMM yyyy HH:mm" />

						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${goodsInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="goods.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${goodsInstance}" field="lastUpdated"
								url="${request.contextPath}/Goods/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadGoodsTable();" />--}%
							
								<g:formatDate date="${goodsInstance?.lastUpdated}" format="EEEE, dd MMMM yyyy HH:mm"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${goodsInstance?.id}"
					update="[success:'goods-form',failure:'goods-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteGoods('${goodsInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
