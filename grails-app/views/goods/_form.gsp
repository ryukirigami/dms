<%@ page import="com.kombos.parts.Satuan; com.kombos.parts.Goods" %>



<div class="control-group fieldcontain ${hasErrors(bean: goodsInstance, field: 'm111ID', 'error')} required">
	<label class="control-label" for="m111ID">
		<g:message code="goods.m111ID.label" default="Kode Goods" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m111ID" maxlength="50" id="m111ID" required="" value="${goodsInstance?.m111ID}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: goodsInstance, field: 'm111Nama', 'error')} required">
	<label class="control-label" for="m111Nama">
		<g:message code="goods.m111Nama.label" default="Nama Goods" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m111Nama" maxlength="50" required="" value="${goodsInstance?.m111Nama}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: goodsInstance, field: 'satuan', 'error')} required">
	<label class="control-label" for="satuan">
		<g:message code="goods.satuan.label" default="Satuan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="satuan" name="satuan.id" from="${Satuan.createCriteria().list(){eq("staDel", "0")}}" optionKey="id" required="" value="${goodsInstance?.satuan?.id}" class="many-to-one"/>
	</div>
</div>
<g:javascript>
    document.getElementById("m111ID").focus();
</g:javascript>