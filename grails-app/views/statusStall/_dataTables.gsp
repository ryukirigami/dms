
<%@ page import="com.kombos.administrasi.StatusStall" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="statusStall_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="statusStall.stall.label" default="Nama Stall" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="statusStall.m025Tmt.label" default="Tgl Berlaku" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="statusStall.m025Sta.label" default="Status" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_stall" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_stall" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m025Tmt" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m025Tmt" value="date.struct">
					<input type="hidden" name="search_m025Tmt_day" id="search_m025Tmt_day" value="">
					<input type="hidden" name="search_m025Tmt_month" id="search_m025Tmt_month" value="">
					<input type="hidden" name="search_m025Tmt_year" id="search_m025Tmt_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m025Tmt_dp" value="" id="search_m025Tmt" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m025Sta" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					
					<select name="search_m025Sta" id="search_m025Sta" style="width:100%">
                                    <option value=""></option>
                                    <option value="0">Stall Booking Telepon</option>
                                    <option value="1">Stall Booking Web</option>

                             </select>
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var statusStallTable;
var reloadStatusStallTable;
$(function(){
	
	reloadStatusStallTable = function() {
		statusStallTable.fnDraw();
	}

    $('#search_m025Sta').change(function(){
        statusStallTable.fnDraw();
    });
    var recordsStatusStallPerPage = [];
    var anStatusStallSelected;
    var jmlRecStatusStallPerPage=0;
    var id;
	
	$('#search_m025Tmt').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m025Tmt_day').val(newDate.getDate());
			$('#search_m025Tmt_month').val(newDate.getMonth()+1);
			$('#search_m025Tmt_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			statusStallTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	statusStallTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	statusStallTable = $('#statusStall_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsStatusStall = $("#statusStall_datatables tbody .row-select");
            var jmlStatusStallCek = 0;
            var nRow;
            var idRec;
            rsStatusStall.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsStatusStallPerPage[idRec]=="1"){
                    jmlStatusStallCek = jmlStatusStallCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsStatusStallPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecStatusStallPerPage = rsStatusStall.length;
            if(jmlStatusStallCek==jmlRecStatusStallPerPage && jmlRecStatusStallPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "stall",
	"mDataProp": "stall",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m025Tmt",
	"mDataProp": "m025Tmt",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m025Sta",
	"mDataProp": "m025Sta",
	"aTargets": [2],
	"mRender": function ( data, type, row ) {
          
            if(data=="0"){
                  return "Stall Booking Telepon";
             }else
             {
                  return "Stall Booking Web";
             }
        
        },
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var stall = $('#filter_stall input').val();
						if(stall){
							aoData.push(
									{"name": 'sCriteria_stall', "value": stall}
							);
						}

						var m025Tmt = $('#search_m025Tmt').val();
						var m025TmtDay = $('#search_m025Tmt_day').val();
						var m025TmtMonth = $('#search_m025Tmt_month').val();
						var m025TmtYear = $('#search_m025Tmt_year').val();
						
						if(m025Tmt){
							aoData.push(
									{"name": 'sCriteria_m025Tmt', "value": "date.struct"},
									{"name": 'sCriteria_m025Tmt_dp', "value": m025Tmt},
									{"name": 'sCriteria_m025Tmt_day', "value": m025TmtDay},
									{"name": 'sCriteria_m025Tmt_month', "value": m025TmtMonth},
									{"name": 'sCriteria_m025Tmt_year', "value": m025TmtYear}
							);
						}
	
						var m025Sta = $('#filter_m025Sta select').val();
						if(m025Sta){
							aoData.push(
									{"name": 'sCriteria_m025Sta', "value": m025Sta}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#statusStall_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsStatusStallPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsStatusStallPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#statusStall_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsStatusStallPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anStatusStallSelected = statusStallTable.$('tr.row_selected');

            if(jmlRecStatusStallPerPage == anStatusStallSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsStatusStallPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
