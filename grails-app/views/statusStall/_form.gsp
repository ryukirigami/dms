<%@ page import="com.kombos.administrasi.Stall; com.kombos.administrasi.StatusStall" %>



<div class="control-group fieldcontain ${hasErrors(bean: statusStallInstance, field: 'stall', 'error')} required">
	<label class="control-label" for="stall">
		<g:message code="statusStall.stall.label" default="Nama Stall" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="stall" name="stall.id" from="${Stall.createCriteria().list{eq("staDel", "0");order("m022NamaStall", "asc")}}" optionKey="id" required="" value="${statusStallInstance?.stall?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: statusStallInstance, field: 'm025Tmt', 'error')} required">
	<label class="control-label" for="m025Tmt">
		<g:message code="statusStall.m025Tmt.label" default="Tgl berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<ba:datePicker  name="m025Tmt" precision="day"  value="${statusStallInstance?.m025Tmt}"   format="dd/mm/yyyy" required="true" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: statusStallInstance, field: 'm025Sta', 'error')} required">
	<label class="control-label" for="m025Sta">
		<g:message code="statusStall.m025Sta.label" default="Status" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	 
	 <g:radioGroup name="m025Sta" values="['0','1']" labels="['Stall Booking Telepon','Stall Booking Web']" value="${statusStallInstance?.m025Sta}" required="" >
            ${it.radio} ${it.label}
        </g:radioGroup>
	</div>
</div>

