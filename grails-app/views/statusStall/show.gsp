

<%@ page import="com.kombos.administrasi.StatusStall" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'statusStall.label', default: 'Status Stall')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteStatusStall;

$(function(){ 
	deleteStatusStall=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/statusStall/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadStatusStallTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-statusStall" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="statusStall"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${statusStallInstance?.stall}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="stall-label" class="property-label"><g:message
					code="statusStall.stall.label" default="Nama Stall" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="stall-label">
						%{--<ba:editableValue
								bean="${statusStallInstance}" field="stall"
								url="${request.contextPath}/StatusStall/updatefield" type="text"
								title="Enter stall" onsuccess="reloadStatusStallTable();" />--}%
							
                        <g:fieldValue bean="${statusStallInstance?.stall}" field="m022NamaStall"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${statusStallInstance?.m025Tmt}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m025Tmt-label" class="property-label"><g:message
					code="statusStall.m025Tmt.label" default="Tgl Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m025Tmt-label">
						%{--<ba:editableValue
								bean="${statusStallInstance}" field="m025Tmt"
								url="${request.contextPath}/StatusStall/updatefield" type="text"
								title="Enter m025Tmt" onsuccess="reloadStatusStallTable();" />--}%
							
								<g:formatDate date="${statusStallInstance?.m025Tmt}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${statusStallInstance?.m025Sta}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m025Sta-label" class="property-label"><g:message
					code="statusStall.m025Sta.label" default="Status" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m025Sta-label">
						%{--<ba:editableValue
								bean="${statusStallInstance}" field="m025Sta"
								url="${request.contextPath}/StatusStall/updatefield" type="text"
								title="Enter m025Sta" onsuccess="reloadStatusStallTable();" />--}%
							
								 
							<g:if test="${statusStallInstance.m025Sta == '0'}">
			                        <g:message code="statusStallInstance.m025Sta.label" default="Stall Booking Telepon"/>
			                    </g:if>
			                    <g:else>
			                        <g:message code="statusStallInstance.m025Sta.label" default="Stall Booking Web"/>
			                    </g:else>		
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${statusStallInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="perhitunganNerxtService.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${statusStallInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/perhitunganNerxtService/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadperhitunganNerxtServiceTable();" />--}%

                        <g:fieldValue bean="${statusStallInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${statusStallInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="jenisStall.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${statusStallInstance}" field="dateCreated"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadJenisStallTable();" />--}%

                        <g:formatDate date="${statusStallInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>


			<g:if test="${statusStallInstance?.createdBy}">
            <tr>
                <td class="span2" style="text-align: right;"><span
                        id="createdBy-label" class="property-label"><g:message
                            code="perhitunganNerxtService.createdBy.label" default="Created By" />:</span></td>

                <td class="span3"><span class="property-value"
                                        aria-labelledby="createdBy-label">
                    %{--<ba:editableValue
                            bean="${statusStallInstance}" field="createdBy"
                            url="${request.contextPath}/perhitunganNerxtService/updatefield" type="text"
                            title="Enter createdBy" onsuccess="reloadperhitunganNerxtServiceTable();" />--}%

                    <g:fieldValue bean="${statusStallInstance}" field="createdBy"/>

                </span></td>

            </tr>
        </g:if>

        <g:if test="${statusStallInstance?.lastUpdated}">
            <tr>
                <td class="span2" style="text-align: right;"><span
                        id="lastUpdated-label" class="property-label"><g:message
                            code="perhitunganNerxtService.lastUpdated.label" default="Last Updated" />:</span></td>

                <td class="span3"><span class="property-value"
                                        aria-labelledby="lastUpdated-label">
                    %{--<ba:editableValue
                            bean="${statusStallInstance}" field="lastUpdated"
                            url="${request.contextPath}/perhitunganNerxtService/updatefield" type="text"
                            title="Enter lastUpdated" onsuccess="reloadperhitunganNerxtServiceTable();" />--}%

                    <g:formatDate date="${statusStallInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />

                </span></td>

            </tr>
        </g:if>

        <g:if test="${statusStallInstance?.updatedBy}">
            <tr>
                <td class="span2" style="text-align: right;"><span
                        id="updatedBy-label" class="property-label"><g:message
                            code="perhitunganNerxtService.updatedBy.label" default="Updated By" />:</span></td>

                <td class="span3"><span class="property-value"
                                        aria-labelledby="updatedBy-label">
                    %{--<ba:editableValue
                            bean="${statusStallInstance}" field="updatedBy"
                            url="${request.contextPath}/perhitunganNerxtService/updatefield" type="text"
                            title="Enter updatedBy" onsuccess="reloadperhitunganNerxtServiceTable();" />--}%

                    <g:fieldValue bean="${statusStallInstance}" field="updatedBy"/>
				
								
                </span></td>

            </tr>
        </g:if>

			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${statusStallInstance?.id}"
					update="[success:'statusStall-form',failure:'statusStall-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteStatusStall('${statusStallInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
