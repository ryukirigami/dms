<g:javascript>
	function openInputPart(){
        var jum=0;
        var idJobPart ;
        $("#job_order_datatables tbody .row-select").each(function() {
            if(this.checked){
                idJobPart = $(this).next("input:hidden").val();
                jum++;
            }
        });
        if(jum==0){
            alert('Pilih satu job yang akan ditambahkan part');
            return;
        }
        if(jum>1){
            alert('Anda hanya dapat memilih satu job');
            return;
        }
        clearPart();
        setIdJobPart(idJobPart);
        closeDialog('dialog-reception-joborder');
		openDialog('dialog-reception-inputparts');
	}
	
	function doOpenRequestPart(){
		closeDialog('dialog-reception-joborder');
		openRequestPart();
	}
</g:javascript>
<div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Reception - Job Order</h3>
</div>
<div class="modal-body">
	<div class="box">
		<input type="button" class="btn cancel" onclick="openInputPart();" value="Input Parts"/>
		<g:render template="jobnPartsDataTablesJobOrder" />
		<table class="display table" width="100%" cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px; width: 930px;">
			<tr>
				<td>
					<input type="button" class="btn cancel" onclick="selectAllCheckbox('idKeluhan');" value="Select All"/>
					<input type="button" class="btn cancel" onclick="unselectAllCheckbox('idKeluhan');" value="Unselect All"/>
					<input type="button" class="btn cancel" onclick="deleteCheckbox('idKeluhan');" value="Delete"/>
				</td>
			</tr>
		</table>
	</div>
</div>
<div class="modal-footer">
    <a onclick="doOpenRequestPart();" class="btn btn-success">Request Parts</a>
</div>