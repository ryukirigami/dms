<%@ page import="com.kombos.administrasi.BaseModel" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="reception.editJobParts.label" default="Edit Job dan Parts"/></title>
    <r:require modules="baseapplayout"/>
    <g:javascript disposition="head">
	    var newAppointment;
	    var namaPemilik = "";
        var alamatPemilik = "";
        var telpPemilik = "";
        var mobilPemilik = "";
        var tglBayarBooking = "";
        var tgljanjiDatang = "";
		var openCancelEditBookingFee;
        var isCharOnly;
        var isNumberKey;
        var dataCustomer;
		var savePartApp;
		var gatePass;
		var idVincode = '-1';
		var idJobTempPart='-1';
		var selectAllCheckbox;
        var unselectAllCheckbox;
        var deleteCheckbox;
		var showReception;
		var openRequestPart;
		var openEstimasi;
		var billToAndDiscount;
		var openInputJobDanKeluhan;
		var showAppSummary;
		var showPrediagnose;
		var showApproval;
		var staValidate = 0;
		var openCustomer;
		var jpbBp;
        var jpbGr;
        var showAproval;
        var showAprovalRateJob;
        var showAprovalDiscount;
        var showKurangiJob;
        var deleteJobPart;
        var listJobKurangi = [];
        var listPartKurangi = [];
        var listJobDelete = [];
        var listPartDelete = [];
        var parNoWo = "${noWO}";
        var idRec = 0;
		$(function(){
            hideUnusedButton();
		    if(parNoWo){
		        cekJob = [];
                $.ajax({url: '${request.contextPath}/editJobParts/getReception',
                    type: "POST",
                    data: {noWo : parNoWo},
                    success: function(data) {
                        var kosong = false;
                        if(data[0].hasil=="ada"){
                            idVincode=data[0].idCV;
                            staValidate = 1;
                            document.getElementById('lblStaBooking').innerHTML = data[0].staBooking
                            $('#txtnopolDiscount').val(data[0].fullnopol);
                            $('#txtmodelDiscount').val(data[0].mobil);
                            $('#lblPemilik').html(data[0].pemilik);
                            $('#lblKendaraan').html(data[0].kendaraan);
                            $('#lblVincode').html(data[0].vincode);
                            $('#lblSA').html(data[0].namaSA);
                            $('#lblJenisWo').html(data[0].jenisWo);
                            $('#lblTeknisi').html(data[0].teknisi);
                            if(data[0].idReception){
                                $('#receptionId').val(data[0].idReception);
                            }
                            if(data[0].tgglReception){
                                document.getElementById('lblTgglReception').innerHTML = data[0].tgglReception
                            }
                            if(data[0].noWO){
                                document.getElementById('lblNoWO').innerHTML = data[0].noWO
                            }
                            if(data[0].kmSekarang ){
                                $('#i_km').val(data[0].kmSekarang)
                            }
                            if(data[0].keluhan.length>0 ){
                                var keluhan = data[0].keluhan;
                                countKeluhan = 1;
                                for(var i = 0;i< keluhan.length ; i++){
                                    var yaTidak = (keluhan[i][1]=='1' ? 'Ya' : 'Tidak');
                                    var newRow = "<tr><td><input id='countKeluhan"+countKeluhan+"' type='hidden' value='"+countKeluhan+"'/><input id='keluhan"+countKeluhan+"' type='hidden' value='"+keluhan[i][0]+"'/><input id='staDiagnose"+countKeluhan+"' type='hidden' value='"+keluhan[i][1]+"'/><input type='checkbox' class='idKeluhan' />"+countKeluhan+"</td><td>"+keluhan[i][0]+"</td><td>"+yaTidak+"</td></tr>";
			                        $('#tableKeluhan > tbody:last').append(newRow);
			                        countKeluhan++;
                                }
                                $('#i_km').val(data[0].kmSekarang)
                            }
                            if(data[0].kategori ){
                                $('#kategoriJob').val(data[0].kategori)
                            }
                            if(data[0].statusWarranty ){
                                $('#staWarranty').val(data[0].statusWarranty)
                            }
                            $('#kodeKota').val(data[0].kodeKota);
                            $('#nomorTengah').val(data[0].tengah);
                            $('#nomorBelakang').val(data[0].belakang);
                            reloadjobnPartsTable();
                            if(data[0].staInvoice){
                                hideUnusedButton();
                                $("#btnSummary").show();
                            }else{
                                showAllButton();
                            }
                        }else if(data[0].hasil=="nothing"){
                            alert('Data dengan nomor polisi tersebut tidak ditemukan');
                            $('#lbltotalsebelumdiscount').text('');
                            $('#lbltotaljasa').text('');
                            $('#lbltotalparts').text('');
                            $('#lbldiscountjasa').text('');
                            $('#lbldiscountpart').text('');
                            $('#lblstadiscountjasa').text('');
                            $('#lblstadiscountpart').text('');
                            $('#lbltotalsetelahdiscount').text('');
                            $('#lblpersendiscountjasa').text('')
                            $('#lblpersendiscountpart').text('')
                            staValidate = 0;
                            $('#receptionId').val('-1');
                            document.getElementById('lblTgglReception').innerHTML="";
                            document.getElementById('lblNoWO').innerHTML = "";
                            document.getElementById('lblStaBooking').innerHTML = "";
                            $('#lblPemilik').html('');
                            $('#lblKendaraan').html('');
                            $('#lblVincode').html('');
                            $('#lblSA').html('');
                            $('#lblJenisWo').html('');
                            $('#lblTeknisi').html('');
                            $('#txtnopolDiscount').val('');
                            $('#txtmodelDiscount').val('');
                            $('#i_km').val('');
                            $('#kategoriJob').val('');
                            $('#staWarranty').val('');
                            $('#tableKeluhan').find("tr:gt(0)").remove();
                            countKeluhan=1;
                            reloadjobnPartsTable();
                        }
                    },
                    error: function (data, status, e){
                        alert(e);
                    }
                });
		    }

		    jpbGr = function(){
                if(staValidate==0){
                    alert('Validasi terlebih dahulu nomor polisi dengan menekan tombol OK');
                    return
                }
                var kodeKota=$('#kodeKota').val();
                var noPolTengah=$('#nomorTengah').val();
                var noPolBelakang=$('#nomorBelakang').val();
                var noWo = $('#lblNoWO').text();
                window.location.herf = '#/inputJPBGR';
                $.ajax({
                        url:'${request.contextPath}/JPBGRInput/list?staReception=1&noWo='+noWo,
                         type:"GET",dataType: "html",
                         complete : function (req, err) {
                            $('#main-content').html(req.responseText);
                            $('#spinner').fadeOut();
                            loading = false;
                         }
                    });
            }
            jpbBp = function(){
                if(staValidate==0){
                    alert('Validasi terlebih dahulu nomor polisi dengan menekan tombol OK');
                    return
                }
                var kodeKota=$('#kodeKota').val();
                var noPolTengah=$('#nomorTengah').val();
                var noPolBelakang=$('#nomorBelakang').val();
                var noWo = $('#lblNoWO').text();
                window.location.herf = '#/InputJpBBp';
                $.ajax({
                        url:'${request.contextPath}/JPBBPInput/list?staReception=1&noWo='+noWo,
                         type:"GET",dataType: "html",
                         complete : function (req, err) {
                            $('#main-content').html(req.responseText);
                            $('#spinner').fadeOut();
                            loading = false;
                         }
                    });
            }

            savePartApp = function() {
                if(staValidate==0){
                    alert('Validasi terlebih dahulu nomor polisi dengan menekan tombol OK');
                    return
                }
                var partparams = {};
                partparams['idReception'] =  $('#receptionId').val();
                partparams['idJob'] =  idJobTempPart;

		        var checkPart =[];
                var id = 1;
                $("#inputPart_datatables tbody .row-select").each(function() {
                    if(this.checked){
                        var nRow = $(this).parents('tr')[0];
                        var aData = inputPartTable.fnGetData(nRow);
                        partparams['good' + id] = aData['id'];
                        partparams['prosesBP' + id] = $('#prosesBP' + aData['id']).val();
                        partparams['qty' + id] = $('#qty' + aData['id']).val();
                        checkPart.push(id);
                       id++;
                    }
                });

                partparams['countRowPart'] = id;
                partparams['staTambah'] = '0';
                $.ajax({url: '${request.contextPath}/editJobParts/savePartInput',
					type: "POST",
					data: partparams,
					success: function(data) {
				        toastr.success('Data parts berhasil ditambahkan');
				        $('#appointmentInputPartModal').modal('hide');
				        reloadjobnPartsTable();
					},
					complete : function (req, err) {
						$('#spinner').fadeOut();
						loading = false;
					}
				});
		    }

            showAppSummary = function() {
                if(staValidate==0){
                    alert('Validasi terlebih dahulu nomor polisi dengan menekan tombol OK');
                    return
                }
                $("#summaryNamaCustomer").text("");
                $("#summaryAlamat").text("");
                $("#summaryTelepon").text("");
                $("#summaryMobil").text("");
                $("#summaryNamaCustomer").text(namaPemilik);
                $("#summaryAlamat").text(alamatPemilik);
                $("#summaryTelepon").text(telpPemilik);
                $("#summaryMobil").text(mobilPemilik);
                $("#tglBayarBooking").text(tglBayarBooking);
                $("#tgljanjiDatang").text(tgljanjiDatang);
                var noWo = $('#lblNoWO').text();
                $('#noWo').val(noWo);
                initSummary(noWo);
                openDialog('dialog-reception-summary');
             }

            showAproval = function() {
                if(staValidate==0){
                    alert('Validasi terlebih dahulu nomor polisi dengan menekan tombol OK');
                    return
                }
                var noWo = $('#lblNoWO').text();
                $('#noWoApproval').val(noWo);
                openDialog('dialog-reception-permohonan-approval');
            }

            showAprovalRateJob = function() {
                if(staValidate==0){
                    alert('Validasi terlebih dahulu nomor polisi dengan menekan tombol OK');
                    return
                }
                var jobTemp=0;
                var length=0;
                var indChecked = 0;
                $("#job_n_parts_datatables tbody .row-select").each(function() {
                    var data = $(this).next("input:hidden").val();
                    if(data.indexOf("*")<0){
                        indChecked++;
                        if(this.checked){
                            length++;
                            jobTemp = indChecked - 1;
                        }
                    }
                });
                if(length==0){
                    alert('Pilih satu job yang akan diubah rate nya');
                    return;
                }
                if(length>1){
                    alert('Anda hanya dapat memilih satu job');
                    return;
                }
                $('#txtrate').val();
                $('#rateBefore').val();
                $('#idJobRate').val();
                var aData = jobnPartsTable.fnGetData(parseInt(jobTemp));
                document.getElementById('lbljobparts').innerHTML=aData.namaJob
                $('#txtrate').val(aData.rate);
                $('#rateBefore').val(aData.rate);
                $('#idJobRate').val(aData.idJob);
                var noWo = $('#lblNoWO').text();
                $('#noWoApproval').val(noWo);
                openDialog('dialog-reception-permohonan-approval-ratejob');
            }

            showAprovalDiscount = function() {
                if(staValidate==0){
                    alert('Validasi terlebih dahulu nomor polisi dengan menekan tombol OK');
                    return
                }
                var noWo = $('#lblNoWO').text();
                $('#noWoApprovalDiscount').val(noWo);
                openDialog('dialog-reception-permohonan-approval-discount');
            }
            deleteJobPart = function() {
                listJobDelete = [];
                listPartDelete = [];
                if(staValidate==0){
                    alert('Validasi terlebih dahulu nomor polisi dengan menekan tombol OK');
                    return
                }
                 $("#job_n_parts_datatables tbody .row-select").each(function() {
                    if(this.checked){
                        length++;
                        var data = $(this).next("input:hidden").val();
                        if(data.indexOf("*")<0){
                            listJobDelete.push(data);
                        }else{
                            data = data.substring(0,data.length-1);
                            listPartDelete.push(data);
                        }
                    }
                });
                if(length==0){
                    alert('Pilih job/part yang akan dikurangi');
                    return;
                }
                var conf = confirm("Apakah Anda yakin?");
                if(conf){
                    $.ajax({type:'POST',
                        data : {listJobDelete : JSON.stringify(listJobDelete),listPartDelete : JSON.stringify(listPartDelete)},
                        url:'${request.contextPath}/editJobParts/deleteJobPart',
                        success:function(data,textStatus){
                        if(data=="ok"){
                            toastr.success("Success Delete data");
                          }else{
                            alert("DATA TIDAK BISA DIHAPUS..!!");
                          }
                           retrieveData();
                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                               retrieveData();
                        }
                    });
                }
            }
            showKurangiJob = function() {
                listJobKurangi = [];
                listPartKurangi = [];
                if(staValidate==0){
                    alert('Validasi terlebih dahulu nomor polisi dengan menekan tombol OK');
                    return
                }
                var length=0;
                $("#job_n_parts_datatables tbody .row-select").each(function() {
                    if(this.checked){
                        length++;
                        var data = $(this).next("input:hidden").val();
                        if(data.indexOf("*")<0){
                            listJobKurangi.push(data);
                        }else{
                            listPartKurangi.push(data);
                        }
                    }
                });
                if(length==0){
                    alert('Pilih job/part yang akan dikurangi');
                    return;
                }
                var noWo = $('#lblNoWO').text();
                $('#noWoPengurangan').val(noWo);
                openDialog('dialog-reception-permohonan-approval-pengurangan');
            }

            openRequestPart = function(){
			        $("#requestPartContent").empty();
                    $.ajax({type:'POST',
                        url:'${request.contextPath}/editJobParts/requestPart',
                        success:function(data,textStatus){
                                $("#requestPartContent").html(data);
                                $("#receptionRequestPartModal").modal({
                                    "backdrop" : "static",
                                    "keyboard" : true,
                                    "show" : true
                                }).css({'width': '1200px','margin-left': function () {return -($(this).width() / 2);}});

                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });

            }

            billToAndDiscount = function(){
                if(staValidate==0){
                    alert('Validasi terlebih dahulu nomor polisi dengan menekan tombol OK');
                    return
                }
                $('#spinner').fadeIn(1);
                var noWo = $('#lblNoWO').text();
                loadPath('editJobParts/billtoanddiscount?noWo='+noWo);
            };

            $("#nomorBelakang").bind('keypress', function(e) {
                var code = (e.keyCode ? e.keyCode : e.which);
                if(code == 13) {
                    e.preventDefault();
                    e.stopPropagation();
                    retrieveData();
                }
            });

            $("#noWoCari,#nomorBelakang,#nomorTengah").bind('keypress', function(e) {
                var code = (e.keyCode ? e.keyCode : e.which);
                if(code == 13) {
                    e.preventDefault();
                    e.stopPropagation();
                    retrieveData();
                }
            });



        });

    function printWoGr(){
        window.location = "${request.contextPath}/reception/printWoGr?id="+$('#receptionId').val();
    }

    function printWoBp(){
        window.location = "${request.contextPath}/reception/printWoBp?id="+$('#receptionId').val();
    }

    function openDialog(divModal){
        var depan = $('#kodeKota').val();
        var tengah=$('#nomorTengah').val();
        var belakang=$('#nomorBelakang').val();
        if(divModal=="dialog-reception-jobkeluhan"){
            if(depan=="" || tengah=="" || belakang==""){
                alert('Data nomor polisi belum lengkap')
                return
            }
            if(staValidate==0){
                alert('Validasi terlebih dahulu nomor polisi dengan menekan tombol OK');
                return
            }
            if(idVincode=="-1"){
                alert('Nomor polisi tidak dikenal');
                return
            }
            openJobKeluhan();
        }else if(divModal=="dialog-reception-parts"){
            var length=0;
            var idJobTemp;
            $("#job_n_parts_datatables tbody .row-select").each(function() {
                if(this.checked){
                    length++;
                    idJobTemp = $(this).next("input:hidden").val();
                }
            });
            if(length==0){
                alert('Pilih satu job yang akan ditambahkan part');
                return;
            }
            if(length>1){
                alert('Anda hanya dapat memilih satu job');
                return;
            }
            idJobTempPart = idJobTemp;
        }
        else if(divModal=="dialog-reception-prediagnose"){
            var km = $('#i_km').val()
	        if(depan=="" || tengah=="" || belakang==""){
                alert('Data nomor polisi belum lengkap')
                return
            }
            if(km==""){
                alert('Masukan KM sekarang terlebih dahulu');
                return
            }

        }
        $( "#"+divModal ).modal("show");
    }

    function closeDialog(divModal){
        $( "#"+divModal ).modal("hide");
    }

    function hideUnusedButton(){
        $("#btnTambahJob").hide();
        $("#btnTambahPart").hide();
        $("#btnPengurangan").hide();
        $("#btnEditRate").hide();
        $("#btnBillTo").hide();
        $("#btnRequest").hide();
        $("#btnJpbGR").hide();
        $("#btnJpbBP").hide();
        $("#btnSummary").hide();
        $("#btnCancel").hide();
        $("#changePemilik").hide();
        $("#changeJenis").hide();
        $("#changeSA").hide();
        $("#changeTeknisi").hide();
        $("#btnDeleteJobPart").hide();
        // $("#btnPrintWoBp").hide();
    }

    function showAllButton(){
        $("#btnTambahJob").show();
        $("#btnTambahPart").show();
        $("#btnPengurangan").show();
        $("#btnEditRate").show();
        $("#btnBillTo").show();
        $("#btnRequest").show();
        $("#btnJpbGR").show();
        $("#btnJpbBP").show();
        $("#btnSummary").show();
        $("#btnCancel").show();
        $("#btnPrintWoGr").show();
        $("#btnPrintWoBp").show();
        $("#changePemilik").show();
        $("#changeJenis").show();
        $("#changeSA").show();
        $("#changeTeknisi").show();
        $("#btnDeleteJobPart").show();
    }

    function retrieveData(){

            cekJob = [];
            var noDepan = $('#kodeKota').val();
            var noTengah = $('#nomorTengah').val();
            var noBelakang = $('#nomorBelakang').val();
            var key = $('#noWoCari').val();
            var kategori = $('#kategori').val();
            var isNull = false;
            if(kategori=="Nomor WO"){
                if(key==""){
                    isNull=true;
                }
            }else{
                if(noDepan=="" && noTengah=="" && noBelakang==""){
                    isNull=true;
                }
            }
            if(isNull){
                alert("Data Belum Lengkap");
                return
            }else{
                toastr.info("Harap Tunggu Sebentar");
                hideUnusedButton();
                $.ajax({url: '${request.contextPath}/editJobParts/getNewReception',
                    type: "POST",
                    data: {kodeKotaNoPol : noDepan,noPolTengah : noTengah, noPolBelakang : noBelakang,kategori : kategori, noWO : key},
                    success: function(data) {
                        var kosong = false;
                        if(data[0].hasil=="ada"){
                            idVincode=data[0].idCV;
                            staValidate = 1;
                            document.getElementById('lblStaBooking').innerHTML = data[0].staBooking
                            $('#txtnopolDiscount').val(data[0].fullnopol);
                            $('#txtmodelDiscount').val(data[0].mobil);
                            if(data[0].idReception){
                                $('#receptionId').val(data[0].idReception);
                            }
                            if(data[0].tgglReception){
                                document.getElementById('lblTgglReception').innerHTML = data[0].tgglReception
                            }
                            if(data[0].noWO){
                                document.getElementById('lblNoWO').innerHTML = data[0].noWO
                            }
                            reloadjobnPartsTable();
                            if(data[0].staInvoice){
                                hideUnusedButton();
                                $("#btnSummary").show();
                            }else{
                                showAllButton();
                            }

                            $('#lblPemilik').html(data[0].pemilik);
                            $('#lblKendaraan').html(data[0].kendaraan);
                            $('#lblVincode').html(data[0].vincode);
                            $('#lblSA').html(data[0].namaSA);
                            $('#lblJenisWo').html(data[0].jenisWo);
                            $('#lblTeknisi').html(data[0].teknisi);
                            $('#kodeKota').val(data[0].kodeKota);
                            $('#nomorTengah').val(data[0].tengah);
                            $('#nomorBelakang').val(data[0].belakang);
                            $('#noWoCari').val(data[0].noWO);
                            if(data[0].kmSekarang ){
                                $('#i_km').val(data[0].kmSekarang)
                            }
                            if(data[0].keluhan.length>0 ){
                                var keluhan = data[0].keluhan;
                                countKeluhan = 1;
                                for(var i = 0;i< keluhan.length ; i++){
                                    var yaTidak = (keluhan[i][1]=='1' ? 'Ya' : 'Tidak');
                                    var newRow = "<tr><td><input id='countKeluhan"+countKeluhan+"' type='hidden' value='"+countKeluhan+"'/><input id='keluhan"+countKeluhan+"' type='hidden' value='"+keluhan[i][0]+"'/><input id='staDiagnose"+countKeluhan+"' type='hidden' value='"+keluhan[i][1]+"'/><input type='checkbox' class='idKeluhan' />"+countKeluhan+"</td><td>"+keluhan[i][0]+"</td><td>"+yaTidak+"</td></tr>";
			                        $('#tableKeluhan > tbody:last').append(newRow);
			                        countKeluhan++;
                                }
                                $('#i_km').val(data[0].kmSekarang)
                            }
                            if(data[0].kategori ){
                                $('#kategoriJob').val(data[0].kategori)
                            }
                            if(data[0].statusWarranty ){
                                $('#staWarranty').val(data[0].statusWarranty)
                            }

                        }else if(data[0].hasil=="replaced"){
                            kosong = true;
                            alert('Nomor polisi sudah tidak berlaku');
                        }else if(data[0].hasil=="nothing"){
                            kosong = true;
                            alert('Data dengan nomor polisi tersebut tidak ditemukan');
                        }
                        if(kosong){
                            $('#lbltotalsebelumdiscount').text('');
                            $('#lbltotaljasa').text('');
                            $('#lbltotalparts').text('');
                            $('#lbldiscountjasa').text('');
                            $('#lbldiscountpart').text('');
                            $('#lblstadiscountjasa').text('');
                            $('#lblstadiscountpart').text('');
                            $('#lbltotalsetelahdiscount').text('');
                            $('#lblpersendiscountjasa').text('')
                            $('#lblpersendiscountpart').text('')
                            staValidate = 0;
                            $('#receptionId').val('-1');
                            document.getElementById('lblTgglReception').innerHTML="";
                            document.getElementById('lblNoWO').innerHTML = "";
                            document.getElementById('lblStaBooking').innerHTML = "";
                            $('#lblPemilik').html('');
                            $('#lblKendaraan').html('');
                            $('#lblVincode').html('');
                            $('#lblSA').html('');
                            $('#lblJenisWo').html('');
                            $('#lblTeknisi').html('');
                            $('#txtnopolDiscount').val('');
                            $('#txtmodelDiscount').val('');
                            $('#i_km').val('');
                            $('#kategoriJob').val('');
                            $('#staWarranty').val('');
                            $('#tableKeluhan').find("tr:gt(0)").remove();
                            countKeluhan=1;
                            reloadjobnPartsTable();
                        }
                    },
                    error: function (data, status, e){
                        alert(e);
                    }
                });
            }
	    }

        function initSummary(id){

			$.ajax({
				url:'${request.getContextPath()}/editJobParts/getDataJobSummary?noWo='+id,
				type: 'POST',
				success: function (res) {

					$('#tableJobSummaryReception').find("tr:gt(0)").remove();
					$('#tableJobSummaryReception').append(res);
				},
				error: function (data, status, e){
					alert(e);
				},
				complete: function(xhr, status) {

				},
				cache: false,
				contentType: false,
				processData: false
			});

			$.ajax({
				url:'${request.getContextPath()}/editJobParts/getDataKeluhanSummary?noWo='+id,
				type: 'POST',
				success: function (res) {
					$('#keluhanSummary').val(res);
				},
				error: function (data, status, e){
					alert(e);
				},
				complete: function(xhr, status) {

				},
				cache: false,
				contentType: false,
				processData: false
			});

			$.ajax({
				url:'${request.getContextPath()}/editJobParts/getDataPembayaran?noWo='+id,
				type: 'POST',
				success: function (res) {
					$('#tablePembayaranSummary').find("tr:gt(0)").remove();
					$('#tablePembayaranSummary').append(res);
				},
				error: function (data, status, e){
					alert(e);
				},
				complete: function(xhr, status) {

				},
				cache: false,
				contentType: false,
				processData: false
			});
		}
        function saveHarga(id,dari,tes){
                var harga = $('#'+dari+'-'+id).val();
                var params;
                params = {
                    id: id,
                    harga: tes,
                    dari : dari
                }
                $.post("${request.getContextPath()}/salesQuotation/updateHarga", params, function(data) {
                    if (data){
                        reloadjobnPartsTable();
                    }else{
                        alert("Internal Server Error");
                    }
                });
            }
            function ubahCustomer(){
                   if($("#idCustomerUbah").val()!="" || $("#idCustomerUbah").val()!=null){
                       alert($("#idCustomerUbah").val());
                       $.ajax({
                            url:'${request.contextPath}/reception/ubahCustomer',
                            type: "POST", // Always use POST when deleting data
                            data: { id : $('#receptionId').val(),idCustomer : $("#idCustomerUbah").val() },
                            success: function(data) {
                                toastr.success('Ubah Data Sukses');
                                retrieveData();
                            },error:function(){
                                alert('failed to confirm');
                            }
                        });
                   }
            }

            function ubahSA(){
                   if($("#idSAUbah").val()!="" || $("#idSAUbah").val()!=null){
                       $.ajax({
                            url:'${request.contextPath}/reception/ubahSA',
                            type: "POST", // Always use POST when deleting data
                            data: { id : $('#receptionId').val(),idSAUbah : $("#idSAUbah").val() },
                            success: function(data) {
                                toastr.success('Ubah Data Sukses');
                                retrieveData();
                            },error:function(){
                                alert('failed to confirm');
                            }
                        });
                   }
            }
            function ubahTeknisi(){
                   if($("#idTeknisiUbah").val()!="" || $("#idTeknisiUbah").val()!=null){
                       $.ajax({
                            url:'${request.contextPath}/reception/ubahTeknisi',
                            type: "POST", // Always use POST when deleting data
                            data: { id : $('#receptionId').val(),idTeknisi : $("#idTeknisiUbah").val() },
                            success: function(data) {
                                toastr.success('Ubah Data Sukses');
                                retrieveData();
                            },error:function(){
                                alert('failed to confirm');
                            }
                        });
                   }
            }
            function ubahJenisWo(){
                   if($("#idJenisUbah").val()!="" || $("#idJenisUbah").val()!=null){
                       $.ajax({
                            url:'${request.contextPath}/reception/ubahJenisWo',
                            type: "POST", // Always use POST when deleting data
                            data: { id : $('#receptionId').val(),idJenis : $("#idJenisUbah").val() },
                            success: function(data) {
                                toastr.success('Ubah Data Sukses');
                                retrieveData();
                            },error:function(){
                                alert('failed to confirm');
                            }
                        });
                   }
            }
        function saveChangeData(id){
            $('#spinner').fadeIn(1);
            $.ajax({
                url:'${request.contextPath}/reception/changeValue',
                type: "POST", // Always use POST when deleting data
                data: { id : id , nilai : $("#"+id).val() },
                success: function(data) {
                    $('#spinner').fadeOut();
                },error:function(){
                    alert('Internal Server Error');
                }
            });
        }

        function changeKategori(){
            if($('#kategori').val()=="Nomor WO"){
                $("#no_wo_search").show();
                $("#no_pol_search").hide();
            }else{
                $("#no_wo_search").hide();
                $("#no_pol_search").show();
            }
        }

    </g:javascript>
    <style>
    body .modal-reception {
        /* new custom width */
        width: 1020px;
        /* must be half of the width, minus scrollbar on the left (30px) */
        margin-left: -510px;
    }
    body .modal-kecilSekali{
        width: 310px;
        margin-left: -240px;
    }
    body .modal-kecil2{
        width: 410px;
        margin-left: -240px;
    }
    body .modal-kecil {
        /* new custom width */
        width: 650px;
        /* must be half of the width, minus scrollbar on the left (30px) */
        margin-left: -240px;
    }

    .datepicker{z-index:1151;}
    </style>
</head>

<body>

<div id="dialog-reception-jobkeluhan" class="modal hide fade in modal-reception" style="display: none; ">
    <g:render template="receptionJobKeluhan"/>
</div>

<div id="dialog-reception-customer" class="modal hide fade in modal-reception" style="display: none; ">
    <g:render template="receptionCustomer"/>
</div>

<div id="dialog-reception-teknisi" class="modal hide fade in modal-kecil" style="display: none; ">
    <g:render template="receptionTeknisi"/>
</div>

<div id="dialog-reception-sa" class="modal hide fade in modal-kecil2" style="display: none; ">
    <g:render template="receptionSA"/>
</div>

<div id="dialog-reception-jenis" class="modal hide fade in modal-kecilSekali" style="display: none; ">
    <g:render template="receptionJenisWo"/>
</div>

<div id="dialog-reception-searchparts" class="modal hide fade in modal-reception" style="display: none; ">
    <g:render template="receptionSearchParts"/>
</div>

<div id="dialog-reception-inputjob" class="modal hide fade in modal-reception" style="display: none; ">
    <g:render template="receptionInputJob"/>
</div>

<div id="dialog-reception-prediagnose" class="modal hide fade in modal-reception" style="display: none; ">
    <g:render template="receptionPrediagnose"/>
</div>

<div id="dialog-reception-parts" class="modal hide fade in modal-reception" style="display: none; ">
    <g:render template="receptionParts"/>
</div>

<div id="dialog-reception-summary" class="modal hide fade in modal-reception" style="display: none; ">
    <g:render template="receptionSummary"/>
</div>

<div id="dialog-reception-cancel" class="modal hide fade in modal-reception" style="display: none; ">
    <g:render template="receptionCancel"/>
</div>

<div id="dialog-reception-permohonan-approval" class="modal hide fade in modal-reception" style="display: none; ">
    <g:render template="receptionPermohonanApproval"/>
</div>

<div id="dialog-reception-permohonan-approval-pengurangan" class="modal hide fade in modal-reception" style="display: none; ">
    <g:render template="approvalPengurangan"/>
</div>

<div id="dialog-reception-permohonan-approval-ratejob" class="modal hide fade in modal-reception" style="display: none; ">
    <g:render template="approvalRateJob"/>
</div>

<div id="dialog-reception-permohonan-approval-discount" class="modal hide fade in modal-reception" style="display: none; ">
    <g:render template="approvalDiscount"/>
</div>

<div id="dialog-reception-searchjob" class="modal hide fade in modal-reception" style="display: none; ">
    <g:render template="receptionSearchJob"/>
</div>

<div id="dialog-reception-inputparts" class="modal hide fade in modal-reception" style="display: none; ">
    <g:render template="receptionInputParts"/>
</div>

<div id="dialog-reception-joborder" class="modal hide fade in modal-reception" style="display: none; ">
    <g:render template="receptionJobOrder"/>
</div>

<div id="dialog-reception-paketjob" class="modal hide fade in modal-reception" style="display: none; ">
    <g:render template="receptionPaketJob"/>
</div>

<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="reception.editJobParts.label" default="Edit Job dan Parts"/></span>
</div>

<div class="box">
    <div class="span12" id="editJobnParts-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <g:hiddenField name="receptionId" id="receptionId" value="-1"/>
        <div class="row-fluid">
            <div class="span6 form-horizontal" >
                <div class="control-group">
                    <label class="control-label" for="noWo" style="font-size: 20px; height: 50px; text-align: left;line-height: 50px;">
                        Kategori Search
                    </label>
                    <div class="controls">
                        <g:select name="kategori" id="kategori" onchange="changeKategori();" from="${['Nomor WO','Nomor Polisi']}" style="font-size: 30px; height: 50px; border-top-left-radius: 0; border-bottom-left-radius: 0;"/>
                    </div>
                </div>

                <div class="control-group" id="no_wo_search" >
                    <label class="control-label" for="noWoCari" style="font-size: 20px; height: 50px; text-align: left;line-height: 50px;">
                        Nomor WO
                        <span class="required-indicator">*</span>
                    </label>

                    <div class="controls" style="margin-left: 100px;">
                        <g:textField name="noWoCari" id="noWoCari" style="width: 350px; font-size: 25px; height: 50px; border-top-left-radius: 0; border-bottom-left-radius: 0;" value="${noWO}"/>
                        <a class="btn cancel" href="javascript:void(0);" onclick="retrieveData();"
                           style="font-size: 20px; height: 50px;line-height: 50px;">OK</a>
                        <span></span>
                    </div>
                </div>

                <div class="control-group" id="no_pol_search" style="display: none">
                    <label class="control-label" for="kodeKota"
                                                  style="font-size: 20px; height: 50px; text-align: left;line-height: 50px;">Nomor Polisi
                        <span class="required-indicator">*</span>
                    </label>

                    <div class="controls" style="margin-left: 100px;">
                        <g:select style="width: 90px; font-size: 30px; height: 61px; border-radius: 0;" id="kodeKota"
                                  name="kodeKota"
                                  from="${com.kombos.administrasi.KodeKotaNoPol.createCriteria().list() {
                                      eq("staDel", "0"); order("m116ID")
                                  }}" optionValue="m116ID" optionKey="id" required="" class="many-to-one"/>
                        <g:textField class="numberonly" name="nomorTengah" id="nomorTengah" maxlength="5" value=""
                                     style="width: 95px; font-size: 30px; height: 50px; border-radius: 0;"/>
                        <g:textField class="charonly" name="nomorBelakang" id="nomorBelakang" maxlength="3" value=""
                                     style="width: 70px; font-size: 30px; height: 50px; border-top-left-radius: 0; border-bottom-left-radius: 0;text-transform:uppercase;"/>
                        <a class="btn cancel" href="javascript:void(0);" onclick="retrieveData();"
                           style="font-size: 20px; height: 50px;line-height: 50px;">OK</a>
                    </div>
                </div>
                <div class="control-group" id="warning" style="display: none">
                    <br>
                    <H4 id="showWarningJob" style="color: red"></H4>
                    <H4 id="showWarningParts" style="color: red"></H4>
                </div>
            </div>

            <div class="span4 offset2" id="tgl_appointment">
                <table class="table">
                    <tbody>
                    <tr>
                        <td>
                            <table class="table table-bordered table-dark table-hover">
                                <tbody>
                                <tr>
                                    <td>
                                        <span class="property-label" id="lblStaBooking"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="property-label" id="lblJenisWo"></span>
                                            <a class="pull-right cell-action" href="javascript:void(0);" id="changeJenis" onclick="openDialog('dialog-reception-jenis')"><i class="icon-pencil"></i></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                        <td>
                            <table class="table table-bordered table-dark table-hover">
                                <tbody>
                                <tr>
                                    <td><span
                                            class="property-label">Tanggal Reception</span></td>
                                    <td><span class="property-value" id="lblTgglReception"></span></td>
                                </tr>
                                <tr>
                                    <td><span
                                            class="property-label">No. Reception</span></td>
                                    <td><span class="property-value" id="lblNoWO"></span></td>
                                </tr>
                                <tr>
                                    <td><span
                                            class="property-label">Nama Pemilik</span></td>
                                    <td><span class="property-value" id="lblPemilik"></span>
                                        <a class="pull-right cell-action" href="javascript:void(0);" id="changePemilik" onclick="openDialog('dialog-reception-customer')"><i class="icon-pencil"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span
                                            class="property-label">Jenis Kendaraan</span></td>
                                    <td><span class="property-value" id="lblKendaraan"></span></td>
                                </tr>
                                <tr>
                                    <td><span
                                            class="property-label">Nomor Rangka</span></td>
                                    <td><span class="property-value" id="lblVincode"></span></td>
                                </tr>
                                <tr>
                                    <td><span
                                            class="property-label">Nama SA</span></td>
                                    <td><span class="property-value" id="lblSA"></span>
                                        <a class="pull-right cell-action" href="javascript:void(0);" id="changeSA" onclick="openDialog('dialog-reception-sa')"><i class="icon-pencil"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span
                                            class="property-label">Teknisi</span></td>
                                    <td><span class="property-value" id="lblTeknisi"></span>
                                        <a class="pull-right cell-action" href="javascript:void(0);" id="changeTeknisi" onclick="openDialog('dialog-reception-teknisi')"><i class="icon-pencil"></i></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <a class="btn btn-primary" href="javascript:void(0);" id="btnTambahJob"
           onclick="openDialog('dialog-reception-jobkeluhan');">Tambah Job</a>
        <a class="btn btn-primary" href="javascript:void(0);" id="btnTambahPart" onclick="openDialog('dialog-reception-parts');">Tambah Part</a>
        <a class="btn btn-default" href="javascript:void(0);" id="btnPengurangan" onclick="showKurangiJob();">Approval Pengurangan Job/Part</a>
        <a class="btn btn-default" href="javascript:void(0);" id="btnEditRate" onclick="showAprovalRateJob();">Edit Rate Job</a>
        <a class="btn btn-default" href="javascript:void(0);" id="btnBillTo" onclick="billToAndDiscount();">BillTo & Discount</a>
        <g:if test="${session.userAuthority.toUpperCase()?.contains("ADMIN")}">
            <a class="btn btn-warning" href="javascript:void(0);" id="btnDeleteJobPart" onclick="deleteJobPart();">Delete Job / Parts</a>
        </g:if>
        <g:render template="jobnPartsDataTables"/>

        %{--<a class="btn cancel" href="javascript:void(0);" id="btnRequest" onclick="openRequestPart();">Request Parts</a>--}%
        <a class="btn btn-success" href="javascript:void(0);" id="btnJpbGR" onclick="jpbGr();">Cek JPB GR</a>
        <a class="btn btn-success" href="javascript:void(0);" id="btnJpbBP" onclick="jpbBp();">Cek JPB BP</a>
        <a class="btn cancel" href="javascript:void(0);" id="btnSummary" onclick="showAppSummary();">Summary Reception</a>
        <a class="btn cancel" href="javascript:void(0);" id="btnCancel" onclick="showAproval();">Cancel Reception</a>
        <a class="btn btn-success" href="javascript:void(0);" id="btnPrintWoGr" onclick="printWoGr();">Print WO GR</a>
        <a class="btn btn-success" href="javascript:void(0);" id="btnPrintWoBp" onclick="printWoBp();">Print WO BP</a>
    </div>

    <div class="span7" id="editJobnParts-form" style="display: none;"></div>

</div>
</body>
</html>
