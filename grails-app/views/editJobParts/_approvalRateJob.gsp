<%@ page import="com.kombos.administrasi.KegiatanApproval; com.kombos.reception.Reception" %>
<div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Reception - Edit Job dan Parts - Edit Rate</h3>
</div>
<g:javascript>
    $(function(){
        sendRate = function(){
            var idJob = $('#idJobRate').val();
            var pesan = $('#txtAlasanRate').val();
            var kegiatanApproval = $('#kegiatanApprovalRate').val();
            var rate = $('#txtrate').val();
            var noWo = $('#noWoApproval').val();
            $.ajax({
                    url:'${request.contextPath}/editJobParts/approvalRate',
                    type: "POST", // Always use POST when deleting data
                    data : {idJob : idJob,pesan:pesan,rate:rate,kegiatanApproval:kegiatanApproval,noWo:noWo},
                    success : function(data){
                        toastr.success('<div>Approval Sudah Dikirim</div>');
                        reloadjobnPartsTable();
                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });
        };

    });
</g:javascript>
<div class="modal-body">
    <div class="box">
        <table class="table">
            <tbody>
            <tr>
                <td>
                    Nama Job/Parts
                </td>
                <td>
                    <input type="hidden" name="kegiatanApprovalRate" id="kegiatanApprovalRate" value="${KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.EDIT_RATE).id}" />
                    <input type="hidden" name="idJobRate" id="idJobRate" />
                    <input type="hidden" name="noWoApproval" id="noWoApproval" />
                    <span id="lbljobparts"></span>
                </td>
            </tr>
            <tr>
                <td>
                    Rate
                </td>
                <td>
                    <input type="hidden" name="rateBefore" id="rateBefore" />
                    <g:textField name="txtrate" id="txtrate" />
                </td>
            </tr>
            <tr>
                <td>
                    alasan
                </td>
                <td>
                    <textarea id="txtAlasanRate" name="txtAlasanRate" cols="200" rows="3" style="resize: none">-</textarea>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="modal-footer">
    <a href="#" class="btn btn-success" onclick="sendRate()" data-dismiss="modal">Send</a>
    <a href="#" class="btn" data-dismiss="modal">Close</a>
</div>