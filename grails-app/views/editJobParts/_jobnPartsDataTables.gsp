<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="job_n_parts_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
			<th style="border-bottom: none;padding: 5px;">
				<div>Nama Job</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Rate</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Status Warranty</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Nominal</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Status Tambah Kurang Job/Part</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Int/Ext</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Status Approval Pengurangan Job/Part</div>
			</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="3" style="text-align: center"><span>TOTAL JASA</span></td>
			<td><span class="pull-right numeric" id="lbltotaljasa"></span></td>
            <td colspan="3"></td>
		</tr>
		<tr>
            <td colspan="3" style="text-align: center"><span>TOTAL PARTS</span></td>
			<td><span class="pull-right numeric" id="lbltotalparts"></span></td>
            <td colspan="3"></td>
        </tr>
		<tr>
            <td colspan="3" style="text-align: center"><span>TOTAL SEBELUM DISCOUNT</span></td>
			<td><span class="pull-right numeric" id="lbltotalsebelumdiscount"></span></td>
            <td colspan="3"></td>
        </tr>
		<tr>
            <td colspan="2" style="text-align: center"><span>SPECIAL DISCOUNT (JASA)</span></td>
            <td><span class="pull-right numeric" id="lblpersendiscountjasa"></span></td>
            <td><span class="pull-right numeric" id="lbldiscountjasa"></span></td>
            <td colspan="3"><span class="pull-right numeric" id="lblstadiscountjasa"></span></td>
        </tr>
		<tr>
            <td colspan="2" style="text-align: center"><span>SPECIAL DISCOUNT (PART)</span></td>
            <td><span class="pull-right numeric" id="lblpersendiscountpart"></span></td>
            <td><span class="pull-right numeric" id="lbldiscountpart"></span></td>
            <td colspan="3"><span class="pull-right numeric" id="lblstadiscountpart"></span></td>
        </tr>
		<tr>
            <td colspan="3" style="text-align: center"><span>TOTAL SETELAH DISCOUNT</span></td>
			<td><span class="pull-right numeric" id="lbltotalsetelahdiscount"></span></td>
            <td colspan="3"></td>
        </tr>
	<tr>
		<td colspan="3" style="text-align: center"><span>BOOKING FEE / PANJAR</span></td>
		<td><span class="pull-right numeric" id="lblPanjar"></span></td>
		<td colspan="3"></td>
	</tr>
	</tfoot>
</table>

<g:javascript>
    function changeStaJob(id){
        var ieVal = $('#intExt'+id).val()
        $.ajax({url: '${request.contextPath}/reception/ganti?value='+ieVal+"&idChange="+id,
            type: "POST",
            complete : function (req, err) {
                $('#spinner').fadeOut();
            }
        });
    }
var jobnPartsTable;
var reloadjobnPartsTable;
$(function(){
	reloadjobnPartsTable = function() {
		$('#warning').hide();
		toastr.info("Ambil Data SUKSES");
		jobnPartsTable.fnDraw();
	}

	jobnPartsTable = $('#job_n_parts_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'p>>",
		bFilter: false,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			var aData = jobnPartsTable.fnGetData(nRow);
			if(aData.staPart!="tidak"){
                $.ajax({type:'POST',
                    data : aData,
                    url:'${g.createLink(action: "partList")}',
                    success:function(data,textStatus){
                        var nDetailsRow = jobnPartsTable.fnOpen(nRow,data,'details');

                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){},
                    complete:function(XMLHttpRequest,textStatus){
                        $('#spinner').fadeOut();
                    }
                });
            }
	   		$('#lbltotalsebelumdiscount').text(aData.totalNominal);
	   		$('#lbltotaljasa').text(aData.totalJasa);
	   		$('#lbltotalparts').text(aData.totalPart);
	   		$('#lbldiscountjasa').text(aData.discJasa);
	   		$('#lbldiscountpart').text(aData.discPart);
	   		$('#lblstadiscountjasa').text(aData.staDiskon);
	   		$('#lblstadiscountpart').text(aData.staDiskon);
	   		$('#lbltotalsetelahdiscount').text(aData.total);
	   		$('#lblPanjar').text(aData.panjar);
	   		if(aData.staBaru!="Kurang" && aData.nominalJasa==0){
	   			$('#warning').show();
	   			$('#showWarningJob').text("*PERHATIAN!! Terdapat Nilai Jasa yang masih NOL*");
	   			toastr.warning("PERHATIAN!! Nilai Jasa ada yang masih NOL");
	   		}

	   		if(aData.jasa){
	   		    $('#lblpersendiscountjasa').text(aData.jasa+" %")
	   		}
			if(aData.jasa){
	   		    $('#lblpersendiscountpart').text(aData.part+" %")
	   		}
			return nRow;
		},
		"fnDrawCallback":function(oSettings){
			if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
				$(oSettings.nTableWrapper).find('.dataTables_paginate').hide();
			}
		},
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${request.contextPath}/editJobParts/jobnPartsDatatablesList",
		"aoColumns": [
{
	"sName": "namaJob",
	"mDataProp": "namaJob",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		 if(row['staApproveJob']=="Approved" && row['staBaru']!="Tambah"){
			return '<input type="hidden" value="'+row['idJob']+'">&nbsp;&nbsp;'+data;
		 }else{
			return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['idJob']+'" title="Select this"><input type="hidden" value="'+row['idJob']+'">&nbsp;&nbsp;'+data;
		 }
	},
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"527px",
	"bVisible": true
},{
	"sName": "rate",
	"mDataProp": "rate",
	"aTargets": [1],
	"mRender": function ( data, type, row ) {
		if(data != 'null')
			return '<span class="pull-right numeric">'+data+'</span>';
		else 
			return '<span></span>';
	},
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"69px",
	"bVisible": true
},{
	"sName": "statusWarranty",
	"mDataProp": "statusWarranty",
	"aTargets": [2],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"73px",
	"bVisible": true
},{
	"sName": "nominalJasa",
	"mDataProp": "nominalJasa",
	"aTargets": [3],
	"mRender": function ( data, type, row ) {
	    if((row['isBP'] && row['isBP']=="BP") ||  row['customJob']=="CUSTOM JOB"){
            return '<input type="text" class="numeric pull-right" id="jasa-'+row['idJob']+'" style="text-align: right; width : 100px" onblur="saveHarga('+row['idJob']+',\'jasa\',this.value);" value="'+data+'">';
	    }else{
	        return '<span class="pull-right numeric">'+data+'</span>';
	    }
	},
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"119px",
	"bVisible": true
}
,{
	"sName": "staBaru",
	"mDataProp": "staBaru",
	"aTargets": [4],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"119px",
	"bVisible": true
}
,{
	"sName": "staIntExt",
	"mDataProp": "staIntExt",
	"aTargets": [5],
    "mRender": function ( data, type, row ) {
        var tipeOrderCombo = '<select id="intExt'+row['idJob']+'" onchange="changeStaJob(\''+row['idJob']+'\');" name="intExt.id" class="many-to-one inline-edit" style="width:105px;" >';
        if(data == 'e'){
            tipeOrderCombo += '<option value="e" selected="selected">Ext</option>';
            tipeOrderCombo += '<option value="i">Int</option>';
        }else{
            tipeOrderCombo += '<option value="e">Ext</option>';
            tipeOrderCombo += '<option value="i" selected="selected">Int</option>';
		}
        tipeOrderCombo += '</select>';
        return tipeOrderCombo;
	},
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"119px",
	"bVisible": true
}
,{
	"sName": "staApproveJob",
	"mDataProp": "staApproveJob",
	"aTargets": [6],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"119px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        aoData.push(
                            {"name": 'sReceptionId', "value": $('#receptionId').val()}
                        );
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
