<g:javascript>
	var addParts = function() {
   		var recordsToDelete = [];
		
		var idPart = "";
		var kodePart = "";
		var namaPart = "";
		var satuan = "";
		$("#goods_datatables tbody .row-select").each(function() {
			if(this.checked){
    			idPart = $(this).next("input:hidden").val();
				kodePart = $($(this).next("input:hidden")).next("input:hidden").val();
				namaPart = $($($(this).next("input:hidden")).next("input:hidden")).next("input:hidden").val();
				satuan = $($($($(this).next("input:hidden")).next("input:hidden")).next("input:hidden")).next("input:hidden").val();
				addPartReception(idPart,kodePart,namaPart,satuan);
    		}
		});
		closeSearchParts();
   	}	
	
	function closeSearchParts(){
		closeDialog('dialog-reception-searchparts');
		openDialog('dialog-reception-inputparts');
	}
</g:javascript>
<div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Reception - Search Job</h3>
</div>
<div class="modal-body">
    <div class="dataTables_scroll">
        <div class="span12" id="companyDealerPopUp-table">
            <g:render template="datatablesParts"/>
        </div>
    </div>
</div>
<div class="modal-footer">
    <a onclick="addParts();" href="javascript:void(0);" class="btn btn-success" id="add_part_btn">Add</a>
    <a onclick="closeSearchParts();" href="javascript:void(0);" class="btn" data-dismiss="modal">Close</a>
</div>