
<%@ page import="com.kombos.parts.Satuan; com.kombos.parts.Goods" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="goods_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%"
	style="margin-left: 0px; width: 930px;"
	>
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<input type="hidden" name="search_m111ID" id="search_m111ID"/>
				<input type="hidden" name="search_m111Nama" id="search_m111Nama"/>
				<div><g:message code="goods.m111ID.label" default="Kode Goods" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="goods.m111Nama.label" default="Nama Goods" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="goods.satuan.label" default="Satuan" /></div>
			</th>
		</tr>
	</thead>
</table>

<g:javascript>
var goodsTable;
var reloadGoodsTable;
$(function(){
	
	reloadGoodsTable = function() {
		goodsTable.fnDraw();
	}
    $('#search_satuan').change(function(){
        goodsTable.fnDraw();
    });
	var recordsGoodsPerPage = [];
    var anGoodsSelected;
    var jmlRecGoodsPerPage=0;
    var id;

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	goodsTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	goodsTable = $('#goods_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsGoods = $("#goods_datatables tbody .row-select");
            var jmlGoodsCek = 0;
            var nRow;
            var idRec;
            rsGoods.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsGoodsPerPage[idRec]=="1"){
                    jmlGoodsCek = jmlGoodsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsGoodsPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecGoodsPerPage = rsGoods.length;
            if(jmlGoodsCek==jmlRecGoodsPerPage && jmlRecGoodsPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(controller: "goods", action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m111ID",
	"mDataProp": "m111ID",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'"><input type="hidden" value="'+row['m111ID']+'"><input type="hidden" value="'+row['m111Nama']+'"><input type="hidden" value="'+row['satuan']+'">&nbsp;&nbsp;'+data+'&nbsp;&nbsp;';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m111Nama",
	"mDataProp": "m111Nama",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "satuan",
	"mDataProp": "satuan",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m111ID = $('#search_m111ID').val();
						if(m111ID){
							aoData.push(
									{"name": 'sCriteria_m111ID', "value": m111ID}
							);
						}
	
						var m111Nama = $('#search_m111Nama').val();
						if(m111Nama){
							aoData.push(
									{"name": 'sCriteria_m111Nama', "value": m111Nama}
							);
						}
						
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

	$('.select-all').click(function(e) {

        $("#goods_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsGoodsPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsGoodsPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#goods_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsGoodsPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anGoodsSelected = goodsTable.$('tr.row_selected');

            if(jmlRecGoodsPerPage == anGoodsSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsGoodsPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>