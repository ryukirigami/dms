<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<input type="hidden" id="idTeknisiUbah">

<div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Ubah Nama Teknisi</h3>
</div>
<div class="modal-body">
    <div class="span12" id="companyDealerPopUp-table">
        <table id="teknisiReception_table" cellpadding="10" cellspacing="10"
               border="0"
               class="display table table-striped table-bordered table-hover fixed" style="table-layout: fixed; ">
            <col width="150px" />
            <col width="250px" />
            <col width="150px" />
            <thead>
            <tr>
                <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:150px;">
                    NIK
                </th>
                <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:250px;">
                    Nama Lengkap
                </th>
                <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:150px;">
                    Nama Board
                </th>
            </tr>
            <tr>
                <th style="border-top: none;padding: 0px 0px 5px 0px; width:150px;">
                    <div id="filter_nik" style="padding-top: 0px;position:relative; margin-top: 0px;width: 145px;">
                        <input type="text" name="nik" class="search_init" />
                    </div>
                </th>

                <th style="border-top: none;padding: 0px 0px 5px 0px; width:250px;">
                    <div id="filter_namalengkap" style="padding-top: 0px;position:relative; margin-top: 0px;width: 145px;">
                        <input type="text" name="nama" class="search_init" />
                    </div>
                </th>
                <th style="border-top: none;padding: 0px 0px 5px 0px; width:150px;">
                    <div id="filter_board" style="padding-top: 0px;position:relative; margin-top: 0px;width: 145px;">
                        <input type="text" name="board" class="search_init" />
                    </div>
                </th>

            </tr>

            </thead>
        </table>

        <g:javascript>
var teknisiReceptionTable;
var reloadteknisiReceptionTable;
var setValues;
$(function(){
    setValues = function(el,noReff) {
        var idReff = noReff;
        $("#idTeknisiUbah").val(idReff);
	}

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	teknisiReceptionTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	teknisiReceptionTable = $('#teknisiReception_table').dataTable({
		"sScrollX": "900px",
		"bScrollCollapse": true,
		"bAutoWidth" : true,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : 5, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(controller: "reception", action: "teknisiDatatables")}",
		"aoColumns": [

{
	"sName": "nik",
	"mDataProp": "nik",
	"aTargets": [1],
	"bSortable": false,
	"bSearchable": true,
	"bVisible": true,
	"mRender": function ( data, type, row ) {
	    return "<input type='radio' onclick='setValues(this,"+row['id']+");' name='nik'> &nbsp; " + data;
	}
},
{
    "sName": "nama",
	"mDataProp": "nama",
	"aTargets": [2],
	"bSortable": false,
	"bSearchable": true,
	"sClass": "tglReff",
	"bVisible": true
},
{
    "sName": "board",
	"mDataProp": "board",
	"aTargets": [3],
	"bSortable": false,
	"bSearchable": true,
	"sClass": "tglReff",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var nama = $('#filter_namalengkap input').val();

						if(nama){
							aoData.push(
									{"name": 'sCriteria_namalengkap', "value": nama}
							);
						}

						var nik = $('#filter_nik input').val();

						if(nik){
							aoData.push(
									{"name": 'sCriteria_nik', "value": nik}
							);
						}

						var board = $('#filter_board input').val();

						if(board){
							aoData.push(
									{"name": 'sCriteria_board', "value": board}
							);
						}


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
        </g:javascript>
    </div>
</div>
<div class="modal-footer">
    <a onclick="ubahTeknisi();" href="javascript:void(0);" class="btn btn-primary create" id="ubah_teknisi_btn" data-dismiss="modal">Ganti</a>
    <a href="#" class="btn" data-dismiss="modal">Close</a>
</div>