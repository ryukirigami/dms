
<%@ page import="com.kombos.parts.Satuan; com.kombos.parts.Goods" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="goodsBillTo_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="100%">
    <thead>
    <tr>


        <th style="border-bottom: none;padding: 5px;">
            <div>&nbsp;<input type="checkbox" class="select-all"/>&nbsp;
                <g:message code="operation.m053NamaOperation.label" default="Job" />
            </div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div>
                <g:message code="goods.m111ID.label" default="Kode Goods" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.m111Nama.label" default="Nama Goods" /></div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.satuan.label" default="Harga Satuan" /></div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.qty.label" default="Qty" /></div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.total.label" default="Total" /></div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="default.discount.label" default="Discount" /></div>
        </th>


    </tr>
    </thead>
</table>

<g:javascript>
var goodsBillToTable;
var reloadGoodsBillToTable;
$(function(){

	reloadGoodsBillToTable = function() {
		goodsBillToTable.fnDraw();
	}
    $('#search_satuan').change(function(){
        goodsBillToTable.fnDraw();
    });
	var recordsGoodsPerPage = [];
    var anGoodsSelected;
    var jmlRecGoodsPerPage=0;
    var id;

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	goodsBillToTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	goodsBillToTable = $('#goodsBillTo_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsGoods = $("#goodsBillTo_datatables tbody .row-select");
            var jmlGoodsCek = 0;
            var nRow;
            var idRec;
            rsGoods.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsGoodsPerPage[idRec]=="1"){
                    jmlGoodsCek = jmlGoodsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsGoodsPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecGoodsPerPage = rsGoods.length;
            if(jmlGoodsCek==jmlRecGoodsPerPage && jmlRecGoodsPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesGoodsList")}",
		"aoColumns": [

{
	"sName": "namaJob",
	"mDataProp": "namaJob",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "m111ID",
	"mDataProp": "m111ID",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}

,

{
	"sName": "m111Nama",
	"mDataProp": "m111Nama",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
}

,

{
	"sName": "harga",
	"mDataProp": "harga",
	"aTargets": [3],
    "mRender": function ( data, type, row ) {
        return '<span class="pull-right numeric">'+data+'</span>';
    },
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"120px",
	"bVisible": true
}

,

{
	"sName": "qty",
	"mDataProp": "qty",
	"aTargets": [4],
    "mRender": function ( data, type, row ) {
        return '<span class="pull-right numeric">'+data+'</span>';
    },
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"80px",
	"bVisible": true
}

,

{
	"sName": "total",
	"mDataProp": "total",
	"aTargets": [5],
    "mRender": function ( data, type, row ) {
        return '<span class="pull-right numeric">'+data+'</span>';
    },
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"120px",
	"bVisible": true
}

,


{
	"sName": "discount",
	"mDataProp": "discount",
	"aTargets": [6],
    "mRender": function ( data, type, row ) {
        return '<input id="discParts-'+row['id']+'" class="inline-edit" type="text" style="width:80px;" value="'+data+'">';
    },
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                        aoData.push(
                                {"name": 'noWo', "value": "${receptionInstance?.t401NoWO}"}
                        );

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});

	$('.select-all').click(function(e) {

        $("#goodsBillTo_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsGoodsPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsGoodsPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#goodsBillTo_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsGoodsPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anGoodsSelected = goodsBillToTable.$('tr.row_selected');

            if(jmlRecGoodsPerPage == anGoodsSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsGoodsPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>