<%--
  Created by IntelliJ IDEA.
  User: Ahmad Fawaz
  Date: 10/02/15
  Time: 11:10
--%>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="billToDiscount.label" default="Bill To And Discount" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
        function closeBillto(){
            loadPath('editJobParts/index?nowo='+$('#noWoBillTo').val());
        }
        var id = "";
        $('#txtBillTo').typeahead({
            source: function (query, process) {
                return $.get('${request.contextPath}/editJobParts/getBillTo?from='+$("#from").val(), { query: query }, function (data) {
                    return process(data.options);
                });
            }
        });

        function saveBillTo(){
            var aoData =  new Array();
		    var recordsGoods = [];
            var recordsJobs = [];
            var recordsDiscJobs = [];
            var recordsDiscParts = [];
            $("#operationBillTo_datatables .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    recordsJobs.push(id);
                    recordsDiscJobs.push($('#discJobs-'+id).val());
                }
            });

            $("#goodsBillTo_datatables .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    recordsGoods.push(id);
                    recordsDiscParts.push($('#discParts-'+id).val())
                }
            });

            if(recordsGoods.length < 1 && recordsJobs.length < 1){
                alert('Anda belum memilih data job/parts');
                return
            }
            var jsonJ = JSON.stringify(recordsJobs);
            var jsonP = JSON.stringify(recordsGoods);
            var jsonDJ = JSON.stringify(recordsDiscJobs);
            var jsonDP = JSON.stringify(recordsDiscParts);
            var pesan = $("#txtPesan").val();
            var jenisBayar = $('input[name="jenisBayar"]:checked').val();
            var jenisAksi = $('input[name="jenisAksi"]:checked').val();
            var from = $('#from').val();
            var billTo = $('#txtBillTo').val();
            if(pesan=="" || jenisAksi=="" || jenisAksi==null || jenisAksi=="undefined"){
                alert("Data belum lengkap");
                return
            }
            if(jenisAksi!="discount" && ((from=="" || billTo=="" ) || (jenisBayar=="" || jenisBayar==null || jenisBayar=="undefined" ))){
                alert("Data belum lengkap");
                return
            }
            aoData.push(
                {"name": 'jobs', "value": jsonJ},
                {"name": 'goods', "value": jsonP},
                {"name": 'discJobs', "value": jsonDJ},
                {"name": 'discParts', "value": jsonDP},
                {"name": 'jenisBayar', "value": jenisBayar},
                {"name": 'jenisAksi', "value": jenisAksi},
                {"name": 'billTo', "value": from},
                {"name": 'pesan', "value": pesan},
                {"name": 'namaBillTo', "value": billTo},
                {"name": 'noWo', "value": $("#noWoBillTo").val()}
            );
            $.ajax({
                url:'${request.contextPath}/editJobParts/doBillTo',
                type: "POST", // Always use POST when deleting data
                data: aoData,
                complete: function(xhr, status) {
                    toastr.success('<div>Data Saved.</div>');
                },error:function(){
                    alert("Internal Server Error");
                }
            });

        }

        function cekJenis(){
            if($('input:radio[name=jenisAksi]:nth(0)').is(':checked')){
                $('input[name=jenisBayar]').attr('checked',false);
                $('input[name=jenisBayar]').attr('disabled',true);
                $("#from").attr('disabled',true);
                $("#txtBillTo").val("");
                $("#txtBillTo").attr('disabled',true);
            }else{
                $('input[name=jenisBayar]').attr('disabled',false);
                $("#from").attr('disabled',false);
                $("#txtBillTo").attr('disabled',false);
            }
        }

    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="billToDiscount.label" default="Bill To And Discount" /></span>
</div>
<div class="box">
    <div class="span12" id="billToDiscount-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <table>
            <tr>
                <td style="padding: 5px">
                    Jenis Aksi
                </td>
                <td style="padding: 5px">
                    <g:radioGroup name="jenisAksi" id="jenisAksi" values="['discount','billto']" onchange="cekJenis();" labels="['Discount Only','BillTo']" required="" >
                        ${it.radio} ${it.label}
                    </g:radioGroup>
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    Nomor WO
                </td>
                <td style="padding: 5px">
                    <g:textField name="noWoBillTo" id="noWoBillTo" value="${receptionInstance?.t401NoWO}" readonly="" />
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    Jenis Pembayaran
                </td>
                <td style="padding: 5px">
                    <g:radioGroup name="jenisBayar" id="jenisBayar" values="['Tunai','Kredit']" labels="['Tunai','Kredit']" required="" >
                        ${it.radio} ${it.label}
                    </g:radioGroup>
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    Bill To
                </td>
                <td style="padding: 5px">
                    <g:select name="from" id="from" from="${["Customer","Company","Asuransi","Dealer","Cabang","Partner Service"]}" />
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    Bill To Name
                </td>
                <td style="padding: 5px">
                    <g:textField name="txtBillTo" id="txtBillTo" class="typeahead" autocomplete="off"/>
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    Pesan untuk approver
                </td>
                <td style="padding: 5px">
                    <g:textArea name="txtPesan" id="txtPesan" style="resize:none" maxlength="50"/>
                </td>
            </tr>
        </table>
        <br/>
        <g:render template="dataTablesJobsBillTo" />
        <g:render template="dataTablesPartsBillTo" />
        <br/>
        <div class="control-group">
            <g:field type="button" onclick="closeBillto();" class="btn cancel" name="close" id="close" value="${message(code: 'default.button.close.label', default: 'Close')}" />
            <g:field type="button" onclick="saveBillTo();" class="btn btn-primary create" name="tambah" id="tambah" value="${message(code: 'default.button.save.label', default: 'Save')}" />
        </div>
    </div>
    <div class="span7" id="billToDiscount-form" style="display: none;"></div>
</div>
</body>
</html>