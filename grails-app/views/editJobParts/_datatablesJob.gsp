<table id="operation_datatables_search" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       style="margin-left: 0px; width: 900px;">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px; width:300px;">
            <div><g:message code="operation.section.label" default="Section" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px; width:200px;">
            <div><g:message code="operation.serial.label" default="Serial" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px; width:200px;">
            <div><g:message code="operation.m053Id.label" default="Kode Job" /></div>
            <input type="hidden" name="search_m053Id" id="search_m053Id"/>
        </th>


        <th style="border-bottom: none;padding: 5px; width:200px;">
            <div><g:message code="operation.m053NamaOperation.label" default="Nama Job" /></div>
            <input type="hidden" name="search_m053NamaOperation" id="search_m053NamaOperation"/>
        </th>

        %{--ian--}%
        <th style="border-bottom: none;padding: 5px; width:200px;">
            <div><g:message code="operation.rate.label" default="Rate" /></div>
            <input type="hidden" name="search_rate" id="search_rate"/>
        </th>
        %{--ian--}%


        <th style="border-bottom: none;padding: 5px; width:200px;">
            <div><g:message code="operation.m053StaLift.label" default="Butuh Lift" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px; width:200px;">
            <div><g:message code="operation.m053StaPaket.label" default="Status Paket" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px; width:200px;">
            <div><g:message code="operation.kategoriJob.label" default="Kategori Job" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px; width:200px;">
            <div><g:message code="operation.m053StaPaint.label" default="Paint" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px; width:200px;">
            <div><g:message code="operation.m053Ket.label" default="Keterangan" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px; width:200px;">
            <div><g:message code="operation.m053JobsId.label" default="M053 Jobs Id" /></div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var operationTableSearch;
var reloadoperationTableSearch;
$(function(){
    $('#sectionFilter').typeahead({
        source: function (query, process) {
            return $.get('${request.contextPath}/operation/getSection', { query: query }, function (data) {
                return process(data.options);
            });
        }
    });

    $('#serialFilter').typeahead({
        source: function (query, process) {
            return $.get('${request.contextPath}/operation/getSerial', { query: query }, function (data) {
                return process(data.options);
            });
        }
    });
	
	$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	operationTableSearch.fnDraw();
		}
	});
	
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});
	
	reloadoperationTableSearch = function() {
		operationTableSearch.fnDraw();
	}
	
	operationTableSearch = $('#operation_datatables_search').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(controller: "operation", action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "section",
	"mDataProp": "section",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'"><input type="hidden" value="'+row['m053Id']+'"><input type="hidden" value="'+row['m053NamaOperation']+'"><input type="hidden" value="'+row['rate']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": false
}
,

{
	"sName": "serial",
	"mDataProp": "serial",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "m053Id",
	"mDataProp": "m053Id",
	"aTargets": [2],
    "mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'"><input type="hidden" value="'+row['m053Id']+'"><input type="hidden" value="'+row['m053NamaOperation']+'"><input type="hidden" value="'+row['rate']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m053NamaOperation",
	"mDataProp": "m053NamaOperation",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

//ian
{
	"sName": "rate",
	"mDataProp": "rate",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

//ian


{
	"sName": "m053StaLift",
	"mDataProp": "m053StaLift",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "m053StaPaket",
	"mDataProp": "m053StaPaket",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "kategoriJob",
	"mDataProp": "kategoriJob",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "m053StaPaint",
	"mDataProp": "m053StaPaint",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "m053Ket",
	"mDataProp": "m053Ket",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "m053JobsId",
	"mDataProp": "m053JobsId",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
			
            var m053Id = $('#search_m053Id').val();
            if(m053Id){
                aoData.push(
                        {"name": 'sCriteria_m053Id', "value": m053Id}
                );
            }

            var m053NamaOperation = $('#search_m053NamaOperation').val();
            if(m053NamaOperation){
                aoData.push(
                        {"name": 'sCriteria_m053NamaOperation', "value": m053NamaOperation}
                );
            }

            var kategoriCari = $('#kategoriJob').val();
            aoData.push(
                    {"name": 'reception', "value": true},
                    {"name": 'sCriteria_kategoriJob', "value": kategoriCari}
            );

            var noDepan = $('#kodeKota').val();
            var noTengah = $('#nomorTengah').val();
            var noBelakang = $('#nomorBelakang').val();
            aoData.push(
                    {"name": 'noDepan', "value": noDepan},
                    {"name": 'noTengah', "value": noTengah},
                    {"name": 'noBelakang', "value": noBelakang}
            );

            var recordsExist = [];
            $("#tableJob").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    recordsExist.push(id);
                }
            });

            if(recordsExist.length > 0){
                aoData.push(
                        {"name": 'sCriteria_exist', "value": recordsExist}
                );
            }

            $.ajax({ "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData ,
                "success": function (json) {
                    fnCallback(json);
                   },
                "complete": function () {
                   }
            });
		}			
	});
});
</g:javascript>