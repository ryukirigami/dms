<g:javascript>
	var countKeluhan = 1;
	var keluhanDelete = []
    var minKM=0;
    var cekJob = []

	function openJobKeluhan(){
	    var kodeKota=$('#kodeKota').val();
        var noPolTengah=$('#nomorTengah').val();
        var noPolBelakang=$('#nomorBelakang').val();
        $('#spinner').fadeIn(1);
        $.ajax({
            url:'${request.contextPath}/editJobParts/jobKeluhan',
            type: "POST",
            data: { kode: kodeKota , tengah : noPolTengah , belakang : noPolBelakang },
            success : function(data){
                minKM = data.minKM
                document.getElementById('lblpetugas').innerHTML = data.petugas
                $('#tableJob').find("tr:gt(0)").remove();
                cekJob=[];
            },
            error: function(xhr, textStatus, errorThrown) {
                alert('Internal server error');
            }
        });
	}
	function addKeluhan(){
		var inputKeluhan = $('#inputKeluhan').val();
		if(!inputKeluhan){
			alert('Silahkan isi keluhan');
		}else{
			var inputDiagnose = "";
			var selectedDiagnose = '';
			var selected = $("input[type='radio'][name='inputDiagnose']:checked");
			if (selected.length > 0) {
				selectedDiagnose = selected.val();
				inputDiagnose = selected.val();
				inputDiagnose = (inputDiagnose == '1'? "Ya" : "Tidak");
			}
			
			var newRow = "<tr><td><input id='countKeluhan"+countKeluhan+"' type='hidden' value='"+countKeluhan+"'/><input id='keluhan"+countKeluhan+"' type='hidden' value='"+inputKeluhan+"'/><input id='staDiagnose"+countKeluhan+"' type='hidden' value='"+selectedDiagnose+"'/><input type='checkbox' class='idKeluhan' />"+countKeluhan+"</td><td>"+inputKeluhan+"</td><td>"+inputDiagnose+"</td></tr>";
			
			countKeluhan++;
			
			$('#tableKeluhan > tbody:last').append(newRow);
			
			$('#inputKeluhan').val("");
		}
	}

	function deleteKeluhan(){
	    var id=1;
        $('.idKeluhan').each(function() {
            if(this.checked){
                keluhanDelete.push(id);
                $(this).closest("tr").remove();
            }
            id++;
        });
	}
	function checkAll(tableId){
		alert(tableId);
	}
	
	function searchJob(){
		var kriteriaPencarian = $('#kriteriaPencarian').find(":selected").val();
		var idText = '#search_'+kriteriaPencarian;
		var kataKunciPencarian = $('#kataKunciPencarian').val();
        $(idText).val(kataKunciPencarian);
        reloadoperationTableSearch();
        openDialog('dialog-reception-searchjob');
	}
	
	function addJobReception(idJob,kodeJob,namaJob,rate){
	    var allVar = idJob+kodeJob+namaJob+rate
	    if(cekJob.indexOf(allVar)<0){
            cekJob.push(idJob+kodeJob+namaJob+rate);
            var newRow = "<tr><td><input class='idJobs' type='hidden' id='"+idJob+"idJob' value='"+idJob+"'/>"+kodeJob+"</td><td>"+namaJob+"</td><td>"+rate+"</td><td><input type='text' id='noKeluhan"+idJob+"'/></td></tr>";
            $('#tableJob > tbody:last').append(newRow);
	    }
	    toastr.success("Add Success");
	}
	
	function saveJobInput(){
	    var km = $('#i_km').val()
	    if(parseInt(km) < parseInt(minKM)){
	        alert('KM saat ini harus lebih dari '+minKM);
	        return
	    }
	    if(km==null || km==""){
	        alert("Data belum lengkap, silahkan isi data dengan tanda (*)")
	        return
	    }
	    var aodataJob =  new Array();
		var idx = 1;
		var nameNoUrut = '';
		var nameKeluhan = '';
		var nameStaDiagnose = ''; 
		
		var idNoUrut = '';
		var idKeluhan = '';
		var idStaDiagnose = '';
		
		aodataJob.push(
			{"name": 'idReception', "value": $('#receptionId').val()},
			{"name": 'statusWarranty', "value": $('#staWarranty').val()},
			{"name": 'workshopSumber', "value": $('#workshopSumber').val()},
			{"name": 'workshopTujuan', "value": $('#workshopTujuan').val()},
			{"name": 'kategoriJob', "value": $('#kategoriJob').val()},
			{"name": 'kategoriJob', "value": $('#kategoriJob').val()},
			{"name": 'idVincode', "value": idVincode}
		);

		var idKirim = 0;
		while (idx < countKeluhan){
		    if(!keluhanDelete.indexOf(idx)){
                idKirim++;
                nameNoUrut = 't411NoUrut' + idKirim;
                nameKeluhan = 't411NamaKeluhan' + idKirim;
                nameStaDiagnose = 't411StaButuhDiagnose' + idKirim;

                idNoUrut = '#countKeluhan' + idx;
                idKeluhan = '#keluhan' + idx;
                idStaDiagnose = '#staDiagnose' + idx;

                //nourut
                aodataJob.push(
                    {"name": nameNoUrut, "value": $(idNoUrut).val()}
                );
                //keluhan
                aodataJob.push(
                    {"name": nameKeluhan, "value": $(idKeluhan).val()}
                );
                //stadiagnose
                aodataJob.push(
                    {"name": nameStaDiagnose, "value": $(idStaDiagnose).val()}
                );
                }
			idx++;
		}
        aodataJob.push(
			{"name": 'countRow', "value": idKirim}
		);

		
		
		var idx1 = 1;
		var nameIdJob = '';
		var nameKeluhans = '';
		
		var idKeluhans = '';
		$('.idJobs').each(
		    function() {
				nameIdJob = 'operation' + idx1;
				nameKeluhans = 't702NoUrutKeluhan' + idx1;
				
				idKeluhans = '#noKeluhan' + this.value;
				
				aodataJob.push(
					{"name": nameIdJob, "value": this.value}
				);

				aodataJob.push(
					{"name": nameKeluhans, "value": $(idKeluhans).val()}
				);
				idx1++;	
			}
		);

		if(idx1==1){
		    alert('Anda belum menambahkan Job');
		    return;
		}
		
		aodataJob.push(
			{"name": 'countRowJob', "value": idx1},
			{"name": 'kmSekarang', "value": km},
			{"name": 'staTambah', "value": "0"}
		);
		
		//saving Job and Keluhan
		$('#spinner').fadeIn(1);
		$.ajax({
			url:'${request.contextPath}/editJobParts/saveJobInput',
			type: "POST",
			async : false,
			data: aodataJob,
			success : function(data){
				$('#spinner').fadeOut();
				closeDialog('dialog-reception-jobkeluhan');
				reloadjobnPartsTable();
				reloadJoborderTable();
				initInputParts();
				openDialog('dialog-reception-joborder');
			},
			error: function(xhr, textStatus, errorThrown) {
				alert('Internal server error');
			}
		});
	}
	
	function initInputParts(){
		var id =  $('#receptionId').val();
		
		$.ajax({
			url:'${request.getContextPath()}/editJobParts/getInputParts?id='+id,
			type: 'POST',
			success: function (res) {
				$('#tableInputParts').find("tr:gt(0)").remove();
				$('#tableInputParts').append(res);
			},
			error: function (data, status, e){
				alert(e);
			},
			complete: function(xhr, status) {

			},
			cache: false,
			contentType: false,
			processData: false
		});
	}
</g:javascript>
<div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Add Job</h3>
</div>
<div class="modal-body">
     <div class="box">
         <table class="table">
            <tr>
                <td>
                    <div class="box">
                        Km Sekarang *&nbsp;&nbsp;&nbsp; <input type="text" class="numberonly" name="i_km" id="i_km" />
                    </div>
                </td>
                <td>
                    <div class="box">
                        Petugas Sekarang &nbsp;&nbsp;&nbsp; <span id="lblpetugas"></span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="box">
                        Keluhan & Permintaan Customer
                        <table class="" width="100%" cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px;">
                            <tr>
                                <th>
                                    Keluhan
                                </th>
                                <th>
                                    Butuh Diagnose
                                </th>
								<th>
                                    &nbsp;
                                </th>
                            </tr>
							<tr>
                                <td>
                                    <input type="text" name="inputKeluhan" id="inputKeluhan"/>
                                </td>
                                <td>
                                    <input type="radio" name="inputDiagnose" value="1" checked="true"/> Ya 
									</br>
									<input type="radio" name="inputDiagnose" value="2"/> Tidak
                                </td>
								<td>
									<input type="button" class="btn cancel" onclick="addKeluhan();" value="Add"/>
								</td>
                            </tr>
                        </table>
						
						<table id="tableKeluhan" class="display table table-striped table-bordered table-hover dataTable" width="100%" 
						cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px;">
                            <tr>
                                <th>                                    
									No
                                </th>
                                <th>
                                    Keluhan
                                </th>
                                <th>
                                    Butuh Diagnose
                                </th>
                            </tr>
                        </table>
						<table class="display table" width="100%" 
						cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px;">
							<tr>
                                <td>
                                    <input type="button" class="btn cancel" onclick="selectAllCheckbox('idKeluhan');" value="Select All"/>
                                    <input type="button" class="btn cancel" onclick="unselectAllCheckbox('idKeluhan');" value="Unselect All"/>
                                	<input type="button" class="btn cancel" onclick="deleteKeluhan();" value="Delete"/>
								</td>
								<td>
									<input type="button" class="btn cancel" onclick="showPrediagnose()" value="Diagnose [GR]"/>
								</td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td>
                    <div class="box">
                        Job Suggest
                        <textarea cols="200" id="txtJobSuggest" readonly="" style="resize: none" rows="5"/>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="box">
        <table class="table">
            <tr>
                <td>
                    Kategori Job *
                </td>
                <td colspan="2">
                    <g:select id="kategoriJob" name="kategoriJob" from="${com.kombos.administrasi.KategoriJob.createCriteria().list(){order("m055ID")}}" noSelection="['':'Pilih Kategori Job']" optionValue="${{it.m055KategoriJob}}" optionKey="id" required="" class="many-to-one"/>
                </td>
            </tr>
            <tr>
                <td>
                    Workshop (Sumber - Tujuan)
                </td>
                <td>
                    <g:select id="workshopSumber" name="workshopSumber" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list(){eq("staDel","0");order("m011ID")}}" noSelection="['':'Pilih Workshop Sumber']" optionValue="${{it.m011NamaWorkshop}}" optionKey="id" required="" class="many-to-one"/>
                </td>
                <td>
                    <g:select id="workshopTujuan" name="workshopTujuan" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list(){eq("staDel","0");order("m011ID")}}" noSelection="['':'Pilih Workshop Tujuan']" optionValue="${{it.m011NamaWorkshop}}" optionKey="id" required="" class="many-to-one"/>
                </td>
            </tr>
            <tr>
                <td>
					Status Warranty *
                </td> 
                <td colspan="2">
                    <g:select name="staWarranty" id="staWarranty" from="${com.kombos.maintable.StatusWarranty.createCriteria().list {order("m058NamaStatusWarranty")}}" noSelection="['':'Pilih Status Warranty']" optionKey="id" optionValue="m058NamaStatusWarranty" />
                </td>
            </tr>
            <tr>
                <td>
                    Kriteria Pencarian
                </td>
                <td colspan="2">
                    <select id="kriteriaPencarian" name="kriteriaPencarian">
						<option value="m053Id">Kode Job</option>
						<option value="m053NamaOperation">Nama Job</option>
					</select>
                </td>
            </tr>
            <tr>
                <td>
                    Kata Kunci Pencarian
                </td>
                <td colspan="2">
                    <input type="text" id="kataKunciPencarian" name="kataKunciPencarian"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <input class="btn" type="button" value="Search" onclick="searchJob();"/>
                    <input class="btn" type="button" value="Input Job Baru" onclick="openDialog('dialog-reception-inputjob');"/>
                </td>
            </tr>
        </table>
    </div>
    <div class="box">
        <div class="dataTables_scroll">
            <table id="tableJob" class="display table table-striped table-bordered table-hover dataTable" width="100%" cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px;">
                <tr>
                    <th>
                        Kode Job
                    </th>
                    <th>
                        Nama Job
                    </th>
                    <th>
                        Rate
                    </th>
                    <th>
                        No Keluhan
                    </th>
                </tr>
                %{--
				<g:if test="${htmlData}">
                    ${htmlData}
                </g:if>
                <g:else>
                    <tr class="odd">
                        <td class="dataTables_empty" valign="top" colspan="9">No data available in table</td>
                    </tr>
                </g:else>
				--}%
            </table>
        </div>
    </div>
</div>
<div class="modal-footer">
    <a onclick="saveJobInput();" class="btn btn-success" id="add_part_btn">Save</a>
    <a href="#" class="btn" data-dismiss="modal">Close</a>
</div>