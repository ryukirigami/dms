<%@ page import="com.kombos.administrasi.KegiatanApproval; com.kombos.reception.Reception" %>
<div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Send Approval Special Discount</h3>
</div>
<g:javascript>
    $(function(){
        sendDiscount = function(){
            var noWo = $('#noWoApprovalDiscount').val();
            var pesan = $('#pesanApprovalDiscount').val();
            var kegiatanApproval = $('#kegiatanApprovalDiscount').val();
            $.ajax({
                    url:'${request.contextPath}/editJobParts/approvalDiscount',
                    type: "POST", // Always use POST when deleting data
                    data : {noWo : noWo,pesan:pesan,kegiatanApproval:kegiatanApproval},
                    success : function(data){
                        toastr.success('<div>Approval Sudah Dikirim</div>');
                        reloadjobnPartsTable();
                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });
        };

    });
</g:javascript>
<div class="modal-body">
    <div class="box">
        <table class="table">
            <tbody>
            <tr>
                <td>
                    Nama Kegiatan *
                </td>
                <td>
                    <g:select id="kegiatanApprovalDiscount" name="kegiatanApprovalDiscount" from="${com.kombos.administrasi.KegiatanApproval.list()}"  value="${KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.REQUEST_SPECIAL_DISCOUNT)?.id}" optionKey="id" required="" class="many-to-one"/>
                </td>
            </tr>
            <tr>
                <td>
                    Nomor WO
                </td>
                <td>
                    <g:textField name="noWoApprovalDiscount" id="noWoApprovalDiscount" readonly="" />
                </td>
            </tr>
            <tr>
                <td>
                    Nomor Polisi
                </td>
                <td>
                    <g:textField name="txtnopolDiscount" id="txtnopolDiscount" readonly="" />
                </td>
            </tr>
            <tr>
                <td>
                    Model Kendaraan
                </td>
                <td>
                    <g:textField name="txtmodelDiscount" id="txtmodelDiscount" readonly="" />
                </td>
            </tr>
            <tr>
                <td>
                    Tanggal Permohonan
                </td>
                <td>
                    ${new Date().format("dd-MM-yyyy HH:mm")}
                </td>
            </tr>
            <tr>
                <td>
                    Pesan
                </td>
                <td>
                    <textarea id="pesanApprovalDiscount" name="pesanApprovalDiscount" cols="200" rows="3"></textarea>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="modal-footer">
    <a href="#" class="btn" onclick="sendDiscount()" data-dismiss="modal">Send</a>
    <a href="#" class="btn" data-dismiss="modal">Close</a>
</div>