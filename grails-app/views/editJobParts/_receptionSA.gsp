<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<input type="hidden" id="idSAUbah">

<div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Ubah Nama SA</h3>
</div>
<div class="modal-body">
    <div class="span12" id="companyDealerPopUp-table">
        <table id="SAReception_table" cellpadding="10" cellspacing="10"
               border="0"
               class="display table table-striped table-bordered table-hover fixed" style="table-layout: fixed; ">
            <col width="150px" />
            <col width="200px" />
            <thead>
            <tr>
                <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:150px;">
                    Username
                </th>
                <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:200px;">
                    Nama Lengkap
                </th>
            </tr>
            <tr>
                <th style="border-top: none;padding: 0px 0px 5px 0px; width:150px;">
                    <div id="filter_username" style="padding-top: 0px;position:relative; margin-top: 0px;width: 145px;">
                        <input type="text" name="username" class="search_init" />
                    </div>
                </th>

                <th style="border-top: none;padding: 0px 0px 5px 0px; width:200px;">
                    <div id="filter_namaPegawai" style="padding-top: 0px;position:relative; margin-top: 0px;width: 145px;">
                        <input type="text" name="nama" class="search_init" />
                    </div>
                </th>
            </tr>
            </thead>
        </table>

        <g:javascript>
var SAReceptionTable;
var reloadSAReceptionTable;
var setValuesSA;
$(function(){
    setValuesSA = function(el,noReff) {
        var idReff = noReff;
        $("#idSAUbah").val(idReff);
	}

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	SAReceptionTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	SAReceptionTable = $('#SAReception_table').dataTable({
		"sScrollX": "600px",
		"bScrollCollapse": true,
		"bAutoWidth" : true,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : 5, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(controller: "reception", action: "SADatatables")}",
		"aoColumns": [

{
	"sName": "username",
	"mDataProp": "username",
	"aTargets": [1],
	"bSortable": false,
	"bSearchable": true,
	"bVisible": true,
	"mRender": function ( data, type, row ) {
	    return "<input type='radio' onclick='setValuesSA(this,\""+data+"\");' name='username'> &nbsp; " + data;
	}
},
{
    "sName": "nama",
	"mDataProp": "nama",
	"aTargets": [2],
	"bSortable": false,
	"bSearchable": true,
	"sClass": "nama",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var nama = $('#filter_namaPegawai input').val();

						if(nama){
							aoData.push(
									{"name": 'sCriteria_namalengkap', "value": nama}
							);
						}

						var username = $('#filter_username input').val();

						if(username){
							aoData.push(
									{"name": 'sCriteria_username', "value": username}
							);
						}



						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
        </g:javascript>
    </div>
</div>
<div class="modal-footer">
    <a onclick="ubahSA();" href="javascript:void(0);" class="btn btn-primary create" id="ubah_SA_btn" data-dismiss="modal">Ganti</a>
    <a href="#" class="btn" data-dismiss="modal">Close</a>
</div>