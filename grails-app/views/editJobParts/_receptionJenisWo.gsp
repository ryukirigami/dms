<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<input type="hidden" id="idJenisUbah">

<div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h5>Ubah Jenis Wo</h5>
</div>
<div class="modal-body">
    <div class="span12" id="companyDealerPopUp-table">
        <table id="jenisReception_table" cellpadding="10" cellspacing="10"
               border="0"
               class="display table table-striped table-bordered table-hover fixed" style="table-layout: fixed; ">
            <col width="80px" />
            <col width="160px" />
            <thead>
            <tr>
                <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:80px;">
                    <span id="filter_jenis" style="padding-top: 0px;position:relative; margin-top: 0px;width: 60px;">
                        Jenis
                    </span>
                </th>
                <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:160px;">
                    <span id="filter_data" style="padding-top: 0px;position:relative; margin-top: 0px;width: 160px;">
                        Nama
                    </span>
                </th>
            </tr>
            <tr>
                <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:80px;">
                </th>
                <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:160px;">
                </th>
            </tr>
            </thead>
        </table>

        <g:javascript>
var jenisReceptionTable;
var reloadjenisReceptionTable;
var setValuesData;
$(function(){
    setValuesData = function(el,noReff) {
        var idReff = noReff;
        $("#idJenisUbah").val(idReff);
	}

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	jenisReceptionTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	jenisReceptionTable = $('#jenisReception_table').dataTable({
		"sScrollX": "300px",
		"bScrollCollapse": true,
		"bAutoWidth" : true,
		"bPaginate" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : 5, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(controller: "reception", action: "jenisWODatatables")}",
		"aoColumns": [

{
	"sName": "jenis",
	"mDataProp": "jenis",
	"aTargets": [1],
	"bSortable": false,
	"bSearchable": true,
	"bVisible": true,
	"mRender": function ( data, type, row ) {
	    return "<input type='radio' onclick='setValuesData(this,"+row['id']+");' name='nik'> &nbsp; " + data;
	}
},
{
    "sName": "nama",
	"mDataProp": "nama",
	"aTargets": [2],
	"bSortable": false,
	"bSearchable": true,
	"sClass": "tglReff",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
        </g:javascript>
    </div>
</div>
<div class="modal-footer">
    <a onclick="ubahJenisWo();" href="javascript:void(0);" class="btn btn-primary create" id="ubah_jenis_btn" data-dismiss="modal">Ganti</a>
    <a href="#" class="btn" data-dismiss="modal">Close</a>
</div>