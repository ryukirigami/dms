<%@ page import="com.kombos.administrasi.KegiatanApproval; com.kombos.reception.Reception" %>
<div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Approval - Permohonan Approval</h3>
</div>
<g:javascript>
    $(function(){
        sendPengurangan = function(){
            var noWo = $('#noWoPengurangan').val();
            var pesan = $('#pesanApprovalPengurangan').val();
            var kegiatanApproval = $('#kegiatanApprovalPengurangan').val();
            var listJob = JSON.stringify(listJobKurangi);
            var listPart = JSON.stringify(listPartKurangi);

            $.ajax({
                    url:'${request.contextPath}/editJobParts/approvalKurangiJob',
                    type: "POST", // Always use POST when deleting data
                    data : {noWo : noWo,pesan:pesan,kegiatanApproval:kegiatanApproval,jobs:listJob,parts:listPart},
                    success : function(data){
                        if(data =="ok"){
                            toastr.success('<div>Approval Sudah Dikirim</div>');
                            reloadjobnPartsTable();
                        }else{
                             alert("Tidak Bisa dikurangi, karena ada parts yang sudah di anfrak. \n \n " + data);
                        }

                    },
                error: function(xhr, textStatus, errorThrown) {
                    alert('Internal Server Error');
                },complete : function(){
                    listJobKurangi = []
                    listPartKurangi=[]
                }
                });
        };

    });
</g:javascript>
<div class="modal-body">
    <div class="box">
        <table class="table">
            <tbody>
            <tr>
                <td>
                    Nama Kegiatan *
                </td>
                <td>
                    <g:select id="kegiatanApprovalPengurangan" name="kegiatanApprovalPengurangan" from="${com.kombos.administrasi.KegiatanApproval.list()}"  value="${KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.PENGURANGAN_JOB_ATAU_PART).id}" optionKey="id" required="" class="many-to-one"/>
                </td>
            </tr>
            <tr>
                <td>
                    Nomor WO
                </td>
                <td>
                    <g:textField name="noWoPengurangan" id="noWoPengurangan" readonly="" />
                </td>
            </tr>
            <tr>
                <td>
                    Tanggal dan Jam
                </td>
                <td>
                    ${new Date().format("dd-MM-yyyy HH:mm")}
                </td>
            </tr>
            <tr>
                <td>
                    Pesan
                </td>
                <td>
                    <textarea id="pesanApprovalPengurangan" name="pesanApprovalPengurangan" cols="200" rows="3"></textarea>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="modal-footer">
    <a href="#" class="btn" onclick="sendPengurangan()" data-dismiss="modal">Send</a>
    <a href="#" class="btn" data-dismiss="modal">Close</a>
</div>