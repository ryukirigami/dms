

<%@ page import="com.kombos.finance.PersenTarget" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'persenTarget.label', default: 'Persen Target')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deletePersenTarget;

$(function(){ 
	deletePersenTarget=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/persenTarget/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadPersenTargetTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-persenTarget" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="persenTarget"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${persenTargetInstance?.companyDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="companyDealer-label" class="property-label"><g:message
					code="persenTarget.companyDealer.label" default="Company Dealer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="companyDealer-label">
						%{--<ba:editableValue
								bean="${persenTargetInstance}" field="companyDealer"
								url="${request.contextPath}/PersenTarget/updatefield" type="text"
								title="Enter companyDealer" onsuccess="reloadPersenTargetTable();" />--}%
							
								<g:fieldValue bean="${persenTargetInstance?.companyDealer}" field="m011NamaWorkshop"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${persenTargetInstance?.accountNumber}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="accountNumber-label" class="property-label"><g:message
					code="persenTarget.accountNumber.label" default="Account Nuber" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="accountNumber-label">
							
								<g:fieldValue bean="${persenTargetInstance?.accountNumber}" field="accountNumber"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${persenTargetInstance?.persenGR}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="persenGR-label" class="property-label"><g:message
					code="persenTarget.persenGR.label" default="Persen GR" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="persenGR-label">
						%{--<ba:editableValue
								bean="${persenTargetInstance}" field="persenGR"
								url="${request.contextPath}/PersenTarget/updatefield" type="text"
								title="Enter persenGR" onsuccess="reloadPersenTargetTable();" />--}%
							
								<g:fieldValue bean="${persenTargetInstance}" field="persenGR"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${persenTargetInstance?.persenBP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="persenBP-label" class="property-label"><g:message
					code="persenTarget.persenBP.label" default="Alamat" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="persenBP-label">
						%{--<ba:editableValue
								bean="${persenTargetInstance}" field="persenBP"
								url="${request.contextPath}/PersenTarget/updatefield" type="text"
								title="Enter persenBP" onsuccess="reloadPersenTargetTable();" />--}%
							
								<g:fieldValue bean="${persenTargetInstance}" field="persenBP"/>
							
						</span></td>
					
				</tr>
				</g:if>


				<g:if test="${persenTargetInstance?.evtGroup}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="persenBP-label" class="property-label"><g:message
					code="persenTarget.evtGrup.label" default="Evt Grup" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="evtGrup-label">
						%{--<ba:editableValue
								bean="${persenTargetInstance}" field="persenBP"
								url="${request.contextPath}/PersenTarget/updatefield" type="text"
								title="Enter persenBP" onsuccess="reloadPersenTargetTable();" />--}%

								<g:fieldValue bean="${persenTargetInstance}" field="evtGroup"/>

						</span></td>

				</tr>
				</g:if>



            <g:if test="${persenTargetInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="persenTarget.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${persenTargetInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/persenTarget/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadpersenTargetTable();" />--}%

                        <g:fieldValue bean="${persenTargetInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${persenTargetInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="persenTarget.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${persenTargetInstance}" field="dateCreated"
                                url="${request.contextPath}/persenTarget/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadpersenTargetTable();" />--}%

                        <g:formatDate date="${persenTargetInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${persenTargetInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="persenTarget.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${persenTargetInstance}" field="createdBy"
                                url="${request.contextPath}/persenTarget/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadpersenTargetTable();" />--}%

                        <g:fieldValue bean="${persenTargetInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${persenTargetInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="persenTarget.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${persenTargetInstance}" field="lastUpdated"
                                url="${request.contextPath}/persenTarget/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadpersenTargetTable();" />--}%

                        <g:formatDate date="${persenTargetInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${persenTargetInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="persenTarget.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${persenTargetInstance}" field="updatedBy"
                                url="${request.contextPath}/persenTarget/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadpersenTargetTable();" />--}%

                        <g:fieldValue bean="${persenTargetInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
%{--				<g:if test="${persenTargetInstance?.m702staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m702staDel-label" class="property-label"><g:message
					code="persenTarget.m702staDel.label" default="M702sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m702staDel-label">
						<ba:editableValue
								bean="${persenTargetInstance}" field="m702staDel"
								url="${request.contextPath}/PersenTarget/updatefield" type="text"
								title="Enter m702staDel" onsuccess="reloadPersenTargetTable();" />
							
								<g:fieldValue bean="${persenTargetInstance}" field="m702staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>--}%
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${persenTargetInstance?.id}"
					update="[success:'persenTarget-form',failure:'persenTarget-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deletePersenTarget('${persenTargetInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
