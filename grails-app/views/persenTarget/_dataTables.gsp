
<%@ page import="com.kombos.finance.PersenTarget" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="persenTarget_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="persenTarget.companyDealer.label" default="Company Dealer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="persenTarget.accountNumber.label" default="Akun" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="persenTarget.persenGR.label" default="Persen GR" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="persenTarget.persenBP.label" default="Persen BP" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="persenTarget.evtGrup.label" default="Evt Grup" /></div>
			</th>


		</tr>
		<tr>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_companyDealer" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_accountNumber" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_accountNumber" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_persenGR" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_persenGR" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_persenBP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_persenBP" class="search_init" />
				</div>
			</th>

					<th style="border-top: none;padding: 5px;">
				<div id="filter_evtGrup" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_evtGrup" class="search_init" />
				</div>
			</th>

		</tr>
	</thead>
</table>

<g:javascript>
var PersenTargetTable;
var reloadPersenTargetTable;
$(function(){
	
	reloadPersenTargetTable = function() {
		PersenTargetTable.fnDraw();
	}

    var recordspersenTargetperpage = [];
    var anPersenTargetSelected;
    var jmlRecPersenTargetPerPage=0;
    var id;

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	PersenTargetTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	PersenTargetTable = $('#persenTarget_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsPersenTarget = $("#persenTarget_datatables tbody .row-select");
            var jmlPersenTargetCek = 0;
            var nRow;
            var idRec;
            rsPersenTarget.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordspersenTargetperpage[idRec]=="1"){
                    jmlPersenTargetCek = jmlPersenTargetCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordspersenTargetperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecPersenTargetPerPage = rsPersenTarget.length;
            if(jmlPersenTargetCek==jmlRecPersenTargetPerPage && jmlRecPersenTargetPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "companyDealer",
	"mDataProp": "companyDealer",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "accountNumber",
	"mDataProp": "accountNumber",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "persenGR",
	"mDataProp": "persenGR",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "persenBP",
	"mDataProp": "persenBP",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "evtGrup",
	"mDataProp": "evtGrup",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var accountNumber = $('#filter_accountNumber input').val();
						if(accountNumber){
							aoData.push(
									{"name": 'sCriteria_accountNumber', "value": accountNumber}
							);
						}
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}
	
						var persenBP = $('#filter_persenBP input').val();
						if(persenBP){
							aoData.push(
									{"name": 'sCriteria_persenBP', "value": persenBP}
							);
						}
	
						var persenGR = $('#filter_persenGR input').val();
						if(persenGR){
							aoData.push(
									{"name": 'sCriteria_persenGR', "value": persenBP}
							);
						}
	
						var evtGrup = $('#filter_evtGrup input').val();
						if(evtGrup){
							aoData.push(
									{"name": 'sCriteria_evtGrup', "value": persenBP}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

	$('.select-all').click(function(e) {
        $("#persenTarget_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordspersenTargetperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordspersenTargetperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

    $('#persenTarget_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordspersenTargetperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anPersenTargetSelected = PersenTargetTable.$('tr.row_selected');
            if(jmlRecPersenTargetPerPage == anPersenTargetSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordspersenTargetperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
