
<%@ page import="com.kombos.administrasi.NamaManPower; com.kombos.administrasi.Operation; com.kombos.administrasi.NamaProsesBP; com.kombos.parts.Returns" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'returnsAdd.label', default: 'Rate Teknisi')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
        return false;
	});


        editRateTeknisi = function(){
                var teknisi = $('#teknisi').val();
                var noWo = $('#nomorWO').val();
                var dataKu = $('#job').val();
                var dataKu2 = dataKu.split('.');
                var job = dataKu2[0];
                var rateTeknisi = $('#rateTeknisi').val();
                $.ajax({
                    url:'${request.contextPath}/woDetailGr/editRateTeknisi',
                    type: "POST", // Always use POST when deleting data
                    data : {noWo:noWo,job:job,teknisi:teknisi,rateTeknisi:rateTeknisi},
                    success : function(data){
                        toastr.success('<div>Ubah Sukses</div>');
                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });

        }
   });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
    <ul class="nav pull-right">
        <li></li>
        <li></li>
        <li class="separator"></li>
    </ul>
</div>
<div class="box">
    <fieldset>
        <div class="row-fluid">
                <table style="width: 95%;">


                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Nomor WO" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="nomorWO" id="nomorWO" value="${noWo}" readonly="" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Nomor Polisi" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="nomorPolisi" readonly="" value="${nopol}" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Model Kendaraan" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="modelKendaraan" readonly="" value="${model}"/>
                        </td>
                    </tr>

                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Nama Stall" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="namaCustomer" readonly="" value="${stall}" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Tanggal WO" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="namaCustomer" readonly="" value="${tanggalWo}" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Job" />
                        </td>
                        <td>
                            %{--<g:select name="job" id="job" from="${Operation.list()}" />--}%
                            <g:textField style="width:100%" name="namaJob" readonly="" value="${namaJob}" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Inisial & Nama Teknisi" />
                        </td>
                        <td>
                            %{--<g:select name="teknisi" id="teknisi" from="${NamaManPower.list()}" />--}%
                            <g:textField style="width:100%" name="namaJob" readonly="" value="${teknisi}" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Rate Teknisi" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="rateTeknisi" id="rateTeknisi" value="${findRate}" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Tanggal Revisi" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="tanggalRevisi" readonly="" value="${tanggal}" />
                        </td>
                    </tr>

                    <tr>
                        <td style="padding: 5px" colspan="2">
                            <button id='btn1' onclick="editRateTeknisi();" type="button" class="btn btn-cancel" style="width: 150px;">Ok</button>
                            <button id='btn2' type="button" class="btn btn-cancel" style="width: 150px;">Close</button>
                        </td>
                    </tr>
                </table>
        </div>
    </fieldset>
    </div>
</div>
</body>
</html>

