
<%@ page import="com.kombos.customerprofile.HistoryCustomerVehicle; com.kombos.reception.Reception; com.kombos.administrasi.Stall" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="woDetailGr.label" default="Wo Detail" /></title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>
			var show;
			var edit;
            if($("#search_nomorWO").val()){
                findData();
            }
            finalInspection = function() {
                var nRow = $('#search_nomorWO').val();
                 if(nRow==""){
                     alert('Isi Nomor Wo');
                }else{
                    shrinkTableLayout();
                    $('#spinner').fadeIn(1);
                    $.ajax({type:'POST', url:'${request.contextPath}/woDetailGr/finalInspection',
                        data : {noWo : nRow},
                        success:function(data,textStatus){
                            loadForm(data, textStatus);
                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
                }
            };
            loadForm = function(data, textStatus){
                $('#final-form').empty();
                $('#final-form').append(data);
            }

            shrinkTableLayout = function(){
                $("#woDetailGr-table").hide()
                $("#final-form").css("display","block");
            }

            expandTableLayout = function(){
                $("#woDetailGr-table").show()
                $("#final-form").css("display","none");
            }
			 //darisini
            $("#wojalanAddModal").on("show", function() {
                $("#wojalanAddModal .btn").on("click", function(e) {
                    $("#wojalanAddModal").modal('hide');
                });
            });
            $("#wojalanAddModal").on("hide", function() {
                $("#wojalanAddModal a.btn").off("click");
            });

            $("#revisiAddModal").on("show", function() {
                $("#revisiAddModal .btn").on("click", function(e) {
                    $("#revisiAddModal").modal('hide');
                });
            });
            $("#revisiAddModal").on("hide", function() {
                $("#revisiAddModal a.btn").off("click");
            });


			 function aproval(){
                var noWo = $('#nomorWO').val();
                if(noWo==""){
                     alert('Isi Nomor Wo');
                }else{
                    $("#wojalanAddContent").empty();
                    expandTableLayout();
                    $.ajax({type:'POST', url:'${request.contextPath}/woDetailGr/woJalan',
                    data : {noWo : noWo},
                        success:function(data,textStatus){
                                $("#wojalanAddContent").html(data);
                                $("#wojalanAddModal").modal({
                                    "backdrop" : "static",
                                    "keyboard" : true,
                                    "show" : true
                                }).css({'width': '880px','margin-left': function () {return -($(this).width() / 2);}});

                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
                }
            }

            function revisiJamJanji(){
                var noWo = $('#nomorWO').val();
                if(noWo==""){
                     alert('Isi Nomor Wo');
                }else{
                    $("#revisiAddContent").empty();
                    expandTableLayout();
                    $.ajax({type:'POST', url:'${request.contextPath}/woDetailGr/revisi',
                    data : {noWo : noWo},
                        success:function(data,textStatus){
                                $("#revisiAddContent").html(data);
                                $("#revisiAddModal").modal({
                                    "backdrop" : "static",
                                    "keyboard" : true,
                                    "show" : true
                                }).css({'width': '880px','margin-left': function () {return -($(this).width() / 2);}});

                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
                }
            }

            function rateTeknisi(idReception,namaJob,teknisi){
                var noWo = idReception;
                var namaJob = namaJob;
                var teknisi = teknisi;
                alert(noWo+" -- " + namaJob + " -- " + teknisi);
                if(noWo==""){
                     alert('Isi Nomor Wo');
                }else{
                    $("#wojalanAddContent").empty();
                    expandTableLayout();
                    $.ajax({type:'POST', url:'${request.contextPath}/woDetailGr/rateTeknisi',
                    data : {noWo : noWo,namaJob:namaJob,teknisi:teknisi},
                        success:function(data,textStatus){
                                $("#wojalanAddContent").html(data);
                                $("#wojalanAddModal").modal({
                                    "backdrop" : "static",
                                    "keyboard" : true,
                                    "show" : true
                                }).css({'width': '880px','margin-left': function () {return -($(this).width() / 2);}});

                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
                }
            }

        function isiData(noWo){
            var id=noWo;
            $.ajax({
                url:'${request.contextPath}/woDetailGr/getTableBiayaData',
                type: "POST",
                data: { noWO : id },
                success : function(data){
                    if(data.length>0){
                        $.each(data,function(i,item){
                            biayaTable.fnAddData({
                                'namaJob': item.namaJobParts,
                                'hargaJob': item.hargaJob,
                                'hargaParts': item.hargaParts,
                                'total': item.total
                            });
                        });
                    }
                }
            });

            $.ajax({
                url:'${request.contextPath}/woDetailGr/getTableRateData',
                type: "POST",
                data: { noWO : id },
                success : function(data){
                     jQuery.each(data, function (index, value) {
                       rateTeknisiTable.fnAddData({
                            'id' :  value.id,
                            'namaJob':value.namaJob,
                            'teknisi': value.teknisi,
                            'rate': value.rate,
                             'noWo' : id
                        });
                      });
                }
            });
        }
        function findWoTerkait(){
            var noWo = $('#search_nomorWO').val();
            $.ajax({
                url:'${request.contextPath}/woDetailGr/findWoTerkait',
                type: "POST",
                data : { noWo : noWo },
                success : function(data){
                        $('#woTerkait').empty();
                        jQuery.each(data, function (index, value) {
                                $('#woTerkait').append("<option value='" + value.id + "'>" + value.noWo + "</option>");
                         });
                },
                error: function(xhr, textStatus, errorThrown) {
                    alert('Internal Server Error');
                }
            });

        }
        function findData(){
            var noWO = $('#search_nomorWO').val();
            $.ajax({
                url:'${request.contextPath}/woDetailGr/findData',
                type: "POST",
                data : { noWO : noWO },
                success : function(data){

                    if(data.length>0){
                     reloadBiayaTable();
                     reloadRateTeknisiTable();
                        isiData(noWO);
                        $('#search_nomorWO').val(noWO)
                        $('#noPolAwal').val(data[0].noPolAwal);
                        $('#noPolTengah').val(data[0].noPolTengah);
                        $('#noPolAkhir').val(data[0].noPolAkhir);
                        $('#nomorWO').val(noWO);
                        $('#nomorPolisi').val(data[0].noPolisi)
                        $('#modelKendaraan').val(data[0].modelKendaraan)
                        $('#namaStall').val(data[0].namaStall)
                        $('#tanggalWO').val(data[0].tanggalWO)
                        $('#waktuPenyerahan').val(data[0].waktuPenyerahan)
                        $('#namaCustomer').val(data[0].namaCustomer)
                        $('#alamatCustomer').val(data[0].alamatCustomer)
                        $('#telponCustomer').val(data[0].telponCustomer)
//                        $('#keluhan').val(data[0].keluhan)
                        $('#permintaan').val(data[0].permintaan)
//                        $('#fir').val(data[0].fir)
                        $('#staKendaraan').val(data[0].staKendaraan)
                        if(data[0].staObatJalan=="1"){
                            $('#btn4').prop("disabled",true);
                        }else{
                            $('#btn4').prop("disabled",false);
                        }
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    alert('Internal Server Error');
                }
            });

        };

        function addJobParts(){
            var vNowo = $('#search_nomorWO').val();
            if(vNowo==""){
                alert('Data masih kosong');
                return
            }
            window.location.href='#/EditJobParts';
            $('#spinner').fadeIn(1);
            $.ajax({
                url: '${request.contextPath}/editJobParts?nowo='+vNowo,
                type: "GET",dataType:"html",
                complete : function (req, err) {
                    $('#main-content').html(req.responseText);
                    $('#spinner').fadeOut();
                }
            });
        }

        function backButton(){
            loadPath('jobForTeknisi/index');
        }
    </g:javascript>
</head>

<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="woDetailGr.label" default="WO Detail" /></span>
    <ul class="nav pull-right">
    </ul>
</div>
<div class="box">
    <div class="span12" id="woDetailGr-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>

        <table style="width: 50%" >
            <tr>
                <td style="padding: 5px">
                    <g:message code="wo.category1.search.label" default="Nomor WO" />
                </td>
                <td>
                    <g:textField style="width:220px" name="search_nomorWO" id="search_nomorWO" value="${noWo}" />
                </td>
                <td>
                    <g:field type="button" style="padding: 5px;width: 100px" class="btn cancel" onclick="findData();"
                             name="view" id="view" value="${message(code: 'default.search.label', default: 'Search')}" />
                </td>
            </tr>

            <tr>
                <td style="padding: 5px">
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    <g:message code="wo.key2.label" default="Nomor Polisi" />
                </td>
                <td style="padding: 5px">
                    <g:textField style="width:40px" name="noPolAwal" id="noPolAwal" maxlength="2"   onblur="fromNoPol=true;" />
                    <g:textField style="width:100px" name="noPolTengah" id="noPolTengah"   onkeypress="return isNumberKey(event);" onblur="fromNoPol=true;" maxlength="5" />
                    <g:textField style="width:40px" name="noPolAkhir" id="noPolAkhir"   maxlength="3" onblur="fromNoPol=true;" />
                </td>
                <td>
                    <g:field type="button" style="padding: 5px;width: 130px" class="btn cancel" onclick="findWoTerkait();"
                             name="view" id="view" value="${message(code: 'default.search.label', default: 'Search Wo Terkait')}" />
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    <g:message code="wo.key2.label" default="Wo Terkait" />
                </td>
                <td>
                    <select name="woTerkait" id="woTerkait" style="width:300px" size="5"></select>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>

        </table>

        <fieldset>
            <legend>WO Detail</legend>
            <div class="row-fluid">
                <div id="kiri" class="span5">
                    <table style="width: 100%;">
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.t401NoWo.label" default="Nomor WO" />
                            </td>
                            <td>
                                <g:textField style="width:100%"   name="nomorWO" id="nomorWO" readonly="" value="${receptionInstance?.t401NoWO}"  />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.noPolisi.label" default="Nomor Polisi" />
                            </td>
                            <td>
                                <g:textField style="width:100%" id="nomorPolisi" name="nomorPolisi" readonly="" value="${receptionInstance?.historyCustomerVehicle?.fullNoPol}"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.modelKendaraan.label" default="Model Kendaraan" />
                            </td>
                            <td>
                                <g:textField style="width:100%" id="modelKendaraan" name="modelKendaraan" readonly="" value="${receptionInstance?.historyCustomerVehicle?.fullModelCode?.modelName?.m104NamaModelName}" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.namaStall.label" default="Nama Stall" />
                            </td>
                            <td>
                                <g:textField style="width:100%" name="namaStall" id="namaStall"  readonly="" value="${getStall}" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.tanggalWO.label" default="Tanggal WO" />
                            </td>
                            <td>
                                <g:textField style="width:100%" name="tanggalWO" id="tanggalWO" readonly=""  value="${tangalWO}"  />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.tanggalPenyerahan.label" default="Tanggal Penyerahan" />
                            </td>
                            <td>
                                <g:textField style="width:100%" name="waktuPenyerahan"  id="waktuPenyerahan" readonly="" value="${tanggalPenyerahan}" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.namaCustomer.label" default="Nama Customer" />
                            </td>
                            <td>
                                <g:textField style="width:100%" name="namaCustomer" id="namaCustomer" readonly="" value="${namaCustomer}"  />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.alamatCustomer.label" default="Alamat Customer" />
                            </td>
                            <td>
                                <g:textArea style="width:100%;resize: none" name="alamatCustomer" id="alamatCustomer"  readonly="" value="${receptionInstance?.historyCustomer?.t182Alamat}"  />
                            </td>
                        </tr>
                         <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.teleponCustomer.label" default="Telepon Customer" />
                            </td>
                            <td>
                                <g:textField  style="width:100%" name="telponCustomer"  id="telponCustomer" readonly=""  value="${receptionInstance?.historyCustomer?.t182NoHp}"  />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.keluhan.label" default="Keluhan" />
                            <td>
                               %{--<g:textField style="width:100%; resize: none" name="keluhan" id="keluhan" readonly="" value="osdofsod"/>--}%
                                <g:textArea style="width:100%;resize: none" name="keluhan" id="keluhan" readonly="" value="${keluhan}"  />

                        </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.permintaan.label" default="Permintaan" />
                            </td>
                            <td>
                                <g:textArea style="width:100%;resize: none" name="permintaan" id="permintaan"  readonly="" value="${receptionInstance?.t401PermintaanCusts.each {it.permintaan}}"  />
                            </td>
                        </tr>

                    </table>
                </div>

                <div id="kanan" class="span7">
                    <fieldset>
                        <legend style="font-size: 14px">
                            <g:message code="wo.biaya.label" default="Biaya" />
                        </legend>
                        <div style="width: 100%" >
                            <g:render template="dataTablesBiaya" />
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend style="font-size: 14px">
                            <g:message code="wo.rateTeknisi.label" default="Rate Teknisi" />
                        </legend>
                        <div style="width: 100%" >
                            <g:render template="dataTablesRateTeknisi" />
                        </div>
                    </fieldset>
                </div>
            </div>
        </fieldset>
        <fieldset class="buttons controls" style="padding-top: 10px;">
            <button id='btn1' onclick="addJobParts();" type="button" class="btn btn-cancel" style="width: 100px;">Add New job</button>
            <button id='btn2' onclick="addJobParts();" type="button" class="btn btn-cancel" style="width: 150px;">Add New Parts</button>
            <button id='btn3' onclick="finalInspection();" type="button" class="btn btn-primary" style="width: 150px;">Final Inspection</button>
            <button id='btn4' onclick="aproval();" type="button" class="btn btn-cancel" style="width: 250px;">Aproval Close WO Berobat Jalan</button>
            <button id='btn5' onclick="revisiJamJanji();" type="button" class="btn btn-cancel" style="width: 200px;">Revisi Jam Janji Penyerahan</button>
            <button id='btn6' onclick="backButton();" type="button" class="btn btn-cancel" style="width: 200px;">Job For Teknisi</button>
        </fieldset>
    </div>
    <div class="span7" id="final-form" style="display: none; width:95%"></div>

</div>
<div id="wojalanAddModal" class="modal fade"style="width: 800px;">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content" style="width: 800px;">
            <!-- dialog body -->
            <div class="modal-body" style="max-height: 800px; width: 800px;">
                <div id="wojalanAddContent"/>
                %{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}%
                <div class="iu-content"></div>
            </div>

        </div>
    </div>
</div>

<div id="revisiAddModal" class="modal fade"style="width: 800px;">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content" style="width: 800px;">
            <!-- dialog body -->
            <div class="modal-body" style="max-height: 800px; width: 800px;">
                <div id="revisiAddContent"/>
                <div class="iu-content"></div>
            </div>

        </div>
    </div>
</div>

</body>


<g:javascript>
    $(function(){
        $('#woTerkait').empty();
    });
</g:javascript>
</html>
