
<%@ page import="com.kombos.administrasi.Operation; com.kombos.administrasi.NamaProsesBP; com.kombos.parts.Returns" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'returnsAdd.label', default: 'Revisi Jam Janji Penyerahan')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
        return false;
	});


        editRevisi = function(){
                var pesan = $('#pesan').val();
                var jamBaru = $('#jamBaru').val();
                var menitBaru = $('#menitBaru').val();
                var tanggalBaru = $('#tanggalBaru').val();
                var nomorWO = $('#nomorWO').val();
                $.ajax({
                    url:'${request.contextPath}/woDetailGr/editRevisi',
                    type: "POST", // Always use POST when deleting data
                    data : {pesan:pesan,noWo:nomorWO,tanggalBaru:tanggalBaru,jamBaru:jamBaru,menitBaru:menitBaru},
                    success : function(data){
                        toastr.success('<div>Jam Janji Penyerahan Sukses Diubah</div>');
                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });

        }
   });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
    <ul class="nav pull-right">
        <li></li>
        <li></li>
        <li class="separator"></li>
    </ul>
</div>
<div class="box">
    <fieldset>
        <div class="row-fluid">
                <table style="width: 95%;">

                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Nomor WO" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="nomorWO" id="nomorWO" value="${noWo}" readonly="" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Nomor Polisi" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="nomorPolisi" readonly="" value="${nopol}" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Model Kendaraan" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="modelKendaraan" readonly="" value="${model}"/>
                        </td>
                    </tr>

                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Nama Stall" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="namaCustomer" readonly="" value="${stall}" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Tanggal Penyerahan Awal" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="namaCustomer" readonly="" value="${tanggal}" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Tanggal Penyerahan Baru" />
                        </td>
                        <td>
                            <ba:datePicker format="dd/MM/yyyy" name="tanggalBaru" id="tanggalBaru" precision="day" required="required" />
                            <select id="jamBaru" name="jamBaru" style="width: 60px" required="">
                                %{
                                    for (int i=0;i<24;i++){
                                        if(i<10){
                                            out.println('<option value="'+i+'">0'+i+'</option>');
                                        } else {
                                            out.println('<option value="'+i+'">'+i+'</option>');
                                        }
                                    }
                                }%
                            </select> :
                            <select id="menitBaru" name="menitBaru" style="width: 60px" required="">
                                %{
                                    for (int i=0;i<60;i++){
                                        if(i<10){
                                            out.println('<option value="'+i+'">0'+i+'</option>');
                                        } else {
                                            out.println('<option value="'+i+'">'+i+'</option>');
                                        }
                                    }
                                }%
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Pesan" />
                        </td>
                        <td>
                            <g:textArea style="width:100%;resize: none" name="pesan" id="pesan"  />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px" colspan="2">
                            <button id='btn1' onclick="editRevisi();" type="button" class="btn btn-cancel" style="width: 150px;">Update</button>
                            <button id='btn2' type="button" class="btn btn-cancel" style="width: 150px;" onclick="expandTableLayout();">Close</button>
                        </td>
                    </tr>
                </table>
        </div>
    </fieldset>
    </div>
</div>
</body>
</html>
