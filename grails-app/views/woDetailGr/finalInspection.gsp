
<%@ page import="com.kombos.customerprofile.FA; com.kombos.administrasi.Operation; com.kombos.parts.Returns" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'returnsAdd.label', default: 'Final Inspection')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;
	$(function(){

        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

        $('.box-action').click(function(){
            return false;
        });


   });
    $(document).ready(function()
    {
        $('#staRedo0').click(function(){
            var isChecked = $('#staRedo0').prop('checked');
            if(isChecked==true){
                $('#redoKeJob').prop('disabled',true);
            }
        });
        $('#staRedo1').click(function(){
            var isChecked = $('#staRedo1').prop('checked');
            if(isChecked==true){
                $('#redoKeJob').prop('disabled',false);
            }
        });

                $('#deskripsiFA').prop('disabled',true);
                $('#lokasiFA1').prop('disabled',true);
                $('#lokasiFA0').prop('disabled',true);
                $('#FAdiperiksa0').prop('disabled',true);
                $('#FAdiperiksa1').prop('disabled',true);
                $('#FAdiperbaiki0').prop('disabled',true);
                $('#FAdiperbaiki1').prop('disabled',true);

                $('#redoKeJob').prop('disabled',true);

                $('#lokasiFA1').attr('checked',false);
                $('#lokasiFA0').attr('checked',false);
                $('#FAdiperiksa0').attr('checked',false);
                $('#FAdiperiksa1').attr('checked',false);
                $('#FAdiperbaiki0').attr('checked',false);
                $('#FAdiperbaiki1').attr('checked',false);

        $('#fa0').click(function(){
            var isChecked = $('#fa0').prop('checked');
            if(isChecked==true){
                $('#deskripsiFA').prop('disabled',true);
                $('#lokasiFA1').prop('disabled',true);
                $('#lokasiFA0').prop('disabled',true);
                $('#FAdiperiksa0').prop('disabled',true);
                $('#FAdiperiksa1').prop('disabled',true);
                $('#FAdiperbaiki0').prop('disabled',true);
                $('#FAdiperbaiki1').prop('disabled',true);

                $('#lokasiFA1').attr('checked',false);
                $('#lokasiFA0').attr('checked',false);
                $('#FAdiperiksa0').attr('checked',false);
                $('#FAdiperiksa1').attr('checked',false);
                $('#FAdiperbaiki0').attr('checked',false);
                $('#FAdiperbaiki1').attr('checked',false);
            }
        });
        $('#fa1').click(function(){
            var isChecked = $('#fa1').prop('checked');
            if(isChecked==true){
                $('#deskripsiFA').prop('disabled',false);
                $('#lokasiFA1').prop('disabled',false);
                $('#lokasiFA0').prop('disabled',false);
                $('#FAdiperiksa0').prop('disabled',false);
                $('#FAdiperiksa1').prop('disabled',false);
                $('#FAdiperbaiki0').prop('disabled',false);
                $('#FAdiperbaiki1').prop('disabled',false);


            }
        });

         editFinal = function(){
                var id = $('#id').val();
                var idV = $('#idV').val();
                var nomorWO = $('#nomorWO').val();
                var butuhRSA = $('input[name="staRSA"]:checked').val();
                var kat = "";
                if(document.getElementById("kategori1").checked){kat = "1"}else{kat = "0"}
                if(document.getElementById("kategori2").checked){kat += "1"}else{kat += "0"}
                if(document.getElementById("kategori3").checked){kat += "1"}else{kat += "0"}
                if(document.getElementById("kategori4").checked){kat += "1"}else{kat += "0"}
                var kategori = kat
                var fa = $('input[name="fa"]:checked').val();
                var lokasiFA = $('input[name="lokasiFA"]:checked').val();
                var deskripsiFA = $('#deskripsiFA').val();
                var FAdiperiksa = $('input[name="FAdiperiksa"]:checked').val();
                var FAdiperbaiki = $('input[name="FAdiperbaiki"]:checked').val();
                var ket =  $('#ket').val();
                var staFI = $('input[name="staFI"]:checked').val();
                var staRedo = $('input[name="staRedo"]:checked').val();
                var dataKu = $('#redoKeJob').val();
                var jobSugest = $('#jobSugest').val();
                var tglNext = $('#tanggalNext').val();
                var jamMulai = $('#jamMulai').val();
                var redoKeJob = dataKu;
                if(tglNext=="")
                {
                alert("Tanggal Next Job Harus Di isi")
                }
                else
              {  $.ajax({
                    url:'${request.contextPath}/woDetailGr/editFinal',
                    type: "POST", // Always use POST when deleting data
                    data : {id:id,idV:idV,noWo : nomorWO,butuhRSA:butuhRSA,kategori:kategori,fa:fa,lokasiFA:lokasiFA,
                            deskripsiFA:deskripsiFA,FAdiperiksa:FAdiperiksa ,jobSugest:jobSugest,tglNext:tglNext,
                            FAdiperbaiki:FAdiperbaiki,ket:ket,staFI:staFI, staRedo : staRedo,redoKeJob:redoKeJob,
                            jamMulai:jamMulai
                            },
                    success : function(data){
                        toastr.success('<div>Final Inspection Add</div>');
                        expandTableLayout();
                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });
                }

        }
    });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
    <ul class="nav pull-right">
        <li></li>
        <li></li>
        <li class="separator"></li>
    </ul>
</div>
<div class="box">
        <div class="row-fluid">
            <div id="kiri" class="span5">
                <table style="width: 100%;">
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.t401NoWo.label" default="Nomor WO" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="nomorWO" id="nomorWO" readonly="" value="${noWo}" />
                            <g:hiddenField name="id" id="id" value="${id}" />
                            <g:hiddenField name="jamMulai" id="jamMulai" value="${jamMulai}" />
                            <g:hiddenField name="idV" id="idV" value="${idV}" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.noPolisi.label" default="Nomor Polisi" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="nomorPolisi" readonly="" value="${nopol}"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Model Kendaraan" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="modelKendaraan" readonly="" value="${model}"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Nama Stall" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="stall" readonly="" value="${stall}" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Teknisi" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="teknisi" readonly="" value="${nama}" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Clock Off" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="clockOff" readonly="" value="${clockOff}" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Status Kendaraan" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="statusKendarann" readonly="" value="${statusKendaraan}" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Butuh RSA?" />
                        </td>
                        <td>
                            <input type="radio" name="staRSA" value="1" required="true"  ${staRSA=='1'?'checked="checked"':''} />Ya  &nbsp;
                            <input type="radio" name="staRSA" value="0" required="true"   ${staRSA=='1'?'':'checked="checked"'}  />Tidak
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Job Suggest - Saran" />
                        </td>
                        <td>
                            <g:textArea style="width:100%;resize: none" name="jobSugest" id="jobSugest" value="${jobSugest}" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Kategori" />
                        </td>
                        <td>
                            <g:checkBox name="kategori1" id="kategori1" value="1"  /> Safety <g:checkBox name="kategori2" id="kategori2" value="1" /> Regulation
                            <g:checkBox name="kategori3" id="kategori3" value="1"  /> Comfortable <g:checkBox name="kategori4" id="kategori4" value="1" /> Efficiency
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Tanggal Next Job *" />
                        </td>
                        <td>
                            <ba:datePicker format="dd/MM/yyyy" name="tanggalNext" id="tanggalNext" precision="day" required="required" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Tanggal Update" />
                        </td>
                        <td>
                            <g:textField style="width:100%" name="namaCustomer" readonly="" value="${tanggal}" />
                        </td>
                    </tr>
                </table>
            </div>

            <div id="kanan" class="span7">
                <table style="width: 100%;">
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Field Action (FA) ? " />
                        </td>
                        <td>

                            <input type="radio" name="fa" id="fa1" value="1" required="true"   ${fa=='1'?'checked="checked"':''} />Ya  &nbsp;
                            <input type="radio" name="fa" id="fa0" value="0" required="true"   ${fa=='1'?'':'checked="checked"'}  />Tidak
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Lokasi Pekerjaan FA" />
                        </td>
                        <td>
                            <input type="radio" name="lokasiFA" id="lokasiFA1" value="1" required="true"  />Bengkel Lain  &nbsp;
                            <input type="radio" name="lokasiFA" id="lokasiFA0" value="0" required="true"   />TAM Sunter
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Deskripsi FA" />
                        </td>
                        <td>
                            <g:select name="deskripsiFA.id" id="deskripsiFA" from="${FA.list()}"  />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="FA sudah diperiksa?" />
                        </td>
                        <td>
                            <input type="radio" name="FAdiperiksa" id="FAdiperiksa1" value="1" required="true"  />Ya  &nbsp;
                            <input type="radio" name="FAdiperiksa" id="FAdiperiksa0" value="0" required="true"  />Tidak
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="FA sudah diperbaiki?" />
                        </td>
                        <td>
                            <input type="radio" name="FAdiperbaiki" id="FAdiperbaiki1" value="1" required="true" />Ya  &nbsp;
                            <input type="radio" name="FAdiperbaiki" id="FAdiperbaiki0" value="0" required="true" />Tidak
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="FIR" />
                        </td>
                        <td>
                            <g:textArea style="width:100%;resize: none" name="alamatCustomer" readonly="" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Keterangan Final Inspection" />
                        </td>
                        <td>
                            <g:textArea style="width:100%;resize: none" name="ket" id="ket" value="${ket}" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Status Inpection" />
                        </td>
                        <td>
                            <input type="radio" name="staFI" value="1" required="true"  ${staFI=='1'?'checked="checked"':''} />Fix   &nbsp;
                            <input type="radio" name="staFI" value="0" required="true"   ${staFI=='1'?'':'checked="checked"'}  />Non-Fix
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Perlu Redo" />
                        </td>
                        <td>
                            <input type="radio" name="staRedo" id="staRedo1" value="1" required="true"  ${staRedo=='1'?'checked="checked"':''} />Ya  &nbsp;
                            <input type="radio" name="staRedo" id="staRedo0" value="0" required="true"  ${staRedo=='1'?'':'checked="checked"'}  />Tidak
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="wo.test.label" default="Redo Ke Job" />
                        </td>
                        <td>
                            <g:select name="kegiatan" id="redoKeJob" from="${jobRcp}" noSelection="['':'Pilih Job Untuk Redo']" />
                        </td>
                    </tr>
                </table>
                <a class="btn cancel" href="javascript:void(0);" onclick="editFinal();">Ok</a>
                <a class="btn cancel" href="javascript:void(0);" onclick="expandTableLayout();"><g:message code="default.button.cancel.label" default="Close" /></a>
            </div>
        </div>
    </div>
</div>
</body>
</html>
