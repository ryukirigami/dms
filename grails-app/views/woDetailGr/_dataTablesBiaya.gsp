
<%@ page import="com.kombos.reception.Reception" %>

<r:require modules="baseapplayout" />

<g:render template="../menu/maxLineDisplay"/>

<table id="biaya_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="WO.jobPart.label" default="Nama Job / Parts" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="WO.hargaJob.label" default="Harga Job" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="WO.hargaJob.label" default="Harga Parts" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="WO.total.label" default="Total" /></div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var biayaTable;
var reloadBiayaTable;
$(function(){

    reloadBiayaTable = function() {
        biayaTable.fnClearTable();
        biayaTable.fnDraw();
	}
	
	biayaTable = $('#biaya_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : false,
        "bInfo":false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		},
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            return nRow;
        },
		"bSort": false,
		"bDestroy" : true,
		"aoColumns": [

{
    "sName": "",
    "mDataProp": "namaJob",
    "aTargets": [0],
    "bSearchable": false,
    "bSortable": false,
    "sWidth":"40%",
    "bVisible": true
}
,

{
    "sName": "",
    "mDataProp": "hargaJob",
    "aTargets": [1],
    "bSearchable": false,
    "mRender": function ( data, type, row ) {
        if(data != null)
            return '<span class="pull-right numeric">'+data+'</span>';
        else
            return '<span></span>';
    },
    "bSortable": false,
    "sWidth":"20%",
    "bVisible": true
}
,
{
    "sName": "",
    "mDataProp": "hargaParts",
    "aTargets": [2],
    "mRender": function ( data, type, row ) {
        if(data != null)
            return '<span class="pull-right numeric">'+data+'</span>';
        else
            return '<span></span>';
    },
    "bSearchable": true,
    "bSortable": false,
    "sWidth":"20%",
    "bVisible": true
}

,
{
    "sName": "",
    "mDataProp": "total",
    "aTargets": [3],
    "mRender": function ( data, type, row ) {
        if(data != null)
            return '<span class="pull-right numeric">'+data+'</span>';
        else
            return '<span></span>';
    },
    "bSearchable": true,
    "bSortable": false,
    "sWidth":"20%",
    "bVisible": true
}

]

	});


});
</g:javascript>

