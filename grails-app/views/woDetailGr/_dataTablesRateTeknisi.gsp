
<%@ page import="com.kombos.reception.Reception" %>

<r:require modules="baseapplayout" />

<g:render template="../menu/maxLineDisplay"/>

<table id="rateTeknisi_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="WO.jobOnly.label" default="Nama Job" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="WO.teknisi.label" default="Nama Teknisi" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="WO.rate.label" default="Rate" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="WO.rate2.label" default="Action" /></div>
        </th>


    </tr>
    </thead>
</table>

<g:javascript>
    var rateTeknisiTable;
    var reloadRateTeknisiTable;
    $(function(){

        reloadRateTeknisiTable = function() {
            rateTeknisiTable.fnClearTable();
            rateTeknisiTable.fnDraw();
        }

        rateTeknisiTable = $('#rateTeknisi_datatables').dataTable({
            "sScrollX": "100%",
            "bScrollCollapse": true,
            "bAutoWidth" : false,
            "bPaginate" : false,
            "sInfo" : "",
            "sInfoEmpty" : "",
            "sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
            bFilter: true,
            "bStateSave": false,
            'sPaginationType': 'bootstrap',
            "fnInitComplete": function () {
                this.fnAdjustColumnSizing(true);
            },
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                return nRow;
            },
            "bSort": true,
            "bDestroy" : true,
            "iDisplayLength" : maxLineDisplay,
            "aoColumns": [

                {
                    "sName": "",
                    "mDataProp": "namaJob",
                    "aTargets": [0],
                    "bSearchable": true,
                    "bSortable": true,
                    "sWidth":"20%",
                    "bVisible": true
                }

                ,

                {
                    "sName": "",
                    "mDataProp": "teknisi",
                    "aTargets": [1],
                    "bSearchable": true,
                    "bSortable": true,
                    "sWidth":"30%",
                    "bVisible": true
                }

                ,

                {
                    "sName": "",
                    "mDataProp": "rate",
                    "aTargets": [2],
                    "bSearchable": true,
                    "bSortable": true,
                    "sWidth":"10%",
                    "bVisible": true
                }

                ,

                {
                    "sName": "",
                    "mDataProp": "noWo",
                    "aTargets": [4],
                    "mRender": function ( data, type, row ) {
                        return '<input type="button" value=" Revisi Rate" onclick="rateTeknisi(\''+data+'\',\''+row['teknisi']+'\',\''+row['namaJob']+'\')">';
                    },
                    "bSearchable": false,
                    "bSortable": false,
                    "sWidth":"20%",
                    "bVisible": true
                }

            ]

        });


    });
</g:javascript>

