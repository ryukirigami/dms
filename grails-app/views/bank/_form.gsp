<%@ page import="com.kombos.administrasi.Bank" %>


<g:if test="${bankInstance?.m702ID}">
    <div class="control-group fieldcontain ${hasErrors(bean: bankInstance, field: 'm702ID', 'error')} required">
        <label class="control-label" for="m702ID">
            <g:message code="bank.m702ID.label" default="Kode Bank" />
            <span class="required-indicator">*</span>
        </label>
        <div class="controls">
            ${bankInstance?.m702ID}
        </div>
    </div>
</g:if>

<div class="control-group fieldcontain ${hasErrors(bean: bankInstance, field: 'm702NamaBank', 'error')} required">
	<label class="control-label" for="m702NamaBank">
		<g:message code="bank.m702NamaBank.label" default="Nama Bank" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m702NamaBank" id="m702NamaBank" required="" value="${bankInstance?.m702NamaBank}" maxlength="50"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: bankInstance, field: 'm702Cabang', 'error')} required">
	<label class="control-label" for="m702Cabang">
		<g:message code="bank.m702Cabang.label" default="Cabang" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m702Cabang" required="" value="${bankInstance?.m702Cabang}" maxlength="50"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: bankInstance, field: 'm702Alamat', 'error')} required">
	<label class="control-label" for="m702Alamat">
		<g:message code="bank.m702Alamat.label" default="Alamat" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textArea name="m702Alamat" required="" value="${bankInstance?.m702Alamat}" maxlength="100"/>
	</div>
</div>

<g:javascript>
    document.getElementById("m702NamaBank").focus();
</g:javascript>
