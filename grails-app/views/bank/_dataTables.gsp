
<%@ page import="com.kombos.administrasi.Bank" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="bank_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="bank.m702ID.label" default="Kode Bank" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="bank.m702NamaBank.label" default="Nama Bank" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="bank.m702Cabang.label" default="Cabang" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="bank.m702Alamat.label" default="Alamat" /></div>
			</th>


<!--			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="bank.m702staDel.label" default="M702sta Del" /></div>
			</th>-->

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
                <div id="filter_m702ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_m702ID" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m702NamaBank" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m702NamaBank" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m702Cabang" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m702Cabang" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m702Alamat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m702Alamat" class="search_init" />
				</div>
			</th>
	
%{--			<th style="border-top: none;padding: 5px;">
				<div id="filter_m702staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m702staDel" class="search_init" />
				</div>
			</th>--> --}%


		</tr>
	</thead>
</table>

<g:javascript>
var BankTable;
var reloadBankTable;
$(function(){
	
	reloadBankTable = function() {
		BankTable.fnDraw();
	}

    var recordsbankperpage = [];
    var anBankSelected;
    var jmlRecBankPerPage=0;
    var id;

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	BankTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	BankTable = $('#bank_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsBank = $("#bank_datatables tbody .row-select");
            var jmlBankCek = 0;
            var nRow;
            var idRec;
            rsBank.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsbankperpage[idRec]=="1"){
                    jmlBankCek = jmlBankCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsbankperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecBankPerPage = rsBank.length;
            if(jmlBankCek==jmlRecBankPerPage && jmlRecBankPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m702ID",
	"mDataProp": "m702ID",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m702NamaBank",
	"mDataProp": "m702NamaBank",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m702Cabang",
	"mDataProp": "m702Cabang",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m702Alamat",
	"mDataProp": "m702Alamat",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "staDel",
	"mDataProp": "staDel",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m702ID = $('#filter_m702ID input').val();
						if(m702ID){
							aoData.push(
									{"name": 'sCriteria_m702ID', "value": m702ID}
							);
						}
	
						var m702NamaBank = $('#filter_m702NamaBank input').val();
						if(m702NamaBank){
							aoData.push(
									{"name": 'sCriteria_m702NamaBank', "value": m702NamaBank}
							);
						}
	
						var m702Cabang = $('#filter_m702Cabang input').val();
						if(m702Cabang){
							aoData.push(
									{"name": 'sCriteria_m702Cabang', "value": m702Cabang}
							);
						}
	
						var m702Alamat = $('#filter_m702Alamat input').val();
						if(m702Alamat){
							aoData.push(
									{"name": 'sCriteria_m702Alamat', "value": m702Alamat}
							);
						}
	
						var m702staDel = $('#filter_m702staDel input').val();
						if(m702staDel){
							aoData.push(
									{"name": 'sCriteria_m702staDel', "value": m702staDel}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {
        $("#bank_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsbankperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsbankperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

    $('#bank_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsbankperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anBankSelected = BankTable.$('tr.row_selected');
            if(jmlRecBankPerPage == anBankSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsbankperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
