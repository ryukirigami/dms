

<%@ page import="com.kombos.administrasi.Bank" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'bank.label', default: 'Bank')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteBank;

$(function(){ 
	deleteBank=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/bank/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadBankTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-bank" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="bank"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${bankInstance?.m702ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m702ID-label" class="property-label"><g:message
					code="bank.m702ID.label" default="Kode Bank" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m702ID-label">
						%{--<ba:editableValue
								bean="${bankInstance}" field="m702ID"
								url="${request.contextPath}/Bank/updatefield" type="text"
								title="Enter m702ID" onsuccess="reloadBankTable();" />--}%
							
								<g:fieldValue bean="${bankInstance}" field="m702ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bankInstance?.m702NamaBank}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m702NamaBank-label" class="property-label"><g:message
					code="bank.m702NamaBank.label" default="Nama Bank" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m702NamaBank-label">
						%{--<ba:editableValue
								bean="${bankInstance}" field="m702NamaBank"
								url="${request.contextPath}/Bank/updatefield" type="text"
								title="Enter m702NamaBank" onsuccess="reloadBankTable();" />--}%
							
								<g:fieldValue bean="${bankInstance}" field="m702NamaBank"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bankInstance?.m702Cabang}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m702Cabang-label" class="property-label"><g:message
					code="bank.m702Cabang.label" default="Cabang" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m702Cabang-label">
						%{--<ba:editableValue
								bean="${bankInstance}" field="m702Cabang"
								url="${request.contextPath}/Bank/updatefield" type="text"
								title="Enter m702Cabang" onsuccess="reloadBankTable();" />--}%
							
								<g:fieldValue bean="${bankInstance}" field="m702Cabang"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bankInstance?.m702Alamat}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m702Alamat-label" class="property-label"><g:message
					code="bank.m702Alamat.label" default="Alamat" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m702Alamat-label">
						%{--<ba:editableValue
								bean="${bankInstance}" field="m702Alamat"
								url="${request.contextPath}/Bank/updatefield" type="text"
								title="Enter m702Alamat" onsuccess="reloadBankTable();" />--}%
							
								<g:fieldValue bean="${bankInstance}" field="m702Alamat"/>
							
						</span></td>
					
				</tr>
				</g:if>



            <g:if test="${bankInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="bank.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${bankInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/bank/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadbankTable();" />--}%

                        <g:fieldValue bean="${bankInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${bankInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="bank.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${bankInstance}" field="dateCreated"
                                url="${request.contextPath}/bank/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadbankTable();" />--}%

                        <g:formatDate date="${bankInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${bankInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="bank.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${bankInstance}" field="createdBy"
                                url="${request.contextPath}/bank/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadbankTable();" />--}%

                        <g:fieldValue bean="${bankInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${bankInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="bank.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${bankInstance}" field="lastUpdated"
                                url="${request.contextPath}/bank/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadbankTable();" />--}%

                        <g:formatDate date="${bankInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${bankInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="bank.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${bankInstance}" field="updatedBy"
                                url="${request.contextPath}/bank/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadbankTable();" />--}%

                        <g:fieldValue bean="${bankInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
%{--				<g:if test="${bankInstance?.m702staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m702staDel-label" class="property-label"><g:message
					code="bank.m702staDel.label" default="M702sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m702staDel-label">
						<ba:editableValue
								bean="${bankInstance}" field="m702staDel"
								url="${request.contextPath}/Bank/updatefield" type="text"
								title="Enter m702staDel" onsuccess="reloadBankTable();" />
							
								<g:fieldValue bean="${bankInstance}" field="m702staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>--}%
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${bankInstance?.id}"
					update="[success:'bank-form',failure:'bank-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteBank('${bankInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
