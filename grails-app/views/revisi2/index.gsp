
<%@ page import="com.kombos.maintable.EFaktur" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'EFaktur.label', default: 'E Faktur - Revisi PPN Keluaran')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>
			var show;
			var edit;
			$(function(){

                $('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :
							loadPath("Revisi2/uploadData");
							break;
						case '_ALL_' :
							loadPath("Revisi2/list");
							break;

				   }
				   return false;
				});
                var checkin = $('#search_Tanggal').datepicker({
                    onRender: function(date) {
                        return '';
                    }
                }).on('changeDate', function(ev) {
                            var newDate = new Date(ev.date)
                            newDate.setDate(newDate.getDate());
                            checkout.setValue(newDate);
                            checkin.hide();
                            $('#search_Tanggal2')[0].focus();
                        }).data('datepicker');

                var checkout = $('#search_Tanggal2').datepicker({
                    onRender: function(date) {
                        return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
                    }
                }).on('changeDate', function(ev) {
                            checkout.hide();
                        }).data('datepicker');

                printEfaktur = function(){
                       checkId =[];
                        $("#EFaktur-table tbody .row-select").each(function() {
                              if(this.checked){
                                 var nRow = $(this).next("input:hidden").val();
                                    checkId.push(nRow);
                              }
                        });
                        if(checkId.length<1 ){
                            alert('Silahkan pilih salah satu data untuk dicetak');
                            return;

                        }
                        if(checkId.length>1 ){
                            alert('Pilih salah satu data saja');
                            return;

                        }
                       var docNumber = checkId[0];

                       window.location = "${request.contextPath}/Revisi2/printEfaktur?docNumber="+docNumber;

                }


                generateData = function(){

                     var tgl1= $('#search_Tanggal').val();
                     var tgl2= $('#search_Tanggal2').val();
                     var conf = confirm("Apakah Anda Yakin Generate Data ?")
                     if(conf){
                         if(tgl1==null || tgl1=="" || tgl2==null||tgl2==""){
                            alert("Anda Belum memilih Tanggal");
                            return
                         }

                         jQuery("#generate").click(function (event) {
                            jQuery('#spinner').fadeIn(1);
                            $(this).submit();
                            event.preventDefault();
                        });

                         $.ajax({
                                type:'POST',
                                url:'${request.contextPath}/Revisi2/generateData',
                                data : {tgl1 : tgl1, tgl2 : tgl2},

                                error:function(XMLHttpRequest,textStatus,errorThrown){},
                                complete:function(XMLHttpRequest,textStatus){
                                    $('#spinner').fadeOut();
                                    toastr.success("Generate Data Selesai");
                                }
                         });
                               toastr.success("Anda Sedang Melakukan Generate Data");


                     }
                }

                showDetail = function(docNumber){
                    $("#detailContent").empty();
                    $.ajax({
                        type:'POST',
                        url:'${request.contextPath}/Revisi2/showDetail',
                        data : {docNumber : docNumber},
                        success:function(data,textStatus){
                                $("#detailContent").html(data);
                                $("#detailModal").modal({
                                    "backdrop" : "dynamic",
                                    "keyboard" : true,
                                    "show" : true
                                }).css({'width': '1100px','margin-left': function () {return -($(this).width() / 2);}});

                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
                }

});
    </g:javascript>
</head>
<body>

<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
    <ul class="nav pull-right">
        <li>
            <g:if test="${!session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                <a class="pull-right box-action" href="#"
                   style="display: block;" target="_CREATE_">&nbsp;&nbsp;Upload Data</i>&nbsp;&nbsp;
                </a>
            </g:if>
        </li>
        <li class="separator"></li>
        <li>
            <g:if test="${!session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                <a class="pull-right box-action" href="#"
                   style="display: block;" target="_ALL_">&nbsp;&nbsp;List All </i>&nbsp;&nbsp;
                </a>
            </g:if>
        </li>
    </ul>
</div>

<div class="box">
    <div class="span12" id="EFaktur-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <fieldset>
            <table style="padding-right: 10px">
                <tr>
                    <td style="width: 130px">
                        <label class="control-label" for="t951Tanggal">
                            <g:message code="auditTrail.wo.label" default="Company Dealer" />&nbsp;
                        </label>&nbsp;&nbsp;
                    </td>
                    <td>
                        <div id="filter_wo" class="controls">
                            <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                <g:select name="companyDealer" id="companyDealer" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" noSelection="${['-':'Semua']}" />
                            </g:if>
                            <g:else>
                                <g:select name="companyDealer" id="companyDealer" readonly="" disabled="" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                            </g:else>
                        </div>
                    </td>
                </tr><tr>
                <td style="width: 130px">
                    <label class="control-label" for="t951Tanggal">
                        <g:message code="auditTrail.tanggal.label" default="Tanggal Upload" />&nbsp;
                    </label>&nbsp;&nbsp;
                </td>
                <td>
                    <div id="filter_m777Tgl" class="controls">
                        <ba:datePicker id="search_Tanggal" name="search_Tanggal" precision="day" format="dd-MM-yyyy"  value="${new Date()}" />
                        &nbsp;&nbsp;&nbsp;s.d.&nbsp;&nbsp;&nbsp;
                        <ba:datePicker id="search_Tanggal2" name="search_Tanggal2" precision="day" format="dd-MM-yyyy"  value="${new Date()}" />
                    </div>
                </td>
            </tr>
                <tr>
                    <td colspan="3" >
                        <div class="controls" style="right: 0">
                            <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-primary view" name="view" id="view" >View</button>
                            <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-cancel clear" name="clear" id="clear" >Clear</button>
                            %{--<g:if test="${!session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">--}%
                            %{--<button style="width: 220px;height: 30px; border-radius: 5px" class="btn btn-primary view" name="generate" id="generate" onclick="generateData()" >Generate Data</button>--}%
                            %{--</g:if>--}%
                            %{--</div>--}%
                    </td>
                </tr>
            </table>

        </fieldset>
        <g:render template="dataTables1" />
    </div>
    <button style="width: 100px;height: 30px; border-radius: 5px" class="btn btn-primary print" name="print" id="print" onclick="printEfaktur();" >Print TO CSV</button>
</div>
<div id="detailModal" class="modal fade">
    <div class="modal-dialog" style="width: 1090px;">
        <div class="modal-content" style="width: 1090px;">
            <div class="modal-body" style="max-height: 450px;">
                <div id="detailContent"/>
                <div class="iu-content"></div>

            </div>
        </div>
    </div>
</div>
</body>
</html>
