
<%@ page import="com.kombos.parts.GoodsHargaJual" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'goodsHargaJual.label', default: 'Upload Harga Jual Goods')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <r:require modules="highslide" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/swfobject/2.1/swfobject.js"></script>

    <g:javascript>

        <g:if test="${jmlhDataError && jmlhDataError>0}">
            $('#save').attr("disabled", true);
        </g:if>
        <g:else>
            <g:if test="${dataSama}">
                var cek = window.confirm('Data dengan goods pada baris berwarna hijau sudah ada. Apakah anda akan mengganti data yang sudah ada?');
                if(!cek){
                    $('#save').attr("disabled", true);
                }
            </g:if>
            <g:if test="${jmlBlmTerdaftar}">
                var cek = window.confirm('Data dengan goods pada baris berwarna Biru Belum Terdaftar. Apakah anda akan Menambahkannya secara otomatis?');
                if(!cek){
                    $('#save').attr("disabled", true);
                }
            </g:if>
        </g:else>

        var saveForm;
        $(function(){

            function progress(e){
                if(e.lengthComputable){
                    //kalo mau pake progress bar
                    //$('progress').attr({value:e.loaded,max:e.total});
                }
            }

            saveForm = function() {
        <g:if test="${jsonData}">
            var cek =window.confirm('${message(code: 'default.button.save.confirm.message', default: 'Anda yakin data akan disimpan?')}');
                if(cek){
                var sendData = ${jsonData};
                        $.ajax({
                            url:'${request.getContextPath()}/uploadHargaJualGoods/upload',
                            type: 'POST',
                            xhr: function() {
                                var myXhr = $.ajaxSettings.xhr();
                                if(myXhr.upload){
                                    myXhr.upload.addEventListener('progress',progress, false);
                                }
                                return myXhr;
                            },
                            //add beforesend handler to validate or something
                            //beforeSend: functionname,
                            success: function (res) {
                                $('#hargaJualGoods').empty();
                                $('#hargaJualGoods').append(res);
                                toastr.success("Sukses Upload");
                                //reloadGoodsHargaJualTable();
                            },
                            //add error handler for when a error occurs if you want!
                            error: function (data, status, e){
                                alert(e);
                            },
                            complete: function(xhr, status) {

                            },
                            data: JSON.stringify(sendData),
                            contentType: "application/json; charset=utf-8",
                            traditional: true,
                            cache: false
                        });
                }
        </g:if>
        <g:else>
            alert('No File Selected');
        </g:else>
        }


    });

    </g:javascript>

</head>
<body>
<div id="hargaJualGoods">
    <div class="navbar box-header no-border">
        <span class="pull-left">
            <g:message code="default.list.label" args="[entityName]" />
        </span>
        <ul class="nav pull-right">
            <li></li>
            <li></li>
            <li class="separator"></li>
        </ul>
    </div>
    <div class="box">
        <div class="span12" id="goodsHargaJual-table">
            <fieldset>
                <form id="uploadHargaJualGoods-save" class="form-vertical" action="${request.getContextPath()}/uploadHargaJualGoods/save" method="post" onsubmit="submitForm();return false;">
                    <table>
                            <tr>
                                <td>
                                    <fieldset class="form">
                                        <g:render template="form"/>
                                    </fieldset>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <fieldset class="buttons controls">
                                        <g:submitButton class="btn btn-primary create" name="view" id="view" value="${message(code: 'default.button.view.label', default: 'Upload')}" />
                                        <g:field type="button" onclick="saveForm();" class="btn btn-primary create" name="save" id="save" value="${message(code: 'default.button.upload.label', default: 'Simpan')}"/>
                                        &nbsp;&nbsp;&nbsp;
                                        <g:if test="${flash.message}">
                                            ${flash.message}
                                        </g:if>
                                    </fieldset>
                                </td>
                            </tr>
                    </table>
                </form>
                <g:javascript>
                        var submitForm;
                        $(function(){

                            function progress(e){
                                if(e.lengthComputable){
                                    //kalo mau pake progress bar
                                    //$('progress').attr({value:e.loaded,max:e.total});
                                }
                            }

                            submitForm = function() {

                                var form = new FormData($('#uploadHargaJualGoods-save')[0]);

                                $.ajax({
                                    url:'${request.getContextPath()}/uploadHargaJualGoods/view',
                                    type: 'POST',
                                    xhr: function() {
                                        var myXhr = $.ajaxSettings.xhr();
                                        if(myXhr.upload){
                                            myXhr.upload.addEventListener('progress',progress, false);
                                        }
                                        return myXhr;
                                    },
                                    //add beforesend handler to validate or something
                                    //beforeSend: functionname,
                                    success: function (res) {
                                        $('#hargaJualGoods').empty();
                                        $('#hargaJualGoods').append(res);
                                        //reloadGoodsHargaJualTable();
                                    },
                                    //add error handler for when a error occurs if you want!
                                    error: function (data, status, e){
                                        alert(e + " -> " + status + " -> "+ data);
                                    },
                                    complete: function(xhr, status) {

                                    },
                                    data: form,
                                    cache: false,
                                    contentType: false,
                                    processData: false
                                });

                            }
                        });
                </g:javascript>
            </fieldset>
            <br>

            <table class="display table table-striped table-bordered table-hover dataTable" width="100%" cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px; width: 1284px;">
                <tr>
                    <th>
                        Tanggal Berlaku
                    </th>
                    <th>
                        Kode Goods
                    </th>
                    <th>
                        Nama Goods
                    </th>
                    <th>
                        Harga Jual
                    </th>
                    <th>
                        Harga Jual PPN
                    </th>
                    <th>
                        Satuan
                    </th>
                </tr>
                <g:if test="${htmlData}">
                    ${htmlData}
                </g:if>
                <g:else>
                    <tr class="odd">
                        <td class="dataTables_empty" valign="top" colspan="9">No data available in table</td>
                    </tr>
                </g:else>
            </table>
            %{--<g:if test="${jsonData}">--}%
            %{--${jsonData}--}%
            %{--</g:if>--}%
        </div>
    </div>
</div>
</body>
</html>
