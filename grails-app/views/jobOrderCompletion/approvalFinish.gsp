<%@ page import="com.kombos.administrasi.KegiatanApproval" %>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="joc.approval.label" default="Delivery - Send Approval Finish JOC" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
        $(function(){
            $('#noWo').val(paramNoWo);
            $('#noPol').val(paramNoPol);
            $('#model').val(paramModel);
        })

        function sendApprovalJOC(){
            var nowoKirim = $('#noWo').val().replace(" ","");
            var pesanKirim = $('#pesanApproval').val().replace(" ","");
            if(nowoKirim=="" || nowoKirim==null){
                alert('Data belum lengkap');
                return
            }
            if(pesanKirim=="" || pesanKirim==null){
                alert('Silahkan isi pesan terlebih dahulu');
                return
            }
            var toAjax=true;
            var notif = "Proses JOC belum selesai\nAlsasan : Customer tidak membawa \n";
            var needConfirm = false;
            var paramStaPks = "1";
            var paramStaBawaPks = "1";
            var paramStaAsuransi = dokumenSPK;
            var paramStaBawaAsuransi = dokumenAsuransi;
            if(isAssurance){
                paramStaAsuransi = "0";
            }
            if(isPKS){
                paramStaPks = "0";
            }
            if(isAssurance==true && dokumenAsuransi=="1"){
                toAjax=false;
                needConfirm =true;
                notif+="-Dokumen PKS\n"
            }
            if(isPKS==true && dokumenSPK=="1"){
                toAjax=false;
                needConfirm=true;
                notif+="-Dokumen Asuransi\n"
            }
            if(isAssurance==true && (dokumenAsuransi=="" && dokumenSPK=="")){
                var notifFail = "";
                if(dokumenAsuransi==""){
                    notifFail += "dokumen Asuransi"
                }
                if(dokumenSPK==""){
                    if(notifFail!=""){
                        notifFail+=" dan "
                    }
                    notifFail+="dokumen SPK"
                }
                alert('Silahkan isi apakah customer membawa '+notifFail);
                return
            }
            notif+="Jika anda tetap ingin menyelesaikan JOC\nSilahkan klik button \"OK\" untuk memperoleh approval";
            if(needConfirm==true){
                var apakah = confirm(notif)
                if(apakah){
                    toAjax=true;
                }
            }
            var aoData = []
            aoData.push(
                    {"name": 'nowo', "value": nowoKirim},
                    {"name": 'pesan', "value": pesanKirim},
                    {"name": 'staPks', "value": paramStaPks},
                    {"name": 'staAsurans', "value": paramStaAsuransi},
                    {"name": 'waktuJOC', "value": waktuJOC}
            );
            if(paramStaPks=="0"){
                aoData.push(
                    {"name": 'staBawaPks', "value": paramStaBawaPks}
                )
            }
            if(paramStaAsuransi=="0"){
                aoData.push(
                    {"name": 'staBawaAsuransi', "value": paramStaBawaAsuransi}
                )
            }
            if(toAjax==true){
                $.ajax({
                    type:'POST',
                    url:'${request.contextPath}/jobOrderCompletion/sendApprovalJOC',
                    data : aoData,
                    success:function(data,textStatus){
                        toastr.success('<div>Approval Berhasil Dikirim</div>');
                        $('#closeModal').click();
                    },
                    error:function(){
                        alert('Internal Server Error');
                    }
                });
            }
        }
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="joc.approval.label" default="Delivery - Send Approval Finish JOC" /></span>
    <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
</div>
<div class="box">
    <div class="span12" id="jenisStall-table">
        <table class="table">
            <tbody>
            <tr>
                <td>
                    Nama Kegiatan *
                </td>
                <td>
                    <g:select id="kegiatanApprovalSPK" name="kegiatanApprovalSPK" from="${com.kombos.administrasi.KegiatanApproval.list()}"  value="${KegiatanApproval?.findByM770KegiatanApproval(KegiatanApproval.FINISH_JOC)?.id}" optionKey="id" required="" class="many-to-one" readonly=""/>
                </td>
            </tr>
            <tr>
                <td>
                    Nomor WO
                </td>
                <td>
                    <g:textField name="noWo" id="noWo" readonly="" />
                </td>
            </tr>
            <tr>
                <td>
                    Nomor Polisi
                </td>
                <td>
                    <g:textField name="noPol" id="noPol" readonly="" />
                </td>
            </tr>
            <tr>
                <td>
                    Model Kendaraan
                </td>
                <td>
                    <g:textField name="model" id="model" readonly="" />
                </td>
            </tr>
            <tr>
                <td>
                    Tanggal Permohonan
                </td>
                <td>
                    ${new Date().format("dd-MM-yyyy HH:mm")}
                </td>
            </tr>
            <tr>
                <td>
                    Pesan
                </td>
                <td>
                    <textarea id="pesanApproval" name="pesanApproval" cols="200" rows="3"></textarea>
                </td>
            </tr>
            </tbody>
        </table>
        <div class="pull-right">
            <button class="btn btn-primary" id="btnSend" onclick="sendApprovalJOC()">Send</button>
            <button class="btn cancel" id="closeModal" data-dismiss="modal" >Close</button>
        </div>
    </div>
    <div class="span7" id="jenisStall-form" style="display: none;"></div>
</div>
</body>
</html>
