
<%@ page contentType="text/html;charset=UTF-8" %>
<html>

<head>
    <meta name="layout" content="main">
    <title>Job Order Completion</title>
    <r:require modules="baseapplayout"/>

    <g:javascript disposition="head">
        var paramNoWo = "";
        var paramNoPol = "";
        var paramModel = "";
        var isAssurance = false;
        var isPKS = false;
        var waktuJOC = "${new java.util.Date().format("dd/MM/yyyy HH:mm:ss")}";
        $(function () {

//            jQuery("#buttonSave").click(function (event) {
//                jQuery('#spinner').fadeIn(1);
//                $(this).submit();
//                event.preventDefault();
//            });

            jQuery("#buttonClose").click(function (event) {
                event.preventDefault(); //to stop the default loading
            });

            jQuery("#buttonClose1").click(function (event) {
                event.preventDefault(); //to stop the default loading
            });

            jQuery("#buttonNext").click(function (event) {
                document.getElementById("halaman1").style.display = 'none';
                document.getElementById("halaman2").style.display = '';
                event.preventDefault(); //to stop the default loading
            });

            jQuery("#buttonBack").click(function (event) {
                document.getElementById("halaman1").style.display = '';
                document.getElementById("halaman2").style.display = 'none';
                event.preventDefault(); //to stop the default loading
            });


            jQuery("#loadData").click(function (event) {
                loadInformasiUmum();
                loadWorkOrder();
                loadActualPengerjaanJob();
                loadPenambahanPenguranganJob();
                event.preventDefault(); //to stop the default loading
            });

            completeProcess = function () {

            }

            doResponse = function (data) {
                jQuery('#spinner').fadeOut();
                if (data.status == 'OK') {
                    alert('Save success');
                    window.location.replace('#/home');
                } else {
                    alert('Save fail\n' + data.error);
                }

            }


            checkNumber = function (n) {
                var number = !isNaN(parseFloat(n.value)) && isFinite(n.value);
                if (!number) {
                    // alert("tanggalFeedbackEnd !");
                    n.focus();
                    n.value = '';
                }
            }


            loadInformasiUmum = function(){
                $("#lblSubTotal").text("")
                $("#lblDiscJasa").text("")
                $("#lblDiscPart").text("")
                $("#lblBookingFee").text("")
                $("#lblSisa").text("")
                var kategoriView = document.getElementById("kategoriView").value;
                var kataKunci = document.getElementById("kataKunci").value;
                $('#informasiUmum').empty();
                $('#informasiUmumHalaman2').empty();
                jQuery('#spinner').fadeIn(1);
                jQuery.getJSON('${request.contextPath}/jobOrderCompletion/loadInformasiUmum?kategoriView=' + encodeURIComponent(kategoriView)+'&kataKunci='+encodeURIComponent(kataKunci), function (data) {
                    jQuery('#spinner').fadeOut(1);
                    if(data.status == 'Error'){
                        paramNoWo = "";
                        paramNoPol = "";
                        paramModel = "";
                        isAssurance = false;
                        isPKS = false;
                        $('#divDokumenAsuransi').hide();
                        $('#divDokumenPKS').hide();
                        $('#lblstajoc').text('');
                        alert(data.message);
                        return
                    }



                    var hasil = data.hasil;
                    paramNoWo = hasil.noWo;
                    paramNoPol = hasil.nopol;
                    paramModel = hasil.model;
                    waktuJOC = "${new java.util.Date().format("dd/MM/yyyy HH:mm:ss")}";
                    $('#lblstajoc').text(hasil.staJoc);
                    if(hasil.SPKAss=="Ya"){
                        isAssurance = true;
                        $('#divDokumenAsuransi').show();
                    }else{
                        isAssurance = false;
                    }
                    if(hasil.pks=="Ya"){
                        isPKS = true;
                        $('#divDokumenPKS').show();
                    }else{
                        isPKS = false;
                    }
                    $("#lblSubTotal").text(hasil.subTotal)
                    $("#lblDiscJasa").text(hasil.discJasa)
                    $("#lblDiscPart").text(hasil.discParts)
                    $("#lblBookingFee").text(hasil.bookingFee)
                    $("#lblSisa").text(hasil.sisaBayar)
                    var tiket = "<tr>" +
                        "       <td style='vertical-align: middle; border-style: solid;border-width: 1px;'>" +hasil.noWo + "</td>" +
                        "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;'>" +hasil.nopol + "</td>" +
                        "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;'>" +hasil.model + "</td>" +
                        "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;'>" +hasil.tanggalWO + "</td>" +
                        "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;'>" +hasil.waktuPenyerahan + "</td>" +
                        "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;'>" +hasil.SPKAss + "</td>" +
                        "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;'>" +hasil.SPKAsuransi + "</td>" +
                        "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;'>" +hasil.pks + "</td>" +
                        "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;'>" +hasil.saldoPKS + "</td>" +
                        "</tr>";
                    $('#informasiUmum').append(tiket);
                    $('#informasiUmumHalaman2').append(tiket);
                });
            }


            loadWorkOrder = function(){
                var kategoriView = document.getElementById("kategoriView").value;
                var kataKunci = document.getElementById("kataKunci").value;
                $('#workOrder').empty();
                jQuery('#spinner').fadeIn(1);
                jQuery.getJSON('${request.contextPath}/jobOrderCompletion/loadWorkOrder?kategoriView=' + encodeURIComponent(kategoriView)+'&kataKunci='+encodeURIComponent(kataKunci), function (data) {
                    jQuery('#spinner').fadeOut(1);
                    if(data.status == 'Error'){
                        return
                    }


                    jQuery.each(data.hasils, function (index, value) {
                        var hasil = value;

                        if(hasil.col5 == true){
                            var tiket = "<tr>" +
                                "       <td style='vertical-align: middle; border-style: solid;border-width: 1px;'>" +hasil.col1 + "</td>" +
                                "       <td colspan='2' style='vertical-align: middle;border-style: solid;border-width: 1px;'>" +hasil.col2 + "</td>" +
                                "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;'>" +hasil.col3 + "</td>" +
                                "</tr>";
                            $('#workOrder').append(tiket);
                        }
                        else {
                            var tiket = "<tr>" +
                            "       <td style='vertical-align: middle; border-style: solid;border-width: 1px;background-color: #fffee8;'>" +hasil.col1 + "</td>" +
                            "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;background-color: #fffee8;'>" +hasil.col2 + "</td>" +
                            "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;background-color: #fffee8;'>" +hasil.col3 + "</td>" +
                            "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;background-color: #fffee8;'>" +hasil.col4 + "</td>" +
                            "</tr>";
                            $('#workOrder').append(tiket);
                        }

                     });
                });
            }


            loadActualPengerjaanJob = function(){
                var kategoriView = document.getElementById("kategoriView").value;
                var kataKunci = document.getElementById("kataKunci").value;
                $('#ActualOrder').empty();
                jQuery('#spinner').fadeIn(1);
                jQuery.getJSON('${request.contextPath}/jobOrderCompletion/loadActualPengerjaanJob?kategoriView=' + encodeURIComponent(kategoriView)+'&kataKunci='+encodeURIComponent(kataKunci), function (data) {
                    jQuery('#spinner').fadeOut(1);
                    if(data.status == 'Error'){
                        return
                    }


                    jQuery.each(data.hasils, function (index, value) {
                        var hasil = value;

                        if(hasil.col5 == true){
                            var tiket = "<tr>" +
                            "       <td style='vertical-align: middle; border-style: solid;border-width: 1px;'>" +hasil.col1 + "</td>" +
                            "       <td colspan='2' style='vertical-align: middle;border-style: solid;border-width: 1px;'>" +hasil.col2 + "</td>" +
                            "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;'>" +hasil.col3 + "</td>" +
                            "</tr>";
                            $('#ActualOrder').append(tiket);
                        }
                        else {
                            var tiket = "<tr>" +
                            "       <td style='vertical-align: middle; border-style: solid;border-width: 1px;background-color: #fffee8;'>" +hasil.col1 + "</td>" +
                            "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;background-color: #fffee8;'>" +hasil.col2 + "</td>" +
                            "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;background-color: #fffee8;'>" +hasil.col3 + "</td>" +
                            "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;background-color: #fffee8;'>" +hasil.col4 + "</td>" +
                            "</tr>";
                            $('#ActualOrder').append(tiket);
                        }

                     });
                });
            }


            loadPenambahanPenguranganJob = function(){
                var kategoriView = document.getElementById("kategoriView").value;
                var kataKunci = document.getElementById("kataKunci").value;
                $('#PenambahanPenguranganJob').empty();
                jQuery('#spinner').fadeIn(1);
                jQuery.getJSON('${request.contextPath}/jobOrderCompletion/loadPenambahanPenguranganJob?kategoriView=' + encodeURIComponent(kategoriView)+'&kataKunci='+encodeURIComponent(kataKunci), function (data) {
                    jQuery('#spinner').fadeOut(1);
                    if(data.status == 'Error'){
                        return
                    }


                    jQuery.each(data.hasils, function (index, value) {
                        var hasil = value;

                        if(hasil.col5 == true){
                            var tiket = "<tr>" +
                            "       <td style='vertical-align: middle; border-style: solid;border-width: 1px;'>" +hasil.col1 + "</td>" +
                            "       <td colspan='4' style='vertical-align: middle;border-style: solid;border-width: 1px;'>" +hasil.col2 + "</td>" +
                            "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;'>" +hasil.col4 + "</td>" +
                            "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;'>" +hasil.col6 + "</td>" +
                            "</tr>";
                            $('#PenambahanPenguranganJob').append(tiket);
                        }
                        else {
                            var tiket = "<tr>" +
                            "       <td style='vertical-align: middle; border-style: solid;border-width: 1px;background-color: #fffee8;'>" +hasil.col1 + "</td>" +
                            "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;background-color: #fffee8;'>" +hasil.col2 + "</td>" +
                            "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;background-color: #fffee8;'>" +hasil.col3 + "</td>" +
                            "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;background-color: #fffee8;'>" +hasil.col4 + "</td>" +
                            "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;background-color: #fffee8;'>" +hasil.col6 + "</td>" +
                            "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;background-color: #fffee8;'>" +hasil.col7 + "</td>" +
                            "       <td style='vertical-align: middle;border-style: solid;border-width: 1px;background-color: #fffee8;'>" +hasil.col8 + "</td>" +
                            "</tr>";
                            $('#PenambahanPenguranganJob').append(tiket);
                        }

                     });
                });
            }

        });

        function doFinishJOC(){
            $("#approvalFinishJOCContent").empty();
		    $.ajax({
		        type:'POST',
		        url:'${request.contextPath}/jobOrderCompletion/approvalFinish',
                success:function(data,textStatus){
                        $("#approvalFinishJOCContent").html(data);
                        $("#approvalFinishJOCModal").modal({
                            "backdrop" : "static",
                            "keyboard" : true,
                            "show" : true
                        }).css({'width': '500px','margin-left': function () {return -($(this).width() / 2);}});

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
   		    });
        }

        function editJobParts(){
            if(paramNoWo==""){
                alert('Data masih kosong');
                return
            }
            window.location.href='#/EditJobParts';
            $('#spinner').fadeIn(1);
            $.ajax({
                url: '${request.contextPath}/editJobParts?nowo='+paramNoWo,
                type: "GET",dataType:"html",
                complete : function (req, err) {
                    $('#main-content').html(req.responseText);
                    $('#spinner').fadeOut();
                }
            });
        }
        var dokumenSPK="";
        var dokumenAsuransi="";
        function cekDokumen(asal){
            if(asal=="pks"){
                if($('input:radio[name=staDokumenSPK]:nth(0)').is(':checked')){
                    dokumenSPK="0";
                }else{
                    dokumenSPK="1";
                }
            }else{
                if($('input:radio[name=staDokumenAsuransi]:nth(0)').is(':checked')){
                    dokumenAsuransi="0";
                }else{
                    dokumenAsuransi="1";
                }
            }

        }
    </g:javascript>

    <style>
    th {
        border-style: solid;
        border-width: 1px;
        background-color: #f4ffff;
    }

    </style>
</head>

<body>
<g:formRemote name="form" id="form" on404="alert('not found!')" update="updateMe"
              method="post"
              onComplete="completeProcess()"
              onSuccess="doResponse(data)"
              url="[controller: 'jobOrderCompletion', action: 'sendApprovalJOC']">
    <div id="halaman1">
        <div class="box">
            <legend>Delivery - Job Order Completion</legend>
            <fieldset class="form">
                <table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">
                    <tr>
                        <td style="width: 50%;vertical-align: top;">

                            <table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">
                                <tr>
                                    <td style="width: 80%;vertical-align: top;">

                                        <div class="box">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td>
                                                        Kategori View
                                                    </td>
                                                    <td>
                                                        <g:select name="kategoriView"
                                                                  from="${["NOMOR WO", "NOMOR POLISI"]}"/>
                                                    </td>
                                                    <td>
                                                        Kata Kunci
                                                    </td>
                                                    <td>
                                                        <g:textField name="kataKunci"/>
                                                    </td>
                                                    <td>
                                                        <button id="loadData" class="btn cancel">OK</button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>

                                    </td>
                                    <td style="width: 20%;vertical-align: middle;padding-left: 50px;padding-right: 50px;padding-top: 10px;padding-bottom: 20px;font-size: 23px">
                                        <span id="lblstajoc"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="box">
                                            <legend style="font-size: medium">Informasi Umum</legend>

                                            <table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">
                                                <tr>
                                                    <td style="width: 80%;vertical-align: top;">

                                                        <table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">
                                                            <thead>
                                                            <tr>
                                                                <th>Nomor WO</th>
                                                                <th>Nomor Polisi</th>
                                                                <th>Model</th>
                                                                <th>Tanggal WO</th>
                                                                <th>Waktu Penyerahan</th>
                                                                <th>SPK Ass.</th>
                                                                <th>No. SPK Asuransi</th>
                                                                <th>PKS</th>
                                                                <th>Saldo PKS</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="informasiUmum">

                                                            </tbody>
                                                        </table>

                                                    </td>
                                                    <td style="width: 20%;vertical-align: top;padding-left: 50px;padding-right: 50px;">
                                                        <button style="height: 95%;width: 95%;font-size: small"
                                                                disabled>Open Asuransi...</button>
                                                    </td>
                                                </tr>
                                            </table>

                                        </div>

                                        <div class="box" id="divDokumenPKS" style="display: none">
                                            Bawa Dokumen PKS ?
                                            <g:radioGroup name="staDokumenSPK" id="staDokumenSPK" values="['0','1']" labels="['Ya','Tidak']" onclick="cekDokumen('pks');" required="" >
                                                ${it.radio} ${it.label}
                                            </g:radioGroup>
                                        </div>
                                        <div class="box" id="divDokumenAsuransi"  style="display: none">
                                            Bawa Dokumen Asuransi ?
                                            <g:radioGroup name="staDokumenAsuransi" id="staDokumenAsuransi" values="['0','1']" labels="['Ya','Tidak']" onclick="cekDokumen('asuransi');" required="" >
                                                ${it.radio} ${it.label}
                                            </g:radioGroup>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2">

                                        <table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">
                                            <tr>
                                                <td style="padding-right: 20px;width: 50%;">
                                                    <div class="box">
                                                        <legend style="font-size: medium">Service / Work Order</legend>

                                                        <table style="width: 100%;border: 1px;padding-left: 10px;padding-right: 10px;border-style: solid;">
                                                            <thead>
                                                            <tr>
                                                                <th rowspan="2">Kode Job</th>
                                                                <th colspan="2">Nama Job</th>
                                                                <th>Rate</th>
                                                            </tr>
                                                            <tr>
                                                                <th>Kode Parts</th>
                                                                <th>Nama Parts</th>
                                                                <th>Qty Satuan</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="workOrder">

                                                            </tbody>
                                                        </table>

                                                    </div>
                                                </td>
                                                <td style="padding-left: 20px;width: 50%;">
                                                    <div class="box">
                                                        <legend style="font-size: medium">Actual Pengerjaan Job dan Pemakaian Parts</legend>

                                                        <table style="width: 100%;border: 1px;padding-left: 10px;padding-right: 10px;border-style: solid;">
                                                            <thead>
                                                            <tr>
                                                                <th rowspan="2">Kode Job</th>
                                                                <th colspan="2">Nama Job</th>
                                                                <th>Rate</th>
                                                            </tr>
                                                            <tr>
                                                                <th>Kode Parts</th>
                                                                <th>Nama Parts</th>
                                                                <th>Qty Satuan</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="ActualOrder">

                                                            </tbody>
                                                        </table>

                                                    </div>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2">
                                        <div class="box">
                                            <legend style="font-size: medium">Grid View Penambahan/Pengurangan Job/Part Selama Production</legend>
                                            <table style="width: 100%;border: 1px;padding-left: 10px;padding-right: 10px;border-style: solid;">
                                                <thead>
                                                <tr>
                                                    <th rowspan="2">Kode Job</th>
                                                    <th colspan="4">Nama Job</th>
                                                    <th rowspan="2">Status</th>
                                                    <th rowspan="2" style="width: 50%;">Alasan</th>
                                                </tr>
                                                <tr>
                                                    <th>Kode Parts</th>
                                                    <th>Nama Parts</th>
                                                    <th>Qty</th>
                                                    <th>Satuan</th>
                                                </tr>
                                                </thead>
                                                <tbody id="PenambahanPenguranganJob">

                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>

                            </table>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: right">
                            Page 1
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <fieldset class="buttons controls pull-right" style="padding-top: 10px;">

            %{--<button class="btn btn-primary" id="buttonEdit" onclick="onEdit()">Edit</button>--}%
            <button class="btn btn-primary" id="buttonNext">Next</button>
            %{--<button class="btn btn-primary" id="buttonCancel" onclick="onCancel()">Cancel</button>--}%

            <button class="btn btn-primary" id="buttonClose1" onclick="editJobParts();">Edit Job dan Parts</button>

        </fieldset>
    </div>
    <div id="halaman2" style="display: none">

        <div class="box">

            <table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">
                <tr>
                    <td colspan="2">

                        <div class="box">
                            <legend style="font-size: medium">Informasi Umum</legend>

                            <table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">
                                <tr>
                                    <td style="width: 80%;vertical-align: top;">

                                        <table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">
                                            <thead>
                                            <tr>
                                                <th>Nomor WO</th>
                                                <th>Nomor Polisi</th>
                                                <th>Model</th>
                                                <th>Tanggal WO</th>
                                                <th>Waktu Penyerahan</th>
                                                <th>SPK Ass.</th>
                                                <th>No. SPK Asuransi</th>
                                                <th>PKS</th>
                                                <th>Saldo PKS</th>
                                            </tr>
                                            </thead>
                                            <tbody id="informasiUmumHalaman2">

                                            </tbody>
                                        </table>

                                    </td>
                                </tr>
                            </table>

                        </div>

                    </td>
                </tr>
                <tr>
                    <td style="width: 70%;vertical-align: top;">

                        <div class="box">
                            <legend style="font-size: medium">Job Order Completion</legend>

                            <table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">
                                <tr>
                                    <td style="width: 80%;vertical-align: top;">

                                        <table style="width: 98%;border: 1px;padding-left: 10px;padding-right: 10px;border-style: solid;">
                                            <thead>
                                            <tr>
                                                <th rowspan="2">Kode Job</th>
                                                <th colspan="5">Nama Job</th>
                                                <th rowspan="2">TWC</th>
                                                <th rowspan="2">RTJ</th>
                                                <th rowspan="2" style="width: 15%;">Status</th>
                                                <th rowspan="2" style="width: 15%;">Nominal</th>
                                            </tr>
                                            <tr>
                                                <th>Kode Parts</th>
                                                <th>Nama Parts</th>
                                                <th>Qty</th>
                                                <th>Satuan</th>
                                                <th>PWC</th>
                                            </tr>
                                            </thead>
                                            <tbody id="JobOrderCompletion">

                                            </tbody>
                                        </table>

                                    </td>
                                    <td style="width: 20%;vertical-align: top;">
                                        <legend style="font-size: 14px">
                                            Ringkasan Total Biaya
                                        </legend>
                                        <table class="table table-bordered table-dark table-hover">
                                            <tr>
                                                <td style="width: 35%">Sub Total</td>
                                                <td style="width: 65%">
                                                    <span id="lblSubTotal" class="property-value"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Disc. Jasa</td>
                                                <td style="color: red">
                                                    <span id="lblDiscJasa" class="property-value"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Disc Part</td>
                                                <td style="color: red">
                                                    <span id="lblDiscPart" class="property-value"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Booking Fee</td>
                                                <td style="color: red">
                                                    <span id="lblBookingFee" class="property-value"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Sisa</td>
                                                <td>
                                                    <span id="lblSisa" class="property-value"></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                        </div>

                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: right">
                        Page 2
                    </td>
                </tr>
            </table>

        </div>

        <fieldset class="buttons controls pull-right" style="padding-top: 10px;">

            <button class="btn btn-primary" id="buttonBack">Back</button>
            <button class="btn btn-primary" id="buttonEditJobPart" onclick="editJobParts()">Edit Job dan Parts..</button>
            <button class="btn btn-primary" id="buttonSave" onclick="doFinishJOC();">JOC Finish</button>

        </fieldset>
    </div>
    </g:formRemote>
    <div id="approvalFinishJOCModal" class="modal fade">
        <div class="modal-dialog" style="width: 500px;">
            <div class="modal-content" style="width: 500px;">
                <!-- dialog body -->

                <div class="modal-body" style="max-height: 500px;">
                    <div id="approvalFinishJOCContent"/>
                    <div class="iu-content"></div>
    
                </div>
                <!-- dialog buttons -->
                %{--<div class="modal-footer">--}%
                    %{--<button id='closePopup' type="button" class="btn btn-primary" data-dismiss="modal">Close</button>--}%
                %{--</div>--}%
            </div>
        </div>
    </div>
</body>
</html>