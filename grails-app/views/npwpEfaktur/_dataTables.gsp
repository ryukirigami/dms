
<%@ page import="com.kombos.maintable.NpwpEfaktur" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="npwpEfaktur_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="npwpEfaktur.namaPerusahaan.label" default="Nama Perusahaan" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="npwpEfaktur.npwp.label" default="Npwp" /></div>
            </th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="npwpEfaktur.alamatNpwp.label" default="Alamat Npwp" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="npwpEfaktur.penandaTangan.label" default="Penanda Tangan" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="npwpEfaktur.penandaTangan.label" default="Jabatan" /></div>
			</th>

		
		</tr>
		<tr>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_namaPerusahaan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_namaPerusahaan" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_npwp" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_npwp" class="search_init" />
                </div>
            </th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_alamatNpwp" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_alamatNpwp" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_penandaTangan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_penandaTangan" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_jabatan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_jabatan" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var npwpEfakturTable;
var reloadNpwpEfakturTable;
$(function(){
	
	reloadNpwpEfakturTable = function() {
		npwpEfakturTable.fnDraw();
	}

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	npwpEfakturTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	npwpEfakturTable = $('#npwpEfaktur_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "namaPerusahaan",
	"mDataProp": "namaPerusahaan",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "npwp",
	"mDataProp": "npwp",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}


,

{
	"sName": "alamatNpwp",
	"mDataProp": "alamatNpwp",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}


,

{
	"sName": "penandaTangan",
	"mDataProp": "penandaTangan",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "jabatan",
	"mDataProp": "jabatan",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var alamatNpwp = $('#filter_alamatNpwp input').val();
						if(alamatNpwp){
							aoData.push(
									{"name": 'sCriteria_alamatNpwp', "value": alamatNpwp}
							);
						}
	
						var namaPerusahaan = $('#filter_namaPerusahaan input').val();
						if(namaPerusahaan){
							aoData.push(
									{"name": 'sCriteria_namaPerusahaan', "value": namaPerusahaan}
							);
						}
	
						var npwp = $('#filter_npwp input').val();
						if(npwp){
							aoData.push(
									{"name": 'sCriteria_npwp', "value": npwp}
							);
						}
	
						var penandaTangan = $('#filter_penandaTangan input').val();
						if(penandaTangan){
							aoData.push(
									{"name": 'sCriteria_penandaTangan', "value": penandaTangan}
							);
						}
						var jabatan = $('#filter_jabatan input').val();
						if(jabatan){
							aoData.push(
									{"name": 'sCriteria_jabatan', "value": jabatan}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
