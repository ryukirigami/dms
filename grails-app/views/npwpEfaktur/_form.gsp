<%@ page import="com.kombos.maintable.NpwpEfaktur" %>

<div class="control-group fieldcontain ${hasErrors(bean: npwpEfakturInstance, field: 'namaPerusahaan', 'error')} ">
    <label class="control-label" for="namaPerusahaan">
        <g:message code="npwpEfaktur.namaPerusahaan.label" default="Nama Perusahaan" />
        <span class="required-indicator">*</span>

    </label>
    <div class="controls">
        <g:textField name="namaPerusahaan" required="" value="${npwpEfakturInstance?.namaPerusahaan}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: npwpEfakturInstance, field: 'npwp', 'error')} ">
    <label class="control-label" for="npwp">
        <g:message code="npwpEfaktur.npwp.label" default="Npwp" />
        <span class="required-indicator">*</span>

    </label>
    <div class="controls">
        <g:textField name="npwp" required="" value="${npwpEfakturInstance?.npwp}" maxlength="15" />
    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: npwpEfakturInstance, field: 'alamatNpwp', 'error')} ">
	<label class="control-label" for="alamatNpwp">
		<g:message code="npwpEfaktur.alamatNpwp.label" default="Alamat Npwp" />
        <span class="required-indicator">*</span>

	</label>
	<div class="controls">
    <g:textArea name="alamatNpwp" required="" value="${npwpEfakturInstance?.alamatNpwp}" />
	</div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: npwpEfakturInstance, field: 'penandaTangan', 'error')} ">
	<label class="control-label" for="penandaTangan">
		<g:message code="npwpEfaktur.penandaTangan.label" default="Penanda Tangan" />
        <span class="required-indicator">*</span>

	</label>
	<div class="controls">
	<g:textField name="penandaTangan" required="" value="${npwpEfakturInstance?.penandaTangan}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: npwpEfakturInstance, field: 'jabatan', 'error')} ">
    <label class="control-label" for="jabatan">
        <g:message code="npwpEfaktur.penandaTangan.label" default="Jabatan" />
        <span class="required-indicator">*</span>

    </label>
    <div class="controls">
        <g:textField name="jabatan" required="" value="${npwpEfakturInstance?.jabatan}" />
    </div>
</div>
