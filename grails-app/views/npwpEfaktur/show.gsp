

<%@ page import="com.kombos.maintable.NpwpEfaktur" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'npwpEfaktur.label', default: 'Npwp Efaktur')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteNpwpEfaktur;

$(function(){ 
	deleteNpwpEfaktur=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/npwpEfaktur/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadNpwpEfakturTable();
   				expandTableLayout('npwpEfaktur');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-npwpEfaktur" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="npwpEfaktur"
			class="table table-bordered table-hover">
			<tbody>
            <g:if test="${npwpEfakturInstance?.namaPerusahaan}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="namaPerusahaan-label" class="property-label"><g:message
                                code="npwpEfaktur.namaPerusahaan.label" default="Nama Perusahaan" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="namaPerusahaan-label">
                        %{--<ba:editableValue
                                bean="${npwpEfakturInstance}" field="namaPerusahaan"
                                url="${request.contextPath}/NpwpEfaktur/updatefield" type="text"
                                title="Enter namaPerusahaan" onsuccess="reloadNpwpEfakturTable();" />--}%

                        <g:fieldValue bean="${npwpEfakturInstance}" field="namaPerusahaan"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${npwpEfakturInstance?.npwp}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="npwp-label" class="property-label"><g:message
                                code="npwpEfaktur.npwp.label" default="Npwp" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="npwp-label">
                        %{--<ba:editableValue
                                bean="${npwpEfakturInstance}" field="npwp"
                                url="${request.contextPath}/NpwpEfaktur/updatefield" type="text"
                                title="Enter npwp" onsuccess="reloadNpwpEfakturTable();" />--}%

                        <g:fieldValue bean="${npwpEfakturInstance}" field="npwp"/>

                    </span></td>

                </tr>
            </g:if>

				
				<g:if test="${npwpEfakturInstance?.alamatNpwp}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="alamatNpwp-label" class="property-label"><g:message
					code="npwpEfaktur.alamatNpwp.label" default="Alamat Npwp" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="alamatNpwp-label">
						%{--<ba:editableValue
								bean="${npwpEfakturInstance}" field="alamatNpwp"
								url="${request.contextPath}/NpwpEfaktur/updatefield" type="text"
								title="Enter alamatNpwp" onsuccess="reloadNpwpEfakturTable();" />--}%
							
								<g:fieldValue bean="${npwpEfakturInstance}" field="alamatNpwp"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${npwpEfakturInstance?.penandaTangan}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="penandaTangan-label" class="property-label"><g:message
                                code="npwpEfaktur.penandaTangan.label" default="Penanda Tangan" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="penandaTangan-label">
                        %{--<ba:editableValue
                                bean="${npwpEfakturInstance}" field="penandaTangan"
                                url="${request.contextPath}/NpwpEfaktur/updatefield" type="text"
                                title="Enter penandaTangan" onsuccess="reloadNpwpEfakturTable();" />--}%

                        <g:fieldValue bean="${npwpEfakturInstance}" field="penandaTangan"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${npwpEfakturInstance?.jabatan}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="jabatan-label" class="property-label"><g:message
                                code="npwpEfaktur.jabatan.label" default="Jabatan" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="penandaTangan-label">
                        %{--<ba:editableValue
                                bean="${npwpEfakturInstance}" field="penandaTangan"
                                url="${request.contextPath}/NpwpEfaktur/updatefield" type="text"
                                title="Enter penandaTangan" onsuccess="reloadNpwpEfakturTable();" />--}%

                        <g:fieldValue bean="${npwpEfakturInstance}" field="jabatan"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${npwpEfakturInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="npwpEfaktur.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${npwpEfakturInstance}" field="createdBy"
								url="${request.contextPath}/NpwpEfaktur/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadNpwpEfakturTable();" />--}%
							
								<g:fieldValue bean="${npwpEfakturInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${npwpEfakturInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="npwpEfaktur.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${npwpEfakturInstance}" field="dateCreated"
								url="${request.contextPath}/NpwpEfaktur/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadNpwpEfakturTable();" />--}%
							
								<g:formatDate date="${npwpEfakturInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>


			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('npwpEfaktur');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${npwpEfakturInstance?.id}"
					update="[success:'npwpEfaktur-form',failure:'npwpEfaktur-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteNpwpEfaktur('${npwpEfakturInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
