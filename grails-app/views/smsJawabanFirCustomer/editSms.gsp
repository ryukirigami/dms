
<%@ page import="com.kombos.administrasi.Operation; com.kombos.administrasi.NamaProsesBP; com.kombos.parts.Returns" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'returnsAdd.label', default: 'Edit Sms Jawaban FIR')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
        return false;
	});


   });
    $(document).ready(function()
    {

        save = function(){
                var id = $('#id').val();
                var pertanyaan = $('#pertanyaan').val();
                var jawaban = $('#jawaban').val();
                $.ajax({
                    url:'${request.contextPath}/smsJawabanFirCustomer/save',
                    type: "POST", // Always use POST when deleting data
                    data : {id:id,jawaban: jawaban,pertanyaan:pertanyaan},
                    success : function(data){
                        window.location.href = '#/smsJawabanFirCustomer'
                        $("#valid").html(data.valid)
                        $("#invalid").html(data.invalid)
                        $("#validate").html(data.validate)
                        $("#all").html(data.all)
                       	reloadSmsJawabanFirCustomerTable();
                       	reloadDataSmsTable();
                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });

        }
    });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
    <ul class="nav pull-right">
        <li></li>
        <li></li>
        <li class="separator"></li>
    </ul>
</div>
<div class="box">
    <div class="span12" id="sms-table">
        <p>Pilih nilai di kolom Q1, Q2, Q3 dan seterusnya untuk mengedit jawaban yang formatnya tidak valid <br> pada TextBox di bawah</p>
        <br>
        <table>
            <tr>
                <td style="width: 50%">Nomor Wo :  ${noWo} </td>
                <td style="width: 50%; padding-left: 20px" rowspan="2">${pertanyaan} &nbsp; <g:textArea name="jawaban" id="jawaban" value="${nilai}" /></td>
            </tr>
            <tr>
                <g:hiddenField name="id" value="${id}" id="id" />
                <g:hiddenField name="pertanyaan" value="${tanyaId}" id="pertanyaan" />
                <td style="width: 50%"> Nomor Polisi  : ${nopol}</td>
            </tr>
            <tr>
                <td> &nbsp;</td>
                <td> <button style="width: 60px;height: 30px; border-radius: 5px" type="button" class="btn btn-cancel" onclick="save()" >Save</button>
                    <button id='btn2' type="button" class="btn btn-cancel" style="width: 60px;">Close</button>
            </tr>
        </table>
    </div>
</div>
</body>
</html>

