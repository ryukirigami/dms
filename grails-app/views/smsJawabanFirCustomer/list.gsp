
<%@ page import="com.kombos.administrasi.KodeKotaNoPol"%>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'smsJawabanFirCustomer.label', default: 'SMS Jawaban FIR Customer')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>
        $(function(){
        $("#smsAddModal").on("show", function() {
		$("#smsAddModal .btn").on("click", function(e) {
			$("#smsAddModal").modal('hide');
		});
        });
        $("#smsAddModal").on("hide", function() {
            $("#smsAddModal a.btn").off("click");
        });
            ubah = function(id,a,p){

                $("#smsAddContent").empty();
                    expandTableLayout();
                    $.ajax({type:'POST', url:'${request.contextPath}/smsJawabanFirCustomer/editSms',
                    data: { id: id, isi:a, pertanyaan : p},
                        success:function(data,textStatus){
                                $("#smsAddContent").html(data);
                                $("#smsAddModal").modal({
                                    "backdrop" : "static",
                                    "keyboard" : true,
                                    "show" : true
                                }).css({'width': '840px','margin-left': function () {return -($(this).width() / 2);}});

                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
            }
            clickFollowUp = function(){
                var id = ""
                checkSmsJawabanFirCustomer =[];
                $("#smsJawabanFirCustomer-table tbody .row-select").each(function() {
                     if(this.checked){
                       id = $(this).next("input:hidden").val();
                       checkSmsJawabanFirCustomer .push(id);
                      }
                  });
                   if(checkSmsJawabanFirCustomer .length<1 || checkSmsJawabanFirCustomer .length>1 ){
                    alert('Silahkan Pilih Salah satu');
                    return;

                   }else{
                        $.ajax({
                            url:'${request.contextPath}/followUp/list',
                            type: "POST",
                            data: { id: id},
                            dataType: "html",
                            complete : function (req, err) {
                                $('#main-content').html(req.responseText);
                                $('#spinner').fadeOut();
                                loading = false;
                            }
                        });

                   }
            }
        });
                loadForm = function(data, textStatus){
                    $('#smsJawabanFirCustomer-form').empty();
                    $('#smsJawabanFirCustomer-form').append(data);
                }
                shrinkTableLayout = function(){
                    $("#smsJawabanFirCustomer-table").hide();
                    $("#smsJawabanFirCustomer-form").css("display","block");
                }

                expandTableLayout = function(){
                    $("#smsJawabanFirCustomer-table").show();
                    $("#smsJawabanFirCustomer-form").css("display","none");
                }


    var checkin = $('#search_TanggalSms').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
        var newDate = new Date(ev.date)
        newDate.setDate(newDate.getDate() + 1);
        checkout.setValue(newDate);
        checkin.hide();
        $('#search_TanggalSmsAkhir')[0].focus();
    }).data('datepicker');
    
    var checkout = $('#search_TanggalSmsAkhir').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

    var checkin2 = $('#search_TanggalService').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
                var newDate2 = new Date(ev.date)
                newDate2.setDate(newDate2.getDate() + 1);
                checkout2.setValue(newDate2);
                checkin2.hide();
                $('#search_TanggalServiceAkhir')[0].focus();
    }).data('datepicker');

    var checkout2 = $('#search_TanggalServiceAkhir').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin2.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
                checkout2.hide();
    }).data('datepicker');


    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

        return true;
    }
                $(document).ready(function() {
                    $("#nopol3").keyup(function(e) {
                        var isi = $(e.target).val();
                        $(e.target).val(isi.toUpperCase());
                    });


                });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="smsJawabanFirCustomer.view.label" default="SMS Jawaban FIR Customer" /></span>
    <ul class="nav pull-right">

    </ul>
</div>
<div class="box">
    <div class="span12" id="smsJawabanFirCustomer-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>

        <table>
            <tr>
                <td style="padding: 5px">
                    <g:message code="wo.tanggalWOs.label" default="Tanggal SMS"/>
                </td>
                <td style="padding: 5px">
                    <ba:datePicker name="search_TanggalSms" id="search_TanggalSms" precision="day" value="" disable="true" format="dd-MM-yyyy"/>&nbsp;s.d.&nbsp;
                    <ba:datePicker name="search_TanggalSmsAkhir" id="search_TanggalSmsAkhir" precision="day" value="" disable="true" format="dd-MM-yyyy"/>
                </td>
                <td style="padding: 5px">
                    <g:message code="smsJawabanFirCustomer.staJobTambah.label" default="Nomor Polisi"/>
                </td>
                <td style="padding: 5px">
                    <g:select name="nopol1" id="nopol1" from="${KodeKotaNoPol.createCriteria().list {eq("staDel",'0')}}" optionKey="id" style="width: 70px;"/>
                    <g:textField name="nopol2" id="nopol2" style="width: 70px" maxlength="5" onkeypress="return isNumberKey(event)" />
                    <g:textField name="nopol3" id="nopol3" style="width: 40px" maxlength="3" />
                </td>

                <td rowspan="3" >
                    <div class="controls" style="right: 0">
                        <button style="width: 70px;height: 30px; border-radius: 5px" class="btn-primary view" name="view" id="view" >Filter</button>
                        <button style="width: 70px;height: 30px; border-radius: 5px" class="btn cancel" name="clear" id="clear" >Clear</button>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    <g:message code="wo.tanggalWOs.label" default="Tanggal Service"/>
                </td>
                <td style="padding: 5px">
                    <ba:datePicker name="search_TanggalService" id="search_TanggalService" precision="day" value="" disable="true" format="dd-MM-yyyy"/>&nbsp;s.d.&nbsp;
                    <ba:datePicker name="search_TanggalServiceAkhir" id="search_TanggalServiceAkhir" precision="day" value="" disable="true" format="dd-MM-yyyy"/>
                </td>
                <td style="padding: 5px">
                    <g:message code="smsJawabanFirCustomer.staCarryOver.label" default="Nomor WO"/>
                </td>
                <td style="padding: 5px">
                    <g:textField name="nomorWo" id="nomorWo" style="width: 200px"/>
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    <g:message code="smsJawabanFirCustomer.staProblemFinding.label" default="Inisial SA"/>
                </td>

                <td style="padding: 5px">
                    <g:select style="width:100%" name="inisialSA" id="inisialSA" from="${realisasiFu}" noSelection="${['':'Semua']}" />
                </td>
                <td style="padding: 5px">
                    <g:message code="smsJawabanFirCustomer.staFinalInspection.label" default="Status Balasan"/>
                </td>
                <td style="padding: 5px">
                    <select name="statusBalasan" id="statusBalasan">
                        <option value="">Semua</option>
                        <option value="Valid">Valid</option>
                        <option value="Invalid">Invalid</option>
                        <option value="Please Validate!!!">Need Validation</option>
                    </select>
                </td>
            </tr>
        </table>
        <br/>
        <g:render template="dataTables" />
        <br/>
        <table style="width: 60%; margin-left: 500px">
            <tr>
                <td style="background-color: #06B33A; width: 50px; text-align: center; height: 30px" id="valid">${cValid}</td>
                <td style="padding-left: 5px;">Valid</td>
                <td style="width: 20px;">&nbsp;</td>
                <td style="background-color: #cd0a0a; width: 50px; text-align: center" id="invalid">${cInvalid}</td>
                <td style="padding-left: 5px;">Invalid</td>
                <td style="width: 15px;">&nbsp;</td>
                <td style="background-color: #fbed50; width: 50px; text-align: center" id="validate">${cValidate}</td>
                <td style="padding-left: 10px;">Need Validation</td>
                <td style="width: 15px;">&nbsp;</td>
                <td style="background-color: #999999; width: 50px; text-align: center" id="all">${cAll}</td>
                <td style="padding-left: 10px;">Total Follow Up</td>
            </tr>
        </table>
        <table style="width: 100%">
            <tr style="height: 100%">
                <td style="width: 30%">
                    <g:render template="dataTablesDataSms" /></td>
                <td style="width: 70%; padding: 60px;">
                    &nbsp;
                </td>
            </tr>
        </table>
        <div class="controls" style="right: 0">
            <button style="width: 120px;height: 30px; border-radius: 5px" class="btn cancel" name="followUp" id="followUp" disabled="disabled" onclick="clickFollowUp()" >Follow Up</button>
        </div>
    </div>

    <div class="span7" id="smsJawabanFirCustomer-form" style="display: none; width: 1200px;"></div>
</div>
<div id="smsAddModal" class="modal fade"style="width: 790px;">
    <div class="modal-dialog" style="width:780px;">
        <div class="modal-content" style="width: 790px;">
            <!-- dialog body -->
            <div class="modal-body" style="max-height: 460px; width: 790px;">
                <div id="smsAddContent"/>
                <div class="iu-content"></div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
