
<r:require modules="baseapplayout" />

<g:render template="../menu/maxLineDisplay"/>

<table id="dataSms_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="WO.jobOnly.label" default="Jawaban" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="WO.jobOnly.label" default="Q1" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="WO.teknisi.label" default="Q2" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="WO.rate.label" default="Q3" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="WO.rate.label" default="Q4" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="WO.rate.label" default="Q5" /></div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
    var dataSmsTable;
    var reloadDataSmsTable;
    $(function(){

        reloadDataSmsTable = function() {
            dataSmsTable.fnClearTable();
            dataSmsTable.fnDraw();
        }

        dataSmsTable = $('#dataSms_datatables').dataTable({
            "sScrollX": "100%",
            "bScrollCollapse": true,
            "bAutoWidth" : false,
            "bPaginate" : false,
            "sInfo" : "",
            "sInfoEmpty" : "",
            "sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
            bFilter: true,
            "bStateSave": false,
            'sPaginationType': 'bootstrap',
            "fnInitComplete": function () {
                this.fnAdjustColumnSizing(true);
            },
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                return nRow;
            },
            "bSort": true,
            "bProcessing": true,
            "bServerSide": true,
            "bInfo" : false,
            "sServerMethod": "POST",
            "iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
            "sAjaxSource": "${g.createLink(action: "getData")}",
            "bDestroy": true,
            "aoColumns": [

                {
                    "sName": "",
                    "mDataProp": "jawaban",
                    "aTargets": [0],
                    "bSearchable": true,
                    "bSortable": false,
                    "sWidth":"50px",
                    "bVisible": true
                }

                ,

                {
                    "sName": "",
                    "mDataProp": "Q1",
                    "aTargets": [1],
                    "bSearchable": true,
                    "bSortable": false,
                    "sWidth":"20px",
                    "bVisible": true
                }


                ,

                {
                    "sName": "",
                    "mDataProp": "Q2",
                    "aTargets": [2],
                    "bSearchable": true,
                    "bSortable": false,
                    "sWidth":"20px",
                    "bVisible": true
                }

                ,

                {
                    "sName": "",
                    "mDataProp": "Q3",
                    "aTargets": [3],
                    "bSearchable": true,
                    "bSortable": false,
                    "sWidth":"20px",
                    "bVisible": true
                }

                ,

                {
                    "sName": "",
                    "mDataProp": "Q4",
                    "aTargets": [4],
                    "bSearchable": true,
                    "bSortable": false,
                    "sWidth":"20px",
                    "bVisible": true
                }

                ,

                {
                    "sName": "",
                    "mDataProp": "Q5",
                    "aTargets": [5],
                    "bSearchable": true,
                    "bSortable": false,
                    "sWidth":"20px",
                    "bVisible": true
                }


            ],
            "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                $.ajax({ "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData ,
                    "success": function (json) {
                        fnCallback(json);
                       },
                    "complete": function () {
                       }
                });
            }

        });

    });
</g:javascript>

