
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>


<table id="smsJawabanFirCustomer_datatables_${idTable}" cellpadding="0" cellspacing="0" border="0"
       class="display table table-striped table-bordered table-hover" style="table-layout: fixed; ">
    <thead>
    <tr>
        <th style="border-bottom: none;padding: 5px;">
            <g:message code="smsJawabanFirCustomerss.label" default="Tanggal Kirim SMS"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="smsJawabanFirCustomerss.label" default="Nomor Wo"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="smsJawabanFirCustomer.tanggalWO.label" default="SA"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="smsJawabanFirCustomer.start.label" default="Nomor Polisi"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="smsJawabanFirCustomer.stop.label" default="Q1"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="smsJawabanFirCustomer.finalInspection.label" default="Q2"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="smsJawabanFirCustomer.teknisi.label" default="Q3"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="smsJawabanFirCustomer.teknisi.label" default="Q4"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="smsJawabanFirCustomer.teknisi.label" default="Q5"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="smsJawabanFirCustomer.sa.label" default="Status"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="smsJawabanFirCustomer.sa.label" default="Keterangan"/>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var smsJawabanFirCustomerTable;
var reloadSmsJawabanFirCustomerTable;
var followUp="";
$(function(){

 	$('#view').click(function(e){
 	    followUp=$('#statusBalasan').val();
 	    if(followUp=='Invalid'){
 	        $('#followUp').prop('disabled',false)
 	    }else{
 	        $('#followUp').prop('disabled',true)
 	    }
        smsJawabanFirCustomerTable.fnDraw();
	});
	$('#clear').click(function(e){
	    followUp = "";
	    $('#search_TanggalSms').val('');
	    $('#search_TanggalSmsAkhir').val('');
        $('#search_TanggalService').val('');
	    $('#search_TanggalServiceAkhir').val('');
	    $('#nopol1').val('');
	    $('#nopol2').val('');
	    $('#nopol3').val('');
	    $('#nomorWo').val('');
	    $('#inisialSA').val('');
	    $('#statusBalasan').val('');
	    $('#followUp').prop('disabled',true);
        smsJawabanFirCustomerTable.fnDraw();
	});
 
	reloadSmsJawabanFirCustomerTable = function() {

		smsJawabanFirCustomerTable.fnDraw();
	}


    smsJawabanFirCustomerTable = $('#smsJawabanFirCustomer_datatables_${idTable}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
               $(nRow).children().each(function(index, td) {
                        if(index === 9){
                            if (aData.ket == "Valid") {
                                 $(td).html("<img src='images/followUp/ok.png' style='display: block; margin: 0 auto; text-align: center; '/>");
                            }else if (aData.ket == "Invalid") {
                                 $(td).html("<img src='images/followUp/remove.png' style='display: block; margin: 0 auto; text-align: center; '/>");
                            }else{
                                 $(td).html("<img src='images/followUp/warning.png' style='display: block; margin: 0 auto; text-align: center; '/>");
                            }

                        }
                        if(index == 10){
                            if ($(td).html() == "Valid") {
                                   $(td).css("background-color","#06B33A");
                            }else if ($(td).html() == "Invalid") {
                                   $(td).css("background-color","#cd0a0a");
                            }else{
                                   $(td).css("background-color","#fbed50");
                            }

                        }
               });

			    return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"bDestroy": true,
		"aoColumns": [
 {
	"sName": "t802TglJamFU",
	"mDataProp": "tanggalSMS",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select"  aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'"> &nbsp; '+data;
	},
	"bSortable": false,
	"sWidth":"160px",
	"bVisible": true
}
,
{
	"sName": "t802NamaSA_FU",
	"mDataProp": "nomorWo",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"110px",
	"bVisible": true
}
,
{
	"sName": "followUp",
	"mDataProp": "SA",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"120px",
	"bVisible": true
}
,
{
	"sName": "followUp",
	"mDataProp": "noPol",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"100px",
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "Q1",
	"aTargets": [4],
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="ubah('+row['id']+',\''+data+'\',\'Q1\');">'+data+'</a>';
	},
	"sWidth":"80px",
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "Q2",
	"aTargets": [5],
	"sWidth":"80px",
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="ubah(\''+row['id']+'\',\''+data+'\',\'Q2\');">'+data+'</a>';
	},
	"bSortable": false,
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "Q3",
	"aTargets": [6],
	"sWidth":"80px",
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="ubah(\''+row['id']+'\',\''+data+'\',\'Q3\');">'+data+'</a>';
	},
	"bSortable": false,
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "Q4",
	"aTargets": [7],
	"sWidth":"80px",
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="ubah(\''+row['id']+'\',\''+data+'\',\'Q4\');">'+data+'</a>';
	},
	"bSortable": false,
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "Q5",
	"aTargets": [8],
	"sWidth":"80px",
	"mRender": function ( data, type, row ) {
        return '<input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="ubah(\''+row['id']+'\',\''+data+'\',\'Q5\');">'+data+'</a>';
	},
	"bSortable": false,
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "status",
	"aTargets": [9],
	"bSortable": false,
	"sWidth":"55px",
	"bVisible": true
},
{
	"sName": "followUp",
	"mDataProp": "ket",
	"aTargets": [10],
	"bSortable": false,
	"sWidth":"130px",
	"bVisible": true
}

],
    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
    
                        var Tanggal = $('#search_TanggalSms').val();
						var TanggalDay = $('#search_TanggalSms_day').val();
						var TanggalMonth = $('#search_TanggalSms_month').val();
						var TanggalYear = $('#search_TanggalSms_year').val();

	                    var Tanggalakhir = $('#search_TanggalSmsAkhir').val();
                        var TanggalDayakhir = $('#search_TanggalSmsAkhir_day').val();
                        var TanggalMonthakhir = $('#search_TanggalSmsAkhir_month').val();
                        var TanggalYearakhir = $('#search_TanggalSmsAkhir_year').val();


                        if(Tanggal){
                            aoData.push(
                                    {"name": 'search_TanggalSms', "value": "date.struct"},
                                    {"name": 'search_TanggalSms_dp', "value": Tanggal},
                                    {"name": 'search_TanggalSms_day', "value": TanggalDay},
                                    {"name": 'search_TanggalSms_month', "value": TanggalMonth},
                                    {"name": 'search_TanggalSms_year', "value": TanggalYear}
                            );
                        }

                        if(Tanggalakhir){
                            aoData.push(
                                    {"name": 'search_TanggalSmsAkhir', "value": "date.struct"},
                                    {"name": 'search_TanggalSmsAkhir_dp', "value": Tanggalakhir},
                                    {"name": 'search_TanggalSmsAkhir_day', "value": TanggalDayakhir},
                                    {"name": 'search_TanggalSmsAkhir_month', "value":TanggalMonthakhir},
                                    {"name": 'search_TanggalSmsAkhir_year', "value": TanggalYearakhir}
                            );
                        }

                        var Tanggal2 = $('#search_TanggalService').val();
						var Tanggal2Day = $('#search_TanggalService_day').val();
						var Tanggal2Month = $('#search_TanggalService_month').val();
						var Tanggal2Year = $('#search_TanggalService_year').val();

	                    var Tanggal2akhir = $('#search_TanggalServiceAkhir').val();
                        var Tanggal2Dayakhir = $('#search_TanggalServiceAkhir_day').val();
                        var Tanggal2Monthakhir = $('#search_TanggalServiceAkhir_month').val();
                        var Tanggal2Yearakhir = $('#search_TanggalServiceAkhir_year').val();


                        if(Tanggal2){
                            aoData.push(
                                    {"name": 'search_TanggalService', "value": "date.struct"},
                                    {"name": 'search_TanggalService_dp', "value": Tanggal2},
                                    {"name": 'search_TanggalService_day', "value": Tanggal2Day},
                                    {"name": 'search_TanggalService_month', "value": Tanggal2Month},
                                    {"name": 'search_TanggalService_year', "value": Tanggal2Year}
                            );
                        }

                        if(Tanggal2akhir){
                            aoData.push(
                                    {"name": 'search_TanggalServiceAkhir', "value": "date.struct"},
                                    {"name": 'search_TanggalServiceAkhir_dp', "value": Tanggal2akhir},
                                    {"name": 'search_TanggalServiceAkhir_day', "value": Tanggal2Dayakhir},
                                    {"name": 'search_TanggalServiceAkhir_month', "value":Tanggal2Monthakhir},
                                    {"name": 'search_TanggalServiceAkhir_year', "value": Tanggal2Yearakhir}
                            );
                        }


						var statusBalasan = $('#statusBalasan').val();
						if(statusBalasan){
							aoData.push(
									{"name": 'statusBalasan', "value": statusBalasan}
							);
						}

						var nopol1 = $('#nopol1').val();
						if(nopol1){
							aoData.push(
									{"name": 'nopol1', "value": nopol1}
							);
						}
						var nopol2 = $('#nopol2').val();
						if(nopol2){
							aoData.push(
									{"name": 'nopol2', "value": nopol2}
							);
						}
						var nopol3 = $('#nopol3').val();
						if(nopol3){
							aoData.push(
									{"name": 'nopol3', "value": nopol3}
							);
						}

						var inisialSA = $('#inisialSA').val();
						if(inisialSA){
							aoData.push(
									{"name": 'inisialSA', "value": inisialSA}
							);
						}
						var nomorWo = $('#nomorWo').val();
						if(nomorWo){
							aoData.push(
									{"name": 'nomorWo', "value": nomorWo}
							);
						}



    $.ajax({ "dataType": 'json',
        "type": "POST",
        "url": sSource,
        "data": aoData ,
        "success": function (json) {
            fnCallback(json);
           },
        "complete": function () {
           }
    });
}
});
});


</g:javascript>
