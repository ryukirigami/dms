<%@ page import="com.kombos.finance.PecahanUang" %>



<div class="control-group fieldcontain ${hasErrors(bean: pecahanUangInstance, field: 'pecahanUang', 'error')} ">
	<label class="control-label" for="pecahanUang">
		<g:message code="pecahanUang.pecahanUang.label" default="Pecahan Uang" />
		
	</label>
	<div class="controls">
	<g:textField name="pecahanUang" value="${pecahanUangInstance?.pecahanUang}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: pecahanUangInstance, field: 'nilai', 'error')} ">
	<label class="control-label" for="nilai">
		<g:message code="pecahanUang.nilai.label" default="Nilai" />
		
	</label>
	<div class="controls">
	<g:field name="nilai" type="number" value="${pecahanUangInstance.nilai}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: pecahanUangInstance, field: 'type', 'error')} ">
    <label class="control-label" for="type">
        Tipe Pecahan
    </label>
    <div class="controls">
        <g:select name="type" from="${pecahanUangInstance.constraints.type.inList}" value="${pecahanUangInstance?.type}"/>
    </div>
</div>


%{--

<div class="control-group fieldcontain ${hasErrors(bean: pecahanUangInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="pecahanUang.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${pecahanUangInstance?.lastUpdProcess}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: pecahanUangInstance, field: 'cashBalanceDetail', 'error')} ">
	<label class="control-label" for="cashBalanceDetail">
		<g:message code="pecahanUang.cashBalanceDetail.label" default="Cash Balance Detail" />
		
	</label>
	<div class="controls">
	
<ul class="one-to-many">
<g:each in="${pecahanUangInstance?.cashBalanceDetail?}" var="c">
    <li><g:link controller="cashBalanceDetail" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="cashBalanceDetail" action="create" params="['pecahanUang.id': pecahanUangInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'cashBalanceDetail.label', default: 'CashBalanceDetail')])}</g:link>
</li>
</ul>

	</div>
</div>

--}%
