<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
		<g:javascript>
var vopSubTable;
$(function(){ 
    vopSubTable = $('#vop_datatables_sub_${idTable}').dataTable({
		"sScrollX": "1246px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": '${g.createLink(action: "datatablesSubList")}',
		"aoColumns": [
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"11px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"11px",
   "sDefaultContent": ''
},
{
	"sName": "",
	"mDataProp": "noWO",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left sub-select row-sub-'+row['idInv']+'" onclick="cekSubList(\''+row['idInv']+'\');" ${isSelected=='true' ? 'checked' : ''} aria-label="Row '+data+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;' + data;
	},
	"bSortable": false,
	"sWidth":"212px",
	"bVisible": true
},
{
	"sName": "",
	"mDataProp": "noPolisi",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"212px",
	"bVisible": true
},
{
	"sName": "",
	"mDataProp": "kodeJob",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"170px",
	"bVisible": true
},
{
	"sName": "",
	"mDataProp": "namaJob",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"399px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "harga",
	"aTargets": [4],
	"bSortable": false,
	"sWidth":"248px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
            aoData.push(
                {"name": 'noInvoice', "value": '${noInvoice}'}
            );
            aoData.push(
                {"name": 'idInv', "value": '${idInv}'}
            );

            $.ajax({ "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData ,
                "success": function (json) {
                    fnCallback(json);
                   },
                "complete": function () {
                   }
                    });
		}			
	});
});
		</g:javascript>
	</head>
	<body>
<div class="innerDetails">
<table id="vop_datatables_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
	class="display table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th></th>
			<th></th>
			<th style="border-bottom: none; padding: 5px; width: 200px;">
				<div>Nomor WO</div>
			</th>
			<th style="border-bottom: none; padding: 5px; width: 200px;">
				<div>Nomor Polisi</div>
			</th>
			<th style="border-bottom: none; padding: 5px; width: 200px;">
				<div>Kode Job</div>
			</th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div>Nama Job</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div>Harga</div>
            </th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
</body>
</html>
