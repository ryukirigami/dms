
<%@ page import="com.kombos.parts.Vendor; com.kombos.parts.Invoice" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<title><g:message code="viewInvoice.label" default="View Invoice" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	var isSelected;

	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		window.location.href = '#/inputInvoiceReception' ;
                $('#spinner').fadeIn(1);
                $.ajax({
                    url: '${request.contextPath}/inputInvoiceReception',
                    type: "GET",dataType:"html",
                    complete : function (req, err) {
                        $('#main-content').html(req.responseText);
                        $('#spinner').fadeOut();
                    }
                });
            break;
            case '_DELETE_' :
                bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
                    function(result){
                        if(result){
                            var recordsToDeleteInvoice = [];
                            $("#viewInvoice-table tbody .row-select").each(function() {
                                if(this.checked){
                                    var id = $(this).next("input:hidden").val();
                                    recordsToDeleteInvoice.push(id);
                                }
                            });

                            var jsonInvoice = JSON.stringify(recordsToDeleteInvoice);

                            var recordsToDelete = [];
                            $("#viewInvoice-table tbody .sub-select").each(function() {
                                if(this.checked){
                                    var id = $(this).next("input:hidden").val();
                                    recordsToDelete.push(id);
                                }
                            });

                            var json = JSON.stringify(recordsToDelete);

                            $.ajax({
                                url:'${request.contextPath}/viewInvoice/massdelete',
                                type: "POST", // Always use POST when deleting data
                                data: {
                                    idsInvoice: jsonInvoice,
                                    idsJobInv: json
                                },
                                complete: function(xhr, status) {
                                    reloadViewInvoiceTable();
                                }
                            });
                        }
                    });

                    break;
               };
               return false;
        });

    edit = function(id) {
        $('#spinner').fadeIn(1);
        window.location.href='#/editInvoice'
        $.ajax({url: '${request.contextPath}/inputInvoiceReception?idUbah='+id,
            type:"GET",dataType: "html",
            complete : function (req, err) {
                $('#main-content').html(req.responseText);
                $('#spinner').fadeOut();
                loading = false;
            }
        });
    };
    
});

    function clearSearch(){
        $('#search_t413TanggalInv_start').val('');
        $('#search_t413TanggalInv_end').val('');
        reloadViewInvoiceTable();
    };

    function cekList(id){
        if($('#invoice-row-'+id).is(':checked')){
            $(".row-sub-"+id+"").attr("checked",true);
        }else{
            $(".row-sub-"+id+"").attr("checked",false);
        }
    }

    function cekSubList(id){
        var total=0;
        var cek=0;
        $("#viewInvoice-table tbody .row-sub-"+id+"").each(function() {
            total=parseInt(total)+1
            if(this.checked){
                cek=parseInt(cek)+1
            }
        });
        if(total==cek){
            $('#invoice-row-'+id).attr("checked",true);
        }else{
            $('#invoice-row-'+id).attr("checked",false);
        }
        var terCek = 0,takCek = 0
        $("#viewInvoice-table tbody .row-select").each(function() {
            takCek+=1
            if(this.checked){
                terCek+=1
            }
        });
        console.log(terCek+" --- "+takCek)
        if(terCek==takCek){
            console.log('SAMA');
            $('#btnCheckAll').prop("checked",true);
            alert('check-all')
            console.log('SYUDAH')
        }
    }

</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="viewInvoice.label" default="View Invoice" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
            <li><a class="pull-right box-action" href="#"
                   style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
                        class="icon-remove"></i>&nbsp;&nbsp;
            </a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="viewInvoice-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <fieldset>
                <table style="padding-right: 10px">
                    <tr>
                        <td style="width: 130px">
                            <label class="control-label" for="t413TanggalInv">
                                <g:message code="viewInvoice.tanggal.label" default="Tanggal Invoice" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="t413TanggalInv" class="controls">
                                <ba:datePicker id="search_t413TanggalInv_start" name="search_t413TanggalInv_start" precision="day" format="dd/MM/yyyy"  value="" />
                                &nbsp;s.d.&nbsp;
                                <ba:datePicker id="search_t413TanggalInv_end" name="search_t413TanggalInv_end" precision="day" format="dd/MM/yyyy"  value="" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" >
                            <div class="controls pull-right" style="right: 0">
                                <a class="btn btn-primary create" href="javascript:void(0);" onclick="reloadViewInvoiceTable();">
                                    <g:message code="default.search.label" default="Search"/>
                                </a>

                                <a class="btn cancel" href="javascript:void(0);" onclick="clearSearch();">
                                    <g:message code="default.clear.label" default="Clear Search"/>
                                </a>
                            </div>
                        </td>
                    </tr>
                </table>
            </fieldset>
			<g:render template="dataTables" />
            </div>
		<div class="span7" id="viewInvoice-form" style="display: none;"></div>
	</div>
</body>
</html>
