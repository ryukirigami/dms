
<%@ page import="com.kombos.parts.Invoice" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="viewInvoice_datatables_${idTable}" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover" style="table-layout: fixed; ">
    <thead>
        <tr>
            <th><input type="checkbox" class="select-all" id="btnCheckAll" aria-label="Select all" title="Select all"/></th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div>Nama Vendor</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div>Nomor Invoice</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div>Tanggal Invoice</div>
            </th>
            %{--<th style="border-bottom: none; padding: 5px; width: 200px;"/>--}%
        </tr>
    </thead>
</table>

<g:javascript>
var viewInvoiceTable;
var reloadViewInvoiceTable;

$(function(){

	reloadViewInvoiceTable = function() {
		viewInvoiceTable.fnDraw();
	}

    var recordsviewInvoiceperpage = [];//new Array();
    var anViewInvoicesSelected;
    var jmlRecViewInvoicesPerPage=0;
    var id;
    var anOpen = [];

    $('#viewInvoice_datatables_${idTable} td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var vendorId = $(this).closest('tr').find("input:hidden").val().substring(0,$(this).closest('tr').find("input:hidden").val().indexOf(';'));
            var sname = "invoice-row-"  + vendorId;
            var idInvSub = vendorId.substring(0,vendorId.indexOf('-'))
            if ($('#' + sname).is(':checked')) {
                isSelected = "true";
            } else {
                isSelected = "false";
            }

    		var oData = viewInvoiceTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${request.contextPath}/viewInvoice/sublist?isSelected='+isSelected,
	   			success:function(data,textStatus){
	   				var nDetailsRow = viewInvoiceTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});

  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			viewInvoiceTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

	viewInvoiceTable = $('#viewInvoice_datatables_${idTable}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            //alert( $('li.active').text() );
            var rsViewInvoice = $("#viewInvoice_datatables_${idTable} tbody .row-select");
            var jmlViewInvoiceCek = 0;
            var idRec;
            var nRow;
            rsViewInvoice.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsviewInvoiceperpage[idRec]=="1"){
                    jmlViewInvoiceCek = jmlViewInvoiceCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsviewInvoiceperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecViewInvoicePerPage = rsViewInvoice.length;
            if(jmlViewInvoiceCek==jmlRecViewInvoicePerPage && jmlRecViewInvoicePerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
   "mDataProp": null,
   "sClass": "control center",
   "bSortable": false,
   "sWidth":"10px",
   "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "",
	"mDataProp": "vendor",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
    	return '<input type="checkbox" id="invoice-row-'+row['id']+'" onclick="cekList(\''+row['id']+'\');"  class="pull-left row-select" aria-label="Row '+data+'" title="Select this"><input type="hidden" value="'+row['id']+';'+row['idVendor']+'">&nbsp;&nbsp;'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onClick="edit(\''+row['noInvoice']+'\')">&nbsp;&nbsp;<i class="icon-pencil" ></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"435px",
	"bVisible": true
},
{
	"sName": "",
	"mDataProp": "noInvoice",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"495px",
	"bVisible": true
},
{
	"sName": "",
	"mDataProp": "tglInvoice",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"260px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var tanggalStart = $("#search_t413TanggalInv_start").val();
						var tanggalStartDay = $('#search_t413TanggalInv_start_day').val();
						var tanggalStartMonth = $('#search_t413TanggalInv_start_month').val();
						var tanggalStartYear = $('#search_t413TanggalInv_start_year').val();
                        if(tanggalStart){
							aoData.push(
									{"name": 'tanggalStart', "value": "date.struct"},
									{"name": 'tanggalStart_dp', "value": tanggalStart},
									{"name": 'tanggalStart_day', "value": tanggalStartDay},
									{"name": 'tanggalStart_month', "value": tanggalStartMonth},
									{"name": 'tanggalStart_year', "value": tanggalStartYear}
							);
						}

                        var tanggalEnd = $("#search_t413TanggalInv_end").val();
                        var tanggalEndDay = $('#search_t413TanggalInv_end_day').val();
						var tanggalEndMonth = $('#search_t413TanggalInv_end_month').val();
						var tanggalEndYear = $('#search_t413TanggalInv_end_year').val();
						if(tanggalEnd){
							aoData.push(
									{"name": 'tanggalEnd', "value": "date.struct"},
									{"name": 'tanggalEnd_dp', "value": tanggalEnd},
									{"name": 'tanggalEnd_day', "value": tanggalEndDay},
									{"name": 'tanggalEnd_month', "value": tanggalEndMonth},
									{"name": 'tanggalEnd_year', "value": tanggalEndYear}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
	
        $('.select-all').click(function(e) {
            $("#viewInvoice_datatables_${idTable} tbody .row-select").each(function() {
                var vendorId = $(this).next("input:hidden").val().replace(';','-');
                var idInvSub = vendorId.substring(0,vendorId.indexOf('-'))
                if(this.checked){
                    $(".row-sub-"+idInvSub).attr("checked",true);
                    recordsviewInvoiceperpage[$(this).next("input:hidden").val()] = "1";
                } else {
                    $(".row-sub-"+idInvSub).attr("checked",false);
                    recordsviewInvoiceperpage[$(this).next("input:hidden").val()] = "0";
                }
            });
        });

        $('#viewInvoice_datatables_${idTable} tbody tr').live('click', function () {
            id = $(this).find('.row-select').next("input:hidden").val();
            if($(this).find('.row-select').is(":checked")){
                recordsviewInvoiceperpage[id] = "1";
                $(this).find('.row-select').parent().parent().addClass('row_selected');
                anViewInvoiceSelected = viewInvoiceTable.$('tr.row_selected');
                if(jmlRecViewInvoicePerPage == anViewInvoiceSelected.length){
                    $('.select-all').attr('checked', true);
                }
            } else {
                recordsviewInvoiceperpage[id] = "0";
                $('.select-all').attr('checked', false);
                $(this).find('.row-select').parent().parent().removeClass('row_selected');
            }
        });
});
</g:javascript>


			
