<%@ page import="com.kombos.parts.PartsAdjust" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    %{--<g:set var="entityName" value="${message(code: 'partsAdjust.label', default: 'Approve On Hand Adjustment')}"/>--}%
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <r:require modules="baseapplayout"/>
    <g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :
                window.location.replace('#/inputOnHandAdjustment');
                break;
            case '_DELETE_' :
                bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});


    resetCari = function(){
        $('#search_t145TglAdj').val('');
        $('#search_t145TglAdjakhir').val('');
        $('input[name=t145StatusApprove]').attr('checked',false);
        $('#nilaiRadio').val('');
        reloadPartsAdjustTable();
    }

    cekRadio = function(){
        if(document.getElementById('t145StatusApproveSudah').checked) {
          $('#nilaiRadio').val('1');
        }else if(document.getElementById('t145StatusApproveBelum').checked) {
          $('#nilaiRadio').val('0');
        }else if(document.getElementById('t145StatusApproveTidak').checked) {
          $('#nilaiRadio').val('2');
        }
    }
    //darisini
	$("#partsAdjModal").on("show", function() {
		$("#partsAdjModal .btn").on("click", function(e) {
			$("#partsAdjModal").modal('hide');
		});
	});
	$("#partsAdjModal").on("hide", function() {
		$("#partsAdjModal a.btn").off("click");
	});

    loadpartsAdjModal = function(id){
         var checkGoods =[];
            $("#partsAdjust-table tbody .row-select").each(function() {
            if(this.checked){
                var id = $(this).next("input:hidden").val();
                var nRow = $(this).parents('tr')[0];
                var aData = partsAdjustTable.fnGetData(nRow);
                checkGoods.push(id);
            }
            });
            if(checkGoods.length<1){
                alert('Anda belum memilih data');
            }else{
                $("#partsAdjContent").empty();
                $.ajax({type:'POST', url:'${request.contextPath}/partsAdjustApprove/popUp',
                    data : {status:id},
                    success:function(data,textStatus){
                            $("#partsAdjContent").html(data);
                            $("#partsAdjModal").modal({
                                "backdrop" : "static",
                                "keyboard" : true,
                                "show" : true
                            }).css({'width': '800px','margin-left': function () {return -($(this).width() / 2);}});

                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){},
                    complete:function(XMLHttpRequest,textStatus){
                        $('#spinner').fadeOut();
                    }
                });
            }
    }

});
    </g:javascript>

    <style>
    div.innerDetails {
        display: block;
    }
    </style>

</head>

<body>
<div class="navbar box-header no-border">
    <span class="pull-left">Approve On Hand Adjustment</span>
    <ul class="nav pull-right">

        <li class="separator"></li>
    </ul>
</div>

<div class="box">
    <div class="span12" id="partsAdjust-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>

        <table>
            <tr>
                <td colspan="2">
                    <legend style="font-size: small">
                        Search Kriteria
                    </legend>
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    <g:message code="partsAdjust.t145TglAdj.label" default="Tanggal Adjusment"/>
                </td>
                <td style="padding: 5px">
                    <ba:datePicker style="width:70px" name="search_t145TglAdj" id="search_t145TglAdj" precision="day" value="" format="dd-MM-yyyy"/>&nbsp;s.d.&nbsp;
                    <ba:datePicker style="width:70px" name="search_t145TglAdjakhir" id="search_t145TglAdjakhir" precision="day" value="" format="dd-MM-yyyy"/>
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    <g:message code="partsAdjust.t145StatusApprove.label" default="Status Adjusment"/>
                </td>
                <td style="padding: 5px">
                    <input type="hidden" name="nilaiRadio" id="nilaiRadio" />
                    <input type="radio" name="t145StatusApprove" id="t145StatusApproveSudah" onclick="cekRadio();" />&nbsp;Approved
                    <input type="radio" name="t145StatusApprove" id="t145StatusApproveTidak" onclick="cekRadio();" />&nbsp;Unapproved
                    <input type="radio" name="t145StatusApprove" id="t145StatusApproveBelum" onclick="cekRadio();" />&nbsp;Belum Approve
                </td>
            </tr>
            <tr>
                <td colspan="2" class="pull-right">

                        <g:field type="button" class="btn btn-primary create" onclick="reloadPartsAdjustTable();"
                                 name="view" id="view" value="${message(code: 'default.search.label', default: 'Search')}" />
                        &nbsp;&nbsp;
                        <g:field type="button" class="btn cancel" onclick="resetCari();"
                                     name="clear" id="clear" value="${message(code: 'default.clearsearch.label', default: 'Clear Search')}" />

                </td>
            </tr>
        </table>
        <br/><br/>
        <g:render template="dataTables"/>
    </div>
    <div>
        <g:field type="button" class="btn btn-primary create" onclick="loadpartsAdjModal('Approve')"
                 name="view" id="view" value="${message(code: 'default.Approve.label', default: 'Approve')}" />
        &nbsp;&nbsp;
        <g:field type="button" class="btn btn-primary create" onclick="loadpartsAdjModal('UnApprove')"
                 name="view" id="view" value="${message(code: 'default.UnApprove.label', default: 'UnApprove')}" />
    </div>
</div>
<div id="partsAdjModal" class="modal fade">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content" style="width: 800px;">
            <!-- dialog body -->
            <div class="modal-body" style="max-height: 800px;">
                <div id="partsAdjContent"/>
                <div class="iu-content"></div>

            </div>
        </div>
    </div>
</div>
</body>
</html>
