
<%@ page import="com.kombos.parts.Claim" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
        var show;
        var loadForm;
        var shrinkTableLayout;
        var expandTableLayout;

        $(function(){

            $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

            $('.box-action').click(function(){
                return false;
            });

            approve = function(){
                     var formAdjust = $('#PopUp-table').find('form');
                    var checkGoods =[];
                    $("#partsAdjust-table tbody .row-select").each(function() {
                        if(this.checked){
                            var id = $(this).next("input:hidden").val();
                            var nRow = $(this).parents('tr')[0];
                            var aData = partsAdjustTable.fnGetData(nRow);
                            checkGoods.push(id);
                        }
                    });
                    $("#ids").val(JSON.stringify(checkGoods));
                    $.ajax({type:'POST', url:'${request.contextPath}/partsAdjustApprove/sendApprove',
                    data : formAdjust.serialize(),
                    success:function(data,textStatus){
                        alert('Succes Approve');
                        reloadPartsAdjustTable();
                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){},
                    complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                    }
                    });
                reloadPartsAdjustTable();
           }

           unApprove = function(){
                     var formAdjust = $('#PopUp-table').find('form');
                    var checkGoods =[];
                    $("#partsAdjust-table tbody .row-select").each(function() {
                        if(this.checked){
                            var id = $(this).next("input:hidden").val();
                            var nRow = $(this).parents('tr')[0];
                            var aData = partsAdjustTable.fnGetData(nRow);
                            checkGoods.push(id);
                        }
                    });
                    $("#ids").val(JSON.stringify(checkGoods));
                    $.ajax({type:'POST', url:'${request.contextPath}/partsAdjustApprove/sendUnApprove',
                    data : formAdjust.serialize(),
                    success:function(data,textStatus){
                        alert('Succes unApprove');
                        reloadPartsAdjustTable();
                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){},
                    complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                    }
                    });
                reloadPartsAdjustTable();
           }


    });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left">Pesan ${status}</span>
</div>
<div class="box">
    <div class="span12" id="PopUp-table">
        <form id="form-adjust" class="form-horizontal">
            <input type="hidden" name="ids" id="ids" value="">
        <g:textArea name="pesan" id="pesan" style="width: 100%" /> <br> <br>
        <g:if test="${status=='Approve'}">
            <g:field type="button" onclick="approve();" class="btn btn-primary create" name="tambah" id="tambah" value="${message(code: 'default.button.upload.label', default: 'Approve')}" />
        </g:if>
        <g:else>
            <g:field type="button" onclick="unApprove();" class="btn btn-primary create" name="tambah" id="unapprove" value="${message(code: 'default.button.upload.label', default: 'UnApprove')}" />
        </g:else>
        <span style="margin-left: 550px"><button id='closeSpkDetail' type="button" class="btn btn-primary">Close</button></span>
        </form>
    </div>
</div>
</body>
</html>
