
<%@ page import="com.kombos.parts.PartsAdjust" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="partsAdjust_datatables_${idTable}" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

            <th style="border-bottom: none;width: 9px;">
            </th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="partsAdjust.t145ID.label" default="T145 ID" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="partsAdjust.t145TglAdj.label" default="T145 Tgl Adj" /></div>
			</th>

            <th style="border-bottom: none;width: 50px;">
                <div><g:message code="partsAdjust.status.label" default="Status" /></div>
            </th>

		</tr>
	</thead>
</table>

<g:javascript>
var partsAdjustTable;
var reloadPartsAdjustTable;
var anOpen = [];
//var sImageUrl = "${request.contextPath}/images/";

$(function(){
	
	reloadPartsAdjustTable = function() {
		partsAdjustTable.fnDraw();
	}

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	partsAdjustTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

    var anOpen = [];
	$('#partsAdjust_datatables_${idTable} td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = partsAdjustTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${request.contextPath}/partsAdjustApprove/sublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = partsAdjustTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});

  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			partsAdjustTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});



	partsAdjustTable = $('#partsAdjust_datatables_${idTable}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

		{
               "mDataProp": null,
               "sClass": "control center",
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},


{
	"sName": "t145ID",
	"mDataProp": "t145ID",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "t145TglAdj",
	"mDataProp": "t145TglAdj",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "status",
	"mDataProp": "status",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	

						aoData.push(
									{"name": 'idGoods', "value": ${idGoods}}
						);

						var t145ID = $('#filter_t145ID input').val();
						if(t145ID){
							aoData.push(
									{"name": 'sCriteria_t145ID', "value": t145ID}
							);
						}


                        var t145StatusApprove = $('#nilaiRadio').val();
						if(t145StatusApprove){
							aoData.push(
									{"name": 'sCriteria_t145StatusApprove', "value": t145StatusApprove}
							);
						}

						var t145TglAdj = $('#search_t145TglAdj').val();
						var t145TglAdjDay = $('#search_t145TglAdj_day').val();
						var t145TglAdjMonth = $('#search_t145TglAdj_month').val();
						var t145TglAdjYear = $('#search_t145TglAdj_year').val();
						
						if(t145TglAdj){
							aoData.push(
									{"name": 'sCriteria_t145TglAdj', "value": "date.struct"},
									{"name": 'sCriteria_t145TglAdj_dp', "value": t145TglAdj},
									{"name": 'sCriteria_t145TglAdj_day', "value": t145TglAdjDay},
									{"name": 'sCriteria_t145TglAdj_month', "value": t145TglAdjMonth},
									{"name": 'sCriteria_t145TglAdj_year', "value": t145TglAdjYear}
							);
						}
	
						var t145TglAdj = $('#search_t145TglAdjakhir').val();
						var t145TglAdjDay = $('#search_t145TglAdjakhir_day').val();
						var t145TglAdjMonth = $('#search_t145TglAdjakhir_month').val();
						var t145TglAdjYear = $('#search_t145TglAdjakhir_year').val();

						if(t145TglAdj){
							aoData.push(
									{"name": 'sCriteria_t145TglAdjakhir', "value": "date.struct"},
									{"name": 'sCriteria_t145TglAdjakhir_dp', "value": t145TglAdj},
									{"name": 'sCriteria_t145TglAdjakhir_day', "value": t145TglAdjDay},
									{"name": 'sCriteria_t145TglAdjakhir_month', "value": t145TglAdjMonth},
									{"name": 'sCriteria_t145TglAdjakhir_year', "value": t145TglAdjYear}
							);
						}



						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});


});
</g:javascript>


			
