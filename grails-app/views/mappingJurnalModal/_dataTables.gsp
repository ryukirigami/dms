
<%@ page import="com.kombos.finance.AccountNumber" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="accountNumber_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover fixed " style="table-layout: fixed; ">
    <col width="200px" />
    <col width="300px" />
    <thead>
    <tr>
        <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:200px;">
            <div>&nbsp;&nbsp;
                %{--<input type="checkbox" class="select-all" aria-label="Select all" title="Select all"/>&nbsp;&nbsp;--}%
                <g:message code="accountNumber.accountNumber.label" default="Account Number" />
            </div>
        </th>

        <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:300px;">
            <div>&nbsp;&nbsp;
                <g:message code="accountNumber.accountName.label" default="Account Name" />
            </div>
        </th>
    </tr>
    <tr>
        <th style="border-top: none;padding: 0px 0px 5px 0px; width:200px;">
            <div id="filter_accountNumber" style="width: 120px;">&nbsp;&nbsp;
                <input type="text" name="search_accountNumber" class="search_init" />
            </div>
        </th>
        <th style="border-top: none;padding: 0px 0px 5px 0px; width:300px">
            <div id="filter_accountName" style="width: 120px;">&nbsp;&nbsp;
                <input type="text" name="search_accountName" class="search_init" />
            </div>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
var accountNumberTable;
var reloadAccountNumberTable;
$(function(){

	reloadAccountNumberTable = function() {
	    accountNumberTable.fnDraw();
	}
    
    var recordsaccountNumberperpage = [];
    var anAccountNumberSelected;
    var jmlRecAccountNumberPerPage=0;
    var id;


    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	accountNumberTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	accountNumberTable = $('#accountNumber_datatables').dataTable({
		"sScrollX": "500px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsAccountNumber = $("#accountNumber_datatables tbody .row-select");
            var jmlAccountNumberCek = 0;
            var nRow;
            var idRec;
            rsAccountNumber.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsaccountNumberperpage[idRec]=="1"){
                    jmlAccountNumberCek = jmlAccountNumberCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsaccountNumberperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecAccountNumberPerPage = rsAccountNumber.length;
            if(jmlAccountNumberCek==jmlRecAccountNumberPerPage && jmlRecAccountNumberPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
        "bDestroy" : true,
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "accountNumber",
	"mDataProp": "accountNumber",
	"aTargets": [0],
    "mRender": function ( data, type, row ) {
		return '<input name="accRadio" value="'+row['id']+'" type="radio" class="pull-left radio-select" aria-label="Row '+row['id']+'" title="Select this"/>&nbsp;'+data;
	},
    "bSearchable": true,
	"bSortable": true,
	"sWidth" : "200px",
	"bVisible": true
}

,

{
	"sName": "accountName",
	"mDataProp": "accountName",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth" : "300px",
	"bVisible": true
}
,

{
	"sName": "id",
	"mDataProp": "id",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"bVisible": false
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

            var accountName = $('#filter_accountName input').val();
            if(accountName){
                aoData.push(
                        {"name": 'sCriteria_accountName', "value": accountName}
                );
            }

            var m053JobsId = $('#filter_accountNumber input').val();
            if(m053JobsId){
                aoData.push(
                        {"name": 'sCriteria_accountNumber', "value": m053JobsId}
                );
            }


            $.ajax({ "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData ,
                "success": function (json) {
                    fnCallback(json);
                   },
                "complete": function () {
                   }
            });
		}
	});

    $('.select-all').click(function(e) {
        $("#accountNumber_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsaccountNumberperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsaccountNumberperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });
     $('#accountNumber_datatables tbody tr').live('click', function () {
        $(this).find('.radio-select').attr("checked",true);
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsaccountNumberperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anAccountNumberSelected = accountNumberTable.$('tr.row_selected');
            if(jmlRecAccountNumberPerPage == anAccountNumberSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsaccountNumberperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>



