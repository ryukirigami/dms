<%@ page import="com.kombos.parts.Invoice" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="inputInvoice.label" default="Input Invoice" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
        var table;
        var searchInvoice;
        var cekStatus = "${status}";
        $(function(){
            searchInvoice = function(){
                reloadModalInvoiceTable();
                $("#addJobSubletModal").modal({
                    "backdrop" : "static",
                    "keyboard" : true,
                    "show" : true
                }).css({'width': '850px','margin-left': function () {return -($(this).width() / 2);}});
            };
        });

        %{--$(document).ready(function() {--}%
            %{--var idVendor = '${vendor.id}'--}%
            %{--var root="${resource()}";--}%
            %{--var dataVendor = root+'/inputInvoiceReception/getVendor?id='+idVendor;--}%
            %{--jQuery('#vendor').load(dataVendor);--}%
        %{--});--}%

        function saveInvoice(){
            var hargaNol = false;
            var vendor = $('#vendor').val();
            var tanggal = $('#t413TanggalInv').val();
            var nomor = $('#t413NoInv').val();
            var formInvoice = $('#invoice-table').find('form');
            formInvoice.append('<input type="hidden" name="vendorId" value="'+vendor+'" class="deleteafter">');
            if(vendor=='' || tanggal=='' || nomor==''){
                alert('Data vendor, tanggal invoice dan nomor invoice tidak boleh kosong');
                return;
            }else{
                var checkInvoice =[];
                var idJob =[];
                $("#invoice-table tbody .row-select").each(function() {
                    if(this.checked){
                        var id = $(this).next("input:hidden").val();
                        var nRow = $(this).parents('tr')[0];
                        var aData = inputInvoiceTable.fnGetData(nRow);
                        var hargaInput = $('#harga'+aData['id'], nRow);
                            if(hargaInput.val()==0 || hargaInput.val()==""){
                                hargaNol = true
                            }
                        checkInvoice.push(id);
                        idJob.push(aData['kodeJob']);
                        formInvoice.append('<input type="hidden" name="harga'+aData['id'] + '" value="'+hargaInput[0].value+'" class="deleteafter">');
                    }
                });
            }

            if(checkInvoice.length<1){
                alert('Anda belum memilih data yang akan ditambahkan');
                 reloadInputInvoiceTable();
            }else if(hargaNol == true){
                alert('Harga tidak boleh 0')
                formInvoice.find('.deleteafter').remove();
                return;
            }else{
                var r = confirm("Anda yakin data akan disimpan?");
                if(r==true){
                    $("#idsReception").val(JSON.stringify(checkInvoice));
                    $("#idsJob").val(JSON.stringify(idJob));
                    $.ajax({
                        url:'${request.contextPath}/inputInvoiceReception/save',
                        type: "POST", // Always use POST when deleting data
                        data : formInvoice.serialize(),
                        success : function(data){
                            if(data=="ada"){
                                alert('Data dengan nomor invoice tersebut sudah ada')
                            }else{
                                window.location.href = '#/viewInvoice' ;
                                toastr.success('<div>Succes</div>');
                            }
                            formInvoice.find('.deleteafter').remove();
                            $("#idsReception").val('');
                            $("#idsJob").val('');
                    },
                    error: function(xhr, textStatus, errorThrown) {
                    alert('Internal Server Error');
                    }
                    });
                    checkInvoice = [];
                    idJob = [];
                }
            }
        };

        function updateInvoice(){
            var hargaNol = false;
            var vendor = $('#vendor').val();
            var tanggal = $('#t413TanggalInv').val();
            var nomor = $('#t413NoInv').val();
            var formInvoice = $('#invoice-table').find('form');
            if(vendor=='' || tanggal=='' || nomor==''){
                alert('Data vendor, tanggal invoice dan nomor invoice tidak boleh kosong');
                return;
            }else{
                var checkInvoice =[];
                var idJob =[];
                $("#invoice-table tbody .row-select").each(function() {
                    if(this.checked){
                        var id = $(this).next("input:hidden").val();
                        var nRow = $(this).parents('tr')[0];
                        var aData = inputInvoiceTable.fnGetData(nRow);
                        var hargaInput = $('#harga'+aData['id'], nRow);
                            if(hargaInput.val()==0 || hargaInput.val()==""){
                                hargaNol = true
                            }
                        checkInvoice.push(id);
                        idJob.push(aData['kodeJob']);
                        formInvoice.append('<input type="hidden" name="harga'+aData['id'] + '" value="'+hargaInput[0].value+'" class="deleteafter">');
                    }
                });
            }

            if(checkInvoice.length<1){
                alert('Anda belum memilih data yang akan ditambahkan');
                 reloadInputInvoiceTable();
            }else if(hargaNol == true){
                alert('Harga tidak boleh 0')
                formInvoice.find('.deleteafter').remove();
                return;
            }else{
                var r = confirm("Anda yakin data akan disimpan?");
                if(r==true){
                    $("#idsReception").val(JSON.stringify(checkInvoice));
                    $("#idsJob").val(JSON.stringify(idJob));
                    $.ajax({
                        url:'${request.contextPath}/inputInvoiceReception/update',
                        type: "POST", // Always use POST when deleting data
                        data : formInvoice.serialize(),
                        success : function(data){
                            window.location.href = '#/viewInvoice' ;
                            toastr.success('<div>Succes</div>');
                            formInvoice.find('.deleteafter').remove();
                            $("#idsReception").val('');
                            $("#idsJob").val('');
                    },
                    error: function(xhr, textStatus, errorThrown) {
                    alert('Internal Server Error');
                    }
                    });
                    checkInvoice = [];
                    idJob = [];
                }
            }
        }

        function addJobSublet(){
        var jobSubletTemp =[];
        $("#modalInvoice_datatables tbody .row-select").each(function() {
            if(this.checked){
                var nRow = $(this).parents('tr')[0];
                jobSubletTemp.push(nRow);
            }
        });
        if(jobSubletTemp.length<1){
            alert('Anda belum memilih data yang akan ditambahkan');
        } else {
            for (var i=0;i < jobSubletTemp.length;i++){
                var aData = modalInvoiceInputTable.fnGetData(jobSubletTemp[i]);
                inputInvoiceTable.fnAddData({
                    'id': aData['id'],
                    'noWO': aData['noWO_modal'],
                    'noPolisi': aData['noPolisi_modal'],
                    'kodeJob': aData['kodeJob_modal'],
                    'namaJob': aData['namaJob_modal'],
                    'harga': 0
                });
            }
            $('#selectAll').attr('checked',true);
            $('#selectAll').click();
            reloadModalInvoiceTable();
//            $('#addJobSubletModal').modal('hide');
//            table = $('#inputInvoice_datatables').DataTable();
        }
        }
        
        function deleteInvoice(){
            var checkID =[];
            var a=parseInt(-1);
            $('#invoice-table tbody .row-select').each(function() {
                a=parseInt(a)+1;
                if(this.checked){
                    checkID.push(a);
                }
            });

            if(checkID.length<1){
                alert('Anda belum memilih data yang akan dihapus');
                return;
            }else{
                for(var b = (checkID.length-1) ; b >= 0 ; b--){
                    inputInvoiceTable.fnDeleteRow(parseInt(checkID[b]));
                }
            }
            checkID = [];
        }

        function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode != 95  && charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
        }

    </g:javascript>
</head>
<body>
<div class="box">
    <div class="span12" id="invoice-table">
        <form id="form-invoice" class="form-horizontal">
            <input type="hidden" name="idsReception" id="idsReception" value="">
            <input type="hidden" name="idsJob" id="idsJob" value="">
            <input type="hidden" name="idsInvoice" id="idsInvoice" value="${invoiceInstance.t413NoInv}">
            <fieldset class="form" style="width: 28%">
                <g:render template="form"/>
            </fieldset>
        </form>
        <legend style="font-size: 14px"><g:message code="jobSubletInvoice.label" default="Job Sublet Invoice" /></legend>
        <g:render template="dataTables" />
        <a class="btn cancel" href="javascript:void(0);" onclick="deleteInvoice();">
            <g:message code="default.button.delete.label" default="Delete" />
        </a>
        <g:if test="${status=='update'}">
            <g:submitButton class="btn btn-primary create" onclick="updateInvoice();" name="update" id="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
        </g:if>
        <g:else>
            <g:submitButton class="btn btn-primary create" onclick="saveInvoice();" name="save" id="save" value="${message(code: 'default.button.save.label', default: 'Save')}" />
        </g:else>
        <a class="btn cancel" href="javascript:void(0);" onclick="window.location.replace('#/viewInvoice')">
            <g:message code="default.button.cancel.label" default="Cancel" />
        </a>

    </div>
    
</div>
<div id="addJobSubletModal" class="modal hide">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" style="max-height: 550px;">
                <div id="addJobSubletContent">
                    %{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}%
                    <div class="iu-content">
                        <g:render template="dataTablesModal" />
                        <a onclick="addJobSublet();" href="javascript:void(0);" class="btn btn-primary create" id="search_part_btn">Add Selected</a>
                        <a href="#" class="btn cancel pull-right" onclick="$('#selectAll').attr('checked',true);" data-dismiss="modal">Close</a>
                        %{--<div class="pull-right">--}%
                        %{--</div>--}%
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</body>
</html>
