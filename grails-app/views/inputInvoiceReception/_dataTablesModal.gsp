
<%@ page import="com.kombos.reception.InvoiceSublet" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="modalInvoice_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover fixed table-selectable" style="table-layout: fixed; ">
    <col width="200px" />
    <col width="200px" />
    <col width="200px" />
    <col width="200px" />
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:199px;">
            <div>&nbsp;&nbsp;<input type="checkbox" checked="true" name="selectAllModal" id="selectAllModal" class="select-all">&nbsp;&nbsp;
            <g:message code="reception.t401NoWO.label" default="Nomor WO" />
            </div>
        </th>
        <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:199px;">
            <div>&nbsp;&nbsp;<g:message code="wo.noPolisi.label" default="Nomor Polisi" /></div>
        </th>
        <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:199px;">
            <div>&nbsp;&nbsp;<g:message code="operation.m053Id.label" default="Kode Job" /></div>
        </th>
        <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:199px;">
            <div>&nbsp;&nbsp;<g:message code="operation.m053NamaOperation.label" default="Nama Job" /></div>
        </th>
    </tr>
    <tr>
        <th style="border-top: none;padding: 0px 0px 5px 0px; width:199px;">
            <div id="filter_nomorWO" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                &nbsp;&nbsp;<input type="text" name="search_nomorWO" class="search_init" onkeypress="return isNumberKey(event)" />
            </div>
        </th>
        <th style="border-top: none;padding: 0px 0px 5px 0px; width:199px;">
            <div id="filter_nomorPolisi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                &nbsp;&nbsp;<input type="text" name="search_nomorPolisi" class="search_init"/>
            </div>
        </th>
        <th style="border-top: none;padding: 0px 0px 5px 0px; width:199px;">
            <div id="filter_kodeJob" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                &nbsp;&nbsp;<input type="text" name="search_kodeJob" class="search_init" />
            </div>
        </th>
        <th style="border-top: none;padding: 0px 0px 5px 0px; width:199px;">
            <div id="filter_namaJob" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                &nbsp;&nbsp;<input type="text" name="search_namaJob" class="search_init"/>
            </div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var modalInvoiceInputTable;
var reloadModalInvoiceTable;
$(function(){

	var recordsModalInvoicePerPage = [];
    var anModalInvoiceSelected;
    var jmlRecModalInvoicePerPage=0;
    var id;

	reloadModalInvoiceTable = function() {
		modalInvoiceInputTable.fnDraw();
	}

	$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	modalInvoiceInputTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});


	modalInvoiceInputTable = $('#modalInvoice_datatables').dataTable({
		"sScrollX": "800px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsModalInvoice = $("#modalInvoice_datatables tbody .row-select");
            var jmlModalInvoiceCek = 0;
            var nRow;
            var idRec;
            rsModalInvoice.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsModalInvoicePerPage[idRec]=="1"){
                    jmlModalInvoiceCek = jmlModalInvoiceCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsModalInvoicePerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecModalInvoicePerPage = rsModalInvoice.length;
            if(jmlModalInvoiceCek==jmlRecModalInvoicePerPage && jmlRecModalInvoicePerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bDestroy" : true,
		"bProcessing": true,
	     "bServerSide": true,
		 "sServerMethod": "POST",
		 "iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
	     "sAjaxSource": "${g.createLink(action: "datatablesModalList")}",
		 "aoColumns": [

{
	"sName": "",
	"mDataProp": "noWO_modal",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data ;
	},
	"bSearchable": true,
	"bSortable": false,
	"bVisible": true
}

,

{
	"sName": "",
	"mDataProp": "noPolisi_modal",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"bVisible": true
}

,

{
	"sName": "",
	"mDataProp": "kodeJob_modal",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"bVisible": true
}
,

{
	"sName": "",
	"mDataProp": "namaJob_modal",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                        var nomorWO = $('#filter_nomorWO input').val();
						if(nomorWO){
							aoData.push(
									{"name": 'nomorWO', "value": nomorWO}
							);
						}
                        var nomorPolisi = $('#filter_nomorPolisi input').val();
						if(nomorPolisi){
							aoData.push(
									{"name": 'nomorPolisi', "value": nomorPolisi}
							);
						}
                        var kodeJob = $('#filter_kodeJob input').val();
						if(kodeJob){
							aoData.push(
									{"name": 'kodeJob', "value": kodeJob}
							);
						}
                        var namaJob = $('#filter_namaJob input').val();
						if(namaJob){
							aoData.push(
									{"name": 'namaJob', "value": namaJob}
							);
						}

                        var vendorId = $('#vendor').val();
						aoData.push(
                                {"name": 'vendorId', "value": vendorId}
                        );

                        var exist =[];
                        $("#inputInvoice_datatables tbody .row-select").each(function() {
                            var id = $(this).next("input:hidden").val();
                            exist.push(id);
                        });
                        if(exist.length > 0){
                            aoData.push(
                                        {"name": 'sCriteria_exist', "value": JSON.stringify(exist)}
                            );
                        }


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});

	$('.select-all').click(function(e) {

        $("#modalInvoice_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsModalInvoicePerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsModalInvoicePerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#modalInvoice_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsModalInvoicePerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anModalInvoiceSelected = modalInvoiceInputTable.$('tr.row_selected');

            if(jmlRecModalInvoicePerPage == anModalInvoiceSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsModalInvoicePerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
