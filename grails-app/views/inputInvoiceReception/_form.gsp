<%@ page import="com.kombos.parts.Vendor; com.kombos.reception.InvoiceSublet" %>

<g:javascript>
    var idVend;
    var status="belum";
    function bersihTable(){
        if(status=="belum"){
            status = "sudah";
            idVend = $('#vendor').val();
        }
        var idTemp = $('#vendor').val();
        if(idVend!=idTemp){
            var oTable = $('#inputInvoice_datatables').dataTable();
            for(var i=parseInt(oTable.fnGetData().length)-1;i>=0 ;i--){
                inputInvoiceTable.fnDeleteRow(parseInt(i));
            }
            idVend = $('#vendor').val();
        }
    }
</g:javascript>

<legend style="font-size: 14px">
    <g:if test="${status=='create'}">
        <g:message code="inputInvoice.label" default="Input Invoice" />
    </g:if>
    <g:else>
        <g:message code="updateInvoice.label" default="Update Invoice" />
    </g:else>
</legend>

<div class="control-group fieldcontain">
    <label class="control-label" for="vendor" style="text-align: left">
        <g:message code="invoice.vendor.label" default="Vendor" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="vendor" onchange="bersihTable();" name="vendor.id" from="${Vendor.createCriteria().list {eq("staDel","0");order("m121Nama")}}" noSelection="[0:'Pilih Vendor']" optionKey="id" required="" value="${vendor?.id}" class="many-to-one"/>
    </div>
</div>

<div class="control-group fieldcontain">
    <label class="control-label" for="t413TanggalInv" style="text-align: left">
        <g:message code="invoice.t413TanggalInv.label" default="Tanggal Invoice" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <ba:datePicker name="t413TanggalInv" id="t413TanggalInv" precision="day" format="dd/mm/yyyy" value="${invoiceInstance?.t413TanggalInv}" required="true"/>
    </div>
</div>

<div class="control-group fieldcontain">
	<label class="control-label" for="t413NoInv" style="text-align: left">
		<g:message code="invoice.t413NoInv.label" default="Nomor Invoice" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
            <g:textField name="t413NoInv" id="t413NoInv" maxlength="50" required="" value="${invoiceInstance?.t413NoInv}"/>
	</div>
</div>



<div class="control-group fieldcontain">
    <div class="controls pull-right" >
        <a onclick="searchInvoice();" href="javascript:void(0);" class="btn btn-primary">Search Job Sublet</a>
    </div>
</div>
