
<%@ page import="com.kombos.reception.InvoiceSublet" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="inputInvoice_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div>&nbsp;&nbsp;<input type="checkbox" name="selectAll" id="selectAll" class="select-all">&nbsp;&nbsp;
                <g:message code="reception.t401NoWO.label" default="Nomor WO" />
            </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="wo.noPolisi.label" default="Nomor Polisi" /></div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="operation.m053Id.label" default="Kode Job" /></div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="operation.m053NamaOperation.label" default="Nama Job" /></div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="default.harga.label" default="Harga" /></div>
        </th>
    </tr>

    </thead>
</table>

<g:javascript>
var inputInvoiceTable;
var reloadInputInvoiceTable;
$(function(){
	
	var recordsInputInvoicePerPage = [];
    var anInputInvoiceSelected;
    var jmlRecInputInvoicePerPage=0;
    var id;
    
	reloadInputInvoiceTable = function() {
		inputInvoiceTable.fnDraw();
	}

	
	$('#search_t162TglInputInvoice').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t162TglInputInvoice_day').val(newDate.getDate());
			$('#search_t162TglInputInvoice_month').val(newDate.getMonth()+1);
			$('#search_t162TglInputInvoice_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			inputInvoiceTable.fnDraw();
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	inputInvoiceTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	inputInvoiceTable = $('#inputInvoice_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsInputInvoice = $("#inputInvoice_datatables tbody .row-select");
            var jmlInputInvoiceCek = 0;
            var nRow;
            var idRec;
            rsInputInvoice.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsInputInvoicePerPage[idRec]=="1"){
                    jmlInputInvoiceCek = jmlInputInvoiceCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsInputInvoicePerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecInputInvoicePerPage = rsInputInvoice.length;
            if(jmlInputInvoiceCek==jmlRecInputInvoicePerPage && jmlRecInputInvoicePerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bDestroy" : true,
//		 "bProcessing": true,
//		 "bServerSide": true,
//		 "sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
//	 "sAjaxSource": "${g.createLink(action: "datablesList")}",
		"aoColumns": [

{
	"sName": "",
	"mDataProp": "noWO",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" checked="true" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data ;
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "",
	"mDataProp": "noPolisi",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "",
	"mDataProp": "kodeJob",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "",
	"mDataProp": "namaJob",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "",
	"mDataProp": "harga",
	"aTargets": [4],
    "mRender": function ( data, type, row ) {
       return '<input type="text" id="harga'+row['id']+'" class="inline-edit auto" maxlength="8" onkeypress="return isNumberKey(event);" value="'+data+'"/>';
    },
    "bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "",
	"mDataProp": "id",
	"bSortable": false,
	"sWidth":"0px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
	$('.select-all').click(function(e) {

        $("#inputInvoice_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsInputInvoicePerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsInputInvoicePerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#inputInvoice_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsInputInvoicePerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anInputInvoiceSelected = inputInvoiceTable.$('tr.row_selected');

            if(jmlRecInputInvoicePerPage == anInputInvoiceSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsInputInvoicePerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
	if(cekStatus=="update"){
        var noInvo = '${invoiceInstance?.t413NoInv}'
        $.ajax({
    		url:'${request.contextPath}/inputInvoiceReception/getTableData',
    		type: "POST", // Always use POST when deleting data
    		data: { noInvo: noInvo },
    		success : function(data){
    		    $.each(data,function(i,item){
                    inputInvoiceTable.fnAddData({
                        'id': item.id,
                        'noWO': item.noWO,
                        'noPolisi': item.noPolisi,
                        'kodeJob': item.kodeJob,
                        'namaJob': item.namaJob,
                        'harga': item.harga
                    });
                });
                $('.select-all').attr('checked',true);
            }
		});
    }
});
</g:javascript>


			
