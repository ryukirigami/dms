<%@ page import="com.kombos.administrasi.WarnaVendor" %>

<div class="control-group fieldcontain ${hasErrors(bean: warnaVendorInstance, field: 'vendorCat', 'error')} ">
    %{--<label class="control-label" for="vendorCat">--}%
        %{--<g:message code="operation.upload.label" default="Upload File" />--}%

    %{--</label>--}%
    <div class="controls">
        <input type="file" required="" id="fileExcelWarna" name="fileExcelWarna" accept="application/excel|application/vnd.ms-excel" />
        <br/>
        <a href="${request.getContextPath()}/formatFileUpload/UploadWarna.xls" >File Example Upload Warna</a>
    </div>
</div>

%{--<div class="control-group fieldcontain ${hasErrors(bean: warnaVendorInstance, field: 'vendorCat', 'error')} required">--}%
	%{--<label class="control-label" for="vendorCat">--}%
		%{--<g:message code="warnaVendor.vendorCat.label" default="Nama Vendor" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:select id="vendorCat" name="vendorCat.id" from="${com.kombos.baseapp.maintable.VendorCat.list()}" optionKey="id" required="" value="${warnaVendorInstance?.vendorCat?.id}" class="many-to-one"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: warnaVendorInstance, field: 'm192IDWarna', 'error')} required">--}%
	%{--<label class="control-label" for="m192IDWarna">--}%
		%{--<g:message code="warnaVendor.m192IDWarna.label" default="Kode Warna" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="m192IDWarna" maxlength="10" required="" value="${warnaVendorInstance?.m192IDWarna}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: warnaVendorInstance, field: 'm192NamaWarna', 'error')} required">--}%
	%{--<label class="control-label" for="m192NamaWarna">--}%
		%{--<g:message code="warnaVendor.m192NamaWarna.label" default="Nama Warna" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="m192NamaWarna" maxlength="50" required="" value="${warnaVendorInstance?.m192NamaWarna}"/>--}%
	%{--</div>--}%
%{--</div>--}%
%{--
<div class="control-group fieldcontain ${hasErrors(bean: warnaVendorInstance, field: 'createdBy', 'error')} ">
	<label class="control-label" for="createdBy">
		<g:message code="warnaVendor.createdBy.label" default="Created By" />
		
	</label>
	<div class="controls">
	<g:textField name="createdBy" value="${warnaVendorInstance?.createdBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: warnaVendorInstance, field: 'updatedBy', 'error')} ">
	<label class="control-label" for="updatedBy">
		<g:message code="warnaVendor.updatedBy.label" default="Updated By" />
		
	</label>
	<div class="controls">
	<g:textField name="updatedBy" value="${warnaVendorInstance?.updatedBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: warnaVendorInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="warnaVendor.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${warnaVendorInstance?.lastUpdProcess}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: warnaVendorInstance, field: 'staDel', 'error')} required">
	<label class="control-label" for="staDel">
		<g:message code="warnaVendor.staDel.label" default="Sta Del" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="staDel" maxlength="1" required="" value="${warnaVendorInstance?.staDel}"/>
	</div>
</div>
--}%
