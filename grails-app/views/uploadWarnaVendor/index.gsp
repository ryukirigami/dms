
<%@ page import="com.kombos.administrasi.WarnaVendor" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'warnaVendor.label', default: 'Upload File Master Warna Vendor')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />

            <g:javascript>

                <g:if test="${jmlhDataError && jmlhDataError>0}">
                    $('#save').attr("disabled", true);
                </g:if>

                var saveForm;
                $(function(){

                    function progress(e){
                        if(e.lengthComputable){
                            //kalo mau pake progress bar
                            //$('progress').attr({value:e.loaded,max:e.total});
                        }
                    }

                    saveForm = function() {

                    <g:if test="${jsonData}">
                        var sendData = ${jsonData};
                        $.ajax({
                            url:'${request.getContextPath()}/uploadWarnaVendor/upload',
                            type: 'POST',
                            xhr: function() {
                                var myXhr = $.ajaxSettings.xhr();
                                if(myXhr.upload){
                                    myXhr.upload.addEventListener('progress',progress, false);
                                }
                                return myXhr;
                            },
                            //add beforesend handler to validate or something
                            //beforeSend: functionname,
                            success: function (res) {
                                $('#warnaTable').empty();
                                $('#warnaTable').append(res);
                                //reloadOperationTable();
                            },
                            //add error handler for when a error occurs if you want!
                            error: function (data, status, e){
                                alert(e);
                            },
                            complete: function(xhr, status) {

                            },
                            data: JSON.stringify(sendData),
                            contentType: "application/json; charset=utf-8",
                            traditional: true,
                            cache: false
                        });
                    </g:if>
                    <g:else>
                        alert('No File Selected');
                    </g:else>
                    }


                });

            </g:javascript>

	</head>
	<body>
    <div id="warnaTable">
        <div class="navbar box-header no-border">
            <span class="pull-left">
                <g:message code="default.list.label" args="[entityName]" />
            </span>
            <ul class="nav pull-right">
                <li></li>
                <li></li>
                <li class="separator"></li>
            </ul>
        </div>
        <div class="box">
            <div class="span12" id="warnaVendor-table">
                <fieldset>
                    <form id="uploadWarnaVendor-save" class="form-vertical" action="${request.getContextPath()}/uploadWarnaVendor/save" method="post" onsubmit="submitForm();return false;">
                        <table>
                            <tr>
                                <td>
                                    <fieldset class="form">
                                        <g:render template="form"/>
                                    </fieldset>
                                </td>
                                <td>
                                    <fieldset class="buttons controls">
                                        <g:submitButton class="btn btn-primary create" name="view" id="view" value="${message(code: 'default.button.view.label', default: 'Upload')}" />
                                        <g:field type="button" onclick="saveForm();" class="btn btn-primary create" name="save" id="save" value="${message(code: 'default.button.upload.label', default: 'Simpan')}"/>
                                        &nbsp;&nbsp;&nbsp;
                                        <g:if test="${flash.message}">
                                            ${flash.message}
                                        </g:if>
                                    </fieldset>
                                </td>
                            </tr>
                        </table>
                    </form>
                    <g:javascript>
                        var submitForm;
                        $(function(){

                            function progress(e){
                                if(e.lengthComputable){
                                    //kalo mau pake progress bar
                                    //$('progress').attr({value:e.loaded,max:e.total});
                                }
                            }

                            submitForm = function() {

                                var form = new FormData($('#uploadWarnaVendor-save')[0]);

                                $.ajax({
                                    url:'${request.getContextPath()}/uploadWarnaVendor/view',
                                    type: 'POST',
                                    xhr: function() {
                                        var myXhr = $.ajaxSettings.xhr();
                                        if(myXhr.upload){
                                            myXhr.upload.addEventListener('progress',progress, false);
                                        }
                                        return myXhr;
                                    },
                                    //add beforesend handler to validate or something
                                    //beforeSend: functionname,
                                    success: function (res) {
                                        $('#warnaTable').empty();
                                        $('#warnaTable').append(res);
                                        //reloadOperationTable();
                                    },
                                    //add error handler for when a error occurs if you want!
                                    error: function (data, status, e){
                                        alert(e);
                                    },
                                    complete: function(xhr, status) {

                                    },
                                    data: form,
                                    cache: false,
                                    contentType: false,
                                    processData: false
                                });

                            }
                        });
                    </g:javascript>
                </fieldset>
                <br>

                    <table class="display table table-striped table-bordered table-hover dataTable" width="100%" cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px; width: 1284px;">
                        <tr>
                            <th>
                                Kode Vendor cat
                            </th>
                            <th>
                                Kode Warna
                            </th>
                            <th>
                                Nama Warna
                            </th>
                            </tr>
                        <g:if test="${htmlData}">
                            ${htmlData}
                        </g:if>
                        <g:else>
                            <tr class="odd">
                                <td class="dataTables_empty" valign="top" colspan="9">No data available in table</td>
                            </tr>
                        </g:else>
                    </table>
                    %{--<g:if test="${jsonData}">--}%
                        %{--${jsonData}--}%
                    %{--</g:if>--}%
            </div>
        </div>
    </div>
</body>
</html>
