
<%@ page import="com.kombos.administrasi.Kecamatan" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="kecamatan_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>





			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="kecamatan.m003ID.label" default="ID" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="kecamatan.m003NamaKecamatan.label" default="Nama Kecamatan" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="kecamatan.kabKota.label" default="Kab Kota" /></div>
            </th>



            <th style="border-bottom: none;padding: 5px;">
				<div><g:message code="kecamatan.provinsi.label" default="Provinsi" /></div>
			</th>

		
		</tr>
		<tr>
		
	


	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m003ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m003ID" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m003NamaKecamatan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m003NamaKecamatan" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_kabKota" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_kabKota" class="search_init" />
                </div>
            </th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_provinsi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_provinsi" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var kecamatanTable;
var reloadKecamatanTable;
$(function(){
	
	reloadKecamatanTable = function() {
		kecamatanTable.fnDraw();
	}

	var recordsPerPage = [];
    var anSelected;
    var jmlRecPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	kecamatanTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	kecamatanTable = $('#kecamatan_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rs = $("#kecamatan_datatables tbody .row-select");
            var jmlCek = 0;
            var nRow;
            var idRec;
            rs.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsPerPage[idRec]=="1"){
                    jmlCek = jmlCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecPerPage = rs.length;
            if(jmlCek==jmlRecPerPage && jmlRecPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [


{
	"sName": "m003ID",
	"mDataProp": "m003ID",
	"aTargets": [2],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m003NamaKecamatan",
	"mDataProp": "m003NamaKecamatan",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "kabKota",
	"mDataProp": "kabKota",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,



{
	"sName": "provinsi",
	"mDataProp": "provinsi",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var kabKota = $('#filter_kabKota input').val();
						if(kabKota){
							aoData.push(
									{"name": 'sCriteria_kabKota', "value": kabKota}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}
	
						var m003ID = $('#filter_m003ID input').val();
						if(m003ID){
							aoData.push(
									{"name": 'sCriteria_m003ID', "value": m003ID}
							);
						}
	
						var m003NamaKecamatan = $('#filter_m003NamaKecamatan input').val();
						if(m003NamaKecamatan){
							aoData.push(
									{"name": 'sCriteria_m003NamaKecamatan', "value": m003NamaKecamatan}
							);
						}
	
						var provinsi = $('#filter_provinsi input').val();
						if(provinsi){
							aoData.push(
									{"name": 'sCriteria_provinsi', "value": provinsi}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#kecamatan_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });
	$('#kecamatan_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anSelected = kecamatanTable.$('tr.row_selected');

            if(jmlRecPerPage == anSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
