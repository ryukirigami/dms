<%@ page import="com.kombos.administrasi.Kecamatan" %>
<g:javascript>

var selectKabupaten;

			$(function(){

			    selectKabupaten = function () {
                var idProp = $('#provinsi option:selected').val();

               if(idProp != undefined){
                    jQuery.getJSON('${request.contextPath}/kelurahan/getKabupatens?idProp='+idProp, function (data) {
                            $('#kabKota').empty();
                            if (data) {
                                jQuery.each(data, function (index, value) {
                                    $('#kabKota').append("<option value='" + value.id + "'>" + value.namaKabupaten + "</option>");
                                });
                            }

                        });

                }else{
                     $('#kabKota').empty();

                }

        };

    });
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: kecamatanInstance, field: 'provinsi', 'error')} ">
    <label class="control-label" for="provinsi">
        <g:message code="kecamatan.provinsi.label" default="Provinsi" />

    </label>
    <div class="controls">
        <g:select id="provinsi" name="provinsi.id" from="${com.kombos.administrasi.Provinsi.createCriteria().list {eq("staDel","0")}}" optionKey="id" onchange="selectKabupaten();"  required="" value="${kecamatanInstance?.provinsi?.id}" class="many-to-one"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: kecamatanInstance, field: 'kabKota', 'error')} ">
    <label class="control-label" for="kabKota">
        <g:message code="kecamatan.kabKota.label" default="Kab Kota" />

    </label>
    <div class="controls">
        <g:select id="kabKota" name="kabKota.id" from="${com.kombos.administrasi.KabKota.createCriteria().list {eq("staDel","0")}}" optionKey="id" required="" value="${kecamatanInstance?.kabKota?.id}" class="many-to-one"/>
    </div>
</div>

<g:if test="${kecamatanInstance?.m003ID}">
    <g:hiddenField name="m003ID" value="${kecamatanInstance?.m003ID}" />
</g:if>
<g:else>
    <g:hiddenField name="m003ID" value="${Kecamatan.last().m003ID.toInteger() + 1}" />
</g:else>

<div class="control-group fieldcontain ${hasErrors(bean: kecamatanInstance, field: 'm003NamaKecamatan', 'error')} ">
	<label class="control-label" for="m003NamaKecamatan">
		<g:message code="kecamatan.m003NamaKecamatan.label" default="Nama Kecamatan * " />
		
	</label>
	<div class="controls">
	<g:textField name="m003NamaKecamatan" value="${kecamatanInstance?.m003NamaKecamatan}" required="required" maxlength="40" />
	</div>
</div>

<g:javascript>

    $(function(){

        selectKabupaten();

    });

</g:javascript>


