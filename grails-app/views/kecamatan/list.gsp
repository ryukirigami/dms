
<%@ page import="com.kombos.administrasi.Kecamatan" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'kecamatan.label', default: 'Kecamatan')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			var selectKabupaten;
			$(function(){ 

			    selectKabupaten = function () {
                var idProp = $('#provinsi option:selected').val();

                alert("hallo");
                if(idProp != undefined){
                    jQuery.getJSON('${request.contextPath}/companyDealer/getKabupatens?idProp='+idProp, function (data) {
                            $('#kabkota').empty();
                            if (data) {
                                jQuery.each(data, function (index, value) {
                                    $('#kabkota').append("<option value='" + value.id + "'>" + value.namaKabupaten + "</option>");
                                });
                            }

                           // selectKecamatan();
                        });

                }else{
                     $('#kabkota').empty();
                   // $('#kecamatan').empty();

                }

        };

				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('kecamatan');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('kecamatan', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('kecamatan', '${request.contextPath}/kecamatan/massdelete', reloadKecamatanTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('kecamatan','${request.contextPath}/kecamatan/show/'+id);
				};
				
				edit = function(id) {
					editInstance('kecamatan','${request.contextPath}/kecamatan/edit/'+id);
				};

});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="kecamatan-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
		</div>
		<div class="span7" id="kecamatan-form" style="display: none;"></div>
	</div>
</body>
</html>
