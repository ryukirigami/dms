

<%@ page import="com.kombos.administrasi.Kecamatan" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'kecamatan.label', default: 'Kecamatan')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteKecamatan;

$(function(){ 
	deleteKecamatan=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/kecamatan/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadKecamatanTable();
   				expandTableLayout('kecamatan');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-kecamatan" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="kecamatan"
			class="table table-bordered table-hover">
			<tbody>


				<g:if test="${kecamatanInstance?.m003ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m003ID-label" class="property-label"><g:message
					code="kecamatan.m003ID.label" default="ID" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m003ID-label">

								<g:fieldValue bean="${kecamatanInstance}" field="m003ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kecamatanInstance?.m003NamaKecamatan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m003NamaKecamatan-label" class="property-label"><g:message
					code="kecamatan.m003NamaKecamatan.label" default="Nama Kecamatan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m003NamaKecamatan-label">

							
								<g:fieldValue bean="${kecamatanInstance}" field="m003NamaKecamatan"/>
							
						</span></td>
					
				</tr>
				</g:if>
            <g:if test="${kecamatanInstance?.kabKota}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="kabKota-label" class="property-label"><g:message
                                code="kecamatan.kabKota.label" default="Kab Kota" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="kabKota-label">

                        <g:fieldValue bean="${kecamatanInstance?.kabKota}" field="m002NamaKabKota"/>
                    </span></td>

                </tr>
            </g:if>

            <g:if test="${kecamatanInstance?.provinsi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="provinsi-label" class="property-label"><g:message
					code="kecamatan.provinsi.label" default="Provinsi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="provinsi-label">

                                <g:fieldValue bean="${kecamatanInstance?.provinsi}" field="m001NamaProvinsi"/>
						</span></td>
					
				</tr>
				</g:if>
			
		    <g:if test="${kecamatanInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="kecamatan.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">

                        <g:fieldValue bean="${kecamatanInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${kecamatanInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="kecamatan.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">

                        <g:formatDate date="${kecamatanInstance?.dateCreated}" format="EEEE, dd MMMM yyyy HH:mm"/>

                    </span></td>

                </tr>
            </g:if>


            <g:if test="${kecamatanInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="kecamatan.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${kecamatanInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/Kecamatan/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadKecamatanTable();" />--}%

                        <g:fieldValue bean="${kecamatanInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${kecamatanInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="kecamatan.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${kecamatanInstance}" field="lastUpdated"
                                url="${request.contextPath}/Kecamatan/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadKecamatanTable();" />--}%

                        <g:formatDate date="${kecamatanInstance?.lastUpdated}" format="EEEE, dd MMMM yyyy HH:mm"/>

                    </span></td>

                </tr>
            </g:if>

				<g:if test="${kecamatanInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="kecamatan.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${kecamatanInstance}" field="updatedBy"
								url="${request.contextPath}/Kecamatan/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadKecamatanTable();" />--}%
							
								<g:fieldValue bean="${kecamatanInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('kecamatan');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${kecamatanInstance?.id}"
					update="[success:'kecamatan-form',failure:'kecamatan-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteKecamatan('${kecamatanInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
