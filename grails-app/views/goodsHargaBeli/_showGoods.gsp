<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="nomorDO_table" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover fixed" style="table-layout: fixed; ">
    <col width="230px" />
    <col width="230px" />
    <col width="150px" />
    <col width="150px" />
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:230px;">
            Kode Goods
        </th>
        <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:230px;">
            Nama Goods
        </th>
        <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:150px;">
            Satuan
        </th>
        <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:150px;">
            Klasifikasi
        </th>
    </tr>

    <tr>

        <th style="border-top: none;padding: 0px 0px 5px 0px; width:230px;">
            <div id="filter_koGo" style="padding-top: 0px;position:relative; margin-top: 0px;width: 150px;">
                <input type="text" id="src_koGo" name="koGo" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 0px 0px 5px 0px; width:230px;">
            <div id="filter_naGo" style="padding-top: 0px;position:relative; margin-top: 0px;width: 150px;">
                <input type="text" id="src_naGo" name="naGo" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 0px 0px 5px 0px; width:150px;">
            <div id="filter_satGo" style="padding-top: 0px;position:relative; margin-top: 0px;width: 150px;">
                <input type="text" id="src_satGo" name="satGo" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 0px 0px 5px 0px; width:300px;">
            <div id="filter_klsGo" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">

            </div>
        </th>

    </tr>

    </thead>
</table>
<g:javascript>
var nomorDOTable;
var reloadNomorDOTable;
var setValues;
$(function(){

    $("#btnPilih").click(function() {
        $("#nomorDO_table tbody").find("tr").each(function() {
            var radio = $(this).find("td:eq(0) input[type='radio']");
            if (radio.is(":checked")) {
                var id = radio.data("id");
                var kode = radio.data("kode");
                var nama = radio.data("nama");
                $("#idGoods").val(id);
                $("#good").val(kode+" || "+nama);
            }
        })
    });

    // $("th div input").bind('keypress', function(e) {
	// 	var code = (e.keyCode ? e.keyCode : e.which);
	// 	if(code == 13) {
	// 	    alert("Test");
	// 		e.stopPropagation();
	// 	 	nomorDOTable.fnDraw();
	// 	}
	// });

	$('th div input').on('keyup keypress', function(e) {
				var keyCode = e.keyCode || e.which;
				if (keyCode === 13) {
				    nomorDOTable.fnDraw();
					e.preventDefault();
					return false;
				}
			});

	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});


    nomorDOTable = $('#nomorDO_table').dataTable({
		"sScrollX": "800px",
		"bScrollCollapse": true,
		"bAutoWidth" : true,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "dataTablesGoods")}",
		"aoColumns": [

{
	"sName": "kodeGoods",
	"mDataProp": "kodeGoods",
	"aTargets": [0],
	"bSortable": false,
	"bSearchable": true,
	"sClass": "koGo",
	"bVisible": true,
	"mRender": function ( data, type, row ) {
	    return "<input type='radio'  name='sublet' data-id='"+row["id"]+"' data-kode='"+row["kodeGoods"]+"' data-nama='"+row["namaGoods"]+"'><span class='koGo'>"+data+"</span>";
	}
},
{
    "sName": "namaGoods",
	"mDataProp": "namaGoods",
	"aTargets": [1],
	"bSortable": false,
	"bSearchable": true,
	"sClass": "naGo",
	"bVisible": true
},
{
    "sName": "satuan",
	"mDataProp": "satuan",
	"aTargets": [2],
	"bSortable": false,
	"bSearchable": true,
	"sClass": "satGo",
	"bVisible": true
},
{
    "sName": "klasifikasi",
	"mDataProp": "klasifikasi",
	"aTargets": [3],
	"bSortable": false,
	"bSearchable": true,
	"sClass": "klsGo",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var kode  = $('#filter_koGo input').val();
						var nama = $("#filter_naGo input").val();
						var satuan  = $("#filter_satGo input").val();
						var klas  = $("#filter_klsGo input").val();

						if(kode){
							aoData.push(
									{"name": 'sCriteria_koGo', "value": kode}
							);
						}

						if (nama) {
						    aoData.push(
									{"name": 'sCriteria_naGo', "value": nama}
							);
						}

                        if (satuan) {
                            aoData.push(
									{"name": 'sCriteria_satGo', "value": satuan}
							);
                        }

                        if (klas) {
                            aoData.push(
									{"name": 'sCriteria_klsGo', "value": klas}
							);
                        }

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});



});
</g:javascript>
