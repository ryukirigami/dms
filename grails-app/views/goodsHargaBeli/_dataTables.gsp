
<%@ page import="com.kombos.parts.GoodsHargaBeli" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="goodsHargaBeli_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="goodsHargaBeli.goods.label" default="Kode Goods" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="goodsHargaBeli.lastUpdProcess.label" default="Nama Goods" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="goodsHargaBeli.t150Harga.label" default="Harga Beli" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="goodsHargaBeli.t150TMT.label" default="Tanggal Berlaku" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="goodsHargaBeli.vendor.label" default="Vendor" /></div>
			</th>

		
		</tr>
		<tr>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_goods" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_goods" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_goods2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_goods2" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t150Harga" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t150Harga" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t150TMT" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_t150TMT" value="date.struct">
					<input type="hidden" name="search_t150TMT_day" id="search_t150TMT_day" value="">
					<input type="hidden" name="search_t150TMT_month" id="search_t150TMT_month" value="">
					<input type="hidden" name="search_t150TMT_year" id="search_t150TMT_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t150TMT_dp" value="" id="search_t150TMT" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_vendor" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_vendor" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var goodsHargaBeliTable;
var reloadGoodsHargaBeliTable;
$(function(){
	
	reloadGoodsHargaBeliTable = function() {
		goodsHargaBeliTable.fnDraw();
	}

	
	$('#search_t150TMT').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t150TMT_day').val(newDate.getDate());
			$('#search_t150TMT_month').val(newDate.getMonth()+1);
			$('#search_t150TMT_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			goodsHargaBeliTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	goodsHargaBeliTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	goodsHargaBeliTable = $('#goodsHargaBeli_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "goods1",
	"mDataProp": "kode",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "goods2",
	"mDataProp": "goods",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t150Harga",
	"mDataProp": "t150Harga",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t150TMT",
	"mDataProp": "t150TMT",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "vendor",
	"mDataProp": "vendor",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}
	
						var goods = $('#filter_goods input').val();
						if(goods){
							aoData.push(
									{"name": 'sCriteria_goods', "value": goods}
							);
						}
						var goods = $('#filter_goods2 input').val();
						if(goods){
							aoData.push(
									{"name": 'sCriteria_goods2', "value": goods}
							);
						}
	

	
						var t150Harga = $('#filter_t150Harga input').val();
						if(t150Harga){
							aoData.push(
									{"name": 'sCriteria_t150Harga', "value": t150Harga}
							);
						}

						var t150TMT = $('#search_t150TMT').val();
						var t150TMTDay = $('#search_t150TMT_day').val();
						var t150TMTMonth = $('#search_t150TMT_month').val();
						var t150TMTYear = $('#search_t150TMT_year').val();
						
						if(t150TMT){
							aoData.push(
									{"name": 'sCriteria_t150TMT', "value": "date.struct"},
									{"name": 'sCriteria_t150TMT_dp', "value": t150TMT},
									{"name": 'sCriteria_t150TMT_day', "value": t150TMTDay},
									{"name": 'sCriteria_t150TMT_month', "value": t150TMTMonth},
									{"name": 'sCriteria_t150TMT_year', "value": t150TMTYear}
							);
						}
	
						var vendor = $('#filter_vendor input').val();
						if(vendor){
							aoData.push(
									{"name": 'sCriteria_vendor', "value": vendor}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
