

<%@ page import="com.kombos.parts.GoodsHargaBeli" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'goodsHargaBeli.label', default: 'Goods Harga Beli')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteGoodsHargaBeli;

$(function(){ 
	deleteGoodsHargaBeli=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/goodsHargaBeli/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadGoodsHargaBeliTable();
   				expandTableLayout('goodsHargaBeli');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-goodsHargaBeli" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="goodsHargaBeli"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${goodsHargaBeliInstance?.companyDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="companyDealer-label" class="property-label"><g:message
					code="goodsHargaBeli.companyDealer.label" default="Company Dealer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="companyDealer-label">
						%{--<ba:editableValue
								bean="${goodsHargaBeliInstance}" field="companyDealer"
								url="${request.contextPath}/GoodsHargaBeli/updatefield" type="text"
								title="Enter companyDealer" onsuccess="reloadGoodsHargaBeliTable();" />--}%
							
								%{--<g:link controller="companyDealer" action="show" id="${goodsHargaBeliInstance?.companyDealer?.id}">${goodsHargaBeliInstance?.companyDealer?.encodeAsHTML()}</g:link>--}%
                        <g:fieldValue bean="${goodsHargaBeliInstance?.companyDealer}" field="m011NamaWorkshop"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${goodsHargaBeliInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="goodsHargaBeli.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${goodsHargaBeliInstance}" field="createdBy"
								url="${request.contextPath}/GoodsHargaBeli/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadGoodsHargaBeliTable();" />--}%
							
								<g:fieldValue bean="${goodsHargaBeliInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${goodsHargaBeliInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="goodsHargaBeli.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${goodsHargaBeliInstance}" field="dateCreated"
								url="${request.contextPath}/GoodsHargaBeli/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadGoodsHargaBeliTable();" />--}%
							
								<g:formatDate date="${goodsHargaBeliInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${goodsHargaBeliInstance?.goods}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="goods-label" class="property-label"><g:message
					code="goodsHargaBeli.goods.label" default="Goods" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="goods-label">
						%{--<ba:editableValue
								bean="${goodsHargaBeliInstance}" field="goods"
								url="${request.contextPath}/GoodsHargaBeli/updatefield" type="text"
								title="Enter goods" onsuccess="reloadGoodsHargaBeliTable();" />--}%
							
								%{--<g:link controller="goods" action="show" id="${goodsHargaBeliInstance?.goods?.id}">${goodsHargaBeliInstance?.goods?.encodeAsHTML()}</g:link>--}%
                        <g:fieldValue bean="${goodsHargaBeliInstance?.goods}" field="m111ID"/> ||
                        <g:fieldValue bean="${goodsHargaBeliInstance?.goods}" field="m111Nama"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${goodsHargaBeliInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="goodsHargaBeli.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${goodsHargaBeliInstance}" field="lastUpdProcess"
								url="${request.contextPath}/GoodsHargaBeli/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadGoodsHargaBeliTable();" />--}%
							
								<g:fieldValue bean="${goodsHargaBeliInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${goodsHargaBeliInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="goodsHargaBeli.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${goodsHargaBeliInstance}" field="lastUpdated"
								url="${request.contextPath}/GoodsHargaBeli/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadGoodsHargaBeliTable();" />--}%
							
								<g:formatDate date="${goodsHargaBeliInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>

			
				<g:if test="${goodsHargaBeliInstance?.t150Harga}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t150Harga-label" class="property-label"><g:message
					code="goodsHargaBeli.t150Harga.label" default="Harga Beli" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t150Harga-label">
								<g:fieldValue bean="${goodsHargaBeliInstance}" field="t150Harga"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${goodsHargaBeliInstance?.t150TMT}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t150TMT-label" class="property-label"><g:message
					code="goodsHargaBeli.t150TMT.label" default="Tanggal Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t150TMT-label">
						 		<g:formatDate date="${goodsHargaBeliInstance?.t150TMT}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${goodsHargaBeliInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="goodsHargaBeli.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
					 			<g:fieldValue bean="${goodsHargaBeliInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${goodsHargaBeliInstance?.vendor}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="vendor-label" class="property-label"><g:message
					code="goodsHargaBeli.vendor.label" default="Vendor" />:</span></td>
						<td class="span3"><span class="property-value"
						aria-labelledby="vendor-label">
					 			%{--<g:link controller="vendor" action="show" id="${goodsHargaBeliInstance?.vendor?.id}">${goodsHargaBeliInstance?.vendor?.encodeAsHTML()}</g:link>--}%
                                <g:fieldValue bean="${goodsHargaBeliInstance?.vendor}" field="m121Nama"/>
						</span></td>
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('goodsHargaBeli');"><g:message
						code="default.button.close.label" default="Close" /></a>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
