
<%@ page import="com.kombos.administrasi.KalenderKerja" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'kalenderKerja.label', default: 'KalenderKerja')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />

        <r:require modules="calendario" />


    <g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	var refreshDataKalenderKerja;
    var dateSelectedAdd;

	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/kalenderKerja/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/kalenderKerja/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#kalenderKerja-form').empty();
    	$('#kalenderKerja-form').append(data);

        try{
//            alert(dateSelectedAdd);
            $("#m031ID_start").val(dateSelectedAdd);
        }catch(e){

        }
   	}
    
    shrinkTableLayout = function(){
    	if($("#kalenderKerja-table").hasClass("span12")){
   			$("#kalenderKerja-table").toggleClass("span12 span5");
        }
        $("#kalenderKerja-form").css("display","block"); 

   	    $("#table_calendario").hide();
   	}
   	
   	expandTableLayout = function(){
   		if($("#kalenderKerja-table").hasClass("span5")){
   			$("#kalenderKerja-table").toggleClass("span5 span12");
   		}
        $("#kalenderKerja-form").css("display","none");

        $("#table_calendario").show();

        refreshDataKalenderKerja();
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#kalenderKerja-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/kalenderKerja/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		//reloadKalenderKerjaTable();
    		}
		});
		
   	}

    /*calendario*/
    var codropsEvents = {};

    $("#custom-prev").click(function(){
        cal1.gotoPreviousYear();
        cal2.gotoPreviousYear();
        cal3.gotoPreviousYear();
        cal4.gotoPreviousYear();
        cal5.gotoPreviousYear();
        cal6.gotoPreviousYear();
        cal7.gotoPreviousYear();
        cal8.gotoPreviousYear();
        cal9.gotoPreviousYear();
        cal10.gotoPreviousYear();
        cal11.gotoPreviousYear();
        cal12.gotoPreviousYear();

        $("#custom-year").html( cal1.getYear());
        refreshDataKalenderKerja();
    });

    $("#custom-next").click(function(){
        cal1.gotoNextYear();
        cal2.gotoNextYear();
        cal3.gotoNextYear();
        cal4.gotoNextYear();
        cal5.gotoNextYear();
        cal6.gotoNextYear();
        cal7.gotoNextYear();
        cal8.gotoNextYear();
        cal9.gotoNextYear();
        cal10.gotoNextYear();
        cal11.gotoNextYear();
        cal12.gotoNextYear();

        $("#custom-year").html( cal1.getYear());
        refreshDataKalenderKerja();
    });

    refreshDataKalenderKerja = function(){
        var year = 2015;
        $.ajax({
            type: "POST",
            url: '${request.contextPath}/kalenderKerja/fetchAllData',
            data: { year : year},
            async: false,
            dataType: 'json',
            success: function(data) {
                codropsEvents = [];
                $.each( data, function( key, value ) {
                    $.each( value, function( key, value ) {
                        codropsEvents[key]=value;
                    });
                });
    		}
        });

        cal1.setData(codropsEvents);
        cal2.setData(codropsEvents);
        cal3.setData(codropsEvents);
        cal4.setData(codropsEvents);
        cal5.setData(codropsEvents);
        cal6.setData(codropsEvents);
        cal7.setData(codropsEvents);
        cal8.setData(codropsEvents);
        cal9.setData(codropsEvents);
        cal10.setData(codropsEvents);
        cal11.setData(codropsEvents);
        cal12.setData(codropsEvents);
    }

    function getIdByDate(month,date,year){
        var inputDate = ""+month+"/"+date+"/"+year;
        var idResult = "0";
        $.ajax({
            type: "POST",
            url: '${request.contextPath}/kalenderKerja/fetchIdByDate',
            data: { inputDate: inputDate},
            async: false,
            success: function(data) {
                idResult= data;
    		}
        });

        return idResult;
    }

    function pad(n) {
        return (n < 10) ? ("0" + n) : n;
    }

    function showFromCalendar(month,date,year){
        var id = getIdByDate(month,date,year);
        dateSelectedAdd = ""+pad(date)+"/"+pad(month)+"/"+year;
        if(id!="0"){
            show(id);
        }else{
            $('#add_button_kalender_kerja').click();
        }
    }

    var cal1 = $("#calendar1").calendario( {
            month:1,
            year:2015,
            onDayClick : function( $el, $contentEl, dateProperties ) {
                showFromCalendar(dateProperties.month,dateProperties.day,dateProperties.year);
            },
            caldata : codropsEvents,
            displayWeekAbbr : true
        } );
    $( '#custom-month1' ).html(cal1.getMonthName());
    $( '#custom-year1' ).html(cal1.getYear());

    var cal2 = $("#calendar2").calendario( {
            month:2,
            year:2015,
            onDayClick : function( $el, $contentEl, dateProperties ) {
                showFromCalendar(dateProperties.month,dateProperties.day,dateProperties.year);
            },
            caldata : codropsEvents,
            displayWeekAbbr : true
        } );
    $( '#custom-month2' ).html( cal2.getMonthName() ),
    $( '#custom-year2' ).html( cal2.getYear() );

    var cal3 = $("#calendar3").calendario( {
            month:3,
            year:2015,
            onDayClick : function( $el, $contentEl, dateProperties ) {
                showFromCalendar(dateProperties.month,dateProperties.day,dateProperties.year);
            },
            caldata : codropsEvents,
            displayWeekAbbr : true
        } );
    $( '#custom-month3' ).html( cal3.getMonthName() ),
    $( '#custom-year3' ).html( cal3.getYear() );

    var cal4 = $("#calendar4").calendario( {
            month:4,
            year:2015,
            onDayClick : function( $el, $contentEl, dateProperties ) {
                showFromCalendar(dateProperties.month,dateProperties.day,dateProperties.year);
            },
            caldata : codropsEvents,
            displayWeekAbbr : true
        } );
    $( '#custom-month4' ).html( cal4.getMonthName() ),
    $( '#custom-year4' ).html( cal4.getYear() );

    var cal5 = $("#calendar5").calendario( {
            month:5,
            year:2015,
            onDayClick : function( $el, $contentEl, dateProperties ) {
                showFromCalendar(dateProperties.month,dateProperties.day,dateProperties.year);
            },
            caldata : codropsEvents,
            displayWeekAbbr : true
        } );
    $( '#custom-month5' ).html( cal5.getMonthName() ),
    $( '#custom-year5' ).html( cal5.getYear() );

    var cal6 = $("#calendar6").calendario( {
            month:6,
            year:2015,
            onDayClick : function( $el, $contentEl, dateProperties ) {
                showFromCalendar(dateProperties.month,dateProperties.day,dateProperties.year);
            },
            caldata : codropsEvents,
            displayWeekAbbr : true
        } );
    $( '#custom-month6' ).html( cal6.getMonthName() ),
    $( '#custom-year6' ).html( cal6.getYear() );

    var cal7 = $("#calendar7").calendario( {
            month:7,
            year:2015,
            onDayClick : function( $el, $contentEl, dateProperties ) {
                showFromCalendar(dateProperties.month,dateProperties.day,dateProperties.year);
            },
            caldata : codropsEvents,
            displayWeekAbbr : true
        } );
    $( '#custom-month7' ).html( cal7.getMonthName() ),
    $( '#custom-year7' ).html( cal7.getYear() );

    var cal8 = $("#calendar8").calendario( {
            month:8,
            year:2015,
            onDayClick : function( $el, $contentEl, dateProperties ) {
                showFromCalendar(dateProperties.month,dateProperties.day,dateProperties.year);
            },
            caldata : codropsEvents,
            displayWeekAbbr : true
        } );
    $( '#custom-month8' ).html( cal8.getMonthName() ),
    $( '#custom-year8' ).html( cal8.getYear() );

    var cal9 = $("#calendar9").calendario( {
            month:9,
            year:2015,
            onDayClick : function( $el, $contentEl, dateProperties ) {
                showFromCalendar(dateProperties.month,dateProperties.day,dateProperties.year);
            },
            caldata : codropsEvents,
            displayWeekAbbr : true
        } );
    $( '#custom-month9' ).html( cal9.getMonthName() ),
    $( '#custom-year9' ).html( cal9.getYear() );

    var cal10 = $("#calendar10").calendario( {
            month:10,
            year:2015,
            onDayClick : function( $el, $contentEl, dateProperties ) {
                showFromCalendar(dateProperties.month,dateProperties.day,dateProperties.year);
            },
            caldata : codropsEvents,
            displayWeekAbbr : true
        } );
    $( '#custom-month10' ).html( cal10.getMonthName() ),
    $( '#custom-year10' ).html( cal10.getYear() );

    var cal11 = $("#calendar11").calendario( {
            month:11,
            year:2015,
            onDayClick : function( $el, $contentEl, dateProperties ) {
                showFromCalendar(dateProperties.month,dateProperties.day,dateProperties.year);
            },
            caldata : codropsEvents,
            displayWeekAbbr : true
        } );
    $( '#custom-month11' ).html( cal11.getMonthName() );
    $( '#custom-year11' ).html( cal11.getYear() );

    var cal12 = $("#calendar12").calendario( {
            month:12,
            year:2015,
            onDayClick : function( $el, $contentEl, dateProperties ) {
                showFromCalendar(dateProperties.month,dateProperties.day,dateProperties.year);
            },
            caldata : null,
            displayWeekAbbr : true
        } );
    $( '#custom-month12' ).html( cal12.getMonthName());
    $( '#custom-year12' ).html( cal12.getYear() );

    refreshDataKalenderKerja();
});

</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a id="add_button_kalender_kerja" class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>

	<!--div class="box" style="display:block; height:500px"-->
    <div class="box">
			<div class="span12" id="kalenderKerja-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <%--
            <g:render template="dataTables" />
            --%>
            <div class="custom-header1 clearfix">
                <nav>
                    <span class="custom-prev" id="custom-prev"></span>
                    <span class="custom-next" id="custom-next"></span>
                </nav>
                <h3 class="custom-year" id="custom-year">2015</h3>
            </div>
            <table id="table_calendario" style="margin: 0 auto;">
                <tr>
                    <td>
                        <div class="custom-calendar-wrap">
                            <div id="custom-inner1" class="custom-inner">
                                <div class="custom-header clearfix">
                                    <h2 id="custom-month1" class="custom-month"></h2>
                                    <h3 id="custom-year1" class="custom-year"></h3>
                                </div>
                                <div id="calendar1" class="fc-calendar-container"></div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="custom-calendar-wrap">
                            <div id="custom-inner2" class="custom-inner">
                                <div class="custom-header clearfix">
                                    <h2 id="custom-month2" class="custom-month"></h2>
                                    <h3 id="custom-year2" class="custom-year"></h3>
                                </div>
                                <div id="calendar2" class="fc-calendar-container"></div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="custom-calendar-wrap">
                            <div id="custom-inner3" class="custom-inner">
                                <div class="custom-header clearfix">
                                    <h2 id="custom-month3" class="custom-month"></h2>
                                    <h3 id="custom-year3" class="custom-year"></h3>
                                </div>
                                <div id="calendar3" class="fc-calendar-container"></div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="custom-calendar-wrap">
                            <div id="custom-inner4" class="custom-inner">
                                <div class="custom-header clearfix">
                                    <h2 id="custom-month4" class="custom-month"></h2>
                                    <h3 id="custom-year4" class="custom-year"></h3>
                                </div>
                                <div id="calendar4" class="fc-calendar-container"></div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="custom-calendar-wrap">
                            <div id="custom-inner5" class="custom-inner">
                                <div class="custom-header clearfix">
                                    <h2 id="custom-month5" class="custom-month"></h2>
                                    <h3 id="custom-year5" class="custom-year"></h3>
                                </div>
                                <div id="calendar5" class="fc-calendar-container"></div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="custom-calendar-wrap">
                            <div id="custom-inner6" class="custom-inner">
                                <div class="custom-header clearfix">
                                    <h2 id="custom-month6" class="custom-month"></h2>
                                    <h3 id="custom-year6" class="custom-year"></h3>
                                </div>
                                <div id="calendar6" class="fc-calendar-container"></div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="custom-calendar-wrap">
                            <div id="custom-inner7" class="custom-inner">
                                <div class="custom-header clearfix">
                                    <h2 id="custom-month7" class="custom-month"></h2>
                                    <h3 id="custom-year7" class="custom-year"></h3>
                                </div>
                                <div id="calendar7" class="fc-calendar-container"></div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="custom-calendar-wrap">
                            <div id="custom-inner8" class="custom-inner">
                                <div class="custom-header clearfix">
                                    <h2 id="custom-month8" class="custom-month"></h2>
                                    <h3 id="custom-year8" class="custom-year"></h3>
                                </div>
                                <div id="calendar8" class="fc-calendar-container"></div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="custom-calendar-wrap">
                            <div id="custom-inner9" class="custom-inner">
                                <div class="custom-header clearfix">
                                    <h2 id="custom-month9" class="custom-month"></h2>
                                    <h3 id="custom-year9" class="custom-year"></h3>
                                </div>
                                <div id="calendar9" class="fc-calendar-container"></div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="custom-calendar-wrap">
                            <div id="custom-inner10" class="custom-inner">
                                <div class="custom-header clearfix">
                                    <h2 id="custom-month10" class="custom-month"></h2>
                                    <h3 id="custom-year10" class="custom-year"></h3>
                                </div>
                                <div id="calendar10" class="fc-calendar-container"></div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="custom-calendar-wrap">
                            <div id="custom-inner11" class="custom-inner">
                                <div class="custom-header clearfix">
                                    <h2 id="custom-month11" class="custom-month"></h2>
                                    <h3 id="custom-year11" class="custom-year"></h3>
                                </div>
                                <div id="calendar11" class="fc-calendar-container"></div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="custom-calendar-wrap">
                            <div id="custom-inner12" class="custom-inner">
                                <div class="custom-header clearfix">
                                    <h2 id="custom-month12" class="custom-month"></h2>
                                    <h3 id="custom-year12" class="custom-year"></h3>
                                </div>
                                <div id="calendar12" class="fc-calendar-container"></div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            </div>
		<div class="span7" id="kalenderKerja-form" style="display: none;"></div>
	</div>
    <!--
    <link rel="stylesheet" type="text/css" href="css/calendar.css" />
    <link rel="stylesheet" type="text/css" href="css/custom_2.css" />
    <script type="text/javascript" src="js/jquery.calendario.js"></script>
    -->
</body>
</html>
