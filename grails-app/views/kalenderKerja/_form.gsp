<%@ page import="com.kombos.administrasi.CompanyDealer; com.kombos.administrasi.KalenderKerja" %>

<g:javascript disposition="head">
    $(function(){
        var dataradio = "${kalenderKerjaInstance.m031StaLibur}";
        if(dataradio=="Y"){
            $(".jam_m031").attr('disabled','disabled');
        }else if(dataradio=="N"){
            $(".jam_m031").prop('disabled',false);
        }else{
             if( $('#m031StaLibur_1').val() == 'Y'){
                 $(".jam_m031").attr('disabled','disabled');
             }else{
                 $(".jam_m031").prop('disabled',false);
             }
        }

        $('input:radio[name="m031StaLibur"]').change(function(){
            if($(this).val() == 'Y'){
                $(".jam_m031").attr('disabled','disabled');
            } else{
                $(".jam_m031").removeAttr('disabled');
            }
        });
    });
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: kalenderKerjaInstance, field: 'm031ID', 'error')} required">
	<label class="control-label" for="m031ID">
		<g:message code="kalenderKerja.m031ID.label" default="Periode Tanggal" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        %{
            if(request.getRequestURI().contains("edit") || request.getRequestURI().contains("update")){
        }%
            %{--<ba:datePicker name="m031ID" id="m031ID" precision="day" format="dd/mm/yyyy" required="true" value="${kalenderKerjaInstance?.m031ID}" readonly="readonly"/>--}%
            <g:textField name="m031ID" value="${kalenderKerjaInstance?.m031ID.format("dd/MM/yyyy")}" disabled="disabled" />
        %{
            }else{
        }%
        <input type="hidden" name="m031ID" id="m031ID" value="${kalenderKerjaInstance?.m031ID}" />
        <ba:datePicker name="m031ID_start" id="m031ID_start" precision="day" format="dd/mm/yyyy" required="true"/> -
        <ba:datePicker name="m031ID_end" id="m031ID_end" precision="day" format="dd/mm/yyyy" required="true"/>
        %{
            }
        }%
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: kalenderKerjaInstance, field: 'companyDealer', 'error')} required">
	<label class="control-label" for="companyDealer">
		<g:message code="kalenderKerja.companyDealer.label" default="Company Dealer" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:select id="companyDealer" name="companyDealer.id" from="${CompanyDealer.list()}" optionKey="id" required="" value="${kalenderKerjaInstance?.companyDealer?.id}" class="many-to-one"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: kalenderKerjaInstance, field: 'm031StaLibur', 'error')} required">
	<label class="control-label" for="m031StaLibur">
		<g:message code="kalenderKerja.m031StaLibur.label" default="Sta Libur" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <input type="radio" name="m031StaLibur" id="m031StaLibur_1" value="Y"  ${kalenderKerjaInstance.m031StaLibur=="Y"?'checked':''}> Ya
        <input type="radio" name="m031StaLibur" id="m031StaLibur_2" value="N" ${kalenderKerjaInstance.m031StaLibur=="N"?'checked':''}> Tidak
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: kalenderKerjaInstance, field: 'm031JamMulai1', 'error')} required">
	<label class="control-label" for="m031JamMulai1">
		<g:message code="kalenderKerja.m031JamMulai1.label" default="Jam Mulai1" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <select id="m031JamMulai1_hour" name="m031JamMulai1_hour" style="width: 60px" required="" class="jam_m031">
            %{

                for (int i=0;i<24;i++){
                    if(i<10){
                        if(dataInstance.jamMulai1==i){
                            out.println('<option value="'+i+'" selected>0'+i+'</option>');
                        }else
                            out.println('<option value="'+i+'">0'+i+'</option>');
                    } else {
                        if(dataInstance.jamMulai1==i){
                            out.println('<option value="'+i+'" selected>'+i+'</option>');
                        }else
                            out.println('<option value="'+i+'">'+i+'</option>');
                    }

                }
            }%
        </select> H :
        <select id="m031JamMulai1_minute" name="m031JamMulai1_minute" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(dataInstance.menitMulai1.equals("0"+i.toString())){
                            out.println('<option value="0'+i+'" selected>0'+i+'</option>');
                        }else
                            out.println('<option value="0'+i+'">0'+i+'</option>');

                    } else {
                        if(dataInstance.menitMulai1.equals(i.toString())){
                            out.println('<option value="'+i+'" selected>'+i+'</option>');
                        }else
                            out.println('<option value="'+i+'">'+i+'</option>');
                    }
                }
            }%
        </select> m
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: kalenderKerjaInstance, field: 'm031JamMulai2', 'error')} required">
	<label class="control-label" for="m031JamMulai2">
		<g:message code="kalenderKerja.m031JamMulai2.label" default="Jam Mulai2" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	    <%--g:field name="m031JamMulai2" type="number" value="${kalenderKerjaInstance.m031JamMulai2}" required=""/--%>
        <select id="m031JamMulai2_hour" name="m031JamMulai2_hour" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(dataInstance.jamMulai2==i){
                            out.println('<option value="'+i+'" selected>0'+i+'</option>');
                        }else
                            out.println('<option value="'+i+'">0'+i+'</option>');
                    } else {
                        if(dataInstance.jamMulai2==i){
                            out.println('<option value="'+i+'" selected>'+i+'</option>');
                        }else
                            out.println('<option value="'+i+'">'+i+'</option>');
                    }

                }
            }%
        </select> H :
        <select id="m031JamMulai2_minute" name="m031JamMulai2_minute" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(dataInstance.menitMulai2.equals("0"+i.toString())){
                            out.println('<option value="0'+i+'" selected>0'+i+'</option>');
                        }else
                            out.println('<option value="0'+i+'">0'+i+'</option>');

                    } else {
                        if(dataInstance.menitMulai2.equals(i.toString())){
                            out.println('<option value="'+i+'" selected>'+i+'</option>');
                        }else
                            out.println('<option value="'+i+'">'+i+'</option>');
                    }
                }
            }%
        </select> m
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: kalenderKerjaInstance, field: 'm031JamMulai3', 'error')} required" >
	<label class="control-label" for="m031JamMulai3">
		<g:message code="kalenderKerja.m031JamMulai3.label" default="Jam Mulai3" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	    <%--g:field name="m031JamMulai3" type="number" value="${kalenderKerjaInstance.m031JamMulai3}" required=""/--%>
        <select id="m031JamMulai3_hour" name="m031JamMulai3_hour" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(dataInstance.jamMulai3==i){
                            out.println('<option value="'+i+'" selected>0'+i+'</option>');
                        }else
                            out.println('<option value="'+i+'">0'+i+'</option>');
                    } else {
                        if(dataInstance.jamMulai3==i){
                            out.println('<option value="'+i+'" selected>'+i+'</option>');
                        }else
                            out.println('<option value="'+i+'">'+i+'</option>');
                    }

                }
            }%
        </select> H :
        <select id="m031JamMulai3_minute" name="m031JamMulai3_minute" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(dataInstance.menitMulai3.equals("0"+i.toString())){
                            out.println('<option value="0'+i+'" selected>0'+i+'</option>');
                        }else
                            out.println('<option value="0'+i+'">0'+i+'</option>');

                    } else {
                        if(dataInstance.menitMulai3.equals(i.toString())){
                            out.println('<option value="'+i+'" selected>'+i+'</option>');
                        }else
                            out.println('<option value="'+i+'">'+i+'</option>');
                    }
                }
            }%
        </select> m
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: kalenderKerjaInstance, field: 'm031JamMulai4', 'error')} required">
	<label class="control-label" for="m031JamMulai4">
		<g:message code="kalenderKerja.m031JamMulai4.label" default="Jam Mulai4" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<%--g:field name="m031JamMulai4" type="number" value="${kalenderKerjaInstance.m031JamMulai4}" required=""/--%>
        <select id="m031JamMulai4_hour" name="m031JamMulai4_hour" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(dataInstance.jamMulai4==i){
                            out.println('<option value="'+i+'" selected>0'+i+'</option>');
                        }else
                            out.println('<option value="'+i+'">0'+i+'</option>');
                    } else {
                        if(dataInstance.jamMulai4==i){
                            out.println('<option value="'+i+'" selected>'+i+'</option>');
                        }else
                            out.println('<option value="'+i+'">'+i+'</option>');
                    }

                }
            }%
        </select> H :
        <select id="m031JamMulai4_minute" name="m031JamMulai4_minute" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(dataInstance.menitMulai4.equals("0"+i.toString())){
                            out.println('<option value="0'+i+'" selected>0'+i+'</option>');
                        }else
                            out.println('<option value="0'+i+'">0'+i+'</option>');

                    } else {
                        if(dataInstance.menitMulai4.equals(i.toString())){
                            out.println('<option value="'+i+'" selected>'+i+'</option>');
                        }else
                            out.println('<option value="'+i+'">'+i+'</option>');
                    }
                }
            }%
        </select> m
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: kalenderKerjaInstance, field: 'm031JamSelesai1', 'error')} required">
	<label class="control-label" for="m031JamSelesai1">
		<g:message code="kalenderKerja.m031JamSelesai1.label" default="Jam Selesai1" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	    <%--g:field name="m031JamSelesai1" type="number" value="${kalenderKerjaInstance.m031JamSelesai1}" required=""/--%>
        <select id="m031JamSelesai1_hour" name="m031JamSelesai1_hour" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(dataInstance.jamSelesai1==i){
                            out.println('<option value="'+i+'" selected>0'+i+'</option>');
                        }else
                            out.println('<option value="'+i+'">0'+i+'</option>');
                    } else {
                        if(dataInstance.jamSelesai1==i){
                            out.println('<option value="'+i+'" selected>'+i+'</option>');
                        }else
                            out.println('<option value="'+i+'">'+i+'</option>');
                    }

                }
            }%
        </select> H :
        <select id="m031JamSelesai1_minute" name="m031JamSelesai1_minute" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(dataInstance.menitSelesai1.equals("0"+i.toString())){
                            out.println('<option value="0'+i+'" selected>0'+i+'</option>');
                        }else
                            out.println('<option value="0'+i+'">0'+i+'</option>');

                    } else {
                        if(dataInstance.menitSelesai1.equals(i.toString())){
                            out.println('<option value="'+i+'" selected>'+i+'</option>');
                        }else
                            out.println('<option value="'+i+'">'+i+'</option>');
                    }
                }
            }%
        </select> m
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: kalenderKerjaInstance, field: 'm031JamSelesai2', 'error')} required">
	<label class="control-label" for="m031JamSelesai2">
		<g:message code="kalenderKerja.m031JamSelesai2.label" default="Jam Selesai2" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<%--g:field name="m031JamSelesai2" type="number" value="${kalenderKerjaInstance.m031JamSelesai2}" required=""/--%>
        <select id="m031JamSelesai2_hour" name="m031JamSelesai2_hour" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(dataInstance.jamSelesai2==i){
                            out.println('<option value="'+i+'" selected>0'+i+'</option>');
                        }else
                            out.println('<option value="'+i+'">0'+i+'</option>');
                    } else {
                        if(dataInstance.jamSelesai2==i){
                            out.println('<option value="'+i+'" selected>'+i+'</option>');
                        }else
                            out.println('<option value="'+i+'">'+i+'</option>');
                    }

                }
            }%
        </select> H :
        <select id="m031JamSelesai2_minute" name="m031JamSelesai2_minute" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(dataInstance.menitSelesai2.equals("0"+i.toString())){
                            out.println('<option value="0'+i+'" selected>0'+i+'</option>');
                        }else
                            out.println('<option value="0'+i+'">0'+i+'</option>');

                    } else {
                        if(dataInstance.menitSelesai2.equals(i.toString())){
                            out.println('<option value="'+i+'" selected>'+i+'</option>');
                        }else
                            out.println('<option value="'+i+'">'+i+'</option>');
                    }
                }
            }%
        </select> m
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: kalenderKerjaInstance, field: 'm031JamSelesai3', 'error')} required">
	<label class="control-label" for="m031JamSelesai3">
		<g:message code="kalenderKerja.m031JamSelesai3.label" default="Jam Selesai3" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<%--g:field name="m031JamSelesai3" type="number" value="${kalenderKerjaInstance.m031JamSelesai3}" required=""/--%>
        <select id="m031JamSelesai3_hour" name="m031JamSelesai3_hour" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(dataInstance.jamSelesai3==i){
                            out.println('<option value="'+i+'" selected>0'+i+'</option>');
                        }else
                            out.println('<option value="'+i+'">0'+i+'</option>');
                    } else {
                        if(dataInstance.jamSelesai3==i){
                            out.println('<option value="'+i+'" selected>'+i+'</option>');
                        }else
                            out.println('<option value="'+i+'">'+i+'</option>');
                    }

                }
            }%
        </select> H :
        <select id="m031JamSelesai3_minute" name="m031JamSelesai3_minute" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(dataInstance.menitSelesai3.equals("0"+i.toString())){
                            out.println('<option value="0'+i+'" selected>0'+i+'</option>');
                        }else
                            out.println('<option value="0'+i+'">0'+i+'</option>');

                    } else {
                        if(dataInstance.menitSelesai3.equals(i.toString())){
                            out.println('<option value="'+i+'" selected>'+i+'</option>');
                        }else
                            out.println('<option value="'+i+'">'+i+'</option>');
                    }
                }
            }%
        </select> m
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: kalenderKerjaInstance, field: 'm031JamSelesai4', 'error')} required">
	<label class="control-label" for="m031JamSelesai4">
		<g:message code="kalenderKerja.m031JamSelesai4.label" default="Jam Selesai4" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<%--g:field name="m031JamSelesai4" type="number" value="${kalenderKerjaInstance.m031JamSelesai4}" required=""/--%>
        <select id="m031JamSelesai4_hour" name="m031JamSelesai4_hour" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(dataInstance.jamSelesai4==i){
                            out.println('<option value="'+i+'" selected>0'+i+'</option>');
                        }else
                            out.println('<option value="'+i+'">0'+i+'</option>');
                    } else {
                        if(dataInstance.jamSelesai4==i){
                            out.println('<option value="'+i+'" selected>'+i+'</option>');
                        }else
                            out.println('<option value="'+i+'">'+i+'</option>');
                    }

                }
            }%
        </select> H :
        <select id="m031JamSelesai4_minute" name="m031JamSelesai4_minute" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(dataInstance.menitSelesai4.equals("0"+i.toString())){
                            out.println('<option value="0'+i+'" selected>0'+i+'</option>');
                        }else
                            out.println('<option value="0'+i+'">0'+i+'</option>');

                    } else {
                        if(dataInstance.menitSelesai4.equals(i.toString())){
                            out.println('<option value="'+i+'" selected>'+i+'</option>');
                        }else
                            out.println('<option value="'+i+'">'+i+'</option>');
                    }
                }
            }%
        </select> m
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: kalenderKerjaInstance, field: 'm031Ket', 'error')} required">
	<label class="control-label" for="m031Ket">
		<g:message code="kalenderKerja.m031Ket.label" default="Keterangan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textArea rows="5"  name="m031Ket" value="${kalenderKerjaInstance?.m031Ket}" maxlength="255"/>
	</div>
</div>
