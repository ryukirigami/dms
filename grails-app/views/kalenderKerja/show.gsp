

<%@ page import="com.kombos.administrasi.KalenderKerja" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'kalenderKerja.label', default: 'KalenderKerja')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteKalenderKerja;

$(function(){ 
	deleteKalenderKerja=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/kalenderKerja/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				//reloadKalenderKerjaTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-kalenderKerja" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="kalenderKerja"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${kalenderKerjaInstance?.m031ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m031ID-label" class="property-label"><g:message
					code="kalenderKerja.m031ID.label" default="Periode Tanggal" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m031ID-label">
						%{--<ba:editableValue
								bean="${kalenderKerjaInstance}" field="m031ID"
								url="${request.contextPath}/KalenderKerja/updatefield" type="text"
								title="Enter m031ID" onsuccess="reloadKalenderKerjaTable();" />--}%
							
								<g:formatDate date="${kalenderKerjaInstance?.m031ID}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kalenderKerjaInstance?.companyDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="companyDealer-label" class="property-label"><g:message
					code="kalenderKerja.companyDealer.label" default="Company Dealer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="companyDealer-label">
						%{--<ba:editableValue
								bean="${kalenderKerjaInstance}" field="companyDealer"
								url="${request.contextPath}/KalenderKerja/updatefield" type="text"
								title="Enter companyDealer" onsuccess="reloadKalenderKerjaTable();" />--}%
							
								<g:link controller="companyDealer" action="show" id="${kalenderKerjaInstance?.companyDealer?.id}">${kalenderKerjaInstance?.companyDealer?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kalenderKerjaInstance?.m031StaLibur}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m031StaLibur-label" class="property-label"><g:message
					code="kalenderKerja.m031StaLibur.label" default="Sta Libur" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m031StaLibur-label">
						%{--<ba:editableValue
								bean="${kalenderKerjaInstance}" field="m031StaLibur"
								url="${request.contextPath}/KalenderKerja/updatefield" type="text"
								title="Enter m031StaLibur" onsuccess="reloadKalenderKerjaTable();" />--}%
							
								<g:fieldValue bean="${kalenderKerjaInstance}" field="m031StaLibur"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kalenderKerjaInstance?.m031JamMulai1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m031JamMulai1-label" class="property-label"><g:message
					code="kalenderKerja.m031JamMulai1.label" default="Jam Mulai1" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m031JamMulai1-label">
						%{--<ba:editableValue
								bean="${kalenderKerjaInstance}" field="m031JamMulai1"
								url="${request.contextPath}/KalenderKerja/updatefield" type="text"
								title="Enter m031JamMulai1" onsuccess="reloadKalenderKerjaTable();" />--}%
							
								<g:fieldValue bean="${kalenderKerjaInstance}" field="m031JamMulai1"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kalenderKerjaInstance?.m031JamMulai2}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m031JamMulai2-label" class="property-label"><g:message
					code="kalenderKerja.m031JamMulai2.label" default="Jam Mulai2" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m031JamMulai2-label">
						%{--<ba:editableValue
								bean="${kalenderKerjaInstance}" field="m031JamMulai2"
								url="${request.contextPath}/KalenderKerja/updatefield" type="text"
								title="Enter m031JamMulai2" onsuccess="reloadKalenderKerjaTable();" />--}%
							
								<g:fieldValue bean="${kalenderKerjaInstance}" field="m031JamMulai2"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kalenderKerjaInstance?.m031JamMulai3}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m031JamMulai3-label" class="property-label"><g:message
					code="kalenderKerja.m031JamMulai3.label" default="Jam Mulai3" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m031JamMulai3-label">
						%{--<ba:editableValue
								bean="${kalenderKerjaInstance}" field="m031JamMulai3"
								url="${request.contextPath}/KalenderKerja/updatefield" type="text"
								title="Enter m031JamMulai3" onsuccess="reloadKalenderKerjaTable();" />--}%
							
								<g:fieldValue bean="${kalenderKerjaInstance}" field="m031JamMulai3"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kalenderKerjaInstance?.m031JamMulai4}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m031JamMulai4-label" class="property-label"><g:message
					code="kalenderKerja.m031JamMulai4.label" default="Jam Mulai4" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m031JamMulai4-label">
						%{--<ba:editableValue
								bean="${kalenderKerjaInstance}" field="m031JamMulai4"
								url="${request.contextPath}/KalenderKerja/updatefield" type="text"
								title="Enter m031JamMulai4" onsuccess="reloadKalenderKerjaTable();" />--}%
							
								<g:fieldValue bean="${kalenderKerjaInstance}" field="m031JamMulai4"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kalenderKerjaInstance?.m031JamSelesai1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m031JamSelesai1-label" class="property-label"><g:message
					code="kalenderKerja.m031JamSelesai1.label" default="Jam Selesai1" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m031JamSelesai1-label">
						%{--<ba:editableValue
								bean="${kalenderKerjaInstance}" field="m031JamSelesai1"
								url="${request.contextPath}/KalenderKerja/updatefield" type="text"
								title="Enter m031JamSelesai1" onsuccess="reloadKalenderKerjaTable();" />--}%
							
								<g:fieldValue bean="${kalenderKerjaInstance}" field="m031JamSelesai1"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kalenderKerjaInstance?.m031JamSelesai2}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m031JamSelesai2-label" class="property-label"><g:message
					code="kalenderKerja.m031JamSelesai2.label" default="Jam Selesai2" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m031JamSelesai2-label">
						%{--<ba:editableValue
								bean="${kalenderKerjaInstance}" field="m031JamSelesai2"
								url="${request.contextPath}/KalenderKerja/updatefield" type="text"
								title="Enter m031JamSelesai2" onsuccess="reloadKalenderKerjaTable();" />--}%
							
								<g:fieldValue bean="${kalenderKerjaInstance}" field="m031JamSelesai2"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kalenderKerjaInstance?.m031JamSelesai3}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m031JamSelesai3-label" class="property-label"><g:message
					code="kalenderKerja.m031JamSelesai3.label" default="Jam Selesai3" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m031JamSelesai3-label">
						%{--<ba:editableValue
								bean="${kalenderKerjaInstance}" field="m031JamSelesai3"
								url="${request.contextPath}/KalenderKerja/updatefield" type="text"
								title="Enter m031JamSelesai3" onsuccess="reloadKalenderKerjaTable();" />--}%
							
								<g:fieldValue bean="${kalenderKerjaInstance}" field="m031JamSelesai3"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kalenderKerjaInstance?.m031JamSelesai4}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m031JamSelesai4-label" class="property-label"><g:message
					code="kalenderKerja.m031JamSelesai4.label" default="Jam Selesai4" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m031JamSelesai4-label">
						%{--<ba:editableValue
								bean="${kalenderKerjaInstance}" field="m031JamSelesai4"
								url="${request.contextPath}/KalenderKerja/updatefield" type="text"
								title="Enter m031JamSelesai4" onsuccess="reloadKalenderKerjaTable();" />--}%
							
								<g:fieldValue bean="${kalenderKerjaInstance}" field="m031JamSelesai4"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kalenderKerjaInstance?.m031Ket}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m031Ket-label" class="property-label"><g:message
					code="kalenderKerja.m031Ket.label" default="Keterangan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m031Ket-label">
						%{--<ba:editableValue
								bean="${kalenderKerjaInstance}" field="m031Ket"
								url="${request.contextPath}/KalenderKerja/updatefield" type="text"
								title="Enter m031Ket" onsuccess="reloadKalenderKerjaTable();" />--}%
							
								<g:fieldValue bean="${kalenderKerjaInstance}" field="m031Ket"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kalenderKerjaInstance?.m031XNamaUser}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m031XNamaUser-label" class="property-label"><g:message
					code="kalenderKerja.m031XNamaUser.label" default="Nama User" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m031XNamaUser-label">
						%{--<ba:editableValue
								bean="${kalenderKerjaInstance}" field="m031XNamaUser"
								url="${request.contextPath}/KalenderKerja/updatefield" type="text"
								title="Enter m031XNamaUser" onsuccess="reloadKalenderKerjaTable();" />--}%
							
								<g:fieldValue bean="${kalenderKerjaInstance}" field="m031XNamaUser"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kalenderKerjaInstance?.m031XNamaDivisi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m031XNamaDivisi-label" class="property-label"><g:message
					code="kalenderKerja.m031XNamaDivisi.label" default="Nama Divisi User" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m031XNamaDivisi-label">
						%{--<ba:editableValue
								bean="${kalenderKerjaInstance}" field="m031XNamaDivisi"
								url="${request.contextPath}/KalenderKerja/updatefield" type="text"
								title="Enter m031XNamaDivisi" onsuccess="reloadKalenderKerjaTable();" />--}%
							
								<g:fieldValue bean="${kalenderKerjaInstance}" field="m031XNamaDivisi"/>
							
						</span></td>
					
				</tr>
				</g:if>

				<g:if test="${kalenderKerjaInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="kalenderKerja.dateCreated.label" default="Create Date" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${kalenderKerjaInstance}" field="createDtm"
								url="${request.contextPath}/KalenderKerja/updatefield" type="text"
								title="Enter createDtm" onsuccess="reloadKalenderKerjaTable();" />--}%
							
								<g:formatDate date="${kalenderKerjaInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kalenderKerjaInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="kalenderKerja.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${kalenderKerjaInstance}" field="createdBy"
								url="${request.contextPath}/KalenderKerja/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadKalenderKerjaTable();" />--}%
							
								<g:fieldValue bean="${kalenderKerjaInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kalenderKerjaInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdBy-label" class="property-label"><g:message
					code="kalenderKerja.updatedBy.label" default="Last Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${kalenderKerjaInstance}" field="lastUpdBy"
								url="${request.contextPath}/KalenderKerja/updatefield" type="text"
								title="Enter lastUpdBy" onsuccess="reloadKalenderKerjaTable();" />--}%
							
								<g:fieldValue bean="${kalenderKerjaInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kalenderKerjaInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="kalenderKerja.lastUpdated.label" default="Last Update Date" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${kalenderKerjaInstance}" field="lastUpdDtm"
								url="${request.contextPath}/KalenderKerja/updatefield" type="text"
								title="Enter lastUpdDtm" onsuccess="reloadKalenderKerjaTable();" />--}%
							
								<g:formatDate date="${kalenderKerjaInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kalenderKerjaInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="kalenderKerja.lastUpdProcess.label" default="Last Update Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${kalenderKerjaInstance}" field="lastUpdProcess"
								url="${request.contextPath}/KalenderKerja/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadKalenderKerjaTable();" />--}%
							
								<g:fieldValue bean="${kalenderKerjaInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${kalenderKerjaInstance?.id}"
					update="[success:'kalenderKerja-form',failure:'kalenderKerja-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteKalenderKerja('${kalenderKerjaInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
