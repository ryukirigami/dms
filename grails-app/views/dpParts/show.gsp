
<%@ page import="com.kombos.parts.DpParts" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'dpParts.label', default: 'DP Parts')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteDpParts;

$(function(){
	deleteDpParts=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/dpParts/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadDpPartsTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-dpParts" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="dpParts"
			class="table table-bordered table-hover">
			<tbody>

            <g:if test="${dpPartsInstance?.m162TglBerlaku}">
                <tr>
                <td class="span2" style="text-align: right;"><span
                    id="m162TglBerlaku-label" class="property-label"><g:message
                    code="dpPartsInstance.m162TglBerlaku.label" default="Tanggal Berlaku" />:</span></td>

                        <td class="span3"><span class="property-value"
                        aria-labelledby="m162TglBerlaku-label">
                                <g:formatDate date="${dpPartsInstance?.m162TglBerlaku}" />
                        </span></td>

                </tr>
            </g:if>

			<g:if test="${dpPartsInstance?.m162BatasNilaiKenaDP1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m162BatasNilaiKenaDP1-label" class="property-label"><g:message
					code="dpParts.m162BatasNilaiKenaDP1.label" default="Batas Awal Nilai Kena DP " />:</span></td>
						<td class="span3"><span class="property-value"
						aria-labelledby="m162BatasNilaiKenaDP1-label">
								<g:fieldValue bean="${dpPartsInstance}" field="m162BatasNilaiKenaDP1"/>
						</span></td>
				</tr>
		    </g:if>

            <g:if test="${dpPartsInstance?.m162BatasNilaiKenaDP2}">
                <tr>
                <td class="span2" style="text-align: right;"><span
                    id="m162BatasNilaiKenaDP2-label" class="property-label"><g:message
                    code="dpParts.m162BatasNilaiKenaDP2.label" default="Batas Akhir Nilai Kena DP " />:</span></td>
                        <td class="span3"><span class="property-value"
                        aria-labelledby="m162BatasNilaiKenaDP2-label">
                                <g:fieldValue bean="${dpPartsInstance}" field="m162BatasNilaiKenaDP2"/>
                        </span></td>
                </tr>
            </g:if>

            <g:if test="${dpPartsInstance?.m162PersenDP}">
                <tr>
                <td class="span2" style="text-align: right;"><span
                    id="m162PersenDP-label" class="property-label"><g:message
                    code="dpParts.m162PersenDP.label" default="Persentase DP" />:</span></td>
                        <td class="span3"><span class="property-value"
                        aria-labelledby="m162PersenDP-label">
                                <g:fieldValue bean="${dpPartsInstance}" field="m162PersenDP"/>
                        </span></td>
                </tr>
            </g:if>

            <g:if test="${dpPartsInstance?.m162MaxHariBayarDP}">
                <tr>
                <td class="span2" style="text-align: right;"><span
                    id="m162MaxHariBayarDP-label" class="property-label"><g:message
                    code="dpParts.m162MaxHariBayarDP.label" default="Maximum Waktu Pembayaran DP" />:</span></td>
                        <td class="span3"><span class="property-value"
                        aria-labelledby="m162MaxHariBayarDP-label">
                                <g:fieldValue bean="${dpPartsInstance}" field="m162MaxHariBayarDP"/>
                        </span></td>
                </tr>
            </g:if>

				%{--<g:if test="${dpPartsInstance?.staDel}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="staDel-label" class="property-label"><g:message--}%
					%{--code="dpParts.staDel.label" default="Sta Del" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="staDel-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${dpPartsInstance}" field="staDel"--}%
								%{--url="${request.contextPath}/DpParts/updatefield" type="text"--}%
								%{--title="Enter staDel" onsuccess="reloadDpPartsTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${dpPartsInstance}" field="staDel"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%

				<g:if test="${dpPartsInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="dpParts.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">

								<g:fieldValue bean="${dpPartsInstance}" field="lastUpdProcess"/>

						</span></td>

				</tr>
				</g:if>

				<g:if test="${dpPartsInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="dpParts.dateCreated.label" default="Date Created" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">

								<g:formatDate date="${dpPartsInstance?.dateCreated}" type="datetime" style="MEDIUM" />

						</span></td>

				</tr>
				</g:if>

            <g:if test="${dpPartsInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="dpParts.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">

                        <g:fieldValue bean="${dpPartsInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

				<g:if test="${dpPartsInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="dpParts.lastUpdated.label" default="Last Updated" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">

								<g:formatDate date="${dpPartsInstance?.lastUpdated}" type="datetime" style="MEDIUM" />

						</span></td>

				</tr>
				</g:if>

            <g:if test="${dpPartsInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="dpParts.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">

                        <g:fieldValue bean="${dpPartsInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${dpPartsInstance?.id}"
					update="[success:'dpParts-form',failure:'dpParts-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Anda yakin data akan dihapus?')}"
					onsuccess="deleteDpParts('${dpPartsInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
