
<%@ page import="com.kombos.parts.DpParts" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>
<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

</g:javascript>

<table id="dpParts_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="dpParts.m162TglBerlaku.label" default="Tanggal Berlaku" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="dpParts.m162BatasNilaiKenaDP1.label" default="Batas Nilai Awal Dikenakan DP" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="dpParts.m162BatasNilaiKenaDP2.label" default="Batas Nilai Akhir Dikenakan DP" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="dpParts.m162PersenDP.label" default="Persen DP" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="dpParts.m162MaxHariBayarDP.label" default="Maximum Hari Pembayaran DP" /></div>
            </th>

		</tr>
		<tr>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m162TglBerlaku" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="hidden" name="search_m162TglBerlaku" value="date.struct">
                    <input type="hidden" name="search_m162TglBerlaku_day" id="search_m162TglBerlaku_day" value="">
                    <input type="hidden" name="search_m162TglBerlaku_month" id="search_m162TglBerlaku_month" value="">
                    <input type="hidden" name="search_m162TglBerlaku_year" id="search_m162TglBerlaku_year" value="">
                    <input type="text" data-date-format="dd/mm/yyyy" name="search_m162TglBerlaku_dp" value="" id="search_m162TglBerlaku" class="search_init">
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_m162BatasNilaiKenaDP1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_m162BatasNilaiKenaDP1" class="search_init" onkeypress="return isNumberKey(event)" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_m162BatasNilaiKenaDP2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_m162BatasNilaiKenaDP2" class="search_init" onkeypress="return isNumberKey(event)" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_m162PersenDP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_m162PersenDP" class="search_init"  onkeypress="return isDecimalKey(event)" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_m162MaxHariBayarDP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_m162MaxHariBayarDP" class="search_init" onkeypress="return isNumberKey(event)" />
                </div>
            </th>

		</tr>
	</thead>
</table>

<g:javascript>
var dpPartsTable;
var reloadDpPartsTable;
$(function(){

	reloadDpPartsTable = function() {
		dpPartsTable.fnDraw();
	}
                             

	$('#search_m162TglBerlaku').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m162TglBerlaku_day').val(newDate.getDate());
			$('#search_m162TglBerlaku_month').val(newDate.getMonth()+1);
			$('#search_m162TglBerlaku_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			dpPartsTable.fnDraw();
	});
	        
	var recordsDpPartsPerPage = [];
    var anDpPartsSelected;
    var jmlRecDpPartsPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	dpPartsTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	dpPartsTable = $('#dpParts_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsDpParts = $("#dpParts_datatables tbody .row-select");
            var jmlDpPartsCek = 0;
            var nRow;
            var idRec;
            rsDpParts.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsDpPartsPerPage[idRec]=="1"){
                    jmlDpPartsCek = jmlDpPartsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsDpPartsPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecDpPartsPerPage = rsDpParts.length;
            if(jmlDpPartsCek==jmlRecDpPartsPerPage && jmlRecDpPartsPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m162TglBerlaku",
	"mDataProp": "m162TglBerlaku",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
			return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
		},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}

,

{
	"sName": "m162BatasNilaiKenaDP1",
	"mDataProp": "m162BatasNilaiKenaDP1",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true,
	"mRender": function ( data, type, row ) {
        if(data != null)
        return '<span class="pull-right numeric">'+data+'</span>';
        else
        return '<span></span>';
    }
}

,

{
	"sName": "m162BatasNilaiKenaDP2",
	"mDataProp": "m162BatasNilaiKenaDP2",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px","mRender": function ( data, type, row ) {
        if(data != null)
        return '<span class="pull-right numeric">'+data+'</span>';
        else
        return '<span></span>';
    },
	"bVisible": true
}

,

{
	"sName": "m162PersenDP",
	"mDataProp": "m162PersenDP",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}

,

{
	"sName": "m162MaxHariBayarDP",
	"mDataProp": "m162MaxHariBayarDP",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var m162TglBerlaku = $('#search_m162TglBerlaku').val();
						var m162TglBerlakuDay = $('#search_m162TglBerlaku_day').val();
						var m162TglBerlakuMonth = $('#search_m162TglBerlaku_month').val();
						var m162TglBerlakuYear = $('#search_m162TglBerlaku_year').val();
						
						if(m162TglBerlaku){
							aoData.push(
									{"name": 'sCriteria_m162TglBerlaku', "value": "date.struct"},
									{"name": 'sCriteria_m162TglBerlaku_dp', "value": m162TglBerlaku},
									{"name": 'sCriteria_m162TglBerlaku_day', "value": m162TglBerlakuDay},
									{"name": 'sCriteria_m162TglBerlaku_month', "value": m162TglBerlakuMonth},
									{"name": 'sCriteria_m162TglBerlaku_year', "value": m162TglBerlakuYear}
							);
						}

						var m162BatasNilaiKenaDP1 = $('#filter_m162BatasNilaiKenaDP1 input').val();
						if(m162BatasNilaiKenaDP1){
							aoData.push(
									{"name": 'sCriteria_m162BatasNilaiKenaDP1', "value": m162BatasNilaiKenaDP1}
							);
						}

						var m162BatasNilaiKenaDP2 = $('#filter_m162BatasNilaiKenaDP2 input').val();
						if(m162BatasNilaiKenaDP2){
							aoData.push(
									{"name": 'sCriteria_m162BatasNilaiKenaDP2', "value": m162BatasNilaiKenaDP2}
							);
						}

						var m162PersenDP = $('#filter_m162PersenDP input').val();
						if(m162PersenDP){
							aoData.push(
									{"name": 'sCriteria_m162PersenDP', "value": m162PersenDP}
							);
						}

						var m162MaxHariBayarDP = $('#filter_m162MaxHariBayarDP input').val();
						if(m162MaxHariBayarDP){
							aoData.push(
									{"name": 'sCriteria_m162MaxHariBayarDP', "value": m162MaxHariBayarDP}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							        $('td span.numeric').text(function(index, text) {
                                    return $.number(text,0);
                                    });
							   }
						});
		}
	});

	$('.select-all').click(function(e) {
        $("#dpParts_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsDpPartsPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsDpPartsPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });
	
	$('#dpParts_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsDpPartsPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anDpPartsSelected = dpPartsTable.$('tr.row_selected');
            console.log("anDpPartsSelected.length=" + anDpPartsSelected.length);
            if(jmlRecDpPartsPerPage == anDpPartsSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsDpPartsPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>



