<%@ page import="com.kombos.parts.DpParts" %>

<div class="control-group fieldcontain ${hasErrors(bean: dpPartsInstance, field: 'm162TglBerlaku', 'error')} required">
	<label class="control-label" for="m162TglBerlaku">
		<g:message code="dpParts.m162TglBerlaku.label" default="Tanggal Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <ba:datePicker name="m162TglBerlaku" precision="day" value="${dpPartsInstance?.m162TglBerlaku}" format="dd/MM/yyyy" required="true"/>
	</div>
</div>
<g:javascript>
    document.getElementById("m162TglBerlaku").focus();
    jQuery(function($) {
        $('.persen').autoNumeric('init',{
            vMin:'0',
            vMax:'100',
            aSep:'',
            mDec: '2'
        });$('.auto').autoNumeric('init',{
            mDec:''
        });
    });
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: dpPartsInstance, field: 'm162BatasNilaiKenaDP1', 'error')} required">
	<label class="control-label" for="m162BatasNilaiKenaDP1">
		<g:message code="dpParts.m162BatasNilaiKenaDP1.label" default="Batas Nilai Kena DP" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	    <g:textField class="auto" name="m162BatasNilaiKenaDP1" id="m162BatasNilaiKenaDP1" maxlength="15" required="" value="${dpPartsInstance?.m162BatasNilaiKenaDP1}" onkeypress="return isNumberKey(event)"/>
        <g:message code="dpParts.m162BatasNilaiKenaDP2.label" default="s/d" />
        <g:textField class="auto" name="m162BatasNilaiKenaDP2" id="m162BatasNilaiKenaDP2" maxlength="15" required="" value="${dpPartsInstance?.m162BatasNilaiKenaDP2}" onkeypress="return isNumberKey(event)"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: dpPartsInstance, field: 'm162PersenDP', 'error')} required">
	<label class="control-label" for="m162PersenDP">
		<g:message code="dpParts.m162PersenDP.label" default="Persentase DP" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	    <g:textField name="m162PersenDP" maxlength="3" required="" value="${dpPartsInstance?.m162PersenDP}" class="persen" onkeypress="return isDecimalKey(event)"/>
        <g:message code="dpParts.m162PersenDP.label.desc" default="%" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: dpPartsInstance, field: 'm162MaxHariBayarDP', 'error')} required">
	<label class="control-label" for="m162MaxHariBayarDP">
		<g:message code="dpParts.m162MaxHariBayarDP.label" default="Maximum Waktu Pembayaran DP" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	    <g:textField name="m162MaxHariBayarDP" maxlength="3" required="" value="${dpPartsInstance?.m162MaxHariBayarDP}" onkeypress="return isNumberKey(event)"/>
        <g:message code="dpParts.m162MaxHariBayarDP.label.desc" default="Hari Setelah Tanggal Appointment" />
	</div>
</div>

