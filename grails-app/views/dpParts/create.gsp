
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.kombos.parts.DpParts" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'dpParts.label', default: 'DP Part')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>

	<body>
		<div id="create-dpParts" class="content scaffold-create" role="main">
			<legend><g:message code="default.create.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${dpPartsInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${dpPartsInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form action="save" >--}%
			<g:formRemote class="form-horizontal" name="create" on404="alert('not found!');" onSuccess="reloadDpPartsTable();" update="dpParts-form"
              url="[controller: 'dpParts', action:'save']" >
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons controls">
					<a class="btn cancel" href="javascript:void(0);"
                        onclick="expandTableLayout();"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
                    %{--<a class="btn btn-primary create" href="javascript:void(0);"
                        onclick="createConfirm();"><g:message
                            code="default.button.create.label" default="Create" /></a>--}%
					<g:actionSubmit class="btn btn-primary create" name="save" value="${message(code: 'default.button.create.label', default: 'Create')}"
                        onclick="return confirm('${message(code: 'default.button.save.confirm.message', default: 'Anda yakin data akan disimpan?')}');"/>
				</fieldset>
			</g:formRemote>

			%{--</g:form>--}%
		</div>
	</body>
</html>
