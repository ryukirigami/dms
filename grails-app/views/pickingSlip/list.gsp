
<%@ page import="com.kombos.parts.PickingSlipDetail" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="title" value="View Picking Slip" />
    <title>${title}</title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){

	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
                                   onLoading="jQuery('#spinner').fadeIn(1);"
                                   onSuccess="loadForm(data, textStatus);"
                                   onComplete="jQuery('#spinner').fadeOut();" />
        break;
    case '_DELETE_' :
        bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});

         		break;
       }
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/pickingSlip/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };

    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);

   		%{--$.ajax({type:'POST', url:'${request.contextPath}/pickingSlip/edit/1',--}%
   		$.ajax({type:'POST', url:'${request.contextPath}/pickingSlip/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };

    loadForm = function(data, textStatus){
		$('#pickingSlip-form').empty();
    	$('#pickingSlip-form').append(data);
   	}

    shrinkTableLayout = function(){
    	$("#pickingSlip-table").hide()
        $("#pickingSlip-form").css("display","block");
   	}

   	expandTableLayout = function(){
        $("#pickingSlip-table").show()
        $("#pickingSlip-form").css("display","none");
   	}

   	massDelete = function() {
   		var recordsToDelete = [];
		$("#pickingSlip-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});

		var json = JSON.stringify(recordsToDelete);

		$.ajax({
    		url:'${request.contextPath}/pickingSlip/massdelete',
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		success:function(data,textStatus){
   				if(data=="not"){
   				    alert("GAGAL, NOMOR SO TELAH DIBUAT SI")
   				}else{
   				    toastr.success("SUKSES, HARAP LIHAT STOCK");
   				}
   			},
    		complete: function(xhr, status) {
        		reloadPickingSlipTable();
    		}
		});

   	}

});
        var checkin = $('#search_Tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
        }).on('changeDate', function(ev) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
            checkin.hide();
            $('#search_Tanggal2')[0].focus();
        }).data('datepicker');

        var checkout = $('#search_Tanggal2').datepicker({
            onRender: function(date) {
                return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');

        printPickingSlip = function(){
           checkPickingSlip =[];
           var noWO,noPickingSlip,tglPickingSlip,noPolisi,SA,cv;
            $("#pickingSlip-table tbody .row-select").each(function() {
                  if(this.checked){
                     var nRow = $(this).next("input:hidden").val();
					    checkPickingSlip.push(nRow);
                  }
            });
            if(checkPickingSlip.length<1 ){
                alert('Silahkan Pilih Salah satu Vendor Untuk Dicetak');
                return;

            }
           var idPickingSlip =  JSON.stringify(checkPickingSlip);
           window.location = "${request.contextPath}/pickingSlip/printPickingSlip?idPickingSlip="+idPickingSlip;

    }
    klik = function(){

        $('#spinner').fadeIn(1);
        $.ajax({
            url: '${request.contextPath}/pickingSlipInput',
            type: "GET",dataType:"html",
            complete : function (req, err) {
                $('#main-content').html(req.responseText);
                $('#spinner').fadeOut();
            }
        });
    }
     var checkin = $('#search_Tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
        }).on('changeDate', function(ev) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
            checkin.hide();
            $('#search_Tanggal2')[0].focus();
        }).data('datepicker');

        var checkout = $('#search_Tanggal2').datepicker({
            onRender: function(date) {
                return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');


    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left">${title}</span>
    <ul class="nav pull-right">
        <li><a class="pull-right" href="#/pickingSlipInput" onclick="klik()" style="display: block;" >&nbsp;&nbsp;<i class="icon-plus"></i>&nbsp;&nbsp;
        </a></li>
        <li><a class="pull-right box-action" href="#"
               style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
                    class="icon-remove"></i>&nbsp;&nbsp;
        </a></li>
        <li class="separator"></li>
    </ul>
</div>
<div class="box">
    <div class="span12" id="pickingSlip-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <fieldset>
            <table style="padding-right: 10px">
                <tr>
                    <td style="width: 130px">
                        <label class="control-label" for="t951Tanggal">
                            <g:message code="auditTrail.wo.label" default="Nomor WO" />&nbsp;
                        </label>&nbsp;&nbsp;
                    </td>
                    <td>
                        <div id="filter_wo" class="controls">
                            <g:textField name="noWo" id="noWo" maxlength="20" />
                        </div>
                    </td>
                </tr><tr>
                    <td style="width: 130px">
                        <label class="control-label" for="t951Tanggal">
                            <g:message code="auditTrail.tanggal.label" default="Tanggal Picking Slip" />&nbsp;
                        </label>&nbsp;&nbsp;
                    </td>
                    <td>
                        <div id="filter_m777Tgl" class="controls">
                            <ba:datePicker id="search_Tanggal" name="search_Tanggal" precision="day" format="dd/MM/yyyy"  value="" />
                            &nbsp;&nbsp;&nbsp;s.d.&nbsp;&nbsp;&nbsp;
                            <ba:datePicker id="search_Tanggal2" name="search_Tanggal2" precision="day" format="dd/MM/yyyy"  value=""  />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" >
                        <div class="controls" style="right: 0">
                            <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-primary view" name="view" id="view" >View</button>
                            <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-cancel clear" name="clear" id="clear" >Clear</button>
                        </div>
                    </td>
                </tr>
            </table>

        </fieldset>
        <g:render template="dataTables" />
        <fieldset class="buttons controls" style="padding-top: 10px;">
            <button id='printPickingSlipData' onclick="printPickingSlip();" type="button" class="btn btn-primary">Print Picking Slip</button>
        </fieldset>

    </div>
    <div class="span7" id="pickingSlip-form" style="display: none;"></div>
</div>
</body>
</html>
