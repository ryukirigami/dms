<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript>
var pickingSlipSubTable;
$(function(){ 
var anOpen = [];
	$('#pickingSlip_datatables_sub_${idTable} td.subcontrol').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
  		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = pickingSlipSubTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST', 
	   			data : oData,
	   			url:'${request.contextPath}/pickingSlip/subsublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = pickingSlipSubTable.fnOpen(nTr,data,'details');
    				$('div.innerinnerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
    		
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerinnerDetails', $(nTr).next()[0]).slideUp( function () {
      			pickingSlipSubTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

    $('#pickingSlip_datatables_sub_${idTable} a.edit').live('click', function (e) {
        e.preventDefault();
         
        var nRow = $(this).parents('tr')[0];
         
        if ( nEditing !== null && nEditing != nRow ) {
            restoreRow( pickingSlipSubTable, nEditing );
            editRow( pickingSlipSubTable, nRow );
            nEditing = nRow;
        }
        else if ( nEditing == nRow && this.innerHTML == "Save" ) {
            saveRow( pickingSlipSubTable, nEditing );
            nEditing = null;
        }
        else {
            editRow( pickingSlipSubTable, nRow );
            nEditing = nRow;
        }
    } );
    if(pickingSlipSubTable)
    	pickingSlipSubTable.dataTable().fnDestroy();
pickingSlipSubTable = $('#pickingSlip_datatables_sub_${idTable}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": ''
},
{
	"sName": "goods",
	"mDataProp": "goods",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"> <input type="hidden" value="'+row['id']+'">&nbsp;&nbsp; ' + data ;
	},
	"bSortable": false,
	"sWidth":"198px",
	"bVisible": true
},
{
	"sName": "goods",
	"mDataProp": "goods2",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"116px",
	"bVisible": true
},
{
	"sName": "t142Qty1",
	"mDataProp": "t142Qty1",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"118px",
	"bVisible": true
},
{
	"sName": "goods",
	"mDataProp": "satuan",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"115px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						aoData.push(
									{"name": 'goods', "value": "${goods}"},
									{"name": 't141ID', "value": "${t141ID}"},
									{"name": 'tanggal', "value": "${tanggal}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
    </g:javascript>
</head>
<body>
<div class="innerDetails">
    <table id="pickingSlip_datatables_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
           class="display table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th></th>
            <th></th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div>Kode Parts</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div>Nama Parts</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div>Qty</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div>Satuan</div>
            </th>

        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
</body>
</html>
