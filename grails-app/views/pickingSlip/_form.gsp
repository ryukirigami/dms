<%@ page import="com.kombos.parts.PickingSlipDetail" %>

<r:require modules="baseapplayout" />

<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</g:javascript>
<div class="control-group">
    <label class="control-label" for="Nomor" style="text-align: left;">Nomor WO</label>
    <div id="nomor" class="controls">

        <g:textField name="nomorWO" id="nomorWO" value="${pickingSlipInstance?.pickingSlip?.reception?.t401NoWO}" maxlength="25" disabled="disabled"  />
    </div><br>
    <label class="control-label" for="Nomor" style="text-align: left;">Tanggal </label>
    <div id="tanggal1" class="controls">
        <g:textField name="tanggal" id="tanggal" value="${pickingSlipInstance?.pickingSlip?.t141TglPicking.format("dd/MM/yyyy")}" maxlength="20" disabled="disabled"  />
    </div><br>
    <label class="control-label" for="Nomor" style="text-align: left;">Nama Teknisi</label>
    <div id="vendor2" class="controls">
        <g:textField name="namaTeknisi" id="namaTeknisi" value="${pickingSlipInstance?.pickingSlip?.t141NamaTeknisi}" maxlength="50" disabled="disabled"  />
    </div>
</div><br>
<table id="form_${idTable}" cellpadding="0" cellspacing="0" border="0"
       class="display table table-striped table-bordered table-hover" style="table-layout: fixed; width: 100%; ">
    <thead>
    <tr style="height: 30px;">

        <th style="border-bottom: none; padding: 5px; width: 150px;" >
            <div>Kode Parts</div>
        </th>
        <th style="border-bottom: none; padding: 5px; width: 150px;"  >
            <div>Nama Parts</div>
        </th>
        <th style="border-bottom: none; padding: 5px; width: 150px;" >
            <div>Qty</div>
        </th>
        <th style="border-bottom: none; padding: 5px; width: 150px;"  >
            <div>Satuan</div>
        </th>
    </tr>

    </thead>
    <tr>

        <td style="border-bottom: none; padding: 5px;">
            <div><g:fieldValue bean="${pickingSlipInstance?.goods}" field="m111ID"/></div>
        </td>
        <td style="border-bottom: none; padding: 5px;">
            <div><g:fieldValue bean="${pickingSlipInstance?.goods}" field="m111Nama"/></div>
        </td>

        <td style="border-bottom: none; padding: 5px;">
            <div><g:textField  name="t142Qty1" value="${pickingSlipInstance.t142Qty1}" onkeypress="return isNumberKey(event)" style="width:70px" /></div>
        </td>
        <td style="border-bottom: none; padding: 5px;">
            <div><g:fieldValue bean="${pickingSlipInstance?.goods?.satuan}" field="m118Satuan1"/></div>
        </td>
    </tr>
</table>
