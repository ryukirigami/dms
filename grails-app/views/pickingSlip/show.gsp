
<%@ page import="com.kombos.parts.PickingSlipDetail" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'pickingSlip.label', default: 'Picking Slip')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deletePickingSlip;

$(function(){ 
	deletePickingSlip=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/pickingSlip/delete/',
			data: { id: id },
   			success:function(data,textStatus){
  				reloadPickingSlipTable();
   				expandTableLayout('pickingSlip');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-pickingSlip" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="pickingSlip"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${pickingSlipInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="pickingSlip.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${pickingSlipInstance}" field="createdBy"
								url="${request.contextPath}/PickingSlip/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadPickingSlipTable();" />--}%
							
								<g:fieldValue bean="${pickingSlipInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${pickingSlipInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="pickingSlip.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${pickingSlipInstance}" field="dateCreated"
								url="${request.contextPath}/PickingSlip/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadPickingSlipTable();" />--}%
							
								<g:formatDate date="${pickingSlipInstance?.dateCreated}" format="EEEE, dd MMMM yyyy HH:mm"/>
							
						</span></td>
					
				</tr>
				</g:if>
                <g:if test="${pickingSlipInstance?.pickingSlip}">
                    <tr>
                        <td class="span2" style="text-align: right;"><span id="goods3-label" class="property-label"><g:message code="pickingSlip.updatedBy.label" default="Nomor Picking" />:</span></td>
                        <td class="span3"><span class="property-value" aria-labelledby="updatedBy-label">
                            <g:fieldValue bean="${pickingSlipInstance?.pickingSlip}" field="t141ID"/>
                        </span></td>
                    </tr>
                </g:if>
                <g:if test="${pickingSlipInstance?.pickingSlip}">
                    <tr>
                        <td class="span2" style="text-align: right;"><span id="goods4-label" class="property-label"><g:message code="pickingSlip.updatedBy.label" default="Tgl Picking" />:</span></td>
                        <td class="span3"><span class="property-value" aria-labelledby="updatedBy-label">
                            %{--<g:fieldValue bean="${pickingSlipInstance?.pickingSlip}" field="t141TglPicking"/>--}%
                            <g:formatDate date="${pickingSlipInstance?.pickingSlip?.t141TglPicking}" format="dd MMMM yyyy"/>
                        </span></td>
                    </tr>
                </g:if>
                <g:if test="${pickingSlipInstance?.pickingSlip}">
                    <tr>
                        <td class="span2" style="text-align: right;"><span id="goods5-label" class="property-label"><g:message code="pickingSlip.updatedBy.label" default="Nomor WO" />:</span></td>
                        <td class="span3"><span class="property-value" aria-labelledby="updatedBy-label">
                            <g:fieldValue bean="${pickingSlipInstance?.pickingSlip?.reception}" field="t401NoWO"/>
                        </span></td>
                    </tr>
                </g:if>
                <g:if test="${pickingSlipInstance?.goods}">
                    <tr>
                        <td class="span2" style="text-align: right;"><span id="goods2-label" class="property-label"><g:message code="pickingSlip.updatedBy.label" default="Kode Parts" />:</span></td>
                        <td class="span3"><span class="property-value" aria-labelledby="updatedBy-label">
                            <g:fieldValue bean="${pickingSlipInstance?.goods}" field="m111ID"/>
                        </span></td>
                    </tr>
                </g:if>
				<g:if test="${pickingSlipInstance?.goods}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="goods-label" class="property-label"><g:message
					code="pickingSlip.goods.label" default="Nama Parts" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="goods-label">
						%{--<ba:editableValue
								bean="${pickingSlipInstance}" field="goods"
								url="${request.contextPath}/PickingSlip/updatefield" type="text"
								title="Enter goods" onsuccess="reloadPickingSlipTable();" />--}%
                        <g:fieldValue bean="${pickingSlipInstance?.goods}" field="m111Nama"/>
							
						</span></td>
					
				</tr>
				</g:if>
            <g:if test="${pickingSlipInstance?.t142Qty1}">
                <tr>
                    <td class="span2" style="text-align: right;"><span id="t142Qty1-label" class="property-label"><g:message code="pickingSlip.updatedBy.label" default="Qty Perubahan" />:</span></td>
                    <td class="span3"><span class="property-value" aria-labelledby="updatedBy-label">
                        <g:fieldValue bean="${pickingSlipInstance}" field="t142Qty1"/>
                    </span></td>
                </tr>
            </g:if>

				<g:if test="${pickingSlipInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="pickingSlip.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${pickingSlipInstance}" field="lastUpdProcess"
								url="${request.contextPath}/PickingSlip/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadPickingSlipTable();" />--}%
							
								<g:fieldValue bean="${pickingSlipInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${pickingSlipInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="pickingSlip.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${pickingSlipInstance}" field="lastUpdated"
								url="${request.contextPath}/PickingSlip/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadPickingSlipTable();" />--}%
							
								<g:formatDate date="${pickingSlipInstance?.lastUpdated}" format="EEEE, dd MMMM yyyy HH:mm"/>
							
						</span></td>
					
				</tr>
				</g:if>

			
				<g:if test="${pickingSlipInstance?.updatedBy}">
		    		<tr>
			        	<td class="span2" style="text-align: right;"><span id="updatedBy-label" class="property-label"><g:message code="pickingSlip.updatedBy.label" default="Updated By" />:</span></td>
						<td class="span3"><span class="property-value" aria-labelledby="updatedBy-label">
								<g:fieldValue bean="${pickingSlipInstance}" field="updatedBy"/>
						</span></td>
			    	</tr>
				</g:if>

			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn btn-primary" href="javascript:void(0);"
					onclick="expandTableLayout('pickingSlip');">Back To List </a>

			</fieldset>
		</g:form>
	</div>
</body>
</html>
