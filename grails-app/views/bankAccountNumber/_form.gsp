<%@ page import="com.kombos.finance.BankAccountNumber" %>

<g:javascript>
$(function(){
			$('#nama').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/bankAccountNumber/listAccount', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});

    function detailAccount(){
                var nama = $('#nama').val();
                $.ajax({
                    url:'${request.contextPath}/bankAccountNumber/detailAccount?noAkun='+nama,
                    type: "POST", // Always use POST when deleting data
                    success : function(data){
                        $('#idAkun').val("");
                        if(data.hasil=="ada"){
                            $('#idAkun').val(data.id);
                            console.log("idAkun : " + data.id)
                        }
                    }
                })
        }
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: bankAccountNumberInstance, field: 'accountNumber', 'error')} required">
	<label class="control-label" for="accountNumber">
		<g:message code="bankAccountNumber.accountNumber.label" default="Account Number" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:select id="accountNumber" name="accountNumber.id" from="${com.kombos.finance.AccountNumber.list()}" optionKey="id" required="" value="${bankAccountNumberInstance?.accountNumber?.id}" class="many-to-one"/>--}%
        <input type='text' id='nama' style='width: 200px;' class="typeahead" onblur="detailAccount();" value="${bankAccountNumberInstance?.accountNumber?.accountNumber?bankAccountNumberInstance?.accountNumber?.accountNumber?.trim()+' || '+bankAccountNumberInstance?.accountNumber?.accountName:""}">
        <input type='hidden' id='idAkun' name="idAkun" value="${bankAccountNumberInstance?.accountNumber?.id}">
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: bankAccountNumberInstance, field: 'bank', 'error')} required">
	<label class="control-label" for="bank">
		<g:message code="bankAccountNumber.bank.label" default="Bank" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="bank" name="bank.id" from="${com.kombos.administrasi.Bank.list()}" optionKey="id" required="" value="${bankAccountNumberInstance?.bank?.id}" class="many-to-one"/>
	</div>
</div>
