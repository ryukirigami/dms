<%@ page import="com.kombos.maintable.Company"%>

<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode != 44  && charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    $(function(){
        $("#namaJabatanPIC").hide();
        $("#saveNamaJabatan").hide();
        $("#cancelNamaJabatan").hide();

        createNamaJabatanPIC = function(){
            $("#jabatanPIC").hide();
            $("#createNamaJabatan").hide();
            $("#namaJabatanPIC").show();
            $("#saveNamaJabatan").show();
            $("#cancelNamaJabatan").show();
        };

        saveNamaJabatanPIC = function(){
            var namaJabatanPIC = $('#namaJabatanPIC').val();
            if(namaJabatanPIC == ""){
                alert("Nama Jabatan PIC Tidak boleh kosong");
                return;
            }

            $.ajax({type:'POST', url:'${request.contextPath}/company/insertNamaJabatanPIC',
                data : {namaJabatanPIC : namaJabatanPIC},
                success:function(data,textStatus){
                    if(data){

                        if(data.error === "duplicate"){
                            alert("Data Duplicate, Silakan input yang belum ada");
                            $('#namaJabatanPIC').val("");
                            $("#namaJabatanPIC").focus();
                        }else{
                            $('#namaJabatanPIC').val("");
                            $('#jabatanPIC').append("<option value='" + data.id + "'>" + data.namaJabatan + "</option>");
                            toastr.success("Nama Jabatan PIC Berhasil disimpan");
                            cancelNamaJabatanPIC();
                        }
                    }
                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
        };

        cancelNamaJabatanPIC = function(){
            $("#namaJabatanPIC").hide();
            $("#saveNamaJabatan").hide();
            $("#cancelNamaJabatan").hide();
            $("#jabatanPIC").show();
            $("#createNamaJabatan").show();
        };

        $('#kodePos').autoNumeric('init',{
            vMin:'0',
            vMax:'99999',
            mDec: null,
            aSep:''
        });
        $('#kodePosNPWP').autoNumeric('init',{
            vMin:'0',
            vMax:'99999',
            mDec: null,
            aSep:''
        });
        $('#rtPIC').autoNumeric('init',{
            vMin:'0',
            vMax:'999',
            mDec: null,
            aSep:''
        });
        $('#rwPIC').autoNumeric('init',{
            vMin:'0',
            vMax:'999',
            mDec: null,
            aSep:''
        });
        $('#kodePosPIC').autoNumeric('init',{
            vMin:'0',
            vMax:'99999',
            mDec: null,
            aSep:''
        });
        $('#provinsi').typeahead({
			    source: function (query, process) {
			        provinsiList = [];
                    map = {};
			        return $.get('${request.contextPath}/company/getProvinsiList', { query: query }, function (data) {
			            $.each(data.options, function (i, provinsi) {
                            map[provinsi.nama] = provinsi;
                            provinsiList.push(provinsi.nama);
                        });
			            return process(provinsiList);
			        });
			    } ,
                updater: function (item) {
                    $("#provinsi_id").val(map[item].id);
                    $('#kabKota').val('');
                    $("#kabKota_id").val('');
                    $('#kecamatan').val('');
                    $('#kecamatan_id').val('');
                    $('#kelurahan').val('');
                    $('#kelurahan_id').val('');
                    return item;
                }

	    });

	    $('#kabKota').typeahead({
			    source: function (query, process) {
			        kabKotaList = [];
                    map = {};
			        return $.get('${request.contextPath}/company/getKabKotaList', { query: query, provinsi: $("#provinsi_id").val() }, function (data) {
			            $.each(data.options, function (i, kabKota) {
                            map[kabKota.nama] = kabKota;
                            kabKotaList.push(kabKota.nama);
                        });
			            return process(kabKotaList);
			        });
			    } ,
                updater: function (item) {
                    $("#kabKota_id").val(map[item].id);
                    $('#kecamatan').val('');
                    $('#kecamatan_id').val('');
                    $('#kelurahan').val('');
                    $('#kelurahan_id').val('');
                    return item;
                }

	    });

	    $('#kecamatan').typeahead({
			    source: function (query, process) {
			        kecamatanList = [];
                    map = {};
			        return $.get('${request.contextPath}/company/getKecamatanList', { query: query, kabKota: $("#kabKota_id").val() }, function (data) {
			            $.each(data.options, function (i, kecamatan) {
                            map[kecamatan.nama] = kecamatan;
                            kecamatanList.push(kecamatan.nama);
                        });
			            return process(kecamatanList);
			        });
			    } ,
                updater: function (item) {
                    $("#kecamatan_id").val(map[item].id);
                    $('#kelurahan').val('');
                    $('#kelurahan_id').val('');
                    return item;
                }

	    });

        $('#kelurahan').typeahead({
			    source: function (query, process) {
			        kelurahanList = [];
                    map = {};
			        return $.get('${request.contextPath}/company/getKelurahanList', { query: query, kecamatan: $("#kecamatan_id").val() }, function (data) {
			            $.each(data.options, function (i, kelurahan) {
                            map[kelurahan.nama] = kelurahan;
                            kelurahanList.push(kelurahan.nama);
                        });
			            return process(kelurahanList);
			        });
			    } ,
                updater: function (item) {
                    $("#kelurahan_id").val(map[item].id);
                    return item;
                }

	    });


	    $('#provinsiNPWP').typeahead({
			    source: function (query, process) {
			        provinsiList = [];
                    map = {};
			        return $.get('${request.contextPath}/company/getProvinsiList', { query: query }, function (data) {
			            $.each(data.options, function (i, provinsi) {
                            map[provinsi.nama] = provinsi;
                            provinsiList.push(provinsi.nama);
                        });
			            return process(provinsiList);
			        });
			    } ,
                updater: function (item) {
                    $("#provinsiNPWP_id").val(map[item].id);
                    $('#kabKotaNPWP').val('');
                    $("#kabKotaNPWP_id").val('');
                    $('#kecamatanNPWP').val('');
                    $('#kecamatanNPWP_id').val('');
                    $('#kelurahanNPWP').val('');
                    $('#kelurahanNPWP_id').val('');
                    return item;
                }

	    });

	    $('#kabKotaNPWP').typeahead({
			    source: function (query, process) {
			        kabKotaList = [];
                    map = {};
			        return $.get('${request.contextPath}/company/getKabKotaList', { query: query, provinsi: $("#provinsiNPWP_id").val() }, function (data) {
			            $.each(data.options, function (i, kabKota) {
                            map[kabKota.nama] = kabKota;
                            kabKotaList.push(kabKota.nama);
                        });
			            return process(kabKotaList);
			        });
			    } ,
                updater: function (item) {
                    $("#kabKotaNPWP_id").val(map[item].id);
                    $('#kecamatanNPWP').val('');
                    $('#kecamatanNPWP_id').val('');
                    $('#kelurahanNPWP').val('');
                    $('#kelurahanNPWP_id').val('');
                    return item;
                }

	    });

	    $('#kecamatanNPWP').typeahead({
			    source: function (query, process) {
			        kecamatanList = [];
                    map = {};
			        return $.get('${request.contextPath}/company/getKecamatanList', { query: query, kabKota: $("#kabKotaNPWP_id").val() }, function (data) {
			            $.each(data.options, function (i, kecamatan) {
                            map[kecamatan.nama] = kecamatan;
                            kecamatanList.push(kecamatan.nama);
                        });
			            return process(kecamatanList);
			        });
			    } ,
                updater: function (item) {
                    $("#kecamatanNPWP_id").val(map[item].id);
                    $('#kelurahanNPWP').val('');
                    $('#kelurahanNPWP_id').val('');
                    return item;
                }

	    });

        $('#kelurahanNPWP').typeahead({
			    source: function (query, process) {
			        kelurahanList = [];
                    map = {};
			        return $.get('${request.contextPath}/company/getKelurahanList', { query: query, kecamatan: $("#kecamatanNPWP_id").val() }, function (data) {
			            $.each(data.options, function (i, kelurahan) {
                            map[kelurahan.nama] = kelurahan;
                            kelurahanList.push(kelurahan.nama);
                        });
			            return process(kelurahanList);
			        });
			    } ,
                updater: function (item) {
                    $("#kelurahanNPWP_id").val(map[item].id);
                    return item;
                }

	    });

	    $('#provinsiPIC').typeahead({
			    source: function (query, process) {
			        provinsiList = [];
                    map = {};
			        return $.get('${request.contextPath}/company/getProvinsiList', { query: query }, function (data) {
			            $.each(data.options, function (i, provinsi) {
                            map[provinsi.nama] = provinsi;
                            provinsiList.push(provinsi.nama);
                        });
			            return process(provinsiList);
			        });
			    } ,
                updater: function (item) {
                    $("#provinsiPIC_id").val(map[item].id);
                    $('#kabKotaPIC').val('');
                    $("#kabKotaPIC_id").val('');
                    $('#kecamatanPIC').val('');
                    $('#kecamatanPIC_id').val('');
                    $('#kelurahanPIC').val('');
                    $('#kelurahanPIC_id').val('');
                    return item;
                }

	    });

	    $('#kabKotaPIC').typeahead({
			    source: function (query, process) {
			        kabKotaList = [];
                    map = {};
			        return $.get('${request.contextPath}/company/getKabKotaList', { query: query, provinsi: $("#provinsiPIC_id").val() }, function (data) {
			            $.each(data.options, function (i, kabKota) {
                            map[kabKota.nama] = kabKota;
                            kabKotaList.push(kabKota.nama);
                        });
			            return process(kabKotaList);
			        });
			    } ,
                updater: function (item) {
                    $("#kabKotaPIC_id").val(map[item].id);
                    $('#kecamatanPIC').val('');
                    $('#kecamatanPIC_id').val('');
                    $('#kelurahanPIC').val('');
                    $('#kelurahanPIC_id').val('');
                    return item;
                }

	    });

	    $('#kecamatanPIC').typeahead({
			    source: function (query, process) {
			        kecamatanList = [];
                    map = {};
			        return $.get('${request.contextPath}/company/getKecamatanList', { query: query, kabKota: $("#kabKotaPIC_id").val() }, function (data) {
			            $.each(data.options, function (i, kecamatan) {
                            map[kecamatan.nama] = kecamatan;
                            kecamatanList.push(kecamatan.nama);
                        });
			            return process(kecamatanList);
			        });
			    } ,
                updater: function (item) {
                    $("#kecamatanPIC_id").val(map[item].id);
                    $('#kelurahanPIC').val('');
                    $('#kelurahanPIC_id').val('');
                    return item;
                }

	    });

        $('#kelurahanPIC').typeahead({
			    source: function (query, process) {
			        kelurahanList = [];
                    map = {};
			        return $.get('${request.contextPath}/company/getKelurahanList', { query: query, kecamatan: $("#kecamatanPIC_id").val() }, function (data) {
			            $.each(data.options, function (i, kelurahan) {
                            map[kelurahan.nama] = kelurahan;
                            kelurahanList.push(kelurahan.nama);
                        });
			            return process(kelurahanList);
			        });
			    } ,
                updater: function (item) {
                    $("#kelurahanPIC_id").val(map[item].id);
                    return item;
                }

	    });
    });

    function getKodePos(dari){
        var kelurahan = $("#kelurahan"+dari).val();
        var kecamatan = $("#kecamatan"+dari).val();
        var kabupaten = $("#kabKota"+dari).val();
        if(kelurahan.toString().length > 0 && kecamatan.toString().length > 0 && kabupaten.toString().length > 0 ){
            jQuery.getJSON('${request.contextPath}/company/getKodePos?kelurahan='+kelurahan+"&kecamatan="+kecamatan+"&kabKota="+kabupaten, function (data) {
                $("#kodePos"+dari).val(data);
            });
        }
    }
</g:javascript>

<div class="row-fluid">
	<div id="companyProfileForm" class="span4">
		<fieldset>
			<legend>Company Profile</legend>
            <div
                class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'jenisPerusahaan', 'error')} ">
                <label class="control-label" for="kodePerusahaan"> <g:message
                        code="company.jenisPerusahaan.label" default="Kode Perusahaan" />

                </label>
                <div class="controls customer-company">
                    <g:textField name="kodePerusahaan"
                                 value="${companyInstance?.kodePerusahaan}" readonly=""/>
                </div>
            </div>
			<div
				class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'jenisPerusahaan', 'error')} ">
				<label class="control-label" for="jenisPerusahaan"> <g:message
						code="company.jenisPerusahaan.label" default="Jenis Perusahaan" /><span class="required-indicator">*</span>

				</label>
				<div class="controls customer-company">
                    %{--<g:select name="jenisPerusahaan" from="${['CV', 'PT', 'Firma']}" value="${companyInstance?.jenisPerusahaan}"--}%
                              %{--noSelection="['':'-Pilih salah satu-']" required=""/>--}%
                    <g:select id="jenisPerusahaan" name="jenisPerusahaan.id"
                              from="${com.kombos.customerprofile.JenisPerusahaan.list()}"
                              optionKey="id" required=""
                              value="${companyInstance?.jenisPerusahaan?.id}" class="many-to-one" style="width: 130px;"/>
				</div>
			</div>
        <g:javascript>
            document.getElementById("jenisPerusahaan").focus();
        </g:javascript>
			<div
				class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'namaPerusahaan', 'error')} ">
				<label class="control-label" for="namaPerusahaan"> <g:message
						code="company.namaPerusahaan.label" default="Nama Perusahaan" /><span class="required-indicator">*</span>

				</label>
				<div class="controls customer-company">
					<g:textField name="namaPerusahaan"
						value="${companyInstance?.namaPerusahaan}" required="" maxlength="50"/>
				</div>
			</div>

			<div class="tabbable control-group">
				<ul class="nav nav-tabs">
					<li id="tab_cc_alamat_korespondensi" class="active"><a
						href="#cc_alamat_korespondensi" data-toggle="tab">Alamat
							Korespondensi</a></li>
					<li id="tab_cc_alamat_npwp" class=""><a href="#cc_alamat_npwp"
						data-toggle="tab">Alamat NPWP</a></li>
				</ul>

				<div class="tab-content">
					<div class="tab-pane active" id="cc_alamat_korespondensi">
						<div
							class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'alamatPerusahaan', 'error')} ">
							<label class="control-label" for="alamatPerusahaan"> <g:message
									code="company.alamatPerusahaan.label"
									default="Alamat Perusahaan" />
							</label><span class="required-indicator">*</span>
							<div class="controls customer-company">
								<g:textArea name="alamatPerusahaan"
									value="${companyInstance?.alamatPerusahaan}" required="" maxlength="100"/>
							</div>
						</div>
						<div
							class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'provinsi', 'error')} ">
							<label class="control-label" for="provinsi"> <g:message
									code="company.provinsi.label" default="Provinsi" />
							</label>
							<div class="controls customer-company">
                                <g:textField name="__propinsi" id="provinsi"  value="${companyInstance?.provinsi?.m001NamaProvinsi}" class="typeahead"  autocomplete="off" maxlength="220" onchange="void(0);"/>
                                <g:hiddenField name="provinsi.id" id="provinsi_id" value="${companyInstance?.provinsi?.id}" />
								%{--<g:select id="provinsi" name="provinsi.id"--}%
									%{--from="${com.kombos.administrasi.Provinsi.list()}"--}%
									%{--optionKey="id" required=""--}%
									%{--value="${companyInstance?.provinsi?.id}" class="many-to-one" />--}%
							</div>
						</div>
						<div
							class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'kabKota', 'error')} ">
							<label class="control-label" for="kabKota"> <g:message
									code="company.kabKota.label" default="Kabupaten/Kota" />
							</label>
							<div class="controls customer-company">
                                <g:textField name="__kabKota" id="kabKota"  value="${companyInstance?.kabKota?.m002NamaKabKota}" class="typeahead"  autocomplete="off" maxlength="220" onchange="void(0);"/>
                                <g:hiddenField name="kabKota.id" id="kabKota_id" value="${companyInstance?.kabKota?.id}" />

                                %{--<g:select id="kabKota" name="kabKota.id"--}%
									%{--from="${com.kombos.administrasi.KabKota.list()}"--}%
									%{--optionKey="id" required=""--}%
									%{--value="${companyInstance?.kabKota?.id}" class="many-to-one" />--}%
							</div>
						</div>
						<div
							class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'kecamatan', 'error')} ">
							<label class="control-label" for="kecamatan"> <g:message
									code="company.kecamatan.label" default="Kecamatan" />

							</label>
							<div class="controls customer-company">
                                <g:textField name="__kecamatan" id="kecamatan"  value="${companyInstance?.kecamatan?.m003NamaKecamatan}" class="typeahead"  autocomplete="off" maxlength="220" onchange="void(0);"/>
                                <g:hiddenField name="kecamatan.id" id="kecamatan_id" value="${companyInstance?.kecamatan?.id}" />

                                %{--<g:select id="kecamatan" name="kecamatan.id"--}%
									%{--from="${com.kombos.administrasi.Kecamatan.list()}"--}%
									%{--optionKey="id" required=""--}%
									%{--value="${companyInstance?.kecamatan?.id}" class="many-to-one" />--}%
							</div>
						</div>
						<div
							class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'kelurahan', 'error')} ">
							<label class="control-label" for="kelurahan"> <g:message
									code="company.kelurahan.label" default="Kelurahan/Desa" />

							</label>
							<div class="controls customer-company">
                                <g:textField name="__kelurahan" onblur="getKodePos('');" id="kelurahan"  value="${companyInstance?.kelurahan?.m004NamaKelurahan}" class="typeahead"  autocomplete="off" maxlength="220" onchange="void(0);"/>
                                <g:hiddenField name="kelurahan.id" id="kelurahan_id" value="${companyInstance?.kelurahan?.id}" />

                            </div>
						</div>
						<div
							class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'kodePos', 'error')} ">
							<label class="control-label" for="kodePos"> <g:message
									code="company.kodePos.label" default="Kode Pos" />

							</label>
							<div class="controls customer-company">
								<g:textField readonly="" name="kodePos" value="${companyInstance?.kodePos}" />
							</div>
						</div>
					</div>
					<div class="tab-pane" id="cc_alamat_npwp">
						<div
							class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'alamatNPWP', 'error')} ">
							<label class="control-label" for="alamatNPWP"> <g:message
									code="company.alamatNPWP.label"
									default="Alamat NPWP Perusahaan" /><span class="required-indicator">*</span>

							</label>
							<div class="controls customer-company">
								<g:textArea name="alamatNPWP"
									value="${companyInstance?.alamatNPWP}" required="" maxlength="100"/>
							</div>
						</div>
						<div
							class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'provinsiNPWP', 'error')} ">
							<label class="control-label" for="provinsiNPWP"> <g:message
									code="company.provinsiNPWP.label" default="Provinsi" />

							</label>
							<div class="controls customer-company">
                                <g:textField name="__provinsiNPWP" id="provinsiNPWP"  value="${companyInstance?.provinsiNPWP?.m001NamaProvinsi}" class="typeahead"  autocomplete="off" maxlength="220" onchange="void(0);"/>
                                <g:hiddenField name="provinsiNPWP.id" id="provinsiNPWP_id" value="${companyInstance?.provinsiNPWP?.id}" />

                                %{--<g:select id="provinsiNPWP" name="provinsiNPWP.id"--}%
									%{--from="${com.kombos.administrasi.Provinsi.list()}"--}%
									%{--optionKey="id" required=""--}%
									%{--value="${companyInstance?.provinsiNPWP?.id}"--}%
									%{--class="many-to-one" />--}%
							</div>
						</div>
						<div
							class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'kabKotaNPWP', 'error')} ">
							<label class="control-label" for="kabKotaNPWP"> <g:message
									code="company.kabKotaNPWP.label" default="Kabupaten/Kota" />

							</label>

                            <div class="controls customer-company">
                                <g:textField name="__kabKotaNPWP" id="kabKotaNPWP"  value="${companyInstance?.kabKotaNPWP?.m002NamaKabKota}" class="typeahead"  autocomplete="off" maxlength="220" onchange="void(0);"/>
                                <g:hiddenField name="kabKotaNPWP.id" id="kabKotaNPWP_id" value="${companyInstance?.kabKotaNPWP?.id}" />

                                %{--<g:select id="kabKotaNPWP" name="kabKotaNPWP.id"--}%
									%{--from="${com.kombos.administrasi.KabKota.list()}"--}%
									%{--optionKey="id" required=""--}%
									%{--value="${companyInstance?.kabKotaNPWP?.id}" class="many-to-one" />--}%
							</div>
						</div>
						<div
							class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'kecamatanNPWP', 'error')} ">
							<label class="control-label" for="kecamatanNPWP"> <g:message
									code="company.kecamatanNPWP.label" default="Kecamatan" />

							</label>
							<div class="controls customer-company">
                                <g:textField name="__kecamatanNPWP" id="kecamatanNPWP"  value="${companyInstance?.kecamatanNPWP?.m003NamaKecamatan}" class="typeahead"  autocomplete="off" maxlength="220" onchange="void(0);"/>
                                <g:hiddenField name="kecamatanNPWP.id" id="kecamatanNPWP_id" value="${companyInstance?.kecamatanNPWP?.id}" />

                                %{--<g:select id="kecamatanNPWP" name="kecamatanNPWP.id"--}%
									%{--from="${com.kombos.administrasi.Kecamatan.list()}"--}%
									%{--optionKey="id" required=""--}%
									%{--value="${companyInstance?.kecamatanNPWP?.id}"--}%
									%{--class="many-to-one" />--}%
							</div>
						</div>
						<div
							class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'kelurahanNPWP', 'error')} ">
							<label class="control-label" for="kelurahanNPWP"> <g:message
									code="company.kelurahanNPWP.label" default="Kelurahan/Desa" />

							</label>
							<div class="controls customer-company">
                                <g:textField name="__kelurahanNPWP" id="kelurahanNPWP"  onblur="getKodePos('NPWP');" value="${companyInstance?.kelurahanNPWP?.m004NamaKelurahan}" class="typeahead"  autocomplete="off" maxlength="220" onchange="void(0);"/>
                                <g:hiddenField name="kelurahanNPWP.id" id="kelurahanNPWP_id" value="${companyInstance?.kelurahanNPWP?.id}" />

                            </div>
						</div>
						<div
							class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'kodePosNPWP', 'error')} ">
							<label class="control-label" for="kodePosNPWP"> <g:message
									code="company.kodePosNPWP.label" default="Kode Pos" />

							</label>
							<div class="controls customer-company">
								<g:textField name="kodePosNPWP" readonly="" value="${companyInstance?.kodePosNPWP}" />
							</div>
						</div>
					</div>
				</div>
			</div>

			<div
				class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'telp', 'error')} ">
				<label class="control-label" for="telp"> <g:message
						code="company.telp.label" default="Telepon" /><span class="required-indicator">*</span>

				</label>
				<div class="controls customer-company">
					<g:textField name="telp" onkeypress="return isNumberKey(event)" value="${companyInstance?.telp}" required="" maxlength="25"/>
				</div>
			</div>
			<div
				class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'fax', 'error')} ">
				<label class="control-label" for="fax"> <g:message
						code="company.fax.label" default="Faximile" />

				</label>
				<div class="controls customer-company">
					<g:textField name="fax" onkeypress="return isNumberKey(event)" value="${companyInstance?.fax}" maxlength="25"/>
				</div>
			</div>
			<div
				class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'website', 'error')} ">
				<label class="control-label" for="website"> <g:message
						code="company.website.label" default="Website" />

				</label>
				<div class="controls customer-company">
					<g:textField name="website" value="${companyInstance?.website}" maxlength="50"/>
				</div>
			</div>
			<div
				class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'noNPWP', 'error')} ">
				<label class="control-label" for="noNPWP"> <g:message
						code="company.noNPWP.label" default="NPWP" />

				</label>
				<div class="controls customer-company">
					<g:textField name="noNPWP" value="${companyInstance?.noNPWP}" maxlength="20"/>
				</div>
			</div>
		</fieldset>
	</div>
	<div class="span8 row-fluid">
		<div id="companyPICForm" class="span12 row-fluid">
			<fieldset>
				<legend>Company PIC</legend>
			</fieldset>
			<div class="span5">
				<div
					class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'namaDepanPIC', 'error')} ">
					<label class="control-label" for="namaDepanPIC"> <g:message
							code="company.namaDepanPIC.label" default="Nama Depan" /><span class="required-indicator">*</span>

					</label>
					<div class="controls customer-company">
						<g:textField name="namaDepanPIC"
							value="${companyInstance?.namaDepanPIC}" required="" maxlength="50"/>
					</div>
				</div>
				<div
					class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'namaBelakangPIC', 'error')} ">
					<label class="control-label" for="namaBelakangPIC"> <g:message
							code="company.namaBelakangPIC.label" default="Nama Belakang" />

					</label>
					<div class="controls customer-company">
						<g:textField name="namaBelakangPIC"
							value="${companyInstance?.namaBelakangPIC}" maxlength="50"/>
					</div>
				</div>
				<div
					class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'jenisKelaminPIC', 'error')} ">
					<label class="control-label" for="jenisKelaminPIC"> <g:message
							code="company.jenisKelaminPIC.label" default="Jenis Kelamin" /><span class="required-indicator">*</span>
					</label>
					<div class="controls" style="height: 35px; width: 250px;">
						<g:radioGroup name="jenisKelaminPIC" values="['L','P']" value="${companyInstance?.jenisKelaminPIC}" labels="['Laki-laki','Perempuan']" required="">
							${it.radio} <g:message code="${it.label}" />
						</g:radioGroup>
					</div>
				</div>
				<div
					class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'tglLahirPIC', 'error')} ">
					<label class="control-label" for="tglLahirPIC"> <g:message
							code="company.tglLahirPIC.label" default="Tanggal Lahir" />

					</label>
					<div class="controls customer-company">
						<ba:datePicker name="tglLahirPIC" precision="day"
							value="${companyInstance?.tglLahirPIC}" format="dd/MM/yyyy" />
					</div>
				</div>
				<div
					class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'telpPIC', 'error')} ">
					<label class="control-label" for="telpPIC"> <g:message
							code="company.telpPIC.label" default="Telepon Rumah" /><span class="required-indicator">*</span>

					</label>
					<div class="controls customer-company">
						<g:textField onkeypress="return isNumberKey(event)" name="telpPIC" value="${companyInstance?.telpPIC}" required="" maxlength="50"/>
					</div>
				</div>
				<div
					class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'hpPIC', 'error')} ">
					<label class="control-label" for="hpPIC"> <g:message
							code="company.hpPIC.label" default="Telepon Seluler" /><span class="required-indicator">*</span>

					</label>
					<div class="controls customer-company">
						<g:textField name="hpPIC" onkeypress="return isNumberKey(event)" value="${companyInstance?.hpPIC}" required="" maxlength="25"/>
					</div>
				</div>
				<div
					class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'jabatanPIC', 'error')} ">
					<label class="control-label" for="jabatanPIC"> <g:message
							code="company.jabatanPIC.label" default="Jabatan PIC" />

					</label>
					<div class="controls customer-company" style="width: 250px;">
                        <g:select id="jabatanPIC" name="jabatanPIC.id" from="${com.kombos.maintable.NamaJabatanPIC.findAll('from NamaJabatanPIC j where j.staDel = 0 order by j.m093NamaJabatan')}"
                                  optionKey="id" value="${companyInstance?.jabatanPIC?.id}" noSelection="['':'Silahkan Pilih']"
                                  class="many-to-one" style="width: 130px;"/>
                        <g:textField name="namaJabatanPIC" id="namaJabatanPIC"/>
                        <g:field type="button" onclick="createNamaJabatanPIC();" class="btn btn-primary create" name="createNamaJabatan" id="createNamaJabatan" value="${message(code: 'default.button.add.label', default: '+')}" style="width: 15px;"/>
                        <br/>
                        <g:field type="button" onclick="saveNamaJabatanPIC();" class="btn btn-primary create" name="saveNamaJabatan" id="saveNamaJabatan" value="${message(code: 'default.button.save.label', default: 'Simpan')}" style="width: 25px;"/>
                        <g:field type="button" onclick="cancelNamaJabatanPIC();" class="btn btn-primary create" name="cancelNamaJabatan" id="cancelNamaJabatan" value="${message(code: 'default.button.cancel.label', default: 'Cancel')}" style="width: 25px;"/>
					</div>
				</div>
				<div
					class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'emailPIC', 'error')} ">
					<label class="control-label" for="emailPIC"> <g:message
							code="company.emailPIC.label" default="Email" />

					</label>
					<div class="controls customer-company">
						<g:textField name="emailPIC" value="${companyInstance?.emailPIC}" />
					</div>
				</div>
			</div>
			<div class="span5">
				<div
					class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'alamatPIC', 'error')} ">
					<label class="control-label" for="alamatPIC"> <g:message
							code="company.alamatPIC.label" default="Alamat PIC" /><span class="required-indicator">*</span>

					</label>
					<div class="controls customer-company">
						<g:textArea name="alamatPIC"
							value="${companyInstance?.alamatPIC}" required="" maxlength="100"/>
					</div>
				</div>
				<div
					class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'rtPIC', 'error')} ">
					<label class="control-label" for="rtPIC"> <g:message
							code="company.rtPIC.label" default="RT / RW" />

					</label>
					<div class="controls customer-company" style="width: 200px;">
						<g:textField name="rtPIC" value="${companyInstance?.rtPIC}"
							style="width: 51px;" maxlength="10"/>
						&nbsp;/&nbsp;
						<g:textField name="rwPIC" value="${companyInstance?.rwPIC}"
							style="width: 51px;" maxlength="10"/>
					</div>
				</div>

				<div
					class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'provinsiPIC', 'error')} ">
					<label class="control-label" for="provinsiPIC"> <g:message
							code="company.provinsiPIC.label" default="Provinsi" />

					</label>
					<div class="controls customer-company">
                        <g:textField name="__propinsiPIC" id="provinsiPIC"  value="${companyInstance?.provinsiPIC?.m001NamaProvinsi}" class="typeahead"  autocomplete="off" maxlength="220" onchange="void(0);"/>
                        <g:hiddenField name="provinsiPIC.id" id="provinsiPIC_id" value="${companyInstance?.provinsiPIC?.id}" />
						%{--<g:select id="provinsiPIC" name="provinsiPIC.id"--}%
							%{--from="${com.kombos.administrasi.Provinsi.list()}"--}%
							%{--optionKey="id"--}%
							%{--value="${companyInstance?.provinsiPIC?.id}" class="many-to-one" />--}%
					</div>
				</div>
				<div
					class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'kabKotaPIC', 'error')} ">
					<label class="control-label" for="kabKotaPIC"> <g:message
							code="company.kabKotaPIC.label" default="Kabupaten/Kota " />

					</label>
					<div class="controls customer-company">
                        <g:textField name="__kabKotaPIC" id="kabKotaPIC"  value="${companyInstance?.kabKotaPIC?.m002NamaKabKota}" class="typeahead"  autocomplete="off" maxlength="220" onchange="void(0);"/>
                        <g:hiddenField name="kabKotaPIC.id" id="kabKotaPIC_id" value="${companyInstance?.kabKotaPIC?.id}" />

                        %{--<g:select id="kabKotaPIC" name="kabKotaPIC.id"--}%
							%{--from="${com.kombos.administrasi.KabKota.list()}"--}%
							%{--optionKey="id"--}%
							%{--value="${companyInstance?.kabKotaPIC?.id}" class="many-to-one" />--}%
					</div>
				</div>
				<div
					class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'kecamatanPIC', 'error')} ">
					<label class="control-label" for="kecamatanPIC"> <g:message
							code="company.kecamatanPIC.label" default="Kecamatan" />

					</label>
					<div class="controls customer-company">
                        <g:textField name="__kecamatanPIC" id="kecamatanPIC"  value="${companyInstance?.kecamatanPIC?.m003NamaKecamatan}" class="typeahead"  autocomplete="off" maxlength="220" onchange="void(0);"/>
                        <g:hiddenField name="kecamatanPIC.id" id="kecamatanPIC_id" value="${companyInstance?.kecamatanPIC?.id}" />

                        %{--<g:select id="kecamatanPIC" name="kecamatanPIC.id"--}%
							%{--from="${com.kombos.administrasi.Kecamatan.list()}"--}%
							%{--optionKey="id"--}%
							%{--value="${companyInstance?.kecamatanPIC?.id}" class="many-to-one" />--}%
					</div>
				</div>
				<div
					class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'kelurahanPIC', 'error')} ">
					<label class="control-label" for="kelurahanPIC"> <g:message
							code="company.kelurahanPIC.label" default="Kelurahan/Desa" />

					</label>
					<div class="controls customer-company">
                        <g:textField name="__kelurahanPIC" id="kelurahanPIC" onblur="getKodePos('PIC');" value="${companyInstance?.kelurahanPIC?.m004NamaKelurahan}" class="typeahead"  autocomplete="off" maxlength="220" onchange="void(0);"/>
                        <g:hiddenField name="kelurahanPIC.id" id="kelurahanPIC_id" value="${companyInstance?.kelurahanPIC?.id}" />

                    </div>
				</div>
				<div
					class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'kodePosPic', 'error')} ">
					<label class="control-label" for="kodePosPIC"> <g:message
							code="company.kodePosPIC.label" default="Kode Pos" />

					</label>
					<div class="controls customer-company">
						<g:textField name="kodePosPIC" readonly="" value="${companyInstance?.kodePosPIC}" />
					</div>
				</div>
			</div>

		</div>
		<g:if test="${companyInstance?.id}">
			<div id="spkList" class="span11">
				<fieldset>
					<legend>SPK List
					<a class="pull-right box-action" href="javascript:void(0);" onclick="showSPK();"
						style="display: block;">&nbsp;&nbsp;<i
						class="icon-plus"></i>&nbsp;&nbsp;
						</a>
					</legend>
				</fieldset>
				<g:render template="spkDataTables" />
			</div>
		</g:if>
	</div>
</div>
<g:if test="${companyInstance?.id}">
	<div class="row-fluid">
		<div id="komposisiKendaraanSelainToyota" class="span4">
			<fieldset>
				<legend>Komposisi Kendaraan Selain Toyota</legend>
			</fieldset>
			<g:render template="komposisiKendaraanSelainToyotaDataTables" />
		</div>
		<div id="companyVehicle" class="span8">
			<fieldset>
				<legend>Vehicles List</legend>
			</fieldset>
			<g:render template="companyVehicleDataTables" />
		</div>
	</div>
</g:if>
