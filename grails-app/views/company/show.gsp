

<%@ page import="com.kombos.maintable.Company" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'company.label', default: 'Company')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteCompany;

$(function(){ 
	deleteCompany=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/company/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadCompanyTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-company" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="company"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${companyInstance?.alamatNPWP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="alamatNPWP-label" class="property-label"><g:message
					code="company.alamatNPWP.label" default="Alamat NPWP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="alamatNPWP-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="alamatNPWP"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter alamatNPWP" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="alamatNPWP"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.alamatPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="alamatPIC-label" class="property-label"><g:message
					code="company.alamatPIC.label" default="Alamat PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="alamatPIC-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="alamatPIC"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter alamatPIC" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="alamatPIC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.alamatPerusahaan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="alamatPerusahaan-label" class="property-label"><g:message
					code="company.alamatPerusahaan.label" default="Alamat Perusahaan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="alamatPerusahaan-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="alamatPerusahaan"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter alamatPerusahaan" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="alamatPerusahaan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.companyDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="companyDealer-label" class="property-label"><g:message
					code="company.companyDealer.label" default="Company Dealer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="companyDealer-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="companyDealer"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter companyDealer" onsuccess="reloadCompanyTable();" />--}%
							
								<g:link controller="companyDealer" action="show" id="${companyInstance?.companyDealer?.id}">${companyInstance?.companyDealer?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.emailPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="emailPIC-label" class="property-label"><g:message
					code="company.emailPIC.label" default="Email PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="emailPIC-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="emailPIC"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter emailPIC" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="emailPIC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.fax}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="fax-label" class="property-label"><g:message
					code="company.fax.label" default="Fax" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="fax-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="fax"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter fax" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="fax"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.hpPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="hpPIC-label" class="property-label"><g:message
					code="company.hpPIC.label" default="Hp PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="hpPIC-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="hpPIC"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter hpPIC" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="hpPIC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.jabatanPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="jabatanPIC-label" class="property-label"><g:message
					code="company.jabatanPIC.label" default="Jabatan PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="jabatanPIC-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="jabatanPIC"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter jabatanPIC" onsuccess="reloadCompanyTable();" />--}%
							
								<g:link controller="namaJabatanPIC" action="show" id="${companyInstance?.jabatanPIC?.id}">${companyInstance?.jabatanPIC?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.jenisKelaminPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="jenisKelaminPIC-label" class="property-label"><g:message
					code="company.jenisKelaminPIC.label" default="Jenis Kelamin PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="jenisKelaminPIC-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="jenisKelaminPIC"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter jenisKelaminPIC" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="jenisKelaminPIC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.jenisPerusahaan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="jenisPerusahaan-label" class="property-label"><g:message
					code="company.jenisPerusahaan.label" default="Jenis Perusahaan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="jenisPerusahaan-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="jenisPerusahaan"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter jenisPerusahaan" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="jenisPerusahaan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.kabKota}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kabKota-label" class="property-label"><g:message
					code="company.kabKota.label" default="Kab Kota" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kabKota-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="kabKota"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter kabKota" onsuccess="reloadCompanyTable();" />--}%
							
								<g:link controller="kabKota" action="show" id="${companyInstance?.kabKota?.id}">${companyInstance?.kabKota?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.kabKotaNPWP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kabKotaNPWP-label" class="property-label"><g:message
					code="company.kabKotaNPWP.label" default="Kab Kota NPWP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kabKotaNPWP-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="kabKotaNPWP"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter kabKotaNPWP" onsuccess="reloadCompanyTable();" />--}%
							
								<g:link controller="kabKota" action="show" id="${companyInstance?.kabKotaNPWP?.id}">${companyInstance?.kabKotaNPWP?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.kabKotaPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kabKotaPIC-label" class="property-label"><g:message
					code="company.kabKotaPIC.label" default="Kab Kota PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kabKotaPIC-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="kabKotaPIC"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter kabKotaPIC" onsuccess="reloadCompanyTable();" />--}%
							
								<g:link controller="kabKota" action="show" id="${companyInstance?.kabKotaPIC?.id}">${companyInstance?.kabKotaPIC?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.kecamatan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kecamatan-label" class="property-label"><g:message
					code="company.kecamatan.label" default="Kecamatan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kecamatan-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="kecamatan"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter kecamatan" onsuccess="reloadCompanyTable();" />--}%
							
								<g:link controller="kecamatan" action="show" id="${companyInstance?.kecamatan?.id}">${companyInstance?.kecamatan?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.kecamatanNPWP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kecamatanNPWP-label" class="property-label"><g:message
					code="company.kecamatanNPWP.label" default="Kecamatan NPWP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kecamatanNPWP-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="kecamatanNPWP"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter kecamatanNPWP" onsuccess="reloadCompanyTable();" />--}%
							
								<g:link controller="kecamatan" action="show" id="${companyInstance?.kecamatanNPWP?.id}">${companyInstance?.kecamatanNPWP?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.kecamatanPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kecamatanPIC-label" class="property-label"><g:message
					code="company.kecamatanPIC.label" default="Kecamatan PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kecamatanPIC-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="kecamatanPIC"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter kecamatanPIC" onsuccess="reloadCompanyTable();" />--}%
							
								<g:link controller="kecamatan" action="show" id="${companyInstance?.kecamatanPIC?.id}">${companyInstance?.kecamatanPIC?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.kelurahan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kelurahan-label" class="property-label"><g:message
					code="company.kelurahan.label" default="Kelurahan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kelurahan-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="kelurahan"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter kelurahan" onsuccess="reloadCompanyTable();" />--}%
							
								<g:link controller="kelurahan" action="show" id="${companyInstance?.kelurahan?.id}">${companyInstance?.kelurahan?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.kelurahanNPWP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kelurahanNPWP-label" class="property-label"><g:message
					code="company.kelurahanNPWP.label" default="Kelurahan NPWP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kelurahanNPWP-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="kelurahanNPWP"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter kelurahanNPWP" onsuccess="reloadCompanyTable();" />--}%
							
								<g:link controller="kelurahan" action="show" id="${companyInstance?.kelurahanNPWP?.id}">${companyInstance?.kelurahanNPWP?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.kelurahanPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kelurahanPIC-label" class="property-label"><g:message
					code="company.kelurahanPIC.label" default="Kelurahan PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kelurahanPIC-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="kelurahanPIC"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter kelurahanPIC" onsuccess="reloadCompanyTable();" />--}%
							
								<g:link controller="kelurahan" action="show" id="${companyInstance?.kelurahanPIC?.id}">${companyInstance?.kelurahanPIC?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.kodePerusahaan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kodePerusahaan-label" class="property-label"><g:message
					code="company.kodePerusahaan.label" default="Kode Perusahaan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kodePerusahaan-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="kodePerusahaan"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter kodePerusahaan" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="kodePerusahaan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.kodePos}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kodePos-label" class="property-label"><g:message
					code="company.kodePos.label" default="Kode Pos" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kodePos-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="kodePos"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter kodePos" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="kodePos"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.kodePosNPWP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kodePosNPWP-label" class="property-label"><g:message
					code="company.kodePosNPWP.label" default="Kode Pos NPWP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kodePosNPWP-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="kodePosNPWP"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter kodePosNPWP" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="kodePosNPWP"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.kodePosPic}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kodePosPic-label" class="property-label"><g:message
					code="company.kodePosPic.label" default="Kode Pos Pic" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kodePosPic-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="kodePosPic"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter kodePosPic" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="kodePosPic"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.namaBelakangPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="namaBelakangPIC-label" class="property-label"><g:message
					code="company.namaBelakangPIC.label" default="Nama Belakang PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="namaBelakangPIC-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="namaBelakangPIC"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter namaBelakangPIC" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="namaBelakangPIC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.namaDepanPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="namaDepanPIC-label" class="property-label"><g:message
					code="company.namaDepanPIC.label" default="Nama Depan PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="namaDepanPIC-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="namaDepanPIC"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter namaDepanPIC" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="namaDepanPIC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.namaPerusahaan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="namaPerusahaan-label" class="property-label"><g:message
					code="company.namaPerusahaan.label" default="Nama Perusahaan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="namaPerusahaan-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="namaPerusahaan"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter namaPerusahaan" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="namaPerusahaan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.noNPWP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="noNPWP-label" class="property-label"><g:message
					code="company.noNPWP.label" default="No NPWP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="noNPWP-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="noNPWP"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter noNPWP" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="noNPWP"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.provinsi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="provinsi-label" class="property-label"><g:message
					code="company.provinsi.label" default="Provinsi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="provinsi-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="provinsi"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter provinsi" onsuccess="reloadCompanyTable();" />--}%
							
								<g:link controller="provinsi" action="show" id="${companyInstance?.provinsi?.id}">${companyInstance?.provinsi?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.provinsiNPWP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="provinsiNPWP-label" class="property-label"><g:message
					code="company.provinsiNPWP.label" default="Provinsi NPWP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="provinsiNPWP-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="provinsiNPWP"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter provinsiNPWP" onsuccess="reloadCompanyTable();" />--}%
							
								<g:link controller="provinsi" action="show" id="${companyInstance?.provinsiNPWP?.id}">${companyInstance?.provinsiNPWP?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.provinsiPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="provinsiPIC-label" class="property-label"><g:message
					code="company.provinsiPIC.label" default="Provinsi PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="provinsiPIC-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="provinsiPIC"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter provinsiPIC" onsuccess="reloadCompanyTable();" />--}%
							
								<g:link controller="provinsi" action="show" id="${companyInstance?.provinsiPIC?.id}">${companyInstance?.provinsiPIC?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.rtPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="rtPIC-label" class="property-label"><g:message
					code="company.rtPIC.label" default="Rt PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="rtPIC-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="rtPIC"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter rtPIC" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="rtPIC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.rwPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="rwPIC-label" class="property-label"><g:message
					code="company.rwPIC.label" default="Rw PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="rwPIC-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="rwPIC"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter rwPIC" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="rwPIC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="company.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="staDel"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.t101GelarB}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t101GelarB-label" class="property-label"><g:message
					code="company.t101GelarB.label" default="T101 Gelar B" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t101GelarB-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="t101GelarB"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter t101GelarB" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="t101GelarB"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.t101GelarD}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t101GelarD-label" class="property-label"><g:message
					code="company.t101GelarD.label" default="T101 Gelar D" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t101GelarD-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="t101GelarD"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter t101GelarD" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="t101GelarD"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.t101Ket}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t101Ket-label" class="property-label"><g:message
					code="company.t101Ket.label" default="T101 Ket" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t101Ket-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="t101Ket"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter t101Ket" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="t101Ket"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.t101xNamaDivisi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t101xNamaDivisi-label" class="property-label"><g:message
					code="company.t101xNamaDivisi.label" default="T101x Nama Divisi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t101xNamaDivisi-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="t101xNamaDivisi"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter t101xNamaDivisi" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="t101xNamaDivisi"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.t101xNamaUser}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t101xNamaUser-label" class="property-label"><g:message
					code="company.t101xNamaUser.label" default="T101x Nama User" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t101xNamaUser-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="t101xNamaUser"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter t101xNamaUser" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="t101xNamaUser"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.telp}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="telp-label" class="property-label"><g:message
					code="company.telp.label" default="Telp" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="telp-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="telp"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter telp" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="telp"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.telpPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="telpPIC-label" class="property-label"><g:message
					code="company.telpPIC.label" default="Telp PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="telpPIC-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="telpPIC"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter telpPIC" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="telpPIC"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.tglLahirPIC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tglLahirPIC-label" class="property-label"><g:message
					code="company.tglLahirPIC.label" default="Tgl Lahir PIC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tglLahirPIC-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="tglLahirPIC"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter tglLahirPIC" onsuccess="reloadCompanyTable();" />--}%
							
								<g:formatDate date="${companyInstance?.tglLahirPIC}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${companyInstance?.website}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="website-label" class="property-label"><g:message
					code="company.website.label" default="Website" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="website-label">
						%{--<ba:editableValue
								bean="${companyInstance}" field="website"
								url="${request.contextPath}/Company/updatefield" type="text"
								title="Enter website" onsuccess="reloadCompanyTable();" />--}%
							
								<g:fieldValue bean="${companyInstance}" field="website"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${companyInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="agama.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">

                        <g:fieldValue bean="${companyInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${companyInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="agama.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">

                        <g:formatDate date="${companyInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${companyInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="agama.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">

                        <g:fieldValue bean="${companyInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${companyInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="agama.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">

                        <g:formatDate date="${companyInstance?.lastUpdated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${companyInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="agama.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">

                        <g:fieldValue bean="${companyInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${companyInstance?.id}"
					update="[success:'company-form',failure:'company-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteCompany('${companyInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
