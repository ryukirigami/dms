<table id="companyVehicle_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
			
			<th style="border-bottom: none;padding: 5px;">
				<div>Nomor Polisi</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Model</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Nama Customer</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>VinCode</div>
			</th>				
		</tr>
	</thead>
</table>

<g:javascript>
var companyVehicleTable;
var reloadcompanyVehicleTable;
$(function(){
	companyVehicleTable = $('#companyVehicle_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "companyVehicleDatatablesList")}",
		"aoColumns": [

{
	"sName": "noPol",
	"mDataProp": "noPol",
	"aTargets": [0],
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "model",
	"mDataProp": "model",
	"aTargets": [1],
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "namaCustomer",
	"mDataProp": "namaCustomer",
	"aTargets": [2],
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "vinCode",
	"mDataProp": "vinCode",
	"aTargets": [3],
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
        				aoData.push(
									{"name": 'companyId', "value": ${companyInstance?.id}}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
