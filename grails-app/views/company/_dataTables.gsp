
<%@ page import="com.kombos.maintable.Company" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="company_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>
			
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="company.namaPerusahaan.label" default="Nama Perusahaan" /></div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="company.jenisPerusahaan.label" default="Jenis" /></div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="company.alamatPerusahaan.label" default="Alamat Perusahaan" /></div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="company.telp.label" default="Telepon" /></div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="company.fax.label" default="Faximile" /></div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="company.provinsi.label" default="Provinsi" /></div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="company.kabKota.label" default="Kabupaten/Kota" /></div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="company.kecamatan.label" default="Kecamatan, KodePos" /></div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="company.kelurahan.label" default="Kelurahan" /></div>
			</th>	
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="company.namaPIC.label" default="Nama PIC" /></div>
			</th>				
		</tr>
		<tr>
			
			<th style="border-top: none;padding: 5px;">
				<div id="filter_namaPerusahaan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_namaPerusahaan" class="search_init" />
				</div>
			</th>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_jenisPerusahaan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 105px;">
					<input type="text" name="search_jenisPerusahaan" class="search_init" />
				</div>
			</th>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_alamatPerusahaan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_alamatPerusahaan" class="search_init" />
				</div>
			</th>			
			<th style="border-top: none;padding: 5px;">
				<div id="filter_telp" style="padding-top: 0px;position:relative; margin-top: 0px;width: 50px;">
					<input type="text" name="search_telp" class="search_init" />
				</div>
			</th>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_fax" style="padding-top: 0px;position:relative; margin-top: 0px;width: 50px;">
					<input type="text" name="search_fax" class="search_init" />
				</div>
			</th>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_provinsi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_provinsi" class="search_init" />
				</div>
			</th>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_kabKota" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_kabKota" class="search_init" />
				</div>
			</th>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_kecamatan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_kecamatan" class="search_init" />
				</div>
			</th>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_kelurahan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_kelurahan" class="search_init" />
				</div>
			</th>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_namaPIC" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_namaPIC" class="search_init" />
				</div>
			</th>
		</tr>
	</thead>
</table>

<g:javascript>
var companyTable;
var reloadCompanyTable;
var checked = [];
$(function(){

	
	reloadCompanyTable = function() {
		companyTable.fnDraw();
	}

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	companyTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	companyTable = $('#company_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnDrawCallback": function () {
		         $('.select-all').attr('checked', false);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
		        var aData = companyTable.fnGetData(nRow);
		        var checkbox = $('.row-select', nRow);
		        if(checked.indexOf(""+aData['id'])>=0){
		              checkbox.attr("checked",true);
		        }
			    return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "namaPerusahaan",
	"mDataProp": "namaPerusahaan",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="edit('+row['id']+');">'+data+'</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "jenisPerusahaan",
	"mDataProp": "jenisPerusahaan",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"170px",
	"bVisible": true
},
{
	"sName": "alamatPerusahaan",
	"mDataProp": "alamatPerusahaan",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "telp",
	"mDataProp": "telp",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "fax",
	"mDataProp": "fax",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "provinsi",
	"mDataProp": "provinsi",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "kabKota",
	"mDataProp": "kabKota",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "kecamatan",
	"mDataProp": "kecamatan",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "kelurahan",
	"mDataProp": "kelurahan",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "namaPIC",
	"mDataProp": "namaPIC",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                        $("#company_datatables tbody .row-select").each(function() {
                            var id = $(this).next("input:hidden").val();
                            if(checked.indexOf(""+id)>=0){
                                checked[checked.indexOf(""+id)]="";
                            }
                            if(this.checked){
                                checked.push(""+id);
                            }
                        });

						var alamatPerusahaan = $('#filter_alamatPerusahaan input').val();
						if(alamatPerusahaan){
							aoData.push(
									{"name": 'sCriteria_alamatPerusahaan', "value": alamatPerusahaan}
							);
						}
	
						var fax = $('#filter_fax input').val();
						if(fax){
							aoData.push(
									{"name": 'sCriteria_fax', "value": fax}
							);
						}
	
						var jenisPerusahaan = $('#filter_jenisPerusahaan input').val();
						if(jenisPerusahaan){
							aoData.push(
									{"name": 'sCriteria_jenisPerusahaan', "value": jenisPerusahaan}
							);
						}
	
						var kabKota = $('#filter_kabKota input').val();
						if(kabKota){
							aoData.push(
									{"name": 'sCriteria_kabKota', "value": kabKota}
							);
						}
	
						var kecamatan = $('#filter_kecamatan input').val();
						if(kecamatan){
							aoData.push(
									{"name": 'sCriteria_kecamatan', "value": kecamatan}
							);
						}
	
						var kelurahan = $('#filter_kelurahan input').val();
						if(kelurahan){
							aoData.push(
									{"name": 'sCriteria_kelurahan', "value": kelurahan}
							);
						}
	
						var namaPIC = $('#filter_namaPIC input').val();
						if(namaPIC){
							aoData.push(
									{"name": 'sCriteria_namaPIC', "value": namaPIC}
							);
						}
	
						var namaPerusahaan = $('#filter_namaPerusahaan input').val();
						if(namaPerusahaan){
							aoData.push(
									{"name": 'sCriteria_namaPerusahaan', "value": namaPerusahaan}
							);
						}
	
						var provinsi = $('#filter_provinsi input').val();
						if(provinsi){
							aoData.push(
									{"name": 'sCriteria_provinsi', "value": provinsi}
							);
						}
	
						var telp = $('#filter_telp input').val();
						if(telp){
							aoData.push(
									{"name": 'sCriteria_telp', "value": telp}
							);
						}



						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
