<table id="skp_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
			
			<th style="border-bottom: none;padding: 5px;">
				<div>Nama Perusahaan</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Nomor SPK</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Tanggal SPK</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Tanggal Awal</div>
			</th>	
			<th style="border-bottom: none;padding: 5px;">
				<div>Tanggal Akhir</div>
			</th>	
			<th style="border-bottom: none;padding: 5px;">
				<div>Jumlah Total (Rp)</div>
			</th>				
		</tr>
	</thead>
</table>

<g:javascript>
var skpTable;
var reloadskpTable;
$(function(){
	skpTable = $('#skp_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "spkDatatablesList")}",
		"aoColumns": [

{
	"sName": "namaPerusahaan",
	"mDataProp": "namaPerusahaan",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
	    return data + ", " + row['jenisPerusahaan'];
	},
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "noSPK",
	"mDataProp": "noSPK",
	"aTargets": [1],
	"bSortable": true,
	"sWidth":"70px",
	"bVisible": true
},
{
	"sName": "tanggalSPK",
	"mDataProp": "tanggalSPK",
	"aTargets": [2],
	"bSortable": true,
	"sWidth":"70px",
	"bVisible": true
},
{
	"sName": "tanggalAwal",
	"mDataProp": "tanggalAwal",
	"aTargets": [3],
	"bSortable": true,
	"sWidth":"70px",
	"bVisible": true
},
{
	"sName": "tanggalAkhir",
	"mDataProp": "tanggalAkhir",
	"aTargets": [4],
	"bSortable": true,
	"sWidth":"70px",
	"bVisible": true
},
{
	"sName": "jumlahTotal",
	"mDataProp": "jumlahTotal",
	"aTargets": [5],
	"mRender": function ( data, type, row ) {
				            			if(data != null)
				            				return '<span class="pull-right numeric">'+data+'</span>';
				            			else
				            				return '<span></span>';
								},
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
        				aoData.push(
									{"name": 'companyId', "value": ${companyInstance?.id}}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							        $('td span.numeric').text(function(index, text) {
												return $.number(text,0);
									});
							   }
						});
		}			
	});
});
</g:javascript>


			
