<%@ page import="com.kombos.maintable.Company" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'company.label', default: 'Company')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="create-company" class="content scaffold-create" role="main">
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${companyInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${companyInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:formRemote class="form-horizontal" name="create" on404="alert('not found!');" onSuccess="reloadCompanyTable();" update="company-form"--}%
              %{--url="[controller: 'company', action:'save']" >--}%
            <g:javascript>
                var submitForm;
                $(function(){
                    submitForm = function(){
                    bootbox.confirm('Anda yakin data akan disimpan?', function(result) {
                    if(result){

                        if($('#formCompanySave').valid()){
                            $.ajax({
                                type:'POST',
                                data:jQuery('#formCompanySave').serialize(),
                                url:'${request.getContextPath()}/company/save',
                                success:function(data,textStatus){
                                    jQuery('#company-form').html(data);reloadCompanyTable();
                                },
                                error:function(XMLHttpRequest,textStatus,errorThrown){}});
                        }
                        }});
                    }

                    $( "#formCompanySave" ).validate({
                        rules: {
                            emailPIC: {
                                email: true
                            }
                        }
                    });
                })
            </g:javascript>
            <form id="formCompanySave" class="form-horizontal" action="${request.getContextPath()}/company/save" method="post" onsubmit="submitForm();return false;">
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				
				
				<fieldset class="buttons controls">
					<a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout();"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
					<g:submitButton class="btn btn-primary create" name="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
				</fieldset>
             </form>
			%{--</g:formRemote>--}%
		</div>
	</body>
</html>
