<%@ page import="com.kombos.maintable.Company" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'company.label', default: 'Company')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="edit-company" class="content scaffold-edit" role="main">
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${companyInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${companyInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
            <g:javascript>
                var submitForm;
                $(function(){
                    submitForm = function(){
                    bootbox.confirm('Anda yakin data akan disimpan?', function(result) {
                    if(result){

                        if($('#formCompanyEdit').valid()){
                            $.ajax({
                                type:'POST',
                                data:jQuery('#formCompanyEdit').serialize(),
                                url:'${request.getContextPath()}/company/update',
                                success:function(data,textStatus){
                                    jQuery('#company-form').html(data);reloadCompanyTable();
                                },
                                error:function(XMLHttpRequest,textStatus,errorThrown){}});
                        }
                    }});
                    }

                    $( "#formCompanyEdit" ).validate({
                        rules: {
                            emailPIC: {
                                email: true
                            }
                        }
                    });
                })
            </g:javascript>
			%{--<g:formRemote before="if( ValidateEmail(this)){" after="};" class="form-horizontal" name="edit" on404="alert('not found!');" onSuccess="reloadCompanyTable();" update="company-form"--}%
				%{--url="[controller: 'company', action:'update']"  >--}%
            <form id="formCompanyEdit" class="form-horizontal" action="${request.getContextPath()}/company/update" method="post" onsubmit="submitForm();;return false;">
				<g:hiddenField name="id" value="${companyInstance?.id}" />
				<g:hiddenField name="version" value="${companyInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
                <fieldset class="buttons controls">
                    <g:actionSubmit class="btn btn-primary save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                    <a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout();"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
                </fieldset>
			%{--</g:formRemote>--}%
                </form>
		</div>
	</body>
</html>
