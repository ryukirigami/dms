
<%@ page import="com.kombos.maintable.Company" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'company.label', default: 'Customer Company')}" />
		<title>Customer Company</title>
		<r:require modules="baseapplayout" />
		<g:javascript>
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	var showSPK;
	$(function(){ 
	showSPK = function(){
		if (mainInterval)
			window.clearInterval(mainInterval);
		$('#main-content').empty();
		$('#spinner').fadeIn(1);
		$.ajax({url: "${request.getContextPath()}/SPK",type: "POST",dataType: "html",
				complete : function (req, err) {
					$('#main-content').append(req.responseText);
					 $('#spinner').fadeOut();
				}
			});
	}
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.pull-right .box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/company/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/company/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#company-form').empty();
    	$('#company-form').append(data);
   	}
    
    shrinkTableLayout = function(){
        $("#company-table").hide(); 
        $("#company-form").css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
   		$("#company-table").show(); 
   		$("#company-form").css("display","none");
   		reloadCompanyTable();
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#company-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/company/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadCompanyTable();
    		}
		});
		
   	}

});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left">Customer Company</span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="company-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
		</div>
		<div class="span12" id="company-form" style="display: none;margin-left: 0;"></div>
	</div>
</body>
</html>
