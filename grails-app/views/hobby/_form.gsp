<%@ page import="com.kombos.customerprofile.Hobby" %>


<g:if test="${hobbyInstance?.m063ID}">
<div class="control-group fieldcontain ${hasErrors(bean: hobbyInstance, field: 'm063ID', 'error')} required">
	<label class="control-label" for="m063ID">
		<g:message code="hobby.m063ID.label" default="Kode Hobby" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	${hobbyInstance?.m063ID}
	</div>
</div>
</g:if>

<div class="control-group fieldcontain ${hasErrors(bean: hobbyInstance, field: 'm063NamaHobby', 'error')} required">
	<label class="control-label" for="m063NamaHobby">
		<g:message code="hobby.m063NamaHobby.label" default="Hobby" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m063NamaHobby" maxlength="20" required="" value="${hobbyInstance?.m063NamaHobby}"/>
	</div>
</div>
<g:javascript>
    document.getElementById("m063NamaHobby").focus();
</g:javascript>