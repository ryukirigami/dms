<%--
  Created by IntelliJ IDEA.
  User: DENDYSWARAN
  Date: 10/17/14
  Time: 8:30 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title>Vehicle Tracking Status Board</title>
    <r:require modules="baseapplayout, baseapplist" />
    <style>
        #boardTable tbody tr td div {
            text-align: center;
        }
        #boardTable tbody tr td:first-child {
            font-weight: bolder !important;
            font-size: 15px;
        }
    </style>
</head>

<body>

<g:render template="../menu/maxLineDisplay"/>

<div class="box">
    <table id="boardTable"
           cellpadding="0" cellspacing="0"
           border="0"
           class="display table table-striped table-bordered table-hover table-selectable"
           width="100%">
        <thead>
            <tr>
                <th rowspan="2">No Polisi</th>
                <th colspan="9" style="text-align: center;">
                    Status Kendaraan
                </th>
                <th rowspan="2">Janji Penyerahan</th>
            </tr>
            <tr>
                <th>Tunggu Antrian</th>
                <th>Receive</th>
                <th>Tunggu Service</th>
                <th>Proses Service</th>
                <th>Final Inspection</th>
                <th>Service Plus</th>
                <th>Dokumen</th>
                <th>Call Customer</th>
                <th>Finish</th>
            </tr>
        </thead>
    </table>
</div>

<g:javascript>
var boardTable;
var reloadBoardTable;
var indMulai = 0;
$(function(){

	boardTable = $('#boardTable').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "GET",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "boardDataTable")}",
		"aoColumns": [

{
	"sName": "noPolisi",
	"mDataProp": "noPolisi",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": false,
	"bVisible": true
}

,

{
	"sName": "tungguAntrian",
	"mDataProp": "tungguAntrian",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"bVisible": true,
	"mRender": function ( data, type, row ) {
	    if (data=='0')
		    return "<div id='tungguAntrian_"+row["id"]+"'><img src='${request.getContextPath()}/images/board/"+row["car"]+"'></div>";
		else
		    return "";
	}
}

,

{
	"sName": "receive",
	"mDataProp": "receive",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"bVisible": true	                ,
	"mRender": function ( data, type, row ) {
        if (data == '0')
		    return "<div id='receive_"+row["id"]+"'><img src='${request.getContextPath()}/images/board/"+row["car"]+"'></div>";
		else
		    return "";
	}
}

,

{
	"sName": "tungguService",
	"mDataProp": "tungguService",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"bVisible": true,
	"mRender": function ( data, type, row ) {
	    if (data=='0') {
	        return "<div id='tungguService_"+row["id"]+"'><img src='${request.getContextPath()}/images/board/"+row["car"]+"'></div>";
	    } else {
	        return "";
	    }
	}
}

,

{
	"sName": "prosesService",
	"mDataProp": "prosesService",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": false,
	"bVisible": true,
	"mRender": function ( data, type, row ) {
	    if (data=='0') {
	        return "<div id='prosesService_"+row["id"]+"'><img src='${request.getContextPath()}/images/board/"+row["car"]+"'></div>";
	    } else {
	        return "";
	    }
	}
}

,

{
	"sName": "finalInspection",
	"mDataProp": "finalInspection",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": false,
	"bVisible": true,
	"mRender": function ( data, type, row ) {
	    if (data=='0') {
	        return "<div id='finalInspection_"+row["id"]+"'><img src='${request.getContextPath()}/images/board/"+row["car"]+"'></div>";
	    } else {
	        return "";
	    }
	}
}

,

{
	"sName": "servicePlus",
	"mDataProp": "servicePlus",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": false,
	"bVisible": true,
	"mRender": function ( data, type, row ) {
	    if (data=='0') {
	        return "<div id='servicePlus_"+row["id"]+"'><img src='${request.getContextPath()}/images/board/"+row["car"]+"'></div>";
	    } else {
	        return "";
	    }
	}
}

,

{
	"sName": "dokumen",
	"mDataProp": "dokumen",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": false,
	"bVisible": true,
	"mRender": function ( data, type, row ) {
	    if (data=='0') {
	        return "<div id='dokumen_"+row["id"]+"'><img src='${request.getContextPath()}/images/board/"+row["car"]+"'></div>";
	    } else {
	        return "";
	    }
	}
}

,

{
	"sName": "callCustomer",
	"mDataProp": "callCustomer",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": false,
	"bVisible": true,
	"mRender": function ( data, type, row ) {
	    if (data=='0') {
	        return "<div id='dokumen_"+row["id"]+"'><img src='${request.getContextPath()}/images/board/"+row["car"]+"'></div>";
	    } else {
	        return "";
	    }
	}
}

,

{
	"sName": "finish",
	"mDataProp": "finish",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": false,
	"bVisible": true,
	"mRender": function ( data, type, row ) {
	    if (data=='0') {
	        return "<div id='dokumen_"+row["id"]+"'><img src='${request.getContextPath()}/images/board/"+row["car"]+"'></div>";
	    } else {
	        return "";
	    }
	}
},

{
	"sName": "janjiPenyerahan",
	"mDataProp": "janjiPenyerahan",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	                    aoData.push(
                                {"name": 'indMulai', "value": indMulai}
                        );
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
							    indMulai = json.indMulai;
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});

    var timer = setInterval(function() {
        boardTable.fnDraw();
    }, 60000);

});
</g:javascript>
</body>
</html>