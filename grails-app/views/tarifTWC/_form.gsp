<%@ page import="com.kombos.administrasi.TarifTWC;com.kombos.administrasi.CompanyDealer;com.kombos.administrasi.KategoriKendaraan" %>
<script type="text/javascript">
    jQuery(function($) {
        $('.auto').autoNumeric('init');
    });

	document.getElementById('companyDealer').value = ${session?.userCompanyDealer?.id};
</script>


<div class="control-group fieldcontain ${hasErrors(bean: tarifTWCInstance, field: 'companyDealer', 'error')} required">
	<label class="control-label" for="companyDealer">
		<g:message code="tarifTWC.companyDealer.label" default="Company Dealer" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:select id="companyDealer" name="companyDealer.id" from="${CompanyDealer.createCriteria().list{eq("staDel", "0");order("m011NamaWorkshop","asc")}}" optionKey="id" required="" value="${tarifTWCInstance?.companyDealer?.id}" class="many-to-one"/>--}%
	<g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
		<g:select name="companyDealer.id" id="companyDealer" from="${CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${tarifTWCInstance?.companyDealer?.id}" />
	</g:if>
	<g:else>
		<g:select name="companyDealer.id" id="companyDealer" readonly="" from="${CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${tarifTWCInstance?.companyDealer?.id}" />
	</g:else>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: tarifTWCInstance, field: 'kategoriKendaraan', 'error')} required">
	<label class="control-label" for="kategoriKendaraan">
		<g:message code="tarifTWC.kategoriKendaraan.label" default="Kategori Kendaraan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="kategoriKendaraan" name="kategoriKendaraan.id" from="${KategoriKendaraan.createCriteria().list{eq("staDel", "0");order("m101NamaKategori","asc")}}" optionKey="id" required="" value="${tarifTWCInstance?.kategoriKendaraan?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: tarifTWCInstance, field: 't113aTMT', 'error')} required">
	<label class="control-label" for="t113aTMT">
		<g:message code="tarifTWC.t113aTMT.label" default="Tanggal Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<ba:datePicker name="t113aTMT" precision="day"  value="${tarifTWCInstance?.t113aTMT}" format="dd-MM-yyyy" required="true" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: tarifTWCInstance, field: 't113aTarifPerjamTWC', 'error')} required">
	<label class="control-label" for="t113aTarifPerjamTWC">
		<g:message code="tarifTWC.t113aTarifPerjamTWC.label" default="Tarif Per Jam TWC" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField class="auto" name="t113aTarifPerjamTWC" value="${fieldValue(bean: tarifTWCInstance, field: 't113aTarifPerjamTWC')}" required=""/>
	</div>
</div>
%{--
<div class="control-group fieldcontain ${hasErrors(bean: tarifTWCInstance, field: 'staDel', 'error')} ">
	<label class="control-label" for="staDel">
		<g:message code="tarifTWC.staDel.label" default="Sta Del" />
		
	</label>
	<div class="controls">
	<g:textField name="staDel" maxlength="1" value="${tarifTWCInstance?.staDel}"/>
	</div>
</div>
--}%
<g:javascript>
    document.getElementById("companyDealer").focus();
</g:javascript>