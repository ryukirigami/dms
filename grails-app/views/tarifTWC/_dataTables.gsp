
<%@ page import="com.kombos.administrasi.TarifTWC" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<script type="text/javascript">
    jQuery(function($) {
        $('.cek').autoNumeric('init');
    });
</script>

<table id="tarifTWC_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="tarifTWC.companyDealer.label" default="Company Dealer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="tarifTWC.kategoriKendaraan.label" default="Kategori Kendaraan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="tarifTWC.t113aTMT.label" default="Tanggal Berlaku" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="tarifTWC.t113aTarifPerjamTWC.label" default="Tarif Per Jam TWC" /></div>
			</th>

%{--
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="tarifTWC.staDel.label" default="Sta Del" /></div>
			</th>
--}%
		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_companyDealer" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_kategoriKendaraan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_kategoriKendaraan" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t113aTMT" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_t113aTMT" value="date.struct">
					<input type="hidden" name="search_t113aTMT_day" id="search_t113aTMT_day" value="">
					<input type="hidden" name="search_t113aTMT_month" id="search_t113aTMT_month" value="">
					<input type="hidden" name="search_t113aTMT_year" id="search_t113aTMT_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t113aTMT_dp" value="" id="search_t113aTMT" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t113aTarifPerjamTWC" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t113aTarifPerjamTWC" class="search_init cek" />
				</div>
			</th>
%{--	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_staDel" class="search_init" />
				</div>
			</th>
--}%

		</tr>
	</thead>
</table>

<g:javascript>
var TarifTWCTable;
var reloadTarifTWCTable;
$(function(){

	reloadTarifTWCTable = function() {
		TarifTWCTable.fnDraw();
	}

	
	$('#search_t113aTMT').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t113aTMT_day').val(newDate.getDate());
			$('#search_t113aTMT_month').val(newDate.getMonth()+1);
			$('#search_t113aTMT_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			TarifTWCTable.fnDraw();	
	});

	var recordsTarifTwcperpage = [];
    var anTarifTwcSelected;
    var jmlRecTarifTwcPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	TarifTWCTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	TarifTWCTable = $('#tarifTWC_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            //alert( $('li.active').text() );
            var rsTarifTwc = $("#tarifTWC_datatables tbody .row-select");
            var jmlTarifTwcCek = 0;
            var idRec;
            var nRow;
            rsTarifTwc.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsTarifTwcperpage[idRec]=="1"){
                    jmlTarifTwcCek = jmlTarifTwcCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsTarifTwcperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecTarifTwcPerPage = rsTarifTwc.length;
            if(jmlTarifTwcCek==jmlRecTarifTwcPerPage && jmlRecTarifTwcPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "companyDealer",
	"mDataProp": "companyDealer",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "kategoriKendaraan",
	"mDataProp": "kategoriKendaraan",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t113aTMT",
	"mDataProp": "t113aTMT",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t113aTarifPerjamTWC",
	"mDataProp": "t113aTarifPerjamTWC",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "staDel",
	"mDataProp": "staDel",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}
	
						var kategoriKendaraan = $('#filter_kategoriKendaraan input').val();
						if(kategoriKendaraan){
							aoData.push(
									{"name": 'sCriteria_kategoriKendaraan', "value": kategoriKendaraan}
							);
						}

						var t113aTMT = $('#search_t113aTMT').val();
						var t113aTMTDay = $('#search_t113aTMT_day').val();
						var t113aTMTMonth = $('#search_t113aTMT_month').val();
						var t113aTMTYear = $('#search_t113aTMT_year').val();
						
						if(t113aTMT){
							aoData.push(
									{"name": 'sCriteria_t113aTMT', "value": "date.struct"},
									{"name": 'sCriteria_t113aTMT_dp', "value": t113aTMT},
									{"name": 'sCriteria_t113aTMT_day', "value": t113aTMTDay},
									{"name": 'sCriteria_t113aTMT_month', "value": t113aTMTMonth},
									{"name": 'sCriteria_t113aTMT_year', "value": t113aTMTYear}
							);
						}
	
						var t113aTarifPerjamTWC = $('#filter_t113aTarifPerjamTWC input').val();
						if(t113aTarifPerjamTWC){
							aoData.push(
									{"name": 'sCriteria_t113aTarifPerjamTWC', "value": t113aTarifPerjamTWC}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

    $('.select-all').click(function(e) {
        $("#tarifTWC_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsTarifTwcperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsTarifTwcperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

	$('#tarifTWC_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsTarifTwcperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anTarifTwcSelected = TarifTWCTable.$('tr.row_selected');
            if(jmlRecTarifTwcPerPage == anTarifTwcSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsTarifTwcperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });


});
</g:javascript>


			
