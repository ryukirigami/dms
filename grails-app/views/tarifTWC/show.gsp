

<%@ page import="com.kombos.administrasi.TarifTWC" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'tarifTWC.label', default: 'Tarif Per Jam TWC')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteTarifTWC;

$(function(){ 
	deleteTarifTWC=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/tarifTWC/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadTarifTWCTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-tarifTWC" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="tarifTWC"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${tarifTWCInstance?.companyDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="companyDealer-label" class="property-label"><g:message
					code="tarifTWC.companyDealer.label" default="Company Dealer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="companyDealer-label">
						%{--<ba:editableValue
								bean="${tarifTWCInstance}" field="companyDealer"
								url="${request.contextPath}/TarifTWC/updatefield" type="text"
								title="Enter companyDealer" onsuccess="reloadTarifTWCTable();" />--}%
							
								${tarifTWCInstance?.companyDealer?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tarifTWCInstance?.kategoriKendaraan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kategoriKendaraan-label" class="property-label"><g:message
					code="tarifTWC.kategoriKendaraan.label" default="Kategori Kendaraan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kategoriKendaraan-label">
						%{--<ba:editableValue
								bean="${tarifTWCInstance}" field="kategoriKendaraan"
								url="${request.contextPath}/TarifTWC/updatefield" type="text"
								title="Enter kategoriKendaraan" onsuccess="reloadTarifTWCTable();" />--}%
							
								${tarifTWCInstance?.kategoriKendaraan?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tarifTWCInstance?.t113aTMT}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t113aTMT-label" class="property-label"><g:message
					code="tarifTWC.t113aTMT.label" default="Tanggal Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t113aTMT-label">
						%{--<ba:editableValue
								bean="${tarifTWCInstance}" field="t113aTMT"
								url="${request.contextPath}/TarifTWC/updatefield" type="text"
								title="Enter t113aTMT" onsuccess="reloadTarifTWCTable();" />--}%
							
								<g:formatDate date="${tarifTWCInstance?.t113aTMT}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tarifTWCInstance?.t113aTarifPerjamTWC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t113aTarifPerjamTWC-label" class="property-label"><g:message
					code="tarifTWC.t113aTarifPerjamTWC.label" default="Tarif Per Jam TWC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t113aTarifPerjamTWC-label">
						%{--<ba:editableValue
								bean="${tarifTWCInstance}" field="t113aTarifPerjamTWC"
								url="${request.contextPath}/TarifTWC/updatefield" type="text"
								title="Enter t113aTarifPerjamTWC" onsuccess="reloadTarifTWCTable();" />--}%
							
								<g:fieldValue bean="${tarifTWCInstance}" field="t113aTarifPerjamTWC"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${tarifTWCInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="tarifTWC.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${tarifTWCInstance}" field="dateCreated"
                                url="${request.contextPath}/tarifTWC/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadtarifTWCTable();" />--}%

                        <g:formatDate date="${tarifTWCInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${tarifTWCInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="tarifTWC.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${tarifTWCInstance}" field="createdBy"
                                url="${request.contextPath}/tarifTWC/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadtarifTWCTable();" />--}%

                        <g:fieldValue bean="${tarifTWCInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${tarifTWCInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="tarifTWC.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${tarifTWCInstance}" field="lastUpdated"
                                url="${request.contextPath}/tarifTWC/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadtarifTWCTable();" />--}%

                        <g:formatDate date="${tarifTWCInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${tarifTWCInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="tarifTWC.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${tarifTWCInstance}" field="updatedBy"
                                url="${request.contextPath}/tarifTWC/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadtarifTWCTable();" />--}%

                        <g:fieldValue bean="${tarifTWCInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${tarifTWCInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="tarifTWC.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${tarifTWCInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/tarifTWC/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadtarifTWCTable();" />--}%

                        <g:fieldValue bean="${tarifTWCInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
<!--			
				<g:if test="${tarifTWCInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="tarifTWC.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${tarifTWCInstance}" field="staDel"
								url="${request.contextPath}/TarifTWC/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadTarifTWCTable();" />--}%
							
								<g:fieldValue bean="${tarifTWCInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
-->			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${tarifTWCInstance?.id}"
					update="[success:'tarifTWC-form',failure:'tarifTWC-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteTarifTWC('${tarifTWCInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
