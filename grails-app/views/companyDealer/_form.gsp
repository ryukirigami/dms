<%@ page import="com.kombos.administrasi.Bank; com.kombos.administrasi.Kelurahan; com.kombos.administrasi.Kecamatan; com.kombos.administrasi.KabKota; com.kombos.administrasi.Provinsi; com.kombos.administrasi.JenisDealer; com.kombos.administrasi.CompanyDealer" %>
<g:javascript>
    var selectKabupaten;
    var selectKecamatan;
    var selectKelurahan;

    $(function(){
        selectKabupaten = function () {
                var idProp = $('#provinsi').val();
                if(idProp != null){
                    jQuery.getJSON('${request.contextPath}/companyDealer/getKabupatens?idProp='+idProp, function (data) {
                            $('#kabupaten').empty();
                            if (data) {
                                jQuery.each(data, function (index, value) {
                                    $('#kabupaten').append("<option value='" + value.id + "'>" + value.namaKabupaten + "</option>");
                                });
                            }
                        selectKecamatan();
                        });
                }else{
                     $('#kabupaten').empty();
                     $('#kecamatan').empty();
                     $('#kelurahan').empty();
                }
        };

        selectKecamatan = function() {
                var idKab = $('#kabupaten').val();
              if(idKab!=null){
                  jQuery.getJSON('${request.contextPath}/companyDealer/getKecamatans?idKab='+idKab, function (data) {
                    if (data) {
                        $('#kecamatan').empty();
                        jQuery.each(data, function (index, value) {
                            $('#kecamatan').append("<option value='" + value.id + "'>" + value.namaKecamatan + "</option>");
                        });
                    }
                selectKelurahan();
                });
              }else{
                     $('#kecamatan').empty();
                     $('#kelurahan').empty();
                }
        };

   	    selectKelurahan = function() {
   	            var idKec = $('#kecamatan').val();
                if(idKec != null){
                    jQuery.getJSON('${request.contextPath}/companyDealer/getKelurahans?idKec='+idKec, function (data) {
                    if (data) {
                        $('#kelurahan').empty();

                        jQuery.each(data, function (index, value) {
                                $('#kelurahan').append("<option value='" + value.id + "'>" + value.namaKelurahan + "</option>");
                            });
                        }
                    });
                }else{
                     $('#kelurahan').empty();
                }
        };
    });

    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode != 40  && charCode != 41  && charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    $(function(){
			$('#m011NomorAkunTransfer').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/companyDealer/listAccount', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
			$('#m011NomorAkunLainnya').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/companyDealer/listAccount', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});

    function detailAccount(){
                var noAccount = $('#m011NomorAkunTransfer').val();
                $.ajax({
                    url:'${request.contextPath}/companyDealer/detailAccount?noAccount='+noAccount,
                    type: "POST", // Always use POST when deleting data
                    success : function(data){
                        $('#idM011NomorAkunTransfer').val("");
                        if(data.hasil=="ada"){
                            $('#idM011NomorAkunTransfer').val(data.id);
                            console.log("idM011NomorAkunTransfer : " + data.id)
                        }
                    }
                })
        }
    function detailAccount2(){
                var noAccount = $('#m011NomorAkunLainnya').val();
                $.ajax({
                    url:'${request.contextPath}/companyDealer/detailAccount?noAccount='+noAccount,
                    type: "POST", // Always use POST when deleting data
                    success : function(data){
                        $('#idM011NomorAkunLainnya').val("");
                        if(data.hasil=="ada"){
                            $('#idM011NomorAkunLainnya').val(data.id);
                            console.log("idM011NomorAkunLainnya : " + data.id)
                        }
                    }
                })
        }
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'm011ID', 'error')} required">
	<label class="control-label" for="m011NamaWorkshop">
		<g:message code="companyDealer.m011ID.label" default="ID" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
    <g:hiddenField name="id" value="${companyDealerInstance?.id}"/>
	<g:textField  id="m011ID" name="m011ID" maxlength="3" required="" value="${companyDealerInstance?.m011ID}"/>
	</div>
</div>
<g:javascript>
    document.getElementById("m011ID").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'm011NamaWorkshop', 'error')} required">
    <label class="control-label" for="m011NamaWorkshop">
        <g:message code="companyDealer.m011NamaWorkshop.label" default="Nama" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField  id="m011NamaWorkshop" name="m011NamaWorkshop" maxlength="25" required="" value="${companyDealerInstance?.m011NamaWorkshop}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'jenisDealer', 'error')} required">
	<label class="control-label" for="jenisDealer">
		<g:message code="companyDealer.jenisDealer.label" default="Jenis Dealer" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="jenisDealer" name="jenisDealer.id" from="${JenisDealer.list()}" optionKey="id" required="" value="${companyDealerInstance?.jenisDealer?.id}" class="many-to-one"/>
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'm011Alamat', 'error')} required">
	<label class="control-label" for="m011Alamat">
		<g:message code="companyDealer.m011Alamat.label" default="Alamat" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textArea rows="2" cols="50"  name="m011Alamat" maxlength="100" required="" value="${companyDealerInstance?.m011Alamat}"/>
	</div>
</div>
<div class="control-group required">
	<label class="control-label" for="provinsi">
		<g:message code="companyDealer.provinsi.label" default="Provinsi" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="provinsi" name="provinsi.id" optionKey="id" required="" from="${Provinsi.createCriteria().list(){eq("staDel", "0");order("m001NamaProvinsi", "asc")}}"  value="${companyDealerInstance?.provinsi?.id}"  noSelection="['':'Pilih Provinsi']" onchange="selectKabupaten();" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'kabKota', 'error')} required">
	<label class="control-label" for="kabKota">
		<g:message code="companyDealer.kabKota.label" default="Kabupaten" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:select id="kabupaten" name="kabupaten.id" optionKey="id" required=""  from="${KabKota.createCriteria().list(){eq("staDel", "0");order("m002NamaKabKota", "asc")}}"  value="${companyDealerInstance?.kabKota?.id}"  noSelection="['':'Pilih KabKota']" onchange="selectKecamatan();" class="many-to-one"/>--}%
        <select id="kabupaten" name="kabupaten.id" onchange="selectKecamatan();">
            <g:if test="${companyDealerInstance?.kabKota?.id}">
                <option value="${companyDealerInstance?.kabKota?.id}">${companyDealerInstance?.kabKota?.m002NamaKabKota}</option>
            </g:if>
        </select>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'kecamatan', 'error')} required">
	<label class="control-label" for="kecamatan">
		<g:message code="companyDealer.kecamatan.label" default="Kecamatan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:select id="kecamatan" name="kecamatan.id" required="" optionKey="id" from="${Kecamatan.createCriteria().list(){eq("staDel", "0");order("m003NamaKecamatan", "asc")}}"  value="${companyDealerInstance?.kecamatan?.id}" noSelection="['':'Pilih Kecamatan']" onchange="selectKelurahan();" class="many-to-one"/>--}%
        <select id="kecamatan" name="kecamatan.id" onchange="selectKelurahan();">
            <g:if test="${companyDealerInstance?.kecamatan?.id}">
                <option value="${companyDealerInstance?.kecamatan?.id}">${companyDealerInstance?.kecamatan?.m003NamaKecamatan}</option>
            </g:if>
        </select>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'kelurahan', 'error')} required">
	<label class="control-label" for="kelurahan">
		<g:message code="companyDealer.kelurahan.label" default="Kelurahan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:select id="kelurahan" name="kelurahan.id" class="many-to-one" optionKey="id" from="${Kelurahan.createCriteria().list(){eq("staDel", "0");order("m004NamaKelurahan", "asc")}}"  noSelection="['':'Pilih Kelurahan']" value="${companyDealerInstance?.kelurahan?.id}" />--}%
        <select id="kelurahan" name="kelurahan.id">
            <g:if test="${companyDealerInstance?.kelurahan?.id}">
                <option value="${companyDealerInstance?.kelurahan?.id}">${companyDealerInstance?.kelurahan?.m004NamaKelurahan}</option>
            </g:if>
        </select>
	</div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'm011Telp', 'error')}">
	<label class="control-label" for="m011Telp">
		<g:message code="companyDealer.m011Telp.label" default="Telp" />

	</label>
	<div class="controls">
	<g:textField name="m011Telp" maxlength="20"  value="${companyDealerInstance?.m011Telp}" onkeypress="return isNumberKey(event)"/> <br/>Ext.
    <g:textField name="m011TelpExt" maxlength="4"  value="${companyDealerInstance?.m011TelpExt}" style="width: 30pt" onkeypress="return isNumberKey(event)"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'm011Fax', 'error')} ">
    <label class="control-label" for="m011Fax">
        <g:message code="companyDealer.m011Fax.label" default="Fax" />

    </label>
    <div class="controls">
        <g:textField name="m011Fax" maxlength="20" value="${companyDealerInstance?.m011Fax}" onkeypress="return isNumberKey(event)"/><br/>Ext.
        <g:textField name="m011FaxExt" maxlength="4" value="${companyDealerInstance?.m011FaxExt}" style="width: 30pt" onkeypress="return isNumberKey(event)"/>
    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'm011Email', 'error')}">
	<label class="control-label" for="m011Email">
		<g:message code="companyDealer.m011Email.label" default="Email" />

	</label>
	<div class="controls">
	<g:field name="m011Email" type="email" maxlength="50" value="${companyDealerInstance?.m011Email}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'm011NPWP', 'error')} ">
    <label class="control-label" for="m011NPWP">
        <g:message code="companyDealer.m011NPWP.label" default="No. NPWP" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="m011NPWP" maxlength="25" required="" value="${companyDealerInstance?.m011NPWP}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'm011NamaNpwp', 'error')} ">
    <label class="control-label" for="m011NamaNpwp">
        <g:message code="companyDealer.m011NamaNpwp.label" default="Nama NPWP" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="m011NamaNpwp" maxlength="50" required="" value="${companyDealerInstance?.m011NamaNpwp}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'm011AlamatNpwp', 'error')} ">
    <label class="control-label" for="m011AlamatNpwp">
        <g:message code="companyDealer.m011AlamatNpwp.label" default="Alamat NPWP" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textArea required="" rows="2" cols="50" maxlength="255" id="m011AlamatNpwp" name="m011AlamatNpwp" value="${companyDealerInstance?.m011AlamatNpwp}" class="many-to-one"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'm011NomorRekening', 'error')}">
	<label class="control-label" for="m011NomorRekening">
		<g:message code="companyDealer.m011NomorRekening.label" default="Nomor Rekening" />

	</label>
	<div class="controls">
	<g:textField name="m011NomorRekening" maxlength="50"  value="${companyDealerInstance?.m011NomorRekening}"/>
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'bank', 'error')}">
	<label class="control-label" for="bank">
		<g:message code="companyDealer.bank.label" default="Bank" />

	</label>
	<div class="controls">
	<g:select id="bank" name="bank.id" from="${Bank.list()}" optionKey="id" value="${companyDealerInstance?.bank?.id}" class="many-to-one" onselect="   showAlamatBank(this.value)" onchange="showAlamatBank(this.value)"/>
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'bank', 'error')}">
    <label class="control-label" for="bank">
        <g:message code="companyDealer.alamatbank.label" default="Alamat Bank" />

    </label>
    <div class="controls">
        %{--<g:textField readonly="true" id="alamatBank" name="alamatBank"   value="" class="many-to-one"/>--}%
        <g:textArea rows="2" cols="50" readonly="true" id="alamatBank" name="alamatBank" value="" class="many-to-one"/>
    </div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'm011AtasNama', 'error')}">
	<label class="control-label" for="m011AtasNama">
		<g:message code="companyDealer.m011AtasNama.label" default="Atas Nama" />

	</label>
	<div class="controls">
	<g:textField name="m011AtasNama" maxlength="50" value="${companyDealerInstance?.m011AtasNama}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'm011Ket', 'error')} ">
	<label class="control-label" for="m011Ket">
		<g:message code="companyDealer.m011Ket.label" default="Keterangan" />

	</label>
	<div class="controls">
        <g:textArea rows="2" cols="50"  name="m011Ket" maxlength="20" value="${companyDealerInstance?.m011Ket}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'm011StaBPCenter', 'error')} ">
	<label class="control-label" for="m011StaBPCenter">
		<g:message code="companyDealer.m011StaBPCenter.label" default="BP Center" />
		
	</label>
	<div class="controls">
	<g:radioGroup name="m011StaBPCenter" values="['1','0']" labels="['Ya','Tidak']" value="${companyDealerInstance?.m011StaBPCenter}" >
        ${it.radio} ${it.label}
    </g:radioGroup>
       </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'm011PKP', 'error')} ">
	<label class="control-label" for="m011PKP">
		<g:message code="companyDealer.m011PKP.label" default="Pengusaha Kena Pajak" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:textArea rows="2" cols="50"  name="m011PKP" value="${companyDealerInstance?.m011PKP}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'm011EmailComplainSerius', 'error')}">
	<label class="control-label" for="m011EmailComplainSerius">
		<g:message code="companyDealer.m011EmailComplainSerius.label" default="Email Complain Serius" />

	</label>
	<div class="controls">
        <g:textArea rows="2" cols="50"  name="m011EmailComplainSerius" maxlength="50" value="${companyDealerInstance?.m011EmailComplainSerius}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'm011EmailComplainTdkSerius', 'error')}">
	<label class="control-label" for="m011EmailComplainTdkSerius">
		<g:message code="companyDealer.m011EmailComplainTdkSerius.label" default="Email Complain Tdk Serius" />

	</label>
	<div class="controls">
        <g:textArea rows="2" cols="50"  name="m011EmailComplainTdkSerius" maxlength="50" value="${companyDealerInstance?.m011EmailComplainTdkSerius}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'm011Logo', 'error')} ">
	<label class="control-label" for="m011Logo">
		<g:message code="companyDealer.m011Logo.label" default="Logo" />
		
	</label>
	<div class="controls">
        <g:if test="${companyDealerInstance?.m011Logo}">

            <img height="70pt" width="70pt" class="" src="${createLink(controller:'companyDealer', action:'showLogo', id : companyDealerInstance.id, random : logostamp)}" />

        </g:if>
	<input type="file" id="m011Logo" name="m011Logo" />
	</div>


</div>
<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'm011NomorAkunTransfer', 'error')} ">
    <label class="control-label" for="m011NomorAkunTransfer">
        <g:message code="franc.m011NomorAkunTransfer.label" default="Nomor Akun Transfer" />

    </label>
    <div class="controls">
      <g:textField name="nomorAkunTransfer" id="m011NomorAkunTransfer" class="typeahead" value="${companyDealerInstance?.m011NomorAkunTransfer?.accountNumber?companyDealerInstance?.m011NomorAkunTransfer?.accountNumber+' || '+companyDealerInstance?.m011NomorAkunTransfer?.accountName:""}" autocomplete="off" onblur="detailAccount();"/>
        <g:hiddenField name="m011NomorAkunTransfer.id" id="idM011NomorAkunTransfer"  value="${companyDealerInstance?.m011NomorAkunTransfer?.id}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'm011NomorAkunTransfer', 'error')} ">
    <label class="control-label" for="m011NomorAkunLainnya">
        <g:message code="franc.m011NomorAkunLainnya.label" default="Nomor Akun Lainnya" />

    </label>
    <div class="controls">
        <g:textField name="nomorAkunLainnya" id="m011NomorAkunLainnya" class="typeahead" value="${companyDealerInstance?.m011NomorAkunLainnya?.accountNumber?companyDealerInstance?.m011NomorAkunLainnya?.accountNumber+' || '+companyDealerInstance?.m011NomorAkunLainnya?.accountName:""}" autocomplete="off" onblur="detailAccount2();"/>
        <g:hiddenField name="m011NomorAkunLainnya.id" id="idM011NomorAkunLainnya"  value="${companyDealerInstance?.m011NomorAkunLainnya?.id}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'm011OutletCode', 'error')} ">
    <label class="control-label" for="m011OutletCode">
        <g:message code="franc.m011NomorAkunLainnya.label" default="Outlet Code" />

    </label>
    <div class="controls">
        <g:textField name="m011OutletCode" id="m011OutletCode"  value="${companyDealerInstance?.m011OutletCode}" />
    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: companyDealerInstance, field: 'm011SelisihWaktu', 'error')} ">
    <label class="control-label" for="m011SelisihWaktu">
        <g:message code="franc.m011SelisihWaktu.label" default="Selisih Waktu (Jam)" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="m011SelisihWaktu" id="m011SelisihWaktu" required="" value="${companyDealerInstance?.m011SelisihWaktu}" />
    </div>
</div>
<g:javascript>
    document.getElementById('m011ID').focus();

    $(function(){
      var idBank = $("#bank").val();
      selectKabupaten();
      showAlamatBank(idBank);


    });
</g:javascript>