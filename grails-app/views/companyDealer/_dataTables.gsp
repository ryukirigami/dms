
<%@ page import="com.kombos.administrasi.CompanyDealer" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="companyDealer_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="companyDealer.m011ID.label" default="Kode Company" /></div>
            </th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="companyDealer.m011NamaWorkshop.label" default="Nama Workshop" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="companyDealer.jenisDealer.label" default="Jenis Dealer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="companyDealer.m011Alamat.label" default="Alamat" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="companyDealer.provinsi.label" default="Provinsi" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="companyDealer.kabKota.label" default="Kab/Kota" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="companyDealer.kecamatan.label" default="Kecamatan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="companyDealer.kelurahan.label" default="Kelurahan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="companyDealer.m011Fax.label" default="Fax" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="companyDealer.m011Telp.label" default="Telp" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="companyDealer.m011Email.label" default="Email" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="companyDealer.m011NPWP.label" default="NPWP" /></div>
			</th>



			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="companyDealer.m011NomorRekening.label" default="Nomor Rekening" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="companyDealer.bank.label" default="Bank" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="companyDealer.m011AtasNama.label" default="Atas Nama" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="companyDealer.m011Ket.label" default="Ket" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="companyDealer.m011EmailComplainSerius.label" default="Email Complain Serius" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="companyDealer.m011EmailComplainTdkSerius.label" default="Email Complain Tdk Serius" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="companyDealer.m011PKP.label" default="PKP" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="companyDealer.m011StaBPCenter.label" default="BP Center" /></div>
			</th>

<!-- 
			<th style="border-bottom: none;padding: 5px; ">
				<div><g:message code="companyDealer.m011StaDel.label" default="Sta Del" /></div>
			</th>
-->	
		</tr>
		<tr>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_m011ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_m011ID" class="search_init" />
                </div>
            </th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m011NamaWorkshop" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m011NamaWorkshop" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_jenisDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_jenisDealer" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m011Alamat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m011Alamat" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_provinsi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_provinsi" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_kabKota" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_kabKota" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_kecamatan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_kecamatan" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_kelurahan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_kelurahan" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m011Fax" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m011Fax" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m011Telp" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m011Telp" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m011Email" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m011Email" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m011NPWP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m011NPWP" class="search_init" />
				</div>
			</th>
	
			
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m011NomorRekening" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m011NomorRekening" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_bank" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_bank" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m011AtasNama" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m011AtasNama" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m011Ket" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m011Ket" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m011EmailComplainSerius" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m011EmailComplainSerius" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m011EmailComplainTdkSerius" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m011EmailComplainTdkSerius" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m011PKP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m011PKP" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m011StaBPCenter" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					
                                  <select name="search_m011StaBPCenter"  id="search_m011StaBPCenter" style="width:100%" onchange="selectStaBPCenter();">
                                    <option value=""></option>
                                    <option value="1">Ya</option>
                                    <option value="0">Tidak</option>
                                    </select>
                                 </div>
			</th>
 <!--	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m011StaDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m011StaDel" class="search_init" />
				</div>
			</th>
-->

		</tr>
	</thead>
</table>

<g:javascript>
var CompanyDealerTable;
var reloadCompanyDealerTable;
var selectStaBPCenter;
$(function(){
	
	reloadCompanyDealerTable = function() {
		CompanyDealerTable.fnDraw();
	}

    selectStaBPCenter = function (){
       CompanyDealerTable.fnDraw();
    }

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	CompanyDealerTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	CompanyDealerTable = $('#companyDealer_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m011ID",
	"mDataProp": "m011ID",
	"aTargets": [1],
        "mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "m011NamaWorkshop",
	"mDataProp": "m011NamaWorkshop",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "jenisDealer",
	"mDataProp": "jenisDealer",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m011Alamat",
	"mDataProp": "m011Alamat",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "provinsi",
	"mDataProp": "provinsi",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "kabKota",
	"mDataProp": "kabKota",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "kecamatan",
	"mDataProp": "kecamatan",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "kelurahan",
	"mDataProp": "kelurahan",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m011Fax",
	"mDataProp": "m011Fax",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m011Telp",
	"mDataProp": "m011Telp",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m011Email",
	"mDataProp": "m011Email",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m011NPWP",
	"mDataProp": "m011NPWP",
	"aTargets": [11],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,



{
	"sName": "m011NomorRekening",
	"mDataProp": "m011NomorRekening",
	"aTargets": [14],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "bank",
	"mDataProp": "bank",
	"aTargets": [15],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m011AtasNama",
	"mDataProp": "m011AtasNama",
	"aTargets": [16],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m011Ket",
	"mDataProp": "m011Ket",
	"aTargets": [17],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m011EmailComplainSerius",
	"mDataProp": "m011EmailComplainSerius",
	"aTargets": [18],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m011EmailComplainTdkSerius",
	"mDataProp": "m011EmailComplainTdkSerius",
	"aTargets": [19],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m011PKP",
	"mDataProp": "m011PKP",
	"aTargets": [20],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m011StaBPCenter",
	"mDataProp": "m011StaBPCenter",
	"aTargets": [21],
        "mRender": function ( data, type, row ) {
          
            if(data=="0"){
                  return "Tidak";
             }else
             {
                  return "Ya";
             }
        
        },
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m011StaDel",
	"mDataProp": "m011StaDel",
	"aTargets": [22],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m011ID = $('#filter_m011ID input').val();
						if(m011ID){
							aoData.push(
									{"name": 'sCriteria_m011ID', "value": m011ID}
							);
						}
	
						var m011NamaWorkshop = $('#filter_m011NamaWorkshop input').val();
						if(m011NamaWorkshop){
							aoData.push(
									{"name": 'sCriteria_m011NamaWorkshop', "value": m011NamaWorkshop}
							);
						}
	
						var jenisDealer = $('#filter_jenisDealer input').val();
						if(jenisDealer){
							aoData.push(
									{"name": 'sCriteria_jenisDealer', "value": jenisDealer}
							);
						}
	
						var m011Alamat = $('#filter_m011Alamat input').val();
						if(m011Alamat){
							aoData.push(
									{"name": 'sCriteria_m011Alamat', "value": m011Alamat}
							);
						}
	
						var provinsi = $('#filter_provinsi input').val();
						if(provinsi){
							aoData.push(
									{"name": 'sCriteria_provinsi', "value": provinsi}
							);
						}
	
						var kabKota = $('#filter_kabKota input').val();
						if(kabKota){
							aoData.push(
									{"name": 'sCriteria_kabKota', "value": kabKota}
							);
						}
	
						var kecamatan = $('#filter_kecamatan input').val();
						if(kecamatan){
							aoData.push(
									{"name": 'sCriteria_kecamatan', "value": kecamatan}
							);
						}
	
						var kelurahan = $('#filter_kelurahan input').val();
						if(kelurahan){
							aoData.push(
									{"name": 'sCriteria_kelurahan', "value": kelurahan}
							);
						}
	
						var m011Fax = $('#filter_m011Fax input').val();
						if(m011Fax){
							aoData.push(
									{"name": 'sCriteria_m011Fax', "value": m011Fax}
							);
						}
	
						var m011Telp = $('#filter_m011Telp input').val();
						if(m011Telp){
							aoData.push(
									{"name": 'sCriteria_m011Telp', "value": m011Telp}
							);
						}
	
						var m011Email = $('#filter_m011Email input').val();
						if(m011Email){
							aoData.push(
									{"name": 'sCriteria_m011Email', "value": m011Email}
							);
						}
	
						var m011NPWP = $('#filter_m011NPWP input').val();
						if(m011NPWP){
							aoData.push(
									{"name": 'sCriteria_m011NPWP', "value": m011NPWP}
							);
						}
	
						var m011Logo = $('#filter_m011Logo input').val();
						if(m011Logo){
							aoData.push(
									{"name": 'sCriteria_m011Logo', "value": m011Logo}
							);
						}
	
						var imageMime = $('#filter_imageMime input').val();
						if(imageMime){
							aoData.push(
									{"name": 'sCriteria_imageMime', "value": imageMime}
							);
						}
	
						var m011NomorRekening = $('#filter_m011NomorRekening input').val();
						if(m011NomorRekening){
							aoData.push(
									{"name": 'sCriteria_m011NomorRekening', "value": m011NomorRekening}
							);
						}
	
						var bank = $('#filter_bank input').val();
						if(bank){
							aoData.push(
									{"name": 'sCriteria_bank', "value": bank}
							);
						}
	
						var m011AtasNama = $('#filter_m011AtasNama input').val();
						if(m011AtasNama){
							aoData.push(
									{"name": 'sCriteria_m011AtasNama', "value": m011AtasNama}
							);
						}
	
						var m011Ket = $('#filter_m011Ket input').val();
						if(m011Ket){
							aoData.push(
									{"name": 'sCriteria_m011Ket', "value": m011Ket}
							);
						}
	
						var m011EmailComplainSerius = $('#filter_m011EmailComplainSerius input').val();
						if(m011EmailComplainSerius){
							aoData.push(
									{"name": 'sCriteria_m011EmailComplainSerius', "value": m011EmailComplainSerius}
							);
						}
	
						var m011EmailComplainTdkSerius = $('#filter_m011EmailComplainTdkSerius input').val();
						if(m011EmailComplainTdkSerius){
							aoData.push(
									{"name": 'sCriteria_m011EmailComplainTdkSerius', "value": m011EmailComplainTdkSerius}
							);
						}
	
						var m011PKP = $('#filter_m011PKP input').val();
						if(m011PKP){
							aoData.push(
									{"name": 'sCriteria_m011PKP', "value": m011PKP}
							);
						}
	
						var m011StaBPCenter = $('#search_m011StaBPCenter').val();
						if(m011StaBPCenter){
							aoData.push(
									{"name": 'sCriteria_m011StaBPCenter', "value": m011StaBPCenter}
							);
						}
	
						var m011StaDel = $('#filter_m011StaDel input').val();
						if(m011StaDel){
							aoData.push(
									{"name": 'sCriteria_m011StaDel', "value": m011StaDel}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
