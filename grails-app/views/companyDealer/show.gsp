

<%@ page import="com.kombos.administrasi.CompanyDealer" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'companyDealer.label', default: 'Company Dealer')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteCompanyDealer;

$(function(){ 
	deleteCompanyDealer=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/companyDealer/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadCompanyDealerTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-companyDealer" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="companyDealer"
			class="table table-bordered table-hover">
			<tbody>

            <g:if test="${companyDealerInstance?.m011ID}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m011ID-label" class="property-label"><g:message
                                code="companyDealer.m011ID.label" default="ID Company Dealer" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m011ID-label">
                        %{--<ba:editableValue
                                bean="${companyDealerInstance}" field="m011NamaWorkshop"
                                url="${request.contextPath}/CompanyDealer/updatefield" type="text"
                                title="Enter m011NamaWorkshop" onsuccess="reloadCompanyDealerTable();" />--}%

                        <g:fieldValue bean="${companyDealerInstance}" field="m011ID"/>

                    </span></td>

                </tr>
            </g:if>
            <g:if test="${companyDealerInstance?.m011NamaWorkshop}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m011NamaWorkshop-label" class="property-label"><g:message
                                code="companyDealer.m011NamaWorkshop.label" default="Nama Workshop" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m011NamaWorkshop-label">
                        %{--<ba:editableValue
                                bean="${companyDealerInstance}" field="m011NamaWorkshop"
                                url="${request.contextPath}/CompanyDealer/updatefield" type="text"
                                title="Enter m011NamaWorkshop" onsuccess="reloadCompanyDealerTable();" />--}%

                        <g:fieldValue bean="${companyDealerInstance}" field="m011NamaWorkshop"/>

                    </span></td>

                </tr>
            </g:if>




				<g:if test="${companyDealerInstance?.jenisDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="jenisDealer-label" class="property-label"><g:message
					code="companyDealer.jenisDealer.label" default="Jenis Dealer" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="jenisDealer-label">
						%{--<ba:editableValue
								bean="${companyDealerIndelnce}" field="jenisDealer"
								url="${request.contextPath}/CompanyDealer/updatefield" type="text"
								title="Enter jenisDealer" onsuccess="reloadCompanyDealerTable();" />--}%

								${companyDealerInstance?.jenisDealer?.encodeAsHTML()}

						</span></td>

				</tr>
				</g:if>

				<g:if test="${companyDealerInstance?.m011Alamat}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m011Alamat-label" class="property-label"><g:message
					code="companyDealer.m011Alamat.label" default="Alamat" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="m011Alamat-label">
						%{--<ba:editableValue
								bean="${companyDealerInstance}" field="m011Alamat"
								url="${request.contextPath}/CompanyDealer/updatefield" type="text"
								title="Enter m011Alamat" onsuccess="reloadCompanyDealerTable();" />--}%

								<g:fieldValue bean="${companyDealerInstance}" field="m011Alamat"/>

						</span></td>

				</tr>
				</g:if>

				<g:if test="${companyDealerInstance?.provinsi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="provinsi-label" class="property-label"><g:message
					code="companyDealer.provinsi.label" default="Provinsi" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="provinsi-label">
						%{--<ba:editableValue
								bean="${companyDealerInstance}" field="provinsi"
								url="${request.contextPath}/CompanyDealer/updatefield" type="text"
								title="Enter provinsi" onsuccess="reloadCompanyDealerTable();" />--}%

								${companyDealerInstance?.provinsi?.encodeAsHTML()}

						</span></td>

				</tr>
				</g:if>

				<g:if test="${companyDealerInstance?.kabKota}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kabKota-label" class="property-label"><g:message
					code="companyDealer.kabKota.label" default="Kab/Kota" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="kabKota-label">
						%{--<ba:editableValue
								bean="${companyDealerInstance}" field="kabKota"
								url="${request.contextPath}/CompanyDealer/updatefield" type="text"
								title="Enter kabKota" onsuccess="reloadCompanyDealerTable();" />--}%

								${companyDealerInstance?.kabKota?.encodeAsHTML()}

						</span></td>

				</tr>
				</g:if>

				<g:if test="${companyDealerInstance?.kecamatan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kecamatan-label" class="property-label"><g:message
					code="companyDealer.kecamatan.label" default="Kecamatan" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="kecamatan-label">
						%{--<ba:editableValue
								bean="${companyDealerInstance}" field="kecamatan"
								url="${request.contextPath}/CompanyDealer/updatefield" type="text"
								title="Enter kecamatan" onsuccess="reloadCompanyDealerTable();" />--}%

							${companyDealerInstance?.kecamatan?.encodeAsHTML()}

						</span></td>

				</tr>
				</g:if>

				<g:if test="${companyDealerInstance?.kelurahan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kelurahan-label" class="property-label"><g:message
					code="companyDealer.kelurahan.label" default="Kelurahan" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="kelurahan-label">
						%{--<ba:editableValue
								bean="${companyDealerInstance}" field="kelurahan"
								url="${request.contextPath}/CompanyDealer/updatefield" type="text"
								title="Enter kelurahan" onsuccess="reloadCompanyDealerTable();" />--}%

								${companyDealerInstance?.kelurahan?.encodeAsHTML()}

						</span></td>

				</tr>
				</g:if>

				<g:if test="${companyDealerInstance?.m011Fax}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m011Fax-label" class="property-label"><g:message
					code="companyDealer.m011Fax.label" default="Fax" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="m011Fax-label">
						%{--<ba:editableValue
								bean="${companyDealerInstance}" field="m011Fax"
								url="${request.contextPath}/CompanyDealer/updatefield" type="text"
								title="Enter m011Fax" onsuccess="reloadCompanyDealerTable();" />--}%

								<g:fieldValue bean="${companyDealerInstance}" field="m011Fax"/> Ext. <g:fieldValue bean="${companyDealerInstance}" field="m011FaxExt"/>


                        </span></td>

				</tr>
				</g:if>

				<g:if test="${companyDealerInstance?.m011Telp}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m011Telp-label" class="property-label"><g:message
					code="companyDealer.m011Telp.label" default="Telp" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="m011Telp-label">
						%{--<ba:editableValue
								bean="${companyDealerInstance}" field="m011Telp"
								url="${request.contextPath}/CompanyDealer/updatefield" type="text"
								title="Enter m011Telp" onsuccess="reloadCompanyDealerTable();" />--}%

								<g:fieldValue bean="${companyDealerInstance}" field="m011Telp"/> Ext. <g:fieldValue bean="${companyDealerInstance}" field="m011TelpExt"/>

                        </span></td>

				</tr>
				</g:if>

				<g:if test="${companyDealerInstance?.m011Email}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m011Email-label" class="property-label"><g:message
					code="companyDealer.m011Email.label" default="Email" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="m011Email-label">
						%{--<ba:editableValue
								bean="${companyDealerInstance}" field="m011Email"
								url="${request.contextPath}/CompanyDealer/updatefield" type="text"
								title="Enter m011Email" onsuccess="reloadCompanyDealerTable();" />--}%

								<g:fieldValue bean="${companyDealerInstance}" field="m011Email"/>

						</span></td>

				</tr>
				</g:if>

				<g:if test="${companyDealerInstance?.m011NPWP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m011NPWP-label" class="property-label"><g:message
					code="companyDealer.m011NPWP.label" default="NPWP" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="m011NPWP-label">
						%{--<ba:editableValue
								bean="${companyDealerInstance}" field="m011NPWP"
								url="${request.contextPath}/CompanyDealer/updatefield" type="text"
								title="Enter m011NPWP" onsuccess="reloadCompanyDealerTable();" />--}%

								<g:fieldValue bean="${companyDealerInstance}" field="m011NPWP"/>

						</span></td>

				</tr>
				</g:if>

				<g:if test="${companyDealerInstance?.m011Logo}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m011Logo-label" class="property-label"><g:message
					code="companyDealer.m011Logo.label" default="Logo" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="m011Logo-label">
						%{--<ba:editableValue
								bean="${companyDealerInstance}" field="m011Logo"
								url="${request.contextPath}/CompanyDealer/updatefield" type="text"
								title="Enter m011Logo" onsuccess="reloadCompanyDealerTable();" />--}%
                         <img class="" src="${createLink(controller:'companyDealer', action:'showLogo', id : companyDealerInstance.id, random : logostamp)}" />
                        </span></td>
                </tr>
				</g:if>


				<g:if test="${companyDealerInstance?.m011NomorRekening}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m011NomorRekening-label" class="property-label"><g:message
					code="companyDealer.m011NomorRekening.label" default="Nomor Rekening" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="m011NomorRekening-label">
						%{--<ba:editableValue
								bean="${companyDealerInstance}" field="m011NomorRekening"
								url="${request.contextPath}/CompanyDealer/updatefield" type="text"
								title="Enter m011NomorRekening" onsuccess="reloadCompanyDealerTable();" />--}%

								<g:fieldValue bean="${companyDealerInstance}" field="m011NomorRekening"/>

						</span></td>

				</tr>
				</g:if>

				<g:if test="${companyDealerInstance?.bank}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="bank-label" class="property-label"><g:message
					code="companyDealer.bank.label" default="Bank" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="bank-label">
						%{--<ba:editableValue
								bean="${companyDealerInstance}" field="bank"
								url="${request.contextPath}/CompanyDealer/updatefield" type="text"
								title="Enter bank" onsuccess="reloadCompanyDealerTable();" />--}%

								${companyDealerInstance?.bank?.encodeAsHTML()}

						</span></td>

				</tr>
				</g:if>

				<g:if test="${companyDealerInstance?.m011AtasNama}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m011AtasNama-label" class="property-label"><g:message
					code="companyDealer.m011AtasNama.label" default="Atas Nama" />:</span></td>


						<td class="span3"><span class="property-value"
						aria-labelledby="m011AtasNama-label">
						%{--<ba:editableValue
								bean="${companyDealerInstance}" field="m011AtasNama"
								url="${request.contextPath}/CompanyDealer/updatefield" type="text"
								title="Enter m011AtasNama" onsuccess="reloadCompanyDealerTable();" />--}%

								<g:fieldValue bean="${companyDealerInstance}" field="m011AtasNama"/>

						</span></td>

				</tr>
				</g:if>

				<g:if test="${companyDealerInstance?.m011Ket}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m011Ket-label" class="property-label"><g:message
					code="companyDealer.m011Ket.label" default="Ket" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="m011Ket-label">
						%{--<ba:editableValue
								bean="${companyDealerInstance}" field="m011Ket"
								url="${request.contextPath}/CompanyDealer/updatefield" type="text"
								title="Enter m011Ket" onsuccess="reloadCompanyDealerTable();" />--}%

								<g:fieldValue bean="${companyDealerInstance}" field="m011Ket"/>

						</span></td>

				</tr>
				</g:if>

				<g:if test="${companyDealerInstance?.m011EmailComplainSerius}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m011EmailComplainSerius-label" class="property-label"><g:message
					code="companyDealer.m011EmailComplainSerius.label" default="Email Complain Serius" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="m011EmailComplainSerius-label">
						%{--<ba:editableValue
								bean="${companyDealerInstance}" field="m011EmailComplainSerius"
								url="${request.contextPath}/CompanyDealer/updatefield" type="text"
								title="Enter m011EmailComplainSerius" onsuccess="reloadCompanyDealerTable();" />--}%

								<g:fieldValue bean="${companyDealerInstance}" field="m011EmailComplainSerius"/>

						</span></td>

				</tr>
				</g:if>

				<g:if test="${companyDealerInstance?.m011EmailComplainTdkSerius}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m011EmailComplainTdkSerius-label" class="property-label"><g:message
					code="companyDealer.m011EmailComplainTdkSerius.label" default="Email Complain Tdk Serius" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="m011EmailComplainTdkSerius-label">
						%{--<ba:editableValue
								bean="${companyDealerInstance}" field="m011EmailComplainTdkSerius"
								url="${request.contextPath}/CompanyDealer/updatefield" type="text"
								title="Enter m011EmailComplainTdkSerius" onsuccess="reloadCompanyDealerTable();" />--}%

								<g:fieldValue bean="${companyDealerInstance}" field="m011EmailComplainTdkSerius"/>

						</span></td>

				</tr>
				</g:if>

				<g:if test="${companyDealerInstance?.m011PKP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m011PKP-label" class="property-label"><g:message
					code="companyDealer.m011PKP.label" default="PKP" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="m011PKP-label">
						%{--<ba:editableValue
								bean="${companyDealerInstance}" field="m011PKP"
								url="${request.contextPath}/CompanyDealer/updatefield" type="text"
								title="Enter m011PKP" onsuccess="reloadCompanyDealerTable();" />--}%

								<g:fieldValue bean="${companyDealerInstance}" field="m011PKP"/>

						</span></td>

				</tr>
				</g:if>

				<g:if test="${companyDealerInstance?.m011StaBPCenter}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m011StaBPCenter-label" class="property-label"><g:message
					code="companyDealer.m011StaBPCenter.label" default="BP Center" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="m011StaBPCenter-label">
						%{--<ba:editableValue
								bean="${companyDealerInstance}" field="m011StaBPCenter"
								url="${request.contextPath}/CompanyDealer/updatefield" type="text"
								title="Enter m011StaBPCenter" onsuccess="reloadCompanyDealerTable();" />--}%
						 <g:if test="${companyDealerInstance.m011StaBPCenter == '0'}">
                                                    <g:message code="companyDealer.m011StaBPCenter.label.No" default="Tidak" />
                                                </g:if>
                                                <g:else>
                                                     <g:message code="companyDealer.m011StaBPCenter.label.Yes" default="Ya" />
                                                </g:else>

                                                </td>

				</tr>
                                </g:if>

				<g:if test="${companyDealerInstance?.m011SelisihWaktu}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m011SelisihWaktu-label" class="property-label"><g:message
					code="companyDealer.m011SelisihWaktu.label" default="Selisih Waktu (Jam)" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="m011SelisihWaktu-label">
						    <g:fieldValue field="m011SelisihWaktu" bean="${companyDealerInstance}" />
                        </td>

				</tr>
                                </g:if>

            <g:if test="${companyDealerInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="jenisStall.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${companyDealerInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadJenisStallTable();" />--}%

                        <g:fieldValue bean="${companyDealerInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${companyDealerInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="jenisStall.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${companyDealerInstance}" field="dateCreated"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadJenisStallTable();" />--}%

                        <g:formatDate date="${companyDealerInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${companyDealerInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="jenisStall.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${companyDealerInstance}" field="createdBy"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadJenisStallTable();" />--}%

                        <g:fieldValue bean="${companyDealerInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${companyDealerInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="jenisStall.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${companyDealerInstance}" field="lastUpdated"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadJenisStallTable();" />--}%

                        <g:formatDate date="${companyDealerInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${companyDealerInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="jenisStall.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${companyDealerInstance}" field="updatedBy"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadJenisStallTable();" />--}%

                        <g:fieldValue bean="${companyDealerInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
            
            %{--<g:if test="${companyDealerInstance?.staDel}">--}%
                                 %{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="m011StaDel-label" class="property-label"><g:message--}%
					%{--code="companyDealer.m011StaDel.label" default="M011 Sta Del" />:</span></td>--}%

						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="m011StaDel-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${companyDealerInstance}" field="m011StaDel"--}%
								%{--url="${request.contextPath}/CompanyDealer/updatefield" type="text"--}%
								%{--title="Enter m011StaDel" onsuccess="reloadCompanyDealerTable();" />--}%

								%{--<g:fieldValue bean="${companyDealerInstance}" field="staDel"/>--}%

						%{--</span></td>--}%

				%{--</tr>    --}%
				%{--</g:if>--}%

			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${companyDealerInstance?.id}"
					update="[success:'companyDealer-form',failure:'companyDealer-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteCompanyDealer('${companyDealerInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
