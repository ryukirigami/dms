
<%@ page import="com.kombos.customerprofile.Model" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

</g:javascript>
<table id="model_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="model.m065ID.label" default="Kode Model" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="model.m065NamaModel.label" default="Nama Merk" /></div>
            </th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="model.m065NamaModel.label" default="Nama Model" /></div>
			</th>
        </tr>
		<tr>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m065ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m065ID" class="search_init" onkeypress="return isNumberKey(event)"  />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_merk" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_merk" class="search_init" />
                </div>
            </th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m065NamaModel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m065NamaModel" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var modelTable;
var reloadModelTable;
$(function(){
	
	reloadModelTable = function() {
		modelTable.fnDraw();
	}
	var recordsModelPerPage = [];//new Array();
    var anModelSelected;
    var jmlRecModelPerPage=0;
    var id;

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {

			e.stopPropagation();
		 	modelTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	modelTable = $('#model_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsModel = $("#model_datatables tbody .row-select");
            var jmlModelCek = 0;
            var nRow;
            var idRec;
            rsModel.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsModelPerPage[idRec]=="1"){
                    jmlModelCek = jmlModelCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsModelPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecModelPerPage = rsModel.length;
            if(jmlModelCek==jmlRecModelPerPage && jmlRecModelPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m065ID",
	"mDataProp": "m065ID",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "merk",
	"mDataProp": "merk",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}
,


{
	"sName": "m065NamaModel",
	"mDataProp": "m065NamaModel",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}



],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m065ID = $('#filter_m065ID input').val();
						if(m065ID){
							aoData.push(
									{"name": 'sCriteria_m065ID', "value": m065ID}
							);
						}
	
						var merk = $('#filter_merk input').val();
						if(merk){
							aoData.push(
									{"name": 'sCriteria_merk', "value": merk}
							);
						}
	
						var m065NamaModel = $('#filter_m065NamaModel input').val();
						if(m065NamaModel){
							aoData.push(
									{"name": 'sCriteria_m065NamaModel', "value": m065NamaModel}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
	$('.select-all').click(function(e) {

        $("#model_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsModelPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsModelPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#model_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsModelPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anModelSelected = modelTable.$('tr.row_selected');
            if(jmlRecModelPerPage == anModelSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsModelPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
