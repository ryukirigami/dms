

<%@ page import="com.kombos.customerprofile.Model" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'model.label', default: 'Model')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteModel;

$(function(){ 
	deleteModel=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/model/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadModelTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-model" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="model"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${modelInstance?.m065ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m065ID-label" class="property-label"><g:message
					code="model.m065ID.label" default="Kode Model" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m065ID-label">
						%{--<ba:editableValue
								bean="${modelInstance}" field="m065ID"
								url="${request.contextPath}/Model/updatefield" type="text"
								title="Enter m065ID" onsuccess="reloadModelTable();" />--}%
							
								<g:fieldValue bean="${modelInstance}" field="m065ID"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${modelInstance?.merk}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="merk-label" class="property-label"><g:message
                                code="model.merk.label" default="Nama Merk" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="merk-label">
                        %{--<ba:editableValue
                                bean="${modelInstance}" field="merk"
                                url="${request.contextPath}/Model/updatefield" type="text"
                                title="Enter merk" onsuccess="reloadModelTable();" />--}%

                        %{--<g:link controller="merk" action="show" id="${modelInstance?.merk?.id}">${modelInstance?.merk?.encodeAsHTML()}</g:link>--}%
                        <g:fieldValue bean="${modelInstance?.merk}" field="m064NamaMerk"/>
                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${modelInstance?.m065NamaModel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m065NamaModel-label" class="property-label"><g:message
					code="model.m065NamaModel.label" default="Nama Model" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m065NamaModel-label">
						%{--<ba:editableValue
								bean="${modelInstance}" field="m065NamaModel"
								url="${request.contextPath}/Model/updatefield" type="text"
								title="Enter m065NamaModel" onsuccess="reloadModelTable();" />--}%
							
								<g:fieldValue bean="${modelInstance}" field="m065NamaModel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${modelInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="model.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${modelInstance}" field="lastUpdProcess"
								url="${request.contextPath}/Model/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadModelTable();" />--}%
							
								<g:fieldValue bean="${modelInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${modelInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="model.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${modelInstance}" field="dateCreated"
								url="${request.contextPath}/Model/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadModelTable();" />--}%
							
								<g:formatDate date="${modelInstance?.dateCreated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>


            <g:if test="${modelInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="model.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${modelInstance}" field="createdBy"
                                url="${request.contextPath}/Model/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadModelTable();" />--}%

                        <g:fieldValue bean="${modelInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${modelInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="model.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${modelInstance}" field="lastUpdated"
								url="${request.contextPath}/Model/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadModelTable();" />--}%
							
								<g:formatDate date="${modelInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${modelInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="model.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${modelInstance}" field="updatedBy"
                                url="${request.contextPath}/Model/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadModelTable();" />--}%

                        <g:fieldValue bean="${modelInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${modelInstance?.id}"
					update="[success:'model-form',failure:'model-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteModel('${modelInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
