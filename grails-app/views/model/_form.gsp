<%@ page import="com.kombos.customerprofile.Merk; com.kombos.customerprofile.Model" %>


<g:if test="${modelInstance?.m065ID}">
<div class="control-group fieldcontain ${hasErrors(bean: modelInstance, field: 'm065ID', 'error')} required">
	<label class="control-label" for="m065ID">
		<g:message code="model.m065ID.label" default="Kode Model" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        ${modelInstance?.m065ID}
	</div>
</div>
</g:if>

<div class="control-group fieldcontain ${hasErrors(bean: modelInstance, field: 'merk', 'error')} required">
    <label class="control-label" for="merk">
        <g:message code="model.merk.label" default="Nama Merk" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="merk" name="merk.id" from="${Merk.createCriteria().list(){eq("staDel", "0");order("m064NamaMerk", "asc")}}" optionKey="id" required="" value="${modelInstance?.merk?.id}" class="many-to-one"/>
    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: modelInstance, field: 'm065NamaModel', 'error')} required">
    <label class="control-label" for="m065NamaModel">
        <g:message code="model.m065NamaModel.label" default="Nama Model" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="m065NamaModel" maxlength="50" required="" value="${modelInstance?.m065NamaModel}"/>
    </div>
</div>
