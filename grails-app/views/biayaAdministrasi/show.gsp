

<%@ page import="com.kombos.administrasi.BiayaAdministrasi" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'biayaAdministrasi.label', default: 'Biaya Administrasi')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteBiayaAdministrasi;

$(function(){ 
	deleteBiayaAdministrasi=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/biayaAdministrasi/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadBiayaAdministrasiTable();
   				expandTableLayout('biayaAdministrasi');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-biayaAdministrasi" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="biayaAdministrasi"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${biayaAdministrasiInstance?.biaya}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="biaya-label" class="property-label"><g:message
					code="biayaAdministrasi.biaya.label" default="Biaya" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="biaya-label">
						%{--<ba:editableValue
								bean="${biayaAdministrasiInstance}" field="biaya"
								url="${request.contextPath}/BiayaAdministrasi/updatefield" type="text"
								title="Enter biaya" onsuccess="reloadBiayaAdministrasiTable();" />--}%
							
								<g:fieldValue bean="${biayaAdministrasiInstance}" field="biaya"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${biayaAdministrasiInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="biayaAdministrasi.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${biayaAdministrasiInstance}" field="createdBy"
								url="${request.contextPath}/BiayaAdministrasi/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadBiayaAdministrasiTable();" />--}%
							
								<g:fieldValue bean="${biayaAdministrasiInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${biayaAdministrasiInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="biayaAdministrasi.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${biayaAdministrasiInstance}" field="dateCreated"
								url="${request.contextPath}/BiayaAdministrasi/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadBiayaAdministrasiTable();" />--}%
							
								<g:formatDate date="${biayaAdministrasiInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${biayaAdministrasiInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="biayaAdministrasi.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${biayaAdministrasiInstance}" field="lastUpdProcess"
								url="${request.contextPath}/BiayaAdministrasi/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadBiayaAdministrasiTable();" />--}%
							
								<g:fieldValue bean="${biayaAdministrasiInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${biayaAdministrasiInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="biayaAdministrasi.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${biayaAdministrasiInstance}" field="lastUpdated"
								url="${request.contextPath}/BiayaAdministrasi/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadBiayaAdministrasiTable();" />--}%
							
								<g:formatDate date="${biayaAdministrasiInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${biayaAdministrasiInstance?.tglBerlaku}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tglBerlaku-label" class="property-label"><g:message
					code="biayaAdministrasi.tglBerlaku.label" default="Tgl Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tglBerlaku-label">
						%{--<ba:editableValue
								bean="${biayaAdministrasiInstance}" field="tglBerlaku"
								url="${request.contextPath}/BiayaAdministrasi/updatefield" type="text"
								title="Enter tglBerlaku" onsuccess="reloadBiayaAdministrasiTable();" />--}%
							
								<g:formatDate date="${biayaAdministrasiInstance?.tglBerlaku}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${biayaAdministrasiInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="biayaAdministrasi.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${biayaAdministrasiInstance}" field="updatedBy"
								url="${request.contextPath}/BiayaAdministrasi/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadBiayaAdministrasiTable();" />--}%
							
								<g:fieldValue bean="${biayaAdministrasiInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('biayaAdministrasi');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${biayaAdministrasiInstance?.id}"
					update="[success:'biayaAdministrasi-form',failure:'biayaAdministrasi-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteBiayaAdministrasi('${biayaAdministrasiInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
