<%@ page import="com.kombos.administrasi.BiayaAdministrasi" %>



<div class="control-group fieldcontain ${hasErrors(bean: biayaAdministrasiInstance, field: 'tglBerlaku', 'error')} ">
    <label class="control-label" for="tglBerlaku">
        <g:message code="biayaAdministrasi.tglBerlaku.label" default="Tgl Berlaku" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <ba:datePicker name="tglBerlaku" precision="day" required="true" value="${biayaAdministrasiInstance?.tglBerlaku}" format="dd-MM-yyyy"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: biayaAdministrasiInstance, field: 'biaya', 'error')} ">
	<label class="control-label" for="biaya">
		<g:message code="biayaAdministrasi.biaya.label" default="Biaya" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="biaya" value="${biayaAdministrasiInstance.biaya ? Double.valueOf(biayaAdministrasiInstance.biaya).longValue() : ""}" required="" />
	</div>
</div>