
<%@ page import="com.kombos.administrasi.BiayaAdministrasi" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="biayaAdministrasi_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="biayaAdministrasi.tglBerlaku.label" default="Tanggal Berlaku" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="biayaAdministrasi.biaya.label" default="Biaya" /></div>
        </th>

    </tr>
    %{--<tr>--}%

        %{--<th style="border-top: none;padding: 5px;">--}%
            %{--<div id="filter_tglBerlaku" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >--}%
                %{--<input type="hidden" name="search_tglBerlaku" value="date.struct">--}%
                %{--<input type="hidden" name="search_tglBerlaku_day" id="search_tglBerlaku_day" value="">--}%
                %{--<input type="hidden" name="search_tglBerlaku_month" id="search_tglBerlaku_month" value="">--}%
                %{--<input type="hidden" name="search_tglBerlaku_year" id="search_tglBerlaku_year" value="">--}%
                %{--<input type="text" data-date-format="dd/mm/yyyy" name="search_tglBerlaku_dp" value="" id="search_tglBerlaku" class="search_init">--}%
            %{--</div>--}%
        %{--</th>--}%


        %{--<th style="border-top: none;padding: 5px;">--}%
            %{--<div id="filter_biaya" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
                %{--<input type="text" name="search_biaya" class="search_init" />--}%
            %{--</div>--}%
        %{--</th>--}%

    %{--</tr>--}%
    </thead>
</table>

<g:javascript>
var biayaAdministrasiTable;
var reloadBiayaAdministrasiTable;
$(function(){

	reloadBiayaAdministrasiTable = function() {
		biayaAdministrasiTable.fnDraw();
	}

	
	$('#search_tglBerlaku').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tglBerlaku_day').val(newDate.getDate());
			$('#search_tglBerlaku_month').val(newDate.getMonth()+1);
			$('#search_tglBerlaku_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			biayaAdministrasiTable.fnDraw();	
	});

	


//    $("th div input").bind('keypress', function(e) {
//		var code = (e.keyCode ? e.keyCode : e.which);
//		if(code == 13) {
//			e.stopPropagation();
//		 	biayaAdministrasiTable.fnDraw();
//		}
//	});
//	$("th div input").click(function (e) {
//	 	e.stopPropagation();
//	});

	biayaAdministrasiTable = $('#biayaAdministrasi_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
//		        var aData = biayaAdministrasiTable.fnGetData(nRow);
//		        var checkbox = $('.row-select', nRow);
//		        if(checked.indexOf(""+aData['id'])>=0){
//		              checkbox.attr("checked",true);
//		              checkbox.parent().parent().addClass('row_selected');
//		        }
//
//		        checkbox.click(function (e) {
//            	    var tc = $(this);
//
//                    if(this.checked)
//            			tc.parent().parent().addClass('row_selected');
//            		else {
//            			tc.parent().parent().removeClass('row_selected');
//            			var selectAll = $('.select-all');
//            			selectAll.removeAttr('checked');
//                    }
//            	 	e.stopPropagation();
//                });
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "tglBerlaku",
	"mDataProp": "tglBerlaku",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "biaya",
	"mDataProp": "biaya",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                var biaya = $('#filter_biaya input').val();
						if(biaya){
							aoData.push(
									{"name": 'sCriteria_biaya', "value": biaya}
							);
						}

						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						var tglBerlaku = $('#search_tglBerlaku').val();
						var tglBerlakuDay = $('#search_tglBerlaku_day').val();
						var tglBerlakuMonth = $('#search_tglBerlaku_month').val();
						var tglBerlakuYear = $('#search_tglBerlaku_year').val();

						if(tglBerlaku){
							aoData.push(
									{"name": 'sCriteria_tglBerlaku', "value": "date.struct"},
									{"name": 'sCriteria_tglBerlaku_dp', "value": tglBerlaku},
									{"name": 'sCriteria_tglBerlaku_day', "value": tglBerlakuDay},
									{"name": 'sCriteria_tglBerlaku_month', "value": tglBerlakuMonth},
									{"name": 'sCriteria_tglBerlaku_year', "value": tglBerlakuYear}
							);
						}
                        var selectAll = $('.select-all');
                        selectAll.removeAttr('checked');
                         $("#biayaAdministrasi_datatables tbody .row-select").each(function() {
                            var id = $(this).next("input:hidden").val();
                            if(checked.indexOf(""+id)>=0){
                                checked[checked.indexOf(""+id)]="";
                            }
                            if(this.checked){
                                checked.push(""+id);
                            }
                        });
				$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
