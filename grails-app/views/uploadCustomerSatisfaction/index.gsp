
<%@ page import="com.kombos.administrasi.Operation" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'operation.label', default: 'Upload Job')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />

            <g:javascript>

                <g:if test="${jmlhDataError && jmlhDataError>0}">
                    $('#save').attr("disabled", true);
                </g:if>

                var saveForm;
                $(function(){

                    function progress(e){
                        if(e.lengthComputable){
                            //kalo mau pake progress bar
                            //$('progress').attr({value:e.loaded,max:e.total});
                        }
                    }

                    saveForm = function(insertUpdate) {

                    <g:if test="${jsonData}">
                        var sendData = ${jsonData};
                        $.ajax({
                            url:'${request.getContextPath()}/uploadCustomerSatisfaction/upload?bpGr=${bpGr}&companyDealer.id=${companyDealer}&insertUpdate='+insertUpdate,
                            type: 'POST',
                            xhr: function() {
                                var myXhr = $.ajaxSettings.xhr();
                                if(myXhr.upload){
                                    myXhr.upload.addEventListener('progress',progress, false);
                                }
                                return myXhr;
                            },
                            //add beforesend handler to validate or something
                            //beforeSend: functionname,
                            success: function (res) {
                                $('#jobTable').empty();
                                $('#jobTable').append(res);
                                //reloadOperationTable();
                            },
                            //add error handler for when a error occurs if you want!
                            error: function (data, status, e){
                                alert(e);
                            },
                            complete: function(xhr, status) {

                            },
                            data: JSON.stringify(sendData),
                            contentType: "application/json; charset=utf-8",
                            traditional: true,
                            cache: false
                        });
                    </g:if>
                    <g:else>
                        alert('No File Selected');
                    </g:else>
                    }


                });

            </g:javascript>

	</head>
	<body>
    <div id="jobTable">

        <div class="navbar box-header no-border">
            <span class="pull-left">
                <g:message code="default.list.label" args="[entityName]" />
            </span>
            <ul class="nav pull-right">
                <li></li>
                <li></li>
                <li class="separator"></li>
            </ul>
        </div>
        <div class="box">
            <div class="span12" id="operation-table">
                <fieldset>
                    <form id="uploadCustomerSatisfaction-save" class="form-vertical" action="${request.getContextPath()}/uploadCustomerSatisfaction/save" method="post" onsubmit="submitForm();return false;">
                        <table>
                            <tr>
                                <td>
                                    <fieldset class="form">
                                        <g:render template="form"/>
                                    </fieldset>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <fieldset class="buttons controls">
                                        <g:submitButton class="btn btn-primary create" name="view" id="view" value="${message(code: 'default.button.view.label', default: 'Upload')}" />
                                        &nbsp;&nbsp;&nbsp;
                                        <g:if test="${flash.message}">
                                            ${flash.message}
                                        </g:if>
                                        <%if(jsonData){%>
                                        <g:field type="button" onclick="saveForm('INSERT');" class="btn btn-primary create" name="save" id="save" value="${message(code: 'default.button.upload.label', default: 'Insert Only')}"/>
                                        <g:field type="button" onclick="saveForm('REPLACE');" class="btn btn-primary create" name="save" id="save" value="${message(code: 'default.button.upload.label', default: 'Insert And Update')}"/>
                                        <%} %>
                                    </fieldset>
                                </td>
                            </tr>
                        </table>
                    </form>
                    <g:javascript>
                        var submitForm;
                        $(function(){

                            function progress(e){
                                if(e.lengthComputable){
                                    //kalo mau pake progress bar
                                    //$('progress').attr({value:e.loaded,max:e.total});
                                }
                            }

                            submitForm = function() {

                                var form = new FormData($('#uploadCustomerSatisfaction-save')[0]);

                                $.ajax({
                                    url:'${request.getContextPath()}/uploadCustomerSatisfaction/view',
                                    type: 'POST',
                                    xhr: function() {
                                        var myXhr = $.ajaxSettings.xhr();
                                        if(myXhr.upload){
                                            myXhr.upload.addEventListener('progress',progress, false);
                                        }
                                        return myXhr;
                                    },
                                    //add beforesend handler to validate or something
                                    //beforeSend: functionname,
                                    success: function (res) {
                                        $('#jobTable').empty();
                                        $('#jobTable').append(res);
                                        //reloadOperationTable();
                                    },
                                    //add error handler for when a error occurs if you want!
                                    error: function (data, status, e){
                                        alert(e);
                                    },
                                    complete: function(xhr, status) {

                                    },
                                    data: form,
                                    cache: false,
                                    contentType: false,
                                    processData: false
                                });

                            }
                        });
                    </g:javascript>
                </fieldset>
                <br>

                <table class="display table table-striped table-bordered table-hover dataTable" width="100%" cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px; width: 1284px;">
                    <tr>
                        <th>
                            Tanggal Berlaku
                        </th>
                        <th>
                            YTD-S
                        </th>
                        <th>
                            YTD-D
                        </th>
                        <th>
                            This Month-S
                        </th>
                        <th>
                            This Month-D
                        </th>
                        <th>
                            Q1-S
                        </th>
                        <th>
                            Q1-D
                        </th>
                        <th>
                            Q2-S
                        </th>
                        <th>
                            Q2-D
                        </th>
                        <th>
                            Q3-S
                        </th>
                        <th>
                            Q3-D
                        </th>
                        <th>
                            Q4-S
                        </th>
                        <th>
                            Q4-D
                        </th>
                        <th>
                            Q5-S
                        </th>
                        <th>
                            Q5-D
                        </th>
                    </tr>
                    <g:if test="${htmlData}">
                        ${htmlData}
                    </g:if>
                    <g:else>
                        <tr class="odd">
                            <td class="dataTables_empty" valign="top" colspan="15">No data available in table</td>
                        </tr>
                    </g:else>
                </table>
                %{--<g:if test="${jsonData}">--}%
                %{--${jsonData}--}%
                %{--</g:if>--}%
            </div>
        </div>
    </div>
    </body>
</html>