<%@ page import="com.kombos.finance.JournalType" %>



<div class="control-group fieldcontain ${hasErrors(bean: journalTypeInstance, field: 'journalType', 'error')} required">
	<label class="control-label" for="journalType">
		<g:message code="journalType.journalType.label" default="Journal Type" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="journalType" required="" value="${journalTypeInstance?.journalType}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: journalTypeInstance, field: 'description', 'error')} required">
	<label class="control-label" for="description">
		<g:message code="journalType.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
    	%{--<g:textField name="description" required="" value="${journalTypeInstance?.description}"/>--}%
	    <g:textArea name="description" required="" value="${journalTypeInstance?.description}"/>
    </div>
</div>
