

<%@ page import="com.kombos.finance.JournalType" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'journalType.label', default: 'JournalType')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteJournalType;

$(function(){ 
	deleteJournalType=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/journalType/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadJournalTypeTable();
   				expandTableLayout('journalType');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-journalType" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="journalType"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${journalTypeInstance?.journalType}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="journalType-label" class="property-label"><g:message
					code="journalType.journalType.label" default="Journal Type" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="journalType-label">
						%{--<ba:editableValue
								bean="${journalTypeInstance}" field="journalType"
								url="${request.contextPath}/JournalType/updatefield" type="text"
								title="Enter journalType" onsuccess="reloadJournalTypeTable();" />--}%
							
								<g:fieldValue bean="${journalTypeInstance}" field="journalType"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalTypeInstance?.description}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="description-label" class="property-label"><g:message
					code="journalType.description.label" default="Description" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="description-label">
						%{--<ba:editableValue
								bean="${journalTypeInstance}" field="description"
								url="${request.contextPath}/JournalType/updatefield" type="text"
								title="Enter description" onsuccess="reloadJournalTypeTable();" />--}%
							
								<g:fieldValue bean="${journalTypeInstance}" field="description"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalTypeInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="journalType.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${journalTypeInstance}" field="staDel"
								url="${request.contextPath}/JournalType/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadJournalTypeTable();" />--}%
							
								<g:fieldValue bean="${journalTypeInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalTypeInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="journalType.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${journalTypeInstance}" field="createdBy"
								url="${request.contextPath}/JournalType/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadJournalTypeTable();" />--}%
							
								<g:fieldValue bean="${journalTypeInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalTypeInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="journalType.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${journalTypeInstance}" field="updatedBy"
								url="${request.contextPath}/JournalType/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadJournalTypeTable();" />--}%
							
								<g:fieldValue bean="${journalTypeInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalTypeInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="journalType.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${journalTypeInstance}" field="lastUpdProcess"
								url="${request.contextPath}/JournalType/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadJournalTypeTable();" />--}%
							
								<g:fieldValue bean="${journalTypeInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalTypeInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="journalType.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${journalTypeInstance}" field="dateCreated"
								url="${request.contextPath}/JournalType/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadJournalTypeTable();" />--}%
							
								<g:formatDate date="${journalTypeInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalTypeInstance?.journal}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="journal-label" class="property-label"><g:message
					code="journalType.journal.label" default="Journal" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="journal-label">
						%{--<ba:editableValue
								bean="${journalTypeInstance}" field="journal"
								url="${request.contextPath}/JournalType/updatefield" type="text"
								title="Enter journal" onsuccess="reloadJournalTypeTable();" />--}%
							
								<g:each in="${journalTypeInstance.journal}" var="j">
								<g:link controller="journal" action="show" id="${j.id}">${j?.encodeAsHTML()}</g:link>
								</g:each>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalTypeInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="journalType.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${journalTypeInstance}" field="lastUpdated"
								url="${request.contextPath}/JournalType/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadJournalTypeTable();" />--}%
							
								<g:formatDate date="${journalTypeInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${journalTypeInstance?.transaction}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="transaction-label" class="property-label"><g:message
					code="journalType.transaction.label" default="Transaction" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="transaction-label">
						--}%%{--<ba:editableValue
								bean="${journalTypeInstance}" field="transaction"
								url="${request.contextPath}/JournalType/updatefield" type="text"
								title="Enter transaction" onsuccess="reloadJournalTypeTable();" />--}%%{--
							
								<g:each in="${journalTypeInstance.transaction}" var="t">
								<g:link controller="transaction" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link>
								</g:each>
							
						</span></td>
					
				</tr>
				</g:if>--}%
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('journalType');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${journalTypeInstance?.id}"
					update="[success:'journalType-form',failure:'journalType-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteJournalType('${journalTypeInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
