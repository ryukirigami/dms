
<%@ page import="com.kombos.hrd.TugasLuarKaryawan" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="tugasLuarKaryawan_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">

</table>

<g:javascript>
var tugasLuarKaryawanTable;
var reloadTugasLuarKaryawanTable;
$(function(){
	
	reloadTugasLuarKaryawanTable = function() {
		tugasLuarKaryawanTable.fnDraw();
	}


    $("#btnCari").click(function() {
	    reloadTugasLuarKaryawanTable();
    });


	$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	tugasLuarKaryawanTable.fnDraw();
		}
	});

	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	tugasLuarKaryawanTable = $('#tugasLuarKaryawan_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [


{
    "sTitle": "Nama Karyawan",
	"sName": "karyawan",
	"mDataProp": "karyawan",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true,
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	}
}
,


{
    "sTitle": "Tanggal Tugas Luar",
	"sName": "dateBegin",
	"mDataProp": "dateBegin",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
    "sTitle": "Tujuan",
	"sName": "assignmentObjectives",
	"mDataProp": "assignmentObjectives",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
    "sTitle": "Dasar Penugasan",
	"sName": "assignmentReason",
	"mDataProp": "assignmentReason",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
    "sTitle": "Keterangan",
	"sName": "explanation",
	"mDataProp": "explanation",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}
,

{   "sTitle": "Cabang",
	"sName": "cabang",
	"mDataProp": "cabang",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var assignmentObjectives = $('#filter_assignmentObjectives').val();
						if(assignmentObjectives){
							aoData.push(
									{"name": 'sCriteria_assignmentObjectives', "value": assignmentObjectives}
							);
						}

						var dateBegin = $('#search_dateBegin').val();
						var dateBeginDay = $('#search_dateBegin_day').val();
						var dateBeginMonth = $('#search_dateBegin_month').val();
						var dateBeginYear = $('#search_dateBegin_year').val();
						
						if(dateBegin){
							aoData.push(
									{"name": 'sCriteria_dateBegin', "value": "date.struct"},
									{"name": 'sCriteria_dateBegin_dp', "value": dateBegin},
									{"name": 'sCriteria_dateBegin_day', "value": dateBeginDay},
									{"name": 'sCriteria_dateBegin_month', "value": dateBeginMonth},
									{"name": 'sCriteria_dateBegin_year', "value": dateBeginYear}
							);
						}

						var dateFinish = $('#search_dateFinish').val();
						var dateFinishDay = $('#search_dateFinish_day').val();
						var dateFinishMonth = $('#search_dateFinish_month').val();
						var dateFinishYear = $('#search_dateFinish_year').val();
						
						if(dateFinish){
							aoData.push(
									{"name": 'sCriteria_dateFinish', "value": "date.struct"},
									{"name": 'sCriteria_dateFinish_dp', "value": dateFinish},
									{"name": 'sCriteria_dateFinish_day', "value": dateFinishDay},
									{"name": 'sCriteria_dateFinish_month', "value": dateFinishMonth},
									{"name": 'sCriteria_dateFinish_year', "value": dateFinishYear}
							);
						}

						var sCriteria_cabang = $('#sCriteria_dealer').val();
						if(sCriteria_cabang){
							aoData.push(
									{"name": 'sCriteria_dealer', "value": sCriteria_cabang}
							);
						}

	
						var karyawan = $('#filter_karyawan').val();
						if(karyawan){
							aoData.push(
									{"name": 'sCriteria_karyawan', "value": karyawan}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>

