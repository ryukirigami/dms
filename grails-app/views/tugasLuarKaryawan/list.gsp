
<%@ page import="com.kombos.hrd.TugasLuarKaryawan" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tugasLuarKaryawan.label', default: 'TugasLuarKaryawan')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
        <style>
        .modal.fade.in {
            left: 25.5% !important;
            width: 1205px !important;
        }
        .modal.fade {
            top: -100%;
        }
        </style>
        <g:javascript>
			var show;
			var edit;
			var oFormService;
			$(function(){ 
			
				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('tugasLuarKaryawan');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('tugasLuarKaryawan', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('tugasLuarKaryawan', '${request.contextPath}/tugasLuarKaryawan/massdelete', reloadTugasLuarKaryawanTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('tugasLuarKaryawan','${request.contextPath}/tugasLuarKaryawan/show/'+id);
				};
				
				edit = function(id) {
					editInstance('tugasLuarKaryawan','${request.contextPath}/tugasLuarKaryawan/edit/'+id);
				};
				oFormService = {
			         fnShowKaryawanDataTable: function() {
                        $("#karyawanModal").modal("show");
                        $("#karyawanModal").fadeIn();
			        },
			        fnHideKaryawanDataTable: function() {
			            $("#karyawanModal").modal("hide");
			        }
			    }

			    oFormService.fnHideKaryawanDataTable();
});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="tugasLuarKaryawan-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

            <!--
            ***************************************************************
            ======================= SEARCH FORM ===========================
            ***************************************************************
            -->


            <form class="form-horizontal">
                <fieldset>

                    <!-- Form Name -->
                    <legend>Pencarian Tugas Luar </legend>

                    <!-- Text input-->
                    <div class="control-group">
                        <label class="control-label" for="filter_karyawan">Nama Karyawan</label>
                        <div class="controls">
                            <input id="filter_karyawan" name="filter_karyawan" placeholder="" class="input-xlarge" type="text">

                        </div>
                    </div>
                    <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                        <div class="control-group">
                           <label class="control-label" for="filter_karyawan">Cabang</label>
                           <div class="controls">
                                <g:select name="sCriteria_dealer" id="sCriteria_dealer" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" noSelection="['':'SEMUA']" />
                            </div>
                       </div>
                    </g:if>
                    <!-- Text input-->
                    <div class="control-group">
                        <label class="control-label" for="filter_assignmentObjectives">Tujuan</label>
                        <div class="controls">
                            <input id="filter_assignmentObjectives" name="filter_assignmentObjectives" placeholder="" class="input-xlarge" type="text">

                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="search_dateBegin">Tanggal Tugas Luar (Mulai)</label>
                        <div class="controls">
                            <ba:datePicker id="search_dateBegin" name="search_dateBegin" precision="day" format="dd/MM/yyyy"/>
                            <span style="margin-left: 5px;"> - </span>
                            <ba:datePicker id="search_dateFinish" name="search_dateFinish" precision="day" format="dd/MM/yyyy"/>
                        </div>
                    </div>

                    <!-- Button -->
                    <div class="control-group">
                        <label class="control-label" for="btnCari"></label>
                        <div class="controls">
                            <a id="btnCari" name="btnCari" class="btn btn-primary" href="javascript:void(0);">Cari</a>
                        </div>
                    </div>

                </fieldset>
            </form>


            <g:render template="dataTables" />
		</div>
		<div class="span7" id="tugasLuarKaryawan-form" style="display: none;"></div>
	</div>
</body>
</html>
