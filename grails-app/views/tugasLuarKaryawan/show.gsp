

<%@ page import="com.kombos.hrd.TugasLuarKaryawan" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'tugasLuarKaryawan.label', default: 'TugasLuarKaryawan')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteTugasLuarKaryawan;

$(function(){ 
	deleteTugasLuarKaryawan=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/tugasLuarKaryawan/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadTugasLuarKaryawanTable();
   				expandTableLayout('tugasLuarKaryawan');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-tugasLuarKaryawan" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="tugasLuarKaryawan"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${tugasLuarKaryawanInstance?.assignmentObjectives}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="assignmentObjectives-label" class="property-label"><g:message
					code="tugasLuarKaryawan.assignmentObjectives.label" default="Assignment Objectives" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="assignmentObjectives-label">
						%{--<ba:editableValue
								bean="${tugasLuarKaryawanInstance}" field="assignmentObjectives"
								url="${request.contextPath}/TugasLuarKaryawan/updatefield" type="text"
								title="Enter assignmentObjectives" onsuccess="reloadTugasLuarKaryawanTable();" />--}%
							
								<g:fieldValue bean="${tugasLuarKaryawanInstance}" field="assignmentObjectives"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tugasLuarKaryawanInstance?.assignmentReason}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="assignmentReason-label" class="property-label"><g:message
					code="tugasLuarKaryawan.assignmentReason.label" default="Assignment Reason" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="assignmentReason-label">
						%{--<ba:editableValue
								bean="${tugasLuarKaryawanInstance}" field="assignmentReason"
								url="${request.contextPath}/TugasLuarKaryawan/updatefield" type="text"
								title="Enter assignmentReason" onsuccess="reloadTugasLuarKaryawanTable();" />--}%
							
								<g:fieldValue bean="${tugasLuarKaryawanInstance}" field="assignmentReason"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tugasLuarKaryawanInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="tugasLuarKaryawan.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${tugasLuarKaryawanInstance}" field="createdBy"
								url="${request.contextPath}/TugasLuarKaryawan/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadTugasLuarKaryawanTable();" />--}%
							
								<g:fieldValue bean="${tugasLuarKaryawanInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tugasLuarKaryawanInstance?.dateBegin}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateBegin-label" class="property-label"><g:message
					code="tugasLuarKaryawan.dateBegin.label" default="Date Begin" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateBegin-label">
						%{--<ba:editableValue
								bean="${tugasLuarKaryawanInstance}" field="dateBegin"
								url="${request.contextPath}/TugasLuarKaryawan/updatefield" type="text"
								title="Enter dateBegin" onsuccess="reloadTugasLuarKaryawanTable();" />--}%
							
								<g:formatDate date="${tugasLuarKaryawanInstance?.dateBegin}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tugasLuarKaryawanInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="tugasLuarKaryawan.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${tugasLuarKaryawanInstance}" field="dateCreated"
								url="${request.contextPath}/TugasLuarKaryawan/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadTugasLuarKaryawanTable();" />--}%
							
								<g:formatDate date="${tugasLuarKaryawanInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tugasLuarKaryawanInstance?.dateFinish}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateFinish-label" class="property-label"><g:message
					code="tugasLuarKaryawan.dateFinish.label" default="Date Finish" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateFinish-label">
						%{--<ba:editableValue
								bean="${tugasLuarKaryawanInstance}" field="dateFinish"
								url="${request.contextPath}/TugasLuarKaryawan/updatefield" type="text"
								title="Enter dateFinish" onsuccess="reloadTugasLuarKaryawanTable();" />--}%
							
								<g:formatDate date="${tugasLuarKaryawanInstance?.dateFinish}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tugasLuarKaryawanInstance?.dateFinishSchedule}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateFinishSchedule-label" class="property-label"><g:message
					code="tugasLuarKaryawan.dateFinishSchedule.label" default="Date Finish Schedule" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateFinishSchedule-label">
						%{--<ba:editableValue
								bean="${tugasLuarKaryawanInstance}" field="dateFinishSchedule"
								url="${request.contextPath}/TugasLuarKaryawan/updatefield" type="text"
								title="Enter dateFinishSchedule" onsuccess="reloadTugasLuarKaryawanTable();" />--}%
							
								<g:formatDate date="${tugasLuarKaryawanInstance?.dateFinishSchedule}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tugasLuarKaryawanInstance?.explanation}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="explanation-label" class="property-label"><g:message
					code="tugasLuarKaryawan.explanation.label" default="Explanation" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="explanation-label">
						%{--<ba:editableValue
								bean="${tugasLuarKaryawanInstance}" field="explanation"
								url="${request.contextPath}/TugasLuarKaryawan/updatefield" type="text"
								title="Enter explanation" onsuccess="reloadTugasLuarKaryawanTable();" />--}%
							
								<g:fieldValue bean="${tugasLuarKaryawanInstance}" field="explanation"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tugasLuarKaryawanInstance?.karyawan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="karyawan-label" class="property-label"><g:message
					code="tugasLuarKaryawan.karyawan.label" default="Karyawan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="karyawan-label">
						%{--<ba:editableValue
								bean="${tugasLuarKaryawanInstance}" field="karyawan"
								url="${request.contextPath}/TugasLuarKaryawan/updatefield" type="text"
								title="Enter karyawan" onsuccess="reloadTugasLuarKaryawanTable();" />--}%
							
								<g:link controller="karyawan" action="show" id="${tugasLuarKaryawanInstance?.karyawan?.id}">${tugasLuarKaryawanInstance?.karyawan?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tugasLuarKaryawanInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="tugasLuarKaryawan.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${tugasLuarKaryawanInstance}" field="lastUpdProcess"
								url="${request.contextPath}/TugasLuarKaryawan/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadTugasLuarKaryawanTable();" />--}%
							
								<g:fieldValue bean="${tugasLuarKaryawanInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tugasLuarKaryawanInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="tugasLuarKaryawan.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${tugasLuarKaryawanInstance}" field="lastUpdated"
								url="${request.contextPath}/TugasLuarKaryawan/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadTugasLuarKaryawanTable();" />--}%
							
								<g:formatDate date="${tugasLuarKaryawanInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tugasLuarKaryawanInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="tugasLuarKaryawan.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${tugasLuarKaryawanInstance}" field="staDel"
								url="${request.contextPath}/TugasLuarKaryawan/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadTugasLuarKaryawanTable();" />--}%
							
								<g:fieldValue bean="${tugasLuarKaryawanInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tugasLuarKaryawanInstance?.transportation}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="transportation-label" class="property-label"><g:message
					code="tugasLuarKaryawan.transportation.label" default="Transportation" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="transportation-label">
						%{--<ba:editableValue
								bean="${tugasLuarKaryawanInstance}" field="transportation"
								url="${request.contextPath}/TugasLuarKaryawan/updatefield" type="text"
								title="Enter transportation" onsuccess="reloadTugasLuarKaryawanTable();" />--}%
							
								<g:fieldValue bean="${tugasLuarKaryawanInstance}" field="transportation"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tugasLuarKaryawanInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="tugasLuarKaryawan.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${tugasLuarKaryawanInstance}" field="updatedBy"
								url="${request.contextPath}/TugasLuarKaryawan/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadTugasLuarKaryawanTable();" />--}%
							
								<g:fieldValue bean="${tugasLuarKaryawanInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('tugasLuarKaryawan');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${tugasLuarKaryawanInstance?.id}"
					update="[success:'tugasLuarKaryawan-form',failure:'tugasLuarKaryawan-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteTugasLuarKaryawan('${tugasLuarKaryawanInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
