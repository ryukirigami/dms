<%@ page import="com.kombos.hrd.TugasLuarKaryawan" %>


<g:javascript>
    $(function() {
        $("#btnCariKaryawan").click(function() {
            oFormService.fnShowKaryawanDataTable();
        })

        $('.disable_submit').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();

                return false;
            }
        });
    })
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: tugasLuarKaryawanInstance, field: 'karyawan', 'error')} ">
    <label class="control-label" for="karyawan">
        <g:message code="tugasLuarKaryawan.karyawan.label" default="Nama Karyawan" />

    </label>
    <div class="controls">
        <div class="input-append">
            <g:hiddenField name="karyawan.id" readonly="readonly" id="karyawan_id" value="${historyKaryawanInstance?.karyawan?.id}"/>
            <input type="text" value="${absensiKaryawanInstance?.karyawan?.nama}" readonly id="karyawan_nama"/>
            <span class="add-on">
                <a href="javascript:void(0);" id="btnCariKaryawan">
                    <i class="icon-search"/>
                </a>
            </span>
        </div>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: tugasLuarKaryawanInstance, field: 'dateBegin', 'error')} ">
    <label class="control-label" for="dateBegin">
        <g:message code="tugasLuarKaryawan.dateBegin.label" default="Tanggal Mulai Tugas" />

    </label>
    <div class="controls">
        <ba:datePicker name="dateBegin" precision="day" value="${tugasLuarKaryawanInstance?.dateBegin}" format="yyyy-MM-dd"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: tugasLuarKaryawanInstance, field: 'dateFinishSchedule', 'error')} ">
    <label class="control-label" for="dateFinishSchedule">
        <g:message code="tugasLuarKaryawan.dateFinishSchedule.label" default="Tanggal Rencana Akhir Tugas" />

    </label>
    <div class="controls">
        <ba:datePicker name="dateFinishSchedule" precision="day" value="${tugasLuarKaryawanInstance?.dateFinishSchedule}" format="yyyy-MM-dd"/>
    </div>
</div>

<div class="control-group fieldcontain">
    <label class="control-label" for="dateFinish">
        <g:message code="tugasLuarKaryawan.dateFinish.label" default="Tanggal Akhir Tugas" />

    </label>
    <div class="controls">
        <ba:datePicker name="dateFinish" precision="day" value="${tugasLuarKaryawanInstance?.dateFinish}" format="yyyy-MM-dd"/>
    </div>
</div>




<div class="control-group fieldcontain ${hasErrors(bean: tugasLuarKaryawanInstance, field: 'transportation', 'error')} ">
    <label class="control-label" for="transportation">
        <g:message code="tugasLuarKaryawan.transportation.label" default="Transportasi" />

    </label>
    <div class="controls">
        <g:textField name="transportation" value="${tugasLuarKaryawanInstance?.transportation}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: tugasLuarKaryawanInstance, field: 'assignmentReason', 'error')} ">
    <label class="control-label" for="assignmentReason">
        <g:message code="tugasLuarKaryawan.assignmentReason.label" default="Dasar Penugasan" />

    </label>
    <div class="controls">
        <g:textField name="assignmentReason" value="${tugasLuarKaryawanInstance?.assignmentReason}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: tugasLuarKaryawanInstance, field: 'assignmentObjectives', 'error')} ">
	<label class="control-label" for="assignmentObjectives">
		<g:message code="tugasLuarKaryawan.assignmentObjectives.label" default="Tujuan Tugas" />
		
	</label>
	<div class="controls">
	<g:textField name="assignmentObjectives" value="${tugasLuarKaryawanInstance?.assignmentObjectives}" />
	</div>
</div>


<div class="control-group fieldcontain">
    <label class="control-label" for="explanation">
        <g:message code="tugasLuarKaryawan.explanation.label" default="Keterangan" />

    </label>
    <div class="controls">
        <g:textField name="explanation" value="${tugasLuarKaryawanInstance?.explanation}" />
    </div>
</div>


<!-- Karyawan Modal -->

<div id="karyawanModal" class="modal fade">
    <div class="modal-dialog" style="width: 1100px;">
        <div class="modal-content" style="width: 1100px;">
            <div class="modal-header">
                <a class="close" href="javascript:void(0);" data-dismiss="modal">×</a>
                <h4>Data Karyawan - List</h4>
            </div>
            <!-- dialog body -->
            <div class="modal-body" id="karyawanModal-body" style="max-height: 1000px;">
                <div class="box">
                    <div class="span12">
                        <fieldset>
                            <table>
                                <tr style="display:table-row;">
                                    <td style="width: 130px; display:table-cell; padding:5px;">
                                        <label class="control-label" for="sCriteria_nama">Nama Karyawan</label>
                                    </td>
                                    <td style="width: 130px; display:table-cell; padding:5px;">
                                        <input type="text" id="sCriteria_nama" class="disable_submit">
                                    </td>
                                    <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                        <td style="width: 130px; display:table-cell; padding:5px;">
                                            <g:select name="sCriteria_cabang" id="sCriteria_cabang" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" noSelection="['':'SEMUA CABANG']"/>
                                        </td>
                                    </g:if>
                                    <td>
                                        <a id="btnSearchKaryawan" class="btn btn-primary" href="javascript:void(0);">
                                            Cari
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                </div>
                <g:render template="dataTablesKaryawan" />
            </div>

            <div class="modal-footer">
                <a href="javascript:void(0);" class="btn cancel" data-dismiss="modal">
                    Tutup
                </a>
                <a href="javascript:void(0);" id="btnPilihKaryawan" class="btn btn-primary">
                    Pilih Karyawan
                </a>
            </div>
        </div>
    </div>
</div>
