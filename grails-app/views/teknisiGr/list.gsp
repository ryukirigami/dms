
<%@ page import="com.kombos.administrasi.Operation; com.kombos.administrasi.NamaManPower; com.kombos.administrasi.ManPowerAbsensi" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'Teknisi.label', default: 'ManPower Rencana Hadir')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var closeAbsensi;
	$(function(){

	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;

       }    
       return false;
	});

    
   save  = function() {
    	  var formTeknisi = $('#Teknisi-table').find('form');
	      var jobKosong = $('#job').val();
	      if(jobKosong==null || jobKosong==0){
	            alert("Job Sudah Tidak Ada, Tidak Bisa Melanjutkan Pekerjaan");
	            return
	      }

   		 $.ajax({
            url:'${request.contextPath}/teknisiGr/save',
            type: "POST", // Always use POST when deleting data
            data : formTeknisi.serialize(),
            success : function(data){
                window.location.replace('#/jobForTeknisi')
                toastr.success('<div>Disimpan.</div>');
                if(params.aksi==4){
                    toastr.success('<div>Succes Save With Alert</div>');
                }
            },
        error: function(xhr, textStatus, errorThrown) {
        alert('Internal Server Error');
        }
        });
    };
    
    loadForm = function(data, textStatus){
		$('#Teknisi-form').empty();
    	$('#Teknisi-form').append(data);
   	}

    shrinkTableLayout = function(){
    	$("#Teknisi-table").hide()
        $("#Teknisi-form").css("display","block");
   	}
            $("#alasan1").prop('disabled', true);
            $("#alasan2").prop('disabled', true);
              if($('input[name="aksi"]:checked').val()==2){
                    $("#alasan1").prop('disabled', false);
                    $("#alasan2").prop('disabled', false);
                }else{
                    $("#alasan1").prop('disabled', true);
                    $("#alasan2").prop('disabled', true);
                }
   	        $('input:radio[name=aksi]').click(function(){

                if($('input[name="aksi"]:checked').val()==2){
                    $("#alasan1").prop('disabled', false);
                    $("#alasan2").prop('disabled', false);
                }else{
                    $("#alasan1").prop('disabled', true);
                    $("#alasan2").prop('disabled', true);
                     document.getElementById("alasan1").checked = false;
                     document.getElementById("alasan2").checked = false;
                }
             });
});

</g:javascript>


	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left">Teknisi</span>
		<ul class="nav pull-right">

		</ul>
	</div>
	<div class="box">
		<div class="span12" id="Teknisi-table">
            <g:if test="${id==''}">
                <div class="message" style="margin-left: 520px; color: red">
                    <button style="width: 170px;height: 40px; border-radius: 5px" class="btn delete" onclick="window.location.replace('#/jobForTeknisi')">Job For Teknisi</button>
                </div>
            </g:if>
            <g:else>
            <fieldset style="margin-left: 20px; padding: 10px;">
                <form id="form-teknisi" style="padding: 10px;">
                <table style="padding: 10px;">
                    <g:hiddenField name="id" value="${id}" />
                    <tr>
                        <td style="width: 150px;">Nomor WO</td>
                        <td><g:textField name="noWo" readonly="true" value="${noWo}" /> </td>
                    </tr>
                    <tr>
                        <td>Nomor Polisi</td>
                        <td><g:textField name="nopol" readonly="true" value="${nopol}" /> </td>
                    </tr>
                    <tr>
                        <td>Model Kendaraan</td>
                        <td><g:textField name="model" readonly="true" value="${model}" /> </td>
                    </tr>
                    <tr>
                        <td>Nama Stall</td>
                        <td><g:textField name="stall" readonly="true" value="${stall}" /> </td>
                    </tr>
                    <tr>
                        <td>Teknisi</td>
                        <td>
                            <g:select id="teknisi" name="teknisi.id" optionKey="id" required=""
                                      from="${NamaManPower.createCriteria().list {
                                                eq("staDel","0")
                                                eq("companyDealer", session.userCompanyDealer)
                                                manPowerDetail{
                                                    manPower{
                                                          inList("m014Inisial", ["TKGR", "TSP"])
                                                    }
                                                }
                                      }}" optionValue="t015NamaLengkap" value="${nama}" disabled="true"/>
                        </td>
                    </tr>
                    <tr>
                        <td>Job</td>
                        <td>
                            <g:if test="${staReadOnly==true}">
                                <g:select id="job" name="job.id" optionKey="id" from="${namaJob}" />
                            </g:if>
                            %{--<g:if test="${staReadOnly==true}">--}%
                                %{--<g:textField name="jobSimpen" value="${dataJob}" />--}%
                                %{--<g:hiddenField name="job.id" value="${idJob}" />--}%
                            %{--</g:if>--}%
                            <g:else>
                                <g:if test="${namaJob.size()>0}">
                                    <g:select id="job" name="job.id" optionKey="id" from="${namaJob}" />
                                </g:if>
                                <g:else>
                                  Maaf Job Sudah Tidak Tersedia
                                </g:else>
                            </g:else>
                        </td>
                    </tr>
                    <tr>
                        <td>Status Kendaraan</td>
                        <td>
                            <g:textField name="status" readonly="true" value="${status}" />
                        </td>
                    </tr>
                    <tr style="height: 40px">
                        <td>Aksi * </td>
                        <td>
                            <g:if test="${radio==0}">
                                <g:radio name="aksi" value="1" checked="checked"/> Clock On &nbsp; &nbsp;
                                <g:radio name="aksi" value="2" disabled="true"/> Panggil Foreman &nbsp; &nbsp;
                                <g:radio name="aksi" value="4" disabled="true"/> Clock Off
                            </g:if>
                            <g:elseif test="${radio == 2}">
                                <g:radio name="aksi" value="1" disabled="true"/> Clock On &nbsp; &nbsp;
                                <g:radio name="aksi" value="3" required="" checked="checked"/> Resume &nbsp; &nbsp;
                                <g:radio name="aksi" value="4" required=""/> Clock Off
                            </g:elseif>
                            <g:elseif test="${radio==1 || radio==2 || radio==3}">
                                <g:radio name="aksi" value="1"  disabled="true"/> Clock On &nbsp; &nbsp;
                                <g:radio name="aksi" value="2"  required=""/> Panggil Foreman &nbsp; &nbsp;
                                <g:radio name="aksi" value="4"  required="" checked="checked"/> Clock Off
                            </g:elseif>
                            <g:elseif test="${radio == 4}">
                                <g:radio name="aksi" value="1" required="" checked="checked"/> Clock On &nbsp; &nbsp;
                                <g:radio name="aksi" value="2" disabled="true"/> Panggil Foreman &nbsp; &nbsp;
                                <g:radio name="aksi" value="4" disabled="true" /> Clock Off
                            </g:elseif>
                        </td>
                    </tr>
                    <tr style="height: 40px">
                        <td>Alasan Pause</td>
                        <td>
                            <g:if test="${alasan == 1}">
                                <g:radio name="alasan" id="alasan1" value="1" checked="checked" disabled=""/> Teknis &nbsp; &nbsp;
                                <g:radio name="alasan" id="alasan2" value="0" disabled=""/> Non Teknis
                            </g:if>
                            <g:elseif test="${alasan==0}">
                                <g:radio name="alasan" id="alasan1" value="1" disabled=""/> Teknis &nbsp; &nbsp;
                                <g:radio name="alasan" id="alasan2" value="0" checked="checked" disabled=""/> Non Teknis
                            </g:elseif>
                            <g:else>
                                <g:radio name="alasan" id="alasan1" value="1"  disabled=""/> Teknis &nbsp; &nbsp;
                                <g:radio name="alasan" id="alasan2" value="0"  disabled=""/> Non Teknis
                            </g:else>

                        </td>
                    </tr>
                    <tr style="height: 40px">
                        <td> </td>
                        <td style="right: 0px"> <button id='btn3' onclick="window.location.replace('#/foreman')" type="button" class="btn cancel">Lihat Status Foreman</button></td>
                    </tr>
                    <tr>
                        <td>Catatan Tambahan</td>
                        <td><g:textArea name="catatan" cols="50" rows="5" style="font-size:14px; resize:none" value="${catatan}" /> </td>
                    </tr>
                    <tr>
                        <td>Tanggal Update</td>
                        <td> <g:textField name="tanggal" readonly="true" value="${tanggal}" /> </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>   <button id='btn' onclick="save();" type="button" class="btn btn-primary">Save</button>
                            <button id='btn2' onclick="window.location.replace('#/jobForTeknisi')" type="button" class="btn cancel">Cancel</button> </td>
                    </tr>
                </table>
               </form>
            </fieldset>
            <fieldset class="buttons controls" style="padding-top: 10px; margin-left: 430px;">
                <g:if test="${namaJob.size()>0}">

                </g:if>

            </fieldset>
            </g:else>
		</div>
	</div>
</body>
</html>
