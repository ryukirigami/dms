
<%@ page import="com.kombos.administrasi.Gear" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="gear_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="gear.baseModel.label" default="Base Model" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="gear.modelName.label" default="Model Name" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="gear.bodyType.label" default="Body Type" /></div>
			</th>



			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="gear.m106KodeGear.label" default="Kode Gear" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="gear.m106NamaGear.label" default="Nama Gear" /></div>
			</th>

%{--
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="gear.staDel.label" default="Sta Del" /></div>
			</th>
--}%
		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_baseModel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_baseModel" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_modelName" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_modelName" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_bodyType" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_bodyType" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m106KodeGear" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m106KodeGear" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m106NamaGear" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m106NamaGear" class="search_init" />
				</div>
			</th>
%{--	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_staDel" class="search_init" />
				</div>
			</th>
--}%

		</tr>
	</thead>
</table>

<g:javascript>
    var GearTable;
    var reloadGearTable;
    $(function(){

        reloadGearTable = function() {
            GearTable.fnDraw();
        }

        var recordsGearperpage = [];
        var anGearSelected;
        var jmlRecGearPerPage=0;
        var id;


        $("th div input").bind('keypress', function(e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if(code == 13) {
                e.stopPropagation();
                GearTable.fnDraw();
            }
        });
        $("th div input").click(function (e) {
            e.stopPropagation();
        });

        GearTable = $('#gear_datatables').dataTable({
            "sScrollX": "100%",
            "bScrollCollapse": true,
            "bAutoWidth" : false,
            "bPaginate" : true,
            "sInfo" : "",
            "sInfoEmpty" : "",
            "sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
            bFilter: true,
            "bStateSave": false,
            'sPaginationType': 'bootstrap',
            "fnDrawCallback": function () {
                //alert( $('li.active').text() );
                var rsGear = $("#gear_datatables tbody .row-select");
                var jmlGearCek = 0;
                var idRec;
                var nRow;
                rsGear.each(function() {
                    idRec = $(this).next("input:hidden").val();
                    nRow = $(this).parent().parent();//.addClass('row_selected');
                    if(recordsGearperpage[idRec]=="1"){
                        jmlGearCek = jmlGearCek + 1;
                        $(this).attr('checked', true);
                        nRow.addClass('row_selected');
                    } else if(recordsGearperpage[idRec]=="0"){
                        $(this).attr('checked', false);
                        nRow.removeClass('row_selected');
                    }

                });
                jmlRecGearPerPage = rsGear.length;
                if(jmlGearCek==jmlRecGearPerPage && jmlRecGearPerPage>0){
                    $('.select-all').attr('checked', true);
                } else {
                    $('.select-all').attr('checked', false);
                }
                //alert("apakah ini : "+rs.length);
            },
            "fnInitComplete": function () {
                this.fnAdjustColumnSizing(true);
            },
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                return nRow;
            },
            "bSort": true,
            "bProcessing": true,
            "bServerSide": true,
            "sServerMethod": "POST",
            "iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
            "sAjaxSource": "${g.createLink(action: "datatablesList")}",
            "aoColumns": [
                {
                    "sName": "baseModel",
                    "mDataProp": "baseModel",
                    "aTargets": [0],
                    "mRender": function ( data, type, row ) {
                        return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
                    },
                    "bSearchable": true,
                    "bSortable": true,
                    "sWidth":"300px",
                    "bVisible": true
                }

                ,

                {
                    "sName": "modelName",
                    "mDataProp": "modelName",
                    "aTargets": [1],
                    "bSearchable": true,
                    "bSortable": true,
                    "sWidth":"200px",
                    "bVisible": true
                }

                ,

                {
                    "sName": "bodyType",
                    "mDataProp": "bodyType",
                    "aTargets": [2],
                    "bSearchable": true,
                    "bSortable": true,
                    "sWidth":"200px",
                    "bVisible": true
                }

                ,

                {
                    "sName": "m106KodeGear",
                    "mDataProp": "m106KodeGear",
                    "aTargets": [3],
                    "bSearchable": true,
                    "bSortable": true,
                    "sWidth":"200px",
                    "bVisible": true
                }

                ,

                {
                    "sName": "m106NamaGear",
                    "mDataProp": "m106NamaGear",
                    "aTargets": [4],
                    "bSearchable": true,
                    "bSortable": true,
                    "sWidth":"200px",
                    "bVisible": true
                }

                ,

                {
                    "sName": "staDel",
                    "mDataProp": "staDel",
                    "aTargets": [5],
                    "bSearchable": false,
                    "bSortable": false,
                    "sWidth":"200px",
                    "bVisible": false
                }
            ],
            "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                var baseModel = $('#filter_baseModel input').val();
                if(baseModel){
                    aoData.push(
                            {"name": 'sCriteria_baseModel', "value": baseModel}
                    );
                }

                var modelName = $('#filter_modelName input').val();
                if(modelName){
                    aoData.push(
                            {"name": 'sCriteria_modelName', "value": modelName}
                    );
                }

                var bodyType = $('#filter_bodyType input').val();
                if(bodyType){
                    aoData.push(
                            {"name": 'sCriteria_bodyType', "value": bodyType}
                    );
                }

                var m106KodeGear = $('#filter_m106KodeGear input').val();
                if(m106KodeGear){
                    aoData.push(
                            {"name": 'sCriteria_m106KodeGear', "value": m106KodeGear}
                    );
                }

                var m106NamaGear = $('#filter_m106NamaGear input').val();
                if(m106NamaGear){
                    aoData.push(
                            {"name": 'sCriteria_m106NamaGear', "value": m106NamaGear}
                    );
                }

                var staDel = $('#filter_staDel input').val();
                if(staDel){
                    aoData.push(
                            {"name": 'sCriteria_staDel', "value": staDel}
                    );
                }

                $.ajax({ "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData ,
                    "success": function (json) {
                        fnCallback(json);
                       },
                    "complete": function () {
                       }
                });
            }
        });

        $('.select-all').click(function(e) {
            $("#gear_datatables tbody .row-select").each(function() {
                if(this.checked){
                    recordsGearperpage[$(this).next("input:hidden").val()] = "1";
                } else {
                    recordsGearperpage[$(this).next("input:hidden").val()] = "0";
                }
            });
        });

        $('#gear_datatables tbody tr').live('click', function () {
            id = $(this).find('.row-select').next("input:hidden").val();
            if($(this).find('.row-select').is(":checked")){
                recordsGearperpage[id] = "1";
                $(this).find('.row-select').parent().parent().addClass('row_selected');
                anGearSelected = GearTable.$('tr.row_selected');
                if(jmlRecGearPerPage == anGearSelected.length){
                    $('.select-all').attr('checked', true);
                }
            } else {
                recordsGearperpage[id] = "0";
                $('.select-all').attr('checked', false);
                $(this).find('.row-select').parent().parent().removeClass('row_selected');
            }
        });

    });
</g:javascript>


			
