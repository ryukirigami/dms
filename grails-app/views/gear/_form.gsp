<%@ page import="com.kombos.administrasi.BodyType; com.kombos.administrasi.ModelName; com.kombos.administrasi.BaseModel; com.kombos.administrasi.Gear" %>

<g:javascript>
    var selectModelName;
    var selectBodyType;
			$(function(){
			selectModelName = function () {
			var idBaseModel = $('#baseModel option:selected').val();
			    if(idBaseModel != undefined){
                    jQuery.getJSON('${request.contextPath}/bodyType/getModelNames?idBaseModel='+idBaseModel, function (data) {
                            $('#modelName').empty();
                            if (data) {
                                jQuery.each(data, function (index, value) {
                                    $('#modelName').append("<option value='" + value.id + "'>" + value.kodeModelName + "</option>");
                                });
                            }
                            selectBodyType();
                        });
                }else{
                     $('#modelName').empty();
                     $('#bodyType').empty();
                }
            };

			selectBodyType = function () {
			var idModelName = $('#modelName').val();
			    if(idModelName != undefined){
                    jQuery.getJSON('${request.contextPath}/bodyType/getBodyTypes?idModelName='+idModelName, function (data) {
                            $('#bodyType').empty();
                            if (data) {
                                jQuery.each(data, function (index, value) {
                                    $('#bodyType').append("<option value='" + value.id + "'>" + value.kodeBodyType + "</option>");
                                });
                            }
                        });
                }else{
                     $('#bodyType').empty();
                }
            };
    });
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: gearInstance, field: 'baseModel', 'error')} required">
	<label class="control-label" for="baseModel">
		<g:message code="gear.baseModel.label" default="Base Model" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="baseModel" onchange="selectModelName();" name="baseModel.id" noSelection="['':'Pilih Base Model']" from="${BaseModel.createCriteria().list(){eq("staDel", "0");order("m102KodeBaseModel", "asc")}}" optionValue="${{it.m102KodeBaseModel + " - " + it.m102NamaBaseModel}}" optionKey="id" required="" value="${gearInstance?.baseModel?.id}" class="many-to-one"/>
	</div>
</div>

<g:javascript>
    document.getElementById("baseModel").focus();
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: gearInstance, field: 'modelName', 'error')} required">
	<label class="control-label" for="modelName">
		<g:message code="gear.modelName.label" default="Model Name" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="modelName" onchange="selectBodyType();" name="modelName.id" noSelection="['':'Pilih Model Name']" from="${ModelName.createCriteria().list(){eq("staDel", "0");order("m104KodeModelName", "asc")}}" optionValue="${{it.m104KodeModelName + " - " + it.m104NamaModelName}}" optionKey="id" required="" value="${gearInstance?.modelName?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: gearInstance, field: 'bodyType', 'error')} required">
	<label class="control-label" for="bodyType">
		<g:message code="gear.bodyType.label" default="Body Type" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="bodyType" name="bodyType.id" noSelection="['':'Pilih Body Type']" from="${BodyType.createCriteria().list(){eq("staDel", "0");order("m105KodeBodyType", "asc")}}" optionValue="${{it.m105KodeBodyType + " - " + it.m105NamaBodyType}}" optionKey="id" required="" value="${gearInstance?.bodyType?.id}" class="many-to-one"/>
	</div>
</div>
%{--
<div class="control-group fieldcontain ${hasErrors(bean: gearInstance, field: 'm106ID', 'error')} ">
	<label class="control-label" for="m106ID">
		<g:message code="gear.m106ID.label" default="ID" />
		
	</label>
	<div class="controls">
	<g:field name="m106ID" type="number" value="${gearInstance.m106ID}"/>
	</div>
</div>
--}%
<div class="control-group fieldcontain ${hasErrors(bean: gearInstance, field: 'm106KodeGear', 'error')} required">
	<label class="control-label" for="m106KodeGear">
		<g:message code="gear.m106KodeGear.label" default="Kode Gear" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m106KodeGear" maxlength="2" required="" value="${gearInstance?.m106KodeGear}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: gearInstance, field: 'm106NamaGear', 'error')} required">
	<label class="control-label" for="m106NamaGear">
		<g:message code="gear.m106NamaGear.label" default="Nama Gear" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m106NamaGear" maxlength="50" required="" value="${gearInstance?.m106NamaGear}"/>
	</div>
</div>
%{--
<div class="control-group fieldcontain ${hasErrors(bean: gearInstance, field: 'staDel', 'error')} required">
	<label class="control-label" for="staDel">
		<g:message code="gear.staDel.label" default="Sta Del" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="staDel" required="" value="${gearInstance?.staDel}"/>
	</div>
</div>
--}%
