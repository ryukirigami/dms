

<%@ page import="com.kombos.administrasi.Gear" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'gear.label', default: 'Gear')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteGear;

$(function(){ 
	deleteGear=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/gear/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadGearTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-gear" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="gear"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${gearInstance?.baseModel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="baseModel-label" class="property-label"><g:message
					code="gear.baseModel.label" default="Base Model" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="baseModel-label">
						%{--<ba:editableValue
								bean="${gearInstance}" field="baseModel"
								url="${request.contextPath}/Gear/updatefield" type="text"
								title="Enter baseModel" onsuccess="reloadGearTable();" />--}%
							
								${gearInstance?.baseModel?.m102NamaBaseModel?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${gearInstance?.modelName}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="modelName-label" class="property-label"><g:message
					code="gear.modelName.label" default="Model Name" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="modelName-label">
						%{--<ba:editableValue
								bean="${gearInstance}" field="modelName"
								url="${request.contextPath}/Gear/updatefield" type="text"
								title="Enter modelName" onsuccess="reloadGearTable();" />--}%
							
								${gearInstance?.modelName?.m104NamaModelName?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${gearInstance?.bodyType}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="bodyType-label" class="property-label"><g:message
					code="gear.bodyType.label" default="Body Type" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="bodyType-label">
						%{--<ba:editableValue
								bean="${gearInstance}" field="bodyType"
								url="${request.contextPath}/Gear/updatefield" type="text"
								title="Enter bodyType" onsuccess="reloadGearTable();" />--}%
							
								${gearInstance?.bodyType?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
<!--			
				<g:if test="${gearInstance?.m106ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m106ID-label" class="property-label"><g:message
					code="gear.m106ID.label" default="ID" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m106ID-label">
						%{--<ba:editableValue
								bean="${gearInstance}" field="m106ID"
								url="${request.contextPath}/Gear/updatefield" type="text"
								title="Enter m106ID" onsuccess="reloadGearTable();" />--}%
							
								<g:fieldValue bean="${gearInstance}" field="m106ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
-->
				<g:if test="${gearInstance?.m106KodeGear}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m106KodeGear-label" class="property-label"><g:message
					code="gear.m106KodeGear.label" default="Kode Gear" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m106KodeGear-label">
						%{--<ba:editableValue
								bean="${gearInstance}" field="m106KodeGear"
								url="${request.contextPath}/Gear/updatefield" type="text"
								title="Enter m106KodeGear" onsuccess="reloadGearTable();" />--}%
							
								<g:fieldValue bean="${gearInstance}" field="m106KodeGear"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${gearInstance?.m106NamaGear}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m106NamaGear-label" class="property-label"><g:message
					code="gear.m106NamaGear.label" default="Nama Gear" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m106NamaGear-label">
						%{--<ba:editableValue
								bean="${gearInstance}" field="m106NamaGear"
								url="${request.contextPath}/Gear/updatefield" type="text"
								title="Enter m106NamaGear" onsuccess="reloadGearTable();" />--}%
							
								<g:fieldValue bean="${gearInstance}" field="m106NamaGear"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${gearInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="gear.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${gearInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/gear/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadgearTable();" />--}%

                        <g:fieldValue bean="${gearInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${gearInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="gear.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${gearInstance}" field="dateCreated"
                                url="${request.contextPath}/gear/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadgearTable();" />--}%

                        <g:formatDate date="${gearInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${gearInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="gear.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${gearInstance}" field="createdBy"
                                url="${request.contextPath}/gear/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadgearTable();" />--}%

                        <g:fieldValue bean="${gearInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${gearInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="gear.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${gearInstance}" field="lastUpdated"
                                url="${request.contextPath}/gear/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadgearTable();" />--}%

                        <g:formatDate date="${gearInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${gearInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="gear.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${gearInstance}" field="updatedBy"
                                url="${request.contextPath}/gear/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadgearTable();" />--}%

                        <g:fieldValue bean="${gearInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
<!--			
				<g:if test="${gearInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="gear.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${gearInstance}" field="staDel"
								url="${request.contextPath}/Gear/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadGearTable();" />--}%
							
								<g:fieldValue bean="${gearInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
-->			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${gearInstance?.id}"
					update="[success:'gear-form',failure:'gear-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteGear('${gearInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
