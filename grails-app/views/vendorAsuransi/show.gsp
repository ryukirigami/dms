

<%@ page import="com.kombos.administrasi.VendorAsuransi" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'vendorAsuransi.label', default: 'Master Vendor Asuransi')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteVendorAsuransi;

$(function(){ 
	deleteVendorAsuransi=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/vendorAsuransi/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadVendorAsuransiTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-vendorAsuransi" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="vendorAsuransi"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${vendorAsuransiInstance?.m193Nama}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m193Nama-label" class="property-label"><g:message
					code="vendorAsuransi.m193Nama.label" default="Nama Asuransi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m193Nama-label">
						%{--<ba:editableValue
								bean="${vendorAsuransiInstance}" field="m193Nama"
								url="${request.contextPath}/VendorAsuransi/updatefield" type="text"
								title="Enter m193Nama" onsuccess="reloadVendorAsuransiTable();" />--}%
							
								<g:fieldValue bean="${vendorAsuransiInstance}" field="m193Nama"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vendorAsuransiInstance?.m193Alamat}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m193Alamat-label" class="property-label"><g:message
					code="vendorAsuransi.m193Alamat.label" default="Alamat Asuransi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m193Alamat-label">
						%{--<ba:editableValue
								bean="${vendorAsuransiInstance}" field="m193Alamat"
								url="${request.contextPath}/VendorAsuransi/updatefield" type="text"
								title="Enter m193Alamat" onsuccess="reloadVendorAsuransiTable();" />--}%
							
								<g:fieldValue bean="${vendorAsuransiInstance}" field="m193Alamat"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vendorAsuransiInstance?.m193Npwp}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m193Npwp-label" class="property-label"><g:message
					code="vendorAsuransi.m193Npwp.label" default="N.P.W.P" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m193Npwp-label">
						%{--<ba:editableValue
								bean="${vendorAsuransiInstance}" field="m193Npwp"
								url="${request.contextPath}/VendorAsuransi/updatefield" type="text"
								title="Enter m193Npwp" onsuccess="reloadVendorAsuransiTable();" />--}%
							
								<g:fieldValue bean="${vendorAsuransiInstance}" field="m193Npwp"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vendorAsuransiInstance?.m193NoTelp}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m193NoTelp-label" class="property-label"><g:message
					code="vendorAsuransi.m193NoTelp.label" default="Telepon" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m193NoTelp-label">
						%{--<ba:editableValue
								bean="${vendorAsuransiInstance}" field="m193NoTelp"
								url="${request.contextPath}/VendorAsuransi/updatefield" type="text"
								title="Enter m193NoTelp" onsuccess="reloadVendorAsuransiTable();" />--}%
							
								<g:fieldValue bean="${vendorAsuransiInstance}" field="m193NoTelp"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vendorAsuransiInstance?.m193Email}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m193Email-label" class="property-label"><g:message
					code="vendorAsuransi.m193Email.label" default="Email" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m193Email-label">
						%{--<ba:editableValue
								bean="${vendorAsuransiInstance}" field="m193Email"
								url="${request.contextPath}/VendorAsuransi/updatefield" type="text"
								title="Enter m193Email" onsuccess="reloadVendorAsuransiTable();" />--}%
							
								<g:fieldValue bean="${vendorAsuransiInstance}" field="m193Email"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vendorAsuransiInstance?.m193JmlToleransiAsuransiBP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m193JmlToleransiAsuransiBP-label" class="property-label"><g:message
					code="vendorAsuransi.m193JmlToleransiAsuransiBP.label" default="Jumlah Toleransi bisa Produksi tanpa SPK" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m193JmlToleransiAsuransiBP-label">
						%{--<ba:editableValue
								bean="${vendorAsuransiInstance}" field="m193JmlToleransiAsuransiBP"
								url="${request.contextPath}/VendorAsuransi/updatefield" type="text"
								title="Enter m193JmlToleransiAsuransiBP" onsuccess="reloadVendorAsuransiTable();" />--}%
							
								<g:fieldValue bean="${vendorAsuransiInstance}" field="m193JmlToleransiAsuransiBP"/>
							
						</span></td>
					
				</tr>
				</g:if>
<!--			
				<g:if test="${vendorAsuransiInstance?.m193ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m193ID-label" class="property-label"><g:message
					code="vendorAsuransi.m193ID.label" default="M193 ID" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m193ID-label">
						%{--<ba:editableValue
								bean="${vendorAsuransiInstance}" field="m193ID"
								url="${request.contextPath}/VendorAsuransi/updatefield" type="text"
								title="Enter m193ID" onsuccess="reloadVendorAsuransiTable();" />--}%
							
								<g:fieldValue bean="${vendorAsuransiInstance}" field="m193ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vendorAsuransiInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="vendorAsuransi.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${vendorAsuransiInstance}" field="staDel"
								url="${request.contextPath}/VendorAsuransi/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadVendorAsuransiTable();" />--}%
							
								<g:fieldValue bean="${vendorAsuransiInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
-->			

			
				<g:if test="${vendorAsuransiInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="vendorAsuransi.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${vendorAsuransiInstance}" field="lastUpdProcess"
								url="${request.contextPath}/VendorAsuransi/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadVendorAsuransiTable();" />--}%
							
								<g:fieldValue bean="${vendorAsuransiInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vendorAsuransiInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="vendorAsuransi.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${vendorAsuransiInstance}" field="dateCreated"
								url="${request.contextPath}/VendorAsuransi/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadVendorAsuransiTable();" />--}%
							
								<g:formatDate date="${vendorAsuransiInstance?.dateCreated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if><g:if test="${vendorAsuransiInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="vendorAsuransi.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${vendorAsuransiInstance}" field="createdBy"
                                url="${request.contextPath}/VendorAsuransi/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadVendorAsuransiTable();" />--}%

                        <g:fieldValue bean="${vendorAsuransiInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${vendorAsuransiInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="vendorAsuransi.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${vendorAsuransiInstance}" field="lastUpdated"
								url="${request.contextPath}/VendorAsuransi/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadVendorAsuransiTable();" />--}%
							
								<g:formatDate date="${vendorAsuransiInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${vendorAsuransiInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="vendorAsuransi.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${vendorAsuransiInstance}" field="updatedBy"
                                url="${request.contextPath}/VendorAsuransi/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadVendorAsuransiTable();" />--}%

                        <g:fieldValue bean="${vendorAsuransiInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${vendorAsuransiInstance?.id}"
					update="[success:'vendorAsuransi-form',failure:'vendorAsuransi-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteVendorAsuransi('${vendorAsuransiInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
