
<%@ page import="com.kombos.administrasi.VendorAsuransi" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>
<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode != 45  && charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</g:javascript>

<table id="vendorAsuransi_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vendorAsuransi.m193Nama.label" default="Nama Asuransi" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vendorAsuransi.m193Alamat.label" default="Alamat Asuransi" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vendorAsuransi.m193Npwp.label" default="N.P.W.P" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vendorAsuransi.m193NoTelp.label" default="Telepon" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vendorAsuransi.m193Email.label" default="Email" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vendorAsuransi.m193JmlToleransiAsuransiBP.label" default="Toleransi bisa Pro tanpa SPK" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vendorAsuransi.m193ID.label" default="M193 ID" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vendorAsuransi.staDel.label" default="Sta Del" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vendorAsuransi.createdBy.label" default="Created By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vendorAsuransi.updatedBy.label" default="Updated By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vendorAsuransi.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m193Nama" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m193Nama" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m193Alamat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m193Alamat" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m193Npwp" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m193Npwp" onkeypress="return isNumberKey(event)" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m193NoTelp" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m193NoTelp" onkeypress="return isNumberKey(event)" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m193Email" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m193Email" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m193JmlToleransiAsuransiBP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m193JmlToleransiAsuransiBP" onkeypress="return isNumberKey(event)" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m193ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m193ID" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_staDel" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_createdBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_updatedBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var vendorAsuransiTable;
var reloadVendorAsuransiTable;
$(function(){
	
	reloadVendorAsuransiTable = function() {
		vendorAsuransiTable.fnDraw();
	}

	var recordsVendorAsuransiPerPage = [];
    var anVendorAsuransiSelected;
    var jmlRecVendorAsuransiPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	vendorAsuransiTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	vendorAsuransiTable = $('#vendorAsuransi_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsVendorAsuransi = $("#vendorAsuransi_datatables tbody .row-select");
            var jmlVendorAsuransiCek = 0;
            var nRow;
            var idRec;
            rsVendorAsuransi.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsVendorAsuransiPerPage[idRec]=="1"){
                    jmlVendorAsuransiCek = jmlVendorAsuransiCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsVendorAsuransiPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecVendorAsuransiPerPage = rsVendorAsuransi.length;
            if(jmlVendorAsuransiCek==jmlRecVendorAsuransiPerPage && jmlRecVendorAsuransiPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m193Nama",
	"mDataProp": "m193Nama",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m193Alamat",
	"mDataProp": "m193Alamat",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m193Npwp",
	"mDataProp": "m193Npwp",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m193NoTelp",
	"mDataProp": "m193NoTelp",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m193Email",
	"mDataProp": "m193Email",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m193JmlToleransiAsuransiBP",
	"mDataProp": "m193JmlToleransiAsuransiBP",
	"aTargets": [5],
	"mRender": function ( data, type, row ) {
        if(data != null)
        return '<span class="pull-right numeric">'+data+'</span>';
        else
        return '<span></span>';
    },
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m193ID",
	"mDataProp": "m193ID",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "staDel",
	"mDataProp": "staDel",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "createdBy",
	"mDataProp": "createdBy",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "updatedBy",
	"mDataProp": "updatedBy",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m193Nama = $('#filter_m193Nama input').val();
						if(m193Nama){
							aoData.push(
									{"name": 'sCriteria_m193Nama', "value": m193Nama}
							);
						}
	
						var m193Alamat = $('#filter_m193Alamat input').val();
						if(m193Alamat){
							aoData.push(
									{"name": 'sCriteria_m193Alamat', "value": m193Alamat}
							);
						}
	
						var m193Npwp = $('#filter_m193Npwp input').val();
						if(m193Npwp){
							aoData.push(
									{"name": 'sCriteria_m193Npwp', "value": m193Npwp}
							);
						}
	
						var m193NoTelp = $('#filter_m193NoTelp input').val();
						if(m193NoTelp){
							aoData.push(
									{"name": 'sCriteria_m193NoTelp', "value": m193NoTelp}
							);
						}
	
						var m193Email = $('#filter_m193Email input').val();
						if(m193Email){
							aoData.push(
									{"name": 'sCriteria_m193Email', "value": m193Email}
							);
						}
	
						var m193JmlToleransiAsuransiBP = $('#filter_m193JmlToleransiAsuransiBP input').val();
						if(m193JmlToleransiAsuransiBP){
							aoData.push(
									{"name": 'sCriteria_m193JmlToleransiAsuransiBP', "value": m193JmlToleransiAsuransiBP}
							);
						}
	
						var m193ID = $('#filter_m193ID input').val();
						if(m193ID){
							aoData.push(
									{"name": 'sCriteria_m193ID', "value": m193ID}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							$('td span.numeric').text(function(index, text) {
                                    return $.number(text,0);
                                    });
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#vendorAsuransi_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsVendorAsuransiPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsVendorAsuransiPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#vendorAsuransi_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsVendorAsuransiPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anVendorAsuransiSelected = vendorAsuransiTable.$('tr.row_selected');
            if(jmlRecVendorAsuransiPerPage == anVendorAsuransiSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsVendorAsuransiPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
