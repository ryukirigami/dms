<%@ page import="com.kombos.administrasi.VendorAsuransi" %>
<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode != 45  && charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    $(document).ready(function() {
        if($('#m193Alamat').val().length>0){
            $('#hitung').text(250 - $('#m193Alamat').val().length);
        }
        $('#m193Alamat').keyup(function() {
            var len = this.value.length;
            if (len >= 250) {
                this.value = this.value.substring(0, 250);
                len = 250;
            }
            $('#hitung').text(250 - len);
        });
    });
</g:javascript>


<div class="control-group fieldcontain ${hasErrors(bean: vendorAsuransiInstance, field: 'm193Nama', 'error')} required">
	<label class="control-label" for="m193Nama">
		<g:message code="vendorAsuransi.m193Nama.label" default="Nama Asuransi" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m193Nama" maxlength="100" required="" value="${vendorAsuransiInstance?.m193Nama}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorAsuransiInstance, field: 'm193Alamat', 'error')} required">
	<label class="control-label" for="m193Alamat">
		<g:message code="vendorAsuransi.m193Alamat.label" default="Alamat Asuransi" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textArea name="m193Alamat" id="m193Alamat" required="" value="${vendorAsuransiInstance?.m193Alamat}"/>
        <br/>
        <span id="hitung">250</span> Karakter Tersisa.
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorAsuransiInstance, field: 'm193Npwp', 'error')} required">
	<label class="control-label" for="m193Npwp">
		<g:message code="vendorAsuransi.m193Npwp.label" default="N.P.W.P" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m193Npwp" maxlength="20" required="" value="${vendorAsuransiInstance?.m193Npwp}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorAsuransiInstance, field: 'm193NoTelp', 'error')} required">
	<label class="control-label" for="m193NoTelp">
		<g:message code="vendorAsuransi.m193NoTelp.label" default="Telepon" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m193NoTelp" onkeypress="return isNumberKey(event)" maxlength="15" required="" value="${vendorAsuransiInstance?.m193NoTelp}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorAsuransiInstance, field: 'm193Email', 'error')} required">
	<label class="control-label" for="m193Email">
		<g:message code="vendorAsuransi.m193Email.label" default="Email" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m193Email" maxlength="50" required="" value="${vendorAsuransiInstance?.m193Email}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorAsuransiInstance, field: 'm193JmlToleransiAsuransiBP', 'error')} required">
	<label class="control-label" for="m193JmlToleransiAsuransiBP">
		<g:message code="vendorAsuransi.m193JmlToleransiAsuransiBP.label" default="Jumlah Toleransi bisa Produksi tanpa SPK" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField maxlength="15" name="m193JmlToleransiAsuransiBP" onkeypress="return isNumberKey(event)" value="${fieldValue(bean: vendorAsuransiInstance, field: 'm193JmlToleransiAsuransiBP')}" required=""/>
	</div>
</div>
%{--
<div class="control-group fieldcontain ${hasErrors(bean: vendorAsuransiInstance, field: 'm193ID', 'error')} ">
	<label class="control-label" for="m193ID">
		<g:message code="vendorAsuransi.m193ID.label" default="M193 ID" />
		
	</label>
	<div class="controls">
	<g:textField name="m193ID" maxlength="3" value="${vendorAsuransiInstance?.m193ID}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorAsuransiInstance, field: 'staDel', 'error')} required">
	<label class="control-label" for="staDel">
		<g:message code="vendorAsuransi.staDel.label" default="Sta Del" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="staDel" maxlength="1" required="" value="${vendorAsuransiInstance?.staDel}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorAsuransiInstance, field: 'createdBy', 'error')} ">
	<label class="control-label" for="createdBy">
		<g:message code="vendorAsuransi.createdBy.label" default="Created By" />
		
	</label>
	<div class="controls">
	<g:textField name="createdBy" value="${vendorAsuransiInstance?.createdBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorAsuransiInstance, field: 'updatedBy', 'error')} ">
	<label class="control-label" for="updatedBy">
		<g:message code="vendorAsuransi.updatedBy.label" default="Updated By" />
		
	</label>
	<div class="controls">
	<g:textField name="updatedBy" value="${vendorAsuransiInstance?.updatedBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorAsuransiInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="vendorAsuransi.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${vendorAsuransiInstance?.lastUpdProcess}"/>
	</div>
</div>
--}%
