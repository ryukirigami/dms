
<%@ page import="com.kombos.parts.Invoice" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'InvoiceAdd.label', default: 'Add Parts')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript disposition="head">
        var show;
        var loadForm;
        var addParts;
        var doSave;
        var vendorId = "${vendorId}";
        var shrinkTableLayout;
        var expandTableLayout;
        $(function(){
            $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

            $('.box-action').click(function(){
                return false;
            });
                addParts = function(){
                  var checkGoods =[];
                    $("#InvoiceAdd-table tbody .row-select").each(function() {
                        if(this.checked){
                            var id = $(this).next("input:hidden").val();
                            var nRow = $(this).parents('tr')[0];

                            checkGoods.push(nRow);
                        }
                    });
                    if(checkGoods.length<1){
                        alert('Pilih Data Yang akan ditambahkan?');
                    } else {
                        for (var i=0;i < checkGoods.length;i++){

                            var aData = POTable.fnGetData(checkGoods[i]);

                            invoiceTable.fnAddData({
                                'id': aData['id'],
                                'kodePart': aData['kodePart'],
                                'namaPart': aData['goods'],
                                'noPo': aData['t164NoPO'],
                                'tglPo': aData['t164TglPO'],
                                'tipeOrder': aData['validasiOrder'],
                                'qDiscount': '0',
                                'qReceive': aData['t164Qty1'],
                                'qRetail': aData['t164HargaSatuan'],
                                'qNetSalePrice': aData['t164TotalHarga']

                                });

                            $('#qDiscount-'+aData['id']).autoNumeric('init',{
                                vMin:'0',
                                vMax:'100',
                                mDec: '2',
                                aSep:',',
                                aDec:'.'
                            });
                             $('#qReceive-'+aData['id']).autoNumeric('init',{
                                vMin:'0',
                                vMax:'9999999999',
                                mDec: '2',
                                aSep:',',
                                aDec:'.'
                            });
                             $('#qRetail-'+aData['id']).autoNumeric('init',{
                                vMin:'0',
                                vMax:'9999999999',
                                mDec: '2',
                                aSep:',',
                                aDec:'.'
                            });
                             $('#qNetSalePrice-'+aData['id']).autoNumeric('init',{
                                vMin:'0',
                                vMax:'999999999999999999',
                                mDec: '2',
                                aSep:',',
                                aDec:'.'
                            });
                        }
                         reloadPOTable();

                    }

              }
        });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
</div>
<div class="box">
    <div class="span12" id="InvoiceAdd-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <g:render template="dataTablesPO" />
        <g:field type="button" onclick="addParts();" class="btn btn-primary create" name="tambah" id="tambah" value="${message(code: 'default.button.upload.label', default: 'Add Selected')}" />
        <span style="margin-left: 870px">
            <button class="btn cancel" data-dismiss="modal" name="close" >Close</button>
        </span>
    </div>
</div>
</body>
</html>