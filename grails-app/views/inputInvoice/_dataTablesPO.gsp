
<%@ page import="com.kombos.parts.PO" %>

<r:require modules="baseapplayout, baseapplist" />
<g:render template="../menu/maxLineDisplay"/>

<table id="PO_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover" style="table-layout: fixed; width: 1100px">
    <col width="150px" />
    <col width="150px" />
    <col width="200px" />
    <col width="100px" />
    <col width="100px" />
    <col width="100px" />
    <col width="150px" />
    <col width="150px" />
    <col width="150px" />
    <col width="100px" />
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="PO.t164NoPO.label" default="No PO" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="PO.t164TglPO.label" default="Tgl PO" /></div>
			</th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="PO.goods.label" default="Goods" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="PO.t164Qty1.label" default="Qty1" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="PO.t164HargaSatuan.label" default="Harga Satuan" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="PO.t164TotalHarga.label" default="Total Harga" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
				<div><g:message code="PO.vendor.label" default="Vendor" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="PO.validasiOrder.label" default="Tipe Order" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="PO.request.label" default="Request" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="PO.eta.label" default="Eta" /></div>
			</th>

		
		</tr>
	</thead>
</table>

<g:javascript>
var POTable;
var reloadPOTable;

    POTable = $('#PO_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesListPO")}",
		"aoColumns": [

{
	"sName": "t164NoPO",
	"mDataProp": "t164NoPO",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'"> &nbsp; &nbsp;'+data ;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}

,

{
	"sName": "t164TglPO",
	"mDataProp": "t164TglPO",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}

,

{
	"sName": "goods",
	"mDataProp": "goods",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t164Qty1",
	"mDataProp": "t164Qty1",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}

,


{
	"sName": "t164HargaSatuan",
	"mDataProp": "t164HargaSatuan",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}

,

{
	"sName": "t164TotalHarga",
	"mDataProp": "t164TotalHarga",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}

,

{
	"sName": "vendor",
	"mDataProp": "vendor",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
}

,

{
	"sName": "validasiOrder",
	"mDataProp": "validasiOrder",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
}

,

{
	"sName": "request",
	"mDataProp": "request",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
}


,

{
	"sName": "eta",
	"mDataProp": "eta",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                            aoData.push(
									{"name": 'vendorId', "value": vendorId}
							);

						var t164NoPO = $('#filter_t164NoPO input').val();
						if(t164NoPO){
							aoData.push(
									{"name": 'sCriteria_t164NoPO', "value": t164NoPO}
							);
						}

						var t164TglPO = $('#search_t164TglPO').val();
						var t164TglPODay = $('#search_t164TglPO_day').val();
						var t164TglPOMonth = $('#search_t164TglPO_month').val();
						var t164TglPOYear = $('#search_t164TglPO_year').val();

						if(t164TglPO){
							aoData.push(
									{"name": 'sCriteria_t164TglPO', "value": "date.struct"},
									{"name": 'sCriteria_t164TglPO_dp', "value": t164TglPO},
									{"name": 'sCriteria_t164TglPO_day', "value": t164TglPODay},
									{"name": 'sCriteria_t164TglPO_month', "value": t164TglPOMonth},
									{"name": 'sCriteria_t164TglPO_year', "value": t164TglPOYear}
							);
						}

						var vendor = $('#filter_vendor input').val();
						if(vendor){
							aoData.push(
									{"name": 'sCriteria_vendor', "value": vendor}
							);
						}

						var validasiOrder = $('#filter_validasiOrder input').val();
						if(validasiOrder){
							aoData.push(
									{"name": 'sCriteria_validasiOrder', "value": validasiOrder}
							);
						}

						var request = $('#filter_request input').val();
						if(request){
							aoData.push(
									{"name": 'sCriteria_request', "value": request}
							);
						}

						var goods = $('#filter_goods input').val();
						if(goods){
							aoData.push(
									{"name": 'sCriteria_goods', "value": goods}
							);
						}

						var t164Qty1 = $('#filter_t164Qty1 input').val();
						if(t164Qty1){
							aoData.push(
									{"name": 'sCriteria_t164Qty1', "value": t164Qty1}
							);
						}

						var t164Qty2 = $('#filter_t164Qty2 input').val();
						if(t164Qty2){
							aoData.push(
									{"name": 'sCriteria_t164Qty2', "value": t164Qty2}
							);
						}

						var t164HargaSatuan = $('#filter_t164HargaSatuan input').val();
						if(t164HargaSatuan){
							aoData.push(
									{"name": 'sCriteria_t164HargaSatuan', "value": t164HargaSatuan}
							);
						}

						var t164TotalHarga = $('#filter_t164TotalHarga input').val();
						if(t164TotalHarga){
							aoData.push(
									{"name": 'sCriteria_t164TotalHarga', "value": t164TotalHarga}
							);
						}


						var eta = $('#filter_eta input').val();
						if(eta){
							aoData.push(
									{"name": 'sCriteria_eta', "value": eta}
							);
						}

                        var exist =[];
						var rows = $("#invoiceInput_datatables").dataTable().fnGetNodes();
                        for(var i=0;i<rows.length ;i++)
                        {
							var id = $(rows[i]).find("#idPopUp").val();
							exist.push(id);
						}
						if(exist.length > 0){
							aoData.push(
										{"name": 'sCriteria_exist', "value": JSON.stringify(exist)}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});


$(function(){
	
	reloadPOTable = function() {
		POTable.fnDraw();
	}

	
	$('#search_t164TglPO').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t164TglPO_day').val(newDate.getDate());
			$('#search_t164TglPO_month').val(newDate.getMonth()+1);
			$('#search_t164TglPO_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			POTable.fnDraw();	
	});

	


    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	POTable.fnDraw();
		}
	});

	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

    //initTablePO();
});
</g:javascript>


			
