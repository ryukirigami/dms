
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="invoiceInput_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="100%">
    <thead>
    <tr>
        <th style="border-bottom: none; padding: 5px;">
            <div>Kode Part</div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div>Nama Part</div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div>No PO</div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div>Tanggal PO</div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div>Tipe Order</div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div>Qty Receive</div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div>Retail Price</div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div>Discount (%)</div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div>Net Sale Price</div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var invoiceTable;
var reloadInvoiceTable;
$(function(){
    $('.auto').autoNumeric('init',{
            mDec:'2'
        });
	var recordsInvoicePerPage = [];
    var anInvoiceSelected;
    var jmlRecInvoicePerPage=0;
    var id;
	reloadInvoiceTable = function() {
		invoiceTable.fnDraw();
	}

	
	$('#search_t162TglInvoice').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t162TglInvoice_day').val(newDate.getDate());
			$('#search_t162TglInvoice_month').val(newDate.getMonth()+1);
			$('#search_t162TglInvoice_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			invoiceTable.fnDraw();
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	invoiceTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});
       if(invoiceTable)
    	invoiceTable.dataTable().fnDestroy();
	invoiceTable = $('#invoiceInput_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsInvoice = $("#invoiceInput_datatables tbody .row-select");
            var jmlInvoiceCek = 0;
            var nRow;
            var idRec;
            rsInvoice.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsInvoicePerPage[idRec]=="1"){
                    jmlInvoiceCek = jmlInvoiceCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsInvoicePerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecInvoicePerPage = rsInvoice.length;
            if(jmlInvoiceCek==jmlRecInvoicePerPage && jmlRecInvoicePerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"aoColumns": [

{
	"sName": "kodePart",
	"mDataProp": "kodePart",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" id="idPopUp" value="'+row['id']+'">&nbsp;&nbsp'+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"250px",
	"bVisible": true
}

,

{
	"sName": "namaPart",
	"mDataProp": "namaPart",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"250px",
	"bVisible": true
}
,

{
	"sName": "noPo",
	"mDataProp": "noPo",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "tglPo",
	"mDataProp": "tglPo",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "tipeOrder",
	"mDataProp": "tipeOrder",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "qReceive",
	"mDataProp": "qReceive",
	"aTargets": [5],
	"bSearchable": true,
    "mRender": function ( data, type, row ) {
        return '<input id="qReceive-'+row['id']+'" onkeyup="ubahJumlah(\''+row['id']+'\');" class="inline-edit" type="text" style="width:90px;" value="'+data+'">';
    },
    "bSortable": true,
    "sWidth":"100px",
    "bVisible": true
}
,

{
	"sName": "qRetail",
	"mDataProp": "qRetail",
	"aTargets": [6],
	"bSearchable": true,
    "mRender": function ( data, type, row ) {
        return '<input id="qRetail-'+row['id']+'" onkeyup="ubahJumlah(\''+row['id']+'\');" class="inline-edit" type="text" style="width:90px;" value="'+data+'">';
    },
    "bSortable": true,
    "sWidth":"100px",
    "bVisible": true
},

{
	"sName": "qDiscount",
	"mDataProp": "qDiscount",
	"aTargets": [6],
	"bSearchable": true,
    "mRender": function ( data, type, row ) {
        return '<input id="qDiscount-'+row['id']+'" onkeyup="ubahJumlah(\''+row['id']+'\');" class="inline-edit" type="text" style="width:90px;" value="'+data+'">';
    },
    "bSortable": true,
    "sWidth":"100px",
    "bVisible": true
},

{
	"sName": "qNetSalePrice",
	"mDataProp": "qNetSalePrice",
	"aTargets": [6],
	"bSearchable": true,
    "mRender": function ( data, type, row ) {
        return '<input id="qNetSalePrice-'+row['id']+'" class="inline-edit" type="text" style="width:90px;" value="'+data+'" readonly="readonly">';
    },
    "bSortable": true,
    "sWidth":"100px",
    "bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var t162ID = $('#filter_t162ID input').val();
						if(t162ID){
							aoData.push(
									{"name": 'sCriteria_t162ID', "value": t162ID}
							);
						}
	
						var goods = $('#filter_goods input').val();
						if(goods){
							aoData.push(
									{"name": 'sCriteria_goods', "value": goods}
							);
						}
	
						var t162Qty1 = $('#filter_t162Qty1 input').val();
						if(t162Qty1){
							aoData.push(
									{"name": 'sCriteria_t162Qty1', "value": t162Qty1}
							);
						}
	
						var t162Qty2 = $('#filter_t162Qty2 input').val();
						if(t162Qty2){
							aoData.push(
									{"name": 'sCriteria_t162Qty2', "value": t162Qty2}
							);
						}

						var t162TglInvoice = $('#search_t162TglInvoice').val();
						var t162TglInvoiceDay = $('#search_t162TglInvoice_day').val();
						var t162TglInvoiceMonth = $('#search_t162TglInvoice_month').val();
						var t162TglInvoiceYear = $('#search_t162TglInvoice_year').val();
						
						if(t162TglInvoice){
							aoData.push(
									{"name": 'sCriteria_t162TglInvoice', "value": "date.struct"},
									{"name": 'sCriteria_t162TglInvoice_dp', "value": t162TglInvoice},
									{"name": 'sCriteria_t162TglInvoice_day', "value": t162TglInvoiceDay},
									{"name": 'sCriteria_t162TglInvoice_month', "value": t162TglInvoiceMonth},
									{"name": 'sCriteria_t162TglInvoice_year', "value": t162TglInvoiceYear}
							);
						}
	
						var t162StaFA = $('#filter_t162StaFA input').val();
						if(t162StaFA){
							aoData.push(
									{"name": 'sCriteria_t162StaFA', "value": t162StaFA}
							);
						}
	
						var t162NoReff = $('#filter_t162NoReff input').val();
						if(t162NoReff){
							aoData.push(
									{"name": 'sCriteria_t162NoReff', "value": t162NoReff}
							);
						}
	
						var t162Qty1Available = $('#filter_t162Qty1Available input').val();
						if(t162Qty1Available){
							aoData.push(
									{"name": 'sCriteria_t162Qty1Available', "value": t162Qty1Available}
							);
						}
	
						var t162Qty2Available = $('#filter_t162Qty2Available input').val();
						if(t162Qty2Available){
							aoData.push(
									{"name": 'sCriteria_t162Qty2Available', "value": t162Qty2Available}
							);
						}
	
						var t162NamaPemohon = $('#filter_t162NamaPemohon input').val();
						if(t162NamaPemohon){
							aoData.push(
									{"name": 'sCriteria_t162NamaPemohon', "value": t162NamaPemohon}
							);
						}
	
						var t162xNamaUser = $('#filter_t162xNamaUser input').val();
						if(t162xNamaUser){
							aoData.push(
									{"name": 'sCriteria_t162xNamaUser', "value": t162xNamaUser}
							);
						}
	
						var t162xNamaDivisi = $('#filter_t162xNamaDivisi input').val();
						if(t162xNamaDivisi){
							aoData.push(
									{"name": 'sCriteria_t162xNamaDivisi', "value": t162xNamaDivisi}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#invoiceInput_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsInvoicePerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsInvoicePerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#invoiceInput_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsInvoicePerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anInvoiceSelected = invoiceTable.$('tr.row_selected');

            if(jmlRecInvoicePerPage == anInvoiceSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsInvoicePerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
    if(cekStatus=="ubah"){
        var nomorInvoice = "${nomorInvoice}";
        $.ajax({
        url:'${request.contextPath}/invoiceInput/getTableData',
    		type: "POST", // Always use POST when deleting data
    		data: { id: nomorInvoice },
    		success : function(data){
    		    $.each(data,function(i,item){
                    invoiceTable.fnAddData({
                        'id': item.id,
						'goods': item.kode,
						'goods2': item.nama,
						'tglReceive': item.tglReceive,
						'po': item.po,
						'tglpo': item.tglpo,
						'nomorInvoice': item.nomorInvoice,
						'tglInvoice': item.tglInvoice,
						'QInvoice': item.QInvoice,
						'Qreceive': item.Qreceive,
						'Qreturn': item.Qreturn,
						'Qreff': item.Qreff
                    });
                });
            }
		});
    }
});
</g:javascript>


			
