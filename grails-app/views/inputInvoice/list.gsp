
<%@ page import="com.kombos.parts.Invoice" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName" value="${message(code: 'invoice.label', default: 'Input Parts Return')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
<r:require modules="baseapplayout" />
<g:javascript>
		$(function(){
			$('#m121Nama').typeahead({
			    source: function (query, process) {
			   // alert("nilai query : "+query);
			       return $.get('${request.contextPath}/inputInvoice/kodeList', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});
</g:javascript>
<g:javascript disposition="head">
 var m121Nama = "";
    var noUrut = 1;
    var cekStatus = "${aksi}";
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

   	massDelete = function() {
   		var recordsToDelete = [];
		$("#inputInvoice-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/inputInvoice/massdelete',
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadRequestTable();
    		}
		});
		
   	}

});
            $(document).ready(function()
         {
             $('input:radio[name=search_t162StaFA]').click(function(){
                if($('input:radio[name=search_t162StaFA]:nth(0)').is(':checked')){
                    $("#search_t162NoReff").prop('disabled', false);

                }else{
                    $("#search_t162NoReff").val("")
                    $("#search_t162NoReff").prop('disabled', true);
                }
             });


         });

    loadInvoiceAddModal = function(){
                if(m121Nama != $('#m121Nama').val()){
               $("#inputInvoice-table tbody .row-select").each(function() {
                    var id = $(this).next("input:hidden").val();
                    var row = $(this).closest("tr").get(0);
                    invoiceTable.fnDeleteRow(invoiceTable.fnGetPosition(row));
                });
          }
    	    m121Nama = $('#m121Nama').val();
    	    if($('#m121Nama').val()=="" || $('#noInvoice').val()==""){
                alert("Isi Data Terlebih Dahulu");
    	    }else{
                    $("#invoiceAddContent").empty();
                    $.ajax({type:'POST',
                    url:'${request.contextPath}/inputInvoice/addModal',
                    data : { vendor :m121Nama},
                        success:function(data,textStatus){
                                $("#invoiceAddContent").html(data);
                                $("#invoiceAddModal").modal({
                                    "backdrop" : "static",
                                    "keyboard" : true,
                                    "show" : true
                                }).css({'width': '1200px','margin-left': function () {return -($(this).width() / 2);}});

                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });

    	    }

    }

    

      deleteData = function(){
                var checkGoods =[];
                $("#inputInvoice-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    var row = $(this).closest("tr").get(0);
                    invoiceTable.fnDeleteRow(invoiceTable.fnGetPosition(row));
                }
                });
      }
      updateInvoice = function(){
            var formInvoice = $('#inputInvoice-table').find('form');
                var qtyNol = false
                var checkGoods =[];
                $("#inputInvoice-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    var nRow = $(this).parents('tr')[0];
                    var aData = invoiceTable.fnGetData(nRow)
                    var qDiscount = $('#qDiscount-' + aData['id'], nRow);
                    var qReceive = $('#qReceive-' + aData['id'], nRow);
                    var qRetail = $('#qRetail-' + aData['id'], nRow);
                    var qNetSalePrice = $('#qNetSalePrice-' + aData['id'], nRow);
                    if(qDiscount.val()==0 || qDiscount.val()==""){
                       qtyNol = true
                    }
                    if(qReceive.val()==0 || qReceive.val()==""){
                       qtyNol = true
                    }
                    if(qRetail.val()==0 || qRetail.val()==""){
                       qtyNol = true
                    }
                     if(qNetSalePrice.val()==0 || qNetSalePrice.val()==""){
                       qtyNol = true
                    }
                    checkGoods.push(id);

                    formInvoice.append('<input type="hidden" name="qDiscount-'+aData['id'] + '" value="'+qDiscount[0].value+'" class="deleteafter">');
                    formInvoice.append('<input type="hidden" name="qReceive-'+aData['id'] + '" value="'+qReceive[0].value+'" class="deleteafter">');
                    formInvoice.append('<input type="hidden" name="qRetail-'+aData['id'] + '" value="'+qRetail[0].value+'" class="deleteafter">');
                    formInvoice.append('<input type="hidden" name="qNetSalePrice-'+aData['id'] + '" value="'+qNetSalePrice[0].value+'" class="deleteafter">');
                }
                });
                if(checkGoods.length<1){
                    alert('Anda belum memilih data yang akan ditambahkan');
                     reloadInvoiceTable();
                }else{
                    if($("#tanggal").val()=="" || $("#m121Nama").val()==""){
                        toastr.error('<div>Mohon Diisi Dengan Benar.</div>');
                    }else if(qtyNol == true){
                        toastr.error('<div>Quantity Tidak Boleh NOL</div>');
                        formInvoice.find('.deleteafter').remove();
                    }
                    else{
                         var r = confirm("Anda yakin data akan disimpan?");
                         if(r==true){
                            var rows = invoiceTable.fnGetNodes();
                            $("#ids").val(JSON.stringify(checkGoods));
                            $.ajax({
                                url:'${request.contextPath}/inputInvoice/ubah',
                                type: "POST", // Always use POST when deleting data
                                data : formInvoice.serialize(),
                                success : function(data){
                                  if(data=='fail'){
                                    toastr.error('<div>Vendor Tidak Benar.</div>');
                                  }else{
                                    if(rows.length > 10){
                                        $("#inputInvoice-table tbody .row-select").each(function() {
                                            if(this.checked){
                                                var row = $(this).closest("tr").get(0);
                                                invoiceTable.fnDeleteRow(invoiceTable.fnGetPosition(row));
                                                reloadInvoiceTable();
                                            }
                                        });
                                        formInvoice.append('<input type="hidden" name="noReturnBefore" value="'+data+'" class="deleteafter">');
                                        toastr.success('<div>Sukses</div>');
                                    }else{
                                        window.location.href = '#/invoice' ;
                                        toastr.success('<div>Parts Invoice telah di Ubah.</div>');
                                        formInvoice.find('.deleteafter').remove();
                                        $("#ids").val('');
                                    }

                                  }
                                },
                            error: function(xhr, textStatus, errorThrown) {
                            alert('Internal Server Error');
                            }
                            });

                            checkGoods = [];
                          }
                    }
                }
      };
      createRequest = function(){
                var formInvoice = $('#inputInvoice-table').find('form');
                var qtyNol = false
                var checkGoods =[];
                $("#inputInvoice-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    var nRow = $(this).parents('tr')[0];
                    var aData = invoiceTable.fnGetData(nRow)
                     var qDiscount = $('#qDiscount-' + aData['id'], nRow);
                    var qReceive = $('#qReceive-' + aData['id'], nRow);
                    var qRetail = $('#qRetail-' + aData['id'], nRow);
                    var qNetSalePrice = $('#qNetSalePrice-' + aData['id'], nRow);

                    if(qReceive.val()==0 || qReceive.val()==""){
                       qtyNol = true
                    }
                    if(qRetail.val()==0 || qRetail.val()==""){
                       qtyNol = true
                    }

                    checkGoods.push(id);

                    formInvoice.append('<input type="hidden" name="qDiscount-'+aData['id'] + '" value="'+qDiscount[0].value+'" class="deleteafter">');
                    formInvoice.append('<input type="hidden" name="qReceive-'+aData['id'] + '" value="'+qReceive[0].value+'" class="deleteafter">');
                    formInvoice.append('<input type="hidden" name="qRetail-'+aData['id'] + '" value="'+qRetail[0].value+'" class="deleteafter">');
                    formInvoice.append('<input type="hidden" name="qNetSalePrice-'+aData['id'] + '" value="'+qNetSalePrice[0].value+'" class="deleteafter">');
                }
                });
                if(checkGoods.length<1){
                    alert('Anda belum memilih data yang akan ditambahkan');
                     reloadInvoiceTable();
                }else{
                    if($("#tanggal").val()=="" || $("#m121Nama").val()==""){
                        toastr.error('<div>Mohon Diisi Dengan Benar.</div>');
                    }else if(qtyNol == true){
                        toastr.error('<div>Quantity Tidak Boleh NOL</div>');
                        formInvoice.find('.deleteafter').remove();
                    }
                    else{
                         var r = confirm("Anda yakin data akan disimpan?");
                         if(r==true){
                            var rows = invoiceTable.fnGetNodes();
                            $("#ids").val(JSON.stringify(checkGoods));
                            $.ajax({
                                url:'${request.contextPath}/inputInvoice/req',
                                type: "POST", // Always use POST when deleting data
                                data : formInvoice.serialize(),
                                success : function(data){
                                  if(data=='fail'){
                                    toastr.error('<div>Vendor Tidak Benar.</div>');
                                  }else{
                                        $("#inputInvoice-table tbody .row-select").each(function() {
                                            if(this.checked){
                                                var row = $(this).closest("tr").get(0);
                                                invoiceTable.fnDeleteRow(invoiceTable.fnGetPosition(row));
                                                reloadInvoiceTable();
                                            }
                                        });
                                        formInvoice.append('<input type="hidden" name="noInvoice" value="'+data+'" class="deleteafter">');
                                        window.location.href = '#/invoice' ;
                                        toastr.success('<div>Parts Invoice telah dibuat.</div>');
                                        formInvoice.find('.deleteafter').remove();
                                        $("#ids").val('');
                                  }
                                },
                            error: function(xhr, textStatus, errorThrown) {
                            alert('Internal Server Error');
                            }
                            });

                            checkGoods = [];
                          }
                    }
                }

    };
        function ubahJumlah(param){
            var total = 0;
            var valueParam = param;
            var qRetail = $('#qRetail-'+valueParam).val().replace(/,/g,"");
            var valQty = $('#qReceive-'+valueParam).val().replace(/,/g,"");
            var valDisc = $('#qDiscount-'+valueParam).val().replace(/,/g,"");
            var valHarga = qRetail
            var val1 = valQty=="" ? valHarga : valQty*valHarga;
            var val2 = valDisc=="" ? (val1=="" ? valHarga : val1)  : ( val1=="" ? valHarga - valDisc/100*valHarga : val1 - valDisc/100*val1);
            val2 = val2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            $('#qNetSalePrice-'+valueParam+"").val(val2);
        }
</g:javascript>

</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left">
        <g:if test="${aksi=='ubah'}">
            Edit Invoice
        </g:if>
        <g:else>
            Input Invoice
        </g:else>
    </span>
    <ul class="nav pull-right">
        <li>&nbsp;</li>
    </ul>
</div><br>
<div class="box">
    <div class="span12" id="inputInvoice-table">
        <div>${flash.message}</div>
        <fieldset>
            <form id="form-invoice" class="form-horizontal">
                <input type="hidden" name="ids" id="ids" value="">
                <div class="control-group">
                    <label class="control-label" for="t951Tanggal" style="text-align: left;">Vendor *</label>
                    <div id="filter_vendor" class="controls">
                        <g:if test="${aksi=='ubah'}">
                            <g:textField name="vendor" id="vendor" value="${u_vendor}" maxlength="220" readonly="readonly"  />
                        </g:if>
                        <g:else>
                            <g:textField name="namaVendor" id="m121Nama" class="typeahead"  autocomplete="off" maxlength="220"  />
                        </g:else>
                    </div>
                </div>

                    <div class="control-group">
                        <label class="control-label" for="t951Tanggal" style="text-align: left;">
                            Nomor Invoice
                        </label>
                        <div id="filter_wo" class="controls">
                            <g:if test="${aksi=='ubah'}">
                                <g:textField name="noInvoice" readonly="readonly" id="noInvoice" value="${nomorInvoice}" autocomplete="off" maxlength="220"  />
                            </g:if>
                            <g:else>
                                <g:textField name="noInvoice" id="noInvoice" autocomplete="off" maxlength="220"  />
                            </g:else>
                        </div>
                    </div>

                <div class="control-group">
                    <label class="control-label" for="userRole"  style="text-align: left;">Tanggal Invoice *
                    </label>
                    <div id="filter_status" class="controls">
                        <g:if test="${aksi=='ubah'}">
                            <ba:datePicker id="tanggal" name="tanggal" precision="day" format="dd-MM-yyyy" required="" value="${u_tanggal}"  />
                        </g:if>
                        <g:else>
                            <ba:datePicker id="tanggal" name="tanggal" precision="day" format="dd-MM-yyyy" required="" value="${new Date()}"  />
                        </g:else>
                    </div>
                </div>

            </form>
        </fieldset>
        <g:if test="${aksi!='ubah'}">
            <fieldset>
                <table>
                    <tr>
                        <td>
                            <button style="width: 150px;height: 30px; border-radius: 5px" class="btn btn-primary" name="view" id="add" onclick="loadInvoiceAddModal()">Add Parts</button>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </g:if>
        <br>
        <g:render template="dataTables" />
        <g:if test="${aksi=='ubah'}">
            <g:field type="button" onclick="updateInvoice()" class="btn btn-primary create" name="Edit"  value="${message(code: 'default.button.upload.label', default: 'Update')}" />
        </g:if>
        <g:else>
            <g:field type="button" onclick="deleteData()" class="btn btn-cancel create" name="deleteData" id="delData" value="${message(code: 'default.button.upload.label', default: 'Delete')}" />
            <g:field type="button" onclick="createRequest()" class="btn btn-primary create" name="tambahSave" id="tambahSave" value="${message(code: 'default.button.upload.label', default: 'Save')}" />
        </g:else>
        <g:field type="button" onclick="window.location.replace('#/invoice');" class="btn btn-cancel" name="cancel" id="cancel" value="Cancel" />
    </div>
    <div class="span7" id="invoice-form" style="display: none;"></div>
</div>
<div id="invoiceAddModal" class="modal fade">
    <div class="modal-dialog" style="width: 1200px;">
        <div class="modal-content" style="width: 1200px;">
            <div class="modal-body" style="max-height: 800px;">
                <div id="invoiceAddContent"/>
                <div class="iu-content"></div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<g:javascript>
    <g:if test="${aksi=='ubah'}">
    %{--document.getElementById("tanggal").focus();--}%
    </g:if>
    <g:else>
        document.getElementById("m121Nama").focus();
    </g:else>
</g:javascript>

