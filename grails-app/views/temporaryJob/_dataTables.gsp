
<%@ page import="com.kombos.administrasi.Operation" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="operation_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="operation.m053JobsId.label" default="Kd Job Temporary" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
				<div><g:message code="operation.m053NamaOperation.label" default="Nama Job Temporary" /></div>
			</th>

		</tr>
		<tr>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_m053JobsId" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_m053JobsId" class="search_init" />
                </div>
            </th>


            <th style="border-top: none;padding: 5px;">
				<div id="filter_m053NamaOperation" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m053NamaOperation" class="search_init" />
				</div>
			</th>

		</tr>
	</thead>
</table>

<g:javascript>
var operationTable;
var reloadOperationTable;
$(function(){
	
	reloadOperationTable = function() {
		operationTable.fnDraw();
	}

	var recordsOperationperpage = [];//new Array();
    var anOperationSelected;
    var jmlRecOperationPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	operationTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	operationTable = $('#operation_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            //alert( $('li.active').text() );
            var rsOperation = $("#operation_datatables tbody .row-select");
            var jmlOperationCek = 0;
            var nRow;
            var idRec;
            rsOperation.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsOperationperpage[idRec]=="1"){
                    jmlOperationCek = jmlOperationCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsOperationperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecOperationPerPage = rsOperation.length;
            if(jmlOperationCek==jmlRecOperationPerPage && jmlRecOperationPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m053JobsId",
	"mDataProp": "m053JobsId",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m053NamaOperation",
	"mDataProp": "m053NamaOperation",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

            var m053JobsId = $('#filter_m053JobsId input').val();
            if(m053JobsId){
                aoData.push(
                        {"name": 'sCriteria_m053JobsId', "value": m053JobsId}
                );
            }

            var m053NamaOperation = $('#filter_m053NamaOperation input').val();
            if(m053NamaOperation){
                aoData.push(
                        {"name": 'sCriteria_m053NamaOperation', "value": m053NamaOperation}
                );
            }

            $.ajax({ "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData ,
                "success": function (json) {
                    fnCallback(json);
                   },
                "complete": function () {
                   }
            });
		}			
	});
	$('.select-all').click(function(e) {

        $("#operation_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordsOperationperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsOperationperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#operation_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsOperationperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anOperationSelected = operationTable.$('tr.row_selected');
            if(jmlRecOperationPerPage == anOperationSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsOperationperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
