<%@ page import="com.kombos.administrasi.Operation" %>
<g:javascript>
    $(document).ready(function() {
        if($('#m053NamaOperation').val().length>0){
            $('#hitung').text(250 - $('#m053NamaOperation').val().length);
        }

        $('#m053NamaOperation').keyup(function() {
            var len = this.value.length;
            if (len >= 250) {
                this.value = this.value.substring(0, 250);
                len = 250;
            }
            $('#hitung').text(250 - len);
        });
    });
</g:javascript>



<g:if test="${operationInstance?.m053JobsId}">
<div class="control-group fieldcontain ${hasErrors(bean: operationInstance, field: 'm053JobsId', 'error')} required">
    <label class="control-label" for="m053JobsId">
        <g:message code="operation.m053JobsId.label" default="Kode Temporary Job" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        ${operationInstance?.m053JobsId}
    </div>
</div>
</g:if>
<div class="control-group fieldcontain ${hasErrors(bean: operationInstance, field: 'm053NamaOperation', 'error')} required">
	<label class="control-label" for="m053NamaOperation">
		<g:message code="operation.m053NamaOperation.label" default="Nama Temporary Job" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textArea name="m053NamaOperation" id="m053NamaOperation" required="" value="${operationInstance?.m053NamaOperation}"/>
        <br/>
        <span id="hitung">250</span> Karakter Tersisa.
	</div>
</div>

<g:javascript>
    document.getElementById('m053NamaOperation').focus();
</g:javascript>