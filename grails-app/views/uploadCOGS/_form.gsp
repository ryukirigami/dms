<%@ page import="com.kombos.administrasi.CompanyDealer; com.kombos.administrasi.Operation" %>
%{--<g:javascript>--}%
%{--$("#section").change(function(){--}%
%{--$('#spinner').fadeIn(1);--}%
%{--$.ajax({--}%
%{--url: '${request.contextPath}/uploadJob/generateSerial/'+$("#section").val(),--}%
%{--type: 'GET',--}%
%{--dataType: 'html',--}%
%{--success: function(data, textStatus, xhr) {--}%
%{--if(data!=""){--}%
%{--$('#serial').html(data);--}%
%{--}--}%
%{--},--}%
%{--error: function(xhr, textStatus, errorThrown) {--}%
%{--alert(textStatus);--}%
%{--},--}%
%{--complete:function(XMLHttpRequest,textStatus){--}%
%{--$('#spinner').fadeOut();--}%
%{--}--}%
%{--});--}%
%{--//alert($("#section").val());--}%
%{--});--}%
%{--</g:javascript>--}%

<div class="control-group fieldcontain ${hasErrors(bean: operationInstance, field: 'm053JobsId', 'error')} ">
    %{--<label class="control-label" for="m053JobsId">--}%
    %{--<g:message code="operation.upload.label" default="Upload File" />--}%

    %{--</label>--}%
    <div class="controls">
        <table>
            <tr>
                <td>
                    Target
                </td>
                <td>
                    <g:radioGroup values="['BP','GR']" labels="['BP','GR']" name="bpGr" value="${bpGr==null?'BP':bpGr}">
                        ${it.label} ${it.radio}
                    </g:radioGroup>
                </td>
            </tr>
            <tr>
                <td>
                    Company Dealer
                </td>
                <td>
                    <g:select id="companyDealer" name="companyDealer.id" from="${CompanyDealer.list()}" optionKey="id" required="" value="${companyDealer}" class="many-to-one"/>
                </td>
            </tr>
            <tr>
                <td>
                    File To Be Upload
                </td>
                <td>
                    <input type="file" required="" id="fileExcel" name="fileExcel" accept="application/excel|application/vnd.ms-excel" />
                    <a href="${request.getContextPath()}/formatFileUpload/MasterTarget-UploadCOGS.xls" >File Example Upload COGS</a>
                </td>
            </tr>
        </table>
    </div>
</div>
