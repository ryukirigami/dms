<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="kwitansi_datatables_${idTable}" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>
        <th style="border-bottom: none;">
            <div style="width: 30px;">No. Urut</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Tgl Bayar</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Metode Bayar</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Nama Bank</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">No. Kartu</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Bunga</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Jumlah</div>
        </th>
    </tr>
    </thead>
</table>
<g:javascript>
var kwitansiTable;
var kwitansiKuitansiTable;
var reloadKwitansiTable;

//function searchData() {
//    reloadKwitansiTable();
//}

$(function() {
	reloadKwitansiTable = function() {
		kwitansiTable.fnDraw();
	}

	kwitansiTable = $('#kwitansi_datatables_${idTable}').dataTable({
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		},
	    "fnRowCallback": function (nRow) {
			return nRow;
	    },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "kwitansiDataTablesList")}",
		"aoColumns": [
			//id
			{
				"sName": "id",
				"mDataProp": "id",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"30px",
				"bVisible": true
			},
			//tanggalBayar
			{
				"sName": "tanggalBayar",
				"mDataProp": "tanggalBayar",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			//metodeBayar
			{
				"sName": "metodeBayar",
				"mDataProp": "metodeBayar",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			//namaBank
			{
				"sName": "namaBank",
				"mDataProp": "namaBank",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			//nomorKartu
			{
				"sName": "nomorKartu",
				"mDataProp": "nomorKartu",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			//bunga
			{
				"sName": "bunga",
				"mDataProp": "bunga",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			//jumlah
			{
				"sName": "jumlah",
				"mDataProp": "jumlah",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			}
		],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        aoData.push(
                                {"name": 'idWO', "value": noWoKW}
                        );

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});

});
</g:javascript>