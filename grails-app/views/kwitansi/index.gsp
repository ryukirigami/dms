<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'delivery.kwitansi.label', default: 'Delivery - Kwitansi')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript>
        var noWoKW = "-1";
        $(function(){
            var msg='';
            var totalUangInput = $('input[name=totalUang]');
            totalUangInput.focusout(function(){
                calculate();
            });

            totalUangInput.blur(function(){
                calculate();
            });
            var totalBayarInput = $('input[name=totalBayar]');
            totalBayarInput.focusout(function(){
                calculate();
            });

            totalBayarInput.blur(function(){
                calculate();
            });

            calculate = function() {
                var totalBayar = $('input[name=totalBayar]').val();
                if(!totalBayar)totalBayar=0;
                var totalUang = $('input[name=totalUang]').val();
                if(!totalUang)totalUang=0;
                if(totalUang > totalBayar){
                    var kembalikan = totalUang - totalBayar;
                    $('input[name=kembalikan]').val(kembalikan);
                }else{
                    $('input[name=kembalikan]').val(0);
                    $('input[name=totalUang]').val(totalBayar);
                }
            };

            changeJenisBayar = function() {
                var jenisBayarSelect = $('#jenisBayarSelect').val();
                if(jenisBayarSelect=='0'){
                    $('#jenisBayar').val('0');
                }else if(jenisBayarSelect=='1'){
                    $('#jenisBayar').val('1');
                }else{
                    $('#jenisBayar').val('2');
                }
            };

            $("#nomorWO").bind('keypress', function(e) {
                var code = (e.keyCode ? e.keyCode : e.which);
                    if(code == 13) {
                            searchData();
                    }
                });

            searchData = function() {
                var jenisBayar = $('input[name=jenisBayar]').val();
                var nomorWO = $('input[name=nomorWO]').val();
                if(nomorWO){
                    doSearch(jenisBayar,nomorWO);
                }else{
                    clearDataWorkOrder();
                    alert('Silahkan masukkan nomor WO');
                    return false;
                }

                if(checkPembayaran()=="1"){
                    $("#save").attr("disabled", false);
                    $("#print").attr("disabled", false);
                    $("#preview").attr("disabled", false);
                }else{
                    $("#save").attr("disabled", true);
                    $("#print").attr("disabled", true);
                    $("#preview").attr("disabled", true);
                }
            };

            doSearch = function(a,b) {
                noWoKW="-1";
                $("#save").attr("disabled", true);
                $("#bayar").attr("disabled", false);
                $("#print").attr("disabled", true);
                $.ajax({
                    url:'${request.contextPath}/kwitansi/searchData',
                    type: "POST",
                    data : { jenisBayar: a, nomorWO: b },
                    success : function(data){
                        clearDataWorkOrder();
                        if(data.nomorWO){
                            noWoKW = data.idReception;
                            enabledAllInvoice();
                            setDataWorkOrder(data);
                            reloadKwitansiTable();
                            $("#totalDetailPembayaran").text(data.total);
                            $("#noKuitansi").text(data.noKw);
                            if(data.total!="0"){
                                $("#save").attr("disabled", true);
                                $("#bayar").attr("disabled", true);
                                $("#print").attr("disabled", false);
                            }
                        }else{
                            clearDataWorkOrder();
                            alert('Data Tidak Ditemukan');
                            return false;
                        }
                    },
                    error: function() {
                        clearDataWorkOrder();
                        alert('Internal Server Error');
                        return false;
                    }
                });
            };

            clearDataWorkOrder = function() {
                $('input[name=outNomorWO]').val('');
                $('input[name=outJenisService]').val('');
                $('input[name=outNamaCustomer]').val('');
                $('input[name=outNomorPolisi]').val('');
                $('input[name=outModel]').val('');
                $('input[name=outCustomerSPK]').val('');
                $('input[name=outJumlahOnRisk]').val('');
                $('input[name=outJumlahMinBookingFee]').val('');
                $('input[name=outKasir]').val('');
                $('input[name=outDivisi]').val('');
            };

            setDataWorkOrder = function(data) {
                $('input[name=outNomorWO]').val(data.nomorWO);
                $('input[name=outJenisService]').val(data.jenisService);
                $('input[name=outNamaCustomer]').val(data.namaCustomer);
                $('input[name=outNomorPolisi]').val(data.nomorPolisi);
                $('input[name=outModel]').val(data.model);
                $('input[name=outCustomerSPK]').val(data.customerSPK);
                $('input[name=outJumlahOnRisk]').val(data.jumlahOnRisk);
                $('input[name=outJumlahMinBookingFee]').val(data.jumlahMinBookingFee);
                $('input[name=outKasir]').val(data.kasir);
                $('input[name=outDivisi]').val(data.divisi);
            };

            enabledAllInvoice = function() {
                $('select[name=metodeBayarSelect]').prop("disabled",false);
                var metodeBayar = $('input[name=metodeBayar]').val();
//                if(metodeBayar!='0'){
//                    $('select[name=namaBankSelect]').prop("disabled",false);
//                    $('select[name=mesinEDCSelect]').prop("disabled",false);
//                    $('input[name=nomorKartu]').prop("readonly",false);
//                    $('input[name=noBuktiTransfer]').prop("readonly",false);
//                }
                $('input[name=terimaDari]').prop("readonly",false);
                $('.untukPembayaran').attr("disabled",false);
                $('input[name=totalBayar]').prop("readonly",false);
                $('input[name=totalUang]').prop("readonly",false);
            };

            disableAllPembayaran = function() {
                $('select[name=namaBankSelect]').prop("disabled",true);
                $('select[name=mesinEDCSelect]').prop("disabled",true);
                $('input[name=nomorKartu]').prop("readonly",true);
                $('input[name=noBuktiTransfer]').prop("readonly",true);
            };

            enableTransferAndDebit = function() {
                $('select[name=mesinEDCSelect]').prop("disabled",true);
                $('input[name=nomorKartu]').prop("readonly",true);
                $('select[name=namaBankSelect]').prop("disabled",false);
                $('input[name=noBuktiTransfer]').prop("readonly",false);
            };

            enabledKartuKredit = function() {
                $('select[name=namaBankSelect]').prop("disabled",false);
                $('select[name=mesinEDCSelect]').prop("disabled",false);
                $('input[name=nomorKartu]').prop("readonly",false);
                $('input[name=noBuktiTransfer]').prop("readonly",true);
            };

            clearAllPembayaran = function() {
                $('#namaBank').val(0);
                $('select[name=namaBankSelect]').val(0);
                $('input[name=mesinEDC]').val(0);
                $('select[name=mesinEDCSelect]').val(0);
                $('input[name=nomorKartu]').val('');
                $('input[name=noBuktiTransfer]').val('');
                $('input[name=outBankCharge]').val('');
            };

            changeMetodeBayar  = function() {
                var metodeBayarSelect = $('select[name=metodeBayarSelect] option:selected').val();
                $('input[name=metodeBayar]').val(metodeBayarSelect);
                if(metodeBayarSelect==1){ //Cash
                    $('input[name=nomorWBS]').prop("readonly",true);
                    $('select[name=namaBankSelect]').prop("disabled",true);
                    $('select[name=mesinEDCSelect]').prop("disabled",true);
                    $('input[name=nomorKartu]').prop("readonly",true);
                    $('input[name=noBuktiTransfer]').prop("readonly",true);
                }else if(metodeBayarSelect==2){ //Transfer
                    $('input[name=nomorWBS]').prop("readonly",true);
                    $('select[name=namaBankSelect]').prop("disabled",false);
                    $('select[name=mesinEDCSelect]').prop("disabled",true);
                    $('input[name=nomorKartu]').prop("readonly",true);
                    $('input[name=noBuktiTransfer]').prop("readonly",false);
                }else if(metodeBayarSelect==3){ //DEBIT
                    $('input[name=nomorWBS]').prop("readonly",true);
                    $('select[name=namaBankSelect]').prop("disabled",false);
                    $('select[name=mesinEDCSelect]').prop("disabled",false);
                    $('input[name=nomorKartu]').prop("readonly",false);
                    $('input[name=noBuktiTransfer]').prop("readonly",true);
                }else if(metodeBayarSelect==4){ //WBS
                    $('input[name=nomorWBS]').prop("readonly",false);
                    $('select[name=namaBankSelect]').prop("disabled",true);
                    $('select[name=mesinEDCSelect]').prop("disabled",true);
                    $('input[name=nomorKartu]').prop("readonly",true);
                    $('input[name=noBuktiTransfer]').prop("readonly",true);
                }else if(metodeBayarSelect==5){ //Credit Card
                    $('input[name=nomorWBS]').prop("readonly",true);
                    $('select[name=namaBankSelect]').prop("disabled",false);
                    $('select[name=mesinEDCSelect]').prop("disabled",false);
                    $('input[name=nomorKartu]').prop("readonly",false);
                    $('input[name=noBuktiTransfer]').prop("readonly",true);
                }else if(metodeBayarSelect==6){ //Third Party
                    $('input[name=nomorWBS]').prop("readonly",true);
                    $('select[name=namaBankSelect]').prop("disabled",true);
                    $('select[name=mesinEDCSelect]').prop("disabled",true);
                    $('input[name=nomorKartu]').prop("readonly",true);
                    $('input[name=noBuktiTransfer]').prop("readonly",true);
                    var tobar = $("#totalBayar").val();
                    document.getElementById('totalUang').value = tobar;
                }else{
                    $('input[name=nomorWBS]').prop("readonly",true);
                    $('select[name=namaBankSelect]').prop("disabled",true);
                    $('select[name=mesinEDCSelect]').prop("disabled",true);
                    $('input[name=nomorKartu]').prop("readonly",true);
                    $('input[name=noBuktiTransfer]').prop("readonly",true);
                }
            };

            changeNamaBank = function() {
                var namaBankSelect = $('select[name=namaBankSelect] option:selected').val();
                var mesinEDCSelect = $('select[name=mesinEDCSelect] option:selected').val();
                $('#namaBank').val(namaBankSelect);
                if(namaBankSelect!="0" && mesinEDCSelect!="0"){
                    getRate(namaBankSelect, mesinEDCSelect);
                }else{
                    $('input[name=outBankCharge]').val(0);
                }
            };

            changeMesinEDC = function() {
                var mesinEDCSelect = $('select[name=mesinEDCSelect] option:selected').val();
                var namaBankSelect = $('select[name=namaBankSelect] option:selected').val();
                $('input[name=mesinEDC]').val(mesinEDCSelect);
                if(mesinEDCSelect!="0" && namaBankSelect!="0"){
                    getRate(namaBankSelect, mesinEDCSelect);
                }else{
                    $('input[name=outBankCharge]').val(0);
                }
            };

            getRate = function(bank,edc) {
                $.ajax({
                    url: '${request.contextPath}/kwitansi/getRate',
                    type: 'POST',
                    data: { namaBank: bank, mesinEDC: edc },
                    success : function(data){
                        $('input[name=outBankCharge]').val(data.m703RatePersen);
                    },
                    error: function() {
                        alert('Can not gate rate');
                        $('input[name=outBankCharge]').val(0);
                    }
                });
            };

            isValid = function() {
                var metodeBayar = $('input[name=metodeBayar]').val();
                var namaBank = $('#namaBank').val();
                if(metodeBayar==2 ){
                    if(namaBank=="0"){msg += "Bank harus dipilih \n";}

                    var noBuktiTransfer = $('input[name=noBuktiTransfer]').val();
                    if(!noBuktiTransfer || noBuktiTransfer==''){msg += 'Nomor Bukti Transfer harus diisi \n';}
                }else if(metodeBayar==5 || metodeBayar==3){
                    if(namaBank=="0"){msg += "Bank harus dipilih \n";}

                    var mesinEDC = $('input[name=mesinEDC]').val();
                    if(mesinEDC=="0"){msg += "Mesin EDC harus dipilih \n";}

                    var nomorKartu = $('input[name=nomorKartu]').val();
                    if(!nomorKartu || nomorKartu==''){msg += 'Nomor Kartu harus diisi \n';}
                }
                var terimaDari = $('input[name=terimaDari]').val();
                var totalBayar = $('input[name=totalBayar]').val();
                if(!terimaDari || terimaDari=='0'){msg += 'Terima Dari harus diisi \n';}
                if(!totalBayar || totalBayar=='0'){msg += 'Total Bayar harus diisi \n';}
                return msg=='';
            };

            inputBayar = function() {
                var arrData = {};
                var metodeBayar = $('#metodeBayarSelect').val();
                arrData["metodeBayar"] = metodeBayar;
                if(metodeBayar==2){
                    arrData["noBuktiTransfer"] = $('input[name=noBuktiTransfer]').val();
                    arrData["namaBank"] = $('#namaBank').val();
                    arrData["bankCharge"] = $('input[name=outBankCharge]').val();
                }else if(metodeBayar==4){
                    arrData["nomorKartu"] = $('input[name=nomorKartu]').val();
                    arrData["namaBank"] = $('#namaBank').val();
                    arrData["mesinEDC"] = $('input[name=mesinEDC]').val();
                    arrData["bankCharge"] = $('input[name=outBankCharge]').val();
                }
                else if(metodeBayar==5 || metodeBayar==3){
                    arrData["nomorKartu"] = $('input[name=nomorKartu]').val();
                    arrData["namaBank"] = $('#namaBank').val();
                    arrData["mesinEDC"] = $('input[name=mesinEDC]').val();
                    arrData["bankCharge"] = $('input[name=outBankCharge]').val();
                }
                arrData["terimaDari"] = $('input[name=terimaDari]').val();
                arrData["untukPembayaran"] = $('.untukPembayaran').val();
                arrData["totalBayar"] = $('input[name=totalBayar]').val();
                $.ajax({
                        url: '${request.contextPath}/kwitansi/inputBayar',
                        type: 'POST',
                        data: arrData,
                        success : function(data){
                            if(data.err){
                                clearAllPembayaran();
                                disableAllPembayaran();
                                alert(data.err);
                            }else{
                                $('#totalDetailPembayaran').text(data.total);
                                reloadKwitansiTable();
                            }
                        },
                        error: function() {
                            clearAllPembayaran();
                            disableAllPembayaran();
                            return false;
                        }
                });
                if(checkPembayaran()=="1"){
                    $("#bayar").attr("disabled", true);
                    $("#save").attr("disabled", false);
                    $("#print").attr("disabled", false);
                    $("#preview").attr("disabled", false);
                }else{
                    $("#bayar").attr("disabled", false);
                    $("#save").attr("disabled", true);
                    $("#print").attr("disabled", true);
                    $("#preview").attr("disabled", true);
                }
            };

            checkPembayaran = function() {
                var onRisk = parseInt($('#outJumlahOnRisk').val());
                var bookingFee = parseInt($('#outJumlahMinBookingFee').val());
                var check = "0";
                var total = parseInt($('#totalDetailPembayaran').text());
                var jenisBayarSelect = $('#jenisBayarSelect').val();
                var totalInput = parseInt($('#totalBayar').val());
                if(jenisBayarSelect=="2"){
                    if(total+totalInput >= onRisk){check = "1";}else{check = "0";}
                }else{
                    if(total+totalInput >= bookingFee){check = "1";}else{check = "0";}
                }
                console.log(check)
                return check;
            }

            bayarData = function() {
                msg = '';
                if(isValid()){
                    inputBayar();
                }else{
                    alert(msg);
                    return false;
                }
            };

            doSave = function() {
                var arrData = {};
                arrData["jenisBayar"] = $('input[name=jenisBayar]').val();
                arrData["terimaDari"] = $('input[name=terimaDari]').val();
                arrData["nopol"] = $('input[name=outNomorPolisi]').val();
                arrData["totalUang"] = $('#totalDetailPembayaran').text();
                arrData["kasir"] = $('input[name=outKasir]').val();
                arrData["divisi"] = $('input[name=outDivisi]').val();
                arrData["untukPembayaran"] = $('#untukPembayaran').val();
                $.ajax({
                    url: '${request.contextPath}/kwitansi/save',
                    type: 'POST',
                    data: arrData,
                    success : function(data){
                        $('#noKuitansi').text(data.kwitansiId);
                        $('#save').attr("disabled",true);
                        $('#bayar').attr("disabled",true);
                    },
                    error: function() {
                        alert('Data can\'t be saved !!!');
                        $('#noKuitansi').text('');
                    }
                });
            };

            saveData = function() {
                var total = parseInt($('#totalDetailPembayaran').text());
                var noKuitansi = $('#noKuitansi').text();
                if(noKuitansi && noKuitansi!="null"){
                    alert('Data sudah pernah disimpan');
                    return false;
                }else{
                    if(checkPembayaran()=="1"){
                        if (total > 0) {
                           doSave();
                        } else {
                            alert('Total Detail Pembayaran tidak boleh 0 rupiah, silahkan simpan pembayaran terlebih dahulu!');
                            return false;
                        }
                    }else{
                        alert('Total Pembayaran kurang dari Booking Fee / On Risk');
                        return false;
                    }
                }
            };

            closeData = function() {
                var url = '${request.contextPath}/#/home';
                $(location).attr('href',url);
            };

            isNumberKey = function(e){
                var charCode = (e.which) ? e.which : e.keyCode;
                return !(charCode > 31 && (charCode < 48 || charCode > 57)) || (charCode = 189);
            };

            previewData = function() {
                var noKuitansi = $('#noKuitansi').text();
                if(noKuitansi){
                    doPreview(noKuitansi);
                }else{
                    alert('Data tidak ditemukan');
                    return false;
                }
            };

            doPreview = function(a){
                $("#kuitansiListPreviewContent").empty();
                $.ajax({
                    type: 'POST',
                    url: '${request.contextPath}/slipList/previewKuitansi',
                    data:{nomorKuitansi:a},
                    success: function (data) {
                        $("#kuitansiListPreviewContent").html(data);
                        $("#kuitansiListPreviewModal").modal({
                            "backdrop": "static",
                            "keyboard": true,
                            "show": true
                        }).css({
                            'width': '1200px',
                            'margin-left': function () {
                                return - ( $(this).width() / 2 );
                            }
                        });
                    },
                    error: function () {
                        alert('Data not found');
                        return false;
                    },
                    complete: function () {
                        $('#spinner').fadeOut();
                    }
                });
            };

            printData = function(){
                var noKuitansi = $('#noKuitansi').text();
                if(noKuitansi){
                    doPrint(noKuitansi);
                }else{
                    alert('Data tidak ditemukan');
                    return false;
                }
            };

            doPrint = function(a){
                var jenisBayar = $("#jenisBayar").val();
                window.location = "${request.contextPath}/kwitansi/print?nomorKuitansi="+a+"&jenis="+jenisBayar;
            };
        });
    </g:javascript>
</head>

<body>
    <div class="navbar box-header no-border">
        <span class="pull-left">${message(code: 'delivery.kwitansi.label', default: 'Delivery - Kwitansi')}</span>
    </div>
    <div>&nbsp;</div>
    <div class="box" style="margin-bottom: 0; padding-bottom: 0;">
        <table width="100%" style="border-spacing: 5px; border-collapse: separate;">
            <tr valign="top">
                <td width="30%">
                    <span style="text-decoration: underline;"><g:message code="delivery.kwitansi.left.label" default="Data Work Order / Appoinment"/></span>
                    <div class="box" style="margin-bottom: 0; padding-bottom: 0;">
                        <legend style="font-size: small;">
                            <table width="100%" style="border-spacing: 1px; border-collapse: separate; margin-bottom: 5px;">
                                <tr>
                                    <td width="35%">Jenis Bayar</td>
                                    <td width="60%">
                                        <g:select style="width:100%" name="jenisBayarSelect" id="jenisBayarSelect" onchange="changeJenisBayar();" from="${jenisBayar}" optionKey="tipeKey" optionValue="tipeValue"/>
                                        <input type="hidden" id="jenisBayar" name="jenisBayar" value="0"/>
                                        <input type="hidden" id="outDivisi" name="outDivisi"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="30%">Nomor WO/Appointment</td>
                                    <td width="70%">
                                        <g:textField name="nomorWO" id="nomorWO" style="width:95%" onkeypress="return isNumberKey(event)"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="right">
                                        <g:field type="button" style="width: 90px" class="btn btn-primary search" onclick="searchData();"
                                                 name="search" id="search" value="${message(code: 'default.ok.label', default: 'OK')}" />
                                    </td>
                                </tr>
                            </table>
                        </legend>
                        <table width="100%" border="1" style="margin-bottom: 10px;">
                            <tr style="height: 30px;">
                                <td width="35%" style="padding-left: 5px;">Nomor WO/Appointment</td>
                                <td width="65%" align="center"><g:textField name="outNomorWO" id="outNomorWO" style="margin: 1px; width: 90%" readonly=""/></td>
                            </tr>
                            <tr style="height: 30px;">
                                <td width="35%" style="padding-left: 5px;">Jenis Service</td>
                                <td width="65%" align="center"><g:textField name="outJenisService" id="outJenisService" style="margin: 1px; width: 90%" readonly=""/></td>
                            </tr>
                            <tr style="height: 30px;">
                                <td width="35%" style="padding-left: 5px;">Nama Customer</td>
                                <td width="65%" align="center"><g:textField name="outNamaCustomer" id="outNamaCustomer" style="margin: 1px; width: 90%" readonly=""/></td>
                            </tr>
                            <tr style="height: 30px;">
                                <td width="35%" style="padding-left: 5px;">Nomor Polisi</td>
                                <td width="65%" align="center"><g:textField name="outNomorPolisi" id="outNomorPolisi" style="margin: 1px; width: 90%" readonly=""/></td>
                            </tr>
                            <tr style="height: 30px;">
                                <td width="35%" style="padding-left: 5px;">Model</td>
                                <td width="65%" align="center"><g:textField name="outModel" id="outModel" style="margin: 1px; width: 90%" readonly=""/></td>
                            </tr>
                            <tr style="height: 30px;">
                                <td width="35%" style="padding-left: 5px;">Customer SPK ?</td>
                                <td width="65%" align="center"><g:textField name="outCustomerSPK" id="outCustomerSPK" style="margin: 1px; width: 90%" readonly=""/></td>
                            </tr>
                            <tr style="height: 30px;">
                                <td width="35%" style="padding-left: 5px;">Jumlah On Risk</td>
                                <td width="65%" align="center"><g:textField name="outJumlahOnRisk" id="outJumlahOnRisk" style="margin: 1px; width: 90%" readonly=""/></td>
                            </tr>
                            <tr style="height: 30px;">
                                <td width="35%" style="padding-left: 5px;">Jumlah Min Booking Fee</td>
                                <td width="65%" align="center"><g:textField name="outJumlahMinBookingFee" id="outJumlahMinBookingFee" style="margin: 1px; width: 90%" readonly=""/></td>
                            </tr>
                            <tr style="height: 30px;">
                                <td width="35%" style="padding-left: 5px;">Kasir</td>
                                <td width="65%" align="center"><g:textField name="outKasir" id="outKasir" style="margin: 1px; width: 90%" readonly=""/></td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td width="70%">
                    <span style="text-decoration: underline;"><g:message code="delivery.settlement.right.top.label" default="Pembayaran Kwitansi"/></span>
                    <div class="box" style="padding-top: 0; padding-left: 15px;">
                        <table width="100%" style="border-spacing: 5px; border-collapse: separate;">
                            <tr valign="top">
                                <td width="45%">
                                    <table width="100%" border="1">
                                        <tr style="height: 30px;">
                                            <td width="40%" style="padding-left: 5px;">Metode Bayar</td>
                                            <td width="60%" valign="midle" align="center">
                                                <g:select style="margin: 1px;" name="metodeBayarSelect" id="metodeBayarSelect" onchange="changeMetodeBayar();" from="${metodeBayar}" optionKey="m701Id" optionValue="m701MetodeBayar" disabled=""/>
                                                <input type="hidden" id="metodeBayar" name="metodeBayar" value="1">
                                            </td>
                                        </tr>
                                        <tr style="height: 30px;">
                                            <td width="40%" style="padding-left: 5px;">Nama Bank</td>
                                            <td width="60%" align="center">
                                                <g:select style="margin: 1px;" name="namaBankSelect" id="namaBankSelect" onchange="changeNamaBank();" from="${namaBank}" optionKey="${{it.bank.m702ID}}" optionValue="${{it.bank.m702NamaBank + '-' + it.bank.m702Cabang}}" disabled="" noSelection="${['0':'Please Select']}"/>
                                                <input type="hidden" id="namaBank" name="namaBank" value="0">
                                            </td>
                                        </tr>
                                        <tr style="height: 30px;">
                                            <td width="40%" style="padding-left: 5px;">Mesin EDC</td>
                                            <td width="60%" align="center">
                                                <g:select style="margin: 1px;" name="mesinEDCSelect" id="mesinEDCSelect" onchange="changeMesinEDC();" from="${mesinEDC}" optionKey="m704Id" optionValue="m704NamaMesinEdc" disabled="" noSelection="${['0':'Please Select']}"/>
                                                <input type="hidden" id="mesinEDC" name="mesinEDC" value="0">
                                            </td>
                                        </tr>
                                        <tr style="height: 30px;">
                                            <td width="40%" style="padding-left: 5px;">Nomor Kartu</td>
                                            <td width="60%" align="center"><g:textField name="nomorKartu" id="nomorKartu" style="margin: 1px;" readonly="" maxlength="16" onkeypress="return isNumberKey(event)"/></td>
                                        </tr>
                                        <tr style="height: 30px;">
                                            <td width="40%" style="padding-left: 5px;">No Bukti Transfer</td>
                                            <td width="60%" align="center"><g:textField name="noBuktiTransfer" id="noBuktiTransfer" style="margin: 1px;" readonly="" onkeypress="return isNumberKey(event)"/></td>
                                        </tr>
                                        <tr style="height: 30px;">
                                            <td width="40%" style="padding-left: 5px;">Bank Charge</td>
                                            <td width="60%" align="center"><g:textField name="outBankCharge" id="outBankCharge" style="margin: 1px; width: 50px" readonly=""/>%</td>
                                        </tr>
                                    </table>
                                    <br/>

                                    <span style="text-decoration: underline;">Input Pembayaran</span>
                                    <table border="1" width="100%">
                                        <tr style="height: 30px;">
                                            <td width="40%" style="padding-left: 5px;">Terima Dari</td>
                                            <td width="60%" align="center"><g:textField name="terimaDari" id="terimaDari" style="margin: 1px;" readonly=""/></td>
                                        </tr>
                                        <tr style="height: 60px;">
                                            <td width="40%" style="padding-left: 5px;">Untuk Pembayaran</td>
                                            <td width="60%" align="center"><g:textArea class="untukPembayaran" name="untukPembayaran" id="untukPembayaran" rows="2" style="width:204px;height:44px;margin-bottom:0;resize:none;" disabled=""/></td>
                                        </tr>
                                        <tr style="height: 30px;">
                                            <td width="40%" style="padding-left: 5px;">Total Bayar</td>
                                            <td width="60%" align="center"><g:textField name="totalBayar" id="totalBayar" style="margin: 1px;" readonly="" onkeypress="return isNumberKey(event)"/></td>
                                        </tr>
                                    </table>

                                    <br/>
                                    <g:field type="button" style="width: 150px" class="btn btn-primary" onclick="bayarData();"
                                             name="bayar" id="bayar" value="${message(code: 'default.bayar.label', default: 'Save Pembayaran')}" />
                                    <br/>

                                </td>
                                <td width="55%">
                                    <span style="text-decoration: underline;">Kalkulator</span>
                                    <table width="100%" border="1">
                                        <tr>
                                            <td width="50%" style="padding-left: 5px;">Total Uang</td>
                                            <td width="50%">
                                                <g:textField name="totalUang" id="totalUang" style="margin: 1px;" readonly="" onkeypress="return isNumberKey(event)"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="50%" style="padding-left: 5px;">Kembalikan</td>
                                            <td width="50%">
                                                <g:textField name="kembalikan" id="kembalikan" style="margin: 1px;" readonly=""/>
                                            </td>
                                        </tr>
                                    </table>
                                    <br/>
                                    <span style="text-decoration: underline;">Detail Pembayaran</span>
                                    <div class="box" style="padding: 0 15px; width: 400px; margin-bottom: 0;">
                                        <g:render template="kwitansiDataTables"/>
                                        <table width="100%" border="1">
                                            <tr style="height:30px;">
                                                <td width="70%" style="padding-left: 5px;">Total Detail Pembayaran</td>
                                                <td width="30%" align="right"><span id="totalDetailPembayaran" style="font-weight: bold">0</span></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <br/>
                                    &nbsp;
                                    <g:field type="button" style="width: 90px" class="btn btn-primary" onclick="saveData();" disabled="true"
                                             name="save" id="save" value="${message(code: 'default.save.kuitansi.label', default: 'Save Kuitansi')}"/>
                                    &nbsp;
                                    <g:field type="button" style="width: 90px" class="btn btn-primary" onclick="printData();" disabled="true"
                                             name="print" id="print" value="${message(code: 'default.print.kuitansi.label', default: 'Print Kuitansi')}" />
                                    &nbsp;
                                    <g:field type="button" style="width: 90px" class="btn btn-primary" onclick="previewData();" disabled="true"
                                             name="preview" id="preview" value="${message(code: 'default.preview.kuitansi.label', default: 'Preview Kuitansi')}" />
                                    <br/>
                                    <br/>
                                    <table width="100%">
                                        <tr style="height: 30px;">
                                            <td width="40%" style="border: 1px solid #000000; padding-left: 5px;">NOMOR KUITANSI</td>
                                            <td width="60%" style="border: 1px solid #000000;"><span id="noKuitansi" name="noKuitansi" style="font-weight: bold"></span></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <br/>
    <ul class="nav pull-right">
        <g:field type="button" style="width: 90px" class="btn btn-primary" onclick="closeData();"
                 name="close" id="close" value="${message(code: 'default.close.label', default: 'Close')}" />
    </ul>

    <!-- Start Modal Preview -->
    <div id="kuitansiListPreviewModal" class="modal fade">
        <div class="modal-dialog" style="width: 1200px;">
            <div class="modal-content" style="width: 1200px;">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 1200px;">
                    <div id="kuitansiListPreviewContent"></div>
                    <div class="iu-content"></div>
                </div>
                <!-- dialog buttons -->
                <div class="modal-footer">
                    <g:field type="button" style="width: 90px" class="btn btn-primary" name="close_modal" id="close_modal"
                             value="${message(code: 'default.close.label', default: 'Close')}" data-dismiss="modal"/>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal Preview -->
</body>
</html>