
<%@ page import="com.kombos.parts.Request" %>

<r:require modules="baseapplayout" />

<g:render template="../menu/maxLineDisplay"/>

<table id="request_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="97%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="request.t162ID.label" default="No Urut" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="request.goods.label" default="Kode Parts" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="request.goods.label" default="Nama Parts" /></div>
            </th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="request.t162Qty1.label" default="Jumlah" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="request.goods.label" default="Satuan" /></div>
            </th>
		</tr>

	</thead>
</table>

<g:javascript>
var requestTable;
var reloadRequestTable;
$(function(){
	
	reloadRequestTable = function() {
		requestTable.fnDraw();
	}	
	
	requestTable = $('#request_datatables').dataTable({
		"sScrollX": "97%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": false,
//		"iDisplayLength" : maxLineDisplay,
		"aoColumns": [

{
	"sName": "request",
	"mDataProp": "norut",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"40px",
	"bVisible": true
}

,

{
	"sName": "goods",
	"mDataProp": "goods",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "goods",
	"mDataProp": "goods2",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t162Qty1",
	"mDataProp": "t162Qty1",
	"aTargets": [2],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
		return '<input id="qty-'+row['id']+'" class="inline-edit" type="text" style="width:218px;" value="'+data+'">';
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "goods",
	"mDataProp": "satuan1",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"130px",
	"bVisible": true
}

]

	});
});
</g:javascript>


			
