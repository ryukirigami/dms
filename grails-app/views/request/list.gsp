
<%@ page import="com.kombos.parts.Request" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'request.label', default: 'Request')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, autoNumeric" />
    <g:javascript>
		$(function(){
			$('#search_t162StaFA').typeahead({

			    source: function (query, process) {
			    alert("aa")
			        return $.get('${request.contextPath}/request/kodeList', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});
    </g:javascript>
		<g:javascript disposition="head">
	var show;
	var noUrut = 1;
	var checkOnValue;
	$(function(){ 

	//$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});
    
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#request-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/request/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadRequestTable();
    		}
		});
		
   	}

});


  //darisini
	$("#requestAddModal").on("show", function() {
		$("#requestAddModal .btn").on("click", function(e) {
			$("#requestAddModal").modal('hide');
		});
	});
	$("#requestAddModal").on("hide", function() {
		$("#requestAddModal a.btn").off("click");
	});

    loadRequestAddModal = function(){
		$("#requestAddContent").empty();
		$.ajax({type:'POST', url:'${request.contextPath}/requestAdd',
   			success:function(data,textStatus){
					$("#requestAddContent").html(data);
				    $("#requestAddModal").modal({
						"backdrop" : "static",
						"keyboard" : true,
						"show" : true
					}).css({'width': '1000px','margin-left': function () {return -($(this).width() / 2);}});

   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
		}


       tambahReq = function(){
            var checkGoods =[];
            $("#requestAdd-table tbody .row-select").each(function() {
                if(this.checked){
                    //var id = $(this).next("input:hidden").val();
                    var nRow = $(this).parents('tr')[0];
					checkGoods.push(nRow);
                }
            });
            if(checkGoods.length<1){
                alert('Anda belum memilih data yang akan ditambahkan');
                loadRequestAddModal();
            } else {
				for (var i=0;i < checkGoods.length;i++){
					var aData = requestAddTable.fnGetData(checkGoods[i]);
					requestTable.fnAddData({
						'id': aData['id'], 
						'norut': parseInt(noUrut),
						'goods': aData['goods'], 
						'goods2': aData['goods2'], 
						't162Qty1': '0', 
						'satuan1':  aData['satuan1']});
					noUrut++;
					$('#qty-'+aData['id']).autoNumeric('init',{
						vMin:'0',
						vMax:'999999999999999999',
						mDec: '2',
						aDec:',',
						aSep:''
					}).keypress(checkOnValue)
					.change(checkOnValue);
				}
				
			}
//			loadRequestAddModal();
      }
	checkOnValue = function() {
		var $this   = $(this);
		var valuedata = "" + $this.val();
		var nRow = $this.parents('tr')[0];
		var checkInput = $('input[type="checkbox"]', nRow);
		if(valuedata == "0,00" || valuedata == "0"){
			checkInput.removeAttr("checked");
		} else {
			checkInput.attr("checked","checked");
		}
	}
    createRequest = function(){
		var formRequest = $('#request-table').find('form');

        var checkGoods =[];
        $("#request-table tbody .row-select").each(function() {
        if(this.checked){
            var id = $(this).next("input:hidden").val();
            var nRow = $(this).parents('tr')[0];
            var aData = requestTable.fnGetData(nRow);
			var qtyInput = $('#qty-' + aData['id'], nRow);
			
            checkGoods.push(id);
			
			formRequest.append('<input type="hidden" name="qty-'+aData['id'] + '" value="'+qtyInput[0].value+'" class="deleteafter">');
        }
        });
        if(checkGoods.length<1){
            alert('Anda belum memilih data yang akan ditambahkan');
             reloadRequestTable();
        }
        
        $("#ids").val(JSON.stringify(checkGoods));
        

        $.ajax({
            url:'${request.contextPath}/request/save',
            type: "POST", // Always use POST when deleting data
            data : formRequest.serialize(),
            success : function(data){
				toastr.success('<div>Request Parts telah dibuat.</div>');
				formRequest.find('.deleteafter').remove();
				$("#ids").val('');
				$('#spinner').fadeIn(1);
                $.ajax({type:'POST',
                    url:'${request.contextPath}/cekKetersediaanParts/list',
                    success:function(data,textStatus){
                        window.location.replace('#/cekKetersediaanParts');
                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){},
                    complete:function(XMLHttpRequest,textStatus){
                        $('#spinner').fadeOut();
                    }
                });
            },
        error: function(xhr, textStatus, errorThrown) {
        alert('Internal Server Error');
        }
        });

        checkGoods = [];

    }
</g:javascript>

	</head>
	<body>
	<div class="navbar box-header no-border">
        <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
        <ul class="nav pull-right">
            <li></li>
			%{--<li><a class="pull-right box-action" href="#"--}%
				%{--style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i--}%
					%{--class="icon-remove"></i>&nbsp;&nbsp;--}%
			%{--</a></li>--}%
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		
		<div class="span12" id="request-table">
			<form id="form-request" class="form-horizontal">
				<input type="hidden" name="ids" id="ids" value="">
				<fieldset>
					<div class="control-group">
						<label class="control-label" for="t162StaFA" style="text-align: left;">Field Action ?
						</label>
						<div id="filter_ac" class="controls">
									<g:radioGroup name="t162StaFA" values="[1,0]" value="1" labels="['Ya','Tidak']" required="">
										${it.radio} <g:message code="${it.label}" />
									</g:radioGroup>	
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="t162NoReff" style="text-align: left;">Nomor Referensi
						</label>
						<div id="filter_nomor" class="controls">
									<input type="text" name="t162NoReff" id="t162NoReff" value="${iNoref}" readonly="readonly" >
						</div>
					</div>
				</fieldset>
			</form>
			<fieldset class="buttons controls" style="padding-bottom: 30px;">
					<button style="width: 170px;height: 30px; border-radius: 5px" class="btn btn-primary add" name="view" id="add" onclick="loadRequestAddModal()">Add Parts</button>
			</fieldset>
            
			<g:render template="dataTables" />
            <g:field type="button" onclick="createRequest();" class="btn btn-primary create" name="tambah" id="tambah" value="${message(code: 'default.button.upload.label', default: 'Request Parts')}" />
		</div>
		<div class="span7" id="request-form" style="display: none;"></div>
	</div>
    <div id="requestAddModal" class="modal fade">
        <div class="modal-dialog" style="width: 1000px;">
            <div class="modal-content" style="width: 1000px;">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 800px;">
                    <div id="requestAddContent"/>
                        %{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}%
                <div class="iu-content"></div>

                </div>
            </div>
        </div>
    </div>
</body>
</html>
