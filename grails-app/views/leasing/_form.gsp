<%@ page import="com.kombos.administrasi.Leasing" %>

<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode != 45  && charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    $(document).ready(function() {
        if($('#m1000Alamat').val().length>0){
            $('#hitung').text(255 - $('#m1000Alamat').val().length);
        }
        $('#m1000Alamat').keyup(function() {
            var len = this.value.length;
            if (len >= 255) {
                this.value = this.value.substring(0, 250);
                len = 255;
            }
            $('#hitung').text(255 - len);
        });
    });
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: leasingInstance, field: 'm1000NamaLeasing', 'error')} required">
	<label class="control-label" for="m1000NamaLeasing">
		<g:message code="leasing.m1000NamaLeasing.label" default="Nama Leasing" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m1000NamaLeasing" maxlength="75" required="" value="${leasingInstance?.m1000NamaLeasing}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: leasingInstance, field: 'm1000Alamat', 'error')} required">
	<label class="control-label" for="m1000Alamat">
		<g:message code="leasing.m1000Alamat.label" default="Alamat" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textArea name="m1000Alamat" id="m1000Alamat" cols="40" rows="5" required="" maxlength="255" value="${leasingInstance?.m1000Alamat}"/>
        <br/>
        <span id="hitung">255</span> Karakter Tersisa.
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: leasingInstance, field: 'm1000Npwp', 'error')}">
	<label class="control-label" for="m1000Npwp">
		<g:message code="leasing.m1000Npwp.label" default="Npwp" />
	</label>
	<div class="controls">
	<g:textField name="m1000Npwp" maxlength="50" value="${leasingInstance?.m1000Npwp}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: leasingInstance, field: 'm1000NoTelp', 'error')} required">
	<label class="control-label" for="m1000NoTelp">
		<g:message code="leasing.m1000NoTelp.label" default="Nomor Telp" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m1000NoTelp" onkeypress="return isNumberKey(event)" maxlength="50" required="" value="${leasingInstance?.m1000NoTelp}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: leasingInstance, field: 'm1000Email', 'error')} required">
	<label class="control-label" for="m1000Email">
		<g:message code="leasing.m1000Email.label" default="Email" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="email" name="m1000Email" maxlength="50" required="" value="${leasingInstance?.m1000Email}"/>
	</div>
</div>

