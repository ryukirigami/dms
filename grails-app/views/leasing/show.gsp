

<%@ page import="com.kombos.administrasi.Leasing" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'leasing.label', default: 'Leasing')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteLeasing;

$(function(){ 
	deleteLeasing=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/leasing/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadLeasingTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-leasing" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="leasing"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${leasingInstance?.m1000NamaLeasing}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m1000NamaLeasing-label" class="property-label"><g:message
					code="leasing.m1000NamaLeasing.label" default="Nama Leasing" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m1000NamaLeasing-label">
						%{--<ba:editableValue
								bean="${leasingInstance}" field="m1000NamaLeasing"
								url="${request.contextPath}/Leasing/updatefield" type="text"
								title="Enter m1000NamaLeasing" onsuccess="reloadLeasingTable();" />--}%
							
								<g:fieldValue bean="${leasingInstance}" field="m1000NamaLeasing"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${leasingInstance?.m1000Alamat}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m1000Alamat-label" class="property-label"><g:message
					code="leasing.m1000Alamat.label" default="Alamat" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m1000Alamat-label">
						%{--<ba:editableValue
								bean="${leasingInstance}" field="m1000Alamat"
								url="${request.contextPath}/Leasing/updatefield" type="text"
								title="Enter m1000Alamat" onsuccess="reloadLeasingTable();" />--}%
							
								<g:fieldValue bean="${leasingInstance}" field="m1000Alamat"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${leasingInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="leasing.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${leasingInstance}" field="staDel"
								url="${request.contextPath}/Leasing/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadLeasingTable();" />--}%
							
								<g:fieldValue bean="${leasingInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${leasingInstance?.m1000Npwp}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m1000Npwp-label" class="property-label"><g:message
					code="leasing.m1000Npwp.label" default="NPWP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m1000Npwp-label">
						%{--<ba:editableValue
								bean="${leasingInstance}" field="m1000Npwp"
								url="${request.contextPath}/Leasing/updatefield" type="text"
								title="Enter m1000Npwp" onsuccess="reloadLeasingTable();" />--}%
							
								<g:fieldValue bean="${leasingInstance}" field="m1000Npwp"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${leasingInstance?.m1000NoTelp}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m1000NoTelp-label" class="property-label"><g:message
					code="leasing.m1000NoTelp.label" default="No Telp" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m1000NoTelp-label">
						%{--<ba:editableValue
								bean="${leasingInstance}" field="m1000NoTelp"
								url="${request.contextPath}/Leasing/updatefield" type="text"
								title="Enter m1000NoTelp" onsuccess="reloadLeasingTable();" />--}%
							
								<g:fieldValue bean="${leasingInstance}" field="m1000NoTelp"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${leasingInstance?.m1000Email}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m1000Email-label" class="property-label"><g:message
					code="leasing.m1000Email.label" default="Email" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m1000Email-label">
						%{--<ba:editableValue
								bean="${leasingInstance}" field="m1000Email"
								url="${request.contextPath}/Leasing/updatefield" type="text"
								title="Enter m1000Email" onsuccess="reloadLeasingTable();" />--}%
							
								<g:fieldValue bean="${leasingInstance}" field="m1000Email"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${leasingInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="leasing.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${leasingInstance}" field="createdBy"
								url="${request.contextPath}/Leasing/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadLeasingTable();" />--}%
							
								<g:fieldValue bean="${leasingInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${leasingInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="leasing.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${leasingInstance}" field="updatedBy"
								url="${request.contextPath}/Leasing/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadLeasingTable();" />--}%
							
								<g:fieldValue bean="${leasingInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${leasingInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="leasing.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${leasingInstance}" field="lastUpdProcess"
								url="${request.contextPath}/Leasing/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadLeasingTable();" />--}%
							
								<g:fieldValue bean="${leasingInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${leasingInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="leasing.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${leasingInstance}" field="dateCreated"
								url="${request.contextPath}/Leasing/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadLeasingTable();" />--}%
							
								<g:formatDate date="${leasingInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${leasingInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="leasing.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${leasingInstance}" field="lastUpdated"
								url="${request.contextPath}/Leasing/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadLeasingTable();" />--}%
							
								<g:formatDate date="${leasingInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('leasing');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${leasingInstance?.id}"
					update="[success:'leasing-form',failure:'leasing-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteLeasing('${leasingInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
