
<%@ page import="com.kombos.administrasi.Leasing" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>
<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode != 45  && charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</g:javascript>

<table id="leasing_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="leasing.m1000NamaLeasing.label" default="Nama Leasing" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="leasing.m1000Alamat.label" default="Alamat Leasing" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="leasing.m1000Npwp.label" default="NPWP" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="leasing.m1000NoTelp.label" default="Nomor Telp" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="leasing.m1000Email.label" default="Email" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m1000NamaLeasing" style="padding-top: 0px;position:relative; margin-top: 0px;width: 175px;">
					<input type="text" name="search_m1000NamaLeasing" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m1000Alamat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 175px;">
					<input type="text" name="search_m1000Alamat" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m1000Npwp" style="padding-top: 0px;position:relative; margin-top: 0px;width: 175px;">
					<input type="text" name="search_m1000Npwp" class="search_init" onkeypress="return isNumberKey(event)" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m1000NoTelp" style="padding-top: 0px;position:relative; margin-top: 0px;width: 175px;">
					<input type="text" name="search_m1000NoTelp" class="search_init" onkeypress="return isNumberKey(event)" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m1000Email" style="padding-top: 0px;position:relative; margin-top: 0px;width: 175px;">
					<input type="text" name="search_m1000Email" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var leasingTable;
var reloadLeasingTable;
$(function(){
	
	reloadLeasingTable = function() {
		leasingTable.fnDraw();
	}

	var recordsLeasingPerPage = [];
    var anLeasingSelected;
    var jmlRecLeasingPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	leasingTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	leasingTable = $('#leasing_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsLeasing = $("#leasing_datatables tbody .row-select");
            var jmlLeasingCek = 0;
            var nRow;
            var idRec;
            rsLeasing.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsLeasingPerPage[idRec]=="1"){
                    jmlLeasingCek = jmlLeasingCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsLeasingPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecLeasingPerPage = rsLeasing.length;
            if(jmlLeasingCek==jmlRecLeasingPerPage && jmlRecLeasingPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m1000NamaLeasing",
	"mDataProp": "m1000NamaLeasing",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m1000Alamat",
	"mDataProp": "m1000Alamat",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m1000Npwp",
	"mDataProp": "m1000Npwp",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m1000NoTelp",
	"mDataProp": "m1000NoTelp",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m1000Email",
	"mDataProp": "m1000Email",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

//,
//
//{
//	"sName": "lastUpdProcess",
//	"mDataProp": "lastUpdProcess",
//	"aTargets": [5],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m1000NamaLeasing = $('#filter_m1000NamaLeasing input').val();
						if(m1000NamaLeasing){
							aoData.push(
									{"name": 'sCriteria_m1000NamaLeasing', "value": m1000NamaLeasing}
							);
						}
	
						var m1000Alamat = $('#filter_m1000Alamat input').val();
						if(m1000Alamat){
							aoData.push(
									{"name": 'sCriteria_m1000Alamat', "value": m1000Alamat}
							);
						}
	
						var m1000Npwp = $('#filter_m1000Npwp input').val();
						if(m1000Npwp){
							aoData.push(
									{"name": 'sCriteria_m1000Npwp', "value": m1000Npwp}
							);
						}
	
						var m1000NoTelp = $('#filter_m1000NoTelp input').val();
						if(m1000NoTelp){
							aoData.push(
									{"name": 'sCriteria_m1000NoTelp', "value": m1000NoTelp}
							);
						}
	
						var m1000Email = $('#filter_m1000Email input').val();
						if(m1000Email){
							aoData.push(
									{"name": 'sCriteria_m1000Email', "value": m1000Email}
							);
						}
	
//						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
//						if(lastUpdProcess){
//							aoData.push(
//									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
//							);
//						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

	$('.select-all').click(function(e) {

        $("#leasing_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsLeasingPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsLeasingPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#leasing_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsLeasingPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anLeasingSelected = leasingTable.$('tr.row_selected');
            if(jmlRecLeasingPerPage == anLeasingSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsLeasingPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
