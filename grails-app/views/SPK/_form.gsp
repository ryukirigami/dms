<%@ page import="com.kombos.customerprofile.SPK" %>

<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    $(function(){

        var checkin = $('#t191TglAwal').datepicker({
            onRender: function(date) {
                return '';
            }
        }).on('changeDate', function(ev) {
                    var newDate = new Date(ev.date)
                    newDate.setDate(newDate.getDate() + 1);
                    checkout.setValue(newDate);
                    checkin.hide();
                    $('#t191TglAkhir')[0].focus();
                }).data('datepicker');

        var checkout = $('#t191TglAkhir').datepicker({
            onRender: function(date) {
                return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
                    checkout.hide();
                }).data('datepicker');

    })
</g:javascript>

<g:if test="${SPKInstance?.t191IDSPK}">
    <div class="control-group fieldcontain ${hasErrors(bean: SPKInstance, field: 't191IDSPK', 'error')} required">
        <label class="control-label" for="t191IDSPK">
            <g:message code="SPK.t191IDSPK.label" default="ID SPK" />
            <span class="required-indicator">*</span>
        </label>
        <div class="controls">
            ${SPKInstance?.t191IDSPK}
        </div>
    </div>
</g:if>

<div class="control-group fieldcontain ${hasErrors(bean: SPKInstance, field: 'company', 'error')} required">
    <label class="control-label" for="company">
        <g:message code="SPK.company.label" default="Nama Perusahaan" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="company" name="company.id" from="${com.kombos.maintable.Company.list()}" optionKey="id" required="" value="${SPKInstance?.company?.id}" class="many-to-one"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: SPKInstance, field: 't191NoSPK', 'error')} required">
	<label class="control-label" for="t191NoSPK">
		<g:message code="SPK.t191NoSPK.label" default="No SPK" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t191NoSPK" maxlength="15" required=""  onkeypress="return isNumberKey(event)" value="${SPKInstance?.t191NoSPK}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: SPKInstance, field: 't191TanggalSPK', 'error')} required">
	<label class="control-label" for="t191TanggalSPK">
		<g:message code="SPK.t191TanggalSPK.label" default="Tanggal SPK" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <ba:datePicker name="t191TanggalSPK" precision="day"  value="${SPKInstance?.t191TanggalSPK}" format="dd/mm/yyyy" required="true"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: SPKInstance, field: 't191TglAwal', 'error')} required">
	<label class="control-label" for="t191TglAwal">
		<g:message code="SPK.masaberlaku.label" default="Masa Berlaku SPK" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <ba:datePicker name="t191TglAwal" precision="day"  value="${SPKInstance?.t191TglAwal}"  format="dd/mm/yyyy" required="true"/>

	</div>
</div>

<div class="control-group">
    <label class="control-label" >

    </label>
    <div class="controls">
        s.d
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: SPKInstance, field: 't191TglAkhir', 'error')} required">
    <label class="control-label" for="t191TglAkhir">
       </label>
    <div class="controls">
        <ba:datePicker name="t191TglAkhir" precision="day"  value="${SPKInstance?.t191TglAkhir}" format="dd/mm/yyyy" required="true" />
    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: SPKInstance, field: 't191JmlSPK', 'error')} required">
	<label class="control-label" for="t191JmlSPK">
		<g:message code="SPK.t191JmlSPK.label" default="Jumlah Total (Rp)" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t191JmlSPK" value="${fieldValue(bean: SPKInstance, field: 't191JmlSPK')}" maxlength="15" style="text-align: right"" onkeypress="return isNumberKey(event)" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: SPKInstance, field: 't191MaxHariPelunasan', 'error')} required">
	<label class="control-label" for="t191MaxHariPelunasan">
		<g:message code="SPK.t191MaxHariPelunasan.label" default="Maksimal Lama Pelunasan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t191MaxHariPelunasan"  maxlength="4" value="${SPKInstance.t191MaxHariPelunasan}" style="width: 100pt;text-align: right" onkeypress="return isNumberKey(event)" required=""/>
    <g:message code="SPK.t191MaxHariPelunasanKet.label" default="Hari setelah berlaku SPK" />

    </div>
</div>

