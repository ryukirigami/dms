
<%@ page import="com.kombos.customerprofile.SPK" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>
<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</g:javascript>
<table id="SPK_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="SPK.company.label" default="Nama Perusahaan" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
				<div><g:message code="SPK.t191NoSPK.label" default="No SPK" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="SPK.t191TanggalSPK.label" default="Tanggal SPK" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="SPK.t191TglAwal.label" default="Tanggal Awal" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="SPK.t191TglAkhir.label" default="Tanggal Akhir" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="SPK.t191JmlSPK.label" default="Jumlah SPK" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="SPK.t191MaxHariPelunasan.label" default="Maks. Lama Pelunasan" /></div>
			</th>



		
		</tr>
		<tr>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_company" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_company" class="search_init" />
                </div>
            </th>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t191NoSPK" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t191NoSPK" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t191TanggalSPK" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_t191TanggalSPK" value="date.struct">
					<input type="hidden" name="search_t191TanggalSPK_day" id="search_t191TanggalSPK_day" value="">
					<input type="hidden" name="search_t191TanggalSPK_month" id="search_t191TanggalSPK_month" value="">
					<input type="hidden" name="search_t191TanggalSPK_year" id="search_t191TanggalSPK_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t191TanggalSPK_dp" value="" id="search_t191TanggalSPK" class="search_init">
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t191TglAwal" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_t191TglAwal" value="date.struct">
					<input type="hidden" name="search_t191TglAwal_day" id="search_t191TglAwal_day" value="">
					<input type="hidden" name="search_t191TglAwal_month" id="search_t191TglAwal_month" value="">
					<input type="hidden" name="search_t191TglAwal_year" id="search_t191TglAwal_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t191TglAwal_dp" value="" id="search_t191TglAwal" class="search_init">
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t191TglAkhir" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_t191TglAkhir" value="date.struct">
					<input type="hidden" name="search_t191TglAkhir_day" id="search_t191TglAkhir_day" value="">
					<input type="hidden" name="search_t191TglAkhir_month" id="search_t191TglAkhir_month" value="">
					<input type="hidden" name="search_t191TglAkhir_year" id="search_t191TglAkhir_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t191TglAkhir_dp" value="" id="search_t191TglAkhir" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t191JmlSPK" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t191JmlSPK" class="search_init"  onkeypress="return isNumberKey(event)"/>
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t191MaxHariPelunasan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t191MaxHariPelunasan" class="search_init"  onkeypress="return isNumberKey(event)"/>
				</div>
			</th>
	




		</tr>
	</thead>
</table>

<g:javascript>
var SPKTable;
var reloadSPKTable;
$(function(){
	
	reloadSPKTable = function() {
		SPKTable.fnDraw();
	}
	
	var recordsSPKperpage = [];//new Array();
    var anSPKSelected;
    var jmlRecSPKPerPage=0;
    var id;


	
	$('#search_t191TanggalSPK').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t191TanggalSPK_day').val(newDate.getDate());
			$('#search_t191TanggalSPK_month').val(newDate.getMonth()+1);
			$('#search_t191TanggalSPK_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			SPKTable.fnDraw();	
	});

	

	$('#search_t191TglAwal').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t191TglAwal_day').val(newDate.getDate());
			$('#search_t191TglAwal_month').val(newDate.getMonth()+1);
			$('#search_t191TglAwal_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			SPKTable.fnDraw();	
	});

	

	$('#search_t191TglAkhir').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t191TglAkhir_day').val(newDate.getDate());
			$('#search_t191TglAkhir_month').val(newDate.getMonth()+1);
			$('#search_t191TglAkhir_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			SPKTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	SPKTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	SPKTable = $('#SPK_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsSPK = $("#SPK_datatables tbody .row-select");
            var jmlSPKCek = 0;
            var nRow;
            var idRec;
            rsSPK.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsSPKperpage[idRec]=="1"){
                    jmlSPKCek = jmlSPKCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsSPKperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecSPKPerPage = rsSPK.length;
            if(jmlSPKCek==jmlRecSPKPerPage && jmlRecSPKPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "company",
	"mDataProp": "company",
	"aTargets": [10],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "t191NoSPK",
	"mDataProp": "t191NoSPK",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "t191TanggalSPK",
	"mDataProp": "t191TanggalSPK",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t191TglAwal",
	"mDataProp": "t191TglAwal",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t191TglAkhir",
	"mDataProp": "t191TglAkhir",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t191JmlSPK",
	"mDataProp": "t191JmlSPK",
	"aTargets": [4],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        if(data != null)
        return '<span class="pull-right numeric">'+data+'</span>';
        else
        return '<span></span>';
    },
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t191MaxHariPelunasan",
	"mDataProp": "t191MaxHariPelunasan",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}




],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var t191NoSPK = $('#filter_t191NoSPK input').val();
						if(t191NoSPK){
							aoData.push(
									{"name": 'sCriteria_t191NoSPK', "value": t191NoSPK}
							);
						}

                    	var t191TanggalSPK = $('#search_t191TanggalSPK').val();
						var t191TanggalSPKDay = $('#search_t191TanggalSPK_day').val();
						var t191TanggalSPKMonth = $('#search_t191TanggalSPK_month').val();
						var t191TanggalSPKYear = $('#search_t191TanggalSPK_year').val();
						
						if(t191TanggalSPK){
							aoData.push(
									{"name": 'sCriteria_t191TanggalSPK', "value": "date.struct"},
									{"name": 'sCriteria_t191TanggalSPK_dp', "value": t191TanggalSPK},
									{"name": 'sCriteria_t191TanggalSPK_day', "value": t191TanggalSPKDay},
									{"name": 'sCriteria_t191TanggalSPK_month', "value": t191TanggalSPKMonth},
									{"name": 'sCriteria_t191TanggalSPK_year', "value": t191TanggalSPKYear}
							);
						}

						var t191TglAwal = $('#search_t191TglAwal').val();
						var t191TglAwalDay = $('#search_t191TglAwal_day').val();
						var t191TglAwalMonth = $('#search_t191TglAwal_month').val();
						var t191TglAwalYear = $('#search_t191TglAwal_year').val();
						
						if(t191TglAwal){
							aoData.push(
									{"name": 'sCriteria_t191TglAwal', "value": "date.struct"},
									{"name": 'sCriteria_t191TglAwal_dp', "value": t191TglAwal},
									{"name": 'sCriteria_t191TglAwal_day', "value": t191TglAwalDay},
									{"name": 'sCriteria_t191TglAwal_month', "value": t191TglAwalMonth},
									{"name": 'sCriteria_t191TglAwal_year', "value": t191TglAwalYear}
							);
						}

						var t191TglAkhir = $('#search_t191TglAkhir').val();
						var t191TglAkhirDay = $('#search_t191TglAkhir_day').val();
						var t191TglAkhirMonth = $('#search_t191TglAkhir_month').val();
						var t191TglAkhirYear = $('#search_t191TglAkhir_year').val();
						
						if(t191TglAkhir){
							aoData.push(
									{"name": 'sCriteria_t191TglAkhir', "value": "date.struct"},
									{"name": 'sCriteria_t191TglAkhir_dp', "value": t191TglAkhir},
									{"name": 'sCriteria_t191TglAkhir_day', "value": t191TglAkhirDay},
									{"name": 'sCriteria_t191TglAkhir_month', "value": t191TglAkhirMonth},
									{"name": 'sCriteria_t191TglAkhir_year', "value": t191TglAkhirYear}
							);
						}
	
						var t191JmlSPK = $('#filter_t191JmlSPK input').val();
						if(t191JmlSPK){
							aoData.push(
									{"name": 'sCriteria_t191JmlSPK', "value": t191JmlSPK}
							);
						}
	
						var t191MaxHariPelunasan = $('#filter_t191MaxHariPelunasan input').val();
						if(t191MaxHariPelunasan){
							aoData.push(
									{"name": 'sCriteria_t191MaxHariPelunasan', "value": t191MaxHariPelunasan}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}
	
						var company = $('#filter_company input').val();
						if(company){
							aoData.push(
									{"name": 'sCriteria_company', "value": company}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
                                $('td span.numeric').text(function(index, text) {
                                    return $.number(text,0);
                                    });
							}
						});
		}			
	});

	$('.select-all').click(function(e) {

        $("#SPK_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordsSPKperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsSPKperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#SPK_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsSPKperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anSPKSelected = SPKTable.$('tr.row_selected');
            if(jmlRecSPKPerPage == anSPKSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsSPKperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
