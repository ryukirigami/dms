

<%@ page import="com.kombos.customerprofile.SPK" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'SPK.label', default: 'SPK Form')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteSPK;

$(function(){ 
	deleteSPK=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/SPK/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadSPKTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-SPK" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="SPK"
			class="table table-bordered table-hover">
			<tbody>

            <g:if test="${SPKInstance?.t191IDSPK}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t191IDSPK-label" class="property-label"><g:message
                                code="SPK.t191IDSPK.label" default="ID SPK" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t191IDSPK-label">
                        %{--<ba:editableValue
                                bean="${SPKInstance}" field="t191NoSPK"
                                url="${request.contextPath}/SPK/updatefield" type="text"
                                title="Enter t191NoSPK" onsuccess="reloadSPKTable();" />--}%

                        <g:fieldValue bean="${SPKInstance}" field="t191IDSPK"/>

                    </span></td>

                </tr>
            </g:if>


            <g:if test="${SPKInstance?.company}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="company-label" class="property-label"><g:message
                                code="SPK.company.label" default="Company" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="company-label">
                        %{--<ba:editableValue
                                bean="${SPKInstance}" field="company"
                                url="${request.contextPath}/SPK/updatefield" type="text"
                                title="Enter company" onsuccess="reloadSPKTable();" />--}%

                        ${SPKInstance?.company?.encodeAsHTML()}

                    </span></td>

                </tr>
            </g:if>


            <g:if test="${SPKInstance?.t191NoSPK}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t191NoSPK-label" class="property-label"><g:message
					code="SPK.t191NoSPK.label" default="No SPK" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t191NoSPK-label">
						%{--<ba:editableValue
								bean="${SPKInstance}" field="t191NoSPK"
								url="${request.contextPath}/SPK/updatefield" type="text"
								title="Enter t191NoSPK" onsuccess="reloadSPKTable();" />--}%
							
								<g:fieldValue bean="${SPKInstance}" field="t191NoSPK"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${SPKInstance?.t191TanggalSPK}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t191TanggalSPK-label" class="property-label"><g:message
					code="SPK.t191TanggalSPK.label" default="Tanggal SPK" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t191TanggalSPK-label">
						%{--<ba:editableValue
								bean="${SPKInstance}" field="t191TanggalSPK"
								url="${request.contextPath}/SPK/updatefield" type="text"
								title="Enter t191TanggalSPK" onsuccess="reloadSPKTable();" />--}%
							
								<g:formatDate date="${SPKInstance?.t191TanggalSPK}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${SPKInstance?.t191TglAwal}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t191TglAwal-label" class="property-label"><g:message
					code="SPK.masaBerlakuSPK.label" default="Tanggal Awal Masa berlaku SPK" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t191TglAwal-label">
						%{--<ba:editableValue
								bean="${SPKInstance}" field="t191TglAwal"
								url="${request.contextPath}/SPK/updatefield" type="text"
								title="Enter t191TglAwal" onsuccess="reloadSPKTable();" />--}%
							
								<g:formatDate date="${SPKInstance?.t191TglAwal}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${SPKInstance?.t191TglAkhir}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t191TglAkhir-label" class="property-label"><g:message
					code="SPK.t191TglAkhir.label" default="Tanggal Akhir Masa berlaku SPK" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t191TglAkhir-label">
						%{--<ba:editableValue
								bean="${SPKInstance}" field="t191TglAkhir"
								url="${request.contextPath}/SPK/updatefield" type="text"
								title="Enter t191TglAkhir" onsuccess="reloadSPKTable();" />--}%
							
								<g:formatDate date="${SPKInstance?.t191TglAkhir}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${SPKInstance?.t191JmlSPK}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t191JmlSPK-label" class="property-label"><g:message
					code="SPK.t191JmlSPK.label" default="Jumlah Total" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t191JmlSPK-label">
						%{--<ba:editableValue
								bean="${SPKInstance}" field="t191JmlSPK"
								url="${request.contextPath}/SPK/updatefield" type="text"
								title="Enter t191JmlSPK" onsuccess="reloadSPKTable();" />--}%
							
								Rp&nbsp;<g:fieldValue bean="${SPKInstance}" field="t191JmlSPK"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${SPKInstance?.t191MaxHariPelunasan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t191MaxHariPelunasan-label" class="property-label"><g:message
					code="SPK.t191MaxHariPelunasan.label" default="Maksimal Lama Pelunasan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t191MaxHariPelunasan-label">
						%{--<ba:editableValue
								bean="${SPKInstance}" field="t191MaxHariPelunasan"
								url="${request.contextPath}/SPK/updatefield" type="text"
								title="Enter t191MaxHariPelunasan" onsuccess="reloadSPKTable();" />--}%
							
								<g:fieldValue bean="${SPKInstance}" field="t191MaxHariPelunasan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${SPKInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="SPK.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${SPKInstance}" field="createdBy"
								url="${request.contextPath}/SPK/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadSPKTable();" />--}%
							
								<g:fieldValue bean="${SPKInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${SPKInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="SPK.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${SPKInstance}" field="updatedBy"
								url="${request.contextPath}/SPK/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadSPKTable();" />--}%
							
								<g:fieldValue bean="${SPKInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${SPKInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="SPK.lastUpdProcess.label" default="Last Update Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${SPKInstance}" field="lastUpdProcess"
								url="${request.contextPath}/SPK/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadSPKTable();" />--}%
							
								<g:fieldValue bean="${SPKInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			

				<g:if test="${SPKInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="SPK.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${SPKInstance}" field="dateCreated"
								url="${request.contextPath}/SPK/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadSPKTable();" />--}%
							
								<g:formatDate type="datetime" style="MEDIUM" date="${SPKInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${SPKInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="SPK.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${SPKInstance}" field="lastUpdated"
								url="${request.contextPath}/SPK/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadSPKTable();" />--}%
							
								<g:formatDate date="${SPKInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${SPKInstance?.id}"
					update="[success:'SPK-form',failure:'SPK-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteSPK('${SPKInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
