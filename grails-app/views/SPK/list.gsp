
<%@ page import="com.kombos.customerprofile.SPK" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'SPK.label', default: 'SPK Form')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>

        <g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	var loadSPKDetail;
	var massDeleteSPK;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

	$('#popup').click(function(){
	    $(".highslide-maincontent").load('${request.contextPath}/SPKDetail');

	});
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDeleteSPK();
         				}
         			});

         			break;

         	case '_SPKDetail_' :
                loadSPKDetailModal(); //via Modal
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/SPK/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/SPK/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };



    loadForm = function(data, textStatus){
		$('#SPK-form').empty();
    	$('#SPK-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#SPK-table").hasClass("span12")){
   			$("#SPK-table").toggleClass("span12 span5");
        }
        $("#SPK-form").css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
   		if($("#SPK-table").hasClass("span5")){
   			$("#SPK-table").toggleClass("span5 span12");
   		}
        $("#SPK-form").css("display","none");
   	}
   	
   	massDeleteSPK = function() {
   		var recordsToDelete = [];
		$("#SPK-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/SPK/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadSPKTable();
    		}
		});
		
   	}



});
	$("#spkDetailModal").on("show", function() { 
		$("#spkDetailModal .btn").on("click", function(e) {
			$("#spkDetailModal").modal('hide'); 
		});
	}); 
	$("#spkDetailModal").on("hide", function() { 
		$("#spkDetailModal a.btn").off("click");
	});
    loadSPKDetailModal = function(){
		$("#spkDetailContent").empty();       
		$.ajax({type:'POST', url:'${request.contextPath}/SPKDetail',
   			success:function(data,textStatus){
					$("#spkDetailContent").html(data);
				    $("#spkDetailModal").modal({ 
						"backdrop" : "static",
						"keyboard" : true,
						"show" : true 
					}).css({'width': '1200px','margin-left': function () {return -($(this).width() / 2);}});
					
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});

     

    }

   loadSPKDetail = function(){

           $("<div id='dialogku'>").dialog({
                        modal: true,
                        open: function () {
                            $(this).load('${request.contextPath}/SPKDetail');
                        }, close: function(event, ui) {
                            $(this).remove();
                        },

                        position : 'bottom',
                        height: 600,
                        width: 1200,
                       title: 'SPK Detail'
                    });

    }
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>

		<ul class="nav pull-right">
            <li>
                <a class="pull-right box-action" href="#"
                   style="display: block;" target="_SPKDetail_">&nbsp;&nbsp;<i>SPK Detail</i>&nbsp;&nbsp;
            </a></li>
            <li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>

		</ul>
	</div>
	<div class="box">
		<div class="span12" id="SPK-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
		</div>
		<div class="span7" id="SPK-form" style="display: none;"></div>
	</div>
	<div id="spkDetailModal" class="modal fade">
	<div class="modal-dialog" style="width: 1200px;">
		<div class="modal-content" style="width: 1200px;">
			<!-- dialog body -->
			<div class="modal-body" style="max-height: 1200px;">
				<div id="spkDetailContent"/>
				<div class="iu-content"></div>
			</div>
			<!-- dialog buttons -->
			<div class="modal-footer"><button id='closeSpkDetail' type="button" class="btn btn-primary">Close</button></div>
		</div>
	</div>
	</div>
</div>
</body>
</html>
