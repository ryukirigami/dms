<%@ page import="com.kombos.baseapp.AppSettingParam; com.kombos.baseapp.AppSettingParamService" %>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<html>
<!--<![endif]-->

<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'userLoginLogs.label', default: 'Logon Reports')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="export"></r:require>
</head>
<body>

<h3 class="navbar box-header no-border">
    ${entityName} List
    <ul class="nav pull-right">
        
    </ul>
</h3>
<div class="box">
    <div class="span12" id="instance-table">

        <g:render template="../menu/maxLineDisplay"/>
        <script type="text/javascript">
            var shrinkInstanceTable;
            var reloadInstanceTable;
            var expandInstanceTable;
            jQuery(function () {

                var oTable = $('#instanceDatatables_datatable').dataTable({
                	"sScrollX": "100%",            				
    				"bScrollCollapse": true,
					"bAutoWidth" : false,
                    "bPaginate" : true,
                    "sInfo" : "",
                    "sInfoEmpty" : "",

                    "sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
                    bFilter: false,
                    "bStateSave": false,
                    'sPaginationType': 'bootstrap',
                    "fnInitComplete": function () {
                    },
                    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    },
                    "fnServerParams": function ( aoData ) {
                		aoData.push( {"name": "sFilter_1", "value": $('#operator_username li.active').attr('name')} );
                    	aoData.push( {"name": "sFilter_2", "value": $('#operator_loginDatetime li.active').attr('name')} );
                    	aoData.push( {"name": "sFilter_3", "value": $('#operator_loginStatusLookupCode li.active').attr('name')} );
                	},
                    "bSort": true,
                    "bProcessing": true,
                    "bServerSide": true,
                    "iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
                    "sAjaxSource": "${request.contextPath}/userLoginLogs/getList",
                    "aoColumnDefs": [
                        {   "sName": "pkid",
                            "aTargets": [0],
                            "bSearchable": true,
                            "bSortable": true,
                            "bVisible": false
                        }  ,
                        {   "sName": "username",
                            "aTargets": [1],
                            "bSearchable": true,
                            "bSortable": true,
                            "bVisible": true
                        }  ,
                        {   "sName": "loginDatetime",
                            "aTargets": [2],
                            "bSearchable": true,
                            "bSortable": true,
                            "bVisible": true
                        }  ,
                        {   "sName": "loginStatusLookupCode",
                            "aTargets": [3],
                            "bSearchable": true,
                            "bSortable": true,
                            "bVisible": true
                        }
                    ]
                    ,"fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        var username = jQuery('#username').val();
                        var loginStatusLookupCode = jQuery('#loginStatusLookupCode').val();
                        var from_date = jQuery('#from_date').val();
                        var to_date = jQuery('#to_date').val();

                        aoData.push(
                                {"name": "username", "value": username},
                                {"name": "loginStatusLookupCode", "value": loginStatusLookupCode},
                                {"name": "from_date", "value": from_date},
                                {"name": "to_date", "value": to_date}
                        );

                        $.ajax({ "dataType": 'json',
                            "type": "POST",
                            "url": sSource,
                            "data": aoData ,
                            "success": function (json) {
                                fnCallback(json);
                                <g:if test="${summaryColumns && summaryColumns.size() > 0}">
                                $.each(json.summary, function(key, val) {
                                    $("#sum_" + key).html(val);
                                });
                                </g:if>
                            }
                        });
                    }
                });

                $("#instanceDatatables-filters tbody")
                        .append("<tr><td align=\"center\">role.id.label&nbsp;&nbsp;</td><td align=\"center\" id=\"filter_id\"> <input type=\"text\" name=\"search_id\" class=\"search_init\" size=\"10\"/></td></tr>")
                        .append("<tr><td align=\"center\">Name&nbsp;&nbsp;</td><td align=\"center\" id=\"filter_name\"> <input type=\"text\" name=\"search_name\" class=\"search_init\" size=\"10\"/></td></tr>")
                        .append("<tr><td align=\"center\">Label&nbsp;&nbsp;</td><td align=\"center\" id=\"filter_permissions\"> <input type=\"text\" name=\"search_permissions\" class=\"search_init\" size=\"10\"/></td></tr>")
                        .append("<tr><td align=\"center\">Users&nbsp;&nbsp;</td><td align=\"center\" id=\"filter_users\"> <input type=\"text\" name=\"search_users\" class=\"search_init\" size=\"10\"/></td></tr>")
                ;

                $("#filter_id input").keyup(function () {
                    /* Filter on the column (the index) of this element */
                    oTable.fnFilter(this.value, 0);

                });

                $("#filter_menuCode input").keypress(function (e) {
                    /* Filter on the column (the index) of this element */
                     var code = (e.keyCode ? e.keyCode : e.which);
		            if (code == 13) { //Enter keycode
		                oTable.fnFilter(this.value, 1);
		                e.stopPropagation();
		            }

                });

                $("#filter_loginDatetime input").keyup(function (e) {
                    /* Filter on the column (the index) of this element */
                    var code = (e.keyCode ? e.keyCode : e.which);
		            if (code == 13) { //Enter keycode
		                oTable.fnFilter(this.value, 2);
		                e.stopPropagation();
		            }
                });

                $("#filter_actionType input").keyup(function (e) {
                    /* Filter on the column (the index) of this element */
                    var code = (e.keyCode ? e.keyCode : e.which);
		            if (code == 13) { //Enter keycode
		                oTable.fnFilter(this.value, 3);
		                e.stopPropagation();
		            }

                });

                $("#filter_menuCode input").click(function (e) {				    	
			    	 e.stopPropagation();  
			    }); 
                $("#filter_loginDatetime input").click(function (e) {				    	
			    	 e.stopPropagation();  
			    }); 
                $("#filter_actionType input").click(function (e) {				    	
			    	 e.stopPropagation();  
			    }); 

                shrinkInstanceTable = function(){
                    var oTable = $('#instanceDatatables_datatable').dataTable();

                }

                expandInstanceTable = function(){
                    var oTable = $('#instanceDatatables_datatable').dataTable();

                }

                reloadInstanceTable = function(){
                    var oTable = $('#instanceDatatables_datatable').dataTable();
                    oTable.fnReloadAjax();
                }

                $('#instanceDatatables_datatable th .select-all').click(function(e) {
                    var tc = $('#instanceDatatables_datatable tbody .row-select').attr('checked', this.checked);

                    if(this.checked)
                        tc.parent().parent().addClass('row_selected');
                    else
                        tc.parent().parent().removeClass('row_selected');
                    e.stopPropagation();
                });
                $('#instanceDatatables_datatable tbody tr .row-select').live('click', function (e) {
                    if(this.checked)
                        $(this).parent().parent().addClass('row_selected');
                    else
                        $(this).parent().parent().removeClass('row_selected');
                    e.stopPropagation();

                } );

                $('#instanceDatatables_datatable tbody tr a').live('click', function (e) {
                    e.stopPropagation();
                } );

                $('#instanceDatatables_datatable tbody td').live('click', function(e) {
                    if (e.target.tagName.toUpperCase() != "INPUT") {
                        var $tc = $(this).parent().find('input:checkbox'),
                                tv = $tc.attr('checked');
                        $tc.attr('checked', !tv);
                        $(this).parent().toggleClass('row_selected');
                    }
                });

            });

            $("#search_button").bind('click', function (e) {
                var from_date = jQuery('#from_date').val();
                var to_date = jQuery('#to_date').val();

                if(!from_date){
                    return alert('Please Enter From Date');
                }

                if(!to_date){
                    return alert('Please Enter To Date');
                }

                var oTable = $('#instanceDatatables_datatable').dataTable();
                oTable.fnDraw();
            });

            $(".operator li a").click(function (e) {
                $(this).parent().addClass('active').siblings().removeClass('active');
            });
        </script>

    <div class="form-horizontal">
        <div class="span12">
            <div class="row-fluid">
                <div class="span4">
                    <label>User</label>
                    <g:select class="span12" name="username" id="username" from="${com.kombos.baseapp.sec.shiro.User.list()}" optionKey="username" size="1" default="none" noSelection="['': '']"/>
                </div>
                <div class="span4">
                    <label><g:message code="app.userLoginLogs.loginStatusLookupCode.label" default="Action&nbsp;Type"/></label>
                    <g:select class="span12" id="loginStatusLookupCode" name="loginStatusLookupCode" from="${com.kombos.baseapp.sec.shiro.UserLoginLogs.STATUS}" default="none" noSelection="['': '']"></g:select>
                </div>
            </div>
            <div class="row-fluid">
                &nbsp;
            </div>
            <div class="row-fluid">
                <div class="span4">
                    <label>From Date</label>
                    <input type="text" class="span12" id="from_date" name="from_date" data-date-format="dd/mm/yyyy">
                    <g:javascript>
                        $('#from_date').datepicker();
                    </g:javascript>
                </div>
                <div class="span4">
                    <label>To Date</label>
                    <input type="text" class="span12" id="to_date" name="to_date" data-date-format="dd/mm/yyyy">
                    <g:javascript>
                        $('#to_date').datepicker();
                    </g:javascript>
                </div>
            </div>
            <div class="row-fluid">
                &nbsp;
            </div>
            <div class="row-fluid">
                <div class="span4">
                    <a href="#search" id="search_button" role="button" class="btn" data-toggle="modal">Search</a>
                </div>
            </div>
            <div class="row-fluid">
                &nbsp;
            </div>
        </div>
    </div>
        <export:formats formats="['excel','csv']" action="export"></export:formats>
        <table id="instanceDatatables_datatable" cellpadding="0" cellspacing="0"
               border="0"
               class="display table table-striped table-bordered table-hover" width="100%">
            <thead>
            <tr>
                <th>
                    <g:message code="app.userLoginLogs.pkid.label" default="pkid"/>
                </th>
                <th>
                    <g:message code="app.userLoginLogs.username.label" default="User&nbsp;Name"/>
                    <div id="filter_menuCode" style="margin-top: 38px;">
                        <input type="text" name="search_menuCode" class="search_init" />
                        <span class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#"> <i class="icon-filter"></i>	</a>
                            <ul id="operator_username" class="dropdown-menu operator" role="menu"
                                aria-labelledby="dLabel">
                                <li name="beginwith"><a href="#">Begins with</a></li>
                                <li name="contains"><a href="#">Contains</a></li>
                                <li name="endswith"><a href="#">Ends with</a></li>
                                <li name="equals" class="active"><a href="#">Equals</a></li>
                                <li name="doesntequal"><a href="#">Doesn't equal</a></li>
                                <li name="islessthan"><a href="#">Is less than</a></li>
                                <li name="islessthanorequalto"><a href="#">Is less than or equal to</a></li>
                                <li name="isgreaterthan"><a href="#">Is greater than</a></li>
                                <li name="isgreaterthanorequalto"><a href="#">Is greater than or equal to</a></li>
                            </ul>
                        </span>
                    </div>
                </th>
                <th>
                    <g:message code="app.userLoginLogs.loginDatetime.label" default="Timestamp"/>
                    <div id="filter_loginDatetime" style="margin-top: 38px;">
                        <input type="text" name="search_userName" class="search_init" />
                        <span class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#"> <i class="icon-filter"></i>	</a>
                            <ul id="operator_loginDatetime" class="dropdown-menu operator" role="menu"
                                aria-labelledby="dLabel">
                                <li name="beginwith"><a href="#">Begins with</a></li>
                                <li name="contains"><a href="#">Contains</a></li>
                                <li name="endswith"><a href="#">Ends with</a></li>
                                <li name="equals" class="active"><a href="#">Equals</a></li>
                                <li name="doesntequal"><a href="#">Doesn't equal</a></li>
                                <li name="islessthan"><a href="#">Is less than</a></li>
                                <li name="islessthanorequalto"><a href="#">Is less than or equal to</a></li>
                                <li name="isgreaterthan"><a href="#">Is greater than</a></li>
                                <li name="isgreaterthanorequalto"><a href="#">Is greater than or equal to</a></li>
                            </ul>
                        </span>
                    </div>
                </th>
                <th>
                    <g:message code="app.userLoginLogs.loginStatusLookupCode.label" default="Action&nbsp;Type"/>
                    <div id="filter_actionType" style="margin-top: 38px;">
                        <input type="text" name="search_actionType" class="search_init" />
                        <span class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#"> <i class="icon-filter"></i>	</a>
                            <ul id="operator_loginStatusLookupCode" class="dropdown-menu operator" role="menu"
                                aria-labelledby="dLabel">
                                <li name="beginwith"><a href="#">Begins with</a></li>
                                <li name="contains"><a href="#">Contains</a></li>
                                <li name="endswith"><a href="#">Ends with</a></li>
                                <li name="equals" class="active"><a href="#">Equals</a></li>
                                <li name="doesntequal"><a href="#">Doesn't equal</a></li>
                                <li name="islessthan"><a href="#">Is less than</a></li>
                                <li name="islessthanorequalto"><a href="#">Is less than or equal to</a></li>
                                <li name="isgreaterthan"><a href="#">Is greater than</a></li>
                                <li name="isgreaterthanorequalto"><a href="#">Is greater than or equal to</a></li>
                            </ul>
                        </span>
                    </div>
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td colspan="4" class="dataTables_empty">Loading data from server</td>
            </tr>
            </tbody>
        </table>



    </div>
    <div class="span7" id="role-form" style="display: none;"></div>
</div>

<script type="text/javascript">
    var show;
    var loadForm;
    var shrinkTableLayout;
    var expandTableLayout;
    $(function(){

        $('.box-action').click(function(){
            switch($(this).attr('target')){
                case '_CREATE_' :
                    shrinkTableLayout();
                    jQuery('#spinner').fadeIn(1);jQuery.ajax({type:'POST', url:'${request.contextPath}/role/create',success:function(data,textStatus){loadForm(data, textStatus);;},error:function(XMLHttpRequest,textStatus,errorThrown){},complete:function(XMLHttpRequest,textStatus){jQuery('#spinner').fadeOut();}});
                    break;
                case '_DELETE_' :
                    bootbox.confirm('Are you sure?',
                            function(result){
                                if(result){
                                    massDelete();
                                }
                            });

                    break;
            }
            return false;
        });

        show = function(id) {
            shrinkTableLayout();
            $('#spinner').fadeIn(1);
            $.ajax({type:'POST', url:'${request.contextPath}/role/show/'+id,
                success:function(data,textStatus){
                    loadForm(data, textStatus);
                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
        };

        edit = function(id) {
            shrinkTableLayout();
            $('#spinner').fadeIn(1);
            $.ajax({type:'POST', url:'${request.contextPath}/role/edit/'+id,
                success:function(data,textStatus){
                    loadForm(data, textStatus);
                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
        };

        loadForm = function(data, textStatus){
            $('#role-form').empty();
            $('#role-form').append(data);
        }

        shrinkTableLayout = function(){
            if($("#instance-table").hasClass('span12')){
                $("#instance-table").toggleClass('span12 span5');
            }
            $("#role-form").css("display","block");
            shrinkInstanceTable();
        }

        expandTableLayout = function(){
            if($("#instance-table").hasClass('span5')){
                $("#instance-table").toggleClass('span5 span12');
            }
            $("#role-form").css("display","none");
            expandInstanceTable();
        }

        massDelete = function() {
            var recordsToDelete = [];
            $("#instance-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    recordsToDelete.push(id);
                }
            });

            var json = JSON.stringify(recordsToDelete);

            $.ajax({
                url:'${request.contextPath}/role/massdelete',
                type: "POST", // Always use POST when deleting data
                data: { ids: json },
                complete: function(xhr, status) {
                    reloadInstanceTable();
                }
            });

        }

    });
</script>
</body>
</html>