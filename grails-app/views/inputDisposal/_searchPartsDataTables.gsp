<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="searchParts_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover fixed" style="table-layout: fixed; width: 750px">
    <col width="300px" />
    <col width="350px" />
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:300px;">
            <div>
                &nbsp; &nbsp; <input type="checkbox" class="pull-left select-all" aria-label="Select all" title="Select all"/>
                <g:message code="ClaimAdd.goods.label" default="Kode Parts" /></div>
        </th>

        <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:350px;">
            <div><g:message code="ClaimAdd.goods.label" default="Nama Parts" /></div>
        </th>

    </tr>

    <tr>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_kode" style="padding-top: 0px;position:relative; margin-top: 0px;width: 300px;">
                <input type="text" name="search_kode" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_nama" style="padding-top: 0px;position:relative; margin-top: 0px;width: 350px;">
                <input type="text" name="search_nama" class="search_init" />
            </div>
        </th>

    </tr>

    </thead>
</table>

<g:javascript>
var searchPartsTable;
var reloadsearchPartsTable;
$(function(){
    var recordsClaimPerPage = [];
    var anClaimSelected;
    var jmlRecClaimPerPage=0;
    var id;
    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	searchPartsTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	searchPartsTable = $('#searchParts_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : true,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsClaim = $("#searchParts_datatables tbody .row-select");
            var jmlClaimCek = 0;
            var nRow;
            var idRec;
            rsClaim.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsClaimPerPage[idRec]=="1"){
                    jmlClaimCek = jmlClaimCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsClaimPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecClaimPerPage = rsClaim.length;
            if(jmlClaimCek==jmlRecClaimPerPage && jmlRecClaimPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "searchPartsDatatablesList")}",
		"aoColumns": [

{
	"sName": "kode",
	"mDataProp": "kode",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input id="checkBoxVendor" type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSortable": false,
	"bSearchable": true,
	"sWidth":"300px",
	"bVisible": true
},
{
	"sName": "nama",
	"mDataProp": "nama",
	"aTargets": [1],
	"bSortable": false,
	"bSearchable": true,
	"sWidth":"350px",
	"bVisible":  true
}],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var kode = $('#filter_kode input').val();

						if(kode){
							aoData.push(
									{"name": 'sCriteria_kode', "value": kode}
							);
						}

						var nama = $('#filter_nama input').val();
						if(nama){
							aoData.push(
									{"name": 'sCriteria_nama', "value": nama}
							);
						}

                        var exist =[];
						var rows = $("#parts_datatables").dataTable().fnGetNodes();
                        for(var i=0;i<rows.length;i++)
                        {
							var id = $(rows[i]).find("#idPopUp").val();
							exist.push(id);
						}
						if(exist.length > 0){
							aoData.push(
										{"name": 'sCriteria_exist', "value": JSON.stringify(exist)}
							);
						}
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
    $('.select-all').click(function(e) {

        $("#searchParts_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsClaimPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsClaimPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#searchParts_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsClaimPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anClaimSelected = searchPartsTable.$('tr.row_selected');

            if(jmlRecClaimPerPage == anClaimSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsClaimPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>



