<%--
  Created by IntelliJ IDEA.
  User: Mohammad Fauzi
  Date: 2/6/14
  Time: 4:50 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title>Search Part</title>
    <r:require modules="baseapplayout"/>
    <g:render template="../menu/maxLineDisplay"/>
</head>

<body>

<div class="content scaffold-edit" role="main">
    <fieldset class="form">
        <div class="box">
            %{--<legend style="font-size: small">Search Parts</legend>--}%
            <g:render template="searchPartsDataTables"/>
        </div>
    </fieldset>
</div>

</body>
</html>