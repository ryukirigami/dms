<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="parts_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="100%">
    <thead>
    <tr>
        <th style="border-bottom: none;padding: 5px;">
            <div>Kode Parts</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Nama Parts</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Qty</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Satuan</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>ICC</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Lokasi/Rak</div>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
var partsTable;
var reloadpartsTable;
$(function(){

    reloadpartsTable = function() {
		partsTable.fnDraw();
	}

var recordsClaimPerPage = [];
    var anClaimSelected;
    var jmlRecClaimPerPage=0;
    var id;

	partsTable = $('#parts_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsClaim = $("#parts_datatables tbody .row-select");
            var jmlClaimCek = 0;
            var nRow;
            var idRec;
            rsClaim.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsClaimPerPage[idRec]=="1"){
                    jmlClaimCek = jmlClaimCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsClaimPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecClaimPerPage = rsClaim.length;
            if(jmlClaimCek==jmlRecClaimPerPage && jmlRecClaimPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
//		 "bProcessing": true,
//		 "bServerSide": true,
//		 "sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
//	 "sAjaxSource": "${g.createLink(action: "datablesList")}",
		"aoColumns": [
{
	"sName": "kode",
	"mDataProp": "kode",
	"aTargets": [0],
	"bSortable": false,
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" id="idPopUp" value="'+row['id']+'">&nbsp;&nbsp;'+data ;
	},
	"sWidth":"160px",
	"bVisible": true
},
{
	"sName": "nama",
	"mDataProp": "nama",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"140px",
	"bVisible": true
},
{
	"sName": "qty",
	"mDataProp": "qty",
	"aTargets": [2],
	"mRender": function ( data, type, row ) {
	    return '<input id="qty_'+row['id']+'"  class="pull-left row-qty" size=5 value="'+data+'" type="text">';
	},
	"bSortable": false,
	"sWidth":"80px",
	"bVisible": true
},
{
	"sName": "satuan",
	"mDataProp": "satuan",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"70px",
	"bVisible": true
},
{
	"sName": "icc",
	"mDataProp": "icc",
	"aTargets": [4],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "lokasi",
	"mDataProp": "lokasi",
	"aTargets": [5],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
    $('.select-all').click(function(e) {

        $("#claimInput_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsClaimPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsClaimPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#parts_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsClaimPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anClaimSelected = partsTable.$('tr.row_selected');

            if(jmlRecClaimPerPage == anClaimSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsClaimPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
    var cekStatus = "${aksi}"
    if(cekStatus=="ubah"){
        var id = "${idUbah}";
        $.ajax({
        url:'${request.contextPath}/inputDisposal/getTableData',
    		type: "POST", // Always use POST when deleting data
    		data: { id: id},
    		success : function(data){
    		    $.each(data,function(i,item){
                    partsTable.fnAddData({
                        'id': item.id,
						'kode': item.kode,
						'nama': item.nama,
						'qty': item.qty,
						'satuan': item.satuan,
						'icc': item.icc,
						'lokasi': item.lokasi

                    });
                });
            }
		});
    }
});
</g:javascript>



