<%--
  Created by IntelliJ IDEA.
  User: Mohammad Fauzi
  Date: 2/5/14
  Time: 12:25 AM
--%>

<%@ page import="com.kombos.administrasi.KegiatanApproval" contentType="text/html;charset=UTF-8" %>
<html>
<head>
<meta name="layout" content="main">
<title>Input Disposal</title>
<r:require modules="baseapplayout"/>


<g:javascript disposition="head">

        $(function () {



            onDoSendToApproval = function() {
                $("#permohonanApprovalModal").modal({
                            "backdrop": "static",
                            "keyboard": true,
                            "show": true
                        }).css({'width': '1200px', 'margin-left': function () {
                                    return -($(this).width() / 2);
                                }});
            }


            onSearchAndAddParts = function() {
                $("#searchAndAddPartsContent").empty();
                $.ajax({type: 'POST', url: '${request.contextPath}/inputDisposal/searchAndAddParts',
                    success: function (data, textStatus) {
                        $("#searchAndAddPartsContent").html(data);
                        $("#searchAndAddPartsModal").modal({
                            "backdrop": "static",
                            "keyboard": true,
                            "show": true
                        }).css({'width': '900px', 'margin-left': function () {
                                    return -($(this).width() / 2);
                                }});

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                    },
                    complete: function (XMLHttpRequest, textStatus) {
                        $('#spinner').fadeOut();
                    }
                });
            }


            tambahParts = function(){
                    var checkGoods =[];
                    $("#searchParts_datatables tbody .row-select").each(function() {
                        if(this.checked){
                            var id = $(this).next("input:hidden").val();
                            var nRow = $(this).parents('tr')[0];

                            checkGoods.push(nRow);
                        }
                    });
                    if(checkGoods.length<1){
                        alert('Anda belum memilih data yang akan ditambahkan');

                    }else {
                        for (var i=0;i < checkGoods.length;i++){

                            var aData = searchPartsTable.fnGetData(checkGoods[i]);

                            partsTable.fnAddData({
                                'id': aData['id'],

                                'kode': aData['kode'],
                                'nama': aData['nama'],
                                'satuan': aData['satuan'],
                                'qty': aData['qty'],
                                'icc': aData['icc'],
                                'lokasi': aData['lokasi']

                                });

                            $('#qty_'+aData['id']).autoNumeric('init',{
                                vMin:'0',
                                vMax:'999999999999999999',
                                mDec: null,
                                aSep:''
                            });
                        }

                    }
                    onSearchAndAddParts();
             }

             onDeleteParts = function(){
                   var checkPart =[];
                    $("#parts_datatables tbody .row-select").each(function() {
                        if(this.checked){
                            var id = $(this).next("input:hidden").val();
                            checkPart.push(id);
                        }
                    });
                    if(checkPart.length == 0){
                        alert('Anda belum memilih data yang akan dihapus');
                        return;
                    }
                    var checkPartMap = JSON.stringify(checkPart);
                    var fa = document.getElementById("fa").value;
                    $.ajax({
                        url:'${request.contextPath}/inputDisposal/deleteParts',
                        type: "POST", // Always use POST when deleting data
                        data : { ids: checkPartMap },
                        success : function(data){
                            reloadpartsTable();
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            alert('Internal Server Error');
                        }
                    });

                checkPart = [];

             }



             onSaveData = function(){

                try{
                    var checkJob =[];
                    var paramQty = "";

                        $("#parts_datatables tbody .row-select").each(function() {
                            if(this.checked){
                                var id = $(this).next("input:hidden").val();
                                checkJob.push(id);
                                paramQty += "&qty_"+id+"="+document.getElementById("qty_"+id).value
                            }
                        });
                        if(checkJob.length == 0){
                            alert('Anda belum memilih data yang akan disimpan');
                            return;
                        }
                        var checkBoxVendor = JSON.stringify(checkJob);
                        $.ajax({
                            url:'${request.contextPath}/inputDisposal/doSave?1=1'+paramQty,
                            type: "POST", // Always use POST when deleting data
                            data : { ids: checkBoxVendor, t147TglDisp: document.getElementById("t147TglDisp").value },
                            success : function(data){
                                reloadpartsTable()
                                alert('Save success');

                            },
                            error: function(xhr, textStatus, errorThrown) {
                                alert('Internal Server Error');
                            }
                        });

                    checkJob = [];
                }catch (e){
                    alert(e)
                }


             }



             onSendData = function(){

                try{
                    var checkJob =[];
                    var paramQty = "";
                    var Pesan = document.getElementById("Pesan").value;
                    var dokumen = document.getElementById("dokumen").value;
                    if(Pesan == ''){
                        alert('Pesan harus diisi');
                        return;
                    }
                    Pesan = encodeURIComponent(Pesan)

                        $("#parts_datatables tbody .row-select").each(function() {
                            if(this.checked){
                                var id = $(this).next("input:hidden").val();
                                checkJob.push(id);
                                paramQty += "&qty_"+id+"="+document.getElementById("qty_"+id).value
                            }
                        });
                        if(checkJob.length == 0){
                            alert('Anda belum memilih data yang akan disimpan');
                            return;
                        }

                        var checkBoxVendor = JSON.stringify(checkJob);
                        $.ajax({
                            url:'${request.contextPath}/inputDisposal/doSend?1=1'+paramQty+'&Pesan='+Pesan+'&dokumen='+dokumen,
                            type: "POST", // Always use POST when deleting data
                            data : { ids: checkBoxVendor, t147TglDisp: document.getElementById("t147TglDisp").value },
                            success : function(data){
                                reloadpartsTable();
                                alert('Send to approval success');
                                window.location.replace('#/partsDisposal');
                            },
                            error: function(xhr, textStatus, errorThrown) {
                                alert('Internal Server Error');
                            }
                        });

                    checkJob = [];
                }catch (e){
                    alert(e)
                }


             }


             onSelectAll = function(){
                $("#parts_datatables tbody .row-select").each(function() {
                            this.checked = true
                        });
            }

            onUnSelectAll = function(){
                $("#parts_datatables tbody .row-select").each(function() {
                            this.checked = false
                        });
            }





        });
</g:javascript>

</head>

<body>

<fieldset class="form">

    <div class="box">
        <legend style="font-size: small">Disposal</legend>

        <table>
            <tr style="display: none">
                <td>Nomor Disposal:</td>
                <td><input type="text" readonly value="${partsDisposalInstance?.t147ID}"></td>
            </tr>
            <tr>
                <td>Tanggal Disposal:</td>
                <td>
                    <ba:datePicker name="t147TglDisp" id="t147TglDisp" format="dd-MM-yyyy" value="${partsDisposalInstance?.t147TglDisp ? partsDisposalInstance?.t147TglDisp : new java.util.Date()}"/>
                </td>
            </tr>
        </table>
    </div>


    <div class="box">
        <legend style="font-size: small">Disposal List</legend>
        <fieldset class="buttons controls">
            <g:if test="${aksi!='ubah'}">
                <button class="btn btn-primary" onclick="onSearchAndAddParts();">Add Parts...</button>
            </g:if>

        </fieldset>
<br/>
        <div id="partsDataTables_tabs-1">
            <g:render template="partsDataTables"/>
        </div>
    </div>

</fieldset>


<fieldset class="buttons controls">

    <g:if test="${!params?.action?.toString()?.equalsIgnoreCase("showApprove")}">
        <button class="btn btn-primary" onclick="onDoSendToApproval()">Send To Approval</button>
    </g:if>

    <g:if test="${params?.action?.toString()?.equalsIgnoreCase("showApprove")}">
        <button class="btn btn-primary" onclick="onApproveData()">Approve</button>
        <button class="btn btn-primary" onclick="onUnApproveData()">UnApprove</button>
    </g:if>

    <button class="btn btn-primary" onclick="window.location.replace('#/partsDisposal');">Cancel</button>

</fieldset>


<div id="searchAndAddPartsModal" class="modal hide">
    <div class="modal-dialog" style="width: 850px;">
        <div class="modal-content" style="width: 850px;">
            <!-- dialog body -->
            <div class="modal-body" style="max-height: 850px;">
                <div id="searchAndAddPartsContent"/>
                <div class="iu-content"></div>
            <button class="btn btn-primary" data-dismiss="modal" onclick="tambahParts()">Add Selected</button>
            <button id='closesearchAndAddParts' type="button" data-dismiss="modal"
                    class="btn btn-primary">Close</button>
            </div>

        </div>
    </div>
</div>


<div id="permohonanApprovalModal" class="modal hide">
    <div class="modal-dialog" style="width: 1200px;">
        <div class="modal-content" style="width: 1200px;">
            <!-- dialog body -->
            <div class="modal-body">
                <div class="iu-content">

                        Nama Kegiatan  :

                        <g:select name="kegiatanApproval" id="kegiatanApproval" from="${KegiatanApproval.list()}" optionKey="id"  value="${kegiatanApproval.id}" optionValue="m770KegiatanApproval" class="one-to-many" readonly="readonly"/>

                    <br>

                        Nomor Dokumen :
                        <g:textField name="dokumen" id="dokumen" value="${noDokumen}" />
                    <br>
                        Tanggal Dan Jam :

                        ${ new Date().format("dd MM yyyy")}

                    <br>
                    Approve(s)
                    <br>
                    <br>
                          ${approver}
                    <br>
                    <br>
                    Pesan :
                    <br>
                    <g:textArea name="Pesan" id="Pesan" style="width: 99%" />

                </div>
            </div>

            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" onclick="onSendData()">Send</button>
                <button id='closepermohonanApproval' type="button" data-dismiss="modal"
                        class="btn btn-primary">Close</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>