<%--
  Created by IntelliJ IDEA.
  User: Ahmad Fawaz
  Date: 25/02/15
  Time: 13:56
--%>
<%@ page import="com.kombos.finance.MonthlyBalance" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="jurnalAkhirTahun.label" default="Jurnal Akhir Tahun" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
        var accNumberCari = "-1-1";

        function doSearch(){
            var peTahun = $("#tahunPeriode").val();
            if(peTahun==""){
                alert("Data belum lengkap")
                return
            }
            var cek = confirm("Apakah anda yakin akan melakukan proses ini?")
            if(cek){
                $.ajax({
                    url:'${request.contextPath}/jurnalAkhirTahun/doProses',
                    type: "POST", // Always use POST when deleting data
                    data: { tahun : peTahun},
                    success : function(data){
                        if(data=="tidak"){
                            alert("Belum ada data monthly balance bulan terakhir tahun tersebut");
                        }else{
                            alert("Proses penjurnalan telah selesai");
                            accNumberCari = "5.10.30.01.002";
                        }
                    },
                    error: function(xhr, status) {
                        alert("Internal server error");
                    }
                });
            }
        }

        function doView(){
            accNumberCari = "5.10.30.01.002";
            var peTahun = $("#tahunPeriode").val();
            if(peTahun==""){
                alert("Data belum lengkap")
                return
            }
            reloadJurnalAkhirTahunTable();
        }
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="jurnalAkhirTahun.label" default="Jurnal Akhir Tahun" /></span>
</div>
<div class="box">
    <div class="span12" id="jurnalAkhirTahun-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <table style="margin-left: 30px">
            <tr>
                <td style="padding-right: 10px">Periode</td>
                <td>
                    <g:textField name="tahunPeriode" id="tahunPeriode" value="${skrg.format("yyyy")}" maxlength="4" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <button class="btn btn-primary" onclick="doSearch();">Proses</button>
                    <button class="btn btn-primary" onclick="doView();">View</button>
                </td>
            </tr>
        </table>
        <br/>
        <g:render template="dataTables" />
        %{--<g:render template="dataTables" />--}%
    </div>
    <div class="span7" id="jurnalAkhirTahun-form" style="display: none;"></div>
</div>
</body>
</html>