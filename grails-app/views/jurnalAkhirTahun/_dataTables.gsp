
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="jurnalAkhirTahun_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="jurnalAkhirTahun.accountNumber.label" default="Nomor Akun" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="jurnalAkhirTahun.accountName.label" default="Nama Akun" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="jurnalAkhirTahun.startingBalance.label" default="Saldo Awal" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="jurnalAkhirTahun.debetMutation.label" default="Mutasi Debet" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="jurnalAkhirTahun.creditMutation.label" default="Mutasi Kredit" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="jurnalAkhirTahun.endingBalance.label" default="Saldo Akhir" /></div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var JurnalAkhirTahunTable;
var reloadJurnalAkhirTahunTable;
$(function(){
	
	reloadJurnalAkhirTahunTable = function() {
		JurnalAkhirTahunTable.fnDraw();
	}

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	JurnalAkhirTahunTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	JurnalAkhirTahunTable = $('#jurnalAkhirTahun_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
        "fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "accountNumber",
	"mDataProp": "accountNumber",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "accountName",
	"mDataProp": "accountName",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "startingBalance",
	"mDataProp": "startingBalance",
	"aTargets": [4],
    "mRender": function ( data, type, row ) {
		return '<span class="pull-right">'+data+'</span>';
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "debetMutation",
	"mDataProp": "debetMutation",
	"aTargets": [5],
    "mRender": function ( data, type, row ) {
		return '<span class="pull-right">'+data+'</span>';
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "creditMutation",
	"mDataProp": "creditMutation",
	"aTargets": [6],
    "mRender": function ( data, type, row ) {
		return '<span class="pull-right">'+data+'</span>';
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "endingBalance",
	"mDataProp": "endingBalance",
	"aTargets": [7],
    "mRender": function ( data, type, row ) {
		return '<span class="pull-right">'+data+'</span>';
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        aoData.push(
                            {"name": 'akun', "value": accNumberCari},
                            {"name": 'tahun', "value": $("#tahunPeriode").val()}
                        );

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

});
</g:javascript>