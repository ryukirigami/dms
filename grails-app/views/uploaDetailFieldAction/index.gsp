
<%@ page import="com.kombos.customerprofile.FA" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="main">
    <title><g:message code="uploadDetailFA.label" default="Upload Detail Field Action"/></title>
    <r:require modules="baseapplayout" />
    <r:require modules="highslide" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/swfobject/2.1/swfobject.js"></script>

    <g:javascript>
        <g:if test="${jmlhDataError && jmlhDataError>0}">
            $('#save').attr("disabled", true);
        </g:if>

        var saveForm;
        $(function(){

        changeFA = function(){
            var idTemp = $("#fa").val();
            var idFA = idTemp ? idTemp : '-'
            var rowCount = $('#uploaDetailFieldAction_datatables tr').length;
            for(var a=0;a < parseInt(rowCount)-1;a++){
                $('#txt'+a+'').val(idFA)
            }
            return

        }

        addMore = function(){
            $("#addMoreContent").empty();
            $.ajax({type:'POST', url:'${request.contextPath}/FA?isFrom=uploadDetail',
                success:function(data,textStatus){
                        $("#addMoreContent").html(data);
                        $("#addMoreModal").modal({
                            "backdrop" : "static",
                            "keyboard" : false,
                            "show" : true
                        }).css({'width': '1200px','margin-left': function () {return -($(this).width() / 2);}});

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
        }

        closeModal = function(modal){
            var root="${resource()}";
            var url = root+'/addDetailFieldAction/findList';
            jQuery('#fa').load(url);
            $('#addMoreModal').modal('hide');
        }

            function progress(e){
                if(e.lengthComputable){
                    //kalo mau pake progress bar
                    //$('progress').attr({value:e.loaded,max:e.total});
                }
            }

            saveForm = function() {
                var jum=0;
                var id = $('#fa').val()
                var idFA = []
                var vinCode = []
                var mainDealer = []
                var dealer = []
                var ind = 0
                $("#uploaDetailFieldAction_datatables tbody .row-select").each(function() {
                    if(this.checked){
                        jum++;
                        idFA.push(document.getElementById("txt"+ind).value)
                        vinCode.push(document.getElementById("customerVehicle"+ind).value)
                        mainDealer.push(document.getElementById("t185MainDealer"+ind).value)
                        dealer.push(document.getElementById("t185Dealer"+ind).value)
                    }
                    ind++
                });
                if(jum==0){
                    alert('Pilih data yang akan di simpan');
                    return
                }
                if(id==null || id=='' || id=='null'){
                    alert('Pilih deskripsi Field Action');
                    return
                }
                var cek = confirm('Anda yakin data akan disimpan?')
                if(cek){
                    var jvinCode = JSON.stringify(vinCode);
                    var jid = JSON.stringify(idFA);
                    var jmainDelaer = JSON.stringify(mainDealer);
                    var jdelaer = JSON.stringify(dealer);

                    $.ajax({
                        url:'${request.getContextPath()}/uploaDetailFieldAction/upload',
                        type: "POST",
                        data : { vincode : jvinCode , ids : jid, dealer : jdelaer , mainDealer : jmainDelaer},
                        success : function(data){
                            $('#detailFATable').empty();
                            $('#detailFATable').append(data);
                            toastr.success("Sukses Upload");
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            alert('Internal Server Error');
                        }
                    });
                }
            }


});

function selectAll(){
if($('input[name=checkSelect]').is(':checked')){
    $(".row-select").attr("checked",true);
}else {
    $(".row-select").attr("checked",false);
}
}

$('#uploaDetailFieldAction_datatables tbody tr').on('click', function () {
var jum=0;
$("#uploaDetailFieldAction_datatables tbody .row-select").each(function() {
    if(this.checked){
        jum++;
    }
});
var rowCount = $('#uploaDetailFieldAction_datatables tbody tr').length;
if($(this).find('.row-select').is(":checked")){
    if(rowCount == jum){
        $('.select-all').attr('checked', true);
    }
} else {
    $('.select-all').attr('checked', false);
}
});

    </g:javascript>

</head>
<body>
<div id="detailFATable">
    <div class="navbar box-header no-border">
        <span class="pull-left">
            <g:message code="uploadDetailFA.label" default="Upload Detail Field Action"/>
        </span>
        <ul class="nav pull-right">
            <li></li>
            <li></li>
            <li class="separator"></li>
        </ul>
    </div>
    <div class="box">
        <div class="span12" id="vehicleFA-table">
            <fieldset>
                <form id="uploaDetailFieldAction-save" class="form-vertical" action="${request.getContextPath()}/uploaDetailFieldAction/save" method="post" onsubmit="submitForm();return false;">
                    <table>
                        <tr>
                            <td>
                                <fieldset class="form">
                                    <g:render template="form"/>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <fieldset class="buttons controls">
                                    <g:submitButton class="btn btn-primary create" name="view" id="view" value="${message(code: 'default.button.view.label', default: 'Upload')}" />
                                    <g:field type="button" onclick="saveForm();" class="btn btn-primary create" name="save" id="save" value="${message(code: 'default.button.upload.label', default: 'Simpan')}"/>
                                    &nbsp;&nbsp;&nbsp;
                                    <g:if test="${flash.message}">
                                        ${flash.message}
                                    </g:if>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </form>
                <g:javascript>
                        var submitForm;
                        $(function(){

                            function progress(e){
                                if(e.lengthComputable){
                                    //kalo mau pake progress bar
                                    //$('progress').attr({value:e.loaded,max:e.total});
                                }
                            }

                            submitForm = function() {
                                var fa = $('#fa').val()
                                var form = new FormData($('#uploaDetailFieldAction-save')[0]);

                                $.ajax({
                                    url:'${request.getContextPath()}/uploaDetailFieldAction/view?fa='+fa,
                                    type: 'POST',
                                    xhr: function() {
                                        var myXhr = $.ajaxSettings.xhr();
                                        if(myXhr.upload){
                                            myXhr.upload.addEventListener('progress',progress, false);
                                        }
                                        return myXhr;
                                    },
                                    //add beforesend handler to validate or something
                                    //beforeSend: functionname,
                                    success: function (res) {
                                        $('#detailFATable').empty();
                                        $('#detailFATable').append(res);
                                    },
                                    //add error handler for when a error occurs if you want!
                                    error: function (data, status, e){
                                        alert(e);
                                    },
                                    complete: function(xhr, status) {
                                    },
                                    data: form,
//                                    cache: false,
                                    contentType: false,
                                    processData: false
                                });

                            }
                        });
                </g:javascript>
            </fieldset>
            <br>
            <div class="controls">
                Deskripsi Field Action  *
                <g:select id="fa" name="fa.id" style="width:85%" noSelection="['':'Pilih Field Action']" from="${FA.createCriteria().list{order("m185NamaFA");}}"
                    optionKey="id" required="" value="${idFA}" onchange="changeFA();" class="many-to-one"/>
                <button class="btn cancel" onclick="addMore();"> ... </button>
            </div>
            <br>
            <table id="uploaDetailFieldAction_datatables" cellpadding="0" cellspacing="0"
                   border="0"
                   class="display table table-striped table-bordered table-hover fixed table-selectable" style="margin-left: 0px; width: 100%;">
                <thead>
                <tr>
                    <th style="width: 20%">&nbsp;&nbsp;
                        <input type="checkbox" class="select-all" name="checkSelect" id="checkSelect" onclick="selectAll();"/>&nbsp;&nbsp;
                    Kode FA
                    </th>
                    <th style="width: 23%">
                        VinCode
                    </th>
                    <th style="width: 23%">
                        Main Dealer
                    </th>
                    <th style="width: 33%">
                        Dealer
                    </th>
                </tr>
                </thead>
                <tbody>
                <g:if test="${htmlData}">
                    ${htmlData}
                </g:if>
                <g:else>
                    <tr class="odd">
                        <td class="dataTables_empty" valign="top" colspan="9">No data available in table</td>
                    </tr>
                </g:else>
                </tbody>
            </table>
        </div>
    </div>
    <div id="addMoreModal" class="modal hide" style="width: 1200px;">
        <div class="modal-dialog" style="width: 1200px;">
            <div class="modal-content" style="width: 1200px; height: 550">
                <div class="modal-body" style="max-height: 550px;">
                    <div id="addMoreContent">
                        <div class="iu-content"></div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button name="closeModal" type="button" onclick="closeModal();" class="btn cancel">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
