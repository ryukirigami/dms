<%@ page import="com.kombos.administrasi.Gear; com.kombos.administrasi.BodyType; com.kombos.administrasi.ModelName; com.kombos.administrasi.BaseModel; com.kombos.administrasi.Grade" %>

<g:javascript>
    var selectModelName;
    var selectBodyType;
    var selectGear;
			$(function(){
			selectModelName = function () {
			var idBaseModel = $('#baseModel option:selected').val();
			    if(idBaseModel != undefined){
                    jQuery.getJSON('${request.contextPath}/bodyType/getModelNames?idBaseModel='+idBaseModel, function (data) {
                            $('#modelName').empty();
                            if (data) {
                                jQuery.each(data, function (index, value) {
                                    $('#modelName').append("<option value='" + value.id + "'>" + value.kodeModelName + "</option>");
                                });
                            }
                            selectBodyType();
                        });
                }else{
                     $('#modelName').empty();
                     $('#bodyType').empty();
                     $('#gear').empty();
                }
            };

			selectBodyType = function () {
			var idModelName = $('#modelName').val();
			    if(idModelName != undefined){
                    jQuery.getJSON('${request.contextPath}/bodyType/getBodyTypes?idModelName='+idModelName, function (data) {
                            $('#bodyType').empty();
                            if (data) {
                                jQuery.each(data, function (index, value) {
                                    $('#bodyType').append("<option value='" + value.id + "'>" + value.kodeBodyType + "</option>");
                                });
                            }
                            selectGear();
                        });
                }else{
                     $('#bodyType').empty();
                     $('#gear').empty();
                }
            };

            selectGear = function() {
            var idBodyType = $('#bodyType').val();
                if (idBodyType != undefined){
                    jQuery.getJSON('${request.contextPath}/bodyType/getGears?idBodyType='+idBodyType, function (data) {
                            $('#gear').empty();
                            if (data) {
                                jQuery.each(data, function (index, value) {
                                    $('#gear').append("<option value='" + value.id + "'>" + value.kodeGear + "</option>");
                                });
                            }
                        });
                }else{
                     $('#gear').empty();
                }
            };
    });
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: gradeInstance, field: 'baseModel', 'error')} required">
    <label class="control-label" for="baseModel">
        <g:message code="grade.baseModel.label" default="Base Model" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="baseModel" onchange="selectModelName();" name="baseModel.id" noSelection="['':'Pilih Base Model']" from="${BaseModel.createCriteria().list(){eq("staDel","0");order("m102KodeBaseModel")}}" optionValue="${{it.m102KodeBaseModel + " - " + it.m102NamaBaseModel}}" optionKey="id" required="" value="${gradeInstance?.baseModel?.id}" class="many-to-one"/>
    </div>
</div>
<g:javascript>
    document.getElementById("baseModel").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: gradeInstance, field: 'modelName', 'error')} required">
    <label class="control-label" for="modelName">
        <g:message code="grade.modelName.label" default="Model Name" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="modelName" onchange="selectBodyType();" name="modelName.id" noSelection="['':'Pilih Model Name']" optionValue="${{it.m104KodeModelName + " - " + it.m104NamaModelName}}" from="${ModelName.createCriteria().list(){eq("staDel","0");order("m104KodeModelName")}}" optionKey="id" required="" value="${gradeInstance?.modelName?.id}" class="many-to-one"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: gradeInstance, field: 'bodyType', 'error')} required">
    <label class="control-label" for="bodyType">
        <g:message code="grade.bodyType.label" default="Body Type" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="bodyType" onchange="selectGear();" name="bodyType.id" optionValue="${{it.m105KodeBodyType + " - " + it.m105NamaBodyType}}" noSelection="['':'Pilih Body Type']" from="${BodyType.createCriteria().list(){eq("staDel","0");order("m105KodeBodyType")}}" optionKey="id" required="" value="${gradeInstance?.bodyType?.id}" class="many-to-one"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: gradeInstance, field: 'gear', 'error')} required">
    <label class="control-label" for="gear">
        <g:message code="grade.gear.label" default="Gear" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="gear" name="gear.id" noSelection="['':'Pilih Gear']" optionValue="${{it.m106KodeGear + " - " + it.m106NamaGear}}" from="${Gear.createCriteria().list(){eq("staDel","0");order("m106KodeGear")}}" optionKey="id" required="" value="${gradeInstance?.gear?.id}" class="many-to-one"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: gradeInstance, field: 'm107KodeGrade', 'error')} required">
	<label class="control-label" for="m107KodeGrade">
		<g:message code="grade.m107KodeGrade.label" default="Kode Grade" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m107KodeGrade" maxlength="2" required="" value="${gradeInstance?.m107KodeGrade}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: gradeInstance, field: 'm107NamaGrade', 'error')} required">
	<label class="control-label" for="m107NamaGrade">
		<g:message code="grade.m107NamaGrade.label" default="Nama Grade" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m107NamaGrade" maxlength="20" required="" value="${gradeInstance?.m107NamaGrade}"/>
	</div>
</div>
