<%@ page import="com.kombos.administrasi.Grade" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="bootstrapeditable"/>
    <g:set var="entityName"
           value="${message(code: 'grade.label', default: 'Grade')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <g:javascript disposition="head">
var deleteGrade;

$(function(){ 
	deleteGrade=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/grade/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadGradeTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

    </g:javascript>
</head>

<body>
<div id="show-grade" role="main">
<legend>
    <g:message code="default.show.label" args="[entityName]"/>
</legend>
<g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
</g:if>
<table id="grade"
       class="table table-bordered table-hover">
<tbody>

<g:if test="${gradeInstance?.baseModel}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="baseModel-label" class="property-label"><g:message
                    code="grade.baseModel.label" default="Base Model"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="baseModel-label">
            %{--<ba:editableValue
                    bean="${gradeInstance}" field="baseModel"
                    url="${request.contextPath}/Grade/updatefield" type="text"
                    title="Enter baseModel" onsuccess="reloadGradeTable();" />--}%

            ${gradeInstance?.baseModel?.m102NamaBaseModel?.encodeAsHTML()}

        </span></td>

    </tr>
</g:if>

<g:if test="${gradeInstance?.modelName}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="modelName-label" class="property-label"><g:message
                    code="grade.modelName.label" default="Model Name"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="modelName-label">
            %{--<ba:editableValue
                    bean="${gradeInstance}" field="modelName"
                    url="${request.contextPath}/Grade/updatefield" type="text"
                    title="Enter modelName" onsuccess="reloadGradeTable();" />--}%

            ${gradeInstance?.modelName?.m104NamaModelName?.encodeAsHTML()}

        </span></td>

    </tr>
</g:if>

<g:if test="${gradeInstance?.bodyType}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="bodyType-label" class="property-label"><g:message
                    code="grade.bodyType.label" default="Body Type"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="bodyType-label">
            %{--<ba:editableValue
                    bean="${gradeInstance}" field="bodyType"
                    url="${request.contextPath}/Grade/updatefield" type="text"
                    title="Enter bodyType" onsuccess="reloadGradeTable();" />--}%

            ${gradeInstance?.bodyType?.m105NamaBodyType?.encodeAsHTML()}

        </span></td>

    </tr>
</g:if>

<g:if test="${gradeInstance?.gear}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="gear-label" class="property-label"><g:message
                    code="grade.gear.label" default="Gear"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="gear-label">
            %{--<ba:editableValue
                    bean="${gradeInstance}" field="gear"
                    url="${request.contextPath}/Grade/updatefield" type="text"
                    title="Enter gear" onsuccess="reloadGradeTable();" />--}%

            ${gradeInstance?.gear?.m106NamaGear?.encodeAsHTML()}

        </span></td>

    </tr>
</g:if>

<g:if test="${gradeInstance?.m107KodeGrade}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="m107KodeGrade-label" class="property-label"><g:message
                    code="grade.m107KodeGrade.label" default="Kode Grade"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="m107KodeGrade-label">
            %{--<ba:editableValue
                    bean="${gradeInstance}" field="m107KodeGrade"
                    url="${request.contextPath}/Grade/updatefield" type="text"
                    title="Enter m107KodeGrade" onsuccess="reloadGradeTable();" />--}%

            <g:fieldValue bean="${gradeInstance}" field="m107KodeGrade"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${gradeInstance?.m107NamaGrade}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="m107NamaGrade-label" class="property-label"><g:message
                    code="grade.m107NamaGrade.label" default="Nama Grade"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="m107NamaGrade-label">
            %{--<ba:editableValue
                    bean="${gradeInstance}" field="m107NamaGrade"
                    url="${request.contextPath}/Grade/updatefield" type="text"
                    title="Enter m107NamaGrade" onsuccess="reloadGradeTable();" />--}%

            <g:fieldValue bean="${gradeInstance}" field="m107NamaGrade"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${gradeInstance?.lastUpdProcess}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="lastUpdProcess-label" class="property-label"><g:message
                    code="grade.lastUpdProcess.label" default="Last Upd Process"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="lastUpdProcess-label">
            %{--<ba:editableValue
                    bean="${gradeInstance}" field="lastUpdProcess"
                    url="${request.contextPath}/grade/updatefield" type="text"
                    title="Enter lastUpdProcess" onsuccess="reloadgradeTable();" />--}%

            <g:fieldValue bean="${gradeInstance}" field="lastUpdProcess"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${gradeInstance?.createdBy}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="createdBy-label" class="property-label"><g:message
                    code="grade.createdBy.label" default="Created By"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="createdBy-label">
            %{--<ba:editableValue
                    bean="${gradeInstance}" field="createdBy"
                    url="${request.contextPath}/grade/updatefield" type="text"
                    title="Enter createdBy" onsuccess="reloadgradeTable();" />--}%

            <g:fieldValue bean="${gradeInstance}" field="createdBy"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${gradeInstance?.lastUpdated}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="lastUpdated-label" class="property-label"><g:message
                    code="grade.lastUpdated.label" default="Last Updated"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="lastUpdated-label">
            %{--<ba:editableValue
                    bean="${gradeInstance}" field="lastUpdated"
                    url="${request.contextPath}/grade/updatefield" type="text"
                    title="Enter lastUpdated" onsuccess="reloadgradeTable();" />--}%

            <g:formatDate date="${gradeInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${gradeInstance?.updatedBy}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="updatedBy-label" class="property-label"><g:message
                    code="grade.updatedBy.label" default="Updated By"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="updatedBy-label">
            %{--<ba:editableValue
                    bean="${gradeInstance}" field="updatedBy"
                    url="${request.contextPath}/grade/updatefield" type="text"
                    title="Enter updatedBy" onsuccess="reloadgradeTable();" />--}%

            <g:fieldValue bean="${gradeInstance}" field="updatedBy"/>

        </span></td>

    </tr>
</g:if>

%{--
                    <g:if test="${gradeInstance?.m107ID}">
    <tr>
    <td class="span2" style="text-align: right;"><span
        id="m107ID-label" class="property-label"><g:message
        code="grade.m107ID.label" default="M107 ID" />:</span></td>
        
            <td class="span3"><span class="property-value"
            aria-labelledby="m107ID-label">
            <ba:editableValue
                    bean="${gradeInstance}" field="m107ID"
                    url="${request.contextPath}/Grade/updatefield" type="text"
                    title="Enter m107ID" onsuccess="reloadGradeTable();" />
                
                    <g:fieldValue bean="${gradeInstance}" field="m107ID"/>
                
            </span></td>
        
    </tr>
    </g:if>
    <g:if test="${gradeInstance?.staDel}">
    <tr>
    <td class="span2" style="text-align: right;"><span
        id="staDel-label" class="property-label"><g:message
        code="grade.staDel.label" default="Sta Del" />:</span></td>
        
            <td class="span3"><span class="property-value"
            aria-labelledby="staDel-label">
            <ba:editableValue
                    bean="${gradeInstance}" field="staDel"
                    url="${request.contextPath}/Grade/updatefield" type="text"
                    title="Enter staDel" onsuccess="reloadGradeTable();" />
                
                    <g:fieldValue bean="${gradeInstance}" field="staDel"/>
                
            </span></td>
        
    </tr>
    </g:if>--}%

</tbody>
</table>
<g:form class="form-horizontal">
    <fieldset class="buttons controls">
        <a class="btn cancel" href="javascript:void(0);"
           onclick="expandTableLayout();"><g:message
                code="default.button.cancel.label" default="Cancel"/></a>
        <g:remoteLink class="btn btn-primary edit" action="edit"
                      id="${gradeInstance?.id}"
                      update="[success: 'grade-form', failure: 'grade-form']"
                      on404="alert('not found');">
            <g:message code="default.button.edit.label" default="Edit"/>
        </g:remoteLink>
        <ba:confirm id="delete" class="btn cancel"
                    message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
                    onsuccess="deleteGrade('${gradeInstance?.id}')"
                    label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
    </fieldset>
</g:form>
</div>
</body>
</html>
