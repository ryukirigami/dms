
<%@ page import="com.kombos.administrasi.Grade" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="grade_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="grade.baseModel.label" default="Base Model" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="grade.modelName.label" default="Model Name" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="grade.bodyType.label" default="Body Type" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="grade.gear.label" default="Gear" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="grade.m107KodeGrade.label" default="Kode Grade" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="grade.m107NamaGrade.label" default="Nama Grade" /></div>
			</th>
		</tr>
		<tr>
		
	
			
			<th style="border-top: none;padding: 5px;">
				<div id="filter_baseModel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_baseModel" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_modelName" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_modelName" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_bodyType" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_bodyType" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_gear" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_gear" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m107KodeGrade" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m107KodeGrade" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m107NamaGrade" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m107NamaGrade" class="search_init" />
				</div>
			</th>
		</tr>
	</thead>
</table>

<g:javascript>
var gradeTable;
var reloadGradeTable;
$(function(){
	
	reloadGradeTable = function() {
		gradeTable.fnDraw();
	}

    var recordsgradeperpage = [];
    var anGradeSelected;
    var jmlRecGradePerPage=0;
    var id;

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	gradeTable.fnDraw();
		}
	});

	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	gradeTable = $('#grade_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsGrade = $("#grade_datatables tbody .row-select");
            var jmlGradeCek = 0;
            var nRow;
            var idRec;
            rsGrade.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsgradeperpage[idRec]=="1"){
                    jmlGradeCek = jmlGradeCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsgradeperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecGradePerPage = rsGrade.length;
            if(jmlGradeCek==jmlRecGradePerPage && jmlRecGradePerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "baseModel",
	"mDataProp": "baseModel",
	"aTargets": [1],
        "mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "modelName",
	"mDataProp": "modelName",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "bodyType",
	"mDataProp": "bodyType",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "gear",
	"mDataProp": "gear",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m107KodeGrade",
	"mDataProp": "m107KodeGrade",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m107NamaGrade",
	"mDataProp": "m107NamaGrade",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "staDel",
	"mDataProp": "staDel",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m107ID = $('#filter_m107ID input').val();
						if(m107ID){
							aoData.push(
									{"name": 'sCriteria_m107ID', "value": m107ID}
							);
						}
	
						var baseModel = $('#filter_baseModel input').val();
						if(baseModel){
							aoData.push(
									{"name": 'sCriteria_baseModel', "value": baseModel}
							);
						}
	
						var modelName = $('#filter_modelName input').val();
						if(modelName){
							aoData.push(
									{"name": 'sCriteria_modelName', "value": modelName}
							);
						}
	
						var bodyType = $('#filter_bodyType input').val();
						if(bodyType){
							aoData.push(
									{"name": 'sCriteria_bodyType', "value": bodyType}
							);
						}
	
						var gear = $('#filter_gear input').val();
						if(gear){
							aoData.push(
									{"name": 'sCriteria_gear', "value": gear}
							);
						}
	
						var m107KodeGrade = $('#filter_m107KodeGrade input').val();
						if(m107KodeGrade){
							aoData.push(
									{"name": 'sCriteria_m107KodeGrade', "value": m107KodeGrade}
							);
						}
	
						var m107NamaGrade = $('#filter_m107NamaGrade input').val();
						if(m107NamaGrade){
							aoData.push(
									{"name": 'sCriteria_m107NamaGrade', "value": m107NamaGrade}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
    $('.select-all').click(function(e) {
        $("#grade_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsgradeperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsgradeperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

    $('#grade_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsgradeperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anGradeSelected = gradeTable.$('tr.row_selected');
            if(jmlRecGradePerPage == anGradeSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsgradeperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
