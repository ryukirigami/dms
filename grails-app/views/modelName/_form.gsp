<%@ page import="com.kombos.administrasi.BaseModel; com.kombos.administrasi.ModelName" %>



<div class="control-group fieldcontain ${hasErrors(bean: modelNameInstance, field: 'baseModel', 'error')} required">
	<label class="control-label" for="baseModel">
		<g:message code="modelName.baseModel.label" default="Base Model" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="baseModel" name="baseModel.id" noSelection="['':'Pilih Base Model']" from="${BaseModel.createCriteria().list(){eq("staDel","0");order("m102KodeBaseModel")}}" optionValue="${{it.m102KodeBaseModel + " - " + it.m102NamaBaseModel}}" optionKey="id" required="" value="${modelNameInstance?.baseModel?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: modelNameInstance, field: 'm104KodeModelName', 'error')} required">
	<label class="control-label" for="m104KodeModelName">
		<g:message code="modelName.m104KodeModelName.label" default="Kode Model Name" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m104KodeModelName" maxlength="2" required="" value="${modelNameInstance?.m104KodeModelName}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: modelNameInstance, field: 'm104NamaModelName', 'error')} required">
	<label class="control-label" for="m104NamaModelName">
		<g:message code="modelName.m104NamaModelName.label" default="Nama Model Name" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m104NamaModelName" maxlength="20" required="" value="${modelNameInstance?.m104NamaModelName}"/>
	</div>
</div>
<g:javascript>
    document.getElementById("baseModel").focus();
</g:javascript>
%{--
<div class="control-group fieldcontain ${hasErrors(bean: modelNameInstance, field: 'staDel', 'error')} required">
	<label class="control-label" for="staDel">
		<g:message code="modelName.staDel.label" default="Sta Del" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="staDel" maxlength="1" required="" value="${modelNameInstance?.staDel}"/>
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: modelNameInstance, field: 'm104ID', 'error')} required">
	<label class="control-label" for="m104ID">
		<g:message code="modelName.m104ID.label" default="M104 ID" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field name="m104ID" type="number" value="${modelNameInstance.m104ID}" required=""/>
	</div>
</div>
--}%
