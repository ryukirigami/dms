

<%@ page import="com.kombos.administrasi.ModelName" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'modelName.label', default: 'Model Name')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteModelName;

$(function(){ 
	deleteModelName=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/modelName/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadModelNameTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-modelName" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="modelName"
			class="table table-bordered table-hover">
			<tbody>

				
				
			
				<g:if test="${modelNameInstance?.baseModel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="baseModel-label" class="property-label"><g:message
					code="modelName.baseModel.label" default="Base Model" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="baseModel-label">
								${modelNameInstance?.baseModel?.m102NamaBaseModel?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${modelNameInstance?.m104KodeModelName}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m104KodeModelName-label" class="property-label"><g:message
					code="modelName.m104KodeModelName.label" default="Kode Model Name" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m104KodeModelName-label">
						%{--<ba:editableValue
								bean="${modelNameInstance}" field="m104KodeModelName"
								url="${request.contextPath}/ModelName/updatefield" type="text"
								title="Enter m104KodeModelName" onsuccess="reloadModelNameTable();" />--}%
							
								<g:fieldValue bean="${modelNameInstance}" field="m104KodeModelName"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${modelNameInstance?.m104NamaModelName}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m104NamaModelName-label" class="property-label"><g:message
					code="modelName.m104NamaModelName.label" default="Nama Model Name" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m104NamaModelName-label">
						%{--<ba:editableValue
								bean="${modelNameInstance}" field="m104NamaModelName"
								url="${request.contextPath}/ModelName/updatefield" type="text"
								title="Enter m104NamaModelName" onsuccess="reloadModelNameTable();" />--}%
							
								<g:fieldValue bean="${modelNameInstance}" field="m104NamaModelName"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${modelNameInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="modelName.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${modelNameInstance}" field="dateCreated"
                                url="${request.contextPath}/modelName/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadmodelNameTable();" />--}%

                        <g:formatDate date="${modelNameInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${modelNameInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="modelName.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${modelNameInstance}" field="createdBy"
                                url="${request.contextPath}/modelName/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadmodelNameTable();" />--}%

                        <g:fieldValue bean="${modelNameInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${modelNameInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="modelName.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${modelNameInstance}" field="lastUpdated"
                                url="${request.contextPath}/modelName/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadmodelNameTable();" />--}%

                        <g:formatDate date="${modelNameInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${modelNameInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="modelName.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${modelNameInstance}" field="updatedBy"
                                url="${request.contextPath}/modelName/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadmodelNameTable();" />--}%

                        <g:fieldValue bean="${modelNameInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${modelNameInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="modelName.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${modelNameInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/modelName/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadmodelNameTable();" />--}%

                        <g:fieldValue bean="${modelNameInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
			
                        %{--
                                <g:if test="${modelNameInstance?.m104ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m104ID-label" class="property-label"><g:message
					code="modelName.m104ID.label" default="M104 ID" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m104ID-label">
						<ba:editableValue
								bean="${modelNameInstance}" field="m104ID"
								url="${request.contextPath}/ModelName/updatefield" type="text"
								title="Enter m104ID" onsuccess="reloadModelNameTable();" />
							
								<g:fieldValue bean="${modelNameInstance}" field="m104ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
				<g:if test="${modelNameInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="modelName.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						<ba:editableValue
								bean="${modelNameInstance}" field="staDel"
								url="${request.contextPath}/ModelName/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadModelNameTable();" />
							
								<g:fieldValue bean="${modelNameInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>--}%
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${modelNameInstance?.id}"
					update="[success:'modelName-form',failure:'modelName-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteModelName('${modelNameInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
