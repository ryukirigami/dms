
<%@ page import="com.kombos.administrasi.ModelName" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="modelName_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="modelName.baseModel.label" default="Base Model" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="modelName.m104KodeModelName.label" default="Kode Model Name" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="modelName.m104NamaModelName.label" default="Nama Model Name" /></div>
			</th>


		</tr>
		<tr>
		
	
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_baseModel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_baseModel" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m104KodeModelName" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m104KodeModelName" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m104NamaModelName" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m104NamaModelName" class="search_init" />
				</div>
			</th>
	

		</tr>
	</thead>
</table>

<g:javascript>
var ModelNameTable;
var reloadModelNameTable;
$(function(){
	
	reloadModelNameTable = function() {
		ModelNameTable.fnDraw();
	}

    var recordsmodelNameperpage = [];
    var anModelNameSelected;
    var jmlRecModelNamePerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	ModelNameTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	ModelNameTable = $('#modelName_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsModelName = $("#modelName_datatables tbody .row-select");
            var jmlModelNameCek = 0;
            var nRow;
            var idRec;
            rsModelName.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsmodelNameperpage[idRec]=="1"){
                    jmlModelNameCek = jmlModelNameCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsmodelNameperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecModelNamePerPage = rsModelName.length;
            if(jmlModelNameCek==jmlRecModelNamePerPage && jmlRecModelNamePerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
        "fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "baseModel",
	"mDataProp": "baseModel",
	"aTargets": [1],
        "mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m104KodeModelName",
	"mDataProp": "m104KodeModelName",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m104NamaModelName",
	"mDataProp": "m104NamaModelName",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "staDel",
	"mDataProp": "staDel",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m104ID = $('#filter_m104ID input').val();
						if(m104ID){
							aoData.push(
									{"name": 'sCriteria_m104ID', "value": m104ID}
							);
						}
	
						var baseModel = $('#filter_baseModel input').val();
						if(baseModel){
							aoData.push(
									{"name": 'sCriteria_baseModel', "value": baseModel}
							);
						}
	
						var m104KodeModelName = $('#filter_m104KodeModelName input').val();
						if(m104KodeModelName){
							aoData.push(
									{"name": 'sCriteria_m104KodeModelName', "value": m104KodeModelName}
							);
						}
	
						var m104NamaModelName = $('#filter_m104NamaModelName input').val();
						if(m104NamaModelName){
							aoData.push(
									{"name": 'sCriteria_m104NamaModelName', "value": m104NamaModelName}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
    $('.select-all').click(function(e) {
        $("#modelName_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsmodelNameperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsmodelNameperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

    $('#modelName_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsmodelNameperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anModelNameSelected = modelNameTable.$('tr.row_selected');
            if(jmlRecModelNamePerPage == anModelNameSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsmodelNameperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
