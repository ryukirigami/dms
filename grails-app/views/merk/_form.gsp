<%@ page import="com.kombos.customerprofile.Merk" %>


<g:if test="${merkInstance?.m064ID}">
<div class="control-group fieldcontain ${hasErrors(bean: merkInstance, field: 'Kode', 'error')} ">
	<label class="control-label" for="m064ID">
		<g:message code="merk.m064ID.label" default="Kode Merk" />
		
	</label>
	<div class="controls">
        ${merkInstance?.m064ID}
	</div>
</div>
</g:if>

<div class="control-group fieldcontain ${hasErrors(bean: merkInstance, field: 'NamaMerk', 'error')} ">
	<label class="control-label" for="m064NamaMerk">
		<g:message code="merk.m064NamaMerk.label" default="Nama Merk *" />
		
	</label>
	<div class="controls">
	<g:textField name="m064NamaMerk" maxlength="50" value="${merkInstance?.m064NamaMerk}" required=""/>
	</div>
</div>
