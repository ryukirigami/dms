<%@ page import="com.kombos.customerprofile.Merk" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'merk.label', default: 'Merk')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteMerk;

$(function(){ 
	deleteMerk=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/merk/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadMerkTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-merk" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="merk"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${merkInstance?.m064ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m064ID-label" class="property-label"><g:message
					code="merk.m064ID.label" default="Kode Merk" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m064ID-label">
						%{--<ba:editableValue
								bean="${merkInstance}" field="m064ID"
								url="${request.contextPath}/Merk/updatefield" type="text"
								title="Enter m064ID" onsuccess="reloadMerkTable();" />--}%
							
								<g:fieldValue bean="${merkInstance}" field="m064ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${merkInstance?.m064NamaMerk}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m064NamaMerk-label" class="property-label"><g:message
					code="merk.m064NamaMerk.label" default="Nama Merk" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m064NamaMerk-label">
						%{--<ba:editableValue
								bean="${merkInstance}" field="m064NamaMerk"
								url="${request.contextPath}/Merk/updatefield" type="text"
								title="Enter m064NamaMerk" onsuccess="reloadMerkTable();" />--}%
							
								<g:fieldValue bean="${merkInstance}" field="m064NamaMerk"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${merkInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="merk.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${merkInstance}" field="lastUpdProcess"
								url="${request.contextPath}/Merk/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadMerkTable();" />--}%
							
								<g:fieldValue bean="${merkInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${merkInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="merk.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${merkInstance}" field="dateCreated"
								url="${request.contextPath}/Merk/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadMerkTable();" />--}%
							
								<g:formatDate date="${merkInstance?.dateCreated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${merkInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="merk.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${merkInstance}" field="createdBy"
                                url="${request.contextPath}/Merk/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadMerkTable();" />--}%

                        <g:fieldValue bean="${merkInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${merkInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="merk.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${merkInstance}" field="lastUpdated"
								url="${request.contextPath}/Merk/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadMerkTable();" />--}%
							
								<g:formatDate date="${merkInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${merkInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="merk.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${merkInstance}" field="updatedBy"
                                url="${request.contextPath}/Merk/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadMerkTable();" />--}%

                        <g:fieldValue bean="${merkInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${merkInstance?.id}"
					update="[success:'merk-form',failure:'merk-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteMerk('${merkInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
