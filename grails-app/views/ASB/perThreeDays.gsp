
<%@ page import="com.kombos.board.ASB" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'ASB.label', default: 'ASB GR')}" />
		%{--<title><g:message code="default.list.label" args="[entityName]" /></title>--}%
		<r:require modules="baseapplayout, baseapplist" />
        <style>
        .dss-selected3 {
            background-color: #FF0000 !important;
        }
        </style>
		<g:javascript>
			var show;
			var edit;
            var dss3;
            var saveASB3;
            var tanggalDss = [];
            var jamStart = [];
			var jamStop = [];
			var arrStall = [];
			$(function(){

			$(".checkJanji3").click(function() {
                selectedBox = this.id;

                $(".checkJanji3").each(function() {
                    if ( this.id == selectedBox )
                    {
                        this.checked = true;
                    }
                    else
                    {
                        this.checked = false;
                    };
                });
            });

	$('#search_tglView3Hari').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tglView3Hari_day').val(newDate.getDate());
			$('#search_tglView3Hari_month').val(newDate.getMonth()+1);
			$('#search_tglView3Hari_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
//			var oTable = $('#ASB_datatables').dataTable();
//            oTable.fnReloadAjax();
	});

   	asbView3Hari = function(){
        var oTableDay1 = $('#ASB_datatablesDay1').dataTable();
        oTableDay1.fnReloadAjax();
        var oTableDay2 = $('#ASB_datatablesDay2').dataTable();
        oTableDay2.fnReloadAjax();
        var oTableDay3 = $('#ASB_datatablesDay3').dataTable();
        oTableDay3.fnReloadAjax();
        var from = $("#search_tglView3Hari").val().split("/");
        var theDate = new Date();
        theDate.setFullYear(from[2], from[1] - 1, from[0]);
        //alert("theDate=" + theDate);
        var m_names = new Array("January", "February", "March", "April", "Mei", "June", "July", "August", "September", "October", "November", "Desember");
        var formattedDate =  theDate.getDate() + " " + m_names[theDate.getMonth()] + " " + theDate.getFullYear();
        var formattedDatePlus1 =  (theDate.getDate() + 1) + " " + m_names[theDate.getMonth()] + " " + theDate.getFullYear();
        var formattedDatePlus2 =  (theDate.getDate() + 2) + " " + m_names[theDate.getMonth()] + " " + theDate.getFullYear();
        document.getElementById("lblTglViewPlus0").innerHTML = formattedDate;
        document.getElementById("lblTglViewPlus1").innerHTML = formattedDatePlus1;
        document.getElementById("lblTglViewPlus2").innerHTML = formattedDatePlus2;
   	}

            saveASB3 = function(){
                $first = $('.dss-selected3').first();
                jamDatang = $first.data("jam");
                menitDatang = $first.data("menit");
                $last = $('.dss-selected3').last();
                jamPenyerahan = $last.data("jam");
                menitPenyerahan = $last.data("menit");
                stallid = $first.data("stallid");
                var idApp = "${params.idApp}";
                var tglRencana = $('#input_rcvDate3').val();
			    var tglPenyerahan = $('#input_penyerahan3').val();
			    var checkJanjiDatang = $('#checkJanjiDatang3').attr('checked');
			    var checkJanjiPenyerahan = $('#checkJanjiPenyerahan3').attr('checked');

			    //Validasi Tanggal Receive
                if (tglRencana == null || tglRencana == ""){
                    alert('Tanggal Janji Datang belum lengkap!');
                    return
                }
                //Validasi Jam Receive
                if ($('#input_rcvHour3').val()=="null" || $('#input_rcvMinute3').val()=="null"){
                    alert('Jam janji datang belum lengkap!');
                    return
                }

                //Validasi Tanggal Penyerahan
                if (tglPenyerahan == null || tglPenyerahan == ""){
                    alert('Tanggal Janji Penyerahan masih Kosong!');
                    return
                }
                //Validasi Jam Penyerahan
                if ($('#input_penyerahanHour3').val()=="null" || $('#input_penyerahanMinute3').val()=="null"){
                    alert('Jam janji penyerahan belum lengkap!');
                    return
                }

			    if(checkJanjiPenyerahan == "checked"){
                    tanggal = tglPenyerahan;
			    } else if(checkJanjiDatang == "checked"){
			        tanggal = tglRencana;
			    }

			    if(!tanggal){
			        tanggal = $('#search_tglView').val();
			    }
                var jsonT = JSON.stringify(tanggalDss);
                var jsonSa = JSON.stringify(jamStart);
                var jsonSo = JSON.stringify(jamStop);
                var jsonS = JSON.stringify(arrStall);

                $.ajax({url: '${request.contextPath}/ASB/saveASB',
					type: "POST",
					data: {tanggal:jsonT,
					idApp:idApp,
					jamRencana:jsonSa,
					jamPenyerahan:jsonSo,
					stallid: jsonS
					},
					success: function(data) {
					    var oTable1 = $('#ASB_datatablesDay1').dataTable();
                        oTable1.fnReloadAjax();
                         var oTable2 = $('#ASB_datatablesDay2').dataTable();
                        oTable2.fnReloadAjax();
                         var oTable3 = $('#ASB_datatablesDay3').dataTable();
                        oTable3.fnReloadAjax();
					},
					complete : function (req, err) {
						$('#spinner').fadeOut();
						loading = false;
					}
				});
            }

            dss3 = function(){
                tanggalDss = []
                jamStart = []
                jamStop = []
                arrStall = []
                var idApp = "${params.idApp}";
                var tglRencana = $('#input_rcvDate3').val();
			    var jamRencana = $('#input_rcvHour3').val() + ":" + $('#input_rcvMinute3').val();
			    var tglPenyerahan = $('#input_penyerahan3').val();
			    var jamPenyerahan = $('#input_penyerahanHour3').val()+":"+$('#input_penyerahanMinute3').val();
			    var checkJanjiDatang = $('#checkJanjiDatang3').attr('checked');
			    var checkJanjiPenyerahan = $('#checkJanjiPenyerahan3').attr('checked');

                $.ajax({url: '${request.contextPath}/ASB/dssAppointment3',
					type: "POST",
					data: {tglRencana3:tglRencana,
					tglPenyerahan3:tglPenyerahan,
					idApp:idApp,
					jamRencana3:jamRencana,
					jamPenyerahan3:jamPenyerahan,
					checkJanjiDatang3: checkJanjiDatang,
					checkJanjiPenyerahan3: checkJanjiPenyerahan,
					aksi:"input"
					},
					success: function(data) {
					    var slots;
					    if(data.status == 'found'){
					        if(data.jumData > 1){
                                for(var a=0; a < data.jumData; a++){
                                    slots = data.slot[a]
                                    tanggalDss.push(data.tanggalDSS[a])
                                    arrStall.push(data.stallId[a])
                                    for(var i = 0; i < slots.length; i++){
                                        $('.'+slots[i]+'[data-stallId="'+data.stallId[a]+'"][data-date="'+data.tanggal[a]+'"]').addClass("dss-selected3");
                                        if(i==0){
                                            jamStart.push(slots[i]);
                                        }
                                        if(i+1==slots.length){
                                            jamStop.push(slots[i])
                                        }
                                    }
					            }
					        }
					        else{
					            slots = data.slot[0]
					            tanggalDss.push(data.tanggalDSS[0]);
					            jamStart.push(slots[0]);
					            jamStop.push(slots[slots.length-1]);
                                arrStall.push(data.stallId[0])
                                for(var j = 0; j < slots.length; j++){
                                    $('.'+slots[j]+'[data-stallId="'+data.stallId[0]+'"][data-date="'+data.tanggal[0]+'"]').addClass("dss-selected3");
                                }
					        }
					    }
//					    reloadASBTable();
					},
					complete : function (req, err) {
						$('#spinner').fadeOut();
						loading = false;
					}
				});
                }
});

</g:javascript>
	</head>
	<body>

	<div class="box">
        <div class="span4 row-fluid">

            <div id="searchInput" class="box">
                <legend style="font-size: small">Input ASB</legend>
                <table style="width: 100%;border: 0px">
                    <tr>
                        <td style="width: 35%;border: 0px">
                            <label class="control-label" for="lbl_rcvDate3">
                                Tgl/Jam Janji Datang <span>*</span>
                            </label>
                        </td>
                        <td>
                            <g:checkBox name="checkJanjiDatang3" class="checkJanji3"/>
                        </td>
                        <td>
                            <div id="lbl_rcvDate3" class="controls">
                                <ba:datePicker id="input_rcvDate3" name="input_rcvDate3" precision="day" format="dd-MM-yyyy"  value=""  />
                                <select id="input_rcvHour3" name="input_rcvHour3" style="width: 60px" required="">
                                %{
                                    out.println('<option value=null>Jam</option>');
                                    for (int i=0;i<24;i++){
                                        if(i<10){
                                            out.println('<option value="0'+i+'">0'+i+'</option>');
                                        } else {
                                            out.println('<option value="'+i+'">'+i+'</option>');
                                        }
                                    }
                                }%
                                </select> :
                                <select id="input_rcvMinute3" name="input_rcvMinute3" style="width: 60px" required="">
                                %{
                                    out.println('<option value=null>Menit</option>');
                                    for (int i=0;i<60;i++){
                                        if(i<10){
                                            out.println('<option value="0'+i+'">0'+i+'</option>');
                                        } else {
                                            out.println('<option value="'+i+'">'+i+'</option>');
                                        }
                                    }
                                }%
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Status Mobil</td>
                        <td colspan="2">
                            <div class="controls">
                                <g:radioGroup name="statusMobil" values="['W','L']" value="${ASBInstance?.t351StaOkCancelReSchedule}" labels="['Ditunggu','Ditinggal']">
                                    ${it.radio} <g:message code="${it.label}" />
                                </g:radioGroup>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 40%;border: 0px">
                            <label class="control-label" for="lbl_penyerahan3">
                                Tgl/Jam Janji Penyerahan <span>*</span>
                            </label>
                        </td>
                        <td><g:checkBox name="checkJanjiPenyerahan3" class="checkJanji3"/></td>
                        <td>
                            <div id="lbl_penyerahan3" class="controls">
                                <ba:datePicker id="input_penyerahan3" name="input_penyerahan3" precision="day" format="dd-MM-yyyy"  value=""  />
                                <select id="input_penyerahanHour3" name="input_penyerahanHour3" style="width: 60px" required="">
                                %{
                                    out.println('<option value=null>Jam</option>');
                                    for (int i=0;i<24;i++){
                                        if(i<10){
                                            out.println('<option value="0'+i+'">0'+i+'</option>');
                                        } else {
                                            out.println('<option value="'+i+'">'+i+'</option>');
                                        }
                                    }
                                }%
                                </select> :
                                <select id="input_penyerahanMinute3" name="input_penyerahanMinute3" style="width: 60px" required="">
                                %{
                                    out.println('<option value=null>Menit</option>');
                                    for (int i=0;i<60;i++){
                                        if(i<10){
                                            out.println('<option value="0'+i+'">0'+i+'</option>');
                                        } else {
                                            out.println('<option value="'+i+'">'+i+'</option>');
                                        }
                                    }
                                }%
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button class="btn btn-primary" id="buttonSearch" onclick="dss3();">Search</button><button class="btn btn-primary" id="buttonSave" onclick="saveASB3();">Save</button></td>
                    </tr>
                </table>
            </div>

            <div id="searchView" class="box">
                <legend style="font-size: small">View ASB</legend>
                <table style="width: 100%;border: 0px">
                    <tr>
                        <td>
                            <label class="control-label" for="lbl_rcvDate">
                                Nama Workshop
                            </label>
                        </td>
                        <td>
                            <div id="lbl_companyDealer" class="controls">
                                <g:select id="input_companyDealer" disabled="" name="companyDealer.id" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop")}}" optionKey="id" required="" value="${session?.userCompanyDealer?.id}" class="many-to-one"/>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="control-label" for="lbl_tglView3Hari">
                                Tanggal
                            </label>
                        </td>
                        <td>
                            <div id="lbl_tglView3Hari" class="controls">
                                <div id="filter_tglView3Hari" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                                     <input type="hidden" name="search_tglView3Hari" value="date.struct">
                                     <input type="hidden" name="search_tglView3Hari_day" id="search_tglView3Hari_day" value="">
                                     <input type="hidden" name="search_tglView3Hari_month" id="search_tglView3Hari_month" value="">
                                     <input type="hidden" name="search_tglView3Hari_year" id="search_tglView3Hari_year" value="">
                                     <input type="text" data-date-format="dd/mm/yyyy" name="search_tglView3Hari_dp" value="" id="search_tglView3Hari" class="search_init">
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button class="btn btn-primary" id="buttonView3Hari" onclick="asbView3Hari()">View</button>
                        </td>
                    </tr>

                </table>
            </div>
        </div>

        <div class="span8 row-fluid">
            <div id="day1">
                <g:render template="dataTablesDay1" />
            </div>
            <div id="day2">
                <g:render template="dataTablesDay2" />
            </div>
            <div id="day3">
                <g:render template="dataTablesDay3" />
            </div>
        </div>

	</div>
</body>
</html>
