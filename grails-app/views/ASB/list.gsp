
<%@ page import="com.kombos.board.ASB" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'ASB.label', default: 'ASB GR')}" />
		<r:require modules="baseapplayout, baseapplist, bootstraptooltip" />
        <link rel="stylesheet" type="text/css" href="css/baseapp.css" />
		<g:javascript>
			var show;
			var edit;

			$(function(){
			    $('#asb_per_hari').load('${request.contextPath}/ASB/perDay?idApp='+$('#idApp').val());

                $('#tab_asb_per_hari').click(function() {
                    $('#asb_per_hari').load('${request.contextPath}/ASB/perDay?idApp='+$('#idApp').val());
                });

                $('#tab_asb_per_3hari').click(function() {
                    $('#asb_per_3hari').load('${request.contextPath}/ASB/perThreeDays?idApp='+$('#idApp').val());
                });

                 var reloadAll = function(){
                    var table = $('#ASB_datatables');
                    if(table.length > 0){
                        asbView();
                        setTimeout(function(){reloadAll()}, 10000);
                    } else {
                        var table3 = $('#ASB_datatablesDay1');
                        if(table3.length > 0){
                            asbView3Hari();
                            setTimeout(function(){reloadAll()}, 10000);
                        }
                    }
                }
//                setTimeout(function(){reloadAll()}, 10000);

            });


</g:javascript>
	</head>
	<body>
    <div class="navbar box-header no-border">
   		<span class="pull-left">Appointment Schedule Board (ASB) General Repair</span>
   	</div>

	<div class="box">
        <div class="tabbable control-group">
            <g:hiddenField name="idApp" id="idApp" value="${params.appointmentId}" />
            <ul class="nav nav-tabs">
                <li id="tab_asb_per_hari" class="active">
                    <a href="#asb_per_hari" data-toggle="tab">Per Hari</a>
                </li>
                <li id="tab_asb_per_3hari" class="">
                    <a href="#asb_per_3hari" data-toggle="tab">3 Hari</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="asb_per_hari" />

                <div class="tab-pane" id="asb_per_3hari" />

            </div>
        </div>
	</div>
</body>
</html>
