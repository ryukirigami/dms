
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>


<table id="kendaraanSedangCuci_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>


        <th style="border-bottom: none;padding: 5px;">
            <div>No. Antrian</div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div>No. Polisi</div>
        </th>


        <th style="border-bottom: none;padding: 5px;width: 40%;">
            <div>Stall Cuci</div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div>Washer</div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div>SA</div>
        </th>



    </tr>

    </thead>
</table>

<g:javascript>
var kendaraanSedangCuciTable;
var reloadKendaraanSedangCuciTable;

$(function(){

	reloadKendaraanSedangCuciTable = function() {
		kendaraanSedangCuciTable.fnDraw();
	}

	kendaraanSedangCuciTable = $('#kendaraanSedangCuci_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "kendaraanSedangCuci")}",
		"aoColumns": [




{
	"sName": "noAntri",
	"mDataProp": "noAntri",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input onclick="pilihAntrian(this.value);" name="customer" value="'+row['id']+'" type="radio" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"/>&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}

,

{
	"sName": "nopol",
	"mDataProp": "nopol",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "stall",
	"mDataProp": "stall",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "washer",
	"mDataProp": "washer",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "sa",
	"mDataProp": "sa",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {



$.ajax({ "dataType": 'json',
    "type": "POST",
    "url": sSource,
    "data": aoData ,
    "success": function (json) {
        fnCallback(json);
       },
    "complete": function () {
       }
});
}
});
});
</g:javascript>



