<%--
  Created by IntelliJ IDEA.
  User: Mohammad Fauzi
  Date: 4/22/14
  Time: 1:23 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title>Board Antrian Cuci</title>
    <r:require modules="baseapplayout"/>
</head>

<body>

<div class="box">
    <legend style="font-size: small">Kendaraan Selesai Cuci</legend>
    <g:render template="kendaraanSelesaiCuci"/>
</div>

</body>
</html>