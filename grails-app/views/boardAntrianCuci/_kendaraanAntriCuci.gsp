
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>
<table id="kendaraanAntriCuci_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>


        <th style="border-bottom: none;padding: 5px;">
            <div>No. Antrian</div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div>No. Polisi</div>
        </th>


        <th style="border-bottom: none;padding: 5px;width: 40%;">
            <div>Stall Parkir</div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div>Waktu Serah</div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div>SA</div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div>SKIP</div>
        </th>



    </tr>

    </thead>
</table>

<g:javascript>
function skip(id){
   $.ajax({
            type:'POST',
            url:'${request.contextPath}/boardAntrianCuci/skipkendaraancuci/',data: { id: id },
            	success:function(data,textStatus){

   			},

	error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
//   				$('#spinner').fadeOut();

   			   reloadKendaraanAntriCuciTable()
                reloadKendaraanSedangCuciTable()
   			}
        });

}

var kendaraanAntriCuciTable;
var reloadKendaraanAntriCuciTable;
$(function(){

	reloadKendaraanAntriCuciTable = function() {
		kendaraanAntriCuciTable.fnDraw();
	}

	kendaraanAntriCuciTable = $('#kendaraanAntriCuci_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "kendaraanAntriCuci")}",
		"aoColumns": [

{
	"sName": "noAntri",
	"mDataProp": "noAntri",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input onclick="pilihAntrian(this.value);" name="customer" value="'+row['id']+'" type="radio" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"/>&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"80px",
	"bVisible": true
}

,

{
	"sName": "nopol",
	"mDataProp": "nopol",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "stall",
	"mDataProp": "stall",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "waktuSerah",
	"mDataProp": "waktuSerah",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "sa",
	"mDataProp": "sa",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "id",
	"mDataProp": "id",
	"mRender": function ( data, type, row ) {
        return '<input  onclick="skip('+row['id']+')" class="inline-edit"  type="button"  style="width:40px;" value="SKIP">';
    },
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"80px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {


$.ajax({ "dataType": 'json',
    "type": "POST",
    "url": sSource,
    "data": aoData ,
    "success": function (json) {
        fnCallback(json);
       },
    "complete": function () {
       }
});
}
});
});
</g:javascript>