
<r:require modules="baseapplayout" />

<g:render template="../menu/maxLineDisplay"/>

<table id="dataTerhubung_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover" style="table-layout: fixed; width: 870px"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="WO.jobOnly.label" default="Pertanyaan" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="WO.jobOnly.label" default="Jawaban Customer" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="WO.teknisi.label" default="Alasan Customer" /></div>
        </th>


    </tr>
    </thead>
</table>

<g:javascript>
    var dataTerhubungTable;
    var reloadDataTerhubungTable;
    $(function(){

        reloadDataTerhubungTable = function() {
            dataTerhubungTable.fnClearTable();
            dataTerhubungTable.fnDraw();
        }

        dataTerhubungTable = $('#dataTerhubung_datatables').dataTable({
            "sScrollX": "100%",
            "bScrollCollapse": true,
            "bAutoWidth" : false,
            "bPaginate" : false,
            "sInfo" : "",
            "sInfoEmpty" : "",
            "sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
            bFilter: true,
            "bStateSave": false,
            'sPaginationType': 'bootstrap',
            "fnInitComplete": function () {
                this.fnAdjustColumnSizing(true);
            },
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                return nRow;
            },
            "bSort": true,
            "bProcessing": true,
            "bServerSide": true,
            "bInfo" : false,
            "sServerMethod": "POST",
            "iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
            "sAjaxSource": "${g.createLink(action: "getData")}",
            "bDestroy": true,
            "aoColumns": [

                {
                    "sName": "pertanyaanFu",
                    "mDataProp": "pertanyaan",
                    "aTargets": [0],
                    "bSearchable": true,
                    "mRender": function ( data, type, row ) {
                        return '<input type="hidden" value="'+row['idFU']+'">&nbsp;'+data;
                    },
                    "bSortable": false,
                    "sWidth":"240px",
                    "bVisible": true
                }

                ,

                {
                    "sName": "t803StaPuasTdkPuas",
                    "mDataProp": "staPuas",
                    "aTargets": [1],
                    "bSearchable": true,
                    "mRender": function ( data, type, row ) {

                        return ' &nbsp; <input id="jwb-'+row['id']+'" name="jawaban-'+row['id']+'" class="inline-edit" type="radio" ' + (data==1?"checked":"") +' value="1" > &nbsp; Ya &nbsp; &nbsp; &nbsp;' +
                         ' <input id="jwb-'+row['id']+'" name="jawaban-'+row['id']+'" class="inline-edit" type="radio" ' + (data==0?"checked":"") +'  value="0" > &nbsp;  Tidak';
                    },
                    "bSortable": false,
                    "sWidth":"140px",
                    "bVisible": true
                }
                ,

                {
                    "sName": "t803AlasanTdkPuas",
                    "mDataProp": "alasan",
                    "aTargets": [2],
                    "bSearchable": true,
                    "mRender": function ( data, type, row ) {
                        return '<input id="qty-'+row['id']+'" class="inline-edit" type="text" style="width:300px;" value="'+data+'">';
                    },
                    "bSortable": false,
                    "sWidth":"250px",
                    "bVisible": true
                }


            ],
            "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        var id = $('#id').val();
						if(id){
							aoData.push(
									{"name": 'id', "value": id}
							);
						}

                $.ajax({ "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData ,
                    "success": function (json) {
                        fnCallback(json);
                       },
                    "complete": function () {
                       }
                });
            }

        });

    });
</g:javascript>

