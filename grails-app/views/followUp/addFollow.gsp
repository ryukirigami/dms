
<%@ page import="com.kombos.administrasi.Operation; com.kombos.administrasi.NamaProsesBP; com.kombos.parts.Returns" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'returnsAdd.label', default: 'Ubah Data')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
        return false;
	});


   });
    $(document).ready(function()
    {

        save = function(){
                var metodeSms,nomorValid,statusFollowUp;

                metodeSms = $('input:radio[name=metodeSms]:nth(0)').is(':checked')?'1':'2';
                nomorValid = $('input:radio[name=nomorValid]:nth(0)').is(':checked')?'1':'0';
                statusFollowUp = $('input:radio[name=statusFU]:nth(0)').is(':checked')?'1':'0';
                var id = $('#id').val();
                $.ajax({
                    url:'${request.contextPath}/followUp/editFollow',
                    type: "POST", // Always use POST when deleting data
                    data : {id:id,metodeSms: metodeSms,nomorValid:nomorValid,statusFollowUp:statusFollowUp},
                    success : function(data){
                        toastr.success('<div>Penyimpanan Sukses</div>');
                        reloadFollowUpTable();
                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });

        }
    });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
    <ul class="nav pull-right">
        <li></li>
        <li></li>
        <li class="separator"></li>
    </ul>
</div>
<div class="box">
    <div class="span12" id="follow-table">
        <fieldset style="align-content: center">
            <legend style="font-size: 15px">Switch Metode Follow Up</legend>
            <g:hiddenField name="id" value="${id}" />
            <table style="align-content: center">
                <tr>
                    <td style="width: 50%">Metode SMS</td>
                    <td style="width: 50%; padding-left: 20px"> &nbsp;
                    %{--Perubahan Disini--}%
                    <g:radioGroup name="metodeSms" id="metodeSms" values="['1','2']" labels="['SMS','Telepon']" value="${metodeSms}" required="" >
                        ${it.radio} ${it.label}
                    </g:radioGroup>
                    </td>
                </tr>
            </table>
        </fieldset> <br> <br>
        <fieldset>
            <legend style="font-size: 15px">Validasi Nomor HP</legend>
            <table style="align-content: center">
                <tr>
                    <td style="width: 50%">Nomor Valid? </td>
                    <td style="width: 50%; padding-left: 20px"> &nbsp;
                    <g:radioGroup name="nomorValid" id="nomorValid" values="['1','0']" labels="['Ya','Tidak']" value="${nomorValid}" required="" >
                        ${it.radio} ${it.label}
                    </g:radioGroup>
                    </td>
                </tr>
            </table>
        </fieldset> <br> <br>
        <fieldset>
            <legend style="font-size: 15px">Status Follow UP</legend>
            <table style="align-content: center">
                <tr>
                    <td style="width: 50%">Berhasil? </td>
                    <td style="width: 50%; padding-left: 20px"> &nbsp;
                    <g:radioGroup name="statusFU" id="statusFU" values="['1','0']" labels="['Ya','Tidak']" value="${statusFollowUp}" required="" >
                        ${it.radio} ${it.label}
                    </g:radioGroup>
                    </td>
                </tr>
            </table>
        </fieldset><br><br><br>
        <table>
            <tr>
                <td> &nbsp;</td>
                <td> <button style="width: 60px;height: 30px; border-radius: 5px" type="button" class="btn btn-primary" onclick="save()" >Update</button>
                    <button id='btn2' type="button" class="btn btn-cancel" style="width: 60px;">Cancel</button>
            </tr>
        </table>
    </div>
</div>
</body>
</html>

