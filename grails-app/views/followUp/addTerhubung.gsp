
<%@ page import="com.kombos.administrasi.Operation; com.kombos.administrasi.NamaProsesBP; com.kombos.parts.Returns" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'returnsAdd.label', default: 'Status Terhubung Customer')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
        return false;
	});


   });
   $(document).ready(function()
    {
//          $('input:radio[name=terhubung]:nth(1)').attr('checked',true);
//          $("#tr-terhubung").show();
//          if($('input:radio[name=terhubung]:nth(0)').is(':checked')){
//            $("#tr-terhubung").hide();
//          }
//          $('input:radio[name=terhubung]').click(function(){
//             if($('input:radio[name=terhubung]:nth(0)').is(':checked')){
//                $("#tr-terhubung").hide();
//             }
//         });

        save = function(){
                    var id = $('#id').val();
                var terhubung;
                var alasan = new Array();
                var rows = dataTerhubungTable.fnGetNodes();
                console.log(rows.length);
                for(var i=0;i<rows.length;i++)
                {
                    alasan[i] = new Array()
                    var aData = dataTerhubungTable.fnGetData(i);
                    var qtyInput = $('#qty-' + aData['id']).val();
                    var jawaban = $('input:radio[name=jawaban-'+ aData['id']+']:nth(0)').is(':checked')?'1':'0';
                    alasan[i][0] = aData['id'];
                    alasan[i][1] = aData['idFU'];
                    alasan[i][2] = jawaban;
                    alasan[i][3] = qtyInput;
                }
//                    console.log(alasan);
                jsonArray = JSON.stringify(alasan);
                terhubung = $('input:radio[name=terhubung]:nth(0)').is(':checked')?'1':'0';
                $.ajax({
                    url:'${request.contextPath}/followUp/editTerhubung',
                    type: "POST", // Always use POST when deleting data
                    data : {id:id,alasan:jsonArray,terhubung: terhubung},
                    success : function(data){
                        toastr.success('<div>Sukses</div>')
                        reloadFollowUpTable();
                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });

        }
    });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
    <ul class="nav pull-right">
        <li></li>
        <li></li>
        <li class="separator"></li>
    </ul>
</div>
<div class="box">
    <div class="span12" id="sms-table">
        <table style="width: 900px">
            <tr>
                <td style="width: 50%">Terhubung ke Customer ? </td>
                <td style="width: 50%; padding-left: 20px">
                    <g:radioGroup name="terhubung" id="terhubung" values="['1','0']" labels="['Ya','Tidak']" value="${staTerhubung}"  required="" >
                        ${it.radio} ${it.label}
                    </g:radioGroup>
                </td>
            </tr>
            <tr id="tr-terhubung">
                <td colspan="2">
                    <g:hiddenField name="id" id="id" value="${id}" />
                    <g:render template="dataTablesDataTerhubung" />
                </td>
            </tr>
            <tr>
                <td> &nbsp;</td>
                <td> <button style="width: 60px;height: 30px; border-radius: 5px" type="button" class="btn btn-primary" onclick="save()" >Save</button>
                    <button id='btn2' type="button" class="btn btn-cancel" style="width: 60px;">Close</button>
                </td>
            </tr>
        </table>
    </div>
</div>
</body>
</html>

