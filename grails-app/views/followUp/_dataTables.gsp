
<%@ page import="com.kombos.parts.StokOPName" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>


<table id="followUp_datatables_${idTable}" cellpadding="0" cellspacing="0" border="0"
       class="display table table-striped table-bordered table-hover" style="table-layout: fixed; ">
    <thead>
    <tr>
        <th style="border-bottom: none;padding: 5px;">
            <g:message code="followUpss.label" default="Tanggal Plan"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="followUpss.label" default="Tanggal Action"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="followUp.tanggalWO.label" default="Waktu / Metode"/>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <g:message code="smsHistory.tanggalWO.label" default="Status Follow Up"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="followUp.teknisi.label" default="Nomor Polisi"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="followUp.stop.label" default="Nama Customer"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="followUp.finalInspection.label" default="Nomor HP"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="followUp.noTambahan.label" default="Nomor Tambahan"/>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <g:message code="followUp.start.label" default="Inisial SA"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="followUp.sa.label" default="Model Kendaraan"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="followUp.teknisi.label" default="Nomor WO"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="followUp.sa.label" default="Tanggal Service"/>
        </th>

        <th style="border-bottom: none;padding: 10px;">
            <g:message code="followUp.sa.label" default="Keterangan"/>
        </th>

    </tr>
    %{--<tr>--}%
        %{--<th style="border-top: none;padding: 5px;">--}%
            %{--<div id="filter_inisialSaAction" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
                %{--<input type="text" name="search_inisialSaAction" class="search_init" />--}%
            %{--</div>--}%
        %{--</th>--}%
    %{--</tr>--}%

    </thead>
</table>

<g:javascript>
var followUpTable;
var reloadFollowUpTable;
var oneChecked;
$(function(){
 	$('#view').click(function(e){
        followUpTable.fnDraw();
	});
	$('#clear').click(function(e){
	    $('#search_TanggalPlan').val('');
	    $('#search_TanggalPlanAkhir').val('');
        $('#search_TanggalAction').val('');
	    $('#search_TanggalActionAkhir').val('');
	    $('#kategoriWaktuFu').val('');
	    $('#metodeFu').val('');
	    $('#inisialSaPlan').val('');
	    $('#statusSms').val('');
	    $('#inisialSaAction').val('');
	    $('#statusFollowUp').val('');
        followUpTable.fnDraw();
	});

    oneChecked = function(comp){

       $(".row-select").prop("checked", false);
        $(comp).prop("checked", true);
    }
	reloadFollowUpTable = function() {

		followUpTable.fnDraw();
	}


    followUpTable = $('#followUp_datatables_${idTable}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
		   $(nRow).children().each(function(index, td) {
                    if(index == 3){
                        if ($(td).html() == "Belum") {
                                $(td).css("background-color", "yellow");
                        }
                    }
		   });

			    return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"bDestroy": true,
		"aoColumns": [
 {
	"sName": "t802TglJamFU",
	"mDataProp": "tanggalPlan",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" onchange="oneChecked(this)" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'"> &nbsp; <a href="javascript:void(0);" onclick="ubah('+row['id']+')">'+data+'</a>';
	},
	"bSortable": false,
	"sWidth":"130px",
	"bVisible": true
}
,
{
	"sName": "t802NamaSA_FU",
	"mDataProp": "tanggalAction",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"105px",
	"bVisible": true
}
,
{
	"sName": "followUp",
	"mDataProp": "waktu",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"110px",
	"bVisible": true
},
{
	"sName": "followUp",
	"mDataProp": "statusFollowUp",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"130px",
	"bVisible": true
}
,
{
	"sName": "followUp",
	"mDataProp": "noPol",
	"aTargets": [4],
	"bSortable": false,
	"sWidth":"90px",
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "followUp",
	"mDataProp": "namaCustomer",
	"aTargets": [5],
	"bSortable": false,
	"sWidth":"135px",
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "followUp",
	"mDataProp": "noHp",
	"aTargets": [6],
	"sWidth":"110px",
	"bSortable": false,
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "followUp",
	"mDataProp": "cpTambahan",
	"aTargets": [7],
	"sWidth":"200px",
	"bSortable": false,
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "followUp",
	"mDataProp": "inisialSA",
	"aTargets": [8],
	"sWidth":"100px",
	"bSortable": false,
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "followUp",
	"mDataProp": "model",
	"aTargets": [9],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "followUp",
	"mDataProp": "nomorWo",
	"aTargets": [10],
	"sWidth":"100px",
	"bSortable": false,
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "followUp",
	"mDataProp": "tanggalService",
	"aTargets": [11],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}

,


{
	"sName": "followUp",
	"mDataProp": "keterangn",
	"aTargets": [12],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}

],
    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
    
                        var Tanggal = $('#search_TanggalPlan').val();
						var TanggalDay = $('#search_TanggalPlan_day').val();
						var TanggalMonth = $('#search_TanggalPlan_month').val();
						var TanggalYear = $('#search_TanggalPlan_year').val();

	                    var Tanggalakhir = $('#search_TanggalPlanAkhir').val();
                        var TanggalDayakhir = $('#search_TanggalPlanAkhir_day').val();
                        var TanggalMonthakhir = $('#search_TanggalPlanAkhir_month').val();
                        var TanggalYearakhir = $('#search_TanggalPlanAkhir_year').val();


                        if(Tanggal){
                            aoData.push(
                                    {"name": 'search_TanggalPlan', "value": "date.struct"},
                                    {"name": 'search_TanggalPlan_dp', "value": Tanggal},
                                    {"name": 'search_TanggalPlan_day', "value": TanggalDay},
                                    {"name": 'search_TanggalPlan_month', "value": TanggalMonth},
                                    {"name": 'search_TanggalPlan_year', "value": TanggalYear}
                            );
                        }

                        if(Tanggalakhir){
                            aoData.push(
                                    {"name": 'search_TanggalPlanAkhir', "value": "date.struct"},
                                    {"name": 'search_TanggalPlanAkhir_dp', "value": Tanggalakhir},
                                    {"name": 'search_TanggalPlanAkhir_day', "value": TanggalDayakhir},
                                    {"name": 'search_TanggalPlanAkhir_month', "value":TanggalMonthakhir},
                                    {"name": 'search_TanggalPlanAkhir_year', "value": TanggalYearakhir}
                            );
                        }

                        var Tanggal2 = $('#search_TanggalAction').val();
						var Tanggal2Day = $('#search_TanggalAction_day').val();
						var Tanggal2Month = $('#search_TanggalAction_month').val();
						var Tanggal2Year = $('#search_TanggalAction_year').val();

	                    var Tanggal2akhir = $('#search_TanggalActionAkhir').val();
                        var Tanggal2Dayakhir = $('#search_TanggalActionAkhir_day').val();
                        var Tanggal2Monthakhir = $('#search_TanggalActionAkhir_month').val();
                        var Tanggal2Yearakhir = $('#search_TanggalActionAkhir_year').val();


                        if(Tanggal2){
                            aoData.push(
                                    {"name": 'search_TanggalAction', "value": "date.struct"},
                                    {"name": 'search_TanggalAction_dp', "value": Tanggal2},
                                    {"name": 'search_TanggalAction_day', "value": Tanggal2Day},
                                    {"name": 'search_TanggalAction_month', "value": Tanggal2Month},
                                    {"name": 'search_TanggalAction_year', "value": Tanggal2Year}
                            );
                        }

                        if(Tanggal2akhir){
                            aoData.push(
                                    {"name": 'search_TanggalActionAkhir', "value": "date.struct"},
                                    {"name": 'search_TanggalActionAkhir_dp', "value": Tanggal2akhir},
                                    {"name": 'search_TanggalActionAkhir_day', "value": Tanggal2Dayakhir},
                                    {"name": 'search_TanggalActionAkhir_month', "value":Tanggal2Monthakhir},
                                    {"name": 'search_TanggalActionAkhir_year', "value": Tanggal2Yearakhir}
                            );
                        }


						var statusFollowUp = $('#statusFollowUp').val();
						if(statusFollowUp){
							aoData.push(
									{"name": 'statusFollowUp', "value": statusFollowUp}
							);
						}

						var statusSms = $('#statusSms').val();
						if(statusSms){
							aoData.push(
									{"name": 'statusSms', "value": statusSms}
							);
						}

						var metodeFu = $('#metodeFu').val();
						if(metodeFu){
							aoData.push(
									{"name": 'metodeFu', "value": metodeFu}
							);
						}

						var kategoriWaktuFu = $('#kategoriWaktuFu').val();
						if(kategoriWaktuFu){
							aoData.push(
									{"name": 'kategoriWaktuFu', "value": kategoriWaktuFu}
							);
						}

						var inisialSaAction = $('#inisialSaAction').val();
						if(inisialSaAction){
							aoData.push(
									{"name": 'inisialSaAction', "value": inisialSaAction}
							);
						}

//						var inisialSaAction = $('#filter_inisialSaAction input').val();
//						if(inisialSaAction){
//							aoData.push(
//									{"name": 'sCriteria_inisialSaAction', "value": inisialSaAction}
//							);
//						}


						var inisialSaPlan = $('#inisialSaPlan').val();
						if(inisialSaPlan){
							aoData.push(
									{"name": 'inisialSaPlan', "value": inisialSaPlan}
							);
						}


    $.ajax({ "dataType": 'json',
        "type": "POST",
        "url": sSource,
        "data": aoData ,
        "success": function (json) {
            fnCallback(json);
           },
        "complete": function () {
           }
    });
}
});
});


</g:javascript>
