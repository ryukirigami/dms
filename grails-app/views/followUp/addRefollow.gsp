
<%@ page import="com.kombos.administrasi.Operation; com.kombos.administrasi.NamaProsesBP; com.kombos.parts.Returns" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'returnsAdd.label', default: 'Re Follow Up')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
        return false;
	});


   });
    $(document).ready(function()
    {
         $('input:radio[name=followUpUlang]:nth(1)').attr('checked',true);
         if($('input:radio[name=followUpUlang]:nth(0)').is(':checked')){
             $('input:radio[name=metodeFu]:nth(0)').prop('disabled', false);
             $('input:radio[name=metodeFu]:nth(1)').prop('disabled', false);
             $('input:radio[name=kategoriWaktuFu]:nth(0)').prop('disabled', false);
             $('input:radio[name=kategoriWaktuFu]:nth(1)').prop('disabled', false);
             $('#tanggal').prop('disabled', false);
             $('#ket').prop('disabled', false);
         }else{
             $('input:radio[name=metodeFu]:nth(0)').prop('disabled', true);
             $('input:radio[name=metodeFu]:nth(1)').prop('disabled', true);
             $('input:radio[name=kategoriWaktuFu]:nth(0)').prop('disabled', true);
             $('input:radio[name=kategoriWaktuFu]:nth(1)').prop('disabled', true);
             $('input:radio[name=kategoriWaktuFu]:nth(2)').prop('disabled', true);
             $('#tanggal').prop('disabled', true);
             $('#ket').prop('disabled', true);
         }

         $('input:radio[name=followUpUlang]').click(function(){
             if($('input:radio[name=followUpUlang]:nth(0)').is(':checked')){
                 $('input:radio[name=metodeFu]:nth(0)').prop('disabled', false);
                 $('input:radio[name=metodeFu]:nth(1)').prop('disabled', false);
                 $('input:radio[name=kategoriWaktuFu]:nth(0)').prop('disabled', false);
                 $('input:radio[name=kategoriWaktuFu]:nth(1)').prop('disabled', false);
                 $('input:radio[name=kategoriWaktuFu]:nth(2)').prop('disabled', false);
                 $('#tanggal').prop('disabled', false);
                 $('#ket').prop('disabled', false);
             }else{
                 $('input:radio[name=metodeFu]:nth(0)').prop('disabled', true);
                 $('input:radio[name=metodeFu]:nth(1)').prop('disabled', true);
                 $('input:radio[name=kategoriWaktuFu]:nth(0)').prop('disabled', true);
                 $('input:radio[name=kategoriWaktuFu]:nth(1)').prop('disabled', true);
                 $('input:radio[name=kategoriWaktuFu]:nth(2)').prop('disabled', true);
                 $('#tanggal').prop('disabled', true);
                 $('#ket').prop('disabled', true);
             }
         });

        save = function(){
                var id = $('#id').val();
                var metodeFu,kategoriWaktuFu,ket,tanggal;

                metodeFu = $('input:radio[name=metodeFu]:nth(0)').is(':checked')?'1':'0';
                if($('input:radio[name=kategoriWaktuFu]:nth(0)').is(':checked')){
                    kategoriWaktuFu = "1"
                }else if($('input:radio[name=kategoriWaktuFu]:nth(1)').is(':checked')){
                    kategoriWaktuFu = "2"
                }else{
                    kategoriWaktuFu = "3"
                }
                ket = $('#ket').val();
                tanggal = $('#tanggal').val();
                $.ajax({
                    url:'${request.contextPath}/followUp/editReFollow',
                    type: "POST", // Always use POST when deleting data
                    data : {id:id,metodeFu: metodeFu,kategoriWaktuFu:kategoriWaktuFu,ket:ket,tanggal:tanggal},
                    success : function(data){
                        toastr.success('<div>Penyimpanan Sukses</div>');
                        reloadFollowUpTable();
                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });

        }
    });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
    <ul class="nav pull-right">
        <li></li>
        <li></li>
        <li class="separator"></li>
    </ul>
</div>
<div class="box">
    <div class="span12" id="sms-table">
        <g:hiddenField name="id" value="${id}" />
        <table>
            <tr>
                <td>Ingin Follow Up Ulang</td>
                <td style="width: 280px;padding-left: 20px"> &nbsp;
                <g:radioGroup name="followUpUlang" id="followUpUlang" values="['1','0']" labels="['Ya','Tidak']" required="" >
                    ${it.radio} ${it.label}
                </g:radioGroup>
                </td>
                <td >Metode Follow Up</td>
                <td  style="padding-left: 20px"> &nbsp;
                <g:radioGroup name="metodeFu" id="metodeFu" values="['1','3']" labels="['SMS','Telp']" value="${metodeSms}" required="" >
                    ${it.radio} ${it.label}
                </g:radioGroup>
                </td>
                <td  style="padding-left: 60px" rowspan="2">Ket Tambahan</td>
                <td rowspan="2"> &nbsp;
                    <g:textArea name="ket" id="ket" cols="20" rows="4" value="-" />
                </td>
            </tr>
            <tr>
                <td>Tanggal Next Follow Up</td>
                <td> &nbsp;
                    <ba:datePicker id="tanggal" name="tanggal" precision="day" format="yyyy-MM-dd"  value="" required="true"  />
                </td>
                <td>Waktu Follow Up</td>
                <td style="padding-left: 20px"> &nbsp;
                <g:radioGroup name="kategoriWaktuFu" id="kategoriWaktuFu" values="['1','2','3']" labels="['Pagi','Siang','Sore']" value="${kategoriWaktuFu}" required="" >
                    ${it.radio} ${it.label}
                </g:radioGroup>
                </td>
            </tr>
            <tr>
                <td> &nbsp;</td>
                <td> <button style="width: 80px;height: 30px; border-radius: 5px" type="button" class="btn btn-primary" onclick="save()" >Update</button>
                    <button id='btn2' type="button" class="btn btn-cancel" style="width: 80px;">Close</button>
                </td>
            </tr>
        </table>
    </div>
</div>
</body>
</html>

