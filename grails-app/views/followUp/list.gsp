
<%@ page import="com.kombos.maintable.KategoriWaktuFu; com.kombos.customerFollowUp.MetodeFu; com.kombos.administrasi.KodeKotaNoPol"%>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'followUp.label', default: 'Follow Up')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>
        $(function(){
        $("#followUpAddModal").on("show", function() {
		$("#followUpAddModal .btn").on("click", function(e) {
			$("#followUpAddModal").modal('hide');
		});
        });
        $("#followUpAddModal").on("hide", function() {
            $("#followUpAddModal a.btn").off("click");
        });

        ubah = function(id){
            $("#followUpAddContent").empty();
            expandTableLayout();
            $.ajax({type:'POST', url:'${request.contextPath}/followUp/addFollow',
            data: { id: id},
                success:function(data,textStatus){
                        $("#followUpAddContent").html(data);
                        $("#followUpAddModal").modal({
                            "backdrop" : "static",
                            "keyboard" : true,
                            "show" : true
                        }).css({'width': '400px','margin-left': function () {return -($(this).width() / 2);}});

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
        }

        clickReFollow = function(){
            var id = ""
            $("#followUp-table tbody .row-select").each(function() {
                 if(this.checked){
                    id = $(this).next("input:hidden").val();
                    $("#followUpAddContent").empty();
                    expandTableLayout();
                    $.ajax({type:'POST', url:'${request.contextPath}/followUp/addReFollow',
                    data: { id: id},
                        success:function(data,textStatus){
                                $("#followUpAddContent").html(data);
                                $("#followUpAddModal").modal({
                                    "backdrop" : "static",
                                    "keyboard" : true,
                                    "show" : true
                                }).css({'width': '1240px','margin-left': function () {return -($(this).width() / 2);}});

                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
                  }
              });
        }

        clickTerhubung = function(){
            var id = ""
            $("#followUp-table tbody .row-select").each(function() {
                 if(this.checked){
                    id = $(this).next("input:hidden").val();
                    $("#followUpAddContent").empty();
                    expandTableLayout();
                    $.ajax({type:'POST', url:'${request.contextPath}/followUp/addTerhubung',
                    data: { id: id},
                        success:function(data,textStatus){
                                $("#followUpAddContent").html(data);
                                $("#followUpAddModal").modal({
                                    "backdrop" : "static",
                                    "keyboard" : true,
                                    "show" : true
                                }).css({'width': '930px','margin-left': function () {return -($(this).width() / 2);}});

                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
                  }
              });
        }
            clickAppoinment = function(){
                $.ajax({
                    url:'${request.contextPath}/appointment/index',
                    type: "GET",
                    dataType: "html",
                    complete : function (req, err) {
                        $('#main-content').html(req.responseText);
                        $('#spinner').fadeOut();
                        loading = false;
                    }
                });
                  window.location.replace('#/appointment');
            }
            clickServiceHistory = function(){
                $.ajax({
                    url:'${request.contextPath}/serviceHistory/list',
                    type: "GET",
                    dataType: "html",
                    complete : function (req, err) {
                        $('#main-content').html(req.responseText);
                        $('#spinner').fadeOut();
                        loading = false;
                    }
                });
                window.location.replace('#/serviceHistory');
            }
        });
                loadForm = function(data, textStatus){
                    $('#followUp-form').empty();
                    $('#followUp-form').append(data);
                }
                shrinkTableLayout = function(){
                    $("#followUp-table").hide();
                    $("#followUp-form").css("display","block");
                }

                expandTableLayout = function(){
                    $("#followUp-table").show();
                    $("#followUp-form").css("display","none");
                }


    var checkin = $('#search_TanggalPlan').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
        var newDate = new Date(ev.date)
        newDate.setDate(newDate.getDate() + 1);
        checkout.setValue(newDate);
        checkin.hide();
        $('#search_TanggalPlanAkhir')[0].focus();
    }).data('datepicker');
    
    var checkout = $('#search_TanggalPlanAkhir').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

    var checkin2 = $('#search_TanggalAction').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
                var newDate2 = new Date(ev.date)
                newDate2.setDate(newDate2.getDate() + 1);
                checkout2.setValue(newDate2);
                checkin2.hide();
                $('#search_TanggalActionAkhir')[0].focus();
    }).data('datepicker');

    var checkout2 = $('#search_TanggalActionAkhir').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin2.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
                checkout2.hide();
    }).data('datepicker');


    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

        return true;
    }
                $(document).ready(function() {
                    $("#nopol3").keyup(function(e) {
                        var isi = $(e.target).val();
                        $(e.target).val(isi.toUpperCase());
                    });


                });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="followUp.view.label" default="Follow Up" /></span>
    <ul class="nav pull-right">

    </ul>
</div>
<div class="box">
    <div class="span12" id="followUp-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>

        <table>
            <tr>
                <td style="padding: 5px">
                    <g:message code="wo.tanggalWOs.label" default="Tanggal Plan"/>
                </td>
                <td style="padding: 5px">
                    <ba:datePicker name="search_TanggalPlan" id="search_TanggalPlan" precision="day" value="" disable="true" format="dd-MM-yyyy"/>&nbsp;s.d.&nbsp;
                    <ba:datePicker name="search_TanggalPlanAkhir" id="search_TanggalPlanAkhir" precision="day" value="" disable="true" format="dd-MM-yyyy"/>
                </td>
                <td style="padding: 5px">
                    <g:message code="followUp.staJobTambah.label" default="Waktu Follow Up"/>
                </td>
                <td style="padding: 5px">
                    <g:select name="kategoriWaktuFu" id="kategoriWaktuFu" from="${KategoriWaktuFu.list()}" optionKey="id" optionValue="m804Kategori" noSelection="${['':'Semua']}" />
                </td>

                <td>
                    <div class="controls" style="right: 0">
                        <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-primary view" name="view" id="view" >Filter</button>
                        <button style="width: 70px;height: 30px; border-radius: 5px" class="btn cancel" name="clear" id="clear" >Clear</button>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    <g:message code="wo.tanggalWOs.label" default="Tanggal Action"/>
                </td>
                <td style="padding: 5px">
                    <ba:datePicker name="search_TanggalAction" id="search_TanggalAction" precision="day" value="" disable="true" format="dd-MM-yyyy"/>&nbsp;s.d.&nbsp;
                    <ba:datePicker name="search_TanggalActionAkhir" id="search_TanggalActionAkhir" precision="day" value="" disable="true" format="dd-MM-yyyy"/>
                </td>
                <td style="padding: 5px">
                    <g:message code="followUp.staCarryOver.label" default="Metode Follow Up"/>
                </td>
                <td style="padding: 5px">
                    <g:select name="metodeFu" id="metodeFu" from="${MetodeFu.createCriteria().list {eq("staDel",'0')}}" optionKey="id" noSelection="${['':'Semua']}" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    <g:message code="followUp.staFinalInspection.label" default="Status SMS"/>
                </td>
                <td style="padding: 5px">
                    <select name="statusSms" id="statusSms" style="width: 100%">
                        <option value="">Semua</option>
                        <option value="1">Sudah Dibalas</option>
                        <option value="0">Belum Dibalas</option>
                    </select>
                </td>
                <td style="padding: 5px">
                    <g:message code="followUp.staProblemFinding.label" default="SA Action"/>
                </td>

                <td style="padding: 5px">
                    %{--<g:select style="width:100%" name="inisialSaAction" id="inisialSaAction" from="${saAction}"  noSelection="${['':'Semua']}" />--}%
                    <g:select style="width:100%" name="inisialSaAction" id="inisialSaAction" from="${saAction}"  noSelection="${['':'Semua']}" optionValue="username" />
                 </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    <g:message code="followUp.staFinalInspection.label" default="Status Follow UP"/>
                </td>
                <td style="padding: 5px">
                    <select name="statusFollowUp" id="statusFollowUp" style="width: 100%">
                        <option value="">Semua</option>
                        <option value="1">Sudah</option>
                        <option value="0">Belum</option>
                    </select>
                </td>

                    &nbsp;
                </td>
            </tr>
        </table>
        <br/><br/>
        <div class="controls" style="right: 0">
            <button style="width: 120px;height: 30px; border-radius: 5px" class="btn cancel" name="ReFollowUp" id="ReFollowUp"  onclick="clickReFollow()" >Re Follow Up</button>
            <button style="width: 180px;height: 30px; border-radius: 5px" class="btn cancel" name="terhubung" id="terhubung"  onclick="clickTerhubung()" >Terhubung Customer</button>
        </div>
        <g:render template="dataTables" />
        <br/>
        <div class="controls" style="right: 0">
            <br/><br/>
            <button style="width: 120px;height: 30px; border-radius: 5px" class="btn cancel" name="appoinment" id="appoinment"  onclick="clickAppoinment()" >Appoinment</button>
            <button style="width: 120px;height: 30px; border-radius: 5px" class="btn cancel" name="serviceHistory" id="serviceHistory"  onclick="clickServiceHistory()" >Service History</button>
        </div>
    </div>

    <div class="span7" id="followUp-form" style="display: none; width: 1200px;"></div>
</div>

<div id="followUpAddModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- dialog body -->
            <div class="modal-body" style="max-height: 460px;">
                <div id="followUpAddContent"/>
                <div class="iu-content"></div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
