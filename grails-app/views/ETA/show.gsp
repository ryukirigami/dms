

<%@ page import="com.kombos.parts.ETA" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'ETA.label', default: 'ETA')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteETA;

$(function(){ 
	deleteETA=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/ETA/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadETATable();
   				expandTableLayout('ETA');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-ETA" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="ETA"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${ETAInstance?.goods}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="goods-label" class="property-label"><g:message
					code="ETA.goods.label" default="Goods" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="goods-label">
						%{--<ba:editableValue
								bean="${ETAInstance}" field="goods"
								url="${request.contextPath}/ETA/updatefield" type="text"
								title="Enter goods" onsuccess="reloadETATable();" />--}%
							
								<g:link controller="goods" action="show" id="${ETAInstance?.goods?.id}">${ETAInstance?.goods?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${ETAInstance?.po}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="po-label" class="property-label"><g:message
					code="ETA.po.label" default="Po" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="po-label">
						%{--<ba:editableValue
								bean="${ETAInstance}" field="po"
								url="${request.contextPath}/ETA/updatefield" type="text"
								title="Enter po" onsuccess="reloadETATable();" />--}%
							
								<g:link controller="PO" action="show" id="${ETAInstance?.po?.id}">${ETAInstance?.po?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${ETAInstance?.t165ETA}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t165ETA-label" class="property-label"><g:message
					code="ETA.t165ETA.label" default="T165 ETA" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t165ETA-label">
						%{--<ba:editableValue
								bean="${ETAInstance}" field="t165ETA"
								url="${request.contextPath}/ETA/updatefield" type="text"
								title="Enter t165ETA" onsuccess="reloadETATable();" />--}%
							
								<g:formatDate date="${ETAInstance?.t165ETA}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${ETAInstance?.t165Qty1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t165Qty1-label" class="property-label"><g:message
					code="ETA.t165Qty1.label" default="T165 Qty1" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t165Qty1-label">
						%{--<ba:editableValue
								bean="${ETAInstance}" field="t165Qty1"
								url="${request.contextPath}/ETA/updatefield" type="text"
								title="Enter t165Qty1" onsuccess="reloadETATable();" />--}%
							
								<g:fieldValue bean="${ETAInstance}" field="t165Qty1"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('ETA');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${ETAInstance?.id}"
					update="[success:'ETA-form',failure:'ETA-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteETA('${ETAInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
