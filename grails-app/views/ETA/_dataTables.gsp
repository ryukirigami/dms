
<%@ page import="com.kombos.parts.ETA" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="ETA_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="ETA.goods.label" default="Goods" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="ETA.po.label" default="Po" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="ETA.t165ETA.label" default="T165 ETA" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="ETA.t165Qty1.label" default="T165 Qty1" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_goods" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_goods" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_po" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_po" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t165ETA" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_t165ETA" value="date.struct">
					<input type="hidden" name="search_t165ETA_day" id="search_t165ETA_day" value="">
					<input type="hidden" name="search_t165ETA_month" id="search_t165ETA_month" value="">
					<input type="hidden" name="search_t165ETA_year" id="search_t165ETA_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t165ETA_dp" value="" id="search_t165ETA" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t165Qty1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t165Qty1" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var ETATable;
var reloadETATable;
$(function(){
	
	reloadETATable = function() {
		ETATable.fnDraw();
	}

	
	$('#search_t165ETA').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t165ETA_day').val(newDate.getDate());
			$('#search_t165ETA_month').val(newDate.getMonth()+1);
			$('#search_t165ETA_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			ETATable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	ETATable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	ETATable = $('#ETA_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "goods",
	"mDataProp": "goods",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "po",
	"mDataProp": "po",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t165ETA",
	"mDataProp": "t165ETA",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t165Qty1",
	"mDataProp": "t165Qty1",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var goods = $('#filter_goods input').val();
						if(goods){
							aoData.push(
									{"name": 'sCriteria_goods', "value": goods}
							);
						}
	
						var po = $('#filter_po input').val();
						if(po){
							aoData.push(
									{"name": 'sCriteria_po', "value": po}
							);
						}

						var t165ETA = $('#search_t165ETA').val();
						var t165ETADay = $('#search_t165ETA_day').val();
						var t165ETAMonth = $('#search_t165ETA_month').val();
						var t165ETAYear = $('#search_t165ETA_year').val();
						
						if(t165ETA){
							aoData.push(
									{"name": 'sCriteria_t165ETA', "value": "date.struct"},
									{"name": 'sCriteria_t165ETA_dp', "value": t165ETA},
									{"name": 'sCriteria_t165ETA_day', "value": t165ETADay},
									{"name": 'sCriteria_t165ETA_month', "value": t165ETAMonth},
									{"name": 'sCriteria_t165ETA_year', "value": t165ETAYear}
							);
						}
	
						var t165Qty1 = $('#filter_t165Qty1 input').val();
						if(t165Qty1){
							aoData.push(
									{"name": 'sCriteria_t165Qty1', "value": t165Qty1}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
