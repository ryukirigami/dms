<%@ page import="com.kombos.parts.ETA" %>



<div class="control-group fieldcontain ${hasErrors(bean: ETAInstance, field: 'goods', 'error')} ">
	<label class="control-label" for="goods">
		<g:message code="ETA.goods.label" default="Goods" />
		
	</label>
	<div class="controls">
	<g:select id="goods" name="goods.id" from="${com.kombos.parts.Goods.list()}" optionKey="id" required="" value="${ETAInstance?.goods?.id}" class="many-to-one"/>
	</div>
</div>
<g:javascript>
    document.getElementById("goods").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: ETAInstance, field: 'po', 'error')} ">
	<label class="control-label" for="po">
		<g:message code="ETA.po.label" default="Po" />
		
	</label>
	<div class="controls">
	<g:select id="po" name="po.id" from="${com.kombos.parts.PO.list()}" optionKey="id" required="" value="${ETAInstance?.po?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: ETAInstance, field: 't165ETA', 'error')} ">
	<label class="control-label" for="t165ETA">
		<g:message code="ETA.t165ETA.label" default="T165 ETA" />
		
	</label>
	<div class="controls">
	<ba:datePicker name="t165ETA" precision="day" value="${ETAInstance?.t165ETA}" format="yyyy-MM-dd"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: ETAInstance, field: 't165Qty1', 'error')} ">
	<label class="control-label" for="t165Qty1">
		<g:message code="ETA.t165Qty1.label" default="T165 Qty1" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="t165Qty1" value="${ETAInstance.t165Qty1}" />
	</div>
</div>

