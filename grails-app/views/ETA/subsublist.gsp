<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
		<g:javascript>
var vopSubSubTable;
$(function(){ 
vopSubSubTable = $('#vop_datatables_sub_sub_${idTable}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubSubList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": ''
},
{
	"sName": "kodePart",
	"mDataProp": "kodePart",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"> <input type="hidden" value="'+data+'">&nbsp;&nbsp;' + data;
	},
	"bSortable": false,
	"sWidth":"241px",
	"bVisible": true
},
{
	"sName": "namaPart",
	"mDataProp": "namaPart",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"183px",
	"bVisible": true
},
{
	"sName": "qty",
	"mDataProp": "qty",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"85px",
	"bVisible": true
},
{
	"sName": "satuan",
	"mDataProp": "satuan",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"89px",
	"bVisible": true
},
{
	"sName": "vendor",
	"mDataProp": "vendor",
	"aTargets": [4],
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "hargaSatuan",
	"mDataProp": "hargaSatuan",
	"aTargets": [5],
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "totalHarga",
	"mDataProp": "totalHarga",
	"aTargets": [6],
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						aoData.push(
									{"name": 'noReferensi', "value": "${noReferensi}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
		</g:javascript>
	</head>
	<body>
	<div class="innerinnerDetails">
<table id="vop_datatables_sub_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
	class="display table table-striped table-bordered table-hover">
    <thead>
        <tr>
           <th></th>
           <th></th>
           <th></th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Kode Part</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Nama Part</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Qty</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Satuan</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Vendor</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Harga Satuan (Rp)</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Total Harga (Rp)</div>
			</th>	
         </tr>
			    </thead>
			    <tbody></tbody>
			</table>
	</div>
</body>
</html>
