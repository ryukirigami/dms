
<%@ page import="com.kombos.administrasi.BahanBakar" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="bahanBakar_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="bahanBakar.m110ID.label" default="Kode Bahan Bakar" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="bahanBakar.m110NamaBahanBakar.label" default="Nama Bahan Bakar" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m110ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m110ID" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m110NamaBahanBakar" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m110NamaBahanBakar" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var BahanBakarTable;
var reloadBahanBakarTable;
$(function(){
	
	reloadBahanBakarTable = function() {
		BahanBakarTable.fnDraw();
	}

	var recordsbahanBakarperpage = [];//new Array();
    var anBahanBakarSelected;
    var jmlRecBahanBakarPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	BahanBakarTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	BahanBakarTable = $('#bahanBakar_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            //alert( $('li.active').text() );
            var rsBahanBakar = $("#bahanBakar_datatables tbody .row-select");
            var jmlBahanBakarCek = 0;
            var nRow;
            var idRec;
            rsBahanBakar.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsbahanBakarperpage[idRec]=="1"){
                    jmlBahanBakarCek = jmlBahanBakarCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsbahanBakarperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecBahanBakarPerPage = rsBahanBakar.length;
            if(jmlBahanBakarCek==jmlRecBahanBakarPerPage && jmlRecBahanBakarPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m110ID",
	"mDataProp": "m110ID",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m110NamaBahanBakar",
	"mDataProp": "m110NamaBahanBakar",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m110ID = $('#filter_m110ID input').val();
						if(m110ID){
							aoData.push(
									{"name": 'sCriteria_m110ID', "value": m110ID}
							);
						}
	
						var m110NamaBahanBakar = $('#filter_m110NamaBahanBakar input').val();
						if(m110NamaBahanBakar){
							aoData.push(
									{"name": 'sCriteria_m110NamaBahanBakar', "value": m110NamaBahanBakar}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	 $('.select-all').click(function(e) {

        $("#bahanBakar_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordsbahanBakarperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsbahanBakarperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#bahanBakar_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsbahanBakarperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anBahanBakarSelected = BahanBakarTable.$('tr.row_selected');
            if(jmlRecBahanBakarPerPage == anBahanBakarSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsbahanBakarperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
