

<%@ page import="com.kombos.administrasi.BahanBakar" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'bahanBakar.label', default: 'Bahan Bakar')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteBahanBakar;

$(function(){ 
	deleteBahanBakar=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/bahanBakar/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadBahanBakarTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-bahanBakar" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="bahanBakar"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${bahanBakarInstance?.m110ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m110ID-label" class="property-label"><g:message
					code="bahanBakar.m110ID.label" default="Kode Bahan Bakar" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m110ID-label">
						%{--<ba:editableValue
								bean="${bahanBakarInstance}" field="m110ID"
								url="${request.contextPath}/BahanBakar/updatefield" type="text"
								title="Enter m110ID" onsuccess="reloadBahanBakarTable();" />--}%
							
								<g:fieldValue bean="${bahanBakarInstance}" field="m110ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bahanBakarInstance?.m110NamaBahanBakar}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m110NamaBahanBakar-label" class="property-label"><g:message
					code="bahanBakar.m110NamaBahanBakar.label" default="Nama Bahan Bakar" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m110NamaBahanBakar-label">
						%{--<ba:editableValue
								bean="${bahanBakarInstance}" field="m110NamaBahanBakar"
								url="${request.contextPath}/BahanBakar/updatefield" type="text"
								title="Enter m110NamaBahanBakar" onsuccess="reloadBahanBakarTable();" />--}%
							
								<g:fieldValue bean="${bahanBakarInstance}" field="m110NamaBahanBakar"/>
							
						</span></td>
					
				</tr>
				</g:if>



            <g:if test="${bahanBakarInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="jenisStall.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${bahanBakarInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadJenisStallTable();" />--}%

                        <g:fieldValue bean="${bahanBakarInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

				<g:if test="${bahanBakarInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="jenisStall.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${bahanBakarInstance}" field="dateCreated"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:formatDate date="${bahanBakarInstance?.dateCreated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bahanBakarInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="jenisStall.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${bahanBakarInstance}" field="createdBy"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:fieldValue bean="${bahanBakarInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bahanBakarInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="jenisStall.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${bahanBakarInstance}" field="lastUpdated"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:formatDate date="${bahanBakarInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bahanBakarInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="jenisStall.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${bahanBakarInstance}" field="updatedBy"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:fieldValue bean="${bahanBakarInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>

			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${bahanBakarInstance?.id}"
					update="[success:'bahanBakar-form',failure:'bahanBakar-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteBahanBakar('${bahanBakarInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
