<%@ page import="com.kombos.administrasi.BahanBakar" %>



<div class="control-group fieldcontain ${hasErrors(bean: bahanBakarInstance, field: 'm110ID', 'error')} required">
	<label class="control-label" for="m110ID">
		<g:message code="bahanBakar.m110ID.label" default="Kode Bahan Bakar" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	    <g:textField name="m110ID" id="m110ID" maxlength="10" value="${bahanBakarInstance.m110ID}" required=""/>
	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: bahanBakarInstance, field: 'm110NamaBahanBakar', 'error')} required">
	<label class="control-label" for="m110NamaBahanBakar">
		<g:message code="bahanBakar.m110NamaBahanBakar.label" default="Nama Bahan Bakar" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m110NamaBahanBakar" maxlength="50" required="" value="${bahanBakarInstance?.m110NamaBahanBakar}"/>
	</div>
</div>

<g:javascript>
    document.getElementById("m110ID").focus();

</g:javascript>