
<%@ page import="com.kombos.reception.GatePassT800" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="gatePassT800_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="gatePassT800.gatePass.label" default="Nomor WO" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="gatePassT800.lastUpdProcess.label" default="Tanggal WO" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="gatePassT800.reception.label" default="Nopol" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="gatePassT800.t800StaDel.label" default="Model" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="gatePassT800.t800TglJamGatePass.label" default="Nama Customer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="gatePassT800.t800xDivisi.label" default="Nomor GatePass" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="gatePassT800.t800xKet.label" default="Tanggal GatePass" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="gatePassT800.t800xNamaUser.label" default="Jenis GatePass" /></div>
			</th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="gatePassT800.nama.label" default="Nama Pegawai" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="gatePassT800.catatan.label" default="Catatan" /></div>
            </th>

        </tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_wo" style="padding-top: 0px;position:relative; margin-top: 0px;width: 205px;">
					<input type="text" name="search_wo" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_tglWO" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
                    <input type="hidden" name="search_tglWO" value="date.struct">
                    <input type="hidden" name="search_tglWO_day" id="search_tglWO_day" value="">
                    <input type="hidden" name="search_tglWO_month" id="search_tglWO_month" value="">
                    <input type="hidden" name="search_tglWO_year" id="search_tglWO_year" value="">
                    <input type="text" data-date-format="dd/mm/yyyy" name="search_tglWO_dp" value="" id="search_tglWO" class="search_init">
                </div>
            </th>
			<th style="border-top: none;padding: 5px;">
				<div id="filter_nopol" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_nopol" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_model" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_model" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_namaCustomer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_namaCustomer" class="search_init" />
				</div>
			</th>


	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_noGatepass" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_noGatepass" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_tglgatepass" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
                    <input type="hidden" name="search_tglgatepass" value="date.struct">
                    <input type="hidden" name="search_tglgatepass_day" id="search_tglgatepass_day" value="">
                    <input type="hidden" name="search_tglgatepass_month" id="search_tglgatepass_month" value="">
                    <input type="hidden" name="search_tglgatepass_year" id="search_tglgatepass_year" value="">
                    <input type="text" data-date-format="dd/mm/yyyy" name="search_tglgatepass_dp" value="" id="search_tglgatepass" class="search_init">
                </div>

            </th>


            <th style="border-top: none;padding: 5px;">
				<div id="filter_gatePass" style="padding-top: 0px;position:relative; margin-top: 0px;width: 205px;">
					<input type="text" name="search_gatePass" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t800xNamaUser" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t800xNamaUser" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t800xKet" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t800xKet" class="search_init" />
                </div>
            </th>

		</tr>
	</thead>
</table>

<g:javascript>
var gatePassT800Table;
var reloadGatePassT800Table;
$(function(){
	
	reloadGatePassT800Table = function() {
		gatePassT800Table.fnDraw();
	}

    var recordsgatePassT800perpage = [];
    var anGatePassT800Selected;
    var jmlRecGatePassT800PerPage=0;
    var id;

    $('#view').click(function(e){
        e.stopPropagation();
		gatePassT800Table.fnDraw();

	});

	$('#clear').click(function(e){
         $('#search_Tanggal').val("");
         $('#search_Tanggal2').val("");
         e.stopPropagation();
		 gatePassT800Table.fnDraw();
    });

	$('#search_tglWO').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tglWO_day').val(newDate.getDate());
			$('#search_tglWO_month').val(newDate.getMonth()+1);
			$('#search_tglWO_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			gatePassT800Table.fnDraw();
	});

    $('#search_tglgatepass').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tglgatepass_day').val(newDate.getDate());
			$('#search_tglgatepass_month').val(newDate.getMonth()+1);
			$('#search_tglgatepass_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			gatePassT800Table.fnDraw();
	});



$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	gatePassT800Table.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	gatePassT800Table = $('#gatePassT800_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsGatePassT800 = $("#gatePassT800_datatables tbody .row-select");
            var jmlGatePassT800Cek = 0;
            var nRow;
            var idRec;
            rsGatePassT800.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsgatePassT800perpage[idRec]=="1"){
                    jmlGatePassT800Cek = jmlGatePassT800Cek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsgatePassT800perpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecGatePassT800PerPage = rsGatePassT800.length;
            if(jmlGatePassT800Cek==jmlRecGatePassT800PerPage && jmlRecGatePassT800PerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "reception",
	"mDataProp": "wo",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "reception",
	"mDataProp": "tglWo",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "reception",
	"mDataProp": "noPol",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "reception",
	"mDataProp": "model",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "namaCustomer",
	"mDataProp": "namaCustomer",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t800ID",
	"mDataProp": "t800ID",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t800TglJamGatePass",
	"mDataProp": "t800TglJamGatePass",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "gatePass",
	"mDataProp": "gatePass",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t800xNamaUser",
	"mDataProp": "t800xNamaUser",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "t800xKet",
	"mDataProp": "t800xKet",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var wo = $('#filter_wo input').val();
						if(wo){
							aoData.push(
									{"name": 'sCriteria_wo', "value": wo}
							);
						}

                        var tglWO = $('#search_tglWO').val();
						var tglWODay = $('#search_tglWO_day').val();
						var tglWOMonth = $('#search_tglWO_month').val();
						var tglWOYear = $('#search_tglWO_year').val();

						if(tglWO){
							aoData.push(
									{"name": 'sCriteria_tglWO', "value": "date.struct"},
									{"name": 'sCriteria_tglWO_dp', "value": tglWO},
									{"name": 'sCriteria_tglWO_day', "value": tglWODay},
									{"name": 'sCriteria_tglWO_month', "value": tglWOMonth},
									{"name": 'sCriteria_tglWO_year', "value": tglWOYear}
							);
						}
                        var nopol = $('#filter_nopol input').val();
						if(nopol){
							aoData.push(
									{"name": 'sCriteria_nopol', "value": nopol}
							);
						}

						var model = $('#filter_model input').val();
						if(model){
							aoData.push(
									{"name": 'sCriteria_model', "value": model}
							);
						}

						var namaCustomer = $('#filter_namaCustomer input').val();
						if(namaCustomer){
							aoData.push(
									{"name": 'sCriteria_namaCustomer', "value": namaCustomer}
							);
						}

						var gatePass = $('#filter_gatePass input').val();
						if(gatePass){
							aoData.push(
									{"name": 'sCriteria_gatePass', "value": gatePass}
							);
						}

						var noGatepass = $('#filter_noGatepass input').val();
						if(noGatepass){
							aoData.push(
									{"name": 'sCriteria_noGatepass', "value": noGatepass}
							);
						}

                        var tglgatepass = $('#search_tglgatepass').val();
						var tglgatepassDay = $('#search_tglgatepass_day').val();
						var tglgatepassMonth = $('#search_tglgatepass_month').val();
						var tglgatepassYear = $('#search_tglgatepass_year').val();

						if(tglgatepass){
							aoData.push(
									{"name": 'sCriteria_tglgatepass', "value": "date.struct"},
									{"name": 'sCriteria_tglgatepass_dp', "value": tglgatepass},
									{"name": 'sCriteria_tglgatepass_day', "value": tglgatepassDay},
									{"name": 'sCriteria_tglgatepass_month', "value": tglgatepassMonth},
									{"name": 'sCriteria_tglgatepass_year', "value": tglgatepassYear}
							);
						}


						var Tanggal = $('#search_Tanggal').val();


						var Tanggal2 = $('#search_Tanggal2').val();


						if(Tanggal){
							aoData.push(
									{"name": 'sCriteria_Tanggal', "value": Tanggal}

							);
						}


						if(Tanggal2){
							aoData.push(
									{"name": 'sCriteria_Tanggal2', "value":Tanggal2}

							);
						}
	

	
						var t800xKet = $('#filter_t800xKet input').val();
						if(t800xKet){
							aoData.push(
									{"name": 'sCriteria_t800xKet', "value": t800xKet}
							);
						}
	
						var t800xNamaUser = $('#filter_t800xNamaUser input').val();
						if(t800xNamaUser){
							aoData.push(
									{"name": 'sCriteria_t800xNamaUser', "value": t800xNamaUser}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
    $('.select-all').click(function(e) {
        $("#gatePassT800_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsgatePassT800perpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsgatePassT800perpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

    $('#gatePassT800_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsgatePassT800perpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anGatePassT800Selected = gatePassT800Table.$('tr.row_selected');
            if(jmlRecGatePassT800PerPage == anGatePassT800Selected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsgatePassT800perpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
