

<%@ page import="com.kombos.reception.GatePassT800" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'gatePassT800.label', default: 'Gate Pass')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteGatePassT800;

$(function(){ 
	deleteGatePassT800=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/gatePassT800/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadGatePassT800Table();
   				expandTableLayout('gatePassT800');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-gatePassT800" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="gatePassT800"
			class="table table-bordered table-hover">
			<tbody>

				<g:if test="${gatePassT800Instance?.gatePass}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="gatePass-label" class="property-label"><g:message
					code="gatePassT800.gatePass.label" default="Gate Pass" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="gatePass-label">

                                <g:fieldValue bean="${gatePassT800Instance?.gatePass}" field="m800NamaGatePass"/>
						</span></td>
					
				</tr>
				</g:if>

				<g:if test="${gatePassT800Instance?.reception}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="reception-label" class="property-label"><g:message
					code="gatePassT800.reception.label" default="Nomor WO" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="reception-label">

                                     <g:fieldValue bean="${gatePassT800Instance?.reception}" field="t401NoWO"/>
						</span></td>
					
				</tr>
				</g:if>

                <g:if test="${gatePassT800Instance?.reception}">
                    <tr>
                        <td class="span2" style="text-align: right;"><span
                                id="reception1-label" class="property-label"><g:message
                                    code="gatePassT800.reception.label" default="tgl WO" />:</span></td>

                        <td class="span3"><span class="property-value"
                                                aria-labelledby="reception-label">

                            <g:formatDate date="${gatePassT800Instance?.reception?.t401TanggalWO}" />
                        </span></td>

                    </tr>
                </g:if>
			
				<g:if test="${gatePassT800Instance?.t800TglJamGatePass}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t800TglJamGatePass-label" class="property-label"><g:message
					code="gatePassT800.t800TglJamGatePass.label" default="Tgl Jam Gate Pass" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t800TglJamGatePass-label">
								<g:formatDate date="${gatePassT800Instance?.t800TglJamGatePass}" />
							
						</span></td>
					
				</tr>
				</g:if>

                <g:if test="${gatePassT800Instance?.t800xNamaUser}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t800xNamaUser-label" class="property-label"><g:message
                                code="gatePassT800.t800xNamaUser.label" default="Nama Pegawai" />:</span></td>

                    <td class="span3"><span class="property-value">

                        <g:fieldValue bean="${gatePassT800Instance}" field="t800xNamaUser"/>

                    </span></td>

                </tr>
                </g:if>

				<g:if test="${gatePassT800Instance?.t800xKet}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t800xKet-label" class="property-label"><g:message
					code="gatePassT800.t800xKet.label" default="Catatan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t800xKet-label">

							
								<g:fieldValue bean="${gatePassT800Instance}" field="t800xKet"/>
							
						</span></td>
					
				</tr>
				</g:if>

                <g:if test="${gatePassT800Instance?.createdBy}">
                    <tr>
                        <td class="span2" style="text-align: right;"><span
                                id="createdBy-label" class="property-label"><g:message
                                    code="gatePassT800.createdBy.label" default="Created By" />:</span></td>

                        <td class="span3"><span class="property-value"
                                                aria-labelledby="createdBy-label">

                            <g:fieldValue bean="${gatePassT800Instance}" field="createdBy"/>

                        </span></td>

                    </tr>
                </g:if>

                <g:if test="${gatePassT800Instance?.dateCreated}">
                    <tr>
                        <td class="span2" style="text-align: right;"><span
                                id="dateCreated-label" class="property-label"><g:message
                                    code="gatePassT800.dateCreated.label" default="Date Created" />:</span></td>

                        <td class="span3"><span class="property-value"
                                                aria-labelledby="dateCreated-label">


                            <g:formatDate date="${gatePassT800Instance?.dateCreated}" />

                        </span></td>

                    </tr>
                </g:if>


            <g:if test="${gatePassT800Instance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="gatePassT800.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${gatePassT800Instance}" field="lastUpdProcess"
                                url="${request.contextPath}/GatePassT800/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadGatePassT800Table();" />--}%

                        <g:fieldValue bean="${gatePassT800Instance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${gatePassT800Instance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="gatePassT800.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${gatePassT800Instance}" field="lastUpdated"
                                url="${request.contextPath}/GatePassT800/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadGatePassT800Table();" />--}%

                        <g:formatDate date="${gatePassT800Instance?.lastUpdated}" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${gatePassT800Instance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="gatePassT800.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${gatePassT800Instance}" field="updatedBy"
								url="${request.contextPath}/GatePassT800/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadGatePassT800Table();" />--}%
							
								<g:fieldValue bean="${gatePassT800Instance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('gatePassT800');"><g:message
						code="default.button.close.label" default="Close" /></a>

			</fieldset>
		</g:form>
	</div>
</body>
</html>
