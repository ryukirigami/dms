
<%@ page import="com.kombos.reception.GatePassT800" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'gatePassT800.label', default: 'Gate Pass')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			$(function(){ 
			
				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('gatePassT800');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('gatePassT800', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('gatePassT800', '${request.contextPath}/gatePassT800/massdelete', reloadGatePassT800Table);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('gatePassT800','${request.contextPath}/gatePassT800/show/'+id);
				};
				
				edit = function(id) {
					editInstance('gatePassT800','${request.contextPath}/gatePassT800/edit/'+id);
				};

});
       var checkin = $('#search_Tanggal').datepicker({
            onRender: function(date) {
                return '';
            }
        }).on('changeDate', function(ev) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate());
            checkout.setValue(newDate);
            checkin.hide();
            $('#search_Tanggal2')[0].focus();
        }).data('datepicker');

        var checkout = $('#search_Tanggal2').datepicker({
            onRender: function(date) {
                return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');

        printGatePass = function(){
           checkGatePass =[];
            $("#gatePassT800-table tbody .row-select").each(function() {
                 if(this.checked){
                  var nRow = $(this).next("input:hidden").val();
					checkGatePass.push(nRow);
                }
            });
            if(checkGatePass.length<1 || checkGatePass.length>1){
                alert('Silahkan Pilih Salah satu Gatepass Untuk Dicetak');
                return;

            }
           var idGatepass =  JSON.stringify(checkGatePass);
           window.location = "${request.contextPath}/gatePassT800/printGatePass?idGatepass="+idGatepass;
        }
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="gatePassT800-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <fieldset>
                <table style="padding-right: 10px">
                    <tr>
                        <td style="width: 130px">
                            <label class="control-label" for="t951Tanggal">
                                <g:message code="auditTrail.tanggal.label" default="Tanggal Gate Pass" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="filter_m777Tgl" class="controls">
                                <ba:datePicker id="search_Tanggal" name="search_Tanggal" precision="day" format="dd/MM/yyyy"  value="${new Date()}" />
                                &nbsp;&nbsp;&nbsp;s.d.&nbsp;&nbsp;&nbsp;
                                <ba:datePicker id="search_Tanggal2" name="search_Tanggal2" precision="day" format="dd/MM/yyyy"  value="${new Date()}" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" >
                            <div class="controls" style="right: 0">
                                <button class="btn btn-primary view" name="view" id="view" >Search</button>
                                <button class="btn btn-cancel" name="clear" id="clear">Clear Search</button>
                            </div>
                        </td>
                    </tr>
                </table>

            </fieldset>
			<g:render template="dataTables" />
            <fieldset class="buttons controls" style="padding-top: 5px;">
                <button id='printGatePassData' onclick="printGatePass();" type="button" class="btn btn-primary">Print Gate Pass</button>
            </fieldset>
		</div>
		<div class="span7" id="gatePassT800-form" style="display: none;"></div>
	</div>
</body>
</html>
