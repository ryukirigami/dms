<%@ page import="com.kombos.reception.Reception; java.text.SimpleDateFormat; java.text.DateFormat; com.kombos.reception.GatePassT800" %>


<g:javascript>
         $(function(){
            $('form').on('keypress', function(e) {
              var keyCode = e.keyCode || e.which;
              if (keyCode === 13) {
                findData();
                e.preventDefault();
                return false;
              }
            });
              findData = function(){
                    var nopol=$('#nopol').val().trim();
                    if(nopol==""){
                        return false;
                    }
                    $('#nopol').val(nopol.toUpperCase());
                    nopol = nopol.toUpperCase();
                    $.ajax({
                        url:'${request.contextPath}/gatePassT800/cekData',
                        type:'POST',
                        data:'nopol=' + nopol,
                        dataType: 'json',
    		            success: function(data,textStatus,xhr){
    		                if(data.length>0){
    		                    $('#namaCustomer').attr('value',data[0].namaCustomer);
                                $('#model').attr('value',data[0].models);
                                $('#warna').attr('value',data[0].warna);
                                $('#nowo').attr('value',data[0].id);
    		                }else{
                                $('#namaCustomer').attr('value','');
                                $('#model').attr('value','');
                                $('#warna').attr('value','');
                                $('#nowo').attr('value','');
                                alert("Nopol Tidak Ada");
    		                }

    		            },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(data,textStatus){
                        }
                   });
             }

        });
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: gatePassT800Instance, field: 'reception', 'error')} ">
    <label class="control-label" for="nopol">
        <g:message code="gatePassT800.reception.label" default="No Polisi" />
    </label>
    <div class="controls">
        <g:textField name="nopol" id="nopol" style="text-transform:uppercase" />
        <g:hiddenField name="reception.id" id="nowo" />
        <g:field type="button" style="color: white;" class="btn btn-primary create" onclick="findData();"
                 name="view" id="view" value="${message(code: 'default.search.label', default: 'Search')}" />
    </div>
</div>
<hr/>
<div class="control-group">
    <label class="control-label" for="t800StaDel">
        <g:message code="gatePassT800.t800StaDel.label" default="Nama Customer" />
    </label>
    <div class="controls">
        <g:textField name="namaCustomer" id="namaCustomer" value="${gatePassT800Instance?.reception?.historyCustomer ? gatePassT800Instance?.reception?.historyCustomer?.t182NamaDepan+" "+gatePassT800Instance?.reception?.historyCustomer?.t182NamaBelakang : " "}" readonly="" />
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="t800StaDel">
        <g:message code="gatePassT800.t800StaDel.label" default="Model Vehicle" />
    </label>
    <div class="controls">
        <g:textField name="model" id="model" value="${gatePassT800Instance?.reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel}" readonly="" />
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="t800StaDel">
        <g:message code="gatePassT800.t800StaDel.label" default="Warna Vehicle" />
    </label>
    <div class="controls">
        <g:textField name="warna" id="warna" value="${gatePassT800Instance?.reception?.historyCustomerVehicle?.warna?.m092NamaWarna}" readonly="" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: gatePassT800Instance, field: 'gatePass', 'error')} ">
    <label class="control-label" for="gatePass">
        <g:message code="gatePassT800.gatePass.label" default="Gate Pass" />

    </label>
    <div class="controls">
        <g:radioGroup name="gatePass" id="gatePass" values="['1','2','3','4']" labels="['Gate Pass','Test Jalan Kendaraan','Rawat Jalan Kendaraan','Sublet']" value="${gatePassT800Instance?.gatePass?.m800Id}" >
            ${it.radio} ${it.label}
        </g:radioGroup>
    </div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: gatePassT800Instance, field: 't800xKet', 'error')} ">
    <label class="control-label" for="t800xKet">
        <g:message code="gatePassT800.t800xKet.label" default="Catatan" />

    </label>
    <div class="controls">
        <g:textArea cols="50" rows="5" name="t800xKet" value="${gatePassT800Instance?.t800xKet}" />
    </div>
</div>
