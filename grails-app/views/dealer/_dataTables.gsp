
<%@ page import="com.kombos.administrasi.DealerPenjual" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</g:javascript>

<table id="dealer_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="dealerPenjual.m091NamaDealer.label" default="Nama Dealer" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="dealerPenjual.m091AlamatDealer.label" default="Alamat Dealer" /></div>
            </th>


		</tr>
		<tr>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_m091NamaDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_m091NamaDealer" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_m091AlamatDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_m091AlamatDealer" class="search_init" />
                </div>
            </th>
	
		</tr>
	</thead>
</table>

<g:javascript>
var DealerTable;
var reloadDealerTable;
$(function(){
	
	reloadDealerTable = function() {
		DealerTable.fnDraw();
	}
	var recordsDealerPerPage = [];//new Array();
    var anDealerSelected;
    var jmlRecDealerPerPage=0;
    var id;

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	DealerTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	DealerTable = $('#dealer_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsDealer = $("#dealer_datatables tbody .row-select");
            var jmlDealerCek = 0;
            var nRow;
            var idRec;
            rsDealer.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsDealerPerPage[idRec]=="1"){
                    jmlDealerCek = jmlDealerCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsDealerPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecDealerPerPage = rsDealer.length;
            if(jmlDealerCek==jmlRecDealerPerPage && jmlRecDealerPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m091NamaDealer",
	"mDataProp": "m091NamaDealer",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m091AlamatDealer",
	"mDataProp": "m091AlamatDealer",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m091NamaDealer = $('#filter_m091NamaDealer input').val();
						if(m091NamaDealer){
							aoData.push(
									{"name": 'sCriteria_m091NamaDealer', "value": m091NamaDealer}
							);
						}
	
						var m091AlamatDealer = $('#filter_m091AlamatDealer input').val();
						if(m091AlamatDealer){
							aoData.push(
									{"name": 'sCriteria_m091AlamatDealer', "value": m091AlamatDealer}
							);
						}
	

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
	$('.select-all').click(function(e) {

        $("#dealer_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsDealerPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsDealerPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#dealer_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsDealerPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anDealerSelected = DealerTable.$('tr.row_selected');
            if(jmlRecDealerPerPage == anDealerSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsDealerPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
