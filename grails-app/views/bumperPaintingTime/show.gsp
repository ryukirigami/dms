

<%@ page import="com.kombos.administrasi.BumperPaintingTime" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'bumperPaintingTime.label', default: 'Plastic Bumper Painting Time')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteBumperPaintingTime;

$(function(){ 
	deleteBumperPaintingTime=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/bumperPaintingTime/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadBumperPaintingTimeTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-bumperPaintingTime" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="bumperPaintingTime"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${bumperPaintingTimeInstance?.m045TglBerlaku}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m045TglBerlaku-label" class="property-label"><g:message
					code="bumperPaintingTime.m045TglBerlaku.label" default="Tanggal Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m045TglBerlaku-label">
						%{--<ba:editableValue
								bean="${bumperPaintingTimeInstance}" field="m045TglBerlaku"
								url="${request.contextPath}/BumperPaintingTime/updatefield" type="text"
								title="Enter m045TglBerlaku" onsuccess="reloadBumperPaintingTimeTable();" />--}%
							
								<g:formatDate date="${bumperPaintingTimeInstance?.m045TglBerlaku}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bumperPaintingTimeInstance?.masterPanel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="masterPanel-label" class="property-label"><g:message
					code="bumperPaintingTime.masterPanel.label" default="Panel" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="masterPanel-label">
						%{--<ba:editableValue
								bean="${bumperPaintingTimeInstance}" field="masterPanel"
								url="${request.contextPath}/BumperPaintingTime/updatefield" type="text"
								title="Enter masterPanel" onsuccess="reloadBumperPaintingTimeTable();" />--}%
							
								${bumperPaintingTimeInstance?.masterPanel?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bumperPaintingTimeInstance?.m0452New}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m0452New-label" class="property-label"><g:message
					code="bumperPaintingTime.m0452New.label" default="2 Coat New Panel" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m0452New-label">
						%{--<ba:editableValue
								bean="${bumperPaintingTimeInstance}" field="m0452New"
								url="${request.contextPath}/BumperPaintingTime/updatefield" type="text"
								title="Enter m0452New" onsuccess="reloadBumperPaintingTimeTable();" />--}%
							
								<g:fieldValue bean="${bumperPaintingTimeInstance}" field="m0452New"/>
							
						</span></td>
					
				</tr>
				</g:if>

                <g:if test="${bumperPaintingTimeInstance?.m0452Def}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m0452Def-label" class="property-label"><g:message
                                code="bumperPaintingTime.m0452Def.label" default="2 Coat Deformation Repair" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m0452Def-label">
                        %{--<ba:editableValue
                                bean="${bumperPaintingTimeInstance}" field="m0452Def"
                                url="${request.contextPath}/BumperPaintingTime/updatefield" type="text"
                                title="Enter m0452Def" onsuccess="reloadBumperPaintingTimeTable();" />--}%

                        <g:fieldValue bean="${bumperPaintingTimeInstance}" field="m0452Def"/>

                    </span></td>

                </tr>
                </g:if>

                <g:if test="${bumperPaintingTimeInstance?.m0452Scr}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m0452Scr-label" class="property-label"><g:message
                                code="bumperPaintingTime.m0452Scr.label" default="2 Coat Scratch Repair" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m0452Scr-label">
                        %{--<ba:editableValue
                                bean="${bumperPaintingTimeInstance}" field="m0452Scr"
                                url="${request.contextPath}/BumperPaintingTime/updatefield" type="text"
                                title="Enter m0452Scr" onsuccess="reloadBumperPaintingTimeTable();" />--}%

                        <g:fieldValue bean="${bumperPaintingTimeInstance}" field="m0452Scr"/>

                    </span></td>

                </tr>
                </g:if>
			
				<g:if test="${bumperPaintingTimeInstance?.m0453New}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m0453New-label" class="property-label"><g:message
					code="bumperPaintingTime.m0453New.label" default="3 Coat New Panel" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m0453New-label">
						%{--<ba:editableValue
								bean="${bumperPaintingTimeInstance}" field="m0453New"
								url="${request.contextPath}/BumperPaintingTime/updatefield" type="text"
								title="Enter m0453New" onsuccess="reloadBumperPaintingTimeTable();" />--}%
							
								<g:fieldValue bean="${bumperPaintingTimeInstance}" field="m0453New"/>
							
						</span></td>
					
				</tr>
				</g:if>
			

			
				<g:if test="${bumperPaintingTimeInstance?.m0453Def}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m0453Def-label" class="property-label"><g:message
					code="bumperPaintingTime.m0453Def.label" default="3 Coat Deformation Repair" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m0453Def-label">
						%{--<ba:editableValue
								bean="${bumperPaintingTimeInstance}" field="m0453Def"
								url="${request.contextPath}/BumperPaintingTime/updatefield" type="text"
								title="Enter m0453Def" onsuccess="reloadBumperPaintingTimeTable();" />--}%
							
								<g:fieldValue bean="${bumperPaintingTimeInstance}" field="m0453Def"/>
							
						</span></td>
					
				</tr>
				</g:if>
			

			
				<g:if test="${bumperPaintingTimeInstance?.m0453Scr}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m0453Scr-label" class="property-label"><g:message
					code="bumperPaintingTime.m0453Scr.label" default="3 Coat Scratch Repair" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m0453Scr-label">
						%{--<ba:editableValue
								bean="${bumperPaintingTimeInstance}" field="m0453Scr"
								url="${request.contextPath}/BumperPaintingTime/updatefield" type="text"
								title="Enter m0453Scr" onsuccess="reloadBumperPaintingTimeTable();" />--}%
							
								<g:fieldValue bean="${bumperPaintingTimeInstance}" field="m0453Scr"/>
							
						</span></td>
					
				</tr>
				</g:if>



            <g:if test="${bumperPaintingTimeInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="bumperPaintingTime.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${bumperPaintingTimeInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/BumperPaintingTime/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadBumperPaintingTimeTable();" />--}%

                        <g:fieldValue bean="${bumperPaintingTimeInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${bumperPaintingTimeInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="bumperPaintingTime.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${bumperPaintingTimeInstance}" field="dateCreated"
								url="${request.contextPath}/BumperPaintingTime/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadBumperPaintingTimeTable();" />--}%
							
								<g:formatDate date="${bumperPaintingTimeInstance?.dateCreated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${bumperPaintingTimeInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="bumperPaintingTime.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${bumperPaintingTimeInstance}" field="createdBy"
                                url="${request.contextPath}/BumperPaintingTime/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadBumperPaintingTimeTable();" />--}%

                        <g:fieldValue bean="${bumperPaintingTimeInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${bumperPaintingTimeInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="bumperPaintingTime.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${bumperPaintingTimeInstance}" field="lastUpdated"
								url="${request.contextPath}/BumperPaintingTime/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadBumperPaintingTimeTable();" />--}%
							
								<g:formatDate date="${bumperPaintingTimeInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${bumperPaintingTimeInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="bumperPaintingTime.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${bumperPaintingTimeInstance}" field="updatedBy"
                                url="${request.contextPath}/BumperPaintingTime/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadBumperPaintingTimeTable();" />--}%

                        <g:fieldValue bean="${bumperPaintingTimeInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
            %{--
                            <g:if test="${bumperPaintingTimeInstance?.companyDealer}">
                            <tr>
                            <td class="span2" style="text-align: right;"><span
                                id="companyDealer-label" class="property-label"><g:message
                                code="bumperPaintingTime.companyDealer.label" default="Company Dealer" />:</span></td>

                                    <td class="span3"><span class="property-value"
                                    aria-labelledby="companyDealer-label">
                                    <ba:editableValue
                                            bean="${bumperPaintingTimeInstance}" field="companyDealer"
                                            url="${request.contextPath}/BumperPaintingTime/updatefield" type="text"
                                            title="Enter companyDealer" onsuccess="reloadBumperPaintingTimeTable();" />

                                            <g:link controller="companyDealer" action="show" id="${bumperPaintingTimeInstance?.companyDealer?.id}">${bumperPaintingTimeInstance?.companyDealer?.encodeAsHTML()}</g:link>

                                    </span></td>

                            </tr>
                            </g:if>
            --}%
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${bumperPaintingTimeInstance?.id}"
					update="[success:'bumperPaintingTime-form',failure:'bumperPaintingTime-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteBumperPaintingTime('${bumperPaintingTimeInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
