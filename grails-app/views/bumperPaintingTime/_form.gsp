<%@ page import="com.kombos.administrasi.MasterPanel; com.kombos.administrasi.BumperPaintingTime" %>
<r:require modules="autoNumeric" />
<script type="text/javascript">
    jQuery(function($) {
        $('.auto').autoNumeric('init',{
            vMin:'0',
            aSep:''
        });
    });
</script>


<div class="control-group fieldcontain ${hasErrors(bean: bumperPaintingTimeInstance, field: 'm045TglBerlaku', 'error')} required">
	<label class="control-label" for="m045TglBerlaku">
		<g:message code="bumperPaintingTime.m045TglBerlaku.label" default="Tanggal Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<ba:datePicker name="m045TglBerlaku"  precision="day" value="${bumperPaintingTimeInstance?.m045TglBerlaku}" format="dd/MM/yyyy" required="true" />
	</div>
</div>
<g:javascript>
    document.getElementById('m045TglBerlaku').focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: bumperPaintingTimeInstance, field: 'masterPanel', 'error')} required">
	<label class="control-label" for="masterPanel">
		<g:message code="bumperPaintingTime.masterPanel.label" default="Panel" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="masterPanel" name="masterPanel.id" from="${MasterPanel.list()}" optionKey="id" required="" value="${bumperPaintingTimeInstance?.masterPanel?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: bumperPaintingTimeInstance, field: 'm0452New', 'error')} required">
	<label class="control-label" for="m0452New">
		<g:message code="bumperPaintingTime.m0452New.label" default="2 Coat New Panel" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m0452New" class="auto" value="${fieldValue(bean: bumperPaintingTimeInstance, field: 'm0452New')}" required=""/> Jam
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: bumperPaintingTimeInstance, field: 'm0452Def', 'error')} required">
    <label class="control-label" for="m0452Def">
        <g:message code="bumperPaintingTime.m0452Def.label" default="2 Coat Deformation Repair" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="m0452Def" class="auto" value="${fieldValue(bean: bumperPaintingTimeInstance, field: 'm0452Def')}" required=""/> Jam
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: bumperPaintingTimeInstance, field: 'm0452Scr', 'error')} required">
    <label class="control-label" for="m0452Scr">
        <g:message code="bumperPaintingTime.m0452Scr.label" default="2 Coat Scratch Repair" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="m0452Scr" class="auto" value="${fieldValue(bean: bumperPaintingTimeInstance, field: 'm0452Scr')}" required=""/> Jam
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: bumperPaintingTimeInstance, field: 'm0453New', 'error')} required">
	<label class="control-label" for="m0453New">
		<g:message code="bumperPaintingTime.m0453New.label" default="3 Coat New Panel" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m0453New" class="auto" value="${fieldValue(bean: bumperPaintingTimeInstance, field: 'm0453New')}" required=""/> Jam
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: bumperPaintingTimeInstance, field: 'm0453Def', 'error')} required">
	<label class="control-label" for="m0453Def">
		<g:message code="bumperPaintingTime.m0453Def.label" default="3 Coat Deformation Repair" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m0453Def" class="auto" value="${fieldValue(bean: bumperPaintingTimeInstance, field: 'm0453Def')}" required=""/> Jam
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: bumperPaintingTimeInstance, field: 'm0453Scr', 'error')} required">
	<label class="control-label" for="m0453Scr">
		<g:message code="bumperPaintingTime.m0453Scr.label" default="3 Coat Scratch Repair" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m0453Scr" class="auto" value="${fieldValue(bean: bumperPaintingTimeInstance, field: 'm0453Scr')}" required=""/> Jam
	</div>
</div>
%{--
<div class="control-group fieldcontain ${hasErrors(bean: bumperPaintingTimeInstance, field: 'createdBy', 'error')} ">
	<label class="control-label" for="createdBy">
		<g:message code="bumperPaintingTime.createdBy.label" default="Created By" />
		
	</label>
	<div class="controls">
	<g:textField name="createdBy" value="${bumperPaintingTimeInstance?.createdBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: bumperPaintingTimeInstance, field: 'updatedBy', 'error')} ">
	<label class="control-label" for="updatedBy">
		<g:message code="bumperPaintingTime.updatedBy.label" default="Updated By" />
		
	</label>
	<div class="controls">
	<g:textField name="updatedBy" value="${bumperPaintingTimeInstance?.updatedBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: bumperPaintingTimeInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="bumperPaintingTime.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${bumperPaintingTimeInstance?.lastUpdProcess}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: bumperPaintingTimeInstance, field: 'companyDealer', 'error')} required">
	<label class="control-label" for="companyDealer">
		<g:message code="bumperPaintingTime.companyDealer.label" default="Company Dealer" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="companyDealer" name="companyDealer.id" from="${com.kombos.administrasi.CompanyDealer.list()}" optionKey="id" required="" value="${bumperPaintingTimeInstance?.companyDealer?.id}" class="many-to-one"/>
	</div>
</div>
--}%
