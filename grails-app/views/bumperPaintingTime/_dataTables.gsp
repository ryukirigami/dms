
<%@ page import="com.kombos.administrasi.BumperPaintingTime" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</g:javascript>

<table id="bumperPaintingTime_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="bumperPaintingTime.m045TglBerlaku.label" default="Tanggal Berlaku" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="bumperPaintingTime.masterPanel.label" default="Panel" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="bumperPaintingTime.m0452New.label" default="2CNP" /></div>
			</th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bumperPaintingTime.m0452Def.label" default="2CDR" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bumperPaintingTime.m0452Scr.label" default="2CSR" /></div>
            </th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="bumperPaintingTime.m0453New.label" default="3CNP" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="bumperPaintingTime.m0453Def.label" default="3CDR" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="bumperPaintingTime.m0453Scr.label" default="3CSR" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="bumperPaintingTime.createdBy.label" default="Created By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="bumperPaintingTime.updatedBy.label" default="Updated By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="bumperPaintingTime.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="bumperPaintingTime.companyDealer.label" default="Company Dealer" /></div>
			</th>

		
		</tr>
		<tr>
		

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m045TglBerlaku" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m045TglBerlaku" value="date.struct">
					<input type="hidden" name="search_m045TglBerlaku_day" id="search_m045TglBerlaku_day" value="">
					<input type="hidden" name="search_m045TglBerlaku_month" id="search_m045TglBerlaku_month" value="">
					<input type="hidden" name="search_m045TglBerlaku_year" id="search_m045TglBerlaku_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m045TglBerlaku_dp" value="" id="search_m045TglBerlaku" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_masterPanel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_masterPanel" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m0452New" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m0452New" onkeypress="return isNumberKey(event)" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_m0452Def" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_m0452Def" onkeypress="return isNumberKey(event)" class="search_init" />
                </div>
            </th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m0453New" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m0453New" onkeypress="return isNumberKey(event)" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_m0452Scr" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_m0452Scr" onkeypress="return isNumberKey(event)" class="search_init" />
                </div>
            </th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m0453Def" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m0453Def" onkeypress="return isNumberKey(event)" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m0453Scr" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m0453Scr" onkeypress="return isNumberKey(event)" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_createdBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_updatedBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_companyDealer" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var bumperPaintingTimeTable;
var reloadBumperPaintingTimeTable;
$(function(){
	
	reloadBumperPaintingTimeTable = function() {
		bumperPaintingTimeTable.fnDraw();
	}

	var recordsBumperPaintingTimeperpage = [];//new Array();
    var anBumperPaintingTimeSelected;
    var jmlRecBumperPaintingTimePerPage=0;
    var id;
    
	$('#search_m045TglBerlaku').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m045TglBerlaku_day').val(newDate.getDate());
			$('#search_m045TglBerlaku_month').val(newDate.getMonth()+1);
			$('#search_m045TglBerlaku_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			bumperPaintingTimeTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	bumperPaintingTimeTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	bumperPaintingTimeTable = $('#bumperPaintingTime_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            //alert( $('li.active').text() );
            var rsBumperPaintingTime = $("#bumperPaintingTime_datatables tbody .row-select");
            var jmlBumperPaintingTimeCek = 0;
            var nRow;
            var idRec;
            rsBumperPaintingTime.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsBumperPaintingTimeperpage[idRec]=="1"){
                    jmlBumperPaintingTimeCek = jmlBumperPaintingTimeCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsBumperPaintingTimeperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecBumperPaintingTimePerPage = rsBumperPaintingTime.length;
            if(jmlBumperPaintingTimeCek==jmlRecBumperPaintingTimePerPage && jmlRecBumperPaintingTimePerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m045TglBerlaku",
	"mDataProp": "m045TglBerlaku",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "masterPanel",
	"mDataProp": "masterPanel",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m0452New",
	"mDataProp": "m0452New",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m0452Def",
	"mDataProp": "m0452Def",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m0452Scr",
	"mDataProp": "m0452Scr",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m0453New",
	"mDataProp": "m0453New",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "m0453Def",
	"mDataProp": "m0453Def",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "m0453Scr",
	"mDataProp": "m0453Scr",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "createdBy",
	"mDataProp": "createdBy",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "updatedBy",
	"mDataProp": "updatedBy",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "companyDealer",
	"mDataProp": "companyDealer",
	"aTargets": [11],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var m045TglBerlaku = $('#search_m045TglBerlaku').val();
						var m045TglBerlakuDay = $('#search_m045TglBerlaku_day').val();
						var m045TglBerlakuMonth = $('#search_m045TglBerlaku_month').val();
						var m045TglBerlakuYear = $('#search_m045TglBerlaku_year').val();
						
						if(m045TglBerlaku){
							aoData.push(
									{"name": 'sCriteria_m045TglBerlaku', "value": "date.struct"},
									{"name": 'sCriteria_m045TglBerlaku_dp', "value": m045TglBerlaku},
									{"name": 'sCriteria_m045TglBerlaku_day', "value": m045TglBerlakuDay},
									{"name": 'sCriteria_m045TglBerlaku_month', "value": m045TglBerlakuMonth},
									{"name": 'sCriteria_m045TglBerlaku_year', "value": m045TglBerlakuYear}
							);
						}
	
						var masterPanel = $('#filter_masterPanel input').val();
						if(masterPanel){
							aoData.push(
									{"name": 'sCriteria_masterPanel', "value": masterPanel}
							);
						}
	
						var m0452New = $('#filter_m0452New input').val();
						if(m0452New){
							aoData.push(
									{"name": 'sCriteria_m0452New', "value": m0452New}
							);
						}
	
						var m0453New = $('#filter_m0453New input').val();
						if(m0453New){
							aoData.push(
									{"name": 'sCriteria_m0453New', "value": m0453New}
							);
						}
	
						var m0452Def = $('#filter_m0452Def input').val();
						if(m0452Def){
							aoData.push(
									{"name": 'sCriteria_m0452Def', "value": m0452Def}
							);
						}
	
						var m0453Def = $('#filter_m0453Def input').val();
						if(m0453Def){
							aoData.push(
									{"name": 'sCriteria_m0453Def', "value": m0453Def}
							);
						}
	
						var m0452Scr = $('#filter_m0452Scr input').val();
						if(m0452Scr){
							aoData.push(
									{"name": 'sCriteria_m0452Scr', "value": m0452Scr}
							);
						}
	
						var m0453Scr = $('#filter_m0453Scr input').val();
						if(m0453Scr){
							aoData.push(
									{"name": 'sCriteria_m0453Scr', "value": m0453Scr}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#bumperPaintingTime_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordsBumperPaintingTimeperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsBumperPaintingTimeperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#bumperPaintingTime_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsBumperPaintingTimeperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anBumperPaintingTimeSelected = bumperPaintingTimeTable.$('tr.row_selected');
            if(jmlRecBumperPaintingTimePerPage == anBumperPaintingTimeSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsBumperPaintingTimeperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
