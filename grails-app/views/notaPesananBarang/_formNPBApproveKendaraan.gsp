<table>
<tr>
    <td>
        <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'namaPemilik', 'error')} ">
            <label class="control-label" for="namaPemilik">
                <g:message code="notaPesananBarang.namaPemilik.label" default="Nama Pemilik" />

            </label>
            <div class="controls">
                <g:textField readonly="" name="namaPemilik" id="namaPemilik" value="${notaPesananBarangInstance?.namaPemilik}" />
            </div>
        </div>
    </td>
    <td>
        <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'tahunPembuatan', 'error')} ">
            <label class="control-label" for="tahunPembuatan">
                <g:message code="notaPesananBarang.tahunPembuatan.label" default="Tahun Pembuatan" />

            </label>
            <div class="controls">
                <g:field type="number" name="tahunPembuatan" id="tahunPembuatan" value="${notaPesananBarangInstance.tahunPembuatan}" />
                <g:javascript>
                    $("#tahunPembuatan").attr("disabled", true);
                </g:javascript>
            </div>
        </div>
    </td>



</tr>
<tr>
    <td>
        <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'noPolisi', 'error')} ">
            <label class="control-label" for="noPolisi">
                <g:message code="notaPesananBarang.noPolisi.label" default="No Polisi" />

            </label>
            <div class="controls">
                <g:textField readonly="" name="noPolisi" id="noPolisi" value="${notaPesananBarangInstance?.noPolisi}" />
            </div>
        </div>
    </td>


</tr>
<tr>
    <td>
        <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'noRangka', 'error')} ">
            <label class="control-label" for="noRangka">
                <g:message code="notaPesananBarang.noRangka.label" default="No Rangka" />

            </label>
            <div class="controls">
                <g:textField readonly="" name="noRangka" id="noRangka" value="${notaPesananBarangInstance?.noRangka}" />
            </div>
        </div>

    </td>

</tr>
<tr>
    <td>
        <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'noEngine', 'error')} ">
            <label class="control-label" for="noEngine">
                <g:message code="notaPesananBarang.noEngine.label" default="No Engine" />

            </label>
            <div class="controls">
                <g:textField readonly="" name="noEngine" id="noEngine" value="${notaPesananBarangInstance?.noEngine}" />
            </div>
        </div>
    </td>

</tr>
<tr>
    <td>
        <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'tipeKendaraan', 'error')} ">
            <label class="control-label" for="tipeKendaraan">
                <g:message code="notaPesananBarang.tipeKendaraan.label" default="Tipe Kendaraan" />

            </label>
            <div class="controls">
                <g:textField readonly="" name="tipeKendaraan" id="tipeKendaraan" value="${notaPesananBarangInstance?.tipeKendaraan}" />
            </div>
        </div>
    </td>


</tr>
<tr>

 </tr>
<tr>

</tr>

</table>
