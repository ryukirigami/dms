
<%@ page import="com.kombos.parts.PickingSlipDetail" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="goods_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>

         <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goodsDetail.goods.goods.m111ID.label" default="Kode Goods" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goodsDetail.goods.goods.m111Nama.label" default="Nama Goods" /></div>
        </th>


    </tr>
    <tr>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_m111ID_modal" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_m111ID_modal" id="name="search_m111ID_modal"" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_m111Nama_modal" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_m111Nama_modal" id="search_m111Nama_modal" class="search_init" />
            </div>
        </th>


    </tr>
    </thead>
</table>

<g:javascript>
var goodsTable;
var reloadPickingSlipTable;
$(function(){

	reloadPickingSlipTable = function() {
		goodsTable.fnDraw();
	}



$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		//	alert("hallo");
		 	goodsTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	goodsTable = $('#goods_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "goodsList")}",
		"aoColumns": [

{
	"sName": "m111ID",
	"mDataProp": "m111ID_modal",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['noUrut']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m111Nama",
	"mDataProp": "m111Nama_modal",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var m111ID = $('#filter_m111ID_modal input').val();
						if(m111ID){
							aoData.push(
									{"name": 'sCriteria_m111ID_modal', "value": m111ID}
							);
						}

						var m111Nama = $('#filter_m111Nama_modal input').val();
						if(m111Nama){
							aoData.push(
									{"name": 'sCriteria_m111Nama_modal', "value": m111Nama}
							);
						}

						var exist =[];
						$("#notaPesananBarangDetail_datatables tbody .row-select").each(function() {
							var id = $(this).next("input:hidden").val();
							exist.push(id);
						});
						if(exist.length > 0){
							aoData.push(
										{"name": 'sCriteria_exist', "value": JSON.stringify(exist)}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>



