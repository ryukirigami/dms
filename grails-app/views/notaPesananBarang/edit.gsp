<%@ page import="com.kombos.parts.NotaPesananBarang" %>
<!DOCTYPE html>
<g:javascript>
$(function(){
$("#minimizeCreate").click(function(event){
         event.preventDefault();
        <g:remoteFunction action="list"
                          onLoading="jQuery('#spinner').fadeIn(1);"
                          onSuccess="bukaTableLayout()"
                          onComplete="jQuery('#spinner').fadeOut();" />
    });

});


</g:javascript>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'notaPesananBarang.label', default: 'NotaPesananBarang')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
    <div class="nav pull-right">
        <a  id="minimizeCreate" class="pull-right box-action" href="#"
            style="display: block;" target="_MINIMIZE_">&nbsp;&nbsp;<i
                class="icon-minus"></i>&nbsp;&nbsp;
        </a>
    </div>

    <div id="edit-notaPesananBarang" class="content scaffold-edit" role="main">
			<legend><g:message code="default.edit.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${notaPesananBarangInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${notaPesananBarangInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form method="post" >--}%
			<fieldset class="form">
                    <input type="hidden" name="ids" id="ids" value="">
                    <g:hiddenField name="id" value="${notaPesananBarangInstance?.id}" />
                    <g:hiddenField name="version" value="${notaPesananBarangInstance?.version}" />
                    <legend>Info NPB</legend>
                    <g:render template="form_npb_info"/>
                    <legend>Info Pemilik</legend>
                    <g:render template="form_npb_kendaraan"/>
                    <g:render template="npbDetailEdit" />
                    <a class="btn cancel" id="cancel"  href="javascript:void(0);"
                       onclick="bukaTableLayout()" ><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
                    <g:field type="button" onclick="updateNotaPesananBarang();" class="btn btn-primary create" name="tambah" id="tambah" value="${message(code: 'default.button.update.label', default: 'Update')}" />

                </fieldset>
			%{--</g:form>--}%
		</div>
	</body>
</html>
