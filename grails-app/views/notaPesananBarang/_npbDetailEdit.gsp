
<%@ page import="com.kombos.parts.NotaPesananBarang; com.kombos.parts.NotaPesananBarangDetail; com.kombos.parts.NotaPesananBarang" %>

<r:require modules="baseapplayout" />

<r:require modules="autoNumeric" />
<g:render template="../menu/maxLineDisplay"/>
<div>
    <g:field style="display: block" type="button" onclick="loadNotaPesananBarangInputModal();" class="btn cancel pull-right box-action"
             name="cari" id="cari" value="${message(code: 'notaPesananBarang.button.cariGoods.label', default: 'Add Parts')}" />
</div>
<br/><br/><br/>
<table id="notaPesananBarangDetail_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>
        <th></th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="notaPesananBarang.kode.part.label" default="Kode Part" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="notaPesananBarang.nama.part.label" default="Nama Part" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="notaPesananBarang.qty.picking.label" default="Qty" /></div>
        </th>


    </tr>
    </thead>
</table>

<g:javascript>
var notaPesananBarangDetailTable;
var reloadNotaPesananBarangFormTable;
var selectLocation;
var qtyNotaPesananBarangCek;
$(function(){

  if(notaPesananBarangDetailTable)
      notaPesananBarangDetailTable.fnDestroy();

	reloadNotaPesananBarangFormTable = function() {
		notaPesananBarangDetailTable.fnDraw();
	}


qtyNotaPesananBarangCek = function(rowId){
    var value = jQuery('#qtyNotaPesananBarang'+rowId).val();

    if(value==""){
        alert("Mohon QTY Block Stok diisi");
        return false;
    }
    else
        return true;
}

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	notaPesananBarangDetailTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	notaPesananBarangDetailTable = $('#notaPesananBarangDetail_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
	//	"bProcessing": true,
	//	"bServerSide": true,
	//	"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubList")}",
		"aoColumns": [

{
	"sName": "id",
	"mDataProp": "id",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}
,

{
	"sName": "kodeParts",
	"mDataProp": "kodeParts",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<label>&nbsp;&nbsp;'+data+'</label><input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"  checked="true"><input type="hidden" value="'+row['id']+'" id="idNotaPesananBarangDetail'+row['id']+'">&nbsp;&nbsp;&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "namaParts",
	"mDataProp": "namaParts",
	"aTargets": [1],
	"mRender": function ( data, type, row ) {
                        return '<label>'+data+'</label><input type="hidden" value="'+data+'" id="kodeParts'+row['id']+'"><input id="namaParts'+row['id']+'" type="hidden" value="'+data+'"/>';
    },
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "qty",
	"mDataProp": "jumlah",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"mRender": function ( data, type, row ) {
                        return '<input type="text" id="qtyDetail'+row['id']+'" class="inline-edit" value="'+data+'"/>';
    },
	"sWidth":"200px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                        aoData.push(
									{"name": 'idNPB', "value": "${idNPB}"}
						);



						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});


            function selectLocation(id) {
                jQuery.getJSON('${request.contextPath}/notaPesananBarang/getLocations', function (data) {
                    if (data) {
                        jQuery.each(data, function (index, value) {
                            $('#location'+id).append("<option value='" + value.id + "'>" + value.namaLokasi + "</option>");
                        });
                    }
                });

 }

 function editRow (nRow)
    {
        var idTr = '#notaPesananBarangDetail_datatables tr:eq('+nRow+')';
        var trElm = $(idTr);
        trHtml = trElm.html(); //in case need of cancel button
        var id = jQuery('#idNotaPesananBarangDetail'+nRow).val();
        var oldSatuanSatu = jQuery('#satuanSatu'+nRow).val();
        var oldSatuanDua = jQuery('#satuanDua'+nRow).val();


        var kodeParts = jQuery('#kodeParts'+nRow).val();
        var namaParts = jQuery('#namaParts'+nRow).val();
        var qtyPicking = jQuery('#qtyPickingSlip'+nRow).val();
        var oldQtyNotaPesananBarang = jQuery('#qtyNotaPesananBarang'+nRow).val();
        var location = jQuery('#location'+nRow).val();

       var htmlEdit =
                '<td class="">&nbsp;&nbsp;' +kodeParts+
'<input type="checkbox" class="pull-left row-select" aria-label="Row '+id+'" title="Select this"><input type="hidden" value="'+id+'" id="idNotaPesananBarangDetail'+nRow+'"><input type="hidden" value="'+kodeParts+'" id="kodeParts'+id+'">&nbsp;&nbsp;&nbsp;&nbsp;</a>' +
'</td>' +
                        '<td class=""><label>'+namaParts+'</label><input id="namaParts'+id+'" type="hidden" value="'+namaParts+'"/></td>' +
                        '<td class=""><label>'+qtyPicking+'</label><input id="qtyPickingSlip'+id+'" type="hidden" value="'+qtyPicking+'"/></td>' +
                        '<td class=""><label>'+oldSatuanSatu+'</label><input id="satuanSatu'+id+'" type="hidden" value="'+oldSatuanSatu+'"/></td>' +
                        '<td class=""><input id="qtyNotaPesananBarang'+id+'"  type="text" size="3" value="'+oldQtyNotaPesananBarang+'"/></td>' +
                        '<td class=""><label>'+oldSatuanDua+'</label><input id="satuanDua'+id+'" type="hidden" value="'+oldSatuanDua+'"/></td>' +
                         '<td class=""><input id="location'+id+'" type="hidden" value="'+location+'"/><select id="locationSelect"/></td>' +
                        '<td class=""><a class="edit" href="javascript:saveEditRow('+id+')">Save</a>&nbsp;/&nbsp;<a class="edit" href="javascript:cancelEditRow('+id+')">Cancel</a></td>';

        trElm.html(htmlEdit);

        selectLocation();


    }

    function cancelEditRow(nRow){
//        var idTr = '#rolePermissionDatatables_datatable tr:eq('+nRow+')';
//        var trElm = $(idTr);
//        trElm.html(trHtml);
        reloadNotaPesananBarangFormTable();
    }

    function saveEditRow(nRow){
        //do ajax
        var cekQty = qtyNotaPesananBarangCek(nRow);
        if(cekQty == false)
            return;

        var idTr = '#notaPesananBarangDetail_datatables tr:eq('+nRow+')';
        var trElm = $(idTr);
        trHtml = trElm.html(); //in case need of cancel button
        var id = jQuery('#idNotaPesananBarangDetail'+nRow).val();

		var oldSatuanSatu = jQuery('#satuanSatu'+nRow).val();
        var oldSatuanDua = jQuery('#satuanDua'+nRow).val();


        var kodeParts = jQuery('#kodeParts'+nRow).val();
        var namaParts = jQuery('#namaParts'+nRow).val();
        var qtyPicking = jQuery('#qtyPickingSlip'+nRow).val();
        var oldQtyNotaPesananBarang = jQuery('#qtyNotaPesananBarang'+nRow).val();
        var location = jQuery('#locationSelect').val();

        jQuery.ajax({
            type: 'POST',
            url: '${request.getContextPath()}/notaPesananBarang/editNotaPesananBarangDetail',
            data: {
                idNotaPesananBarangDetail : id,
				qtyNotaPesananBarang : oldQtyNotaPesananBarang,
				location : location

            },
            async: false,
            success: function(data) {
                if(data.message)
                	alert(data.message);
                else
                    alert("Your request will be submitted to request for approval");
            },
            error: function(){
                alert('Server Response Error');
            }
        });

        reloadNotaPesananBarangFormTable();
    }


</g:javascript>