<%@ page import="com.kombos.parts.NotaPesananBarang" %>
<!DOCTYPE html>
<html>
<head>
    <g:javascript>

        var updateAsUser;
        var approveNPBKabengPerNPB;
        var approveNPBPartsHAPerNPB;
        var cekDokumenKabeng;
        var cekDokumenHA;
        var cekDokumenHO;
        var unnapproveNPBKabeng;
        var konfirmasi;
        var konfirmasiUpdate;

    $(function(){


        cekDokumenKabeng = function(){
         var idNotaPesananBarang = ${notaPesananBarangInstance?.id}

            $.ajax({
                       url:'${request.contextPath}/notaPesananBarang/cekDokumenKabeng',
                                            type: "POST", // Always use POST when deleting data
                                            data : {idNPB : idNotaPesananBarang},
                                            async : false,
                                            success : function(data){
                                                if(data.statusDokumen == "0"){
                                                    toastr.error("Mohon upload dokumen kabeng terlebih dahulu");
                                                    return false;
                                                }else if(data.statusDokumen == "1"){
                                                    toastr.success("Dokumen tersedia, Proses approval dijalankan");
                                                    return true;
                                                }
                                            },
                                        error: function(xhr, textStatus, errorThrown) {
                                        alert('Internal Server Error');
                                        }
                                        });

        }

        cekDokumenHA = function(){
         var idNotaPesananBarang = ${notaPesananBarangInstance?.id}

        $.ajax({
                   url:'${request.contextPath}/notaPesananBarang/cekDokumenHA',
                                            type: "POST", // Always use POST when deleting data
                                            data : {idNPB : idNotaPesananBarang},
                                            async : false,
                                            success : function(data){
                                                if(data.statusDokumen === "0"){
                                                    toastr.error("Mohon upload dokumen HA terlebih dahulu");
                                                    return false;
                                                }else if(data.statusDokumen === "1"){
                                                    toastr.success("Dokumen tersedia, Proses approval dijalankan");
                                                    return true;
                                                }
                                            },
                                        error: function(xhr, textStatus, errorThrown) {
                                        alert('Internal Server Error');
                                        }
                                        });

        }

        cekDokumenHO = function(){
         var idNotaPesananBarang = ${notaPesananBarangInstance?.id}

                $.ajax({
                           url:'${request.contextPath}/notaPesananBarang/cekDokumenHO',
                                                        type: "POST", // Always use POST when deleting data
                                                        data : {idNPB : idNotaPesananBarang},
                                                        async : false,
                                                        success : function(data){
                                                            if(data.statusDokumen == "0"){
                                                                toastr.error("Mohon upload dokumen HO terlebih dahulu");
                                                                return false;
                                                            }else if(data.statusDokumen == "1"){
                                                                toastr.success("Dokumen tersedia, Proses update dijalankan");
                                                                return true;
                                                            }
                                                        },
                                                    error: function(xhr, textStatus, errorThrown) {
                                                    alert('Internal Server Error');
                                                    }
                                                    });

        }

        konfirmasi = function(){
            var hasil = confirm("Apakah anda yakin untuk mengapprove NPB ini?");
            return hasil;

        }

        konfirmasiUpdate = function(){
            var hasil = confirm("Apakah anda yakin untuk mengupdate NPB ini?");
            return hasil;
        }

         unnapproveNPBKabeng = function (){

                       var konfirmasiUnapprove = confirm("Anda yakin ingin UNNAPPROVE NPB ini?");
                       if(kofirmasiUnnapprove === false){
                            return;
                       }

                       var idNotaPesananBarang = ${notaPesananBarangInstance?.id}

                                    $.ajax({
                                        url:'${request.contextPath}/notaPesananBarang/unapproveKabengPerNPB',
                                                                            type: "POST", // Always use POST when deleting data
                                                                            data : {idNPB : idNotaPesananBarang},
                                                                            async : false,
                                                                            success : function(data){
                                                                                toastr.success('<div>Nota Pesanan Barang Telah di Unapprove</div>');
                                                                                $("#ids").val('');
                                                                                bukaTableLayout();
                                                                            },
                                                                        error: function(xhr, textStatus, errorThrown) {
                                                                        alert('Internal Server Error');
                                        }
                                        });
                }

        approveNPBKabengPerNPB = function (){

                        if(!konfirmasi()){
                            return;
                        }

                        if(!cekDokumenKabeng){
                            return;
                        }

                       var idNotaPesananBarang = ${notaPesananBarangInstance?.id}

                        $.ajax({
                            url:'${request.contextPath}/notaPesananBarang/approveKabengPerNPB',
                                            type: "POST", // Always use POST when deleting data
                                            data : {idNPB : idNotaPesananBarang},
                                            async : false,
                                            success : function(data){
                                                toastr.success('<div>Nota Pesanan Barang Telah di Approve</div>');
                                                $("#ids").val('');
                                                bukaTableLayout();
                                            },
                                        error: function(xhr, textStatus, errorThrown) {
                                        alert('Internal Server Error');
                                        }
                                        });
                }

              approveNPBPartsHAPerNPB = function (){

                 if(!konfirmasi()){
                    return;
                 }

                 var idNotaPesananBarang = ${notaPesananBarangInstance?.id}

                $.ajax({
                    url:'${request.contextPath}/notaPesananBarang/approvePartsHAPerNPB',
                                    type: "POST", // Always use POST when deleting data
                                    data : {idNPB : idNotaPesananBarang},
                                    async : false,
                                    success : function(data){
                                        toastr.success('<div>Nota Pesanan Barang Telah di Approve</div>');
                                        $("#ids").val('');
                                        bukaTableLayout();
                                    },
                                error: function(xhr, textStatus, errorThrown) {
                                alert('Internal Server Error');
                                }
                                });
          };


         updateAsUser = function(id){

                    if(!konfirmasiUpdate()){
                        return;
                    }


                    var formNotaPesananBarang = $("#form-notaPesananBarangDetail");

                    var noNpb = $('#noNpb').val();
                    var tahunPembuatan = $('#tahunPembuatan').val();
                    var cabangTujuan = $('#cabangTujuan').val();
                    var noSpk = $('#noSpk').val();
                    var namaPemilik = $('#namaPemilik').val();
                    var tglSpk = $('#tglSpk').val();
                    var noPolisi = $('#noPolisi').val();
                    var ketTambahan = $("#keteranganTambahan").val();
                    var noRangka = $('#noRangka').val();
                    var ketPenegasan = $('#keteranganPenegasan').val();
                    var noEngine = $('#noEngine').val();
                    var penegasanPengiriman = $('#penegasanPengiriman').val();
                    var tipeKendaraan = $('#tipeKendaraan').val();
                    var tglNpb = $('#tglNpb').val();
                    var idNPB = $('#idNPB').val();

                    var tglJKTKirimCabang = $("#tglJKTKirimCabang").val();
                    var tglJKTDiterima = $("#tglJKTDiterima").val();
                    var tglHOKeJKT = $("#tglHOKeJKT").val();
                    var tglCabangDiterima = $("#tglCabangDiterima").val();
                    var tglJKTProsesToko = $("#tglJKTProsesToko").val();
                    var tglHODiterima = $("#tglHODiterima").val();
                    var tglJKTBukaDO = $("#tglJKTBukaDO").val();

                    formNotaPesananBarang.append('<input type="hidden" id="noNpb"  value="'+noNpb+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tahunPembuatan"  value="'+tahunPembuatan+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="cabangTujuan"  value="'+cabangTujuan+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="noSpk"  value="'+noSpk+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="namaPemilik"  value="'+namaPemilik+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tglSpk"  value="'+tglSpk+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="noPolisi"  value="'+noPolisi+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="ketTambahan"  value="'+ketTambahan+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="noRangka"  value="'+noRangka+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="ketPenegasan"  value="'+ketPenegasan+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="noEngine"  value="'+noEngine+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="penegasanPengiriman"  value="'+penegasanPengiriman+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tipeKendaraan"  value="'+tipeKendaraan+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tglNpb"  value="'+tglNpb+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="idNPB"  value="'+idNPB+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tglJKTKirimCabang"  value="'+tglJKTKirimCabang+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tglJKTDiterima"  value="'+tglJKTDiterima+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tglHOKeJKT"  value="'+tglHOKeJKT+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tglCabangDiterima"  value="'+tglCabangDiterima+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tglJKTProsesToko"  value="'+tglJKTProsesToko+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tglHODiterima"  value="'+tglHODiterima+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tglJKTBukaDO"  value="'+tglJKTBukaDO+'" class="deleteafter">');



                        $.ajax({
                            url:'${request.contextPath}/notaPesananBarang/updateAsUser',
                            type: "POST", // Always use POST when deleting data
                            data : formNotaPesananBarang.serialize(),
                            async : false,
                            success : function(data){
                                toastr.success('<div>NPB Telah diupdate.</div>');
                                formNotaPesananBarang.find('.deleteafter').remove();
                                $("#ids").val('');
                                bukaTableLayout();
                            },
                        error: function(xhr, textStatus, errorThrown) {
                        alert('Internal Server Error');
                        }
                        });
                }

    })
    </g:javascript>
    <meta name="layout" content="main">
    <title>Approve/Update Nota Pesanan Barang</title>
</head>
<body>
<div id="edit-notaPesananBarang" class="content scaffold-edit" role="main">
    <legend>Approve/Update Nota Pesanan Barang</legend>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${notaPesananBarangInstance}">
        <ul class="errors" role="alert">
            <g:eachError bean="${notaPesananBarangInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
%{--<g:form method="post" >--}%
    <fieldset class="form">
        <input type="hidden" name="ids" id="ids" value="">
        <g:hiddenField name="id" value="${notaPesananBarangInstance?.id}" />
        <g:hiddenField name="version" value="${notaPesananBarangInstance?.version}" />
        <g:render template="formApproval"/>

        <g:render template="npbApprovalDetailEdit"/>
        <a class="btn cancel" id="cancel"  href="javascript:void(0);"
           onclick="bukaTableLayout()" ><g:message
                code="default.button.cancel.label" default="Cancel" /></a>
        <g:if test="${petugasNPBHO}">
         <g:field type="button" onclick="updateAsUser();" class="btn btn-primary create" name="btnUpdateAsUser" id="btnUpdateAsUser" value="${message(code: 'default.button.update.label', default: 'Update')}" />
        </g:if>
        <g:if test="${petugasNPBJKT || partsman || petugasDO}">
            <g:field type="button" onclick="updateNotaPesananBarangApprove();" class="btn btn-primary create" name="btnUpdateAsUser" id="btnUpdateAsUser" value="${message(code: 'default.button.update.label', default: 'Update')}" />
        </g:if>
        <g:if test="${kepalaBengkel}">
            <button id='btnApproveNPBKabeng' onclick="approveNPBKabengPerNPB();" type="button" class="btn btn-primary">Approve NPB as Kabeng</button>
        </g:if>
        <g:if test="${partsHA}">
            <button id='btnApprovePartsHA' onclick="approveNPBPartsHAPerNPB();" type="button" class="btn btn-primary">Approve NPB as Parts HA</button>
        </g:if>

    </fieldset>
    %{--</g:form>--}%
</div>
</body>
</html>
