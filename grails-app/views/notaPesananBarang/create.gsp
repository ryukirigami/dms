<%@ page import="com.kombos.parts.NotaPesananBarang" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'notaPesananBarang.label', default: 'Nota Pesanan Barang')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
<g:javascript>
	    $("#notaPesananBarangInputModal").on("show", function() {
		$("#notaPesananBarangInputModal .btn").on("click", function(e) {
			$("#notaPesananBarangInputModal").modal('hide');
		});
	});
	$("#notaPesananBarangInputModal").on("hide", function() {
		$("#notaPesananBarangInputModal a.btn").off("click");
	});

   	loadNotaPesananBarangInputModal = function(){

		$("#notaPesananBarangInputContent").empty();
		$.ajax({type:'POST', url:'${request.contextPath}/notaPesananBarang/inputGoods',
   			success:function(data,textStatus){
					$("#notaPesananBarangInputContent").html(data);
				//	$("#modal-dialog").css({'width': '1200px', 'height' : '600px'});
					$("#notaPesananBarangInputModal").modal({
						"backdrop" : "static",
						"keyboard" : true,
						"show" : true
					}).css({'width': '1200px','margin-left': function () {
            return -($(this).outerWidth() / 2);
        }});

   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    }

    tambahGoods = function(){
           checkGoods =[];
            $("#blokStokDetail-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    checkGoods.push(id);
                }
            });
            if(checkGoods.length<1){
                alert('Anda belum memilih data yang akan ditambahkan');
                loadNotaPesananBarangInputsModal();
            }
                var checkGoodsMap = JSON.stringify(checkGoods);
                $.ajax({
                url:'${request.contextPath}/notaPesananBarang/tambah',
                type: "POST", // Always use POST when deleting data
                data : { ids: checkGoodsMap },
                success : function(data){
                    $('#result_search').empty();
                    reloadNotaPesananBarangFormTable();
                },
                error: function(xhr, textStatus, errorThrown) {
                    alert('Internal Server Error');
                }
                });

        checkGoods = [];

    }


     $("#minimizeCreate").click(function(event){
         event.preventDefault();
        <g:remoteFunction action="list"
                          onLoading="jQuery('#spinner').fadeIn(1);"
                          onSuccess="bukaTableLayout()"
                          onComplete="jQuery('#spinner').fadeOut();" />
        });



function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}
        </g:javascript>
	</head>
	<body>

    <div class="nav pull-right">
        <a  id="minimizeCreate" class="pull-right box-action" href="#"
            style="display: block;" target="_MINIMIZE_">&nbsp;&nbsp;<i
                class="icon-minus"></i>&nbsp;&nbsp;
        </a>
    </div>


    <div id="create-notaPesananBarang" class="content scaffold-create" role="main">
			<legend><g:message code="default.create.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${notaPesananBarangInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${notaPesananBarangInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form action="save" >--}%
             <fieldset class="form">
                 <legend>Info NPB</legend>
                 <g:render template="form_npb_info"/>
                 <legend>Info Pemilik</legend>
                 <g:render template="form_npb_kendaraan"/>

                 <g:if test="${flash.message}">
                            <div class="message" role="status">
                                ${flash.message}
                            </div>
                        </g:if>
                    <input type="hidden" name="ids" id="ids" value="">

                  <g:render template="dataTablesNPBDetail" />
				</fieldset>
               %{--</g:form>--}%
    	</div>


    </body>
</html>
