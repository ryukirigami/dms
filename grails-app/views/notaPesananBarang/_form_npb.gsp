<%@ page import="com.kombos.parts.NotaPesananBarang" %>
<table>
    <tr>
        <td>
            <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'kelompokNPB', 'error')} ">
                <label class="control-label" for="kelompokNPB">
                    <g:message code="notaPesananBarang.kelompokNPB.label" default="Kelompok Npb" />

                </label>
                <div class="controls">
                    <g:select id="kelompokNPB" name="kelompokNPB.id" from="${com.kombos.administrasi.KelompokNPB.list()}" optionKey="id" required="" value="${notaPesananBarangInstance?.kelompokNPB?.id}" class="many-to-one"/>
                </div>
            </div>
        </td>
        <td>
            <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'tahunPembuatan', 'error')} ">
                <label class="control-label" for="tahunPembuatan">
                    <g:message code="notaPesananBarang.tahunPembuatan.label" default="Tahun Pembuatan" />

                </label>
                <div class="controls">
                    <g:field type="number" name="tahunPembuatan" id="tahunPembuatan" value="${notaPesananBarangInstance.tahunPembuatan}" />
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'tglNpb', 'error')} ">
                <label class="control-label" for="tglNpb">
                    <g:message code="notaPesananBarang.tglNpb.label" default="Tgl Npb" />

                </label>
                <div class="controls">
                    <ba:datePicker name="tglNpb" id="tglNpb" precision="day" value="${notaPesananBarangInstance?.tglNpb}" format="yyyy-MM-dd"/>
                </div>
            </div>
        </td>
        <td><div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'cabangTujuan', 'error')} ">
            <label class="control-label" for="cabangTujuan">
                <g:message code="notaPesananBarang.cabangTujuan.label" default="Cabang Tujuan" />

            </label>
            <div class="controls">
                <g:select id="cabangTujuan" name="cabangTujuan.id" from="${com.kombos.administrasi.CompanyDealer.list()}" optionKey="id" required="" value="${notaPesananBarangInstance?.cabangTujuan?.id}" class="many-to-one"/>
            </div>
        </div>
        </td>

    </tr>
    <tr>
        <td>
            <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'noSpk', 'error')} ">
                <label class="control-label" for="noSpk">
                    <g:message code="notaPesananBarang.noSpk.label" default="No Spk" />

                </label>
                <div class="controls">
                    <g:textField name="noSpk" id="noSpk" value="${notaPesananBarangInstance?.noSpk}" />
                </div>
            </div>
        </td>

        <td>
            <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'namaPemilik', 'error')} ">
                <label class="control-label" for="namaPemilik">
                    <g:message code="notaPesananBarang.namaPemilik.label" default="Nama Pemilik" />

                </label>
                <div class="controls">
                    <g:textField name="namaPemilik" id="namaPemilik" value="${notaPesananBarangInstance?.namaPemilik}" />
                </div>
            </div>
        </td>
        <td>
            <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'tglSpk', 'error')} ">
                <label class="control-label" for="tglSpk">
                    <g:message code="notaPesananBarang.tglSpk.label" default="Tgl Spk" />

                </label>
                <div class="controls">
                    <ba:datePicker name="tglSpk" id="tglSpk" precision="day" value="${notaPesananBarangInstance?.tglSpk}" format="yyyy-MM-dd"/>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'noPolisi', 'error')} ">
                <label class="control-label" for="noPolisi">
                    <g:message code="notaPesananBarang.noPolisi.label" default="No Polisi" />

                </label>
                <div class="controls">
                    <g:textField name="noPolisi" id="noPolisi" value="${notaPesananBarangInstance?.noPolisi}" />
                </div>
            </div>
        </td>
        <td>
            <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'keteranganTambahan', 'error')} ">
                <label class="control-label" for="keteranganTambahan">
                    <g:message code="notaPesananBarang.keteranganTambahan.label" default="Keterangan Tambahan" />

                </label>
                <div class="controls">
                    <g:textField name="keteranganTambahan" id="keteranganTambahan" value="${notaPesananBarangInstance?.keteranganTambahan}" />
                </div>
            </div>

        </td>
    </tr>
    <tr>
        <td>
            <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'noRangka', 'error')} ">
                <label class="control-label" for="noRangka">
                    <g:message code="notaPesananBarang.noRangka.label" default="No Rangka" />

                </label>
                <div class="controls">
                    <g:textField name="noRangka" id="noRangka" value="${notaPesananBarangInstance?.noRangka}" />
                </div>
            </div>

        </td>
        <td>
            <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'keteranganPenegasan', 'error')} ">
                <label class="control-label" for="keteranganPenegasan">
                    <g:message code="notaPesananBarang.keteranganPenegasan.label" default="Keterangan Penegasan" />

                </label>
                <div class="controls">
                    <g:textField name="keteranganPenegasan" id="keteranganPenegasan" value="${notaPesananBarangInstance?.keteranganPenegasan}" />
                </div>
            </div>


        </td>
    </tr>
    <tr>
        <td>
            <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'noEngine', 'error')} ">
                <label class="control-label" for="noEngine">
                    <g:message code="notaPesananBarang.noEngine.label" default="No Engine" />

                </label>
                <div class="controls">
                    <g:textField name="noEngine" id="noEngine" value="${notaPesananBarangInstance?.noEngine}" />
                </div>
            </div>
        </td>
        <td><div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'penegasanPengiriman', 'error')} ">
            <label class="control-label" for="penegasanPengiriman">
                <g:message code="notaPesananBarang.penegasanPengiriman.label" default="Penegasan Pengiriman" />

            </label>
            <div class="controls">
                <g:select id="penegasanPengiriman" name="penegasanPengiriman.id" from="${com.kombos.administrasi.PenegasanPengiriman.list()}" optionKey="id" required="" value="${notaPesananBarangInstance?.penegasanPengiriman?.id}" class="many-to-one"/>
            </div>
        </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'tipeKendaraan', 'error')} ">
                <label class="control-label" for="tipeKendaraan">
                    <g:message code="notaPesananBarang.tipeKendaraan.label" default="Tipe Kendaraan" />

                </label>
                <div class="controls">
                    <g:textField name="tipeKendaraan" id="tipeKendaraan" value="${notaPesananBarangInstance?.tipeKendaraan}" />
                </div>
            </div>
        </td>

    </tr>
</table>
