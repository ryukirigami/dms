<%@ page import="com.kombos.parts.NotaPesananBarang" %>
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>
<table id="notaPesananBarang_datatables_${idTable}" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
<thead>
<tr>
    <th></th>

    <th style="border-bottom: none;padding: 5px;">
        <div><g:message code="notaPesananBarang.noNpb.label" default="No Npb" /></div>
    </th>

    <th style="border-bottom: none;padding: 5px;">
        <div><g:message code="notaPesananBarang.tglNpb.label" default="Tgl Npb" /></div>
    </th>


    <th style="border-bottom: none;padding: 5px;">
        <div><g:message code="notaPesananBarang.tglSpk.label" default="Tgl Spk" /></div>
    </th>

    <th style="border-bottom: none;padding: 5px;">
        <div><g:message code="notaPesananBarang.namaPemilik.label" default="Nama Pemilik" /></div>
    </th>
    <th style="border-bottom: none;padding: 5px;">
        <div><g:message code="notaPesananBarang.tipeKendaraan.label" default="Tipe Kendaraan" /></div>
    </th>
    <th style="border-bottom: none;padding: 5px;">
        <div><g:message code="notaPesananBarang.noEngine.label" default="No Engine" /></div>
    </th>

    <th style="border-bottom: none;padding: 5px;">
        <div><g:message code="notaPesananBarang.noPolisi.label" default="No Polisi" /></div>
    </th>


    <th style="border-bottom: none;padding: 5px;">
        <div><g:message code="notaPesananBarang.noRangka.label" default="No Rangka" /></div>
    </th>

    <th style="border-bottom: none;padding: 5px;">
        <div><g:message code="notaPesananBarang.tahunPembuatan.label" default="Tahun Pembuatan" /></div>
    </th>

    <th style="border-bottom: none;padding: 5px;">
        <div><g:message code="notaPesananBarang.noSpk.label" default="No Spk" /></div>
    </th>

    <th style="border-bottom: none;padding: 5px;">
        <div><g:message code="notaPesananBarang.cabangPengirim.label" default="Cabang Pengirim" /></div>
    </th>


    <th style="border-bottom: none;padding: 5px;">
        <div><g:message code="notaPesananBarang.cabangTujuan.label" default="Cabang Tujuan" /></div>
    </th>


    <th style="border-bottom: none;padding: 5px;">
        <div><g:message code="notaPesananBarang.penegasanPengiriman.label" default="Penegasan Pengiriman" /></div>
    </th>

    <th style="border-bottom: none;padding: 5px;">
        <div><g:message code="notaPesananBarang.keteranganPenegasan.label" default="Keterangan Penegasan" /></div>
    </th>


    <th style="border-bottom: none;padding: 5px;">
        <div><g:message code="notaPesananBarang.keteranganTambahan.label" default="Keterangan Tambahan" /></div>
    </th>

    <th style="border-bottom: none;padding: 5px;">
        <div><g:message code="notaPesananBarang.pemesan.label" default="Pemesan" /></div>
    </th>

    <th style="border-bottom: none;padding: 5px;">
        <div><g:message code="notaPesananBarang.staApproval.label" default="Approval Kabeng" /></div>
    </th>

    <th style="border-bottom: none;padding: 5px;">
        <div><g:message code="notaPesananBarang.tglApproveUnApprove.label" default="Tgl Approve Un Approve" /></div>
    </th>


    <th style="border-bottom: none;padding: 5px;">
        <div><g:message code="notaPesananBarang.tglApproveUnApprove.label" default="Download File" /></div>
    </th>
</tr>
<tr>

    <th></th>

    <th style="border-top: none;padding: 5px;">
        <div id="filter_noNpb" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
            <input type="text" name="search_noNpb" class="search_init" />
        </div>
    </th>

    <th style="border-top: none;padding: 5px;">
        <div id="filter_tglNpb" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
            <input type="hidden" name="search_tglNpb" value="date.struct">
            <input type="hidden" name="search_tglNpb_day" id="search_tglNpb_day" value="">
            <input type="hidden" name="search_tglNpb_month" id="search_tglNpb_month" value="">
            <input type="hidden" name="search_tglNpb_year" id="search_tglNpb_year" value="">
            <input type="text" data-date-format="dd/mm/yyyy" name="search_tglNpb_dp" value="" id="search_tglNpb" class="search_init">
        </div>
    </th>

    <th style="border-top: none;padding: 5px;">
        <div id="filter_tglSpk" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
            <input type="hidden" name="search_tglSpk" value="date.struct">
            <input type="hidden" name="search_tglSpk_day" id="search_tglSpk_day" value="">
            <input type="hidden" name="search_tglSpk_month" id="search_tglSpk_month" value="">
            <input type="hidden" name="search_tglSpk_year" id="search_tglSpk_year" value="">
            <input type="text" data-date-format="dd/mm/yyyy" name="search_tglSpk_dp" value="" id="search_tglSpk" class="search_init">
        </div>
    </th>

    <th style="border-top: none;padding: 5px;">
        <div id="filter_namaPemilik" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
            <input type="text" name="search_namaPemilik" class="search_init" />
        </div>
    </th>

    <th style="border-top: none;padding: 5px;">
        <div id="filter_tipeKendaraan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
            <input type="text" name="search_tipeKendaraan" class="search_init" />
        </div>
    </th>

    <th style="border-top: none;padding: 5px;">
        <div id="filter_noEngine" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
            <input type="text" name="search_noEngine" class="search_init" />
        </div>
    </th>

    <th style="border-top: none;padding: 5px;">
        <div id="filter_noPolisi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
            <input type="text" name="search_noPolisi" class="search_init" />
        </div>
    </th>

    <th style="border-top: none;padding: 5px;">
        <div id="filter_noRangka" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
            <input type="text" name="search_noRangka" class="search_init" />
        </div>
    </th>

    <th style="border-top: none;padding: 5px;">
        <div id="filter_tahunPembuatan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
            <input type="text" name="search_tahunPembuatan" class="search_init" />
        </div>
    </th>

    <th style="border-top: none;padding: 5px;">
        <div id="filter_noSpk" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
            <input type="text" name="search_noSpk" class="search_init" />
        </div>
    </th>

    <th style="border-top: none;padding: 5px;">
        <div id="filter_cabangPengirim" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
            <input type="text" name="search_cabangPengirim" class="search_init" />
        </div>
    </th>

    <th style="border-top: none;padding: 5px;">
        <div id="filter_cabangTujuan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
            <input type="text" name="search_cabangTujuan" class="search_init" />
        </div>
    </th>


    <th style="border-top: none;padding: 5px;">
        <div id="filter_penegasanPengiriman" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
            <input type="text" name="search_penegasanPengiriman" class="search_init" />
        </div>
    </th>


    <th style="border-top: none;padding: 5px;">
        <div id="filter_keteranganPenegasan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
            <input type="text" name="search_keteranganPenegasan" class="search_init" />
        </div>
    </th>

    <th style="border-top: none;padding: 5px;">
        <div id="filter_keteranganTambahan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
            <input type="text" name="search_keteranganTambahan" class="search_init" />
        </div>
    </th>

    <th style="border-top: none;padding: 5px;">
        <div id="filter_pemesan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
            <input type="text" name="search_pemesan" class="search_init" />
        </div>
    </th>

    <th style="border-top: none;padding: 5px;">
        <div id="filter_staApproval" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
            <input type="text" name="search_staApproval" class="search_init" />
        </div>
    </th>


    <th style="border-top: none;padding: 5px;">
        <div id="filter_tglApproveUnApprove" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
            <input type="hidden" name="search_tglApproveUnApprove" value="date.struct">
            <input type="hidden" name="search_tglApproveUnApprove_day" id="search_tglApproveUnApprove_day" value="">
            <input type="hidden" name="search_tglApproveUnApprove_month" id="search_tglApproveUnApprove_month" value="">
            <input type="hidden" name="search_tglApproveUnApprove_year" id="search_tglApproveUnApprove_year" value="">
            <input type="text" data-date-format="dd/mm/yyyy" name="search_tglApproveUnApprove_dp" value="" id="search_tglApproveUnApprove" class="search_init">
        </div>
    </th>


    <th style="border-top: none;padding: 5px;">
        <div id="filter_foto" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                &nbsp;
        </div>
    </th>


</tr>
</thead>
</table>

<g:javascript>
var notaPesananBarangTable;
var otoritas = "${otoritas}";
var reloadNotaPesananBarangTable;
$(function(){

	$('#search_tglSpk').datepicker().on('changeDate', function(ev) {
                    var newDate = new Date(ev.date);
                    $('#search_tglSpk_day').val(newDate.getDate());
                    $('#search_tglSpk_month').val(newDate.getMonth()+1);
                    $('#search_tglSpk_year').val(newDate.getFullYear());
                    $(this).datepicker('hide');
                    notaPesananBarangTable.fnDraw();
            });

            $('#search_tglNpb').datepicker().on('changeDate', function(ev) {
                    var newDate = new Date(ev.date);
                    $('#search_tglNpb_day').val(newDate.getDate());
                    $('#search_tglNpb_month').val(newDate.getMonth()+1);
                    $('#search_tglNpb_year').val(newDate.getFullYear());
                    $(this).datepicker('hide');
                    notaPesananBarangTable.fnDraw();
            });


    $('#clear').click(function(e){
            $('#search_t157TglNotaPesananBarang').val("");
            $('#search_t157TglNotaPesananBarangAkhir').val("");

        });
    $('#view').click(function(e){
        e.stopPropagation();
		notaPesananBarangTable.fnDraw();
//        reloadAuditTrailTable();
	});

var anOpen = [];
	$('#notaPesananBarang_datatables_${idTable} td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = notaPesananBarangTable.fnGetData(nTr);

    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${request.contextPath}/notaPesananBarang/sublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = notaPesananBarangTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});

  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			notaPesananBarangTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});


	reloadNotaPesananBarangTable = function() {
		notaPesananBarangTable.fnDraw();
	}


	$('#search_t157TglNotaPesananBarang').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t157TglNotaPesananBarang_day').val(newDate.getDate());
			$('#search_t157TglNotaPesananBarang_month').val(newDate.getMonth()+1);
			$('#search_t157TglNotaPesananBarang_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			notaPesananBarangTable.fnDraw();
	});




$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	notaPesananBarangTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	notaPesananBarangTable = $('#notaPesananBarang_datatables_${idTable}').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "nbpApprovalDatatables")}",
		"aoColumns": [
{
               "sName": "id",
               "mDataProp": null,
               "sClass": "control center",
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "noNpb",
	"mDataProp": "noNpb",
	"aTargets": [9],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
//		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "tglNpb",
	"mDataProp": "tglNpb",
	"aTargets": [18],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "tglSpk",
	"mDataProp": "tglSpk",
	"aTargets": [19],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "namaPemilik",
	"mDataProp": "namaPemilik",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "tipeKendaraan",
	"mDataProp": "tipeKendaraan",
	"aTargets": [20],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "noEngine",
	"mDataProp": "noEngine",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,


{
	"sName": "noPolisi",
	"mDataProp": "noPolisi",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "noRangka",
	"mDataProp": "noRangka",
	"aTargets": [11],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "tahunPembuatan",
	"mDataProp": "tahunPembuatan",
	"aTargets": [16],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "noSpk",
	"mDataProp": "noSpk",
	"aTargets": [12],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "cabangPengirim",
	"mDataProp": "cabangPengirim",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "cabangTujuan",
	"mDataProp": "cabangTujuan",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "penegasanPengiriman",
	"mDataProp": "penegasanPengiriman",
	"aTargets": [14],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "keteranganPenegasan",
	"mDataProp": "keteranganPenegasan",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "keteranganTambahan",
	"mDataProp": "keteranganTambahan",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "pemesan",
	"mDataProp": "pemesan",
	"aTargets": [13],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,



{
	"sName": "staApproval",
	"mDataProp": "staApproval",
	"aTargets": [15],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "tglApproveUnApprove",
	"mDataProp": "tglApproveUnApprove",
	"aTargets": [17],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},

{
	"sName": "foto",
	"mDataProp": "foto",
	"aTargets": [17],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
	    if(otoritas=="petugasNPBHO" && row['fotoKabeng']!=""){
		    return '<a href="javascript:void(0);" onClick="showFoto('+row['id']+',otoritas)">Download</a>';
	    }else if(otoritas=="petugasNPBJKT" && row['fotoHo']!=""){
		    return '<a href="javascript:void(0);" onClick="showFoto('+row['id']+',otoritas)">Download</a>';
	    }else if(row['cd'] == row['cabangDiteruskan'] && row['fotoHo']!=""){
		    return '<a href="javascript:void(0);" onClick="showFoto('+row['id']+',"petugasNPBJKT")">Download</a>';
	    }else{
	        return "-"
	    }

	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}



],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var alasanUnApprove = $('#filter_alasanUnApprove input').val();
						if(alasanUnApprove){
							aoData.push(
									{"name": 'sCriteria_alasanUnApprove', "value": alasanUnApprove}
							);
						}

						var cabangPengirim = $('#filter_cabangPengirim input').val();
						if(cabangPengirim){
							aoData.push(
									{"name": 'sCriteria_cabangPengirim', "value": cabangPengirim}
							);
						}

						var cabangTujuan = $('#filter_cabangTujuan input').val();
						if(cabangTujuan){
							aoData.push(
									{"name": 'sCriteria_cabangTujuan', "value": cabangTujuan}
							);
						}

						var keteranganPenegasan = $('#filter_keteranganPenegasan input').val();
						if(keteranganPenegasan){
							aoData.push(
									{"name": 'sCriteria_keteranganPenegasan', "value": keteranganPenegasan}
							);
						}

						var keteranganTambahan = $('#filter_keteranganTambahan input').val();
						if(keteranganTambahan){
							aoData.push(
									{"name": 'sCriteria_keteranganTambahan', "value": keteranganTambahan}
							);
						}

						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						var namaPemilik = $('#filter_namaPemilik input').val();
						if(namaPemilik){
							aoData.push(
									{"name": 'sCriteria_namaPemilik', "value": namaPemilik}
							);
						}

						var namaUserApproveUnApprove = $('#filter_namaUserApproveUnApprove input').val();
						if(namaUserApproveUnApprove){
							aoData.push(
									{"name": 'sCriteria_namaUserApproveUnApprove', "value": namaUserApproveUnApprove}
							);
						}

						var noEngine = $('#filter_noEngine input').val();
						if(noEngine){
							aoData.push(
									{"name": 'sCriteria_noEngine', "value": noEngine}
							);
						}

						var noNpb = $('#filter_noNpb input').val();
						if(noNpb){
							aoData.push(
									{"name": 'sCriteria_noNpb', "value": noNpb}
							);
						}

						var noPolisi = $('#filter_noPolisi input').val();
						if(noPolisi){
							aoData.push(
									{"name": 'sCriteria_noPolisi', "value": noPolisi}
							);
						}

						var noRangka = $('#filter_noRangka input').val();
						if(noRangka){
							aoData.push(
									{"name": 'sCriteria_noRangka', "value": noRangka}
							);
						}

						var noSpk = $('#filter_noSpk input').val();
						if(noSpk){
							aoData.push(
									{"name": 'sCriteria_noSpk', "value": noSpk}
							);
						}

						var pemesan = $('#filter_pemesan input').val();
						if(pemesan){
							aoData.push(
									{"name": 'sCriteria_pemesan', "value": pemesan}
							);
						}

						var penegasanPengiriman = $('#filter_penegasanPengiriman input').val();
						if(penegasanPengiriman){
							aoData.push(
									{"name": 'sCriteria_penegasanPengiriman', "value": penegasanPengiriman}
							);
						}

						var staApproval = $('#filter_staApproval input').val();
						if(staApproval){
							aoData.push(
									{"name": 'sCriteria_staApproval', "value": staApproval}
							);
						}

						var tahunPembuatan = $('#filter_tahunPembuatan input').val();
						if(tahunPembuatan){
							aoData.push(
									{"name": 'sCriteria_tahunPembuatan', "value": tahunPembuatan}
							);
						}

						var tglApproveUnApprove = $('#search_tglApproveUnApprove').val();
						var tglApproveUnApproveDay = $('#search_tglApproveUnApprove_day').val();
						var tglApproveUnApproveMonth = $('#search_tglApproveUnApprove_month').val();
						var tglApproveUnApproveYear = $('#search_tglApproveUnApprove_year').val();

						if(tglApproveUnApprove){
							aoData.push(
									{"name": 'sCriteria_tglApproveUnApprove', "value": "date.struct"},
									{"name": 'sCriteria_tglApproveUnApprove_dp', "value": tglApproveUnApprove},
									{"name": 'sCriteria_tglApproveUnApprove_day', "value": tglApproveUnApproveDay},
									{"name": 'sCriteria_tglApproveUnApprove_month', "value": tglApproveUnApproveMonth},
									{"name": 'sCriteria_tglApproveUnApprove_year', "value": tglApproveUnApproveYear}
							);
						}

						var tglNpb = $('#search_tglNpb').val();
						var tglNpbDay = $('#search_tglNpb_day').val();
						var tglNpbMonth = $('#search_tglNpb_month').val();
						var tglNpbYear = $('#search_tglNpb_year').val();

						if(tglNpb){
							aoData.push(
									{"name": 'sCriteria_tglNpb', "value": "date.struct"},
									{"name": 'sCriteria_tglNpb_dp', "value": tglNpb},
									{"name": 'sCriteria_tglNpb_day', "value": tglNpbDay},
									{"name": 'sCriteria_tglNpb_month', "value": tglNpbMonth},
									{"name": 'sCriteria_tglNpb_year', "value": tglNpbYear}
							);
						}

						var tglSpk = $('#search_tglSpk').val();
						var tglSpkDay = $('#search_tglSpk_day').val();
						var tglSpkMonth = $('#search_tglSpk_month').val();
						var tglSpkYear = $('#search_tglSpk_year').val();

						if(tglSpk){
							aoData.push(
									{"name": 'sCriteria_tglSpk', "value": "date.struct"},
									{"name": 'sCriteria_tglSpk_dp', "value": tglSpk},
									{"name": 'sCriteria_tglSpk_day', "value": tglSpkDay},
									{"name": 'sCriteria_tglSpk_month', "value": tglSpkMonth},
									{"name": 'sCriteria_tglSpk_year', "value": tglSpkYear}
							);
						}

						var tipeKendaraan = $('#filter_tipeKendaraan input').val();
						if(tipeKendaraan){
							aoData.push(
									{"name": 'sCriteria_tipeKendaraan', "value": tipeKendaraan}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>