<table>
<tr>
    <td>
        <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'kelompokNPB', 'error')} ">
            <label class="control-label" for="kelompokNPB">
                <g:message code="notaPesananBarang.kelompokNPB.label" default="Kelompok NPB" />

            </label>
            <div class="controls">
                <g:select id="kelompokNPB" name="kelompokNPB.id" from="${com.kombos.administrasi.KelompokNPB.list()}" optionKey="id" required="" value="${notaPesananBarangInstance?.kelompokNPB?.id}" class="many-to-one"/>
            </div>
        </div>
    </td>
    <td><div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'penegasanPengiriman', 'error')} ">
        <label class="control-label" for="penegasanPengiriman">
            <g:message code="notaPesananBarang.penegasanPengiriman.label" default="Penegasan Pengiriman" />

        </label>
        <div class="controls">
            <g:select id="penegasanPengiriman" name="penegasanPengiriman.id" from="${com.kombos.administrasi.PenegasanPengiriman.list()}" optionKey="id" required="" value="${notaPesananBarangInstance?.penegasanPengiriman?.id}" class="many-to-one"/>

        </div>
    </div>
    </td>

</tr>
<tr>
    <td>
        <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'tglNpb', 'error')} ">
            <label class="control-label" for="tglNpb">
                <g:message code="notaPesananBarang.tglNpb.label" default="Tgl Npb" />

            </label>
            <div class="controls">
                <ba:datePicker name="tglNpb" id="tglNpb" precision="day" value="${notaPesananBarangInstance?.tglNpb}" format="yyyy-MM-dd"/>
            </div>
        </div>
    </td>
    <td><div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'keteranganPenegasan', 'error')} ">
        <label class="control-label" for="keteranganPenegasan">
            <g:message code="notaPesananBarang.penegasanPengiriman.label" default="Keterangan Penegasan" />

        </label>
        <div class="controls">
            <g:textField id="keteranganPenegasan" name="keteranganPenegasan" value="${notaPesananBarangInstance?.keteranganPenegasan}"/>
        </div>
    </div>
    </td>
</tr>
<tr>

    <td><div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'cabangTujuan', 'error')} ">
        <label class="control-label" for="cabangTujuan">
            <g:message code="notaPesananBarang.cabangTujuan.label" default="Cabang Tujuan" />

        </label>
        <div class="controls">
            <g:select id="cabangTujuan" name="cabangTujuan.id" from="${com.kombos.administrasi.CompanyDealer.list()}" optionKey="id" required="" value="${notaPesananBarangInstance?.cabangTujuan?.id}" class="many-to-one"/>
        </div>
    </div>
    </td>


</td>

</tr>
<tr>
    <td>
        <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'noSpk', 'error')} ">
            <label class="control-label" for="noSpk">
                <g:message code="notaPesananBarang.noSpk.label" default="No Spk" />

            </label>
            <div class="controls">
                <g:textField  name="noSpk"  required="" id="noSpk" value="${notaPesananBarangInstance?.noSpk}" />
            </div>
        </div>
    </td>


</tr>
<tr>
    <td>
        <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'tglSpk', 'error')} ">
            <label class="control-label" for="tglSpk">
                <g:message code="notaPesananBarang.tglSpk.label" default="Tgl Spk" />

            </label>
            <div class="controls">
                <ba:datePicker name="tglSpk"  required="" id="tglSpk" precision="day" value="${notaPesananBarangInstance?.tglSpk}" format="yyyy-MM-dd"/>
            </div>
        </div>
    </td>



</tr>
<tr>
    <td>
        <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'keteranganTambahan', 'error')} ">
            <label class="control-label" for="keteranganTambahan">
                <g:message code="notaPesananBarang.keteranganTambahan.label" default="Keterangan Tambahan" />

            </label>
            <div class="controls">
                <g:textField  name="keteranganTambahan" id="keteranganTambahan" value="${notaPesananBarangInstance?.keteranganTambahan}" />
            </div>
        </div>

    </td>
</tr>

</table>