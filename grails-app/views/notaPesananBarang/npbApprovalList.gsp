
<%@ page import="com.kombos.parts.NotaPesananBarang" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName" value="${message(code: 'notaPesananBarang.label', default: 'Nota Pesanan Barang')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
<r:require modules="baseapplayout, baseapplist, autoNumeric, ajaxfileupload" />
<g:javascript>
			var show;
			var edit;
			var addGoods;
            var addEditGoods;
			var printNotaPesananBarangSlip;
			var printNotaPesananBarangSlipCont;
			var showNotaPesananBarangDetail;
			var saveNotaPesananBarang;
			var loadNotaPesananBarangEditModal;
			var massDeleteNotaPesananBarang;
			var approveNPBKabeng;
			var approveNPBPartsHA;
		    var approveNPBKabengPerNPB;
			var approveNPBPartsHAPerNPB;
		    var updateAsUser;
		    var updateNotaPesananBarangApprove;


			function isNumberKey(evt)
            {
                var charCode = (evt.which) ? evt.which : evt.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;

                return true;
            }

			$(function(){


			  approveNPBKabeng = function (){
                       checkNotaPesananBarang =[];
                       var idNotaPesananBarang;
                        $("#notaPesananBarang-table tbody .row-select").each(function() {
                            if(this.checked){
                                idNotaPesananBarang = $(this).next("input:hidden").val();
                               checkNotaPesananBarang.push(idNotaPesananBarang);
                            }
                        });
                        if(checkNotaPesananBarang.length<1){
                            alert('Anda belum memilih data yang akan ditambahkan');
                            return;

                        }

                        var idNotaPesananBarangs = JSON.stringify(checkNotaPesananBarang);

                        $.ajax({
                            url:'${request.contextPath}/notaPesananBarang/approveKabeng',
                            type: "POST", // Always use POST when deleting data
                            data : {idNPBs : idNotaPesananBarangs},
                            async : false,
                            success : function(data){
                                toastr.success('<div>Nota Pesanan Barang Telah di Approve</div>');
                                $("#ids").val('');
                                bukaTableLayout();
                            },
                        error: function(xhr, textStatus, errorThrown) {
                        alert('Internal Server Error');
                        }
                        });
                }

              approveNPBPartsHA = function (){
                       checkNotaPesananBarang =[];
                       var idNotaPesananBarang;
                        $("#notaPesananBarang-table tbody .row-select").each(function() {
                            if(this.checked){
                                idNotaPesananBarang = $(this).next("input:hidden").val();
                               checkNotaPesananBarang.push(idNotaPesananBarang);
                            }
                        });
                        if(checkNotaPesananBarang.length<1){
                            alert('Anda belum memilih data yang akan ditambahkan');
                            return;

                        }

                        var idNotaPesananBarangs = JSON.stringify(checkNotaPesananBarang);

                        $.ajax({
                            url:'${request.contextPath}/notaPesananBarang/approvePartsHA',
                            type: "POST", // Always use POST when deleting data
                            data : {idNPBs : idNotaPesananBarangs},
                            async : false,
                            success : function(data){
                                toastr.success('<div>Nota Pesanan Barang Telah di Approve</div>');
                                $("#ids").val('');
                                bukaTableLayout();
                            },
                        error: function(xhr, textStatus, errorThrown) {
                        alert('Internal Server Error');
                        }
                        });
                }






             showNotaPesananBarangDetail = function(){
                        $(".span7").css("display","none");
                        $(".span12").css("display","block");
                    }

				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :
							shrinkTableLayout();
					//	showNotaPesananBarangDetail();
							<g:remoteFunction action="create"
                                              onLoading="jQuery('#spinner').fadeIn(1);"
                                              onSuccess="loadFormInstance('notaPesananBarang', data, textStatus);"
                                              onComplete="jQuery('#spinner').fadeOut();" />
                break;
                case '_MINIMIZE_' :

                <g:remoteFunction action="list"
                                  onLoading="jQuery('#spinner').fadeIn(1);"
                                  onSuccess="bukaTableLayout()"
                                  onComplete="jQuery('#spinner').fadeOut();" />
                break;
                case '_DELETE_' :
                bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
                                            function(result){
                                                if(result){
                                                    massDeleteNotaPesananBarang();
                                                }
                                    });

         		break;
				   }
				   return false;
				});

				massDeleteNotaPesananBarang = function() {
                    var recordsToDelete = [];

                    $("#notaPesananBarang-table tbody .row-select").each(function() {
                        if(this.checked){
                            var id = $(this).next("input:hidden").val();
                   		    recordsToDelete.push(id);
                        }
                    });

                    var json = JSON.stringify(recordsToDelete);

                    $.ajax({
                        url:'${request.contextPath}/notaPesananBarang/massdelete',
                        type: "POST", // Always use POST when deleting data
                        data: { ids: json },
                        complete: function(xhr, status) {
                            notaPesananBarangTable.fnDraw();
                             toastr.success('<div>Block Stok Telah dihapus.</div>');

                        }
                    });

                }


				show = function(id) {
					showInstance('notaPesananBarang','${request.contextPath}/notaPesananBarang/show/'+id);
				};

				edit = function(id) {
					editInstance('notaPesananBarang','${request.contextPath}/notaPesananBarang/editNPBApproval/'+id);
				};

				shrinkTableLayout = function(){
                    $("#notaPesananBarang-table").hide();
                    $("#notaPesananBarang-form").show();
                }

                bukaTableLayout = function(){
                    $("#notaPesananBarang-table").show();
                    $("#notaPesananBarang-form").hide();
                    notaPesananBarangTable.fnDraw();
                }




          updateNotaPesananBarangApprove = function(id){
                    var formNotaPesananBarang = $("#form-notaPesananBarangDetail");

                    var checkGoods =[];

                    $("#notaPesananBarangDetail_datatables tbody .row-select").each(function() {
                    if(this.checked){
                        var id = $(this).next("input:hidden").val();
                        var nRow = $(this).parents('tr')[0];
                        var aData = notaPesananBarangDetailTable.fnGetData(nRow);
                        var ktrJKTTglDiproses = $('#ktrJKTTglDiproses'+id).val();
                        var ktrJKTBukaDO = $("#ktrJKTBukaDO"+id).val();
                        var ktrJktTglKrmKeCabang = $("#ktrJktTglKrmKeCabang"+id).val();
                        var cabangTglDiterima = $("#cabangTglDiterima"+id).val();
                        var cabangKeterangan = $("#cabangKeterangan"+id).val();

                     //   alert("INI : "+ktrJKTTglDiproses);

                      //  alert("id dan qty : "+id+" qtyInput : "+qtyInput);
                        formNotaPesananBarang.append('<input type="hidden" id="ktrJKTTglDiproses'+id+'" name="ktrJKTTglDiproses'+id +'"   value="'+ktrJKTTglDiproses+'" class="deleteafter">');
                        formNotaPesananBarang.append('<input type="hidden" id="ktrJKTBukaDO'+id+'" name="ktrJKTBukaDO'+id +'"   value="'+ktrJKTBukaDO+'" class="deleteafter">');
                        formNotaPesananBarang.append('<input type="hidden" id="ktrJktTglKrmKeCabang'+id+'" name="ktrJktTglKrmKeCabang'+id +'"   value="'+ktrJktTglKrmKeCabang+'" class="deleteafter">');
                        formNotaPesananBarang.append('<input type="hidden" id="cabangTglDiterima'+id+'" name="cabangTglDiterima'+id +'"   value="'+cabangTglDiterima+'" class="deleteafter">');
                        formNotaPesananBarang.append('<input type="hidden" id="cabangKeterangan'+id+'" name="cabangKeterangan'+id +'"   value="'+cabangKeterangan+'" class="deleteafter">');


                        checkGoods.push(id);

                       }
                    });

                    var idNPB = $('#idNPB').val();

                     if(checkGoods.length<1){
                        alert('Anda belum memilih data yang akan ditambahkan');
                         return;
                    }else{
                        $("#ids").val(JSON.stringify(checkGoods));


                        $.ajax({
                            url:'${request.contextPath}/notaPesananBarang/updateNPBApprove',
                            type: "POST", // Always use POST when deleting data
                            data : formNotaPesananBarang.serialize(),
                            async : false,
                            success : function(data){
                                toastr.success('<div>Update NPB Telah dilakukan.</div>');
                                formNotaPesananBarang.find('.deleteafter').remove();
                                $("#ids").val('');
                                bukaTableLayout();
                            },
                        error: function(xhr, textStatus, errorThrown) {
                        alert('Internal Server Error');
                        }
                        });

                        checkGoods = []
                    }


                }

       updateAsUser = function(id){
                    var formNotaPesananBarang = $("#form-notaPesananBarangDetail");

                    var noNpb = $('#noNpb').val();
                    var tahunPembuatan = $('#tahunPembuatan').val();
                    var cabangTujuan = $('#cabangTujuan').val();
                    var noSpk = $('#noSpk').val();
                    var namaPemilik = $('#namaPemilik').val();
                    var tglSpk = $('#tglSpk').val();
                    var noPolisi = $('#noPolisi').val();
                    var ketTambahan = $("#keteranganTambahan").val();
                    var noRangka = $('#noRangka').val();
                    var ketPenegasan = $('#keteranganPenegasan').val();
                    var noEngine = $('#noEngine').val();
                    var penegasanPengiriman = $('#penegasanPengiriman').val();
                    var tipeKendaraan = $('#tipeKendaraan').val();
                    var tglNpb = $('#tglNpb').val();
                    var idNPB = $('#idNPB').val();

                    var tglJKTKirimCabang = $("#tglJKTKirimCabang").val();
                    var tglJKTDiterima = $("#tglJKTDiterima").val();
                    var tglHOKeJKT = $("#tglHOKeJKT").val();
                    var tglCabangDiterima = $("#tglCabangDiterima").val();
                    var tglJKTProsesToko = $("#tglJKTProsesToko").val();
                    var tglHODiterima = $("#tglHODiterima").val();
                    var tglJKTBukaDO = $("#tglJKTBukaDO").val();

                    formNotaPesananBarang.append('<input type="hidden" id="noNpb"  value="'+noNpb+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tahunPembuatan"  value="'+tahunPembuatan+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="cabangTujuan"  value="'+cabangTujuan+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="noSpk"  value="'+noSpk+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="namaPemilik"  value="'+namaPemilik+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tglSpk"  value="'+tglSpk+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="noPolisi"  value="'+noPolisi+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="ketTambahan"  value="'+ketTambahan+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="noRangka"  value="'+noRangka+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="ketPenegasan"  value="'+ketPenegasan+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="noEngine"  value="'+noEngine+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="penegasanPengiriman"  value="'+penegasanPengiriman+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tipeKendaraan"  value="'+tipeKendaraan+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tglNpb"  value="'+tglNpb+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="idNPB"  value="'+idNPB+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tglJKTKirimCabang"  value="'+tglJKTKirimCabang+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tglJKTDiterima"  value="'+tglJKTDiterima+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tglHOKeJKT"  value="'+tglHOKeJKT+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tglCabangDiterima"  value="'+tglCabangDiterima+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tglJKTProsesToko"  value="'+tglJKTProsesToko+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tglHODiterima"  value="'+tglHODiterima+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tglJKTBukaDO"  value="'+tglJKTBukaDO+'" class="deleteafter">');



                        $.ajax({
                            url:'${request.contextPath}/notaPesananBarang/updateAsUser',
                            type: "POST", // Always use POST when deleting data
                            data : formNotaPesananBarang.serialize(),
                            async : false,
                            success : function(data){
                                toastr.success('<div>NPB Telah diupdate.</div>');
                                formNotaPesananBarang.find('.deleteafter').remove();
                                $("#ids").val('');
                                bukaTableLayout();
                            },
                        error: function(xhr, textStatus, errorThrown) {
                        alert('Internal Server Error');
                        }
                        });
                }

                printNotaPesananBarangSlip = function(){
                       checkNotaPesananBarang =[];
                       var idNotaPesananBarang;
                        $("#notaPesananBarang-table tbody .row-select").each(function() {
                            if(this.checked){
                                idNotaPesananBarang = $(this).next("input:hidden").val();
                               checkNotaPesananBarang.push(idNotaPesananBarang);
                            }
                        });
                        if(checkNotaPesananBarang.length<1){
                            alert('Anda belum memilih data yang akan ditambahkan');
                            return;

                        }

                        var idNotaPesananBarangs = JSON.stringify(checkNotaPesananBarang);

                        window.location = "${request.contextPath}/notaPesananBarang/printNotaPesananBarang?id="+idNotaPesananBarangs;

                }



});
    function showFoto(id,otoritas){
        var url = ""
        if(otoritas=="petugasNPBHO"){
            url = "${request.contextPath}/notaPesananBarang/showFotoKabeng?id="+id;
        }else if(otoritas=="petugasNPBJKT"){
            url = "${request.contextPath}/notaPesananBarang/showFotoHo?id="+id;
        }
        window.open(url,'_blank');
    }
    function doApproveKabeng(){
            $("#approveKabengContent").empty();
                var checkNpb =[];
                var id= ""
                $("#notaPesananBarang-table tbody .row-select").each(function() {
                    if(this.checked){
                        id = $(this).next("input:hidden").val();
                        checkNpb.push(id);
                    }
                });

                if(checkNpb.length<1){
                    alert('Anda belum memilih data yang akan DiApprove');
                }else if(checkNpb.length>1){
                    alert('Silahkan Pilih salah satu data yang akan DiApprove');
                }else{
                    $.ajax({
                        type:'POST',
                        url:'${request.contextPath}/notaPesananBarang/formApprovalKabeng',
                        data:{id : id},
                        success:function(data,textStatus){
                                $("#approveKabengContent").html(data);
                                $("#approveKabengModal").modal({
                                    "backdrop" : "static",
                                    "keyboard" : true,
                                    "show" : true
                                }).css({'width': '500px','margin-left': function () {return -($(this).width() / 2);}});

                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
                }
        }

        function doApproveHo(){
            $("#approveHoContent").empty();
                var checkNpb =[];
                var id= ""
                $("#notaPesananBarang-table tbody .row-select").each(function() {
                    if(this.checked){
                        id = $(this).next("input:hidden").val();
                        checkNpb.push(id);
                    }
                });

                if(checkNpb.length<1){
                    alert('Anda belum memilih data yang akan DiApprove');
                }else if(checkNpb.length>1){
                    alert('Silahkan Pilih salah satu data yang akan DiApprove');
                }else{
                    $.ajax({
                        type:'POST',
                        url:'${request.contextPath}/notaPesananBarang/formApprovalHo',
                        data:{id : id},
                        success:function(data,textStatus){
                                $("#approveHoContent").html(data);
                                $("#approveHoModal").modal({
                                    "backdrop" : "static",
                                    "keyboard" : true,
                                    "show" : true
                                }).css({'width': '1000px','margin-left': function () {return -($(this).width() / 2);}});

                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
                }
        }
        function doApproveJkt(){
            $("#approveJktContent").empty();
                var checkNpb =[];
                var id= ""
                $("#notaPesananBarang-table tbody .row-select").each(function() {
                    if(this.checked){
                        id = $(this).next("input:hidden").val();
                        checkNpb.push(id);
                    }
                });

                if(checkNpb.length<1){
                    alert('Anda belum memilih data yang akan DiApprove');
                }else if(checkNpb.length>1){
                    alert('Silahkan Pilih salah satu data yang akan DiApprove');
                }else{
                    $.ajax({
                        type:'POST',
                        url:'${request.contextPath}/notaPesananBarang/formApprovalJkt',
                        data:{id : id},
                        success:function(data,textStatus){
                                $("#approveJktContent").html(data);
                                $("#approveJktModal").modal({
                                    "backdrop" : "static",
                                    "keyboard" : true,
                                    "show" : true
                                }).css({'width': '1000px','margin-left': function () {return -($(this).width() / 2);}});

                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
                }
        }
        function doCabang(){
            $("#approveJktContent").empty();
                var checkNpb =[];
                var id= ""
                $("#notaPesananBarang-table tbody .row-select").each(function() {
                    if(this.checked){
                        id = $(this).next("input:hidden").val();
                        checkNpb.push(id);
                    }
                });

                if(checkNpb.length<1){
                    alert('Anda belum memilih data yang akan DiApprove');
                }else if(checkNpb.length>1){
                    alert('Silahkan Pilih salah satu data yang akan DiApprove');
                }else{
                    $.ajax({
                        type:'POST',
                        url:'${request.contextPath}/notaPesananBarang/doCabang',
                        data:{id : id},
                        success:function(data,textStatus){
                            toastr.success('<div>Sukses</div>');
                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
                }
        }
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
   </div>
<div class="box">
    <div class="span12" id="notaPesananBarang-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <g:render template="dataTables_npb_approval" />
        <button id='printNotaPesananBarang' onclick="printNotaPesananBarangSlip();" type="button" class="btn btn-primary">Print Nota Pesanan barang</button>
        <g:if test="${otoritas=="kepalaBengkel"}">
            <button id='doApproveKabeng' onclick="doApproveKabeng();" type="button" class="btn btn-primary">Approval Kabeng</button>
        </g:if>
        <g:elseif test="${otoritas=="petugasNPBHO"}">
            <button id='doApproveHo' onclick="doApproveHo();" type="button" class="btn btn-primary">Approval HO</button>
        </g:elseif>
        <g:elseif test="${otoritas=="petugasNPBJKT"}">
            <button id='doApproveJkt' onclick="doApproveJkt();" type="button" class="btn btn-primary">Approval Kantor Jakarta</button>
        </g:elseif>
        <g:else>
            <button id='doCabang' onclick="doCabang();" type="button" class="btn btn-primary">Kirim</button>
        </g:else>
    </div>
    <form id="form-notaPesananBarangDetail" class="form-horizontal">
        <div class="span12" id="notaPesananBarang-form" hidden=""></div>
    </form>


</div>
<div id="approveHoModal" class="modal fade">
    <div class="modal-dialog" style="width: 1000px;">
        <div class="modal-content" style="width: 1000px;">
            <div class="modal-body" style="max-height: 800px;">
                <div id="approveHoContent"/>
                <div class="iu-content"></div>

            </div>
        </div>
    </div>
</div>
<div id="approveKabengModal" class="modal fade">
    <div class="modal-dialog" style="width: 500px;">
        <div class="modal-content" style="width: 500px;">
            <div class="modal-body" style="max-height: 600px;">
                <div id="approveKabengContent"/>
                <div class="iu-content"></div>

            </div>
        </div>
    </div>
</div>

<div id="approveJktModal" class="modal fade">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content" style="width: 900px;">
            <div class="modal-body" style="max-height: 600px;">
                <div id="approveJktContent"/>
                <div class="iu-content"></div>

            </div>
        </div>
    </div>
</div>

</body>
</html>
