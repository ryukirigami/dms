
<%@ page import="com.kombos.parts.NotaPesananBarang; com.kombos.parts.NotaPesananBarangDetail; com.kombos.parts.NotaPesananBarang" %>

<r:require modules="baseapplayout" />

<r:require modules="autoNumeric" />
<g:render template="../menu/maxLineDisplay"/>
<div>
    <g:field style="display: block" type="button" onclick="loadNotaPesananBarangInputModal();" class="btn cancel pull-right box-action"
             name="cari" id="cari" value="${message(code: 'notaPesananBarang.button.cariGoods.label', default: 'Add Parts')}" />
</div>
<br/><br/><br/>
<table id="notaPesananBarangDetail_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
            <th></th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="notaPesananBarang.kode.part.label" default="Kode Part" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="notaPesananBarang.nama.part.label" default="Nama Part" /></div>
            </th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="notaPesananBarang.qty.picking.label" default="Qty" /></div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var notaPesananBarangDetailTable;
var reloadNotaPesananBarangFormTable;
var qtyNotaPesananBarangCek;
var checkSelected;
$(function(){


    if(notaPesananBarangDetailTable){
        notaPesananBarangDetailTable.fnDestroy();
	}

	reloadNotaPesananBarangFormTable = function() {
		notaPesananBarangDetailTable.fnDraw();
	}

	checkSelected = function(rowId){
        $('#checkBox'+rowId).prop("checked", true);
	}


    qtyNotaPesananBarangCek = function(rowId){
        var value = jQuery('#qtyNotaPesananBarang'+rowId).val();

        if(value==""){
            alert("Mohon QTY NPB diisi");
            return false;
        }
        else
            return true;
    }




    $("th div input").bind('keypress', function(e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if(code == 13) {
                e.stopPropagation();
                notaPesananBarangDetailTable.fnDraw();
            }
        });
        $("th div input").click(function (e) {
            e.stopPropagation();
        });

	notaPesananBarangDetailTable = $('#notaPesananBarangDetail_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
	//	"bProcessing": true,
	//	"bServerSide": true,
	//	"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
	//	"sAjaxSource": "${g.createLink(action: "datatablesNotaPesananBarangDetail")}",
		"aoColumns": [

{
	"sName": "id",
	"mDataProp": "id",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}
,

{
	"sName": "kodeParts",
	"mDataProp": "kodeParts",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<label>&nbsp;&nbsp;'+data+'</label><input type="checkbox" id="checkBox'+row['id']+'" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'" id="idNotaPesananBarangDetail'+row['id']+'">&nbsp;&nbsp;&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "namaParts",
	"mDataProp": "namaParts",
	"aTargets": [1],
	"mRender": function ( data, type, row ) {
                        return '<label>'+data+'</label><input type="hidden" value="'+data+'" id="kodeParts'+row['id']+'"><input id="namaParts'+row['id']+'" type="hidden" value="'+data+'"/>';
    },
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "qty",
	"mDataProp": "jumlah",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"mRender": function ( data, type, row ) {
                        return '<input type="text" id="qtyDetail'+row['id']+'" name="qtyDetail'+row['id']+'" onkeypress="return isNumberKey(event);" onchange="checkSelected('+row['id']+');" class="inline-edit" value="0"/>';
    },
	"sWidth":"200px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                        aoData.push(
									{"name": 'notaPesananBarangDetailList', "value": "${notaPesananBarangDetailList}"}
						);



						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});




</g:javascript>
    <a class="btn cancel" id="cancel"  href="javascript:void(0);"
       onclick="bukaTableLayout()" >
        <g:message code="default.button.cancel.label" default="Cancel" /></a>
%{--<g:submitButton class="btn btn-primary create" name="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />--}%
<g:field type="button" onclick="saveNotaPesananBarang();" class="btn btn-primary create" name="tambah" id="tambah" value="${message(code: 'default.button.upload.label', default: 'Create')}" />


			
