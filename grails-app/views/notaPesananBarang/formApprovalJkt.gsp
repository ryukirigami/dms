<%@ page import="com.kombos.parts.NotaPesananBarang" %>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>
$(function(){
function progress(e){
		        if(e.lengthComputable){
		            //kalo mau pake progress bar
		            //$('progress').attr({value:e.loaded,max:e.total});
		        }
		    }
    kirim = function(){
       var form = new FormData($('#form-jkt')[0]);
        $.ajax({
            type:'POST',
            url:'${request.contextPath}/notaPesananBarang/sendApproveJkt',
            xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){
                        myXhr.upload.addEventListener('progress',progress, false);
                    }
                    return myXhr;
                },
            success:function(data,textStatus){
                toastr.success('<div> '+data+' Sukses</div>');
            },
            error:function(XMLHttpRequest,textStatus,errorThrown){},
            complete:function(XMLHttpRequest,textStatus){
                $('#spinner').fadeOut();
            },
                data: form,
                cache: false,
                contentType: false,
                processData: false
        });
    }
});
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="salesOrder.addParts.label" default="Form Approval Jkt" /></span>
</div>
<div class="box">
    <div class="span12" id="approveJkt-table">
        <form id="form-jkt" class="form-horizontal">
            <table>
                <tr>
                    <td style="padding: 10px">
                        <g:message code="salesOrder.salesType.label" default="Nomor NPB" />
                    </td>
                    <td style="padding: 10px">
                        <g:textField name="noNpb" readonly="readonly" value="${npb.noNpb}" style="width:250px" />
                        <g:hiddenField name="idNpb" id="idNpb" readonly="readonly" value="${npb.id}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 10px">
                        <g:message code="salesOrder.salesType.label" default="Nomor DO" />
                    </td>
                    <td style="padding: 10px">
                        <g:textField name="nomorDO" style="width:250px" /> Tgl Do <ba:datePicker name="tglDO" format="dd/MM/yyyy" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 10px">
                        <g:message code="salesOrder.salesType.label" default="Tgl Diproses" />
                    </td>
                    <td style="padding: 10px">
                        <ba:datePicker name="tglProses" format="dd/MM/yyyy" /> Tgl Kirim Ke Cabang <ba:datePicker name="tglKirimCabang" format="dd/MM/yyyy" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 10px">
                        <g:message code="salesOrder.salesType.label" default="Status Approval" />
                    </td>
                    <td style="padding: 10px">
                        <select name="staApproval">
                            <option value="1">Approve</option>
                            <option value="2">unApprove</option>
                        </select>
                    </td>
                </tr>
                %{--<tr>--}%
                    %{--<td style="padding: 10px">--}%
                        %{--<g:message code="salesOrder.salesType.label" default="Dokumen" />--}%
                    %{--</td>--}%
                    %{--<td style="padding: 10px">--}%
                        %{--<input type="file" id="fileDokumenHO" name="fileDokumenHO" required="" value="Upload Dokumen HO" />--}%
                    %{--</td>--}%
                %{--</tr>--}%
            </table>

            <div class="span11" id="detail-form" style="display: block; margin-top: 10px;">
                <legend>Detail</legend>
                <table id="detail-table" class="table table-bordered">
                    <thead>
                    <th>No</th>
                    <th>Kode Parts</th>
                    <th>Nama Parts</th>
                    <th>Keterangan</th>
                    </thead>
                    <tbody>
                    <g:if test="${npbDetail}">
                        <g:each in="${npbDetail}" status="i" var="j" >
                            <tr>
                                <td>${i + 1}</td>
                                <td>${j.goods.m111ID}</td>
                                <td>${j.goods.m111Nama}</td>
                                <td><g:textField name="ket_${j.id}" style="width:300px" /></td>
                            </tr>
                        </g:each>
                    </g:if>
                    </tbody>
                </table>
            </div>
        </form>
        <table>
            <tr>
                <td style="padding: 10px" colspan="2">
                    <button class="btn btn-primary" data-dismiss="modal" name="add" onclick="kirim();">Send</button>
                    <button class="btn cancel" data-dismiss="modal" name="close" >Close</button>
                </td>
            </tr>
        </table>
    </div>
</div>
</body>
</html>
