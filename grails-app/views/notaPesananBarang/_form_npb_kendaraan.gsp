<table>
    <tr>
        <td>
            <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'namaPemilik', 'error')} ">
                <label class="control-label" for="namaPemilik">
                    <g:message code="notaPesananBarang.namaPemilik.label" default="Nama Pemilik" />

                </label>
                <div class="controls">
                    <g:textField  name="namaPemilik" id="namaPemilik" value="${notaPesananBarangInstance?.namaPemilik}"  required="" />
                </div>
            </div>
        </td>
        <td>
            <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'tahunPembuatan', 'error')} ">
                <label class="control-label" for="tahunPembuatan">
                    <g:message code="notaPesananBarang.tahunPembuatan.label" default="Tahun Pembuatan" />

                </label>
                <div class="controls">
                    <g:textField onkeypress="return isNumberKey(event);" required="" name="tahunPembuatan" id="tahunPembuatan" value="${notaPesananBarangInstance.tahunPembuatan}" />
                                   </div>
            </div>
        </td>



    </tr>
    <tr>
        <td>
            <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'noPolisi', 'error')} ">
                <label class="control-label" for="noPolisi">
                    <g:message code="notaPesananBarang.noPolisi.label" default="No Polisi" />

                </label>
                <div class="controls">
                    <g:textField  name="noPolisi" id="noPolisi" required="" value="${notaPesananBarangInstance?.noPolisi}" />
                </div>
            </div>
        </td>


    </tr>
    <tr>
        <td>
            <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'noRangka', 'error')} ">
                <label class="control-label" for="noRangka">
                    <g:message code="notaPesananBarang.noRangka.label" default="No Rangka" />

                </label>
                <div class="controls">
                    <g:textField  name="noRangka" required="" id="noRangka" value="${notaPesananBarangInstance?.noRangka}" />
                </div>
            </div>

        </td>

    </tr>
    <tr>
        <td>
            <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'noEngine', 'error')} ">
                <label class="control-label" for="noEngine">
                    <g:message code="notaPesananBarang.noEngine.label" default="No Engine" />

                </label>
                <div class="controls">
                    <g:textField  name="noEngine" required="" id="noEngine" value="${notaPesananBarangInstance?.noEngine}" />
                </div>
            </div>
        </td>

    </tr>
    <tr>
        <td>
            <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'tipeKendaraan', 'error')} ">
                <label class="control-label" for="tipeKendaraan">
                    <g:message code="notaPesananBarang.tipeKendaraan.label" default="Tipe Kendaraan" />

                </label>
                <div class="controls">
                    <g:select  name="tipeKendaraan" id="tipeKendaraan" from="${com.kombos.administrasi.FullModelCode.createCriteria().list{eq("staDel","0");isNotNull("baseModel");baseModel{order("m102NamaBaseModel")}}}" optionKey="id" optionValue="${{it.t110FullModelCode + " | "+it.baseModel.m102NamaBaseModel}}" required="" value="${notaPesananBarangInstance?.tipeKendaraan?.id}" class="many-to-one"/>

                </div>
            </div>
        </td>


    </tr>
    <tr>

    </tr>
    <tr>

    </tr>

</table>
