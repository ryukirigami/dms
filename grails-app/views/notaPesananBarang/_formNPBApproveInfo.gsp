<table>
    <tr>
        <td>
            <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'kelompokNPB', 'error')} ">
                <label class="control-label" for="kelompokNPB">
                    <g:message code="notaPesananBarang.kelompokNPB.label" default="Kelompok Npb" />

                </label>
                <div class="controls">
                    <g:select id="kelompokNPB" name="kelompokNPB.id" from="${com.kombos.administrasi.KelompokNPB.list()}" optionKey="id" required="" value="${notaPesananBarangInstance?.kelompokNPB?.id}" class="many-to-one"/>
                    <g:javascript>
                        $("#kelompokNPB").attr("disabled", true);
                    </g:javascript>
                </div>
            </div>  </td>
        <td><div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'penegasanPengiriman', 'error')} ">
            <label class="control-label" for="penegasanPengiriman">
                <g:message code="notaPesananBarang.penegasanPengiriman.label" default="Penegasan Pengiriman" />

            </label>
            <div class="controls">
                <g:select id="penegasanPengiriman" disabled="" name="penegasanPengiriman.id" from="${com.kombos.administrasi.PenegasanPengiriman.list()}" optionKey="id" required="" value="${notaPesananBarangInstance?.penegasanPengiriman?.id}" class="many-to-one"/>

            </div>
        </div>
        </td>

    </tr>
    <tr>
        <td>
            <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'tglNpb', 'error')} ">
                <label class="control-label" for="tglNpb">
                    <g:message code="notaPesananBarang.tglNpb.label" default="Tgl Npb" />

                </label>
                <div class="controls">
                    <ba:datePicker name="tglNpb" id="tglNpb" precision="day" value="${notaPesananBarangInstance?.tglNpb}" format="yyyy-MM-dd"/>
                    <g:javascript>
                        $("#tglNpb").attr("disabled", true);
                    </g:javascript>
                </div>
            </div>
        </td>
        <td><div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'keteranganPenegasan', 'error')} ">
            <label class="control-label" for="keteranganPenegasan">
                <g:message code="notaPesananBarang.penegasanPengiriman.label" default="Keterangan Penegasan" />

            </label>
            <div class="controls">
                <g:textField id="keteranganPenegasan" disabled="" name="keteranganPenegasan" value="${notaPesananBarangInstance?.keteranganPenegasan}"/>
            </div>
        </div>
        </td>
    </tr>
    <tr>

        <td><div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'cabangTujuan', 'error')} ">
            <label class="control-label" for="cabangTujuan">
                <g:message code="notaPesananBarang.cabangTujuan.label" default="Cabang Tujuan" />

            </label>
            <div class="controls">
                <g:select id="cabangTujuan" name="cabangTujuan.id" disabled="true" from="${com.kombos.administrasi.CompanyDealer.list()}" optionKey="id" required="" value="${notaPesananBarangInstance?.cabangTujuan?.id}" class="many-to-one"/>
            </div>
        </div>
        </td>
        <td>

            <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'tglHODiterima', 'error')} ">
                <label class="control-label" for="tglHODiterima">
                    <g:message code="notaPesananBarang.tglHODiterima.label" default="Tgl HO Diterima"/>

                </label>

                <div class="controls">
                    <g:if test="${petugasNPBHO}">
                        <ba:datePicker name="tglHODiterima" precision="day"  value="${notaPesananBarangInstance?.tglHODiterima}"
                                       format="yyyy-MM-dd"/>
                    </g:if>
                    <g:else>
                        <ba:datePicker name="tglHODiterima" precision="day" readonly="" value="${notaPesananBarangInstance?.tglHODiterima}"
                                       format="yyyy-MM-dd"/>
                        <g:javascript>
                            $("#tglHODiterima").attr("disabled", true);
                        </g:javascript>

                    </g:else>
                </div>
            </div>

        </td>


    </td>

    </tr>
    <tr>
        <td>
            <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'noSpk', 'error')} ">
                <label class="control-label" for="noSpk">
                    <g:message code="notaPesananBarang.noSpk.label" default="No Spk" />

                </label>
                <div class="controls">
                    <g:textField readonly="" name="noSpk" id="noSpk" value="${notaPesananBarangInstance?.noSpk}" />
                </div>
            </div>
        </td>
        <td>
            <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'tglHOKeJKT', 'error')} ">
                <label class="control-label" for="tglHOKeJKT">
                    <g:message code="notaPesananBarang.tglHOKeJKT.label" default="Tgl HO Ke JKT"/>

                </label>

                <div class="controls">
                    <g:if test="${petugasNPBHO}">

                        <ba:datePicker name="tglHOKeJKT" precision="day" value="${notaPesananBarangInstance?.tglHOKeJKT}"
                                       format="yyyy-MM-dd"/>

                    </g:if>
                    <g:else>

                        <ba:datePicker name="tglHOKeJKT" precision="day" value="${notaPesananBarangInstance?.tglHOKeJKT}"
                                       format="yyyy-MM-dd"/>
                        <g:javascript>
                            $("#tglHOKeJKT").attr("disabled", true);
                        </g:javascript>


                    </g:else>

                </div>
            </div>

        </td>


    </tr>
    <tr>
        <td>
            <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'tglSpk', 'error')} ">
                <label class="control-label" for="tglSpk">
                    <g:message code="notaPesananBarang.tglSpk.label" default="Tgl Spk" />

                </label>
                <div class="controls">
                    <ba:datePicker name="tglSpk" id="tglSpk" precision="day" value="${notaPesananBarangInstance?.tglSpk}" format="yyyy-MM-dd"/>
                    <g:javascript>
                        $("#tglSpk").attr("disabled",true);
                    </g:javascript>
                </div>
            </div>
        </td>
        <td>
            <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'tglJKTDiterima', 'error')} ">
                <label class="control-label" for="tglJKTDiterima">
                    <g:message code="notaPesananBarang.tglHOKeJKT.label" default="Tgl NPB Diterima di JKT"/>

                </label>

                <div class="controls">
                    <g:if test="${petugasNPBJKT}">

                        <ba:datePicker name="tglJKTDiterima" id="tglJKTDiterima" precision="day" value="${notaPesananBarangInstance?.tglJKTDiterima}"
                                       format="yyyy-MM-dd"/>

                    </g:if>
                    <g:else>


                        <ba:datePicker name="tglJKTDiterima" id="tglJKTDiterima" precision="day" value="${notaPesananBarangInstance?.tglJKTDiterima}"
                                       format="yyyy-MM-dd"/>
                        <g:javascript>
                            $("#tglJKTDiterima").attr("disabled", true);
                        </g:javascript>


                    </g:else>

                </div>
            </div>

        </td>



    </tr>
    <tr>
        <td>
            <div class="control-group fieldcontain ${hasErrors(bean: notaPesananBarangInstance, field: 'keteranganTambahan', 'error')} ">
                <label class="control-label" for="keteranganTambahan">
                    <g:message code="notaPesananBarang.keteranganTambahan.label" default="Keterangan Tambahan" />

                </label>
                <div class="controls">
                    <g:textField readonly="" name="keteranganTambahan" id="keteranganTambahan" value="${notaPesananBarangInstance?.keteranganTambahan}" />
                </div>
            </div>

        </td>
    </tr>

</table>