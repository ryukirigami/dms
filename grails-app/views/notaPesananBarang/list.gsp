
<%@ page import="com.kombos.parts.NotaPesananBarang" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'notaPesananBarang.label', default: 'Nota Pesanan Barang')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist, autoNumeric" />
		<g:javascript>
			var show;
			var edit;
			var addGoods;
            var addEditGoods;
			var printNotaPesananBarangSlip;
			var printNotaPesananBarangSlipCont;
			var showNotaPesananBarangDetail;
			var saveNotaPesananBarang;
			var loadNotaPesananBarangEditModal;
			var massDeleteNotaPesananBarang;
			var isNumberKey;




			$(function(){ 

            isNumberKey = function(evt)
            {
                var charCode = (evt.which) ? evt.which : evt.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;

                return true;
            }

             showNotaPesananBarangDetail = function(){
                        $(".span7").css("display","none");
                        $(".span12").css("display","block");
                    }

				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout();
					//	showNotaPesananBarangDetail();
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('notaPesananBarang', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_MINIMIZE_' :

                            <g:remoteFunction action="list"
                              onLoading="jQuery('#spinner').fadeIn(1);"
                              onSuccess="bukaTableLayout()"
                              onComplete="jQuery('#spinner').fadeOut();" />
							break;
					    case '_DELETE_' :
                        bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
                            function(result){
                                if(result){
                                    massDeleteNotaPesananBarang();
                                }
         			});

         		break;
				   }    
				   return false;
				});

				massDeleteNotaPesananBarang = function() {
                    var recordsToDelete = [];

                    $("#notaPesananBarang-table tbody .row-select").each(function() {
                        if(this.checked){
                            var id = $(this).next("input:hidden").val();
                   		    recordsToDelete.push(id);
                        }
                    });

                    var json = JSON.stringify(recordsToDelete);

                    $.ajax({
                        url:'${request.contextPath}/notaPesananBarang/massdelete',
                        type: "POST", // Always use POST when deleting data
                        data: { ids: json },
                        complete: function(xhr, status) {
                            notaPesananBarangTable.fnDraw();
                             toastr.success('<div>Nota Pesanan Barang Telah dihapus.</div>');

                        }
                    });

                }


				show = function(id) {
					showInstance('notaPesananBarang','${request.contextPath}/notaPesananBarang/show/'+id);
				};
				
				edit = function(id) {
					editInstance('notaPesananBarang','${request.contextPath}/notaPesananBarang/edit/'+id);
			    };

			    editSampe = function(id) {
                    $.ajax({
                        type:'POST',
                        url:'${request.contextPath}/notaPesananBarang/formApproval',
                        data:{id : id},
                        success:function(data,textStatus){
                                $("#editSampeContent").html(data);
                                $("#editSampeModal").modal({
                                    "backdrop" : "static",
                                    "keyboard" : true,
                                    "show" : true
                                }).css({'width': '900px','margin-left': function () {return -($(this).width() / 2);}});

                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
			    };

				shrinkTableLayout = function(){
                    $("#notaPesananBarang-table").hide();
                    $("#notaPesananBarang-form").show();
                }

                bukaTableLayout = function(){
                    $("#notaPesananBarang-table").show();
                    $("#notaPesananBarang-form").hide();
                    notaPesananBarangTable.fnDraw();
                }



                  addGoods = function(){
                       checkGoods =[];
                        $("#goods_datatables .row-select").each(function() {
                            if(this.checked){
                              var nRow = $(this).parents('tr')[0];
                                checkGoods.push(nRow);

                            }
                        });
                        if(checkGoods.length<1){
                            alert('Anda belum memilih data yang akan ditambahkan');
                            return;
                        }else{
                            for (var i=0;i<checkGoods.length;i++){
                                var aData = goodsTable.fnGetData(checkGoods[i]);
                                notaPesananBarangDetailTable.fnAddData({
                                    'id' : aData['id'],
                                    'kodeParts': aData['m111ID_modal'],
                                    'namaParts': aData['m111Nama_modal'],
                                    'jumlah': '0'
                                    });

                            }
                        }

                    checkGoods = [];

                }

                 addEditGoods = function(){
                       checkGoods =[];
                        $("#goods_datatables .row-select").each(function() {
                            if(this.checked){
                              var nRow = $(this).parents('tr')[0];
                                checkGoods.push(nRow);

                            }
                        });
                        if(checkGoods.length<1){
                            alert('Anda belum memilih data yang akan ditambahkan');
                            return;
                         //   loadPilihJobModal();
                        }else{
                          //  alert("lenght : "+checkGoods.length);
                            for (var i=0;i<checkGoods.length;i++){
                               var aData = goodsTable.fnGetData(checkGoods[i]);

                                notaPesananBarangDetailTableEdit.fnAddData({
                                    'id' : aData['id'],
                                    'kodeParts': aData['m111ID_modal'],
                                    'namaParts': aData['m111Nama_modal'],
                                    'jumlah': aData['qtyPicking'],
                                    'satuan1': aData['satuan'],
                                    'qtyBlockStock': '0',
                                    'satuan2':  aData['satuan'],
                                    'location':'0'});
                                    $('#qtyNotaPesananBarang'+aData['id']).autoNumeric('init',{
                                                            vMin:'0',
                                                            vMax:'999999999999999999',
                                                            mDec: null,
                                                            aSep:''
                                                        });
                                notaPesananBarangDetailTableEdit.fnDraw();

                            }
                        }

                    checkGoods = [];

                }

                saveNotaPesananBarang = function(){

                    var formNotaPesananBarang = $("#form-notaPesananBarangDetail");

                    var checkGoods =[];
                     var cabangTujuan = $('#cabangTujuan').val();
                    var tglNpb = $('#tglNpb').val();
                    if(tglNpb == ""){
                        toastr.error("Harap masukan tanggal NPB");
                        return;
                    }

                    var noSpk = $('#noSpk').val();
                    if(noSpk == ""){
                        toastr.error("Harap masukan no SPK");
                        return;
                    }

                    var tglSpk = $('#tglSpk').val();
                    if(tglSpk == ""){
                        toastr.error("Harap masukan tgl SPK");
                        return;
                    }

                    var kelompokNPB = $('kelompokNPB').val();

                    var namaPemilik = $('#namaPemilik').val();
                    if(namaPemilik === ""){
                        toastr.error("Harap masukan nama pemilik");
                        return;
                    }

                    var noPolisi = $('#noPolisi').val();
                    if(noPolisi == ""){
                        toastr.error("Harap masukan no polisi");
                        return;
                    }
                    var ketTambahan = $("#keteranganTambahan").val();
                    var noRangka = $('#noRangka').val();
                    if(noRangka == ""){
                        toastr.error("Harap masukan no rangka");
                        return;
                    }
                    var ketPenegasan = $('#keteranganPenegasan').val();
                    var noEngine = $('#noEngine').val();
                    if(noEngine == ""){
                        toastr.error("Harap masukan no engine");
                        return;
                    }
                    var tahunPembuatan = $('#tahunPembuatan').val();

                    var penegasanPengiriman = $('#penegasanPengiriman').val();
                    var tipeKendaraan = $('#tipeKendaraan').val();


                  $("#notaPesananBarangDetail_datatables tbody .row-select").each(function() {
                    if(this.checked){
                        var id = $(this).next("input:hidden").val();
                        var nRow = $(this).parents('tr')[0];
                        var aData = notaPesananBarangDetailTable.fnGetData(nRow);
                        var qtyInput = $('#qtyDetail'+id).val();
                        formNotaPesananBarang.append('<input type="hidden" id="qty'+id+'" name="qty'+id +'"   value="'+qtyInput+'" class="deleteafter">');

                        checkGoods.push(id);

                       }
                    });

                    formNotaPesananBarang.append('<input type="hidden" id="tahunPembuatan"  value="'+tahunPembuatan+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="cabangTujuan"  value="'+cabangTujuan+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="noSpk"  value="'+noSpk+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="namaPemilik"  value="'+namaPemilik+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tglSpk"  value="'+tglSpk+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="noPolisi"  value="'+noPolisi+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="ketTambahan"  value="'+ketTambahan+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="noRangka"  value="'+noRangka+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="ketPenegasan"  value="'+ketPenegasan+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="noEngine"  value="'+noEngine+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="penegasanPengiriman"  value="'+penegasanPengiriman+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tipeKendaraan"  value="'+tipeKendaraan+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tglNpb"  value="'+tglNpb+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="kelompokNPB"  value="'+kelompokNPB+'" class="deleteafter">');

                     if(checkGoods.length<1){
                        alert('Anda belum memilih data yang akan ditambahkan');
                        formNotaPesananBarang.find('.deleteafter').remove();
                        return;
                    }else{
                        $("#ids").val(JSON.stringify(checkGoods));
                     $.ajax({
                            url:'${request.contextPath}/notaPesananBarang/save',
                            type: "POST", // Always use POST when deleting data
                            data : formNotaPesananBarang.serialize(),
                            async : false,
                            success : function(data){
                                toastr.success('<div>NPB Telah dibuat.</div>');
                                formNotaPesananBarang.find('.deleteafter').remove();
                                $("#ids").val('');
                                bukaTableLayout();
                            },
                        error: function(xhr, textStatus, errorThrown) {
                             alert('Internal Server Error');
                        }
                        });

                        checkGoods = []
                    }


                }

                updateNotaPesananBarang = function(id){
                    var formNotaPesananBarang = $("#form-notaPesananBarangDetail");

                    var checkGoods =[];
                    var cabangTujuan = $('#cabangTujuan').val();
                    var tglNpb = $('#tglNpb').val();
                    if(tglNpb == ""){
                        toastr.error("Harap masukan tanggal NPB");
                        return;
                    }

                    var noSpk = $('#noSpk').val();
                    if(noSpk == ""){
                        toastr.error("Harap masukan no SPK");
                        return;
                    }

                    var tglSpk = $('#tglSpk').val();
                    if(tglSpk == ""){
                        toastr.error("Harap masukan tgl SPK");
                        return;
                    }

                    var kelompokNPB = $('kelompokNPB').val();

                    var namaPemilik = $('#namaPemilik').val();
                    if(namaPemilik === ""){
                        toastr.error("Harap masukan nama pemilik");
                        return;
                    }

                    var noPolisi = $('#noPolisi').val();
                    if(noPolisi == ""){
                        toastr.error("Harap masukan no polisi");
                        return;
                    }
                    var ketTambahan = $("#keteranganTambahan").val();
                    var noRangka = $('#noRangka').val();
                    if(noRangka == ""){
                        toastr.error("Harap masukan no rangka");
                        return;
                    }
                    var ketPenegasan = $('#keteranganPenegasan').val();
                    var noEngine = $('#noEngine').val();
                    if(noEngine == ""){
                        toastr.error("Harap masukan no engine");
                        return;
                    }
                    var tahunPembuatan = $('#tahunPembuatan').val();

                    var penegasanPengiriman = $('#penegasanPengiriman').val();
                    var penegasanPengiriman = $('#penegasanPengiriman').val();
                    var tipeKendaraan = $('#tipeKendaraan').val();

                $("#notaPesananBarangDetail_datatables tbody .row-select").each(function() {
                    if(this.checked){
                        var id = $(this).next("input:hidden").val();
                        var nRow = $(this).parents('tr')[0];
                        var aData = notaPesananBarangDetailTable.fnGetData(nRow);
                        var qtyInput = $('#qtyDetail'+id).val();
                        formNotaPesananBarang.append('<input type="hidden" id="qty'+id+'" name="qty'+id+'"   value="'+qtyInput+'" class="deleteafter">');
                        checkGoods.push(id);

                       }
                    });

//                    formNotaPesananBarang.append('<input type="hidden" id="noNpb"  value="'+noNpb+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tahunPembuatan"  value="'+tahunPembuatan+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="cabangTujuan"  value="'+cabangTujuan+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="noSpk"  value="'+noSpk+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="namaPemilik"  value="'+namaPemilik+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tglSpk"  value="'+tglSpk+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="noPolisi"  value="'+noPolisi+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="ketTambahan"  value="'+ketTambahan+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="noRangka"  value="'+noRangka+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="ketPenegasan"  value="'+ketPenegasan+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="noEngine"  value="'+noEngine+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="penegasanPengiriman"  value="'+penegasanPengiriman+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tipeKendaraan"  value="'+tipeKendaraan+'" class="deleteafter">');
                    formNotaPesananBarang.append('<input type="hidden" id="tglNpb"  value="'+tglNpb+'" class="deleteafter">');

            if(checkGoods.length<1){
               alert('Anda belum memilih data yang akan ditambahkan');
                formNotaPesananBarang.find('.deleteafter').remove();
                return;
           }else{
               $("#ids").val(JSON.stringify(checkGoods));


               $.ajax({
                   url:'${request.contextPath}/notaPesananBarang/update',
                            type: "POST", // Always use POST when deleting data
                            data : formNotaPesananBarang.serialize(),
                            async : false,
                            success : function(data){
                                toastr.success('<div>Block Stok Telah dibuat.</div>');
                                formNotaPesananBarang.find('.deleteafter').remove();
                                $("#ids").val('');
                                bukaTableLayout();
                            },
                        error: function(xhr, textStatus, errorThrown) {
                        alert('Internal Server Error');
                        }
                        });

                        checkGoods = []
                    }


                }
                printNotaPesananBarangSlip = function(){
                       checkNotaPesananBarang =[];
                       var idNotaPesananBarang;
                        $("#notaPesananBarang-table tbody .row-select").each(function() {
                            if(this.checked){
                                idNotaPesananBarang = $(this).next("input:hidden").val();
                               checkNotaPesananBarang.push(idNotaPesananBarang);
                            }
                        });
                        if(checkNotaPesananBarang.length<1){
                            alert('Anda belum memilih data yang akan ditambahkan');
                            return;

                        }

                        var idNotaPesananBarangs = JSON.stringify(checkNotaPesananBarang);

                        window.location = "${request.contextPath}/notaPesananBarang/printNotaPesananBarang?id="+idNotaPesananBarangs;

                }



});

</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>

	</div>
	<div class="box">
		<div class="span12" id="notaPesananBarang-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <div class="nav pull-right">

                <a class="pull-right box-action" href="#"
                   style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
                        class="icon-remove"></i>&nbsp;&nbsp;
                </a>
                <a class="pull-right box-action" href="#"
                   style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
                        class="icon-plus"></i>&nbsp;&nbsp;
                </a>


            </div>


            <g:render template="dataTables_npb" />
            <button id='printNotaPesananBarang' onclick="printNotaPesananBarangSlip();" type="button" class="btn btn-primary">Print Nota Pesanan barang</button>

    </div>
        <form id="form-notaPesananBarangDetail" class="form-horizontal">
            <div class="span12" id="notaPesananBarang-form" hidden=""></div>
        </form>


	</div>
    <div id="notaPesananBarangInputModal" class="modal fade">
        <div class="modal-dialog" style="width: 1200px;">
            <div class="modal-content" style="width: 1200px; height: 2000">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 450px;">
                    <div class="iu-content"></div>
                    <div id="notaPesananBarangInputContent"/>
                </div>
                <!-- dialog buttons -->
                <div class="modal-footer">
                <button id='addGoods' onclick="addGoods();" type="button" class="btn btn-primary">Add Selected</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="notaPesananBarangEditModal" class="modal fade">
        <div class="modal-dialog" style="width: 1200px;">
            <div class="modal-content" style="width: 1200px; height: 2000">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 450px;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="iu-content"></div>
                    <div id="notaPesananBarangEditContent"/>
                </div>
                <!-- dialog buttons -->
                <div class="modal-footer">
                    <button id='addGoods' onclick="addEditGoods();" type="button" class="btn btn-primary">Add Selected</button></div>
            </div>
        </div>
    </div>
    <div id="editSampeModal" class="modal fade">
        <div class="modal-dialog" style="width: 900px;">
            <div class="modal-content" style="width: 900px; height: 900">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 450px;">
                    <div class="iu-content"></div>
                    <div id="editSampeContent"/>
                </div>
           </div>
        </div>
    </div>
    </body>
</html>
