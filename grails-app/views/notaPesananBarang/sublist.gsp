<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="baseapplayout" />
<g:javascript>
var notaPesananBarangDetailTable_${idTable};
var reloadNotaPesananBarangDetailTable;
$(function(){

	reloadNotaPesananBarangDetailTable = function() {
		notaPesananBarangDetailTable_${idTable}.fnDraw();
	}


	$('#search_cabangTglDiterima').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_cabangTglDiterima_day').val(newDate.getDate());
			$('#search_cabangTglDiterima_month').val(newDate.getMonth()+1);
			$('#search_cabangTglDiterima_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			notaPesananBarangDetailTable_${idTable}.fnDraw();
	});



	$('#search_hoTglDiterima').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_hoTglDiterima_day').val(newDate.getDate());
			$('#search_hoTglDiterima_month').val(newDate.getMonth()+1);
			$('#search_hoTglDiterima_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			notaPesananBarangDetailTable_${idTable}.fnDraw();
	});



	$('#search_hoTglDiteruskan').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_hoTglDiteruskan_day').val(newDate.getDate());
			$('#search_hoTglDiteruskan_month').val(newDate.getMonth()+1);
			$('#search_hoTglDiteruskan_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			notaPesananBarangDetailTable_${idTable}.fnDraw();
	});



	$('#search_ktrJktTglBukaDO').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_ktrJktTglBukaDO_day').val(newDate.getDate());
			$('#search_ktrJktTglBukaDO_month').val(newDate.getMonth()+1);
			$('#search_ktrJktTglBukaDO_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			notaPesananBarangDetailTable_${idTable}.fnDraw();
	});



	$('#search_ktrJktTglDiproses').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_ktrJktTglDiproses_day').val(newDate.getDate());
			$('#search_ktrJktTglDiproses_month').val(newDate.getMonth()+1);
			$('#search_ktrJktTglDiproses_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			notaPesananBarangDetailTable_${idTable}.fnDraw();
	});



	$('#search_ktrJktTglDiterima').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_ktrJktTglDiterima_day').val(newDate.getDate());
			$('#search_ktrJktTglDiterima_month').val(newDate.getMonth()+1);
			$('#search_ktrJktTglDiterima_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			notaPesananBarangDetailTable_${idTable}.fnDraw();
	});



	$('#search_ktrJktTglKrmKeCabang').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_ktrJktTglKrmKeCabang_day').val(newDate.getDate());
			$('#search_ktrJktTglKrmKeCabang_month').val(newDate.getMonth()+1);
			$('#search_ktrJktTglKrmKeCabang_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			notaPesananBarangDetailTable_${idTable}.fnDraw();
	});




$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	notaPesananBarangDetailTable_${idTable}.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});
     if(notaPesananBarangDetailTable_${idTable})
    	notaPesananBarangDetailTable_${idTable}.dataTable().fnDestroy();
	notaPesananBarangDetailTable_${idTable} = $('#notaPesananBarang_datatables_sub_${idTable}').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubList")}",
		"aoColumns": [
{
               "sName": "naon",
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": ''
},
{
               "sName": "naon",
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": ''
},
{
               "sName": "naon",
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": ''
},

{
	"sName": "namaParts",
	"mDataProp": "namaParts",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "jumlah",
	"mDataProp": "jumlah",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "hoTglDiterima",
	"mDataProp": "hoTglDiterima",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "hoTglDiteruskan",
	"mDataProp": "hoTglDiteruskan",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "hoKeterangan",
	"mDataProp": "hoKeterangan",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "ktrJktTglDiterima",
	"mDataProp": "ktrJktTglDiterima",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "ktrJktTglDiproses",
	"mDataProp": "ktrJktTglDiproses",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "ktrJktTglBukaDO",
	"mDataProp": "ktrJktTglBukaDO",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "ktrJktKeterangan",
	"mDataProp": "ktrJktKeterangan",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "nomorDO",
	"mDataProp": "nomorDO",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "ktrJktTglKrmKeCabang",
	"mDataProp": "ktrJktTglKrmKeCabang",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "cabangTglDiterima",
	"mDataProp": "cabangTglDiterima",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "cabangKeterangan",
	"mDataProp": "cabangKeterangan",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {


							aoData.push(
									{"name": 'idNPB', "value": ${idNPB}}
							);


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>
</head>
<body>
<div class="innerDetails">
    <table id="notaPesananBarang_datatables_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
           class="display table table-striped table-bordered table-hover">
        <thead>
        <tr>

            <th></th>
            <th></th>
            <th></th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="notaPesananBarangDetail.goods.label" default="Nama (Uraian Barang)" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="notaPesananBarangDetail.jumlah.label" default="Jumlah" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="notaPesananBarangDetail.hoTglDiterima.label" default="Ho Tgl Diterima" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="notaPesananBarangDetail.hoTglDiteruskan.label" default="Ho Tgl Diteruskan" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="notaPesananBarangDetail.hoTglDiteruskan.label" default="Ho Keterangan" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="notaPesananBarangDetail.ktrJktTglDiterima.label" default="Ktr Jkt Tgl Diterima" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="notaPesananBarangDetail.ktrJktTglDiproses.label" default="Ktr Jkt Tgl Diproses Toko" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="notaPesananBarangDetail.ktrJktTglBukaDO.label" default="Ktr Jkt Tgl Buka DO" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="notaPesananBarangDetail.ktrJktTglDiproses.label" default="Ktr Jkt Keterangan" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="notaPesananBarangDetail.ktrJktTglBukaDO.label" default="Nomor DO" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="notaPesananBarangDetail.ktrJktTglKrmKeCabang.label" default="Ktr Jkt Tgl Krm Ke Cabang" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="notaPesananBarangDetail.cabangTglDiterima.label" default="Cabang Tgl Diterima" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="notaPesananBarangDetail.cabangKeterangan.label" default="Cabang Keterangan" /></div>
            </th>


        </tr>

        </thead>
        <tbody></tbody>
    </table>
</div>
</body>
</html>
