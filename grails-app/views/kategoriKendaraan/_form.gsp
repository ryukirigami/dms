<%@ page import="com.kombos.administrasi.KategoriKendaraan" %>



<div class="control-group fieldcontain ${hasErrors(bean: kategoriKendaraanInstance, field: 'm101KodeKategori', 'error')} required">
	<label class="control-label" for="m101KodeKategori">
		<g:message code="kategoriKendaraan.m101KodeKategori.label" default="Kode Kategori" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:if test="${aksi=='edit'}">--}%
		%{--<g:textField name="m101KodeKategori" maxlength="10" disabled="true" value="${kategoriKendaraanInstance?.m101KodeKategori}"/>--}%
	%{--</g:if>--}%
	%{--<g:else>--}%
		<g:textField name="m101KodeKategori" id="m101KodeKategori" maxlength="10" required="" value="${kategoriKendaraanInstance?.m101KodeKategori}"/>
	%{--</g:else>--}%
	
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: kategoriKendaraanInstance, field: 'm101NamaKategori', 'error')} required">
	<label class="control-label" for="m101NamaKategori">
		<g:message code="kategoriKendaraan.m101NamaKategori.label" default="Nama Kategori" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m101NamaKategori" id="m101NamaKategori" maxlength="20" required="" value="${kategoriKendaraanInstance?.m101NamaKategori}"/>
	</div>
</div>


<g:javascript>
    document.getElementById("m101KodeKategori").focus();
    $(document).ready(function() {
        $("#m101KodeKategori").keyup(function(e) {
            var isi = $(e.target).val();
            $(e.target).val(isi.toUpperCase());
        });
        $("#m101NamaKategori").keyup(function(e) {
            var isi = $(e.target).val();
            $(e.target).val(isi.toUpperCase());
        });
    });
</g:javascript>

