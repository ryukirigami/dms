

<%@ page import="com.kombos.administrasi.KategoriKendaraan" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'kategoriKendaraan.label', default: 'Kategori Kendaraan')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteKategoriKendaraan;

$(function(){ 
	deleteKategoriKendaraan=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/kategoriKendaraan/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadKategoriKendaraanTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-kategoriKendaraan" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="kategoriKendaraan"
			class="table table-bordered table-hover">
			<tbody>

				
	
			
				<g:if test="${kategoriKendaraanInstance?.m101KodeKategori}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m101KodeKategori-label" class="property-label"><g:message
					code="kategoriKendaraan.m101KodeKategori.label" default="Kode Kategori" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m101KodeKategori-label">
						%{--<ba:editableValue
								bean="${kategoriKendaraanInstance}" field="m101KodeKategori"
								url="${request.contextPath}/KategoriKendaraan/updatefield" type="text"
								title="Enter m101KodeKategori" onsuccess="reloadKategoriKendaraanTable();" />--}%
							
								<g:fieldValue bean="${kategoriKendaraanInstance}" field="m101KodeKategori"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kategoriKendaraanInstance?.m101NamaKategori}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m101NamaKategori-label" class="property-label"><g:message
					code="kategoriKendaraan.m101NamaKategori.label" default="Nama Kategori" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m101NamaKategori-label">
						%{--<ba:editableValue
								bean="${kategoriKendaraanInstance}" field="m101NamaKategori"
								url="${request.contextPath}/KategoriKendaraan/updatefield" type="text"
								title="Enter m101NamaKategori" onsuccess="reloadKategoriKendaraanTable();" />--}%
							
								<g:fieldValue bean="${kategoriKendaraanInstance}" field="m101NamaKategori"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${kategoriKendaraanInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="jenisStall.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${kategoriKendaraanInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadJenisStallTable();" />--}%

                        <g:fieldValue bean="${kategoriKendaraanInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${kategoriKendaraanInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="jenisStall.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${kategoriKendaraanInstance}" field="dateCreated"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:formatDate date="${kategoriKendaraanInstance?.dateCreated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kategoriKendaraanInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="jenisStall.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${kategoriKendaraanInstance}" field="createdBy"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:fieldValue bean="${kategoriKendaraanInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kategoriKendaraanInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="jenisStall.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${kategoriKendaraanInstance}" field="lastUpdated"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:formatDate date="${kategoriKendaraanInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${kategoriKendaraanInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="jenisStall.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${kategoriKendaraanInstance}" field="updatedBy"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:fieldValue bean="${kategoriKendaraanInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
				
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${kategoriKendaraanInstance?.id}"
					update="[success:'kategoriKendaraan-form',failure:'kategoriKendaraan-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteKategoriKendaraan('${kategoriKendaraanInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
