
<%@ page import="com.kombos.hrd.HistoryRewardKaryawan" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<div class="navbar box-header no-border">
    <ul class="nav pull-right">
        <li><a class="pull-right box-action _new" href="javascript:void(0);"
               style="display: block;">&nbsp;&nbsp;<i
                    class="icon-plus" onclick="<g:remoteFunction action="create"
                                                                 controller="historyRewardKaryawan"
                                                                 onLoading="jQuery('#spinner').fadeIn(1);"
                                                                 onSuccess="loadFormInstance('historyRewardKaryawan', data, textStatus);"
                                                                 onComplete="jQuery('#spinner').fadeOut();shrinkTableLayout('historyRewardKaryawan');"
                                                                 params="'onDataKaryawan=true&karyawanId=${karyawanInstance?.id}'"
                    />"></i>&nbsp;&nbsp;
        </a></li>
        <li><a class="pull-right box-action _delete" href="#"
               style="display: block;" onclick="oHistoryService.massDelete('historyRewardKaryawan', 'historyRewardKaryawan', reloadHistoryRewardKaryawanTable);">&nbsp;&nbsp;<i
                    class="icon-remove"></i>&nbsp;&nbsp;
        </a></li>
    </ul>
</div>

<table id="historyRewardKaryawan_table" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%" style="width: 1225px !important;">

</table>

%{--<div class="action" data-target="histori" style="padding: 10px;">
    <a class="btn btn-primary _new" href="javascript:void(0);">New</a>
    <a class="btn btn-primary _delete" href="javascript:void(0);" onclick="oHistoryService.massDelete('historyRewardKaryawan', 'historyRewardKaryawan', reloadHistoryRewardKaryawanTable);">Delete</a>
</div>--}%

<g:javascript>
var historyRewardKaryawanTable;
var reloadHistoryRewardKaryawanTable;
$(function(){
	
	reloadHistoryRewardKaryawanTable = function() {
		historyRewardKaryawanTable.fnDraw();
	}

	
	$('#search_tanggalReward').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tanggalReward_day').val(newDate.getDate());
			$('#search_tanggalReward_month').val(newDate.getMonth()+1);
			$('#search_tanggalReward_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			historyRewardKaryawanTable.fnDraw();	
	});


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	historyRewardKaryawanTable.fnDraw();
		}
	});

	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	historyRewardKaryawanTable = $('#historyRewardKaryawan_table').dataTable({
		"sScrollX": "100%",
            "bScrollCollapse": true,
            "bAutoWidth" : false,
            "bPaginate" : true,
            "sInfo" : "",
            "sInfoEmpty" : "",
            "sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
            bFilter: true,
            "bStateSave": false,
            'sPaginationType': 'bootstrap',
            "fnInitComplete": function () {
                this.fnAdjustColumnSizing(true);
               },
               "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                return nRow;
               },
            "bSort": true,
            "bProcessing": true,
            "bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList", controller: "historyRewardKaryawan")}",
		"aoColumns": [

{
    "sTitle": "Reward",
	"sName": "rewardKaryawan",
	"mDataProp": "rewardKaryawan",
	"aTargets": [3],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="oHistoryService.edit(\'historyRewardKaryawan\','+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,


{
    "sTitle": "Tanggal",
	"sName": "tanggalReward",
	"mDataProp": "tanggalReward",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
    "sTitle": "Keterangan",
	"sName": "keterangan",
	"mDataProp": "keterangan",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                        aoData.push(
                                {"name": 'idKaryawan', "value": "${karyawanInstance?.id}"},
                                {"name": 'dari', "value": "karyawan"}
                        );

						var keterangan = $('#filter_keterangan input').val();
						if(keterangan){
							aoData.push(
									{"name": 'sCriteria_keterangan', "value": keterangan}
							);
						}

						var rewardKaryawan = $('#filter_rewardKaryawan input').val();
						if(rewardKaryawan){
							aoData.push(
									{"name": 'sCriteria_rewardKaryawan', "value": rewardKaryawan}
							);
						}

						var tanggalReward = $('#search_tanggalReward').val();
						var tanggalRewardDay = $('#search_tanggalReward_day').val();
						var tanggalRewardMonth = $('#search_tanggalReward_month').val();
						var tanggalRewardYear = $('#search_tanggalReward_year').val();
						
						if(tanggalReward){
							aoData.push(
									{"name": 'sCriteria_tanggalReward', "value": "date.struct"},
									{"name": 'sCriteria_tanggalReward_dp', "value": tanggalReward},
									{"name": 'sCriteria_tanggalReward_day', "value": tanggalRewardDay},
									{"name": 'sCriteria_tanggalReward_month', "value": tanggalRewardMonth},
									{"name": 'sCriteria_tanggalReward_year', "value": tanggalRewardYear}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
