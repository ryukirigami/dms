
<%@ page import="com.kombos.administrasi.CompanyDealer; com.kombos.hrd.Karyawan" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="karyawan_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
    <thead>
    <tr>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.m111ID.label" default="NPK" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.m111Nama.label" default="Nama Karyawan" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.m111ID.label" default="Company Dealer" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.satuan.label" default="Tanggal Lahir" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.m111ID.label" default="Alamat" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.m111Nama.label" default="Pekerjaan" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.satuan.label" default="Status Karyawan" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.m111Nama.label" default="Tgl Masuk" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="goods.satuan.label" default="Avalaible" /></div>
        </th>

    </tr>
    <tr>


        <th style="border-top: none;padding: 5px;">
            <div id="filter_npk" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_npk" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_nama" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_nama" class="search_init" />
            </div>
        </th>
        <th style="border-top: none;padding: 5px;">
            <div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 185px;">
                <g:if test="${params.companyDealer.toString().contains("HO")}">
                    <g:select name="search_companyDealer" id="workshop" from="${CompanyDealer.createCriteria().list{eq('staDel','0');order('m011NamaWorkshop')}}" optionKey="id" optionValue="m011NamaWorkshop" style="width: 100%" noSelection="['':'SEMUA']" onchange="reloadKaryawanTable()" />
                </g:if>
            </div>
        </th>
        <th style="border-top: none;padding: 5px;">
            <div id="filter_tglLahir" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="hidden" name="search_tanggalLahir" value="date.struct">
                <input type="hidden" name="search_tanggalLahir_day" id="search_tanggalLahir_day" value="">
                <input type="hidden" name="search_tanggalLahir_month" id="search_tanggalLahir_month" value="">
                <input type="hidden" name="search_tanggalLahir_year" id="search_tanggalLahir_year" value="">
                <input type="text" data-date-format="dd/mm/yyyy" name="search_tanggalLahir_dp" value="" id="search_tanggalLahir" class="search_init">

            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_alamat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_alamat" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_pekerjaan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_pekerjaan" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_statusKaryawan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_statusKaryawan" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_tglMasuk" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
                <input type="hidden" name="tanggalMasukFrom" value="date.struct">
                <input type="hidden" name="tanggalMasukFrom_day" id="tanggalMasukFrom_day" value="">
                <input type="hidden" name="tanggalMasukFrom_month" id="tanggalMasukFrom_month" value="">
                <input type="hidden" name="tanggalMasukFrom_year" id="tanggalMasukFrom_year" value="">
                <input type="text" data-date-format="dd/mm/yyyy" name="tanggalMasukFrom_dp" value="" id="tanggalMasukFrom" placeholder="Date from.." class="search_init">

                <input type="hidden" name="tanggalMasukTo" value="date.struct">
                <input type="hidden" name="tanggalMasukTo_day" id="tanggalMasukTo_day" value="">
                <input type="hidden" name="tanggalMasukTo_month" id="tanggalMasukTo_month" value="">
                <input type="hidden" name="tanggalMasukTo_year" id="tanggalMasukTo_year" value="">
                <input type="text" data-date-format="dd/mm/yyyy" name="tanggalMasukTo_dp" value="" id="tanggalMasukTo" style="margin-top: 5px;" placeholder="Date to.." class="search_init">
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_available" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
               <select name="search_available" id="search_available" style="width: 110px" onchange="reloadKaryawanTable()">
                   <option value="">Semua</option>
                   <option value="1">Available</option>
                   <option value="0">Tidak Available</option>
               </select>
            </div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var karyawanTable;
var reloadKaryawanTable;
$(function(){
	
	reloadKaryawanTable = function() {
		karyawanTable.fnDraw();
	}

	$('#search_tanggalLahir').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tanggalLahir_day').val(newDate.getDate());
			$('#search_tanggalLahir_month').val(newDate.getMonth()+1);
			$('#search_tanggalLahir_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			karyawanTable.fnDraw();	
	});

	

	$('#tanggalMasukFrom').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#tanggalMasukFrom_day').val(newDate.getDate());
			$('#tanggalMasukFrom_month').val(newDate.getMonth()+1);
			$('#tanggalMasukFrom_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			karyawanTable.fnDraw();	
	});

	$('#tanggalMasukTo').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#tanggalMasukTo_day').val(newDate.getDate());
			$('#tanggalMasukTo_month').val(newDate.getMonth()+1);
			$('#tanggalMasukTo_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			karyawanTable.fnDraw();
	});

	karyawanTable = $('#karyawan_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "nomorPokokKaryawan",
	"mDataProp": "nomorPokokKaryawan",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+', \'span12\');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "nama",
	"mDataProp": "nama",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "branch",
	"mDataProp": "cabang",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "tanggalLahir",
	"mDataProp": "tanggalLahir",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "alamat",
	"mDataProp": "alamat",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "jabatan",
	"mDataProp": "jabatan",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "statusKaryawan",
	"mDataProp": "statusKaryawan",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "tanggalMasuk",
	"mDataProp": "tanggalMasuk",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "staAvailable",
	"mDataProp": "staAvailable",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
                        var npk = $("#filter_npk input").val();

                        if (npk) {
                            aoData.push({"name": "sCriteria_npk", "value": npk});
                        }

                         var companyDealer = $("#workshop").val();

                        if (companyDealer) {
                            aoData.push({"name": "sCriteria_companyDealer", "value": companyDealer});
                        }

                         var namaKaryawan = $("#filter_nama input").val();

                        if (namaKaryawan) {
                            aoData.push({"name": "sCriteria_nama", "value": namaKaryawan});
                        }

                        var alamat = $("#filter_alamat input").val();

                        if (alamat) {
                            aoData.push({"name": "sCriteria_alamat", "value": alamat});
                        }

                        var pekerjaan = $("#filter_pekerjaan input").val();

                        if (pekerjaan) {
                            aoData.push({"name": "sCriteria_pekerjaan", "value": pekerjaan});
                        }

                        var tanggalLahir = $('#search_tanggalLahir').val();
						var tanggalLahirDay = $('#search_tanggalLahir_day').val();
						var tanggalLahirMonth = $('#search_tanggalLahir_month').val();
						var tanggalLahirYear = $('#search_tanggalLahir_year').val();

						if(tanggalLahir){
							aoData.push(
									{"name": 'sCriteria_tanggalLahir', "value": "date.struct"},
									{"name": 'sCriteria_tanggalLahir_dp', "value": tanggalLahir},
									{"name": 'sCriteria_tanggalLahir_day', "value": tanggalLahirDay},
									{"name": 'sCriteria_tanggalLahir_month', "value": tanggalLahirMonth},
									{"name": 'sCriteria_tanggalLahir_year', "value": tanggalLahirYear}
							);
						}

                        var statusKaryawan = $("#filter_statusKaryawan input").val();

                        if (statusKaryawan) {
                            aoData.push({"name": "sCriteria_statusKaryawan", "value": statusKaryawan});
                        }

                        var tanggalMasukFrom = $('#tanggalMasukFrom').val();
						var tanggalMasukFromDay = $('#tanggalMasukFrom_day').val();
						var tanggalMasukFromMonth = $('#tanggalMasukFrom_month').val();
						var tanggalMasukFromYear = $('#tanggalMasukFrom_year').val();

						if(tanggalMasukFrom){
							aoData.push(
									{"name": 'sCriteria_tanggalMasukFrom', "value": "date.struct"},
									{"name": 'sCriteria_tanggalMasukFrom_dp', "value": tanggalMasukFrom},
									{"name": 'sCriteria_tanggalMasukFrom_day', "value": tanggalMasukFromDay},
									{"name": 'sCriteria_tanggalMasukFrom_month', "value": tanggalMasukFromMonth},
									{"name": 'sCriteria_tanggalMasukFrom_year', "value": tanggalMasukFromYear}
							);
						}

                        var tanggalMasukTo = $('#tanggalMasukTo').val();
						var tanggalMasukToDay = $('#tanggalMasukTo_day').val();
						var tanggalMasukToMonth = $('#tanggalMasukTo_month').val();
						var tanggalMasukToYear = $('#tanggalMasukTo_year').val();

						if(tanggalMasukTo){
							aoData.push(
									{"name": 'sCriteria_tanggalMasukTo', "value": "date.struct"},
									{"name": 'sCriteria_tanggalMasukTo_dp', "value": tanggalMasukTo},
									{"name": 'sCriteria_tanggalMasukTo_day', "value": tanggalMasukToDay},
									{"name": 'sCriteria_tanggalMasukTo_month', "value": tanggalMasukToMonth},
									{"name": 'sCriteria_tanggalMasukTo_year', "value": tanggalMasukToYear}
							);
						}

                        var available = $("#filter_available select").val();

                        if (available) {
                            aoData.push({name: "sCriteria_staAvailable", "value": available});
                        }

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
