<%@ page import="com.kombos.administrasi.CompanyDealer; com.kombos.administrasi.ManPower; com.kombos.hrd.Karyawan" %>

<g:if test="${params.action.equals('edit')}">
    <div class="span4" style="margin-left: 0;">
</g:if>
<g:else>
    <div class="span6">
</g:else>

    <div class="control-group fieldcontain ${hasErrors(bean: karyawanInstance, field: 'nomorPokokKaryawan', 'error')} ">
        <label class="control-label" for="nomorPokokKaryawan">
            <g:message code="karyawan.nomorPokokKaryawan.label" default="Nomor Pokok Karyawan" />
            <span class="required-indicator">*</span>
        </label>
        <div class="controls">
            <g:textField name="nomorPokokKaryawan" value="${karyawanInstance?.nomorPokokKaryawan}" maxlength="16" required="" />
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: karyawanInstance, field: 'tanggalMasuk', 'error')} ">
        <label class="control-label" for="tanggalMasuk">
            <g:message code="karyawan.tanggalMasuk.label" default="Tanggal Masuk" />
        </label>
        <div class="controls">
            <ba:datePicker name="tanggalMasuk" precision="day" value="${karyawanInstance?.tanggalMasuk}" format="dd/MM/yyyy"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: karyawanInstance, field: 'branch', 'error')} ">
        <label class="control-label" for="branch">
            <g:message code="karyawan.branch.label" default="Cabang" />
            <span class="required-indicator">*</span>
        </label>
        <div class="controls">
            <g:if test="${data.role=='HRD'}">
                <g:hiddenField name="branch.id" value="${data.dealerId}" />
                <label class="control-label" for="dealer">
                    ${data.dealer}
                </label>
            </g:if>
            <g:else>
                <g:select id="branch" name="branch.id" from="${CompanyDealer.createCriteria().list{eq("staDel","0");order("m011NamaWorkshop")}}" optionKey="id" required="" optionValue="m011NamaWorkshop" value="${karyawanInstance?.branch?.id}" class="many-to-one"/>
            </g:else>
        </div>
    </div>


    <div class="control-group fieldcontain ${hasErrors(bean: karyawanInstance, field: 'nama', 'error')} ">
        <label class="control-label" for="nama">
            <g:message code="karyawan.nama.label" default="Nama" />
        </label>
        <div class="controls">
            <g:textField name="nama" value="${karyawanInstance?.nama}" maxlength="100" />
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: karyawanInstance, field: 'tanggalLahir', 'error')} ">
        <label class="control-label" for="tanggalLahir">
            <g:message code="karyawan.tanggalLahir.label" default="Tanggal Lahir" />
        </label>
        <div class="controls">
            %{--<ba:datePicker name="tanggalLahir" precision="day" value="${karyawanInstance?.tanggalLahir}" format="dd/MM/yyyy"/>--}%
            <g:datePicker name="tanggalLahir" precision="day" value="${karyawanInstance?.tanggalLahir}"/>
        </div>
    </div>

<g:if test="${params.action.equals('edit')}">
    </div>
    <div class="span4" style="margin-left: 0;">
</g:if>

    <div class="control-group fieldcontain ${hasErrors(bean: karyawanInstance, field: 'jenisKelamin', 'error')} ">
        <label class="control-label" for="jenisKelamin">
            <g:message code="karyawan.jenisKelamin.label" default="Jenis Kelamin" />

        </label>
        <div class="controls">
        %{--<g:radioGroup type="number" name="jenisKelamin" value="${karyawanInstance.jenisKelamin}" />--}%
            <g:radioGroup name="jenisKelamin" value="${karyawanInstance.jenisKelamin}" id="jenisKelamin"
                          labels="['Pria','Wanita']"
                          values="[0,1]">
                <label>
                    <span class="radioSpan">${it.radio}</span>
                    ${it.label}
                </label>

            </g:radioGroup>
        </div>
    </div>


    <div class="control-group fieldcontain ${hasErrors(bean: karyawanInstance, field: 'pendidikan', 'error')} ">
        <label class="control-label" for="pendidikan">
            <g:message code="karyawan.pendidikan.label" default="Level Pendidikan" />
            <span class="required-indicator">*</span>
        </label>
        <div class="controls">
            <g:select name="pendidikan" from="${com.kombos.hrd.Pendidikan.list()}" multiple="multiple" optionKey="id" size="5" required="" value="${karyawanInstance?.pendidikan*.id}" class="many-to-many"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: karyawanInstance, field: 'agama', 'error')} ">
        <label class="control-label" for="agama">
            <g:message code="karyawan.agama.label" default="Agama" />
        </label>
        <div class="controls">
            <g:select id="agama" name="agama.id" from="${com.kombos.customerprofile.Agama.list()}" optionKey="id" required="" value="${karyawanInstance?.agama?.id}" class="many-to-one"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: karyawanInstance, field: 'agama', 'error')} ">
        <label class="control-label" for="agama">
            <g:message code="karyawan.statusNikah.label" default="Status Nikah" />
        </label>
        <div class="controls">
            <g:select id="statusNikah" name="statusNikah.id" from="${com.kombos.maintable.Nikah.list()}" optionKey="id" required="" optionValue="m062StaNikah" value="${karyawanInstance?.statusNikah?.id}" class="many-to-one"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: karyawanInstance, field: 'telepon', 'error')} ">
        <label class="control-label" for="telepon">
            <g:message code="karyawan.alamat.label" default="Alamat" />
        </label>
        <div class="controls">
            <g:textField name="alamat" value="${karyawanInstance?.alamat}" maxlength="200"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: karyawanInstance, field: 'kota', 'error')} ">
        <label class="control-label" for="kota">
            <g:message code="karyawan.kota.label" default="Kota" />
        </label>
        <div class="controls">
            <g:select id="kota" name="kota.id" from="${com.kombos.administrasi.KabKota.createCriteria().list{order("m002NamaKabKota","asc")} }" optionKey="id" required="" value="${karyawanInstance?.kota?.id}" class="many-to-one"/>
        </div>
    </div>

<g:if test="${params.action.equals('edit')}">
    </div>
    <div class="span4" style="margin-left: 0;">
</g:if>

    <div class="control-group fieldcontain ${hasErrors(bean: karyawanInstance, field: 'telepon', 'error')} ">
        <label class="control-label" for="telepon">
            <g:message code="karyawan.telepon.label" default="Telepon" />
        </label>
        <div class="controls">
            <g:textField name="telepon" value="${karyawanInstance?.telepon}" />
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: karyawanInstance, field: 'emailAddress', 'error')} ">
        <label class="control-label" for="emailAddress">
            <g:message code="karyawan.emailAddress.label" default="Email Address" />
            <span class="required-indicator">*</span>
        </label>
        <div class="controls">
            <g:textField name="emailAddress" value="${karyawanInstance?.emailAddress}" required="" />
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: karyawanInstance, field: 'divisi', 'error')} ">
        <label class="control-label" for="divisi">
            <g:message code="karyawan.divisi.label" default="Divisi" />
        </label>
        <div class="controls">
            <g:select id="divisi" name="divisi.id" from="${com.kombos.administrasi.Divisi.createCriteria().list {eq("companyDealer", CompanyDealer.findById(session.userCompanyDealerId))}}" optionKey="id" required="" value="${karyawanInstance?.divisi?.id}" class="many-to-one"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: karyawanInstance, field: 'jabatan', 'error')} ">
        <label class="control-label" for="jabatan">
            <g:message code="karyawan.jabatan.label" default="Jabatan" />
        </label>
        <div class="controls">
            <g:select id="jabatan" name="jabatan.id" from="${com.kombos.administrasi.ManPower.createCriteria().list {order("m014JabatanManPower","asc")}}" optionKey="id" optionValue="m014JabatanManPower" required="" value="${karyawanInstance?.jabatan?.id}" class="many-to-one"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: karyawanInstance, field: 'statusKaryawan', 'error')} ">
        <label class="control-label" for="statusKaryawan">
            <g:message code="karyawan.statusKaryawan.label" default="Status Karyawan" />

        </label>
        <div class="controls">
                <g:select id="statusKaryawan" name="statusKaryawan.id" from="${com.kombos.hrd.StatusKaryawan.list()}" optionKey="id" optionValue="statusKaryawan" required="" value="${karyawanInstance?.statusKaryawan?.id}" class="many-to-one"/>
        </div>
    </div>

<g:if test="${params.action.equals('edit')}">
    </div>
    <div class="span4" style="margin-left: 0;">
</g:if>

    <g:if test="${params.action.equals("edit")}">
        <div class="control-group fieldcontain ${hasErrors(bean: karyawanInstance, field: 'staAvailable', 'error')} ">
            <label class="control-label" for="statusKaryawan">
                <g:message code="karyawan.staAvailable.label" default="Status" />
            </label>
            <div class="controls">
                <g:if test="${karyawanInstance.staAvailable.equals("1")}">
                    Available
                </g:if>
                <g:else>
                    Unavailable
                </g:else>
            </div>
        </div>
    </g:if>
</div>

<g:if test="${params.action.equals("edit")}">
    <div class="span12" style="margin-left: 0;">
        <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
            <li class="active"><a href="#histori" data-toggle="tab">Histori</a></li>
            <li><a href="#sertifikasi" data-toggle="tab">Sertifikasi</a></li>
            <li><a href="#reward" data-toggle="tab">Reward</a></li>
            <li><a href="#warning" data-toggle="tab">Warning</a></li>
            <li><a href="#keluarga" data-toggle="tab">Keluarga</a></li>
            <li><a href="#pk" data-toggle="tab">Pendidikan</a></li>
        </ul>
        <div id="my-tab-content" class="tab-content" style="border: none;">
            <div class="tab-pane active" id="histori">
                <div class="box">
                    <div id="historyKaryawan-table" class="table-container span12">
                        <g:render template="dataTablesHistori" />
                    </div>
                    <div class="form-container span7" id="historyKaryawan-form" style="display: none;"></div>
                </div>
            </div>
            <div class="tab-pane" id="sertifikasi">
                <div class="box">
                    <div id="certificationKaryawan-table" class="span12">
                        <g:render template="dataTablesSertifikasi" />
                    </div>
                    <div class="form-container span7" id="certificationKaryawan-form" style="display: none;"></div>
                </div>
            </div>
            <div class="tab-pane" id="reward">
                <div class="box">
                    <div id="historyRewardKaryawan-table" class="span12">
                        <g:render template="dataTablesReward" />
                    </div>
                    <div class="form-container span7" id="historyRewardKaryawan-form" style="display: none;"></div>
                </div>
            </div>
            <div class="tab-pane" id="warning">
                <div class="box">
                    <div id="historyWarningKaryawan-table" class="span12">
                        <g:render template="dataTablesWarning" />
                    </div>
                    <div class="form-container span7" id="historyWarningKaryawan-form" style="display: none;"></div>
                </div>
            </div>
            <div class="tab-pane" id="keluarga">
                <div class="box">
                    <div id="keluargaKaryawan-table" class="span12">
                        <g:render template="dataTablesKeluarga" />
                    </div>
                    <div class="form-container span7" id="keluargaKaryawan-form" style="display: none;"></div>
                </div>
            </div>
            <div class="tab-pane" id="pk">
                <div class="box">
                    <div id="pendidikanKaryawan-table" class="span12">
                        <g:render template="dataTablesPendidikan" />
                    </div>
                    <div class="form-container span7" id="pendidikanKaryawan-form" style="display: none;"></div>
                </div>
            </div>
        </div>
    </div>
</g:if>