

<%@ page import="com.kombos.hrd.Karyawan" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'karyawan.label', default: 'Karyawan')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteKaryawan;

$(function(){ 
	deleteKaryawan=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/karyawan/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadKaryawanTable();
   				expandTableLayout('karyawan');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-karyawan" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="karyawan"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${karyawanInstance?.absensiKaryawan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="absensiKaryawan-label" class="property-label"><g:message
					code="karyawan.absensiKaryawan.label" default="Absensi Karyawan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="absensiKaryawan-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="absensiKaryawan"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter absensiKaryawan" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:each in="${karyawanInstance.absensiKaryawan}" var="a">
								<g:link controller="absensiKaryawan" action="show" id="${a.id}">${a?.id?.encodeAsHTML()}</g:link>
								</g:each>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.accountNumber}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="accountNumber-label" class="property-label"><g:message
					code="karyawan.accountNumber.label" default="Account Number" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="accountNumber-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="accountNumber"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter accountNumber" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:link controller="accountNumber" action="show" id="${karyawanInstance?.accountNumber?.id}">${karyawanInstance?.accountNumber?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.agama}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="agama-label" class="property-label"><g:message
					code="karyawan.agama.label" default="Agama" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="agama-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="agama"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter agama" onsuccess="reloadKaryawanTable();" />--}%
							
								%{--<g:link controller="agama" action="show" id="${karyawanInstance?.agama?.id}">${karyawanInstance?.agama?.encodeAsHTML()}</g:link>--}%
							    <g:fieldValue field="agama" bean="${karyawanInstance}"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.alamat}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="alamat-label" class="property-label"><g:message
					code="karyawan.alamat.label" default="Alamat" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="alamat-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="alamat"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter alamat" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:fieldValue bean="${karyawanInstance}" field="alamat"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.branch}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="branch-label" class="property-label"><g:message
					code="karyawan.branch.label" default="Branch" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="branch-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="branch"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter branch" onsuccess="reloadKaryawanTable();" />--}%
							
								%{--<g:link controller="branchHRD" action="show" id="${karyawanInstance?.branch?.id}">${karyawanInstance?.branch?.encodeAsHTML()}</g:link>--}%
                                <g:fieldValue bean="${karyawanInstance}" field="branch"/>
                        </span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="karyawan.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="createdBy"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:fieldValue bean="${karyawanInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.cutiKaryawan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="cutiKaryawan-label" class="property-label"><g:message
					code="karyawan.cutiKaryawan.label" default="Cuti Karyawan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="cutiKaryawan-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="cutiKaryawan"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter cutiKaryawan" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:each in="${karyawanInstance.cutiKaryawan}" var="c">
								<g:link controller="cutiKaryawan" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link>
								</g:each>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="karyawan.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="dateCreated"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:formatDate date="${karyawanInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.divisi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="divisi-label" class="property-label"><g:message
					code="karyawan.divisi.label" default="Divisi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="divisi-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="divisi"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter divisi" onsuccess="reloadKaryawanTable();" />--}%
							
								%{--<g:link controller="divisiHRD" action="show" id="${karyawanInstance?.divisi?.id}">${karyawanInstance?.divisi?.encodeAsHTML()}</g:link>--}%
                                <g:fieldValue bean="${karyawanInstance}" field="divisi"/>

						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.emailAddress}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="emailAddress-label" class="property-label"><g:message
					code="karyawan.emailAddress.label" default="Email Address" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="emailAddress-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="emailAddress"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter emailAddress" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:fieldValue bean="${karyawanInstance}" field="emailAddress"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
%{--JANGAN DI HAPUS DULU--}%
				%{--<g:if test="${karyawanInstance?.historyKaryawan}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="historyKaryawan-label" class="property-label"><g:message--}%
					%{--code="karyawan.historyKaryawan.label" default="History Karyawan" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="historyKaryawan-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${karyawanInstance}" field="historyKaryawan"--}%
								%{--url="${request.contextPath}/Karyawan/updatefield" type="text"--}%
								%{--title="Enter historyKaryawan" onsuccess="reloadKaryawanTable();" />--}%
							%{----}%
								%{--<g:each in="${karyawanInstance.historyKaryawan}" var="h">--}%
								%{--<g:link controller="historyKaryawan" action="show" id="${h.id}">${h?.encodeAsHTML()}</g:link>--}%
								%{--</g:each>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			
				<g:if test="${karyawanInstance?.historyRewardKaryawan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="historyRewardKaryawan-label" class="property-label"><g:message
					code="karyawan.historyRewardKaryawan.label" default="History Reward Karyawan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="historyRewardKaryawan-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="historyRewardKaryawan"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter historyRewardKaryawan" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:each in="${karyawanInstance.historyRewardKaryawan}" var="h">
								<g:link controller="historyRewardKaryawan" action="show" id="${h.id}">${h?.encodeAsHTML()}</g:link>
								</g:each>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.historyWarningKaryawan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="historyWarningKaryawan-label" class="property-label"><g:message
					code="karyawan.historyWarningKaryawan.label" default="History Warning Karyawan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="historyWarningKaryawan-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="historyWarningKaryawan"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter historyWarningKaryawan" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:each in="${karyawanInstance.historyWarningKaryawan}" var="h">
								<g:link controller="historyWarningKaryawan" action="show" id="${h.id}">${h?.encodeAsHTML()}</g:link>
								</g:each>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.hobby}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="hobby-label" class="property-label"><g:message
					code="karyawan.hobby.label" default="Hobby" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="hobby-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="hobby"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter hobby" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:each in="${karyawanInstance.hobby}" var="h">
								<g:link controller="hobby" action="show" id="${h.id}">${h?.encodeAsHTML()}</g:link>
								</g:each>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.jabatan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="jabatan-label" class="property-label"><g:message
					code="karyawan.jabatan.label" default="Jabatan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="jabatan-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="jabatan"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter jabatan" onsuccess="reloadKaryawanTable();" />--}%
							
								%{--<g:link controller="jabatan" action="show" id="${karyawanInstance?.jabatan?.id}">${karyawanInstance?.jabatan?.encodeAsHTML()}</g:link>--}%
                                <g:fieldValue bean="${karyawanInstance}" field="jabatan"/>
                        </span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.jamsostekIDNumber}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="jamsostekIDNumber-label" class="property-label"><g:message
					code="karyawan.jamsostekIDNumber.label" default="Jamsostek IDN umber" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="jamsostekIDNumber-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="jamsostekIDNumber"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter jamsostekIDNumber" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:fieldValue bean="${karyawanInstance}" field="jamsostekIDNumber"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.jenisKelamin}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="jenisKelamin-label" class="property-label"><g:message
					code="karyawan.jenisKelamin.label" default="Jenis Kelamin" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="jenisKelamin-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="jenisKelamin"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter jenisKelamin" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:fieldValue bean="${karyawanInstance}" field="jenisKelamin"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.kota}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kota-label" class="property-label"><g:message
					code="karyawan.kota.label" default="Kota" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kota-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="kota"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter kota" onsuccess="reloadKaryawanTable();" />--}%
							
								%{--<g:link controller="kabKota" action="show" id="${karyawanInstance?.kota?.id}">${karyawanInstance?.kota?.encodeAsHTML()}</g:link>--}%
                        <g:fieldValue bean="${karyawanInstance}" field="kota"/>

						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.kotaLahir}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kotaLahir-label" class="property-label"><g:message
					code="karyawan.kotaLahir.label" default="Kota Lahir" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kotaLahir-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="kotaLahir"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter kotaLahir" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:link controller="kabKota" action="show" id="${karyawanInstance?.kotaLahir?.id}">${karyawanInstance?.kotaLahir?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="karyawan.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="lastUpdProcess"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:fieldValue bean="${karyawanInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="karyawan.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="lastUpdated"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:formatDate date="${karyawanInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.nama}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="nama-label" class="property-label"><g:message
					code="karyawan.nama.label" default="Nama" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="nama-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="nama"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter nama" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:fieldValue bean="${karyawanInstance}" field="nama"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.nomorPokokKaryawan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="nomorPokokKaryawan-label" class="property-label"><g:message
					code="karyawan.nomorPokokKaryawan.label" default="Nomor Pokok Karyawan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="nomorPokokKaryawan-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="nomorPokokKaryawan"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter nomorPokokKaryawan" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:fieldValue bean="${karyawanInstance}" field="nomorPokokKaryawan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.pendidikan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="pendidikan-label" class="property-label"><g:message
					code="karyawan.pendidikan.label" default="Pendidikan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="pendidikan-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="pendidikan"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter pendidikan" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:each in="${karyawanInstance.pendidikan}" var="p">
								%{--<g:link controller="pendidikan" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link>--}%
                                    <g:fieldValue bean="${karyawanInstance}" field="pendidikan"/>
                                </g:each>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.sisaCutiKaryawan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="sisaCutiKaryawan-label" class="property-label"><g:message
					code="karyawan.sisaCutiKaryawan.label" default="Sisa Cuti Karyawan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="sisaCutiKaryawan-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="sisaCutiKaryawan"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter sisaCutiKaryawan" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:each in="${karyawanInstance.sisaCutiKaryawan}" var="s">
								<g:link controller="sisaCutiKaryawan" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link>
								</g:each>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.staAvailable}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staAvailable-label" class="property-label"><g:message
					code="karyawan.staAvailable.label" default="Sta Available" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staAvailable-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="staAvailable"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter staAvailable" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:fieldValue bean="${karyawanInstance}" field="staAvailable"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="karyawan.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="staDel"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:fieldValue bean="${karyawanInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.statusKaryawan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="statusKaryawan-label" class="property-label"><g:message
					code="karyawan.statusKaryawan.label" default="Status Karyawan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="statusKaryawan-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="statusKaryawan"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter statusKaryawan" onsuccess="reloadKaryawanTable();" />--}%
							
								%{--<g:link controller="statusKaryawan" action="show" id="${karyawanInstance?.statusKaryawan?.id}">${karyawanInstance?.statusKaryawan?.statusKaryawan.encodeAsHTML()}</g:link>--}%
                                <g:fieldValue field="statusKaryawan" bean="${karyawanInstance?.statusKaryawan}" />
                        </span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.statusNikah}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="statusNikah-label" class="property-label"><g:message
					code="karyawan.statusNikah.label" default="Status Nikah" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="statusNikah-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="statusNikah"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter statusNikah" onsuccess="reloadKaryawanTable();" />--}%
							
								%{--<g:link controller="nikah" action="show" id="${karyawanInstance?.statusNikah?.id}">${karyawanInstance?.statusNikah?.encodeAsHTML()}</g:link>--}%
                            <g:fieldValue bean="${karyawanInstance}" field="statusNikah"/>
                        </span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.tanggalLahir}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tanggalLahir-label" class="property-label"><g:message
					code="karyawan.tanggalLahir.label" default="Tanggal Lahir" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tanggalLahir-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="tanggalLahir"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter tanggalLahir" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:formatDate date="${karyawanInstance?.tanggalLahir}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.tanggalMasuk}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tanggalMasuk-label" class="property-label"><g:message
					code="karyawan.tanggalMasuk.label" default="Tanggal Masuk" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tanggalMasuk-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="tanggalMasuk"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter tanggalMasuk" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:formatDate date="${karyawanInstance?.tanggalMasuk}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.telepon}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="telepon-label" class="property-label"><g:message
					code="karyawan.telepon.label" default="Telepon" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="telepon-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="telepon"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter telepon" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:fieldValue bean="${karyawanInstance}" field="telepon"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.trainingInstructor}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="trainingInstructor-label" class="property-label"><g:message
					code="karyawan.trainingInstructor.label" default="Training Instructor" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="trainingInstructor-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="trainingInstructor"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter trainingInstructor" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:each in="${karyawanInstance.trainingInstructor}" var="t">
								<g:link controller="trainingInstructor" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link>
								</g:each>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.trainingMember}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="trainingMember-label" class="property-label"><g:message
					code="karyawan.trainingMember.label" default="Training Member" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="trainingMember-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="trainingMember"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter trainingMember" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:each in="${karyawanInstance.trainingMember}" var="t">
								<g:link controller="trainingMember" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link>
								</g:each>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.tugasLuarKaryawan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tugasLuarKaryawan-label" class="property-label"><g:message
					code="karyawan.tugasLuarKaryawan.label" default="Tugas Luar Karyawan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tugasLuarKaryawan-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="tugasLuarKaryawan"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter tugasLuarKaryawan" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:each in="${karyawanInstance.tugasLuarKaryawan}" var="t">
								<g:link controller="tugasLuarKaryawan" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link>
								</g:each>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${karyawanInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="karyawan.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${karyawanInstance}" field="updatedBy"
								url="${request.contextPath}/Karyawan/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadKaryawanTable();" />--}%
							
								<g:fieldValue bean="${karyawanInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
                   onclick="shrinkLayout('karyawan-form');"><g:message
						code="default.button.cancel.label" default="Close" /></a>
				%{--<g:remoteLink class="btn btn-primary edit" action="edit"--}%
					%{--id="${karyawanInstance?.id}"--}%
					%{--update="[success:'karyawan-form',failure:'karyawan-form']"--}%
					%{--on404="alert('not found');">--}%
					%{--<g:message code="default.button.edit.label" default="Edit" />--}%
				%{--</g:remoteLink>--}%
				%{--<ba:confirm id="delete" class="btn cancel"--}%
					%{--message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"--}%
					%{--onsuccess="deleteKaryawan('${karyawanInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>--}%
			</fieldset>
		</g:form>
	</div>
</body>
</html>
