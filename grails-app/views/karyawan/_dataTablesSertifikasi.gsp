<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<div class="navbar box-header no-border">
    <ul class="nav pull-right">
        <li><a class="pull-right box-action _new" href="javascript:void(0);"
               style="display: block;">&nbsp;&nbsp;<i
                    class="icon-plus" onclick="<g:remoteFunction action="create"
                                                                 controller="certificationKaryawan"
                                                                 onLoading="jQuery('#spinner').fadeIn(1);"
                                                                 onSuccess="loadFormInstance('certificationKaryawan', data, textStatus);"
                                                                 onComplete="jQuery('#spinner').fadeOut();shrinkTableLayout('certificationKaryawan');"
                                                                 params="'onDataKaryawan=true&karyawanId=${karyawanInstance?.id}'"
                    />"></i>&nbsp;&nbsp;
        </a></li>
        <li><a class="pull-right box-action _delete" href="#"
               style="display: block;" onclick="oHistoryService.massDelete('certificationKaryawan', 'certificationKaryawan', reloadCertificationKaryawanTable);">&nbsp;&nbsp;<i
                    class="icon-remove"></i>&nbsp;&nbsp;
        </a></li>
    </ul>
</div>

<table id="certificationKaryawan_table" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%" style="width: 1225px;">

</table>

<g:javascript>
var certificationKaryawanTable;
var reloadCertificationKaryawanTable;
$(function(){

	reloadCertificationKaryawanTable = function() {
		certificationKaryawanTable.fnDraw();
	}


	$('#search_tglSertifikasi').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tglSertifikasi_day').val(newDate.getDate());
			$('#search_tglSertifikasi_month').val(newDate.getMonth()+1);
			$('#search_tglSertifikasi_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			certificationKaryawanTable.fnDraw();
	});




$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	certificationKaryawanTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	certificationKaryawanTable = $('#certificationKaryawan_table').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList", controller: "certificationKaryawan")}",
		"aoColumns": [

{
    "sTitle": "Sertifikasi",
	"sName": "tipeSertifikasi",
	"mDataProp": "tipeSertifikasi",
	"aTargets": [4],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="oHistoryService.edit(\'certificationKaryawan\','+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,


{
    "sTitle": "Tanggal",
	"sName": "tglSertifikasi",
	"mDataProp": "tglSertifikasi",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
    "sTitle": "Training",
	"sName": "training",
	"mDataProp": "training",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        aoData.push(
                                {"name": 'idKaryawan', "value": "${karyawanInstance?.id}"},
                                {"name": 'dari', "value": "karyawan"}
                        );

						var tglSertifikasi = $('#search_tglSertifikasi').val();
						var tglSertifikasiDay = $('#search_tglSertifikasi_day').val();
						var tglSertifikasiMonth = $('#search_tglSertifikasi_month').val();
						var tglSertifikasiYear = $('#search_tglSertifikasi_year').val();

						if(tglSertifikasi){
							aoData.push(
									{"name": 'sCriteria_tglSertifikasi', "value": "date.struct"},
									{"name": 'sCriteria_tglSertifikasi_dp', "value": tglSertifikasi},
									{"name": 'sCriteria_tglSertifikasi_day', "value": tglSertifikasiDay},
									{"name": 'sCriteria_tglSertifikasi_month', "value": tglSertifikasiMonth},
									{"name": 'sCriteria_tglSertifikasi_year', "value": tglSertifikasiYear}
							);
						}

						var tipeSertifikasi = $('#filter_tipeSertifikasi input').val();
						if(tipeSertifikasi){
							aoData.push(
									{"name": 'sCriteria_tipeSertifikasi', "value": tipeSertifikasi}
							);
						}

						var training = $('#filter_training input').val();
						if(training){
							aoData.push(
									{"name": 'sCriteria_training', "value": training}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>



