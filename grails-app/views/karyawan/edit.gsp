<%@ page import="com.kombos.hrd.Karyawan" %>
<%@ page import="com.kombos.hrd.HistoryKaryawan" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'karyawan.label', default: 'Karyawan')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
        <style>
            fieldset .box input, fieldset .box textarea, fieldset .box .uneditable-input {width: auto !important;}
        </style>
        <g:javascript>
            var oHistoryService;
            $(function() {

			    oHistoryService = {
			        edit: function(controller,id) {
			            shrinkTableLayout(controller);
                        $.ajax({
                            type: "GET",
                            url: "${request.getContextPath()}/" + controller + "/edit/" + id,
                            data: {onDataKaryawan: true},
                            success:function(data,textStatus){
                                loadFormInstance(controller, data, textStatus);
                            },
                            error:function(XMLHttpRequest,textStatus,errorThrown){},
                            complete:function(XMLHttpRequest,textStatus){
                                $('#spinner').fadeOut();
                            }
                        });
			        },
			        massDelete: function(tblName, controller, callback) {
			            bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
								function(result){
									if(result){
										massDelete(tblName, '${request.contextPath}/' + controller + "/massdelete", function() {
										    callback();
										});
									}
								});
			        }

			    }

                // =====================================================================================================

                $("#histori .action ._new").click(function() {
                    var id = $("#karyawan-id").val();
                    <g:remoteFunction action="create"
                                      controller="historyKaryawan"
                                      onLoading="jQuery('#spinner').fadeIn(1);"
                                      onSuccess="loadFormInstance('historyKaryawan', data, textStatus);"
                                      onComplete="jQuery('#spinner').fadeOut();"
                                      params="'onDataKaryawan=true&karyawanId='+id"
                    />
                    $("#historyRewardKaryawan-modal").modal("show");
                });
                 $("#reward .action ._new").click(function() {
                    var id = $("#karyawan-id").val();
                    <g:remoteFunction action="create"
                                      controller="historyRewardKaryawan"
                                      onLoading="jQuery('#spinner').fadeIn(1);"
                                      onSuccess="loadFormInstance('historyRewardKaryawan', data, textStatus);"
                                      onComplete="jQuery('#spinner').fadeOut();"
                                      params="'onDataKaryawan=true&karyawanId='+id"
                    />
                    $("#historyRewardKaryawan-modal").modal("show");
                });
        $("#historyKaryawan-modal").on("hidden", function() {
            reloadHistoryKaryawanTable();
        });
        $("#historyRewardKaryawan-modal").on("hidden", function() {
            reloadHistoryRewardKaryawanTable();
        })
    })
        </g:javascript>
	</head>
	<body>
		<div id="edit-karyawan" class="content scaffold-edit" role="main">
			<legend><g:message code="default.edit.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${karyawanInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${karyawanInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form method="post" >--}%
			<g:formRemote class="form-horizontal" name="edit" on404="alert('not found!');" onSuccess="reloadKaryawanTable();" update="karyawan-form"
				url="[controller: 'karyawan', action:'update']">
				<g:hiddenField name="id" value="${karyawanInstance?.id}" id="karyawan-id" />
				<g:hiddenField name="version" value="${karyawanInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons controls" style="margin-top: 50px;">
					<g:actionSubmit class="btn btn-primary save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                    <a class="btn cancel" href="javascript:void(0);"
                       onclick="shrinkLayout('karyawan-form');"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
				</fieldset>
			</g:formRemote>
			%{--</g:form>--}%
		</div>
        %{--<div class="modal fade" id="historyKaryawan-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-body" id="historyKaryawan-form">

                </div>
            </div>
        </div>
        <div class="modal fade" id="historyRewardKaryawan-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-body" id="historyRewardKaryawan-form">

                </div>
            </div>
        </div>--}%
	</body>
</html>
