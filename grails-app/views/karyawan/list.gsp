
<%@ page import="com.kombos.hrd.Karyawan" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'karyawan.label', default: 'Data Karyawan')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist, bootstrapdatepicker" />
		<g:javascript>
			var show;
			var edit;
			var btnLayout;
			var previewData;
			$(function(){


				shrinkLayout = function(onSpotlightElement, className) {
				    var _tblForm = $("#karyawan-table");

				    if (_tblForm.hasClass("span12")) {
				        _tblForm.removeClass("span12");
				        _tblForm.fadeOut("fast");

				        if (onSpotlightElement) {
				            $("#" + onSpotlightElement).fadeIn("fast");
				             if (className) {
				                $("#" + onSpotlightElement).removeClass("span7");
                                $("#" + onSpotlightElement).addClass(className);
				            }
				        }
				    } else {
				        _tblForm.addClass("span12");
				        _tblForm.fadeIn("fast");

				        if (onSpotlightElement) {
				            $("#" + onSpotlightElement).fadeOut("fast");
				            if (className) {
				                $("#" + onSpotlightElement).removeClass("span7");
                                $("#" + onSpotlightElement).addClass(className);
				            }
				        }
				    }
				    reloadKaryawanTable();
				};

				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('karyawan');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('karyawan', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('karyawan', '${request.contextPath}/karyawan/massdelete', reloadKaryawanTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('karyawan','${request.contextPath}/karyawan/show/'+id);
				};
				
				edit = function(id, className) {
                    shrinkLayout("karyawan-form", className);
                    $('#spinner').fadeIn(1);
                    $.ajax({
                        type: "GET",
                        url: "${request.getContextPath()}/karyawan/edit/" + id,
                        success:function(data,textStatus){
                            loadFormInstance("karyawan", data, textStatus);
                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
				};


});

 previewData = function(aksi){
        var invApproval = []
        $("#karyawan_datatables .row-select").each(function() {
                if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			invApproval.push(id);
    		}
		});

        if(invApproval.length<1){
		    alert('Anda belum memilih data yang akan diprint');
		    return;
		}
		if(invApproval.length>1){
		    alert('Anda hanya dapat memilih 1 data untuk Print');
		    return;
		}

        window.location = "${request.contextPath}/karyawan/previewtraining?nik="+invApproval[0]+'&aksi='+aksi;

    }


</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="karyawan-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

            <g:render template="dataTables" />

        </div>
		<div class="span7" id="karyawan-form" style="display: none; margin: 0;"></div>
	</div>
</body>
</html>
