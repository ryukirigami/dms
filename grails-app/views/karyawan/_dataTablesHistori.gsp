<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>
%{--

<div class="action pull-right" data-target="histori" style="padding: 10px;">
    <a class="_new" href="javascript:void(0);"><i class="icon-plus"></i> </a>
    <a class="_delete" href="javascript:void(0);" onclick="oHistoryService.massDelete('historyKaryawan', 'historyKaryawan', reloadHistoryKaryawanTable)"><i class="icon-remove"></i> </a>
</div>
--}%

<div class="navbar box-header no-border">
    <ul class="nav pull-right">
        <li><a class="pull-right box-action _new" href="javascript:void(0);"
               style="display: block;">&nbsp;&nbsp;<i
                    class="icon-plus" onclick="<g:remoteFunction action="create"
                                                                 controller="historyKaryawan"
                                                                 onLoading="jQuery('#spinner').fadeIn(1);"
                                                                 onSuccess="loadFormInstance('historyKaryawan', data, textStatus);"
                                                                 onComplete="jQuery('#spinner').fadeOut();shrinkTableLayout('historyKaryawan');"
                                                                 params="'onDataKaryawan=true&karyawanId=${karyawanInstance?.id}'"
                    />"></i>&nbsp;&nbsp;
        </a></li>
        <li><a class="pull-right box-action _delete" href="#"
               style="display: block;" onclick="oHistoryService.massDelete('historyKaryawan', 'historyKaryawan', reloadHistoryKaryawanTable);">&nbsp;&nbsp;<i
                    class="icon-remove"></i>&nbsp;&nbsp;
        </a></li>
    </ul>
</div>

<table id="historyKaryawan_table" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%" style="width: 1225px;">
</table>

<g:javascript>
var historyKaryawanTable;
var reloadHistoryKaryawanTable;
$(function(){

	reloadHistoryKaryawanTable = function() {
		historyKaryawanTable.fnDraw();
	}

    $("#btnCari").click(function() {
	    reloadHistoriKaryawanTable();
    });


        historyKaryawanTable = $('#historyKaryawan_table').dataTable({
            "sScrollX": "100%",
            "bScrollCollapse": true,
            "bAutoWidth" : false,
            "bPaginate" : true,
            "sInfo" : "",
            "sInfoEmpty" : "",
            "sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
            bFilter: true,
            "bStateSave": false,
            'sPaginationType': 'bootstrap',
            "fnInitComplete": function () {
                this.fnAdjustColumnSizing(true);
               },
               "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                return nRow;
               },
            "bSort": true,
            "bProcessing": true,
            "bServerSide": true,
            "sServerMethod": "POST",
            "iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
            "sAjaxSource": "${g.createLink(action: "datatablesList", controller: "historyKaryawan")}",
            "aoColumns": [
                {
                    "sTitle": "History",
                    "sName": "history",
                    "mDataProp": "history",
                    "aTargets": [3],
                    "mRender": function ( data, type, row ) {
                        return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="oHistoryService.edit(\'historyKaryawan\','+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
                    },
                    "bSearchable": true,
                    "bSortable": true,
                    "sWidth":"300px",
                    "bVisible": true
                },
                {
                    "sTitle": "Tanggal",
                    "sName": "tanggal",
                    "mDataProp": "tanggal",
                    "aTargets": [8],
                    "bSearchable": true,
                    "bSortable": true,
                    "sWidth":"200px",
                    "bVisible": true
                },
                {
                    "sTitle": "Nomor Dokumen",
                    "sName": "letterNumber",
                    "mDataProp": "letterNumber",
                    "aTargets": [6],
                    "bSearchable": true,
                    "bSortable": true,
                    "sWidth":"200px",
                    "bVisible": true
                }
            ],
            "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                        aoData.push(
                                {"name": 'idKaryawan', "value": "${karyawanInstance?.id}"},
                                {"name": 'dari', "value": "karyawan"}
                        );

                        $.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});

});
</g:javascript>



