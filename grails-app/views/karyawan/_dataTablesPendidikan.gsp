<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<div class="navbar box-header no-border">
    <ul class="nav pull-right">
        <li><a class="pull-right box-action _new" href="javascript:void(0);"
               style="display: block;">&nbsp;&nbsp;<i
                    class="icon-plus" onclick="<g:remoteFunction action="create"
                                                                 controller="pendidikanKaryawan"
                                                                 onLoading="jQuery('#spinner').fadeIn(1);"
                                                                 onSuccess="loadFormInstance('pendidikanKaryawan', data, textStatus);"
                                                                 onComplete="jQuery('#spinner').fadeOut();shrinkTableLayout('pendidikanKaryawan');"
                                                                 params="'onDataKaryawan=true&karyawanId=${karyawanInstance?.id}'"
                    />"></i>&nbsp;&nbsp;
        </a></li>
        <li><a class="pull-right box-action _delete" href="#"
               style="display: block;" onclick="oHistoryService.massDelete('pendidikanKaryawan', 'pendidikanKaryawan', reloadPendidikanKaryawanTable);">&nbsp;&nbsp;<i
                    class="icon-remove"></i>&nbsp;&nbsp;
        </a></li>
    </ul>
</div>

<table id="pendidikanKaryawan_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%"  style="width: 1225px !important;">

</table>

<g:javascript>
var pendidikanKaryawanTable;
var reloadPendidikanKaryawanTable;
$(function(){

	reloadPendidikanKaryawanTable = function() {
		pendidikanKaryawanTable.fnDraw();
	}



$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	pendidikanKaryawanTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	pendidikanKaryawanTable = $('#pendidikanKaryawan_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList", controller: "pendidikanKaryawan")}",
		"aoColumns": [

{
	"sName": "pendidikan",
	"mDataProp": "pendidikan",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="oHistoryService.edit(\'pendidikanKaryawan\','+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true,
	"sTitle": "Tingkat Pendidikan"
}



,

{
	"sName": "namaSekolah",
	"mDataProp": "namaSekolah",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true,
	"sTitle": "Nama Sekolah"
}

,

{
	"sName": "jurusan",
	"mDataProp": "jurusan",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true,
	"sTitle": "Jurusan"
}

,

{
	"sName": "nemIpk",
	"mDataProp": "nemIpk",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true,
	"sTitle": "NEM / IPK"
}


,

{
	"sName": "tahunLulus",
	"mDataProp": "tahunLulus",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true,
	"sTitle": "Tahun Lulus",
	"mRender": function ( data, type, row ) {
		return row['tahunMasuk'] + " - " + data;
	}
}

],
            "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                        aoData.push(
                                {"name": 'idKaryawan', "value": "${karyawanInstance?.id}"},
                                {"name": 'dari', "value": "karyawan"}
                        );

                        $.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>



