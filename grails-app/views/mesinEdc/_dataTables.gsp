
<%@ page import="com.kombos.administrasi.MesinEdc" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="mesinEdc_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="mesinEdc.m704Id.label" default="Kode Mesin EDC" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="mesinEdc.m704NamaMesinEdc.label" default="Nama Mesin EDC" /></div>
			</th>


		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m704Id" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m704Id" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m704NamaMesinEdc" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m704NamaMesinEdc" class="search_init" />
				</div>
			</th>
	


		</tr>
	</thead>
</table>

<g:javascript>
var MesinEdcTable;
var reloadMesinEdcTable;
$(function(){
	
	reloadMesinEdcTable = function() {
		MesinEdcTable.fnDraw();
	}

	var recordsMesinEdcperpage = [];//new Array();
    var anMesinEdcSelected;
    var jmlRecMesinEdcPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	MesinEdcTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	MesinEdcTable = $('#mesinEdc_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            //alert( $('li.active').text() );
            var rsMesinEdc = $("#mesinEdc_datatables tbody .row-select");
            var jmlMesinEdcCek = 0;
            var nRow;
            var idRec;
            rsMesinEdc.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsMesinEdcperpage[idRec]=="1"){
                    jmlMesinEdcCek = jmlMesinEdcCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsMesinEdcperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecMesinEdcPerPage = rsMesinEdc.length;
            if(jmlMesinEdcCek==jmlRecMesinEdcPerPage && jmlRecMesinEdcPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m704Id",
	"mDataProp": "m704Id",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m704NamaMesinEdc",
	"mDataProp": "m704NamaMesinEdc",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "staDel",
	"mDataProp": "staDel",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m704Id = $('#filter_m704Id input').val();
						if(m704Id){
							aoData.push(
									{"name": 'sCriteria_m704Id', "value": m704Id}
							);
						}
	
						var m704NamaMesinEdc = $('#filter_m704NamaMesinEdc input').val();
						if(m704NamaMesinEdc){
							aoData.push(
									{"name": 'sCriteria_m704NamaMesinEdc', "value": m704NamaMesinEdc}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	 $('.select-all').click(function(e) {

        $("#mesinEdc_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordsMesinEdcperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsMesinEdcperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#mesinEdc_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsMesinEdcperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anMesinEdcSelected = MesinEdcTable.$('tr.row_selected');
            if(jmlRecMesinEdcPerPage == anMesinEdcSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsMesinEdcperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
