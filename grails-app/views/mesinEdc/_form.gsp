<%@ page import="com.kombos.administrasi.MesinEdc" %>


<g:if test="${mesinEdcInstance?.m704Id}">
<div class="control-group fieldcontain ${hasErrors(bean: mesinEdcInstance, field: 'm704Id', 'error')} required">
	<label class="control-label" for="m704Id">
		<g:message code="mesinEdc.m704Id.label" default="Kode Mesin EDC" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	${mesinEdcInstance?.m704Id}
	</div>
</div></g:if>

<div class="control-group fieldcontain ${hasErrors(bean: mesinEdcInstance, field: 'm704NamaMesinEdc', 'error')} required">
	<label class="control-label" for="m704NamaMesinEdc">
		<g:message code="mesinEdc.m704NamaMesinEdc.label" default="Nama Mesin EDC" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m704NamaMesinEdc" maxlength="50" required="" value="${mesinEdcInstance?.m704NamaMesinEdc}"/>
	</div>
</div>

<g:javascript>
    document.getElementById("m704NamaMesinEdc").focus();
</g:javascript>
