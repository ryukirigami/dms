

<%@ page import="com.kombos.administrasi.MesinEdc" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'mesinEdc.label', default: 'Mesin EDC')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteMesinEdc;

$(function(){ 
	deleteMesinEdc=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/mesinEdc/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadMesinEdcTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-mesinEdc" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="mesinEdc"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${mesinEdcInstance?.m704Id}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m704Id-label" class="property-label"><g:message
					code="mesinEdc.m704Id.label" default="Kode Mesin EDC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m704Id-label">
						%{--<ba:editableValue
								bean="${mesinEdcInstance}" field="m704Id"
								url="${request.contextPath}/MesinEdc/updatefield" type="text"
								title="Enter m704Id" onsuccess="reloadMesinEdcTable();" />--}%
							
								<g:fieldValue bean="${mesinEdcInstance}" field="m704Id"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${mesinEdcInstance?.m704NamaMesinEdc}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m704NamaMesinEdc-label" class="property-label"><g:message
					code="mesinEdc.m704NamaMesinEdc.label" default="Nama Mesin EDC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m704NamaMesinEdc-label">
						%{--<ba:editableValue
								bean="${mesinEdcInstance}" field="m704NamaMesinEdc"
								url="${request.contextPath}/MesinEdc/updatefield" type="text"
								title="Enter m704NamaMesinEdc" onsuccess="reloadMesinEdcTable();" />--}%
							
								<g:fieldValue bean="${mesinEdcInstance}" field="m704NamaMesinEdc"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${mesinEdcInstance?.staDel}">
				<tr style="display:none">
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="mesinEdc.staDel.label" default="M704 Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${mesinEdcInstance}" field="staDel"
								url="${request.contextPath}/MesinEdc/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadMesinEdcTable();" />--}%
							
								<g:fieldValue bean="${mesinEdcInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			<g:if test="${mesinEdcInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="jenisStall.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${mesinEdcInstance}" field="dateCreated"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:formatDate date="${mesinEdcInstance?.dateCreated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${mesinEdcInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="jenisStall.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${mesinEdcInstance}" field="createdBy"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:fieldValue bean="${mesinEdcInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${mesinEdcInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="jenisStall.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${mesinEdcInstance}" field="lastUpdated"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:formatDate date="${mesinEdcInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${mesinEdcInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="jenisStall.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${mesinEdcInstance}" field="updatedBy"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:fieldValue bean="${mesinEdcInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${mesinEdcInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="jenisStall.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${mesinEdcInstance}" field="lastUpdProcess"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:fieldValue bean="${mesinEdcInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${mesinEdcInstance?.id}"
					update="[success:'mesinEdc-form',failure:'mesinEdc-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteMesinEdc('${mesinEdcInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
