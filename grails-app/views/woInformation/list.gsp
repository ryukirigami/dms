
<%@ page import="com.kombos.administrasi.Stall" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="woInformation.label" default="WO Information" /></title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>

            $(function(){

                var cekNOWO = "${noWO}";
                if(cekNOWO){
                    findData(cekNOWO);
                }
                //fungsi enter
                $("#key,#noPolAkhir,#noPolTengah,#noPolTengah,#noPolAwal").bind('keypress', function(e) {
                var code = (e.keyCode ? e.keyCode : e.which);
                    if(code == 13) {
                            findData();
                    }
                });
            });

			var show;
			var edit;
            var saveChange;
            var noWOPar = -1;
        function isiData(noWo){
            var id=noWo;
            $.ajax({
                url:'${request.contextPath}/woInformation/getTableBiayaData',
                type: "POST",
                data: { noWO : id },
                success : function(data){
                    if(data.length>0){
                        $.each(data,function(i,item){
                            biayaTable.fnAddData({
                                'namaJob': item.namaJobParts,
                                'hargaJob': item.hargaJob,
                                'hargaParts': item.hargaParts,
                                'total': item.total
                            });
                        });
                    }
                }
            });

            $.ajax({
                url:'${request.contextPath}/woInformation/getTableRateData',
                type: "POST",
                data: { noWO : id },
                success : function(data){
                    $.each(data,function(i,item){
                       rateTeknisiTable.fnAddData({
                            'namaJob': item.namaJob,
                            'teknisi': item.teknisi,
                            'rate': item.rate,
                            'namaSa': item.namaSa,
                            'namaForeman': item.namaForeman
                        });
                    });
                }
            });
        }

        function findData(noWO){
            reloadBiayaTable();
            reloadRateTeknisiTable();
            var kategori = $('#kategori').val();
            var key = $('#key').val();
            var noPolAwal = $('#noPolAwal').val();
            var noPolTengah = $('#noPolTengah').val();
            var noPolAkhir = $('#noPolAkhir').val();
            var isNull = false;
            if(noWO){
                kategori="Nomor WO";
                key=noWO;
            }else{
                if(kategori=="Nomor WO"){
                    if(key==""){
                        isNull=true;
                    }
                }else{
                    if(noPolAwal=="" && noPolTengah=="" && noPolAkhir==""){
                        isNull=true;
                    }
                }
            }
            if(isNull){
                alert("Data Belum Lengkap");
                return
            }else{
                $.ajax({
                    url:'${request.contextPath}/woInformation/findData',
                    type: "POST",
                    data : { kategori: kategori, key : key , noPolAwal : noPolAwal, noPolTengah : noPolTengah, noPolAkhir : noPolAkhir },
                    success : function(data){
                        $('#nomorWO').val('');
                        $('#nomorPolisi').val('')
                        $('#modelKendaraan').val('')
                        $('#vehicle').val('')
                        $('#tanggalWO').val('')
                        $('#waktuPenyerahan').val('')
                        $('#namaCustomer').val('')
                        $('#alamatCustomer').val('')
                        $('#telponCustomer').val('')
                        $('#keluhan').val('')
                        $('#permintaan').val('')
                        $('#stall').val('')
                        $('#si').val('')
                        $('#fir').val('')
                        $('#staKendaraan').val('')
                        if(data.length>0){
                            isiData(data[0].t401NoWo);
                            $('#nomorWO').val(data[0].t401NoWo);
                            $('#nomorPolisi').val(data[0].noPolisi)
                            $('#modelKendaraan').val(data[0].modelKendaraan)
                            $('#vehicle').val(data[0].vehicle)
                            $('#tanggalWO').val(data[0].tanggalWO)
                            $('#waktuPenyerahan').val(data[0].waktuPenyerahan)
                            $('#namaCustomer').val(data[0].namaCustomer)
                            $('#alamatCustomer').val(data[0].alamatCustomer)
                            $('#telponCustomer').val(data[0].teleponCustomer)
                            $('#keluhan').val(data[0].keluhan)
                            $('#permintaan').val(data[0].permintaan)
                            $('#stall').val(data[0].stall)
                            $('#si').val(data[0].si)
    //                        $('#fir').val(data[0].fir)
                            $('#staKendaraan').val(data[0].staKendaraan)

                        }else if(data.length<=0){//latihan by Deni
                                alert("Data tidak Ditemukan")
                            }
                            //
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        alert('Internal Server Error');
                    }
                });
            }
        };

        function clearForm(){
            reloadBiayaTable();
            reloadRateTeknisiTable();
            $('#kategori').val('');
            $('#key').val('');
            $('#nomorWO').val('');
            $('#nomorPolisi').val('')
            $('#modelKendaraan').val('')
            $('#vehicle').val('')
            $('#tanggalWO').val('')
            $('#waktuPenyerahan').val('')
            $('#namaCustomer').val('')
            $('#alamatCustomer').val('')
            $('#telponCustomer').val('')
            $('#keluhan').val('')
            $('#permintaan').val('')
            $('#stall').val('')
            $('#fir').val('')
            $('#si').val('')
            $('#staKendaraan').val('')
            $('#isiWO').show();
            $('#noPolAwal').val('');
            $('#noPolTengah').val('');
            $('#noPolAkhir').val('');
            $('#isiNopol').hide();
        };

        saveChange = function(){
                var noWO = $('#nomorWO').val();
                var stall = $('#stall').val();
                if(noWO==null || noWO==""){
                    alert('Nomor WO tidak Boleh Kosong')
                    return
                }
                if(stall==null || stall==""){
                    alert('Pilih Stall Parkir')
                    return
                }else{
                    $.ajax({
                    url:'${request.contextPath}/woInformation/save',
                    type: "POST", // Always use POST when deleting data
                    data: { noWO: noWO , stall : stall},
                    success : function(data){
                        toastr.success('<div>Data Saved</div>');
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        toastr.error('<div>Internal server error</div>');
                    }
                });
                }
        }

        function changeKategori(){
            if($('#kategori').val()=="Nomor WO"){
                $('#isiWO').show();
                $('#noPolAwal').val('');
                $('#noPolTengah').val('');
                $('#noPolAkhir').val('');
                $('#isiNopol').hide();
            }else{
                $('#isiWO').hide();
                $('#key').val('');
                $('#isiNopol').show();
            }
        }

        function isNumberKey(evt){
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

            return true;
        }

    </g:javascript>
</head>

<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="woInformation.label" default="WO Information" /></span>
    <ul class="nav pull-right">
    </ul>
</div>
<div class="box">
    <div class="span12" id="woInformation-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>

        <table style="width: 40%" >
            <tr>
                <td style="padding: 5px">
                    <g:message code="wo.category.search.label" default="Kategori Search" />
                </td>
                <td>
                    <g:select style="width:100%" name="kategori" id="kategori" onchange="changeKategori();" from="${['Nomor WO','Nomor Polisi']}" />
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    <g:message code="wo.key.label" default="Key" />
                </td>
                <td>
                    <div id="isiWO">
                        <g:textField style="width:96%; text-transform:uppercase" name="key" id="key" maxlength="20"  />
                    </div>
                    <div id="isiNopol" style="display: none">
                        <g:textField style="width:40px; text-transform:uppercase" name="noPolAwal" id="noPolAwal" maxlength="2"  />
                        <g:textField style="width:100px; " name="noPolTengah" id="noPolTengah" maxlength="5" onkeypress="return isNumberKey(event);" />
                        <g:textField style="width:40px; text-transform:uppercase" name="noPolAkhir" id="noPolAkhir" maxlength="3" />
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <a class="pull-right box-action" style="display: block;" >
                        &nbsp;&nbsp;<i>
                        <g:field type="button" style="color: white;" class="btn btn-primary create" onclick="findData();"
                                 name="view" id="view" value="${message(code: 'default.search.label', default: 'Search1')}" />
                        </i>&nbsp;&nbsp;
                    </a>
                    <a class="pull-right box-action" style="display: block;" >
                        &nbsp;&nbsp;<i>
                        <g:field type="button" style="width: 90px" class="btn cancel" onclick="clearForm();"
                                 name="clear" id="clear" value="${message(code: 'default.clear.search.label', default: 'Clear Search')}" />
                        </i>&nbsp;&nbsp;
                    </a>
                </td>
            </tr>
        </table>

        <fieldset>
            <legend>WO Detail</legend>
            <div class="row-fluid">
                <div id="kiri" class="span5">
                    <table style="width: 100%;">
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.t401NoWo.label" default="Nomor WO" />
                            </td>
                            <td>
                                <g:textField style="width:100%" name="nomorWO" id="nomorWO" readonly="" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.noPolisi.label" default="Nomor Polisi" />
                            </td>
                            <td>
                                <g:textField style="width:100%" name="nomorPolisi" readonly="" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.vehicle.label" default="Nomor Rangka" />
                            </td>
                            <td>
                                <g:textField style="width:100%" name="vehicle" readonly=""/>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.modelKendaraan.label" default="Model Kendaraan" />
                            </td>
                            <td>
                                <g:textField style="width:100%" name="modelKendaraan" readonly=""/>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.tanggalWO.label" default="Tanggal WO" />
                            </td>
                            <td>
                                <g:textField style="width:100%" name="tanggalWO" readonly="" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.tanggalPenyerahan.label" default="Tanggal Penyerahan" />
                            </td>
                            <td>
                                <g:textField style="width:100%" name="waktuPenyerahan" readonly="" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.namaCustomer.label" default="Nama Customer" />
                            </td>
                            <td>
                                <g:textField style="width:100%" name="namaCustomer" readonly="" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.alamatCustomer.label" default="Alamat Customer" />
                            </td>
                            <td>
                                <g:textArea style="width:100%;resize: none" name="alamatCustomer" readonly="" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.teleponCustomer.label" default="Telepon Customer" />
                            </td>
                            <td>
                                <g:textField  style="width:100%" name="telponCustomer" readonly="" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.keluhan.label" default="Keluhan" />
                            </td>
                            <td>
                                <g:textArea style="width:100%;resize: none" name="keluhan" readonly="" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.permintaan.label" default="Permintaan" />
                            </td>
                            <td>
                                <g:textArea style="width:100%;resize: none" name="permintaan" readonly="" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.fir.label" default="FIR (Fix It Right)" />
                            </td>
                            <td>
                                <g:textArea style="width:100%;resize: none" name="fir" readonly="" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.stallParkir.label" default="Stall Parkir" />
                            </td>
                            <td>
                                <g:textField style="width:100%" name="stall" readonly="" />

                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.Daftar.label" default="Daftar SI" />
                            </td>
                            <td>
                                <g:textArea style="width:100%;resize: none" name="si" readonly="" />

                            </td>
                        </tr>
                    </table>
                </div>

                <div id="kanan" class="span7">
                    <fieldset>
                        <legend style="font-size: 14px">
                            <g:message code="wo.biaya.label" default="Biaya" />
                        </legend>
                        <div style="width: 100%" >
                            <g:render template="dataTablesBiaya" />
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend style="font-size: 14px">
                            <g:message code="wo.rateTeknisi.label" default="Rate Teknisi" />
                        </legend>
                        <div style="width: 100%" >
                            <g:render template="dataTablesRateTeknisi" />
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend style="font-size: 14px">
                            <g:message code="wo.statusKendaraan.label" default="Status Kendaraan" />
                        </legend>
                        <g:textField style="width:98%" readonly="" name="staKendaraan" id="staKendaraan" />
                    </fieldset>
                </div>

            </div>
        </fieldset>
    </div>

    <a class="pull-right box-action" style="display: block;" >
        <i>
            <g:field type="button" style="width: 100%" class="btn cancel pull-right box-action" onclick="saveChange();"
                     name="save" id="save" value="${message(code: 'default.save.label', default: 'Save')}" />
        </i>
    </a>

</div>
</body>
</html>
