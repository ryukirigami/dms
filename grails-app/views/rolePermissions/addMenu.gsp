<%--
  Created by IntelliJ IDEA.
  User: Ahmad Fawaz
  Date: 25/03/15
  Time: 17:20
--%>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="addMenu.label" default="Master Pertanyaan FIR" /></title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="addMenu.label" default="Add Data" /></span>
    <ul class="nav pull-right">
    </ul>
</div>
<div class="box">
    <div class="span12" id="addMenu-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>

        <g:render template="dataTablesMenu" />
    </div>
    <div class="span7" id="addMenu-form" style="display: none;"></div>
</div>
</body>
</html>