
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="listMenu_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="700px">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div style="text-align: center"><g:message code="default.menu.label" default="Menu" /></div>
        </th>

    </tr>
    <tr>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_menu" style="padding-top: 0px;position:relative; margin-top: 0px;width: 100%;">
                <input type="text" name="search_menu" class="search_init" style="width: 98%"/>
            </div>
        </th>


    </tr>
    </thead>
</table>

<g:javascript>
var listMenuTable;
var reloadListMenuTable;
$(function(){
	
	reloadListMenuTable = function() {
		listMenuTable.fnDraw();
	}

    var recordslistMenuperpage = [];
    var anListMenuSelected;
    var jmlRecListMenuPerPage=0;
    var id;

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	listMenuTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	listMenuTable = $('#listMenu_datatables').dataTable({
		"sScrollX": "500px;",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsListMenu = $("#listMenu_datatables tbody .row-select");
            var jmlListMenuCek = 0;
            var nRow;
            var idRec;
            rsListMenu.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordslistMenuperpage[idRec]=="1"){
                    jmlListMenuCek = jmlListMenuCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordslistMenuperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecListMenuPerPage = rsListMenu.length;
            if(jmlListMenuCek==jmlRecListMenuPerPage && jmlRecListMenuPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "label",
	"mDataProp": "label",
	"aTargets": [0],
    "mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100%",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var menu = $('#filter_menu input').val();
						if(menu){
							aoData.push(
									{"name": 'sCriteria_menu', "value": menu}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
    $('.select-all').click(function(e) {
        $("#listMenu_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordslistMenuperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordslistMenuperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

    $('#listMenu_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordslistMenuperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anListMenuSelected = listMenuTable.$('tr.row_selected');
            if(jmlRecListMenuPerPage == anListMenuSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordslistMenuperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>