<%@ page import="com.kombos.baseapp.sec.shiro.Role; com.kombos.administrasi.CompanyDealer; com.kombos.baseapp.sec.shiro.User" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <%--		<r:require modules="baseapp" />--%>
    <g:set var="entityName" value="${message(code: 'app.userAccess.label', default: 'User Access')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="export"></r:require>
    <g:javascript>
        function showModalMenu(){
            var roleChosen = $('#roles').val()
            if(roleChosen=="-1"){
                alert("Pilih role terlebih dahulu");
                return
            }
            $("#addMenuContent").empty();
			$('#spinner').fadeIn(1);
            $.ajax({type:'POST',
//                data: { id : $('#rceptionId').val()},
                url:'${request.contextPath}/rolePermissions/addMenu',
                success:function(data,textStatus){
                        $("#addMenuContent").html(data);
                        $("#addMenuModal").modal({
                            "backdrop" : "dynamic",
                            "keyboard" : true,
                            "show" : true
                        }).css({'width': '1100px','margin-left': function () {return -($(this).width() / 2);}});
                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
        }

        function doSaveMenu(){
            var role = $("#roles").val();
            if(role==""){
                alert("Pilih Role terlebih dahulu");
                return
            }
            var recordsToSave = [];
            $("#addMenu-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    recordsToSave.push(id);
                }
            });

            var json = JSON.stringify(recordsToSave);

            $.ajax({
                url:'${request.contextPath}/rolePermissions/save',
                type: "POST", // Always use POST when deleting data
                data: { ids: json, role : role },
                complete: function(xhr, status) {
                    alert("Data berhasil ditambahkan");
                    hideModalMenu();
                    reloadRolePermissionTable();
                }
            });
        }

        function hideModalMenu(){
            $('#listMenuModal').hide();
        }
    </g:javascript>
</head>
<body>
<h3 class="navbar box-header no-border">
    <g:message code="default.list.label" args="[entityName]" />
    <ul class="nav pull-right">
        <li class="separator"></li>
        <li><a class="pull-right box-action" href="javascript:void(0);"
               style="display: block;" onclick="showModalMenu();">&nbsp;&nbsp;<i
                    class="icon-plus"></i>&nbsp;&nbsp;
        </a></li>
        <li class="separator"></li>
    </ul>
</h3>

<ul class="nav nav-tabs">
    <li><a href="javascript:loadPath('user/');">User Profile</a></li>
    <li><a href="javascript:loadPath('role/');">User Role</a></li>
    <li class="active"><a href="#">User Access</a></li>
    <li><a href="javascript:loadPath('userPrinter/');">User Printer</a></li>
    <li><a href="javascript:loadPath('namaForm/');">User Form Name</a></li>
</ul>

<div class="box">
    <div class="span12" id="user-table">

        <table>
            <tr>
                <td style="padding: 5px">
                    <g:message code="role.name.label" default="Role" />
                </td>
                <td style="padding: 5px">
                    <g:select name="role.id" id="roles" from="${com.kombos.baseapp.sec.shiro.Role.createCriteria().list {eq("staDel","0");order("id")}}" noSelection="['-1':'Pilih Role']" optionValue="authority" optionKey="id" onchange="javascript:loadPermission();"/>
                </td>
            </tr>
        </table>

        <table id="role" class="table table-bordered table-hover">
            <tbody>
            <tr>
                <td colspan="2">
                    <span id="permissions-label" class="property-label">
                        <g:message code="role.permissions.label" default="Permissions" />:
                    </span>
                    <g:render template="detailList"/>
                </td>
            </tr>
            </tbody>
        </table>

    </div>
</div>
<div id="addMenuModal" class="modal fade">
    <div class="modal-dialog" style="width: 1100px;">
        <div class="modal-content" style="width: 1100px;">
            <!-- dialog body -->
            <div class="modal-body" style="max-height: 500px;">
                <div id="addMenuContent"/>
                <div class="iu-content"></div>

            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="javascript:void(0);" id="btnSave" onclick="doSaveMenu();" class="btn btn-primary">
            Pilih
        </a>
        <a href="javascript:void(0);" data-dismiss="modal" id="closeModalBillto" class="btn cancel">
            Close
        </a>
    </div>
</div>
</body>
</html>
