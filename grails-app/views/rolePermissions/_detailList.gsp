<html>
<head>
</head>
<body>
<table id="rolePermissionDatatables_datatable" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover" width="100%">
    <thead>
    <tr>
        <th>
            <g:message code="app.role.id.label" default="Id"/>
        </th>
        <th>
            <g:message code="app.rolePermission.menu.label" default="Menu / Form / Fungsi"/>
            <div id="filter_label" style="margin-top: 38px;">
                <input type="text" name="search_label" class="search_init" />
            </div>
        </th>
        <th>
            <g:message code="app.role.permission_string.label" default="Permission String"/>
        </th>
        <th>
            <g:message code="app.role.link_controller.label" default="Link Controller"/>
        </th>
        <th>
            <g:message code="app.role.allow_module.label" default="Baca"/>
        </th>
        <th>
            <g:message code="app.role.allow_add.label" default="Tambah"/>
        </th>
        <th>
            <g:message code="app.role.allow_edit.label" default="Ubah"/>
        </th>
        <th>
            <g:message code="app.role.allow_delete.label" default="Hapus"/>
        </th>
        <th></th>
        <th>
            <g:message code="app.role.edit.label" default="Edit"/>
        </th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td colspan="4" class="dataTables_empty">Loading
        data from server</td>
    </tr>
    </tbody>
</table>
</body>
</html>
<script type="text/javascript">
    var shrinkRolePermissionTable;
    var reloadRolePermissionTable;
    var loadPermission;
    var roleId = 0;

    jQuery(function () {

        var oTable = $('#rolePermissionDatatables_datatable').dataTable({

            bFilter: true,
            "bStateSave": false,
            "fnInitComplete": function () {


            },
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            },
            "bSort": true,
            "bProcessing": true,
            "bServerSide": true,
            "bPaginate" : false,
            "iDisplayLength" : 1000, //maxLineDisplay is set in menu/maxLineDisplay.gsp
            "sAjaxSource": "${request.contextPath}/rolePermissions/getList?role_id="+roleId,
            "fnServerParams": function ( aoData ) {
                $('table .dropdown-menu').each(function(idx){
                    aoData.push( {"name": "sFilter_"+(idx+1), "value": $(this).find('li.active').attr('name')} );
                });
            },
            "aoColumnDefs": [
                {
                    "sName": "label",
                    "aTargets": [1],
                    "mRender": function ( data, type, row ) {
                        return '<input id="id'+row[9]+'" type="hidden" value="'+row[0]+'"/>' +
                                '<input id="permissionString'+row[9]+'" type="hidden" value="'+row[2]+'"/>' +
                                '<input id="linkController'+row[9]+'" type="hidden" value="'+row[3]+'"/>'+
								'<input id="menuCode'+row[9]+'" type="hidden" value="'+row[8]+'"/>'+
                                '<label class="label'+row[9]+'">'+data+'</label>';
                    },
                    "bSearchable": true,
                    "sWidth": "",
                    "bSortable": true,
                    "sWidth":'100%',
                    "sClass":'',
                    "bVisible": true
                }  ,
                {
                    "sName": "id",
                    "aTargets": [0],
                    "bSearchable": true,
                    "mRender": function ( data, type, row ) {
                        return '<label>'+data+'</label>';
                    },
                    "sWidth": "",
                    "bSortable": true,
                    "sWidth":'100%',
                    "sClass":'',
                    "bVisible": false
                }  ,
                {
                    "sName": "permissionString",
                    "aTargets": [2],
                    "mRender": function ( data, type, row ) {
                        return '<label>'+data+'</label>';
                    },
                    "bSearchable": true,
                    "sWidth": "",
                    "bSortable": true,
                    "sWidth":'100%',
                    "sClass":'',
                    "bVisible": false
                }  ,
                {
                    "sName": "linkController",
                    "aTargets": [3],
                    "mRender": function ( data, type, row ) {
                        return '<label>'+data+'</label>';
                    },
                    "bSearchable": true,
                    "sWidth": "",
                    "bSortable": true,
                    "sWidth":'100%',
                    "sClass":'',
                    "bVisible": false
                }  ,
                {
                    "sName": "allowModule",
                    "aTargets": [4],
                    "mRender": function ( data, type, row ) {
                        return '<label>'+data+'</label><input id="allowModule'+row[9]+'" type="hidden" value="'+data+'"/>';
                    },
                    "bSearchable": false,
                    "sWidth": "",
                    "bSortable": false,
                    "sWidth":'100%',
                    "sClass":'',
                    "bVisible": true
                }  ,
                {
                    "sName": "allowAdd",
                    "aTargets": [5],
                    "mRender": function ( data, type, row ) {
                        return '<label>'+data+'</label><input id="allowAdd'+row[9]+'" type="hidden" value="'+data+'"/>';
                    },
                    "bSearchable": false,
                    "sWidth": "",
                    "bSortable": false,
                    "sWidth":'100%',
                    "sClass":'',
                    "bVisible": true
                }  ,
                {
                    "sName": "allowEdit",
                    "aTargets": [6],
                    "mRender": function ( data, type, row ) {
                        return '<label>'+data+'</label><input id="allowEdit'+row[9]+'" type="hidden" value="'+data+'"/>';
                    },
                    "bSearchable": false,
                    "sWidth": "",
                    "bSortable": false,
                    "sWidth":'100%',
                    "sClass":'',
                    "bVisible": true
                }  ,
                {
                    "sName": "allowDelete",
                    "aTargets": [7],
                    "mRender": function ( data, type, row ) {
                        return '<label>'+data+'</label><input id="allowDelete'+row[9]+'" type="hidden" value="'+data+'"/>';
                    },
                    "bSearchable": false,
                    "sWidth": "",
                    "bSortable": false,
                    "sWidth":'100%',
                    "sClass":'',
                    "bVisible": true
                },
                {
                    "sName": "menuCode",
                    "aTargets": [8],
                    "bSearchable": true,
					"mRender": function ( data, type, row ) {
                        return '<label>'+data+'</label>';
                    },
                    "sWidth": "",
                    "bSortable": true,
                    "sWidth":'100%',
                    "sClass":'',
                    "bVisible": false
                }  
                ,
                {
                    "sName": "edit",
                    "aTargets": [9],
                    "mRender": function ( data, type, row ) {
                        return '<a class="edit" href="javascript:editRow('+data+')">Edit</a>';
                    },
                    "bSearchable": false,
                    "sWidth": "",
                    "bSortable": false,
                    "sWidth":'100%',
                    "sClass":'',
                    "bVisible": true
                }
            ],
            "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                var label = $('#filter_label input').val();
                if(label){
                    aoData.push(
                            {"name": 'label', "value": label}
                    );
                }


                $.ajax({ "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData ,
                    "success": function (json) {
                        fnCallback(json);
                    },
                    "complete": function () {
                    }
                });
            }
        });

        $("#roleDatatables-filters tbody")
                .append("<tr><td align=\"center\">role.id.label&nbsp;&nbsp;</td><td align=\"center\" id=\"filter_id\"> <input type=\"text\" name=\"search_id\" class=\"search_init\" size=\"10\"/></td></tr>")
                .append("<tr><td align=\"center\">Name&nbsp;&nbsp;</td><td align=\"center\" id=\"filter_name\"> <input type=\"text\" name=\"search_name\" class=\"search_init\" size=\"10\"/></td></tr>")
                .append("<tr><td align=\"center\">Label&nbsp;&nbsp;</td><td align=\"center\" id=\"filter_permissions\"> <input type=\"text\" name=\"search_permissions\" class=\"search_init\" size=\"10\"/></td></tr>")
                .append("<tr><td align=\"center\">Users&nbsp;&nbsp;</td><td align=\"center\" id=\"filter_users\"> <input type=\"text\" name=\"search_users\" class=\"search_init\" size=\"10\"/></td></tr>")
        ;

        $('.search_init').each(function(idx){
            $(this).keypress(function(e){
                /* Filter on the column (the index) of this element */
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13) { //Enter keycode
                    oTable.fnFilter(this.value, idx+1);
                    e.stopPropagation();
                }
            });
        });

        $("#filter_id input").keyup(function () {
            /* Filter on the column (the index) of this element */
            oTable.fnFilter(this.value, 0);

        });

        $("#filter_name input").keyup(function () {
            /* Filter on the column (the index) of this element */
            oTable.fnFilter(this.value, 1);

        });

        $("#filter_permissions input").keyup(function () {
            /* Filter on the column (the index) of this element */
            oTable.fnFilter(this.value, 2);

        });

        $("#filter_users input").keyup(function () {
            /* Filter on the column (the index) of this element */
            oTable.fnFilter(this.value, 3);

        });

        $("#filter_label input").click(function(e)
        {
            e.stopPropagation();
        });

        $("#filter_label input").keyup(function(e)
        {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) { //Enter keycode
//                oTable.fnFilter(this.value, idx+1);
                oTable.fnDraw();
            }
            e.stopPropagation();
        });

        $("rolePermissionDatatables_datatable").dataTable().columnFilter();

        shrinkRolePermissionTable = function(){
            var oTable = $('#rolePermissionDatatables_datatable').dataTable();

        }

//        expandRolePermissionTable = function(){
//            var oTable = $('#rolePermissionDatatables_datatable').dataTable();
//
//        }

        reloadRolePermissionTable = function(){
            var oTable = $('#rolePermissionDatatables_datatable').dataTable();
            var newURL = "${request.contextPath}/rolePermissions/getList?role_id="+roleId;
            oTable.fnReloadAjax(newURL);
        }

        $('#rolePermissionDatatables_datatable th .select-all').click(function(e) {
            var tc = $('#rolePermissionDatatables_datatable tbody .row-select').attr('checked', this.checked);

            if(this.checked)
                tc.parent().parent().addClass('row_selected');
            else
                tc.parent().parent().removeClass('row_selected');
            e.stopPropagation();
        });

        $('#rolePermissionDatatables_datatable tbody tr .row-select').live('click', function (e) {
            if(this.checked)
                $(this).parent().parent().addClass('row_selected');
            else
                $(this).parent().parent().removeClass('row_selected');
            e.stopPropagation();

        } );

        $('#rolePermissionDatatables_datatable tbody tr a').live('click', function (e) {
            e.stopPropagation();
        } );

        $('#rolePermissionDatatables_datatable tbody td').live('click', function(e) {
            if (e.target.tagName.toUpperCase() != "INPUT") {
                var $tc = $(this).parent().find('input:checkbox'),
                        tv = $tc.attr('checked');
                $tc.attr('checked', !tv);
                $(this).parent().toggleClass('row_selected');
            }
        });

        $("#companyDealer").val($("#target option:first").val());
//        loadRoles(1);
    });

    var trHtml
    function editRow (nRow)
    {
        var idTr = '#rolePermissionDatatables_datatable tr:eq('+nRow+')';
        var trElm = $(idTr);
        trHtml = trElm.html(); //in case need of cancel button
        var id = jQuery('#id'+nRow).val();
        var labelString = jQuery('.label'+nRow).html();
        var permissionString = jQuery('#permissionString'+nRow).val();
        var linkController = jQuery('#linkController'+nRow).val();
		var menuCode = jQuery('#menuCode'+nRow).text();
		
		var oldAllowModule = jQuery('#allowModule'+nRow).val();
        var oldAllowAdd = jQuery('#allowAdd'+nRow).val();
        var oldAllowEdit = jQuery('#allowEdit'+nRow).val();
        var oldAllowDelete = jQuery('#allowDelete'+nRow).val();
		
        var allowModule = oldAllowModule=="Yes"?'checked="true"':"";
        var allowAdd = oldAllowAdd=="Yes"?'checked="true"':"";
        var allowEdit = oldAllowEdit=="Yes"?'checked="true"':"";
        var allowDelete = oldAllowDelete=="Yes"?'checked="true"':"";

        var htmlEdit =
                '<td class="">' +
                        '<input id="id'+nRow+'" value="'+id+'" type="hidden">' +
                        '<input id="permissionString'+nRow+'" value="'+permissionString+'" type="hidden">' +
                        '<input id="linkController'+nRow+'" value="'+linkController+'" type="hidden">' +
						'<input id="menuCode'+nRow+'" value="'+menuCode+'" type="hidden">' +						
						'<input id="oldAllowModule'+nRow+'" value="'+oldAllowModule+'" type="hidden">' +
						'<input id="oldAllowAdd'+nRow+'" value="'+oldAllowAdd+'" type="hidden">' +
						'<input id="oldAllowEdit'+nRow+'" value="'+oldAllowEdit+'" type="hidden">' +
						'<input id="oldAllowDelete'+nRow+'" value="'+oldAllowDelete+'" type="hidden">' +                        
						'<label class="label'+nRow+'">'+labelString+'</label>' +
                        '</td>' +
                        '<td class=""><input id="allowModule_Edit'+nRow+'" type="checkbox" '+allowModule+'/></td>' +
                        '<td class=""><input id="allowAdd'+nRow+'" type="checkbox" '+allowAdd+'/></td>' +
                        '<td class=""><input id="allowEdit'+nRow+'" type="checkbox" '+allowEdit+'/></td>' +
                        '<td class=""><input id="allowDelete'+nRow+'" type="checkbox" '+allowDelete+'/></td>' +
                        '<td class=""><a class="edit" href="javascript:saveEditRow('+nRow+')">Save</a>&nbsp;/&nbsp;<a class="edit" href="javascript:cancelEditRow('+nRow+')">Cancel</a></td>';

        trElm.html(htmlEdit);
    }

    function cancelEditRow(nRow){
//        var idTr = '#rolePermissionDatatables_datatable tr:eq('+nRow+')';
//        var trElm = $(idTr);
//        trElm.html(trHtml);
        reloadRolePermissionTable();
    }

    function saveEditRow(nRow){
        //do ajax
        var id = jQuery('#id'+nRow).val();
        var permissionString = jQuery('#permissionString'+nRow).val();
        var linkController = jQuery('#linkController'+nRow).val();
		
		var oldAllowModule = jQuery('#oldAllowModule'+nRow).val();
        var oldAllowAdd = jQuery('#oldAllowAdd'+nRow).val();
        var oldAllowEdit = jQuery('#oldAllowEdit'+nRow).val();
        var oldAllowDelete = jQuery('#oldAllowDelete'+nRow).val();
		
        var allowModule = document.getElementById('allowModule_Edit'+nRow).checked;
        var allowAdd = document.getElementById('allowAdd'+nRow).checked;
        var allowEdit = document.getElementById('allowEdit'+nRow).checked;
        var allowDelete = document.getElementById('allowDelete'+nRow).checked;
		var menuCode = jQuery('#menuCode'+nRow).val();

        jQuery.ajax({
            type: 'POST',
            url: '${request.getContextPath()}/rolePermissions/edit',
            data: {
                role_id : id,
				menu_code : menuCode,
                link_controller : linkController,
                permissions_string : permissionString,
				old_allow_module : oldAllowModule,
                old_allow_add : oldAllowAdd,
                old_allow_edit : oldAllowEdit,
                old_allow_delete : oldAllowDelete,
                allow_module : allowModule?"Yes":"No",
                allow_add : allowAdd?"Yes":"No",
                allow_edit : allowEdit?"Yes":"No",
                allow_delete : allowDelete?"Yes":"No"
            },
            async: false,
            success: function(data) {
                if(data.message)
                	alert(data.message);
                else
                    alert("Your request will be submitted to request for approval");
            },
            error: function(){
                alert('Server Response Error');
            }
        });

        reloadRolePermissionTable();
    }

    %{--function loadRoles(first)--}%
    %{--{--}%
        %{--var root="${resource()}";--}%
        %{--var companyDealerId=document.getElementById('companyDealer').value;--}%

        %{--var url1 = root+'/user/findRoleByCompanyDealer?companyDealer.id='+companyDealerId;--}%

        %{--if(first){--}%
            %{--jQuery('#roles').load(url1,function(){--}%
                %{--$("#roles").val($("#target option:first").val());--}%
                %{--loadPermission();--}%
            %{--});--}%
        %{--}else{--}%
            %{--jQuery('#roles').load(url1);--}%
        %{--}--}%
    %{--}--}%

    function loadPermission(){
        roleId = document.getElementById('roles').value;
        reloadRolePermissionTable();
    }

//    $(function() {
//        $("#companyDealer").val($("#target option:first").val());
//        loadRoles();
//        $("#roles").val($("#target option:first").val());
//        loadPermission();
//        reloadRolePermissionTable();
        %{--var oTable = $('#rolePermissionDatatables_datatable').dataTable();--}%
        %{--var newURL = "${request.contextPath}/rolePermissions/getList?role_id="+roleId;--}%
        %{--oTable.fnReloadAjax(newURL);--}%
//    });
</script>


