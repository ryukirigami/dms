<%@ page import="com.kombos.administrasi.PenegasanPengiriman" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'penegasanPengiriman.label', default: 'PenegasanPengiriman')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="edit-penegasanPengiriman" class="content scaffold-edit" role="main">
			<legend><g:message code="default.edit.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${penegasanPengirimanInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${penegasanPengirimanInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form method="post" >--}%
			<g:formRemote class="form-horizontal" name="edit" on404="alert('not found!');" onSuccess="reloadPenegasanPengirimanTable();" update="penegasanPengiriman-form"
				url="[controller: 'penegasanPengiriman', action:'update']">
				<g:hiddenField name="id" value="${penegasanPengirimanInstance?.id}" />
				<g:hiddenField name="version" value="${penegasanPengirimanInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons controls">
					<a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout('penegasanPengiriman');"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
					<g:actionSubmit class="btn btn-primary save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
					<g:actionSubmit class="btn delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" formnovalidate="" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:formRemote>
			%{--</g:form>--}%
		</div>
	</body>
</html>
