<%@ page import="com.kombos.administrasi.PenegasanPengiriman" %>



<div class="control-group fieldcontain ${hasErrors(bean: penegasanPengirimanInstance, field: 'jalur', 'error')} ">
	<label class="control-label" for="jalur">
		<g:message code="penegasanPengiriman.jalur.label" default="Jalur" />
		
	</label>
	<div class="controls">
	<g:textField name="jalur" value="${penegasanPengirimanInstance?.jalur}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: penegasanPengirimanInstance, field: 'keterangan', 'error')} ">
	<label class="control-label" for="keterangan">
		<g:message code="penegasanPengiriman.keterangan.label" default="Keterangan" />
		
	</label>
	<div class="controls">
	<g:textField name="keterangan" value="${penegasanPengirimanInstance?.keterangan}" />
	</div>
</div>

