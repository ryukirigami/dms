

<%@ page import="com.kombos.administrasi.PenegasanPengiriman" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'penegasanPengiriman.label', default: 'PenegasanPengiriman')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deletePenegasanPengiriman;

$(function(){ 
	deletePenegasanPengiriman=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/penegasanPengiriman/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadPenegasanPengirimanTable();
   				expandTableLayout('penegasanPengiriman');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-penegasanPengiriman" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="penegasanPengiriman"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${penegasanPengirimanInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="penegasanPengiriman.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${penegasanPengirimanInstance}" field="createdBy"
								url="${request.contextPath}/PenegasanPengiriman/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadPenegasanPengirimanTable();" />--}%
							
								<g:fieldValue bean="${penegasanPengirimanInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${penegasanPengirimanInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="penegasanPengiriman.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${penegasanPengirimanInstance}" field="dateCreated"
								url="${request.contextPath}/PenegasanPengiriman/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadPenegasanPengirimanTable();" />--}%
							
								<g:formatDate date="${penegasanPengirimanInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${penegasanPengirimanInstance?.jalur}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="jalur-label" class="property-label"><g:message
					code="penegasanPengiriman.jalur.label" default="Jalur" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="jalur-label">
						%{--<ba:editableValue
								bean="${penegasanPengirimanInstance}" field="jalur"
								url="${request.contextPath}/PenegasanPengiriman/updatefield" type="text"
								title="Enter jalur" onsuccess="reloadPenegasanPengirimanTable();" />--}%
							
								<g:fieldValue bean="${penegasanPengirimanInstance}" field="jalur"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${penegasanPengirimanInstance?.keterangan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="keterangan-label" class="property-label"><g:message
					code="penegasanPengiriman.keterangan.label" default="Keterangan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="keterangan-label">
						%{--<ba:editableValue
								bean="${penegasanPengirimanInstance}" field="keterangan"
								url="${request.contextPath}/PenegasanPengiriman/updatefield" type="text"
								title="Enter keterangan" onsuccess="reloadPenegasanPengirimanTable();" />--}%
							
								<g:fieldValue bean="${penegasanPengirimanInstance}" field="keterangan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${penegasanPengirimanInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="penegasanPengiriman.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${penegasanPengirimanInstance}" field="lastUpdProcess"
								url="${request.contextPath}/PenegasanPengiriman/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadPenegasanPengirimanTable();" />--}%
							
								<g:fieldValue bean="${penegasanPengirimanInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${penegasanPengirimanInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="penegasanPengiriman.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${penegasanPengirimanInstance}" field="lastUpdated"
								url="${request.contextPath}/PenegasanPengiriman/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadPenegasanPengirimanTable();" />--}%
							
								<g:formatDate date="${penegasanPengirimanInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${penegasanPengirimanInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="penegasanPengiriman.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${penegasanPengirimanInstance}" field="updatedBy"
								url="${request.contextPath}/PenegasanPengiriman/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadPenegasanPengirimanTable();" />--}%
							
								<g:fieldValue bean="${penegasanPengirimanInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('penegasanPengiriman');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${penegasanPengirimanInstance?.id}"
					update="[success:'penegasanPengiriman-form',failure:'penegasanPengiriman-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deletePenegasanPengiriman('${penegasanPengirimanInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
