

<%@ page import="com.kombos.finance.DocumentCategory" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'documentCategory.label', default: 'DocumentCategory')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteDocumentCategory;

$(function(){ 
	deleteDocumentCategory=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/documentCategory/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadDocumentCategoryTable();
   				expandTableLayout('documentCategory');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-documentCategory" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="documentCategory"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${documentCategoryInstance?.documentCategory}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="documentCategory-label" class="property-label"><g:message
					code="documentCategory.documentCategory.label" default="Document Category" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="documentCategory-label">
						%{--<ba:editableValue
								bean="${documentCategoryInstance}" field="documentCategory"
								url="${request.contextPath}/DocumentCategory/updatefield" type="text"
								title="Enter documentCategory" onsuccess="reloadDocumentCategoryTable();" />--}%
							
								<g:fieldValue bean="${documentCategoryInstance}" field="documentCategory"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${documentCategoryInstance?.description}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="description-label" class="property-label"><g:message
					code="documentCategory.description.label" default="Description" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="description-label">
						%{--<ba:editableValue
								bean="${documentCategoryInstance}" field="description"
								url="${request.contextPath}/DocumentCategory/updatefield" type="text"
								title="Enter description" onsuccess="reloadDocumentCategoryTable();" />--}%
							
								<g:fieldValue bean="${documentCategoryInstance}" field="description"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${documentCategoryInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="documentCategory.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${documentCategoryInstance}" field="staDel"
								url="${request.contextPath}/DocumentCategory/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadDocumentCategoryTable();" />--}%
							
								<g:fieldValue bean="${documentCategoryInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${documentCategoryInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="documentCategory.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${documentCategoryInstance}" field="createdBy"
								url="${request.contextPath}/DocumentCategory/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadDocumentCategoryTable();" />--}%
							
								<g:fieldValue bean="${documentCategoryInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${documentCategoryInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="documentCategory.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${documentCategoryInstance}" field="updatedBy"
								url="${request.contextPath}/DocumentCategory/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadDocumentCategoryTable();" />--}%
							
								<g:fieldValue bean="${documentCategoryInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${documentCategoryInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="documentCategory.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${documentCategoryInstance}" field="lastUpdProcess"
								url="${request.contextPath}/DocumentCategory/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadDocumentCategoryTable();" />--}%
							
								<g:fieldValue bean="${documentCategoryInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${documentCategoryInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="documentCategory.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${documentCategoryInstance}" field="dateCreated"
								url="${request.contextPath}/DocumentCategory/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadDocumentCategoryTable();" />--}%
							
								<g:formatDate date="${documentCategoryInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${documentCategoryInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="documentCategory.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${documentCategoryInstance}" field="lastUpdated"
								url="${request.contextPath}/DocumentCategory/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadDocumentCategoryTable();" />--}%
							
								<g:formatDate date="${documentCategoryInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${documentCategoryInstance?.transaction}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="transaction-label" class="property-label"><g:message
					code="documentCategory.transaction.label" default="Transaction" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="transaction-label">
						%{--<ba:editableValue
								bean="${documentCategoryInstance}" field="transaction"
								url="${request.contextPath}/DocumentCategory/updatefield" type="text"
								title="Enter transaction" onsuccess="reloadDocumentCategoryTable();" />--}%
							
								<g:each in="${documentCategoryInstance.transaction}" var="t">
								<g:link controller="transaction" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link>
								</g:each>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('documentCategory');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${documentCategoryInstance?.id}"
					update="[success:'documentCategory-form',failure:'documentCategory-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteDocumentCategory('${documentCategoryInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
