<%@ page import="com.kombos.finance.DocumentCategory" %>



<div class="control-group fieldcontain ${hasErrors(bean: documentCategoryInstance, field: 'documentCategory', 'error')} required">
	<label class="control-label" for="documentCategory">
		<g:message code="documentCategory.documentCategory.label" default="Document Category" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="documentCategory" required="" value="${documentCategoryInstance?.documentCategory}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: documentCategoryInstance, field: 'description', 'error')} required">
	<label class="control-label" for="description">
		<g:message code="documentCategory.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:textField name="description" required="" value="${documentCategoryInstance?.description}"/>--}%
        <g:textArea name="description" required="" value="${documentCategoryInstance?.description}"/>
	</div>
</div>

%{--
<div class="control-group fieldcontain ${hasErrors(bean: documentCategoryInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="documentCategory.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${documentCategoryInstance?.lastUpdProcess}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: documentCategoryInstance, field: 'transaction', 'error')} ">
	<label class="control-label" for="transaction">
		<g:message code="documentCategory.transaction.label" default="Transaction" />
		
	</label>
	<div class="controls">
	
<ul class="one-to-many">
<g:each in="${documentCategoryInstance?.transaction?}" var="t">
    <li><g:link controller="transaction" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="transaction" action="create" params="['documentCategory.id': documentCategoryInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'transaction.label', default: 'Transaction')])}</g:link>
</li>
</ul>

	</div>
</div>

--}%
