<%@ page import="com.kombos.administrasi.CompanyDealer"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="baseapplayout" />
<g:javascript disappointmentGrition="head">
	$(function(){		
		$('#detail').attr('disabled',true);
        $('#filter_periode2').hide();
        $('#filter_periode').show();
        $('#bulan1').change(function(){
        $('#bulan2').val($('#bulan1').val());
        });
        $('#bulan2').change(function(){
            if(parseInt($('#bulan1').val()) > parseInt($('#bulan2').val())){
                $('#bulan2').val($('#bulan1').val());
            }
        });
        $('#namaReport').change(function(){
             if($('#namaReport').val()=="01" || $('#namaReport').val()=="06" || $('#namaReport').val()=="11" || $('#namaReport').val()=="14" || $('#namaReport').val()=="15"){
                $('#detail').prop('disabled',true);
                $('#detail').attr('checked',false);
                $('#filter_periode').hide();
                $('#filter_periode2').show();
             }else if($('#namaReport').val()=="10"){
                $('#detail').prop('disabled',false);
                $('#detail').attr('checked',false);
                $('#filter_periode').show();
                $('#filter_periode2').hide();
             }else if($('#namaReport').val()=="12" || $('#namaReport').val()=="13"){
                      $('#filter_periode2').show();
                      $('#filter_periode').hide();
                      $('#detail').prop('disabled',false);
                      $('#detail').attr('checked',false);
             }else{
                     $('#detail').prop('disabled',false);
                     $('#detail').attr('checked',false);
                     $('#filter_periode').show();
                     $('#filter_periode2').hide();
             }
//                 $('#detail').prop('disabled',true);
//                 $('#detail').attr('checked',false);//$('#detail').show();
             });

	    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    });
    var checkin = $('#search_tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
                checkin.hide();
                $('#search_tanggal2')[0].focus();
            }).data('datepicker');

    var checkout = $('#search_tanggal2').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
                checkout.hide();
    }).data('datepicker');

    previewData = function(){
        var namaReport = $('#namaReport').val();
            var workshop = $('#workshop').val();
            var search_tanggal = $('#search_tanggal').val();
            var search_tanggal2 = $('#search_tanggal2').val();
            var bulan1 = $('#bulan1').val();
            var bulan2= $('#bulan2').val();
            var tahun = $('#tahun').val();
            var detail = "";
            if(document.getElementById("detail").checked){detail = "1"}else{detail = "0"}
            var format = $('input[name="formatReport"]:checked').val();
            if(namaReport == "" || namaReport == null){
                alert('Harap Isi Data Dengan Lengkap');
                return;
            }
           window.location = "${request.contextPath}/kr_mrsa_kpi/previewData?namaReport="+namaReport+"&workshop="+workshop+"&bulan1="+bulan1+"&bulan2="+bulan2+"&tahun="+tahun+"&tanggal="+search_tanggal+"&tanggal2="+search_tanggal2+"&detail="+detail+"&format="+format;
	}


</g:javascript>

</head>
<body>
	<div class="navbar box-header no-border">Report – MRS REPORTS
	</div>
	<br>
	<div class="box">
		<div class="span12" id="appointmentGr-table">
			<div class="row-fluid form-horizontal">
				<div id="kiri" class="span6">
                    <div class="control-group">
                        <label class="control-label" for="filter_periode" style="text-align: left;">
                            Format Report
                        </label>
                        <div id="filter_format" class="controls">
                            <g:radioGroup name="formatReport" id="formatReport" values="['x','p']" labels="['Excel','PDF']" value="x" required="" >
                                ${it.radio} ${it.label}
                            </g:radioGroup>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="t951Tanggal"
                               style="text-align: left;"> Workshop </label>
                        <div id="filter_wo" class="controls">
                            <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                <g:select name="workshop" id="workshop" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                            </g:if>
                            <g:else>
                                <g:select name="workshop" id="workshop" readonly="" disabled="" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                            </g:else>
                            %{--<g:select name="workshop" id="workshop"--}%
                            %{--from="${CompanyDealer.list()}" optionKey="id"--}%
                            %{--optionValue="m011NamaWorkshop" style="width: 44%" />--}%
                        </div>
                    </div>
                    %{--<div class="control-group">--}%
						%{--<label class="control-label" for="t951Tanggal"--}%
							%{--style="text-align: left;"> Workshop </label>--}%
						%{--<div id="filter_wo" class="controls">--}%
                            %{--<g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">--}%
                                %{--<g:select name="workshop" id="workshop" onchange="changeWorkShop();" style="width: 44%" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />--}%
                            %{--</g:if>--}%
                            %{--<g:else>--}%
                                %{--<g:select name="workshop" id="workshop" onchange="changeWorkShop();" style="width: 44%" readonly="" disabled="" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />--}%
                            %{--</g:else>--}%
						%{--</div>--}%
					%{--</div>--}%
					<div class="control-group">
						<label class="control-label" for="t951Tanggal"
							style="text-align: left;"> Nama Report </label>
						<div id="filter_nama" class="controls">
							<select name="namaReport" id="namaReport" size="8"
								style="width: 100%; height: 230px; font-size: 12px;">
								<option value="01">01. MRS - KPI PROCESS</option>												
								<option value="02">02. MRS - KPI DIRECT MAIL</option>												
								<option value="03">03. MRS - KPI SMS</option>												
								<option value="04">04. MRS - KPI CALL</option>												
								<option value="05">05. MRS - KPI APPOINTMENT</option>												
								<option value="06">06. MRS - KPI RESULT</option>												
								<option value="07">07. MRS - APPOINTMENT CONTRIBUTION</option>												
								<option value="08">08. MRS - SBE CONTRIBUTION</option>
								<option value="09">09. MRS - UNIT ENTRY CONTRIBUTION</option>
								<option value="10">10. MRS - KPI PROCESS DETAIL</option>
								<option value="11">11. MRS - KPI TOTAL</option>
								<option value="12">12. MRS - SB RETENTION (FIRST SERVICE)</option>
								<option value="13">13. MRS - SB RETENTION PERIOD</option>
                                <option value="14">14. MRS - CUSTOMIZE REPORT</option>
                                <option value="15">15. SERVICE & FILL RATE</option>

							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="filter_periode" style="text-align: left;">
							Periode
						</label>
						<div id="filter_periode" class="controls">
							<ba:datePicker id="search_tanggal" name="search_tanggal" precision="day" format="dd-MM-yyyy"  value="${new Date()}" />
							 &nbsp;&nbsp;s.d.&nbsp;&nbsp;
							<ba:datePicker id="search_tanggal2" name="search_tanggal2" precision="day" format="dd-MM-yyyy"  value="${new Date()+1}"  />
						</div>
						<div id="filter_periode2" class="controls">
							%{
								def listBulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
							}%
							<select name="bulan1" id="bulan1" style="width: 170px">
								%{
									for(int i=0;i<12;i++){
										out.print('<option value="'+(i)+'">'+listBulan.get(i)+'</option>');
									}
								}%
							</select> &nbsp; - &nbsp;
							<select name="bulan2" id="bulan2" style="width: 170px">
								%{
									for(int i=0;i<12;i++){
										out.print('<option value="'+(i)+'">'+listBulan.get(i)+'</option>');
									}
								}%
							</select> &nbsp;
						   <select name="tahun" id="tahun" style="width: 106px">
							   %{
								for(def a = (new Date().format("yyyy")).toInteger();a >= 2000 ;a--){
									out.print('<option value="'+a+'">'+a+'</option>');
								}
							   }%

						   </select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="t951Tanggal" style="text-align: left;">
							Report Detail
						</label>
						<div id="filter_detail" class="controls">
							<input type="checkbox" name="detail" id="detail" value="1" /> Ya
						</div>
					</div>					
				</div>				
			</div>
			<g:field type="button" onclick="previewData()"
				class="btn btn-primary create" name="preview"
				value="${message(code: 'default.button.upload.label', default: 'Preview')}" />
			<g:field type="button" onclick="window.location.replace('#/home')"
				class="btn btn-cancel cancel" name="cancel"
				value="${message(code: 'default.button.upload.label', default: 'Close')}" />
		</div>
	</div>
</body>
</html>
