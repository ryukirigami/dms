<%@ page import="com.kombos.administrasi.KategoriJob" %>



<div class="control-group fieldcontain ${hasErrors(bean: kategoriJobInstance, field: 'm055KategoriJob', 'error')} required">
	<label class="control-label" for="m055KategoriJob">
		<g:message code="kategoriJob.m055KategoriJob.label" default="Kategori Job" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:if test="${kategoriJobInstance?.id && kategoriJobInstance?.m055staReadOnly=='0'}" >
            <g:textField name="m055KategoriJob" maxlength="50" readonly="" required="" value="${kategoriJobInstance?.m055KategoriJob}"/>
        </g:if>
        <g:else>
            <g:textField name="m055KategoriJob" maxlength="50" required="" value="${kategoriJobInstance?.m055KategoriJob}"/>
        </g:else>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: kategoriJobInstance, field: 'm055WarnaBord', 'error')} required">
    <label class="control-label" for="m055WarnaBord">
        <g:message code="kategoriJob.kategoriJobInstance.label" default="Warna pada board" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <input type="color" id="m055WarnaBord" name="m055WarnaBord" required="" value="${kategoriJobInstance?.m055WarnaBord}">
    </div>
</div>


<g:javascript>
    document.getElementById("m055KategoriJob").focus();
</g:javascript>

%{--<div class="control-group fieldcontain ${hasErrors(bean: kategoriJobInstance, field: 'm055staReadOnly', 'error')} required">--}%
	%{--<label class="control-label" for="m055staReadOnly">--}%
		%{--<g:message code="kategoriJob.m055staReadOnly.label" default="Keterangan" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="m055staReadOnly" maxlength="1" required="" value="${kategoriJobInstance?.m055staReadOnly}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: kategoriJobInstance, field: 'createdBy', 'error')} ">--}%
	%{--<label class="control-label" for="createdBy">--}%
		%{--<g:message code="kategoriJob.createdBy.label" default="Created By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="createdBy" value="${kategoriJobInstance?.createdBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: kategoriJobInstance, field: 'updatedBy', 'error')} ">--}%
	%{--<label class="control-label" for="updatedBy">--}%
		%{--<g:message code="kategoriJob.updatedBy.label" default="Updated By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="updatedBy" value="${kategoriJobInstance?.updatedBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: kategoriJobInstance, field: 'lastUpdProcess', 'error')} ">--}%
	%{--<label class="control-label" for="lastUpdProcess">--}%
		%{--<g:message code="kategoriJob.lastUpdProcess.label" default="Last Upd Process" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="lastUpdProcess" value="${kategoriJobInstance?.lastUpdProcess}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: kategoriJobInstance, field: 'm055ID', 'error')} ">--}%
	%{--<label class="control-label" for="m055ID">--}%
		%{--<g:message code="kategoriJob.m055ID.label" default="M055 ID" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="m055ID" value="${kategoriJobInstance?.m055ID}"/>--}%
	%{--</div>--}%
%{--</div>--}%

