
<%@ page import="com.kombos.finance.AccountNumber" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<title><g:message code="subledger.label" default="Data Subledger" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">

        </g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="subledger.label" default="Data Subledger" /></span>
		<ul class="nav pull-right">
			%{--<li class="separator"></li>--}%
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="subledger-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
			<g:render template="dataTables" />
        </div>
	</div>
</body>
</html>
