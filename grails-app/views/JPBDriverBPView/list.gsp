
<%@ page import="com.kombos.board.JPB" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="JPB" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			$(function(){ 

});
</g:javascript>
	</head>
	<body>
    <div class="navbar box-header no-border">
   		<span class="pull-left">Job Progress Board (JPB) Driver</span>
   	</div>

	<div class="box">

        <table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">
            <tr>
            <td style="width: 20%;vertical-align: top;">
                <button class="btn btn-primary" id="buttonDssDriver" onclick="jpbView()">DSS Driver</button>
                <button class="btn btn-primary" id="buttonPilotManual" onclick="jpbView()">Pilot Manual</button>
            </td>
            <td style="width: 50%;vertical-align: top;padding-left: 10px;">
                <table style="width: 50%;border: 0px">
                    <tr>
                        <td>
                            <label class="control-label" for="lbl_tglView">
                                Tanggal
                            </label>
                        </td>
                        <td>
                            <div id="lbl_tglView" class="controls">
                                 <input type="hidden" name="search_tglView" value="date.struct">
                                 <input type="hidden" name="search_tglView_day" id="search_tglView_day" value="">
                                 <input type="hidden" name="search_tglView_month" id="search_tglView_month" value="">
                                 <input type="hidden" name="search_tglView_year" id="search_tglView_year" value="">
                                 <input type="text" data-date-format="dd/mm/yyyy" name="search_tglView_dp" value="" id="search_tglView" class="search_init">
                                <button class="btn btn-primary" id="buttonView" onclick="jpbView()">View</button>
                            </div>
                        </td>
                        <td>

                        </td>
                    </tr>
                </table>
            </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div id="JPB-table">
                        <g:if test="${flash.message}">
                            <div class="message" role="status">
                                ${flash.message}
                            </div>
                        </g:if>

                        <g:render template="dataTables" />
                    </div>
                    </td>
                </tr>
        </table>

	</div>
</body>
</html>
