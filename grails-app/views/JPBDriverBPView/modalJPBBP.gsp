<%@ page import="com.kombos.parts.ETA" %>

<r:require modules="baseapplayout, baseapplist,autoNumeric" />
<script type="text/javascript">
    jQuery(function($) {
        $('.auto').autoNumeric('init');
    });
</script>

<div class="navbar box-header no-border">
    ${modalType}
</div>
<table class="table table-bordered table-hover">
    <tbody>
    <tr>
        <td class="span2" style="text-align: right;">
            <label class="control-label">
                <g:message code="modalJPBBP.stal1.label" default="Stall 1" />

            </label>

        </td>

        <td class="span3">
            <label class="control-label" for="stal1" >
                <g:select name="stal1" from="${stal1}" value="${stal1}" />
            </label>
        </td>
    </tr>
    </div>

    <tr>
        <td class="span2" style="text-align: right;">
            <label class="control-label" for="po">
                <g:message code="ETA.idpo.label" default="Nomor PO" />

            </label>
        </td>
        <td class="span3" >
            <g:textField name="nomorPO" id="nomorPO" value="${nomorPO}" readonly="true"/>
        </td>
    </tr>

    <tr>
        <td class="span2" style="text-align: right;">
            <label class="control-label" for="t165ETA">
                <g:message code="ETA.tanggalPO.label" default="Tanggal PO" />

            </label>
        </td>
        <td class="span3">
            <label class="control-label" for="tglPO" >
                ${tanggalPO}
            </label>
        </td>
    </tr>
    <tr>
        <td class="span2" style="text-align: right;">
            <label class="control-label" for="kodePart">
                <g:message code="ETA.kodePart.label" default="Kode Part | Nama Part " />

            </label>
        </td>
        <td class="span3">
            <g:select name="idGoods" id="idGoods" from="${goods}" optionKey="id" />
        </td>
    </tr>

    <tr>
        <td class="span2" style="text-align: right;">
            <div class="control-group fieldcontain ${hasErrors(bean: ETAInstance, field: 't165Qty1', 'error')} ">
                <label class="control-label" for="t165Qty1">
                    <g:message code="ETA.t165Qty1.label" default="Qty Issue" />

                </label>
        </td>
        <td class="span3">
            <div class="controls">
                <g:textField name="qtyETA" id="qtyETA" class="auto" number=""/>
            </div>
        </div>
        </td>
    </tr>
    <tr>
        <td class="span2" style="text-align: right;">
            <div class="control-group fieldcontain ${hasErrors(bean: POInstance, field: 't164TglPO', 'error')} ">
                <label class="control-label" for="t164TglPO">
                    <g:message code="PO.t164TglPO.label" default="Tgl ETA" />

                </label>
            </div>
        </td>
        <td class="span3">
            <div class="controls">
                <div style="width:200px">
                    <g:datePicker name="tglETA" id="tglETA" precision="day"  format="dd-mm-yyyy"/>
                </div>
                <br/>
                H :
                <select id="jamETA" name="jamETA"  style="width: 60px" required="">
                    %{
                        for (int i=0;i<24;i++){
                            if(i<10){

                                out.println('<option value="'+i+'">0'+i+'</option>');


                            } else {

                                out.println('<option value="'+i+'">'+i+'</option>');

                            }

                        }
                    }%
                </select>
                <select id="menitETA" name="menitETA" style="width: 60px" required="">
                    %{
                        for (int i=0;i<60;i++){
                            if(i<10){

                                out.println('<option value="'+i+'">0'+i+'</option>');


                            } else {

                                out.println('<option value="'+i+'">'+i+'</option>');

                            }
                        }
                    }%
                </select> m
            </div>
        </td>
    </tr>
    </div>
    </tr>
    </tbody>
</table>
<div class="modal-footer">
</div>

<g:javascript>

    $(function(){

        $("#tglETA_day").css({width : '100px'});
        $("#tglETA_month").css({width : '100px'});
        $("#tglETA_year").css({width : '100px'});

    });
</g:javascript>


