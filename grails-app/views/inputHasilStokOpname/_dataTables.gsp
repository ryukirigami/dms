
<%@ page import="com.kombos.parts.StokOPName" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="inputHasilStokOpname_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="stokOPName.goods.kode.label" default="Kode Part" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="stokOPName.goods.nama.label" default="Nama Part" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="stokOPName.goods.satuan.label" default="Satuan yang Digunakan" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="stokOPName.qty.kode.label" default="Qty" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="stokOPName.icc.nama.label" default="ICC" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="stokOPName.goods.lokasi.label" default="Lokasi/Rak" /></div>
            </th>

		
		</tr>
	</thead>
</table>

<g:javascript>
var inputHasilStokOpnameTable;
var reloadInputHasilStokOpnameTable;
$(function(){
	
	reloadInputHasilStokOpnameTable = function() {
		inputHasilStokOpnameTable.fnDraw();
	}

	



$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	inputHasilStokOpnameTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	inputHasilStokOpnameTable = $('#inputHasilStokOpname_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "goods",
	"mDataProp": "kodegoods",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';//<a href="#" onclick="show('+row['id']+');">'+data+'
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "goods",
	"mDataProp": "namagoods",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "goods",
	"mDataProp": "satuangoods",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "",
	"mDataProp": "qty",
	"aTargets": [3],
	"mRender": function ( data, type, row ) {
		return '<input id="qty-'+row['id']+'" onblur="setProgress('+row['id']+');" onkeypress="return isNumberKey(event);" class="inline-edit" type="text" style="width:218px;" value="'+data+'">';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "goods",
	"mDataProp": "icc",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "goods",
	"mDataProp": "lokasigoods",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var t132ID = $('#search_t132ID').val();
						if(t132ID){
							aoData.push(
									{"name": 'sCriteria_t132ID', "value": t132ID}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
