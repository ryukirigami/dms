
<%@ page import="com.kombos.parts.StokOPName" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'inputHasilStokOpname.label', default: 'Hasil Stok Opname')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			var setProgress;
			var arrayId = [];

			$(function(){
			
				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('inputHasilStokOpname');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('inputHasilStokOpname', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('inputHasilStokOpname', '${request.contextPath}/inputHasilStokOpname/massdelete', reloadInputHasilStokOpnameTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('inputHasilStokOpname','${request.contextPath}/inputHasilStokOpname/show/'+id);
				};
				
				edit = function(id) {
					editInstance('inputHasilStokOpname','${request.contextPath}/inputHasilStokOpname/edit/'+id);
				};


    setProgress = function(params){
        var length=0;
        $("#inputHasilStokOpname_datatables tbody .row-select").each(function() {
            length++;
        });
        var value;
        var progr=100/parseInt(length);
        var cek=false;
        var valueProgress = $('#progressbar').val();
        if(arrayId.length<1){
            arrayId.push(params)
            cek=true;
        }else{
            if(arrayId.indexOf(params)==-1){
                arrayId.push(params);
                cek=true;
            }
        }

        if(cek){
            $('#progressbar').val(parseInt(valueProgress)+parseFloat(progr));
            document.getElementById('lblPersen').innerHTML = $('#progressbar').val();
        }
    }



});

        $('#saveChange').click(function(){
        var idChange =[];
        var qty1Input = [];
        $("#inputHasilStokOpname_datatables tbody .row-select").each(function() {
            var id = $(this).next("input:hidden").val();
            idChange.push(id);
            qty1Input.push($('#qty-'+id).val())
        });

        var jsonId = JSON.stringify(idChange);
        var jsonQty = JSON.stringify(qty1Input);
        if(($('#search_t132ID').val()!=null ||$('#search_t132ID').val()!="") && ($('#search_t132Tanggal').val()!=null || $('#search_t132Tanggal').val()!="")){
            $.ajax({
                url:'${request.contextPath}/inputHasilStokOpname/editQty',
                type: "POST",
                data: {ids: jsonId, qtys : jsonQty} ,
                success: function(data) {
                    if(data=="full"){
                        alert('pindah ke onhand adjustment');
                    }
                    $('#progressbar').val(0);
                    document.getElementById('lblPersen').innerHTML = 0;
                    toastr.success('<div>Data Saved.</div>');
                    inputHasilStokOpnameTable.fnDraw();
                },
                error: function(xhr, textStatus, errorThrown) {
                    alert('Internal Server Error');
                }
    		});
        }else{
            alert('null');
        }
        idChange = []
    });

    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }


</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="stokOPName.inputHasil.label" default="Input Hasil Stok Opname" /></span>
		<ul class="nav pull-right">
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="inputHasilStokOpname-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <table>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="stokOPName.t132ID.label" default="Nomor Stock Opname"/>
                    </td>
                    <td style="padding: 5px">
                        <g:textField name="search_t132ID" id="search_t132ID" value="${part132ID}" readonly="" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="stokOPName.t132Tanggal.label" default="Tanggal Stock Opname"/>
                    </td>
                    <td style="padding: 5px">
                        <g:textField name="search_t132Tanggal" id="search_t132Tanggal" value="${part132Tanggal}" readonly="" />
                    </td>
                </tr>
            </table>
            <br/>
            <div>
                <table>
                    <td style="padding: 1px"><g:message code="stokOPName.sudahSave.label" default="Stock Opname yang Sudah Diinput " /></td>
                    <td style="padding: 3px"><label id="lblPersen" style="font-size: 14px"> 0 </label></td>
                    <td style="padding: 1px">%</td>
                </table>
            </div>
            <br/>
            <div id="divProgressbar">
                <progress style="width: 100%;height: 30px;" id="progressbar" value="0" max="100"></progress>
            </div>
            <br/>
            <div>

                <g:field type="button" class="btn cancel" name="saveChange" id="saveChange" value="${message(code: 'default.button.save.label', default: 'Save')}" />
            </div>
			<g:render template="dataTables" />
            <a class="pull-right box-action" style="display: block;" >
                &nbsp;&nbsp;
                <i>
                    <g:field type="button" onclick="window.location.replace('#/viewSOpnameTdkSesuai');" style="padding: 5px"
                         class="btn cancel pull-right box-action" name="viewSOTdkSesuai" id="viewSOTdkSesuai" value="${message(code: 'stokopname.button.viewSOTdkSesuai.label', default: 'View Stock Opname Tidak Sesuai')}" />
                    %{--<g:field type="button" style="padding: 5px" class="btn cancel pull-right box-action"--}%
                             %{--name="viewSOTdkSesuai" id="viewSOTdkSesuai" value="${message(code: 'stokopname.button.viewSOTdkSesuai.label', default: 'View Stock Opname Tidak Sesuai')}" />--}%
                </i>
                &nbsp;&nbsp;
            </a>
		</div>
		<div class="span7" id="inputHasilStokOpname-form" style="display: none;"></div>
	</div>
</body>
</html>
