<%--
  Created by IntelliJ IDEA.
  User: Kalingga
  Date: 4/6/15
  Time: 12:08 AM
--%>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript>
var partTable_${idTable};
$(function(){
    if(partTable_${idTable})
    	partTable_${idTable}.dataTable().fnDestroy();
partTable_${idTable} = $('#part_datatables_${idTable}').dataTable({
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "t",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": false,
		"bProcessing": false,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "partDatatablesList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": ''
},
{
"sName": "namaPart",
"mDataProp": "namaPart",
"aTargets": [0],
"bSearchable": true,
"bSortable": false,
"sWidth":"90px",
"bVisible": true
},
{
"sName": "harga",
"mDataProp": "harga",
"aTargets": [1],
"bSearchable": true,
"bSortable": false,
"sWidth":"25px",
"bVisible": true
},
{
"sName": "kategori",
"mDataProp": "kategori",
"aTargets": [2],
"bSearchable": true,
"bSortable": false,
"sWidth":"25px",
"bVisible": true
},
{
"sName": "cvPersen",
"mDataProp": "cvPersen",
"aTargets": [3],
"bSearchable": true,
"bSortable": false,
"sWidth":"25px",
"bVisible": true
},
{
"sName": "cvRp",
"mDataProp": "cvRp",
"aTargets": [4],
"bSearchable": true,
"bSortable": false,
"sWidth":"25px",
"bVisible": true
},
{
"sName": "staticPersen",
"mDataProp": "staticPersen",
"aTargets": [5],
"bSearchable": true,
"bSortable": false,
"sWidth":"25px",
"bVisible": true
},
{
"sName": "staticRp",
"mDataProp": "staticRp",
"aTargets": [6],
"bSearchable": true,
"bSortable": false,
"sWidth":"25px",
"bVisible": true
},
{
"sName": "dinPersen",
"mDataProp": "dinPersen",
"aTargets": [7],
"bSearchable": true,
"bSortable": false,
"sWidth":"25px",
"bVisible": true
},
{
"sName": "dinRp",
"mDataProp": "dinRp",
"aTargets": [8],
"bSearchable": true,
"bSortable": false,
"sWidth":"25px",
"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						aoData.push(
									{"name": 'idJob', "value": "${idJob}"},
									{"name": 'idInv', "value": "${idInv}"}
						);

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
    </g:javascript>
</head>
<body>
<table id="part_datatables_${idTable}" cellpadding="0" cellspacing="0" border="0"
       class="display table table-striped table-bordered table-hover">
    <tbody></tbody>
</table>

</body>
</html>
