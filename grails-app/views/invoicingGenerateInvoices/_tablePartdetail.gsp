<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>


<table id="partDetail_dataTable" cellpadding="0" cellspacing="0" class="display table table-bordered" width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 2px;">Kode Part</th>

        <th style="border-bottom: none;padding: 2px;">Nama Part</th>

        <th style="border-bottom: none;padding: 2px;">Qty</th>

        <th style="border-bottom: none;padding: 2px;">Harga Satuan</th>

        <th style="border-bottom: none;padding: 2px;">Disc</th>

        <th style="border-bottom: none;padding: 2px;">Extd. Ammount</th>

    </tr>
    </thead>
    <tfoot align="right">
        <tr>
            <td colspan="5">SUBTOTAL</td>
            <td><span class="pull-right numeric" id="lblsubtotpart"></span></td>
        </tr>
    </tfoot>
</table>

<g:javascript>

var reloadPartDetailTable;
var partDetailTable;

$(function(){

	reloadPartDetailTable = function() {
		partDetailTable.fnDraw();
	}

	partDetailTable = $('#partDetail_dataTable').dataTable({

		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
		    var aData = partDetailTable.fnGetData(nRow);
		    $('#lblsubtotpart').text(aData.stotpart);
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "dataTablePart")}",
		"aoColumns": [

{
	"sName": "kodePart",
	"mDataProp": "kodePart",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"70px",
	"bVisible": true
},
{
	"sName": "namaPart",
	"mDataProp": "namaPart",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "qty",
	"mDataProp": "qty",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"25px",
	"bVisible": true
},
{
	"sName": "harga",
	"mDataProp": "harga",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"70px",
	"mRender": function ( data, type, row ) {
        if(data != null){
        return '<span class="pull-right numeric">'+data+'</span>';
        }else{
        return '<span></span>';
        }
    },
	"bVisible": true
},
{
	"sName": "disc",
	"mDataProp": "disc",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"25px",
	"bVisible": true
},
{
	"sName": "extAmmount",
	"mDataProp": "extAmmount",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"80px",
	"mRender": function ( data, type, row ) {
        if(data != null){
        return '<span class="pull-right numeric">'+data+'</span>';
        }else{
        return '<span></span>';
        }
    },
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                  aoData.push(
                            {"name": 'noInvoice', "value": noInvo}
                  )

                  $.ajax({ "dataType": 'json',
				  		   "type": "POST",
						   "url": sSource,
						   "data": aoData ,
						   "success": function (json) {
								fnCallback(json);
						   },
						   "complete": function(){}
				  });
		}
	});
});
</g:javascript>



