
<%@ page import="com.kombos.administrasi.KegiatanApproval;" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="approval.reversal.label" default="Delivery - Send Approval Invoice Reversal"/></title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>
        var sendApproval;
        $(function(){
            sendApproval = function(){
                var pesanApproval = $('#pesanApproval').val();
                if(pesanApproval=="" || pesanApproval.replace(" ","")==""){
                    alert('Silahkan isi pesan terlebih dahulu');
                    return
                }
                $('#spinner').fadeIn(1);
                $.ajax({url: '${request.contextPath}/invoicingGenerateInvoices/sendApprovalReversal',
                    data : {pesan : pesanApproval, noInvoice : $('#noInvApproval').val()},
                    type: "POST",
                    success: function(data) {
                        alert('SUKSES');
                    },
                    error : function(){
                        alert('Internal Server Error');
                    }
                });
            }
        })
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="approval.reversal.label" default="Delivery - Send Approval Invoice Reversal"/></span>
    <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
</div>
<div class="box">
    <div class="span12" id="approval-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>

        <table class="table">
            <tbody>
            <tr>
                <td>
                    Nama Kegiatan *
                </td>
                <td>
                    <g:select id="kegiatanApproval" name="kegiatanApproval" from="${KegiatanApproval.list()}" readonly="" disabled=""  value="${KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.INVOICE_REVERSAL).id}" optionKey="id" required="" class="many-to-one"/>
                </td>
            </tr>
            <tr>
                <td>
                    Nomor WO
                </td>
                <td>
                    <g:textField name="noWoApproval" id="noWoApproval" readonly="" value="${invoiceInstance?.reception?.t401NoWO}" />
                </td>
            </tr>
            <tr>
                <td>
                    Nomor Invoice
                </td>
                <td>
                    <g:textField name="noInvApproval" id="noInvApproval" readonly="" value="${invoiceInstance?.t701NoInv}"/>
                </td>
            </tr>
            <tr>
                <td>
                    Nomor Polisi
                </td>
                <td>
                    <g:textField name="txtnopolApproval" id="txtnopolApproval" readonly="" value="${invoiceInstance?.reception?.historyCustomerVehicle?.fullNoPol}"/>
                </td>
            </tr>
            <tr>
                <td>
                    Model Kendaraan
                </td>
                <td>
                    <g:textField name="txtmodelApproval" id="txtmodelApproval" readonly="" value="${invoiceInstance?.reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel}"/>
                </td>
            </tr>
            <tr>
                <td>
                    Tanggal Permohonan
                </td>
                <td>
                    ${new Date().format("dd-MMMM-yyyy HH:mm:ss")}
                </td>
            </tr>
            <tr>
                <td>
                    Pesan
                </td>
                <td>
                    <textarea id="pesanApproval" name="pesanApproval" cols="200" rows="3" style="resize: none" />
                </td>
            </tr>
            </tbody>
        </table>
        <div class="pull-right">
            <g:field type="button" class="btn btn-primary" onclick="sendApproval();" name="send" id="send" value="Send" />
            <g:field type="button" class="btn cancel" name="close" id="close" value="Close" data-dismiss="modal" />
        </div>
    </div>
    <div class="span7" id="approval-form" style="display: none;"></div>
</div>
</body>
</html>
