<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="invoicing.discountTable.label" default="Delivery - Discount Table"/></title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>
        $("#lblNoInvoiceDiscount").text("${invoiceInstance?.invoice?.t701NoInv ? invoiceInstance?.invoice?.t701NoInv : '-'}");
        $("#lblNoPartSlipDiscount").text("${invoiceInstance?.t703NoPartInv ? invoiceInstance?.t703NoPartInv : '-'}");
        var noIn = "${invoiceInstance?.invoice?.t701NoInv ? invoiceInstance?.invoice?.t701NoInv : '-'}";
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="invoicing.discountTable.label" default="Delivery - Discount Table"/></span>
    <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
</div>
<div class="box">
    <div class="span12" id="discountTable-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <table class="table table-bordered" style="width: 600px;">
            <tr>
                <td style="width: 50%">
                    No. Invoice/Credit Note : <b id="lblNoInvoiceDiscount"></b>
                </td>
                <td>
                    No. Part Slip : <b id="lblNoPartSlipDiscount"></b>
                </td>
            </tr>
        </table>
        <br/>
        <g:render template="dataTableDiscount" />
    </div>
    <div class="span7" id="discountTable-form" style="display: none;"></div>
</div>
</body>
</html>
