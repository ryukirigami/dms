<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>


<table id="generate_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="100%">

    <thead>
    <tr>

        <th style="border-bottom: none;padding: 2px;">
            <div style="width: 150px"><g:message code="genInvoice.NoInvoice.label" default="No. Invoice" /></div>
        </th>

        <th style="border-bottom: none;padding: 2px;">
            <div style="width: 150px"><g:message code="genInvoice.invReff.label" default="Inv. Ref" /></div>
        </th>

        <th style="border-bottom: none;padding: 2px;">
            <div style="width: 100px"><g:message code="genInvoice.partslip.label" default="Part Slip" /></div>
        </th>

        <th style="border-bottom: none;padding: 2px;">
            <div style="width: 100px"><g:message code="genInvoice.tanggal.label" default="Tanggal" /></div>
        </th>

        <th style="border-bottom: none;padding: 2px;">
            <div style="width: 100px"><g:message code="genInvoice.nomorWO.label" default="Nomor WO" /></div>
        </th>

        <th style="border-bottom: none;padding: 2px;">
            <div style="width: 150px"><g:message code="genInvoice.kepada.label" default="Kepada" /></div>
        </th>
        <th style="border-bottom: none;padding: 2px;">
            <div style="width: 150px"><g:message code="genInvoice.alamat.label" default="Alamat" /></div>
        </th>
        <th style="border-bottom: none;padding: 2px;">
            <div style="width: 100px"><g:message code="genInvoice.npwp.label" default="NPWP" /></div>
        </th>
        <th style="border-bottom: none;padding: 2px;">
            <div style="width: 30px"><g:message code="genInvoice.tipe.label" default="Tipe" /></div>
        </th>
        <th style="border-bottom: none;padding: 2px;">
            <div style="width: 35px"><g:message code="genInvoice.jenis.label" default="Jenis" /></div>
        </th>
        <th style="border-bottom: none;padding: 2px;">
            <div style="width: 60px"><g:message code="genInvoice.warranty.label" default="Warranty" /></div>
        </th>
        <th style="border-bottom: none;padding: 2px;">
            <div style="width: 75px"><g:message code="genInvoice.jasalabor.label" default="Jasa/Labor" /></div>
        </th>
        <th style="border-bottom: none;padding: 2px;">
            <div style="width: 40px"><g:message code="genInvoice.sublet.label" default="Sublet" /></div>
        </th>
        <th style="border-bottom: none;padding: 2px;">
            <div style="width: 40px"><g:message code="genInvoice.parts.label" default="Parts" /></div>
        </th>
        <th style="border-bottom: none;padding: 2px;">
            <div style="width: 40px"><g:message code="genInvoice.oil.label" default="Oil" /></div>
        </th>
        <th style="border-bottom: none;padding: 2px;">
            <div style="width: 60px"><g:message code="genInvoice.material.label" default="Material" /></div>
        </th>
        <th style="border-bottom: none;padding: 2px;">
            <div style="width: 85px"><g:message code="genInvoice.sublet.label" default="Administrasi" /></div>
        </th>
        <th style="border-bottom: none;padding: 2px;">
            <div style="width: 80px"><g:message code="genInvoice.subTotal.label" default="Sub Total" /></div>
        </th>
        <th style="border-bottom: none;padding: 2px;">
            <div style="width: 60px"><g:message code="genInvoice.Discount.label" default="Discount" /></div>
        </th>
        <th style="border-bottom: none;padding: 2px;">
            <div style="width: 150px"><g:message code="genInvoice.subTotalStlhDisc.label" default="Sub Total Stlh Disc" /></div>
        </th>
        <th style="border-bottom: none;padding: 2px;">
            <div style="width: 60px"><g:message code="genInvoice.ppn.label" default="PPN 10%" /></div>
        </th>
        <th style="border-bottom: none;padding: 2px;">
            <div style="width: 50px"><g:message code="genInvoice.totalInv.label" default="Materai" /></div>
        </th>
        <th style="border-bottom: none;padding: 2px;">
            <div style="width: 100px"><g:message code="genInvoice.t921TglKasusDiterima.label" default="Total Invoice" /></div>
        </th>
        <th style="border-bottom: none;padding: 2px;">
            <div style="width: 30px"><g:message code="genInvoice.bf.label" default="BF" /></div>
        </th>
        <th style="border-bottom: none;padding: 2px;">
            <div style="width: 50px"><g:message code="genInvoice.onRisk.label" default="On Risk" /></div>
        </th>
        <th style="border-bottom: none;padding: 2px;">
            <div style="width: 150px"><g:message code="genInvoice.tobar.label" default="Total Yang Dibayar" /></div>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
var generateTable;
var reloadGenerateTable;

$(function()
{
	reloadGenerateTable = function() {
		generateTable.fnDraw();
	}

	generateTable = $('#generate_datatables').dataTable({

		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
           "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
		   $(nRow).children().each(function(index, td) {
                if(aData.t701StaApprovedReversal && aData.t701StaApprovedReversal == "0"){
                    $(td).css("background-color", "lightpink");
                }
		   });
		    $(nRow).children().each(function(index, td) {
                if(aData.t701NoInv_Reff){
                    $(td).css("background-color", "lightblue");
                }
		   });
			return nRow;
		   },
		"bSort": false,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "t701NoInv",
	"mDataProp": "t701NoInv",
	"aTargets": [0],
    "mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="javascript:void(0);" onclick="detail(\''+data+'\',\''+row['t703NoPartInv']+'\');">'+data+'</a>';
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "t701NoInv_Reff",
	"mDataProp": "t701NoInv_Reff",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "t703NoPartInv",
	"mDataProp": "t703NoPartInv",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "t701TglJamInvoice",
	"mDataProp": "t701TglJamInvoice",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "reception",
	"mDataProp": "reception",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "t701Customer",
	"mDataProp": "t701Customer",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "t701Alamat",
	"mDataProp": "t701Alamat",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": false,
	"mRender": function ( data, type, row ) {
        return '<textarea id="alamat-'+row['id']+'" class="inline-edit" style="width:120px;" onblur="saveAdmMaterai(\'alamat\','+row['id']+');">' +data+'</textarea>';
    },
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "t701NPWP",
	"mDataProp": "t701NPWP",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": false,
	"mRender": function ( data, type, row ) {
        return '<input type="text" id="npwp-'+row['id']+'" class="inline-edit" style="width:160px;" value="'+data+'" onblur="saveAdmMaterai(\'npwp\','+row['id']+');" />';

    },
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "t701JenisInv",
	"mDataProp": "t701JenisInv",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"30px",
	"bVisible": true
},
{
	"sName": "t701Tipe",
	"mDataProp": "t701Tipe",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"350px",
	"bVisible": true
},
{
	"sName": "t701StaWarranty",
	"mDataProp": "t701StaWarranty",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"60px",
	"bVisible": true
},
{
	"sName": "t701JasaRp",
	"mDataProp": "t701JasaRp",
	"aTargets": [11],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"75px",
	"mRender": function ( data, type, row ) {
        if(data != null){
            if(row['t701Tipe']=="BP" && row['isEnable']=="YA"){
                return '<input id="jasa-'+row['id']+'" class="inline-edit" type="text" style="width:150px;" onblur="saveAdmMaterai(\'jasa\','+row['id']+');" value="'+data+'">';
            }else{
                return '<span class="pull-right numeric">'+data+'</span>';
            }
        }else{
        return '<span></span>';
        }
    },
	"bVisible": true
},
{
	"sName": "t701SubletRp",
	"mDataProp": "t701SubletRp",
	"aTargets": [12],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"40px",
	"bVisible": true
},
{
	"sName": "t701PartsRp",
	"mDataProp": "t701PartsRp",
	"aTargets": [13],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"40px",
	"mRender": function ( data, type, row ) {
        if(data != null){
        return '<span class="pull-right numeric">'+data+'</span>';
        }else{
        return '<span></span>';
        }
    },
	"bVisible": true
},
{
	"sName": "t701OliRp",
	"mDataProp": "t701OliRp",
	"aTargets": [14],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"40px",
	"bVisible": true
},
{
	"sName": "t701MaterialRp",
	"mDataProp": "t701MaterialRp",
	"aTargets": [15],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"60px",
	"bVisible": true
},

{
	"sName": "t701AdmRp",
	"mDataProp": "t701AdmRp",
	"aTargets": [16],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        if(row['isEnable']=="YA"){
            return '<input id="adm-'+row['id']+'" class="inline-edit" type="text" style="width:80px;" onblur="saveAdmMaterai(\'adm\','+row['id']+');" value="'+data+'">';
        }else{
            return '<span class="pull-right numeric">'+data+'</span>';
        }
    },
	"bSortable": false,
	"sWidth":"85px",
	"bVisible": true
},
{
	"sName": "t701SubTotalRp",
	"mDataProp": "t701SubTotalRp",
	"aTargets": [17],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"80px",
	"mRender": function ( data, type, row ) {
        if(data != null){
        return '<span class="pull-right numeric">'+data+'</span>';
        }else{
        return '<span></span>';
        }
    },
	"bVisible": true
},
{
	"sName": "t701TotalDiscRp",
	"mDataProp": "t701TotalDiscRp",
	"aTargets": [18],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"60px",
	"bVisible": true
},
{
	"sName": "t701TotalRp",
	"mDataProp": "t701TotalRp",
	"aTargets": [19],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"150px",
	"mRender": function ( data, type, row ) {
        if(data != null){
        return '<span class="pull-right numeric">'+data+'</span>';
        }else{
        return '<span></span>';
        }
    },
	"bVisible": true
},
{
	"sName": "t701PPnRp",
	"mDataProp": "t701PPnRp",
	"aTargets": [20],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"60px",
	"bVisible": true
},
{
	"sName": "t701MateraiRp",
	"mDataProp": "t701MateraiRp",
	"mRender": function ( data, type, row ) {
        if(row['isEnable']=="YA"){
            return '<input id="materai-'+row['id']+'" class="inline-edit" type="text" style="width:80px;" onblur="saveAdmMaterai(\'materai\','+row['id']+');"  value="'+data+'">';
        }else{
            return '<span class="pull-right numeric">'+data+'</span>';
        }
    },
	"aTargets": [21],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"50px",
	"bVisible": true
},
{
	"sName": "t701TotalInv",
	"mDataProp": "t701TotalInv",
	"aTargets": [22],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"100px",
	"mRender": function ( data, type, row ) {
        if(data != null){
        return '<span class="pull-right numeric">'+data+'</span>';
        }else{
        return '<span></span>';
        }
    },
	"bVisible": true
},
{
	"sName": "t701BookingFee",
	"mDataProp": "t701BookingFee",
	"aTargets": [23],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"30px",
	"bVisible": true
},
{
	"sName": "t701OnRisk",
	"mDataProp": "t701OnRisk",
	"aTargets": [24],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"50px",
	"bVisible": true
},
{
	"sName": "t701TotalBayarRp",
	"mDataProp": "t701TotalBayarRp",
	"aTargets": [25],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"150px",
	"mRender": function ( data, type, row ) {
        if(data != null){
        return '<span class="pull-right numeric">'+data+'</span>';
        }else{
        return '<span></span>';
        }
    },
	"bVisible": true
}],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

            aoData.push(
                 {"name": 'noWO', "value": vNoWo}
            )

            $.ajax({ "dataType": 'json',
					"type": "POST",
					"url": sSource,
					"data": aoData ,
					"success": function (json) {
						fnCallback(json);
  				   },
 					"complete": function () {}
			});
		}			
	});

    var cekAda = "${receptionInstance?.t401NoWO}";
    if(cekAda){
        vNoWo = "${receptionInstance?.t401NoWO}";
        reloadGenerateTable();
    }
});
</g:javascript>


			
