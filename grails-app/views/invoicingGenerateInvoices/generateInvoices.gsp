<%@ page import="com.kombos.baseapp.sec.shiro.User" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'delivery.invoicing.label', default: 'Delivery - Invoicing')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
    var loadInvoicing;
    var showDetail;
    var vNoWo = "-1";
    var noInvo = "-1";
    var detail;
    function doPopDisc(param){
        var invApproval = []
        $("#generate_datatables .row-select").each(function() {
                if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			lookDisc(id);
    			invApproval.push(id);
    		}
		});

		if(invApproval.length<1){
		    alert('Anda belum memilih nomor invoice untuk melihat discount information');
		    return
		}
		if(invApproval.length>1){
		    alert('Anda hanya dapat memilih 1 data');
		    return
		}
    }

    lookDisc = function(b) {
        $("#discountContent").empty();
        $.ajax({
            type:'POST',
            url:'${request.contextPath}/invoicingGenerateInvoices/discountTable',
            data: {idInv:b},
            success:function(data,textStatus){
              $("#discountContent").html(data);
                $("#discountModal").modal({
                    "backdrop" : "dynamic",
                    "keyboard" : true,
                    "show" : true
                }).css({'width': '1000px','margin-left': function () {return -($(this).width() / 2);}});
            },
            error:function(XMLHttpRequest,textStatus,errorThrown){},
            complete:function(XMLHttpRequest,textStatus){
                $('#spinner').fadeOut();
            }
        });
    }

    $(function () {
        $("#kataKunci").bind('keypress', function(e) {
                var code = (e.keyCode ? e.keyCode : e.which);
                    if(code == 13) {
                            showDetail();
                    }
        });
    detail = function(noInv,noPart){
        noInvo = noInv;
        $('#lblnoinvoice').text(noInv);
        $('#lblnopartslip').text(noPart);
        $('#lblnopartslip2').text(noPart);
        cekApproval(noInv);
        $("#catatan").val('');
        $("#lblsubtotpart").html('');
        $("#lblsubtot").html('');
        reloadJasaLabourTable();
        reloadPartDetailTable();
    }

    cekApproval = function(noInv){
        $('#spinner').fadeIn(1);
        $.ajax({url: '${request.contextPath}/invoicingGenerateInvoices/cekApproval?noInv='+noInv,
                type: "POST",
                success: function(data) {
                    if(data.hasil=="ada"){
                        $("#lblstaapproval").text(data.staApp);
                        $("#lblalasanapproval").text(data.alasan);
                    }else{
                        $("#lblstaapproval").text("");
                        $("#lblalasanapproval").text("");
                    }
                },
                error : function(){
                    alert('Internal Server Error');
                },
                complete : function (req, err) {
                    $('#spinner').fadeOut();
                    loading = false;
                }
            });
        }

        showDetail = function() {
            noInvo = "-1";
            reloadJasaLabourTable();
            reloadPartDetailTable();
            var kategoriView = document.getElementById("kategoriView").value;
            var kataKunci = document.getElementById("kataKunci").value;
				$('#spinner').fadeIn(1);
                $("#tglWO").text("");
                $("#noMesin").text("");
                $("#namCustomer").text("");
                $("#alamat").text("");
                $("#noWo").text("");
                $("#noRangka").text("");
                $("#SA").text("");
                $("#noPol").text("");
                $("#modelName").text("");
                $("#billinit").text("");
                $("#lblsubtotpart").text("");
                $("#lblsubtot").text("");
				$.ajax({url: '${request.contextPath}/invoicingGenerateInvoices/loadDataInv',
					type: "POST",
					data: {kategoriView : kategoriView,kataKunci : kataKunci},
					success: function(data) {
                        //alert("sukses")
						if(data.status == 'OK'){
                            if(data.InfoInvoicing.tglWo){
                                $("#tglWO").text(data.InfoInvoicing.tglWo);
                            }
                            if(data.InfoInvoicing.noMesin){
                                $("#noMesin").text(data.InfoInvoicing.noMesin);
                            }
                            if(data.InfoInvoicing.namaCustomer){
                                $("#namCustomer").text(data.InfoInvoicing.namaCustomer);
                            }
                            if(data.InfoInvoicing.t182Alamat){
                                $("#alamat").text(data.InfoInvoicing.t182Alamat);
                            }
                            if(data.InfoInvoicing.noWo){
                                $("#noWo").text(data.InfoInvoicing.noWo);
                                vNoWo = data.InfoInvoicing.noWo;
                            }
                            if(data.InfoInvoicing.noRangka){
                                $("#noRangka").text(data.InfoInvoicing.noRangka);
                            }
                            if(data.InfoInvoicing.serviceAdv){
                                $("#SA").text(data.InfoInvoicing.serviceAdv);
                            }
                            if(data.InfoInvoicing.nopol){
                                $("#noPol").text(data.InfoInvoicing.nopol);
                            }
                            if(data.InfoInvoicing.modelNm){
                                $("#modelName").text(data.InfoInvoicing.modelNm);
                            }
                            if(data.InfoInvoicing.billing){
                                $("#billinit").text(data.InfoInvoicing.billing);
                            }
                            reloadGenerateTable();
                            $('#lblnoinvoice').text("");
                            $('#lblnopartslip').text("");
                            $('#lblnopartslip2').text("");
                            reloadJasaLabourTable();
                            reloadPartDetailTable();
						}else
						{
                            vNoWo = "-1";
                            noInvo = "-1"
						    alert("Data Tidak Ditemukan")
                            reloadGenerateTable();
                            $('#lblnoinvoice').text("");
                            $('#lblnopartslip').text("");
                            $('#lblnopartslip2').text("");
                            reloadJasaLabourTable();
                            reloadPartDetailTable();
						}
					},
					complete : function (req, err) {
						$('#spinner').fadeOut();
						loading = false;
					}
				});
			}

    });

    function doSendApproval(){
        var invApproval = []
        $("#generate_datatables .row-select").each(function() {
                if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			invApproval.push(id);
    		}
		});

		if(invApproval.length<1){
		    alert('Anda belum memilih data yang akan dibuatkan reversal');
		    return
		}
		if(invApproval.length>1){
		    alert('Anda hanya dapat memilih 1 data');
		    return
		}


        $("#approvalContent").empty();
        $.ajax({
            type:'POST',
            url:'${request.contextPath}/invoicingGenerateInvoices/SendApproval?id='+invApproval[0],
            success:function(data,textStatus){
                    $("#approvalContent").html(data);
                    $("#approvalModal").modal({
                        "backdrop" : "static",
                        "keyboard" : true,
                        "show" : true
                    }).css({'width': '500px','margin-left': function () {return -($(this).width() / 2);}});

            },
            error:function(XMLHttpRequest,textStatus,errorThrown){},
            complete:function(XMLHttpRequest,textStatus){
                $('#spinner').fadeOut();
            }
        });
    }

    printInvoice = function(param){
        var invApproval = []
        $("#generate_datatables .row-select").each(function() {
            if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			invApproval.push(id);
    		}
		});

        if(invApproval.length<1){
		    alert('Anda belum memilih no invoice yang akan dicetak');
		    return;
		}
		if(invApproval.length>1){
		    alert('Anda hanya dapat memilih 1 data untuk cetak invoice');
		    return;
		}

        window.location = "${request.contextPath}/invoicingGenerateInvoices/previewInv?idInv="+invApproval[0]+"&staPrint="+param;

    }

    previewData = function(oaram) {
		var rowSelect = []
        $("#generate_datatables .row-select").each(function() {
            if(this.checked){
                var id = $(this).next("input:hidden").val();
                rowSelect.push(id);
            }
        });

        if(rowSelect.length<1){
		    alert('Anda belum memilih no invoice yang akan dilihat');
		    return;
		}
		if(rowSelect.length>1){
		    alert('Anda hanya dapat memilih 1 data untuk lihat invoice');
		    return;
		}

        doPreview(rowSelect[0]);
	};

	doPreview = function(a) {
	    $("#invoiceListPreviewContent").empty();
	    $.ajax({
			type: 'POST',
			url: '${request.contextPath}/slipList/previewInvoice',
			data: {noInvoices:a},
	        success: function (data) {
	            $("#invoiceListPreviewContent").html(data);
	            $("#invoiceListPreviewModal").modal({
	                "backdrop": "dynamic",
	                "keyboard": true,
	                "show": true
	             }).css({'width': '1000px','margin-left': function () {return -($(this).width() / 2);}});
	        },
	        error: function () {
	            alert('Data not found');
                    return false;
	        },
	        complete: function () {
	            $('#spinner').fadeOut();
	        }
	    });
	};

    printPartslip = function(){
        var nopartslip = $("#lblnopartslip2").text();
        var invApproval = []
        $("#generate_datatables .row-select").each(function() {
                if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			invApproval.push(id);
    		}
		});

        if(invApproval.length<1){
		    alert('Anda belum memilih no invoice yang akan dicetak');
		    return;
		}
		if(invApproval.length>1){
		    alert('Anda hanya dapat memilih 1 data untuk cetak invoice');
		    return;
		}
        if(nopartslip == ""){
            alert("Nomor part slip kosong atau no invoice harus dipilih");
            return;
        }

        window.location = "${request.contextPath}/invoicingGenerateInvoices/previewPart?noPart="+nopartslip+"&idInv="+invApproval[0];
    }
    function cekPickingSlip(){
        var noWo = $('#noWo').text();
         if(noWo=="" || noWo==null){
            alert('Anda belum memasukan nomor wo / nomor polisi yang akan dibuatkan invoice');
            return
        }
        $.ajax({url: '${request.contextPath}/invoicingGenerateInvoices/cekPickingSlip?noWo='+noWo,
                type: "POST",
                success: function(data) {
                    if(data.picking==false){
                        alert("WO belum Picking Slip");
                        return ;
                    }
                    if(data.teknisi==false){
                        alert("Teknisi Untuk WO ini Belum Di Plot");
                        return ;
                    }
                    if(data.approvalSO==false){
                        alert("Ada Approval yg belum di Approve untuk Nomor WO ini");
                        return ;
                    }
                    generateInvoices();
                },
                error : function(){
                    alert('Cek Picking Slip, Internal Server Error');
                },
                complete : function (req, err) {
                    $('#spinner').fadeOut();
                }
            });
    }
    function generateInvoices(){
        var noWo = $('#noWo').text();
        if(noWo=="" || noWo==null){
            alert('Anda belum memasukan nomor wo / nomor polisi yang akan dibuatkan invoice');
            return
        }
        var cek = confirm("Apakah anda yakin akan akan melakukan proses ini?");
        if(cek){
            $('#spinner').fadeIn(1);
            toastr.info("Mohon Tunggu Sebentar, Jangan Melakukan Aktivitas Apapun di DMS Sampai Muncul Pesan di DMS","Info");
            $.ajax({url: '${request.contextPath}/invoicingGenerateInvoices/doGenerate?noWo='+noWo,
                type: "POST",
                success: function(data) {
                toastr.success("Proses Generate Selesai");
                if(data.error){
                    alert(data.error);
                }else{
                    if(data=="oke"){
                        vNoWo = noWo;
                        alert("SELAMAT, INVOICE SUKSES DIBUAT");
                        reloadGenerateTable();
                    }else if(data=="sudah"){
                        vNoWo = noWo;
                        alert('INVOICE SUDAH PERNAH DIBUAT');
                        reloadGenerateTable();
                    }else if(data=="tunggu"){
                        vNoWo = noWo;
                        alert('TUNGGU PROSES REVERSAL INVOICE');
                        reloadGenerateTable();
                    }else if(data=="jobpart"){
                        vNoWo = noWo;
                        alert('ADA APPROVAL PENGURANGAN JOB/PART YANG BELUM DI RESPON');
                        reloadGenerateTable();
                    }
                }
                },
                error : function(){
                    alert('Internal Server Error');
                },
                complete : function (req, err) {
                    $('#spinner').fadeOut();
                    loading = false;
                }
            });
        }
    }

    function generateCreditNotes(){
        var invApproval = []
        $("#generate_datatables .row-select").each(function() {
                if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			invApproval.push(id);
    		}
		});

		if(invApproval.length<1){
		    alert('Anda belum memilih data yang akan dibuatkan reversal');
		    return
		}
		if(invApproval.length>1){
		    alert('Anda hanya dapat memilih 1 data');
		    return
		}

		var cek = confirm("Apakah anda yakin akan akan melakukan proses ini?");
		if(cek){
        $('#spinner').fadeIn(1);
        $.ajax({url: '${request.contextPath}/invoicingGenerateInvoices/doGenerateCreditNote?idInv='+invApproval[0],
            type: "POST",
            success: function(data) {
                if(data=="unapproved"){
                    alert("Proses tidak dapat dilanjutkan, reversal ditolak");
                }
                if(data=="waiting"){
                    alert("Proses tidak dapat dilanjutkan, menunggu approval");
                }
                if(data=="exist"){
                    alert("Proses tidak dapat dilanjutkan, Credit Note sudah ada");
                }
                if(data=="noapproval"){
                    alert("Proses tidak dapat dilanjutkan, harap dibuat approval reversal terlebih dahulu")
                }
                if(data=="ok"){
                    alert("sukses");
                    reloadGenerateTable();
                }
            },
            error : function(){
                alert('Internal Server Error');
            },
            complete : function (req, err) {
                $('#spinner').fadeOut();
                loading = false;
            }
        });
        }

    }

    function doSave(){
        var invApproval = []
        $("#generate_datatables .row-select").each(function() {
                if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			invApproval.push(id);
    		}
		});

		if(invApproval.length<1){
		    alert('Anda belum memilih data yang akan dibuatkan reversal');
		    return
		}
		if(invApproval.length>1){
		    alert('Anda hanya dapat memilih 1 data');
		    return
		}
		var catatan = $('#catatan').val();
        $('#spinner').fadeIn(1);
        $.ajax({url: '${request.contextPath}/invoicingGenerateInvoices/doSaveCatatan?idInv='+invApproval[0]+'&catatan='+catatan,
            type: "POST",
            success: function(data) {
                if(data){
                    alert("Data berhasil disimpan.");
                }
            },
            error : function(){
                alert('Internal Server Error');
            },
            complete : function (req, err) {
                $('#spinner').fadeOut();
                loading = false;
            }
        });

    }

    function fakturpajak(){
        window.location = "${request.contextPath}/fakturPajak";
    }

        function saveAdmMaterai(from,id){
            var nama = "";
            var nilaiKirim = $("#"+from+"-"+id).val();
            if(!nilaiKirim || (nilaiKirim && nilaiKirim==0)){
                nilaiKirim = 0;
                $("#"+from+"-"+id).val(0);
            }
            if(from=="alamat"){
                nilaiKirim = "";
                nilaiKirim = $("#"+from+"-"+id).val();
            }
            $('#spinner').fadeIn(1);
            $.ajax({url: '${request.contextPath}/invoicingGenerateInvoices/doSaveAM',
                type: "POST",
                data : {idInv : id,from : from, nilai : nilaiKirim},
                success: function(data) {
                        reloadGenerateTable()
                },
                error : function(){
                    alert('Internal Server Error');
                },
                complete : function (req, err) {
                    $('#spinner').fadeOut();
                    loading = false;
                }
            });
        }

        function doWoInformation(){
            var paramNoWo = $("#noWo").text();
            if(paramNoWo==""){
                alert('Data masih kosong');
                return
            }
            window.location.href='#/WOInformation';
            $('#spinner').fadeIn(1);
            $.ajax({
                url: '${request.contextPath}/woInformation?nowo='+paramNoWo,
                type: "GET",dataType:"html",
                complete : function (req, err) {
                    $('#main-content').html(req.responseText);
                    $('#spinner').fadeOut();
                }
            });
        }

    </g:javascript>
</head>

<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
</div>
<div>&nbsp;</div>
<div class="box">

<!-- Start Tab -->
<ul class="nav nav-tabs" style="margin-bottom: 0;">
    <li><a href="javascript:loadPath('invoicingViewWO/viewWO');">View WO (Sudah JOC)</a></li>
    <li class="active"><a href="#">Generate Invoices</a></li>
</ul>
<!-- End Tab -->

<!-- Start Kriteria Search -->
<div class="box" style="padding-top: 5px; padding-left: 10px;">
    <div class="span12">
        <legend style="font-size: small;">
            <g:message code="delivery.invoicing.kriteria.search.label" default="Kriteria Search WO (Sudah JOC dan Washing)"/>
        </legend>
        <g:if test="${flash.message}">
            <div class="alert alert-error">
                ${flash.message}
            </div>
        </g:if>
    </div>
    <div class="span7" id="search-table" style="padding-left: 0;">
        <table width="95%;">
            <tr align="left">
                <td align="left">
                    <label class="control-label">
                        <g:message code="delivery.invoicing.kategori.label" default="Kategori"/>
                    </label>
                </td>
                <td align="left" colspan="3">
                    <g:select style="width:100%" name="kategoriView" id="kategoriView" from="${['NOMOR WO','NOMOR POLISI']}"/>
                </td>
                <td align="left" rowspan="2">
                    &nbsp;
                    <g:field type="button" style="width: 80px; height: 55px;" class="btn btn-primary search" onclick="showDetail()"
                             name="ok" id="ok" value="${message(code: 'default.ok.label', default: 'OK')}" />
                </td>
            </tr>
            <tr align="left">
                <td align="left">
                    <label class="control-label">
                        <g:message code="delivery.invoicing.keyWord.label" default="Kata Kunci"/>
                    </label>
                </td>
                <td align="left" colspan="3">
                    <g:textField name="kataKunci" id="kataKunci" value="${receptionInstance?.t401NoWO}" style="width:97%"/>
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>
</div>
<!-- End Kriteria Search -->

<!-- Start Invoicing -->
<div class="box" style="padding-top: 5px; padding-left: 10px;">
    <div class="span12" style="height: 40px;">
        <legend style="font-size: small;">
            <g:message code="delivery.invoicing.wo.list.label" default="Invoicing"/>
        </legend>
        <g:if test="${flash.message}">
            <div class="alert alert-error">
                ${flash.message}
            </div>
        </g:if>
    </div>
    <div class="span12" id="user-table" style="padding-left: 0; margin-left: 0;">
        <table width="95%" class="table table-bordered">
            <tr>
                <td width="8%">Tanggal WO</td>
                <td width="10%"><label id="tglWO">${receptionInstance.dateCreated?.format("dd/MM/yyyy")}</label></td>
                <td width="9%">Nomor Mesin</td>
                <td width="11%"><label id="noMesin">${receptionInstance.historyCustomerVehicle?.t183NoMesin}</label></td>
                <td width="10%">Nama Customer</td>
                <td width="14%"><label id="namCustomer">${receptionInstance.historyCustomer?.fullNama}</label></td>
                <td rowspan="3">Alamat</td>
                <td rowspan="3" width="14%"><label id="alamat">${receptionInstance.historyCustomer?.t182Alamat}</label></td>
                <td rowspan="3">
                    %{--<g:field type="button" style="width: 220px; height: 50px;" class="btn btn-primary search" onclick="generateInvoices();"--}%
                             %{--name="gipart" id="gipart"--}%
                             %{--value="${message(code: 'default.generate.label', default: 'Generate Invoice + Part Slip')}" />--}%
                    <g:field type="button" style="width: 220px; height: 50px;" class="btn btn-primary search" onclick="cekPickingSlip();"
                             name="gipart" id="gipart"
                             value="${message(code: 'default.generate.label', default: 'Generate Invoice + Part Slip')}" />
                </td>
            </tr>
            <tr>
                <td>Nomor WO</td>
                <td><label id="noWo">${receptionInstance.t401NoWO}</label></td>
                <td>Nomor Rangka</td>
                <td><label id="noRangka">${receptionInstance.historyCustomerVehicle?.customerVehicle?.t103VinCode}</label></td>
                <td>Service Advisorr</td>
                <td><label id="SA">${receptionInstance?.t401NamaSA ? User.findByUsernameAndStaDel(receptionInstance?.t401NamaSA,"0")?.fullname : "-"}</label></td>
            </tr>
            <tr>
                <td>Nomor Polisi</td>
                <td><label id="noPol">${receptionInstance.historyCustomerVehicle?.fullNoPol}</label></td>
                <td>Model</td>
                <td><label id="modelName">${receptionInstance.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel}</label></td>
                <td>Billing</td>
                <td><label id="billinit">${receptionInstance.namaManPower?.manPowerDetail?.m015Inisial}</label></td>
            </tr>
        </table>
        <br/>
        <g:field type="button" style="width: 120px" class="btn btn-primary search" onclick="window.location.href ='#/historyCustomer';"
                 name="cuspro" id="cuspro" value="${message(code: 'default.generate.label', default: 'Customer Profil')}" />
        <g:field type="button" style="width: 120px" class="btn btn-primary search" onclick="doWoInformation();"
                 name="woinfo" id="woinfo" value="${message(code: 'default.generate.label', default: 'WO Information')}" />
        <br/>
        <div class="pull-right">
            <table>
                <tr>
                    <td>
                        <table class="table table-bordered" style="width: 10%;background-color: lightpink">
                            <tr><td>&nbsp;&nbsp;</td></tr>
                        </table>
                    </td>
                    <td style="padding: 10px">Reversal Invoice</td>
                    <td style="padding: 10px">
                        <table class="table table-bordered" style="width: 10%;background-color: lightblue">
                            <tr><td>&nbsp;&nbsp;</td></tr>
                        </table>
                    </td>
                    <td>Credit Notes</td>
                </tr>
            </table>
        </div>
        <br/>
        <g:render template="geDataTables"/>
        <g:field type="button" style="width: 120px" class="btn btn-primary search" onclick="doPopDisc();"
                 name="discTable" id="discTable" value="${message(code: 'default.generate.label', default: 'Discount Information')}" />

    </div>
</div>
<!-- End Invoicing -->

<!-- jasa labor detail -->
<table style="width: 100%; border:1px; padding-left: 10px; padding-right: 10px;">
    <tr>
        <td style="padding-right: 10px; vertical-align: top; width: 420px;">
            <div class="box">
                <legend style="font-size: medium">Jasa/Labor</legend>
                <table border="0" style="font-size:9px; width: 100%; border: 1px; padding-right: 10px;">
                    <tr style="font-size:9px;">
                        <td width="35%">No. Invoice/Credit Note</td>
                        <td width="40%">: <b id="lblnoinvoice"></b></td>
                        <td width="25%" rowspan="2">
                            <g:field type="button" class="btn cancel" onclick="previewData();" style="width:88px; height: 50px;"
                                 name="preinv" id="preinv" value="${message(code: 'default.generate.label', default: 'Preview Invoice\n/Credit Notes')}" />
                        </td>
                    </tr>
                    <tr style="font-size:9px;">
                        <td>No. Part Slip</td>
                        <td>: <b id="lblnopartslip"></b></td>
                    </tr>
                    <tr>
                        <td colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <g:render template="jasalabor"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table border="1">
                                <tr>
                                    <td width="20%">Catatan</td>
                                    <td width="50%">
                                        <g:textArea name="catatan" id="catatan" style="resize:none; height: 50px;"/>
                                    </td>
                                    <td width="30%">
                                        <g:field type="button" style="width: 75px; height: 50px; margin-left: 10px;" class="btn cancel" onclick="doSave();"
                                             name="save" align="left" id="save" value="${message(code: 'default.generate.label', default: 'Save')}" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table>
                            <tr style="font-size: 9px;">
                                <td width="21%">No.Faktur Pajak</td>
                                <td width="29%">: <b id="noFakturPajak"></b></td>
                                <td width="21%">No.Nota Retur</td>
                                <td width="29%">: <b id="lblnonotaretur"></b></td>
                            </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td style="padding-right: 10px; vertical-align: top; width: 480px;">
            <div class="box">
                <legend style="font-size: medium">Part Slip Detail</legend>
                <table style="width: 100%; border: 0px;padding-left: 10px;padding-right: 10px;">
                    <tr>
                        <td>No. Part Slip : </td>
                        <td><b id="lblnopartslip2"></b></td>
                        <td align="right">
                            <g:field type="button" style="width: 98px; height: 30px;" class="btn cancel" onclick="printPartslip();"
                                     name="ppartslip" id="ppartslip" value="${message(code: 'default.generate.label', default: 'Preview Part Slip')}" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"><br></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <g:render template="tablePartdetail"/>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td style="vertical-align: top; width: 300px;">
            <div class="box">
                <legend style="font-size: medium">Status Approval Revelsal</legend>
                <table class="table table-bordered">
                    <tr style="height: 60px;">
                        <td  style="width: 70px; padding-top: 17px;">STATUS</td>
                        <td style="font-size: 14px"><b id="lblstaapproval"></b></td>
                    </tr>
                    <tr style="height: 60px;">
                        <td style="width: 70px; padding-top: 17px;">ALASAN</td>
                        <td style="font-size: 14px"><b id="lblalasanapproval"></b></td>
                    </tr>
                    <tr style="align-items: center">
                        <td colspan="2" style="align-content: center">
                            <g:field type="button" style="width: 35%; height: 60px;" class="btn cancel" onclick="doSendApproval();"
                                     name="generate" id="generate" value="${message(code: 'default.generate.label', default: 'Send Approval')}" />
                            <g:field type="button" style="width: 35%; height: 60px;" class="btn cancel" onclick="generateCreditNotes();"
                                     name="generate" id="generate" value="${message(code: 'default.generate.label', default: 'Buat Credit Note\n& Nota Retur')}" />
                        </td>
                    </tr>
                </table>
            </br>
                %{--<g:field type="button" style="width: 150px; margin-bottom: 5px" class="btn cancel" onclick=""--}%
                       %{--name="generate" id="generate" value="${message(code: 'default.generate.label', default: 'Print Invoice/part Slip/Faktur Pajak')}" />--}%
                %{--<g:field type="button" style="width: 150px; margin-bottom: 5px" class="btn cancel" onclick=""--}%
                       %{--name="generate" id="generate" value="${message(code: 'default.generate.label', default: 'Print Credit Note/Nota Retur')}" />--}%

                <g:field type="button" style="width: 35%; margin-bottom: 5px" class="btn cancel" onBlur="reloadGenerateTable();" onclick="printInvoice('print');"
                       name="generate" id="generate" value="${message(code: 'default.generate.label', default: 'Print Invoice')}" />
                <g:field type="button" style="width: 35%; margin-bottom: 5px" class="btn cancel" onclick="printPartslip();"
                       name="generate" id="generate; margin-bottom: 5px" value="${message(code: 'default.generate.label', default: 'Print Part Slip')}" />
                <g:field type="button" style="width: 80%; margin-bottom: 5px" class="btn cancel"  onclick="window.location.href ='#/FakturPajak';"
                       name="generate" id="generate" value="${message(code: 'default.generate.label', default: 'Buat Faktur Pajak')}" />

                <g:field type="button" style="width: 35%; height: 40px; margin-bottom: 5px" class="btn cancel" onclick="printInvoice()"
                       name="generate" id="generate" value="${message(code: 'default.generate.label', default: 'Print Credit\n Note')}" />
                <g:field type="button" style="width: 35%; height: 40px; margin-bottom: 5px" class="btn cancel" onclick=""
                       name="generate" id="generate" value="${message(code: 'default.generate.label', default: 'Print Nota\n Retur')}" />
            </div>
        </td>
    </tr>
</table>

<div id="discountModal" class="modal fade">
    <div class="modal-dialog" style="width: 1000px;">
        <div class="modal-content" style="width: 1000px;">
            <div class="modal-body" style="max-height: 500px;">
                <div id="discountContent"/>
                <div class="iu-content"></div>
            </div>
        </div>
    </div>
</div>

<div id="approvalModal" class="modal fade">
    <div class="modal-dialog" style="width: 500px;">
        <div class="modal-content" style="width: 500px;">
            <div class="modal-body" style="max-height: 500px;">
                <div id="approvalContent"/>
                <div class="iu-content"></div>
            </div>
        </div>
    </div>
</div>

</div>

<!-- Start Modal Preview -->
<div id="invoiceListPreviewModal" class="modal fade">
    <div class="modal-dialog" style="width: 1000px;">
        <div class="modal-content" style="width: 1000px;">
            <!-- dialog body -->
            <div class="modal-body" style="max-height: 700px;">
                <div id="invoiceListPreviewContent"></div>
                <div class="iu-content"></div>
            </div>
            <!-- dialog buttons -->
            <div class="modal-footer">
                <g:field type="button" style="width: 90px" class="btn btn-primary" name="close_modal" id="close_modal"
                         value="${message(code: 'default.close.label', default: 'Close')}" data-dismiss="modal"/>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Preview -->

</body>
</html>