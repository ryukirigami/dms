<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="disc_info" cellpadding="0" cellspacing="0" class="display table table-bordered" style="width: 920px;">

    <thead>
    <tr>
        <td rowspan="2">Pekerjaan/Jasa</td>
        <td rowspan="2">Harga</td>
        <td rowspan="2">Kategori</td>
        <td colspan="6">Discount Part &amp; Jasa</td>
    </tr>
    <tr>
        <td colspan="2">Cust Vehicle</td>
        <td colspan="2">Static</td>
        <td colspan="2">Dynamic/Campaign</td>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <td colspan="2"><b id="lblTotHarga"></b></td>
        <td colspan="3"><b id="lblTotdiscv"></b></td>
        <td colspan="2"><b id="lblTotdisstc"></b></td>
        <td colspan="2"><b id="lblTotdisdin"></b></td>
    </tr>
    </tfoot>
</table>

<g:javascript>

var reloadDiscInfoTable;
var discInfoTable;

$(function(){

	reloadDiscInfoTable = function() {
		discInfoTable.fnDraw();
	}

	discInfoTable = $('#disc_info').dataTable({

		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			var aData = discInfoTable.fnGetData(nRow);
//			console.log("masuk "+aData.staPart)
			if(aData.staPart!="tidak"){
                var staSukses = false;
                var dataTemp ;
                $.ajax({type:'POST',
                    data : aData,
                    url:'${request.contextPath}/invoicingGenerateInvoices/partList',
                    success:function(data,textStatus){
                        staSukses = true;
                        dataTemp = data;
                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){},
                    complete:function(XMLHttpRequest,textStatus){
                        if(staSukses==true){
                            var nDetailsRow = discInfoTable.fnOpen(nRow,dataTemp,'details');
                        }
                        $('#spinner').fadeOut();
                    }
                });
			}
			$('#lblTotHarga').text(aData.totalHrga);
			$('#lblTotdisstc').text(aData.totalDisStc);
			$('#lblTotdisdin').text(aData.totalDisDin);

			return nRow;
		   },
		   "fnDrawCallback":function(oSettings){
                if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
                    $(oSettings.nTableWrapper).find('.dataTables_paginate').hide();
                }
		    },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "dataTableDiscount")}",
    "aoColumns": [

{
"sName": "namaJob",
"mDataProp": "namaJob",
"aTargets": [0],
"bSearchable": true,
"bSortable": false,
"sWidth":"90px",
"bVisible": true
},
{
"sName": "harga",
"mDataProp": "harga",
"aTargets": [1],
"bSearchable": true,
"bSortable": false,
"sWidth":"25px",
"bVisible": true
},
{
"sName": "kategori",
"mDataProp": "kategori",
"aTargets": [2],
"bSearchable": true,
"bSortable": false,
"sWidth":"25px",
"bVisible": true
},
{
"sName": "cvPersen",
"mDataProp": "cvPersen",
"aTargets": [3],
"bSearchable": true,
"bSortable": false,
"sWidth":"25px",
"bVisible": true
},
{
"sName": "cvRp",
"mDataProp": "cvRp",
"aTargets": [4],
"bSearchable": true,
"bSortable": false,
"sWidth":"25px",
"bVisible": true
},
{
"sName": "staticPersen",
"mDataProp": "staticPersen",
"aTargets": [5],
"bSearchable": true,
"bSortable": false,
"sWidth":"25px",
"bVisible": true
},
{
"sName": "staticRp",
"mDataProp": "staticRp",
"aTargets": [6],
"bSearchable": true,
"bSortable": false,
"sWidth":"25px",
"bVisible": true
},
{
"sName": "dinPersen",
"mDataProp": "dinPersen",
"aTargets": [7],
"bSearchable": true,
"bSortable": false,
"sWidth":"25px",
"bVisible": true
},
{
"sName": "dinRp",
"mDataProp": "dinRp",
"aTargets": [8],
"bSearchable": true,
"bSortable": false,
"sWidth":"25px",
"bVisible": true
}
],
    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
        aoData.push(
            {"name": 'noInvoice', "value": noIn}
        )

                    $.ajax({ "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData ,
                        "success": function (json) {
                            fnCallback(json);
                           },
                        "complete": function () {
                           }
                    });
    }
});
});
</g:javascript>