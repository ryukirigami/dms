<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="jasa_labor" cellpadding="0" cellspacing="0" class="display table table-bordered" width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 2px;">Uraian Pekerjaan</th>

        <th style="border-bottom: none;padding: 2px;">Rate</th>

        <th style="border-bottom: none;padding: 2px;">Total</th>

    </tr>
    </thead>
    <tfoot align="right">
    <tr>
        <td colspan="2">SUBTOTAL</td>
        <td><span class="pull-right numeric" id="lblsubtot"></span></td>
    </tr>
    </tfoot>
</table>

<g:javascript>

var reloadJasaLabourTable;
var jasaLabourTable;

$(function(){

	reloadJasaLabourTable = function() {
		jasaLabourTable.fnDraw();
	}

	jasaLabourTable = $('#jasa_labor').dataTable({

		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
		    var aData = jasaLabourTable.fnGetData(nRow);
		    $('#lblsubtot').text(aData.subtot);
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "dataTableJasa")}",
		"aoColumns": [

{
	"sName": "operation",
	"mDataProp": "operation",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"90px",
	"bVisible": true
},
{
	"sName": "rate",
	"mDataProp": "rate",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"25px",
	"bVisible": true
},
{
	"sName": "total",
	"mDataProp": "total",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"45px",
	"mRender": function ( data, type, row ) {
        if(data != null){
        return '<span class="pull-right numeric">'+data+'</span>';
        }else{
        return '<span></span>';
        }
    },
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
               aoData.push(
                      {"name": 'noInvoice', "value": noInvo}
               )

     		   $.ajax({ "dataType": 'json',
						"type": "POST",
						"url": sSource,
						"data": aoData ,
						"success": function (json) {
							fnCallback(json);
							$('#catatan').val(json.catatan);
			    		 },
						"complete": function () {}
			   });
		}
	});
});
</g:javascript>