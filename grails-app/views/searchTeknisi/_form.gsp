<%@ page import="com.kombos.administrasi.NamaManPower" %>

<g:javascript>
    function findData(){
            var kategori = $('#kategori').val();
            var key = $('#key').val();
            if(key=="" || key ==null){
                alert('Anda belum memasukan keyword data yang akan dicari');
            }else{
                $.ajax({
                url:'${request.contextPath}/searchTeknisi/findData',
                type: "POST",
                data : { kategori: kategori, key : key },
                success : function(data){
                        idManPower = -1;
                        $('#initNama').val('');
                        $('#tglLahir').val('')
                        $('#telepon').val('')
                        $('#sertifikat').val('')
                        $('#subJenis').val('')
                        $('#aktif').val('')
                        $('#alamat').val('')
                        $('#ket').val('')
                    if(data.length>0){
                        idManPower = data[0].id;
                        $('#initNama').val(data[0].initNama);
                        $('#tglLahir').val(data[0].tglLahir)
                        $('#telepon').val(data[0].telepon)
                        $('#sertifikat').val(data[0].sertifikat)
                        $('#subJenis').val(data[0].subJenis)
                        $('#aktif').val(data[0].aktif)
                        $('#alamat').val(data[0].alamat)
                        $('#ket').val(data[0].ket)
                    }
                    reloadHistoryTable();
                },
                error: function(xhr, textStatus, errorThrown) {
                    alert('Internal Server Error');
                }
            });
            }
    }

    function clearForm(){
        $('#kategori').val('Inisial Teknisi');
        $('#key').val('');
    }

    function clearFilter(){
        $('#search_Tanggal').val('')
        $('#search_TanggalAkhir').val('')
    }

    function expand(){
        idManPower = -1;
        reloadHistoryTable();
        expandTableLayout();
    }
    
    
    var checkin = $('#search_Tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
        var newDate = new Date(ev.date)
        newDate.setDate(newDate.getDate() + 1);
        checkout.setValue(newDate);
        checkin.hide();
    }).data('datepicker');
    
    var checkout = $('#search_TanggalAkhir').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

</g:javascript>

<table style="width: 40%" >
    <tr>
        <td style="padding: 5px">
            <div class="control-group">
                <label class="control-label" for="kategori">
                    <g:message code="wo.category.search.label" default="Kategori Search" />
                </label>
                <div class="controls">
                    <g:select style="width:100%" name="kategori" id="kategori" from="${['Inisial Teknisi','Nama Teknisi']}" />
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td style="padding: 5px">
            <div class="control-group">
                <label class="control-label" for="key">
                    <g:message code="wo.cari.label" default="Data yang dicari" />
                </label>
                <div class="controls">
                    <g:textField style="width:96%" name="key" id="key" />
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="control-group">
                <g:field type="button" style="padding: 5px" class="btn cancel" onclick="findData();"
                         name="view" id="view" value="${message(code: 'default.search.label', default: 'Search')}" />
                &nbsp;&nbsp;
                <g:field type="button" style="padding: 5px" class="btn cancel" onclick="clearForm();"
                         name="clear" id="clear" value="${message(code: 'default.clear.label', default: 'Clear Search')}" />
            </div>
        </td>
    </tr>
</table>

<div class="row-fluid">
    <div id="kiri" class="span5">
        <fieldset>
            <legend style="font-size: 14px">
                <g:message code="searchTeknisi.teknisiDetail.label" default="Teknisi Detail" />
            </legend>
            <table style="width: 100%;">
                <tr>
                    <td style="padding: 5px">
                        <div class="control-group">
                            <label class="control-label" for="initNama">
                                <g:message code="searchTeknisi.inisial.label" default="Inisial - Nama Lengkap" />
                            </label>
                            <div class="controls">
                                <g:textField style="width:100%" name="initNama" id="initNama" readonly="" />
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <div class="control-group">
                            <label class="control-label" for="tglLahir">
                                <g:message code="searchTeknisi.tgllahir.label" default="Tanggal Lahir" />
                            </label>
                            <div class="controls">
                                <g:textField style="width:100%" name="tglLahir" id="tglLahir" readonly="" />
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <div class="control-group">
                            <label class="control-label" for="telepon">
                                <g:message code="searchTeknisi.telepon.label" default="Telepon" />
                            </label>
                            <div class="controls">
                                <g:textField style="width:100%" id="telepon" name="telepon" readonly=""/>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <div class="control-group">
                            <label class="control-label" for="sertifikat">
                                <g:message code="searchTeknisi.sertifikat.label" default="Sertifikat" />
                            </label>
                            <div class="controls">
                                <g:textField style="width:100%" name="sertifikat" id="sertifikat" readonly="" />
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <div class="control-group">
                            <label class="control-label" for="subJenis">
                                <g:message code="serachTeknisi.subJenis.label" default="Sub Jenis Man Power" />
                            </label>
                            <div class="controls">
                                <g:textField style="width:100%" name="subJenis" id="subJenis" readonly="" />
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <div class="control-group">
                            <label class="control-label" for="aktif">
                                <g:message code="searchTeknisi.aktif.label" default="Aktif?" />
                            </label>
                            <div class="controls">
                                <g:textField style="width:100%" name="aktif" id="aktif" readonly="" />
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <div class="control-group">
                            <label class="control-label" for="alamat">
                                <g:message code="searchTeknisi.alamat.label" default="Alamat" />
                            </label>
                            <div class="controls">
                                <g:textArea style="width:100%;resize: none" name="alamat" id="alamat" readonly="" />
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <div class="control-group">
                            <label class="control-label" for="ket">
                                <g:message code="searchTeknisi.ket.label" default="Keterangan" />
                            </label>
                            <div class="controls">
                                <g:textArea style="width:100%;resize: none" name="ket" id="ket" readonly="" />
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>

    <div id="kanan" class="span7">
        <fieldset>
            <legend style="font-size: 14px">
                <g:message code="searchTeknisi.history.label" default="History Job" />
            </legend>
            <div class="control-group">
                <label class="control-label" for="ket">
                    <g:message code="wo.tanggalWO.label" default="Tanggal WO" />
                </label>
                <div id="filter_Tanggal" class="controls">
                    <ba:datePicker id="search_Tanggal" name="search_Tanggal" precision="day" format="dd/MM/yyyy"  value=""  />
                    &nbsp;&nbsp;&nbsp;s.d.&nbsp;&nbsp;&nbsp;
                    <ba:datePicker id="search_TanggalAkhir" name="search_TanggalAkhir" precision="day" format="dd/MM/yyyy"  value=""  />
                </div>
            </div>
            <div class="control-group">
                <g:field type="button" class="btn cancel" onclick="reloadHistoryTable();" name="filter" id="filter" value="${message(code: 'default.filter.label', default: 'Filter')}" />
                &nbsp;&nbsp;
                <g:field type="button" class="btn cancel" onclick="clearFilter();" name="cfilter" id="cfilter" value="${message(code: 'default.cfilter.label', default: 'Clear Filter')}" />
            </div>

            <div class="control-group" style="width: 100%" >
                <g:render template="dataTablesHistory" />
            </div>
        </fieldset>
    </div>
</div>

<a class="pull-right box-action" style="display: block;" >
    <i>
        <g:field type="button" style="width: 100%" class="btn cancel pull-right box-action" onclick="expand();"
                 name="close" id="close" value="${message(code: 'default.close.label', default: 'Close')}" />
    </i>
</a>
