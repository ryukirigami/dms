
<%@ page import="com.kombos.administrasi.NamaManPower" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="history_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="wo.t401NoWo.label" default="Nomor WO" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="wo.tanggalWO.label" default="Tanggal WO" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="wo.namaJob.label" default="Nama Job" /></div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var historyTable;
var reloadHistoryTable;
$(function(){

	reloadHistoryTable = function() {
		historyTable.fnDraw();
	}



	historyTable = $('#history_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesHistoryList")}",
		"aoColumns": [

{
	"sName": "t401NoWO",
	"mDataProp": "t401NoWO",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"35%",
	"bVisible": true
}

,

{
	"sName": "t401TanggalWO",
	"mDataProp": "t401TanggalWO",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"25%",
	"bVisible": true
}
,

{
	"sName": "operation",
	"mDataProp": "operation",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"40%",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
        
                        var Tanggal = $('#search_Tanggal').val();
						var TanggalDay = $('#search_Tanggal_day').val();
						var TanggalMonth = $('#search_Tanggal_month').val();
						var TanggalYear = $('#search_Tanggal_year').val();
						
						if(Tanggal){
							aoData.push(
									{"name": 'sCriteria_Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_Tanggal_dp', "value": Tanggal},
									{"name": 'sCriteria_Tanggal_day', "value": TanggalDay},
									{"name": 'sCriteria_Tanggal_month', "value": TanggalMonth},
									{"name": 'sCriteria_Tanggal_year', "value": TanggalYear}
							);
						}

                        var TanggalAkhir = $('#search_TanggalAkhir').val();
                        var TanggalDayakhir = $('#search_TanggalAkhir_day').val();
                        var TanggalMonthakhir = $('#search_TanggalAkhir_month').val();
                        var TanggalYearakhir = $('#search_TanggalAkhir_year').val();
                        
                        if(TanggalAkhir){
                            aoData.push(
                                    {"name": 'sCriteria_TanggalAkhir', "value": "date.struct"},
                                    {"name": 'sCriteria_TanggalAkhir_dp', "value": TanggalAkhir},
                                    {"name": 'sCriteria_TanggalAkhir_day', "value": TanggalDayakhir},
                                    {"name": 'sCriteria_TanggalAkhir_month', "value":TanggalMonthakhir},
                                    {"name": 'sCriteria_TanggalAkhir_year', "value": TanggalYearakhir}
                            );
                        }

						aoData.push(
                                {"name": 'sCriteria_id', "value": idManPower}
                        );


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>



