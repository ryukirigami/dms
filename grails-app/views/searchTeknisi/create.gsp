<%@ page import="com.kombos.administrasi.NamaManPower" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<title><g:message code="searchTeknisi.label" default="Search Teknisi" /></title>
	</head>
	<body>
		<div id="create-searchTeknisi" class="content scaffold-create" role="main">
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${namaManPowerInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${namaManPowerInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form action="save" >--}%
			<g:formRemote class="form-horizontal" name="create" on404="alert('not found!');" update="searchTeknisi-form"
              url="[controller: 'searchTeknisi', action:'save']">
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
			</g:formRemote>
			%{--</g:form>--}%
		</div>
	</body>
</html>
