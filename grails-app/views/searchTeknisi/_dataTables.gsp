
<%@ page import="com.kombos.administrasi.NamaManPower" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="searchTeknisi_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="searchTeknisi.initial.label" default="Inisial Teknisi" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="searchTeknisi.nama.label" default="Nama Teknisi" /></div>
        </th>

    </tr>
    <tr>


        <th style="border-top: none;padding: 5px;">
            <div id="filter_initial" style="padding-top: 0px;position:relative; margin-top: 0px;width: 200px;">
                <input type="text" name="search_initial" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_nama" style="padding-top: 0px;position:relative; margin-top: 0px;width: 240px;">
                <input type="text" name="search_nama" class="search_init" />
            </div>
        </th>
    </tr>
    </thead>
</table>


<g:javascript>
var searchTeknisiTable;
var reloadSearchTeknisiTable;

$(function(){

	reloadSearchTeknisiTable = function() {
		searchTeknisiTable.fnDraw();
	    idManPower = -1;
    }


    searchTeknisiTable = $('#searchTeknisi_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "manPowerDetail",
	"mDataProp": "initial",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"50%",
	"bVisible": true
}

,

{
	"sName": "t015NamaLengkap",
	"mDataProp": "nama",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"50%",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var initial = $('#filter_initial input').val();
						if(initial){
							aoData.push(
									{"name": 'sCriteria_initial', "value": initial}
							);
						}

						var nama = $('#filter_nama input').val();
						if(nama){
							aoData.push(
									{"name": 'sCriteria_nama', "value": nama}
							);
						}


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>



