
<%@ page import="com.kombos.administrasi.NamaManPower" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<title><g:message code="searchTeknisi.label" default="Search Teknisi" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	var viewDetail;
	var idManPower = -1;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	viewDetail = function(){
        shrinkTableLayout();
            <g:remoteFunction action="create"
                              onLoading="jQuery('#spinner').fadeIn(1);"
                              onSuccess="loadForm(data, textStatus);"
                              onComplete="jQuery('#spinner').fadeOut();" />
    }

    loadForm = function(data, textStatus){
		$('#searchTeknisi-form').empty();
    	$('#searchTeknisi-form').append(data);
   	}
    
    shrinkTableLayout = function(){
        $("#searchTeknisi-table").hide();
        $("#searchTeknisi-form").css("display","block");
    }

    expandTableLayout = function(){
        try{
            reloadComplaintTable();
        }catch(e){}
        $("#searchTeknisi-table").show();
        $("#searchTeknisi-form").css("display","none");
    }
   	
});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="searchTeknisi.label" default="Search Teknisi" /></span>
		<ul class="nav pull-right">
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="searchTeknisi-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
			<g:render template="dataTables" />
            <a class="pull-right box-action" style="display: block;" >
                <i>
                    <g:field type="button" style="width: 100%" class="btn cancel pull-right box-action" onclick="viewDetail();"
                             name="detail" id="detail" value="${message(code: 'default.detail.label', default: 'View Detail')}" />
                </i>
            </a>
		</div>
		<div class="span11" id="searchTeknisi-form" style="display: none;"></div>
	</div>
</body>
</html>
