<%--
  Created by IntelliJ IDEA.
  User: Mohammad Fauzi
  Date: 3/3/14
  Time: 2:44 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>

<html>

<head>
    <meta name="layout" content="main">
    <title>MRS General Parameter</title>
    <r:require modules="baseapplayout"/>

    <g:javascript disposition="head">
        function countChars(FormatSMSService, hitung, max) {
          var count = max - document.getElementById('FormatSMSService').value.length;
          if (count < 0) {
            document.getElementById('hitung').innerHTML = "<span style=\"color: red;\">" + count + "</span>";
          }else {
            document.getElementById('hitung').innerHTML = count;
          }

          var len = document.getElementById('FormatSMSService').value.length;
          var jmlSms = parseInt(len / 150) + 1
          $('#jmlSms').text(jmlSms);
        }

        jQuery(function($) {
            $('.km').autoNumeric('init',{
                vMin:'0',
                vMax:'10000000',
                aSep:'',
                mDec: '1'
            });
            $('.hari').autoNumeric('init',{
                vMin:'0',
                vMax:'1000',
                aSep:'',
                mDec: '1'
            });
        });
        $(function () {
            if(${generalParameter?.m000StaAktifDM}=='1'){
                    $('#m000HMinusTglReminderDM').prop('disabled', false);
                    $('#m000ReminderDMOp1').prop('disabled', false);
                    $('#m000ReminderDMOp2').prop('disabled', false);
                    $('#m000ReminderDMKm1').prop('disabled', false);
                    $('#m000ReminderDMKm2').prop('disabled', false);

                }else{
                    $('#m000HMinusTglReminderDM').prop('disabled', true);
                    $('#m000HMinusTglReminderDM').val('');
                    $('#m000ReminderDMOp1').prop('disabled', true);
                    $('#m000ReminderDMOp2').prop('disabled', true);
                    $('#m000ReminderDMKm1').prop('disabled', true);
                    $('#m000ReminderDMKm1').val('');
                    $('#m000ReminderDMKm2').prop('disabled', true);
                    $('#m000ReminderDMKm2').val('');
                }
            if(${generalParameter?.m000StaAktifEmail}=='1'){
                    $('#m000JamKirimEmail_hour').prop('disabled', false);
                    $('#m000JamKirimEmail_minute').prop('disabled', false);
                    $('#m000HMinusReminderEmail').prop('disabled', false);
                    $('#m000ReminderEmailOp1').prop('disabled', false);
                    $('#m000ReminderEmailOp2').prop('disabled', false);
                    $('#m000ReminderEmailKm1').prop('disabled', false);
                    $('#m000ReminderEmailKm2').prop('disabled', false);

                }else{
                    $('#m000JamKirimEmail_hour').prop('disabled', true);
                    $('#m000JamKirimEmail_minute').prop('disabled', true);
                    $('#m000HMinusReminderEmail').prop('disabled', true);
                    $('#m000ReminderEmailOp1').prop('disabled', true);
                    $('#m000ReminderEmailOp2').prop('disabled', true);
                    $('#m000ReminderEmailKm1').prop('disabled', true);
                    $('#m000ReminderEmailKm2').prop('disabled', true);
                    $('#m000HMinusReminderEmail').val('');
                    $('#m000ReminderEmailKm1').val('');
                    $('#m000JamKirimEmail_hour').val('');
                    $('#m000JamKirimEmail_minute').val('');
                    $('#m000ReminderEmailKm2').val('');
                }
            if(${generalParameter?.m000StaAktifSMS}=='1'){
                    $('#m000JamKirimSMS_hour').prop('disabled', false);
                    $('#m000JamKirimSMS_minute').prop('disabled', false);
                    $('#m000HMinusReminderSMS').prop('disabled', false);
                    $('#m000ReminderSMSOp1').prop('disabled', false);
                    $('#m000ReminderSMSOp2').prop('disabled', false);
                    $('#m000ReminderSMSKm1').prop('disabled', false);
                    $('#m000ReminderSMSKm2').prop('disabled', false);
                    $('#m000JamKirimSMSSTNK_hour').prop('disabled', false);
                    $('#m000JamKirimSMSSTNK_minute').prop('disabled', false);
                    $('#m000HMinusReminderSTNK').prop('disabled', false);

                }else{
                    $('#m000JamKirimSMS_hour').prop('disabled', true);
                    $('#m000JamKirimSMS_minute').prop('disabled', true);
                    $('#m000JamKirimSMS_hour').val('');
                    $('#m000JamKirimSMS_minute').val('');
                    $('#m000HMinusReminderSMS').prop('disabled', true);
                    $('#m000HMinusReminderSMS').val('');
                    $('#m000ReminderSMSOp1').prop('disabled', true);
                    $('#m000ReminderSMSOp2').prop('disabled', true);
                    $('#m000ReminderSMSKm1').prop('disabled', true);
                    $('#m000ReminderSMSKm2').prop('disabled', true);
                    $('#m000ReminderSMSKm1').val('');
                    $('#m000ReminderSMSKm2').val('');
                    $('#m000JamKirimSMSSTNK_hour').prop('disabled', true);
                    $('#m000JamKirimSMSSTNK_minute').prop('disabled', true);
                    $('#m000HMinusReminderSTNK').prop('disabled', true);
                    $('#m000JamKirimSMSSTNK_hour').val('');
                    $('#m000JamKirimSMSSTNK_minute').val('');
                    $('#m000HMinusReminderSTNK').val('');
                }
            if(${generalParameter?.m000StaAktifCall}=='1'){
                    $('#m000HMinusReminderCall').prop('disabled', false);
                }else{
                    $('#m000HMinusReminderCall').prop('disabled', true);
                    $('#m000HMinusReminderCall').val('');
            }
            $('#m000StaAktifDM').change(function(){
                if($('#m000StaAktifDM').val()=='1'){
                    $('#m000HMinusTglReminderDM').prop('disabled', false);
                    $('#m000ReminderDMOp1').prop('disabled', false);
                    $('#m000ReminderDMOp2').prop('disabled', false);
                    $('#m000ReminderDMKm1').prop('disabled', false);
                    $('#m000ReminderDMKm2').prop('disabled', false);
                    $('#m000HMinusTglReminderDM').val('0');
                    $('#m000ReminderDMKm1').val('0');
                    $('#m000ReminderDMKm2').val('0');
                }else{
                    $('#m000HMinusTglReminderDM').prop('disabled', true);
                    $('#m000ReminderDMOp1').prop('disabled', true);
                    $('#m000ReminderDMOp2').prop('disabled', true);
                    $('#m000ReminderDMKm1').prop('disabled', true);
                    $('#m000ReminderDMKm2').prop('disabled', true);
                    $('#m000HMinusTglReminderDM').val('');
                    $('#m000ReminderDMKm1').val('');
                    $('#m000ReminderDMKm2').val('');
                }
            });
            $('#m000StaAktifEmail').change(function(){
                if($('#m000StaAktifEmail').val()=='1'){
                    $('#m000JamKirimEmail_hour').prop('disabled', false);
                    $('#m000JamKirimEmail_minute').prop('disabled', false);
                    $('#m000HMinusReminderEmail').prop('disabled', false);
                    $('#m000ReminderEmailOp1').prop('disabled', false);
                    $('#m000ReminderEmailOp2').prop('disabled', false);
                    $('#m000ReminderEmailKm1').prop('disabled', false);
                    $('#m000ReminderEmailKm2').prop('disabled', false);
                    $('#m000JamKirimEmail_hour').val('0');
                    $('#m000JamKirimEmail_minute').val('0');
                    $('#m000HMinusReminderEmail').val('0');
                    $('#m000ReminderEmailKm1').val('0');
                    $('#m000ReminderEmailKm2').val('0');
                }else{
                    $('#m000JamKirimEmail_hour').prop('disabled', true);
                    $('#m000HMinusReminderEmail').prop('disabled', true);
                    $('#m000ReminderEmailOp1').prop('disabled', true);
                    $('#m000ReminderEmailOp2').prop('disabled', true);
                    $('#m000ReminderEmailKm1').prop('disabled', true);
                    $('#m000JamKirimEmail_minute').prop('disabled', true);
                    $('#m000ReminderEmailKm2').prop('disabled', true);
                    $('#m000JamKirimEmail_hour').val('');
                    $('#m000JamKirimEmail_minute').val('');
                    $('#m000HMinusReminderEmail').val('');
                    $('#m000ReminderEmailKm1').val('');
                    $('#m000ReminderEmailKm2').val('');
                }
            });
            $('#m000StaAktifSMS').change(function(){
                if($('#m000StaAktifSMS').val()=='1'){
                    $('#m000JamKirimSMS_hour').prop('disabled', false);
                    $('#m000JamKirimSMS_minute').prop('disabled', false);
                    $('#m000HMinusReminderSMS').prop('disabled', false);
                    $('#m000ReminderSMSOp1').prop('disabled', false);
                    $('#m000ReminderSMSOp2').prop('disabled', false);
                    $('#m000ReminderSMSKm1').prop('disabled', false);
                    $('#m000ReminderSMSKm2').prop('disabled', false);
                    $('#m000JamKirimSMSSTNK_hour').prop('disabled', false);
                    $('#m000JamKirimSMSSTNK_minute').prop('disabled', false);
                    $('#m000HMinusReminderSTNK').prop('disabled', false);
                    $('#m000JamKirimSMS_hour').val('0');
                    $('#m000JamKirimSMS_minute').val('0');
                    $('#m000ReminderSMSKm1').val('0');
                    $('#m000ReminderSMSKm2').val('0');
                    $('#m000JamKirimSMSSTNK_hour').val('0');
                    $('#m000JamKirimSMSSTNK_minute').val('0');
                    $('#m000HMinusReminderSTNK').val('0');
                }else{
                    $('#m000JamKirimSMS_hour').prop('disabled', true);
                    $('#m000JamKirimSMS_minute').prop('disabled', true);
                    $('#m000JamKirimSMS_hour').val('');
                    $('#m000JamKirimSMS_minute').val('');
                    $('#m000HMinusReminderSMS').prop('disabled', true);
                    $('#m000HMinusReminderSMS').val('');
                    $('#m000ReminderSMSOp1').prop('disabled', true);
                    $('#m000ReminderSMSOp2').prop('disabled', true);
                    $('#m000ReminderSMSKm1').prop('disabled', true);
                    $('#m000ReminderSMSKm2').prop('disabled', true);
                    $('#m000ReminderSMSKm1').val('');
                    $('#m000ReminderSMSKm2').val('');
                    $('#m000JamKirimSMSSTNK_hour').prop('disabled', true);
                    $('#m000JamKirimSMSSTNK_minute').prop('disabled', true);
                    $('#m000HMinusReminderSTNK').prop('disabled', true);
                    $('#m000JamKirimSMSSTNK_hour').val('');
                    $('#m000JamKirimSMSSTNK_minute').val('');
                    $('#m000HMinusReminderSTNK').val('');
                }
            });
            $('#m000StaAktifCall').change(function(){
                if($('#m000StaAktifCall').val()=='1'){
                    $('#m000HMinusReminderCall').prop('disabled', false);
                    $('#m000HMinusReminderCall').val('0');
                }else{
                    $('#m000HMinusReminderCall').prop('disabled', true);
                    $('#m000HMinusReminderCall').val('');
                }
            });

            jQuery("#buttonSave").click(function (event) {
                var a = confirm('Anda yakin data akan disimpan?')
                if(a){
                    jQuery('#spinner').fadeIn(1);
                    $(this).submit();
                    jQuery('#spinner').fadeOut();
                    event.preventDefault();
                    alert('Save success');
                    window.location.replace('#/home');
                }
            });

            jQuery("#buttonClose").click(function (event) {
                event.preventDefault(); //to stop the default loading
            });

            completeProcess = function () {

            }

            doResponse = function () {
                    alert('Save success');
                    window.location.replace('#/home');
            }

        });
    </g:javascript>
</head>

<body>
<g:formRemote name="form" id="form" on404="alert('not found!')" update="updateMe"
              method="post"
              onSuccess="doResponse();"
              url="[controller: 'mrsGeneralParameter', action: 'doSave']">
<g:hiddenField name="id" value="${generalParameter?.id}"/>
<fieldset class="form">
<table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">
<tr>
<td style="width: 50%;vertical-align: top;">

<div class="box">
    Parameter yang berlaku untuk template pesan MRS:<br><br>
    <table style="width: 100%">
        <tr>
            <td style="width: 30%">&lt;WorkshopTAM&gt;</td>
            <td style="width: 5%">=</td>
            <td>Nama workshop Toyota Astra Motor yang menggunakan aplikasi ini</td>
        </tr>
        <tr>
            <td style="width: 30%">&lt;JenisSB&gt;</td>
            <td style="width: 5%">=</td>
            <td>Jenis Service Berkala</td>
        </tr>
        <tr>
            <td style="width: 30%">&lt;Kendaraan&gt;</td>
            <td style="width: 5%">=</td>
            <td>Model kendaraan customer</td>
        </tr>
        <tr>
            <td style="width: 30%">&lt;TanggalService&gt;</td>
            <td style="width: 5%">=</td>
            <td>Tanggal service</td>
        </tr>
        <tr>
            <td style="width: 30%">&lt;NamaCustomer&gt;</td>
            <td style="width: 5%">=</td>
            <td>Nama Customer</td>
        </tr>
        <tr>
            <td style="width: 30%">&lt;UsiaCustomer&gt;</td>
            <td style="width: 5%">=</td>
            <td>Usia customer</td>
        </tr>
        <tr>
            <td style="width: 30%">&lt;NamaHariIni&gt;</td>
            <td style="width: 5%">=</td>
            <td>Nama hari ini</td>
        </tr>
        <tr>
            <td style="width: 30%">&lt;TanggalHariIni&gt;</td>
            <td style="width: 5%">=</td>
            <td>Tanggal hari ini</td>
        </tr>
        <tr>
            <td style="width: 30%">&lt;Nopol&gt;</td>
            <td style="width: 5%">=</td>
            <td>Nomor polisi kendaraan customer</td>
        </tr>
        <tr>
            <td style="width: 30%">&lt;Waktu&gt;</td>
            <td style="width: 5%">=</td>
            <td>Lamanya waktu kendaraan customer akan diservice dalam satuan hari</td>
        </tr>
        <tr>
            <td style="width: 30%">&lt;Km&gt;</td>
            <td style="width: 5%">=</td>
            <td>Data estimasi Km pada kendaraan customer</td>
        </tr>
        <tr>
            <td style="width: 30%">&lt;Hobby&gt;</td>
            <td style="width: 5%">=</td>
            <td>Hobby Customer</td>
        </tr>
        <tr>
            <td style="width: 30%">&lt;TanggalDEC&gt;</td>
            <td style="width: 5%">=</td>
            <td>Tanggal DEC</td>
        </tr>
    </table>
</div>

<div class="box">

    <table style="width: 100%;border: 0px">
        <tr>
            <td>
                <label class="control-label" for="m000StaAktifDM">
                    Aktifkan Reminder DM ?
                    <span class="required-indicator">*</span>
                </label>
            </td>
            <td>
                <g:select name="m000StaAktifDM" id="m000StaAktifDM" style="width:70px"
                          from="${[[id: "1", value: "Ya"], [id: "0", value: "Tidak"]]}"
                          optionValue="value"
                          optionKey="id" required="required" value="${generalParameter?.m000StaAktifDM}"
                          class="many-to-one"/>
            </td>
        </tr>
        <tr>
            <td>
                <label class="control-label" for="m000HMinusTglReminderDM">
                    H- Reminder DM Service
                    <span class="required-indicator">*</span>
                </label>
            </td>
            <td>
                <g:textField required="required" style="width:50px" name="m000HMinusTglReminderDM" id="m000HMinusTglReminderDM"
                             class="hari"
                             value="${generalParameter?.m000HMinusTglReminderDM}"/>
                hari sebelum tanggal jatuh tempo service
            </td>
        </tr>

        <tr>
            <td>
                <label class="control-label" for="m000ReminderDMOp1">
                    Km Reminder DM
                    Service
                    <span class="required-indicator">*</span>
                </label>
            </td>
            <td>
                <g:select from="${["<", ">", "<=", ">="]}"
                          style="width:50px"
                          required="required" name="m000ReminderDMOp1" id="m000ReminderDMOp1"
                          value="${generalParameter?.m000ReminderDMOp1}"/>
                <g:textField required="required" name="m000ReminderDMKm1" id="m000ReminderDMKm1" style="width:100px" class="km"
                             value="${generalParameter?.m000ReminderDMKm1}"/>
                dan
                <g:select from="${["<", ">", "<=", ">="]}"
                          style="width:50px"
                          required="required" name="m000ReminderDMOp2" id="m000ReminderDMOp2"
                          value="${generalParameter?.m000ReminderDMOp2}"/>
                <g:textField required="required" name="m000ReminderDMKm2" id="m000ReminderDMKm2" style="width:100px" class="km"
                             value="${generalParameter?.m000ReminderDMKm2}"/>
            </td>
        </tr>

    </table>

</div>


<div class="box">

    <table style="width: 100%;border: 0px">
        <tr>
            <td>
                <label class="control-label" for="m000StaAktifEmail">
                    Aktifkan Reminder Email ?
                    <span class="required-indicator">*</span>
                </label>
            </td>
            <td>
                <g:select name="m000StaAktifEmail" id="m000StaAktifEmail" style="width:70px"
                          from="${[[id: "1", value: "Ya"], [id: "0", value: "Tidak"]]}"
                          optionValue="value"
                          optionKey="id" required="required" value="${generalParameter?.m000StaAktifEmail}"
                          class="many-to-one"/>
            </td>
        </tr>
        <tr>
            <td>
                <label class="control-label" for="m000JamKirimEmail_hour">
                    Jam Kirim Email
                    Service
                    <span class="required-indicator">*</span>
                </label>
            </td>
            <td>
                <g:hiddenField name="m000JamKirimEmail_year" value="0"/>
                <g:hiddenField name="m000JamKirimEmail_month" value="0"/>
                <g:hiddenField name="m000JamKirimEmail_day" value="0"/>

                <select id="m000JamKirimEmail_hour" name="m000JamKirimEmail_hour" style="width: 60px" required="required">
                    <g:each var="i" in="${(0..<24)}">
                        <g:if test="${i < 10}">
                            <g:if test="${i == generalParameter?.m000JamKirimEmail?.getHours()}">
                                <option value="${i}" selected="">0${i}</option>
                            </g:if>
                            <g:else>
                                <option value="${i}">0${i}</option>
                            </g:else>
                        </g:if>
                        <g:else>
                            <g:if test="${i == generalParameter?.m000JamKirimEmail?.getHours()}">
                                <option value="${i}" selected="">${i}</option>
                            </g:if>
                            <g:else>
                                <option value="${i}">${i}</option>
                            </g:else>
                        </g:else>
                    </g:each>
                </select> H :
                <select id="m000JamKirimEmail_minute" name="m000JamKirimEmail_minute" style="width: 60px" required="required">
                    <g:each var="i" in="${(0..<60)}">
                        <g:if test="${i < 10}">
                            <g:if test="${i == generalParameter?.m000JamKirimEmail?.getMinutes()}">
                                <option value="${i}" selected="">0${i}</option>
                            </g:if>
                            <g:else>
                                <option value="${i}">0${i}</option>
                            </g:else>
                        </g:if>
                        <g:else>
                            <g:if test="${i == generalParameter?.m000JamKirimEmail?.getMinutes()}">
                                <option value="${i}" selected="">${i}</option>
                            </g:if>
                            <g:else>
                                <option value="${i}">${i}</option>
                            </g:else>
                        </g:else>
                    </g:each>
                </select> m

            </td>
        </tr>
        <tr>
            <td>
                <label class="control-label" for="m000HMinusReminderEmail">
                    H- Reminder Email Service
                    <span class="required-indicator">*</span>
                </label>
            </td>
            <td>
                <g:textField required="required" style="width:50px" name="m000HMinusReminderEmail" id="m000HMinusReminderEmail"
                             class="hari"
                             value="${generalParameter?.m000HMinusReminderEmail}"/>
                hari sebelum tanggal jatuh tempo service
            </td>
        </tr>

        <tr>
            <td>
                <label class="control-label" for="m000ReminderEmailOp1">
                    Km Reminder Email
                    Service
                    <span class="required-indicator">*</span>
                </label>
            </td>
            <td>
                <g:select from="${["<", ">", "<=", ">="]}"
                          style="width:50px"
                          required="required" name="m000ReminderEmailOp1" id="m000ReminderEmailOp1"
                          value="${generalParameter?.m000ReminderEmailOp1}"/>
                <g:textField required="required" name="m000ReminderEmailKm1" id="m000ReminderEmailKm1" style="width:100px" class="km"
                             value="${generalParameter?.m000ReminderEmailKm1}"/>
                dan
                <g:select from="${["<", ">", "<=", ">="]}"
                          style="width:50px"
                          required="required" name="m000ReminderEmailOp2" id="m000ReminderEmailOp2"
                          value="${generalParameter?.m000ReminderEmailOp2}"/>
                <g:textField required="required" name="m000ReminderEmailKm2" id="m000ReminderEmailKm2" style="width:100px" class="km"
                             value="${generalParameter?.m000ReminderEmailKm2}"/>
            </td>
        </tr>

    </table>

</div>











<div class="box">

    <table style="width: 100%;border: 0px">
        <tr>
            <td>
                <label class="control-label" for="m000StaAktifSMS">
                    Aktifkan Reminder SMS ?
                    <span class="required-indicator">*</span>
                </label>
            </td>
            <td>
                <g:select name="m000StaAktifSMS" id="m000StaAktifSMS" style="width:70px"
                          from="${[[id: "1", value: "Ya"], [id: "0", value: "Tidak"]]}"
                          optionValue="value"
                          optionKey="id" required="required" value="${generalParameter?.m000StaAktifSMS}"
                          class="many-to-one"/>
            </td>
        </tr>
        <tr>
            <td>
                <label class="control-label" for="m000JamKirimSMS_hour">
                    Jam Kirim SMS
                    Service
                    <span class="required-indicator">*</span>
                </label>
            </td>
            <td>
                <g:hiddenField name="m000JamKirimSMS_year" value="0"/>
                <g:hiddenField name="m000JamKirimSMS_month" value="0"/>
                <g:hiddenField name="m000JamKirimSMS_day" value="0"/>

                <select id="m000JamKirimSMS_hour" name="m000JamKirimSMS_hour" style="width: 60px" required="required">
                    <g:each var="i" in="${(0..<24)}">
                        <g:if test="${i < 10}">
                            <g:if test="${i == generalParameter?.m000JamKirimSMS?.getHours()}">
                                <option value="${i}" selected="">0${i}</option>
                            </g:if>
                            <g:else>
                                <option value="${i}">0${i}</option>
                            </g:else>
                        </g:if>
                        <g:else>
                            <g:if test="${i == generalParameter?.m000JamKirimSMS?.getHours()}">
                                <option value="${i}" selected="">${i}</option>
                            </g:if>
                            <g:else>
                                <option value="${i}">${i}</option>
                            </g:else>
                        </g:else>
                    </g:each>
                </select> H :
                <select id="m000JamKirimSMS_minute" name="m000JamKirimSMS_minute" style="width: 60px" required="required">
                    <g:each var="i" in="${(0..<60)}">
                        <g:if test="${i < 10}">
                            <g:if test="${i == generalParameter?.m000JamKirimSMS?.getMinutes()}">
                                <option value="${i}" selected="">0${i}</option>
                            </g:if>
                            <g:else>
                                <option value="${i}">0${i}</option>
                            </g:else>
                        </g:if>
                        <g:else>
                            <g:if test="${i == generalParameter?.m000JamKirimSMS?.getMinutes()}">
                                <option value="${i}" selected="">${i}</option>
                            </g:if>
                            <g:else>
                                <option value="${i}">${i}</option>
                            </g:else>
                        </g:else>
                    </g:each>
                </select> m

            </td>
        </tr>
        <tr>
            <td>
                <label class="control-label" for="m000HMinusReminderSMS">
                    H- Reminder SMS Service
                    <span class="required-indicator">*</span>
                </label>
            </td>
            <td>
                <g:textField required="required" style="width:50px" name="m000HMinusReminderSMS" id="m000HMinusReminderSMS"
                             class="hari"
                             value="${generalParameter?.m000HMinusReminderSMS}"/>
                hari sebelum tanggal jatuh tempo service
            </td>
        </tr>

        <tr>
            <td>
                <label class="control-label" for="m000ReminderSMSOp1">
                    Km Reminder SMS
                    Service
                    <span class="required-indicator">*</span>
                </label>
            </td>
            <td>
                <g:select from="${["<", ">", "<=", ">="]}"
                          style="width:50px"
                          required="required" name="m000ReminderSMSOp1" id="m000ReminderSMSOp1"
                          value="${generalParameter?.m000ReminderSMSOp1}"/>
                <g:textField required="required" name="m000ReminderSMSKm1" id="m000ReminderSMSKm1" style="width:100px" class="km"
                             value="${generalParameter?.m000ReminderSMSKm1}"/>
                dan
                <g:select from="${["<", ">", "<=", ">="]}"
                          style="width:50px"
                          required="required" name="m000ReminderSMSOp2" id="m000ReminderSMSOp2"
                          value="${generalParameter?.m000ReminderSMSOp2}"/>
                <g:textField required="required" name="m000ReminderSMSKm2" id="m000ReminderSMSKm2" style="width:100px" class="km"
                             value="${generalParameter?.m000ReminderSMSKm2}"/>
            </td>
        </tr>





        <tr>
            <td>
                <label class="control-label" for="m000JamKirimSMSSTNK_hour">
                    Jam Kirim SMS
                    Jatuh Tempo
                    STNK
                    <span class="required-indicator">*</span>
                </label>
            </td>
            <td>
                <g:hiddenField name="m000JamKirimSMSSTNK_year" value="0"/>
                <g:hiddenField name="m000JamKirimSMSSTNK_month" value="0"/>
                <g:hiddenField name="m000JamKirimSMSSTNK_day" value="0"/>

                <select id="m000JamKirimSMSSTNK_hour" name="m000JamKirimSMSSTNK_hour" style="width: 60px" required="required">
                    <g:each var="i" in="${(0..<24)}">
                        <g:if test="${i < 10}">
                            <g:if test="${i == generalParameter?.m000JamKirimSMSSTNK?.getHours()}">
                                <option value="${i}" selected="">0${i}</option>
                            </g:if>
                            <g:else>
                                <option value="${i}">0${i}</option>
                            </g:else>
                        </g:if>
                        <g:else>
                            <g:if test="${i == generalParameter?.m000JamKirimSMSSTNK?.getHours()}">
                                <option value="${i}" selected="">${i}</option>
                            </g:if>
                            <g:else>
                                <option value="${i}">${i}</option>
                            </g:else>
                        </g:else>
                    </g:each>
                </select> H :
                <select id="m000JamKirimSMSSTNK_minute" name="m000JamKirimSMSSTNK_minute" style="width: 60px" required="required">
                    <g:each var="i" in="${(0..<60)}">
                        <g:if test="${i < 10}">
                            <g:if test="${i == generalParameter?.m000JamKirimSMSSTNK?.getMinutes()}">
                                <option value="${i}" selected="">0${i}</option>
                            </g:if>
                            <g:else>
                                <option value="${i}">0${i}</option>
                            </g:else>
                        </g:if>
                        <g:else>
                            <g:if test="${i == generalParameter?.m000JamKirimSMSSTNK?.getMinutes()}">
                                <option value="${i}" selected="">${i}</option>
                            </g:if>
                            <g:else>
                                <option value="${i}">${i}</option>
                            </g:else>
                        </g:else>
                    </g:each>
                </select> m

            </td>
        </tr>

        <tr>
            <td>
                <label class="control-label" for="m000HMinusReminderSTNK">
                    H- Reminder SMS
                    Jatuh Tempo
                    STNK
                    <span class="required-indicator">*</span>
                </label>
            </td>
            <td>
                <g:textField required="required" style="width:50px" name="m000HMinusReminderSTNK" id="m000HMinusReminderSTNK" class="hari"
                             value="${generalParameter?.m000HMinusReminderSTNK}"/>
                hari sebelum tanggal jatuh tempo STNK
            </td>
        </tr>

    </table>

</div>








</td>
<td style="width: 50%;vertical-align: top;padding-left: 10px;">
    <div class="box">

        <label class="control-label" for="m000FormatSMSService">
            Format SMS Service
            <span class="required-indicator">*</span>
        </label>

        <div class="controls">
            <g:textArea style="width: 100%;height: 100px" name="m000FormatSMSService" id="FormatSMSService" maxlength="500" required="required" onFocus="countChars('FormatSMSService','hitung',500)"
                        onKeyDown="countChars('FormatSMSService','hitung',500)" onKeyUp="countChars('FormatSMSService','hitung',500)" value="${generalParameter?.m000FormatSMSService}"/>
            <br/>
            <span id="hitung">255</span> Karakter Tersisa.<br>
            Jumlah SMS : <span id="jmlSms">1</span>
        </div>
    </div>

    <div class="box">
        <label class="control-label" for="m000FormatSMSService">
            Format Email Service
            <span class="required-indicator">*</span>
        </label>

        <div class="controls">
            <g:textArea style="width: 100%;height: 300px" name="m000FormatEmail" required="required"
                        value="${generalParameter?.m000FormatEmail}"/>
        </div>
    </div>


    <div class="box">
        <label class="control-label" for="m000TextCatatanDM">
            Catatan untuk
            cetakan blanko
            DM
            <span class="required-indicator">*</span>
        </label>

        <div class="controls">
            <g:textArea style="width: 100%;height: 100px" name="m000TextCatatanDM" required="required"
                        value="${generalParameter?.m000TextCatatanDM}"/>
        </div>
    </div>



    <div class="box">

        <table style="width: 100%;border: 0px">
            <tr>
                <td>
                    <label class="control-label" for="m000HPlusUndanganFA">
                        H+ Undangan Field Action
                        <span class="required-indicator">*</span>
                    </label>
                </td>
                <td>
                    <g:textField required="required" style="width:50px" name="m000HPlusUndanganFA" class="hari"
                                 value="${generalParameter?.m000HPlusUndanganFA}"/>
                    hari setelah Part diterima di stock (Stock-In)
                </td>
            </tr>
            <tr>
                <td>
                    <label class="control-label" for="m000JedaWaktuTelpCust_hour">
                        Jeda Waktu
                        telepon Customer
                        <span class="required-indicator">*</span>
                    </label>
                </td>
                <td>
                    <g:hiddenField name="m000JedaWaktuTelpCust_year" value="0"/>
                    <g:hiddenField name="m000JedaWaktuTelpCust_month" value="0"/>
                    <g:hiddenField name="m000JedaWaktuTelpCust_day" value="0"/>

                    <select id="m000JedaWaktuTelpCust_hour" name="m000JedaWaktuTelpCust_hour" style="width: 60px" required="required">
                        <g:each var="i" in="${(0..<24)}">
                            <g:if test="${i < 10}">
                                <g:if test="${i == generalParameter?.m000JedaWaktuTelpCust?.getHours()}">
                                    <option value="${i}" selected="">0${i}</option>
                                </g:if>
                                <g:else>
                                    <option value="${i}">0${i}</option>
                                </g:else>
                            </g:if>
                            <g:else>
                                <g:if test="${i == generalParameter?.m000JedaWaktuTelpCust?.getHours()}">
                                    <option value="${i}" selected="">${i}</option>
                                </g:if>
                                <g:else>
                                    <option value="${i}">${i}</option>
                                </g:else>
                            </g:else>
                        </g:each>
                    </select> H :
                    <select id="m000JedaWaktuTelpCust_minute" name="m000JedaWaktuTelpCust_minute" style="width: 60px" required="required">
                        <g:each var="i" in="${(0..<60)}">
                            <g:if test="${i < 10}">
                                <g:if test="${i == generalParameter?.m000JedaWaktuTelpCust?.getMinutes()}">
                                    <option value="${i}" selected="">0${i}</option>
                                </g:if>
                                <g:else>
                                    <option value="${i}">0${i}</option>
                                </g:else>
                            </g:if>
                            <g:else>
                                <g:if test="${i == generalParameter?.m000JedaWaktuTelpCust?.getMinutes()}">
                                    <option value="${i}" selected="">${i}</option>
                                </g:if>
                                <g:else>
                                    <option value="${i}">${i}</option>
                                </g:else>
                            </g:else>
                        </g:each>
                    </select> m

                </td>
            </tr>
            <tr>
                <td>
                    <label class="control-label" for="m000StaAktifCall">
                        Aktifkan Reminder Call ?
                        <span class="required-indicator">*</span>
                    </label>
                </td>
                <td>
                    <g:select name="m000StaAktifCall" id="m000StaAktifCall" style="width:70px"
                              from="${[[id: "1", value: "Ya"], [id: "0", value: "Tidak"]]}"
                              optionValue="value"
                              optionKey="id" required="required" value="${generalParameter?.m000StaAktifCall}"
                              class="many-to-one"/>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="control-label" for="m000HMinusReminderCall">
                        H- Reminder Call Service
                        <span class="required-indicator">*</span>
                    </label>
                </td>
                <td>
                    <g:textField required="required" style="width:50px" name="m000HMinusReminderCall" id="m000HMinusReminderCall"
                                 class="hari"
                                 value="${generalParameter?.m000HMinusReminderCall}"/>
                    hari sebelum tanggal jatuh tempo service
                </td>
            </tr>


        </table>

    </div>


</td>
</tr>
</table>
</fieldset>


<fieldset class="buttons controls">

    %{--<button class="btn btn-primary" id="buttonEdit" onclick="onEdit()">Edit</button>--}%
    <g:submitButton class="btn btn-primary create" name="save" value="Save" id="save"
                    onclick="return confirm('${message(code: 'default.button.save.confirm.message', default: 'Anda yakin data akan disimpan?')}');"/>
    %{--<button class="btn btn-primary" id="buttonCancel" onclick="onCancel()">Cancel</button>--}%

    <button class="btn btn-primary" id="buttonClose" onclick="window.location.replace('#/home');">Close</button>

</fieldset>
</g:formRemote>

</body>
</html>