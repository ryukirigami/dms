<%@ page import="com.kombos.parts.ETA" %>



<div class="control-group fieldcontain ${hasErrors(bean: ETAInstance, field: 'po', 'error')} ">
	<label class="control-label" for="po">
		<g:message code="ETA.po.label" default="Po" />
		
	</label>
	<div class="controls">
	<g:select id="po" name="po.id" from="${com.kombos.parts.PO.list()}" optionKey="id" value="${ETAInstance?.po?.id}" class="many-to-one" noSelection="['null': '']"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: ETAInstance, field: 'goods', 'error')} ">
	<label class="control-label" for="goods">
		<g:message code="ETA.goods.label" default="Goods" />
		
	</label>
	<div class="controls">
	<g:select id="goods" name="goods.id" from="${com.kombos.parts.Goods.list()}" optionKey="id" value="${ETAInstance?.goods?.id}" class="many-to-one" noSelection="['null': '']"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: ETAInstance, field: 't165Qty1', 'error')} ">
	<label class="control-label" for="t165Qty1">
		<g:message code="ETA.t165Qty1.label" default="T165 Qty1" />
		
	</label>
	<div class="controls">
	<g:field name="t165Qty1" value="${fieldValue(bean: ETAInstance, field: 't165Qty1')}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: ETAInstance, field: 't165ETA', 'error')} ">
	<label class="control-label" for="t165ETA">
		<g:message code="ETA.t165ETA.label" default="T165 ETA" />
		
	</label>
	<div class="controls">
	<g:datePicker name="t165ETA" precision="day"  value="${ETAInstance?.t165ETA}" default="none" noSelection="['': '']" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: ETAInstance, field: 't165xNamaUser', 'error')} ">
	<label class="control-label" for="t165xNamaUser">
		<g:message code="ETA.t165xNamaUser.label" default="T165x Nama User" />
		
	</label>
	<div class="controls">
	<g:textField name="t165xNamaUser" maxlength="20" value="${ETAInstance?.t165xNamaUser}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: ETAInstance, field: 't165xNamaDivisi', 'error')} ">
	<label class="control-label" for="t165xNamaDivisi">
		<g:message code="ETA.t165xNamaDivisi.label" default="T165x Nama Divisi" />
		
	</label>
	<div class="controls">
	<g:textField name="t165xNamaDivisi" maxlength="20" value="${ETAInstance?.t165xNamaDivisi}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: ETAInstance, field: 'staDel', 'error')} ">
	<label class="control-label" for="staDel">
		<g:message code="ETA.staDel.label" default="Sta Del" />
		
	</label>
	<div class="controls">
	<g:textField name="staDel" maxlength="1" value="${ETAInstance?.staDel}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: ETAInstance, field: 'createdBy', 'error')} ">
	<label class="control-label" for="createdBy">
		<g:message code="ETA.createdBy.label" default="Created By" />
		
	</label>
	<div class="controls">
	<g:textField name="createdBy" value="${ETAInstance?.createdBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: ETAInstance, field: 'updatedBy', 'error')} ">
	<label class="control-label" for="updatedBy">
		<g:message code="ETA.updatedBy.label" default="Updated By" />
		
	</label>
	<div class="controls">
	<g:textField name="updatedBy" value="${ETAInstance?.updatedBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: ETAInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="ETA.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${ETAInstance?.lastUpdProcess}"/>
	</div>
</div>

