
<%@ page import="com.kombos.parts.ETA" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="ETA_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="ETA.t165ETA.label" default="ETA" /></div>
            </th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="ETA.po.kode_vendor" default="Kode Vendor" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="ETA.po.nama_vendor" default="Nama Vendor" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="ETA.po.kode_part" default="Kode Part" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="ETA.po.nama_part" default="Nama Part" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="ETA.po.tanggal_po" default="Tanggal PO" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="ETA.po.no_po" default="No PO" /></div>
            </th>
		</tr>
	</thead>
</table>

<g:javascript>
var ETATable;
var reloadETATable;
$(function(){
	
	reloadETATable = function() {
		ETATable.fnDraw();
	}

	
	$('#search_t165ETA').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t165ETA_day').val(newDate.getDate());
			$('#search_t165ETA_month').val(newDate.getMonth()+1);
			$('#search_t165ETA_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			ETATable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	ETATable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	ETATable = $('#ETA_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
	"sName": "t165ETA",
	"mDataProp": "t165ETA",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "kode_vendor",
	"mDataProp": "kode_vendor",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "nama_vendor",
	"mDataProp": "nama_vendor",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "kode_part",
	"mDataProp": "kode_part",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "nama_part",
	"mDataProp": "nama_part",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "tanggal_po",
	"mDataProp": "tanggal_po",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "no_po",
	"mDataProp": "no_po",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        if($('input[name=is_tanggal_eta]').is(':checked')){
                            var tanggalStart = $("#search_Tanggal_start").val();
                            var tanggalStartDay = $('#search_Tanggal_start_day').val();
                            var tanggalStartMonth = $('#search_Tanggal_start_month').val();
                            var tanggalStartYear = $('#search_Tanggal_start_year').val();
                            if(tanggalStart){
                                aoData.push(
                                        {"name": 'tanggalStart', "value": "date.struct"},
                                        {"name": 'tanggalStart_dp', "value": tanggalStart},
                                        {"name": 'tanggalStart_day', "value": tanggalStartDay},
                                        {"name": 'tanggalStart_month', "value": tanggalStartMonth},
                                        {"name": 'tanggalStart_year', "value": tanggalStartYear}
                                );
                            }



                            var tanggalEnd = $("#search_Tanggal_start").val();
                            var tanggalEndDay = $('#search_Tanggal_end_day').val();
                            var tanggalEndMonth = $('#search_Tanggal_end_month').val();
                            var tanggalEndYear = $('#search_Tanggal_end_year').val();
                            if(tanggalEnd){
                                aoData.push(
                                        {"name": 'tanggalEnd', "value": "date.struct"},
                                        {"name": 'tanggalEnd_dp', "value": tanggalEnd},
                                        {"name": 'tanggalEnd_day', "value": tanggalEndDay},
                                        {"name": 'tanggalEnd_month', "value": tanggalEndMonth},
                                        {"name": 'tanggalEnd_year', "value": tanggalEndYear}
                                );
                            }
                        }

                        if($('input[name=is_vendor]').is(':checked')){
                            var vendor = $("#vendor").find(":selected").val();
                            if(vendor){
                                aoData.push(
                                        {"name": 'vendor', "value": vendor}
                                );
                            }
						}

                        if($('input[name=is_goods]').is(':checked')){
                            var goods = $("#goods").find(":selected").val();
                            if(vendor){
                                aoData.push(
                                        {"name": 'goods', "value": goods}
                                );
                            }
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
