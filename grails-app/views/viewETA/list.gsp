
<%@ page import="com.kombos.parts.Goods; com.kombos.parts.Vendor; com.kombos.parts.ETA" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'ETA.label', default: 'ETA')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/ETA/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/ETA/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#ETA-form').empty();
    	$('#ETA-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#ETA-table").hasClass("span12")){
   			$("#ETA-table").toggleClass("span12 span5");
        }
        $("#ETA-form").css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
   		if($("#ETA-table").hasClass("span5")){
   			$("#ETA-table").toggleClass("span5 span12");
   		}
        $("#ETA-form").css("display","none");
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#ETA-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/ETA/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadETATable();
    		}
		});
		
   	}

});

function searchETA(){
    reloadETATable();
}
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			%{--<li><a class="pull-right box-action" href="#"--}%
				%{--style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i--}%
					%{--class="icon-plus"></i>&nbsp;&nbsp;--}%
			%{--</a></li>--}%
			%{--<li><a class="pull-right box-action" href="#"--}%
				%{--style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i--}%
					%{--class="icon-remove"></i>&nbsp;&nbsp;--}%
			%{--</a></li>--}%
			%{--<li class="separator"></li>--}%
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="ETA-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <fieldset>
                <table style="padding-right: 10px">
                    <tr>
                        <td style="width: 130px">
                            <label class="control-label" for="tanggal_eta">
                                <g:message code="eta.tanggal.label" default="Tanggal ETA" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="tanggal_eta" class="controls">
                                <input type="checkbox" name="is_tanggal_eta" id="is_tanggal_eta"/>
                                <ba:datePicker id="search_Tanggal_start" name="search_Tanggal_start" precision="day" format="dd/MM/yyyy"  value="" />
                                &nbsp;-&nbsp;
                                <ba:datePicker id="search_Tanggal_end" name="search_Tanggal_end" precision="day" format="dd/MM/yyyy"  value="" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label" for="vendor">
                                <g:message code="eta.vendor.label" default="Vendor" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="vendor" class="controls">
                                <input type="checkbox" name="is_vendor" id="is_vendor"/>
                                <g:select id="vendor" name="vendor.id" from="${Vendor.list()}" optionKey="id" required="" class="many-to-one" noSelection="['':'']"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label" for="goods">
                                <g:message code="eta.goods.label" default="Kode Part" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="goods" class="controls">
                                <input type="checkbox" name="is_goods" id="is_goods"/>
                                <g:select id="goods" name="goods.id" from="${Goods.list()}" optionKey="id" optionValue="m111ID" required="" class="many-to-one" noSelection="['':'']"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" >
                            <div class="controls" style="right: 0">
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn-primary view" name="view" id="view" onclick="searchETA()" >Search</button>
                            </div>
                        </td>
                    </tr>
                </table>
            </fieldset>
			<g:render template="dataTables" />
		</div>
		<div class="span7" id="ETA-form" style="display: none;"></div>
	</div>
</body>
</html>
