

<%@ page import="com.kombos.parts.ETA" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'ETA.label', default: 'ETA')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteETA;

$(function(){ 
	deleteETA=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/ETA/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadETATable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-ETA" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="ETA"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${ETAInstance?.po}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="po-label" class="property-label"><g:message
					code="ETA.po.label" default="Po" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="po-label">
						%{--<ba:editableValue
								bean="${ETAInstance}" field="po"
								url="${request.contextPath}/ETA/updatefield" type="text"
								title="Enter po" onsuccess="reloadETATable();" />--}%
							
								<g:link controller="PO" action="show" id="${ETAInstance?.po?.id}">${ETAInstance?.po?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${ETAInstance?.goods}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="goods-label" class="property-label"><g:message
					code="ETA.goods.label" default="Goods" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="goods-label">
						%{--<ba:editableValue
								bean="${ETAInstance}" field="goods"
								url="${request.contextPath}/ETA/updatefield" type="text"
								title="Enter goods" onsuccess="reloadETATable();" />--}%
							
								<g:link controller="goods" action="show" id="${ETAInstance?.goods?.id}">${ETAInstance?.goods?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${ETAInstance?.t165Qty1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t165Qty1-label" class="property-label"><g:message
					code="ETA.t165Qty1.label" default="T165 Qty1" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t165Qty1-label">
						%{--<ba:editableValue
								bean="${ETAInstance}" field="t165Qty1"
								url="${request.contextPath}/ETA/updatefield" type="text"
								title="Enter t165Qty1" onsuccess="reloadETATable();" />--}%
							
								<g:fieldValue bean="${ETAInstance}" field="t165Qty1"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${ETAInstance?.t165ETA}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t165ETA-label" class="property-label"><g:message
					code="ETA.t165ETA.label" default="T165 ETA" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t165ETA-label">
						%{--<ba:editableValue
								bean="${ETAInstance}" field="t165ETA"
								url="${request.contextPath}/ETA/updatefield" type="text"
								title="Enter t165ETA" onsuccess="reloadETATable();" />--}%
							
								<g:formatDate date="${ETAInstance?.t165ETA}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${ETAInstance?.t165xNamaUser}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t165xNamaUser-label" class="property-label"><g:message
					code="ETA.t165xNamaUser.label" default="T165x Nama User" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t165xNamaUser-label">
						%{--<ba:editableValue
								bean="${ETAInstance}" field="t165xNamaUser"
								url="${request.contextPath}/ETA/updatefield" type="text"
								title="Enter t165xNamaUser" onsuccess="reloadETATable();" />--}%
							
								<g:fieldValue bean="${ETAInstance}" field="t165xNamaUser"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${ETAInstance?.t165xNamaDivisi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t165xNamaDivisi-label" class="property-label"><g:message
					code="ETA.t165xNamaDivisi.label" default="T165x Nama Divisi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t165xNamaDivisi-label">
						%{--<ba:editableValue
								bean="${ETAInstance}" field="t165xNamaDivisi"
								url="${request.contextPath}/ETA/updatefield" type="text"
								title="Enter t165xNamaDivisi" onsuccess="reloadETATable();" />--}%
							
								<g:fieldValue bean="${ETAInstance}" field="t165xNamaDivisi"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${ETAInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="ETA.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${ETAInstance}" field="staDel"
								url="${request.contextPath}/ETA/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadETATable();" />--}%
							
								<g:fieldValue bean="${ETAInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${ETAInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="ETA.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${ETAInstance}" field="createdBy"
								url="${request.contextPath}/ETA/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadETATable();" />--}%
							
								<g:fieldValue bean="${ETAInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${ETAInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="ETA.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${ETAInstance}" field="updatedBy"
								url="${request.contextPath}/ETA/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadETATable();" />--}%
							
								<g:fieldValue bean="${ETAInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${ETAInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="ETA.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${ETAInstance}" field="lastUpdProcess"
								url="${request.contextPath}/ETA/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadETATable();" />--}%
							
								<g:fieldValue bean="${ETAInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${ETAInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="ETA.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${ETAInstance}" field="dateCreated"
								url="${request.contextPath}/ETA/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadETATable();" />--}%
							
								<g:formatDate date="${ETAInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${ETAInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="ETA.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${ETAInstance}" field="lastUpdated"
								url="${request.contextPath}/ETA/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadETATable();" />--}%
							
								<g:formatDate date="${ETAInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${ETAInstance?.id}"
					update="[success:'ETA-form',failure:'ETA-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteETA('${ETAInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
