<%@ page import="com.kombos.administrasi.AuditTrail" %>



<div class="control-group fieldcontain ${hasErrors(bean: auditTrailInstance, field: 'm777Tgl', 'error')} required">
	<label class="control-label" for="m777Tgl">
		<g:message code="auditTrail.m777Tgl.label" default="M777 Tgl" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:datePicker name="m777Tgl" precision="day"  value="${auditTrailInstance?.m777Tgl}"  />
	</div>
</div>
<g:javascript>
    document.getElementById("m777Tgl").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: auditTrailInstance, field: 'm777Id', 'error')} required">
	<label class="control-label" for="m777Id">
		<g:message code="auditTrail.m777Id.label" default="M777 Id" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field name="m777Id" type="number" value="${auditTrailInstance.m777Id}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: auditTrailInstance, field: 'userProfile', 'error')} required">
	<label class="control-label" for="userProfile">
		<g:message code="auditTrail.userProfile.label" default="User Profile" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="userProfile" name="userProfile.id" from="${com.kombos.administrasi.UserProfile.list()}" optionKey="id" required="" value="${auditTrailInstance?.userProfile?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: auditTrailInstance, field: 'm777StalUD', 'error')} required">
	<label class="control-label" for="m777StalUD">
		<g:message code="auditTrail.m777StalUD.label" default="M777 Stal UD" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m777StalUD" maxlength="1" required="" value="${auditTrailInstance?.m777StalUD}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: auditTrailInstance, field: 'm777NamaForm', 'error')} required">
	<label class="control-label" for="m777NamaForm">
		<g:message code="auditTrail.m777NamaForm.label" default="M777 Nama Form" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m777NamaForm" maxlength="50" required="" value="${auditTrailInstance?.m777NamaForm}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: auditTrailInstance, field: 'm777NamaActivity', 'error')} required">
	<label class="control-label" for="m777NamaActivity">
		<g:message code="auditTrail.m777NamaActivity.label" default="M777 Nama Activity" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m777NamaActivity" maxlength="250" required="" value="${auditTrailInstance?.m777NamaActivity}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: auditTrailInstance, field: 'staDel', 'error')} required">
	<label class="control-label" for="staDel">
		<g:message code="auditTrail.staDel.label" default="M777 Sta Del" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="staDel" maxlength="1" required="" value="${auditTrailInstance?.staDel}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: auditTrailInstance, field: 'createdBy', 'error')} ">
	<label class="control-label" for="createdBy">
		<g:message code="auditTrail.createdBy.label" default="Created By" />
		
	</label>
	<div class="controls">
	<g:textField name="createdBy" value="${auditTrailInstance?.createdBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: auditTrailInstance, field: 'updatedBy', 'error')} ">
	<label class="control-label" for="updatedBy">
		<g:message code="auditTrail.updatedBy.label" default="Updated By" />
		
	</label>
	<div class="controls">
	<g:textField name="updatedBy" value="${auditTrailInstance?.updatedBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: auditTrailInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="auditTrail.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${auditTrailInstance?.lastUpdProcess}"/>
	</div>
</div>

