

<%@ page import="com.kombos.administrasi.AuditTrail" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'auditTrail.label', default: 'AuditTrail')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteAuditTrail;

$(function(){ 
	deleteAuditTrail=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/auditTrail/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadAuditTrailTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-auditTrail" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="auditTrail"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${auditTrailInstance?.m777Tgl}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m777Tgl-label" class="property-label"><g:message
					code="auditTrail.m777Tgl.label" default="M777 Tgl" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m777Tgl-label">
						%{--<ba:editableValue
								bean="${auditTrailInstance}" field="m777Tgl"
								url="${request.contextPath}/AuditTrail/updatefield" type="text"
								title="Enter m777Tgl" onsuccess="reloadAuditTrailTable();" />--}%
							
								<g:formatDate date="${auditTrailInstance?.m777Tgl}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${auditTrailInstance?.m777Id}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m777Id-label" class="property-label"><g:message
					code="auditTrail.m777Id.label" default="M777 Id" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m777Id-label">
						%{--<ba:editableValue
								bean="${auditTrailInstance}" field="m777Id"
								url="${request.contextPath}/AuditTrail/updatefield" type="text"
								title="Enter m777Id" onsuccess="reloadAuditTrailTable();" />--}%
							
								<g:fieldValue bean="${auditTrailInstance}" field="m777Id"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${auditTrailInstance?.userProfile}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="userProfile-label" class="property-label"><g:message
					code="auditTrail.userProfile.label" default="User Profile" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="userProfile-label">
						%{--<ba:editableValue
								bean="${auditTrailInstance}" field="userProfile"
								url="${request.contextPath}/AuditTrail/updatefield" type="text"
								title="Enter userProfile" onsuccess="reloadAuditTrailTable();" />--}%
							
								<g:link controller="userProfile" action="show" id="${auditTrailInstance?.userProfile?.id}">${auditTrailInstance?.userProfile?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${auditTrailInstance?.m777StalUD}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m777StalUD-label" class="property-label"><g:message
					code="auditTrail.m777StalUD.label" default="M777 Stal UD" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m777StalUD-label">
						%{--<ba:editableValue
								bean="${auditTrailInstance}" field="m777StalUD"
								url="${request.contextPath}/AuditTrail/updatefield" type="text"
								title="Enter m777StalUD" onsuccess="reloadAuditTrailTable();" />--}%
							
								<g:fieldValue bean="${auditTrailInstance}" field="m777StalUD"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${auditTrailInstance?.m777NamaForm}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m777NamaForm-label" class="property-label"><g:message
					code="auditTrail.m777NamaForm.label" default="M777 Nama Form" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m777NamaForm-label">
						%{--<ba:editableValue
								bean="${auditTrailInstance}" field="m777NamaForm"
								url="${request.contextPath}/AuditTrail/updatefield" type="text"
								title="Enter m777NamaForm" onsuccess="reloadAuditTrailTable();" />--}%
							
								<g:fieldValue bean="${auditTrailInstance}" field="m777NamaForm"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${auditTrailInstance?.m777NamaActivity}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m777NamaActivity-label" class="property-label"><g:message
					code="auditTrail.m777NamaActivity.label" default="M777 Nama Activity" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m777NamaActivity-label">
						%{--<ba:editableValue
								bean="${auditTrailInstance}" field="m777NamaActivity"
								url="${request.contextPath}/AuditTrail/updatefield" type="text"
								title="Enter m777NamaActivity" onsuccess="reloadAuditTrailTable();" />--}%
							
								<g:fieldValue bean="${auditTrailInstance}" field="m777NamaActivity"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${auditTrailInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="auditTrail.staDel.label" default="M777 Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${auditTrailInstance}" field="staDel"
								url="${request.contextPath}/AuditTrail/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadAuditTrailTable();" />--}%
							
								<g:fieldValue bean="${auditTrailInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${auditTrailInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="agama.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">

                        <g:fieldValue bean="${auditTrailInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${auditTrailInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="agama.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">

                        <g:formatDate date="${auditTrailInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${auditTrailInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="agama.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">

                        <g:fieldValue bean="${auditTrailInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${auditTrailInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="agama.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">

                        <g:formatDate date="${auditTrailInstance?.lastUpdated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${auditTrailInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="agama.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">

                        <g:fieldValue bean="${auditTrailInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${auditTrailInstance?.id}"
					update="[success:'auditTrail-form',failure:'auditTrail-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteAuditTrail('${auditTrailInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
