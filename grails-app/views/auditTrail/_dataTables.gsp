
<%@ page import="com.kombos.administrasi.AuditTrail" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="auditTrail_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="auditTrail.m777Tgl.label" default="Tanggal" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; display: none">
				<div><g:message code="auditTrail.m777Id.label" default="M777 Id" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="auditTrail.userProfile.label" default="Nama User" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; display: none">
				<div><g:message code="auditTrail.m777StalUD.label" default="M777 Stal UD" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;  display: none">
				<div><g:message code="auditTrail.m777NamaForm.label" default="M777 Nama Form" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="auditTrail.m777NamaActivity.label" default="Aktifitas" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; display: none">
				<div><g:message code="auditTrail.staDel.label" default="M777 Sta Del" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; display: none">
				<div><g:message code="auditTrail.createdBy.label" default="Created By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; display: none">
				<div><g:message code="auditTrail.updatedBy.label" default="Updated By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; display: none">
				<div><g:message code="auditTrail.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>

		
		</tr>
		%{--<tr>--}%
		%{----}%

			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_m777Tgl" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >--}%
					%{--<input type="hidden" name="search_m777Tgl" value="date.struct">--}%
					%{--<input type="hidden" name="search_m777Tgl_day" id="search_m777Tgl_day" value="">--}%
					%{--<input type="hidden" name="search_m777Tgl_month" id="search_m777Tgl_month" value="">--}%
					%{--<input type="hidden" name="search_m777Tgl_year" id="search_m777Tgl_year" value="">--}%
					%{--<input type="text" data-date-format="dd/mm/yyyy" name="search_m777Tgl_dp" value="" id="search_m777Tgl" class="search_init">--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_m777Id" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_m777Id" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_userProfile" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_userProfile" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_m777StalUD" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_m777StalUD" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_m777NamaForm" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_m777NamaForm" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_m777NamaActivity" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_m777NamaActivity" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_staDel" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_createdBy" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_updatedBy" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_lastUpdProcess" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%


		%{--</tr>--}%
	</thead>
</table>

<g:javascript>
var auditTrailTable;
var reloadAuditTrailTable;
$(function(){
	
	reloadAuditTrailTable = function() {
		auditTrailTable.fnDraw();
	}

	
//	$('#search_m777Tgl').datepicker().on('changeDate', function(ev) {
//			var newDate = new Date(ev.date);
//			$('#search_m777Tgl_day').val(newDate.getDate());
//			$('#search_m777Tgl_month').val(newDate.getMonth()+1);
//			$('#search_m777Tgl_year').val(newDate.getFullYear());
//			$(this).datepicker('hide');
//			auditTrailTable.fnDraw();
//	});

	


    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	auditTrailTable.fnDraw();
		}
	});
    $('#clear').click(function(e){
        $('#search_m777Tgl').val("");
        $('#search_m777Tgl_day').val("");
        $('#search_m777Tgl_month').val("");
        $('#search_m777Tgl_year').val("");
        $('#search_m777Tglakhir').val("");
        $('#search_m777Tglakhir_day').val("");
        $('#search_m777Tglakhir_month').val("");
        $('#search_m777Tglakhir_year').val("");
        $('#filter_userProfile select').val("");
        $('#filter_role select').val("");
        $('#filter_m777NamaActivity input').val("");
//        reloadAuditTrailTable();
	});
    $('#view').click(function(e){
        e.stopPropagation();
		auditTrailTable.fnDraw();
//        reloadAuditTrailTable();
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});
	auditTrailTable = $('#auditTrail_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m777Tgl",
	"mDataProp": "m777Tgl",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m777Id",
	"mDataProp": "m777Id",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "userProfile",
	"mDataProp": "userProfile",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m777StalUD",
	"mDataProp": "m777StalUD",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "m777NamaForm",
	"mDataProp": "m777NamaForm",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "m777NamaActivity",
	"mDataProp": "m777NamaActivity",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "staDel",
	"mDataProp": "staDel",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "createdBy",
	"mDataProp": "createdBy",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "updatedBy",
	"mDataProp": "updatedBy",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var m777Tgl = $('#search_m777Tgl').val();
						var m777TglDay = $('#search_m777Tgl_day').val();
						var m777TglMonth = $('#search_m777Tgl_month').val();
						var m777TglYear = $('#search_m777Tgl_year').val();
						
						if(m777Tgl){
							aoData.push(
									{"name": 'sCriteria_m777Tgl', "value": "date.struct"},
									{"name": 'sCriteria_m777Tgl_dp', "value": m777Tgl},
									{"name": 'sCriteria_m777Tgl_day', "value": m777TglDay},
									{"name": 'sCriteria_m777Tgl_month', "value": m777TglMonth},
									{"name": 'sCriteria_m777Tgl_year', "value": m777TglYear}
							);
						}

                        var m777Tglakhir = $('#search_m777Tglakhir').val();
                        var m777TglDayakhir = $('#search_m777Tglakhir_day').val();
                        var m777TglMonthakhir = $('#search_m777Tglakhir_month').val();
                        var m777TglYearakhir = $('#search_m777Tglakhir_year').val();

                        if(m777Tglakhir){
                            aoData.push(
                                    {"name": 'sCriteria_m777Tglakhir', "value": "date.struct"},
                                    {"name": 'sCriteria_m777Tglakhir_dp', "value": m777Tglakhir},
                                    {"name": 'sCriteria_m777Tglakhir_day', "value": m777TglDayakhir},
                                    {"name": 'sCriteria_m777Tglakhir_month', "value":m777TglMonthakhir},
                                    {"name": 'sCriteria_m777Tglakhir_year', "value": m777TglYearakhir}
                            );
                        }

						var m777Id = $('#filter_m777Id input').val();
						if(m777Id){
							aoData.push(
									{"name": 'sCriteria_m777Id', "value": m777Id}
							);
						}
	
						var userProfile = $('#filter_userProfile select').val();
						if(userProfile){
							aoData.push(
									{"name": 'sCriteria_userProfile', "value": userProfile}
							);
						}
	
						var m777StalUD = $('#filter_m777StalUD input').val();
						if(m777StalUD){
							aoData.push(
									{"name": 'sCriteria_m777StalUD', "value": m777StalUD}
							);
						}
	
						var m777NamaForm = $('#filter_m777NamaForm input').val();
						if(m777NamaForm){
							aoData.push(
									{"name": 'sCriteria_m777NamaForm', "value": m777NamaForm}
							);
						}
	
						var m777NamaActivity = $('#filter_m777NamaActivity input').val();
						if(m777NamaActivity){
							aoData.push(
									{"name": 'sCriteria_m777NamaActivity', "value": m777NamaActivity}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						var role = $('#filter_role select').val();
                        if(role){
                            aoData.push(
                                            {"name": 'sCriteria_role', "value": role}
                            );
                        }
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
