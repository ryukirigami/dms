
<%@ page import="com.kombos.administrasi.UserProfile; com.kombos.administrasi.UserRole; com.kombos.administrasi.AuditTrail" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'auditTrail.label', default: 'AuditTrail')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
        <r:require module="export"/>
		<g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 

	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	

	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/auditTrail/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/auditTrail/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#auditTrail-form').empty();
    	$('#auditTrail-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#auditTrail-table").hasClass("span12")){
   			$("#auditTrail-table").toggleClass("span12 span5");
        }
        $("#auditTrail-form").css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
   		if($("#auditTrail-table").hasClass("span5")){
   			$("#auditTrail-table").toggleClass("span5 span12");
   		}
        $("#auditTrail-form").css("display","none");
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#auditTrail-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/auditTrail/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadAuditTrailTable();
    		}
		});
		
   	}

});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			%{--<li><a class="pull-right box-action" href="#"--}%
				%{--style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i--}%
					%{--class="icon-plus"></i>&nbsp;&nbsp;--}%
			%{--</a></li>--}%
			%{--<li><a class="pull-right box-action" href="#"--}%
				%{--style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i--}%
					%{--class="icon-remove"></i>&nbsp;&nbsp;--}%
			%{--</a></li>--}%
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="auditTrail-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <fieldset>
                <table style="padding-right: 10px">
                    <tr>
                        <td>
                            <label class="control-label" for="t951Tanggal">
                                <g:message code="auditTrail.tanggal.label" default="Tanggal" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="filter_m777Tgl" class="controls">
                                <ba:datePicker id="search_m777Tgl" name="search_m777Tgl" precision="day" format="dd/MM/yyyy"  value=""  />
                                &nbsp;&nbsp;&nbsp;s.d.&nbsp;&nbsp;&nbsp;
                                <ba:datePicker id="search_m777Tglakhir" name="search_m777Tglakhir" precision="day" format="dd/MM/yyyy"  value=""  />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label" for="userRole">
                                <g:message code="auditTrail.role.label" default="Role" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>

                            <div id="filter_role" class="controls">
                                <g:select style="width:500px" id="userRole" name="userRole.id" from="${UserRole.createCriteria().list(){eq("staDel","0");order("t003NamaRole")}}" optionKey="id" value="" class="many-to-one" noSelection="['':'Pilih User Role']"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label" for="userProfile">
                                <g:message code="auditTrail.userProfile.label" default="User Profile" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="filter_userProfile" class="controls">
                                <g:select style="width:500px; align-items:flex-end;align-content: flex-end;align-self:flex-end" id="userProfile" name="userProfile.id" from="${UserProfile.createCriteria().list(){eq("staDel","0");order("t001NamaUser")}}" optionValue="${{it.t001NamaUser+" ("+it?.t001Inisial+")"}}" optionKey="id" value="" class="many-to-one"  noSelection="['':'Pilih Nama User']"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label" for="kataKunci">
                                <g:message code="auditTrail.kataKunci.label" default="Kata Kunci" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="filter_m777NamaActivity" class="controls">
                                <g:textField style="width:490px" id="search_m777NamaActivity" name="search_m777NamaActivity" maxlength="50" />
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" >
                            <div class="controls" style="right: 0">
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn-primary view" name="view" id="view" >View</button>
                                &nbsp;&nbsp;
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn-primary editable-cancel" name="clear" id="clear" >Clear</button>
                            </div>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <g:render template="dataTables" />
		</div>
        <fieldset>
            <div class="paginateButtons" style="width: 100px">
                <g:paginate total="${AuditTrail.createCriteria().count(){eq("staDel","0")}}" />
            </div>
            <export:formats formats="['excel']" />

        </fieldset>
        <div class="span7" id="auditTrail-form" style="display: none;"></div>
	</div>
</body>
</html>
