<%@ page import="com.kombos.administrasi.MasterPanel; com.kombos.administrasi.Operation; com.kombos.administrasi.MappingJobPanel" %>



<div class="control-group fieldcontain ${hasErrors(bean: mappingJobPanelInstance, field: 'operation', 'error')} required">
	<label class="control-label" for="operation">
		<g:message code="mappingJobPanel.operation.label" default="Nama Job" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="operation" name="operation.id" from="${Operation.createCriteria().list {eq("staDel", "0");order("m053NamaOperation", "asc")}}" optionKey="id" required="" value="${mappingJobPanelInstance?.operation?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: mappingJobPanelInstance, field: 'masterpanel', 'error')} required">
	<label class="control-label" for="masterpanel">
		<g:message code="mappingJobPanel.masterpanel.label" default="Nama Panel" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="masterpanel" name="masterpanel.id" from="${MasterPanel.list()}" optionKey="id" required="" value="${mappingJobPanelInstance?.masterpanel?.id}" class="many-to-one"/>
	</div>
</div>

%{--<div class="control-group fieldcontain ${hasErrors(bean: mappingJobPanelInstance, field: 'createdBy', 'error')} ">--}%
	%{--<label class="control-label" for="createdBy">--}%
		%{--<g:message code="mappingJobPanel.createdBy.label" default="Created By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="createdBy" value="${mappingJobPanelInstance?.createdBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: mappingJobPanelInstance, field: 'updatedBy', 'error')} ">--}%
	%{--<label class="control-label" for="updatedBy">--}%
		%{--<g:message code="mappingJobPanel.updatedBy.label" default="Updated By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="updatedBy" value="${mappingJobPanelInstance?.updatedBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: mappingJobPanelInstance, field: 'lastUpdProcess', 'error')} ">--}%
	%{--<label class="control-label" for="lastUpdProcess">--}%
		%{--<g:message code="mappingJobPanel.lastUpdProcess.label" default="Last Upd Process" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="lastUpdProcess" value="${mappingJobPanelInstance?.lastUpdProcess}"/>--}%
	%{--</div>--}%
%{--</div>--}%

