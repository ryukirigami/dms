
<%@ page import="com.kombos.parts.PartsTransferStok" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'partsTransferStok.label', default: 'Input Data Parts ')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
    <g:javascript>
		$(function(){
			$('#m121Nama').typeahead({
			    source: function (query, process) {
			   // alert("nilai query : "+query);
			       return $.get('${request.contextPath}/partsTransferStokInput/kodeList', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});
    </g:javascript>
		<g:javascript disposition="head">
 var m121Nama = "";
    var noUrut = 1;
    var cekStatus = "${aksi}";
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/partsTransferStokInput/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };

    loadForm = function(data, textStatus){
		$('#partsTransferStokInput-form').empty();
    	$('#partsTransferStokInput-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#partsTransferStokInput-table").hasClass("span12")){
   			$("#partsTransferStokInput-table").toggleClass("span12 span5");
        }
        $("#partsTransferStokInput-form").css("display","block");
   	}
   	
   	expandTableLayout = function(){
   		if($("#partsTransferStokInput-table").hasClass("span5")){
   			$("#partsTransferStokInput-table").toggleClass("span5 span12");
   		}
        $("#partsTransferStokInput-form").css("display","none");
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#partsTransferStokInput-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/partsTransferStokInput/massdelete',
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadRequestTable();
    		}
		});
		
   	}

});
            $(document).ready(function()
         {
             $('input:radio[name=search_t162StaFA]').click(function(){
                if($('input:radio[name=search_t162StaFA]:nth(0)').is(':checked')){
                    $("#search_t162NoReff").prop('disabled', false);

                }else{
                    $("#search_t162NoReff").val("")
                    $("#search_t162NoReff").prop('disabled', true);
                }
             });


         });

    //darisini
	$("#partsTransferStokAddModal").on("show", function() {
		$("#partsTransferStokAddModal .btn").on("click", function(e) {
			$("#partsTransferStokAddModal").modal('hide');
		});
	});
	$("#partsTransferStokAddModal").on("hide", function() {
		$("#partsTransferStokAddModal a.btn").off("click");
	});
    loadPartsTransferStokAddModal = function(){
	    if(m121Nama != $('#m121Nama').val()){
               $("#partsTransferStokInput-table tbody .row-select").each(function() {
                    var id = $(this).next("input:hidden").val();
                    var row = $(this).closest("tr").get(0);
                    partsTransferStokTable.fnDeleteRow(partsTransferStokTable.fnGetPosition(row));
                });
          }
    	    m121Nama = $('#m121Nama').val();
		$("#partsTransferStokAddContent").empty();
		$.ajax({type:'POST', url:'${request.contextPath}/partsTransferStokInput/partsAdd',
		data : {vendor : m121Nama},
   			success:function(data,textStatus){
					$("#partsTransferStokAddContent").html(data);
				    $("#partsTransferStokAddModal").modal({
						"backdrop" : "static",
						"keyboard" : true,
						"show" : true
					}).css({'width': '970px','margin-left': function () {return -($(this).width() / 2);}});

   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});



    }

    
       tambahReq = function(){

          var checkGoods =[];
            $("#PartsTransferStokAdd-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    var nRow = $(this).parents('tr')[0];

					checkGoods.push(nRow);
                }
            });
            if(checkGoods.length<1){
                alert('Pilih Data Yang akan ditambahkan?');
                loadPartsTransferStokAddModal();
            } else {
				for (var i=0;i < checkGoods.length;i++){

					var aData = PartsTransferStokAddTable.fnGetData(checkGoods[i]);

					partsTransferStokTable.fnAddData({
						'id': aData['id'],

						'goods': aData['goods'],
						'goods2': aData['goods2'],
						'hargaBeli': aData['hargaBeli'],
 						'Qreturn': '0'

						});

					$('#qty-'+aData['id']).autoNumeric('init',{
						vMin:'0',
						vMax:'999999999999999999',
						mDec: '2',
						aDec:',',
						aSep:''
					});
				}
				 loadPartsTransferStokAddModal();

			}

      }
      deleteData = function(){
                var checkGoods =[];
                $("#partsTransferStokInput-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    var row = $(this).closest("tr").get(0);
                    partsTransferStokTable.fnDeleteRow(partsTransferStokTable.fnGetPosition(row));
                }
                });
      }
      updatePartsTransferStok = function(){
            var formPartsTransferStok = $('#partsTransferStokInput-table').find('form');
                var qtyNol = false
                var checkGoods =[];
                $("#partsTransferStokInput-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    var nRow = $(this).parents('tr')[0];
                    var aData = partsTransferStokTable.fnGetData(nRow)
                    var qtyInput = $('#qty-' + aData['id'], nRow);
                    if(qtyInput.val()==0 || qtyInput.val()==""){
                       qtyNol = true
                    }
                    checkGoods.push(id);

                    formPartsTransferStok.append('<input type="hidden" name="qty-'+aData['id'] + '" value="'+qtyInput[0].value+'" class="deleteafter">');
                }
                });
                if(checkGoods.length<1){
                    alert('Anda belum memilih data yang akan ditambahkan');
                     reloadPartsTransferStokTable();
                }else{
                    if($("#tanggal").val()=="" || $("#m121Nama").val()==""){
                        toastr.error('<div>Mohon Diisi Dengan Benar.</div>');
                    }else if(qtyNol == true){
                        toastr.error('<div>Quantity Tidak Boleh NOL</div>');
                        formPartsTransferStok.find('.deleteafter').remove();
                    }
                    else{
                         var r = confirm("Anda yakin data akan disimpan?");
                         if(r==true){
                            var rows = partsTransferStokTable.fnGetNodes();
                            $("#ids").val(JSON.stringify(checkGoods));
                            $.ajax({
                                url:'${request.contextPath}/partsTransferStokInput/ubah',
                                type: "POST", // Always use POST when deleting data
                                data : formPartsTransferStok.serialize(),
                                success : function(data){
                                  if(data=='fail'){
                                    toastr.error('<div>Dealer Tujuan Tidak Benar.</div>');
                                  }else{
                                    if(rows.length > 10){
                                        $("#partsTransferStokInput-table tbody .row-select").each(function() {
                                            if(this.checked){
                                                var row = $(this).closest("tr").get(0);
                                                partsTransferStokTable.fnDeleteRow(partsTransferStokTable.fnGetPosition(row));
                                                reloadPartsTransferStokTable();
                                            }
                                        });
                                        formPartsTransferStok.append('<input type="hidden" name="noPartsTransferStokBefore" value="'+data+'" class="deleteafter">');
                                        toastr.success('<div>Sukses</div>');
                                    }else{
                                        window.location.href = '#/partsTransferStok' ;
                                        toastr.success('<div>Parts PartsTransferStok telah di Ubah.</div>');
                                        formPartsTransferStok.find('.deleteafter').remove();
                                        $("#ids").val('');
                                    }

                                  }
                                },
                            error: function(xhr, textStatus, errorThrown) {
                            alert('Internal Server Error');
                            }
                            });

                            checkGoods = [];
                          }
                    }
                }
      };
      createRequest = function(){
                var formPartsTransferStok = $('#partsTransferStokInput-table').find('form');
                var qtyNol = false
                var checkGoods =[];
                $("#partsTransferStokInput-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    var nRow = $(this).parents('tr')[0];
                    var aData = partsTransferStokTable.fnGetData(nRow)
                    var qtyInput = $('#qty-' + aData['id'], nRow);
                    if(qtyInput.val()==0 || qtyInput.val()==""){
                       qtyNol = true
                    }
                    checkGoods.push(id);

                    formPartsTransferStok.append('<input type="hidden" name="qty-'+aData['id'] + '" value="'+qtyInput[0].value+'" class="deleteafter">');
                }
                });
                if(checkGoods.length<1){
                    alert('Anda belum memilih data yang akan ditambahkan');
                     reloadPartsTransferStokTable();
                }else{
                    if($("#tanggal").val()=="" || $("#m121Nama").val()==""){
                        toastr.error('<div>Mohon Diisi Dengan Benar.</div>');
                    }else if(qtyNol == true){
                        toastr.error('<div>Quantity Tidak Boleh NOL</div>');
                        formPartsTransferStok.find('.deleteafter').remove();
                    }
                    else{
                         var r = confirm("Anda yakin data akan disimpan?");
                         if(r==true){
                            var rows = partsTransferStokTable.fnGetNodes();
                            $("#ids").val(JSON.stringify(checkGoods));
                            $.ajax({
                                url:'${request.contextPath}/partsTransferStokInput/req',
                                type: "POST", // Always use POST when deleting data
                                data : formPartsTransferStok.serialize(),
                                success : function(data){
                                  if(data=='fail'){
                                    toastr.error('<div>Dealer Tidak Benar.</div>');
                                  }else{
                                    if(rows.length > 10){
                                        $("#partsTransferStokInput-table tbody .row-select").each(function() {
                                            if(this.checked){
                                                var row = $(this).closest("tr").get(0);
                                                partsTransferStokTable.fnDeleteRow(partsTransferStokTable.fnGetPosition(row));
                                                reloadPartsTransferStokTable();
                                            }
                                        });
                                        formPartsTransferStok.append('<input type="hidden" name="noPartsTransferStokBefore" value="'+data+'" class="deleteafter">');
                                        toastr.success('<div>Sukses</div>');
                                    }else{
                                        window.location.href = '#/partsTransferStok' ;
                                        toastr.success('<div>Parts PartsTransferStok telah dibuat.</div>');
                                        formPartsTransferStok.find('.deleteafter').remove();
                                        $("#ids").val('');
                                    }

                                  }
                                },
                            error: function(xhr, textStatus, errorThrown) {
                            alert('Internal Server Error');
                            }
                            });

                            checkGoods = [];
                          }
                    }
                }

    };

</g:javascript>

	</head>
	<body>
    <div class="navbar box-header no-border">
        <span class="pull-left">
            <g:if test="${aksi=='ubah'}">
                Edit Transfer Parts
            </g:if>
            <g:else>
                Input Transfer Parts
            </g:else>
        </span>
        <ul class="nav pull-right">
            <li>&nbsp;</li>
        </ul>
    </div><br>
	<div class="box">
		<div class="span12" id="partsTransferStokInput-table">
            <div>${flash.message}</div>
            <fieldset>
                <form id="form-partsTransferStok" class="form-horizontal">
                    <input type="hidden" name="ids" id="ids" value="">
                         <div class="control-group">
                            <label class="control-label" for="userRole"  style="text-align: left;">Tanggal Permintaan *
                            </label>
                            <div id="filter_status" class="controls">
                            <g:if test="${aksi=='ubah'}">
                                <ba:datePicker id="tanggal" name="tanggal" precision="day" format="dd-MM-yyyy" required="" value="${u_tanggal}"  />
                            </g:if>
                            <g:else>
                                <ba:datePicker id="tanggal" name="tanggal" precision="day" format="dd-MM-yyyy" required="" value="${new Date()}"  />
                            </g:else>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="t951Tanggal" style="text-align: left;">Company Dealer Tujuan *
                            </label>
                            <div id="filter_vendor" class="controls">
                                <g:if test="${aksi=='ubah'}">
                                    <g:textField name="companyDealer" id="companyDealer" value="${u_companyDealerTujuan}" maxlength="220" readonly="readonly"  />
                                    <g:hiddenField name="noPartsTransferStok" id="noPartsTransferStok" value="${noPartsTransferStok}"/>
                                </g:if>
                                <g:else>
                                    <g:textField name="namaCompanyDealer" id="m121Nama" class="typeahead"  autocomplete="off" maxlength="220"  />
                                </g:else>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="keterangan" style="text-align: left;">Keterangan
                            </label>
                            <div id="filter_status2" class="controls">
                                <g:textArea name="keterangan" id="keterangan" maxlength="50" style="resize:none" />
                            </div>
                        </div>
                </form>
            </fieldset>
            <g:if test="${aksi!='ubah'}">
            <fieldset>
                <table>
                    <tr>
                        <td>
                            <button style="width: 170px;height: 30px; border-radius: 5px" class="btn btn-primary" name="view" id="add" onclick="loadPartsTransferStokAddModal()">Add Parts</button>
                        </td>
                    </tr>
                </table>
            </fieldset>
            </g:if>
                <br>
			<g:render template="dataTables" />
            <g:if test="${aksi=='ubah'}">
                <g:field type="button" onclick="updatePartsTransferStok()" class="btn btn-primary create" name="Edit"  value="${message(code: 'default.button.upload.label', default: 'Update')}" />
            </g:if>
            <g:else>
                <g:field type="button" onclick="deleteData()" class="btn btn-cancel create" name="deleteData" id="delData" value="${message(code: 'default.button.upload.label', default: 'Delete')}" />
                <g:field type="button" onclick="createRequest()" class="btn btn-primary create" name="tambahSave" id="tambahSave" value="${message(code: 'default.button.upload.label', default: 'Save')}" />
            </g:else>
            <g:field type="button" onclick="window.location.replace('#/partsTransferStok');" class="btn btn-cancel" name="cancel" id="cancel" value="Cancel" />
		</div>
		<div class="span7" id="partsTransferStok-form" style="display: none;"></div>
	</div>
    <div id="partsTransferStokAddModal" class="modal fade">
        <div class="modal-dialog" style="width: 980px;">
            <div class="modal-content" style="width: 980px;">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 830px;">
                    <div id="partsTransferStokAddContent"/>
                <div class="iu-content"></div>

                </div>

            </div>
        </div>
    </div>
</body>
</html>

<g:javascript>
    <g:if test="${aksi=='ubah'}">
        %{--document.getElementById("tanggal").focus();--}%
    </g:if>
    <g:else>
        document.getElementById("m121Nama").focus();
        $("input[name=statusParts][value=" + 0 + "]").attr('checked', 'checked');
    </g:else>
</g:javascript>