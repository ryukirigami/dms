
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="partsTransferStokInput_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;"  >
				<div><g:message code="partsTransferStokInput.goods.label" default="Kode Parts" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;" >
                <div><g:message code="partsTransferStokInput.goods.label" default="Nama Parts" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="partsTransferStokInput.t162Qty1.label" default="Harga Beli Parts" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="partsTransferStokInput.t162Qty1.label" default="Qty" /></div>
            </th>
        </tr>


	</thead>
</table>

<g:javascript>
var partsTransferStokTable;
var reloadPartsTransferStokTable;
$(function(){
	var recordsPartsTransferStokPerPage = [];
    var anPartsTransferStokSelected;
    var jmlRecPartsTransferStokPerPage=0;
    var id;
	reloadPartsTransferStokTable = function() {
		partsTransferStokTable.fnDraw();
	}

	
	$('#search_t162TglPartsTransferStok').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t162TglPartsTransferStok_day').val(newDate.getDate());
			$('#search_t162TglPartsTransferStok_month').val(newDate.getMonth()+1);
			$('#search_t162TglPartsTransferStok_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			partsTransferStokTable.fnDraw();
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	partsTransferStokTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	partsTransferStokTable = $('#partsTransferStokInput_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsPartsTransferStok = $("#partsTransferStokInput_datatables tbody .row-select");
            var jmlPartsTransferStokCek = 0;
            var nRow;
            var idRec;
            rsPartsTransferStok.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsPartsTransferStokPerPage[idRec]=="1"){
                    jmlPartsTransferStokCek = jmlPartsTransferStokCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsPartsTransferStokPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecPartsTransferStokPerPage = rsPartsTransferStok.length;
            if(jmlPartsTransferStokCek==jmlRecPartsTransferStokPerPage && jmlRecPartsTransferStokPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"aoColumns": [

{
	"sName": "goods",
	"mDataProp": "goods",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" id="idPopUp" value="'+row['id']+'">&nbsp;&nbsp'+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "goods2",
	"mDataProp": "goods2",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "hargaBeli",
	"mDataProp": "hargaBeli",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
 ,

{
	"sName": "Qreturn",
	"mDataProp": "Qreturn",
	"aTargets": [3],
	"bSearchable": true,
    "mRender": function ( data, type, row ) {
        return '<input id="qty-'+row['id']+'" class="inline-edit" type="text" style="width:218px;" value="'+data+'">';
    },
    "bSortable": true,
    "sWidth":"200px",
    "bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var t162ID = $('#filter_t162ID input').val();
						if(t162ID){
							aoData.push(
									{"name": 'sCriteria_t162ID', "value": t162ID}
							);
						}
	
						var goods = $('#filter_goods input').val();
						if(goods){
							aoData.push(
									{"name": 'sCriteria_goods', "value": goods}
							);
						}
	
						var t162Qty1 = $('#filter_t162Qty1 input').val();
						if(t162Qty1){
							aoData.push(
									{"name": 'sCriteria_t162Qty1', "value": t162Qty1}
							);
						}
	
						var t162Qty2 = $('#filter_t162Qty2 input').val();
						if(t162Qty2){
							aoData.push(
									{"name": 'sCriteria_t162Qty2', "value": t162Qty2}
							);
						}

						var t162TglPartsTransferStok = $('#search_t162TglPartsTransferStok').val();
						var t162TglPartsTransferStokDay = $('#search_t162TglPartsTransferStok_day').val();
						var t162TglPartsTransferStokMonth = $('#search_t162TglPartsTransferStok_month').val();
						var t162TglPartsTransferStokYear = $('#search_t162TglPartsTransferStok_year').val();
						
						if(t162TglPartsTransferStok){
							aoData.push(
									{"name": 'sCriteria_t162TglPartsTransferStok', "value": "date.struct"},
									{"name": 'sCriteria_t162TglPartsTransferStok_dp', "value": t162TglPartsTransferStok},
									{"name": 'sCriteria_t162TglPartsTransferStok_day', "value": t162TglPartsTransferStokDay},
									{"name": 'sCriteria_t162TglPartsTransferStok_month', "value": t162TglPartsTransferStokMonth},
									{"name": 'sCriteria_t162TglPartsTransferStok_year', "value": t162TglPartsTransferStokYear}
							);
						}


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#partsTransferStokInput_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsPartsTransferStokPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsPartsTransferStokPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#partsTransferStokInput_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsPartsTransferStokPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anPartsTransferStokSelected = partsTransferStokTable.$('tr.row_selected');

            if(jmlRecPartsTransferStokPerPage == anPartsTransferStokSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsPartsTransferStokPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
    if(cekStatus=="ubah"){
        var nomorPartsTransferStok = "${noPartsTransferStok}";
        $.ajax({
        url:'${request.contextPath}/partsTransferStokInput/getTableData',
    		type: "POST", // Always use POST when deleting data
    		data: { id: nomorPartsTransferStok },
    		success : function(data){
    		    $.each(data,function(i,item){
                    partsTransferStokTable.fnAddData({
                        'id': item.id,
						'goods': item.kode,
						'goods2': item.nama,
						'hargaBeli': item.hargaBeli,
 						'Qreturn': item.Qty
                    });
                });
            }
		});
    }
});
</g:javascript>


			
