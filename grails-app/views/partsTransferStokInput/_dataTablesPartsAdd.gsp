
<%@ page import="com.kombos.parts.PartsTransferStok" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="PartsTransferStokAdd_datatables_${idTable}" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover fixed" style="table-layout: fixed; width: 900px">
    <col width="200px" />
    <col width="200px" />
    <col width="100px" />
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div>
                &nbsp;<input type="checkbox" class="pull-left select-all" aria-label="Select all" title="Select all"/>
                <g:message code="PartsTransferStokAdd.goods.label" default="Kode Parts" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="PartsTransferStokAdd.goods.label" default="Nama Parts" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="PartsTransferStokAdd.goods.label" default="Stok Parts" /></div>
        </th>

     </tr>
    <tr>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_goods" style="padding-top: 0px;position:relative; margin-top: 0px;width: 120px;">
                <input type="text" name="search_goods" class="search_init" />
            </div>
         </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_goods2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 120px;">
                <input type="text" name="search_goods2" class="search_init" />
            </div>
        </th>
        <th style="border-top: none;padding: 5px;">
            <div id="filter_stok" style="padding-top: 0px;position:relative; margin-top: 0px;width: 100px;">

            </div>
        </th>
     </tr>
    </thead>
</table>

<g:javascript>
var PartsTransferStokAddTable;
var reloadPartsTransferStokAddTable;
$(function(){

	reloadPartsTransferStokAddTable = function() {
		PartsTransferStokAddTable.fnDraw();
	}

	var recordsClaimPerPage = [];
    var anClaimSelected;
    var jmlRecClaimPerPage=0;
    var id;

	$('#search_tglinvoice').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tglinvoice_day').val(newDate.getDate());
			$('#search_tglinvoice_month').val(newDate.getMonth()+1);
			$('#search_tglinvoice_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			PartsTransferStokAddTable.fnDraw();
	});

	$('#search_tglpo').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tglpo_day').val(newDate.getDate());
			$('#search_tglpo_month').val(newDate.getMonth()+1);
			$('#search_tglpo_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			PartsTransferStokAddTable.fnDraw();
	});


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	PartsTransferStokAddTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});
  if(PartsTransferStokAddTable)
        PartsTransferStokAddTable.dataTable().fnDestroy();
	PartsTransferStokAddTable = $('#PartsTransferStokAdd_datatables_${idTable}').dataTable({
		"sScrollX": "900px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsClaim = $("#PartsTransferStokAdd_datatables tbody .row-select");
            var jmlClaimCek = 0;
            var nRow;
            var idRec;
            rsClaim.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsClaimPerPage[idRec]=="1"){
                    jmlClaimCek = jmlClaimCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsClaimPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecClaimPerPage = rsClaim.length;
            if(jmlClaimCek==jmlRecClaimPerPage && jmlRecClaimPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "dataTablesPartsAddList")}",
		"aoColumns": [

{
	"sName": "goods",
	"mDataProp": "goods",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "goods",
	"mDataProp": "goods2",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,


{
	"sName": "stok",
	"mDataProp": "stok",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                      aoData.push(
						    {"name": 'vendor', "value": "${vendor}"}
						);
 						var goods = $('#filter_goods input').val();
						if(goods){
							aoData.push(
									{"name": 'sCriteria_goods', "value": goods}
							);
						}
						var goods2 = $('#filter_goods2 input').val();
						if(goods2){
							aoData.push(
									{"name": 'sCriteria_goods2', "value": goods2}
							);
						}
                        var exist =[];
						var rows = $("#partsTransferStokInput_datatables").dataTable().fnGetNodes();
                        for(var i=0;i<rows.length;i++)
                        {
							var id = $(rows[i]).find("#idPopUp").val();
							exist.push(id);
						}
						if(exist.length > 0){
							aoData.push(
										{"name": 'sCriteria_exist', "value": JSON.stringify(exist)}
							);
						}
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

    $('.select-all').click(function(e) {

        $("#PartsTransferStokAdd_datatables_${idTable} tbody .row-select").each(function() {
            if(this.checked){
                recordsClaimPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsClaimPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#PartsTransferStokAdd_datatables_${idTable} tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsClaimPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anClaimSelected = PartsTransferStokAddTable.$('tr.row_selected');

            if(jmlRecClaimPerPage == anClaimSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsClaimPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});

</g:javascript>


			
