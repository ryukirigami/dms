<%@ page import="com.kombos.customerFollowUp.PertanyaanFu" %>

<g:javascript>
    $(document).ready(function() {
        if($('#m803Pertanyaan').val().length>0){
            $('#hitung').text(255 - $('#m803Pertanyaan').val().length);
        }
        $('#m803Pertanyaan').keyup(function() {
            var len = this.value.length;
            if (len >= 255) {
                this.value = this.value.substring(0, 255);
                len = 255;
            }
            $('#hitung').text(255 - len);
        });
    });
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: pertanyaanFuInstance, field: 'm803Pertanyaan', 'error')} required">
	<label class="control-label" for="m803Pertanyaan">
		<g:message code="pertanyaanFu.m803Pertanyaan.label" default="Pertanyaan" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:textArea name="m803Pertanyaan" id="m803Pertanyaan" value="${pertanyaanFuInstance?.m803Pertanyaan}" maxlength="255" required=""/>
        <br/>
        <span id="hitung">255</span> Karakter Tersisa.
	</div>
</div>

