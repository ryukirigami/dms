
<%@ page import="com.kombos.customerFollowUp.PertanyaanFu" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="pertanyaanFu_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div style="text-align: center"><g:message code="pertanyaanFu.m803Pertanyaan.label" default="Pertanyaan" /></div>
			</th>

		
		</tr>
		<tr>
		
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m803Pertanyaan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 100%;">
					<input type="text" name="search_m803Pertanyaan" class="search_init" style="width: 98%"/>
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var pertanyaanFuTable;
var reloadPertanyaanFuTable;
$(function(){
	
	reloadPertanyaanFuTable = function() {
		pertanyaanFuTable.fnDraw();
	}

    var recordspertanyaanFuperpage = [];
    var anPertanyaanFuSelected;
    var jmlRecPertanyaanFuPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	pertanyaanFuTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	pertanyaanFuTable = $('#pertanyaanFu_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsPertanyaanFu = $("#pertanyaanFu_datatables tbody .row-select");
            var jmlPertanyaanFuCek = 0;
            var nRow;
            var idRec;
            rsPertanyaanFu.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordspertanyaanFuperpage[idRec]=="1"){
                    jmlPertanyaanFuCek = jmlPertanyaanFuCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordspertanyaanFuperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecPertanyaanFuPerPage = rsPertanyaanFu.length;
            if(jmlPertanyaanFuCek==jmlRecPertanyaanFuPerPage && jmlRecPertanyaanFuPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m803Pertanyaan",
	"mDataProp": "m803Pertanyaan",
	"aTargets": [0],
    "mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100%",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m803Pertanyaan = $('#filter_m803Pertanyaan input').val();
						if(m803Pertanyaan){
							aoData.push(
									{"name": 'sCriteria_m803Pertanyaan', "value": m803Pertanyaan}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
    $('.select-all').click(function(e) {
        $("#pertanyaanFu_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordspertanyaanFuperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordspertanyaanFuperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

    $('#pertanyaanFu_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordspertanyaanFuperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anPertanyaanFuSelected = pertanyaanFuTable.$('tr.row_selected');
            if(jmlRecPertanyaanFuPerPage == anPertanyaanFuSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordspertanyaanFuperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
