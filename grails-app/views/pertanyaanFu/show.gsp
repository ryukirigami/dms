

<%@ page import="com.kombos.customerFollowUp.PertanyaanFu" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'pertanyaan.label', default: 'Pertanyaan')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deletePertanyaanFu;

$(function(){ 
	deletePertanyaanFu=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/pertanyaanFu/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadPertanyaanFuTable();
   				expandTableLayout('pertanyaanFu');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-pertanyaanFu" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="pertanyaanFu"
			class="table table-bordered table-hover">
			<tbody>

            <g:if test="${pertanyaanFuInstance?.m803Pertanyaan}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m803Pertanyaan-label" class="property-label"><g:message
                                code="pertanyaanFu.m803Pertanyaan.label" default="Pertanyaan" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m803Pertanyaan-label">
                        %{--<ba:editableValue
                                bean="${pertanyaanFuInstance}" field="m803Pertanyaan"
                                url="${request.contextPath}/PertanyaanFu/updatefield" type="text"
                                title="Enter m803Pertanyaan" onsuccess="reloadPertanyaanFuTable();" />--}%

                        <g:fieldValue bean="${pertanyaanFuInstance}" field="m803Pertanyaan"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${pertanyaanFuInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="pertanyaanFu.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${pertanyaanFuInstance}" field="dateCreated"
                                url="${request.contextPath}/PertanyaanFu/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadPertanyaanFuTable();" />--}%

                        <g:formatDate date="${pertanyaanFuInstance?.dateCreated}" format="dd MMMM yyyy , HH:mm:ss" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${pertanyaanFuInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="pertanyaanFu.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${pertanyaanFuInstance}" field="createdBy"
								url="${request.contextPath}/PertanyaanFu/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadPertanyaanFuTable();" />--}%
							
								<g:fieldValue bean="${pertanyaanFuInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${pertanyaanFuInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="pertanyaanFu.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${pertanyaanFuInstance}" field="lastUpdated"
                                url="${request.contextPath}/PertanyaanFu/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadPertanyaanFuTable();" />--}%

                        <g:formatDate date="${pertanyaanFuInstance?.lastUpdated}"  format="dd MMMM yyyy , HH:mm:ss"/>

                    </span></td>

                </tr>
            </g:if>


            <g:if test="${pertanyaanFuInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="pertanyaanFu.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${pertanyaanFuInstance}" field="updatedBy"
                                url="${request.contextPath}/PertanyaanFu/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadPertanyaanFuTable();" />--}%

                        <g:fieldValue bean="${pertanyaanFuInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>


            <g:if test="${pertanyaanFuInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="pertanyaanFu.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${pertanyaanFuInstance}" field="lastUpdProcess"
								url="${request.contextPath}/PertanyaanFu/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadPertanyaanFuTable();" />--}%
							
								<g:fieldValue bean="${pertanyaanFuInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('pertanyaanFu');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${pertanyaanFuInstance?.id}"
					update="[success:'pertanyaanFu-form',failure:'pertanyaanFu-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deletePertanyaanFu('${pertanyaanFuInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
