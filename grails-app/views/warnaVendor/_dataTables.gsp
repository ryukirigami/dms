
<%@ page import="com.kombos.administrasi.WarnaVendor" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

%{--<g:javascript>--}%
    %{--function isNumberKey(evt)--}%
    %{--{--}%
        %{--var charCode = (evt.which) ? evt.which : event.keyCode--}%
        %{--if (charCode > 31 && (charCode < 48 || charCode > 57))--}%
            %{--return false;--}%
        %{--return true;--}%
    %{--}--}%
%{--</g:javascript>--}%

<table id="warnaVendor_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="warnaVendor.vendorCat.label" default="Nama Vendor" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="warnaVendor.m192IDWarna.label" default="Kode Warna" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="warnaVendor.m192NamaWarna.label" default="Nama Warna" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="warnaVendor.createdBy.label" default="Created By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="warnaVendor.updatedBy.label" default="Updated By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="warnaVendor.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="warnaVendor.staDel.label" default="Sta Del" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_vendorCat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_vendorCat" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m192IDWarna" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m192IDWarna" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m192NamaWarna" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m192NamaWarna" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_createdBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_updatedBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_staDel" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var warnaVendorTable;
var reloadWarnaVendorTable;
$(function(){
	
	reloadWarnaVendorTable = function() {
		warnaVendorTable.fnDraw();
	}

        var recordsWarnaVendorperpage = [];
        var anWarnaVendorSelected;
        var jmlRecWarnaVendorPerPage=0;
        var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	warnaVendorTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	warnaVendorTable = $('#warnaVendor_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            //alert( $('li.active').text() );
            var rsWarnaVendor = $("#warnaVendor_datatables tbody .row-select");
            var jmlWarnaVendorCek = 0;
            var idRec;
            var nRow;
            rsWarnaVendor.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsWarnaVendorperpage[idRec]=="1"){
                    jmlWarnaVendorCek = jmlWarnaVendorCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsWarnaVendorperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecWarnaVendorPerPage = rsWarnaVendor.length;
            if(jmlWarnaVendorCek==jmlRecWarnaVendorPerPage && jmlRecWarnaVendorPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "vendorCat",
	"mDataProp": "vendorCat",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m192IDWarna",
	"mDataProp": "m192IDWarna",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m192NamaWarna",
	"mDataProp": "m192NamaWarna",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "createdBy",
	"mDataProp": "createdBy",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "updatedBy",
	"mDataProp": "updatedBy",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "staDel",
	"mDataProp": "staDel",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var vendorCat = $('#filter_vendorCat input').val();
						if(vendorCat){
							aoData.push(
									{"name": 'sCriteria_vendorCat', "value": vendorCat}
							);
						}
	
						var m192IDWarna = $('#filter_m192IDWarna input').val();
						if(m192IDWarna){
							aoData.push(
									{"name": 'sCriteria_m192IDWarna', "value": m192IDWarna}
							);
						}
	
						var m192NamaWarna = $('#filter_m192NamaWarna input').val();
						if(m192NamaWarna){
							aoData.push(
									{"name": 'sCriteria_m192NamaWarna', "value": m192NamaWarna}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

	     $('.select-all').click(function(e) {
            $("#warnaVendor_datatables tbody .row-select").each(function() {
                if(this.checked){
                    recordsWarnaVendorperpage[$(this).next("input:hidden").val()] = "1";
                } else {
                    recordsWarnaVendorperpage[$(this).next("input:hidden").val()] = "0";
                }
            });
        });

        $('#warnaVendor_datatables tbody tr').live('click', function () {
            id = $(this).find('.row-select').next("input:hidden").val();
            if($(this).find('.row-select').is(":checked")){
                recordsWarnaVendorperpage[id] = "1";
                $(this).find('.row-select').parent().parent().addClass('row_selected');
                anWarnaVendorSelected = warnaVendorTable.$('tr.row_selected');
                if(jmlRecWarnaVendorPerPage == anWarnaVendorSelected.length){
                    $('.select-all').attr('checked', true);
                }
            } else {
                recordsWarnaVendorperpage[id] = "0";
                $('.select-all').attr('checked', false);
                $(this).find('.row-select').parent().parent().removeClass('row_selected');
            }
        });

});
</g:javascript>


			
