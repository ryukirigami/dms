

<%@ page import="com.kombos.administrasi.WarnaVendor" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'warnaVendor.label', default: 'Master Warna Cat')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteWarnaVendor;

$(function(){ 
	deleteWarnaVendor=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/warnaVendor/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadWarnaVendorTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-warnaVendor" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="warnaVendor"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${warnaVendorInstance?.vendorCat}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="vendorCat-label" class="property-label"><g:message
					code="warnaVendor.vendorCat.label" default="Nama Vendor" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="vendorCat-label">
						%{--<ba:editableValue
								bean="${warnaVendorInstance}" field="vendorCat"
								url="${request.contextPath}/WarnaVendor/updatefield" type="text"
								title="Enter vendorCat" onsuccess="reloadWarnaVendorTable();" />--}%
							
								${warnaVendorInstance?.vendorCat?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${warnaVendorInstance?.m192IDWarna}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m192IDWarna-label" class="property-label"><g:message
					code="warnaVendor.m192IDWarna.label" default="Kode Warna" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m192IDWarna-label">
						%{--<ba:editableValue
								bean="${warnaVendorInstance}" field="m192IDWarna"
								url="${request.contextPath}/WarnaVendor/updatefield" type="text"
								title="Enter m192IDWarna" onsuccess="reloadWarnaVendorTable();" />--}%
							
								<g:fieldValue bean="${warnaVendorInstance}" field="m192IDWarna"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${warnaVendorInstance?.m192NamaWarna}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m192NamaWarna-label" class="property-label"><g:message
					code="warnaVendor.m192NamaWarna.label" default="Nama Warna" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m192NamaWarna-label">
						%{--<ba:editableValue
								bean="${warnaVendorInstance}" field="m192NamaWarna"
								url="${request.contextPath}/WarnaVendor/updatefield" type="text"
								title="Enter m192NamaWarna" onsuccess="reloadWarnaVendorTable();" />--}%
							
								<g:fieldValue bean="${warnaVendorInstance}" field="m192NamaWarna"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${warnaVendorInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="warnaVendor.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${warnaVendorInstance}" field="createdBy"
								url="${request.contextPath}/WarnaVendor/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadWarnaVendorTable();" />--}%
							
								<g:fieldValue bean="${warnaVendorInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${warnaVendorInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="warnaVendor.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${warnaVendorInstance}" field="updatedBy"
								url="${request.contextPath}/WarnaVendor/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadWarnaVendorTable();" />--}%
							
								<g:fieldValue bean="${warnaVendorInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${warnaVendorInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="warnaVendor.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${warnaVendorInstance}" field="dateCreated"
								url="${request.contextPath}/WarnaVendor/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadWarnaVendorTable();" />--}%
							
								%{--<g:formatDate date="${warnaVendorInstance?.dateCreated}" />--}%
                        <g:formatDate date="${warnaVendorInstance?.dateCreated}" type="datetime" style="MEDIUM"  />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${warnaVendorInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="warnaVendor.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${warnaVendorInstance}" field="lastUpdated"
								url="${request.contextPath}/WarnaVendor/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadWarnaVendorTable();" />--}%
							
								%{--<g:formatDate date="${warnaVendorInstance?.lastUpdated}" />--}%
                        <g:formatDate date="${warnaVendorInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />
						</span></td>
					
				</tr>
				</g:if>
				
				<g:if test="${warnaVendorInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="warnaVendor.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${warnaVendorInstance}" field="lastUpdProcess"
								url="${request.contextPath}/WarnaVendor/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadWarnaVendorTable();" />--}%
							
								<g:fieldValue bean="${warnaVendorInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${warnaVendorInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="warnaVendor.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						<ba:editableValue
								bean="${warnaVendorInstance}" field="staDel"
								url="${request.contextPath}/WarnaVendor/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadWarnaVendorTable();" />
							
								<g:fieldValue bean="${warnaVendorInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>--}%
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${warnaVendorInstance?.id}"
					update="[success:'warnaVendor-form',failure:'warnaVendor-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteWarnaVendor('${warnaVendorInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
