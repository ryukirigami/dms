<%@ page import="com.kombos.parts.Satuan" %>


<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57)){
            if(charCode == 46){
                return true
            }
            return false;
        }


        return true;
    }
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: satuanInstance, field: 'm118KodeSatuan', 'error')} required">
	<label class="control-label" for="m118KodeSatuan">
		<g:message code="satuan.m118KodeSatuan.label" default="Kode Satuan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m118KodeSatuan" maxlength="50" required="" value="${satuanInstance?.m118KodeSatuan}" />
	</div>
</div>

<g:javascript>
    document.getElementById("m118KodeSatuan").focus();
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: satuanInstance, field: 'm118Satuan1', 'error')} required">
	<label class="control-label" for="m118Satuan1">
		<g:message code="satuan.m118Satuan1.label" default="Satuan 1" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m118Satuan1" maxlength="50" required="" value="${satuanInstance?.m118Satuan1}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: satuanInstance, field: 'm118Satuan2', 'error')} required">
	<label class="control-label" for="m118Satuan2">
		<g:message code="satuan.m118Satuan2.label" default="Satuan 2" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m118Satuan2" maxlength="50" required="" value="${satuanInstance?.m118Satuan2}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: satuanInstance, field: 'm118Konversi', 'error')} required">
	<label class="control-label" for="m118Konversi">
		<g:message code="satuan.m118Konversi.label" default="Konversi" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m118Konversi" value="${fieldValue(bean: satuanInstance, field: 'm118Konversi')}" maxlength="8" onkeypress="return isNumberKey(event)" required=""/>
	</div>
</div>
