
<%@ page import="com.kombos.parts.Satuan" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</g:javascript>

<table id="satuan_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="satuan.m118KodeSatuan.label" default="Kode Satuan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="satuan.m118Satuan1.label" default="Satuan 1" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="satuan.m118Satuan2.label" default="Satuan 2" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="satuan.m118Konversi.label" default="Konversi" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;  display: hidden">
				<div><g:message code="satuan.staDel.label" default="Sta Del" /></div>
			</th>


            <th style="border-bottom: none;padding: 5px; display: hidden">
                <div><g:message code="satuan.m118ID.label" default="M118 ID" /></div>
            </th>

			<th style="border-bottom: none;padding: 5px;  display: hidden">
				<div><g:message code="satuan.createdBy.label" default="Created By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;  display: hidden">
				<div><g:message code="satuan.updatedBy.label" default="Updated By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; display: hidden">
				<div><g:message code="satuan.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>

		
		</tr>
		<tr>
		
	

	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m118KodeSatuan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m118KodeSatuan" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m118Satuan1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m118Satuan1" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m118Satuan2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m118Satuan2" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m118Konversi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m118Konversi" onkeypress="return isNumberKey(event)" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;  display: hidden">
                <div id="filter_m118ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_m118ID" class="search_init" />
                </div>
            </th>
	
			<th style="border-top: none;padding: 5px; display: hidden">
				<div id="filter_staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_staDel" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px; display: hidden">
				<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_createdBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px; display: hidden">
				<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_updatedBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px; display: hidden">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var satuanTable;
var reloadSatuanTable;
$(function(){
	
	reloadSatuanTable = function() {
		satuanTable.fnDraw();
	}

    var recordssatuanperpage = [];
    var anSatuanSelected;
    var jmlRecSatuanPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	satuanTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	satuanTable = $('#satuan_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsSatuan = $("#satuan_datatables tbody .row-select");
            var jmlSatuanCek = 0;
            var nRow;
            var idRec;
            rsSatuan.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordssatuanperpage[idRec]=="1"){
                    jmlSatuanCek = jmlSatuanCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordssatuanperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecSatuanPerPage = rsSatuan.length;
            if(jmlSatuanCek==jmlRecSatuanPerPage && jmlRecSatuanPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [



{
	"sName": "m118KodeSatuan",
	"mDataProp": "m118KodeSatuan",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m118Satuan1",
	"mDataProp": "m118Satuan1",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m118Satuan2",
	"mDataProp": "m118Satuan2",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m118Konversi",
	"mDataProp": "m118Konversi",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m118ID",
	"mDataProp": "m118ID",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": false
}

,

{
	"sName": "staDel",
	"mDataProp": "staDel",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "createdBy",
	"mDataProp": "createdBy",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "updatedBy",
	"mDataProp": "updatedBy",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m118ID = $('#filter_m118ID input').val();
						if(m118ID){
							aoData.push(
									{"name": 'sCriteria_m118ID', "value": m118ID}
							);
						}
	
						var m118KodeSatuan = $('#filter_m118KodeSatuan input').val();
						if(m118KodeSatuan){
							aoData.push(
									{"name": 'sCriteria_m118KodeSatuan', "value": m118KodeSatuan}
							);
						}
	
						var m118Satuan1 = $('#filter_m118Satuan1 input').val();
						if(m118Satuan1){
							aoData.push(
									{"name": 'sCriteria_m118Satuan1', "value": m118Satuan1}
							);
						}
	
						var m118Satuan2 = $('#filter_m118Satuan2 input').val();
						if(m118Satuan2){
							aoData.push(
									{"name": 'sCriteria_m118Satuan2', "value": m118Satuan2}
							);
						}
	
						var m118Konversi = $('#filter_m118Konversi input').val();
						if(m118Konversi){
							aoData.push(
									{"name": 'sCriteria_m118Konversi', "value": m118Konversi}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

	$('.select-all').click(function(e) {
        $("#satuan_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordssatuanperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordssatuanperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

	$('#satuan_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordssatuanperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anSatuanSelected = satuanTable.$('tr.row_selected');
            if(jmlRecSatuanPerPage == anSatuanSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordssatuanperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
