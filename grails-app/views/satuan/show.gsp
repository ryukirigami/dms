

<%@ page import="com.kombos.parts.Satuan" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'satuan.label', default: 'Satuan')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteSatuan;

$(function(){ 
	deleteSatuan=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/satuan/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadSatuanTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-satuan" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="satuan"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${satuanInstance?.m118KodeSatuan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m118KodeSatuan-label" class="property-label"><g:message
					code="satuan.m118KodeSatuan.label" default="Kode Satuan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m118KodeSatuan-label">
						%{--<ba:editableValue
								bean="${satuanInstance}" field="m118KodeSatuan"
								url="${request.contextPath}/Satuan/updatefield" type="text"
								title="Enter m118KodeSatuan" onsuccess="reloadSatuanTable();" />--}%
							
								<g:fieldValue bean="${satuanInstance}" field="m118KodeSatuan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${satuanInstance?.m118Satuan1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m118Satuan1-label" class="property-label"><g:message
					code="satuan.m118Satuan1.label" default="Satuan 1" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m118Satuan1-label">
						%{--<ba:editableValue
								bean="${satuanInstance}" field="m118Satuan1"
								url="${request.contextPath}/Satuan/updatefield" type="text"
								title="Enter m118Satuan1" onsuccess="reloadSatuanTable();" />--}%
							
								<g:fieldValue bean="${satuanInstance}" field="m118Satuan1"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${satuanInstance?.m118Satuan2}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m118Satuan2-label" class="property-label"><g:message
					code="satuan.m118Satuan2.label" default="Satuan 2" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m118Satuan2-label">
						%{--<ba:editableValue
								bean="${satuanInstance}" field="m118Satuan2"
								url="${request.contextPath}/Satuan/updatefield" type="text"
								title="Enter m118Satuan2" onsuccess="reloadSatuanTable();" />--}%
							
								<g:fieldValue bean="${satuanInstance}" field="m118Satuan2"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${satuanInstance?.m118Konversi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m118Konversi-label" class="property-label"><g:message
					code="satuan.m118Konversi.label" default="Konversi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m118Konversi-label">
						%{--<ba:editableValue
								bean="${satuanInstance}" field="m118Konversi"
								url="${request.contextPath}/Satuan/updatefield" type="text"
								title="Enter m118Konversi" onsuccess="reloadSatuanTable();" />--}%
							
								<g:fieldValue bean="${satuanInstance}" field="m118Konversi"/>
							
						</span></td>
					
				</tr>
				</g:if>


            <g:if test="${satuanInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="satuan.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${satuanInstance}" field="dateCreated"
                                url="${request.contextPath}/satuan/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadsatuanTable();" />--}%

                        <g:formatDate date="${satuanInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${satuanInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="satuan.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${satuanInstance}" field="createdBy"
                                url="${request.contextPath}/satuan/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadsatuanTable();" />--}%

                        <g:fieldValue bean="${satuanInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${satuanInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="satuan.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${satuanInstance}" field="lastUpdated"
                                url="${request.contextPath}/satuan/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadsatuanTable();" />--}%

                        <g:formatDate date="${satuanInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${satuanInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="satuan.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${satuanInstance}" field="updatedBy"
                                url="${request.contextPath}/satuan/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadsatuanTable();" />--}%

                        <g:fieldValue bean="${satuanInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${satuanInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="satuan.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${satuanInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/satuan/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadsatuanTable();" />--}%

                        <g:fieldValue bean="${satuanInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>


            %{--<g:if test="${satuanInstance?.staDel}">--}%
                %{--<tr>--}%
                    %{--<td class="span2" style="text-align: right;"><span--}%
                            %{--id="staDel-label" class="property-label"><g:message--}%
                                %{--code="satuan.staDel.label" default="Sta Del" />:</span></td>--}%

                    %{--<td class="span3"><span class="property-value"--}%
                                            %{--aria-labelledby="staDel-label">--}%
                        %{--<ba:editableValue--}%
                                %{--bean="${satuanInstance}" field="staDel"--}%
                                %{--url="${request.contextPath}/Satuan/updatefield" type="text"--}%
                                %{--title="Enter staDel" onsuccess="reloadSatuanTable();" />--}%

                        %{--<g:fieldValue bean="${satuanInstance}" field="staDel"/>--}%

                    %{--</span></td>--}%

                %{--</tr>--}%
            %{--</g:if>--}%
            %{----}%
            %{--<g:if test="${satuanInstance?.m118ID}">--}%
                %{--<tr>--}%
                    %{--<td class="span2" style="text-align: right;"><span--}%
                            %{--id="m118ID-label" class="property-label"><g:message--}%
                                %{--code="satuan.m118ID.label" default="M118 ID" />:</span></td>--}%

                    %{--<td class="span3"><span class="property-value"--}%
                                            %{--aria-labelledby="m118ID-label">--}%
                        %{--<ba:editableValue--}%
                                %{--bean="${satuanInstance}" field="m118ID"--}%
                                %{--url="${request.contextPath}/Satuan/updatefield" type="text"--}%
                                %{--title="Enter m118ID" onsuccess="reloadSatuanTable();" />--}%

                        %{--<g:fieldValue bean="${satuanInstance}" field="m118ID"/>--}%

                    %{--</span></td>--}%

                %{--</tr>--}%
            %{--</g:if>--}%



            </tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${satuanInstance?.id}"
					update="[success:'satuan-form',failure:'satuan-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteSatuan('${satuanInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
