
<%@ page import="com.kombos.finance.CashBalance" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="cashBalance_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="cashBalance.requiredDate.label" default="Required Date" /></div>
            </th>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="cashBalance.cashBalance.label" default="Cash Balance" /></div>
			</th>
		</tr>
	</thead>
</table>

<g:javascript>
var cashBalanceTable;
var reloadCashBalanceTable;
$(function(){
	
	reloadCashBalanceTable = function() {
		cashBalanceTable.fnDraw();
	}

	
	$('#search_requiredDate').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_requiredDate_day').val(newDate.getDate());
			$('#search_requiredDate_month').val(newDate.getMonth()+1);
			$('#search_requiredDate_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	cashBalanceTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	cashBalanceTable = $('#cashBalance_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "requiredDate",
	"mDataProp": "requiredDate",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true,
	"mRender": function ( data, type, row ) {
//		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="edit('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	}
}
,

{
	"sName": "cashBalance",
	"mDataProp": "cashBalance",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}



],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var cashBalance = $('#filter_cashBalance input').val();
						if(cashBalance){
							aoData.push(
									{"name": 'sCriteria_cashBalance', "value": cashBalance}
							);
						}

						var requiredDate = $('#search_requiredDate').val();
						var requiredDateDay = $('#search_requiredDate_day').val();
						var requiredDateMonth = $('#search_requiredDate_month').val();
						var requiredDateYear = $('#search_requiredDate_year').val();
						
						if(requiredDate){
							aoData.push(
									{"name": 'sCriteria_requiredDate', "value": "date.struct"},
									{"name": 'sCriteria_requiredDate_dp', "value": requiredDate},
									{"name": 'sCriteria_requiredDate_day', "value": requiredDateDay},
									{"name": 'sCriteria_requiredDate_month', "value": requiredDateMonth},
									{"name": 'sCriteria_requiredDate_year', "value": requiredDateYear}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
