
<%@ page import="com.kombos.finance.CashBalance" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'cashBalance.label', default: 'CashBalance')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout, baseapplist, autoNumeric" />
    <g:javascript>
            function backtoCB(){
                $('#spinner').fadeIn(1);
                $.ajax({
                    url: '${request.contextPath}/cashBalance',
                    type: "GET",dataType:"html",
                    complete : function (req, err) {
                        $('#main-content').html(req.responseText);
                        $('#spinner').fadeOut();
                    }
                });
            }

            function doMigrate(){
                var recordsInv = [];

                $("#invoiceT701_datatables .row-select").each(function() {
                    if(this.checked){
                        var id = $(this).next("input:hidden").val();
                        recordsInv.push(id);
                    }
                });


                if(recordsInv.length < 1){
                    alert('Anda belum memilih data invoice');
                    return
                }
                if(recordsInv.length > 1){
                    alert('Anda hanya dapat memili 1 data');
                    return
                }
                var jsonInv = JSON.stringify(recordsInv);

                $.ajax({
                    url:'${request.contextPath}/cashBalance/doMigrateInvoice',
                    type: "POST", // Always use POST when deleting data
                    data: {jsonInv : jsonInv},
                    success: function(data) {
                        if(data=="ada"){
                            toastr.success('<div>Data Saved.</div>');
                            reloadInvoiceT701Table();
                        }else{
                            toastr.error('Data gagal disimpan');
                        }
                    },error:function(){
                        alert("Internal Server Error");
                    }
                });

            }
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left">
            Invoice Tunai Belum Bayar (<%print(tgglCari.format("dd/MM/yyyy"))%>)
    </span>
    <ul class="nav pull-right">
        %{--<li><a class="pull-right box-action" href="#"--}%
               %{--style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i--}%
                    %{--class="icon-plus"></i>&nbsp;&nbsp;--}%
        %{--</a></li>--}%
        %{--<li><a class="pull-right box-action" href="#"--}%
               %{--style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i--}%
                    %{--class="icon-remove"></i>&nbsp;&nbsp;--}%
        %{--</a></li>--}%
        <li class="separator"></li>
    </ul>
</div>
<div class="box">
    <div class="span12" id="invoiceT701-table" style="margin-left: 0; padding-left: 0;">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <a class="btn btn-primary" href="javascript:void(0);" id="btnCari" onclick="backtoCB();">Kembali</a>
        <a class="btn" href="javascript:void(0);" onclick="doMigrate();" >Ubah jadi Invoice Kredit (Approval)</a>

        <g:render template="dataTablesInvoice" />
    </div>
    <div class="span7" id="invoiceT701-form" style="display: none;"></div>
</div>
</body>
</html>
