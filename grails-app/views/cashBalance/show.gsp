

<%@ page import="com.kombos.finance.CashBalance" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'cashBalance.label', default: 'CashBalance')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteCashBalance;

$(function(){ 
	deleteCashBalance=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/cashBalance/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadCashBalanceTable();
   				expandTableLayout('cashBalance');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-cashBalance" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="cashBalance"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${cashBalanceInstance?.cashBalance}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="cashBalance-label" class="property-label"><g:message
					code="cashBalance.cashBalance.label" default="Cash Balance" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="cashBalance-label">
						%{--<ba:editableValue
								bean="${cashBalanceInstance}" field="cashBalance"
								url="${request.contextPath}/CashBalance/updatefield" type="text"
								title="Enter cashBalance" onsuccess="reloadCashBalanceTable();" />--}%
							
								<g:fieldValue bean="${cashBalanceInstance}" field="cashBalance"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cashBalanceInstance?.cashBalanceDetail}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="cashBalanceDetail-label" class="property-label"><g:message
					code="cashBalance.cashBalanceDetail.label" default="Cash Balance Detail" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="cashBalanceDetail-label">
						%{--<ba:editableValue
								bean="${cashBalanceInstance}" field="cashBalanceDetail"
								url="${request.contextPath}/CashBalance/updatefield" type="text"
								title="Enter cashBalanceDetail" onsuccess="reloadCashBalanceTable();" />--}%
							
								<g:each in="${cashBalanceInstance.cashBalanceDetail}" var="c">
								<g:link controller="cashBalanceDetail" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link>
								</g:each>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cashBalanceInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="cashBalance.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${cashBalanceInstance}" field="createdBy"
								url="${request.contextPath}/CashBalance/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadCashBalanceTable();" />--}%
							
								<g:fieldValue bean="${cashBalanceInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cashBalanceInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="cashBalance.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${cashBalanceInstance}" field="dateCreated"
								url="${request.contextPath}/CashBalance/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadCashBalanceTable();" />--}%
							
								<g:formatDate date="${cashBalanceInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cashBalanceInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="cashBalance.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${cashBalanceInstance}" field="lastUpdProcess"
								url="${request.contextPath}/CashBalance/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadCashBalanceTable();" />--}%
							
								<g:fieldValue bean="${cashBalanceInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cashBalanceInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="cashBalance.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${cashBalanceInstance}" field="lastUpdated"
								url="${request.contextPath}/CashBalance/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadCashBalanceTable();" />--}%
							
								<g:formatDate date="${cashBalanceInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cashBalanceInstance?.requiredDate}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="requiredDate-label" class="property-label"><g:message
					code="cashBalance.requiredDate.label" default="Required Date" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="requiredDate-label">
						%{--<ba:editableValue
								bean="${cashBalanceInstance}" field="requiredDate"
								url="${request.contextPath}/CashBalance/updatefield" type="text"
								title="Enter requiredDate" onsuccess="reloadCashBalanceTable();" />--}%
							
								<g:formatDate date="${cashBalanceInstance?.requiredDate}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cashBalanceInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="cashBalance.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${cashBalanceInstance}" field="staDel"
								url="${request.contextPath}/CashBalance/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadCashBalanceTable();" />--}%
							
								<g:fieldValue bean="${cashBalanceInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cashBalanceInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="cashBalance.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${cashBalanceInstance}" field="updatedBy"
								url="${request.contextPath}/CashBalance/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadCashBalanceTable();" />--}%
							
								<g:fieldValue bean="${cashBalanceInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('cashBalance');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${cashBalanceInstance?.id}"
					update="[success:'cashBalance-form',failure:'cashBalance-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteCashBalance('${cashBalanceInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
