
<%@ page import="com.kombos.finance.CashBalance" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'cashBalance.label', default: 'CashBalance')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist, autoNumeric" />
		<g:javascript>
			var show;
			var edit;
			$(function(){
            <g:if test="${noLastMB}">
                $("#alertMB").show();
            </g:if>

            $('.box-action').click(function(){
                switch($(this).attr('target')){
                    case '_CREATE_' :
                        /*shrinkTableLayout('cashBalance');*/
                        $("#cashBalance-table").fadeOut();
                        $("#cashBalance-form").fadeIn();
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('cashBalance', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('cashBalance', '${request.contextPath}/cashBalance/massdelete', reloadCashBalanceTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('cashBalance','${request.contextPath}/cashBalance/show/'+id);
				};
				
				edit = function(id) {
					$("#cashBalance-table").fadeOut();
                    $("#cashBalance-form").fadeIn();
                    $('#spinner').fadeIn(1);
                    $.ajax({type:'POST', url:'${request.contextPath}/cashBalance/edit/'+id,
                        success:function(data,textStatus){
                            loadForm(data, textStatus);
                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
				};

				loadForm = function(data, textStatus){
                    $('#cashBalance-form').empty();
                    $('#cashBalance-form').append(data);
                }

});
            function doSearchEOD(){
                reloadCashBalanceTable();
            }

            function unpaidInvoice(){
                var tgglCari = null;
                var tggl = $("#requiredDate").val();
                if(tggl){
                    tgglCari = tggl;
                }
                $('#spinner').fadeIn(1);
                $.ajax({
                    url: '${request.contextPath}/cashBalance/listInvoice',
                    data : {tgglCari : tgglCari},
                    type: "GET",
                    dataType:"html",
                    complete : function (req, err) {
                        $('#main-content').html(req.responseText);
                        $('#spinner').fadeOut();
                    }
                });
            }
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="cashBalance-table" style="margin-left: 0; padding-left: 0;">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            %{--<br/>--}%
            <div class="span4">
                <fieldset class="form">
                    <div id="filter_requiredDate" class="control-group fieldcontain">
                        <label for="search_requiredDate" class="control-label">Cari Berdasar Tanggal :</label>
                        <input type="hidden" name="search_requiredDate" value="date.struct">
                        <input type="hidden" name="search_requiredDate_day" id="search_requiredDate_day" value="">
                        <input type="hidden" name="search_requiredDate_month" id="search_requiredDate_month" value="">
                        <input type="hidden" name="search_requiredDate_year" id="search_requiredDate_year" value="">
                        <input type="text" data-date-format="dd/mm/yyyy" name="search_requiredDate_dp" value="" id="search_requiredDate" class="search_init">
                    </div>
                </fieldset>
                <a class="btn btn-primary" href="javascript:void(0);" id="btnCari" onclick="doSearchEOD();">Search</a>
                <a class="btn" href="javascript:void(0);" id="btnDetail" onclick="unpaidInvoice();">Daftar Invoice Tunai (Belum Bayar)</a>
            </div>

            <div id="alertMB" style="color: red;font-size: 18px;display: none">
                Belum ada data EOM(End Of Month) Bulan Sebelumnya
            </div>

            <g:render template="dataTables" />
		</div>
		<div class="span7" id="cashBalance-form" style="display: none;"></div>
	</div>
</body>
</html>
