<%@ page import="com.kombos.finance.CashBalance" %>
<g:javascript>
            var cancel;
            $(function() {
                cancel = function() {
                    $("#cashBalance-table").fadeIn();
                    $("#cashBalance-form").fadeOut();
                    reloadCashBalanceTable();
                }


                $("input[type='text'].inputan").focusout(function() {
                    var nilai = $(this).data("value");
                    var input = $(this).val().replace(new RegExp(',', 'g'), '');
                    console.log("NILAI : "+nilai);
                    console.log("INPUT : "+input);
                    if (nilai && input) {
                        var iNilai = new Number(nilai);
                        var iInput = new Number(input);
                        var jumlah = iNilai * iInput;

                        $(this).parent().parent().find("td:last-child input").val(jumlah);
                    }else{
                        $(this).parent().parent().find("td:last-child input").val(0);
                    }

                    var total = new Number(0);
                    var target = $(this).data("target");

                    if (target == "kertas") {
                        $("#pecahanUang-table tbody#kertas").find("tr").each(function(k,v) {
                            var jumlahEachRow  = $(this).find("td:last-child input").val().replace(new RegExp(',', 'g'), '');
                            var iJumlahEachRow = new Number(jumlahEachRow);
                            total= total+iJumlahEachRow;
                        });

                        $("#totalUangKertas").val(total);
                    } else {
                        $("#pecahanUang-table tbody#logam").find("tr").each(function(k,v) {
                            var jumlahEachRow  = $(this).find("td:last-child input").val().replace(new RegExp(',', 'g'), '');
                            var iJumlahEachRow = new Number(jumlahEachRow);
                            total= total+iJumlahEachRow;
                        });

                        $("#totalUangLogam").val(total);
                    }

//                    var find = ',';
                    var saldoAkhirKas   = new Number($("#saldoAkhirKas").val().replace(new RegExp(',', 'g'), ''));
                    var totalUangKertas = new Number($("#totalUangKertas").val().replace(new RegExp(',', 'g'), ''));
                    var totalUangLogam  = new Number($("#totalUangLogam").val().replace(new RegExp(',', 'g'), ''));

                    var jumlah   = new Number(0);
                    var totalKas = new Number(0);
                    jumlah = totalUangKertas + totalUangLogam;
                    $("#jumlahSemua").autoNumeric('set',jumlah);


                    totalKas = saldoAkhirKas - jumlah;
                    if(totalKas<0){
                        // totalKas = totalKas * -1;
                    }
                    $("#selisihSaldoFisik").autoNumeric('set',totalKas);

                    if(totalKas >= 20000){
                        document.getElementById('pembulatan').readOnly = true;
                        $("#pembulatan").autoNumeric('set',Math.round(totalKas));
                    }else{
                        document.getElementById('pembulatan').readOnly = false;
                        $("#pembulatan").val("0");
                    }

                })

                $("#btnSimpan").click(function() {
                    var arr = [];
                    if ($("#requiredDate").val() != "") {
                        var reqDate = $("#requiredDate").val();
                        $.post("${request.getContextPath()}/cashBalance/cekTanggal", {
                            tanggal: reqDate,

                        }, function(data) {
                           if(data=="true"){
                           	alert ("Tanggal sudah ada");
                           }else{
                           		var cashBal = $("#pembulatan").val().replace(new RegExp(',', 'g'), '');
							$("tbody#kertas").find("tr").each(function(k,v) {
								var puName = $(this).find("td:first-child").html();
								var puVal  = $(this).find("td.quantity input").val().replace(new RegExp(',', 'g'), '');
								arr.push({
									pecahanUang: puName,
									quantity: puVal,
									tipe: "Kertas"
								});
							});
							$("tbody#logam").find("tr").each(function(k,v) {
								var puName = $(this).find("td:first-child").html();
								var puVal  = $(this).find("td.quantity input").val().replace(new RegExp(',', 'g'), '');
								arr.push({
									pecahanUang: puName,
									quantity: puVal,
									tipe: "Logam"
								});
							});
							$.post("${request.getContextPath()}/cashBalance/save", {
								details: JSON.stringify(arr),
								requiredDate: reqDate,
								cashBalance: cashBal
							}, function(data) {
							   if (data.result == "SUCCESS") {
								   toastr.success("Save Sukses");
								   $("#cashBalance-table").fadeIn();
								   $("#cashBalance-form").fadeOut();
								   reloadCashBalanceTable();
							   } else {
									alert(data.result);
									$("#btnDetail2").show();
							   }
							});
                           }
                       });
                	}
                })

            });

</g:javascript>
<!DOCTYPE html>
<html>
<head>
        <meta name="layout" content="main">
        <g:set var="entityName" value="${message(code: 'cashBalance.label', default: 'CashBalance')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="create-cashBalance" class="content scaffold-create" role="main">
			<legend><g:message code="default.create.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${cashBalanceInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${cashBalanceInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form action="save" >--}%
			<g:formRemote class="form-horizontal" name="create" on404="alert('not found!');" onSuccess="reloadCashBalanceTable();" update="cashBalance-form"
              url="[controller: 'cashBalance', action:'save']">	
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons controls">
					<a class="btn cancel" href="javascript:void(0);"
                       onclick="cancel();"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
					<a class="btn btn-primary" href="javascript:void(0);" id="btnSimpan">Simpan</a>
					<a class="btn" href="javascript:void(0);" style="display: none" id="btnDetail2" onclick="unpaidInvoice();">Daftar Invoice Tunai (Belum Bayar)</a>
				</fieldset>
			</g:formRemote>
			%{--</g:form>--}%
		</div>
	</body>
</html>
