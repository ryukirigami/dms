
<%@ page import="com.kombos.maintable.InvoiceT701" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="invoiceT701_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="100%">
    <thead>
    <tr>
        <th style="border-bottom: none;padding: 5px;">
            Nomor Invoice
        </th>
        <th style="border-bottom: none;padding: 5px;">
            Tanggal Invoice
        </th>
        <th style="border-bottom: none;padding: 5px;">
            Nomor WO
        </th>
        <th style="border-bottom: none;padding: 5px;">
            Nama SA
        </th>
        <th style="border-bottom: none;padding: 5px;">
            Nama Customer
        </th>
        <th style="border-bottom: none;padding: 5px;">
            Jumlah Invoice
        </th>        
    </tr>
    </thead>
</table>

<g:javascript>
var invoiceT701Table;
var reloadInvoiceT701Table;

$(function(){

	reloadInvoiceT701Table = function() {
		invoiceT701Table.fnDraw();
	}

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	invoiceT701Table.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	invoiceT701Table = $('#invoiceT701_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesListInvoice")}",
		"aoColumns": [

{
	"sName": "t701NoInv",
	"mDataProp": "t701NoInv",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
        if(row['t701StaApproveMigrate']!="2"){
    		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
        }else{
            return '&nbsp;<input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
        }
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "tglInvoice",
	"mDataProp": "tglInvoice",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t401NoWO",
	"mDataProp": "t401NoWO",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t401NamaSA",
	"mDataProp": "t401NamaSA",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t701Customer",
	"mDataProp": "t701Customer",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "t701TotalBayarRp",
	"mDataProp": "t701TotalBayarRp",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var t701NoInv = $('#filter_m061ID input').val();
						if(t701NoInv){
							aoData.push(
									{"name": 'sCriteria_m061ID', "value": t701NoInv}
							);
						}

						var tgglCari = "${tgglCari.format("dd/MM/yyyy")}";
						if(tgglCari){
							aoData.push(
									{"name": 'tgglCari', "value": tgglCari}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>



