<%@ page import="com.kombos.finance.CashBalance" %>

<g:javascript>
    var checkin = $('#requiredDate').datepicker({}).on('changeDate', function(ev) {
        reloadForm();
    }).data('datepicker');

    function reloadForm(){
        var tanggal = $("#requiredDate").val();
        var params = {
            tanggal: tanggal
        }
        $.post("${request.getContextPath()}/cashBalance/changeDate", params, function(data) {
            if (!data.error){
                $("#saldoAwalKas").val(data.saldoAkhirKasKemarin);
                $("#totalMutasiOp").val(data.totalMutasiOpHariIni);
                $("#totalMutasiNonOp").val(data.totalMutasiNonOpHariIni);
                $("#saldoAkhirKas").val(data.saldoAkhirKas);
            }else{
                alert("Internal Server Error");
            }
        });
    }

</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: cashBalanceInstance, field: 'requiredDate', 'error')} ">
    <label class="control-label" for="requiredDate">
        <g:message code="cashBalance.requiredDate.label" default="Tanggal Saldo" />

    </label>
    <div class="controls">
        <g:if test="${cashBalanceInstance?.id}">
            <g:textField name="txtTgl" value="${cashBalanceInstance?.requiredDate?.format("dd/MM/yyyy")}" readonly="" />
        </g:if>
        <g:else>
            <ba:datePicker name="requiredDate" id="requiredDate" precision="day" value="${cashBalanceInstance?.requiredDate ? cashBalanceInstance?.requiredDate : new java.util.Date()}" format="dd/MM/yyyy"/>
        </g:else>
    </div>
</div>

<div class="control-group fieldcontain">
    <label class="control-label" for="saldoAwalKas">Saldo Awal Kas</label>
    <div class="controls">
        <input type="text" style="text-align: right" class="numeric" id="saldoAwalKas" value="${saldoAkhirKasKemarin}" readonly>
    </div>
</div>

<div class="control-group fieldcontain">
    <label class="control-label" for="totalMutasiOp">Total Mutasi Hari ini (Operasional)</label>
    <div class="controls">
        <input type="text" style="text-align: right" class="numeric" id="totalMutasiOp" value="${totalMutasiOpHariIni}" readonly>
    </div>
</div>

<div class="control-group fieldcontain">
    <label class="control-label" for="totalMutasiNonOp">Total Mutasi Hari ini (Non Operasional)</label>
    <div class="controls">
        <input type="text" style="text-align: right" class="numeric" id="totalMutasiNonOp" value="${totalMutasiNonOpHariIni}" readonly>
    </div>
</div>

<div class="control-group fieldcontain">
    <label class="control-label" for="saldoAkhirKas">Saldo Akhir Kas</label>
    <div class="controls">
        <input type="text" style="text-align: right" class="numeric" id="saldoAkhirKas" value="${saldoAkhirKas}" readonly>
    </div>
</div>

%{--<div class="control-group fieldcontain ${hasErrors(bean: cashBalanceInstance, field: 'cashBalance', 'error')} ">
	<label class="control-label" for="cashBalance">
		<g:message code="cashBalance.cashBalance.label" default="Cash Balance" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="cashBalance" value="${cashBalanceInstance.cashBalance}" />
	</div>
</div>--}%

<table id="pecahanUang-table"
       border="0"
       class="display table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th colspan="3">Pecahan Uang</th>
            <th>Jumlah</th>
            <th>Total</th>
        </tr>
        <tr>
            <th colspan="2">Uang Tunai Kertas</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody id="kertas">
        <g:each in="${pecahanUangKertas}" var="i">
            <tr>
                <td colspan="2">${konversi.toRupiah(i.nilai)}</td>
                <td>X&nbsp;</td>
                <td class="quantity"><input type="text" class="inputan" data-target="kertas" id="kertas_${i?.nilai}" data-value="${i?.nilai}"> = Rp &nbsp;</td>
                <td><input type="text"  value="0" id="val_kertas_${i?.nilai}"  style="text-align: right" readonly> </td>
            </tr>
        </g:each>
    </tbody>
    <tr>
        <th colspan="3">Jumlah Uang Kertas</th>
        <th style="text-align: right;"><span style="margin-right: 29px;">= Rp</span></th>
        <th><input id="totalUangKertas" type="text"  style="text-align: right" value="0" readonly/> </th>
    </tr>
    <tr>
        <th colspan="5">Uang Tunai Logam</th>
    </tr>
    <tbody id="logam">
        <g:each in="${pecahanUangLogam}" var="l">
            <tr>
                <td colspan="2">${konversi.toRupiah(l.nilai)}</td>
                <td>X&nbsp;</td>
                <td class="quantity"><input type="text" class="inputan" data-target="logam" id="logam_${l?.nilai}" data-value="${l.nilai}"> = Rp &nbsp;</td>
                <td><input type="text"  value="0" id="val_logam_${l?.nilai}" style="text-align: right" readonly> </td>
            </tr>
        </g:each>
    </tbody>
    <tr>
        <th colspan="3">Jumlah Uang Logam</th>
        <th style="text-align: right;"><span style="margin-right: 29px;">= Rp</span></th>
        <th><input id="totalUangLogam" type="text" style="text-align: right" value="0" readonly/> </th>
    </tr>
</table>

<div class="control-group fieldcontain">
    <label class="control-label" for="jumlahSemua">Jumlah Uang Kertas dan Uang Logam</label>
    <div class="controls">
        <input type="text" style="text-align: right" class="numeric" id="jumlahSemua" value="0" readonly>
    </div>
</div>

<div class="control-group fieldcontain">
    <label class="control-label" for="selisihSaldoFisik">Selisih Saldo Fisik Kas</label>
    <div class="controls">
        <input type="text" style="text-align: right" class="numeric" id="selisihSaldoFisik" value="0" readonly>
    </div>
</div>

<div class="control-group fieldcontain">
    <label class="control-label" for="pembulatan">Pembulatan Saldo Fisik Kas</label>
    <div class="controls">
        <input type="text" style="text-align: right" class="numeric" value="${cashBalanceInstance?.cashBalance}" id="pembulatan">
    </div>
</div>

<g:javascript>
    $(".numeric").autoNumeric("init", {
        vMin: '-99999999999.99',
        vMax: '999999999999.99'
    });


    var idada = "${cashBalanceInstance?.id}";
    if(idada){
        var par = {
            id: idada
        }
        $.post("${request.getContextPath()}/cashBalance/viewDetail", par, function(data) {
            var jumKertas = 0;
            var jumLogam = 0;
            $.each(data.dataKertas,function(i,item){
                $("#kertas_"+item.nilai).val(item.jum);
                $("#val_kertas_"+item.nilai).val(item.total);
                jumKertas=jumKertas+item.total;
            });
            $.each(data.dataLogam,function(i,item){
                $("#logam_"+item.nilai).val(item.jum);
                $("#val_logam_"+item.nilai).val(item.total);
                jumLogam=jumLogam+item.total;
            });
            $("#totalUangKertas").val(jumKertas);
            $("#totalUangLogam").val(jumLogam);
            var jumlah = jumKertas+jumLogam;
            $("#jumlahSemua").autoNumeric('set',jumlah);
            var saldoAkhirTemp = "${saldoAkhirKas ? saldoAkhirKas : 0}";

            var saldoAkhir = saldoAkhirTemp.replace(new RegExp(',', 'g'), '');
            var cariSelisih = saldoAkhir - (jumKertas+jumLogam);
            if(cariSelisih<0){
                // cariSelisih = cariSelisih * -1;
            }
            $("#selisihSaldoFisik").autoNumeric('set',cariSelisih);
        });
    }
</g:javascript>