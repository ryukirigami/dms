
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<g:javascript>
    function selectAll(){
        if($('input[name=cekJenisSPK]').is(':checked')){
            $(".sub-centang-jenisSPK").attr("checked",true);
        }else {
            $(".sub-centang-jenisSPK").attr("checked",false);
        }
    }

    $('#jenisspk-datatables tbody tr').on('click', function () {
        console.log('klik');
        var jum=0;
        $("#jenisspk-datatables tbody .sub-centang-jenisSPK").each(function() {
            if(this.checked){
                jum++;
            }
        });
        var rowCount = $('#jenisspk-datatables tbody tr').length;
        if($(this).find('.sub-centang-jenisSPK').is(":checked")){
            if(rowCount == jum){
                $('.centang-jobdispatch').attr('checked', true);
            }
        } else {
            $('.centang-jobdispatch').attr('checked', false);
        }
    });
</g:javascript>

<table id="jenisspk-datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
        <tr>

            <th>
                &nbsp;<input type="checkbox" name="cekJenisSPK" id="cekJenisSPK" class="centang-jobdispatch" onclick="selectAll();" />&nbsp;
                <g:message code="timeTrackingGR.jenisPekerjaan.label" default="Jenis SPK" />
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                &nbsp;<input type="checkbox" class="sub-centang-jenisSPK" />&nbsp;
                Utama
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;<input type="checkbox" class="sub-centang-jenisSPK" />&nbsp;
                Tambahan
            </td>
        </tr>        
    </tbody>
</table>