<table id="historyService_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
			<th style="border-bottom: none;padding: 5px;">
				<div>Tanggal Service</div>
			</th>
            <th style="border-bottom: none;padding: 5px;">
                <div>Nomor Polisi</div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div>Nomor WO</div>
            </th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Jenis Service</div>
			</th>
            <th style="border-bottom: none;padding: 5px;">
                <div>Service Advisor</div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div>Teknisi</div>
            </th>
		</tr>
	</thead>
</table>

<g:javascript>
var receptionTable;
var reloadReceptionTable;
$(function(){
    reloadReceptionTable = function() {
		receptionTable.fnDraw();
	}

	receptionTable = $('#historyService_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "historyServiceDatatablesList")}",
		"aoColumns": [

{
	"sName": "tanggalService",
	"mDataProp": "tanggalService",
	"aTargets": [0],
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "nopol",
	"mDataProp": "nopol",
	"aTargets": [1],
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "nomorWO",
	"mDataProp": "nomorWO",
	"aTargets": [2],
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "jenisService",
	"mDataProp": "jenisService",
	"aTargets": [3],
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "serviceAdvisor",
	"mDataProp": "serviceAdvisor",
	"aTargets": [4],
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "teknisi",
	"mDataProp": "teknisi",
	"aTargets": [5],
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
        				aoData.push(
									{"name": 'invitation', "value": ${invitation?.id}}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});

</g:javascript>

