<table id="vehicles_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>
        <th style="border-bottom: none;padding: 5px;">
            <div>Nomor Polisi</div>
        </th>
    </tr>
    </thead>
</table>
<g:javascript>

var vehicles;
var reloadVehiclesTable;
$(function(){
    reloadVehiclesTable = function() {
		vehicles.fnDraw();
	}

	vehicles = $('#vehicles_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": false,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : 20, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "vehiclesDatatablesList")}",
		"aoColumns": [

{
	"sName": "nopol",
	"mDataProp": "nopol",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
	    return '<input id="checkBoxVehicle" onclick="clickVehicle(this.value);" value="'+row['id']+'" name="checkBoxVehicle" type="radio" class="radiobut pull-left row-select" style="width:25px !important;" aria-label="Row '+row['id']+'" title="Select this">&nbsp;&nbsp;&nbsp;'+data+'<input type="hidden" value="'+row['id']+'">';
	},
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
        				var estJatuhTempo = $('#estJatuhTempo').val();


                            if(estJatuhTempo){
                                aoData.push(
                                        {"name": 'estJatuhTempo', "value": estJatuhTempo}
                                );
                            }


                            var searchReminder = document.getElementsByName('searchReminder');
                        var checkRetreiveValues = new Array()
                        for(var i = 0; i < searchReminder.length; i++){
                            if(searchReminder[i].checked){
                                checkRetreiveValues.push(searchReminder[i].value)
                            }
                        }
                        aoData.push(
                            {"name": 'searchReminder', "value": checkRetreiveValues}
                        );

                        var method = $('input[name=method]:checked').val();
                        if(method){
                            aoData.push(
                                {"name": 'method', "value": method}
                            );
                        }

                        var woBaru = $('#woBaru').val();
                        if(woBaru){
                            aoData.push(
                                {"name": 'woBaru', "value": woBaru}
                            );
                        }
                            
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
//							    alert("test");

							   }
						});
		}
	});
});

</g:javascript>
