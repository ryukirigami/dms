<%--
  Modified by IntelliJ IDEA.
  User: Ari Tjahyadi
--%>

<%@ page import="com.kombos.administrasi.Provinsi; com.kombos.maintable.JenisInvitation; com.kombos.maintable.JenisReminder" contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>

<head>
    <meta name="layout" content="main">
    <title>MRS General Parameter</title>
    <r:require modules="baseapplayout, baseapplist" />

    <g:javascript disposition="head">
        var maxLineDisplay = 200;
        var doSave;
        $(function () {

            $("#t202TglJamApp_minute").attr('disabled',true);
            $("#t202TglJamApp_hour").attr('disabled',true);
            $("#t202TglJamApp").attr('disabled',true);
            $("#t202CatAppointment").attr('disabled',true);

            $("#t202TglNextReContact").attr('disabled',true);
            $("#t202KetReContact").attr('disabled',true);

            jQuery("#PrintMailButton").click(function (event) {
                var invitation = document.getElementById("invitation").value;
                if(!invitation || invitation == ''){
                    alert("Pilih salah satu customer")
                    return
                }

                window.open('${request.contextPath}/invitation/printMail?invitation='+invitation,'_blank');

                %{--$.ajax({--}%
                    %{--url:'${request.contextPath}/invitation/printMail',--}%
                    %{--type: "POST", // Always use POST when deleting data--}%
                    %{--data : { invitation: invitation },--}%
                    %{--success : function(data){--}%
                        %{--alert("Print Mail successfully")--}%
                    %{--},--}%
                    %{--error: function(xhr, textStatus, errorThrown) {--}%
                        %{--alert('Internal Server Error');--}%
                    %{--}--}%
                %{--});--}%
                event.preventDefault(); //to stop the default loading
            });
            jQuery("#SendMailButton").click(function (event) {
                var invitation = document.getElementById("invitation").value;
                if(!invitation || invitation == ''){
                    alert("Pilih salah satu customer")
                    return
                }
                $.ajax({
                    url:'${request.contextPath}/invitation/sendMail',
                    type: "POST", // Always use POST when deleting data
                    data : { invitation: invitation },
                    success : function(data){
//                        $("input[name=chageMethod][value='Email']").attr('checked', 'checked');
                        alert("Send Mail successfully\n\n"+data.smsContent)
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        alert('Internal Server Error');
                    }
                });
                event.preventDefault(); //to stop the default loading
            });
            jQuery("#SendSMSButton").click(function (event) {
                var invitation = document.getElementById("invitation").value;
                if(!invitation || invitation == ''){
                    alert("Pilih salah satu customer")
                    return
                }
                $.ajax({
                    url:'${request.contextPath}/invitation/sendSMS',
                    type: "POST", // Always use POST when deleting data
                    data : { invitation: invitation },
                    success : function(data){
//                        $("input[name=chageMethod][value='SMS']").attr('checked', 'checked');
                        alert("Send SMS successfully\n\n"+data.smsContent)
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        alert('Internal Server Error');
                    }
                });
                event.preventDefault(); //to stop the default loading
            });

            $('#provinsi').typeahead({
                source: function (query, process) {
                    provinsiList = [];
                    map = {};
                    return $.get('${request.contextPath}/company/getProvinsiList', { query: query }, function (data) {
                        $.each(data.options, function (i, provinsi) {
                            map[provinsi.nama] = provinsi;
                            provinsiList.push(provinsi.nama);
                        });
                        return process(provinsiList);
                    });
                } ,
                updater: function (item) {
                    $("#provinsi_id").val(map[item].id);
                    $('#kabKota').val('');
                    $("#kabKota_id").val('');
                    $('#kecamatan').val('');
                    $('#kecamatan_id').val('');
                    $('#kelurahan').val('');
                    $('#kelurahan_id').val('');
                    return item;
                }
            });

            $('#kabKota').typeahead({
                source: function (query, process) {
                    kabKotaList = [];
                    map = {};
                    return $.get('${request.contextPath}/company/getKabKotaList', { query: query, provinsi: $("#provinsi_id").val() }, function (data) {
                        $.each(data.options, function (i, kabKota) {
                            map[kabKota.nama] = kabKota;
                            kabKotaList.push(kabKota.nama);
                        });
                        return process(kabKotaList);
                    });
                } ,
                updater: function (item) {
                    $("#kabKota_id").val(map[item].id);
                    $('#kecamatan').val('');
                    $('#kecamatan_id').val('');
                    $('#kelurahan').val('');
                    $('#kelurahan_id').val('');
                    return item;
                }
            });

            $('#kecamatan').typeahead({
                source: function (query, process) {
                    kecamatanList = [];
                    map = {};
                    return $.get('${request.contextPath}/company/getKecamatanList', { query: query, kabKota: $("#kabKota_id").val() }, function (data) {
                        $.each(data.options, function (i, kecamatan) {
                            map[kecamatan.nama] = kecamatan;
                            kecamatanList.push(kecamatan.nama);
                        });
                        return process(kecamatanList);
                    });
                } ,
                updater: function (item) {
                    $("#kecamatan_id").val(map[item].id);
                    $('#kelurahan').val('');
                    $('#kelurahan_id').val('');
                    return item;
                }
            });

            $('#kelurahan').typeahead({
                source: function (query, process) {
                    kelurahanList = [];
                    map = {};
                    return $.get('${request.contextPath}/company/getKelurahanList', { query: query, kecamatan: $("#kecamatan_id").val() }, function (data) {
                        $.each(data.options, function (i, kelurahan) {
                            map[kelurahan.nama] = kelurahan;
                            kelurahanList.push(kelurahan.nama);
                        });
                        return process(kelurahanList);
                    });
                } ,
                updater: function (item) {
                    $("#kelurahan_id").val(map[item].id);
                    return item;
                }
            });

            parseDate = function(input) {
              var parts = input.split('/');
              // new Date(year, month [, day [, hours[, minutes[, seconds[, ms]]]]])
              return new Date(parts[2], parts[1]-1, parts[0]); // Note: months are 0-based
            }

            jQuery("#ReCalculate").click(function (event) {
                var t201LastKM = $('#t201LastKM').val();
                var nopol = $('#nomorPolisi').text();
                $.ajax({
                    url:'${request.contextPath}/invitation/reCalculate',
                    type: "POST", // Always use POST when deleting data
                    data : { nopol: nopol },
                    success : function(data){
//                       alert(data.nextServiceDate)
                       document.getElementById("NextServiceKm").innerHTML = data.nextServiceKm;
                       document.getElementById("NextServiceDate").innerHTML = data.nextServiceDate;
                       document.getElementById("LastServiceDate").innerHTML = data.lastServiceDate;
                       document.getElementById("nsd").value = data.nextServiceDate;
                       document.getElementById("nsdkm").value = data.nextServiceKm;

                       $('#t201LastKM').val(data.kmSaatIni);
                       %{--var LastServiceDate = document.getElementById("LastServiceDate").innerHTML;--}%
                       %{--if(LastServiceDate && LastServiceDate != ''){--}%
                           %{--var tglLast = LastServiceDate.split("-");--}%
                           %{--var d = tglLast[0];--}%
                           %{--var m = parseInt(tglLast[1])+6;--}%
                           %{--var y = tglLast[2];--}%
                           %{--var nextSerbylastdate = d + "-" + m + "-" + y;--}%
                           %{--document.getElementById("NextServiceKm").innerHTML = nextSerbylastdate;--}%
                           %{--document.getElementById("NextService").innerHTML = data;--}%
                       %{--}--}%
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        alert('Internal Server Error (Data service kendaraan belum ada)');
                    }
                });

                event.preventDefault(); //to stop the default loading
            });

            jQuery("#buttonSave").click(function (event) {
                    var formInvit = $("#fInvit");
                    $.ajax({type:'POST',
                        data:formInvit.serialize(),
                        url:'${request.contextPath}/invitation/doSave',
                        success:function(data,textStatus){
                            if (data.status == 'OK') {
                                toastr.success("Save success");
                            } else {
                                toastr.error("Save fail\n" + data.error);
                            }
                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){
                            toastr.error("Internal Server Error");
                        }
                     });

                    event.preventDefault();
                    return false;
            });

            jQuery("#buttonClose").click(function (event) {
                event.preventDefault(); //to stop the default loading
            });

            jQuery("#clearSearchButton").click(function (event) {
                event.preventDefault(); //to stop the default loading
            });

            jQuery("#SelectAllButton").click(function (event) {
                event.preventDefault(); //to stop the default loading
            });

            jQuery("#UnSelectAllButton").click(function (event) {
                event.preventDefault(); //to stop the default loading
            });

            jQuery("#ViewServiceHistory").click(function (event) {
                var invitation = document.getElementById("invitation").value;
                if(!invitation){
                    alert("Please select vehicle");
                    event.preventDefault();
                    return;
                }
                $("#ViewServiceHistoryContent").empty();
                $.ajax({type: 'POST', url: '${request.contextPath}/invitation/viewServiceHistory?invitation=' + invitation,
                    success: function (data, textStatus) {
                        $("#ViewServiceHistoryContent").html(data);
                        $("#ViewServiceHistoryModal").modal({
                            "backdrop": "static",
                            "keyboard": true,
                            "show": true
                        }).css({'width': '1200px', 'margin-left': function () {
                                    return -($(this).width() / 2);
                                }});

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                    },
                    complete: function (XMLHttpRequest, textStatus) {
                        $('#spinner').fadeOut();
                    }
                });
                event.preventDefault(); //to stop the default loading
            });

            changeTerhubung = function(nilai) {
                document.getElementById("t202StaInvitationxxya").style.display = 'none';
                document.getElementById("t202StaInvitationxxtidak").style.display = 'none';
                if (nilai == '1') {
                    document.getElementById("t202StaInvitationxxya").style.display = '';
                } else {
                    document.getElementById("t202StaInvitationxxtidak").style.display = '';
                }
            }

            clickVehicle = function (invitationId) {
                    //put triger here


                    $.ajax({
                            url:'${request.contextPath}/invitation/loadInvitation?id='+invitationId,
                            type: "POST", // Always use POST when deleting data
                            data : { ids: '' },
                            success : function(data){
                                var invitation = data.invitation;
                                var historyCustomer = data.historyCustomer
                                var provinsi = data.provinsi
                                var kota = data.kota
                                var kecamatan = data.kecamatan
                                var kelurahan = data.kelurahan
                                var kodepos = data.kodepos
                                var currentCondition = data.currentCondition
                                var reminder = data.reminder
                                var jenisSurvey = data.jenisSurvey
                                var fa = data.fa
                                var sPK = data.sPK
                                var sPkAsuransi = data.sPkAsuransi
                                var jenisInvitation = data.jenisInvitation
                                var statusReminder = data.statusReminder

//                                alert("jenisInvitation.m204JenisInvitation = "+jenisInvitation.m204JenisInvitation)

                                document.getElementById("invitation").value = invitation.id;
                                document.getElementById("nomorPolisi").innerHTML = data.nopol;
                                document.getElementById("baseModel").innerHTML = data.baseModel;
                                document.getElementById("vehicleVinCode").innerHTML = data.vinCode;
                                document.getElementById("tahunBulanRakit").innerHTML = data.currentCondition.t183ThnBlnRakit;
                                document.getElementById("warna").innerHTML = data.warna;


                                document.getElementById("JDPower").innerHTML = jenisSurvey ? jenisSurvey.m131TipeSurvey : "";
                                document.getElementById("TPSS").innerHTML = jenisSurvey ? jenisSurvey.m131JenisSurvey : "";


                                document.getElementById("FA").innerHTML = fa ? fa.m185NamaFA : "";

                                document.getElementById("SPK").innerHTML = sPK ? sPK.t191NoSPK : "";
                                document.getElementById("TglSPK").innerHTML = sPK ? sPK.t191TanggalSPK : "";
                                document.getElementById("SPKAsuransi").innerHTML = sPkAsuransi ? sPkAsuransi.t193NomorSPK : "";
                                $("input[name=chageMethod][value='" + (jenisInvitation && jenisInvitation.m204JenisInvitation ? jenisInvitation.m204JenisInvitation : "-1") + "']").attr('checked', 'checked');

                                if(data.t202StaInvitationxx1 == '1'){

                                   $("input[name=t202StaInvitationxx1][value=" + (data.t202StaInvitationxx1) + "]").attr('checked', 'checked');
                                   $("input[name=t202StaInvitationxx2][value=" + (data.t202StaInvitationxx2) + "]").attr('checked', 'checked');
                                   $("input[name=t202StaInvitationxx3][value=" + (data.t202StaInvitationxx3) + "]").attr('checked', 'checked');
                                   $("input[name=t202StaInvitationxx4][value=" + (data.t202StaInvitationxx4) + "]").attr('checked', 'checked');
                                   $("input[name=t202StaInvitationxx5][value=" + (data.t202StaInvitationxx5) + "]").attr('checked', 'checked');
                                   $("input[name=t202StaInvitationxx6][value=" + (data.t202StaInvitationxx6) + "]").attr('checked', 'checked');
                                } else {
                                   $("input[name=t202StaInvitationxx1][value=" + (data.t202StaInvitationxx1) + "]").attr('checked', 'checked');
                                   $("input[name=t202StaInvitationxx8][value=" + (data.t202StaInvitationxx2) + "]").attr('checked', 'checked');
                                   $("input[name=t202StaInvitationxx9][value=" + (data.t202StaInvitationxx3) + "]").attr('checked', 'checked');
                                   $("input[name=t202StaInvitationxx10][value=" + (data.t202StaInvitationxx4) + "]").attr('checked', 'checked');
                                   $("input[name=t202StaInvitationxx11][value=" + (data.t202StaInvitationxx5) + "]").attr('checked', 'checked');
                                   $("input[name=t202StaInvitationxx12][value=" + (data.t202StaInvitationxx6) + "]").attr('checked', 'checked');
                                }
                                changeTerhubung(data.t202StaInvitationxx1);

                                document.getElementById("lastDM").innerHTML = reminder ? reminder.t201TglDM : "-";
                                document.getElementById("lastSMS").innerHTML = reminder ? reminder.t201TglSMS : "-";
                                document.getElementById("lastEmail").innerHTML = reminder ? reminder.t201TglEmail : "-";
                                document.getElementById("lastPhoneCall").innerHTML = reminder ? reminder.t201TglCall : "-";

                                $("input[name=t202StaReContact][value=" + (invitation.t202StaReContact) + "]").attr('checked', 'checked');

                                var tglNext = new Date(invitation.t202TglNextReContact);
                                var dd = tglNext.getDate();
                                var mm = tglNext.getMonth()+1; //January is 0!
                                var yyyy = tglNext.getFullYear();

                                if(dd<10){
                                    dd='0'+dd
                                }
                                if(mm<10){
                                    mm='0'+mm
                                }
                                var tNext = dd+'-'+mm+'-'+yyyy;
                                $('#t202TglNextReContact').val(tNext);
                                $('#t202KetReContact').val(invitation.t202KetReContact);

                                $('#m202ID').val(statusReminder ? statusReminder.id : 0);
                                $('#t201LastKM').val(reminder ? reminder.t201LastKM : 0);

                                var tglLast = new Date(reminder.t201LastServiceDate);
                                var bulan = parseInt(tglLast.getMonth())+1
                                var tglLast1 = tglLast.getDate() + "-" + bulan + "-" + tglLast.getFullYear();
                                document.getElementById("LastServiceDate").innerHTML = reminder ? tglLast1 : '';
                                document.getElementById("NextServiceKm").innerHTML = "-";
                                document.getElementById("NextServiceDate").innerHTML = "-";
                                $("#nsd").val("")
                                $("#nsdkm").val("")

                                $("input[name=t202StaLangsungAppointment][value=" + (invitation.t202StaLangsungAppointment) + "]").attr('checked', 'checked');

                                var tglApp = new Date(invitation.t202TglJamApp);
                                var dd1 = tglApp.getDate();
                                var mm1 = tglApp.getMonth()+1; //January is 0!
                                var yyyy1 = tglApp.getFullYear();

                                if(dd1<10){
                                    dd1='0'+dd1
                                }
                                if(mm1<10){
                                    mm1='0'+mm1
                                }
                                var tApp = dd1+'-'+mm1+'-'+yyyy1;
                                $('#t202TglJamApp').val(tApp);

                                $('#t202TglJamApp_hour').val(data.t202TglJamApp_hour);
                                $('#t202TglJamApp_minute').val(data.t202TglJamApp_minute);



                                $('#t202CatAppointment').val(invitation.t202CatAppointment);

                                if(historyCustomer){
                                    $('#t182NamaDepan').val(historyCustomer.t182NamaDepan);
                                    $('#t182NamaBelakang').val(historyCustomer.t182NamaBelakang);
                                    $('#t182Alamat').val(historyCustomer.t182Alamat);
                                    $('#t182RT').val(historyCustomer.t182RT);
                                    $('#t182RW').val(historyCustomer.t182RW);
                                    $('#t182KodePos').val(historyCustomer.t182KodePos);
                                    $('#t182NoTelpRumah').val(historyCustomer.t182NoTelpRumah);
                                    $('#t182NoHp').val(historyCustomer.t182NoHp);
                                    $('#t182Email').val(historyCustomer.t182Email);
                                    $('#t182Ket').val(historyCustomer.t182Ket);
                                    if(historyCustomer.kelurahan){
                                        $('#kelurahan').val(kelurahan.m004NamaKelurahan);
                                        $('#kelurahan_id').val(kelurahan.id);
                                    }
                                    else{
                                        $('#kelurahan').val("");
                                    }
                                    if(historyCustomer.kecamatan){
                                        $('#kecamatan').val(kecamatan.m003NamaKecamatan);
                                        $('#kecamatan_id').val(kecamatan.id);
                                    }
                                    else{
                                        $('#kecamatan').val("");
                                    }
                                    if(historyCustomer.provinsi){
                                        $('#provinsi').val(provinsi.m001NamaProvinsi);
                                        $('#provinsi_id').val(provinsi.id);

                                    }
                                    else{
                                        $('#provinsi').val("");
                                    }
                                    if(historyCustomer.kabKota){
                                        $('#kabKota').val(kota.m002NamaKabKota);
                                        $('#kabKota_id').val(kota.id);
                                    }
                                    else{
                                        $('#kabKota').val("");
                                    }
                                }
                                retrieveInvitationHistory();
                            },
                            error: function(xhr, textStatus, errorThrown) {
                                alert('Internal Server Error');
                            }
                        });
            }

            retrieveInvitationHistory = function () {
                var invitation = document.getElementById("invitation").value;
                    if(!invitation || invitation == ''){
                        alert("Pilih salah satu customer")
                        return
                    }
                        $.ajax({
                            url:'${request.contextPath}/invitation/retrieveInvitationHistory',
                            type: "POST", // Always use POST when deleting data
                            data : { invitation: invitation },
                            success : function(data){
                                document.getElementById("lastDM").innerHTML = data.lastDM;
                                document.getElementById("lastSMS").innerHTML = data.lastSMS;
                                document.getElementById("lastEmail").innerHTML = data.lastEmail;
                                document.getElementById("lastPhoneCall").innerHTML = data.lastPhoneCall;

                            },
                            error: function(xhr, textStatus, errorThrown) {
                                alert('Internal Server Error');
                            }
                        });
            }

            onClearSearch = function () {
                $(':input', '#form')
                        .not(':button, :submit, :reset, :checkbox')
                        .val('')
                        .removeAttr('checked')
                        .removeAttr('selected');

                var inputs, index;

                inputs = document.getElementsByTagName('input');
                for (index = 0; index < inputs.length; ++index) {
                    inputs[index].checked = false;
                }

                reloadVehiclesTable();
            }

            jQuery("#searchButton").click(function (event) {
                event.preventDefault(); //to stop the default loading
            });

            onSelectAll = function () {
                $("#vehicles_datatables tbody .row-select").each(function () {
                    this.checked = true
                });
            }

            onUnSelectAll = function () {
                $("#vehicles_datatables tbody .row-select").each(function () {
                    this.checked = false
                });
            }
        });

        function getKodePos(dari){
            var kelurahan = $("#kelurahan"+dari).val();
            var kecamatan = $("#kecamatan"+dari).val();
            var kabupaten = $("#kabKota"+dari).val();
            if(kelurahan.toString().length > 0 && kecamatan.toString().length > 0 && kabupaten.toString().length > 0 ){
                jQuery.getJSON('${request.contextPath}/company/getKodePos?kelurahan='+kelurahan+"&kecamatan="+kecamatan+"&kabKota="+kabupaten, function (data) {
                        $("#kodePos"+dari).val(data);
                });
            }
        }

        function cekRecon(){
            if($('input:radio[name=t202StaReContact]:nth(1)').is(':checked')){
                $("#t202TglNextReContact").attr('disabled',true);
                $("#t202KetReContact").attr('disabled',true);

            }else{
                $("#t202TglNextReContact").attr('disabled',false);
                $("#t202KetReContact").attr('disabled',false);
            }
        }

        function cekApp(){
            if($('input:radio[name=t202StaLangsungAppointment]:nth(1)').is(':checked')){
                $("#t202TglJamApp_minute").attr('disabled',true);
                $("#t202TglJamApp_hour").attr('disabled',true);
                $("#t202TglJamApp").attr('disabled',true);
                $("#t202CatAppointment").attr('disabled',true);

            }else{
                $("#t202TglJamApp_minute").attr('disabled',false);
                $("#t202TglJamApp_hour").attr('disabled',false);
                $("#t202TglJamApp").attr('disabled',false);
                $("#t202CatAppointment").attr('disabled',false);
            }
        }
    </g:javascript>
    <style>
    input {
        /* font-size: xx-small; */
        width: 100px;
        border: 1px solid #c4c4c4;
        border-top-color: #aaa;
        -webkit-box-shadow: rgba(255, 255, 255, 0.9) 0 1px 0;
        -moz-box-shadow: rgba(255, 255, 255, 0.9) 0 1px 0;
        box-shadow: rgba(255, 255, 255, 0.9) 0 1px 0;
    }

    legend {
        display: block;
        width: 100%;
        padding: 0;
        /* margin-bottom: 20px; */
        font-size: 21px;
        line-height: 20px;
        color: #333333;
        border: 0;
        border-bottom: 1px solid #e5e5e5;
    }

    td {
        vertical-align: top;
        padding-bottom: 10px;
        padding-left: 5px;
        padding-top: 10px;
        font-size: x-small;
    }

    input[type=radio]{
         width:25px !important;
    }

    input[type=checkbox]{
        width:25px !important;
    }

    </style>
</head>

<body>
<g:form name="fInvit" id="fInvit">
<g:hiddenField name="invitation" id="invitation"/>
<g:hiddenField name="NextServiceDate" id="nsd" />
<g:hiddenField name="NextServiceKm" id="nsdkm" />
<fieldset class="form">
<table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">
<tr>
<td style="width: 33%;vertical-align: top;">

    <div class="box">
        <legend style="font-size: small">Search Criteria:</legend>
        <table style="width: 100%;" border="0">
            <tr>
                <td>

                </td>
                <td style="width: 50px"></td>
                <td style="width: 70%;">
                </td>
            </tr>
            <tr>
                <td colspan="3"
                    style="padding-top: 10px">Tanggal Estimasi Jatuh Tempo Service <ba:datePicker
                        name="estJatuhTempo" id="estJatuhTempo"
                        precision="day"
                        format="dd-MM-yyyy"/>
                </td>
            </tr>
            <tr>
                <td>
                    Reminder
                </td>
                <td style="width: 50px">:</td>
                <td style="width: 70%; padding-left: 20%">
                    <g:each in="${JenisReminder.listOrderByM201Id()}">
                        <input name="searchReminder" id="searchReminder_${it?.id}" type="checkbox"
                               value="${it?.id}" checked> ${it?.m201NamaJenisReminder} <br>
                    </g:each>
                </td>
            </tr>
            <tr>
                <td>
                    Method
                </td>
                <td style="width: 50px">:</td>
                <td style="width: 70%; padding-left: 20%">
                    <g:radioGroup value="SMS" values="${JenisInvitation.listOrderByM204JenisInvitation().each {
                        it.m204JenisInvitation
                    }}"
                                  name="method">
                        ${it.radio} ${it?.label?.replaceAll("Radio", "")} <br>
                    </g:radioGroup>
                </td>
            </tr>
            <tr>
                <td>
                    Sudah ada WO baru ?
                </td>
                <td style="width: 50px">:</td>
                <td style="width: 70%; padding-left: 20%">
                    <g:radioGroup value="Ya & Tidak" values="['Ya', 'Tidak', 'Ya & Tidak']" name="woBaru">
                        ${it.radio} ${it.label?.replaceAll("Radio", "")} <br>
                    </g:radioGroup>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="padding-top: 10px">
                    <fieldset class="buttons controls">
                        <button class="btn btn-primary" id="searchButton"
                                onclick="reloadVehiclesTable();">Search</button>


                        <button class="btn btn-cancel" id="clearSearchButton"
                                onclick="onClearSearch();">Clear Search</button>

                    </fieldset>
                </td>
            </tr>
        </table>
    </div>

    <div class="box">

        <table style="width: 100%;border: 0px;">

            <tr>

                <td style="vertical-align: top; width: 50%;">
                    <div class="box">
                        <legend style="font-size: small">Search Result:</legend>
                        <g:render template="vehiclesDataTables"/>
                        %{--<fieldset class="buttons controls">--}%
                        %{--<button class="btn btn-primary" id="SelectAllButton"--}%
                        %{--style="font-size: xx-small;padding: 0px 5px;"--}%
                        %{--onclick="onSelectAll();">Select All</button>--}%
                        %{--<button class="btn btn-primary" id="UnSelectAllButton"--}%
                        %{--style="font-size: xx-small;padding: 0px 5px;"--}%
                        %{--onclick="onUnSelectAll();">Unselect All</button>--}%
                        %{--</fieldset>--}%
                    </div>
                </td>

                <td style="vertical-align: top; width: 50%;">
                    <div class="box">
                        <legend style="font-size: small">Invitation History:</legend>
                        <table>
                            <tr>
                                <td>Last DM :</td>
                            </tr>
                            <tr>
                                <td id="lastDM"></td>
                            </tr>
                            <tr>
                                <td>Last SMS :</td>
                            </tr>
                            <tr>
                                <td id="lastSMS"></td>
                            </tr>
                            <tr>
                                <td>Last Email :</td>
                            </tr>
                            <tr>
                                <td id="lastEmail"></td>
                            </tr>
                            <tr>
                                <td>Last Phone Call :</td>
                            </tr>
                            <tr>
                                <td id="lastPhoneCall"></td>
                            </tr>
                        </table>
                    </div>

                    <div class="box">
                        <legend style="font-size: small">References:</legend>
                        <table style="width: 100%;border: 0px;">
                            <tr>
                                <td>JD Power</td>
                                <td>:</td>
                                <td id="JDPower" style="width: 60%;padding-right: 30px">
                                    ---
                                </td>
                                <script type="text/javascript">
                                    window.setInterval (BlinkIt, 500);
                                    var color = "#ffffff";
                                    function BlinkIt () {
                                        var blink = document.getElementById("JDPower");
                                        if(blink){
                                            color = (color == "#ffffff")? "#000000" : "#ffffff";
                                            blink.style.color = color;
                                        }

                                    }
                                </script>
                            </tr>
                            <tr>
                                <td>TPSS</td>
                                <td>:</td>
                                <td id="TPSS" style="width: 60%;padding-right: 30px">

                                </td>
                            </tr>
                            <tr>
                                <td>FA</td>
                                <td>:</td>
                                <td id="FA" style="width: 60%;padding-right: 30px">

                                </td>
                            </tr>
                            <tr>
                                <td>SPK</td>
                                <td>:</td>
                                <td id="SPK" style="width: 60%;padding-right: 30px">

                                </td>
                            </tr>
                            <tr>
                                <td>Tanggal SPK</td>
                                <td>:</td>
                                <td id="TglSPK" style="width: 60%;padding-right: 30px">

                                </td>
                            </tr>
                            <tr>
                                <td>SPK Asuransi</td>
                                <td>:</td>
                                <td id="SPKAsuransi" style="width: 60%;padding-right: 30px">

                                </td>
                            </tr>

                        </table>
                    </div>

                    <div class="box">
                        <legend style="font-size: small">Change Invitation Method:</legend>
                        <g:radioGroup values="${JenisInvitation.listOrderByM204JenisInvitation().each {
                            it.m204JenisInvitation
                        }}" name="chageMethod">
                            ${it.radio} ${it?.label?.replaceAll("Radio", "")?.trim()} <br>
                        </g:radioGroup>
                    </div>

                    <div class="box">
                        <legend style="font-size: small">Action:</legend>
                        <button class="btn btn-primary" id="PrintMailButton"
                                style="font-size: x-small;padding: 2px 2px; width: 150px;">Print Mail</button><br><br>
                        <button class="btn btn-primary" id="SendMailButton"
                                style="font-size: x-small;padding: 2px 2px; width: 150px;">Send Mail</button><br><br>
                        <button class="btn btn-primary" id="SendSMSButton"
                                style="font-size: x-small;padding: 2px 2px; width: 150px;">Send SMS</button>
                    </div>

                </td>

            </tr>

        </table>

    </div>

</td>
<td style="width: 33%;vertical-align: top;padding-left: 10px;">
    <div class="box" style="height: 100%;">

        <table style="width: 100%;height: 100%;border: 0px;">

            <tr>
                <td>Nomor Polisi</td>
                <td>:</td>
                <td style="width: 70%;" id="nomorPolisi"></td>
            </tr>

            <tr>
                <td>Base Model</td>
                <td>:</td>
                <td style="width: 70%" id="baseModel"></td>
            </tr>

            <tr>
                <td>VIN Code</td>
                <td>:</td>
                <td style="width: 70%" id="vehicleVinCode"></td>
            </tr>

            <tr>
                <td>Tahun & Bulan Rakit</td>
                <td>:</td>
                <td style="width: 70%" id="tahunBulanRakit"></td>
            </tr>

            <tr>
                <td>Warna</td>
                <td>:</td>
                <td style="width: 70%" id="warna"></td>
            </tr>

            <tr>
                <td>Nama Depan</td>
                <td>:</td>
                <td style="width: 70%;">
                    <g:textField style="width:100%" name="t182NamaDepan"/>
                </td>
            </tr>

            <tr>
                <td>Nama Belakang</td>
                <td>:</td>
                <td style="width: 70%;">
                    <g:textField style="width:100%" name="t182NamaBelakang"/>
                </td>
            </tr>

            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td style="width: 70%;">
                    <g:textArea style="width:100%;height: 40px" name="t182Alamat"/>
                </td>
            </tr>

            <tr>
                <td>RT / RW</td>
                <td>:</td>
                <td style="width: 70%;">
                    <g:textField style="width:30%" name="t182RT"/> /
                    <g:textField style="width:30%" name="t182RW"/>
                </td>
            </tr>

            <tr>
                <td>Provinsi</td>
                <td>:</td>
                <td style="width: 70%;">
                    <g:textField name="__propinsi" id="provinsi"  value="${historyCustomerInstance?.provinsi?.m001NamaProvinsi}" class="typeahead"  autocomplete="off" maxlength="220" onchange="void(0);"/>
                    <g:hiddenField name="provinsi.id" id="provinsi_id" value="${historyCustomerInstance?.provinsi?.id}" />
                </td>
            </tr>

            <tr>
                <td>Kabupaten / Kota</td>
                <td>:</td>
                <td style="width: 70%;">
                    %{--<g:select id="kabKota" name="kabKota.id" from="${com.kombos.administrasi.KabKota.list()}"--}%
                              %{--optionKey="id"--}%
                              %{--class="many-to-one"/>--}%
                    <g:textField name="__kabKota" id="kabKota"  value="${historyCustomerInstance?.kabKota?.m002NamaKabKota}" class="typeahead"  autocomplete="off" maxlength="220" onchange="void(0);"/>
                    <g:hiddenField name="kabKota.id" id="kabKota_id" value="${historyCustomerInstance?.kabKota?.id}" />
                </td>
            </tr>

            <tr>
                <td>Kecamatan</td>
                <td>:</td>
                <td style="width: 70%;">
                    %{--<g:select id="kecamatan" name="kecamatan.id" from="${com.kombos.administrasi.Kecamatan.list()}"--}%
                              %{--optionKey="id"--}%
                              %{--class="many-to-one"/>--}%
                    <g:textField name="__kecamatan" id="kecamatan"  value="${historyCustomerInstance?.kecamatan?.m003NamaKecamatan}" class="typeahead"  autocomplete="off" maxlength="220" onchange="void(0);"/>
                    <g:hiddenField name="kecamatan.id" id="kecamatan_id" value="${historyCustomerInstance?.kecamatan?.id}" />
                </td>
            </tr>

            <tr>
                <td>Kelurahan / Desa</td>
                <td>:</td>
                <td style="width: 70%;">
                    %{--<g:select id="kelurahan" name="kelurahan.id" from="${com.kombos.administrasi.Kelurahan.list()}"--}%
                              %{--optionKey="id"--}%
                              %{--class="many-to-one"/>--}%
                    <g:textField name="__kelurahan" id="kelurahan" onblur="getKodePos('');" value="${historyCustomerInstance?.kelurahan?.m004NamaKelurahan}" class="typeahead"  autocomplete="off" maxlength="220" onchange="void(0);"/>
                    <g:hiddenField name="kelurahan.id" id="kelurahan_id" value="${historyCustomerInstance?.kelurahan?.id}" />
                </td>
            </tr>

            <tr>
                <td>Kode Pos</td>
                <td>:</td>
                <td style="width: 70%;">
                    <g:textField onkeyup="checkNumber(this);" readonly="" id="kodePos" name="t182KodePos" maxlength="5"
                                 value="${historyCustomerInstance?.t182KodePos}"/>
                </td>
            </tr>

            <tr>
                <td>No. Telpon *</td>
                <td>:</td>
                <td style="width: 70%;">
                    <g:textField style="width:100%" name="t182NoTelpRumah"/>
                </td>
            </tr>

            <tr>
                <td>No. HP *</td>
                <td>:</td>
                <td style="width: 70%;">
                    <g:textField style="width:100%" name="t182NoHp"/>
                </td>
            </tr>

            <tr>
                <td>Email</td>
                <td>:</td>
                <td style="width: 70%;">
                    <g:textField style="width:100%" name="t182Email"/>
                </td>
            </tr>

            <tr>
                <td>Terima MRS ?</td>
                <td>:</td>
                <td style="width: 70%;padding-bottom: 10px">
                    <g:checkBox name="t182StaTerimaMRS"/>
                </td>
            </tr>

            <tr>
                <td>Keterangan</td>
                <td>:</td>
                <td style="width: 70%;">
                    <g:textArea style="width:100%;height: 80px" name="t182Ket"/>
                </td>
            </tr>

        </table>

    </div>

</td>

<td style="width: 33%;vertical-align: top;padding-left: 10px;">
<div class="box" >
    <legend style="font-size: small">Status Terhubung</legend>
    <table style="width: 100%;border: 0px;">



        <tr>
            <td>Terhubung
            ke
            Customer?</td>
            <td>:</td>
            <td style="width: 40%;">
                <input onclick="changeTerhubung(this.value)" type="radio" name="t202StaInvitationxx1" value="1"> Ya
                <input onclick="changeTerhubung(this.value)" type="radio" name="t202StaInvitationxx1" value="0"> Tidak
            </td>
        </tr>

    </table>

    <table style="display: none;width: 100%;border: 0px;" id="t202StaInvitationxxya">
        <tr>
            <td>Nomor Telepon Benar?</td>
            <td>:</td>
            <td style="width: 40%;">
                <input type="radio" name="t202StaInvitationxx2" value="1"> Ya
                <input type="radio" name="t202StaInvitationxx2" value="0"> Tidak
            </td>
        </tr>
        <tr>
            <td>Telepon Seluler Benar?</td>
            <td>:</td>
            <td style="width: 40%;">
                <input type="radio" name="t202StaInvitationxx3" value="1"> Ya
                <input type="radio" name="t202StaInvitationxx3" value="0"> Tidak
            </td>
        </tr>
        <tr>
            <td>Alamat benar?</td>
            <td>:</td>
            <td style="width: 40%;">
                <input type="radio" name="t202StaInvitationxx4" value="1"> Ya
                <input type="radio" name="t202StaInvitationxx4" value="0"> Tidak
            </td>
        </tr>
        <tr>
            <td>Email benar?</td>
            <td>:</td>
            <td style="width: 40%;">
                <input type="radio" name="t202StaInvitationxx5" value="1"> Ya
                <input type="radio" name="t202StaInvitationxx5" value="0"> Tidak
            </td>
        </tr>
        <tr>
            <td>DM Sampai?</td>
            <td>:</td>
            <td style="width: 40%;">
                <input type="radio" name="t202StaInvitationxx6" value="1"> Ya
                <input type="radio" name="t202StaInvitationxx6" value="0"> Tidak
            </td>
        </tr>
    </table>



    <table style="display: none;width: 100%;border: 0px;" id="t202StaInvitationxxtidak">
        <tr>
            <td>Telepon Tidak Diangkat</td>
            <td>:</td>
            <td style="width: 40%;">
                <input type="radio" name="t202StaInvitationxx8" value="1"> Ya
                <input type="radio" name="t202StaInvitationxx8" checked value="0"> Tidak
            </td>
        </tr>
        <tr>
            <td>Nomor Telepon Rumah Salah</td>
            <td>:</td>
            <td style="width: 40%;">
                <input type="radio" name="t202StaInvitationxx9" value="1"> Ya
                <input type="radio" name="t202StaInvitationxx9" checked value="0"> Tidak
            </td>
        </tr>
        <tr>
            <td>Nomor Telepon Rumah Tidak Aktif</td>
            <td>:</td>
            <td style="width: 40%;">
                <input type="radio" name="t202StaInvitationxx10" value="1"> Ya
                <input type="radio" name="t202StaInvitationxx10" checked value="0"> Tidak
            </td>
        </tr>
        <tr>
            <td>Telepon Seluler Tidak Diangkat</td>
            <td>:</td>
            <td style="width: 40%;">
                <input type="radio" name="t202StaInvitationxx11" value="1"> Ya
                <input type="radio" name="t202StaInvitationxx11" checked value="0"> Tidak
            </td>
        </tr>
        <tr>
            <td>Nomor Telepon Seluler Tidak Aktif</td>
            <td>:</td>
            <td style="width: 40%;">
                <input type="radio" name="t202StaInvitationxx12" value="1"> Ya
                <input type="radio" name="t202StaInvitationxx12" checked value="0"> Tidak
            </td>
        </tr>
    </table>

</div>

<div class="box">
    <legend style="font-size: small">Re-Contact</legend>
    <table style="width: 100%;border: 0px;">
        <tr>
            <td>Perlu Re-Contact?</td>
            <td>:</td>
            <td style="width: 70%;">
            <g:radioGroup name="t202StaReContact" id="t202StaReContact" values="[1,0]" value="0" onchange="cekRecon();" labels="['Yes','No']">
                ${it.radio} ${it.label}
            </g:radioGroup>
            </td>
        </tr>
        <tr>
            <td>Next Re-Contact</td>
            <td>:</td>
            <td style="width: 70%;">
                <ba:datePicker format="dd-MM-yyyy" value="${new Date()}" name="t202TglNextReContact" id="t202TglNextReContact"/>
            </td>
        </tr>
        <tr>
            <td>Catatan Re-Contact</td>
            <td>:</td>
            <td style="width: 70%;">
                <g:textArea name="t202KetReContact" id="t202KetReContact" style="width:100%;height: 70px"/>
            </td>
        </tr>

    </table>
</div>

<div class="box">
    <legend style="font-size: small">Status Reminder</legend>
    <table style="width: 100%;border: 0px;">
        <tr>
            <td>Status Reminder</td>
            <td>:</td>
            <td style="width: 70%;">
                <g:select name="m202ID" from="${com.kombos.maintable.StatusReminder.list()}" optionKey="id"
                          optionValue="m202NamaStatusReminder"/>
            </td>
        </tr>

    </table>
</div>

<div class="box">
    <legend style="font-size: small">Update Km</legend>
    <table style="width: 100%;border: 0px;">
        <tr>
            <td>Last Service Km</td>
            <td>:</td>
            <td style="width: 20%;padding-right: 30px">
                <g:textField name="t201LastKM"/>
            </td>
            <td>Next Service By Km</td>
            <td>:</td>
            <td id="NextServiceKm" style="width: 20%;">

            </td>
        </tr>
        <tr>
            <td>Last Service Date</td>
            <td>:</td>
            <td id="LastServiceDate" style="width: 20%;padding-right: 30px">

            </td>
            <td>Next Service By Last Date</td>
            <td>:</td>
            <td id="NextServiceDate" style="width: 20%;">

            </td>
        </tr>

    </table>
    <button id="ReCalculate">Re-Calculate</button>
</div>

<div class="box">
    <legend style="font-size: small">Appointment</legend>
    <table style="width: 100%;border: 0px;">
        <tr>
            <td>Setuju Appointment?</td>
            <td>:</td>
            <td style="width: 75%;">
                <g:radioGroup name="t202StaLangsungAppointment" id="t202StaLangsungAppointment" values="[1,0]" value="0" onchange="cekApp();" labels="['Yes','No']">
                    ${it.radio} ${it.label}
                </g:radioGroup>
            </td>
        </tr>
        <tr>
            <td>Tanggal Appointment</td>
            <td>:</td>
            <td style="width: 75%;">
                <ba:datePicker format="dd-MM-yyyy" value="${new Date()}" name="t202TglJamApp" id="t202TglJamApp"/>
                <select id="t202TglJamApp_hour" name="t202TglJamApp_hour" style="width: 50px" required="">
                    <g:each var="i" in="${(0..<24)}">
                        <g:if test="${i < 10}">
                            <option value="${i}">0${i}</option>
                        </g:if>
                        <g:else>
                            <option value="${i}">${i}</option>
                        </g:else>
                    </g:each>
                </select> H :
                <select id="t202TglJamApp_minute" name="t202TglJamApp_minute" style="width: 50px" required="">
                    <g:each var="i" in="${(0..<60)}">
                        <g:if test="${i < 10}">
                            <option value="${i}">0${i}</option>
                        </g:if>
                        <g:else>
                            <option value="${i}">${i}</option>
                        </g:else>
                    </g:each>
                </select> m
            </td>
        </tr>
        <tr>
            <td>Catatan Appointment</td>
            <td>:</td>
            <td style="width: 75%;">
                <g:textArea name="t202CatAppointment" id="t202CatAppointment" style="width:100%;height: 70px"/>
            </td>
        </tr>

    </table>
</div>

<div class="box">
    <button class="btn btn-primary" id="ViewServiceHistory">View Service History ...</button>
</div>

</td>

</tr>
</table>
</fieldset>


<fieldset class="buttons controls" style="text-align: right;">

    %{--<button class="btn btn-primary" id="buttonEdit" onclick="onEdit()">Edit</button>--}%
    <button class="btn btn-primary" id="buttonSave">Save</button>
    %{--<button class="btn btn-primary" id="buttonCancel" onclick="onCancel()">Cancel</button>--}%

    <button class="btn btn-primary" id="buttonClose" onclick="window.location.replace('#/home');">Close</button>

</fieldset>
</g:form>


<div id="ViewServiceHistoryModal" class="modal fade">
    <div class="modal-dialog" style="width: 1200px;">
        <div class="modal-content" style="width: 1200px;">
            <!-- dialog body -->
            <div class="modal-body" style="max-height: 1200px;">
                <div id="ViewServiceHistoryContent"/>

                <div class="iu-content"></div>
            </div>
            <!-- dialog buttons -->
            <div class="modal-footer">
                <button id='closeViewServiceHistory' type="button" data-dismiss="modal"
                        class="btn btn-primary">Close</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>