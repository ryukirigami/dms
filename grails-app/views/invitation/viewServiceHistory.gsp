<%--
  Created by IntelliJ IDEA.
  User: Mohammad Fauzi
  Date: 2/6/14
  Time: 4:50 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title>Search Job untuk Field Action</title>
    <r:require modules="baseapplayout"/>
</head>

<body>

<div class="content scaffold-edit" role="main">
    <fieldset class="form">

        <div class="box">
            <legend style="font-size: small">Service History</legend>
            <g:render template="historyServiceDataTables"/>
        </div>

    </fieldset>
</div>

</body>
</html>