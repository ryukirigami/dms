
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title>Daftar Piutang</title>
    <r:require modules="baseapplayout, baseapplist" />
</head>

<body>
    <div class="navbar box-header no-border">
        <span class="pull-left">Daftar Piutang</span>
    </div>
    <div class="box">
        <div class="span6">
            <a href="javascript:void(0);" class="btn btn-primary">Cetak Daftar Penagihan</a>
        </div>
        <div class="span12" style="margin-left: 0;">
            <table id="piutangList_dataTable" cellpadding="0" cellspacing="0"
                   border="0"
                   class="display table table-striped table-bordered table-hover"
                   width="100%">
                <thead>
                <tr>
                    <th style="border-bottom: none;padding: 5px;">
                        <div><g:message code="collection.invoiceT071.label" default="Nomor Invoice" /></div>
                    </th>
                    <th style="border-bottom: none;padding: 5px;">
                        <div><g:message code="collection.invoiceT071.label" default="Tanggal Invoice" /></div>
                    </th>
                    <th style="border-bottom: none;padding: 5px;">
                        <div><g:message code="collection.reception.label" default="Nomor WO" /></div>
                    </th>
                    <th style="border-bottom: none;padding: 5px;">
                        <div><g:message code="collection.nopol.label" default="Nomor Polisi" /></div>
                    </th>
                    <th style="border-bottom: none;padding: 5px;">
                        <div><g:message code="collection.nama.label" default="Nama Pelanggan" /></div>
                    </th>
                    <th style="border-bottom: none;padding: 5px;">
                        <div><g:message code="collection.jmlInvoice.label" default="Jml Invoice" /></div>
                    </th>
                    <th style="border-bottom: none;padding: 5px;">
                        <div><g:message code="collection.dp.label" default="Uang Muka" /></div>
                    </th>
                    <th style="border-bottom: none;padding: 5px;">
                        <div><g:message code="collection.paid.label" default="Jml Sudah Di bayar" /></div>
                    </th>
                    <th style="border-bottom: none;padding: 5px;">
                        <div><g:message code="collection.sisa.label" default="Sisa Tagihan" /></div>
                    </th>
                    <th style="border-bottom: none;padding: 5px;">
                        <div><g:message code="collection.namaSa.label" default="Nama SA" /></div>
                    </th>
                    <th style="border-bottom: none;padding: 5px;">
                        <div><g:message code="collection.dueDate.label" default="Jatuh Tempo" /></div>
                    </th>
                </tr>
                <tr>
                    <th style="border-top: none;padding: 5px;">
                        <div id="filter_noInv" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                            <input type="text" name="search_noInv" class="search_init" />
                        </div>
                    </th>
                    <th style="border-top: none;padding: 5px;">
                        <div id="filter_tgglInv" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
                            <input type="hidden" name="search_tgglInv" value="date.struct">
                            <input type="hidden" name="search_tgglInv_day" id="search_tgglInv_day" value="">
                            <input type="hidden" name="search_tgglInv_month" id="search_tgglInv_month" value="">
                            <input type="hidden" name="search_tgglInv_year" id="search_tgglInv_year" value="">
                            <input type="text" data-date-format="dd/mm/yyyy" name="search_tgglInv_dp" value="" id="search_tgglInv" class="search_init">
                        </div>
                    </th>
                    <th style="border-top: none;padding: 5px;">
                        <div id="filter_noWo" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                            <input type="text" name="search_noWo" class="search_init" />
                        </div>
                    </th>
                    <th style="border-top: none;padding: 5px;">
                        <div id="filter_noPol" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                            <input type="text" name="search_noPol" class="search_init" />
                        </div>
                    </th>
                    <th style="border-top: none;padding: 5px;">
                        <div id="filter_customer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                            <input type="text" name="search_customer" class="search_init" />
                        </div>
                    </th>
                    <th style="border-top: none;padding: 5px;">
                        <div id="filter_jumlah" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                        </div>
                    </th>
                    <th style="border-top: none;padding: 5px;">
                        <div id="filter_dp" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                        </div>
                    </th>
                    <th style="border-top: none;padding: 5px;">
                        <div id="filter_paid" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                        </div>
                    </th>
                    <th style="border-top: none;padding: 5px;">
                        <div id="filter_sisa" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                        </div>
                    </th>
                    <th style="border-top: none;padding: 5px;">
                        <div id="filter_sa" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                            <input type="text" name="search_sa" class="search_init" />
                        </div>
                    </th>
                    <th style="border-top: none;padding: 5px;">
                        <div id="filter_dueDate" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
                            <input type="hidden" name="search_dueDate" value="date.struct">
                            <input type="hidden" name="search_dueDate_day" id="search_dueDate_day" value="">
                            <input type="hidden" name="search_dueDate_month" id="search_dueDate_month" value="">
                            <input type="hidden" name="search_dueDate_year" id="search_dueDate_year" value="">
                            <input type="text" data-date-format="dd/mm/yyyy" name="search_dueDate_dp" value="" id="search_dueDate" class="search_init">
                        </div>
                    </th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
<g:render template="../menu/maxLineDisplay"/>
<g:javascript>
    var piutangListTable;
    $(function() {

$('#search_tgglInv').datepicker().on('changeDate', function(ev) {
    var newDate = new Date(ev.date);
    $('#search_tgglInv_day').val(newDate.getDate());
    $('#search_tgglInv_month').val(newDate.getMonth()+1);
    $('#search_tgglInv_year').val(newDate.getFullYear());
    $(this).datepicker('hide');
    piutangListTable.fnDraw();
});

$('#search_dueDate').datepicker().on('changeDate', function(ev) {
    var newDate = new Date(ev.date);
    $('#search_dueDate_day').val(newDate.getDate());
    $('#search_dueDate_month').val(newDate.getMonth()+1);
    $('#search_dueDate_year').val(newDate.getFullYear());
    $(this).datepicker('hide');
    piutangListTable.fnDraw();
});


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	piutangListTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

        piutangListTable = $("#piutangList_dataTable").dataTable({
            "sScrollX": "100%",
            "bScrollCollapse": true,
            "bAutoWidth" : false,
            "bPaginate" : true,
            "sInfo" : "",
            "sInfoEmpty" : "",
            "sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
            bFilter: true,
            "bStateSave": false,
            'sPaginationType': 'bootstrap',
            "fnInitComplete": function () {
                this.fnAdjustColumnSizing(true);
               },
               "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                return nRow;
               },
            "bSort": true,
            "bProcessing": true,
            "bServerSide": true,
            "sServerMethod": "POST",
            "iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
            "sAjaxSource": "${g.createLink(action: "datatablesList")}",
            "aoColumns": [
                {"sName": "t701NoInv","mDataProp": "t701NoInv","aTargets": [1],"bSearchable": true,"bSortable": false,"sWidth":"200px","bVisible": true},
                {"sName": "t701TglJamInvoice","mDataProp": "t701TglJamInvoice","aTargets": [2],"bSearchable": true,"bSortable": false,"sWidth":"200px","bVisible": true},
                {"sName": "reception","mDataProp": "reception","aTargets": [3],"bSearchable": true,"bSortable": false,"sWidth":"200px","bVisible": true},
                {"sName": "t701Nopol","mDataProp": "t701Nopol","aTargets": [4],"bSearchable": true,"bSortable": false,"sWidth":"200px","bVisible": true},
                {"sName": "t701Customer","mDataProp": "t701Customer","aTargets": [5],"bSearchable": true,"bSortable": false,"sWidth":"200px","bVisible": true},
                {"sName": "t701TotalInv","mDataProp": "t701TotalInv","aTargets": [6],"bSearchable": true,"bSortable": false,"sWidth":"200px","bVisible": true},
                {"sName": "t701BookingFee","mDataProp": "t701BookingFee","aTargets": [7],"bSearchable": true,"bSortable": false,"sWidth":"200px","bVisible": true},
                {"sName": "t701TotalBayarRp","mDataProp": "t701TotalBayarRp","aTargets": [8],"bSearchable": true,"bSortable": false,"sWidth":"200px","bVisible": true},
                {"sName": "t701TotalRp","mDataProp": "t701TotalRp","aTargets": [9],"bSearchable": true,"bSortable": false,"sWidth":"200px","bVisible": true},
                {"sName": "t701xNamaUser","mDataProp": "t701xNamaUser","aTargets": [10],"bSearchable": true,"bSortable": false,"sWidth":"200px","bVisible": true},
                {"sName": "t701TglJamCetak","mDataProp": "t701TglJamCetak","aTargets": [11],"bSearchable": true,"bSortable": false,"sWidth":"200px","bVisible": true}],
            "fnServerData": function(sSource, aoData, fnCallback, oSettings) {
                var noInv = $('#filter_noInv input').val();
                if(noInv){
                    aoData.push(
                            {"name": 'sCriteria_noInv', "value": noInv}
                    );
                }

                var tgglInv = $('#search_tgglInv').val();
                var tgglInvDay = $('#search_tgglInv_day').val();
                var tgglInvMonth = $('#search_tgglInv_month').val();
                var tgglInvYear = $('#search_tgglInv_year').val();

                if(tgglInv){
                    aoData.push(
                            {"name": 'sCriteria_tgglInv', "value": "date.struct"},
                            {"name": 'sCriteria_tgglInv_dp', "value": tgglInv},
                            {"name": 'sCriteria_tgglInv_day', "value": tgglInvDay},
                            {"name": 'sCriteria_tgglInv_month', "value": tgglInvMonth},
                            {"name": 'sCriteria_tgglInv_year', "value": tgglInvYear}
                    );
                }

                var dueDate = $('#search_dueDate').val();
                var dueDateDay = $('#search_dueDate_day').val();
                var dueDateMonth = $('#search_dueDate_month').val();
                var dueDateYear = $('#search_dueDate_year').val();

                if(dueDate){
                    aoData.push(
                            {"name": 'sCriteria_dueDate', "value": "date.struct"},
                            {"name": 'sCriteria_dueDate_dp', "value": dueDate},
                            {"name": 'sCriteria_dueDate_day', "value": dueDateDay},
                            {"name": 'sCriteria_dueDate_month', "value": dueDateMonth},
                            {"name": 'sCriteria_dueDate_year', "value": dueDateYear}
                    );
                }

                var noWo = $('#filter_noWo input').val();
                if(noWo){
                    aoData.push(
                            {"name": 'sCriteria_noWo', "value": noWo}
                    );
                }

                var noPol = $('#filter_noPol input').val();
                if(noPol){
                    aoData.push(
                            {"name": 'sCriteria_noPol', "value": noPol}
                    );
                }

                var customer = $('#filter_customer input').val();
                if(customer){
                    aoData.push(
                            {"name": 'sCriteria_customer', "value": customer}
                    );
                }

                var jumlah = $('#filter_jumlah input').val();
                if(jumlah){
                    aoData.push(
                            {"name": 'sCriteria_jumlah', "value": jumlah}
                    );
                }

                var dp = $('#filter_dp input').val();
                if(dp){
                    aoData.push(
                            {"name": 'sCriteria_dp', "value": dp}
                    );
                }

                var sisa = $('#filter_sisa input').val();
                if(sisa){
                    aoData.push(
                            {"name": 'sCriteria_sisa', "value": sisa}
                    );
                }

                var paid = $('#filter_paid input').val();
                if(paid){
                    aoData.push(
                            {"name": 'sCriteria_paid', "value": paid}
                    );
                }

                var sa = $('#filter_sa input').val();
                if(sa){
                    aoData.push(
                            {"name": 'sCriteria_sa', "value": sa}
                    );
                }



                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData ,
                    "success": function (json) {
                        fnCallback(json);
                    },
                    "complete": function () {}
			    });
            }
        });
    });
</g:javascript>
</body>
</html>
