
<%@ page import="com.kombos.administrasi.DealerPenjual" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="dealerPenjual_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="dealerPenjual.m091NamaDealer.label" default="Nama Dealer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="dealerPenjual.m091AlamatDealer.label" default="Alamat Dealer" /></div>
			</th>

%{--
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="dealerPenjual.m091ID.label" default="ID" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="dealerPenjual.staDel.label" default="Sta Del" /></div>
			</th>
--}%
		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m091NamaDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m091NamaDealer" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m091AlamatDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m091AlamatDealer" class="search_init" />
				</div>
			</th>
%{--	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m091ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m091ID" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_staDel" class="search_init" />
				</div>
			</th>
--}%

		</tr>
	</thead>
</table>

<g:javascript>
var DealerPenjualTable;
var reloadDealerPenjualTable;
$(function(){

	reloadDealerPenjualTable = function() {
		DealerPenjualTable.fnDraw();
	}

	var recordsDealerPenjualPerPage = [];
    var anDealerPenjualSelected;
    var jmlRecDealerPenjualPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	DealerPenjualTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	DealerPenjualTable = $('#dealerPenjual_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsDealerPenjual = $("#dealerPenjual_datatables tbody .row-select");
            var jmlDealerPenjualCek = 0;
            var nRow;
            var idRec;
            rsDealerPenjual.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsDealerPenjualPerPage[idRec]=="1"){
                    jmlDealerPenjualCek = jmlDealerPenjualCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsDealerPenjualPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecDealerPenjualPerPage = rsDealerPenjual.length;
            if(jmlDealerPenjualCek==jmlRecDealerPenjualPerPage && jmlRecDealerPenjualPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m091NamaDealer",
	"mDataProp": "m091NamaDealer",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m091AlamatDealer",
	"mDataProp": "m091AlamatDealer",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m091ID",
	"mDataProp": "m091ID",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "staDel",
	"mDataProp": "staDel",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m091NamaDealer = $('#filter_m091NamaDealer input').val();
						if(m091NamaDealer){
							aoData.push(
									{"name": 'sCriteria_m091NamaDealer', "value": m091NamaDealer}
							);
						}
	
						var m091AlamatDealer = $('#filter_m091AlamatDealer input').val();
						if(m091AlamatDealer){
							aoData.push(
									{"name": 'sCriteria_m091AlamatDealer', "value": m091AlamatDealer}
							);
						}
	
						var m091ID = $('#filter_m091ID input').val();
						if(m091ID){
							aoData.push(
									{"name": 'sCriteria_m091ID', "value": m091ID}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

	$('.select-all').click(function(e) {

        $("#dealerPenjual_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsDealerPenjualPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsDealerPenjualPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#dealerPenjual_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsDealerPenjualPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anDealerPenjualSelected = DealerPenjualTable.$('tr.row_selected');
            if(jmlRecDealerPenjualPerPage == anDealerPenjualSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsDealerPenjualPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
