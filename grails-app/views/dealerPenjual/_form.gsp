<%@ page import="com.kombos.administrasi.DealerPenjual" %>
<g:javascript>
    $(document).ready(function() {
        if($('#m091AlamatDealer').val().length>0){
            $('#hitung').text(255 - $('#m091AlamatDealer').val().length);
        }

        $('#m091AlamatDealer').keyup(function() {
            var len = this.value.length;
            if (len >= 255) {
                this.value = this.value.substring(0, 255);
                len = 255;
            }
            $('#hitung').text(255 - len);
        });
    });
</g:javascript>



<div class="control-group fieldcontain ${hasErrors(bean: dealerPenjualInstance, field: 'm091NamaDealer', 'error')} required">
	<label class="control-label" for="m091NamaDealer">
		<g:message code="dealerPenjual.m091NamaDealer.label" default="Nama Dealer" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m091NamaDealer" maxlength="50" required="" value="${dealerPenjualInstance?.m091NamaDealer}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: dealerPenjualInstance, field: 'm091AlamatDealer', 'error')} required">
	<label class="control-label" for="m091AlamatDealer">
		<g:message code="dealerPenjual.m091AlamatDealer.label" default="Alamat Dealer" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textArea name="m091AlamatDealer" required="" value="${dealerPenjualInstance?.m091AlamatDealer}"/>
        <br/>
        <span id="hitung">255</span> Karakter Tersisa.
	</div>
</div>

<g:javascript>
    document.getElementById("m091NamaDealer").focus();
</g:javascript>
