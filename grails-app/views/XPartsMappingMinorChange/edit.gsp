<%@ page import="com.kombos.administrasi.XPartsMappingMinorChange" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'XPartsMappingMinorChange.label', default: 'Mapping Full Mode Parts Minor Change')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="edit-XPartsMappingMinorChange" class="content scaffold-edit" role="main">
			<legend><g:message code="default.edit.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${XPartsMappingMinorChangeInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${XPartsMappingMinorChangeInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form method="post"  enctype="multipart/form-data">--}%

			%{--</g:form>--}%
            <form id="XPartsMappingMinorChange-update" class="form-horizontal" action="${request.getContextPath()}/XPartsMappingMinorChange/update" method="post" onsubmit="updateForm();return false;">
                <fieldset class="form">
                    <g:render template="form"/>
                </fieldset>
                <fieldset class="buttons controls">
                    <g:actionSubmit class="btn btn-primary save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}"
                                    onclick="return confirm('${message(code: 'default.button.save.confirm.message', default: 'Anda yakin data akan disimpan?')}');"/>
                    <a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout();"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
                </fieldset>
            </form>
            %{--</g:form>--}%

            <g:javascript>
			var updateForm;
			$(function(){

			function progress(e){
		        if(e.lengthComputable){
		            //kalo mau pake progress bar
		            //$('progress').attr({value:e.loaded,max:e.total});
		        }
		    }

			updateForm = function() {

			var form = new FormData($('#XPartsMappingMinorChange-update')[0]);

			$.ajax({
                url:'${request.getContextPath()}/XPartsMappingMinorChange/update',
                type: 'POST',
                xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){
                        myXhr.upload.addEventListener('progress',progress, false);
                    }
                    return myXhr;
                },
                //add beforesend handler to validate or something
                //beforeSend: functionname,
                success: function (res) {
     				$('#XPartsMappingMinorChange-form').empty();
					$('#XPartsMappingMinorChange-form').append(res);
					reloadXPartsMappingMinorChangeTable();
                },
                //add error handler for when a error occurs if you want!
                error: function (data, status, e){
					alert(e);
				},
                data: form,
                cache: false,
                contentType: false,
                processData: false
            });

				}
			});
            </g:javascript>
		</div>
	</body>
</html>
