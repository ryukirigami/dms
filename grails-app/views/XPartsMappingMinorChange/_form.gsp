<%@ page import="com.kombos.parts.Goods; com.kombos.parts.Satuan; com.kombos.administrasi.FullModelCode; com.kombos.administrasi.XPartsMappingMinorChange" %>
<g:javascript>
var isNumberKey;

$(function(){

    isNumberKey = function(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
});

</g:javascript>



<div class="control-group fieldcontain ${hasErrors(bean: XPartsMappingMinorChangeInstance, field: 'fullModelCode', 'error')} required">
	<label class="control-label" for="fullModelCode">
		<g:message code="XPartsMappingMinorChange.fullModelCode.label" default="Full Mode" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:hiddenField name="id" value="${XPartsMappingMinorChangeInstance?.id}"/>
	<g:select id="fullModelCode" name="fullModelCode.id" from="${FullModelCode.createCriteria().list(){eq("staDel", "0");order("t110FullModelCode", "asc")}}" optionKey="id" required="" value="${XPartsMappingMinorChangeInstance?.fullModelCode?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: XPartsMappingMinorChangeInstance, field: 't111xBln', 'error')} ">
    <label class="control-label" for="t111xBln">
        <g:message code="XPartsMappingMinorChange.t111xBln.label" default="Bulan Produksi" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="t111xBln" name="t111xBln" value="${bulan}"
                  optionKey="nama" optionValue="nama"
                  from="${bulans}"></g:select>
    </div>

</div>

<div class="control-group fieldcontain ${hasErrors(bean: XPartsMappingMinorChangeInstance, field: 't111xThn', 'error')} ">
    <label class="control-label" for="t111xThn">
        <g:message code="XPartsMappingMinorChange.t111xThn.label" default="Tahun Produksi" />
        <span class="required-indicator">*</span>

    </label>
    <div class="controls">
       <g:select id="t111xThn" name="t111xThn" value="${tahun}"  from="${tahuns}"></g:select>
    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: XPartsMappingMinorChangeInstance, field: 'goods', 'error')} required">
	<label class="control-label" for="goods">
		<g:message code="XPartsMappingMinorChange.goods.label" default="Kode Parts" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="goods" name="goods.id" from="${Goods.createCriteria().list(){eq("staDel", "0");order("m111ID", "asc")}}" optionKey="id" required="" value="${XPartsMappingMinorChangeInstance?.goods?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: XPartsMappingMinorChangeInstance, field: 't111xStaIO', 'error')} required">
	<label class="control-label" for="t111xStaIO">
		<g:message code="XPartsMappingMinorChange.t111xStaIO.label" default="Status Perubahan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
    <g:radioGroup name="t111xStaIO" id="t111xStaIO" values="['1','0']"  labels="['Tambah','Kurang']" value="${XPartsMappingMinorChangeInstance?.t111xStaIO}" >
        ${it.radio} ${it.label}
    </g:radioGroup>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: XPartsMappingMinorChangeInstance, field: 't111xJumlah1', 'error')} required">
	<label class="control-label" for="t111xJumlah1">
		<g:message code="XPartsMappingMinorChange.t111xJumlah1.label" default="Jumlah" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t111xJumlah1" value="${fieldValue(bean: XPartsMappingMinorChangeInstance, field: 't111xJumlah1')}" style="text-align:right; width: 100px" required="" maxlength="12" onkeypress="return isNumberKey(event);"/>
    <g:select id="satuan" name="satuan.id" from="${Satuan.list()}" style="width : 100px;" optionKey="id" required="" value="${XPartsMappingMinorChangeInstance?.satuan?.id}" class="many-to-one"/>
	</div>
</div>



%{--<div class="control-group fieldcontain ${hasErrors(bean: XPartsMappingMinorChangeInstance, field: 'satuan', 'error')} required">--}%
    %{--<label class="control-label" for="satuan">--}%
        %{--<g:message code="XPartsMappingMinorChange.satuan.label" default="Satuan" />--}%
        %{--<span class="required-indicator">*</span>--}%
    %{--</label>--}%
    %{--<div class="controls">--}%
        %{----}%
    %{--</div>--}%
%{--</div>--}%

<div class="control-group fieldcontain ${hasErrors(bean: XPartsMappingMinorChangeInstance, field: 'gambar', 'error')} ">
	<label class="control-label" for="gambar">
		<g:message code="XPartsMappingMinorChange.gambar.label" default="Upload Gambar" />
		
	</label>
	<div class="controls">
	<input type="file" id="gambar" name="gambar" />
	</div>
</div>



