

<%@ page import="com.kombos.administrasi.XPartsMappingMinorChange" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'XPartsMappingMinorChange.label', default: 'Mapping Full Mode Parts Minor Change')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteXPartsMappingMinorChange;

$(function(){ 
	deleteXPartsMappingMinorChange=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/XPartsMappingMinorChange/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadXPartsMappingMinorChangeTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-XPartsMappingMinorChange" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="XPartsMappingMinorChange"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${XPartsMappingMinorChangeInstance?.fullModelCode}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="fullModelCode-label" class="property-label"><g:message
					code="XPartsMappingMinorChange.fullModelCode.label" default="Full Model" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="fullModelCode-label">
						%{--<ba:editableValue
								bean="${XPartsMappingMinorChangeInstance}" field="fullModelCode"
								url="${request.contextPath}/XPartsMappingMinorChange/updatefield" type="text"
								title="Enter fullModelCode" onsuccess="reloadXPartsMappingMinorChangeTable();" />--}%
							
								${XPartsMappingMinorChangeInstance?.fullModelCode?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${XPartsMappingMinorChangeInstance?.t111xBln}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t111xBln-label" class="property-label"><g:message
                                code="XPartsMappingMinorChange.t111xBln.label" default="Bulan" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t111xBln-label">
                        %{--<ba:editableValue
                                bean="${XPartsMappingMinorChangeInstance}" field="t111xBln"
                                url="${request.contextPath}/XPartsMappingMinorChange/updatefield" type="text"
                                title="Enter t111xBln" onsuccess="reloadXPartsMappingMinorChangeTable();" />--}%

                        <g:fieldValue bean="${XPartsMappingMinorChangeInstance}" field="t111xBln"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${XPartsMappingMinorChangeInstance?.t111xThn}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t111xThn-label" class="property-label"><g:message
                                code="XPartsMappingMinorChange.t111xThn.label" default="Tahun" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t111xThn-label">
                        %{--<ba:editableValue
                                bean="${XPartsMappingMinorChangeInstance}" field="t111xThn"
                                url="${request.contextPath}/XPartsMappingMinorChange/updatefield" type="text"
                                title="Enter t111xThn" onsuccess="reloadXPartsMappingMinorChangeTable();" />--}%

                        <g:fieldValue bean="${XPartsMappingMinorChangeInstance}" field="t111xThn"/>

                    </span></td>

                </tr>
            </g:if>



				<g:if test="${XPartsMappingMinorChangeInstance?.goods}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="goods-label" class="property-label"><g:message
					code="XPartsMappingMinorChange.goods.label" default="Kode Parts" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="goods-label">
						%{--<ba:editableValue
								bean="${XPartsMappingMinorChangeInstance}" field="goods"
								url="${request.contextPath}/XPartsMappingMinorChange/updatefield" type="text"
								title="Enter goods" onsuccess="reloadXPartsMappingMinorChangeTable();" />--}%
							
								${XPartsMappingMinorChangeInstance?.goods?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${XPartsMappingMinorChangeInstance?.t111xStaIO}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t111xStaIO-label" class="property-label"><g:message
					code="XPartsMappingMinorChange.t111xStaIO.label" default="Status Perubahan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t111xStaIO-label">
						%{--<ba:editableValue
								bean="${XPartsMappingMinorChangeInstance}" field="t111xStaIO"
								url="${request.contextPath}/XPartsMappingMinorChange/updatefield" type="text"
								title="Enter t111xStaIO" onsuccess="reloadXPartsMappingMinorChangeTable();" />--}%

                        <g:if test="${XPartsMappingMinorChangeInstance?.t111xStaIO == '1'}">
                            <g:message code="companyDealer.m703StaPersenRp.label.Persen" default="Tambah" />
                        </g:if>
                        <g:else>
                            <g:message code="companyDealer.m703StaPersenRp.label.Rupiah" default="Kurang" />
                        </g:else>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${XPartsMappingMinorChangeInstance?.t111xJumlah1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t111xJumlah1-label" class="property-label"><g:message
					code="XPartsMappingMinorChange.t111xJumlah1.label" default="Jumlah" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t111xJumlah1-label">
						%{--<ba:editableValue
								bean="${XPartsMappingMinorChangeInstance}" field="t111xJumlah1"
								url="${request.contextPath}/XPartsMappingMinorChange/updatefield" type="text"
								title="Enter t111xJumlah1" onsuccess="reloadXPartsMappingMinorChangeTable();" />--}%
							
								<g:fieldValue bean="${XPartsMappingMinorChangeInstance}" field="t111xJumlah1"/>
							
						</span></td>
					
				</tr>
				</g:if>
                <g:if test="${XPartsMappingMinorChangeInstance?.satuan}">
                    <tr>
                        <td class="span2" style="text-align: right;"><span
                                id="satuan-label" class="property-label"><g:message
                                    code="XPartsMappingMinorChange.satuan.label" default="Satuan" />:</span></td>

                        <td class="span3"><span class="property-value"
                                                aria-labelledby="satuan-label">
                            %{--<ba:editableValue
                                    bean="${XPartsMappingMinorChangeInstance}" field="satuan"
                                    url="${request.contextPath}/XPartsMappingMinorChange/updatefield" type="text"
                                    title="Enter satuan" onsuccess="reloadXPartsMappingMinorChangeTable();" />--}%

                            ${XPartsMappingMinorChangeInstance?.satuan?.encodeAsHTML()}


                        </span></td>
                    
                    </tr>
                </g:if>

				<g:if test="${XPartsMappingMinorChangeInstance?.gambar}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="gambar-label" class="property-label"><g:message
					code="XPartsMappingMinorChange.gambar.label" default="Gambar" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="gambar-label">
						%{--<ba:editableValue
								bean="${XPartsMappingMinorChangeInstance}" field="gambar"
								url="${request.contextPath}/XPartsMappingMinorChange/updatefield" type="text"
								title="Enter gambar" onsuccess="reloadXPartsMappingMinorChangeTable();" />--}%
                            <img class="" src="${createLink(controller:'XPartsMappingMinorChange', action:'showGambar', params : [id : XPartsMappingMinorChangeInstance.id, random : new Date().toTimestamp()])}" />

                        </span></td>
					
				</tr>
				</g:if>

				<g:if test="${XPartsMappingMinorChangeInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="XPartsMappingMinorChange.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${XPartsMappingMinorChangeInstance}" field="lastUpdProcess"
								url="${request.contextPath}/XPartsMappingMinorChange/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadXPartsMappingMinorChangeTable();" />--}%

								<g:fieldValue bean="${XPartsMappingMinorChangeInstance}" field="lastUpdProcess"/>

						</span></td>

				</tr>
				</g:if>

				<g:if test="${XPartsMappingMinorChangeInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="XPartsMappingMinorChange.dateCreated.label" default="Date Created" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${XPartsMappingMinorChangeInstance}" field="dateCreated"
								url="${request.contextPath}/XPartsMappingMinorChange/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadXPartsMappingMinorChangeTable();" />--}%

								<g:formatDate date="${XPartsMappingMinorChangeInstance?.dateCreated}" type="datetime" style="MEDIUM"/>

						</span></td>

				</tr>
				</g:if>

            <g:if test="${XPartsMappingMinorChangeInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="XPartsMappingMinorChange.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${XPartsMappingMinorChangeInstance}" field="createdBy"
                                url="${request.contextPath}/XPartsMappingMinorChange/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadXPartsMappingMinorChangeTable();" />--}%

                        <g:fieldValue bean="${XPartsMappingMinorChangeInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

				<g:if test="${XPartsMappingMinorChangeInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="XPartsMappingMinorChange.lastUpdated.label" default="Last Updated" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${XPartsMappingMinorChangeInstance}" field="lastUpdated"
								url="${request.contextPath}/XPartsMappingMinorChange/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadXPartsMappingMinorChangeTable();" />--}%

								<g:formatDate date="${XPartsMappingMinorChangeInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>

						</span></td>

				</tr>
				</g:if>

            <g:if test="${XPartsMappingMinorChangeInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="XPartsMappingMinorChange.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${XPartsMappingMinorChangeInstance}" field="updatedBy"
                                url="${request.contextPath}/XPartsMappingMinorChange/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadXPartsMappingMinorChangeTable();" />--}%

                        <g:fieldValue bean="${XPartsMappingMinorChangeInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>


            </tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${XPartsMappingMinorChangeInstance?.id}"
					update="[success:'XPartsMappingMinorChange-form',failure:'XPartsMappingMinorChange-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteXPartsMappingMinorChange('${XPartsMappingMinorChangeInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
