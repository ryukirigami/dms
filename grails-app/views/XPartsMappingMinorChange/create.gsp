<%@ page import="com.kombos.administrasi.XPartsMappingMinorChange" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'XPartsMappingMinorChange.label', default: 'Full Model Parts Mapping Minor Change')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="create-XPartsMappingMinorChange" class="content scaffold-create" role="main">
			<legend><g:message code="default.create.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${XPartsMappingMinorChangeInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${XPartsMappingMinorChangeInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form action="save"  enctype="multipart/form-data">--}%

			%{--</g:form>--}%
            <form id="XPartsMappingMinorChange-save" class="form-horizontal" action="${request.getContextPath()}/XPartsMappingMinorChange/save" method="post" onsubmit="submitForm();return false;">
                <fieldset class="form">
                    <g:render template="form"/>
                </fieldset>
                <fieldset class="buttons controls">
                    <a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout();"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
                    <g:submitButton class="btn btn-primary create" name="save" value="${message(code: 'default.button.create.label', default: 'Create')}"
                                    onclick="return numberVal();"/>
                </fieldset>
            </form>
            <g:javascript>
			var submitForm;
			$(function(){

			function progress(e){
		        if(e.lengthComputable){
		            //kalo mau pake progress bar
		            //$('progress').attr({value:e.loaded,max:e.total});
		        }
		    }

			submitForm = function() {

   			var form = new FormData($('#XPartsMappingMinorChange-save')[0]);

			$.ajax({
                url:'${request.getContextPath()}/XPartsMappingMinorChange/save',
                type: 'POST',
                xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){
                        myXhr.upload.addEventListener('progress',progress, false);
                    }
                    return myXhr;
                },
                success: function (res) {
     				$('#XPartsMappingMinorChange-form').empty();
					$('#XPartsMappingMinorChange-form').append(res);
					reloadXPartsMappingMinorChangeTable();
                },
                //add error handler for when a error occurs if you want!
                error: function (data, status, e){
					alert(e);
				},
                data: form,
                cache: false,
                contentType: false,
                processData: false
            });

				}
			});
            </g:javascript>
        </div>
		</div>
	</body>
</html>
