
<%@ page import="com.kombos.administrasi.XPartsMappingMinorChange" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="XPartsMappingMinorChange_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="XPartsMappingMinorChange.fullModelCode.label" default="Full Model" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="XPartsMappingMinorChange.t111xBln.label" default="Bulan Produksi" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="XPartsMappingMinorChange.t111xThn.label" default="Tahun Produksi" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="XPartsMappingMinorChange.t111xStaIO.label" default="Status Perubahan" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
				<div><g:message code="XPartsMappingMinorChange.goods.label" default="Kode Parts" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="XPartsMappingMinorChange.goods.label" default="Nama Parts" /></div>
            </th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="XPartsMappingMinorChange.t111xJumlah1.label" default="Jumlah" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="XPartsMappingMinorChange.satuan.label" default="Satuan" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_fullModelCode" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_fullModelCode" class="search_init" />
				</div>
			</th>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_t111xBln" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t111xBln" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t111xThn" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t111xThn" class="search_init" onkeypress="return isNumberKey(event)"/>
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t111xStaIO" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <select name="search_t111xStaIO" id="search_t111xStaIO" style="width:100%">
                        <option value=""></option>
                        <option value="1">Tambah</option>
                        <option value="0">Kurang</option>
                    </select>
                </div>
            </th>
            <th style="border-top: none;padding: 5px;">
                <div id="filter_goods" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_goods" class="search_init" />
                </div>
            </th>
            <th style="border-top: none;padding: 5px;">
                <div id="filter_goods2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_goods2" class="search_init" />
                </div>
            </th>
            <th style="border-top: none;padding: 5px;">
                <div id="filter_t111xJumlah1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t111xJumlah1" onkeypress="return isNumberKey(event)" class="search_init" />
                </div>
            </th>
            <th style="border-top: none;padding: 5px;">
                <div id="filter_satuan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_satuan" class="search_init" />
                </div>
            </th>

		</tr>
	</thead>
</table>

<g:javascript>
var XPartsMappingMinorChangeTable;
var reloadXPartsMappingMinorChangeTable;
$(function(){

    var recordsXPartsMappingMinorChangeperpage = [];//new Array();
    var anXPartsMappingMinorChangeSelected;
    var jmlRecXPartsMappingMinorChangePerPage=0;
    var id;

	
	reloadXPartsMappingMinorChangeTable = function() {
		XPartsMappingMinorChangeTable.fnDraw();
	}

	if(XPartsMappingMinorChangeTable){
	    XPartsMappingMinorChangeTable.fnDestroy();
	}

	$("#search_t111xStaIO").on("change", function(event){
		event.stopPropagation();
		XPartsMappingMinorChangeTable.fnDraw();

	});

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	XPartsMappingMinorChangeTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	XPartsMappingMinorChangeTable = $('#XPartsMappingMinorChange_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsXPartsMappingMinorChange = $("#XPartsMappingMinorChange_datatables tbody .row-select");
            var jmlXPartsMappingMinorChangeCek = 0;
            var nRow;
            var idRec;
            rsXPartsMappingMinorChange.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsXPartsMappingMinorChangeperpage[idRec]=="1"){
                    jmlXPartsMappingMinorChangeCek = jmlXPartsMappingMinorChangeCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsXPartsMappingMinorChangeperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecXPartsMappingMinorChangePerPage = rsXPartsMappingMinorChange.length;
            if(jmlXPartsMappingMinorChangeCek==jmlRecXPartsMappingMinorChangePerPage && jmlRecXPartsMappingMinorChangePerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "fullModelCode",
	"mDataProp": "fullModelCode",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "t111xBln",
	"mDataProp": "t111xBln",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t111xThn",
	"mDataProp": "t111xThn",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "t111xStaIO",
	"mDataProp": "t111xStaIO",
	"aTargets": [2],
	 "mRender": function ( data, type, row ) {

            if(data=="1"){
                  return "Tambah";
             }else if(data == "0")
             {
                  return "Kurang";
             }

        },
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "goods",
	"mDataProp": "goods",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "goods2",
	"mDataProp": "goods2",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,



{
	"sName": "t111xJumlah1",
	"mDataProp": "t111xJumlah1",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}


,

{
	"sName": "satuan",
	"mDataProp": "satuan",
	"aTargets": [12],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var fullModelCode = $('#filter_fullModelCode input').val();
						if(fullModelCode){
							aoData.push(
									{"name": 'sCriteria_fullModelCode', "value": fullModelCode}
							);
						}
	
						var goods = $('#filter_goods input').val();
						if(goods){
							aoData.push(
									{"name": 'sCriteria_goods', "value": goods}
							);
						}

						var goods2 = $('#filter_goods2 input').val();
						if(goods2){
							aoData.push(
									{"name": 'sCriteria_goods2', "value": goods2}
							);
						}
	
						var t111xStaIO = $('#filter_t111xStaIO select').val();
						if(t111xStaIO){
							aoData.push(
									{"name": 'sCriteria_t111xStaIO', "value": t111xStaIO}
							);
						}
	
						var t111xJumlah1 = $('#filter_t111xJumlah1 input').val();
						if(t111xJumlah1){
							aoData.push(
									{"name": 'sCriteria_t111xJumlah1', "value": t111xJumlah1}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var gambar = $('#filter_gambar input').val();
						if(gambar){
							aoData.push(
									{"name": 'sCriteria_gambar', "value": gambar}
							);
						}
	
						var imageMime = $('#filter_imageMime input').val();
						if(imageMime){
							aoData.push(
									{"name": 'sCriteria_imageMime', "value": imageMime}
							);
						}
	
						var t111xThn = $('#filter_t111xThn input').val();
						if(t111xThn){
							aoData.push(
									{"name": 'sCriteria_t111xThn', "value": t111xThn}
							);
						}
	
						var t111xBln = $('#filter_t111xBln input').val();
						if(t111xBln){
							aoData.push(
									{"name": 'sCriteria_t111xBln', "value": t111xBln}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}
	
						var satuan = $('#filter_satuan input').val();
						if(satuan){
							aoData.push(
									{"name": 'sCriteria_satuan', "value": satuan}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

	 $('.select-all').click(function(e) {

        $("#XPartsMappingMinorChange_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordsXPartsMappingMinorChangeperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsXPartsMappingMinorChangeperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#XPartsMappingMinorChange_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsXPartsMappingMinorChangeperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anXPartsMappingMinorChangeSelected = XPartsMappingMinorChangeTable.$('tr.row_selected');
            if(jmlRecXPartsMappingMinorChangePerPage == anXPartsMappingMinorChangeSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsXPartsMappingMinorChangeperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
