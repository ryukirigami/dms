
<%@ page import="com.kombos.administrasi.RateBank" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="rateBank_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="rateBank.bank.label" default="Nama Bank" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="rateBank.mesinEdc.label" default="Nama Mesin EDC" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="rateBank.m703TglBerlaku.label" default="Tanggal Berlaku" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="rateBank.m703JmlMin.label" default="Batas Jumlah Min" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="rateBank.m703JmlMax.label" default="Batas Jumlah Max" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="rateBank.m703StaPersenRp.label" default="Satuan Charge" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="rateBank.m703RatePersen.label" default="Persen Charge" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="rateBank.m703RateRp.label" default="Rupiah Charge" /></div>
			</th>


		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_bank" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_bank" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_mesinEdc" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_mesinEdc" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m703TglBerlaku" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m703TglBerlaku" value="date.struct">
					<input type="hidden" name="search_m703TglBerlaku_day" id="search_m703TglBerlaku_day" value="">
					<input type="hidden" name="search_m703TglBerlaku_month" id="search_m703TglBerlaku_month" value="">
					<input type="hidden" name="search_m703TglBerlaku_year" id="search_m703TglBerlaku_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m703TglBerlaku_dp" value="" id="search_m703TglBerlaku" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m703JmlMin" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m703JmlMin" class="search_init auto" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m703JmlMax" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m703JmlMax" class="search_init auto" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m703StaPersenRp" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<select name="search_m703StaPersenRp" id="search_m703StaPersenRp" style="width:100%">
                      <option value=""></option>
                      <option value="p">Persen</option>
                      <option value="r">Rupiah</option>
                    </select>
                </div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m703RatePersen" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m703RatePersen"  class="search_init persen" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m703RateRp" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m703RateRp" class="search_init auto" />
				</div>
			</th>
	
			


		</tr>
	</thead>
</table>

<g:javascript>
var RateBankTable;
var reloadRateBankTable;
$(function(){
	
	reloadRateBankTable = function() {
		RateBankTable.fnDraw();
	}

    var recordsrateBankperpage = [];//new Array();
    var anrateBankSelected;
    var jmlRecrateBankPerPage=0;
    var id;

	
	$('#search_m703TglBerlaku').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m703TglBerlaku_day').val(newDate.getDate());
			$('#search_m703TglBerlaku_month').val(newDate.getMonth()+1);
			$('#search_m703TglBerlaku_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			RateBankTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	RateBankTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

$("#search_m703StaPersenRp").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	RateBankTable.fnDraw();
		}
	});

	$("#search_m703StaPersenRp").change(function (e) {
	 	e.stopPropagation();
        RateBankTable.fnDraw();

	});



	RateBankTable = $('#rateBank_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsrateBank = $("#rateBank_datatables tbody .row-select");
            var jmlrateBankCek = 0;
            var nRow;
            var idRec;
            rsrateBank.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsrateBankperpage[idRec]=="1"){
                    jmlrateBankCek = jmlrateBankCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsrateBankperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecrateBankPerPage = rsrateBank.length;
            if(jmlrateBankCek==jmlRecrateBankPerPage && jmlRecrateBankPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "bank",
	"mDataProp": "bank",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "mesinEdc",
	"mDataProp": "mesinEdc",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m703TglBerlaku",
	"mDataProp": "m703TglBerlaku",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m703JmlMin",
	"mDataProp": "m703JmlMin",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m703JmlMax",
	"mDataProp": "m703JmlMax",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m703StaPersenRp",
	"mDataProp": "m703StaPersenRp",
	"aTargets": [5],
	"mRender": function ( data, type, row ) {

            if(data=="p"){
                  return "Persen";
             }else
             {
                  return "Rupiah";
             }

        },
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m703RatePersen",
	"mDataProp": "m703RatePersen",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m703RateRp",
	"mDataProp": "m703RateRp",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var bank = $('#filter_bank input').val();
						if(bank){
							aoData.push(
									{"name": 'sCriteria_bank', "value": bank}
							);
						}
	
						var mesinEdc = $('#filter_mesinEdc input').val();
						if(mesinEdc){
							aoData.push(
									{"name": 'sCriteria_mesinEdc', "value": mesinEdc}
							);
						}

						var m703TglBerlaku = $('#search_m703TglBerlaku').val();
						var m703TglBerlakuDay = $('#search_m703TglBerlaku_day').val();
						var m703TglBerlakuMonth = $('#search_m703TglBerlaku_month').val();
						var m703TglBerlakuYear = $('#search_m703TglBerlaku_year').val();
						
						if(m703TglBerlaku){
							aoData.push(
									{"name": 'sCriteria_m703TglBerlaku', "value": "date.struct"},
									{"name": 'sCriteria_m703TglBerlaku_dp', "value": m703TglBerlaku},
									{"name": 'sCriteria_m703TglBerlaku_day', "value": m703TglBerlakuDay},
									{"name": 'sCriteria_m703TglBerlaku_month', "value": m703TglBerlakuMonth},
									{"name": 'sCriteria_m703TglBerlaku_year', "value": m703TglBerlakuYear}
							);
						}
	
						var m703JmlMin = $('#filter_m703JmlMin input').val();
						if(m703JmlMin){
							aoData.push(
									{"name": 'sCriteria_m703JmlMin', "value": m703JmlMin}
							);
						}
	
						var m703JmlMax = $('#filter_m703JmlMax input').val();
						if(m703JmlMax){
							aoData.push(
									{"name": 'sCriteria_m703JmlMax', "value": m703JmlMax}
							);
						}
	
						var m703StaPersenRp = $('#filter_m703StaPersenRp select').val();
						if(m703StaPersenRp){
							aoData.push(
									{"name": 'sCriteria_m703StaPersenRp', "value": m703StaPersenRp}
							);
						}
	
						var m703RatePersen = $('#filter_m703RatePersen input').val();
						if(m703RatePersen){
							aoData.push(
									{"name": 'sCriteria_m703RatePersen', "value": m703RatePersen}
							);
						}
	
						var m703RateRp = $('#filter_m703RateRp input').val();
						if(m703RateRp){
							aoData.push(
									{"name": 'sCriteria_m703RateRp', "value": m703RateRp}
							);
						}
	
						var m703StaDel = $('#filter_m703StaDel input').val();
						if(m703StaDel){
							aoData.push(
									{"name": 'sCriteria_m703StaDel', "value": m703StaDel}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

	$('.select-all').click(function(e) {

        $("#rateBank_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordsrateBankperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsrateBankperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#rateBank_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsrateBankperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anrateBankSelected = RateBankTable.$('tr.row_selected');
            if(jmlRecrateBankPerPage == anrateBankSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsrateBankperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

     $('.auto').autoNumeric('init');

     $('.persen').autoNumeric('init',{
         vMin:'0',
         vMax:'100',
         mDec: '2',
         aSep:''
     });


});
</g:javascript>


			
