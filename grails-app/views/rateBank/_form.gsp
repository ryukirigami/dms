<%@ page import="com.kombos.administrasi.MesinEdc; com.kombos.administrasi.Bank; com.kombos.administrasi.RateBank" %>
<r:require modules="autoNumeric" />

<g:javascript>

        var checkBatasMax;
        var checkBatasMin;

         $(document).ready(function()
         {


             checkBatasMax = function(){
                 var max = $("#m703JmlMax").val().replace(",","");
                 var min = $("#m703JmlMin").val().replace(",","");;



                 var maxFloat = parseFloat(max);
                 var minFloat = parseFloat(min);

                 if(maxFloat<minFloat){
                     $("#m703JmlMax").val(min);
                     $("#m703JmlMax").focus();
                     alert("Batas Jumlah Max can not less than Batas Jumlah Min");
                 }
             }

             checkBatasMin = function(){
                 var max = $("#m703JmlMax").val().replace(",","");
                 var min = $("#m703JmlMin").val().replace(",","");

                 var maxFloat = parseFloat(max);
                 var minFloat = parseFloat(min);

                 if(maxFloat<minFloat){
                     $("#m703JmlMin").val(max);
                     $("#m703JmlMin").focus();
                     alert("Batas Jumlah Min can not greater than Batas Jumlah Max");
                 }

             }

             if($('input:radio[name=m703StaPersenRp]:nth(0)').is(':checked')){
                     $("#m703RatePersen").prop('disabled', false);
                     $("#m703RateRp").prop('disabled', true);
                 }else{
                     $("#m703RatePersen").prop('disabled', true);
                     $("#m703RateRp").prop('disabled', false);
                 }

             $('input:radio[name=m703StaPersenRp]').click(function(){
                if($('input:radio[name=m703StaPersenRp]:nth(0)').is(':checked')){
                    $("#m703RatePersen").prop('disabled', false);
                    $("#m703RateRp").prop('disabled', true);
                }else{
                    $("#m703RatePersen").prop('disabled', true);
                    $("#m703RateRp").prop('disabled', false);
                }
             });

             $('.auto').autoNumeric('init');

             $('.persen').autoNumeric('init',{
                 vMin:'0',
                 vMax:'100',
                 mDec: '2',
                 aSep:''
             });

         });

  
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: rateBankInstance, field: 'bank', 'error')} required">
	<label class="control-label" for="bank">
		<g:message code="rateBank.bank.label" default="Nama Bank" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="bank" name="bank.id" from="${Bank.createCriteria().list(){eq("staDel","0");order("m702NamaBank","asc")}}" optionKey="id" required="" value="${rateBankInstance?.bank?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: rateBankInstance, field: 'mesinEdc', 'error')} required">
	<label class="control-label" for="mesinEdc">
		<g:message code="rateBank.mesinEdc.label" default="Nama Mesin EDC" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="mesinEdc" name="mesinEdc.id" from="${MesinEdc.createCriteria().list(){eq("staDel","0");order("m704NamaMesinEdc","asc")}}" optionKey="id" required="" value="${rateBankInstance?.mesinEdc?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: rateBankInstance, field: 'm703TglBerlaku', 'error')} required">
	<label class="control-label" for="m703TglBerlaku">
		<g:message code="rateBank.m703TglBerlaku.label" default="Tanggal Berlaku Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<ba:datePicker name="m703TglBerlaku" precision="day" value="${rateBankInstance?.m703TglBerlaku}" format="dd/mm/yyyy"  required="true"/>
	
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: rateBankInstance, field: 'm703JmlMin', 'error')} required">
	<label class="control-label" for="m703JmlMin">
		<g:message code="rateBank.m703JmlMin.label" default="Batas Jumlah Min" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:textField name="m703JmlMin"  id="m703JmlMin" onchange="checkBatasMin()" class="auto" value="${fieldValue(bean: rateBankInstance, field: 'm703JmlMin')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: rateBankInstance, field: 'm703JmlMax', 'error')} required">
	<label class="control-label" for="m703JmlMax">
		<g:message code="rateBank.m703JmlMax.label" default="Batas Jumlah Max" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:textField name="m703JmlMax" id="m703JmlMax" class="auto" onchange="checkBatasMax()" value="${fieldValue(bean: rateBankInstance, field: 'm703JmlMax')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: rateBankInstance, field: 'm703StaPersenRp', 'error')} required ">
	<label class="control-label" for="m703StaPersenRp">
		<g:message code="rateBank.m703StaPersenRp.label" default="Satuan Charge" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:textField name="m703StaPersenRp" maxlength="1" value="${rateBankInstance?.m703StaPersenRp}"/>--}%
        <g:radioGroup name="m703StaPersenRp" id="m703StaPersenRp" values="['p','r']" labels="['Persen','Rupiah']" value="${rateBankInstance?.m703StaPersenRp}" required="" >
            ${it.radio} ${it.label}
        </g:radioGroup>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: rateBankInstance, field: 'm703RatePersen', 'error')} required">
	<label class="control-label" for="m703RatePersen">
		<g:message code="rateBank.m703RatePersen.label" default="Persen Charge" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:textField name="m703RatePersen" class="persen" id="m703RatePersen" value="${fieldValue(bean: rateBankInstance, field: 'm703RatePersen')}" required=""/> %
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: rateBankInstance, field: 'm703RateRp', 'error')} required">
	<label class="control-label" for="m703RateRp">
		<g:message code="rateBank.m703RateRp.label" default="Rupiah Charge" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:textField name="m703RateRp" class="auto"  id="m703RateRp" value="${fieldValue(bean: rateBankInstance, field: 'm703RateRp')}" required=""/>
	</div>
</div>


