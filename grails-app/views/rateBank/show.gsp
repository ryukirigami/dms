

<%@ page import="com.kombos.administrasi.RateBank" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'rateBank.label', default: 'Persen Charge')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteRateBank;

$(function(){ 
	deleteRateBank=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/rateBank/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadRateBankTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-rateBank" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="rateBank"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${rateBankInstance?.bank}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="bank-label" class="property-label"><g:message
					code="rateBank.bank.label" default="Bank" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="bank-label">
						%{--<ba:editableValue
								bean="${rateBankInstance}" field="bank"
								url="${request.contextPath}/RateBank/updatefield" type="text"
								title="Enter bank" onsuccess="reloadRateBankTable();" />--}%
							
								${rateBankInstance?.bank?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${rateBankInstance?.mesinEdc}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="mesinEdc-label" class="property-label"><g:message
					code="rateBank.mesinEdc.label" default="Mesin EDC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="mesinEdc-label">
						%{--<ba:editableValue
								bean="${rateBankInstance}" field="mesinEdc"
								url="${request.contextPath}/RateBank/updatefield" type="text"
								title="Enter mesinEdc" onsuccess="reloadRateBankTable();" />--}%
							
								${rateBankInstance?.mesinEdc?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${rateBankInstance?.m703TglBerlaku}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m703TglBerlaku-label" class="property-label"><g:message
					code="rateBank.m703TglBerlaku.label" default="Tgl Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m703TglBerlaku-label">
						%{--<ba:editableValue
								bean="${rateBankInstance}" field="m703TglBerlaku"
								url="${request.contextPath}/RateBank/updatefield" type="text"
								title="Enter m703TglBerlaku" onsuccess="reloadRateBankTable();" />--}%
							
								<g:formatDate date="${rateBankInstance?.m703TglBerlaku}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${rateBankInstance?.m703JmlMin}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m703JmlMin-label" class="property-label"><g:message
					code="rateBank.m703JmlMin.label" default="Jml Min" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m703JmlMin-label">
						%{--<ba:editableValue
								bean="${rateBankInstance}" field="m703JmlMin"
								url="${request.contextPath}/RateBank/updatefield" type="text"
								title="Enter m703JmlMin" onsuccess="reloadRateBankTable();" />--}%
							
								<g:fieldValue bean="${rateBankInstance}" field="m703JmlMin"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${rateBankInstance?.m703JmlMax}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m703JmlMax-label" class="property-label"><g:message
					code="rateBank.m703JmlMax.label" default="Jml Max" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m703JmlMax-label">
						%{--<ba:editableValue
								bean="${rateBankInstance}" field="m703JmlMax"
								url="${request.contextPath}/RateBank/updatefield" type="text"
								title="Enter m703JmlMax" onsuccess="reloadRateBankTable();" />--}%
							
								<g:fieldValue bean="${rateBankInstance}" field="m703JmlMax"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${rateBankInstance?.m703StaPersenRp}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m703StaPersenRp-label" class="property-label"><g:message
					code="rateBank.m703StaPersenRp.label" default="Sta Persen Rp" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m703StaPersenRp-label">
						%{--<ba:editableValue
								bean="${rateBankInstance}" field="m703StaPersenRp"
								url="${request.contextPath}/RateBank/updatefield" type="text"
								title="Enter m703StaPersenRp" onsuccess="reloadRateBankTable();" />
							
								<g:fieldValue bean="${rateBankInstance}" field="m703StaPersenRp"/> --}%
						<g:if test="${rateBankInstance.m703StaPersenRp == 'p'}">
                                                    <g:message code="companyDealer.m703StaPersenRp.label.Persen" default="Persen" />
                                                </g:if>
                                                <g:else>
                                                     <g:message code="companyDealer.m703StaPersenRp.label.Rupiah" default="Rupiah" />
                                                </g:else>	
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${rateBankInstance?.m703RatePersen}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m703RatePersen-label" class="property-label"><g:message
					code="rateBank.m703RatePersen.label" default="Rate Persen" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m703RatePersen-label">
						%{--<ba:editableValue
								bean="${rateBankInstance}" field="m703RatePersen"
								url="${request.contextPath}/RateBank/updatefield" type="text"
								title="Enter m703RatePersen" onsuccess="reloadRateBankTable();" />--}%
							
								<g:fieldValue bean="${rateBankInstance}" field="m703RatePersen"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${rateBankInstance?.m703RateRp}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m703RateRp-label" class="property-label"><g:message
					code="rateBank.m703RateRp.label" default="Rate Rp" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m703RateRp-label">
						%{--<ba:editableValue
								bean="${rateBankInstance}" field="m703RateRp"
								url="${request.contextPath}/RateBank/updatefield" type="text"
								title="Enter m703RateRp" onsuccess="reloadRateBankTable();" />--}%
							
								<g:fieldValue bean="${rateBankInstance}" field="m703RateRp"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${rateBankInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="jenisStall.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${jenisStallInstance}" field="dateCreated"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadJenisStallTable();" />--}%

                        <g:formatDate date="${rateBankInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${rateBankInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="jenisStall.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${jenisStallInstance}" field="createdBy"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadJenisStallTable();" />--}%

                        <g:fieldValue bean="${rateBankInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${rateBankInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="jenisStall.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${jenisStallInstance}" field="lastUpdated"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadJenisStallTable();" />--}%

                        <g:formatDate date="${rateBankInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${rateBankInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="jenisStall.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${jenisStallInstance}" field="updatedBy"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadJenisStallTable();" />--}%

                        <g:fieldValue bean="${rateBankInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${rateBankInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="jenisStall.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${jenisStallInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadJenisStallTable();" />--}%

                        <g:fieldValue bean="${rateBankInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
			
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${rateBankInstance?.id}"
					update="[success:'rateBank-form',failure:'rateBank-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteRateBank('${rateBankInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
