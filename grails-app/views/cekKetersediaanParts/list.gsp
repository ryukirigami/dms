<%@ page import="com.kombos.parts.RequestStatus" %>
<%@ page import="com.kombos.parts.SOQ" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="title" value="Cek Ketersediaan Parts" />
		<g:set var="permohonanId" value="RequestBookingFee" />
		<title>${title}</title>
		<r:require modules="baseapplayout, autoNumeric" />
		<g:javascript>
	var orderParts;
	var requestEditBookingFee;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 

    orderParts = function(id) {
    	var checkParts = [];
    	var idTO = [];
    	var none = false;
		$("#ckp-table tbody .row-select").each(function() {
        if(this.checked){
            var id = $(this).next("input:hidden").val();
			checkParts.push(id);
			if($('#tipeOrder'+id+'').val()=='null' || $('#tipeOrder'+id+'').val()==''){
			    none = true;
			}
            idTO.push($('#tipeOrder'+id+'').val());
        }
        });
		
        if(checkParts.length<1){
             alert('Anda belum memilih data yang akan ditambahkan');
             return
        }
        if(none){
             alert('Anda belum menentukan tipe order pada data yang anda pilih');
             return
        }

        var checkPartsMap = JSON.stringify(checkParts);
        var tipeOrderID = JSON.stringify(idTO);

        $('#spinner').fadeIn(1);
        $.ajax({type:'POST',
            data : { requestIds: checkPartsMap, toIds : tipeOrderID},
            url:'${request.contextPath}/cekKetersediaanParts/orderParts',
            success:function(data,textStatus){
                window.location.replace('#/validasiOrderParts');
            },
            error:function(XMLHttpRequest,textStatus,errorThrown){},
            complete:function(XMLHttpRequest,textStatus){
                $('#spinner').fadeOut();
            }
        });
    };

    deleteParts = function() {
                    bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
         			function(result){
         				if(result){
         					deleteData();
         				}
         			});
       }
     deleteData = function(){
    	var checkParts = [];
    	var idTO = [];
    	var none = false;
		$("#ckp-table tbody .row-select").each(function() {
        if(this.checked){
            var id = $(this).next("input:hidden").val();
			checkParts.push(id);
        }
        });

        if(checkParts.length<1){
             alert('Anda belum memilih data yang akan dihapus');
             return
        }
        var checkPartsMap = JSON.stringify(checkParts);

        $('#spinner').fadeIn(1);
        $.ajax({type:'POST',
            data : { requestIds: checkPartsMap},
            url:'${request.contextPath}/cekKetersediaanParts/deleteParts',
            success:function(data,textStatus){
                if(data.sukses == "ok"){
                    toastr.success("Parts Berhasil dihapus..");
                    reloadCkpTable();
                }
            },
            error:function(XMLHttpRequest,textStatus,errorThrown){},
            complete:function(XMLHttpRequest,textStatus){
                $('#spinner').fadeOut();
            }
        });
    };
    
    requestEditBookingFee = function(id) {
		var formPermohonanApproval = $('#formPermohonanApproval${permohonanId}Modal').find('form');
		var checkParts = [];
		var nonEditableParts = [];
        $("#ckp-table tbody .row-select").each(function() {
        if(this.checked){
            var id = $(this).next("input:hidden").val();			
			
			var nRow = $(this).parents('tr')[0];
            var aData = ckpTable.fnGetData(nRow);
			var typeOrderSelect = $('#tipeOrder' + aData['id'], nRow);
			var dpInput = $('#dp' + aData['id'], nRow);
            
			if(dpInput[0].value && typeOrderSelect[0].value){
				checkParts.push(id);
				formPermohonanApproval.append('<input type="hidden" name="tipeOrder-'+aData['id'] + '" value="'+typeOrderSelect[0].value+'" class="deleteafter">');
				formPermohonanApproval.append('<input type="hidden" name="dp-'+aData['id'] + '" value="'+dpInput[0].value+'" class="deleteafter">');
			} else {
				nonEditableParts.push(aData['kodePart']);
			}
        }
        });
		if(nonEditableParts.length > 0){
			alert('Part yang sudah request booking fee tidak disertakan lagi.');
		}
        if(checkParts.length < 1){
             alert('Anda belum memilih data yang akan ditambahkan');
        } else {
			$("#requestIds").val(JSON.stringify(checkParts));
			
			$.ajax({type:'POST',
				url:'${request.contextPath}/cekKetersediaanParts/generateNomorDokumenRequestBookingFee',
				success:function(data,textStatus){
					$("#t770NoDokumen").val(data.value);
				},
				error:function(XMLHttpRequest,textStatus,errorThrown){},
				complete:function(XMLHttpRequest,textStatus){
					$('#spinner').fadeOut();
				}
			});
			
			$("#formPermohonanApproval${permohonanId}Modal").modal({
							"backdrop" : "static",
							"keyboard" : true,
							"show" : true
						}).css({'width': '500px','margin-left': function () {return -($(this).width() / 2);}});
		}			
    };
    
    shrinkTableLayout = function(){
    	if($("#ckp-table").hasClass("span12")){
   			$("#ckp-table").toggleClass("span12 span5");
        }
        $("#ckp-form").css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
   		if($("#ckp-table").hasClass("span5")){
   			$("#ckp-table").toggleClass("span5 span12");
   		}
        $("#ckp-form").css("display","none");
   	}
   	
   	});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left">${title}</span>
		<ul class="nav pull-right">
			
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="ckp-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
           	<g:render template="dataTables" />
           	<fieldset class="buttons controls" style="padding-top: 30px;">
					%{--<g:field type="button" onclick="requestEditBookingFee();" class="btn btn-primary" name="request-edit-booking-fee" id="request-edit-booking-fee" value="Request Edit Booking Fee ..." />--}%
            		<g:field type="button" onclick="deleteParts();" class="btn btn-success" name="delete-order-parts" id="delete-order-parts" value="Delete Parts" />
            		<g:field type="button" onclick="orderParts();" class="btn btn-primary" name="order-parts" id="order-parts" value="Order Parts" />
			</fieldset>
            
		</div>
		<div class="span7" id="ckp-form" style="display: none;"></div>
	</div>
	<g:render template="formPermohonanApproval" />
</body>
</html>
