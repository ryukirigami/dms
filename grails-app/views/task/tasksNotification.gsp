<%@ page import="org.grails.activiti.ActivitiUtils" %>
<li>
<g:if test="${(myTasksCount + unassignedTasksCount) == 0}">
<span class="dropdown-menu-title">You don't have task.</span>
</g:if>
<g:elseif test="${(myTasksCount + unassignedTasksCount) == 1}">
<span class="dropdown-menu-title">You have 1 task.</span>
</g:elseif>
<g:else>
<span class="dropdown-menu-title">You have ${myTasksCount + unassignedTasksCount} tasks.</span>
</g:else>
</li>
<g:each in="${myTasks}" var="task">
	 <g:set var="taskId" value="${task.id}" />
     <li><a href="#" onclick="showTaskForm(${task.id})">	
		<span class="title">${taskDetail[task.id]}</span>
		<% def interval = (now - task.createTime.getTime())
			def intervalLabel = ""
			if(interval > 7200000){
				interval = interval/7200000;
				intervalLabel = Math.round(interval) + " hr"
			} else if(interval > 60000) {
				interval = interval/60000;
				intervalLabel = Math.round(interval) + " min"
			} else {
				interval = interval/1000;
				intervalLabel = Math.round(interval) + " sec"
			}
		%>
		<span class="time"><%=intervalLabel%></span>
	</a></li>
</g:each>
<g:each in="${unassignedTasks}" var="task">
	<g:set var="taskId" value="${task.id}" />
	<li><a href="#" onclick="showTaskForm(${task.id})">	
		<span class="title">${taskDetail[task.id]}</span>
		<% def interval = (now - task.createTime.getTime())
			def intervalLabel = ""
			if(interval > 7200000){
				interval = interval/7200000;
				intervalLabel = Math.round(interval) + " hr"
			} else if(interval > 60000) {
				interval = interval/60000;
				intervalLabel = Math.round(interval) + " min"
			} else {
				interval = interval/1000;
				intervalLabel = Math.round(interval) + " sec"
			}
		%>
		<span class="time"><%=intervalLabel%></span>
	</a></li>
</g:each>

%{--<li><a class="dropdown-menu-sub-footer">View all tasks</a></li>--}%
