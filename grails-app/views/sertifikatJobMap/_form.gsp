<%@ page import="com.kombos.administrasi.Section; com.kombos.administrasi.Operation; com.kombos.administrasi.Sertifikat; com.kombos.administrasi.SertifikatJobMap" %>
<g:setProvider library="jquery" />
<g:javascript>
    var selectJob;
			$(function(){
			selectJob = function () {
			var idSection = $('#section').val();
			    if(idSection != undefined){
                    jQuery.getJSON('${request.contextPath}/sertifkatJobMap/getJobs?idSerial='+idSerial, function (data) {
                            $('#job').empty();
                            if (data) {
                                jQuery.each(data, function (index, value) {
                                    $('#job').append("<option value='" + value.id + "'>" + value.operation + "</option>");
                                });
                            }
                        });
                }else{
                     $('#job').empty();
                }
            };
    });
</g:javascript>



    <div class="control-group fieldcontain ${hasErrors(bean: sertifikatJobMapInstance, field: 'sertifikat', 'error')} required">
        <label class="control-label" for="sertifikat">
            <g:message code="sertifikatJobMap.sertifikat.label" default="Nama Sertifikat" />
            <span class="required-indicator">*</span>
        </label>
        <div class="controls">
            <g:select id="sertifikat" name="sertifikat.id" from="${Sertifikat.createCriteria().list{eq("staDel", "0");order("m016NamaSertifikat","asc")}}" optionKey="id" required="" value="${sertifikatJobMapInstance?.sertifikat?.id}" class="many-to-one"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: sertifikatJobMapInstance, field: 'section', 'error')} required">
        <label class="control-label" for="section">
            <g:message code="sertifikatJobMap.section.label" default="Section" />
            <span class="required-indicator">*</span>
        </label>
        <div class="controls">
            <g:select noSelection="${['':'Select One...']}" id="section" name="section.id" onchange="selectJob();" from="${Section.createCriteria().list{eq("staDel", "0");order("m051NamaSection","asc")}}" optionKey="id" required="" value="${sertifikatJobMapInstance?.section?.id}" class="many-to-one"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: sertifikatJobMapInstance, field: 'operation', 'error')} required">
        <label class="control-label" for="operation">
            <g:message code="sertifikatJobMap.operation.label" default="Job" />
            <span class="required-indicator">*</span>
        </label>
        <div class="controls">
            <g:select id="operation" name="operation.id" from="${Operation.createCriteria().list{eq("staDel", "0");order("m053JobsId","asc")}}" optionValue="${{it.m053JobsId+" - "+it.m053NamaOperation}}" optionKey="id" required="" value="${sertifikatJobMapInstance?.operation?.id}" class="many-to-one"/>
        </div>
    </div>
