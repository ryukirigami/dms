

<%@ page import="com.kombos.administrasi.SertifikatJobMap" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'sertifikatJobMap.label', default: 'Sertifikat Job')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteSertifikatJobMap;

$(function(){ 
	deleteSertifikatJobMap=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/sertifikatJobMap/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadSertifikatJobMapTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-sertifikatJobMap" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="sertifikatJobMap"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${sertifikatJobMapInstance?.sertifikat}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="sertifikat-label" class="property-label"><g:message
					code="sertifikatJobMap.sertifikat.label" default="Nama Sertifikat" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="sertifikat-label">
						%{--<ba:editableValue
								bean="${sertifikatJobMapInstance}" field="sertifikat"
								url="${request.contextPath}/SertifikatJobMap/updatefield" type="text"
								title="Enter sertifikat" onsuccess="reloadSertifikatJobMapTable();" />--}%
                                <g:fieldValue field="sertifikat" bean="${sertifikatJobMapInstance}" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${sertifikatJobMapInstance?.section}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="section-label" class="property-label"><g:message
					code="sertifikatJobMap.section.label" default="Section" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="section-label">
						%{--<ba:editableValue
								bean="${sertifikatJobMapInstance}" field="section"
								url="${request.contextPath}/SertifikatJobMap/updatefield" type="text"
								title="Enter section" onsuccess="reloadSertifikatJobMapTable();" />--}%
                                <g:fieldValue field="section" bean="${sertifikatJobMapInstance}" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${sertifikatJobMapInstance?.operation}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="operation-label" class="property-label"><g:message
					code="sertifikatJobMap.operation.label" default="Job" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="operation-label">
						%{--<ba:editableValue
								bean="${sertifikatJobMapInstance}" field="operation"
								url="${request.contextPath}/SertifikatJobMap/updatefield" type="text"
								title="Enter operation" onsuccess="reloadSertifikatJobMapTable();" />--}%
                            <g:fieldValue field="operation" bean="${sertifikatJobMapInstance}" />
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${sertifikatJobMapInstance?.staDel}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="staDel-label" class="property-label"><g:message--}%
					%{--code="sertifikatJobMap.staDel.label" default="Sta Del" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="staDel-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${sertifikatJobMapInstance}" field="staDel"--}%
								%{--url="${request.contextPath}/SertifikatJobMap/updatefield" type="text"--}%
								%{--title="Enter staDel" onsuccess="reloadSertifikatJobMapTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${sertifikatJobMapInstance}" field="staDel"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%

            <g:if test="${sertifikatJobMapInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="sertifikatJobMap.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${sertifikatJobMapInstance}" field="m013Telp2"
                                url="${request.contextPath}/ContactPerson/updatefield" type="text"
                                title="Enter m013Telp2" onsuccess="reloadContactPersonTable();" />--}%

                        <g:formatDate date="${sertifikatJobMapInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${sertifikatJobMapInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="sertifikatJobMap.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${sertifikatJobMapInstance}" field="m013Telp2"
                                url="${request.contextPath}/ContactPerson/updatefield" type="text"
                                title="Enter m013Telp2" onsuccess="reloadContactPersonTable();" />--}%

                        <g:fieldValue field="createdBy" bean="${sertifikatJobMapInstance}" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${sertifikatJobMapInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdate-label" class="property-label"><g:message
                                code="sertifikatJobMap.lastUpdate.label" default="Last Update" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdate-label">
                        %{--<ba:editableValue
                                bean="${sertifikatJobMapInstance}" field="m013Telp2"
                                url="${request.contextPath}/ContactPerson/updatefield" type="text"
                                title="Enter m013Telp2" onsuccess="reloadContactPersonTable();" />--}%

                        <g:formatDate date="${sertifikatJobMapInstance?.lastUpdated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${sertifikatJobMapInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="sertifikatJobMap.updatedBy.label" default="Update By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${sertifikatJobMapInstance}" field="m013Telp2"
                                url="${request.contextPath}/ContactPerson/updatefield" type="text"
                                title="Enter m013Telp2" onsuccess="reloadContactPersonTable();" />--}%

                        <g:fieldValue field="updatedBy" bean="${sertifikatJobMapInstance}" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${sertifikatJobMapInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="sertifikatJobMap.lastUpdProcess.label" default="last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${sertifikatJobMapInstance}" field="m013Telp2"
                                url="${request.contextPath}/ContactPerson/updatefield" type="text"
                                title="Enter m013Telp2" onsuccess="reloadContactPersonTable();" />--}%

                        <g:fieldValue field="lastUpdProcess" bean="${sertifikatJobMapInstance}" />

                    </span></td>

                </tr>
            </g:if>
			


			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${sertifikatJobMapInstance?.id}"
					update="[success:'sertifikatJobMap-form',failure:'sertifikatJobMap-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteSertifikatJobMap('${sertifikatJobMapInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
