
<%@ page import="com.kombos.administrasi.SertifikatJobMap" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>
<g:javascript>
    $("#copy").click(function(){
        $('#spinner').fadeIn(1);
        $.ajax({
            url: '${request.contextPath}/sertifikatJobMap/copy?sertifikatSumber='+$("#sertifikatSumber").val()+'&sertifikatTujuan='+$("#sertifikatTujuan").val(),
            type: 'GET',
            dataType: 'json',
            success: function(data, textStatus, xhr) {
                if(data=="-1"){
                    //$('#serial').html(data);
                    alert("Sertifikat yang anda pilih sama");
                } else if(data=="1"){
                    alert("copy success");
                }else if(data=="2"){

                }
            },
            error: function(xhr, textStatus, errorThrown) {
                alert(textStatus);
            },
   			complete:function(XMLHttpRequest,textStatus){
   			    $('#sertifikatSumber').val('');
   			    $('#sertifikatTujuan').val('');
   			    reloadSertifikatJobMapTable();
   				$('#spinner').fadeOut();
   			}
        });
        //alert($("#section").val());
    });
    $(function(){
        $('#sertifikatSumber').typeahead({
            source: function (query, process) {
                return $.get('${request.contextPath}/sertifikatJobMap/getSertifikatSumber', { query: query }, function (data) {
                    return process(data.options);
                });
            }
        });
        $('#sertifikatTujuan').typeahead({
            source: function (query, process) {
                return $.get('${request.contextPath}/sertifikatJobMap/getSertifikat', { query: query }, function (data) {
                    return process(data.options);
                });
            }
        });
    });
</g:javascript>

<table style="padding: 50px">
    <tr>
        <td style="padding: 5px">
            <div class="control-group fieldcontain ${hasErrors(bean: sertifikatJobMapInstance, field: 'sertifikat', 'error')} required">
                <label class="control-label" for="sertifikatSumber">
                    <g:message code="sertifikatJobMap.sertifikatSumber.label" default=" Nama Sertifikat Sumber" />
                    <span class="required-indicator">*</span>
                </label>
                <div class="controls">
                    <g:textField name="sertifikatSumber" id="sertifikatSumber"  class="typeahead" value="" autocomplete="off"/>
                </div>
            </div>

        </td>
        <td style="padding: 5px">
            <div class="control-group fieldcontain ${hasErrors(bean: sertifikatJobMapInstance, field: 'sertifikat', 'error')} required">
                <label class="control-label" for="sertifikatTujuan">
                    <g:message code="sertifikatJobMap.sertifikatTujuan.label" default="Nama Sertifikat Tujuan" />
                    <span class="required-indicator">*</span>
                </label>
                <div class="controls">
                    <g:textField name="sertifikatTujuan" id="sertifikatTujuan"  class="typeahead" value="" autocomplete="off"/>
                </div>
            </div>

        </td>
        <td style="padding: 5px">
            <div class="controls">
                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn-primary copy" name="copy" id="copy" value="Copy">Copy</button>
            </div>


        </td>
    </tr>
</table>

<table id="sertifikatJobMap_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="sertifikatJobMap.sertifikat.label" default="Nama Sertifikat" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="sertifikatJobMap.section.label" default="Section" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="sertifikatJobMap.operation.label" default="Job" /></div>
			</th>


		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_sertifikat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_sertifikat" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_section" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_section" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_operation" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_operation" class="search_init" />
				</div>
			</th>
	
		</tr>
	</thead>
</table>

<g:javascript>
var sertifikatJobMapTable;
var reloadSertifikatJobMapTable;
$(function(){
	
	reloadSertifikatJobMapTable = function() {
		sertifikatJobMapTable.fnDraw();
	}

	var recordsSertifikatJobMapPerPage = [];
    var anSertifikatJobMapSelected;
    var jmlRecSertifikatJobMapPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	sertifikatJobMapTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	sertifikatJobMapTable = $('#sertifikatJobMap_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsSertifikatJobMap = $("#sertifikatJobMap_datatables tbody .row-select");
            var jmlSertifikatJobMapCek = 0;
            var nRow;
            var idRec;
            rsSertifikatJobMap.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsSertifikatJobMapPerPage[idRec]=="1"){
                    jmlSertifikatJobMapCek = jmlSertifikatJobMapCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsSertifikatJobMapPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecSertifikatJobMapPerPage = rsSertifikatJobMap.length;
            if(jmlSertifikatJobMapCek==jmlRecSertifikatJobMapPerPage && jmlRecSertifikatJobMapPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "sertifikat",
	"mDataProp": "sertifikat",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "section",
	"mDataProp": "section",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "operation",
	"mDataProp": "operation",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var sertifikat = $('#filter_sertifikat input').val();
						if(sertifikat){
							aoData.push(
									{"name": 'sCriteria_sertifikat', "value": sertifikat}
							);
						}
	
						var section = $('#filter_section input').val();
						if(section){
							aoData.push(
									{"name": 'sCriteria_section', "value": section}
							);
						}
	
						var operation = $('#filter_operation input').val();
						if(operation){
							aoData.push(
									{"name": 'sCriteria_operation', "value": operation}
							);
						}


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
	$('.select-all').click(function(e) {

        $("#sertifikatJobMap_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsSertifikatJobMapPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsSertifikatJobMapPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#sertifikatJobMap_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsSertifikatJobMapPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anSertifikatJobMapSelected = sertifikatJobMapTable.$('tr.row_selected');
            if(jmlRecSertifikatJobMapPerPage == anSertifikatJobMapSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsSertifikatJobMapPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
