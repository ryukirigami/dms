

<%@ page import="com.kombos.customerprofile.StaImport" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'staImport.label', default: 'Status Import')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteStaImport;

$(function(){ 
	deleteStaImport=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/staImport/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadStaImportTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-staImport" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="staImport"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${staImportInstance?.m100ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m100ID-label" class="property-label"><g:message
					code="staImport.m100ID.label" default="ID Status Import" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m100ID-label">
						%{--<ba:editableValue
								bean="${staImportInstance}" field="m100ID"
								url="${request.contextPath}/StaImport/updatefield" type="text"
								title="Enter m100ID" onsuccess="reloadStaImportTable();" />--}%
							
								<g:fieldValue bean="${staImportInstance}" field="m100ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${staImportInstance?.m100KodeStaImport}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m100KodeStaImport-label" class="property-label"><g:message
					code="staImport.m100KodeStaImport.label" default="Kode Status Import" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m100KodeStaImport-label">
						%{--<ba:editableValue
								bean="${staImportInstance}" field="m100KodeStaImport"
								url="${request.contextPath}/StaImport/updatefield" type="text"
								title="Enter m100KodeStaImport" onsuccess="reloadStaImportTable();" />--}%
							
								<g:fieldValue bean="${staImportInstance}" field="m100KodeStaImport"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${staImportInstance?.m100NamaStaImport}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m100NamaStaImport-label" class="property-label"><g:message
					code="staImport.m100NamaStaImport.label" default="Nama Status Import" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m100NamaStaImport-label">
						%{--<ba:editableValue
								bean="${staImportInstance}" field="m100NamaStaImport"
								url="${request.contextPath}/StaImport/updatefield" type="text"
								title="Enter m100NamaStaImport" onsuccess="reloadStaImportTable();" />--}%
							
								<g:fieldValue bean="${staImportInstance}" field="m100NamaStaImport"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${staImportInstance?.staDel}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="staDel-label" class="property-label"><g:message--}%
					%{--code="staImport.staDel.label" default="Sta Del" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="staDel-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${staImportInstance}" field="staDel"--}%
								%{--url="${request.contextPath}/StaImport/updatefield" type="text"--}%
								%{--title="Enter staDel" onsuccess="reloadStaImportTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${staImportInstance}" field="staDel"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%

            <g:if test="${staImportInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="staImport.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${staImportInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/StaImport/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadStaImportTable();" />--}%

                        <g:fieldValue bean="${staImportInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${staImportInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="staImport.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${staImportInstance}" field="dateCreated"
								url="${request.contextPath}/StaImport/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadStaImportTable();" />--}%
							
								<g:formatDate date="${staImportInstance?.dateCreated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${staImportInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="staImport.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${staImportInstance}" field="createdBy"
                                url="${request.contextPath}/StaImport/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadStaImportTable();" />--}%

                        <g:fieldValue bean="${staImportInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${staImportInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="staImport.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${staImportInstance}" field="lastUpdated"
								url="${request.contextPath}/StaImport/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadStaImportTable();" />--}%
							
								<g:formatDate date="${staImportInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${staImportInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="staImport.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${staImportInstance}" field="updatedBy"
                                url="${request.contextPath}/StaImport/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadStaImportTable();" />--}%

                        <g:fieldValue bean="${staImportInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${staImportInstance?.id}"
					update="[success:'staImport-form',failure:'staImport-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteStaImport('${staImportInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
