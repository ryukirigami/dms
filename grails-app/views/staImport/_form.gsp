<%@ page import="com.kombos.customerprofile.StaImport" %>


<g:if test="${staImportInstance?.m100ID}">
<div class="control-group fieldcontain ${hasErrors(bean: staImportInstance, field: 'm100ID', 'error')} required">
	<label class="control-label" for="m100ID">
		<g:message code="staImport.m100ID.label" default="ID Status Import" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	${staImportInstance?.m100ID}
	</div>
</div>
</g:if>

<div class="control-group fieldcontain ${hasErrors(bean: staImportInstance, field: 'm100KodeStaImport', 'error')} required">
	<label class="control-label" for="m100KodeStaImport">
		<g:message code="staImport.m100KodeStaImport.label" default="Kode Status Import" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m100KodeStaImport" maxlength="50" required="" value="${staImportInstance?.m100KodeStaImport}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: staImportInstance, field: 'm100NamaStaImport', 'error')} required">
	<label class="control-label" for="m100NamaStaImport">
		<g:message code="staImport.m100NamaStaImport.label" default="Nama Status Import" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m100NamaStaImport" maxlength="50" required="" value="${staImportInstance?.m100NamaStaImport}"/>
	</div>
</div>

%{--<div class="control-group fieldcontain ${hasErrors(bean: staImportInstance, field: 'staDel', 'error')} required">--}%
	%{--<label class="control-label" for="staDel">--}%
		%{--<g:message code="staImport.staDel.label" default="Sta Del" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="staDel" maxlength="1" required="" value="${staImportInstance?.staDel}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: staImportInstance, field: 'createdBy', 'error')} ">--}%
	%{--<label class="control-label" for="createdBy">--}%
		%{--<g:message code="staImport.createdBy.label" default="Created By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="createdBy" value="${staImportInstance?.createdBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: staImportInstance, field: 'updatedBy', 'error')} ">--}%
	%{--<label class="control-label" for="updatedBy">--}%
		%{--<g:message code="staImport.updatedBy.label" default="Updated By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="updatedBy" value="${staImportInstance?.updatedBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: staImportInstance, field: 'lastUpdProcess', 'error')} ">--}%
	%{--<label class="control-label" for="lastUpdProcess">--}%
		%{--<g:message code="staImport.lastUpdProcess.label" default="Last Upd Process" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="lastUpdProcess" value="${staImportInstance?.lastUpdProcess}"/>--}%
	%{--</div>--}%
%{--</div>--}%

