<%@ page import="com.kombos.hrd.BranchHRD" %>

<g:javascript>
    $(function() {
        $("#namaCabang").focus();
    })
</g:javascript>


<div class="control-group fieldcontain ${hasErrors(bean: branchHRDInstance, field: 'namaCabang', 'error')} ">
    <label class="control-label" for="namaCabang">
        <g:message code="branchHRD.namaCabang.label" default="Nama Cabang" />

    </label>
    <div class="controls">
        <g:textField name="namaCabang" value="${branchHRDInstance?.namaCabang}" />
    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: branchHRDInstance, field: 'alamat', 'error')} ">
    <label class="control-label" for="alamat">
        <g:message code="branchHRD.alamat.label" default="Alamat" />

    </label>
    <div class="controls">
        <g:textField name="alamat" value="${branchHRDInstance?.alamat}" />
    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: branchHRDInstance, field: 'telepon', 'error')} ">
    <label class="control-label" for="telepon">
        <g:message code="branchHRD.telepon.label" default="Telepon" />

    </label>
    <div class="controls">
        <g:textField name="telepon" value="${branchHRDInstance?.telepon}" />
    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: branchHRDInstance, field: 'contactPerson', 'error')} ">
	<label class="control-label" for="contactPerson">
		<g:message code="branchHRD.contactPerson.label" default="Contact Person" />
		
	</label>
	<div class="controls">
	<g:textField name="contactPerson" value="${branchHRDInstance?.contactPerson}" />
	</div>
</div>



