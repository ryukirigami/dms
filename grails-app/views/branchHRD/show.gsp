

<%@ page import="com.kombos.hrd.BranchHRD" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'branchHRD.label', default: 'BranchHRD')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteBranchHRD;

$(function(){ 
	deleteBranchHRD=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/branchHRD/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadBranchHRDTable();
   				expandTableLayout('branchHRD');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-branchHRD" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="branchHRD"
			class="table table-bordered table-hover">
			<tbody>

            <g:if test="${branchHRDInstance?.namaCabang}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="namaCabang-label" class="property-label"><g:message
                                code="branchHRD.namaCabang.label" default="Nama Cabang" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="namaCabang-label">
                        %{--<ba:editableValue
                                bean="${branchHRDInstance}" field="namaCabang"
                                url="${request.contextPath}/BranchHRD/updatefield" type="text"
                                title="Enter namaCabang" onsuccess="reloadBranchHRDTable();" />--}%

                        <g:fieldValue bean="${branchHRDInstance}" field="namaCabang"/>

                    </span></td>

                </tr>
            </g:if>

				
				<g:if test="${branchHRDInstance?.alamat}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="alamat-label" class="property-label"><g:message
					code="branchHRD.alamat.label" default="Alamat" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="alamat-label">
						%{--<ba:editableValue
								bean="${branchHRDInstance}" field="alamat"
								url="${request.contextPath}/BranchHRD/updatefield" type="text"
								title="Enter alamat" onsuccess="reloadBranchHRDTable();" />--}%
							
								<g:fieldValue bean="${branchHRDInstance}" field="alamat"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${branchHRDInstance?.telepon}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="telepon-label" class="property-label"><g:message
                                code="branchHRD.telepon.label" default="Telepon" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="telepon-label">
                        %{--<ba:editableValue
                                bean="${branchHRDInstance}" field="telepon"
                                url="${request.contextPath}/BranchHRD/updatefield" type="text"
                                title="Enter telepon" onsuccess="reloadBranchHRDTable();" />--}%

                        <g:fieldValue bean="${branchHRDInstance}" field="telepon"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${branchHRDInstance?.contactPerson}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="contactPerson-label" class="property-label"><g:message
					code="branchHRD.contactPerson.label" default="Contact Person" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="contactPerson-label">
						%{--<ba:editableValue
								bean="${branchHRDInstance}" field="contactPerson"
								url="${request.contextPath}/BranchHRD/updatefield" type="text"
								title="Enter contactPerson" onsuccess="reloadBranchHRDTable();" />--}%
							
								<g:fieldValue bean="${branchHRDInstance}" field="contactPerson"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${branchHRDInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="branchHRD.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${branchHRDInstance}" field="createdBy"
								url="${request.contextPath}/BranchHRD/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadBranchHRDTable();" />--}%
							
								<g:fieldValue bean="${branchHRDInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${branchHRDInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="branchHRD.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${branchHRDInstance}" field="dateCreated"
								url="${request.contextPath}/BranchHRD/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadBranchHRDTable();" />--}%
							
								<g:formatDate date="${branchHRDInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${branchHRDInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="branchHRD.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${branchHRDInstance}" field="lastUpdProcess"
								url="${request.contextPath}/BranchHRD/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadBranchHRDTable();" />--}%
							
								<g:fieldValue bean="${branchHRDInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${branchHRDInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="branchHRD.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${branchHRDInstance}" field="lastUpdated"
								url="${request.contextPath}/BranchHRD/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadBranchHRDTable();" />--}%
							
								<g:formatDate date="${branchHRDInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			

			
				<g:if test="${branchHRDInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="branchHRD.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${branchHRDInstance}" field="staDel"
								url="${request.contextPath}/BranchHRD/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadBranchHRDTable();" />--}%
							
								<g:fieldValue bean="${branchHRDInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			

			
				<g:if test="${branchHRDInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="branchHRD.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${branchHRDInstance}" field="updatedBy"
								url="${request.contextPath}/BranchHRD/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadBranchHRDTable();" />--}%
							
								<g:fieldValue bean="${branchHRDInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('branchHRD');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${branchHRDInstance?.id}"
					update="[success:'branchHRD-form',failure:'branchHRD-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteBranchHRD('${branchHRDInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
