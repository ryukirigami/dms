
<%@ page import="com.kombos.hrd.BranchHRD" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="branchHRD_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="branchHRD.namaCabang.label" default="Nama Cabang" /></div>
            </th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="branchHRD.alamat.label" default="Alamat" /></div>
			</th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="branchHRD.telepon.label" default="Telepon" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
				<div><g:message code="branchHRD.contactPerson.label" default="Contact Person" /></div>
			</th>



		
		</tr>
		<tr>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_namaCabang" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_namaCabang" class="search_init" />
                </div>
            </th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_alamat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_alamat" class="search_init" />
				</div>
			</th>



            <th style="border-top: none;padding: 5px;">
                <div id="filter_telepon" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_telepon" class="search_init" />
                </div>
            </th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_contactPerson" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_contactPerson" class="search_init" />
				</div>
			</th>




		</tr>
	</thead>
</table>

<g:javascript>
var branchHRDTable;
var reloadBranchHRDTable;
$(function(){
	
	reloadBranchHRDTable = function() {
		branchHRDTable.fnDraw();
	}

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	branchHRDTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	branchHRDTable = $('#branchHRD_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
%{--nama cabang, alamat, telepon, contact person--}%
{
	"sName": "namaCabang",
	"mDataProp": "namaCabang",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "alamat",
	"mDataProp": "alamat",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "telepon",
	"mDataProp": "telepon",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "contactPerson",
	"mDataProp": "contactPerson",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var alamat = $('#filter_alamat input').val();
						if(alamat){
							aoData.push(
									{"name": 'sCriteria_alamat', "value": alamat}
							);
						}
	
						var contactPerson = $('#filter_contactPerson input').val();
						if(contactPerson){
							aoData.push(
									{"name": 'sCriteria_contactPerson', "value": contactPerson}
							);
						}
	
						var namaCabang = $('#filter_namaCabang input').val();
						if(namaCabang){
							aoData.push(
									{"name": 'sCriteria_namaCabang', "value": namaCabang}
							);
						}
	
						var telepon = $('#filter_telepon input').val();
						if(telepon){
							aoData.push(
									{"name": 'sCriteria_telepon', "value": telepon}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
