<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'formulamanagement.label', default: 'Formula Management')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var approveRequest;

var rejectRequest;

$(function(){ 
	approveRequest=function(id){
		$('#spinner').fadeIn();
		$('#create select option').removeAttr('disabled');
		$.ajax({type:'POST', url:'${request.contextPath}/GenericApproval/approve/${params.taskId}',
   			success:function(data,textStatus){
   					$("#show-approvalrequest form").submit();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
	
	rejectRequest=function(id){
		$('#spinner').fadeIn();
		$.ajax({type:'POST', url:'${request.contextPath}/GenericApproval/reject/${params.taskId}',
   			success:function(data,textStatus){
   					toastr.success('<div>Rejected</div>');
   					loadController('${instance.originController}');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
	$('#create input:text').attr('readonly', 'readonly');
	$('#create select').attr('readonly', 'readonly');
	var deactivator = function(event){ event.preventDefault(); };
	$('#create input:checkbox').click(deactivator);
	$('#create select option').attr('disabled', 'disabled');
});

</g:javascript>
</head>
<body>
	
	<div id="show-approvalrequest" role="main">		
		<legend>
				${instance.approvalLabel}
		</legend>		
		<form id="create" class="form-horizontal" action="${request.contextPath}/${instance.originController}/${instance.originAction}" method="post" onsubmit="jQuery.ajax({type:'POST',data:jQuery(this).serialize(), url:'${request.contextPath}/${instance.originController}/${instance.originAction}',success:function(data,textStatus){toastr.success('<div>Approved</div>');loadController('${instance.originController}');},error:function(XMLHttpRequest,textStatus,errorThrown){}});return false">
			<fieldset class="form">
				<g:hiddenField name="id" value="${instance?.id}" />
				<g:render template="/${instance.originController}/form" />
			</fieldset>
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="loadController('${instance.originController}');return false;"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>	
				
				<g:if test="${params.taskId}">
				<a class="btn primary" href="javascript:void(0);"
					onclick="rejectRequest();"><g:message
						code="default.button.reject.label" default="Reject" /></a>
				<a class="btn primary" href="javascript:void(0);"
					onclick="approveRequest();"><g:message
						code="default.button.approve.label" default="Approve" /></a>
				<g:hiddenField name="taskId" value="${params.taskId}" />
				</g:if>
			</fieldset>
		</form>
	</div>
</body>
</html>
