<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'formulamanagement.label', default: 'Formula Management')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var approveRequest;

var rejectRequest;

$(function(){ 
	approveRequest=function(id){
		$('#spinner').fadeIn();
		$.ajax({type:'POST', url:'${request.contextPath}/GenericApproval/approve/${params.taskId}',
   			success:function(data,textStatus){
   					$("#deleteform").submit();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
	
	rejectRequest=function(id){
		$('#spinner').fadeIn();
		$.ajax({type:'POST', url:'${request.contextPath}/GenericApproval/reject/${params.taskId}',
   			success:function(data,textStatus){
   					toastr.success('<div>Rejected</div>');
   					loadController('${instance.originController}');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
	
	$('#approvaldeletedetail').empty();
	$.ajax({url: "${request.contextPath}/${instance.originController}/show/${instance.id}",type: "POST",dataType: "html",
		complete : function (req, err) {
			$('#approvaldeletedetail').append(req.responseText);
			$('#approvaldeletedetail legend').hide();
			$('#approvaldeletedetail form').hide();
		}
    });
});

</g:javascript>
</head>
<body>
	
	<div id="show-approvalrequest" role="main">		
		<legend>
				Approval Request for Delete ${instance.originController}
		</legend>
		<div id="approvaldeletedetail"></div>			
		<form id="deleteform" class="form-horizontal" action="${request.contextPath}/${instance.originController}/${instance.originAction}" method="post" onsubmit="jQuery.ajax({type:'POST',data:jQuery(this).serialize(), url:'${request.contextPath}/${instance.originController}/${instance.originAction}',success:function(data,textStatus){toastr.success('<div>Approved, deleted</div>');loadController('${instance.originController}');},error:function(XMLHttpRequest,textStatus,errorThrown){}});return false">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="loadController('${instance.originController}');return false;"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>	
				
				<g:if test="${params.taskId}">
				<a class="btn primary" href="javascript:void(0);"
					onclick="rejectRequest();"><g:message
						code="default.button.reject.label" default="Reject" /></a>
				<a class="btn primary" href="javascript:void(0);"
					onclick="approveRequest();"><g:message
						code="default.button.approve.label" default="Approve" /></a>
				<g:hiddenField name="taskId" value="${params.taskId}" />
				<g:hiddenField name="id" value="${instance?.id}" />
				</g:if>
			</fieldset>
		</form>
	</div>
</body>
</html>
