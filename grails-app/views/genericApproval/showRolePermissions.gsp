<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<title>Role Permissions</title>
<g:javascript disposition="head">
var approveRequest;

var rejectRequest;

$(function(){ 
	approveRequest=function(id){
		$('#spinner').fadeIn();
		$.ajax({type:'POST', url:'${request.contextPath}/GenericApproval/approve/${params.taskId}',
   			success:function(data,textStatus){
   					$("#show-approvalrequest form").submit();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
	
	rejectRequest=function(id){
		$('#spinner').fadeIn();
		$.ajax({type:'POST', url:'${request.contextPath}/GenericApproval/reject/${params.taskId}',
   			success:function(data,textStatus){
   					toastr.success('<div>Rejected</div>');
   					loadController('role');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	
	<div id="show-approvalrequest" role="main">		
		<legend>
				${instance.approvalLabel}
		</legend>		
		<form id="create" class="form-horizontal" action="${request.contextPath}/${instance.originController}/${instance.originAction}" method="post" onsubmit="jQuery.ajax({type:'POST',data:jQuery(this).serialize(), url:'${request.contextPath}/${instance.originController}/${instance.originAction}',success:function(data,textStatus){toastr.success('<div>Approved</div>');loadController('role');},error:function(XMLHttpRequest,textStatus,errorThrown){}});return false">
			<fieldset class="form">
				<g:hiddenField name="permissions_string" value="${instance?.permissions_string}" />
				<g:hiddenField name="menu_code" value="${instance?.menu_code}" />
				
				<div class="control-group fieldcontain">
				    <label class="control-label" for="role_id">
				        Role Id
				    </label>
				    <div class="controls">
				       <g:textField name="role_id" value="${instance?.role_id}" readonly="readonly" />
				    </div>
				</div>
				<div class="control-group fieldcontain">
				    <label class="control-label" for="link_controller">
				        Module Name
				    </label>
				    <div class="controls">
				       <g:textField name="link_controller" value="${instance?.link_controller}" readonly="readonly" />
				    </div>
				</div>
				<div class="control-group fieldcontain">
				    <label class="control-label" for="allow_module">
				        Allow Module
				    </label>
				    <div class="controls">
				       <g:textField name="allow_module" value="${instance?.allow_module}" readonly="readonly" />
				    </div>
				</div>
				<div class="control-group fieldcontain">
				    <label class="control-label" for="allow_add">
				        Allow Add
				    </label>
				    <div class="controls">
				       <g:textField name="allow_add" value="${instance?.allow_add}" readonly="readonly" />
				    </div>
				</div>
				<div class="control-group fieldcontain">
				    <label class="control-label" for="allow_edit">
				        Allow Edit
				    </label>
				    <div class="controls">
				       <g:textField name="allow_edit" value="${instance?.allow_edit}" readonly="readonly" />
				    </div>
				</div>
				<div class="control-group fieldcontain">
				    <label class="control-label" for="allow_delete">
				        Allow Delete
				    </label>
				    <div class="controls">
				       <g:textField name="allow_delete" value="${instance?.allow_delete}" readonly="readonly" />
				    </div>
				</div>
			</fieldset>
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="loadController('role');return false;"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>	
				
				<g:if test="${params.taskId}">
				<a class="btn primary" href="javascript:void(0);"
					onclick="rejectRequest();"><g:message
						code="default.button.reject.label" default="Reject" /></a>
				<a class="btn primary" href="javascript:void(0);"
					onclick="approveRequest();"><g:message
						code="default.button.approve.label" default="Approve" /></a>
				<g:hiddenField name="taskId" value="${params.taskId}" />
				</g:if>
			</fieldset>
		</form>
	</div>
</body>
</html>
