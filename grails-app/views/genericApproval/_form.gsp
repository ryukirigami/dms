<%@ page import="com.kombos.baseapp.sec.shiro.User" %>

%{--<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'username', 'error')} required">--}%
	%{--<label class="control-label" for="username">--}%
		%{--<g:message code="app.user.username.label" default="Username" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="username" required="" value="${userInstance?.username}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<%--}%
    %{--if(request.requestURI.contains('edit.dispatch') || request.requestURI.contains('update.dispatch')){--}%
%{--%>--}%
%{--<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'passwordHash', 'error')} ">--}%
	%{--<label class="control-label" for="passwordHash">--}%
		%{--<g:message code="app.user.newpassword.label" default="New Password" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
        %{--<input type="password" id="passwordHash" value="" maxlength="500" name="passwordHash">--}%
        %{--<br/>--}%
        %{--<i><g:message code="app.user.remain_old_password.label" default="Leave this blank, the old password will still remain" /></i>--}%
    %{--</div>--}%
%{--</div>--}%
%{--<%--}%
    %{--}else{--}%
%{--%>--}%
%{--<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'passwordHash', 'error')} ">--}%
    %{--<label class="control-label" for="passwordHash">--}%
        %{--<g:message code="app.user.passwordHash.label" default="Password Hash" />--}%

    %{--</label>--}%
    %{--<div class="controls">--}%
        %{--<input type="password" id="passwordHash" value="${userInstance?.passwordHash}" maxlength="500" name="passwordHash">--}%
    %{--</div>--}%
%{--</div>--}%
%{--<%--}%
    %{--}--}%
%{--%>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'roles', 'error')} ">--}%
    %{--<label class="control-label" for="roles">--}%
        %{--<g:message code="app.user.roles.label" default="Roles" />--}%

    %{--</label>--}%
    %{--<div class="controls">--}%
        %{--<g:select name="roles" from="${com.kombos.baseapp.sec.shiro.Role.list()}" multiple="multiple" optionKey="id" size="5" value="${userInstance?.roles*.id}" class="many-to-many"/>--}%
    %{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'status', 'error')} ">--}%
	%{--<label class="control-label" for="status">--}%
		%{--<g:message code="app.user.status.label" default="Status" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:select name="status" from="${userInstance.constraints.status.inList}" value="${userInstance?.status}" valueMessagePrefix="user.status" noSelection="['': '']"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'passwordExpiredOn', 'error')} ">--}%
    %{--<label class="control-label" for="passwordExpiredOn">--}%
        %{--<g:message code="app.user.passwordExpiredOn.label" default="Password Expired On" />--}%

    %{--</label>--}%
    %{--<div class="controls">--}%
        %{--<g:datePicker name="passwordExpiredOn" precision="day"  value="${userInstance?.passwordExpiredOn}" default="none" noSelection="['': '']" />--}%
    %{--</div>--}%
%{--</div>--}%

%{--start here--}%
<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'username', 'error')} required">
    <label class="control-label" for="username">
        <g:message code="app.user.username.label" default="Username" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="username" required="" value="${userInstance?.username}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'staffNo', 'error')} required">
    <label class="control-label" for="staffNo">
        <g:message code="app.user.staff_no.label" default="Staff No" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="staffNo" required="" value="${userInstance?.staffNo}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'firstName', 'error')} ">
    <label class="control-label" for="firstName">
        <g:message code="app.user.first_name.label" default="First Name" />
    </label>
    <div class="controls">
        <g:textField name="firstName" value="${userInstance?.firstName}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'lastName', 'error')} ">
    <label class="control-label" for="lastName">
        <g:message code="app.user.last_name.label" default="Last Name" />
    </label>
    <div class="controls">
        <g:textField name="lastName" value="${userInstance?.lastName}"/>
    </div>
</div>
%{--
<%
    if(request.requestURI.contains('edit.dispatch') || request.requestURI.contains('update.dispatch')){
%>
<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'passwordHash', 'error')} ">
    <label class="control-label" for="passwordHash">
        <g:message code="app.user.newpassword.label" default="New Password" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <input type="password" id="passwordHash" value="" maxlength="500" name="passwordHash"> 
        <br/>
        <i><g:message code="app.user.remain_old_password.label" default="Leave this blank, the old password will still remain" /></i>
    </div> 
</div>
<%
    }else{
%>
<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'passwordHash', 'error')} ">
    <label class="control-label" for="passwordHash">
        <g:message code="app.user.passwordHash.label" default="Password Hash" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <input type="text" id="passwordHash" value="${userInstance?.passwordHash}" maxlength="500" name="passwordHash">&nbsp;
        <button class="btn" onclick="${remoteFunction(action: 'generatePassword',
                       onSuccess:'jQuery(\'#passwordHash\').val(data);return false;')}">Generate</button>
    </div>
</div>
<%
    }
%>
--}%
<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'effectiveStartDate', 'error')} ">
    <label class="control-label" for="effectiveStartDate">
        <g:message code="app.user.effective_start_date.label" default="Effective Start Date" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:datePicker name="effectiveStartDate" precision="day"  value="${userInstance?.effectiveStartDate}" default="none" noSelection="['': '']" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'effectiveEndDate', 'error')} ">
    <label class="control-label" for="effectiveEndDate">
        <g:message code="app.user.effective_end_date.label" default="Effective End Date" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:datePicker name="effectiveEndDate" precision="day"  value="${userInstance?.effectiveEndDate}" default="none" noSelection="['': '']" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'roles', 'error')} ">
    <label class="control-label" for="roles">
        <g:message code="app.user.roles.label" default="Roles" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select name="roles" from="${com.kombos.baseapp.sec.shiro.Role.list()}" multiple="multiple" optionKey="id" size="5" value="${instance?.roless}" class="many-to-many"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'groupLocalBranchId', 'error')} ">
    <label class="control-label" for="groupLocalBranchId">
        <g:message code="app.user.group_local_branch.label" default="Group Local Branch" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select name="groupLocalBranchId" from="${com.kombos.baseapp.GroupLocalBranch?.list()}" optionKey="groupname" size="1" value="${userInstance?.groupLocalBranchId}" default="none" noSelection="['': '']"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'groupFormId', 'error')} ">
    <label class="control-label" for="groupFormId">
        <g:message code="app.user.group_form.label" default="Group Form" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select name="groupFormId" from="${com.kombos.baseapp.GroupForm?.list()}" optionKey="groupname" size="1" value="${userInstance?.groupFormId}" default="none" noSelection="['': '']"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'reportTo', 'error')} ">
    <label class="control-label" for="reportTo">
        <g:message code="app.user.report_to.label" default="Report To" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select name="reportTo" from="${com.kombos.baseapp.sec.shiro.User.findAllByIsActive(true)}" optionKey="username" size="5" value="${instance?.reportToes}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'canPasswordExpire', 'error')} ">
    <label class="control-label" for="canPasswordExpire">
        <g:message code="app.user.password_can_expire.label" default="Can Password Expire" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:checkBox name="canPasswordExpire" value="${userInstance?.canPasswordExpire}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'accountLocked', 'error')} ">
    <label class="control-label" for="accountLocked">
        <g:message code="app.user.account_locked.label" default="Locked" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:checkBox name="accountLocked" value="${userInstance?.accountLocked}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'isActive', 'error')} ">
    <label class="control-label" for="isActive">
        <g:message code="app.user.is_active.label" default="Active" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:checkBox name="isActive" value="${userInstance?.isActive}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'country', 'error')} ">
    <label class="control-label" for="country">
        <g:message code="app.user.country.label" default="Country" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="country" name="country" from="${['en','in']}" valueMessagePrefix="app.login.language"></g:select>
    </div>
</div>

