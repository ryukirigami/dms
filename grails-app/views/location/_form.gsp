<%@ page import="com.kombos.parts.Location ; com.kombos.parts.ParameterICC" %>
<g:javascript>
    $(document).ready(function() {
        if($('#m120Ket').val().length>0){
            $('#hitung').text(50 - $('#m120Ket').val().length);
        }
        $('#m120Ket').keyup(function() {
            var len = this.value.length;
            if (len >= 50) {
                this.value = this.value.substring(0, 255);
                len = 50;
            }
            $('#hitung').text(50 - len);
        });
    });
</g:javascript>


<div class="control-group fieldcontain ${hasErrors(bean: locationInstance, field: 'parameterICC', 'error')} required">
	<label class="control-label" for="parameterICC">
		<g:message code="location.parameterICC.label" default="Kode ICC" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="parameterICC" name="parameterICC.id" from="${ParameterICC.createCriteria().list{eq("staDel", "0");order("m155NamaICC","asc")}}" optionKey="id" required="" value="${locationInstance?.parameterICC?.id}" class="many-to-one"/>
	</div>
</div>

<g:javascript>
    document.getElementById("parameterICC").focus();
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: locationInstance, field: 'm120NamaLocation', 'error')} required">
	<label class="control-label" for="m120NamaLocation">
		<g:message code="location.m120NamaLocation.label" default="Nama Lokasi" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m120NamaLocation" maxlength="50" required="" value="${locationInstance?.m120NamaLocation}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: locationInstance, field: 'm120Ket', 'error')} ">
	<label class="control-label" for="m120Ket">
		<g:message code="location.m120Ket.label" default="Keterangan" />
		%{--<span class="required-indicator">*</span>--}%
	</label>
	<div class="controls">
	<g:textArea name="m120Ket" id="m120Ket" value="${locationInstance?.m120Ket}"/>
        <br/>
        <span id="hitung">50</span> Karakter Tersisa.
	</div>
</div>

%{--<div class="control-group fieldcontain ${hasErrors(bean: locationInstance, field: 'm120ID', 'error')} ">--}%
	%{--<label class="control-label" for="m120ID">--}%
		%{--<g:message code="location.m120ID.label" default="M120 ID" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:field name="m120ID" type="number" value="${locationInstance.m120ID}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: locationInstance, field: 'companyDealer', 'error')} ">--}%
	%{--<label class="control-label" for="companyDealer">--}%
		%{--<g:message code="location.companyDealer.label" default="Company Dealer" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:select id="companyDealer" name="companyDealer.id" from="${com.kombos.baseapp.maintable.CompanyDealer.list()}" optionKey="id" value="${locationInstance?.companyDealer?.id}" class="many-to-one" noSelection="['null': '']"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: locationInstance, field: 'staDel', 'error')} required">--}%
	%{--<label class="control-label" for="staDel">--}%
		%{--<g:message code="location.staDel.label" default="Sta Del" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="staDel" maxlength="1" required="" value="${locationInstance?.staDel}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: locationInstance, field: 'createdBy', 'error')} ">--}%
	%{--<label class="control-label" for="createdBy">--}%
		%{--<g:message code="location.createdBy.label" default="Created By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="createdBy" value="${locationInstance?.createdBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: locationInstance, field: 'updatedBy', 'error')} ">--}%
	%{--<label class="control-label" for="updatedBy">--}%
		%{--<g:message code="location.updatedBy.label" default="Updated By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="updatedBy" value="${locationInstance?.updatedBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: locationInstance, field: 'lastUpdProcess', 'error')} ">--}%
	%{--<label class="control-label" for="lastUpdProcess">--}%
		%{--<g:message code="location.lastUpdProcess.label" default="Last Upd Process" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="lastUpdProcess" value="${locationInstance?.lastUpdProcess}"/>--}%
	%{--</div>--}%
%{--</div>--}%

