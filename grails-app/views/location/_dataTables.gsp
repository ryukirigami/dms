
<%@ page import="com.kombos.parts.Location" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="location_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="location.parameterICC.label" default="Kode ICC" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="location.m120NamaLocation.label" default="Nama Lokasi" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="location.m120Ket.label" default="Keterangan" /></div>
			</th>


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="location.m120ID.label" default="M120 ID" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="location.companyDealer.label" default="Company Dealer" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="location.staDel.label" default="Sta Del" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="location.createdBy.label" default="Created By" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="location.updatedBy.label" default="Updated By" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="location.lastUpdProcess.label" default="Last Upd Process" /></div>--}%
			%{--</th>--}%

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_parameterICC" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_parameterICC" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m120NamaLocation" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m120NamaLocation" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m120Ket" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m120Ket" class="search_init" />
				</div>
			</th>
	
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_m120ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_m120ID" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_companyDealer" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_staDel" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_createdBy" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_updatedBy" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_lastUpdProcess" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%


		</tr>
	</thead>
</table>

<g:javascript>
var locationTable;
var reloadLocationTable;
$(function(){
	
	reloadLocationTable = function() {
		locationTable.fnDraw();
	}

	var recordsLocationPerPage = [];
    var anLocationSelected;
    var jmlRecLocationPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	locationTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	locationTable = $('#location_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsLocation = $("#location_datatables tbody .row-select");
            var jmlLocationCek = 0;
            var nRow;
            var idRec;
            rsLocation.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsLocationPerPage[idRec]=="1"){
                    jmlLocationCek = jmlLocationCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsLocationPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecLocationPerPage = rsLocation.length;
            if(jmlLocationCek==jmlRecLocationPerPage && jmlRecLocationPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "parameterICC",
	"mDataProp": "parameterICC",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m120NamaLocation",
	"mDataProp": "m120NamaLocation",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m120Ket",
	"mDataProp": "m120Ket",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
//
//,
//
//{
//	"sName": "m120ID",
//	"mDataProp": "m120ID",
//	"aTargets": [3],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "companyDealer",
//	"mDataProp": "companyDealer",
//	"aTargets": [4],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "staDel",
//	"mDataProp": "staDel",
//	"aTargets": [5],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "createdBy",
//	"mDataProp": "createdBy",
//	"aTargets": [6],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "updatedBy",
//	"mDataProp": "updatedBy",
//	"aTargets": [7],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "lastUpdProcess",
//	"mDataProp": "lastUpdProcess",
//	"aTargets": [8],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var parameterICC = $('#filter_parameterICC input').val();
						if(parameterICC){
							aoData.push(
									{"name": 'sCriteria_parameterICC', "value": parameterICC}
							);
						}
	
						var m120NamaLocation = $('#filter_m120NamaLocation input').val();
						if(m120NamaLocation){
							aoData.push(
									{"name": 'sCriteria_m120NamaLocation', "value": m120NamaLocation}
							);
						}
	
						var m120Ket = $('#filter_m120Ket input').val();
						if(m120Ket){
							aoData.push(
									{"name": 'sCriteria_m120Ket', "value": m120Ket}
							);
						}
	
//						var m120ID = $('#filter_m120ID input').val();
//						if(m120ID){
//							aoData.push(
//									{"name": 'sCriteria_m120ID', "value": m120ID}
//							);
//						}
//
//						var companyDealer = $('#filter_companyDealer input').val();
//						if(companyDealer){
//							aoData.push(
//									{"name": 'sCriteria_companyDealer', "value": companyDealer}
//							);
//						}
//
//						var staDel = $('#filter_staDel input').val();
//						if(staDel){
//							aoData.push(
//									{"name": 'sCriteria_staDel', "value": staDel}
//							);
//						}
//
//						var createdBy = $('#filter_createdBy input').val();
//						if(createdBy){
//							aoData.push(
//									{"name": 'sCriteria_createdBy', "value": createdBy}
//							);
//						}
//
//						var updatedBy = $('#filter_updatedBy input').val();
//						if(updatedBy){
//							aoData.push(
//									{"name": 'sCriteria_updatedBy', "value": updatedBy}
//							);
//						}
//
//						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
//						if(lastUpdProcess){
//							aoData.push(
//									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
//							);
//						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

	$('.select-all').click(function(e) {

        $("#location_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsLocationPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsLocationPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#location_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsLocationPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anLocationSelected = locationTable.$('tr.row_selected');
            if(jmlRecLocationPerPage == anLocationSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsLocationPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
