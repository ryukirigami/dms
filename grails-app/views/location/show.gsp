

<%@ page import="com.kombos.parts.Location" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'location.label', default: 'Lokasi ICC')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteLocation;

$(function(){ 
	deleteLocation=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/location/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadLocationTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-location" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="location"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${locationInstance?.parameterICC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="parameterICC-label" class="property-label"><g:message
					code="location.parameterICC.label" default="Kode ICC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="parameterICC-label">
						%{--<ba:editableValue
								bean="${locationInstance}" field="parameterICC"
								url="${request.contextPath}/Location/updatefield" type="text"
								title="Enter parameterICC" onsuccess="reloadLocationTable();" />--}%
							
								${locationInstance?.parameterICC?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${locationInstance?.m120NamaLocation}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m120NamaLocation-label" class="property-label"><g:message
					code="location.m120NamaLocation.label" default="Nama Lokasi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m120NamaLocation-label">
						%{--<ba:editableValue
								bean="${locationInstance}" field="m120NamaLocation"
								url="${request.contextPath}/Location/updatefield" type="text"
								title="Enter m120NamaLocation" onsuccess="reloadLocationTable();" />--}%
							
								<g:fieldValue bean="${locationInstance}" field="m120NamaLocation"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${locationInstance?.m120Ket}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m120Ket-label" class="property-label"><g:message
					code="location.m120Ket.label" default="Keterangan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m120Ket-label">
						%{--<ba:editableValue
								bean="${locationInstance}" field="m120Ket"
								url="${request.contextPath}/Location/updatefield" type="text"
								title="Enter m120Ket" onsuccess="reloadLocationTable();" />--}%
							
								<g:fieldValue bean="${locationInstance}" field="m120Ket"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${locationInstance?.m120ID}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="m120ID-label" class="property-label"><g:message--}%
					%{--code="location.m120ID.label" default="M120 ID" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="m120ID-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${locationInstance}" field="m120ID"--}%
								%{--url="${request.contextPath}/Location/updatefield" type="text"--}%
								%{--title="Enter m120ID" onsuccess="reloadLocationTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${locationInstance}" field="m120ID"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${locationInstance?.companyDealer}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="companyDealer-label" class="property-label"><g:message--}%
					%{--code="location.companyDealer.label" default="Company Dealer" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="companyDealer-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${locationInstance}" field="companyDealer"--}%
								%{--url="${request.contextPath}/Location/updatefield" type="text"--}%
								%{--title="Enter companyDealer" onsuccess="reloadLocationTable();" />--}%
							%{----}%
								%{--<g:link controller="companyDealer" action="show" id="${locationInstance?.companyDealer?.id}">${locationInstance?.companyDealer?.encodeAsHTML()}</g:link>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			
				%{--<g:if test="${locationInstance?.staDel}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="staDel-label" class="property-label"><g:message--}%
					%{--code="location.staDel.label" default="Sta Del" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="staDel-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${locationInstance}" field="staDel"--}%
								%{--url="${request.contextPath}/Location/updatefield" type="text"--}%
								%{--title="Enter staDel" onsuccess="reloadLocationTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${locationInstance}" field="staDel"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%

            <g:if test="${locationInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="location.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${locationInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/Location/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadLocationTable();" />--}%

                        <g:fieldValue bean="${locationInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${locationInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="location.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${locationInstance}" field="dateCreated"
								url="${request.contextPath}/Location/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadLocationTable();" />--}%
							
								<g:formatDate date="${locationInstance?.dateCreated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>
			


            <g:if test="${locationInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="location.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${locationInstance}" field="createdBy"
                                url="${request.contextPath}/Location/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadLocationTable();" />--}%

                        <g:fieldValue bean="${locationInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if><g:if test="${locationInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="location.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${locationInstance}" field="lastUpdated"
                                url="${request.contextPath}/Location/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadLocationTable();" />--}%

                        <g:formatDate date="${locationInstance?.lastUpdated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${locationInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="location.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${locationInstance}" field="updatedBy"
                                url="${request.contextPath}/Location/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadLocationTable();" />--}%

                        <g:fieldValue bean="${locationInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${locationInstance?.id}"
					update="[success:'location-form',failure:'location-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteLocation('${locationInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
