<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="booking_fee_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	style="table-layout: fixed;"
	width="100%">
	<thead>
    	<tr>
        	<th style="border-bottom: none;">
            	<div style="width: 75px;">PROCESS DATE</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 75px;">PROCESS TIME</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 75px;">DOC ID</div>
			</th>			
			<th style="border-bottom: none;">
            	<div style="width: 75px;">ITEM ID</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 75px;">POSTING DATE</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 75px;">COMPANY CODE</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 75px;">CURRENCY</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 75px;">REFERENCE</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 75px;">ACCOUNT CODE</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 75px;">AMOUNT</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 75px;">ASSIGNMENT</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 75px;">TAX CODE</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 75px;">BASELINE DATE</div>
			</th>
            <th style="border-bottom: none;">
                <div style="width: 75px;">CUST NAME</div>
            </th>
            <th style="border-bottom: none;">
                <div style="width: 75px;">CUST ADDRESS</div>
            </th>
            <th style="border-bottom: none;">
                <div style="width: 75px;">CITY NPWP</div>
            </th>
            <th style="border-bottom: none;">
                <div style="width: 75px;">KODE POS</div>
            </th>
            <th style="border-bottom: none;">
                <div style="width: 75px;">SP G/L ASSIGNMENT</div>
            </th>
		</tr>
	</thead>
</table>
<g:javascript>
var bookingFeeTable;
var reloadBookingFeeTable;

function searchData() {
    reloadBookingFeeTable();
}

$(function() {
	reloadBookingFeeTable = function() {
		bookingFeeTable.fnDraw();
	}
	
	bookingFeeTable = $('#booking_fee_datatables').dataTable({
		"sScrollX": "1024px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		},
	    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
	    },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "bookingFeeDataTablesList")}",
		"aoColumns": [
			{
				"sName": "processDate",
				"mDataProp": "processDate",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "processTime",
				"mDataProp": "processTime",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "docId",
				"mDataProp": "docId",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "itemId",
				"mDataProp": "itemId",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "postingDate",
				"mDataProp": "postingDate",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "companyCode",
				"mDataProp": "companyCode",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "currency",
				"mDataProp": "currency",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "reference",
				"mDataProp": "reference",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "accountCode",
				"mDataProp": "accountCode",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "amount",
				"mDataProp": "amount",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "assignment",
				"mDataProp": "assignment",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "taxCode",
				"mDataProp": "taxCode",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "baselineDate",
				"mDataProp": "baselineDate",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
            {
                "sName": "custName",
                "mDataProp": "custName",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"75px",
                "bVisible": true
            },
            {
                "sName": "custAddress",
                "mDataProp": "custAddress",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"75px",
                "bVisible": true
            },
            {
                "sName": "cityNpwp",
                "mDataProp": "cityNpwp",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"75px",
                "bVisible": true
            },
            {
                "sName": "kodePos",
                "mDataProp": "kodePos",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"75px",
                "bVisible": true
            },
            {
                "sName": "spGl",
                "mDataProp": "spGl",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"75px",
                "bVisible": true
            }
		],
		"fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
			var tanggalFakturStart = $("#tanggalFaktur_start").val();
			var tanggalFakturStartDay = $('#tanggalFaktur_start_day').val();
			var tanggalFakturStartMonth = $('#tanggalFaktur_start_month').val();
			var tanggalFakturStartYear = $('#tanggalFaktur_start_year').val();
            if(tanggalFakturStart && $('input[name=chkTanggalFaktur]').is(':checked')) {
				aoData.push(
						{"name": 'tanggalFakturStart', "value": "date.struct"},
						{"name": 'tanggalFakturStart_dp', "value": tanggalFakturStart},
						{"name": 'tanggalFakturStart_day', "value": tanggalFakturStartDay},
						{"name": 'tanggalFakturStart_month', "value": tanggalFakturStartMonth},
						{"name": 'tanggalFakturStart_year', "value": tanggalFakturStartYear},
						{"name": 'chkTanggalFaktur', "value": true}
				);
			}
			
            var tanggalFakturEnd = $("#tanggalFaktur_end").val();
            var tanggalFakturEndDay = $('#tanggalFaktur_end_day').val();
			var tanggalFakturEndMonth = $('#tanggalFaktur_end_month').val();
			var tanggalFakturEndYear = $('#tanggalFaktur_end_year').val();
			if(tanggalFakturEnd && $('input[name=chkTanggalFaktur]').is(':checked')) {
				aoData.push(
						{"name": 'tanggalFakturEnd', "value": "date.struct"},
						{"name": 'tanggalFakturEnd_dp', "value": tanggalFakturEnd},
						{"name": 'tanggalFakturEnd_day', "value": tanggalFakturEndDay},
						{"name": 'tanggalFakturEnd_month', "value": tanggalFakturEndMonth},
						{"name": 'tanggalFakturEnd_year', "value": tanggalFakturEndYear}
				);
			}
			
            var kategoriFaktur = $('input[name=kategoriFaktur]').val();
            if(kategoriFaktur && $('input[name=chkKategoriFaktur]').is(':checked')) {
				aoData.push(
						{"name": 'kategoriFaktur', "value": kategoriFaktur},
						{"name": 'chkKategoriFaktur', "value": true}
				);
			}
            
			var kataKunci = $('input[name=kataKunci]').val();
            if(kataKunci && $('input[name=chkKataKunci]').is(':checked')) {
				aoData.push(
						{"name": 'kataKunci', "value": kataKunci},
						{"name": 'chkKataKunci', "value": true}
				);
			}			
			
			$.ajax({ "dataType": 'json',
				"type": "POST",
				"url": sSource,
				"data": aoData ,
				"success": function (json) {
					fnCallback(json);
				},
				"complete": function () {}
			});
			
		}
	});
		
});
</g:javascript>