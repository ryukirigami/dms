<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="parts_datatables_${idTable}" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	style="table-layout: fixed;"
	width="100%">
	<thead>
    	<tr>
        	<th style="border-bottom: none;">
            	<div style="width: 75px;">DEALER CODE</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 75px;">PAY DATE</div>
			</th>			
			<th style="border-bottom: none;">
            	<div style="width: 75px;">INV. DATE</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 75px;">INV. NO</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 75px;">CUST. TYPE</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 75px;">REP. TYPE</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 75px;">CUST. CODE</div>
			</th>
            <th style="border-bottom: none;">
                <div style="width: 75px;">BANK CODE</div>
            </th>
            <th style="border-bottom: none;">
                <div style="width: 75px;">PAID</div>
            </th>
            <th style="border-bottom: none;">
                <div style="width: 75px;">MATERAI</div>
            </th>
            <th style="border-bottom: none;">
                <div style="width: 75px;">POLICE NO</div>
            </th>
            <th style="border-bottom: none;">
                <div style="width: 75px;">CUSTOMER</div>
            </th>
            <th style="border-bottom: none;">
                <div style="width: 75px;">ADDRESS</div>
            </th>
            <th style="border-bottom: none;">
                <div style="width: 75px;">CITY</div>
            </th>
            <th style="border-bottom: none;">
                <div style="width: 75px;">DP</div>
            </th>
            <th style="border-bottom: none;">
                <div style="width: 75px;">PPH 23/PPN</div>
            </th>
            <th style="border-bottom: none;">
                <div style="width: 75px;">DIFFERENCE</div>
            </th>
            <th style="border-bottom: none;">
                <div style="width: 75px;">SERVICE ORDER</div>
            </th>
		</tr>
	</thead>
</table>
<g:javascript>
var partsTable;
var reloadPartsTable;

function searchData() {
    reloadPartsTable();
}

$(function() {
	reloadPartsTable = function() {
		partsTable.fnDraw();
	}
	
	partsTable = $('#parts_datatables_${idTable}').dataTable({
		"bScrollCollapse": false,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		},
	    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
	    },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "spkInvoiceSettlementDataTablesList")}",
		"aoColumns": [
			{
				"sName": "dealerCode",
				"mDataProp": "dealerCode",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "payDate",
				"mDataProp": "payDate",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "invDate",
				"mDataProp": "invDate",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "invNo",
				"mDataProp": "invNo",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "custType",
				"mDataProp": "custType",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "repType",
				"mDataProp": "repType",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "custCode",
				"mDataProp": "custCode",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "bankCode",
				"mDataProp": "bankCode",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
            {
                "sName": "paid",
                "mDataProp": "paid",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"75px",
                "bVisible": true
            },
            {
                "sName": "materai",
                "mDataProp": "materai",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"75px",
                "bVisible": true
            },
            {
                "sName": "policeNo",
                "mDataProp": "policeNo",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"75px",
                "bVisible": true
            },
            {
                "sName": "customer",
                "mDataProp": "customer",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"75px",
                "bVisible": true
            },
            {
                "sName": "address",
                "mDataProp": "address",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"75px",
                "bVisible": true
            },
            {
                "sName": "city",
                "mDataProp": "city",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"75px",
                "bVisible": true
            },
            {
                "sName": "dp",
                "mDataProp": "dp",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"75px",
                "bVisible": true
            },
            {
                "sName": "pph",
                "mDataProp": "pph",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"75px",
                "bVisible": true
            },
            {
                "sName": "difference",
                "mDataProp": "difference",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"75px",
                "bVisible": true
            },
            {
                "sName": "serviceOrder",
                "mDataProp": "serviceOrder",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"75px",
                "bVisible": true
            }
		],
		"fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
			var tanggalStart = $("#tanggalParts_start").val();
			var tanggalStartDay = $('#tanggalParts_start_day').val();
			var tanggalStartMonth = $('#tanggalParts_start_month').val();
			var tanggalStartYear = $('#tanggalParts_start_year').val();
            if(tanggalStart && $('input[name=chkTanggalParts]').is(':checked')) {
				aoData.push(
						{"name": 'tanggalStart', "value": "date.struct"},
						{"name": 'tanggalStart_dp', "value": tanggalStart},
						{"name": 'tanggalStart_day', "value": tanggalStartDay},
						{"name": 'tanggalStart_month', "value": tanggalStartMonth},
						{"name": 'tanggalStart_year', "value": tanggalStartYear},
						{"name": 'chkTanggalParts', "value": true}
				);
			}
			
            var tanggalEnd = $("#tanggalParts_end").val();
            var tanggalEndDay = $('#tanggalParts_end_day').val();
			var tanggalEndMonth = $('#tanggalParts_end_month').val();
			var tanggalEndYear = $('#tanggalParts_end_year').val();
			if(tanggalEnd && $('input[name=chkTanggalParts]').is(':checked')) {
				aoData.push(
						{"name": 'tanggalEnd', "value": "date.struct"},
						{"name": 'tanggalEnd_dp', "value": tanggalEnd},
						{"name": 'tanggalEnd_day', "value": tanggalEndDay},
						{"name": 'tanggalEnd_month', "value": tanggalEndMonth},
						{"name": 'tanggalEnd_year', "value": tanggalEndYear}
				);
			}			
			
            var nomorParts = $('input[name=nomorParts]').val();
            if(nomorParts && $('input[name=chkNomorParts]').is(':checked')) {
				aoData.push(
						{"name": 'nomorParts', "value": nomorParts},
						{"name": 'chkNomorParts', "value": true}
				);
			}
            
			var nomorWO = $('input[name=nomorWO]').val();
            if(nomorWO && $('input[name=chkNomorWO]').is(':checked')) {
				aoData.push(
						{"name": 'nomorWO', "value": nomorWO},
						{"name": 'chkNomorWO', "value": true}
				);
			}
			
			var nomorPolisi = $('input[name=nomorPolisi]').val();
            if(nomorPolisi && $('input[name=chkNomorPolisi]').is(':checked')) {
				aoData.push(
						{"name": 'nomorPolisi', "value": nomorPolisi},
						{"name": 'chkNomorPolisi', "value": true}
				);
			}					
			
			$.ajax({ "dataType": 'json',
				"type": "POST",
				"url": sSource,
				"data": aoData ,
				"success": function (json) {
					fnCallback(json);
				},
				"complete": function () {}
			});			
		}
	});
	
    var anOpen = [];
	$("#parts_datatables_${idTable} tbody").delegate("tr i", "click", function (e) {
		e.preventDefault();		
   		var nTr = this.parentNode.parentNode;		
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this.parentNode).attr( 'class', "icon-minus" );
    		var oData = partsTable.fnGetData(nTr);			
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${request.contextPath}/slipList/partsSlipSubList',
	   			success:function(data,textStatus){
	   				var nDetailsRow = partsTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});	
		} else {
    		$('i', this.parentNode).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			partsTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
		}
	});	
});
</g:javascript>