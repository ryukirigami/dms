<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="invoice_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
    	<tr>
        	<th style="border-bottom: none;">
            	<div style="width: 75px;">DEALER CODE</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 75px;">INV. DATE</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 75px;">INVOICE NO</div>
			</th>			
			<th style="border-bottom: none;">
            	<div style="width: 75px;">CUST. TYPE</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 75px;">REPAIR TYPE</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 75px;">CUST. CODE</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 75px;">LABOR</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 75px;">PART</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 75px;">OIL</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 75px;">MATERIAL</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 75px;">SUBLET</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 75px;">DISCOUNT</div>
			</th>
            <th style="border-bottom: none;">
                <div style="width: 75px;">TURN OVER</div>
            </th>
            <th style="border-bottom: none;">
                <div style="width: 75px;">TAX</div>
            </th>
            <th style="border-bottom: none;">
                <div style="width: 75px;">POLICE_NO</div>
            </th>
            <th style="border-bottom: none;">
                <div style="width: 75px;">CUSTOMER</div>
            </th>
            <th style="border-bottom: none;">
                <div style="width: 75px;">ADDRESS</div>
            </th>
            <th style="border-bottom: none;">
                <div style="width: 75px;">CITY</div>
            </th>
            <th style="border-bottom: none;">
                <div style="width: 75px;">TAX NO</div>
            </th>
            <th style="border-bottom: none;">
                <div style="width: 75px;">TAX DATE</div>
            </th>
            <th style="border-bottom: none;">
                <div style="width: 75px;">NPWP</div>
            </th>
            <th style="border-bottom: none;">
                <div style="width: 75px;">SERVICE ORDER</div>
            </th>
		</tr>
	</thead>
</table>
<g:javascript>
var invoiceTable;
var reloadInvoiceTable;

function searchData() {
    reloadInvoiceTable();
}

$(function() {
	reloadInvoiceTable = function() {
		invoiceTable.fnDraw();
	}
	
	invoiceTable = $('#invoice_datatables').dataTable({
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		},
	    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
	    },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "salesDataTablesList")}",
		"aoColumns": [
			{
				"sName": "dealerCode",
				"mDataProp": "dealerCode",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "invDate",
				"mDataProp": "invDate",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "invNo",
				"mDataProp": "invNo",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "custType",
				"mDataProp": "custType",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "repairType",
				"mDataProp": "repairType",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "custCode",
				"mDataProp": "custCode",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "labor",
				"mDataProp": "labor",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "part",
				"mDataProp": "part",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "oil",
				"mDataProp": "oil",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "material",
				"mDataProp": "material",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "sublet",
				"mDataProp": "sublet",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
			{
				"sName": "discount",
				"mDataProp": "discount",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"75px",
				"bVisible": true
			},
            {
                "sName": "discount",
                "mDataProp": "discount",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"75px",
                "bVisible": true
            },
            {
                "sName": "discount",
                "mDataProp": "discount",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"75px",
                "bVisible": true
            },
            {
                "sName": "discount",
                "mDataProp": "discount",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"75px",
                "bVisible": true
            },
            {
                "sName": "discount",
                "mDataProp": "discount",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"75px",
                "bVisible": true
            },
            {
                "sName": "discount",
                "mDataProp": "discount",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"75px",
                "bVisible": true
            },
            {
                "sName": "discount",
                "mDataProp": "discount",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"75px",
                "bVisible": true
            },
            {
                "sName": "discount",
                "mDataProp": "discount",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"75px",
                "bVisible": true
            },
            {
                "sName": "discount",
                "mDataProp": "discount",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"75px",
                "bVisible": true
            },
            {
                "sName": "discount",
                "mDataProp": "discount",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"75px",
                "bVisible": true
            },
            {
                "sName": "discount",
                "mDataProp": "discount",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"75px",
                "bVisible": true
            }
		],
		"fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
			var tanggalInvoiceStart = $("#tanggalInvoice_start").val();
			var tanggalInvoiceStartDay = $('#tanggalInvoice_start_day').val();
			var tanggalInvoiceStartMonth = $('#tanggalInvoice_start_month').val();
			var tanggalInvoiceStartYear = $('#tanggalInvoice_start_year').val();
            if(tanggalInvoiceStart && $('input[name=chkTanggalInvoice]').is(':checked')) {
				aoData.push(
						{"name": 'tanggalInvoiceStart', "value": "date.struct"},
						{"name": 'tanggalInvoiceStart_dp', "value": tanggalInvoiceStart},
						{"name": 'tanggalInvoiceStart_day', "value": tanggalInvoiceStartDay},
						{"name": 'tanggalInvoiceStart_month', "value": tanggalInvoiceStartMonth},
						{"name": 'tanggalInvoiceStart_year', "value": tanggalInvoiceStartYear},
						{"name": 'chkTanggalInvoice', "value": true}
				);
			}
			
            var tanggalInvoiceEnd = $("#tanggalInvoice_end").val();
            var tanggalInvoiceEndDay = $('#tanggalInvoice_end_day').val();
			var tanggalInvoiceEndMonth = $('#tanggalInvoice_end_month').val();
			var tanggalInvoiceEndYear = $('#tanggalInvoice_end_year').val();
			if(tanggalInvoiceEnd && $('input[name=chkTanggalInvoice]').is(':checked')) {
				aoData.push(
						{"name": 'tanggalInvoiceEnd', "value": "date.struct"},
						{"name": 'tanggalInvoiceEnd_dp', "value": tanggalInvoiceEnd},
						{"name": 'tanggalInvoiceEnd_day', "value": tanggalInvoiceEndDay},
						{"name": 'tanggalInvoiceEnd_month', "value": tanggalInvoiceEndMonth},
						{"name": 'tanggalInvoiceEnd_year', "value": tanggalInvoiceEndYear}
				);
			}
			
			var tanggalSettlementStart = $("#tanggalSettlement_start").val();
			var tanggalSettlementStartDay = $('#tanggalSettlement_start_day').val();
			var tanggalSettlementStartMonth = $('#tanggalSettlement_start_month').val();
			var tanggalSettlementStartYear = $('#tanggalSettlement_start_year').val();
            if(tanggalSettlementStart && $('input[name=chkTanggalSettlement]').is(':checked')) {
				aoData.push(
						{"name": 'tanggalSettlementStart', "value": "date.struct"},
						{"name": 'tanggalSettlementStart_dp', "value": tanggalSettlementStart},
						{"name": 'tanggalSettlementStart_day', "value": tanggalSettlementStartDay},
						{"name": 'tanggalSettlementStart_month', "value": tanggalSettlementStartMonth},
						{"name": 'tanggalSettlementStart_year', "value": tanggalSettlementStartYear},
						{"name": 'chkTanggalSettlement', "value": true}
				);
			}
			
            var tanggalSettlementEnd = $("#tanggalSettlement_end").val();
            var tanggalSetllementEndDay = $('#tanggalSettlement_end_day').val();
			var tanggalSettlementEndMonth = $('#tanggalSettlement_end_month').val();
			var tanggalSettlementEndYear = $('#tanggalSettlement_end_year').val();
			if(tanggalSettlementEnd && $('input[name=chkTanggalSettlement]').is(':checked')) {
				aoData.push(
						{"name": 'tanggalSettlementEnd', "value": "date.struct"},
						{"name": 'tanggalSettlementEnd_dp', "value": tanggalSettlementEnd},
						{"name": 'tanggalSettlementEnd_day', "value": tanggalSetllementEndDay},
						{"name": 'tanggalSettlementEnd_month', "value": tanggalSettlementEndMonth},
						{"name": 'tanggalSettlementEnd_year', "value": tanggalSettlementEndYear}
				);
			}
			
            var statusInvoice = $('input[name=statusInvoice]').val();
			if(statusInvoice == '')statusInvoice = 'SEMUA';
            if(statusInvoice && $('input[name=chkStatusInvoice]').is(':checked')) {
				aoData.push(
						{"name": 'statusInvoice', "value": statusInvoice},
						{"name": 'chkStatusInvoice', "value": true}
				);
			}
            
			var customerSPK = $('input[name=customerSPK]').val();
			if(customerSPK == '')customerSPK = 'SEMUA';
            if(customerSPK && $('input[name=chkCustomerSPK]').is(':checked')) {
				aoData.push(
						{"name": 'customerSPK', "value": customerSPK},
						{"name": 'chkCustomerSPK', "value": true}
				);
			}
			
			var metodeBayar = $('input[name=metodeBayar]').val();
			if(metodeBayar == '')metodeBayar = 'SEMUA';
            if(metodeBayar && $('input[name=chkMetodeBayar]').is(':checked')) {
				aoData.push(
						{"name": 'metodeBayar', "value": metodeBayar},
						{"name": 'chkMetodeBayar', "value": true}
				);
			}
			
			var nomorInvoice = $('input[name=nomorInvoice]').val();
            if(nomorInvoice && $('input[name=chkNomorInvoice]').is(':checked')) {
				aoData.push(
						{"name": 'nomorInvoice', "value": nomorInvoice},
						{"name": 'chkNomorInvoice', "value": true}
				);
			}
			
			var tipeInvoice = $('input[name=tipeInvoice]').val();
			if(tipeInvoice == '')tipeInvoice = 'SEMUA';
            if(tipeInvoice && $('input[name=chkTipeInvoice]').is(':checked')) {
				aoData.push(
						{"name": 'tipeInvoice', "value": tipeInvoice},
						{"name": 'chkTipeInvoice', "value": true}
				);
			}
			
			var customerName = $('input[name=customerName]').val();
            if(customerName && $('input[name=chkCustomerName]').is(':checked')) {
				aoData.push(
						{"name": 'customerName', "value": customerName},
						{"name": 'chkCustomerName', "value": true}
				);
			}
			
			var startAging = $('input[name=startAging]').val();
			var endAging = $('input[name=endAging]').val();
            if(startAging && endAging && $('input[name=chkAging]').is(':checked')) {
				aoData.push(
						{"name": 'startAging', "value": startAging},
						{"name": 'endAging', "value": endAging},
						{"name": 'chkAging', "value": true}
				);
			}
			
			var nomorPolisi = $('input[name=nomorPolisi]').val();
            if(nomorPolisi && $('input[name=chkNomorPolisi]').is(':checked')) {
				aoData.push(
						{"name": 'nomorPolisi', "value": nomorPolisi},
						{"name": 'chkNomorPolisi', "value": true}
				);
			}
			
			$.ajax({ "dataType": 'json',
				"type": "POST",
				"url": sSource,
				"data": aoData ,
				"success": function (json) {
					fnCallback(json);
				},
				"complete": function () {}
			});
			
		}
	});
	
    var anOpen = [];
	$("#invoice_datatables tbody").delegate("tr i", "click", function (e) {
		e.preventDefault();		
   		var nTr = this.parentNode.parentNode;		
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this.parentNode).attr( 'class', "icon-minus" );
    		var oData = invoiceTable.fnGetData(nTr);			
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${request.contextPath}/slipList/invoiceSubList',
	   			success:function(data,textStatus){
	   				var nDetailsRow = invoiceTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});	
		} else {
    		$('i', this.parentNode).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			invoiceTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
		}
	});	
		
});
</g:javascript>