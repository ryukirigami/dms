<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="refund_booking_fee_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	style="table-layout: fixed;"
	width="100%">
	<thead>
    <tr>
            	<th style="border-bottom: none;">
                	<div style="width: 75px;">PROCESS DATE</div>
    			</th>
    			<th style="border-bottom: none;">
                	<div style="width: 75px;">PROCESS TIME</div>
    			</th>
    			<th style="border-bottom: none;">
                	<div style="width: 75px;">DOC ID</div>
    			</th>			
    			<th style="border-bottom: none;">
                	<div style="width: 75px;">ITEM ID</div>
    			</th>
    			<th style="border-bottom: none;">
                	<div style="width: 75px;">POSTING DATE</div>
    			</th>
    			<th style="border-bottom: none;">
                	<div style="width: 75px;">COMPANY CODE</div>
    			</th>
    			<th style="border-bottom: none;">
                	<div style="width: 75px;">CURRENCY</div>
    			</th>
    			<th style="border-bottom: none;">
                	<div style="width: 75px;">REFERENCE</div>
    			</th>
    			<th style="border-bottom: none;">
                	<div style="width: 75px;">ACCOUNT CODE</div>
    			</th>
    			<th style="border-bottom: none;">
                	<div style="width: 75px;">AMOUNT</div>
    			</th>
    			<th style="border-bottom: none;">
                	<div style="width: 75px;">ASSIGNMENT</div>
    			</th>
    			<th style="border-bottom: none;">
                	<div style="width: 75px;">TAX CODE</div>
    			</th>
    			<th style="border-bottom: none;">
                	<div style="width: 75px;">BASELINE DATE</div>
    			</th>
                <th style="border-bottom: none;">
                    <div style="width: 75px;">CUST NAME</div>
                </th>
                <th style="border-bottom: none;">
                    <div style="width: 75px;">CUST ADDRESS</div>
                </th>
                <th style="border-bottom: none;">
                    <div style="width: 75px;">CITY NPWP</div>
                </th>
                <th style="border-bottom: none;">
                    <div style="width: 75px;">KODE POS</div>
                </th>
                <th style="border-bottom: none;">
                    <div style="width: 75px;">SP G/L ASSIGNMENT</div>
                </th>
    		</tr>
	</thead>
</table>
<g:javascript>
var refundBookingFeeTable;
var reloadRefundBookingFeeTable;

function searchData() {
    reloadRefundBookingFeeTable();
}

$(function() {
	reloadRefundBookingFeeTable = function() {
		refundBookingFeeTable.fnDraw();
	}
	
	refundBookingFeeTable = $('#refund_booking_fee_datatables').dataTable({
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		},
	    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
	    },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "refundBookingFeeDataTablesList")}",
		"aoColumns": [
					{
						"sName": "processDate",
						"mDataProp": "processDate",
						"bSearchable": false,
						"bSortable": false,
						"sWidth":"75px",
						"bVisible": true
					},
					{
						"sName": "processTime",
						"mDataProp": "processTime",
						"bSearchable": false,
						"bSortable": false,
						"sWidth":"75px",
						"bVisible": true
					},
					{
						"sName": "docId",
						"mDataProp": "docId",
						"bSearchable": false,
						"bSortable": false,
						"sWidth":"75px",
						"bVisible": true
					},
					{
						"sName": "itemId",
						"mDataProp": "itemId",
						"bSearchable": false,
						"bSortable": false,
						"sWidth":"75px",
						"bVisible": true
					},
					{
						"sName": "postingDate",
						"mDataProp": "postingDate",
						"bSearchable": false,
						"bSortable": false,
						"sWidth":"75px",
						"bVisible": true
					},
					{
						"sName": "companyCode",
						"mDataProp": "companyCode",
						"bSearchable": false,
						"bSortable": false,
						"sWidth":"75px",
						"bVisible": true
					},
					{
						"sName": "currency",
						"mDataProp": "currency",
						"bSearchable": false,
						"bSortable": false,
						"sWidth":"75px",
						"bVisible": true
					},
					{
						"sName": "reference",
						"mDataProp": "reference",
						"bSearchable": false,
						"bSortable": false,
						"sWidth":"75px",
						"bVisible": true
					},
					{
						"sName": "accountCode",
						"mDataProp": "accountCode",
						"bSearchable": false,
						"bSortable": false,
						"sWidth":"75px",
						"bVisible": true
					},
					{
						"sName": "amount",
						"mDataProp": "amount",
						"bSearchable": false,
						"bSortable": false,
						"sWidth":"75px",
						"bVisible": true
					},
					{
						"sName": "assignment",
						"mDataProp": "assignment",
						"bSearchable": false,
						"bSortable": false,
						"sWidth":"75px",
						"bVisible": true
					},
					{
						"sName": "taxCode",
						"mDataProp": "taxCode",
						"bSearchable": false,
						"bSortable": false,
						"sWidth":"75px",
						"bVisible": true
					},
					{
						"sName": "baselineDate",
						"mDataProp": "baselineDate",
						"bSearchable": false,
						"bSortable": false,
						"sWidth":"75px",
						"bVisible": true
					},
		            {
		                "sName": "custName",
		                "mDataProp": "custName",
		                "bSearchable": false,
		                "bSortable": false,
		                "sWidth":"75px",
		                "bVisible": true
		            },
		            {
		                "sName": "custAddress",
		                "mDataProp": "custAddress",
		                "bSearchable": false,
		                "bSortable": false,
		                "sWidth":"75px",
		                "bVisible": true
		            },
		            {
		                "sName": "cityNpwp",
		                "mDataProp": "cityNpwp",
		                "bSearchable": false,
		                "bSortable": false,
		                "sWidth":"75px",
		                "bVisible": true
		            },
		            {
		                "sName": "kodePos",
		                "mDataProp": "kodePos",
		                "bSearchable": false,
		                "bSortable": false,
		                "sWidth":"75px",
		                "bVisible": true
		            },
		            {
		                "sName": "spGl",
		                "mDataProp": "spGl",
		                "bSearchable": false,
		                "bSortable": false,
		                "sWidth":"75px",
		                "bVisible": true
		            }
				],
		"fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
			var tanggalKuitansiStart = $("#tanggalKuitansi_start").val();
			var tanggalKuitansiStartDay = $('#tanggalKuitansi_start_day').val();
			var tanggalKuitansiStartMonth = $('#tanggalKuitansi_start_month').val();
			var tanggalKuitansiStartYear = $('#tanggalKuitansi_start_year').val();
            if(tanggalKuitansiStart && $('input[name=chkTanggalKuitansi]').is(':checked')) {
				aoData.push(
						{"name": 'tanggalKuitansiStart', "value": "date.struct"},
						{"name": 'tanggalKuitansiStart_dp', "value": tanggalKuitansiStart},
						{"name": 'tanggalKuitansiStart_day', "value": tanggalKuitansiStartDay},
						{"name": 'tanggalKuitansiStart_month', "value": tanggalKuitansiStartMonth},
						{"name": 'tanggalKuitansiStart_year', "value": tanggalKuitansiStartYear},
						{"name": 'chkTanggalKuitansi', "value": true}
				);
			}
			
            var tanggalKuitansiEnd = $("#tanggalKuitansi_end").val();
            var tanggalKuitansiEndDay = $('#tanggalKuitansi_end_day').val();
			var tanggalKuitansiEndMonth = $('#tanggalKuitansi_end_month').val();
			var tanggalKuitansiEndYear = $('#tanggalKuitansi_end_year').val();
			if(tanggalKuitansiEnd && $('input[name=chkTanggalKuitansi]').is(':checked')) {
				aoData.push(
						{"name": 'tanggalKuitansiEnd', "value": "date.struct"},
						{"name": 'tanggalKuitansiEnd_dp', "value": tanggalKuitansiEnd},
						{"name": 'tanggalKuitansiEnd_day', "value": tanggalKuitansiEndDay},
						{"name": 'tanggalKuitansiEnd_month', "value": tanggalKuitansiEndMonth},
						{"name": 'tanggalKuitansiEnd_year', "value": tanggalKuitansiEndYear}
				);
			}			
			
            var jenisBayar = $('input[name=jenisBayar]').val();
			if(jenisBayar == '')jenisBayar='SEMUA';
            if(jenisBayar && $('input[name=chkJenisBayar]').is(':checked')) {
				aoData.push(
						{"name": 'jenisBayar', "value": jenisBayar},
						{"name": 'chkJenisBayar', "value": true}
				);
			}
            
			var metodeBayar = $('input[name=metodeBayar]').val();
			if(metodeBayar == '')metodeBayar='SEMUA';
            if(metodeBayar && $('input[name=chkMetodeBayar]').is(':checked')) {
				aoData.push(
						{"name": 'metodeBayar', "value": metodeBayar},
						{"name": 'chkMetodeBayar', "value": true}
				);
			}
			
			var nomorKuitansi = $('input[name=nomorKuitansi]').val();
            if(nomorKuitansi && $('input[name=chkNomorKuitansi]').is(':checked')) {
				aoData.push(
						{"name": 'nomorKuitansi', "value": nomorKuitansi},
						{"name": 'chkNomorKuitansi', "value": true}
				);
			}
			
			var namaKasir = $('input[name=namaKasir]').val();
            if(namaKasir && $('input[name=chkNamaKasir]').is(':checked')) {
				aoData.push(
						{"name": 'namaKasir', "value": namaKasir},
						{"name": 'chkNamaKasir', "value": true}
				);
			}
			
			$.ajax({ "dataType": 'json',
				"type": "POST",
				"url": sSource,
				"data": aoData ,
				"success": function (json) {
					fnCallback(json);
				},
				"complete": function () {}
			});
			
		}
	});
	
    var anOpen = [];
	$("#refund_booking_fee_datatables tbody").delegate("tr i", "click", function (e) {
		e.preventDefault();		
   		var nTr = this.parentNode.parentNode;		
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this.parentNode).attr( 'class', "icon-minus" );
    		var oData = refundBookingFeeTable.fnGetData(nTr);			
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${request.contextPath}/slipList/kuitansiSubList',
	   			success:function(data,textStatus){
	   				var nDetailsRow = refundBookingFeeTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});	
		} else {
    		$('i', this.parentNode).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			refundBookingFeeTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
		}
	});
		
});
</g:javascript>