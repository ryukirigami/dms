<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'delivery.slipList.label', default: 'Delivery - Export File')}" />
        <title>Delivery - Export File</title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
			$(function() {				
				
				changeJenisBayar = function() {
					var jenisBayar = $('select[name=jenisBayarSelect] option:selected').val();
					$('input[name=jenisBayar]').val(jenisBayar);
				}
				
				changeMetodeBayar = function() {
					var metodeBayar = $('select[name=metodeBayarSelect] option:selected').val();
					$('input[name=metodeBayar]').val(metodeBayar);
				}
				
				clearData = function() {
					$('#search-table').find(':input').each(function() {
						switch(this.type) {
							case 'password':
							case 'select-multiple':
							case 'select-one':
							case 'text':
							case 'hidden':
							case 'textarea':
								$(this).val('');
							    break;
							case 'checkbox':
							case 'radio':
								this.checked = false;
						}
					});
				}
				
				closeData = function() {
					var url = '${request.contextPath}/#/home';    
					$(location).attr('href',url);
				}
				
				previewData = function() {
	                $("#kuitansiListPreviewContent").empty();
	                $.ajax({
						type: 'POST', 
						url: '${request.contextPath}/slipList/previewKuitansi',
	                    success: function (data, textStatus) {
	                        $("#kuitansiListPreviewContent").html(data);
	                        $("#kuitansiListPreviewModal").modal({
	                            "backdrop": "static",
	                            "keyboard": true,
	                            "show": true
	                        }).css({
								'width': '1200px', 
								'margin-left': function () {
	                            	return - ( $(this).width() / 2 );
	                            }
							});
	                    },
	                    error: function (XMLHttpRequest, textStatus, errorThrown) {},
	                    complete: function (XMLHttpRequest, textStatus) {
	                        $('#spinner').fadeOut();
	                    }
	                });	                
	            }
			});
		</g:javascript>
	</head>
	<body>
        <div class="navbar box-header no-border">
                  <span class="pull-left">Delivery - Export File</span>
        </div>
        <div>&nbsp;</div>
        <div class="box">

            <!-- Start Tab -->
            <ul class="nav nav-tabs" style="margin-bottom: 0px;">
                    <li><a href="javascript:loadPath('exportFile/salesList');">Sales</a></li>
                <li><a href="javascript:loadPath('exportFile/spkInvoiceSettlementList');">SPK Invoice, Settlement, Refund Pph 23, Refund PPN</a></li>
                <li><a href="javascript:loadPath('exportFile/bookingFeeList');">Booking Fee</a></li>
                <li class="active"><a href="#">Refund Booking Fee</a></li>
            </ul>
            <!-- End Tab -->

            <!-- Start Kriteria Search -->
            <div class="box" style="padding-top: 5px; padding-left: 10px;">
                <div class="span12" id="user-table">
                    <legend style="font-size: small;">
                        <g:message code="delivery.slipList.kriteria.search.label" default="Export From Database to File"/>
                    </legend>
                    <g:if test="${flash.message}">
                        <div class="alert alert-error">
                            ${flash.message}
                        </div>
                    </g:if>
                </div>

                <div class="span12" id="search-table" style="padding-left: 0px;">
                    <table width="95%;">
                        <tr align="left">
                            <td align="left" style="width: 130px; display:table-cell; padding:5px;">
                                <label class="control-label" for="tangal_invoice">
                                    <g:message code="delivery.slipList.tanggal.invoice.label" default="Tanggal Booking Fee"/>
                                </label>
                            </td>
                            <td align="left" style="width: 10px; display:table-cell; padding:5px;">
                                <input type="checkbox" name="chkTanggalInvoice"/>&nbsp;
                            </td>
                            <td align="left" style="width: 130px; display:table-cell; padding:5px;">
                                <ba:datePicker id="tanggalInvoice_start" name="tanggalInvoice_start" precision="day" format="dd/MM/yyyy"  value="" />
                            </td>
                            <td align="left" style="width: 10px; display:table-cell; padding:5px;">
                                <label class="control-label" for="until">
                                    &nbsp;<g:message code="delivery.slipList.until.label" default="s/d"/>&nbsp;
                                </label>
                            </td>
                            <td align="left" style="width: 130px; display:table-cell; padding:5px;">
                                <ba:datePicker id="tanggalInvoice_end" name="tanggalInvoice_end" precision="day" format="dd/MM/yyyy"  value="" />
                            </td>
                            <td>&nbsp;
                                  <g:field type="button" style="width: 90px" class="btn btn-primary search" onclick="searchData();"
                                                               name="search" id="search" value="OK" />
                                  </td>

                        </tr>

                          </table>
                </div>

                <div class="span12" id="print-table" style="padding-left: 0px; margin-left: 0px;">
                    <g:render template="bookingFeeDataTables"/>
                    <br/>
                       <g:field type="button" style="width: 110px" class="btn btn-primary search" onclick="toExcel();"
                                    name="toExcel" id="toExcel" value="Export to Excel File" />
                       <g:field type="button" style="width: 110px" class="btn btn-primary clear" onclick="toText();"
                                    name="toText" id="toText" value="Export to Text File" />
                </div>
            </div>
            <!-- End Data List -->
        </div>

        <ul class="nav pull-right">
                  <g:field type="button" style="width: 90px" class="btn btn-primary" onclick="closeData();"
                           name="close" id="close" value="${message(code: 'default.close.label', default: 'Close')}" />
        </ul>

	</body>
</html>