
<%@ page import="com.kombos.administrasi.PerhitunganNextService" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="perhitunganNextService_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="perhitunganNextService.t114TglBerlaku.label" default="Tanggal Berlaku" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="perhitunganNextService.t114KategoriService.label" default="Kategori Service" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="perhitunganNextService.bahanBakar.label" default="Bahan Bakar" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; text-align: center" colspan="2">
				<div><g:message code="perhitunganNextService.nextService.label" default="Next Service" /></div>
			</th>

		</tr>
		<tr>

			<th style="border-top: none;padding: 5px;">
                <div id="filter_t114TglBerlaku" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
                    <input type="hidden" name="search_t114TglBerlaku" value="date.struct">
					<input type="hidden" name="search_t114TglBerlaku_day" id="search_t114TglBerlaku_day" value="">
					<input type="hidden" name="search_t114TglBerlaku_month" id="search_t114TglBerlaku_month" value="">
					<input type="hidden" name="search_t114TglBerlaku_year" id="search_t114TglBerlaku_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t114TglBerlaku_dp" value="" id="search_t114TglBerlaku" class="search_init">
				</div>
			</th>
	
            <th style="border-top: none;padding: 5px;">
				<div id="filter_t114KategoriService" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <select name="search_t114KategoriService" id="search_t114KategoriService" onchange="reloadPerhitunganNextServiceTable();" style="width:100%">
                        <option value=""></option>
                        <option value="1">SBE</option>
                        <option value="0">SBI</option>
                    </select>
                </div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_bahanBakar" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_bahanBakar" class="search_init" />
				</div>
			</th>
	
            <th align="center" style="border-bottom: none;padding: 5px;">
				<g:message code="perhitunganNextService.t141Km.label" default="KM" />
                <div id="filter_t114Km" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t114Km" onkeypress="return isNumberKey(event);" class="search_init" />
                </div>
			</th>
            <th align="center" style="border-bottom: none;padding: 5px;">
				<g:message code="perhitunganNextService.t141Bulan.label" default="Bulan" />
                <div id="filter_t114Bulan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t114Bulan" onkeypress="return isNumberKey(event);" class="search_init" />
                </div>
			</th>

		</tr>
	</thead>
</table>

<g:javascript>
var perhitunganNextServiceTable;
var reloadPerhitunganNextServiceTable;
$(function(){
	
	reloadPerhitunganNextServiceTable = function() {
		perhitunganNextServiceTable.fnDraw();
	}

    var recordsperhitunganNextServiceperpage = [];
    var anPerhitunganNextServiceSelected;
    var jmlRecPerhitunganNextServicePerPage=0;
    var id;

    $('#search_t114TglBerlaku').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t114TglBerlaku_day').val(newDate.getDate());
			$('#search_t114TglBerlaku_month').val(newDate.getMonth()+1);
			$('#search_t114TglBerlaku_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			perhitunganNextServiceTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	perhitunganNextServiceTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	perhitunganNextServiceTable = $('#perhitunganNextService_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsPerhitunganNextService = $("#perhitunganNextService_datatables tbody .row-select");
            var jmlPerhitunganNextServiceCek = 0;
            var nRow;
            var idRec;
            rsPerhitunganNextService.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsperhitunganNextServiceperpage[idRec]=="1"){
                    jmlPerhitunganNextServiceCek = jmlPerhitunganNextServiceCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsperhitunganNextServiceperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecPerhitunganNextServicePerPage = rsPerhitunganNextService.length;
            if(jmlPerhitunganNextServiceCek==jmlRecPerhitunganNextServicePerPage && jmlRecPerhitunganNextServicePerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
        "fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "t114TglBerlaku",
	"mDataProp": "t114TglBerlaku",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "t114KategoriService",
	"mDataProp": "t114KategoriService",
	"aTargets": [1],
        "mRender": function ( data, type, row ) {
          
            if(data=="0"){
                  return "SBI";
             }else
             {
                  return "SBE";
             }
        
        },
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "bahanBakar",
	"mDataProp": "bahanBakar",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t114Km",
	"mDataProp": "t114Km",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t114Bulan",
	"mDataProp": "t114Bulan",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var t114TglBerlaku = $('#search_t114TglBerlaku').val();
						var t114TglBerlakuDay = $('#search_t114TglBerlaku_day').val();
						var t114TglBerlakuMonth = $('#search_t114TglBerlaku_month').val();
						var t114TglBerlakuYear = $('#search_t114TglBerlaku_year').val();
						
						if(t114TglBerlaku){
							aoData.push(
									{"name": 'sCriteria_t114TglBerlaku', "value": "date.struct"},
									{"name": 'sCriteria_t114TglBerlaku_dp', "value": t114TglBerlaku},
									{"name": 'sCriteria_t114TglBerlaku_day', "value": t114TglBerlakuDay},
									{"name": 'sCriteria_t114TglBerlaku_month', "value": t114TglBerlakuMonth},
									{"name": 'sCriteria_t114TglBerlaku_year', "value": t114TglBerlakuYear}
							);
						}
	
						var t114KategoriService = $('#search_t114KategoriService').val();
						if(t114KategoriService){
							aoData.push(
									{"name": 'sCriteria_t114KategoriService', "value": t114KategoriService}
							);
						}
	
						var bahanBakar = $('#filter_bahanBakar input').val();
						if(bahanBakar){
							aoData.push(
									{"name": 'sCriteria_bahanBakar', "value": bahanBakar}
							);
						}
	
						var t114Km = $('#filter_t114Km input').val();
						if(t114Km){
							aoData.push(
									{"name": 'sCriteria_t114Km', "value": t114Km}
							);
						}
	
						var t114Bulan = $('#filter_t114Bulan input').val();
						if(t114Bulan){
							aoData.push(
									{"name": 'sCriteria_t114Bulan', "value": t114Bulan}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

    $('.select-all').click(function(e) {
        $("#perhitunganNextService_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsperhitunganNextServiceperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsperhitunganNextServiceperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

    $('#perhitunganNextService_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsperhitunganNextServiceperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anPerhitunganNextServiceSelected = perhitunganNextServiceTable.$('tr.row_selected');
            if(jmlRecPerhitunganNextServicePerPage == anPerhitunganNextServiceSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsperhitunganNextServiceperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});


    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57)){
            if(charCode == 46){
                return true
            }
            return false;
        }
        return true;
    }
</g:javascript>


			
