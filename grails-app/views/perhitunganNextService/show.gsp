<%@ page import="com.kombos.administrasi.PerhitunganNextService" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="bootstrapeditable"/>
    <g:set var="entityName"
           value="${message(code: 'perhitunganNextService.label', default: 'Perhitungan Next Service')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <g:javascript disposition="head">
    var deletePerhitunganNextService;

    $(function(){ 
    deletePerhitunganNextService=function(id){
    $.ajax({type:'POST', url:'${request.contextPath}/perhitunganNextService/delete/',
    data: { id: id },
    success:function(data,textStatus){
    reloadPerhitunganNextServiceTable();
    expandTableLayout();
    },
    error:function(XMLHttpRequest,textStatus,errorThrown){},
    complete:function(XMLHttpRequest,textStatus){
    $('#spinner').fadeOut();
    }
    });
    }
    });

    </g:javascript>
</head>

<body>
<div id="show-perhitunganNextService" role="main">
    <legend>
        <g:message code="default.show.label" args="[entityName]"/>
    </legend>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table id="perhitunganNextService"
           class="table table-bordered table-hover">
        <tbody>

        <g:if test="${perhitunganNextServiceInstance?.t114TglBerlaku}">
            <tr>
                <td class="span2" style="text-align: right;"><span
                        id="t114TglBerlaku-label" class="property-label"><g:message
                            code="perhitunganNextService.t114TglBerlaku.label" default="Tanggal Berlaku"/>:</span></td>

                <td class="span3"><span class="property-value"
                                        aria-labelledby="t114TglBerlaku-label">
                    %{--<ba:editableValue
                    bean="${perhitunganNextServiceInstance}" field="t114TglBerlaku"
                    url="${request.contextPath}/PerhitunganNextService/updatefield" type="text"
                    title="Enter t114TglBerlaku" onsuccess="reloadPerhitunganNextServiceTable();" />--}%

                    <g:formatDate date="${perhitunganNextServiceInstance?.t114TglBerlaku}" format="dd/MMMM/yyyy"/>

                </span></td>

            </tr>
        </g:if>

        <g:if test="${perhitunganNextServiceInstance?.t114KategoriService}">
            <tr>
                <td class="span2" style="text-align: right;"><span
                        id="t114KategoriService-label" class="property-label"><g:message
                            code="perhitunganNextService.t114KategoriService.label" default="Kategori Service"/>:</span>
                </td>

                <td class="span3"><span class="property-value"
                                        aria-labelledby="t114KategoriService-label">
                    %{--<ba:editableValue
                    bean="${perhitunganNextServiceInstance}" field="t114KategoriService"
                    url="${request.contextPath}/PerhitunganNextService/updatefield" type="text"
                    title="Enter t114KategoriService" onsuccess="reloadPerhitunganNextServiceTable();" />--}%
                    <g:if test="${perhitunganNextServiceInstance.t114KategoriService == '1'}">
                        <g:message code="perhitunganNextService.t114KategoriService.label" default="SBE"/>
                    </g:if>
                    <g:else>
                        <g:message code="perhitunganNextService.t114KategoriService.label" default="SBI"/>
                    </g:else>
                </span></td>

            </tr>
        </g:if>

        <g:if test="${perhitunganNextServiceInstance?.bahanBakar}">
            <tr>
                <td class="span2" style="text-align: right;"><span
                        id="bahanBakar-label" class="property-label"><g:message
                            code="perhitunganNextService.bahanBakar.label" default="Bahan Bakar"/>:</span></td>

                <td class="span3"><span class="property-value"
                                        aria-labelledby="bahanBakar-label">
                    %{--<ba:editableValue
                    bean="${perhitunganNextServiceInstance}" field="bahanBakar"
                    url="${request.contextPath}/PerhitunganNextService/updatefield" type="text"
                    title="Enter bahanBakar" onsuccess="reloadPerhitunganNextServiceTable();" />--}%

                    ${perhitunganNextServiceInstance?.bahanBakar?.encodeAsHTML()}

                </span></td>

            </tr>
        </g:if>

        <g:if test="${perhitunganNextServiceInstance?.t114Km}">
            <tr>
                <td class="span2" style="text-align: right;"><span
                        id="t114Km-label" class="property-label"><g:message
                            code="perhitunganNextService.nextService.label" default="Next Service"/>:</span></td>

                <td class="span3"><span class="property-value"
                                        aria-labelledby="t114Km-label">
                    %{--<ba:editableValue
                    bean="${perhitunganNextServiceInstance}" field="t114Km"
                    url="${request.contextPath}/PerhitunganNextService/updatefield" type="text"
                    title="Enter t114Km" onsuccess="reloadPerhitunganNextServiceTable();" />--}%

                    <g:fieldValue bean="${perhitunganNextServiceInstance}" field="t114Km"/>&nbsp;&nbsp;KM<br/>
                    <g:fieldValue bean="${perhitunganNextServiceInstance}" field="t114Bulan"/>&nbsp;&nbsp;Bulan<br/>

                </span></td>

            </tr>
        </g:if>

        <g:if test="${perhitunganNextServiceInstance?.lastUpdProcess}">
            <tr>
                <td class="span2" style="text-align: right;"><span
                        id="lastUpdProcess-label" class="property-label"><g:message
                            code="perhitunganNextService.lastUpdProcess.label" default="last Upd Process" />:</span></td>

                <td class="span3"><span class="property-value"
                                        aria-labelledby="lastUpdProcess-label">
                    %{--<ba:editableValue
                            bean="${perhitunganNextServiceInstance}" field="m013Telp2"
                            url="${request.contextPath}/perhitunganNextService/updatefield" type="text"
                            title="Enter m013Telp2" onsuccess="reloadperhitunganNextServiceTable();" />--}%

                    <g:fieldValue field="lastUpdProcess" bean="${perhitunganNextServiceInstance}" />

                </span></td>

            </tr>
        </g:if>

        <g:if test="${perhitunganNextServiceInstance?.dateCreated}">
            <tr>
                <td class="span2" style="text-align: right;"><span
                        id="dateCreated-label" class="property-label"><g:message
                            code="perhitunganNextService.dateCreated.label" default="Date Created" />:</span></td>

                <td class="span3"><span class="property-value"
                                        aria-labelledby="dateCreated-label">
                    %{--<ba:editableValue
                            bean="${perhitunganNextServiceInstance}" field="m013Telp2"
                            url="${request.contextPath}/perhitunganNextService/updatefield" type="text"
                            title="Enter m013Telp2" onsuccess="reloadperhitunganNextServiceTable();" />--}%

                    <g:formatDate date="${perhitunganNextServiceInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                </span></td>

            </tr>
        </g:if>

        <g:if test="${perhitunganNextServiceInstance?.createdBy}">
            <tr>
                <td class="span2" style="text-align: right;"><span
                        id="createdBy-label" class="property-label"><g:message
                            code="perhitunganNextService.createdBy.label" default="Created By" />:</span></td>

                <td class="span3"><span class="property-value"
                                        aria-labelledby="createdBy-label">
                    %{--<ba:editableValue
                            bean="${perhitunganNextServiceInstance}" field="m013Telp2"
                            url="${request.contextPath}/perhitunganNextService/updatefield" type="text"
                            title="Enter m013Telp2" onsuccess="reloadperhitunganNextServiceTable();" />--}%

                    <g:fieldValue field="createdBy" bean="${perhitunganNextServiceInstance}" />

                </span></td>

            </tr>
        </g:if>

        <g:if test="${perhitunganNextServiceInstance?.lastUpdated}">
            <tr>
                <td class="span2" style="text-align: right;"><span
                        id="lastUpdate-label" class="property-label"><g:message
                            code="perhitunganNextService.lastUpdate.label" default="Last Update" />:</span></td>

                <td class="span3"><span class="property-value"
                                        aria-labelledby="lastUpdate-label">
                    %{--<ba:editableValue
                            bean="${perhitunganNextServiceInstance}" field="m013Telp2"
                            url="${request.contextPath}/perhitunganNextService/updatefield" type="text"
                            title="Enter m013Telp2" onsuccess="reloadperhitunganNextServiceTable();" />--}%

                    <g:formatDate date="${perhitunganNextServiceInstance?.lastUpdated}" type="datetime" style="MEDIUM" />

                </span></td>

            </tr>
        </g:if>

        <g:if test="${perhitunganNextServiceInstance?.updatedBy}">
            <tr>
                <td class="span2" style="text-align: right;"><span
                        id="updatedBy-label" class="property-label"><g:message
                            code="perhitunganNextService.updatedBy.label" default="Update By" />:</span></td>

                <td class="span3"><span class="property-value"
                                        aria-labelledby="updatedBy-label">
                    <g:fieldValue field="updatedBy" bean="${perhitunganNextServiceInstance}" />

                </span></td>

            </tr>
        </g:if>
        </tbody>
    </table>
    <g:form class="form-horizontal">
        <fieldset class="buttons controls">
            <a class="btn cancel" href="javascript:void(0);"
               onclick="expandTableLayout();"><g:message
                    code="default.button.cancel.label" default="Cancel"/></a>
            <g:remoteLink class="btn btn-primary edit" action="edit"
                          id="${perhitunganNextServiceInstance?.id}"
                          update="[success: 'perhitunganNextService-form', failure: 'perhitunganNextService-form']"
                          on404="alert('not found');">
                <g:message code="default.button.edit.label" default="Edit"/>
            </g:remoteLink>
            <ba:confirm id="delete" class="btn cancel"
                        message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
                        onsuccess="deletePerhitunganNextService('${perhitunganNextServiceInstance?.id}')"
                        label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
        </fieldset>
    </g:form>
</div>
</body>
</html>
