<%@ page import="com.kombos.administrasi.BahanBakar; com.kombos.administrasi.PerhitunganNextService;java.lang.Double" %>

<g:javascript>
function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)){
        if(charCode == 46){
            return true
        }
        return false;
    }
    return true;
}
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: perhitunganNextServiceInstance, field: 't114TglBerlaku', 'error')} required">
	<label class="control-label" for="t114TglBerlaku">
		<g:message code="perhitunganNextService.t114TglBerlaku.label" default="Tanggal Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <ba:datePicker name="t114TglBerlaku" precision="day"  value="${perhitunganNextServiceInstance?.t114TglBerlaku}" format="dd/mm/yyyy" required="true"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: perhitunganNextServiceInstance, field: 't114KategoriService', 'error')} required">
	<label class="control-label" for="t114KategoriService">
		<g:message code="perhitunganNextService.t114KategoriService.label" default="Kategori Service" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:radioGroup name="t114KategoriService" values="['0','1']" labels="['SBI','SBE']" value="${perhitunganNextServiceInstance?.t114KategoriService}" required="" >
            ${it.radio} ${it.label}
        </g:radioGroup>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: perhitunganNextServiceInstance, field: 'bahanBakar', 'error')} required">
	<label class="control-label" for="bahanBakar">
		<g:message code="perhitunganNextService.bahanBakar.label" default="Bahan Bakar" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="bahanBakar" name="bahanBakar.id" noSelection="['':'Pilih Bahan Bakar']" from="${BahanBakar.list()}" optionKey="id" required="" value="${perhitunganNextServiceInstance?.bahanBakar?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: perhitunganNextServiceInstance, field: 't114Km', 'error')} required">
	<label class="control-label" for="t114Km">
		<g:message code="perhitunganNextService.t114Km.label" default="Next Service" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
          <g:textField onkeypress="return isNumberKey(event);" name="t114Km" maxlength="8" required="" value="${perhitunganNextServiceInstance?.t114Km ? Double.valueOf(perhitunganNextServiceInstance?.t114Km).longValue() : ''}"/>&nbsp;&nbsp;KM<br/><br/>
	</div>
  	<div class="controls">
		<g:textField onkeypress="return isNumberKey(event);" name="t114Bulan" maxlength="8" required="" value="${perhitunganNextServiceInstance?.t114Bulan ? Double.valueOf(perhitunganNextServiceInstance?.t114Bulan).longValue() : ''}"/>&nbsp;&nbsp;Bulan
	</div>
</div>

<g:javascript>
    document.getElementById("t114TglBerlaku").focus();
</g:javascript>