<%--
  Created by IntelliJ IDEA.
  User: DENDYSWARAN
  Date: 5/20/14
  Time: 3:02 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'transaction.label', default: 'Penerimaan Hasil Tagihan Service Invoice')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <style>
    .form-horizontal .form-group {
        margin-bottom: 5px;
    }
    </style>
    <r:require modules="baseapplayout, baseapplist, autoNumeric"/>
    <g:javascript>
        var nomorInvoiceT701DT;
        var tagihan = 0;
        var showModalInvoiceT701;
        function changeSubtype(a){
            $('#subledger'+a).val("");
            $('#idSubledger'+a).val("");
        }
        $(function() {
            showModalInvoiceT701 = function() {
                $("#nomorInvoiceT701DT").modal({
                        "backdrop" : "dynamic",
                        "keyboard" : true,
                        "show" : true
                    }).css({'width': '900px','margin-left': function () {return -($(this).width() / 2);}});
            }

            $("input[type='radio'][name='tipeTransaksi']").click(function() {
                var tipe = $(this).val();

                $("#tanggalTransfer").removeAttr("readonly");
                $("#namaBank").removeAttr("readonly");
                $("#nomorPDCKombos").removeAttr("readonly");
                $("#nomorPDCKombos").removeAttr("readonly");
                $("#tanggalJatuhTempoPDCKombos").removeAttr("readonly");
                $("#namaBank").val("");
                $("#tanggalTransfer").val("");
                $("#nomorPDCKombos").val("");
                $("#tanggalJatuhTempoPDCKombos").val("");
                if (tipe === "KAS") {
                    $("#namaBank").attr("disabled", true);
                    $("#tanggalTransfer").attr("disabled", true);
                    $("#nomorPDCKombos").attr("readonly", "readonly");
                    $("#tanggalJatuhTempoPDCKombos").attr("readonly", "readonly");
                } else if (tipe === "BANK") {
                    $("#namaBank").attr("disabled", false);
                    $("#tanggalTransfer").attr("disabled", false);
                    $("#nomorPDCKombos").attr("readonly", "readonly");
                    $("#tanggalJatuhTempoPDCKombos").attr("readonly", "readonly");
                } else if (tipe === "PDC") {
//                    $("#tanggalTransfer").attr("readonly", "readonly");
                    $("#tanggalTransfer").attr("disabled", false);
                }
            });

            $("#btnSave").click(function() {
                var tanggalTransaksi = $("#tanggalTransaksi").val();
                var tipeTransaksi    = $("input[type='radio'][name='tipeTransaksi']:checked").val();
                var bank             = $("#namaBank").val();
                var tanggalTransfer  = $("#tanggalTransfer").val();
                var noPDCKombos      = $("#nomorPDCKombos").val();
                var tglJatuhTempo    = $("#tanggalJatuhTempoPDCKombos").val();
                var keterangan       = $("#keterangan").val();
                var details         = crawlDetails();
                var noKwitansi       = $("#nomorInvoiceT701").val();
                var jmlPembayaran    = new Number($("#jumlahPembayaran").autoNumeric("get"));
                var tambahanBiaya = new Number(0);

                $("#detail-table tbody").find("tr").each(function() {
                    var sJumlah = $(this).find("td input#jumlah").autoNumeric("get");
                    var iJumlah = new Number(sJumlah);

                    tambahanBiaya += iJumlah;
                });
                if(details=="salah") {
                    return
                }
                if(tipeTransaksi=="BANK" && !tanggalTransfer){
                    alert("mohon lengkapi field terlebih dahulu");
                }else if (tanggalTransaksi && noKwitansi && tipeTransaksi) {
                    var conf = confirm("Apakah Anda yakin untuk menyimpan?")
                    if(conf){
                        $.post("${request.getContextPath()}/transaction/saveByDocCategory", {
                            transactionDate: tanggalTransaksi,
                            transactionType: tipeTransaksi,
                            bank: bank,
                            transferDate: tanggalTransfer,
                            dueDate: tglJatuhTempo,
                            cekNumber: noPDCKombos,
                            description: keterangan,
                            docNumber: noKwitansi,
                            amount: jmlPembayaran,
                            tambahanBiaya: tambahanBiaya,
                            documentCategory: "DOCPI",
                            typeJournal : 'TAGIHAN',
                            details:details
                        }, function(data) {
                            if (data.error) {
                                alert(data.error);
                            } else {
                                alert(data.success);
                                $("#idTrx").val(data.transactionInstance.id);
                                $("#btnReset").show();
                                $("#btnCetak").show();
                            }
                        });
                    }
                }  else {
                    alert("mohon lengkapi field terlebih dahulu");
                }


            })

            $("#btnPilih").click(function() {
                $("#nomorInvoiceT701").val($("#noInv").val());
                $("#nomorPolisi").val($("#noPol").val());
                $("#pelanggan").val($("#customer").val());

                $("#noInv").val();
                $("#noPol").val();
                $("#customer").val();


                $("#nomorInvoiceT701DT").modal("hide");

                var nomorInvoiceT701 = $("#nomorInvoiceT701").val();
                var tipeTransaksi = "INVOICET701";
                var customer      = $("#pelanggan").val();

                $("#keterangan").val("Penerimaan Hasil Tagihan Service No. Invoice " + nomorInvoiceT701 + ", atas nama " + customer);

                $.get("${request.getContextPath()}/transaction/getTagihanPelanggan", {noInv: nomorInvoiceT701}, function(data) {
                    if (data.amount) {
                        $("#jumlahTagihan").val(data.amount);
                        $("#serviceAdvisor").val(data.serviceAdvisor);
                        tagihan = data.amount;
                    } else {
                        alert("data tagihan collection tidak ditemukan");
                    }
                });

                // get transaction by doc category
                $.get("${request.getContextPath()}/transaction/getTransactionByDocNumber", {docNumber: nomorInvoiceT701, docCategory: tipeTransaksi}, function(data) {
                    var sdhBayar = 0;
                    var jmlTagih = data.tagihan;
                    console.log(jmlTagih);

                    $(data.result).each(function(k,v) {
                        sdhBayar += new Number(v.amount);
                    });
                    $("#jumlahSudahBayar").val(sdhBayar);
                    $("#sisaHarusBayar").val(jmlTagih - sdhBayar);
                });
            })
        });

        function doPrint(){
            var idTrx = $("#idTrx").val();
            var noInv = $("#nomorInvoiceT701").val();
            window.location = "${request.contextPath}/transaction/printKW?idTrx="+idTrx+"&noInv="+noInv;
        }
        //tambahan
        var putAccount;
        function modalSubledger(urut) {
            var cekST = $("#subtypeid"+urut).val();
            if(cekST==""){
                alert("Pilih Subtype terlebih dahulu")
                return
            }
            $("#popupSubledgerContent").empty();
                $.ajax({type:'POST', url:'${request.contextPath}/subledgerModal?subtype='+cekST,
                    success:function(data,textStatus){
                            $("#popupSubledgerContent").html(data);
                            $("#popupSubledgerModal").modal({
                                "backdrop" : "dynamic",
                                "keyboard" : true,
                                "show" : true
                            }).css({'width': '500px','margin-left': function () {return -($(this).width() / 2);}});

                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){},
                    complete:function(XMLHttpRequest,textStatus){
                        $('#spinner').fadeOut();
                    }
                });
                $("#idx").val(urut);
        }

        putAccount = function() {
            var idx = $('#idx').val();
           $('#subledger-table tbody').find('tr').each(function(k,v) {
               if ($(this).find('td:first-child input[type="radio"]').is(":checked")) {
                    var aData = subledgerTable.fnGetData(k);
                    var accountNumber = aData.accountNumber;
                    var id = aData.id;
                    $('#subledger'+idx).val(accountNumber);
                    $('#idSubledger'+idx).val(id);
               }
            });
            $("#closeSubledger").click();
        }

        function showModalAccount(idx) {
            $("#popupAccountNumberContent").empty();
            $.ajax({type:'POST', url:'${request.contextPath}/mappingJurnalModal',
                success:function(data,textStatus){
                        $("#popupAccountNumberContent").html(data);
                        $("#popupAccountNumberModal").modal({
                            "backdrop" : "dynamic",
                            "keyboard" : true,
                            "show" : true
                        }).css({'width': '1000px','margin-left': function () {return -($(this).width() / 2);}});

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
            $("#idx").val(idx);
        };

        function setAccount() {
            var idx = $('#idx').val();
            $('#accountNumber-table tbody').find('tr').each(function(k,v) {
               if ($(this).find('td:first-child input[type="radio"]').is(":checked")) {
                    var aData = accountNumberTable.fnGetData(k);
                    var accountNumber = aData.accountNumber;
                    var accountName = aData.accountName;
                    $('#nomorRekeningDetail'+idx).val(accountNumber);
                    $('#namaRekeningDetail'+idx).val(accountName);
                    $("#closeAccount").click();
               }
            });
        }
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]"/></span>
    <ul class="nav pull-right">

    </ul>
</div>
<div class="box">
    <div class="span12">
        <div class="row-fluid">
            <div class="span6">
                <div id="pengeluaran-form" class="form-horizontal">
                    <fieldset class="form">
                        <div class="control-group fieldcontain">
                            <label class="control-label" for="nomorInvoiceT701">
                                Nomor Invoice
                            </label>

                            <div class="controls">
                                <input class="span6" id="nomorInvoiceT701" type="text" readonly/>
                                <div class="input-append">
                                    <button type="submit" class="btn" onclick="showModalInvoiceT701();">
                                        <i class="icon-search"></i>
                                    </button>
                                    <g:hiddenField name="idTrx" id="idTrx" />
                                    <g:hiddenField name="idx" id="idx" />
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="nomorPolisi">
                                Nomor Polisi
                            </label>

                            <div class="controls">
                                <input type="text" readonly id="nomorPolisi">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="pelanggan">
                                Pelanggan
                            </label>

                            <div class="controls">
                                <input type="text" readonly id="pelanggan">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="serviceAdvisor">
                                Service Advisor
                            </label>

                            <div class="controls">
                                <input type="text" readonly id="serviceAdvisor">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="tanggalTransaksi">
                                Tanggal Transaksi
                            </label>

                            <div class="controls">
                                <ba:datePicker format="dd/mm/yyyy" name="tanggalTransaksi"/>
                            </div>
                        </div><!-- Tanggal Transaksi -->

                        <div class="form-group">
                            <label class="control-label" for="tipeTransaksi">
                                Tipe Transaksi
                            </label>

                            <div class="controls">
                                <input type="radio" name="tipeTransaksi" id="tipeTransaksi" value="KAS">KAS
                                <input type="radio" name="tipeTransaksi" value="BANK">BANK
                                <input type="radio" name="tipeTransaksi" value="PDC">PDC
                            </div>
                        </div><!-- Tipe Transaksi -->


                    <div class="form-group">
                        <button id="btnAdd" class="btn btn-default">Tambah</button>
                        <a href="javascript:void(0);" id="btnSave" class="btn btn-primary">Simpan</a>
                        <button id="btnReset" style="display: none" class="btn btn-default" onclick="loadPath('transaction/penerimaanHasilTagihanServiceInvoice');">Reset Form</button>
                        <button id="btnCetak" style="display: none" onclick="doPrint();" class="btn btn-primary">Cetak Kwitansi</button>
                    </div>

                    </fieldset>
                </div>
            </div>

            <div class="span6">
                <div class="form-horizontal">
                    <fieldset class="form">
                        <div class="control-group fieldcontain">
                        <label class="control-label" for="namaBank">
                            Nama Bank
                        </label>

                        <div class="controls">
                            <g:select name="namaBank" from="${com.kombos.finance.BankAccountNumber.createCriteria().list {eq("companyDealer", session.userCompanyDealer);eq("staDel","0");bank{order("m702NamaBank")}}}" id="namaBank" optionValue="${{it.bank.m702NamaBank+" "+it.bank.m702Cabang}}" optionKey="id"
                                      noSelection="['':'']"/>
                        </div>
                    </div> <!-- NAMA BANK -->

                    <div class="form-group">
                        <label class="control-label" for="tanggalTransfer">
                            Tanggal Transfer
                        </label>

                        <div class="controls">
                            <ba:datePicker name="tanggalTransfer" format="dd/mm/yyyy"/>
                        </div>
                    </div> <!-- Tanggal Transfer -->

                    <div class="form-group">
                        <label class="control-label" for="nomorPDCKombos">
                            Nomor PDC Pelanggan
                        </label>

                        <div class="controls">
                            <input type="text" id="nomorPDCKombos">
                        </div>
                    </div> <!-- Nomor PDC Pelanggan -->

                    <div class="form-group">
                        <label class="control-label" for="tanggalJatuhTempoPDCKombos" style="width: 181px;">
                            Tgl Jth Tempo PDC Pelanggan
                        </label>

                        <div class="controls">
                            <ba:datePicker format="dd/mm/yyyy" name="tanggalJatuhTempoPDCKombos"/>
                        </div>
                    </div> <!-- Tanggal Jatuh Tempo PDC Kombos -->

                    <div class="form-group">
                        <label class="control-label" for="jumlahTagihan">
                            Tagihan ke Pelanggan
                        </label>

                        <div class="controls">
                            <input type="text" class="numeric"  id="jumlahTagihan" readonly>
                        </div>
                    </div> <!-- Jumlah Tagihan -->

                    <div class="form-group">
                        <label class="control-label" for="jumlahsudahbayar">
                            Jumlah yang Sudah diBayar
                        </label>

                        <div class="controls">
                            <input type="text" id="jumlahSudahBayar" readonly>
                        </div>
                    </div> <!-- Jumlah yang Sudah diBayar -->

                    <div class="form-group">
                        <label class="control-label" for="sisaHarusBayar">
                            Jumlah yg Harus diBayar
                        </label>

                        <div class="controls">
                            <input type="text" class="numeric" id="sisaHarusBayar" readonly>
                        </div>
                    </div> <!-- Sisa yg Harus diBayar -->

                    <div class="form-group">
                        <label class="control-label" for="jumlahPembayaran">
                            Jumlah Pembayaran
                        </label>

                        <div class="controls">
                            <input type="text" class="numeric" id="jumlahPembayaran">
                        </div>
                    </div> <!-- Jumlah Pembayaran -->
                    <div class="form-group">
                        <label class="control-label" for="keterangan">
                            Keterangan
                        </label>

                        <div class="controls">
                            <g:textArea name="keterangan" rows="5" cols="5"/>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
    </div>
    <div class="span12" id="detail-form" style="display: block; margin-top: 20px;">
        <legend>Detail</legend>
        <table id="detail-table" class="table table-bordered">
            <thead>
            <th>No</th>
            <th>Nomor Rekening</th>
            <th>Nama Rekening</th>
            <th>Tipe Transaksi</th>
            <th>Jumlah</th>
            <th>SubType</th>
            <th>SubLedger</th>
            <th>Keterangan</th>
            <th>Aktivitas</th>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>
<div id="popupAccountNumberModal" class="modal fade">
    <div class="modal-dialog" style="width: 1000px;">
        <div class="modal-content" style="width: 1000px;">
            <!-- dialog body -->
            <div class="modal-body" style="max-height: 480px;">
                %{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}%
                <div id="popupAccountNumberContent">
                    <div class="iu-content"></div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="control-group">
                    <a href="javascript:void(0);" id="btnPilihAkun" onclick="setAccount();" class="btn btn-primary">
                        Add Account
                    </a>
                    <a href="javascript:void(0);" class="btn cancel" id="closeAccount" data-dismiss="modal">
                        Close
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="popupSubledgerModal" class="modal fade">
    <div class="modal-dialog" style="width: 500px;">
        <div class="modal-content" style="width: 500px;">
            <!-- dialog body -->
            <div class="modal-body" style="max-height: 480px;">
                %{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}%
                <div id="popupSubledgerContent">
                    <div class="iu-content"></div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="control-group">
                    <a href="javascript:void(0);" id="btnPilihKaryawan" onclick="putAccount();" class="btn btn-primary">
                        Save
                    </a>
                    <a href="javascript:void(0);" class="btn cancel" id="closeSubledger" data-dismiss="modal">
                        Close
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="nomorInvoiceT701DT" class="modal hide">
    <div class="modal-content">
        <div class="modal-body">
            <g:render template="nomorInvoiceT701DT"/>
        </div>

        <div class="modal-footer">
            <a class="btn btn-primary" id="btnPilih">Pilih</a>
        </div>
    </div>
</div>

<g:javascript>
    var SHOW_BANK_LIST    = 0;
    var SHOW_ACCOUNT_LIST = 1;
    var NOTE_SALDO = "${noteSaldo}";
    $(function() {
        if(NOTE_SALDO!=""){
            alert(NOTE_SALDO)
        }
//        prepareCBBank();

        $(".numeric").autoNumeric("init", {
            vMin: '-999999999.99',
            vMax: '99999999999.99'
        });

        $("#btnAdd").click(function() {
            addRow();
        });


        $("#namaBank").change(function() {
            if (typeof $(this).attr("readOnly") == "undefined") {
                // show bank modal
                var accountNumber = $(this).find("option:selected").data("account");
                $("#nomorRekening").val(accountNumber);
            }
        });

        //
        $("#btnPutAccount").click(function() {
            var row = $("#account-modal input[type='hidden']#row").val();
            putAccountDataToDetails(row, mapAccountData());
            $("#account-modal").modal("hide");
        });

        // Delete account
        $("#detail-table tbody").on("click", "tr td a i#btnDeleteAccount", function(evt) {
            var sure = confirm("Anda yakin ingin menghapus data ini?");
            if (sure) {
                $(this).parent().parent().parent().remove();
                performCalculation();
            }
        });

        // After input jumlah
        $("#detail-table tbody").on("focusout", "tr td input#jumlah", function(evt) {
            performCalculation();
        });


    });

    function addRow() {
        var mRow  = getLastRow();
        var count = new Number(mRow.count) + 1;

        $("#detail-table tbody").append(drawRowColumn(count));
        $(".numeric").autoNumeric("init", {
            vMin: '-999999999.99',
            vMax: '99999999999.99'
        });
    };

    function getLastRow() {
        if ($("#detail-table tbody").html().length <= 18)
            return {
                count: 0,
                hasRow: false
            }
        return {
            count: $("#detail-table tbody tr:last").data("row"),
            hasRow: true
        }
    }

    function drawRowColumn(iCount) {
        return "<tr data-row='"+iCount+"'>" +
"<td ><span id='nomorUrut'>"+iCount+"</span></td>" +
"<td><input type='text' id='nomorRekeningDetail"+iCount+"' readOnly style='width: 100px;'><a href='javascript:void(0);' onclick='showModalAccount("+iCount+")'><i class='icon-search' id='btnSearchAccount'></i></a></td>" +
"<td><input type='text' id='namaRekeningDetail"+iCount+"' readOnly style='width: 100px;'></td>" +
"<td><span id='tipeTransaksi'>Debet</span></td>" +
"<td><input type='text' class='numeric' id='jumlah' style='width: 100px;'></td>" +
'<td><select id="subtypeid'+iCount+'" onchange="changeSubtype('+iCount+');" >' +
' <option value="">-Pilih Subtype-</option>'
    <g:each in="${com.kombos.finance.SubType.createCriteria().list {eq("staDel","0");order("description")}}">
        + '<option value="${it?.subType}">${it?.subType}</option>'
    </g:each>+'</select>' +'&nbsp;' +
"<td><input type='hidden' id='idSubledger"+iCount+"' ><input type='text' id='subledger"+iCount+"' readOnly style='width: 100px;'><a href='javascript:void(0);' onclick='modalSubledger("+iCount+");'><i class='icon-search' id='btnSearchName'></i></a></td>" +
"<td><input type='text' id='deskripsi' style='width: 100px;'></td>" +
"<td><a href='javascript:void(0);'><i id='btnDeleteAccount' class='icon-trash'></i></a></td>" +
"</tr>";
    }

    function countTableRows() {
        var count = 0;
        $("#detail-table tbody").find("tr").each(function(k,v) {
            count++;
        });
        return count;
    }

    function mapAccountData() {
        var accountNumber = null;
        var accountName   = null;
        var id            = null;
        var typeJurnal    = null;

        $("#detailAccountNumber-table tbody").find("tr").each(function() {
            if ($(this).find("td:first-child input[type='radio']").is(":checked")) {
                accountNumber = $(this).find("td#accountNumber").html();
                accountName   = $(this).find("td#accountName").html();
                typeJurnal    = "Debet";
                id            = $(this).find("td#id").html();
            }
        });

        return {
            id: id,
            accountNumber: accountNumber,
            accountName: accountName,
            typeJurnal:typeJurnal
        }
    }

    function prepareCBBank() {
        $.get("${request.getContextPath()}/pengeluaranLainLain/showDetails", {
            type: SHOW_BANK_LIST
        }, function(data) {
            var html = "<option></option>";
            for (var i = 0; i < data.length; i++)
                html += "<option data-account='"+data[i]["accountNumber"]+"' data-bank='"+data[i]["bankId"]+"' value='"+data[i]["bankName"]+"'>"+data[i]["bankName"]+" "+data[i]["bankCabang"]+" </option>";
            $("#namaBank").html(html);
        });
    }

    function performCalculation() {
        var jumlahPembayaran = new Number(0);
        var sisaBayar = $("#sisaHarusBayar").autoNumeric("get");
        sisaBayar = new Number(sisaBayar);
        $("#detail-table tbody").find("tr").each(function() {
            var sJumlah = $(this).find("td input#jumlah").autoNumeric("get");
            var iJumlah = new Number(sJumlah);
            jumlahPembayaran += iJumlah;
        });
        var hasil = sisaBayar - jumlahPembayaran
        $("#jumlahPembayaran").autoNumeric("set", hasil);
    }

    function crawlDetails() {
        var status = ""
         var arr             = new Array();
         var count = 0;
        $("#detail-table tbody").find("tr").each(function() {
            count++;
            var accountNumber   = $(this).find("td input[type='text']#nomorRekeningDetail"+count+"").val();
            var accountName     = $(this).find("td input[type='text']#namaRekeningDetail"+count+"").val();
            var transactionType = $(this).find("td span#tipeTransaksi").html();
            var total           = $(this).find("td input[type='text']#jumlah").autoNumeric("get");
            var description     = $(this).find("td input[type='text']#deskripsi").val();
            var subtype         = $(this).find("td select#subtypeid"+count+"").val();
            var subledger       = $(this).find("td input[type='hidden']#idSubledger"+count+"").val();

            var map = {
                accountNumber: accountNumber,
                accountName: accountName,
                transactionType: transactionType,
                total: total,
                description: description ? description : "-",
                subtype : subtype,
                subledger : subledger
            }

            if((accountNumber == "4.10.10.01.001" || accountNumber == "4.10.30.01.001" ||
            accountNumber == "1.30.20.01.001" || accountNumber == "1.30.40.01.001" || accountNumber == "1.30.10.01.001"
            ) && (subledger=="" || !subledger)){
                alert("Apakah Benar, Subledger Tidak diisi?");
            }

            if(accountNumber==""){
                alert("Nomor akun tidak boleh KOSONG");
                status = "salah"
            }

            if(total==0 || total==""){
                alert("Jumlah tidak Boleh KOSONG atau NOL");
                status = "salah"
            }

            arr.push(map);
        });

        if(status==""){
            return JSON.stringify(arr);
        }else{
            return status
        }
    }

</g:javascript>
</body>
</html>