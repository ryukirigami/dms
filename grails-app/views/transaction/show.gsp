

<%@ page import="com.kombos.finance.Transaction" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'transaction.label', default: 'Transaction')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteTransaction;

$(function(){ 
	deleteTransaction=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/transaction/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadTransactionTable();
   				expandTableLayout('transaction');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-transaction" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="transaction"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${transactionInstance?.transactionCode}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="transactionCode-label" class="property-label"><g:message
					code="transaction.transactionCode.label" default="Transaction Code" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="transactionCode-label">
						%{--<ba:editableValue
								bean="${transactionInstance}" field="transactionCode"
								url="${request.contextPath}/Transaction/updatefield" type="text"
								title="Enter transactionCode" onsuccess="reloadTransactionTable();" />--}%
							
								<g:fieldValue bean="${transactionInstance}" field="transactionCode"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionInstance?.transactionDate}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="transactionDate-label" class="property-label"><g:message
					code="transaction.transactionDate.label" default="Transaction Date" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="transactionDate-label">
						%{--<ba:editableValue
								bean="${transactionInstance}" field="transactionDate"
								url="${request.contextPath}/Transaction/updatefield" type="text"
								title="Enter transactionDate" onsuccess="reloadTransactionTable();" />--}%
							
								<g:formatDate date="${transactionInstance?.transactionDate}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionInstance?.description}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="description-label" class="property-label"><g:message
					code="transaction.description.label" default="Description" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="description-label">
						%{--<ba:editableValue
								bean="${transactionInstance}" field="description"
								url="${request.contextPath}/Transaction/updatefield" type="text"
								title="Enter description" onsuccess="reloadTransactionTable();" />--}%
							
								<g:fieldValue bean="${transactionInstance}" field="description"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionInstance?.amount}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="amount-label" class="property-label"><g:message
					code="transaction.amount.label" default="Amount" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="amount-label">
						%{--<ba:editableValue
								bean="${transactionInstance}" field="amount"
								url="${request.contextPath}/Transaction/updatefield" type="text"
								title="Enter amount" onsuccess="reloadTransactionTable();" />--}%
							
								<g:fieldValue bean="${transactionInstance}" field="amount"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionInstance?.journal}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="journal-label" class="property-label"><g:message
					code="transaction.journal.label" default="Journal" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="journal-label">
						%{--<ba:editableValue
								bean="${transactionInstance}" field="journal"
								url="${request.contextPath}/Transaction/updatefield" type="text"
								title="Enter journal" onsuccess="reloadTransactionTable();" />--}%
							
								<g:link controller="journal" action="show" id="${transactionInstance?.journal?.id}">${transactionInstance?.journal?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionInstance?.docNumber}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="docNumber-label" class="property-label"><g:message
					code="transaction.docNumber.label" default="Doc Number" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="docNumber-label">
						%{--<ba:editableValue
								bean="${transactionInstance}" field="docNumber"
								url="${request.contextPath}/Transaction/updatefield" type="text"
								title="Enter docNumber" onsuccess="reloadTransactionTable();" />--}%
							
								<g:fieldValue bean="${transactionInstance}" field="docNumber"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionInstance?.inOutType}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="inOutType-label" class="property-label"><g:message
					code="transaction.inOutType.label" default="In Out Type" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="inOutType-label">
						%{--<ba:editableValue
								bean="${transactionInstance}" field="inOutType"
								url="${request.contextPath}/Transaction/updatefield" type="text"
								title="Enter inOutType" onsuccess="reloadTransactionTable();" />--}%
							
								<g:fieldValue bean="${transactionInstance}" field="inOutType"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionInstance?.receiptNumber}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="receiptNumber-label" class="property-label"><g:message
					code="transaction.receiptNumber.label" default="Receipt Number" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="receiptNumber-label">
						%{--<ba:editableValue
								bean="${transactionInstance}" field="receiptNumber"
								url="${request.contextPath}/Transaction/updatefield" type="text"
								title="Enter receiptNumber" onsuccess="reloadTransactionTable();" />--}%
							
								<g:fieldValue bean="${transactionInstance}" field="receiptNumber"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionInstance?.transferDate}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="transferDate-label" class="property-label"><g:message
					code="transaction.transferDate.label" default="Transfer Date" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="transferDate-label">
						%{--<ba:editableValue
								bean="${transactionInstance}" field="transferDate"
								url="${request.contextPath}/Transaction/updatefield" type="text"
								title="Enter transferDate" onsuccess="reloadTransactionTable();" />--}%
							
								<g:formatDate date="${transactionInstance?.transferDate}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionInstance?.dueDate}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dueDate-label" class="property-label"><g:message
					code="transaction.dueDate.label" default="Due Date" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dueDate-label">
						%{--<ba:editableValue
								bean="${transactionInstance}" field="dueDate"
								url="${request.contextPath}/Transaction/updatefield" type="text"
								title="Enter dueDate" onsuccess="reloadTransactionTable();" />--}%
							
								<g:formatDate date="${transactionInstance?.dueDate}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionInstance?.cekNumber}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="cekNumber-label" class="property-label"><g:message
					code="transaction.cekNumber.label" default="Cek Number" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="cekNumber-label">
						%{--<ba:editableValue
								bean="${transactionInstance}" field="cekNumber"
								url="${request.contextPath}/Transaction/updatefield" type="text"
								title="Enter cekNumber" onsuccess="reloadTransactionTable();" />--}%
							
								<g:fieldValue bean="${transactionInstance}" field="cekNumber"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionInstance?.pdcStatus}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="pdcStatus-label" class="property-label"><g:message
					code="transaction.pdcStatus.label" default="Pdc Status" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="pdcStatus-label">
						%{--<ba:editableValue
								bean="${transactionInstance}" field="pdcStatus"
								url="${request.contextPath}/Transaction/updatefield" type="text"
								title="Enter pdcStatus" onsuccess="reloadTransactionTable();" />--}%
							
								<g:fieldValue bean="${transactionInstance}" field="pdcStatus"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionInstance?.pdcTransferDate}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="pdcTransferDate-label" class="property-label"><g:message
					code="transaction.pdcTransferDate.label" default="Pdc Transfer Date" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="pdcTransferDate-label">
						%{--<ba:editableValue
								bean="${transactionInstance}" field="pdcTransferDate"
								url="${request.contextPath}/Transaction/updatefield" type="text"
								title="Enter pdcTransferDate" onsuccess="reloadTransactionTable();" />--}%
							
								<g:formatDate date="${transactionInstance?.pdcTransferDate}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="transaction.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${transactionInstance}" field="staDel"
								url="${request.contextPath}/Transaction/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadTransactionTable();" />--}%
							
								<g:fieldValue bean="${transactionInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="transaction.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${transactionInstance}" field="createdBy"
								url="${request.contextPath}/Transaction/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadTransactionTable();" />--}%
							
								<g:fieldValue bean="${transactionInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="transaction.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${transactionInstance}" field="updatedBy"
								url="${request.contextPath}/Transaction/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadTransactionTable();" />--}%
							
								<g:fieldValue bean="${transactionInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="transaction.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${transactionInstance}" field="lastUpdProcess"
								url="${request.contextPath}/Transaction/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadTransactionTable();" />--}%
							
								<g:fieldValue bean="${transactionInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionInstance?.bankAccountNumber}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="bankAccountNumber-label" class="property-label"><g:message
					code="transaction.bankAccountNumber.label" default="Bank Account Number" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="bankAccountNumber-label">
						%{--<ba:editableValue
								bean="${transactionInstance}" field="bankAccountNumber"
								url="${request.contextPath}/Transaction/updatefield" type="text"
								title="Enter bankAccountNumber" onsuccess="reloadTransactionTable();" />--}%
							
								<g:link controller="bankAccountNumber" action="show" id="${transactionInstance?.bankAccountNumber?.id}">${transactionInstance?.bankAccountNumber?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="transaction.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${transactionInstance}" field="dateCreated"
								url="${request.contextPath}/Transaction/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadTransactionTable();" />--}%
							
								<g:formatDate date="${transactionInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionInstance?.documentCategory}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="documentCategory-label" class="property-label"><g:message
					code="transaction.documentCategory.label" default="Document Category" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="documentCategory-label">
						%{--<ba:editableValue
								bean="${transactionInstance}" field="documentCategory"
								url="${request.contextPath}/Transaction/updatefield" type="text"
								title="Enter documentCategory" onsuccess="reloadTransactionTable();" />--}%
							
								<g:link controller="documentCategory" action="show" id="${transactionInstance?.documentCategory?.id}">${transactionInstance?.documentCategory?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="transaction.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${transactionInstance}" field="lastUpdated"
								url="${request.contextPath}/Transaction/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadTransactionTable();" />--}%
							
								<g:formatDate date="${transactionInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionInstance?.transactionType}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="transactionType-label" class="property-label"><g:message
					code="transaction.transactionType.label" default="Transaction Type" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="transactionType-label">
						%{--<ba:editableValue
								bean="${transactionInstance}" field="transactionType"
								url="${request.contextPath}/Transaction/updatefield" type="text"
								title="Enter transactionType" onsuccess="reloadTransactionTable();" />--}%
							
								<g:link controller="transactionType" action="show" id="${transactionInstance?.transactionType?.id}">${transactionInstance?.transactionType?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('transaction');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${transactionInstance?.id}"
					update="[success:'transaction-form',failure:'transaction-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteTransaction('${transactionInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
