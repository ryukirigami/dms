<%@ page import="com.kombos.finance.Transaction" %>



<div class="control-group fieldcontain ${hasErrors(bean: transactionInstance, field: 'transactionCode', 'error')} required">
	<label class="control-label" for="transactionCode">
		<g:message code="transaction.transactionCode.label" default="Transaction Code" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="transactionCode" required="" value="${transactionInstance?.transactionCode}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: transactionInstance, field: 'transactionDate', 'error')} required">
	<label class="control-label" for="transactionDate">
		<g:message code="transaction.transactionDate.label" default="Transaction Date" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:datePicker name="transactionDate" precision="day"  value="${transactionInstance?.transactionDate}"  />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: transactionInstance, field: 'description', 'error')} required">
	<label class="control-label" for="description">
		<g:message code="transaction.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="description" required="" value="${transactionInstance?.description}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: transactionInstance, field: 'amount', 'error')} required">
	<label class="control-label" for="amount">
		<g:message code="transaction.amount.label" default="Amount" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field name="amount" value="${fieldValue(bean: transactionInstance, field: 'amount')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: transactionInstance, field: 'journal', 'error')} required">
	<label class="control-label" for="journal">
		<g:message code="transaction.journal.label" default="Journal" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="journal" name="journal.id" from="${com.kombos.finance.Journal.list()}" optionKey="id" required="" value="${transactionInstance?.journal?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: transactionInstance, field: 'docNumber', 'error')} required">
	<label class="control-label" for="docNumber">
		<g:message code="transaction.docNumber.label" default="Doc Number" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="docNumber" required="" value="${transactionInstance?.docNumber}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: transactionInstance, field: 'inOutType', 'error')} required">
	<label class="control-label" for="inOutType">
		<g:message code="transaction.inOutType.label" default="In Out Type" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="inOutType" required="" value="${transactionInstance?.inOutType}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: transactionInstance, field: 'receiptNumber', 'error')} required">
	<label class="control-label" for="receiptNumber">
		<g:message code="transaction.receiptNumber.label" default="Receipt Number" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="receiptNumber" required="" value="${transactionInstance?.receiptNumber}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: transactionInstance, field: 'transferDate', 'error')} required">
	<label class="control-label" for="transferDate">
		<g:message code="transaction.transferDate.label" default="Transfer Date" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:datePicker name="transferDate" precision="day"  value="${transactionInstance?.transferDate}"  />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: transactionInstance, field: 'dueDate', 'error')} required">
	<label class="control-label" for="dueDate">
		<g:message code="transaction.dueDate.label" default="Due Date" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:datePicker name="dueDate" precision="day"  value="${transactionInstance?.dueDate}"  />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: transactionInstance, field: 'cekNumber', 'error')} required">
	<label class="control-label" for="cekNumber">
		<g:message code="transaction.cekNumber.label" default="Cek Number" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="cekNumber" required="" value="${transactionInstance?.cekNumber}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: transactionInstance, field: 'pdcStatus', 'error')} required">
	<label class="control-label" for="pdcStatus">
		<g:message code="transaction.pdcStatus.label" default="Pdc Status" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="pdcStatus" required="" value="${transactionInstance?.pdcStatus}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: transactionInstance, field: 'pdcTransferDate', 'error')} required">
	<label class="control-label" for="pdcTransferDate">
		<g:message code="transaction.pdcTransferDate.label" default="Pdc Transfer Date" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:datePicker name="pdcTransferDate" precision="day"  value="${transactionInstance?.pdcTransferDate}"  />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: transactionInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="transaction.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${transactionInstance?.lastUpdProcess}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: transactionInstance, field: 'bankAccountNumber', 'error')} required">
	<label class="control-label" for="bankAccountNumber">
		<g:message code="transaction.bankAccountNumber.label" default="Bank Account Number" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="bankAccountNumber" name="bankAccountNumber.id" from="${com.kombos.finance.BankAccountNumber.list()}" optionKey="id" required="" value="${transactionInstance?.bankAccountNumber?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: transactionInstance, field: 'documentCategory', 'error')} required">
	<label class="control-label" for="documentCategory">
		<g:message code="transaction.documentCategory.label" default="Document Category" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="documentCategory" name="documentCategory.id" from="${com.kombos.finance.DocumentCategory.list()}" optionKey="id" required="" value="${transactionInstance?.documentCategory?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: transactionInstance, field: 'transactionType', 'error')} required">
	<label class="control-label" for="transactionType">
		<g:message code="transaction.transactionType.label" default="Transaction Type" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="transactionType" name="transactionType.id" from="${com.kombos.finance.TransactionType.list()}" optionKey="id" required="" value="${transactionInstance?.transactionType?.id}" class="many-to-one"/>
	</div>
</div>

