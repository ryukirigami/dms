<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<legend>Daftar Penjualan Langsung</legend>
<table id="detail-table" class="table table-bordered">

<input type="hidden" id="noDO">
<input type="hidden" id="noSO">
<input type="hidden" id="buyer">

<table id="nomorDO_table" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover fixed" style="table-layout: fixed; ">
    <col width="250px" />
    <col width="250px" />
    <col width="300px" />
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px; width:239px;">
            Nomor DO
        </th>
        <th style="border-bottom: none;padding: 5px; width:239px;">
            Nomor SO
        </th>
        <th style="border-bottom: none;padding: 5px; width:289px;" >
            Pembeli
        </th>
    </tr>

    <tr>

        <th style="border-top: none;padding: 5px; width:239px;">
            <div id="filter_noDO" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="noDO" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px; width:239px;">
            <div id="filter_noSO" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="noSO" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px; width:289px;">
            <div id="filter_buyer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="buyer" class="search_init" />
            </div>
        </th>

    </tr>

    </thead>
</table>
<g:javascript>
var nomorDOTable;
var reloadNomorDOTable;
var setValues;
$(function(){

    setValues = function(el) {
        var noDO  = $(el).parent().parent().find("td.noDO span").html();
        var noSO  = $(el).parent().parent().find("td.noSO").html();
        var cust   = $(el).parent().parent().find("td.buyer").html();

        $("#noDO").val(noDO);
        $("#noSO").val(noSO);
        $("#buyer").val(cust);

    }

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	nomorDOTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});


        nomorDOTable = $('#nomorDO_table').dataTable({
		"sScrollX": "800px",
		"bScrollCollapse": true,
		"bAutoWidth" : true,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : 5, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "nomorDeliveryOrder")}",
		"aoColumns": [

{
	"sName": "doNumber",
	"mDataProp": "doNumber",
	"aTargets": [0],
	"bSortable": false,
	"bSearchable": true,
	"sClass": "noDO",
	"bVisible": true,
	"mRender": function ( data, type, row ) {
	    return "<input type='radio' onclick='setValues(this);' name='sublet' class='radio-select'><span class='noDO'>"+data+"</span>";
	}
},
{
    "sName": "soNumber",
	"mDataProp": "soNumber",
	"aTargets": [1],
	"bSortable": false,
	"bSearchable": true,
	"sClass": "noSO",
	"bVisible": true
},
{
    "sName": "buyer",
	"mDataProp": "buyer",
	"aTargets": [2],
	"bSortable": false,
	"bSearchable": true,
	"sClass": "buyer",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var kode  = $('#filter_noDO input').val();
						var nopol = $("#filter_noSO input").val();
						var cust  = $("#filter_buyer input").val();

						if(kode){
							aoData.push(
									{"name": 'sCriteria_noDO', "value": kode}
							);
						}

						if (nopol) {
						    aoData.push(
									{"name": 'sCriteria_noSO', "value": nopol}
							);
						}

                        if (cust) {
                            aoData.push(
									{"name": 'sCriteria_buyer', "value": cust}
							);
                        }

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
$('#nomorDO_table tbody tr').live('click', function () {
        $(this).find('.radio-select').attr("checked",true);

        var noDO  = $(this).find("td.noDO span").html();
        var noSO  = $(this).find("td.noSO").html();
        var cust   = $(this).find("td.buyer").html();

        $("#noDO").val(noDO);
        $("#noSO").val(noSO);
        $("#buyer").val(cust);
    });


});
</g:javascript>
