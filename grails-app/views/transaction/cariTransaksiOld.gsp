<%--
  Created by IntelliJ IDEA.
  User: DENDYSWARAN
  Date: 5/19/14
  Time: 11:42 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'transaction.label', default: 'Cari Transaksi')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <style>
    .form-horizontal .form-group {
        margin-bottom: 5px;
    }
    </style>
    <r:require modules="baseapplayout, baseapplist"/>
    <g:javascript>
			var show;
			var edit;
			$(function(){

				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :
							shrinkTableLayout('transaction');
							<g:remoteFunction action="create"
                                              onLoading="jQuery('#spinner').fadeIn(1);"
                                              onSuccess="loadFormInstance('transaction', data, textStatus);"
                                              onComplete="jQuery('#spinner').fadeOut();"/>
        break;
    case '_DELETE_' :
        bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
								function(result){
									if(result){
										massDelete('transaction', '${request.contextPath}/transaction/massdelete', reloadTransactionTable);
									}
								});

							break;
				   }
				   return false;
				});

		showDetail = function(id){
                $("#detailContent").empty();
                $.ajax({
                    type:'POST',
                    url:'${request.contextPath}/transaction/showTransactionDetail',
                    data : {id : id},
                    success:function(data,textStatus){
                            $("#detailContent").html(data);
                            $("#detailModal").modal({
                                "backdrop" : "dynamic",
                                "keyboard" : true,
                                "show" : true
                            }).css({'width': '1100px','margin-left': function () {return -($(this).width() / 2);}});

                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){},
                    complete:function(XMLHttpRequest,textStatus){
                        $('#spinner').fadeOut();
                    }
                });
            }

				edit = function(id) {
					editInstance('transaction','${request.contextPath}/transaction/edit/'+id);
				};

});
    </g:javascript>
</head>

<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]"/></span>
    <ul class="nav pull-right">

    </ul>
</div>

<div class="box">

    <div class="span7" style="margin-left: 0;">
        <div class="form-horizontal">
        </div>
    </div>

    <div class="span12" id="transaction-table" style="margin-left: 0;">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>

        <g:render template="cariTransaksiDT"/>
    </div>

    <div class="span7" id="transaction-form" style="display: none;"></div>
</div>
<div id="detailModal" class="modal fade">
    <div class="modal-dialog" style="width: 1090px;">
        <div class="modal-content" style="width: 1090px;">
            <div class="modal-body" style="max-height: 450px;">
                <div id="detailContent"/>
                <div class="iu-content"></div>

            </div>
        </div>
    </div>
</div>


</body>
</html>
