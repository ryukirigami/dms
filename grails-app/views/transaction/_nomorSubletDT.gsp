<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<legend>Daftar Sublet</legend>
<table id="detail-table" class="table table-bordered">

<input type="hidden" id="noSublet_" value="">
<input type="hidden" id="nomorWo_" value="">
<input type="hidden" id="nomorPolisi_" value="">
<input type="hidden" id="pelanggan_" value="">
<input type="hidden" id="bengkel_" value="">

<table id="nomorSublet_table" cellpadding="0" cellspacing="0"
border="0"
class="display table table-striped table-bordered table-hover fixed" style="table-layout: fixed; ">
<col width="150px" />
<col width="150px" />
<col width="150px" />
<col width="150px" />
<col width="150px" />
<col width="100px" />
<thead>
<tr>

<th style="border-bottom: none;padding:5px; width:139px;">
    No. Sublet
</th>
<th style="border-bottom: none;padding:5px; width:139px;">
    No. WO
</th>
<th style="border-bottom: none;padding:5px; width:139px;">
    Nomor Polisi
</th>
<th style="border-bottom: none;padding:5px; width:139px;">
    Nama Customer
</th>
<th style="border-bottom: none;padding:5px; width:139px;">
    Nama Bengkel
</th>
<th style="border-bottom: none;padding:5px; width:89px;">
    Total
</th>
</tr>

<tr>

<th style="border-top: none;padding:5px; width:139px;">
    <div id="filter_noInvSublet" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="nomorInvSublet" class="search_init" />
    </div>
</th>

<th style="border-top: none;padding:5px; width:139px;">
    <div id="filter_nomorWo" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="nomorWo" class="search_init" />
    </div>
</th>

<th style="border-top: none;padding:5px; width:139px;">
    <div id="filter_nomorPolisi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="nomorPolisi" class="search_init" />
    </div>
</th>
<th style="border-top: none;padding:5px; width:139px;">
    <div id="filter_namaCustomer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="namaCustomer" class="search_init" />
    </div>
</th>

<th style="border-top: none;padding: 5px; width:139px;">
            %{--<div id="filter_namaBengkel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
                %{--<input type="text" name="namaBengkel" class="search_init" />--}%
            %{--</div>--}%
        </th>

        <th style="border-top: none;padding: 5px; width:89px;">
            %{--<div id="filter_total" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
                %{--<input type="text" name="total" class="search_init" />--}%
            %{--</div>--}%
        </th>

    </tr>

    </thead>
</table>

<g:javascript>
var nomorSubletTable;
var reloadNomorSubletTable;
var setValues;
$(function(){

     setValues = function(el) {
        var noSublet      = $(el).parent().parent().find("td.noSublet span").html();
        var noWo          = $(el).parent().parent().find("td.noWo").html();
        var noPolisi      = $(el).parent().parent().find("td.noPolisi").html();
        var namaPelanggan = $(el).parent().parent().find("td.namaPelanggan").html();
        var namaBengkel   = $(el).parent().parent().find("td.namaBengkel").html();

        $("#noSublet_").val(noSublet);
        $("#nomorWo_").val(noWo);
        $("#nomorPolisi_").val(noPolisi);
        $("#pelanggan_").val(namaPelanggan);
        $("#bengkel_").val(namaBengkel);
    }

    $("#nomorSublet_table th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	nomorSubletTable.fnDraw();
		}
	});
	$("#nomorSublet_table th div input").click(function (e) {
	 	e.stopPropagation();
	});

        nomorSubletTable = $('#nomorSublet_table').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : true,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "nomorPOSubletDT")}",
		"aoColumns": [

{
	"sName": "t412NoPOSublet",
	"mDataProp": "t412NoPOSublet",
	"aTargets": [0],
	"bSortable": false,
	"bSearchable": true,
	"bVisible": true,
	"sClass": "noSublet",
	"mRender": function ( data, type, row ) {
	    return "<input type='radio' onclick='setValues(this);' name='sublet' class='radio-select'><span>"+data+"</span>";
	}
},
{
	"sName": "reception",
	"mDataProp": "reception",
	"aTargets": [1],
	"bSortable": false,
	"bSearchable": false,
	"bVisible": true,
    "sClass": "noWo"
},

{
	"sName": "nomorPolisi",
	"mDataProp": "nomorPolisi",
	"aTargets": [2],
	"bSortable": false,
	"bSearchable": false,
	"bVisible": true,
    "sClass": "noPolisi"
},

{
	"sName": "namaPelanggan",
	"mDataProp": "namaPelanggan",
	"aTargets": [3],
	"bSortable": false,
	"bSearchable": false,
	"bVisible": true,
    "sClass": "namaPelanggan"
},

{
    "sName": "vendor",
    "mDataProp": "vendor",
    "aTargets": [4],
    "bSortable": false,
    "bSearchable": false,
	"bVisible": true,
    "sClass": "namaBengkel"

},
{
    "sName": "harga",
    "mDataProp": "harga",
    "aTargets": [5],
    "bSortable": false,
    "bSearchable": false,
	"bVisible": true,
    "sClass": "harga"
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var kode = $('#filter_noInvSublet input').val();

						if(kode){
							aoData.push(
									{"name": 'sCriteria_nomorPOSublet', "value": kode}
							);
						}

						var nowo = $('#filter_nomorWo input').val();

						if(nowo){
							aoData.push(
									{"name": 'sCriteria_noWo', "value": nowo}
							);
						}

						var nopol = $('#filter_nomorPolisi input').val();

						if(nopol){
							aoData.push(
									{"name": 'sCriteria_nopol', "value": nopol}
							);
						}

						var cust = $('#filter_namaCustomer input').val();

						if(cust){
							aoData.push(
									{"name": 'sCriteria_namaCustomer', "value": cust}
							);
						}

						var bengkel = $('#filter_namaBengkel input').val();

						if(bengkel){
							aoData.push(
									{"name": 'sCriteria_namaBengkel', "value": bengkel}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});

    $('#nomorSublet_table tbody tr').live('click', function () {
        $(this).find('.radio-select').attr("checked",true);
        var noSublet      = $(this).find("td.noSublet span").html();
        var noWo          = $(this).find("td.noWo").html();
        var noPolisi      = $(this).find("td.noPolisi").html();
        var namaPelanggan = $(this).find("td.namaPelanggan").html();
        var namaBengkel   = $(this).find("td.namaBengkel").html();

        $("#noSublet_").val(noSublet);
        $("#nomorWo_").val(noWo);
        $("#nomorPolisi_").val(noPolisi);
        $("#pelanggan_").val(namaPelanggan);
        $("#bengkel_").val(namaBengkel);
    });

});
</g:javascript>



