<%--
  Created by IntelliJ IDEA.
  User: Ahmad Fawaz
  Date: 24/02/15
  Time: 13:11
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'transaction.label', default: 'Hasil Penjualan Barang Langsung')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <style>
    .form-horizontal .form-group {
        margin-bottom: 5px;
    }
    body .modal {
        width: 860px;
        margin-left: -280px;
    }
    </style>
    <r:require modules="baseapplayout, baseapplist"/>
    <g:javascript>
        var showModalPartSales;
        var tagihan = 0
        $(function() {
            showModalPartSales = function() {
                $("#nomorDeliveriOrderModal").modal({
                        "backdrop" : "dynamic",
                        "keyboard" : true,
                        "show" : true
                    }).css({'width': '900px','margin-left': function () {return -($(this).width() / 2);}});
            }

            $("input[type='radio'][name='tipeTransaksi']").click(function() {
                var tipe = $(this).val();

                $("#tanggalTransfer").removeAttr("readonly");
                $("#namaBank").removeAttr("readonly");
                $("#nomorPDCKombos").removeAttr("readonly");
                $("#nomorPDCKombos").removeAttr("readonly");
                $("#tanggalJatuhTempoPDCKombos").removeAttr("readonly");
                 $("#namaBank").val("");
                $("#tanggalTransfer").val("");
                $("#nomorPDCKombos").val("");
                $("#tanggalJatuhTempoPDCKombos").val("");
                if (tipe === "KAS") {
                    $("#namaBank").attr("disabled", true);
                    $("#tanggalTransfer").attr("disabled", true);
                    $("#nomorPDCKombos").attr("readonly", "readonly");
                    $("#tanggalJatuhTempoPDCKombos").attr("readonly", "readonly");
                } else if (tipe === "BANK") {
                    $("#namaBank").attr("disabled", false);
                    $("#tanggalTransfer").attr("disabled", false);
                    $("#nomorPDCKombos").attr("readonly", "readonly");
                    $("#tanggalJatuhTempoPDCKombos").attr("readonly", "readonly");
                } else if (tipe === "PDC") {
                    $("#tanggalTransfer").attr("readonly", "readonly");
                }
            });

            $("#btnSave").click(function() {
                var tanggalTransaksi = $("#tanggalTransaksi").val();
                var tipeTransaksi    = $("input[type='radio'][name='tipeTransaksi']:checked").val();
                var bank             = $("#namaBank").val();
                var tanggalTransfer  = $("#tanggalTransfer").val();
                var keterangan       = $("#keterangan").val();
                var noKwitansi       = $("#nomorDO").val();
                var jmlPembayaran    = $("#jumlahPembayaran").val();
                var sisaHarusBayar   = $("#sisaHarusBayar").val();
                var sebelumPpn       = $("#sebelumPpn").val();
                var doSave = true;
                if (tanggalTransaksi && noKwitansi && tipeTransaksi) {
                    if(tipeTransaksi=="BANK" && (bank=="" || tanggalTransfer=="")){
                        doSave = false;
                    }
                }  else {
                    doSave = false;
                }

                if(jmlPembayaran!=sisaHarusBayar){
                    alert("Jumlah pembayaran harus sama dengan jumlah yang harus di bayar.\nTidak menerima partial payment")
                    return
                }
                if(jmlPembayaran==0){
                    alert("Jumlah Pembayaran tidak Boleh NOL");
                    return
                }

                if(doSave){
                    var confirmS = confirm("Apakah Anda yakin?");
                    if(confirmS){
                        $.post("${request.getContextPath()}/transaction/saveTransaction", {
                            transactionDate: tanggalTransaksi,
                            transactionType: tipeTransaksi,
                            bank: bank,
                            tipeBeli: $("#tipeBeli").val(),
                            transferDate: tanggalTransfer,
                            description: keterangan,
                            docNumber: noKwitansi,
                            amount: jmlPembayaran,
                            documentCategory: "DOCSO",
                            typeJournal : 'TAGIHAN',
                            isPenjualan : true,
                            sebelumPpn : sebelumPpn
                        }, function(data) {
                            if (data.error) {
                                alert(data.error);
                            } else {
                                alert(data.success);
                                $("#idTrx").val(data.transactionInstance.id);
                                $("#btnReset").show();
                                $("#btnCetak").show();
                            }
                        });
                    }
                }else{
                    alert("mohon lengkapi field terlebih dahulu");
                }
            })

            $("#btnPilih").click(function() {
                $("#nomorDO").val($("#noDO").val());
                $("#nomorSO").val($("#noSO").val());
                $("#pembeli").val($("#buyer").val());

                $("#noDO").val();
                $("#noSO").val();
                $("#buyer").val();


                $("#nomorDeliveriOrderModal").modal("hide");

                var nomorDO = $("#nomorDO").val();
                var buyer      = $("#pembeli").val();

                $("#keterangan").val("Penerimaan Hasil Penjualan Barang, No. DO " + nomorDO + ", atas nama " + buyer);

                // get transaction by doc category
                $.get("${request.getContextPath()}/transaction/getTransactionPartsByDocNumber", {docNumber: nomorDO}, function(data) {
                    var sdhBayar = data.sudahBayar;
                    var jmlTagih = data.tagihan;
                    $("#tipeBeli").val(data.tipe);
                    $("#jumlahTagihan").val(jmlTagih);
                    $("#jumlahSudahBayar").val(sdhBayar);
                    $("#sisaHarusBayar").val(jmlTagih - sdhBayar);
                    $("#sebelumPpn").val(data.sebelumPpn);
                });
            })
        });

        function doPrint(){
            var idTrx = $("#idTrx").val();
            var idDO = $("#nomorDO").val();
            window.location = "${request.contextPath}/transaction/printKW?idTrx="+idTrx+"&noDO="+idDO;
        }
    </g:javascript>
</head>

<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]"/></span>
    <ul class="nav pull-right">

    </ul>
</div>

<body>
<div class="box">
    <div class="span12">

        <fieldset class="form-horizontal">
            <div class="span6">
                <div class="form-group">
                    <label class="control-label" for="nomorDO">
                        Nomor Delivery Order
                    </label>

                    <div class="controls">
                        <input class="span6" id="nomorDO" type="text" readonly/>
                        <div class="input-append">
                            <button type="submit" class="btn" onclick="showModalPartSales();">
                                <i class="icon-search"></i>
                            </button>
                            <g:hiddenField name="idTrx" id="idTrx" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label" for="nomorSO">
                        Nomor Sales Order
                    </label>

                    <div class="controls">
                        <input type="text" readonly id="nomorSO">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label" for="pembeli">
                        Nama Pembeli
                    </label>

                    <div class="controls">
                        <input type="text" readonly id="pembeli">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label" for="tipeBeli">
                        Tipe Pembelian
                    </label>

                    <div class="controls">
                        <input type="text" readonly id="tipeBeli">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label" for="tanggalTransaksi">
                        Tanggal Transaksi
                    </label>

                    <div class="controls">
                        <ba:datePicker format="dd/mm/yyyy" name="tanggalTransaksi"/>
                    </div>
                </div><!-- Tanggal Transaksi -->

                <div class="form-group">
                    <label class="control-label" for="tipeTransaksi">
                        Tipe Transaksi
                    </label>

                    <div class="controls">
                        <input type="radio" name="tipeTransaksi" id="tipeTransaksi" value="KAS">KAS
                        <input type="radio" name="tipeTransaksi" value="BANK">BANK
                    </div>
                </div><!-- Tipe Transaksi -->

            </div>

            <div class="span6">
                <div class="form-group">
                    <label class="control-label" for="namaBank">
                        Nama Bank
                    </label>

                    <div class="controls">
                        <g:select name="namaBank" from="${com.kombos.finance.BankAccountNumber.createCriteria().list {eq("companyDealer", session.userCompanyDealer);eq("staDel","0");bank{order("m702NamaBank")}}}" id="namaBank" optionValue="${{it.bank.m702NamaBank+" "+it.bank.m702Cabang}}" optionKey="id"
                                  noSelection="['':'']"/>
                    </div>
                </div> <!-- NAMA BANK -->

                <div class="form-group">
                    <label class="control-label" for="tanggalTransfer">
                        Tanggal Transfer
                    </label>

                    <div class="controls">
                        <ba:datePicker name="tanggalTransfer" format="dd/mm/yyyy"/>
                    </div>
                </div> <!-- Tanggal Transfer -->

                <div class="form-group">
                    <label class="control-label" for="jumlahTagihan">
                        Tagihan ke Pelanggan
                    </label>

                    <div class="controls">
                        <input type="text" id="jumlahTagihan" readonly>
                    </div>
                </div> <!-- Jumlah Tagihan -->

                <div class="form-group">
                    <label class="control-label" for="jumlahsudahbayar">
                        Jumlah yang Sudah diBayar
                    </label>

                    <div class="controls">
                        <input type="text" id="jumlahSudahBayar" readonly>
                    </div>
                </div> <!-- Jumlah yang Sudah diBayar -->

                <div class="form-group">
                    <label class="control-label" for="sisaHarusBayar">
                        Jumlah yg Harus diBayar
                    </label>

                    <div class="controls">
                        <input type="text" id="sisaHarusBayar" readonly>
                        <input type="hidden" id="sebelumPpn" />
                    </div>
                </div> <!-- Sisa yg Harus diBayar -->

                <div class="form-group">
                    <label class="control-label" for="jumlahPembayaran">
                        Jumlah Pembayaran
                    </label>

                    <div class="controls">
                        <input type="text" id="jumlahPembayaran">
                    </div>
                </div> <!-- Jumlah Pembayaran -->

                <div class="form-group">
                    <label class="control-label" for="keterangan">
                        Keterangan
                    </label>

                    <div class="controls">
                        <g:textArea name="keterangan" rows="5" cols="5"/>
                    </div>
                </div>
            </div>
        </fieldset>

        <div class="span12">
            <a href="javascript:void(0);" id="btnSave" class="btn btn-primary">Simpan</a>
            <button id="btnReset" style="display: none" class="btn btn-default" onclick="loadPath('transaction/penerimaanPenjualanParts');">Reset Form</button>
            <button id="btnCetak" style="display: none" onclick="doPrint();" class="btn btn-primary">Cetak Kwitansi</button>
        </div>
    </div>
</div>

<div id="nomorDeliveriOrderModal" class="modal hide">
    <div class="modal-content">
        <div class="modal-body">
            <g:render template="nomorDeliveryOrder"/>
        </div>

        <div class="modal-footer">
            <a class="btn btn-primary" id="btnPilih">Pilih</a>
        </div>
    </div>
</div>


</body>
</html>