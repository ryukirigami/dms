<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title>Transaction Transaksi</title>
    <r:require modules="baseapplayout" />
    <g:render template="../menu/maxLineDisplay"/>
    <g:javascript>
        $(function() {

            var checkin = $('#filter_tanggalTransaction_from').datepicker({
                    onRender: function(date) {
                        return '';
                    }
                }).on('changeDate', function(ev) {
                            var newDate = new Date(ev.date)
                            newDate.setDate(newDate.getDate());
                            checkout.setValue(newDate);
                            checkin.hide();
                            $('#filter_tanggalTransaction_to')[0].focus();
                        }).data('datepicker');

                var checkout = $('#filter_tanggalTransaction_to').datepicker({
                    onRender: function(date) {
                        return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
                    }
                }).on('changeDate', function(ev) {
                            checkout.hide();
                        }).data('datepicker');



        });


    </g:javascript>
</head>

<body>
<div class="navbar box-header no-border">
    <span id="title" class="pull-left">Cari Transaksi List</span>
</div><br>
<div class="box">
    <div class="span12" id="transaction-table">
        <div class="row-fluid">
            <div class="span9">
                <div id="search-form" class="form-horizontal">
                    <fieldset class="form">
                        <div class="control-group fieldcontain">
                            <label for="transactionCode" class="control-label">
                                Kode Transaksi
                            </label>
                            <div class="controls">
                                <input type="text" name="transactionCode" id="transactionCode" style="width: 300px;" value="${params.dari != null ? params.sCriteria_transactionCode : ''}" autocomplete="off"/>
                            </div>
                        </div>
                        <div class="control-group fieldcontain">
                            <label for="transactionCode" class="control-label">
                                Tanggal Transaksi
                            </label>

                            <div class="controls">
                                <ba:datePicker id="filter_tanggalTransaction_from" name="filter_tanggalTransaction_from" precision="day" format="dd/MM/yyyy"  value="${params.dari != null ? params.sCriteria_transactionDate_dp: ''}" />
                                &nbsp;&nbsp;&nbsp;s.d.&nbsp;&nbsp;&nbsp;
                                <ba:datePicker id="filter_tanggalTransaction_to" name="filter_tanggalTransaction_to" precision="day" format="dd/MM/yyyy"  value="${params.dari != null ? params.sCriteria_transactionDate_dp_to: ''}" />
                            </div>
                        </div>

                        <div class="control-group fieldcontain">
                            <div class="controls">
                                <button onclick="findData();" class="btn btn-primary">Cari Transaksi</button>
                                <button onclick="resetData();" class="btn btn-cancel">Reset Data</button>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div><br>
    <!-- END SPAN12 -->
    <div class="span12" id="transaction-dataTable" style="margin-left: -5px; margin-top: 20px;">
        <table id="transaction_datatables" cellpadding="0" cellspacing="0"
               border="0"
               class="display table table-striped table-bordered table-hover"
               width="100%">
            <!-- DataTables -->
            <thead>
            <tr>
                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="transaction.transactionCode.label" default="Kode Transaksi" /></div>
                </th>
                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="transaction.transactionDate.label" default="Tanggal Transaksi" /></div>
                </th>

                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="transaction.tipe.label" default="Tipe Transaksi" /></div>
                </th>


                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="transaction.jumlah.label" default="Jumlah" /></div>
                </th>

                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="transaction.kategori.label" default="Kategori Dokumen" /></div>
                </th>

                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="transaction.nomorDOkumen.label" default="Nomor Dokumen" /></div>
                </th>

                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="transaction.deskripsi.label" default="Deskripsi Dokumen" /></div>
                </th>

                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="transaction.Status.label" default="Status" /></div>
                </th>

            </tr>

            <tr>

                <th style="border-top: none;padding: 5px;">
                </th>

                <th style="border-top: none;padding: 5px;">
                </th>

                <th style="border-top: none;padding: 5px;">
                    <div id="filter_tipe" style="padding-top: 0px;position:relative; margin-top: 0px;width: 80px;">
                        <input type="text" name="search_tipe" class="search_init" value="${params.dari != null ? params.sCriteria_tipe : ''}" />
                    </div>
                </th>

                <th style="border-top: none;padding: 5px;">
                    <div id="filter_total" style="padding-top: 0px;position:relative; margin-top: 0px;width: 145px;">
                        <input type="text" name="search_total" class="search_init" value="${params.dari != null ? params.sCriteria_total : ''}" />
                    </div>
                </th>

                <th style="border-top: none;padding: 5px;">
                    <div id="filter_kategori" style="padding-top: 0px;position:relative; margin-top: 0px;width: 185px;">
                        <input type="text" name="search_kategori" class="search_init" value="${params.dari != null ? params.sCriteria_kategori : ''}" />
                    </div>
                </th>

                <th style="border-top: none;padding: 5px;">
                    <div id="filter_docNumber" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                        <input type="text" name="search_docNumber" class="search_init" value="${params.dari != null ? params.sCriteria_docNumber : ''}" />
                    </div>
                </th>

                <th style="border-top: none;padding: 5px;">
                    <div id="filter_deskripsi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 255px;">
                        <input type="text" name="search_deskripsi" class="search_init" value="${params.dari != null ? params.sCriteria_deskripsi : ''}" />
                    </div>
                </th>

                <th style="border-top: none;padding: 5px;">
                </th>

            </tr>

            <g:if test="${htmlData}">
                ${htmlData}
            </g:if>
            </thead>
        </table>
    </div>
    <div class="span12" id="transaction-form" style="display: none;margin-left: 0px;border: none;"></div>
    <div>Jumlah : ${params.jumlahdata}</div><br>
</div>

<g:javascript>
var closeWindowDetail;
var showDetail;
$(function(){

    $("div input").bind('keypress', function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code == 13) {
        e.stopPropagation();
            findData();
        }
    });

    resetData = function() {
        toastr.success("Mohon Tunggu..");
        loadPath("transaction/cariTransaksi");
    }

    findData = function() {
            ${htmlData = ""};

            var url = "";

            var transactionCode = $('#transactionCode').val();

            if (transactionCode) {
                    url += "&sCriteria_transactionCode="+transactionCode;
            }

            var transactionDate = $('#filter_tanggalTransaction_from').val();

            if(transactionDate){
                url += "&sCriteria_transactionDate_dp="+transactionDate;
            }

            var transactionDateTo = $('#filter_tanggalTransaction_to').val();

            if (transactionDateTo) {
                    url += "&sCriteria_transactionDate_dp_to="+transactionDateTo;
            }

            var tipeFilter = $('#filter_tipe input').val();
            if(tipeFilter){
                url += "&sCriteria_tipe="+tipeFilter;
            }

            var totalFilter = $('#filter_total input').val();
            if(totalFilter){
                url += "&sCriteria_total="+totalFilter;
            }

            var kategoriFilter = $('#filter_kategori input').val();
            if(kategoriFilter){
                url += "&sCriteria_kategori="+kategoriFilter;
            }

            var docNumberFilter = $('#filter_docNumber input').val();
            if(docNumberFilter){
                url += "&sCriteria_docNumber="+docNumberFilter;
            }

            var deskripsiFilter = $('#filter_deskripsi input').val();
            if(deskripsiFilter){
                url += "&sCriteria_deskripsi="+deskripsiFilter;
            }



            if (url) {
                toastr.success("Mohon Tunggu Sebentar..");
                loadPath("transaction/cariTransaksi?dari=cari"+url);
            }  else {
                alert("Mohon lengkapi field parameter pencarian terlebih dahulu");
                toastr.error("Data Belum Lengkap..");
            }
    }

    showDetail = function(id){
        $("#detailContent").empty();
        $.ajax({
            type:'POST',
            url:'${request.contextPath}/transaction/showTransactionDetail',
            data : {id : id},
            success:function(data,textStatus){
                    $("#detailContent").html(data);
                    $("#detailModal").modal({
                        "backdrop" : "dynamic",
                        "keyboard" : true,
                        "show" : true
                    }).css({'width': '1100px','margin-left': function () {return -($(this).width() / 2);}});

            },
            error:function(XMLHttpRequest,textStatus,errorThrown){},
            complete:function(XMLHttpRequest,textStatus){
                $('#spinner').fadeOut();
            }
        });
    }

    closeWindowDetail = function() {
        $("#detailModal").hide();
    }

});

</g:javascript>
<div id="detailModal" class="modal fade">
    <div class="modal-dialog" style="width: 1090px;">
        <div class="modal-content" style="width: 1090px;">
            <div class="modal-body" style="max-height: 450px;">
                <div id="detailContent"/>
                <div class="iu-content"></div>

            </div>
        </div>
    </div>
</div>

</body>

</html>