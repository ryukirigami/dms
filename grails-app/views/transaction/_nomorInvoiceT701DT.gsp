<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<legend>Daftar Penerimaan Hasil Tagihan Service Invoice</legend>
<table id="detail-table" class="table table-bordered">

<input type="hidden" id="noInv">
<input type="hidden" id="noPol">
<input type="hidden" id="customer">

<table id="nomorInvoiceT701_table" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover fixed " style="table-layout: fixed; ">
    <col width="300px" />
    <col width="200px" />
    <col width="300px" />
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px; width:289px;">
            Nomor Invoice
        </th>
        <th style="border-bottom: none;padding: 5px; width:189px;">
            Nomor Polisi
        </th>
        <th style="border-bottom: none;padding: 5px; width:289px;">
            Pelanggan
        </th>
    </tr>

    <tr>

        <th style="border-top: none;padding: 5px; width:289px;">
            <div id="filter_noInv" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="noInv" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px; width:189px;">
            <div id="filter_noPol" style="padding-top: 0px;position:relative; margin-top: 0px;width: 200px;">
                <input type="text" name="noPol" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px; width:289px;">
            <div id="filter_customer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="customer" class="search_init" />
            </div>
        </th>

    </tr>

    </thead>
</table>
<g:javascript>
var nomorInvoiceT701Table;
var reloadNomorInvoiceT701Table;
var setValues;
$(function(){

    setValues = function(el) {
        var noInv  = $(el).parent().parent().find("td.noInv span").html();
        var noPol  = $(el).parent().parent().find("td.noPol").html();
        var cust   = $(el).parent().parent().find("td.customer").html();

        $("#noInv").val(noInv);
        $("#noPol").val(noPol);
        $("#customer").val(cust);

    }

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	nomorInvoiceT701Table.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});


        nomorInvoiceT701Table = $('#nomorInvoiceT701_table').dataTable({
		"sScrollX": "800px",
		"bScrollCollapse": true,
		"bAutoWidth" : true,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : 5, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "nomorInvoiceT701DT")}",
		"aoColumns": [

{
	"sName": "t701NoInv",
	"mDataProp": "t701NoInv",
	"aTargets": [0],
	"bSortable": false,
	"bSearchable": true,
	"sClass": "noInv",
	"bVisible": true,
	"mRender": function ( data, type, row ) {
	    return "<input type='radio' onclick='setValues(this);' name='sublet' class='radio-select'><span class='noInv'>"+data+"</span>";
	}
},
{
    "sName": "t701Nopol",
	"mDataProp": "t701Nopol",
	"aTargets": [1],
	"bSortable": false,
	"bSearchable": true,
	"sClass": "noPol",
	"bVisible": true
},
{
    "sName": "T701Customer",
	"mDataProp": "T701Customer",
	"aTargets": [2],
	"bSortable": false,
	"bSearchable": true,
	"sClass": "customer",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var kode  = $('#filter_noInv input').val();
						var nopol = $("#filter_noPol input").val();
						var cust  = $("#filter_customer input").val();

						if(kode){
							aoData.push(
									{"name": 'sCriteria_noInv', "value": kode}
							);
						}

						if (nopol) {
						    aoData.push(
									{"name": 'sCriteria_noPol', "value": nopol}
							);
						}

                        if (cust) {
                            aoData.push(
									{"name": 'sCriteria_customer', "value": cust}
							);
                        }

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});

    $('#nomorInvoiceT701_table tbody tr').live('click', function () {
        $(this).find('.radio-select').attr("checked",true);
        var noInv  = $(this).find("td.noInv span").html();
        var noPol  = $(this).find("td.noPol").html();
        var cust   = $(this).find("td.customer").html();
        $("#noInv").val(noInv);
        $("#noPol").val(noPol);
        $("#customer").val(cust);
    });

});
</g:javascript>
