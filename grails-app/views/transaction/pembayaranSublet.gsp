<!-- Transaction/PembayaranSublet -->

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'transaction.label', default: 'Pembayaran Sublet')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <style>
    .form-horizontal .form-group {
        margin-bottom: 5px;
    }
    </style>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>
        var nomorPOSubletDT;

        $(function() {
            nomorSubletDT= function() {
//                $("#nomorPOSubletDT").modal("show");
                $("#nomorPOSubletDT").modal({
                        "backdrop" : "dynamic",
                        "keyboard" : true,
                        "show" : true
                    }).css({'width': '900px','margin-left': function () {return -($(this).width() / 2);}});
            }


            $("input[type='radio'][name='tipeTransaksi']").click(function() {
                var tipe = $(this).val();

                $("#tanggalTransfer").removeAttr("readonly");
                $("#namaBank").removeAttr("readonly");
                $("#nomorPDCKombos").removeAttr("readonly");
                $("#tanggalJatuhTempoPDCKombos").removeAttr("readonly");

                $("#namaBank").val("");
                $("#tanggalTransfer").val("");
                $("#nomorPDCKombos").val("");
                $("#tanggalJatuhTempoPDCKombos").val("");
                if (tipe === "KAS") {
                    $("#namaBank").attr("disabled", true);
                    $("#tanggalTransfer").attr("disabled", true);
                    $("#nomorPDCKombos").attr("readonly", "readonly");
                    $("#tanggalJatuhTempoPDCKombos").attr("readonly", "readonly");
                } else if (tipe === "BANK") {
                    $("#namaBank").attr("disabled", false);
                    $("#tanggalTransfer").attr("disabled", false);
                    $("#nomorPDCKombos").attr("readonly", "readonly");
                    $("#tanggalJatuhTempoPDCKombos").attr("readonly", "readonly");
                } else if (tipe === "PDC") {
                    $("#tanggalTransfer").attr("disabled", false);
                }
            });

            $("#btnPilih").click(function() {
                $("#nomorSublet").val($("#noSublet_").val());
                $("#nomorWO").val($("#nomorWo_").val());
                $("#nomorPolisi").val($("#nomorPolisi_").val());
                $("#pelanggan").val($("#pelanggan_").val());
                $("#namaBengkel").val($("#bengkel_").val());

                $("#nomorPOSubletDT").modal("hide");
                $("#keterangan").val("Pembayaran Sublet, Dengan No. Invoice "+$("#nomorSublet").val()+" untuk WO "+$("#nomorWO").val() +" atas nama " + $("#pelanggan").val());

                $.ajax({
                    async: false,
                    url: "${request.getContextPath()}/transaction/getSubletJumlahBayar",
                    type: "GET",
                    data: {nomorInvoice: $("#nomorSublet").val()},
                    dataType: "json",
                    success: function(data) {
                        if (!data.error) {
                            $("#jumlahTagihan").val(data.val);
                        } else {
                            alert(data.error);
                        }
                    }
                })

                $.get("${request.getContextPath()}/transaction/getSubletJumlahBayar", {nomorInvoice: $("#nomorSublet").val()}, function(data) {
                    if (!data.error) {
                        $("#jumlahTagihan").val(data.val);
                    } else {
                        alert(data.error);
                    }
                })

            })

            $("#btnSave").click(function() {
                var tanggalTransaksi = $("#tanggalTransaksi").val();
                var tipeTransaksi    = $("input[type='radio'][name='tipeTransaksi']:checked").val();
                var bank             = $("#namaBank").val();
                var tanggalTransfer  = $("#tanggalTransfer").val();
                var noPDCKombos      = $("#nomorPDCKombos").val();
                var tglJatuhTempo    = $("#tanggalJatuhTempoPDCKombos").val();
                var keterangan       = $("#keterangan").val();
                var noSublet         = $("#nomorSublet").val();
                var jmlPembayaran    = $("#jumlahPembayaran").val();

                if (tanggalTransaksi && noSublet && tipeTransaksi) {
                    $.post("${request.getContextPath()}/transaction/saveByDocCategory", {
                        transactionDate: tanggalTransaksi,
                        transactionType: tipeTransaksi,
                        bank: bank,
                        transferDate: tanggalTransfer,
                        dueDate: tglJatuhTempo,
                        cekNumber: noPDCKombos,
                        description: keterangan,
                        docNumber: noSublet,
                        amount: jmlPembayaran,
                        documentCategory: "DOCSB",
                        typeJournal: "SUBLET"
                    }, function(data) {
                        if (data.error) {
                            alert(data.error);
                        } else {
                            alert(data.success);
                        }
                    });
                } else {
                    alert("mohon lengkapi field terlebih dahulu");
                }
            });
        });
    </g:javascript>
</head>

<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]"/></span>
    <ul class="nav pull-right">

    </ul>
</div>
    <div class="box">
        <div class="span12">

                <fieldset class="form-horizontal">
                    <div class="span6">
                        <div class="form-group">
                            <label class="control-label" for="nomorSublet">
                                Nomor Invoice
                            </label>
                            <div class="controls">
                                <input class="span6" id="nomorSublet" type="text" readonly/>
                                <div class="input-append">
                                    <button type="submit" class="btn" onclick="nomorSubletDT();">
                                        <i class="icon-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div> <!-- NOMOR SUBLET -->

                        <div class="form-group">
                            <label class="control-label" for="namaBengkel">
                                Nama Bengkel
                            </label>
                            <div class="controls">
                                <input type="text" readonly id="namaBengkel">
                            </div>
                        </div> <!-- NAMA BENGKEL -->

                        <div class="form-group">
                            <label class="control-label" for="nomorWO">
                                Nomor WO
                            </label>
                            <div class="controls">
                                <input type="text" readonly id="nomorWO">
                            </div>
                        </div><!-- NOMOR WO -->

                        <div class="form-group">
                            <label class="control-label" for="nomorPolisi">
                                Nomor Polisi
                            </label>
                            <div class="controls">
                                <input type="text" readonly id="nomorPolisi">
                            </div>
                        </div><!-- NOMOR POLISI -->

                        <div class="form-group">
                            <label class="control-label" for="pelanggan">
                                Pelanggan
                            </label>
                            <div class="controls">
                                <input type="text" readonly id="pelanggan">
                            </div>
                        </div><!-- PELANGGAN -->

                        <div class="form-group">
                            <label class="control-label" for="tanggalTransaksi">
                                Tanggal Transaksi
                            </label>
                            <div class="controls">
                                <ba:datePicker format="dd/mm/yyyy" name="tanggalTransaksi" />
                            </div>
                        </div><!-- Tanggal Transaksi -->

                        <div class="form-group">
                            <label class="control-label" for="tipeTransaksi">
                                Tipe Transaksi
                            </label>
                            <div class="controls">
                                <input type="radio" name="tipeTransaksi" id="tipeTransaksi" value="KAS">KAS
                                <input type="radio" name="tipeTransaksi" value="BANK">BANK
                                <input type="radio" name="tipeTransaksi" value="PDC">PDC
                            </div>
                        </div><!-- Tipe Transaksi -->

                    </div>
                    <div class="span6">
                        <div class="form-group">
                            <label class="control-label" for="namaBank">
                                Nama Bank
                            </label>
                            <div class="controls">
                                <g:select name="namaBank" from="${com.kombos.finance.BankAccountNumber.createCriteria().list {eq("companyDealer", session.userCompanyDealer);eq("staDel","0");bank{order("m702NamaBank")}}}" id="namaBank" optionValue="${{it.bank.m702NamaBank+" "+it.bank.m702Cabang}}" optionKey="id"
                                          noSelection="['':'']"/>
                            </div>
                        </div> <!-- NAMA BANK -->

                        <div class="form-group">
                            <label class="control-label" for="tanggalTransfer">
                                Tanggal Transfer
                            </label>
                            <div class="controls">
                                <ba:datePicker name="tanggalTransfer" format="dd/mm/yyyy"/>
                            </div>
                        </div> <!-- Tanggal Transfer -->

                        <div class="form-group">
                            <label class="control-label" for="nomorPDCKombos">
                                Nomor PDC Kombos
                            </label>
                            <div class="controls">
                                <input type="text" id="nomorPDCKombos">
                            </div>
                        </div> <!-- Nomor PDC Kombos -->

                        <div class="form-group">
                            <label class="control-label" for="tanggalJatuhTempoPDCKombos">
                                Tgl Jth Tempo PDC Kombos
                            </label>
                            <div class="controls">
                                <ba:datePicker format="dd/mm/yyyy" name="tanggalJatuhTempoPDCKombos" />
                            </div>
                        </div> <!-- Tanggal Jatuh Tempo PDC Kombos -->

                        <div class="form-group">
                            <label class="control-label" for="jumlahTagihan">
                                Jumlah Tagihan
                            </label>
                            <div class="controls">
                                <input type="text" id="jumlahTagihan" readonly>
                            </div>
                        </div> <!-- Jumlah Tagihan -->
                    <!-- Sisa yg Harus diBayar -->

                        <div class="form-group">
                            <label class="control-label" for="jumlahPembayaran">
                                Jumlah Pembayaran
                            </label>
                            <div class="controls">
                                <input type="text" id="jumlahPembayaran">
                            </div>
                        </div> <!-- Jumlah Pembayaran -->

                        <div class="form-group">
                            <label class="control-label" for="keterangan">
                                Keterangan
                            </label>
                            <div class="controls">
                                <g:textArea name="keterangan" rows="5" cols="5"/>
                            </div>
                        </div>
                    </div>
                </fieldset>

            <div class="span12">
                <a href="javascript:void(0);" id="btnSave" class="btn btn-primary">Simpan</a>
            </div>
        </div>
    </div>

    <div id="nomorPOSubletDT" class="modal hide">
        <div class="modal-content">
            <div class="modal-body">
                <g:render template="nomorSubletDT"/>
            </div>
            <div class="modal-footer">
                <a class="btn btn-primary" id="btnPilih">Pilih</a>
            </div>
        </div>
    </div> <!-- SUBLET -->


</body>
</html>