<%--
  Created by IntelliJ IDEA.
  User: DENDYSWARAN
  Date: 5/20/14
  Time: 3:02 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'transaction.label', default: 'Pengeluaran Pembelian Parts / Bahan')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <style>
    .form-horizontal .form-group {
        margin-bottom: 5px;
    }
    </style>
    <r:require modules="baseapplayout, baseapplist"/>
    <g:javascript>
        var showModalKwitansi;
        function convertStringToMoney(val){
            var nilai = 0
            var re = new RegExp(',','g')
            var str = val.replace(re,'')
            nilai = new Number(str)
            return nilai
        }
        $(".numeric").autoNumeric("init", {
            vMin: '-999999999.99',
            vMax: '99999999999.99'
        });
        Number.prototype.format = function(n, x) {
            var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
            return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
        };
        $(function() {
            showModalKwitansi = function() {
                $("#noReffDTModal").modal({
                        "backdrop" : "dynamic",
                        "keyboard" : true,
                        "show" : true
                    }).css({'width': '1100px','margin-left': function () {return -($(this).width() / 2);}});
            }

            $("input[type='radio'][name='tipeTransaksi']").click(function() {
                var tipe = $(this).val();

                $("#tanggalTransfer").removeAttr("readonly");
                $("#namaBank").removeAttr("readonly");
                $("#nomorPDCKombos").removeAttr("readonly");
                $("#tanggalJatuhTempoPDCKombos").removeAttr("readonly");

                $("#namaBank").val("");
                $("#tanggalTransfer").val("");
                $("#nomorPDCKombos").val("");
                $("#tanggalJatuhTempoPDCKombos").val("");
                if (tipe === "KAS") {
                    $("#namaBank").attr("disabled", true);
                    $("#tanggalTransfer").attr("disabled", true);
                    $("#nomorPDCKombos").attr("readonly", "readonly");
                    $("#tanggalJatuhTempoPDCKombos").attr("readonly", "readonly");
                } else if (tipe === "BANK") {
                    $("#namaBank").attr("disabled", false);
                    $("#tanggalTransfer").attr("disabled", false);
                    $("#nomorPDCKombos").attr("readonly", "readonly");
                    $("#tanggalJatuhTempoPDCKombos").attr("readonly", "readonly");
                } else if (tipe === "PDC") {
                    $("#tanggalTransfer").attr("disabled", false);

                }
            });

            $("#btnSave").click(function() {
                var tanggalTransaksi = $("#tanggalTransaksi").val();
                var tipeTransaksi    = $("input[type='radio'][name='tipeTransaksi']:checked").val();
                var bank             = $("#namaBank").val();
                var tanggalTransfer  = $("#tanggalTransfer").val();
                var noPDCKombos      = $("#nomorPDCKombos").val();
                var tglReff          = $("#tglReff").val();
                var tglJatuhTempo    = $("#tanggalJatuhTempoPDCKombos").val();
                var keterangan       = $("#keterangan").val();
                var noReff       = $("#noReff").val();
                var jmlPembayaran    = convertStringToMoney($("#jumlahPembayaran").val());
                var jmlHarusBayar    = convertStringToMoney($("#sisaHarusBayar").val());

                if (tanggalTransaksi && noReff && tipeTransaksi) {
                    if(jmlHarusBayar < jmlPembayaran  ){
                        alert('Pembayaran Harus Lebih Kecil dari yang harus dibayar');
                    }else{
                        $.post("${request.getContextPath()}/transaction/saveByDocCategory", {
                        transactionDate: tanggalTransaksi,
                        transactionType: tipeTransaksi,
                        bank: bank,
                        transferDate: tanggalTransfer,
                        dueDate: tglJatuhTempo,
                        cekNumber: noPDCKombos,
                        description: keterangan,
                        docNumber: noReff,
                        tgglInv: tglReff,
                        amount: jmlPembayaran,
                        documentCategory: "DOCBU",
                        typeJournal: "HUTANG"
                        }, function(data) {
                            if (data.error) {
                                alert(data.error);
                            } else {
                                alert(data.success);
                            }
                        });
                    }
                } else {
                    alert("mohon lengkapi field terlebih dahulu");
                }
            });

            $("#btnPilih").click(function() {
                $("#noReff").val($("#noReff").val());
                $("#idReff").val($("#idReff").val());
                $("#tglReff").val($("#tglReff").val());
                $("#supplier").val($("#vendor").val());
                $("#jumlahTagihan").val($("#tagihan").val());
                $("#noReff").val();
                $("#tglReff").val();
                $("#vendor").val();
                $("#tagihan").val();

                $("#noReffDTModal").modal("hide");

                var noReff = $("#noReff").val();
                var idReff = $("#idReff").val();
                var tipeTransaksi = "KWITANSI";
                var supplier      = $("#supplier").val();

                $("#keterangan").val("Pengeluaran Pembayaran Hutang " + noReff + " atas nama PT " +supplier);

                $.get("${request.getContextPath()}/transaction/getTransactionByDocNumber", {docNumber: noReff, docCategory: tipeTransaksi,dari : "pembayaranHutang"}, function(data) {
                    var sdhBayar = new Number(0);
                    var jmlTagih = $("#jumlahTagihan").val()

                    $(data.result).each(function(k,v) {
                        sdhBayar += new Number(v.amount);
                    });
                    var sisaBayar = convertStringToMoney(jmlTagih) - sdhBayar
                    $("#jumlahSudahBayar").val(sdhBayar.format(2));
                    $("#sisaHarusBayar").val(sisaBayar.format(2));
                });
            })
        });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]"/></span>
    <ul class="nav pull-right">

    </ul>
</div>
<div class="box">
    <div class="span12">

        <fieldset class="form-horizontal">
            <div class="span6">
                <div class="form-group">
                    <label class="control-label" for="noReff">
                  Nomor Reff
                    </label>

                    <div class="controls">
                        <input class="span6" id="noReff" type="text" readonly/>
                        <div class="input-append">
                            <button type="submit" class="btn" onclick="showModalKwitansi();">
                                <i class="icon-search"></i>
                            </button>
                        </div>
                    </div>
                </div> <!-- NOMOR SUBLET -->

                <div class="form-group">
                    <label class="control-label" for="tglReff">
                        Tanggal Kwitansi
                    </label>

                    <div class="controls">
                        <input type="text" readonly id="tglReff">
                    </div>
                </div> <!-- NAMA BENGKEL -->

                <div class="form-group">
                    <label class="control-label" for="supplier">
                        Supplier
                    </label>

                    <div class="controls">
                        <input type="text" readonly id="supplier">
                    </div>
                </div><!-- NOMOR WO -->


                <div class="form-group">
                    <label class="control-label" for="tanggalTransaksi">
                        Tanggal Transaksi
                    </label>

                    <div class="controls">
                        <ba:datePicker format="dd/mm/yyyy" name="tanggalTransaksi"/>
                    </div>
                </div><!-- Tanggal Transaksi -->

                <div class="form-group">
                    <label class="control-label" for="tipeTransaksi">
                        Tipe Transaksi
                    </label>

                    <div class="controls">
                        <input type="radio" name="tipeTransaksi" id="tipeTransaksi" value="KAS">KAS
                        <input type="radio" name="tipeTransaksi" value="BANK">BANK
                        <input type="radio" name="tipeTransaksi" value="PDC">PDC
                    </div>
                </div><!-- Tipe Transaksi -->

            </div>

            <div class="span6">
                <div class="form-group">
                    <label class="control-label" for="namaBank">
                        Nama Bank
                    </label>

                    <div class="controls">
                        <g:select name="namaBank" from="${com.kombos.finance.BankAccountNumber.createCriteria().list {eq("companyDealer", session.userCompanyDealer);eq("staDel","0");bank{order("m702NamaBank")}}}" id="namaBank" optionValue="${{it.bank.m702NamaBank+" "+it.bank.m702Cabang}}" optionKey="id"
                                  noSelection="['':'']"/>

                    </div>
                </div> <!-- NAMA BANK -->

                <div class="form-group">
                    <label class="control-label" for="tanggalTransfer">
                        Tanggal Transfer
                    </label>

                    <div class="controls">
                        <ba:datePicker name="tanggalTransfer" format="dd/mm/yyyy"/>
                    </div>
                </div> <!-- Tanggal Transfer -->

                <div class="form-group">
                    <label class="control-label" for="nomorPDCKombos">
                        Nomor PDC Kombos
                    </label>

                    <div class="controls">
                        <input type="text" id="nomorPDCKombos">
                    </div>
                </div> <!-- Nomor PDC Kombos -->

                <div class="form-group">
                    <label class="control-label" for="tanggalJatuhTempoPDCKombos">
                        Tgl Jth Tempo PDC Kombos
                    </label>

                    <div class="controls">
                        <ba:datePicker format="dd/mm/yyyy" name="tanggalJatuhTempoPDCKombos"/>
                    </div>
                </div> <!-- Tanggal Jatuh Tempo PDC Kombos -->

                <div class="form-group">
                    <label class="control-label" for="jumlahTagihan">
                        Jumlah Tagihan
                    </label>

                    <div class="controls">
                        <input type="text" id="jumlahTagihan" readonly>
                    </div>
                </div> <!-- Jumlah Tagihan -->

                <div class="form-group">
                    <label class="control-label" for="jumlahsudahbayar">
                        Jumlah yang Sudah diBayar
                    </label>

                    <div class="controls">
                        <input type="text" id="jumlahSudahBayar" readonly>
                    </div>
                </div> <!-- Jumlah yang Sudah diBayar -->

                <div class="form-group">
                    <label class="control-label" for="sisaHarusBayar">
                        Sisa yg Harus diBayar
                    </label>

                    <div class="controls">
                        <input type="text" id="sisaHarusBayar" readonly>
                    </div>
                </div> <!-- Sisa yg Harus diBayar -->

                <div class="form-group">
                    <label class="control-label" for="jumlahPembayaran">
                        Jumlah Pembayaran
                    </label>

                    <div class="controls">
                        <input type="text" class="numeric"  id="jumlahPembayaran">
                    </div>
                </div> <!-- Jumlah Pembayaran -->

                <div class="form-group">
                    <label class="control-label" for="keterangan">
                        Keterangan
                    </label>

                    <div class="controls">
                        <g:textArea name="keterangan" rows="5" cols="5"/>
                    </div>
                </div>
            </div>
        </fieldset>

        <div class="form-actions">
            <a href="javascript:void(0);" id="btnSave" class="btn btn-primary">Simpan</a>
        </div>
    </div>
</div>

<div id="noReffDTModal" class="modal hide">
    <div class="modal-content">
        <div class="modal-body">
            <g:render template="nomorKwitansiDT"/>
        </div>

        <div class="modal-footer">
            <a class="btn btn-primary" id="btnPilih">Pilih</a>
        </div>
    </div>
</div> <!-- SUBLET -->


</body>
</html>