
<%@ page import="com.kombos.finance.Transaction" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="transaction_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="transaction.transactionCode.label" default="Transaction Code" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="transaction.transactionDate.label" default="Transaction Date" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="transaction.description.label" default="Description" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="transaction.amount.label" default="Amount" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="transaction.journal.label" default="Journal" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="transaction.docNumber.label" default="Doc Number" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="transaction.inOutType.label" default="In Out Type" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="transaction.receiptNumber.label" default="Receipt Number" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="transaction.transferDate.label" default="Transfer Date" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="transaction.dueDate.label" default="Due Date" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="transaction.cekNumber.label" default="Cek Number" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="transaction.pdcStatus.label" default="Pdc Status" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="transaction.pdcTransferDate.label" default="Pdc Transfer Date" /></div>
			</th>

%{--
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="transaction.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>--}%


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="transaction.bankAccountNumber.label" default="Bank Account Number" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="transaction.documentCategory.label" default="Document Category" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="transaction.transactionType.label" default="Transaction Type" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_transactionCode" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_transactionCode" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_transactionDate" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_transactionDate" value="date.struct">
					<input type="hidden" name="search_transactionDate_day" id="search_transactionDate_day" value="">
					<input type="hidden" name="search_transactionDate_month" id="search_transactionDate_month" value="">
					<input type="hidden" name="search_transactionDate_year" id="search_transactionDate_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_transactionDate_dp" value="" id="search_transactionDate" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_description" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_description" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_amount" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_amount" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_journal" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_journal" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_docNumber" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_docNumber" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_inOutType" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_inOutType" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_receiptNumber" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_receiptNumber" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_transferDate" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_transferDate" value="date.struct">
					<input type="hidden" name="search_transferDate_day" id="search_transferDate_day" value="">
					<input type="hidden" name="search_transferDate_month" id="search_transferDate_month" value="">
					<input type="hidden" name="search_transferDate_year" id="search_transferDate_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_transferDate_dp" value="" id="search_transferDate" class="search_init">
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_dueDate" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_dueDate" value="date.struct">
					<input type="hidden" name="search_dueDate_day" id="search_dueDate_day" value="">
					<input type="hidden" name="search_dueDate_month" id="search_dueDate_month" value="">
					<input type="hidden" name="search_dueDate_year" id="search_dueDate_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_dueDate_dp" value="" id="search_dueDate" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_cekNumber" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_cekNumber" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_pdcStatus" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_pdcStatus" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_pdcTransferDate" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_pdcTransferDate" value="date.struct">
					<input type="hidden" name="search_pdcTransferDate_day" id="search_pdcTransferDate_day" value="">
					<input type="hidden" name="search_pdcTransferDate_month" id="search_pdcTransferDate_month" value="">
					<input type="hidden" name="search_pdcTransferDate_year" id="search_pdcTransferDate_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_pdcTransferDate_dp" value="" id="search_pdcTransferDate" class="search_init">
				</div>
			</th>
	
		%{--	<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>
	--}%
			<th style="border-top: none;padding: 5px;">
				<div id="filter_bankAccountNumber" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_bankAccountNumber" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_documentCategory" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_documentCategory" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_transactionType" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_transactionType" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var transactionTable;
var reloadTransactionTable;
$(function(){
	
	reloadTransactionTable = function() {
		transactionTable.fnDraw();
	}

	
	$('#search_transactionDate').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_transactionDate_day').val(newDate.getDate());
			$('#search_transactionDate_month').val(newDate.getMonth()+1);
			$('#search_transactionDate_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			transactionTable.fnDraw();	
	});

	

	$('#search_transferDate').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_transferDate_day').val(newDate.getDate());
			$('#search_transferDate_month').val(newDate.getMonth()+1);
			$('#search_transferDate_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			transactionTable.fnDraw();	
	});

	

	$('#search_dueDate').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_dueDate_day').val(newDate.getDate());
			$('#search_dueDate_month').val(newDate.getMonth()+1);
			$('#search_dueDate_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			transactionTable.fnDraw();	
	});

	

	$('#search_pdcTransferDate').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_pdcTransferDate_day').val(newDate.getDate());
			$('#search_pdcTransferDate_month').val(newDate.getMonth()+1);
			$('#search_pdcTransferDate_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			transactionTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	transactionTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	transactionTable = $('#transaction_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "transactionCode",
	"mDataProp": "transactionCode",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "transactionDate",
	"mDataProp": "transactionDate",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "description",
	"mDataProp": "description",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "amount",
	"mDataProp": "amount",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "journal",
	"mDataProp": "journal",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "docNumber",
	"mDataProp": "docNumber",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "inOutType",
	"mDataProp": "inOutType",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "receiptNumber",
	"mDataProp": "receiptNumber",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "transferDate",
	"mDataProp": "transferDate",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "dueDate",
	"mDataProp": "dueDate",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "cekNumber",
	"mDataProp": "cekNumber",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "pdcStatus",
	"mDataProp": "pdcStatus",
	"aTargets": [11],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "pdcTransferDate",
	"mDataProp": "pdcTransferDate",
	"aTargets": [12],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

/*,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [13],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}*/

,

{
	"sName": "bankAccountNumber",
	"mDataProp": "bankAccountNumber",
	"aTargets": [14],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "documentCategory",
	"mDataProp": "documentCategory",
	"aTargets": [15],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "transactionType",
	"mDataProp": "transactionType",
	"aTargets": [16],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var transactionCode = $('#filter_transactionCode input').val();
						if(transactionCode){
							aoData.push(
									{"name": 'sCriteria_transactionCode', "value": transactionCode}
							);
						}

						var transactionDate = $('#search_transactionDate').val();
						var transactionDateDay = $('#search_transactionDate_day').val();
						var transactionDateMonth = $('#search_transactionDate_month').val();
						var transactionDateYear = $('#search_transactionDate_year').val();
						
						if(transactionDate){
							aoData.push(
									{"name": 'sCriteria_transactionDate', "value": "date.struct"},
									{"name": 'sCriteria_transactionDate_dp', "value": transactionDate},
									{"name": 'sCriteria_transactionDate_day', "value": transactionDateDay},
									{"name": 'sCriteria_transactionDate_month', "value": transactionDateMonth},
									{"name": 'sCriteria_transactionDate_year', "value": transactionDateYear}
							);
						}
	
						var description = $('#filter_description input').val();
						if(description){
							aoData.push(
									{"name": 'sCriteria_description', "value": description}
							);
						}
	
						var amount = $('#filter_amount input').val();
						if(amount){
							aoData.push(
									{"name": 'sCriteria_amount', "value": amount}
							);
						}
	
						var journal = $('#filter_journal input').val();
						if(journal){
							aoData.push(
									{"name": 'sCriteria_journal', "value": journal}
							);
						}
	
						var docNumber = $('#filter_docNumber input').val();
						if(docNumber){
							aoData.push(
									{"name": 'sCriteria_docNumber', "value": docNumber}
							);
						}
	
						var inOutType = $('#filter_inOutType input').val();
						if(inOutType){
							aoData.push(
									{"name": 'sCriteria_inOutType', "value": inOutType}
							);
						}
	
						var receiptNumber = $('#filter_receiptNumber input').val();
						if(receiptNumber){
							aoData.push(
									{"name": 'sCriteria_receiptNumber', "value": receiptNumber}
							);
						}

						var transferDate = $('#search_transferDate').val();
						var transferDateDay = $('#search_transferDate_day').val();
						var transferDateMonth = $('#search_transferDate_month').val();
						var transferDateYear = $('#search_transferDate_year').val();
						
						if(transferDate){
							aoData.push(
									{"name": 'sCriteria_transferDate', "value": "date.struct"},
									{"name": 'sCriteria_transferDate_dp', "value": transferDate},
									{"name": 'sCriteria_transferDate_day', "value": transferDateDay},
									{"name": 'sCriteria_transferDate_month', "value": transferDateMonth},
									{"name": 'sCriteria_transferDate_year', "value": transferDateYear}
							);
						}

						var dueDate = $('#search_dueDate').val();
						var dueDateDay = $('#search_dueDate_day').val();
						var dueDateMonth = $('#search_dueDate_month').val();
						var dueDateYear = $('#search_dueDate_year').val();
						
						if(dueDate){
							aoData.push(
									{"name": 'sCriteria_dueDate', "value": "date.struct"},
									{"name": 'sCriteria_dueDate_dp', "value": dueDate},
									{"name": 'sCriteria_dueDate_day', "value": dueDateDay},
									{"name": 'sCriteria_dueDate_month', "value": dueDateMonth},
									{"name": 'sCriteria_dueDate_year', "value": dueDateYear}
							);
						}
	
						var cekNumber = $('#filter_cekNumber input').val();
						if(cekNumber){
							aoData.push(
									{"name": 'sCriteria_cekNumber', "value": cekNumber}
							);
						}
	
						var pdcStatus = $('#filter_pdcStatus input').val();
						if(pdcStatus){
							aoData.push(
									{"name": 'sCriteria_pdcStatus', "value": pdcStatus}
							);
						}

						var pdcTransferDate = $('#search_pdcTransferDate').val();
						var pdcTransferDateDay = $('#search_pdcTransferDate_day').val();
						var pdcTransferDateMonth = $('#search_pdcTransferDate_month').val();
						var pdcTransferDateYear = $('#search_pdcTransferDate_year').val();
						
						if(pdcTransferDate){
							aoData.push(
									{"name": 'sCriteria_pdcTransferDate', "value": "date.struct"},
									{"name": 'sCriteria_pdcTransferDate_dp', "value": pdcTransferDate},
									{"name": 'sCriteria_pdcTransferDate_day', "value": pdcTransferDateDay},
									{"name": 'sCriteria_pdcTransferDate_month', "value": pdcTransferDateMonth},
									{"name": 'sCriteria_pdcTransferDate_year', "value": pdcTransferDateYear}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}
	
						var bankAccountNumber = $('#filter_bankAccountNumber input').val();
						if(bankAccountNumber){
							aoData.push(
									{"name": 'sCriteria_bankAccountNumber', "value": bankAccountNumber}
							);
						}
	
						var documentCategory = $('#filter_documentCategory input').val();
						if(documentCategory){
							aoData.push(
									{"name": 'sCriteria_documentCategory', "value": documentCategory}
							);
						}
	
						var transactionType = $('#filter_transactionType input').val();
						if(transactionType){
							aoData.push(
									{"name": 'sCriteria_transactionType', "value": transactionType}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
