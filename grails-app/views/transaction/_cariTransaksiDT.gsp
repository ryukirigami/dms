<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="transaksi_table" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">

    <thead>
    <tr>
        <th style="border-bottom: none;padding: 5px;">
            <div>Kode Transaksi</div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div>Tanggal Transaksi</div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div>Tipe Transaksi</div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div>Jumlah</div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div>Kategori Dokumen</div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div>Nomor Dokumen</div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div>Deskripsi</div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div>Approval</div>
        </th>

    </tr>
    <tr>
        <th style="border-top: none;padding: 5px;">
            <div id="filter_transactionCode" style="padding-top: 0px;position:relative; margin-top: 0px;width: 200px;">
                <input type="text" name="search_transactionCode" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_transactionDate" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
                <input type="hidden" name="search_transactionDate" value="date.struct">
                <input type="hidden" name="search_transactionDate_day" id="search_transactionDate_day" value="">
                <input type="hidden" name="search_transactionDate_month" id="search_transactionDate_month" value="">
                <input type="hidden" name="search_transactionDate_year" id="search_transactionDate_year" value="">
                <input type="text" data-date-format="dd/mm/yyyy" name="search_transactionDate_dp" value="" id="search_transactionDate" placeholder="Date from.." class="search_init">

                <input type="hidden" name="search_transactionDate_to" value="date.struct">
                <input type="hidden" name="search_transactionDate_day_to" id="search_transactionDate_day_to" value="">
                <input type="hidden" name="search_transactionDate_month_to" id="search_transactionDate_month_to" value="">
                <input type="hidden" name="search_transactionDate_year_to" id="search_transactionDate_year_to" value="">
                <input type="text" data-date-format="dd/mm/yyyy" name="search_transactionDate_dp_to" value="" id="search_transactionDate_to" style="margin-top: 5px;" placeholder="Date to.." class="search_init">
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_tipe" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_tipe" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_total" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_total" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_kategori" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_kategori" class="search_init" />
            </div>
        </th>


        <th style="border-top: none;padding: 5px;">
            <div id="filter_docNumber" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_docNumber" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_deskripsi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 250px;">
                <input type="text" name="search_deskripsi" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">

        </th>

    </tr>
    </thead>
</table>


<g:javascript>
var transaksiTable;
var reloadTransactionTable;
$(function(){

    reloadTransactionTable = function() {
        transaksiTable.fnDraw();
    };

    $('#search_transactionDate').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_transactionDate_day').val(newDate.getDate());
			$('#search_transactionDate_month').val(newDate.getMonth()+1);
			$('#search_transactionDate_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			transaksiTable.fnDraw();
	});

	$('#search_transactionDate_to').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_transactionDate_day_to').val(newDate.getDate());
			$('#search_transactionDate_month_to').val(newDate.getMonth()+1);
			$('#search_transactionDate_year_to').val(newDate.getFullYear());
			$(this).datepicker('hide');
			transaksiTable.fnDraw();
	});


    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	transaksiTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	$("#btnCari").click(function() {
        transaksiTable.fnDraw();
    });

        transaksiTable = $('#transaksi_table').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : true,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : 5, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "transactionCode",
	"mDataProp": "transactionCode",
	"aTargets": [0],
	"bSortable": false,
	"bSearchable": true,
	"sWidth":"100px",
	"bVisible": true,
	"mRender": function(data, type, row) {
	return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this">' +
	       '<input type="hidden" value="'+row['id']+'">' +
	       '<a href="javascript:void(0);" onclick="showDetail('+row['id']+');">'+data+'</a>'
	}
},
{
	"sName": "transactionDate",
	"mDataProp": "transactionDate",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"250px",
	"bVisible": true
},

{
	"sName": "transactionType",
	"mDataProp": "transactionType",
	"aTargets": [16],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
} ,
{
	"sName": "amount",
	"mDataProp": "amount",
	"aTargets": [3],
	"mRender": function(data, type, row) {
	return '<span class="pull-right">'+data+'</span>'
	},
	"bSearchable": true,
	"bSortable": true,
	"sClass": "numeric",
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "documentCategory",
	"mDataProp": "documentCategory",
	"aTargets": [15],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},

{
	"sName": "docNumber",
	"mDataProp": "docNumber",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},

{
	"sName": "description",
	"mDataProp": "description",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
},
{
	"sName": "approval",
	"mDataProp": "approval",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                        var tipe = $('#filter_tipe input').val();
						if(tipe){
							aoData.push(
									{"name": 'sCriteria_tipe', "value": tipe}
							);
						}
	
						var docNumber = $('#filter_docNumber input').val();
						if(docNumber){
							aoData.push(
									{"name": 'sCriteria_docNumber', "value": docNumber}
							);
						}
	
						var transactionCode = $('#filter_transactionCode input').val();
						if(transactionCode){
							aoData.push(
									{"name": 'sCriteria_transactionCode', "value": transactionCode}
							);
						}

						var transactionDate = $('#search_transactionDate').val();
						var transactionDateDay = $('#search_transactionDate_day').val();
						var transactionDateMonth = $('#search_transactionDate_month').val();
						var transactionDateYear = $('#search_transactionDate_year').val();
						
						if(transactionDate){
							aoData.push(
									{"name": 'sCriteria_transactionDate', "value": "date.struct"},
									{"name": 'sCriteria_transactionDate_dp', "value": transactionDate},
									{"name": 'sCriteria_transactionDate_day', "value": transactionDateDay},
									{"name": 'sCriteria_transactionDate_month', "value": transactionDateMonth},
									{"name": 'sCriteria_transactionDate_year', "value": transactionDateYear}
							);
						}

						var transactionDateTo = $('#search_transactionDate_to').val();
						var transactionDateDayTo = $('#search_transactionDate_day_to').val();
						var transactionDateMonthTo = $('#search_transactionDate_month_to').val();
						var transactionDateYearTo = $('#search_transactionDate_year_to').val();

						if (transactionDateTo) {
						    aoData.push(
									{"name": 'sCriteria_transactionDate_to', "value": "date.struct"},
									{"name": 'sCriteria_transactionDate_dp_to', "value": transactionDateTo},
									{"name": 'sCriteria_transactionDate_day_to', "value": transactionDateDayTo},
									{"name": 'sCriteria_transactionDate_month_to', "value": transactionDateMonthTo},
									{"name": 'sCriteria_transactionDate_year_to', "value": transactionDateYearTo}
							);
						}

						var total = $('#filter_total input').val();
						if(total){
							aoData.push(
									{"name": 'sCriteria_total', "value": total}
							);
						}
	
						var kategori = $('#filter_kategori input').val();
						if(kategori){
							aoData.push(
									{"name": 'sCriteria_kategori', "value": kategori}
							);
						}

						var deskripsi = $('#filter_deskripsi input').val();
						if(deskripsi){
							aoData.push(
									{"name": 'sCriteria_deskripsi', "value": deskripsi}
							);
						}

//						if (cariSelect.length > 0 && cariValue) {
//                            aoData.push({
//                                "name": cariSelect,
//                                "value": cariValue
//                            })
//                        }

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});

});
</g:javascript>



