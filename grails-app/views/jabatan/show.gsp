

<%@ page import="com.kombos.hrd.Jabatan" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'jabatan.label', default: 'Jabatan')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteJabatan;

$(function(){ 
	deleteJabatan=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/jabatan/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadJabatanTable();
   				expandTableLayout('jabatan');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-jabatan" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="jabatan"
			class="table table-bordered table-hover">
			<tbody>
			
				<g:if test="${jabatanInstance?.jabatan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="jabatan-label" class="property-label"><g:message
					code="jabatan.jabatan.label" default="Jabatan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="jabatan-label">
						%{--<ba:editableValue
								bean="${jabatanInstance}" field="jabatan"
								url="${request.contextPath}/Jabatan/updatefield" type="text"
								title="Enter jabatan" onsuccess="reloadJabatanTable();" />--}%
							
								<g:fieldValue bean="${jabatanInstance}" field="jabatan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${jabatanInstance?.keterangan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="keterangan-label" class="property-label"><g:message
					code="jabatan.keterangan.label" default="Keterangan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="keterangan-label">
						%{--<ba:editableValue
								bean="${jabatanInstance}" field="keterangan"
								url="${request.contextPath}/Jabatan/updatefield" type="text"
								title="Enter keterangan" onsuccess="reloadJabatanTable();" />--}%
							
								<g:fieldValue bean="${jabatanInstance}" field="keterangan"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${jabatanInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="jabatan.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${jabatanInstance}" field="createdBy"
                                url="${request.contextPath}/Jabatan/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadJabatanTable();" />--}%

                        <g:fieldValue bean="${jabatanInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${jabatanInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="jabatan.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${jabatanInstance}" field="dateCreated"
                                url="${request.contextPath}/Jabatan/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadJabatanTable();" />--}%

                        <g:formatDate date="${jabatanInstance?.dateCreated}" format="dd/MM/yyyy HH:mm:ss" />

                    </span></td>

                </tr>
            </g:if>

				<g:if test="${jabatanInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="jabatan.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${jabatanInstance}" field="lastUpdProcess"
								url="${request.contextPath}/Jabatan/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadJabatanTable();" />--}%
							
								<g:fieldValue bean="${jabatanInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${jabatanInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="jabatan.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${jabatanInstance}" field="lastUpdated"
								url="${request.contextPath}/Jabatan/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadJabatanTable();" />--}%
							
								<g:formatDate date="${jabatanInstance?.lastUpdated}" format="dd/MM/yyyy HH:mm:ss" />
							
						</span></td>
					
				</tr>
				</g:if>

				<g:if test="${jabatanInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="jabatan.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${jabatanInstance}" field="updatedBy"
								url="${request.contextPath}/Jabatan/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadJabatanTable();" />--}%
							
								<g:fieldValue bean="${jabatanInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>


			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('jabatan');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${jabatanInstance?.id}"
					update="[success:'jabatan-form',failure:'jabatan-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteJabatan('${jabatanInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
