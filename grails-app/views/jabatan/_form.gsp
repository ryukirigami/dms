<%@ page import="com.kombos.hrd.Jabatan" %>



<div class="control-group fieldcontain ${hasErrors(bean: jabatanInstance, field: 'jabatan', 'error')} ">
	<label class="control-label" for="jabatan">
		<g:message code="jabatan.jabatan.label" default="Jabatan" />
		
	</label>
	<div class="controls">
	<g:textField name="jabatan" value="${jabatanInstance?.jabatan}" required="true" maxlength="16" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: jabatanInstance, field: 'keterangan', 'error')} ">
	<label class="control-label" for="keterangan">
		<g:message code="jabatan.keterangan.label" default="Keterangan" />
		
	</label>
	<div class="controls">
	<g:textField name="keterangan" value="${jabatanInstance?.keterangan}" maxlength="128" />
	</div>
</div>

<g:javascript>
    $(function() {
        oFormUtils.fnInit();
    })
</g:javascript>

