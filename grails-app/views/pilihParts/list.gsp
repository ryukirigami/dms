
<%@ page import="com.kombos.parts.Goods" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'goods.label', default: 'Pilih Part')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var showPilihPart;
	var loadFormPilihPart;
	var shrinkTableLayoutPilihPart;
	var expandTableLayoutPilihPart;
	var edit;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	

    showPilihPart = function(id) {
    	shrinkTableLayoutPilihPart();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/goods/showPilihPart/'+id,
   			success:function(data,textStatus){
   				loadFormPilihPart(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    editPilihPart = function(id) {
    	shrinkTableLayoutPilihPart();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/goods/edit/',
   		    data : {id : id},
   			success:function(data,textStatus){
   				loadFormPilihPart(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadFormPilihPart = function(data, textStatus){
		$('#goods-form').empty();
    	$('#goods-form').append(data);
   	}
    
    shrinkTableLayoutPilihPart = function(){
    	if($("#goods-table").hasClass("span12")){
   			$("#goods-table").toggleClass("span12 span5");
        }
        $("#goods-form").css("display","block"); 
   	}
   	
   	expandTableLayoutPilihPart = function(){
   		if($("#goods-table").hasClass("span5")){
   			$("#goods-table").toggleClass("span5 span12");
   		}
        $("#goods-form").css("display","none");
   	}
   	

   	


});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
	</div>
	<div class="box">
		<div class="span12" id="goods-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
		</div>
		<div class="span7" id="goods-form" style="display: none;"></div>
	</div>
    <div id="pilihPartModal" class="modal fade">
        <div class="modal-dialog" style="width: 1200px;">
            <div class="modal-content" style="width: 1200px;">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 1200px;">
                    <div id="pilihPartContent"/>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="iu-content"></div>
                </div>
                <!-- dialog buttons -->
                <div class="modal-footer"><button id='closepilihPart' type="button" class="btn btn-primary">Close</button></div>
            </div>
        </div>
    </div>
</body>
</html>
