
<%@ page import="com.kombos.finance.AccountType" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="accountType_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

%{--
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="accountType.accountNumber.label" default="Account Number" /></div>
			</th>

--}%

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="accountType.accountType.label" default="Account Type" /></div>
			</th>


		%{--	<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="accountType.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>
--}%

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="accountType.saldoNormal.label" default="Saldo Normal" /></div>
			</th>

		
		</tr>
		<tr>
		
	%{--
			<th style="border-top: none;padding: 5px;">
				<div id="filter_accountNumber" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_accountNumber" class="search_init" />
				</div>
			</th>--}%
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_accountType" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_accountType" class="search_init" />
				</div>
			</th>
	%{--
			<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>--}%
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_saldoNormal" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_saldoNormal" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var accountTypeTable;
var reloadAccountTypeTable;
$(function(){
	
	reloadAccountTypeTable = function() {
		accountTypeTable.fnDraw();
	}

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	accountTypeTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	accountTypeTable = $('#accountType_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

/*{
	"sName": "accountNumber",
	"mDataProp": "accountNumber",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}*/

/*,*/

{
	"sName": "accountType",
	"mDataProp": "accountType",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true,
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	}
}
/*
,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}*/

,

{
	"sName": "saldoNormal",
	"mDataProp": "saldoNormal",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var accountNumber = $('#filter_accountNumber input').val();
						if(accountNumber){
							aoData.push(
									{"name": 'sCriteria_accountNumber', "value": accountNumber}
							);
						}
	
						var accountType = $('#filter_accountType input').val();
						if(accountType){
							aoData.push(
									{"name": 'sCriteria_accountType', "value": accountType}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}
	
						var saldoNormal = $('#filter_saldoNormal input').val();
						if(saldoNormal){
							aoData.push(
									{"name": 'sCriteria_saldoNormal', "value": saldoNormal}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
