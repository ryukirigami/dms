<%@ page import="com.kombos.finance.AccountType" %>



%{--<div class="control-group fieldcontain ${hasErrors(bean: accountTypeInstance, field: 'accountNumber', 'error')} ">
	<label class="control-label" for="accountNumber">
		<g:message code="accountType.accountNumber.label" default="Account Number" />
		
	</label>
	<div class="controls">
	
<ul class="one-to-many">
<g:each in="${accountTypeInstance?.accountNumber?}" var="a">
    <li><g:link controller="accountNumber" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="accountNumber" action="create" params="['accountType.id': accountTypeInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'accountNumber.label', default: 'AccountNumber')])}</g:link>
</li>
</ul>

	</div>
</div>--}%

<div class="control-group fieldcontain ${hasErrors(bean: accountTypeInstance, field: 'accountType', 'error')} ">
	<label class="control-label" for="accountType">
		<g:message code="accountType.accountType.label" default="Account Type" />
		
	</label>
	<div class="controls">
	<g:textField name="accountType" value="${accountTypeInstance?.accountType}" />
	</div>
</div>
%{--
<div class="control-group fieldcontain ${hasErrors(bean: accountTypeInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="accountType.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${accountTypeInstance?.lastUpdProcess}" />
	</div>
</div>--}%

<div class="control-group fieldcontain ${hasErrors(bean: accountTypeInstance, field: 'saldoNormal', 'error')} ">
	<label class="control-label" for="saldoNormal">
		<g:message code="accountType.saldoNormal.label" default="Saldo Normal" />
		
	</label>
	<div class="controls">
	<g:textField name="saldoNormal" value="${accountTypeInstance?.saldoNormal}" />
	</div>
</div>

