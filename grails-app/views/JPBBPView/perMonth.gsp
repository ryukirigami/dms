
<%@ page import="com.kombos.board.JPB" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'JPB.label', default: 'JPB GR')}" />
		%{--<title><g:message code="default.list.label" args="[entityName]" /></title>--}%
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			var asbViewM;
			$(function(){ 
			         
	$('#search_tglViewMonth').datepicker().on('changeDate', function(ev) {
	        console.log("inside search_tglView");
			var newDate = new Date(ev.date);
			$('#search_tglViewMonth_day').val(newDate.getDate());
			$('#search_tglViewMonth_month').val(newDate.getMonth()+1);
			$('#search_tglViewMonth_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
//			var oTable = $('#JPB_datatables').dataTable();
//            oTable.fnReloadAjax();
	});

				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('JPB');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('JPB', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('JPB', '${request.contextPath}/JPB/massdelete', reloadJPBTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('JPB','${request.contextPath}/JPB/show/'+id);
				};
				
				edit = function(id) {
					editInstance('JPB','${request.contextPath}/JPB/edit/'+id);
				};

   	asbViewM = function(){
        var oTable = $('#JPB_datatables').dataTable();
        oTable.fnReloadAjax();
        var from = $("#search_tglViewMonth").val().split("/");
        var theDate = new Date();
        theDate.setFullYear(from[2], from[1] - 1, from[0]);
        //alert("theDate=" + theDate);
        var m_names = new Array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
        var formattedDate =  theDate.getDate() + " " + m_names[theDate.getMonth()] + " " + theDate.getFullYear();
        document.getElementById("lblTglView").innerHTML = formattedDate;
   	}
});

</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		%{--<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>--}%
		%{--<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>--}%
	</div>

	<div class="box">
        <table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">
            <tr>
                <td style="width: 40%;vertical-align: top;">
                    <div class="box">
                        <legend style="font-size: small">View JPB</legend>
                        <table style="width: 100%;border: 0px">
                            <tr>
                                <td>
                                    <label class="control-label" for="lbl_rcvDate">
                                        Nama Workshop
                                    </label>
                                </td>
                                <td colspan="2">
                                    <div id="lbl_companyDealer" class="controls">
                                        <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                            <g:select name="companyDealer.id" id="input_companyDealer" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                                        </g:if>
                                        <g:else>
                                            <g:select name="companyDealer.id" id="input_companyDealer" readonly="" disabled="" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                                        </g:else>
                                        %{--<g:select id="input_companyDealer" name="companyDealer.id" from="${com.kombos.administrasi.CompanyDealer.list()}" optionKey="id" required="" class="many-to-one"/>--}%
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label class="control-label" for="lbl_tglViewMonth">
                                        Tanggal
                                    </label>
                                </td>
                                <td>
                                    <div id="lbl_tglViewMonth" class="controls">
                                        <div id="filter_tglViewMonth" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                                             <input type="hidden" name="search_tglViewMonth" value="date.struct">
                                             <input type="hidden" name="search_tglViewMonth_day" id="search_tglViewMonth_day" value="">
                                             <input type="hidden" name="search_tglViewMonth_month" id="search_tglViewMonth_month" value="">
                                             <input type="hidden" name="search_tglViewMonth_year" id="search_tglViewMonth_year" value="">
                                             <input type="text" data-date-format="dd/mm/yyyy" name="search_tglViewMonth_dp" value="" id="search_tglViewMonth" class="search_init" style="width: 160px" >
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <button class="btn btn-primary" id="buttonView" onclick="asbViewM()" style="width: 120px" >View</button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td style="width: 60%;vertical-align: top;padding-left: 10px;">

                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div id="JPB-table">
                        <g:if test="${flash.message}">
                            <div class="message" role="status">
                                ${flash.message}
                            </div>
                        </g:if>

                        <g:render template="dataTablesPerMonth" />
                    </div>
                    </td>
                </tr>
        </table>
	</div>
</body>
</html>
