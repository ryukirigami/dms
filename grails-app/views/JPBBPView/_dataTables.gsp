
<%@ page import="com.kombos.board.JPB" %>

<r:require modules="baseapplayout, jQueryContextMenu" />
<g:render template="../menu/maxLineDisplay"/>

<table id="JPB_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
    <tr>
        <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" rowspan="4" >
            <div>FOREMAN</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="3" rowspan="2">
            <div>JOB PROGRESS BOARD (JPB)<br />BODY & PAINT</div>
        </th>

        <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="21">
            <div><p id="lblTglView" class="lblTglView">${params?.sCriteria_tglView}</p></div>
        </th>
    </tr>
    <tr>
        <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="40">
            <div>JAM</div>
        </th>
    </tr>
    <tr>
        <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" rowspan="2">
            <div>Group</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" rowspan="2">
            <div>Teknisi</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" rowspan="2">
            <div>Stall</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="4">
            <div>07:00</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="4">
            <div>08:00</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="4">
            <div>09:00</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="4">
            <div>10:00</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="4">
            <div>11:00</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="4">
            <div>12:00</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="4">
            <div>13:00</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="4">
            <div>14:00</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="4">
            <div>15:00</div>
        </th>
        <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="4">
            <div>16:00</div>
        </th>
    </tr>
    <tr>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div> </div>
        </th>
    </tr>

	</thead>
    <tfoot>
    <tr><td colspan="4" rowspan="1" style="width: 686.883px;"><span>Total Jumlah Unit Booking</span></td><td rowspan="1" colspan="1" style="width: 114.883px;"><span class="pull-right numeric"></span></td></tr>
    <tr><td colspan="4" rowspan="1" style="width: 686.883px;"><span>Total Sisa Waktu yang Tersedia</span></td><td rowspan="1" colspan="1" style="width: 114.883px;"><span class="pull-right numeric"></span></td></tr>
    </tfoot>
</table>

<g:javascript>
var ASBTable;
var reloadASBTable;
var getTeknisiInfo;
var getForemanInfo;
var getOnProgressInfo;
var selectedId;
var selectId;
$(function(){

    selectId = function(comp){
     //   var selectedId = $(comp).find("hidden").val();
      selectedId = comp;
      alert("a adalah : "+selectedId);
    }


       getForemanInfo = function(id){
                    var content = "nocontent";
                        $.ajax({type:'POST', url:'${request.contextPath}/groupManPower/getManPowerInfo/'+id,
                            success:function(data,textStatus){
                                if(data){

                                   content = data.namaLengkap + " - " + data.jabatan;
                                   content = content +"</br>Group : "+data.group;
                                   content = content +"</br>Anggota Group : </br>"

                                   if(data.anggota){
                                        jQuery.each(data.anggota, function (index, value) {
                                            content = content + "</br>"+value;
                                        });
                                   }


                                 $("#foreman"+id).tooltip({
                                    html : true,
                                    title : content,
                                    position: 'center right'
                                });

                                }

                            },
                            error:function(XMLHttpRequest,textStatus,errorThrown){},
                            complete:function(XMLHttpRequest,textStatus){

                            }
                });

    }

       getTeknisiInfo = function(idTeknisi, idJPB){

            var content = "nocontent";
            $.ajax({type:'GET', url:'${request.contextPath}/groupManPower/getTeknisiJPB/',
            data : {id : idTeknisi, idJPB : idJPB},
   			success:function(data,textStatus){
   				if(data){

   				   content = data.namaLengkap + " - " + data.jabatan;
                   content = content +"</br>Group : "+data.group;
                   content = content +"</br>Kepala Group : "+data.kepalaGroup;
                   content = content +"</br></br>";
                   content = content +"</br>Previous Job : "+data.prevJob;
                   content = content +"</br>No. WO : "+data.prevNoWO;
                   content = content +"</br>"+data.prevTime;

                   content = content +"</br></br>Current Job : "+data.currJob;
                   content = content +"</br>No. WO : "+data.currNoWO;
                   content = content +"</br>"+data.currTime;

                   content = content +"</br></br>Next Job : "+data.nextJob;
                   content = content +"</br>No. WO : "+data.nextNoWO;
                   content = content +"</br>"+data.nextTime;




   				 $("#teknisi"+idTeknisi).tooltip({
                    html : true,
                    title : content,
                    position: 'center right'
                });

   				}

   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){

   			}
   		});

    }

    getOnProgressInfo = function(idJPB, tag, info){

         var content = "nocontent";

 //        console.log("info : "+info);

// console.log("id jpb : "+idJPB+" tag : "+tag);

        if(info != "BREAK"){
        $.ajax({type:'GET', url:'${request.contextPath}/operation/getOnProgressOperation/',
            data : {bPOrGR : "0", idJPB : idJPB},
   			success:function(data,textStatus){
   				if(data){

   				   content = "No. WO : "+data.noWO;
                   content = content +"</br>No. Polisi : "+data.noPol;
                   content = content +"</br></br>Nama Job : "+data.namaJob;
                   content = content +"</br></br>Level : "+data.level;

                  if(info === "Y"){

                   content = content + "</br><span style='color:red'>JOB STOPPED</span></br>";
                   content = content + "</br><span style='color:red'>Alasan : "+data.alasan+"</span>";

                   }

                  if(info === "W"){

                   content = content + "</br><span style='color:red'>Reschedule Delivery</span></br>";
                   content = content + "</br><span style='color:red'>Alasan : "+data.alasan+"</span>";

                   }

                   if(info === "X"){

                   content = content + "</br><span style='color:red'>Reschedule JPB</span></br>";
                   content = content + "</br><span style='color:red'>Alasan : "+data.alasan+"</span>";

                   }

                   content = content +"</br></br>Current Progress : "+data.currProgress;
                   content = content +"</br>Start : "+data.start;
                   content = content +"</br>Target Finish : "+data.targetFinish;
                   content = content +"</br>Delivery Time : "+data.deliveryTime;


//   				 console.log("tag : "+tag);
  // 				 console.log("idjpb : "+idJPB);

   				 $("#jam"+tag+idJPB).tooltip({
                    html : true,
                    title : content,
                    position: 'center right'
                });

   				}

   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){

   			}
   		});

        }

    }


	reloadJPBTable = function() {
		ASBTable.fnDraw();
	}

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	ASBTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	ASBTable = $('#JPB_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
	//		alert("aData :"+aData[2]);

                  $(nRow).children().each(function(index, td) {
                        var noppp = ['noPolJam7','noPolJam715','noPolJam730','noPolJam745',
                                     'noPolJam8','noPolJam815','noPolJam830','noPolJam845',
                                     'noPolJam9','noPolJam915','noPolJam930','noPolJam945',
                                     'noPolJam10','noPolJam1015','noPolJam1030','noPolJam1045',
                                     'noPolJam11','noPolJam1115','noPolJam1130','noPolJam1145',
                                     'noPolJam12','noPolJam1215','noPolJam1230','noPolJam1245',
                                     'noPolJam13','noPolJam1315','noPolJam1330','noPolJam1345',
                                     'noPolJam14','noPolJam1415','noPolJam1430','noPolJam1445',
                                     'noPolJam15','noPolJam1515','noPolJam1530','noPolJam1545',
                                     'noPolJam16','noPolJam1615','noPolJam1630','noPolJam1645',
                                     'noPolJam17'];
                            if(index >= 3 && index <= 42) {
                                if ($(td).html() === "BREAK") {
                                    $(td).css("background-color", "grey");
                                }
                                console.log(aData["stall"])
                                // aData["stall"]

                                if(aData["stall"].includes("PENDEMPULAN/CAT DASAR")){
                                    if ($(td).html() === "V1") {
                                        for(a=0;a<=42;a++){
                                            if(aData[noppp[a]]){
                                                $(td).html(aData[noppp[a]]);
                                            }
                                        }
                                        $(td).css("color", "white");
                                        $(td).css("background-color", "#07BB16");
                                    }
                                    if ($(td).html() === "V") {
                                        $(td).html("");
                                        $(td).css("background-color", "#07BB16");
                                    }
                                }else
                                if(aData["stall"].includes("PELEPASAN")){
                                    if ($(td).html() === "V1") {
                                        for(a=0;a<=42;a++){
                                            if(aData[noppp[a]]){
                                                $(td).html(aData[noppp[a]]);
                                            }
                                        }
                                        $(td).css("color", "white");
                                        $(td).css("background-color", "#000000");
                                    }
                                    if ($(td).html() === "V") {
                                        $(td).html("");
                                        $(td).css("background-color", "#000000");
                                    }
                                }else
                                if(aData["stall"].includes("PERBAIKAN")){
                                    if ($(td).html() === "V1") {
                                        for(a=0;a<=42;a++){
                                            if(aData[noppp[a]]){
                                                $(td).html(aData[noppp[a]]);
                                            }
                                        }
                                        $(td).css("color", "white");
                                        $(td).css("background-color", "#E06133");
                                    }
                                    if ($(td).html() === "V") {
                                        $(td).html("");
                                        $(td).css("background-color", "#E06133");
                                    }
                                }else
                                if(aData["stall"].includes("MASKING")){
                                    if ($(td).html() === "V1") {
                                        for(a=0;a<=42;a++){
                                            if(aData[noppp[a]]){
                                                $(td).html(aData[noppp[a]]);
                                            }
                                        }
                                        $(td).css("color", "white");
                                        $(td).css("background-color", "#33E0D7");
                                    }
                                    if ($(td).html() === "V") {
                                        $(td).html("");
                                        $(td).css("background-color", "#33E0D7");
                                    }
                                }else
                                if(aData["stall"].includes("COMBI BOOTH")){
                                    if ($(td).html() === "V1") {
                                        for(a=0;a<=42;a++){
                                            if(aData[noppp[a]]){
                                                $(td).html(aData[noppp[a]]);
                                            }
                                        }
                                        $(td).css("color", "white");
                                        $(td).css("background-color", "#C18514");
                                    }
                                    if ($(td).html() === "V") {
                                        $(td).html("");
                                        $(td).css("background-color", "#C18514");
                                    }
                                }else
                                if(aData["stall"].includes("POLES")){
                                    if ($(td).html() === "V1") {
                                        for(a=0;a<=42;a++){
                                            if(aData[noppp[a]]){
                                                $(td).html(aData[noppp[a]]);
                                            }
                                        }
                                        $(td).css("color", "white");
                                        $(td).css("background-color", "#DDE033");
                                    }
                                    if ($(td).html() === "V") {
                                        $(td).html("");
                                        $(td).css("background-color", "#DDE033");
                                    }
                                }else {
                                    if ($(td).html() === "V1") {
                                            for(a=0;a<=42;a++){
                                                if(aData[noppp[a]]){
                                                    $(td).html(aData[noppp[a]]);
                                                }
                                            }
                                            $(td).css("color", "white");
                                            $(td).css("background-color", "#078DC6");
                                        }
                                        if ($(td).html() === "V") {
                                            $(td).html("");
                                            $(td).css("background-color", "#078DC6");
                                        }
                                }

                                if ($(td).html() === "NOW") {
                                   $(td).html("");
                                   $(td).css("background-color", "#fabb88");
                                }


                            }
                            if(aData["actual"] === true){
                                if (s.hasClass("A1")) {
                                    $(td).css("background-color", "red");
                                    $(td).css("color", "white");
                                }else if (s.hasClass("NOW")) {
                                   $(td).css("background-color", "#fabb88");
                                } else
                                if (s.hasClass("A")) {
                                    $(td).css("background-color", "red");
                                    $(td).html("a");
                                }else{
                                    $(td).html("b");
                                }
                            }
                        });

                    return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumnDefs": [
            {"aTargets": [4], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "07");
                $(nTd).attr("data-menit", "00");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [5], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "07");
                $(nTd).attr("data-menit", "15");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [6], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "07");
                $(nTd).attr("data-menit", "30");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [7], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "07");
                $(nTd).attr("data-menit", "45");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [8], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "08");
                $(nTd).attr("data-menit", "00");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [9], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "08");
                $(nTd).attr("data-menit", "15");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [10], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "08");
                $(nTd).attr("data-menit", "30");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [11], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "08");
                $(nTd).attr("data-menit", "45");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [12], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "09");
                $(nTd).attr("data-menit", "00");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [13], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "09");
                $(nTd).attr("data-menit", "15");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [14], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "09");
                $(nTd).attr("data-menit", "30");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [15], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "09");
                $(nTd).attr("data-menit", "45");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [16], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "10");
                $(nTd).attr("data-menit", "00");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [17], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "10");
                $(nTd).attr("data-menit", "15");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [18], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "10");
                $(nTd).attr("data-menit", "30");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [19], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "10");
                $(nTd).attr("data-menit", "45");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [20], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "11");
                $(nTd).attr("data-menit", "00");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [21], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "11");
                $(nTd).attr("data-menit", "15");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [22], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "11");
                $(nTd).attr("data-menit", "30");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [23], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "11");
                $(nTd).attr("data-menit", "45");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [24], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "12");
                $(nTd).attr("data-menit", "00");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
                $(nTd).attr("title", sData["noPolJam12"]);
            }},
            {"aTargets": [25], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "12");
                $(nTd).attr("data-menit", "15");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [26], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "12");
                $(nTd).attr("data-menit", "30");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [27], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "12");
                $(nTd).attr("data-menit", "45");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [28], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "13");
                $(nTd).attr("data-menit", "00");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [29], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "13");
                $(nTd).attr("data-menit", "15");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [30], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "13");
                $(nTd).attr("data-menit", "30");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [31], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "13");
                $(nTd).attr("data-menit", "45");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [32], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "14");
                $(nTd).attr("data-menit", "00");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [33], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "14");
                $(nTd).attr("data-menit", "15");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [34], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "14");
                $(nTd).attr("data-menit", "30");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [35], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "14");
                $(nTd).attr("data-menit", "45");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [36], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "15");
                $(nTd).attr("data-menit", "00");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [37], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "15");
                $(nTd).attr("data-menit", "15");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [38], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "15");
                $(nTd).attr("data-menit", "30");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [39], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "15");
                $(nTd).attr("data-menit", "45");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [40], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "16");
                $(nTd).attr("data-menit", "00");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [41], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "16");
                $(nTd).attr("data-menit", "15");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [42], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "16");
                $(nTd).attr("data-menit", "30");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }},
            {"aTargets": [43], "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                $(nTd).attr("data-jam", "16");
                $(nTd).attr("data-menit", "45");
                $(nTd).attr("data-stallId", oData["stallId"]);
                $(nTd).attr("data-date", oData["date"]);
            }}
        ],
		"aoColumns": [

{
	"sName": "foreman",
	"mDataProp": "foreman",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"300px",
	"mRender": function ( data, type, row ) {
	    return '<input type="hidden" id="foreman'+row['foremanId']+'" value="'+row['foremanId']+'"/> <a href="#" id="foreman'+row['foremanId']+'" onmouseover="getForemanInfo('+row['foremanId']+')" data-toggle="tooltip"  data-placement="right" alt="test" >'+data+'</a>'
	},
	"bVisible": true
}

,

{
	"sName": "group",
	"mDataProp": "group",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "teknisi",
	"mDataProp": "teknisi",
	"aTargets": [2],
	"mRender": function ( data, type, row ) {
	    return '<input type="hidden" id="jpbRow'+row['jpbId']+'" value="'+row['jpbId']+'"  /> <a href="#" id="teknisi'+row['teknisiId']+'" onmouseover="getTeknisiInfo('+row['teknisiId']+','+row['jpbId']+')" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "stall",
	"mDataProp": "stall",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "jam7",
	"mDataProp": "jam7",
	"sClass": "jam7",
    "aTargets": [4],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"10px",
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam7"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam715",
	"mDataProp": "jam715",
    "sClass": "jam715",
    "aTargets": [5],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"10px",
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam715"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam730",
	"mDataProp": "jam730",
    "sClass": "jam730",
    "aTargets": [6],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"10px",
//  	"mRender": function ( data, type, row ) {
 // 	    return '<span class='+data+'>' + row["noPolJam730"]+'</span>';
 // 	},
	"bVisible": true
}
,
{
	"sName": "jam745",
	"mDataProp": "jam745",
		"sClass": "jam745",
    "aTargets": [7],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"10px",
//  	"mRender": function ( data, type, row ) {
//  	    return '<span class='+data+'>' + row["noPolJam745"]+'</span>';
//  	},
	"bVisible": true
}
,
{
	"sName": "jam8",
	"mDataProp": "jam8",
		"sClass": "jam8",
    "aTargets": [8],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"10px",
 // 	"mRender": function ( data, type, row ) {
  //	    return '<span class='+data+'>' + row["noPolJam8"]+'</span>';
  //	},
	"bVisible": true
}
,
{
	"sName": "jam815",
	"mDataProp": "jam815",
		"sClass": "jam815",
    "aTargets": [9],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"10px",
//  	"mRender": function ( data, type, row ) {
//  	    return '<span class='+data+'>' + row["noPolJam815"]+'</span>';
//  	},
	"bVisible": true
}
,
{
	"sName": "jam830",
	"mDataProp": "jam830",
		"sClass": "jam830",
    "aTargets": [10],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"10px",
  //	"mRender": function ( data, type, row ) {
  	//    return '<span class='+data+'>' + row["noPolJam830"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam845",
	"mDataProp": "jam845",
		"sClass": "jam845",
    "aTargets": [11],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"10px",
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam845"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam9",
	"mDataProp": "jam9",
		"sClass": "jam9",
    "aTargets": [12],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"10px",
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam9"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam915",
	"mDataProp": "jam915",
		"sClass": "jam915",
    "aTargets": [13],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"10px",
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam915"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam930",
	"mDataProp": "jam930",
		"sClass": "jam930",
    "aTargets": [14],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"10px",
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam930"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam945",
	"mDataProp": "jam945",
		"sClass": "jam945",
    "aTargets": [15],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"10px",
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam945"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam10",
	"mDataProp": "jam10",
		"sClass": "jam10",
    "aTargets": [16],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"10px",
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam10"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam1015",
	"mDataProp": "jam1015",
		"sClass": "jam1015",
    "aTargets": [17],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"10px",
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam1015"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam1030",
	"mDataProp": "jam1030",
		"sClass": "jam1030",
    "aTargets": [18],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"10px",
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam1030"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam1045",
	"mDataProp": "jam1045",
		"sClass": "jam1045",
    "aTargets": [19],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"10px",
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam1045"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam11",
	"mDataProp": "jam11",
		"sClass": "jam11",
    "aTargets": [20],
	"bSearchable": true,
	"sWidth":"10px",
	"bSortable": false,
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam11"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam1115",
	"mDataProp": "jam1115",
		"sClass": "jam1115",
    "aTargets": [21],
	"bSearchable": true,
	"sWidth":"10px",
	"bSortable": false,
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam1115"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam1130",
	"mDataProp": "jam1130",
		"sClass": "jam1130",
    "aTargets": [22],
	"bSearchable": true,
	"sWidth":"10px",
	"bSortable": false,
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'></span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam1145",
	"mDataProp": "jam1145",
		"sClass": "jam1145",
    "aTargets": [23],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"10px",
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'></span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam12",
	"mDataProp": "jam12",
		"sClass": "jam12",
    "aTargets": [24],
	"bSearchable": true,
	"sWidth":"10px",
	// "mRender": function ( data, type, row ) {
	//     return 'aaaaa';
	// },
	"bSortable": false,
	"bVisible": true
}
,
{
	"sName": "jam1215",
	"mDataProp": "jam1215",
		"sClass": "jam1215",
    "aTargets": [25],
	"bSearchable": true,
	"sWidth":"10px",
	"bSortable": false,
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'></span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam1230",
	"mDataProp": "jam1230",
		"sClass": "jam1230",
    "aTargets": [26],
	"bSearchable": true,
	"sWidth":"10px",
	"bSortable": false,
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam1230"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam1245",
	"mDataProp": "jam1245",
		"sClass": "jam1245",
    "aTargets": [27],
	"bSearchable": true,
	"sWidth":"10px",
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam1245"]+'</span>';
//	},
	"bSortable": false,
	"bVisible": true
}
,
{
	"sName": "jam13",
	"mDataProp": "jam13",
		"sClass": "jam13",
    "aTargets": [28],
	"bSearchable": true,
	"sWidth":"10px",
	"bSortable": false,
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam13"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam1315",
	"mDataProp": "jam1315",
		"sClass": "jam1315",
    "aTargets": [29],
	"bSearchable": true,
	"sWidth":"10px",
	"bSortable": false,
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam1315"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam1330",
	"mDataProp": "jam1330",
		"sClass": "jam1330",
    "aTargets": [30],
	"bSearchable": true,
	"sWidth":"10px",
	"bSortable": false,
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam1330"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam1345",
	"mDataProp": "jam1345",
		"sClass": "jam1345",
    "aTargets": [31],
	"bSearchable": true,
	"sWidth":"10px",
	"bSortable": false,
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam1345"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam14",
	"mDataProp": "jam14",
		"sClass": "jam14",
    "aTargets": [32],
	"bSearchable": true,
	"sWidth":"10px",
	"bSortable": false,
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam14"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam1415",
	"mDataProp": "jam1415",
		"sClass": "jam1415",
    "aTargets": [33],
	"bSearchable": true,
	"sWidth":"10px",
	"bSortable": false,
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam1415"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam1430",
	"mDataProp": "jam1430",
		"sClass": "jam1430",
    "aTargets": [34],
	"bSearchable": true,
	"sWidth":"10px",
	"bSortable": false,
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam1430"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam1445",
	"mDataProp": "jam1445",
		"sClass": "jam1445",
    "aTargets": [35],
	"bSearchable": true,
	"sWidth":"10px",
	"bSortable": false,
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam1445"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam15",
	"mDataProp": "jam15",
		"sClass": "jam15",
    "aTargets": [36],
	"bSearchable": true,
	"sWidth":"10px",
	"bSortable": false,
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam15"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam1515",
	"mDataProp": "jam1515",
		"sClass": "jam1515",
    "aTargets": [37],
	"bSearchable": true,
	"sWidth":"10px",
	"bSortable": false,
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam1515"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam1530",
	"mDataProp": "jam1530",
		"sClass": "jam1530",
    "aTargets": [38],
	"bSearchable": true,
	"sWidth":"10px",
	"bSortable": false,
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam1530"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam1545",
	"mDataProp": "jam1545",
		"sClass": "jam1545",
    "aTargets": [39],
	"bSearchable": true,
	"sWidth":"10px",
	"bSortable": false,
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam1545"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam16",
	"mDataProp": "jam16",
		"sClass": "jam16",
    "aTargets": [40],
	"bSearchable": true,
	"sWidth":"10px",
	"bSortable": false,
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam16"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam1615",
	"mDataProp": "jam1615",
		"sClass": "jam1615",
    "aTargets": [41],
	"bSearchable": true,
	"sWidth":"10px",
	"bSortable": false,
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam1615"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam1630",
	"mDataProp": "jam1630",
		"sClass": "jam1630",
    "aTargets": [42],
	"bSearchable": true,
	"sWidth":"10px",
	"bSortable": false,
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam1630"]+'</span>';
//	},
	"bVisible": true
}
,
{
	"sName": "jam1645",
	"mDataProp": "jam1645",
		"sClass": "jam1645",
    "aTargets": [43],
	"bSearchable": true,
	"sWidth":"10px",
	"bSortable": false,
//	"mRender": function ( data, type, row ) {
//	    return '<span class='+data+'>' + row["noPolJam1645"]+'</span>';
//	},
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                var sCriteria_cabang = $('#input_companyDealer').val();
                if(sCriteria_cabang){
                    aoData.push(
                        {"name": 'input_companyDealer', "value": sCriteria_cabang}
                    );
                }


            var tglView = $('#search_tglView').val();
            var tglViewDay = $('#search_tglView_day').val();
            var tglViewMonth = $('#search_tglView_month').val();
            var tglViewYear = $('#search_tglView_year').val();
            if(tglView){
                aoData.push(
                        {"name": 'sCriteria_tglView', "value": "date.struct"},
                        {"name": 'sCriteria_tglView_dp', "value": tglView},
                        {"name": 'sCriteria_tglView_day', "value": tglViewDay},
                        {"name": 'sCriteria_tglView_month', "value": tglViewMonth},
                        {"name": 'sCriteria_tglView_year', "value": tglViewYear}
                );
            }
            aoData.push(
                    {"name": 'aksi', "value": "input"},
                    {"name": 'jenis', "value": "BP"}
            );

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

    new FixedColumns( ASBTable, {
		"iLeftWidth": 150,
		//"iLeftColumns": 2,
		"fnDrawCallback": function ( left, right ) {
			var that = this, groupVal = null, matches = 0, heights = [], index = -1;

			// Get the heights of the cells and remove redundant ones
			$('tbody tr td', left.body).each( function ( i ) {
				var currVal = this.innerHTML;

				// Reset values on new cell data.
				if (currVal != groupVal) {
					groupVal = currVal;
					index++;
					heights[index] = 0;
					matches = 0;
				} else  {
					matches++;
				}

				heights[ index ] += $(this.parentNode).height();
				if ( currVal == groupVal && matches > 0 ) {
					this.parentNode.parentNode.removeChild(this.parentNode);
				}
			} );

			// Now set the height of the cells which remain, from the summed heights
			$('tbody tr td', left.body).each( function ( i ) {
				that.fnSetRowHeight( this.parentNode, heights[ i ] );
			} );
		}
	} );
/*
	$.contextMenu({
        selector: '.context-menu',
        callback: function(key, options) {

         //   var trVar = $("tr:hover").find("input[id^=jpb]").val();

            var m = "clicked: " + key+ " val : "+selectedId;
            window.console && console.log(m) || alert(m);

            switch(key){
                case "AddJob" :
                     showAddJobModal();
                     break;
                case "AddTime" :
                     showAddTimeModal();
                         break;
                case "ReSchedule" :
                     showReScheduleModal();
                     break;
                case "MoveJob" :
                     showMoveJobModal();
                     break;
            }


        },
        items: {
            "AddJob": {name: "Add Job"},
            "AddTime": {name: "Add Time"},
            "ReSchedule": {name: "Re Schedule"},
            "MoveJob": {name: "Move Job"}
        }
    });

    $('.context-menu').on('contextmenu', function(e){
        alert("Hello");
     });

     */

});


</g:javascript>


			
