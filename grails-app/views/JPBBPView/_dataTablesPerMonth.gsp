
<%@ page import="com.kombos.board.JPB" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="JPB_datatablesPerMonth" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
        <tr>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" rowspan="4" >
                <div>FOREMAN</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="35" >
                <div>JOB PROGRESS BOARD (JPB)<br />BODY & PAINT</div>
            </th>
        </tr>
        <tr>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" rowspan="3">
                <div>Group</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" rowspan="3">
                <div>Teknisi</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" rowspan="3">
                <div>Stall</div>
            </th>

            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="31">
                <div><p id="lblYear" class="lblYear">${params?.year}</p></div>
            </th>
        </tr>
        <tr>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="${params?.colspanMonth1}">
                <div><p id="lblMonth1" class="lblMonth1">${params?.month1}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="${params?.colspanMonth2}">
                <div><p id="lblMonth2" class="lblMonth2">${params?.month2}</p></div>
            </th>
        </tr>
        <tr>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate1" class="lblDate1">${params?.date1}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate2" class="lblDate2">${params?.date2}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate3" class="lblDate3">${params?.date3}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate4" class="lblDate4">${params?.date4}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate5" class="lblDate5">${params?.date5}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate6" class="lblDate6">${params?.date6}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate7" class="lblDate7">${params?.date7}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate8" class="lblDate8">${params?.date8}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate9" class="lblDate9">${params?.date9}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate10" class="lblDate10">${params?.date10}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate11" class="lblDate11">${params?.date11}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate12" class="lblDate12">${params?.date12}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate13" class="lblDate13">${params?.date13}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate14" class="lblDate14">${params?.date14}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate15" class="lblDate15">${params?.date15}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate16" class="lblDate16">${params?.date16}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate17" class="lblDate17">${params?.date17}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate18" class="lblDate18">${params?.date18}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate19" class="lblDate19">${params?.date19}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate20" class="lblDate20">${params?.date20}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate21" class="lblDate21">${params?.date21}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate22" class="lblDate22">${params?.date22}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate23" class="lblDate23">${params?.date23}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate24" class="lblDate24">${params?.date24}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate25" class="lblDate25">${params?.date25}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate26" class="lblDate26">${params?.date26}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate27" class="lblDate27">${params?.date27}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate28" class="lblDate28">${params?.date28}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate29" class="lblDate29">${params?.date29}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate30" class="lblDate30">${params?.date30}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle">
                <div><p id="lblDate31" class="lblDate31">${params?.date31}</p></div>
            </th>
        </tr>

	</thead>
    <tfoot>
    <tr><td colspan="4" rowspan="1" style="width: 686.883px;"><span>Total Jumlah Unit Booking</span></td><td rowspan="1" colspan="1" style="width: 114.883px;"><span class="pull-right numeric"></span></td></tr>
    <tr><td colspan="4" rowspan="1" style="width: 686.883px;"><span>Total Sisa Waktu yang Tersedia</span></td><td rowspan="1" colspan="1" style="width: 114.883px;"><span class="pull-right numeric"></span></td></tr>
    </tfoot>
</table>

<g:javascript>
var JPBTable;
var reloadJPBTable;
$(function(){
	
	reloadJPBTable = function() {
		JPBTable.fnDraw();
	}

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	JPBTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	JPBTable = $('#JPB_datatablesPerMonth').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

                $(nRow).children().each(function(index, td) {
                    if(index >= 3 && index <= 22) {
                        if ($(td).html() === "BREAK") {
                            $(td).css("background-color", "grey");
                        }
                        if ($(td).html() === "V1") {
                            $(td).attr("class","board-car1");
                        }
                        if ($(td).html() === "V") {
                            $(td).css("background-color", "#078DC6");
                        }
                        if ($(td).html() === "W1") {
                            $(td).attr("class","board-car2");
                        }
                        if ($(td).html() === "W") {
                            $(td).css("background-color", "#FFDE00");
                        }
                        if ($(td).html() === "X1") {
                            $(td).attr("class","board-car3");
                        }
                        if ($(td).html() === "X") {
                            $(td).css("background-color", "#06B33A");
                        }
                        if ($(td).html() === "Y1") {
                            $(td).attr("class","board-car4");
                        }
                        if ($(td).html() === "Y") {
                            $(td).css("background-color", "#FF3229");
                        }
                        if ($(td).html() === "Z1") {
                            $(td).attr("class","board-car5");
                        }
                        if ($(td).html() === "Z") {
                            $(td).css("background-color", "#ffe2e2");
                        }
                        $(td).html("");
                    }
                });

			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesListMonth")}",
		"aoColumns": [

{
	"sName": "foreman",
	"mDataProp": "foreman",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"mRender": function ( data, type, row ) {
	    return '<a href="#" id="foremanMonth'+row['foremanId']+'" onmouseover="getForemanInfoMonth('+row['foremanId']+')" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
	},
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "group",
	"mDataProp": "group",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "teknisi",
	"mDataProp": "teknisi",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"mRender": function ( data, type, row ) {
	    return '<a href="#" id="teknisiMonth'+row['teknisiId']+'" onmouseover="getTeknisiInfoMonth('+row['teknisiId']+','+row['jpbId']+')" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
	},
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "stall",
	"mDataProp": "stall",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "date1",
	"mDataProp": "date1",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "date2",
	"mDataProp": "date2",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "date3",
	"mDataProp": "date3",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "date4",
	"mDataProp": "date4",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "date5",
	"mDataProp": "date5",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "date6",
	"mDataProp": "date6",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "date7",
	"mDataProp": "date7",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "date8",
	"mDataProp": "date8",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "date9",
	"mDataProp": "date9",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "date10",
	"mDataProp": "date10",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "date11",
	"mDataProp": "date11",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "date12",
	"mDataProp": "date12",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "date13",
	"mDataProp": "date13",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "date14",
	"mDataProp": "date14",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "date15",
	"mDataProp": "date15",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "date16",
	"mDataProp": "date16",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "date17",
	"mDataProp": "date17",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "date18",
	"mDataProp": "date18",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "date19",
	"mDataProp": "date19",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "date20",
	"mDataProp": "date20",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "date21",
	"mDataProp": "date21",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "date22",
	"mDataProp": "date22",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "date23",
	"mDataProp": "date23",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "date24",
	"mDataProp": "date24",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "date25",
	"mDataProp": "date25",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "date26",
	"mDataProp": "date26",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "date27",
	"mDataProp": "date27",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "date28",
	"mDataProp": "date28",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "date29",
	"mDataProp": "date29",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "date30",
	"mDataProp": "date30",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
},
{
	"sName": "date31",
	"mDataProp": "date31",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

            var tglView = $('#search_tglViewMonth').val();
            var tglViewDay = $('#search_tglViewMonth_day').val();
            var tglViewMonth = $('#search_tglViewMonth_month').val();
            var tglViewYear = $('#search_tglViewMonth_year').val();
            //alert("tglView=" + tglView + " tglViewDay=" + tglViewDay);
            if(tglView){
                aoData.push(
                        {"name": 'sCriteria_tglViewMonth', "value": "date.struct"},
                        {"name": 'sCriteria_tglViewMonth_dp', "value": tglView},
                        {"name": 'sCriteria_tglViewMonth_day', "value": tglViewDay},
                        {"name": 'sCriteria_tglViewMonth_month', "value": tglViewMonth},
                        {"name": 'sCriteria_tglViewMonth_year', "value": tglViewYear}
                );
            }
            aoData.push(
                    {"name": 'aksi', "value": "view"}
            );

            $.ajax({ "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData ,
                "success": function (json) {
                    fnCallback(json);
                   },
                "complete": function () {
                   }
            });
		}			
	});

    new FixedColumns( JPBTable, {
		"iLeftWidth": 150,
		//"iLeftColumns": 2,
		"fnDrawCallback": function ( left, right ) {
			var that = this, groupVal = null, matches = 0, heights = [], index = -1;

			// Get the heights of the cells and remove redundant ones
			$('tbody tr td', left.body).each( function ( i ) {
				var currVal = this.innerHTML;

				// Reset values on new cell data.
				if (currVal != groupVal) {
					groupVal = currVal;
					index++;
					heights[index] = 0;
					matches = 0;
				} else  {
					matches++;
				}

				heights[ index ] += $(this.parentNode).height();
				if ( currVal == groupVal && matches > 0 ) {
					this.parentNode.parentNode.removeChild(this.parentNode);
				}
			} );

			// Now set the height of the cells which remain, from the summed heights
			$('tbody tr td', left.body).each( function ( i ) {
				that.fnSetRowHeight( this.parentNode, heights[ i ] );
			} );
		}
	} );

});


</g:javascript>


			
