
<%@ page import="com.kombos.board.JPB" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'JPB.label', default: 'JPB')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			var showAddJobModal;
			var showMoveJobModal;
			var showAddTimeModal;
			var showReScheduleModal;
			$(function(){

			    $('#bp_per_hari').load('${request.contextPath}/JPBBPView/perDay');

                $('#tab_bp_per_hari').click(function() {
                    $('#bp_per_hari').load('${request.contextPath}/JPBBPView/perDay');
                });

                $('#tab_bp_per_minggu').click(function() {
                    $('#bp_per_minggu').load('${request.contextPath}/JPBBPView/perWeek');
                });

                $('#tab_bp_per_bulan').click(function() {
                    $('#bp_per_bulan').load('${request.contextPath}/JPBBPView/perMonth');
                });


                showAddJobModal = function(){

                }

                showAddTimeModal = function(){

                }

                showMoveJobModal = function(){

                }

                showReScheduleModal = function(){

                }

                 var reloadAll = function(){
                    var table = $('#JPB_datatables');
                    if(table.length > 0){
                        asbView();
                        setTimeout(function(){reloadAll()}, 10000);
                    } else {
                        var tableW = $('#JPB_datatablesPerWeek');
                        if(tableW.length > 0){
                            asbViewW();
                            setTimeout(function(){reloadAll()}, 10000);
                        } else {
                            var tableM = $('#JPB_datatablesPerMonth');
                            if(tableM.length > 0){
                                asbViewM();
                                setTimeout(function(){reloadAll()}, 10000);
                            }
                        }
                    }
                }
               // setTimeout(function(){reloadAll()}, 10000);


});
</g:javascript>
	</head>
	<body>
    <div class="navbar box-header no-border">
   		<span class="pull-left">Job Progress Board (JPB) Body & Paint</span>
   	</div>

    <div class="box">
           <div class="tabbable control-group">
               <ul class="nav nav-tabs">
                   <li id="tab_bp_per_hari" class="active">
                       <a href="#bp_per_hari" data-toggle="tab">Per Hari</a>
                   </li>
                   <li id="tab_bp_per_minggu" class="">
                       <a href="#bp_per_minggu" data-toggle="tab">Per Minggu</a>
                   </li>
                   <li id="tab_bp_per_bulan" class="">
                       <a href="#bp_per_bulan" data-toggle="tab">Per Bulan</a>
                   </li>
               </ul>

               <div class="tab-content">
                   <div class="tab-pane active" id="bp_per_hari" />
                   <div class="tab-pane" id="bp_per_minggu" />
                   <div class="tab-pane" id="bp_per_bulan" />
               </div>
           </div>
   	</div>
    <div id="showModal" class="modal fade">
        <div class="modal-dialog" style="width: 600px;">
            <div class="modal-content" style="width: 600px;">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 600px;">
                    <div id="modalContent"/>
                    %{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}%
                    <div class="iu-content"></div>

                </div>
                <!-- dialog buttons -->
                <div class="modal-footer">
                    <g:field type="button" onclick="addETA();" class="btn btn-primary create" name="tambah" id="tambah" value="${message(code: 'default.button.tambahETA.label', default: 'Save')}" />
                    <button id='closeTambahEta' type="button" class="btn btn-primary">Close</button>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
