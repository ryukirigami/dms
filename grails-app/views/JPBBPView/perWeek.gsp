
<%@ page import="com.kombos.board.JPB" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'JPB.label', default: 'JPB GR')}" />
		%{--<title><g:message code="default.list.label" args="[entityName]" /></title>--}%
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			var asbViewW;
			$(function(){ 
			         
	$('#search_tglViewWeek').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tglViewWeek_day').val(newDate.getDate());
			$('#search_tglViewWeek_month').val(newDate.getMonth()+1);
			$('#search_tglViewWeek_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
//			var oTable = $('#JPB_datatables').dataTable();
//            oTable.fnReloadAjax();
	});

				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('JPB');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('JPB', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('JPB', '${request.contextPath}/JPB/massdelete', reloadJPBTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('JPB','${request.contextPath}/JPB/show/'+id);
				};
				
				edit = function(id) {
					editInstance('JPB','${request.contextPath}/JPB/edit/'+id);
				};

   	asbViewW = function(){
        var oTable = $('#JPB_datatables').dataTable();
        oTable.fnReloadAjax();
        var from = $("#search_tglViewWeek").val().split("/");
        var theDate = new Date();
        theDate.setFullYear(from[2], from[1] - 1, from[0]);
        //alert("theDate=" + theDate);
        var m_names = new Array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
        var formattedDate =  m_names[theDate.getMonth()] + " " + theDate.getFullYear();
        document.getElementById("lblTglView").innerHTML = formattedDate;
        //Sunday is 0, Monday is 1, and so on
        var day = theDate.getDay();

        var dayMon, dayTue, dayWed, dayThu, dayFri, daySat, daySun;
        console.log("day=" + day + " dayMon=" + dayMon);
        if (day == 1) {
            dayMon = day; dayTue = day + 1; dayWed = day + 2; dayThu = day + 3;
            dayFri = day + 4; daySat = day + 5; daySun = day + 6;
        } else if (day == 2) {
            dayMon = day - 1; dayTue = day; dayWed = day + 1; dayThu = day + 2;
            dayFri = day + 3; daySat = day + 4; daySun = day + 5;
        } else if (day == 3) {
            dayMon = day - 2; dayTue = day - 1; dayWed = day; dayThu = day + 1;
            dayFri = day + 2; daySat = day + 3; daySun = day + 4;
        } else if (day == 4) {
            dayMon = day - 3; dayTue = day - 2; dayWed = day - 1; dayThu = day;
            dayFri = day + 1; daySat = day + 2; daySun = day + 3;
        } else if (day == 5) {
            dayMon = day - 4; dayTue = day - 3; dayWed = day - 2; dayThu = day - 1;
            dayFri = day; daySat = day + 1; daySun = day + 2;
        } else if (day == 6) {
            dayMon = day - 5; dayTue = day - 4; dayWed = day - 3; dayThu = day - 2;
            dayFri = day - 1; daySat = day; daySun = day + 1;
        } else {
            dayMon = day - 6; dayTue = day - 5; dayWed = day - 4; dayThu = day - 3;
            dayFri = day - 2; daySat = day - 1; daySun = day;
        }
        console.log("day=" + day + " dayMon=" + dayMon);
        document.getElementById("lblSenin").innerHTML = dayMon;
        document.getElementById("lblSelasa").innerHTML = dayTue;
        document.getElementById("lblRabu").innerHTML = dayWed;
        document.getElementById("lblKamis").innerHTML = dayThu;
        document.getElementById("lblJumat").innerHTML = dayFri;
        document.getElementById("lblSabtu").innerHTML = daySat;
        document.getElementById("lblMinggu").innerHTML = daySun;
   	}
});

</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		%{--<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>--}%
		%{--<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>--}%
	</div>

	<div class="box">
        <table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">
            <tr>
                <td style="width: 40%;vertical-align: top;">
                    <div class="box">
                        <legend style="font-size: small">View JPB</legend>
                        <table style="width: 100%;border: 0px">
                            <tr>
                                <td>
                                    <label class="control-label" for="lbl_rcvDate">
                                        Nama Workshop
                                    </label>
                                </td>
                                <td colspan="2">
                                    <div id="lbl_companyDealer" class="controls">
                                        <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                            <g:select name="companyDealer.id" id="input_companyDealer" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                                        </g:if>
                                        <g:else>
                                            <g:select name="companyDealer.id" id="input_companyDealer" readonly="" disabled="" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                                        </g:else>
                                        %{--<g:select id="input_companyDealer" name="companyDealer.id" from="${com.kombos.administrasi.CompanyDealer.list()}" optionKey="id" required="" class="many-to-one"/>--}%
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label class="control-label" for="lbl_tglViewWeek">
                                        Tanggal
                                    </label>
                                </td>
                                <td>
                                    <div id="lbl_tglViewWeek" class="controls">
                                        <div id="filter_tglViewWeek" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                                             <input type="hidden" name="search_tglViewWeek" value="date.struct">
                                             <input type="hidden" name="search_tglViewWeek_day" id="search_tglViewWeek_day" value="">
                                             <input type="hidden" name="search_tglViewWeek_month" id="search_tglViewWeek_month" value="">
                                             <input type="hidden" name="search_tglViewWeek_year" id="search_tglViewWeek_year" value="">
                                             <input type="text" data-date-format="dd/mm/yyyy" name="search_tglViewWeek_dp" value="" id="search_tglViewWeek" class="search_init" style="width: 160px" >
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <button class="btn btn-primary" id="buttonView" onclick="asbViewW()" style="width: 120px" >View</button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td style="width: 60%;vertical-align: top;padding-left: 10px;">

                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div id="JPB-table">
                        <g:if test="${flash.message}">
                            <div class="message" role="status">
                                ${flash.message}
                            </div>
                        </g:if>

                        <g:render template="dataTablesPerWeek" />
                    </div>
                    </td>
                </tr>
        </table>
	</div>
</body>
</html>
