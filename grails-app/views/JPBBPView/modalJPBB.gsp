<%@ page import="com.kombos.parts.ETA" %>

<r:require modules="baseapplayout, baseapplist,autoNumeric" />
<script type="text/javascript">
    jQuery(function($) {
        $('.auto').autoNumeric('init');
    });
</script>

<div class="navbar box-header no-border">
    ${typeModal}
</div>
<table class="table table-bordered table-hover">
    <tbody>
    <tr>
        <td class="span2" style="text-align: right;">
            <label class="control-label">
                <g:message code="jpb.Stall1.label" default="Stall 1" />

            </label>

        </td>

        <td class="span3">
                <g:select name="idGoods" id="idGoods" from="${com.kombos.administrasi.Stall.list()}" optionKey="id" />
        </td>
    </tr>
    <tr>
        <td class="span2" style="text-align: right;">
            <label class="control-label">
                <g:message code="jpb.stall1.Tanggal" default="Tanggal/Jam" />
            </label>
        </td>
        <td class="span3">
            <div class="controls">
                <div style="width:200px">
                    <g:datePicker name="stall1Tgl" id="stall1Tgl" precision="day"  format="dd-mm-yyyy"/>
                </div>
                <br/>
                H :
                <select id="jamStall1Tgl" name="jamStall1Tgl"  style="width: 60px" required="">
                    %{
                        for (int i=0;i<24;i++){
                            if(i<10){

                                out.println('<option value="'+i+'">0'+i+'</option>');


                            } else {

                                out.println('<option value="'+i+'">'+i+'</option>');

                            }

                        }
                    }%
                </select>
                <select id="menitStall1Tgl" name="menitStall1Tgl" style="width: 60px" required="">
                    %{
                        for (int i=0;i<60;i++){
                            if(i<10){

                                out.println('<option value="'+i+'">0'+i+'</option>');


                            } else {

                                out.println('<option value="'+i+'">'+i+'</option>');

                            }
                        }
                    }%
                </select> m
            </div>
        </td>
    </tr>
    <tr>
        <td class="span2" style="text-align: right;">
            <label class="control-label">
                <g:message code="jpb.Stall1.label" default="Stall 2" />

            </label>

        </td>

        <td class="span3">
            <g:select name="stall2" id="stall2" from="${com.kombos.administrasi.Stall.list()}" optionKey="id" />
        </td>
    </tr>
    <tr>
        <td class="span2" style="text-align: right;">
            <label class="control-label">
                <g:message code="jpb.stall1.Tanggal" default="Tanggal/Jam" />
            </label>
        </td>
        <td class="span3">
            <div class="controls">
                <div style="width:200px">
                    <g:datePicker name="stall2Tgl" id="stall2Tgl" precision="day"  format="dd-mm-yyyy"/>
                </div>
                <br/>
                H :
                <select id="jamStall2Tgl" name="jamStall2Tgl"  style="width: 60px" required="">
                    %{
                        for (int i=0;i<24;i++){
                            if(i<10){

                                out.println('<option value="'+i+'">0'+i+'</option>');


                            } else {

                                out.println('<option value="'+i+'">'+i+'</option>');

                            }

                        }
                    }%
                </select>
                <select id="menitStall2Tgl" name="menitStall2Tgl" style="width: 60px" required="">
                    %{
                        for (int i=0;i<60;i++){
                            if(i<10){

                                out.println('<option value="'+i+'">0'+i+'</option>');


                            } else {

                                out.println('<option value="'+i+'">'+i+'</option>');

                            }
                        }
                    }%
                </select> m
            </div>
        </td>
    </tr>
    <tr>
        <td class="span2" style="text-align: right;">
            <label class="control-label">
                <g:message code="jpb.Stall1.label" default="Stall 3" />

            </label>

        </td>

        <td class="span3">
            <g:select name="stall3" id="stall3" from="${com.kombos.administrasi.Stall.list()}" optionKey="id" />
        </td>
    </tr>
    <tr>
        <td class="span2" style="text-align: right;">
            <label class="control-label">
                <g:message code="jpb.stall1.Tanggal" default="Tanggal/Jam" />
            </label>
        </td>
        <td class="span3">
            <div class="controls">
                <div style="width:200px">
                    <g:datePicker name="stall3Tgl" id="stall3Tgl" precision="day"  format="dd-mm-yyyy"/>
                </div>
                <br/>
                H :
                <select id="jamStall3Tgl" name="jamStall3Tgl"  style="width: 60px" required="">
                    %{
                        for (int i=0;i<24;i++){
                            if(i<10){

                                out.println('<option value="'+i+'">0'+i+'</option>');


                            } else {

                                out.println('<option value="'+i+'">'+i+'</option>');

                            }

                        }
                    }%
                </select>
                <select id="menitStall3Tgl" name="menitStall3Tgl" style="width: 60px" required="">
                    %{
                        for (int i=0;i<60;i++){
                            if(i<10){

                                out.println('<option value="'+i+'">0'+i+'</option>');


                            } else {

                                out.println('<option value="'+i+'">'+i+'</option>');

                            }
                        }
                    }%
                </select> m
            </div>
        </td>
    </tr>
    <tr>
        <td class="span2" style="text-align: right;">
            <label class="control-label">
                <g:message code="jpb.Stall1.label" default="Stall 3" />

            </label>

        </td>

        <td class="span3">
            <g:select name="stall3" id="stall3" from="${com.kombos.administrasi.Stall.list()}" optionKey="id" />
        </td>
    </tr>
    <tr>
        <td class="span2" style="text-align: right;">
            <label class="control-label">
                <g:message code="jpb.stall4.Tanggal" default="Tanggal/Jam" />
            </label>
        </td>
        <td class="span3">
            <div class="controls">
                <div style="width:200px">
                    <g:datePicker name="stall4Tgl" id="stall4Tgl" precision="day"  format="dd-mm-yyyy"/>
                </div>
                <br/>
                H :
                <select id="jamStall4Tgl" name="jamStall4Tgl"  style="width: 60px" required="">
                    %{
                        for (int i=0;i<24;i++){
                            if(i<10){

                                out.println('<option value="'+i+'">0'+i+'</option>');


                            } else {

                                out.println('<option value="'+i+'">'+i+'</option>');

                            }

                        }
                    }%
                </select>
                <select id="menitStall4Tgl" name="menitStall4Tgl" style="width: 60px" required="">
                    %{
                        for (int i=0;i<60;i++){
                            if(i<10){

                                out.println('<option value="'+i+'">0'+i+'</option>');


                            } else {

                                out.println('<option value="'+i+'">'+i+'</option>');

                            }
                        }
                    }%
                </select> m
            </div>
        </td>
    </tr>


    </tbody>
</table>
<div class="modal-footer">
</div>

<g:javascript>

    $(function(){

        $("#tglETA_day").css({width : '100px'});
        $("#tglETA_month").css({width : '100px'});
        $("#tglETA_year").css({width : '100px'});

    });
</g:javascript>


