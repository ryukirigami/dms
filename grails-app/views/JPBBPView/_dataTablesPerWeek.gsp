
<%@ page import="com.kombos.board.JPB" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="JPB_datatablesPerWeek" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
        <tr>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" rowspan="3" >
                <div>FOREMAN</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="3" >
                <div>JOB PROGRESS BOARD (JPB)<br />BODY & PAINT</div>
            </th>

            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="14">
                <div><p id="lblTglView" class="lblTglView">${params?.lblTglView}</p></div>
            </th>
        </tr>
        <tr>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" rowspan="2">
                <div>Group</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" rowspan="2">
                <div>Teknisi</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" rowspan="2">
                <div>Stall</div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>Senin <p id="lblSenin" class="lblSenin">${params?.lblSenin}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>Selasa <p id="lblSelasa" class="lblSelasa">${params?.lblSelasa}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>Rabu <p id="lblRabu" class="lblRabu">${params?.lblRabu}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>Kamis <p id="lblKamis" class="lblKamis">${params?.lblKamis}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>Jumat <p id="lblJumat" class="lblJumat">${params?.lblJumat}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>Sabtu <p id="lblSabtu" class="lblSabtu">${params?.lblSabtu}</p></div>
            </th>
            <th style="border-bottom: none;padding: 5px; text-align: center; vertical-align: middle" colspan="2">
                <div>Minggu <p id="lblMinggu" class="lblMinggu">${params?.lblMinggu}</p></div>
            </th>
        </tr>
        <tr>
            <th style="border-bottom: none;padding: 5px;">
                <div>&nbsp;</div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div> </div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div> </div>
            </th>
        </tr>

	</thead>
    <tfoot>
    <tr><td colspan="4" rowspan="1" style="width: 686.883px;"><span>Total Jumlah Unit Booking</span></td><td rowspan="1" colspan="1" style="width: 114.883px;"><span class="pull-right numeric"></span></td></tr>
    <tr><td colspan="4" rowspan="1" style="width: 686.883px;"><span>Total Sisa Waktu yang Tersedia</span></td><td rowspan="1" colspan="1" style="width: 114.883px;"><span class="pull-right numeric"></span></td></tr>
    </tfoot>
</table>

<g:javascript>
var JPBTable;
var reloadJPBTable;
$(function(){
	
	reloadJPBTable = function() {
		JPBTable.fnDraw();
	}

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	JPBTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	JPBTable = $('#JPB_datatablesPerWeek').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

                $(nRow).children().each(function(index, td) {
                    if(index >= 3 && index <= 22) {
                        if ($(td).html() === "BREAK") {
                            $(td).css("background-color", "grey");
                        }
                        if ($(td).html() === "V1") {
                            $(td).attr("class","board-car1");
                        }
                        if ($(td).html() === "V") {
                            $(td).css("background-color", "#078DC6");
                        }
                        if ($(td).html() === "W1") {
                            $(td).attr("class","board-car2");
                        }
                        if ($(td).html() === "W") {
                            $(td).css("background-color", "#FFDE00");
                        }
                        if ($(td).html() === "X1") {
                            $(td).attr("class","board-car3");
                        }
                        if ($(td).html() === "X") {
                            $(td).css("background-color", "#06B33A");
                        }
                        if ($(td).html() === "Y1") {
                            $(td).attr("class","board-car4");
                        }
                        if ($(td).html() === "Y") {
                            $(td).css("background-color", "#FF3229");
                        }
                        if ($(td).html() === "Z1") {
                            $(td).attr("class","board-car5");
                        }
                        if ($(td).html() === "Z") {
                            $(td).css("background-color", "#ffe2e2");
                        }
                        $(td).html("");
                    }
                });

			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesListWeek")}",
		"aoColumns": [

{
	"sName": "foreman",
	"mDataProp": "foreman",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"mRender": function ( data, type, row ) {
	    return '<a href="#" id="foremanWeek'+row['foremanId']+'" onmouseover="getForemanInfoWeek('+row['foremanId']+')" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
	},
	"bVisible": true
}

,

{
	"sName": "group",
	"mDataProp": "group",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "teknisi",
	"mDataProp": "teknisi",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"mRender": function ( data, type, row ) {
	    return '<a href="#" id="teknisiWeek'+row['teknisiId']+'" onmouseover="getTeknisiInfoWeek('+row['teknisiId']+','+row['jpbId']+')" data-toggle="tooltip" data-placement="right" alt="test" >'+data+'</a>'
	},
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "stall",
	"mDataProp": "stall",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "seninFirst",
	"mDataProp": "seninFirst",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "seninSecond",
	"mDataProp": "seninSecond",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "selasaFirst",
	"mDataProp": "selasaFirst",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "selasaSecond",
	"mDataProp": "selasaSecond",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "rabuFirst",
	"mDataProp": "rabuFirst",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "rabuSecond",
	"mDataProp": "rabuSecond",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "kamisFirst",
	"mDataProp": "kamisFirst",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "kamisSecond",
	"mDataProp": "kamisSecond",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jumatFirst",
	"mDataProp": "jumatFirst",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "jumatSecond",
	"mDataProp": "jumatSecond",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "sabtuFirst",
	"mDataProp": "sabtuFirst",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "sabtuSecond",
	"mDataProp": "sabtuSecond",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "mingguFirst",
	"mDataProp": "mingguFirst",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
,
{
	"sName": "mingguSecond",
	"mDataProp": "mingguSecond",
	"bSearchable": true,
	"bSortable": true,
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

            var tglView = $('#search_tglViewWeek').val();
            var tglViewDay = $('#search_tglViewWeek_day').val();
            var tglViewMonth = $('#search_tglViewWeek_month').val();
            var tglViewYear = $('#search_tglViewWeek_year').val();
            //alert("tglView=" + tglView + " tglViewDay=" + tglViewDay);
            if(tglView){
                aoData.push(
                        {"name": 'sCriteria_tglViewWeek', "value": "date.struct"},
                        {"name": 'sCriteria_tglViewWeek_dp', "value": tglView},
                        {"name": 'sCriteria_tglViewWeek_day', "value": tglViewDay},
                        {"name": 'sCriteria_tglViewWeek_month', "value": tglViewMonth},
                        {"name": 'sCriteria_tglViewWeek_year', "value": tglViewYear}
                );
            }
            aoData.push(
                    {"name": 'aksi', "value": "view"}
            );

            $.ajax({ "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData ,
                "success": function (json) {
                    fnCallback(json);
                   },
                "complete": function () {
                   }
            });
		}			
	});

    new FixedColumns( JPBTable, {
		"iLeftWidth": 150,
		//"iLeftColumns": 2,
		"fnDrawCallback": function ( left, right ) {
			var that = this, groupVal = null, matches = 0, heights = [], index = -1;

			// Get the heights of the cells and remove redundant ones
			$('tbody tr td', left.body).each( function ( i ) {
				var currVal = this.innerHTML;

				// Reset values on new cell data.
				if (currVal != groupVal) {
					groupVal = currVal;
					index++;
					heights[index] = 0;
					matches = 0;
				} else  {
					matches++;
				}

				heights[ index ] += $(this.parentNode).height();
				if ( currVal == groupVal && matches > 0 ) {
					this.parentNode.parentNode.removeChild(this.parentNode);
				}
			} );

			// Now set the height of the cells which remain, from the summed heights
			$('tbody tr td', left.body).each( function ( i ) {
				that.fnSetRowHeight( this.parentNode, heights[ i ] );
			} );
		}
	} );

});


</g:javascript>


			
