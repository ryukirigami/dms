<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
<div>
    <g:form action='aclEntrySearch' name='aclEntrySearchForm'>

        <table>
            <tbody>
            <tr>
                <td><g:message code='aclEntry.aclObjectIdentity.label' default='No DO'/>:</td>
                <td style="padding-right: 100px;"><g:textField name='aclObjectIdentity' size='50' maxlength='255' value='${aclObjectIdentity}'/></td>
            </tr>
            <tr>
                <td><g:message code='aclEntry.aclObjectIdentity.label' default='Catatan'/>:</td>
                <td style="padding-right: 100px;"><g:textArea name='aclObjectIdentity' size='50' maxlength='255' value='${aclObjectIdentity}'/></td>
            </tr>

            </tbody>
        </table>
    </g:form>

    <table id="jobSuggestion" class="table table-bordered table-hover">
        <thead><th>Kode Material</th><th>Nama Material</th><th>Banyaknya Material</th><th>Harga PerUnit</th><th>Satuan</th> <th>Pemotongan Harga (%)</th> <th>Jumlah Material</th></thead>
        <tbody><tr><td> A01 </td> <td> Batery x</td> <td> 12 </td> <td>Rp 2000</td> <td>Piecs</td> <td> 0 </td> <td> 12.000 </td> </tr>
        <tr><td> A02 </td> <td> Battery y </td> <td> 12 </td> <td>Rp 2000</td> <td>Piecs</td> <td> 0 </td> <td> 18.000 </td> </tr>
        <tr><th colspan="6"> Total </th>  <th> 20.000 </th> </tr>
        </tbody>
    </table>
    <g:submitButton class="btn btn-primary create" name="Simpan" value="Simpan" />
    <g:submitButton class="btn btn-primary create" name="Batalkan" value="Batalkan" />

</div>
</body>
</html>