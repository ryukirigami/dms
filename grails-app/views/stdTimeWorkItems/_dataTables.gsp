
<%@ page import="com.kombos.administrasi.StdTimeWorkItems" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>
<script type="text/javascript">
    jQuery(function($) {
        $('.auto').autoNumeric('init',{
            vMin:'0',
            aSep:'',
            mDec:'2'
        });
    });
</script>

<table id="stdTimeWorkItems_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="stdTimeWorkItems.m041TanggalBerlaku.label" default="Tanggal Berlaku" /></div>
            </th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stdTimeWorkItems.workItems.label" default="Work Items" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stdTimeWorkItems.m041RdanR.label" default="Removal & Reinstall" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stdTimeWorkItems.m041Replace.label" default="Replace" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stdTimeWorkItems.m041Overhaul.label" default="Overhaul" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stdTimeWorkItems.companyDealer.label" default="Company Dealer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stdTimeWorkItems.createdBy.label" default="Created By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stdTimeWorkItems.updatedBy.label" default="Updated By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stdTimeWorkItems.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>

		
		</tr>
		<tr>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_m041TanggalBerlaku" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
                    <input type="hidden" name="search_m041TanggalBerlaku" value="date.struct">
                    <input type="hidden" name="search_m041TanggalBerlaku_day" id="search_m041TanggalBerlaku_day" value="">
                    <input type="hidden" name="search_m041TanggalBerlaku_month" id="search_m041TanggalBerlaku_month" value="">
                    <input type="hidden" name="search_m041TanggalBerlaku_year" id="search_m041TanggalBerlaku_year" value="">
                    <input type="text" data-date-format="dd/mm/yyyy" name="search_m041TanggalBerlaku_dp" value="" id="search_m041TanggalBerlaku" class="search_init">
                </div>
            </th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_workItems" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_workItems" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m041RdanR" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m041RdanR" class="search_init auto" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m041Replace" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m041Replace" class="search_init auto" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m041Overhaul" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m041Overhaul" class="search_init auto" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_companyDealer" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_createdBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_updatedBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var stdTimeWorkItemsTable;
var reloadStdTimeWorkItemsTable;
$(function(){
	
	reloadStdTimeWorkItemsTable = function() {
		stdTimeWorkItemsTable.fnDraw();
	}

    var recordsstdTimeWorkItemsperpage = [];
    var anStdTimeWorkItemsSelected;
    var jmlRecStdTimeWorkItemsPerPage=0;
    var id;

	$('#search_m041TanggalBerlaku').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m041TanggalBerlaku_day').val(newDate.getDate());
			$('#search_m041TanggalBerlaku_month').val(newDate.getMonth()+1);
			$('#search_m041TanggalBerlaku_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			stdTimeWorkItemsTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	stdTimeWorkItemsTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	stdTimeWorkItemsTable = $('#stdTimeWorkItems_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsStdTimeWorkItems = $("#stdTimeWorkItems_datatables tbody .row-select");
            var jmlStdTimeWorkItemsCek = 0;
            var nRow;
            var idRec;
            rsStdTimeWorkItems.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsstdTimeWorkItemsperpage[idRec]=="1"){
                    jmlStdTimeWorkItemsCek = jmlStdTimeWorkItemsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsstdTimeWorkItemsperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecStdTimeWorkItemsPerPage = rsStdTimeWorkItems.length;
            if(jmlStdTimeWorkItemsCek==jmlRecStdTimeWorkItemsPerPage && jmlRecStdTimeWorkItemsPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
        "fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m041TanggalBerlaku",
	"mDataProp": "m041TanggalBerlaku",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}

,

{
	"sName": "workItems",
	"mDataProp": "workItems",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"500px",
	"bVisible": true
}

,

{
	"sName": "m041RdanR",
	"mDataProp": "m041RdanR",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}

,

{
	"sName": "m041Replace",
	"mDataProp": "m041Replace",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}

,

{
	"sName": "m041Overhaul",
	"mDataProp": "m041Overhaul",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}

,

{
	"sName": "companyDealer",
	"mDataProp": "companyDealer",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "createdBy",
	"mDataProp": "createdBy",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "updatedBy",
	"mDataProp": "updatedBy",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m041TanggalBerlaku = $('#search_m041TanggalBerlaku').val();
						var m041TanggalBerlakuDay = $('#search_m041TanggalBerlaku_day').val();
						var m041TanggalBerlakuMonth = $('#search_m041TanggalBerlaku_month').val();
						var m041TanggalBerlakuYear = $('#search_m041TanggalBerlaku_year').val();

						if(m041TanggalBerlaku){
							aoData.push(
									{"name": 'sCriteria_m041TanggalBerlaku', "value": "date.struct"},
									{"name": 'sCriteria_m041TanggalBerlaku_dp', "value": m041TanggalBerlaku},
									{"name": 'sCriteria_m041TanggalBerlaku_day', "value": m041TanggalBerlakuDay},
									{"name": 'sCriteria_m041TanggalBerlaku_month', "value": m041TanggalBerlakuMonth},
									{"name": 'sCriteria_m041TanggalBerlaku_year', "value": m041TanggalBerlakuYear}
							);
						}

						var workItems = $('#filter_workItems input').val();
						if(workItems){
							aoData.push(
									{"name": 'sCriteria_workItems', "value": workItems}
							);
						}

						var m041RdanR = $('#filter_m041RdanR input').val();
						if(m041RdanR){
							aoData.push(
									{"name": 'sCriteria_m041RdanR', "value": m041RdanR}
							);
						}
	
						var m041Replace = $('#filter_m041Replace input').val();
						if(m041Replace){
							aoData.push(
									{"name": 'sCriteria_m041Replace', "value": m041Replace}
							);
						}
	
						var m041Overhaul = $('#filter_m041Overhaul input').val();
						if(m041Overhaul){
							aoData.push(
									{"name": 'sCriteria_m041Overhaul', "value": m041Overhaul}
							);
						}
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {
        $("#stdTimeWorkItems_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsstdTimeWorkItemsperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsstdTimeWorkItemsperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

	$('#stdTimeWorkItems_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsstdTimeWorkItemsperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anStdTimeWorkItemsSelected = stdTimeWorkItemsTable.$('tr.row_selected');
            if(jmlRecStdTimeWorkItemsPerPage == anStdTimeWorkItemsSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsstdTimeWorkItemsperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
