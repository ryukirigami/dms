<%@ page import="com.kombos.administrasi.WorkItems; com.kombos.administrasi.StdTimeWorkItems" %>
<r:require modules="autoNumeric" />
<script type="text/javascript">
    jQuery(function($) {
        $('.auto').autoNumeric('init',{
            vMin:'0',
            aSep:'',
            mDec:'2'
        });
    });
</script>


<div class="control-group fieldcontain ${hasErrors(bean: stdTimeWorkItemsInstance, field: 'm041TanggalBerlaku', 'error')} required">
    <label class="control-label" for="m041TanggalBerlaku">
        <g:message code="stdTimeWorkItems.m041TanggalBerlaku.label" default="Tanggal Berlaku" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <ba:datePicker name="m041TanggalBerlaku" id="m041TanggalBerlaku" precision="day"  value="${stdTimeWorkItemsInstance?.m041TanggalBerlaku}" format="dd/MM/yyyy" required="true" />
    </div>
</div>
<g:javascript>
    document.getElementById('m041TanggalBerlaku').focus();
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: stdTimeWorkItemsInstance, field: 'workItems', 'error')} required">
	<label class="control-label" for="workItems">
		<g:message code="stdTimeWorkItems.workItems.label" default="Work Items" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="workItems" name="workItems.id" from="${WorkItems.createCriteria().list(){order("m039WorkItems", "asc")}}" optionKey="id" required="" value="${stdTimeWorkItemsInstance?.workItems?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stdTimeWorkItemsInstance, field: 'm041RdanR', 'error')} required">
	<label class="control-label" for="m041RdanR">
		<g:message code="stdTimeWorkItems.m041RdanR.label" default="Removal & Reinstall" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m041RdanR" class="auto" value="${fieldValue(bean: stdTimeWorkItemsInstance, field: 'm041RdanR')}" required=""/> Jam
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stdTimeWorkItemsInstance, field: 'm041Replace', 'error')} required">
	<label class="control-label" for="m041Replace">
		<g:message code="stdTimeWorkItems.m041Replace.label" default="Replace" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m041Replace" class="auto" value="${fieldValue(bean: stdTimeWorkItemsInstance, field: 'm041Replace')}" required=""/> Jam
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stdTimeWorkItemsInstance, field: 'm041Overhaul', 'error')} required">
	<label class="control-label" for="m041Overhaul">
		<g:message code="stdTimeWorkItems.m041Overhaul.label" default="Overhaul" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m041Overhaul" class="auto" value="${fieldValue(bean: stdTimeWorkItemsInstance, field: 'm041Overhaul')}" required=""/> Jam
	</div>
</div>
%{--
<div class="control-group fieldcontain ${hasErrors(bean: stdTimeWorkItemsInstance, field: 'companyDealer', 'error')} ">
	<label class="control-label" for="companyDealer">
		<g:message code="stdTimeWorkItems.companyDealer.label" default="Company Dealer" />
		
	</label>
	<div class="controls">
	<g:select id="companyDealer" name="companyDealer.id" from="${com.kombos.administrasi.CompanyDealer.list()}" optionKey="id" value="${stdTimeWorkItemsInstance?.companyDealer?.id}" class="many-to-one" noSelection="['null': '']"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stdTimeWorkItemsInstance, field: 'createdBy', 'error')} ">
	<label class="control-label" for="createdBy">
		<g:message code="stdTimeWorkItems.createdBy.label" default="Created By" />
		
	</label>
	<div class="controls">
	<g:textField name="createdBy" value="${stdTimeWorkItemsInstance?.createdBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stdTimeWorkItemsInstance, field: 'updatedBy', 'error')} ">
	<label class="control-label" for="updatedBy">
		<g:message code="stdTimeWorkItems.updatedBy.label" default="Updated By" />
		
	</label>
	<div class="controls">
	<g:textField name="updatedBy" value="${stdTimeWorkItemsInstance?.updatedBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stdTimeWorkItemsInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="stdTimeWorkItems.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${stdTimeWorkItemsInstance?.lastUpdProcess}"/>
	</div>
</div>
--}%
