

<%@ page import="com.kombos.administrasi.StdTimeWorkItems" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'stdTimeWorkItems.label', default: 'Standart Time Replace & Reinstalling')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteStdTimeWorkItems;

$(function(){ 
	deleteStdTimeWorkItems=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/stdTimeWorkItems/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadStdTimeWorkItemsTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-stdTimeWorkItems" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="stdTimeWorkItems"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${stdTimeWorkItemsInstance?.workItems}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="workItems-label" class="property-label"><g:message
					code="stdTimeWorkItems.workItems.label" default="Work Items" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="workItems-label">
						%{--<ba:editableValue
								bean="${stdTimeWorkItemsInstance}" field="workItems"
								url="${request.contextPath}/StdTimeWorkItems/updatefield" type="text"
								title="Enter workItems" onsuccess="reloadStdTimeWorkItemsTable();" />--}%
							
								${stdTimeWorkItemsInstance?.workItems?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stdTimeWorkItemsInstance?.m041TanggalBerlaku}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m041TanggalBerlaku-label" class="property-label"><g:message
					code="stdTimeWorkItems.m041TanggalBerlaku.label" default="Tanggal Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m041TanggalBerlaku-label">
						%{--<ba:editableValue
								bean="${stdTimeWorkItemsInstance}" field="m041TanggalBerlaku"
								url="${request.contextPath}/StdTimeWorkItems/updatefield" type="text"
								title="Enter m041TanggalBerlaku" onsuccess="reloadStdTimeWorkItemsTable();" />--}%
							
								<g:formatDate date="${stdTimeWorkItemsInstance?.m041TanggalBerlaku}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stdTimeWorkItemsInstance?.m041RdanR}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m041RdanR-label" class="property-label"><g:message
					code="stdTimeWorkItems.m041RdanR.label" default="R dan R" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m041RdanR-label">
						%{--<ba:editableValue
								bean="${stdTimeWorkItemsInstance}" field="m041RdanR"
								url="${request.contextPath}/StdTimeWorkItems/updatefield" type="text"
								title="Enter m041RdanR" onsuccess="reloadStdTimeWorkItemsTable();" />--}%
							
								<g:fieldValue bean="${stdTimeWorkItemsInstance}" field="m041RdanR"/> Jam
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stdTimeWorkItemsInstance?.m041Replace}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m041Replace-label" class="property-label"><g:message
					code="stdTimeWorkItems.m041Replace.label" default="Replace" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m041Replace-label">
						%{--<ba:editableValue
								bean="${stdTimeWorkItemsInstance}" field="m041Replace"
								url="${request.contextPath}/StdTimeWorkItems/updatefield" type="text"
								title="Enter m041Replace" onsuccess="reloadStdTimeWorkItemsTable();" />--}%
							
								<g:fieldValue bean="${stdTimeWorkItemsInstance}" field="m041Replace"/> Jam
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stdTimeWorkItemsInstance?.m041Overhaul}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m041Overhaul-label" class="property-label"><g:message
					code="stdTimeWorkItems.m041Overhaul.label" default="Overhaul" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m041Overhaul-label">
						%{--<ba:editableValue
								bean="${stdTimeWorkItemsInstance}" field="m041Overhaul"
								url="${request.contextPath}/StdTimeWorkItems/updatefield" type="text"
								title="Enter m041Overhaul" onsuccess="reloadStdTimeWorkItemsTable();" />--}%
							
								<g:fieldValue bean="${stdTimeWorkItemsInstance}" field="m041Overhaul"/> Jam
							
						</span></td>
					
				</tr>
				</g:if>
            %{--
                            <g:if test="${stdTimeWorkItemsInstance?.companyDealer}">
                            <tr>
                            <td class="span2" style="text-align: right;"><span
                                id="companyDealer-label" class="property-label"><g:message
                                code="stdTimeWorkItems.companyDealer.label" default="Company Dealer" />:</span></td>

                                    <td class="span3"><span class="property-value"
                                    aria-labelledby="companyDealer-label">
                                    <ba:editableValue
                                            bean="${stdTimeWorkItemsInstance}" field="companyDealer"
                                            url="${request.contextPath}/StdTimeWorkItems/updatefield" type="text"
                                            title="Enter companyDealer" onsuccess="reloadStdTimeWorkItemsTable();" />
							
								<g:link controller="companyDealer" action="show" id="${stdTimeWorkItemsInstance?.companyDealer?.id}">${stdTimeWorkItemsInstance?.companyDealer?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			--}%
			
				<g:if test="${stdTimeWorkItemsInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="stdTimeWorkItems.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${stdTimeWorkItemsInstance}" field="lastUpdProcess"
								url="${request.contextPath}/StdTimeWorkItems/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadStdTimeWorkItemsTable();" />--}%
							
								<g:fieldValue bean="${stdTimeWorkItemsInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stdTimeWorkItemsInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="stdTimeWorkItems.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${stdTimeWorkItemsInstance}" field="dateCreated"
								url="${request.contextPath}/StdTimeWorkItems/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadStdTimeWorkItemsTable();" />--}%
							
								<g:formatDate date="${stdTimeWorkItemsInstance?.dateCreated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>


            <g:if test="${stdTimeWorkItemsInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="stdTimeWorkItems.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${stdTimeWorkItemsInstance}" field="createdBy"
                                url="${request.contextPath}/StdTimeWorkItems/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadStdTimeWorkItemsTable();" />--}%

                        <g:fieldValue bean="${stdTimeWorkItemsInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${stdTimeWorkItemsInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="stdTimeWorkItems.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${stdTimeWorkItemsInstance}" field="lastUpdated"
								url="${request.contextPath}/StdTimeWorkItems/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadStdTimeWorkItemsTable();" />--}%
							
								<g:formatDate date="${stdTimeWorkItemsInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${stdTimeWorkItemsInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="stdTimeWorkItems.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${stdTimeWorkItemsInstance}" field="updatedBy"
                                url="${request.contextPath}/StdTimeWorkItems/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadStdTimeWorkItemsTable();" />--}%

                        <g:fieldValue bean="${stdTimeWorkItemsInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${stdTimeWorkItemsInstance?.id}"
					update="[success:'stdTimeWorkItems-form',failure:'stdTimeWorkItems-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteStdTimeWorkItems('${stdTimeWorkItemsInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
