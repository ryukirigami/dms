<%@ page import="com.kombos.administrasi.WarnaVendor; com.kombos.administrasi.VendorCat; com.kombos.administrasi.DefaultWarna" %>



<div class="control-group fieldcontain ${hasErrors(bean: defaultWarnaInstance, field: 'vendorCat', 'error')} required">
    <label class="control-label" for="vendorCat">
        <g:message code="defaultWarna.vendorCat.label" default="Nama Vendor" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="vendorCat" name="vendorCat.id" from="${VendorCat.createCriteria().list(){eq("staDel","0")}}" optionKey="id" required="" value="${defaultWarnaInstance?.vendorCat?.id}" class="many-to-one"/>
    </div>
</div>
<g:javascript>
    document.getElementById("vendorCat").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: defaultWarnaInstance, field: 'warnaVendor', 'error')} required">
	<label class="control-label" for="warnaVendor">
		<g:message code="defaultWarna.warnaVendor.label" default="Nama Warna" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="warnaVendor" name="warnaVendor.id" from="${WarnaVendor.createCriteria().list(){eq("staDel","0")}}" optionKey="id" required="" value="${defaultWarnaInstance?.warnaVendor?.id}" class="many-to-one"/>
	</div>
</div>
%{--
<div class="control-group fieldcontain ${hasErrors(bean: defaultWarnaInstance, field: 'createdBy', 'error')} ">
	<label class="control-label" for="createdBy">
		<g:message code="defaultWarna.createdBy.label" default="Created By" />
		
	</label>
	<div class="controls">
	<g:textField name="createdBy" value="${defaultWarnaInstance?.createdBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: defaultWarnaInstance, field: 'updatedBy', 'error')} ">
	<label class="control-label" for="updatedBy">
		<g:message code="defaultWarna.updatedBy.label" default="Updated By" />
		
	</label>
	<div class="controls">
	<g:textField name="updatedBy" value="${defaultWarnaInstance?.updatedBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: defaultWarnaInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="defaultWarna.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${defaultWarnaInstance?.lastUpdProcess}"/>
	</div>
</div>
--}%
