
<%@ page import="com.kombos.administrasi.DefaultWarna" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="defaultWarna_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="defaultWarna.vendorCatID.label" default="Kode Vendor" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="defaultWarna.vendorCatNama.label" default="Nama Vendor" /></div>
            </th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="defaultWarna.warnaVendor.label" default="Nama Warna" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="defaultWarna.createdBy.label" default="Created By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="defaultWarna.updatedBy.label" default="Updated By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="defaultWarna.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>

		
		</tr>
		<tr>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_vendorCatID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_vendorCatID" class="search_init" />
                </div>
            </th>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_vendorCatNama" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_vendorCat" class="search_init" />
                </div>
            </th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_warnaVendor" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_warnaVendor" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_createdBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_updatedBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var DefaultWarnaTable;
var reloadDefaultWarnaTable;
$(function(){
	
	reloadDefaultWarnaTable = function() {
		DefaultWarnaTable.fnDraw();
	}

        var recordsDefaultWarnaperpage = [];
        var anDefaultWarnaSelected;
        var jmlRecDefaultWarnaPerPage=0;
        var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	DefaultWarnaTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	DefaultWarnaTable = $('#defaultWarna_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            //alert( $('li.active').text() );
            var rsDefaultWarna = $("#defaultWarna_datatables tbody .row-select");
            var jmlDefaultWarnaCek = 0;
            var idRec;
            var nRow;
            rsDefaultWarna.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsDefaultWarnaperpage[idRec]=="1"){
                    jmlDefaultWarnaCek = jmlDefaultWarnaCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsDefaultWarnaperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecDefaultWarnaPerPage = rsDefaultWarna.length;
            if(jmlDefaultWarnaCek==jmlRecDefaultWarnaPerPage && jmlRecDefaultWarnaPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "vendorCatID",
	"mDataProp": "vendorCatID",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "vendorCatNama",
	"mDataProp": "vendorCatNama",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "warnaVendor",
	"mDataProp": "warnaVendor",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "createdBy",
	"mDataProp": "createdBy",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "updatedBy",
	"mDataProp": "updatedBy",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var vendorCatID = $('#filter_vendorCatID input').val();
						if(vendorCatID){
							aoData.push(
									{"name": 'sCriteria_vendorCatID', "value": vendorCatID}
							);
						}

						var vendorCatNama = $('#filter_vendorCatNama input').val();
						if(vendorCatNama){
							aoData.push(
									{"name": 'sCriteria_vendorCatNama', "value": vendorCatNama}
							);
						}

						var warnaVendor = $('#filter_warnaVendor input').val();
						if(warnaVendor){
							aoData.push(
									{"name": 'sCriteria_warnaVendor', "value": warnaVendor}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});


	     $('.select-all').click(function(e) {
            $("#defaultWarna_datatables tbody .row-select").each(function() {
                if(this.checked){
                    recordsDefaultWarnaperpage[$(this).next("input:hidden").val()] = "1";
                } else {
                    recordsDefaultWarnaperpage[$(this).next("input:hidden").val()] = "0";
                }
            });
        });

        $('#defaultWarna_datatables tbody tr').live('click', function () {
            id = $(this).find('.row-select').next("input:hidden").val();
            if($(this).find('.row-select').is(":checked")){
                recordsDefaultWarnaperpage[id] = "1";
                $(this).find('.row-select').parent().parent().addClass('row_selected');
                anDefaultWarnaSelected = DefaultWarnaTable.$('tr.row_selected');
                if(jmlRecDefaultWarnaPerPage == anDefaultWarnaSelected.length){
                    $('.select-all').attr('checked', true);
                }
            } else {
                recordsDefaultWarnaperpage[id] = "0";
                $('.select-all').attr('checked', false);
                $(this).find('.row-select').parent().parent().removeClass('row_selected');
            }
        });

});
</g:javascript>


			
