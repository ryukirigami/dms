

<%@ page import="com.kombos.administrasi.DefaultWarna" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'defaultWarna.label', default: 'Default Warna')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteDefaultWarna;

$(function(){ 
	deleteDefaultWarna=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/defaultWarna/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadDefaultWarnaTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-defaultWarna" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="defaultWarna"
			class="table table-bordered table-hover">
			<tbody>


            <g:if test="${defaultWarnaInstance?.vendorCat}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="vendorCat-label" class="property-label"><g:message
                                code="defaultWarna.vendorCat.label" default="Nama Vendor" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="vendorCat-label">
                        %{--<ba:editableValue
                                bean="${defaultWarnaInstance}" field="vendorCat"
                                url="${request.contextPath}/DefaultWarna/updatefield" type="text"
                                title="Enter vendorCat" onsuccess="reloadDefaultWarnaTable();" />--}%

                        ${defaultWarnaInstance?.vendorCat?.encodeAsHTML()}

                    </span></td>

                </tr>
            </g:if>

				<g:if test="${defaultWarnaInstance?.warnaVendor}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="warnaVendor-label" class="property-label"><g:message
					code="defaultWarna.warnaVendor.label" default="Nama Warna" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="warnaVendor-label">
						%{--<ba:editableValue
								bean="${defaultWarnaInstance}" field="warnaVendor"
								url="${request.contextPath}/DefaultWarna/updatefield" type="text"
								title="Enter warnaVendor" onsuccess="reloadDefaultWarnaTable();" />--}%
							
								${defaultWarnaInstance?.warnaVendor?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${defaultWarnaInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="defaultWarna.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${defaultWarnaInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/DefaultWarna/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadDefaultWarnaTable();" />--}%

                        <g:fieldValue bean="${defaultWarnaInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${defaultWarnaInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="defaultWarna.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${defaultWarnaInstance}" field="dateCreated"
								url="${request.contextPath}/DefaultWarna/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadDefaultWarnaTable();" />--}%
							
								%{--<g:formatDate date="${defaultWarnaInstance?.dateCreated}" />--}%
                        <g:formatDate date="${defaultWarnaInstance?.dateCreated}" type="datetime" style="MEDIUM"  />
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${defaultWarnaInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="defaultWarna.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${defaultWarnaInstance}" field="updatedBy"
                                url="${request.contextPath}/DefaultWarna/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadDefaultWarnaTable();" />--}%

                        <g:fieldValue bean="${defaultWarnaInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>


            <g:if test="${defaultWarnaInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="defaultWarna.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${defaultWarnaInstance}" field="createdBy"
                                url="${request.contextPath}/DefaultWarna/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadDefaultWarnaTable();" />--}%

                        <g:fieldValue bean="${defaultWarnaInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

				<g:if test="${defaultWarnaInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="defaultWarna.lastUpdated.label" default="Last Updated" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${defaultWarnaInstance}" field="lastUpdated"
								url="${request.contextPath}/DefaultWarna/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadDefaultWarnaTable();" />--}%

								%{--<g:formatDate date="${defaultWarnaInstance?.lastUpdated}" />--}%
                        <g:formatDate date="${defaultWarnaInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />
						</span></td>

				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${defaultWarnaInstance?.id}"
					update="[success:'defaultWarna-form',failure:'defaultWarna-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteDefaultWarna('${defaultWarnaInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
