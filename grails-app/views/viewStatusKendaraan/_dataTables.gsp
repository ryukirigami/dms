<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="statusKendaraan_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
			<th style="border-bottom: none;padding: 5px;">
				<div>Nama Customer</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Nomor HP</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Kendaraan</div>
			</th>
            <th style="border-bottom: none;padding: 5px;">
                <div>Nopol</div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div>Tanggal WO</div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div>Nomor WO</div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div>Nama SA</div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div>Status</div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div>Delivery Time</div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div>Status Delivery</div>
            </th>
        </tr>
        <tr>
            <th style="border-top: none;padding: 5px;">
                <div id="filter_t182NamaBelakang" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t182NamaBelakang" class="search_init" />
                </div>
            </th>
            <th style="border-top: none;padding: 5px;">
                <div id="filter_t182NoHp" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t182NoHp" class="search_init" />
                </div>
            </th>
            <th style="border-top: none;padding: 5px;">
                <div id="filter_m102NamaBaseModel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_m102NamaBaseModel" class="search_init" />
                </div>
            </th>
            <th style="border-top: none;padding: 5px;">
                <div id="filter_m116NamaKota" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_m116NamaKota" class="search_init" />
                </div>
            </th>
            <th style="border-top: none;padding: 5px;">
                <div id="filter_t401TglJamCetakWO" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t401TglJamCetakWO" class="search_init" />
                </div>
            </th>
            <th style="border-top: none;padding: 5px;">
                <div id="filter_t401NoWO" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t401NoWO" class="search_init" />
                </div>
            </th>
            <th style="border-top: none;padding: 5px;">
                <div id="filter_t401NamaSA" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t401NamaSA" class="search_init" />
                </div>
            </th>
            <th style="border-top: none;padding: 5px;">
                <div id="filter_t401StaApp" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t401StaApp" class="search_init" />
                </div>
            </th>
            <th style="border-top: none;padding: 5px;">
                <div id="filter_t401TglJamPenyerahan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t401TglJamPenyerahan" class="search_init" />
                </div>
            </th>

       </tr>
	</thead>
</table>

<g:javascript>
var statusKendaraanTable;
var reloadStatusKendaraanTable;
$(function(){


     $('#view').click(function(e){
        e.stopPropagation();
		statusKendaraanTable.fnDraw();
	});

	
	reloadStatusKendaraanTable = function() {
		statusKendaraanTable.fnDraw();
	}
    $('#search_satuan').change(function(){
        statusKendaraanTable.fnDraw();
    });
	var recordsStatusKendaraanPerPage = [];
    var anStatusKendaraanSelected;
    var jmlRecStatusKendaraanPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	statusKendaraanTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	statusKendaraanTable = $('#statusKendaraan_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsStatusKendaraan = $("#statusKendaraan_datatables tbody .row-select");
            var jmlStatusKendaraanCek = 0;
            var nRow;
            var idRec;
            rsStatusKendaraan.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsStatusKendaraanPerPage[idRec]=="1"){
                    jmlStatusKendaraanCek = jmlStatusKendaraanCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsStatusKendaraanPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecStatusKendaraanPerPage = rsStatusKendaraan.length;
            if(jmlStatusKendaraanCek==jmlRecStatusKendaraanPerPage && jmlRecStatusKendaraanPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "namaCustomer",
	"mDataProp": "namaCustomer",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"120px",
	"bVisible": true
},

{
	"sName": "nomorHp",
	"mDataProp": "nomorHp",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"120px",
	"bVisible": true
},

{
	"sName": "kendaraan",
	"mDataProp": "kendaraan",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"120px",
	"bVisible": true
},

{
	"sName": 'nopol',
	"mDataProp": "nopol",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"120px",
	"bVisible": true
},
{
	"sName": 'nomorWO',
	"mDataProp": "nomorWO",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"120px",
	"bVisible": true
},
{
	"sName": 'namaSA',
	"mDataProp": "namaSA",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"120px",
	"bVisible": true
},
{
	"sName": 'status',
	"mDataProp": "status",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"120px",
	"bVisible": true
},
{
	"sName": 'deliveryTime',
	"mDataProp": "deliveryTime",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"120px",
	"bVisible": true
},
{
	"sName": 'statusDelivery',
	"mDataProp": "statusDelivery",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"120px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	        var tglFrom = $('#search_tglFrom').val();
            var tglFromDay = $('#search_tglFrom_day').val();
            var tglFromMonth = $('#search_tglFrom_month').val();
            var tglFromYear = $('#search_tglFrom_year').val();

            var tglTo = $('#search_tglTo').val();
            var tglToDay = $('#search_tglTo_day').val();
            var tglToMonth = $('#search_tglTo_month').val();
            var tglToYear = $('#search_tglTo_year').val();

            aoData.push(
                {"name": 'sCriteria_tglFrom', "value": "date.struct"},
                {"name": 'sCriteria_tglFrom_dp', "value": tglFrom},
                {"name": 'sCriteria_tglFrom_day', "value": tglFromDay},
                {"name": 'sCriteria_tglFrom_month', "value": tglFromMonth},
                {"name": 'sCriteria_tglFrom_year', "value": tglFromYear},

                {"name": 'sCriteria_tglTo', "value": "date.struct"},
                {"name": 'sCriteria_tglTo_dp', "value": tglTo},
                {"name": 'sCriteria_tglTo_day', "value": tglToDay},
                {"name": 'sCriteria_tglTo_month', "value": tglToMonth},
                {"name": 'sCriteria_tglTo_year', "value": tglToYear}
            );
            $.ajax({ "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData ,
                "success": function (json) {
                    fnCallback(json);
                   },
                "complete": function () {
                   }
            });
		}			
	});

});
</g:javascript>