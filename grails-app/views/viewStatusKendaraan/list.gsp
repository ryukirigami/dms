
<%@ page import="com.kombos.parts.Goods" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'kendaraanTunggu.label', default: 'Status Kendaraan')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){
        onClearSearch = function () {
            $(':input', '#form')
                    .not(':button, :submit, :reset, :checkbox')
                    .val('')
                    .removeAttr('checked')
                    .removeAttr('selected');

            var inputs, index;

            inputs = document.getElementsByTagName('input');
            for (index = 0; index < inputs.length; ++index) {
                inputs[index].checked = false;
            }
            selectCheckTgl();
            selectCheckNamaCustomer();
            selectCheckNomorHp();
            selectCheckNomorPolisi();
            selectCheckStatus();
        }

        onSearch = function(){
            var oTable = $('#statusKendaraan_datatables').dataTable();
            oTable.fnReloadAjax();
        }

        selectCheckTgl = function() {
            if ($('#checkTglReception').is(':checked')) {
                $('#search_tglFrom').prop('disabled', false);
                $('#search_tglTo').prop('disabled', false);
            }
            else {
                $('#search_tglFrom').val('');
                $('#search_tglTo').val('');
                $('#search_tglFrom').prop('disabled', true);
                $('#search_tglTo').prop('disabled', true);
            }
        }
        selectCheckNamaCustomer = function() {
            if ($('#checkNamaCustomer').is(':checked')) {
                $('#input_namaCustomer').prop('disabled', false);
            } else {
                $('#input_namaCustomer').val('');
                $('#input_namaCustomer').prop('disabled', true);
            }
        }
        selectCheckNomorHp = function() {
            if ($('#checkNomorHp').is(':checked')) {
                $('#input_nomorHp').prop('disabled', false);
            } else {
                $('#input_nomorHp').val('');
                $('#input_nomorHp').prop('disabled', true);
            }
        }
        selectCheckNomorPolisi = function() {
            if ($('#checkNomorPolisi').is(':checked')) {
                $('#input_nomorPolisi').prop('disabled', false);
            } else {
                $('#input_nomorPolisi').val('');
                $('#input_nomorPolisi').prop('disabled', true);
            }
        }
        selectCheckStatus = function() {
            if ($('#checkStatus').is(':checked')) {
                $('#input_status').prop('disabled', false);
            } else {
                $('#input_status').val('');
                $('#input_status').prop('disabled', true);
            }
        }
	});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left">Search</span>
	</div>
	<div class="box">
		<div class="span12" id="goods-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

            <fieldset>
                <table style="padding-right: 10px">
                    <tr style="display:table-row;">
                        <td style="width: 130px; display:table-cell; padding:5px;">
                            <label class="control-label" for="tglReceptionInput">
                                Tanggal Reception
                            </label>&nbsp;&nbsp;
                        </td>
                        <td style="display:table-cell; padding:5px;"><g:checkBox name="checkTglReception" id="checkTglReception" value="checkTgl" onclick="selectCheckTgl();"/></td>
                        <td style="display:table-cell; padding:5px;">
                            <div id="tglReceptionInput" class="controls">
                                <input type="hidden" name="search_tglFrom" value="date.struct">
                                <input type="hidden" name="search_tglFrom_day" id="search_tglFrom_day" value="">
                                <input type="hidden" name="search_tglFrom_month" id="search_tglFrom_month" value="">
                                <input type="hidden" name="search_tglFrom_year" id="search_tglFrom_year" value="">
                                <input type="text" data-date-format="dd/mm/yyyy" name="search_tglFrom_dp" value="" id="search_tglFrom" class="search_init" style="width: 120px">
                                %{--<ba:datePicker id="input_dateFrom" name="input_dateFrom" precision="day" format="yyyy-MM-dd"  value=""  />--}%
                                s.d
                                <input type="hidden" name="search_tglTo" value="date.struct">
                                <input type="hidden" name="search_tglTo_day" id="search_tglTo_day" value="">
                                <input type="hidden" name="search_tglTo_month" id="search_tglTo_month" value="">
                                <input type="hidden" name="search_tglTo_year" id="search_tglTo_year" value="">
                                <input type="text" data-date-format="dd/mm/yyyy" name="search_tglTo_dp" value="" id="search_tglTo" class="search_init" style="width: 120px">
                                %{--<ba:datePicker id="input_dateTo" name="input_dateTo" precision="day" format="yyyy-MM-dd"  value=""  />--}%
                            </div>
                        </td>
                        <td style="width: 130px; display:table-cell; padding:5px;">
                            <label class="control-label" for="namaCustomerInput">
                                Nama Customer
                            </label>
                        </td>
                        <td style="display:table-cell; padding:5px;"><g:checkBox name="checkNamaCustomer" id="checkNamaCustomer" value="checkNamaCustomer" onclick="selectCheckNamaCustomer();"/></td>
                        <td style="display:table-cell; padding:5px;">
                            <div id="namaCustomerInput" class="controls">
                                <g:textField name="input_namaCustomer" id="input_namaCustomer" maxlength="30" />
                            </div>
                        </td>
                    </tr>
                    <tr style="display:table-row;">
                        <td style="width: 130px; display:table-cell; padding:5px;">
                            <label class="control-label" for="nomorHpInput">
                                Nomor HP
                            </label>
                        </td>
                        <td style="display:table-cell; padding:5px;"><g:checkBox name="checkNomorHp" id="checkNomorHp" value="checkNomorHp" onclick="selectCheckNomorHp();"/></td>
                        <td style="display:table-cell; padding:5px;">
                            <div id="nomorHpInput" class="controls">
                                <g:textField name="input_nomorHp" id="input_nomorHp" maxlength="16" />
                            </div>
                        </td>
                        <td style="width: 130px; display:table-cell; padding:5px;">
                            <label class="control-label" for="nomorPolisiInput">
                                Nomor Polisi
                            </label>
                        </td>
                        <td style="display:table-cell; padding:5px;"><g:checkBox name="checkNomorPolisi" id="checkNomorPolisi" value="checkNomorPolisi" onclick="selectCheckNomorPolisi();"/></td>
                        <td style="display:table-cell; padding:5px;">
                            <div id="nomorPolisiInput" class="controls">
                                <g:textField name="input_nomorPolisi" id="input_nomorPolisi" maxlength="16" />
                            </div>
                        </td>
                    </tr>
                    <tr style="display:table-row;">
                        <td style="width: 130px; display:table-cell; padding:5px;">
                            <label class="control-label" for="statusInput">
                                Status
                            </label>
                        </td>
                        <td style="display:table-cell; padding:5px;"><g:checkBox name="checkStatus" id="checkStatus" value="checkStatus" onclick="selectCheckStatus();"/></td>
                        <td style="display:table-cell; padding:5px;">
                            <div id="statusInput" class="controls">
                                <g:select name="search_column"
                                    from="['sedangService': 'Sedang Service', 'tungguParts': 'Tunggu Parts']"
                                    optionKey="key" optionValue="value"
                                    noSelection="['null': '[Pilih Kolom]']" />
                            </div>
                        </td>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td colspan="6" >
                            <div class="controls" style="right: 0">
                                <button class="btn btn-primary" id="buttonSearch" onclick="onSearch();">Search</button>
                                <button class="btn btn-primary" onclick="onClearSearch();">Clear Search</button>
                            </div>
                        </td>
                    </tr>
                </table>

            </fieldset>


            <g:render template="dataTables" />
		</div>
		<div class="span7" id="goods-form" style="display: none;"></div>
	</div>
</body>
</html>
