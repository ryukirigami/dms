
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay" />

<table id="partsDisposal_datatables_${idTable}" cellpadding="0" cellspacing="0" border="0"
       class="display table table-striped table-bordered table-hover table-selectable" style="table-layout: fixed; ">
    <thead>
    <tr>
        <th style="vertical-align: middle;">
            <div style="height: 10px"> </div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div>Nomor Disposal</div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div>Tanggal Disposal</div>
        </th>
        <th style="border-bottom: none; padding: 5px;">
            <div>Petugas Disposal</div>
        </th>
    </tr>

    </thead>
</table>

<g:javascript>
var partsDisposalTable_${idTable};
var reloadPartsDisposalTable;
$(function(){

    reloadPartsDisposalTable = function() {
		partsDisposalTable_${idTable}.fnDraw();
	}
 $('#clear').click(function(e){
        $('#search_t017Tanggal').val("");
        $('#nilaiRadio').val("");
        $('#search_t017Tanggal_day').val("");
        $('#search_t017Tanggal_month').val("");
        $('#search_t017Tanggal_year').val("");
        $('#search_t017Tanggal2').val("");
        $('#search_status1').prop("checked",false);
        $('#search_status2').prop("checked",false);
        $('#search_t017Tanggal2_day').val("");
        $('#search_t017Tanggal2_month').val("");
        $('#search_t017Tanggal2_year').val("");
         e.stopPropagation();
		partsDisposalTable_${idTable}.fnDraw();
	});
    $('#view').click(function(e){
        e.stopPropagation();
		partsDisposalTable_${idTable}.fnDraw();
	});
	var anOpen = [];
	$('#partsDisposal_datatables_${idTable} td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = partsDisposalTable_${idTable}.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST', 
	   			data : oData,
	   			url:'${request.contextPath}/partsDisposal/sublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = partsDisposalTable_${idTable}.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
    		
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			partsDisposalTable_${idTable}.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});
	
    $('#partsDisposal_datatables_${idTable} a.edit').live('click', function (e) {
        e.preventDefault();
         
        var nRow = $(this).parents('tr')[0];
         
        if ( nEditing !== null && nEditing != nRow ) {
            restoreRow( partsDisposalTable_${idTable}, nEditing );
            editRow( partsDisposalTable_${idTable}, nRow );
            nEditing = nRow;
        }
        else if ( nEditing == nRow && this.innerHTML == "Save" ) {
            saveRow( partsDisposalTable_${idTable}, nEditing );
            nEditing = null;
        }
        else {
            editRow( partsDisposalTable_${idTable}, nRow );
            nEditing = nRow;
        }
    } );
    
	reloadPartsDisposalTable = function() {
		partsDisposalTable_${idTable}.fnDraw();
	}

 	partsDisposalTable_${idTable} = $('#partsDisposal_datatables_${idTable}').dataTable({
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "sClass": "control center",
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "nomorDisposal",
	"mDataProp": "nomorDisposal",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"> <input type="hidden" value="'+data+'">&nbsp;&nbsp;' + data;
	},
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "tanggalDisposal",
	"mDataProp": "tanggalDisposal",
	"aTargets": [1] ,
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "petugas",
	"mDataProp": "petugas",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"300px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        var t017Tanggal = $('#search_t017Tanggal').val();
						var t017TanggalDay = $('#search_t017Tanggal_day').val();
						var t017TanggalMonth = $('#search_t017Tanggal_month').val();
						var t017TanggalYear = $('#search_t017Tanggal_year').val();

						var t017Tanggal2 = $('#search_t017Tanggal2').val();
						var t017TanggalDay2 = $('#search_t017Tanggal2_day').val();
						var t017TanggalMonth2 = $('#search_t017Tanggal2_month').val();
						var t017TanggalYear2 = $('#search_t017Tanggal2_year').val();

                        var statusApprove = $('#nilaiRadio').val();
						if(statusApprove){
							aoData.push(
									{"name": 'statusApprove', "value": statusApprove}
							);
						}

						if(t017Tanggal){
							aoData.push(
									{"name": 'sCriteria_t017Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_t017Tanggal_dp', "value": t017Tanggal},
									{"name": 'sCriteria_t017Tanggal_day', "value": t017TanggalDay},
									{"name": 'sCriteria_t017Tanggal_month', "value": t017TanggalMonth},
									{"name": 'sCriteria_t017Tanggal_year', "value": t017TanggalYear}
							);
						}


						if(t017Tanggal2){
							aoData.push(
									{"name": 'sCriteria_t017Tanggal2', "value": "date.struct"},
									{"name": 'sCriteria_t017Tanggal2_dp', "value": t017Tanggal2},
									{"name": 'sCriteria_t017Tanggal2_day', "value": t017TanggalDay2},
									{"name": 'sCriteria_t017Tanggal2_month', "value": t017TanggalMonth2},
									{"name": 'sCriteria_t017Tanggal2_year', "value": t017TanggalYear2}
							);
						}


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});



</g:javascript>



