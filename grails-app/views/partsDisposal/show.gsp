

<%@ page import="com.kombos.parts.PartsDisposal" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'partsDisposal.label', default: 'PartsDisposal')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deletePartsDisposal;

$(function(){ 
	deletePartsDisposal=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/partsDisposal/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadPartsDisposalTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-partsDisposal" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="partsDisposal"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${partsDisposalInstance?.t147ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t147ID-label" class="property-label"><g:message
					code="partsDisposal.t147ID.label" default="T147 ID" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t147ID-label">
						%{--<ba:editableValue
								bean="${partsDisposalInstance}" field="t147ID"
								url="${request.contextPath}/PartsDisposal/updatefield" type="text"
								title="Enter t147ID" onsuccess="reloadPartsDisposalTable();" />--}%
							
								<g:fieldValue bean="${partsDisposalInstance}" field="t147ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsDisposalInstance?.t147TglDisp}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t147TglDisp-label" class="property-label"><g:message
					code="partsDisposal.t147TglDisp.label" default="T147 Tgl Disp" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t147TglDisp-label">
						%{--<ba:editableValue
								bean="${partsDisposalInstance}" field="t147TglDisp"
								url="${request.contextPath}/PartsDisposal/updatefield" type="text"
								title="Enter t147TglDisp" onsuccess="reloadPartsDisposalTable();" />--}%
							
								<g:formatDate date="${partsDisposalInstance?.t147TglDisp}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsDisposalInstance?.t147NamaReff}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t147NamaReff-label" class="property-label"><g:message
					code="partsDisposal.t147NamaReff.label" default="T147 Nama Reff" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t147NamaReff-label">
						%{--<ba:editableValue
								bean="${partsDisposalInstance}" field="t147NamaReff"
								url="${request.contextPath}/PartsDisposal/updatefield" type="text"
								title="Enter t147NamaReff" onsuccess="reloadPartsDisposalTable();" />--}%
							
								<g:fieldValue bean="${partsDisposalInstance}" field="t147NamaReff"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsDisposalInstance?.t147NoReff}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t147NoReff-label" class="property-label"><g:message
					code="partsDisposal.t147NoReff.label" default="T147 No Reff" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t147NoReff-label">
						%{--<ba:editableValue
								bean="${partsDisposalInstance}" field="t147NoReff"
								url="${request.contextPath}/PartsDisposal/updatefield" type="text"
								title="Enter t147NoReff" onsuccess="reloadPartsDisposalTable();" />--}%
							
								<g:fieldValue bean="${partsDisposalInstance}" field="t147NoReff"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsDisposalInstance?.t147Ket}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t147Ket-label" class="property-label"><g:message
					code="partsDisposal.t147Ket.label" default="T147 Ket" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t147Ket-label">
						%{--<ba:editableValue
								bean="${partsDisposalInstance}" field="t147Ket"
								url="${request.contextPath}/PartsDisposal/updatefield" type="text"
								title="Enter t147Ket" onsuccess="reloadPartsDisposalTable();" />--}%
							
								<g:fieldValue bean="${partsDisposalInstance}" field="t147Ket"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsDisposalInstance?.t147xNamaUser}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t147xNamaUser-label" class="property-label"><g:message
					code="partsDisposal.t147xNamaUser.label" default="T147x Nama User" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t147xNamaUser-label">
						%{--<ba:editableValue
								bean="${partsDisposalInstance}" field="t147xNamaUser"
								url="${request.contextPath}/PartsDisposal/updatefield" type="text"
								title="Enter t147xNamaUser" onsuccess="reloadPartsDisposalTable();" />--}%
							
								<g:fieldValue bean="${partsDisposalInstance}" field="t147xNamaUser"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsDisposalInstance?.t147xNamaDivisi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t147xNamaDivisi-label" class="property-label"><g:message
					code="partsDisposal.t147xNamaDivisi.label" default="T147x Nama Divisi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t147xNamaDivisi-label">
						%{--<ba:editableValue
								bean="${partsDisposalInstance}" field="t147xNamaDivisi"
								url="${request.contextPath}/PartsDisposal/updatefield" type="text"
								title="Enter t147xNamaDivisi" onsuccess="reloadPartsDisposalTable();" />--}%
							
								<g:fieldValue bean="${partsDisposalInstance}" field="t147xNamaDivisi"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsDisposalInstance?.t147StaDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t147StaDel-label" class="property-label"><g:message
					code="partsDisposal.t147StaDel.label" default="T147 Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t147StaDel-label">
						%{--<ba:editableValue
								bean="${partsDisposalInstance}" field="t147StaDel"
								url="${request.contextPath}/PartsDisposal/updatefield" type="text"
								title="Enter t147StaDel" onsuccess="reloadPartsDisposalTable();" />--}%
							
								<g:fieldValue bean="${partsDisposalInstance}" field="t147StaDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsDisposalInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="partsDisposal.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${partsDisposalInstance}" field="createdBy"
								url="${request.contextPath}/PartsDisposal/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadPartsDisposalTable();" />--}%
							
								<g:fieldValue bean="${partsDisposalInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsDisposalInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="partsDisposal.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${partsDisposalInstance}" field="updatedBy"
								url="${request.contextPath}/PartsDisposal/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadPartsDisposalTable();" />--}%
							
								<g:fieldValue bean="${partsDisposalInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsDisposalInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="partsDisposal.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${partsDisposalInstance}" field="lastUpdProcess"
								url="${request.contextPath}/PartsDisposal/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadPartsDisposalTable();" />--}%
							
								<g:fieldValue bean="${partsDisposalInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsDisposalInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="partsDisposal.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${partsDisposalInstance}" field="dateCreated"
								url="${request.contextPath}/PartsDisposal/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadPartsDisposalTable();" />--}%
							
								<g:formatDate date="${partsDisposalInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partsDisposalInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="partsDisposal.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${partsDisposalInstance}" field="lastUpdated"
								url="${request.contextPath}/PartsDisposal/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadPartsDisposalTable();" />--}%
							
								<g:formatDate date="${partsDisposalInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${partsDisposalInstance?.id}"
					update="[success:'partsDisposal-form',failure:'partsDisposal-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deletePartsDisposal('${partsDisposalInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
