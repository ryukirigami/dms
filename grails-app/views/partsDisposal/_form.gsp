<%@ page import="com.kombos.parts.PartsDisposal" %>



<div class="control-group fieldcontain ${hasErrors(bean: partsDisposalInstance, field: 't147ID', 'error')} ">
	<label class="control-label" for="t147ID">
		<g:message code="partsDisposal.t147ID.label" default="T147 ID" />
		
	</label>
	<div class="controls">
	<g:textField name="t147ID" maxlength="20" value="${partsDisposalInstance?.t147ID}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partsDisposalInstance, field: 't147TglDisp', 'error')} ">
	<label class="control-label" for="t147TglDisp">
		<g:message code="partsDisposal.t147TglDisp.label" default="T147 Tgl Disp" />
		
	</label>
	<div class="controls">
	<g:datePicker name="t147TglDisp" precision="day"  value="${partsDisposalInstance?.t147TglDisp}" default="none" noSelection="['': '']" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: partsDisposalInstance, field: 't147xNamaUser', 'error')} ">
	<label class="control-label" for="t147xNamaUser">
		<g:message code="partsDisposal.t147xNamaUser.label" default="T147x Nama User" />
		
	</label>
	<div class="controls">
	<g:textField name="t147xNamaUser" maxlength="20" value="${partsDisposalInstance?.t147xNamaUser}"/>
	</div>
</div>

