<%@ page import="com.kombos.administrasi.VendorCat; com.kombos.customerprofile.Warna; com.kombos.administrasi.Operation; com.kombos.reception.Reception; com.kombos.parts.Claim" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
    <g:javascript>
		$(function(){
			$('#noWo').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/posInput/kodeList', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});
    </g:javascript>
		<g:javascript disposition="head">
        var noUrut = 1;
	var show;
	var cekStatus = "${aksi}";
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){
        if(cekStatus=="ubah"){

               var ceks = "${posJob?.pos?.t418StaTagihKe}";
                if(ceks=='1'){
                    $('#tagih1').prop('checked',true);
                    $('#tagih0').prop('checked',false);
                }else if(ceks=='0'){
                    $('#tagih0').prop('checked',true);
                    $('#tagih1').prop('checked',false);
                }
        }

	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/pos/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#pos-form').empty();
    	$('#pos-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#pos-table").hasClass("span12")){
   			$("#pos-table").toggleClass("span12 span5");
        }
        $("#pos-form").css("display","block");
   	}
   	
   	expandTableLayout = function(){
   		if($("#pos-table").hasClass("span5")){
   			$("#pos-table").toggleClass("span5 span12");
   		}
        $("#pos-form").css("display","none");
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#pos-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/posInput/massdelete',
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadClaimTable();
    		}
		});
		
   	}

});

       cek = function(){
           var noWo = $('#noWo').val();
           $.ajax({
            url:'${request.contextPath}/posInput/cek',
            type: "POST", // Always use POST when deleting data
            data : {noWo:noWo},
            success : function(data){
                   $("#data").html(data)
            },
            error: function(xhr, textStatus, errorThrown) {
            alert('Internal Server Error');
            }
        });
       }

      add = function(){

            var idJob = $('#namaJob').val();
            if(idJob!=null && idJob!="-"){
                $.ajax({
                    url:'${request.contextPath}/posInput/addJob',
                    type: "POST", // Always use POST when deleting data
                    data : {idJob : idJob},
                    success : function(data){
                     $('#namaJob').find('option:selected').remove()
                     $.each(data,function(i,item){
                        posInputTable.fnAddData({
                            'id': item.id,
                            'job': item.job
                        });
                     });
                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });
            }

      }

      allJob = function(){
            if($('#namaJob').find('option').size()==1){
                alert("Data Sudah Full")
            }else{
                 $("#pos-table tbody .row-select").each(function() {
                        var id = $(this).next("input:hidden").val();
                        var row = $(this).closest("tr").get(0);
                        var data = posInputTable.fnGetData(row);
                        posInputTable.fnDeleteRow(posInputTable.fnGetPosition(row));
                });
                $.ajax({
                    url:'${request.contextPath}/posInput/allJob',
                    type: "POST", // Always use POST when deleting data
                    success : function(data){
                    $('#namaJob').find('option:not(:first)').remove();
                        $.each(data,function(i,item){
                            posInputTable.fnAddData({
                                'id': item.id,
                                'job': item.job
                            });
                        });
                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });
            }
      }

      deleteData = function(){
                var checkGoods =[];
                $("#pos-table tbody .row-select").each(function() {
                    if(this.checked){
                        var id = $(this).next("input:hidden").val();
                        var row = $(this).closest("tr").get(0);
                        var data = posInputTable.fnGetData(row);
                        posInputTable.fnDeleteRow(posInputTable.fnGetPosition(row));
                        $('#namaJob').append("<option value='" + data['id'] + "'>" + data['job'] + "</option>");
                    }
                });
      }
      updatePos = function(){
            var tagih = ""
            if($('input:radio[name=tagih]:nth(0)').is(':checked')){
                tagih = "1"
            }else if($('input:radio[name=tagih]:nth(1)').is(':checked')){
                tagih = "1"
            }
            var noWo = $("#noWo").val();
            var tanggal = $("#tanggal").val();
            var formPos = $('#pos-table').find('form');
            var checkPos =[];
            $("#pos-table tbody .row-select").each(function() {
            if(this.checked){
                var id = $(this).next("input:hidden").val();
                var nRow = $(this).parents('tr')[0];
                var aData = posInputTable.fnGetData(nRow);
                checkPos.push(id);
            }
            });
            if(checkPos.length<1){
                alert('Anda belum memilih data yang akan ditambahkan');
            }else if(tanggal=="" || noWo=="" || tagih==""){
                toastr.error('<div>Data Belum Lengkap</div>');
            }else{

                $("#ids").val(JSON.stringify(checkPos));
                $.ajax({
                    url:'${request.contextPath}/posInput/ubah',
                    type: "POST", // Always use POST when deleting data
                    data : formPos.serialize(),
                    success : function(data){
                      if(data=='notOk'){
                        toastr.error('<div>Gagal</div>');
                      }else{
                        window.location.href = '#/viewOrderCat' ;
                       toastr.success('<div>Succes</div>');
                        formPos.find('.deleteafter').remove();
                        $("#ids").val('');
                      }

                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });

                checkPos = [];
            }
       }

        createRequest = function(){
            var tagih = ""
            if($('input:radio[name=tagih]:nth(0)').is(':checked')){
                tagih = "1"
            }else if($('input:radio[name=tagih]:nth(1)').is(':checked')){
                tagih = "1"
            }
            var noWo = $("#noWo").val();
            var tanggal = $("#tanggal").val();
            var formPos = $('#pos-table').find('form');
            var checkPos =[];
            $("#pos-table tbody .row-select").each(function() {
            if(this.checked){
                var id = $(this).next("input:hidden").val();
                var nRow = $(this).parents('tr')[0];
                var aData = posInputTable.fnGetData(nRow);
                checkPos.push(id);
            }
            });
            if(checkPos.length<1){
                alert('Anda belum memilih data yang akan ditambahkan');
            }else if(tanggal=="" || noWo=="" || tagih==""){
                toastr.error('<div>Data Belum Lengkap</div>');
            }else{

                $("#ids").val(JSON.stringify(checkPos));
                $.ajax({
                    url:'${request.contextPath}/posInput/req',
                    type: "POST", // Always use POST when deleting data
                    data : formPos.serialize(),
                    success : function(data){
                      if(data=='notOk'){
                        toastr.error('<div>Gagal</div>');
                      }else{
                        window.location.href = '#/viewOrderCat' ;
                       toastr.success('<div>Succes</div>');
                        formPos.find('.deleteafter').remove();
                        $("#ids").val('');
                      }

                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });

                checkPos = [];
            }
        };

</g:javascript>

	</head>
	<body>
	<div class="navbar box-header no-border">
        <g:if test="${aksi=='ubah'}">
            Edit Data Pos
        </g:if>
        <g:else>
            Input Data Pos
        </g:else>

        <ul class="nav pull-right">
            <li>&nbsp;</li>
		</ul>
	</div><br>
	<div class="box">
		<div class="span12" id="pos-table">
            <form id="form-pos" class="form-horizontal">
                <input type="hidden" name="ids" id="ids" value="">
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label" for="t951Tanggal" style="text-align: left;">
                                Nomor WO *
                            </label>
                            <div id="filter_wo" class="controls">
                                <g:if test="${aksi=='ubah'}">
                                    <g:textField name="noWo" id="noWo"  value="${reception?.t401NoWO}" readonly="readonly"  style="width: 400px"  />
                                </g:if>
                                <g:else>
                                    <g:select name="noWo" id="noWo" from="${recc}" style="width: 412px" onchange="cek()" noSelection="['':'Pilih Nomor WO']" />
                                </g:else>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="t951Tanggal" style="text-align: left;">
                                Vehicle
                            </label>
                            <div id="filter_vehicle" class="controls">
                                <span id="data">
                                    <g:if test="${aksi=='ubah'}">
                                    <%
                                        def model = reception.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel
                                        def warna = reception.historyCustomerVehicle?.warna?.m092NamaWarna
                                        def nopol = reception.historyCustomerVehicle?.kodeKotaNoPol?.m116ID +" "+reception.historyCustomerVehicle?.t183NoPolTengah + "" + reception.historyCustomerVehicle?.t183NoPolBelakang
                                        out.println( model + " | " + warna +" | "+ nopol)
                                        %>
                                    </g:if>
                                </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="t951Tanggal" style="text-align: left;">
                                Nomor POS
                            </label>
                            <div id="filter_namaVendor" class="controls">
                                <g:if test="${aksi=='ubah'}">
                                    <g:textField name="noPos" id="noPos" value="${posJob?.pos?.t418NoPOS}" readonly="true" maxlength="220" style="width: 400px"  />
                                    <g:hiddenField name="noPosId" id="noPosId" value="${posJob?.pos?.id}"/>
                                </g:if>
                                <g:else>
                                    <g:textField name="noPos" id="noPos" value="${noPos}" readonly="true" maxlength="220" style="width: 400px"  />
                                </g:else>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="userRole" style="text-align: left;">
                                Tanggal Pos *
                            </label>
                            <div id="filter_status" class="controls">
                                <ba:datePicker id="tanggal" name="tanggal" precision="day" format="dd-MM-yyyy"  value="${posJob?.pos?.t418TglPOS}" required="true" style="width: 400px"  />
                            </div>
                         </div>

                        <div class="control-group">
                            <label class="control-label" for="t951Tanggal" style="text-align: left;">
                                Warna *
                            </label>
                            <div id="filter_warna" class="controls">
                                <g:if test="${aksi=='ubah'}">
                                    <g:select name="warna" from="${Warna.createCriteria().list { eq("staDel",'0')}}" optionKey="id" optionValue="m092NamaWarna" value="${posJob?.pos?.warna.id}" style="width: 412px" />
                                </g:if>
                                <g:else>
                                    <g:select name="warna" from="${Warna.createCriteria().list { eq("staDel",'0')}}" optionKey="id" optionValue="m092NamaWarna" style="width: 412px" />
                                </g:else>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="t951Tanggal" style="text-align: left;">
                               &nbsp;
                            </label>
                            <div id="filter_" class="controls">
                                <button style="width: 100px;height: 30px; border-radius: 5px" class="btn btn-cancel" name="view">Cari Vendor</button>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="vendor" style="text-align: left;">
                                Nama Vendor Cat *
                            </label>
                            <div id="filter_vendorCat" class="controls">

                                <g:if test="${aksi=='ubah'}">
                                    <g:select name="vendorCat" from="${VendorCat.createCriteria().list { eq("staDel",'0')}}" optionKey="id" optionValue="m191Nama" value="${posJob?.pos?.vendorCat.id}" style="width: 412px" />
                                </g:if>
                                <g:else>
                                    <g:select name="vendorCat" from="${VendorCat.createCriteria().list { eq("staDel",'0')}}" optionKey="id" optionValue="m191Nama" style="width: 412px" />
                                </g:else>

                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="ket" style="text-align: left;">
                                Keterangan
                            </label>
                            <div id="filter_ket" class="controls">
                                <g:textField name="ket" id="ket" value="${posJob?.pos?.t418KetOrder}" maxlength="255" style="width: 400px" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="tagih" style="text-align: left;">
                                Ditagih Ke *
                            </label>
                            <div id="filter_ditagihKe" class="controls">
                                <g:if test="${aksi=='ubah'}">
                                    <input type="radio" name="tagih" id="tagih1" value="bengkel" /> Bengkel &nbsp; &nbsp; &nbsp;
                                    <input type="radio" name="tagih" id="tagih0" value="vendor" /> Vendor
                                </g:if>
                                <g:else>
                                    <input type="radio" name="tagih" value="bengkel" /> Bengkel &nbsp; &nbsp; &nbsp;
                                    <input type="radio" name="tagih" value="vendor" /> Vendor
                                </g:else>

                            </div>
                        </div>

                    </fieldset>
                   </form>
                    <fieldset class="form-horizontal">
                        <div class="control-group">
                            <label class="control-label" for="tagih" style="text-align: left;">
                                Nama Job
                            </label>
                            <div id="filter_namaJob" class="controls">
                                <g:select name="job" id="namaJob" from="${Operation.createCriteria().list { eq("staDel",'0')}}" optionKey="id" optionValue="m053NamaOperation" noSelection="['-':'Pilih Job']" style="width: 300px" />
                                &nbsp;
                                <button style="width: 52px;height: 30px; border-radius: 5px" onclick="add()" class="btn btn-primary" name="add">add</button>
                                <button style="width: 52px;height: 30px; border-radius: 5px" onclick="allJob()" class="btn btn-primary" name="add">all</button>
                            </div>
                        </div>
                    </fieldset><br>
			<g:render template="dataTables" />
            <g:if test="${aksi=='ubah'}">
                <g:field type="button" onclick="updatePos()" class="btn btn-primary create" name="Edit"  value="${message(code: 'default.button.upload.label', default: 'Update')}" />
            </g:if>
            <g:else>
                <g:field type="button" onclick="createRequest()" class="btn btn-primary create" name="tambahSave"  value="${message(code: 'default.button.upload.label', default: 'Save')}" />
            </g:else>

            <g:field type="button" onclick="window.location.replace('#/viewOrderCat');" class="btn btn-cancel" name="cancel" id="cancel" value="Cancel" />
            <g:field type="button" onclick="deleteData()" class="btn btn-primary create" name="deleteData" id="delData" value="${message(code: 'default.button.upload.label', default: 'Delete')}" />
		</div>
		<div class="span7" id="pos-form" style="display: none;"></div>
	</div>
</body>
</html>

<g:javascript>

    <g:if test="${aksi=='ubah'}">
        document.getElementById("ket").focus();
    </g:if>
    <g:else>
        document.getElementById("noWo").focus();
    </g:else>
</g:javascript>