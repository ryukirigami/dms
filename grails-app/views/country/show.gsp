

<%@ page import="com.kombos.administrasi.Country" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'country.label', default: 'Country')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteCountry;

$(function(){ 
	deleteCountry=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/country/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadCountryTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-country" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="country"
			class="table table-bordered table-hover">
			<tbody>
 
			
				<g:if test="${countryInstance?.m109KodeNegara}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m109KodeNegara-label" class="property-label"><g:message
					code="country.m109KodeNegara.label" default="Kode Negara" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m109KodeNegara-label">
						%{--<ba:editableValue
								bean="${countryInstance}" field="m109KodeNegara"
								url="${request.contextPath}/Country/updatefield" type="text"
								title="Enter m109KodeNegara" onsuccess="reloadCountryTable();" />--}%
							
								<g:fieldValue bean="${countryInstance}" field="m109KodeNegara"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${countryInstance?.m109NamaNegara}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m109NamaNegara-label" class="property-label"><g:message
					code="country.m109NamaNegara.label" default="Nama Negara" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m109NamaNegara-label">
						%{--<ba:editableValue
								bean="${countryInstance}" field="m109NamaNegara"
								url="${request.contextPath}/Country/updatefield" type="text"
								title="Enter m109NamaNegara" onsuccess="reloadCountryTable();" />--}%
							
								<g:fieldValue bean="${countryInstance}" field="m109NamaNegara"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${countryInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="jenisStall.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${countryInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadJenisStallTable();" />--}%

                        <g:fieldValue bean="${countryInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
			
				 	<g:if test="${countryInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="jenisStall.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${countryInstance}" field="dateCreated"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:formatDate date="${countryInstance?.dateCreated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${countryInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="jenisStall.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${countryInstance}" field="createdBy"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:fieldValue bean="${countryInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${countryInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="jenisStall.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${countryInstance}" field="lastUpdated"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:formatDate date="${countryInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${countryInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="jenisStall.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${countryInstance}" field="updatedBy"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:fieldValue bean="${countryInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${countryInstance?.id}"
					update="[success:'country-form',failure:'country-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteCountry('${countryInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
