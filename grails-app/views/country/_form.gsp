<%@ page import="com.kombos.administrasi.Country" %>

 

<div class="control-group fieldcontain ${hasErrors(bean: countryInstance, field: 'm109KodeNegara', 'error')} required">
	<label class="control-label" for="m109KodeNegara">
		<g:message code="country.m109KodeNegara.label" default="Kode Negara" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">

		    <g:textField name="m109KodeNegara" id="m109KodeNegara" required="" value="${countryInstance?.m109KodeNegara}" maxlength="2"/>

	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: countryInstance, field: 'm109NamaNegara', 'error')} required">
	<label class="control-label" for="m109NamaNegara">
		<g:message code="country.m109NamaNegara.label" default="Nama Negara" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m109NamaNegara" required="" value="${countryInstance?.m109NamaNegara}" maxlength="20"/>
	</div>
</div>

<g:javascript>
    document.getElementById("m109KodeNegara").focus();
    $(document).ready(function() {
        $("#m109KodeNegara").keyup(function(e) {
            var isi = $(e.target).val();
            $(e.target).val(isi.toUpperCase());
        });
    });
    </script>
</g:javascript>