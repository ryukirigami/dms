<%@ page import="com.kombos.administrasi.Country" %>
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="country_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="country.m109KodeNegara.label" default="Kode Negara" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="country.m109NamaNegara.label" default="Nama Negara" /></div>
			</th>

		</tr>
		<tr>

			<th style="border-top: none;padding: 5px;">
                <div id="filter_m109KodeNegara" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    %{--<input type="checkbox" name="cekBox" class="checkall" />--}%
                    <input type="text" name="search_m109KodeNegara" id="search_m109KodeNegara" class="search_init" maxlength="2" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m109NamaNegara" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m109NamaNegara" class="search_init" />
				</div>
			</th>
	

		</tr>
	</thead>
</table>

<g:javascript>
var CountryTable;
var reloadCountryTable;
$(function(){
	reloadCountryTable = function() {
		CountryTable.fnDraw();
	}
	var recordscountryperpage = [];//new Array();
    var anCountrySelected;
    var jmlRecCountryPerPage=0;
    var id;

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	CountryTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	CountryTable = $('#country_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsCountry = $("#country_datatables tbody .row-select");
            var jmlCountryCek = 0;
            var nRow;
            var idRec;
            rsCountry.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordscountryperpage[idRec]=="1"){
                    jmlCountryCek = jmlCountryCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordscountryperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecCountryPerPage = rsCountry.length;
            if(jmlCountryCek==jmlRecCountryPerPage && jmlRecCountryPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            return nRow;
        },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

 

{
	"sName": "m109KodeNegara",
	"mDataProp": "m109KodeNegara",
	"aTargets": [1],
	"mRender": function ( data, type, row ) {
        return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m109NamaNegara",
	"mDataProp": "m109NamaNegara",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

 

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m109ID = $('#filter_m109ID input').val();
						if(m109ID){
							aoData.push(
									{"name": 'sCriteria_m109ID', "value": m109ID}
							);
						}
	
						var m109KodeNegara = $('#search_m109KodeNegara').val();
						if(m109KodeNegara){
							aoData.push(
									{"name": 'sCriteria_m109KodeNegara', "value": m109KodeNegara}
							);
						}
	
						var m109NamaNegara = $('#filter_m109NamaNegara input').val();
						if(m109NamaNegara){
							aoData.push(
									{"name": 'sCriteria_m109NamaNegara', "value": m109NamaNegara}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

    $('.select-all').click(function(e) {

        $("#country_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordscountryperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordscountryperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#country_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordscountryperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anCountrySelected = CountryTable.$('tr.row_selected');
            if(jmlRecCountryPerPage == anCountrySelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordscountryperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });


});
</g:javascript>



