<%@ page import="com.kombos.administrasi.Country" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'country.label', default: 'Country')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="create-country" class="content scaffold-create" role="main">
			<legend><g:message code="default.create.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${countryInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${countryInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form action="save" >--}%
			<g:formRemote class="form-horizontal" name="create" on404="alert('not found!');" onSuccess="reloadCountryTable();" update="country-form"
              url="[controller: 'country', action:'save']">	
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons controls">
					<a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout();"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
					<g:submitButton class="btn btn-primary create" name="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
				</fieldset>
			</g:formRemote>
			%{--</g:form>--}%
		</div>
	</body>
</html>
