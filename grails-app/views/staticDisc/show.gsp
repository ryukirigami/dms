

<%@ page import="com.kombos.administrasi.StaticDisc" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'staticDisc.label', default: 'StaticDisc')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteStaticDisc;

$(function(){ 
	deleteStaticDisc=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/staticDisc/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadStaticDiscTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-staticDisc" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="staticDisc"
			class="table table-bordered table-hover">
			<tbody>
				<g:if test="${staticDiscInstance?.m019TMT}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m019TMT-label" class="property-label"><g:message
					code="staticDisc.m019TMT.label" default="Tanggal Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m019TMT-label">
							<g:formatDate date="${staticDiscInstance?.m019TMT}" />
						</span></td>
					
				</tr>
				</g:if>
				<g:if test="${staticDiscInstance?.m019BookingSBJasa}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m019BookingSBJasa-label" class="property-label"><g:message
					code="staticDisc.m019BookingSBJasa.label" default="Persen Booking Jasa SB" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m019BookingSBJasa-label">
								<g:fieldValue bean="${staticDiscInstance}" field="m019BookingSBJasa"/>
							
						</span></td>
					
				</tr>
				</g:if>					
				<g:if test="${staticDiscInstance?.m019BookingGRBPJasa}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m019BookingGRBPJasa-label" class="property-label"><g:message
					code="staticDisc.m019BookingGRBPJasa.label" default="Persen Booking Jasa GR dan BP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m019BookingGRBPJasa-label">
						%{--<ba:editableValue
								bean="${staticDiscInstance}" field="m019BookingGRBPJasa"
								url="${request.contextPath}/StaticDisc/updatefield" type="text"
								title="Enter m019BookingGRBPJasa" onsuccess="reloadStaticDiscTable();" />--}%
							
								<g:fieldValue bean="${staticDiscInstance}" field="m019BookingGRBPJasa"/>
							
						</span></td>
					
				</tr>
				</g:if>
				
				<g:if test="${staticDiscInstance?.m019BookingSBPart}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m019BookingSBPart-label" class="property-label"><g:message
					code="staticDisc.m019BookingSBPart.label" default="Persen Booking Part SB" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m019BookingSBPart-label">
						%{--<ba:editableValue
								bean="${staticDiscInstance}" field="m019BookingSBPart"
								url="${request.contextPath}/StaticDisc/updatefield" type="text"
								title="Enter m019BookingSBPart" onsuccess="reloadStaticDiscTable();" />--}%
							
								<g:fieldValue bean="${staticDiscInstance}" field="m019BookingSBPart"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${staticDiscInstance?.m019BookingGRBPPart}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m019BookingGRBPPart-label" class="property-label"><g:message
					code="staticDisc.m019BookingGRBPPart.label" default="Persen Booking Part GR dan BP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m019BookingGRBPPart-label">
						%{--<ba:editableValue
								bean="${staticDiscInstance}" field="m019BookingGRBPPart"
								url="${request.contextPath}/StaticDisc/updatefield" type="text"
								title="Enter m019BookingGRBPPart" onsuccess="reloadStaticDiscTable();" />--}%
							
								<g:fieldValue bean="${staticDiscInstance}" field="m019BookingGRBPPart"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				
			
				<g:if test="${staticDiscInstance?.m019NonBookingSBJasa}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m019NonBookingSBJasa-label" class="property-label"><g:message
					code="staticDisc.m019NonBookingSBJasa.label" default="Persen Non Booking Jasa SB" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m019NonBookingSBJasa-label">
						%{--<ba:editableValue
								bean="${staticDiscInstance}" field="m019NonBookingSBJasa"
								url="${request.contextPath}/StaticDisc/updatefield" type="text"
								title="Enter m019NonBookingSBJasa" onsuccess="reloadStaticDiscTable();" />--}%
							
								<g:fieldValue bean="${staticDiscInstance}" field="m019NonBookingSBJasa"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${staticDiscInstance?.m019NonBookingGRBPJasa}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m019NonBookingGRBPJasa-label" class="property-label"><g:message
					code="staticDisc.m019NonBookingGRBPJasa.label" default="Persen Non Booking Jasa GR dan BP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m019NonBookingGRBPJasa-label">
						%{--<ba:editableValue
								bean="${staticDiscInstance}" field="m019NonBookingGRBPJasa"
								url="${request.contextPath}/StaticDisc/updatefield" type="text"
								title="Enter m019NonBookingGRBPJasa" onsuccess="reloadStaticDiscTable();" />--}%
							
								<g:fieldValue bean="${staticDiscInstance}" field="m019NonBookingGRBPJasa"/>
							
						</span></td>
					
				</tr>
				</g:if>
				
				<g:if test="${staticDiscInstance?.m019NonBookingSBPart}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m019NonBookingSBPart-label" class="property-label"><g:message
					code="staticDisc.m019NonBookingSBPart.label" default="Persen Non Booking Part SB" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m019NonBookingSBPart-label">
						%{--<ba:editableValue
								bean="${staticDiscInstance}" field="m019NonBookingSBPart"
								url="${request.contextPath}/StaticDisc/updatefield" type="text"
								title="Enter m019NonBookingSBPart" onsuccess="reloadStaticDiscTable();" />--}%
							
								<g:fieldValue bean="${staticDiscInstance}" field="m019NonBookingSBPart"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${staticDiscInstance?.m019NonBookingGRBPPart}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m019NonBookingGRBPPart-label" class="property-label"><g:message
					code="staticDisc.m019NonBookingGRBPPart.label" default="Persen Non Booking Part GR dan BP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m019NonBookingGRBPPart-label">
						%{--<ba:editableValue
								bean="${staticDiscInstance}" field="m019NonBookingGRBPPart"
								url="${request.contextPath}/StaticDisc/updatefield" type="text"
								title="Enter m019NonBookingGRBPPart" onsuccess="reloadStaticDiscTable();" />--}%
							
								<g:fieldValue bean="${staticDiscInstance}" field="m019NonBookingGRBPPart"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${staticDiscInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="staticDiscInstance.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${goodsInstance}" field="createdBy"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadGoodsTable();" />--}%

                        <g:fieldValue bean="${staticDiscInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${staticDiscInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="staticDiscInstance.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${goodsInstance}" field="updatedBy"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadGoodsTable();" />--}%

                        <g:fieldValue bean="${staticDiscInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${staticDiscInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="staticDiscInstance.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${goodsInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadGoodsTable();" />--}%

                        <g:fieldValue bean="${staticDiscInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${staticDiscInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="staticDiscInstance.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${goodsInstance}" field="dateCreated"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadGoodsTable();" />--}%

                        <g:formatDate date="${staticDiscInstance?.dateCreated}" format="EEEE, dd MMMM yyyy HH:mm" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${staticDiscInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="staticDiscInstance.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${goodsInstance}" field="lastUpdated"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadGoodsTable();" />--}%

                        <g:formatDate date="${staticDiscInstance?.lastUpdated}" format="EEEE, dd MMMM yyyy HH:mm"/>

                    </span></td>

                </tr>
            </g:if>
			
				
			
			
			
				
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${staticDiscInstance?.id}"
					update="[success:'staticDisc-form',failure:'staticDisc-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteStaticDisc('${staticDiscInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
