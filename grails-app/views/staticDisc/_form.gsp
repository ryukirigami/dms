<%@ page import="com.kombos.administrasi.StaticDisc" %>

<g:javascript>
    $(function(){
        $('.percentage').autoNumeric('init',{
            vMin:'0',
            vMax:'100',
            mDec: '2',
            aSep:''
        });
    });
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: staticDiscInstance, field: 'm019TMT', 'error')} ">
						<label class="control-label" for="m019TMT">
							<g:message code="staticDisc.m019TMT.label" default="Tanggal Berlaku" /><span class="required-indicator">*</span>
							
						</label>
						<div class="controls">
						<ba:datePicker name="m019TMT" precision="day" value="${staticDiscInstance?.m019TMT}" format="dd-MM-yyyy" required="true"/>
						</div>
					</div>
					<fieldset>
						<legend>Booking</legend>
						<div class="control-group fieldcontain ${hasErrors(bean: staticDiscInstance, field: 'm019BookingSBJasa', 'error')} ">
							<label class="control-label" for="m019BookingSBJasa">
								<g:message code="staticDisc.m019BookingSBJasa.label" default="Persen Booking Jasa SB" />
								
							</label>
							<div class="controls">
                                <g:textField class="percentage" name="m019BookingSBJasa" value="${staticDiscInstance.m019BookingSBJasa}"/>
							%{--<g:field type="number" name="m019BookingSBJasa" value="${staticDiscInstance.m019BookingSBJasa}" />--}%
							&nbsp;%
							</div>
						</div>
						<div class="control-group fieldcontain ${hasErrors(bean: staticDiscInstance, field: 'm019BookingGRBPJasa', 'error')} ">
							<label class="control-label" for="m019BookingGRBPJasa">
								<g:message code="staticDisc.m019BookingGRBPJasa.label" default="Persen Booking Jasa GR dan BP" />
								
							</label>
							<div class="controls">
                                <g:textField class="percentage" name="m019BookingGRBPJasa" value="${staticDiscInstance.m019BookingGRBPJasa}"/>
							%{--<g:field type="number" name="m019BookingGRBPJasa" value="${staticDiscInstance.m019BookingGRBPJasa}" />--}%
							&nbsp;%
							</div>
						</div>

						<div class="control-group fieldcontain ${hasErrors(bean: staticDiscInstance, field: 'm019BookingSBPart', 'error')} ">
							<label class="control-label" for="m019BookingSBPart">
								<g:message code="staticDisc.m019BookingSBPart.label" default="Persen Booking Part SB" />
								
							</label>
							<div class="controls">
                                <g:textField class="percentage" name="m019BookingSBPart" value="${staticDiscInstance.m019BookingSBPart}"/>
							%{--<g:field type="number" name="m019BookingSBPart" value="${staticDiscInstance.m019BookingSBPart}" />--}%
							&nbsp;%
							</div>
						</div>
						<div class="control-group fieldcontain ${hasErrors(bean: staticDiscInstance, field: 'm019BookingGRBPPart', 'error')} ">
							<label class="control-label" for="m019BookingGRBPPart">
								<g:message code="staticDisc.m019BookingGRBPPart.label" default="Persen Booking Part GR dan BP" />
								
							</label>
							<div class="controls">
                                <g:textField class="percentage" name="m019BookingGRBPPart" value="${staticDiscInstance.m019BookingGRBPPart}"/>
							%{--<g:field type="number" name="m019BookingGRBPPart" value="${staticDiscInstance.m019BookingGRBPPart}" />--}%
							&nbsp;%
							</div>
						</div>
					</fieldset>
					<fieldset>
						<legend>Non Booking</legend>
						<div class="control-group fieldcontain ${hasErrors(bean: staticDiscInstance, field: 'm019NonBookingSBJasa', 'error')} ">
							<label class="control-label" for="m019NonBookingSBJasa">
								<g:message code="staticDisc.m019NonBookingSBJasa.label" default="Persen Non Booking Jasa SB" />
								
							</label>
							<div class="controls">
                                <g:textField class="percentage" name="m019NonBookingSBJasa" value="${staticDiscInstance.m019NonBookingSBJasa}"/>
							%{--<g:field type="number" name="m019NonBookingSBJasa" value="${staticDiscInstance.m019NonBookingSBJasa}" />--}%
							&nbsp;%
							</div>
						</div>
						<div class="control-group fieldcontain ${hasErrors(bean: staticDiscInstance, field: 'm019NonBookingGRBPJasa', 'error')} ">
							<label class="control-label" for="m019NonBookingGRBPJasa">
								<g:message code="staticDisc.m019NonBookingGRBPJasa.label" default="Persen Non Booking Jasa GR dan BP" />
								
							</label>
							<div class="controls">
                                <g:textField class="percentage" name="m019NonBookingGRBPJasa" value="${staticDiscInstance.m019NonBookingGRBPJasa}"/>
							%{--<g:field type="number" name="m019NonBookingGRBPJasa" value="${staticDiscInstance.m019NonBookingGRBPJasa}" />--}%
							&nbsp;%
							</div>
						</div>
						<div class="control-group fieldcontain ${hasErrors(bean: staticDiscInstance, field: 'm019NonBookingSBPart', 'error')} ">
							<label class="control-label" for="m019NonBookingSBPart">
								<g:message code="staticDisc.m019NonBookingSBPart.label" default="Persen Non Booking Part SB" />
								
							</label>
							<div class="controls">
                                <g:textField class="percentage" name="m019NonBookingSBPart" value="${staticDiscInstance.m019NonBookingSBPart}"/>
							%{--<g:field type="number" name="m019NonBookingSBPart" value="${staticDiscInstance.m019NonBookingSBPart}" />--}%
							&nbsp;%
							</div>
						</div>
						<div class="control-group fieldcontain ${hasErrors(bean: staticDiscInstance, field: 'm019NonBookingGRBPPart', 'error')} ">
							<label class="control-label" for="m019NonBookingGRBPPart">
								<g:message code="staticDisc.m019NonBookingGRBPPart.label" default="Persen Non Booking Part GR dan BP" />
								
							</label>
							<div class="controls">
                                <g:textField class="percentage" name="m019NonBookingGRBPPart" value="${staticDiscInstance.m019NonBookingGRBPPart}"/>
							%{--<g:field type="number" name="m019NonBookingGRBPPart" value="${staticDiscInstance.m019NonBookingGRBPPart}" />--}%
							&nbsp;%
							</div>
						</div>
					</fieldset>	

