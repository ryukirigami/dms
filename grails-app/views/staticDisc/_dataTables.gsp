
<%@ page import="com.kombos.administrasi.StaticDisc" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="staticDisc_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>
			
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="staticDisc.m019TMT.label" default="Tanggal Berlaku" /></div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="staticDisc.m019BookingSBJasa.label" default="Persen Booking Jasa SB" /></div>
			</th>			
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="staticDisc.m019BookingGRBPJasa.label" default="Persen Booking Jasa GR dan BP" /></div>
			</th>
            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="staticDisc.m019NonBookingSBJasa.label" default="Persen Non Booking Jasa SB" /></div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="staticDisc.m019NonBookingGRBPJasa.label" default="Persen Non Booking Jasa GR dan BP" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
				<div><g:message code="staticDisc.m019BookingSBPart.label" default="Persen Booking Part SB" /></div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="staticDisc.m019BookingGRBPPart.label" default="Persen Booking Part GR dan BP" /></div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="staticDisc.m019NonBookingSBPart.label" default="Persen Non Booking Part SB" /></div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="staticDisc.m019NonBookingGRBPPart.label" default="Persen Non Booking Part GR dan BP" /></div>
			</th>		
		</tr>
	</thead>
</table>

<g:javascript>
var staticDiscTable;
var reloadStaticDiscTable;
var checked = [];
$(function(){
	
	reloadStaticDiscTable = function() {
		staticDiscTable.fnDraw();
	}

	
	$('#search_m019TMT').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m019TMT_day').val(newDate.getDate());
			$('#search_m019TMT_month').val(newDate.getMonth()+1);
			$('#search_m019TMT_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			staticDiscTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	staticDiscTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	staticDiscTable = $('#staticDisc_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
		        var aData = staticDiscTable.fnGetData(nRow);
		        var checkbox = $('.row-select', nRow);
		        if(checked.indexOf(""+aData['id'])>=0){
		              checkbox.attr("checked",true);
		              checkbox.parent().parent().addClass('row_selected');
		        }

		        checkbox.click(function (e) {
            	    var tc = $(this);

                    if(this.checked)
            			tc.parent().parent().addClass('row_selected');
            		else {
            			tc.parent().parent().removeClass('row_selected');
            			var selectAll = $('.select-all');
            			selectAll.removeAttr('checked');
                    }
            	 	e.stopPropagation();
                });
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m019TMT",
	"mDataProp": "m019TMT",
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "m019BookingSBJasa",
	"mDataProp": "m019BookingSBJasa",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "m019BookingGRBPJasa",
	"mDataProp": "m019BookingGRBPJasa",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "m019NonBookingSBJasa",
	"mDataProp": "m019NonBookingSBJasa",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "m019NonBookingGRBPJasa",
	"mDataProp": "m019NonBookingGRBPJasa",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "m019BookingSBPart",
	"mDataProp": "m019BookingSBPart",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "m019BookingGRBPPart",
	"mDataProp": "m019BookingGRBPPart",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},

{
	"sName": "m019NonBookingSBPart",
	"mDataProp": "m019NonBookingSBPart",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "m019NonBookingGRBPPart",
	"mDataProp": "m019NonBookingGRBPPart",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        var selectAll = $('.select-all');
                selectAll.removeAttr('checked');
                $("#staticDisc_datatables tbody .row-select").each(function() {
                            var id = $(this).next("input:hidden").val();
                            if(checked.indexOf(""+id)>=0){
                                checked[checked.indexOf(""+id)]="";
                            }
                            if(this.checked){
                                checked.push(""+id);
                            }
                        });

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
