
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<g:javascript>
</g:javascript>

<table id="technisian_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div>
                <g:message code="timeTrackingGR.technisian.label" default="Technisian" /></div>
        </th>

    </tr>
    <tr>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_teknisi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_teknisi" class="search_init" />
            </div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var technisianTable;
var reloadTechnisianTable;
$(function(){
	var recordsTeknisiPerPage = [];//new Array();
    var anTeknisiSelected;
    var jmlRecTeknisiPerPage=0;
    var id;
	reloadTechnisianTable = function() {
		technisianTable.fnDraw();
	}

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	technisianTable.fnDraw();
		}
	});

	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	technisianTable = $('#technisian_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsTeknisi = $("#technisian_datatables tbody .row-select");
            var jmlTeknisiCek = 0;
            var nRow;
            var idRec;
            rsTeknisi.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsTeknisiPerPage[idRec]=="1"){
                    jmlTeknisiCek = jmlTeknisiCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsTeknisiPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecTeknisiPerPage = rsTeknisi.length;
            if(jmlTeknisiCek==jmlRecTeknisiPerPage && jmlRecTeknisiPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesTechnisianList")}",
		"aoColumns": [

{
	"sName": "t015NamaBoard",
	"mDataProp": "teknisi",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this" value="'+row['id']+'" name="teknisi">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100%",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

            var teknisi = $('#filter_teknisi input').val();
            if(teknisi){
                aoData.push(
                        {"name": 'teknisi', "value": teknisi}
                );
            }

            var workshop = $('#workshop').val();
            if(workshop){
                aoData.push(
                        {"name": 'workshop', "value": workshop}
                );
            }
            $.ajax({ "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData ,
                "success": function (json) {
                    fnCallback(json);
                   },
                "complete": function () {
                   }
                    });
		}			
	});
	$('.select-all').click(function(e) {

        $("#technisian_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsTeknisiPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsTeknisiPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#technisian_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsTeknisiPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anTeknisiSelected = technisianTable.$('tr.row_selected');
            if(jmlRecTeknisiPerPage == anTeknisiSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsTeknisiPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
	
});
</g:javascript>


			
