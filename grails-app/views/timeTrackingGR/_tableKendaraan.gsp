
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<g:javascript>
</g:javascript>

<table id="kendaraan_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div>
                <g:message code="timeTrackingGR.jenisKendaraan.label" default="Jenis Kendaraan" /></div>
        </th>

    </tr>
    <tr>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_m104NamaModelName" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_m104NamaModelName" class="search_init" />
            </div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var kendaraanTable;
var reloadKendaraanTable;
$(function(){
	
	reloadKendaraanTable = function() {
		kendaraanTable.fnDraw();
	}
    var recordsKendaraanPerPage = [];//new Array();
    var anKendaraanSelected;
    var jmlRecKendaraanPerPage=0;
    var id;


    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	kendaraanTable.fnDraw();
		}
	});

	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	kendaraanTable = $('#kendaraan_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsKendaraan = $("#kendaraan_datatables tbody .row-select");
            var jmlKendaraanCek = 0;
            var nRow;
            var idRec;
            rsKendaraan.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsKendaraanPerPage[idRec]=="1"){
                    jmlKendaraanCek = jmlKendaraanCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsKendaraanPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecKendaraanPerPage = rsKendaraan.length;
            if(jmlKendaraanCek==jmlRecKendaraanPerPage && jmlRecKendaraanPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
        "fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesKendaraanList")}",
		"aoColumns": [

{
	"sName": "m102NamaBaseModel",
	"mDataProp": "m102NamaBaseModel",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this" value="'+row['id']+'" name="jenisKendaraan">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100%",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
            var m102NamaBaseModel = $('#filter_m104NamaModelName input').val();
            if(m102NamaBaseModel){
                aoData.push(
                        {"name": 'm102NamaBaseModel', "value": m102NamaBaseModel}
                );
            }
	
						
            $.ajax({ "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData ,
                "success": function (json) {
                    fnCallback(json);
                   },
                "complete": function () {
                   }
                    });
		}			
	});

	$('.select-all').click(function(e) {

        $("#kendaraan_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsKendaraanPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsKendaraanPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#kendaraan_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsKendaraanPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anKendaraanSelected = kendaraanTable.$('tr.row_selected');
            if(jmlRecKendaraanPerPage == anKendaraanSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsKendaraanPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });	
});
</g:javascript>


			
