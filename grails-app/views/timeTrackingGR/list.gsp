
<%@ page import="com.kombos.administrasi.CompanyDealer; com.kombos.administrasi.TipeBerat" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="timeTrackingGR.label" default="Report Time Tracking (GR)"/></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
        var checkin = $('#search_tanggal').datepicker({
            onRender: function(date) {
                return '';
            }
        }).on('changeDate', function(ev) {
                    var newDate = new Date(ev.date)
                    newDate.setDate(newDate.getDate());
                    checkout.setValue(newDate);
                    checkin.hide();
                    $('#search_tanggal2')[0].focus();
                }).data('datepicker');

        var checkout = $('#search_tanggal2').datepicker({
            onRender: function(date) {
                return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
                    checkout.hide();
                }).data('datepicker');

    previewData = function(){
        var workshop = $('#workshop').val();
        var search_tanggal = $('#search_tanggal').val();
        var search_tanggal2 = $('#search_tanggal2').val();

        var ceklis = "";
		var sa = "";
		var jenisPekerjaan = "";
		var teknisi = "";
		var jobStopped = "";
		var jenisKendaraan = "";

		$("input[name='sa']").each(function (){
			if ($(this).attr("checked")) {
				sa += $(this).val() + ',';
			}
		});
		sa = sa.substr(0, sa.length - 1);

		$("input[name='teknisi']").each(function (){
			if ($(this).attr("checked")) {
				teknisi += $(this).val() + ',';
			}
		});
		teknisi = teknisi.substr(0, teknisi.length - 1);

		$("input[name='jobStopped']").each(function (){
			if ($(this).attr("checked")) {
				jobStopped += $(this).val() + ',';
			}
		});
		jobStopped = jobStopped.substr(0, jobStopped.length - 1);

		$("input[name='jenisPekerjaan']").each(function (){
			if ($(this).attr("checked")) {
				jenisPekerjaan += $(this).val() + ',';
			}
		});
		jenisPekerjaan = jenisPekerjaan.substr(0, jenisPekerjaan.length - 1);

		$("input[name='jenisKendaraan']").each(function (){
			if ($(this).attr("checked")) {
				jenisKendaraan += $(this).val() + ',';
			}
		});
		jenisKendaraan = jenisKendaraan.substr(0, jenisKendaraan.length - 1);

        var url = "workshop="+workshop+"&tanggal="+search_tanggal+"&tanggal2="+search_tanggal2+"&sa="+sa+"&jenisPekerjaan="+jenisPekerjaan+
 		                    "&jenisKendaraan="+jenisKendaraan+"&jobStopped="+jobStopped+"&teknisi="+teknisi;
        console.log(url)
 		window.location = "${request.contextPath}/timeTrackingGR/previewData?"+url;
    }
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="timeTrackingGR.label" default="Report Time Tracking (GR)"/></span>
    <ul class="nav pull-right">
    </ul>
</div>
<div class="box">
    <div class="span12" id="timeTrackingGR-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <div class="row-fluid form-horizontal">
            <div id="kiri" class="span6">
                <div class="control-group">
                    <label class="control-label" for="workshop" style="text-align: left;">
                        Workshop
                    </label>
                    <div class="controls">
                        <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                            <g:select name="workshop" id="workshop" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" onchange="reloadSaTable();reloadTechnisianTable();" />
                        </g:if>
                        <g:else>
                            <g:select name="workshop" id="workshop" readonly="" disabled="" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                        </g:else>

                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="workshop" style="text-align: left;">
                        Kategori Workshop
                    </label>
                    <label class="control-label" for="workshop">
                        <b>GENERAL REPAIR</b>
                    </label>
                </div>
                <div class="control-group">
                    <label class="control-label" for="t951Tanggal"
                           style="text-align: left;"> Periode Awal</label>
                    <div id="filter_periode" class="controls">
                        <ba:datePicker id="search_tanggal" name="search_tanggal"
                                       precision="day" format="dd-MM-yyyy" value="${new Date()}" />

                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="t951Tanggal"
                           style="text-align: left;"> Periode Akhir</label>
                    <div id="filter_periode2" class="controls">
                        <ba:datePicker id="search_tanggal2" name="search_tanggal2"
                                       precision="day" format="dd-MM-yyyy" value="${new Date()}" />
                    </div>
                </div>
                <div class="control-group" style="vertical-align: middle">
                    <label class="control-label" for="workshop" style="text-align: left;">
                        Jenis Pekerjaan
                    </label>
                    <div class="controls">
                        <g:render template="tablePekerjaan" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="workshop" style="text-align: left;">
                        Job Stopped Status
                    </label>
                    <div class="controls">
                        <g:render template="tableStoppedStatus"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="workshop" style="text-align: left;">
                        Jenis Kendaraan
                    </label>
                    <div class="controls">
                        <g:render template="tableKendaraan"/>
                    </div>
                </div>
            </div>
            <div id="kanan" class="span6">
                <div class="control-group">
                    <label class="control-label" for="workshop" style="text-align: left;">
                        Technisian
                    </label>
                    <div class="controls">
                        <g:render template="tableTechnisian" />
                    </div>
                </div>	
                <div class="control-group">
                    <label class="control-label" for="workshop" style="text-align: left;">
                        Service Advisor
                    </label>
                    <div class="controls">
                        <g:render template="tableSA" />
                    </div>
                </div>
            </div>
        </div>
        <g:field type="button" onclick="previewData()" class="btn btn-primary create" name="preview"  value="${message(code: 'default.button.upload.label', default: 'Preview')}" />
        <g:field type="button" onclick="window.location.replace('#/home')" class="btn btn-cancel cancel" name="cancel"  value="${message(code: 'default.button.upload.label', default: 'Close')}" />
    </div>
    <div class="span7" id="timeTrackingGR-form" style="display: none;"></div>
</div>
</body>
</html>
