<%@ page import="com.kombos.administrasi.RepairDifficulty; com.kombos.administrasi.CompanyDealer; com.kombos.administrasi.PanelRepair" %>
<r:require modules="autoNumeric" />
<script type="text/javascript">
    jQuery(function($) {
        $('.auto').autoNumeric('init',{
            vMin:'0',
            aSep:'',
            mDec:'2'
        });
    });
</script>


%{--<div class="control-group fieldcontain ${hasErrors(bean: panelRepairInstance, field: 'companyDealer', 'error')} required">--}%
	%{--<label class="control-label" for="companyDealer">--}%
		%{--<g:message code="panelRepair.companyDealer.label" default="Company Dealer" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:select id="companyDealer" name="companyDealer.id" from="${CompanyDealer.list()}" optionKey="id" required="" value="${panelRepairInstance?.companyDealer?.id}" class="many-to-one"/>--}%
	%{--</div>--}%
%{--</div>--}%

<div class="control-group fieldcontain ${hasErrors(bean: panelRepairInstance, field: 'm043TanggalBerlaku', 'error')} required">
	<label class="control-label" for="m043TanggalBerlaku">
		<g:message code="panelRepair.m043TanggalBerlaku.label" default="Tanggal Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<ba:datePicker name="m043TanggalBerlaku" precision="day"  value="${panelRepairInstance?.m043TanggalBerlaku}" format="dd/mm/yyyy" required="true"  />
	</div>
</div>
<g:javascript>
    document.getElementById('m043TanggalBerlaku').focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: panelRepairInstance, field: 'repairDifficulty', 'error')} required">
	<label class="control-label" for="repairDifficulty">
		<g:message code="panelRepair.repairDifficulty.label" default="Repair Difficulty" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="repairDifficulty" name="repairDifficulty.id" from="${RepairDifficulty.createCriteria().list(){order("m042Tipe", "asc")}}" optionKey="id" required="" value="${panelRepairInstance?.repairDifficulty?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: panelRepairInstance, field: 'm043Area1', 'error')} required">
	<label class="control-label" for="m043Area1">
		<g:message code="panelRepair.m043Area1.label" default="Area" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField class="auto" style="width: 75px" name="m043Area1" value="${fieldValue(bean: panelRepairInstance, field: 'm043Area1')}" required=""/>
&nbsp;&nbsp;s.d.&nbsp;&nbsp;
	<g:textField class="auto" style="width: 75px" name="m043Area2" value="${fieldValue(bean: panelRepairInstance, field: 'm043Area2')}" required=""/>
    &nbsp;100 cm2
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: panelRepairInstance, field: 'm043StdTime', 'error')} required">
	<label class="control-label" for="m043StdTime">
		<g:message code="panelRepair.m043StdTime.label" default="Standar Time" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField class="auto" name="m043StdTime" value="${fieldValue(bean: panelRepairInstance, field: 'm043StdTime')}" required=""/>&nbsp;Jam
	</div>
</div>
