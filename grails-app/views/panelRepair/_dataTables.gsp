
<%@ page import="com.kombos.administrasi.PanelRepair" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="panelRepair_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="panelRepair.companyDealer.label" default="Company Dealer" /></div>--}%
			%{--</th>--}%


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="panelRepair.m043TanggalBerlaku.label" default="Tanggal Berlaku" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="panelRepair.repairDifficulty.label" default="Repair Difficulty" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="panelRepair.m043Area1.label" default="Area 1" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="panelRepair.m043Area2.label" default="Area 2" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="panelRepair.m043StdTime.label" default="Standar Time" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="panelRepair.m043ID.label" default="M043 ID" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="panelRepair.createdBy.label" default="Created By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="panelRepair.updatedBy.label" default="Updated By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="panelRepair.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>

		
		</tr>
		<tr>


			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_companyDealer" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m043TanggalBerlaku" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m043TanggalBerlaku" value="date.struct">
					<input type="hidden" name="search_m043TanggalBerlaku_day" id="search_m043TanggalBerlaku_day" value="">
					<input type="hidden" name="search_m043TanggalBerlaku_month" id="search_m043TanggalBerlaku_month" value="">
					<input type="hidden" name="search_m043TanggalBerlaku_year" id="search_m043TanggalBerlaku_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m043TanggalBerlaku_dp" value="" id="search_m043TanggalBerlaku" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_repairDifficulty" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_repairDifficulty" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m043Area1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m043Area1" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m043Area2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m043Area2" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m043StdTime" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m043StdTime" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m043ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m043ID" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_createdBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_updatedBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var panelRepairTable;
var reloadPanelRepairTable;
$(function(){

    var recordspanelRepairperpage = [];//new Array();
    var anCountrySelected;
    var jmlRecCountryPerPage=0;
    var id;

	
	reloadPanelRepairTable = function() {
		panelRepairTable.fnDraw();
	}

	
	$('#search_m043TanggalBerlaku').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m043TanggalBerlaku_day').val(newDate.getDate());
			$('#search_m043TanggalBerlaku_month').val(newDate.getMonth()+1);
			$('#search_m043TanggalBerlaku_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			panelRepairTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	panelRepairTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	panelRepairTable = $('#panelRepair_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsCountry = $("#panelRepair_datatables tbody .row-select");
            var jmlCountryCek = 0;
            var nRow;
            var idRec;
            rsCountry.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordspanelRepairperpage[idRec]=="1"){
                    jmlCountryCek = jmlCountryCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordspanelRepairperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecCountryPerPage = rsCountry.length;
            if(jmlCountryCek==jmlRecCountryPerPage && jmlRecCountryPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

//{
//	"sName": "companyDealer",
//	"mDataProp": "companyDealer",
//	"aTargets": [0],
//
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"300px",
//	"bVisible": false
//}
//
//,

{
	"sName": "m043TanggalBerlaku",
	"mDataProp": "m043TanggalBerlaku",
	"aTargets": [1],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "repairDifficulty",
	"mDataProp": "repairDifficulty",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m043Area1",
	"mDataProp": "m043Area1",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m043Area2",
	"mDataProp": "m043Area2",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m043StdTime",
	"mDataProp": "m043StdTime",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m043ID",
	"mDataProp": "m043ID",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "createdBy",
	"mDataProp": "createdBy",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "updatedBy",
	"mDataProp": "updatedBy",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}

						var m043TanggalBerlaku = $('#search_m043TanggalBerlaku').val();
						var m043TanggalBerlakuDay = $('#search_m043TanggalBerlaku_day').val();
						var m043TanggalBerlakuMonth = $('#search_m043TanggalBerlaku_month').val();
						var m043TanggalBerlakuYear = $('#search_m043TanggalBerlaku_year').val();
						
						if(m043TanggalBerlaku){
							aoData.push(
									{"name": 'sCriteria_m043TanggalBerlaku', "value": "date.struct"},
									{"name": 'sCriteria_m043TanggalBerlaku_dp', "value": m043TanggalBerlaku},
									{"name": 'sCriteria_m043TanggalBerlaku_day', "value": m043TanggalBerlakuDay},
									{"name": 'sCriteria_m043TanggalBerlaku_month', "value": m043TanggalBerlakuMonth},
									{"name": 'sCriteria_m043TanggalBerlaku_year', "value": m043TanggalBerlakuYear}
							);
						}
	
						var repairDifficulty = $('#filter_repairDifficulty input').val();
						if(repairDifficulty){
							aoData.push(
									{"name": 'sCriteria_repairDifficulty', "value": repairDifficulty}
							);
						}
	
						var m043Area1 = $('#filter_m043Area1 input').val();
						if(m043Area1){
							aoData.push(
									{"name": 'sCriteria_m043Area1', "value": m043Area1}
							);
						}
	
						var m043Area2 = $('#filter_m043Area2 input').val();
						if(m043Area2){
							aoData.push(
									{"name": 'sCriteria_m043Area2', "value": m043Area2}
							);
						}
	
						var m043StdTime = $('#filter_m043StdTime input').val();
						if(m043StdTime){
							aoData.push(
									{"name": 'sCriteria_m043StdTime', "value": m043StdTime}
							);
						}
	
						var m043ID = $('#filter_m043ID input').val();
						if(m043ID){
							aoData.push(
									{"name": 'sCriteria_m043ID', "value": m043ID}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

	$('.select-all').click(function(e) {

        $("#panelRepair_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordspanelRepairperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordspanelRepairperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#panelRepair_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordspanelRepairperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anCountrySelected = CountryTable.$('tr.row_selected');
            if(jmlRecCountryPerPage == anCountrySelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordspanelRepairperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });


});
</g:javascript>


			
