<%@ page import="com.kombos.customerFollowUp.FollowUpByPhone" %>

<g:javascript>
    function loadSerial(idInit){
        var root="${resource()}";
        var sectionId=document.getElementById('section').value;

        if(idInit){
            sectionId = idInit;
        }

        var url = root+'/followUpByPhone/findSerialBySection?section.id='+sectionId;

        jQuery('#serial').load(url);
    }

    function loadJob(idInit){
        var root="${resource()}";
        var serialId=document.getElementById('serial').value;

        if(idInit){
            serialId = idInit;
        }

        var url = root+'/followUpByPhone/findJobBySerial?serial.id='+serialId;

        jQuery('#operation').load(url);
    }
</g:javascript>


<div class="control-group fieldcontain ${hasErrors(bean: followUpByPhoneInstance?.operation, field: 'section', 'error')} required">
	<label class="control-label" for="section">
		<g:message code="followUpByPhone.section.label" default="Section Job" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="section" name="section.id" noSelection="['-1':'Pilih Section']" from="${com.kombos.administrasi.Section.createCriteria().list {eq("staDel","0");order("m051NamaSection")}}" optionKey="id" required="" value="${followUpByPhoneInstance?.operation?.section?.id}" onchange="loadSerial();" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain required">
	<label class="control-label" for="serial">
		<g:message code="followUpByPhone.serial.label" default="Serial Job" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:if test="${followUpByPhoneInstance?.operation}">
            <g:select id="serial" name="serial.id" noSelection="['':'Pilih Serial']" from="${com.kombos.administrasi.Serial.createCriteria().list {eq("staDel","0");section{eq("id",followUpByPhoneInstance?.operation?.section?.id)};order("m052NamaSerial")}}" optionValue="m052NamaSerial" optionKey="id" required="" value="${followUpByPhoneInstance?.operation?.serial?.id}" onchange="loadJob();" class="many-to-one"/>
        </g:if>
        <g:else>
            <g:select id="serial" name="serial.id" noSelection="['':'Pilih Serial']" from="${[]}" optionKey="id" required="" value="${followUpByPhoneInstance?.operation?.serial?.id}" onchange="loadJob();" class="many-to-one"/>
        </g:else>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: followUpByPhoneInstance, field: 'operation', 'error')} required">
	<label class="control-label" for="operation">
		<g:message code="followUpByPhone.operation.label" default="Nama Job" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:if test="${followUpByPhoneInstance?.operation}">
            <g:select id="operation" name="operation.id" noSelection="['':'Pilih Job']" from="${com.kombos.administrasi.Operation.createCriteria().list {eq("staDel","0");serial{eq("id",followUpByPhoneInstance?.operation?.serial?.id)}}}" optionValue="m053NamaOperation" optionKey="id" required="" value="${followUpByPhoneInstance?.operation?.id}" class="many-to-one"/>
        </g:if>
        <g:else>
            <g:select id="operation" name="operation.id" noSelection="['':'Pilih Job']" from="${[]}" optionKey="id" required="" value="${followUpByPhoneInstance?.operation?.id}" class="many-to-one"/>
        </g:else>
	</div>
</div>

