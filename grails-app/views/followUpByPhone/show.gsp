

<%@ page import="com.kombos.customerFollowUp.FollowUpByPhone" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'followUpByPhone.label', default: 'Jobs Follow Up By Phone')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteFollowUpByPhone;

$(function(){ 
	deleteFollowUpByPhone=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/followUpByPhone/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadFollowUpByPhoneTable();
   				expandTableLayout('followUpByPhone');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-followUpByPhone" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="followUpByPhone"
			class="table table-bordered table-hover">
			<tbody>

            <g:if test="${followUpByPhoneInstance?.operation?.section}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="section-label" class="property-label"><g:message
                                code="followUpByPhone.section.label" default="Section Job" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="section-label">
                        <g:fieldValue field="m051NamaSection" bean="${followUpByPhoneInstance?.operation?.section}" />
                    </span></td>

                </tr>
            </g:if>

            <g:if test="${followUpByPhoneInstance?.operation?.serial}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="serial-label" class="property-label"><g:message
                                code="followUpByPhone.serial.label" default="Serial Job" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="serial-label">
                        <g:fieldValue field="m052NamaSerial" bean="${followUpByPhoneInstance?.operation?.serial}" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${followUpByPhoneInstance?.operation}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="operation-label" class="property-label"><g:message
                                code="followUpByPhone.operation.label" default="Nama Job" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="operation-label">
                        <g:fieldValue field="m053NamaOperation" bean="${followUpByPhoneInstance?.operation}" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${followUpByPhoneInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="followUpByPhone.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${followUpByPhoneInstance}" field="dateCreated"
                                url="${request.contextPath}/FollowUpByPhone/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadFollowUpByPhoneTable();" />--}%

                        <g:formatDate date="${followUpByPhoneInstance?.dateCreated}" format="dd MMMM yyyy, HH:mm:ss"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${followUpByPhoneInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="followUpByPhone.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${followUpByPhoneInstance}" field="createdBy"
								url="${request.contextPath}/FollowUpByPhone/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadFollowUpByPhoneTable();" />--}%
							
								<g:fieldValue bean="${followUpByPhoneInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
            </g:if>

            <g:if test="${followUpByPhoneInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="followUpByPhone.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        <g:formatDate date="${followUpByPhoneInstance?.lastUpdated}" format="dd MMMM yyyy, HH:mm:ss"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${followUpByPhoneInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="followUpByPhone.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        <g:fieldValue bean="${followUpByPhoneInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${followUpByPhoneInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="followUpByPhone.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
								<g:fieldValue bean="${followUpByPhoneInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			

			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('followUpByPhone');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${followUpByPhoneInstance?.id}"
					update="[success:'followUpByPhone-form',failure:'followUpByPhone-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteFollowUpByPhone('${followUpByPhoneInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
