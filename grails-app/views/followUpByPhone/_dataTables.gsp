
<%@ page import="com.kombos.customerFollowUp.FollowUpByPhone" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="followUpByPhone_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div style="text-align: center"><g:message code="followUpByPhone.operation.label" default="Nama Job" /></div>
			</th>

		
		</tr>
		<tr>
		

			<th style="border-top: none;padding: 5px;">
				<div id="filter_operation" style="padding-top: 0px;position:relative; margin-top: 0px;width: 98%;">
					<input type="text" name="search_operation" class="search_init" style="width: 98%" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var followUpByPhoneTable;
var reloadFollowUpByPhoneTable;
$(function(){
	
	reloadFollowUpByPhoneTable = function() {
		followUpByPhoneTable.fnDraw();
	}

    var recordsfollowUpByPhoneperpage = [];
    var anFollowUpByPhoneSelected;
    var jmlRecFollowUpByPhonePerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	followUpByPhoneTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	followUpByPhoneTable = $('#followUpByPhone_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsFollowUpByPhone = $("#followUpByPhone_datatables tbody .row-select");
            var jmlFollowUpByPhoneCek = 0;
            var nRow;
            var idRec;
            rsFollowUpByPhone.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsfollowUpByPhoneperpage[idRec]=="1"){
                    jmlFollowUpByPhoneCek = jmlFollowUpByPhoneCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsfollowUpByPhoneperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecFollowUpByPhonePerPage = rsFollowUpByPhone.length;
            if(jmlFollowUpByPhoneCek==jmlRecFollowUpByPhonePerPage && jmlRecFollowUpByPhonePerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "operation",
	"mDataProp": "operation",
    "mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}
	
						var m802Ket = $('#filter_m802Ket input').val();
						if(m802Ket){
							aoData.push(
									{"name": 'sCriteria_m802Ket', "value": m802Ket}
							);
						}
	
						var operation = $('#filter_operation input').val();
						if(operation){
							aoData.push(
									{"name": 'sCriteria_operation', "value": operation}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
    $('.select-all').click(function(e) {
        $("#followUpByPhone_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsfollowUpByPhoneperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsfollowUpByPhoneperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

    $('#followUpByPhone_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsfollowUpByPhoneperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anFollowUpByPhoneSelected = followUpByPhoneTable.$('tr.row_selected');
            if(jmlRecFollowUpByPhonePerPage == anFollowUpByPhoneSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsfollowUpByPhoneperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
