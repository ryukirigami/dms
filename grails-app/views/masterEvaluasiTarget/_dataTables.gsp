
<%@ page import="com.kombos.finance.MasterEvaluasiTarget" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="masterEvaluasiTarget_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.companyDealer.label" default="Company Dealer" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.monthYear.label" default="Periode" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.serviceType.label" default="Service Type" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.antikarat.label" default="Antikarat" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.spooring.label" default="Spooring" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.jasa.label" default="Jasa" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.parts.label" default="Parts" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.bahan.label" default="Bahan" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.lainLain.label" default="Lain Lain" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.gajiMekanik.label" default="Gaji Mekanik" /></div>
            </th>

            %{--<th style="border-bottom: none;padding: 5px;">--}%
                %{--<div><g:message code="masterEvaluasiTarget.gajiMekanikLainLain.label" default="Gaji Mekanik Lain Lain" /></div>--}%
            %{--</th>--}%

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.insentifLemburMek.label" default="Insentif Lembur Mek" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.gajiAdm.label" default="Gaji Adm" /></div>
            </th>

            %{--<th style="border-bottom: none;padding: 5px;">--}%
                %{--<div><g:message code="masterEvaluasiTarget.gajiAdmLainLain.label" default="Gaji Adm Lain Lain" /></div>--}%
            %{--</th>--}%

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.insentifAdm.label" default="Insentif Adm" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.biayaParts.label" default="Biaya Parts" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.biayaBahan.label" default="Biaya Bahan" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.biayaPekLuar.label" default="Biaya Pek Luar" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.biayaSpooring.label" default="Biaya Spooring" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.biayaPerjalanan.label" default="Biaya Perjalanan" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.biayaUmum.label" default="Biaya Umum" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.biayaKomisi.label" default="Biaya Komisi" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.claimRepair.label" default="Claim Repair" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.biayaTakTerduga.label" default="Biaya Tak Terduga" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.biayaPenyusutan.label" default="Biaya Penyusutan" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.biayaSewaGedung.label" default="Biaya Sewa Gedung" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.jumlahStall.label" default="Jumlah Stall" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.mekanik.label" default="Mekanik" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.repairUnit.label" default="Repair Unit" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.repairOrder.label" default="Repair Order" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.paymentRate.label" default="Payment Rate" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="masterEvaluasiTarget.creditPaymentRate.label" default="Credit Payment Rate" /></div>
            </th>


        </tr>
		<tr>
            <th style="border-top: none;padding: 5px;">
                <div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_companyDealer" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_monthYear" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_monthYear" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_serviceType" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_serviceType" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_antikarat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_antikarat" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_spooring" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_spooring" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_jasa" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_jasa" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_parts" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_parts" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_bahan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_bahan" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_lainLain" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_lainLain" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_gajiMekanik" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_gajiMekanik" class="search_init" />
                </div>
            </th>

            %{--<th style="border-top: none;padding: 5px;">--}%
                %{--<div id="filter_gajiMekanikLainLain" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
                    %{--<input type="text" name="search_gajiMekanikLainLain" class="search_init" />--}%
                %{--</div>--}%
            %{--</th>--}%

            <th style="border-top: none;padding: 5px;">
                <div id="filter_insentifLemburMek" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_insentifLemburMek" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_gajiAdm" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_gajiAdm" class="search_init" />
                </div>
            </th>

            %{--<th style="border-top: none;padding: 5px;">--}%
                %{--<div id="filter_gajiAdmLainLain" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
                    %{--<input type="text" name="search_gajiAdmLainLain" class="search_init" />--}%
                %{--</div>--}%
            %{--</th>--}%


            <th style="border-top: none;padding: 5px;">
                <div id="filter_insentifAdm" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_insentifAdm" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_biayaParts" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_biayaParts" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_biayaBahan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_biayaBahan" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_biayaPekLuar" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_biayaPekLuar" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_biayaSpooring" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_biayaSpooring" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_biayaPerjalanan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_biayaPerjalanan" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_biayaUmum" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_biayaUmum" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_biayaKomisi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_biayaKomisi" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_claimRepair" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_claimRepair" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_biayaTakTerduga" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_biayaTakTerduga" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_biayaPenyusutan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_biayaPenyusutan" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_biayaSewaGedung" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_biayaSewaGedung" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_jumlahStall" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_jumlahStall" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_mekanik" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_mekanik" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_repairUnit" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_repairUnit" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_repairOrder" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_repairOrder" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_paymentRate" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_paymentRate" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_creditPaymentRate" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_creditPaymentRate" class="search_init" />
                </div>
            </th>



		</tr>
	</thead>
</table>

<g:javascript>
var masterEvaluasiTargetTable;
var reloadMasterEvaluasiTargetTable;
$(function(){
	
	reloadMasterEvaluasiTargetTable = function() {
		masterEvaluasiTargetTable.fnDraw();
	}

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	masterEvaluasiTargetTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	masterEvaluasiTargetTable = $('#masterEvaluasiTarget_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [


{
	"sName": "companyDealer",
	"mDataProp": "companyDealer",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "monthYear",
	"mDataProp": "monthYear",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "serviceType",
	"mDataProp": "serviceType",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "antikarat",
	"mDataProp": "antikarat",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}
,

{
	"sName": "spooring",
	"mDataProp": "spooring",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "jasa",
	"mDataProp": "jasa",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "parts",
	"mDataProp": "parts",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "bahan",
	"mDataProp": "bahan",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "lainLain",
	"mDataProp": "lainLain",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "gajiMekanik",
	"mDataProp": "gajiMekanik",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

//{
//	"sName": "gajiMekanikLainLain",
//	"mDataProp": "gajiMekanikLainLain",
//	"aTargets": [10],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,

{
	"sName": "insentifLemburMek",
	"mDataProp": "insentifLemburMek",
	"aTargets": [11],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "gajiAdm",
	"mDataProp": "gajiAdm",
	"aTargets": [12],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

//{
//	"sName": "gajiAdmLainLain",
//	"mDataProp": "gajiAdmLainLain",
//	"aTargets": [13],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,

{
	"sName": "insentifAdm",
	"mDataProp": "insentifAdm",
	"aTargets": [14],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "biayaParts",
	"mDataProp": "biayaParts",
	"aTargets": [15],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "biayaBahan",
	"mDataProp": "biayaBahan",
	"aTargets": [16],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "biayaPekLuar",
	"mDataProp": "biayaPekLuar",
	"aTargets": [17],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "biayaSpooring",
	"mDataProp": "biayaSpooring",
	"aTargets": [18],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "biayaPerjalanan",
	"mDataProp": "biayaPerjalanan",
	"aTargets": [19],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "biayaUmum",
	"mDataProp": "biayaUmum",
	"aTargets": [20],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "biayaKomisi",
	"mDataProp": "biayaKomisi",
	"aTargets": [21],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "claimRepair",
	"mDataProp": "claimRepair",
	"aTargets": [22],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "biayaTakTerduga",
	"mDataProp": "biayaTakTerduga",
	"aTargets": [23],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "biayaPenyusutan",
	"mDataProp": "biayaPenyusutan",
	"aTargets": [24],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "biayaSewaGedung",
	"mDataProp": "biayaSewaGedung",
	"aTargets": [25],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "jumlahStall",
	"mDataProp": "jumlahStall",
	"aTargets": [26],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "mekanik",
	"mDataProp": "mekanik",
	"aTargets": [27],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "repairUnit",
	"mDataProp": "repairUnit",
	"aTargets": [28],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "repairOrder",
	"mDataProp": "repairOrder",
	"aTargets": [29],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "paymentRate",
	"mDataProp": "paymentRate",
	"aTargets": [30],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "creditPaymentRate",
	"mDataProp": "creditPaymentRate",
	"aTargets": [31],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}



],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var antikarat = $('#filter_antikarat input').val();
						if(antikarat){
							aoData.push(
									{"name": 'sCriteria_antikarat', "value": antikarat}
							);
						}
	
						var bahan = $('#filter_bahan input').val();
						if(bahan){
							aoData.push(
									{"name": 'sCriteria_bahan', "value": bahan}
							);
						}
	
						var biayaBahan = $('#filter_biayaBahan input').val();
						if(biayaBahan){
							aoData.push(
									{"name": 'sCriteria_biayaBahan', "value": biayaBahan}
							);
						}
	
						var biayaKomisi = $('#filter_biayaKomisi input').val();
						if(biayaKomisi){
							aoData.push(
									{"name": 'sCriteria_biayaKomisi', "value": biayaKomisi}
							);
						}
	
						var biayaParts = $('#filter_biayaParts input').val();
						if(biayaParts){
							aoData.push(
									{"name": 'sCriteria_biayaParts', "value": biayaParts}
							);
						}
	
						var biayaPekLuar = $('#filter_biayaPekLuar input').val();
						if(biayaPekLuar){
							aoData.push(
									{"name": 'sCriteria_biayaPekLuar', "value": biayaPekLuar}
							);
						}
	
						var biayaPenyusutan = $('#filter_biayaPenyusutan input').val();
						if(biayaPenyusutan){
							aoData.push(
									{"name": 'sCriteria_biayaPenyusutan', "value": biayaPenyusutan}
							);
						}
	
						var biayaPerjalanan = $('#filter_biayaPerjalanan input').val();
						if(biayaPerjalanan){
							aoData.push(
									{"name": 'sCriteria_biayaPerjalanan', "value": biayaPerjalanan}
							);
						}
	
						var biayaSewaGedung = $('#filter_biayaSewaGedung input').val();
						if(biayaSewaGedung){
							aoData.push(
									{"name": 'sCriteria_biayaSewaGedung', "value": biayaSewaGedung}
							);
						}
	
						var biayaSpooring = $('#filter_biayaSpooring input').val();
						if(biayaSpooring){
							aoData.push(
									{"name": 'sCriteria_biayaSpooring', "value": biayaSpooring}
							);
						}
	
						var biayaTakTerduga = $('#filter_biayaTakTerduga input').val();
						if(biayaTakTerduga){
							aoData.push(
									{"name": 'sCriteria_biayaTakTerduga', "value": biayaTakTerduga}
							);
						}
	
						var biayaUmum = $('#filter_biayaUmum input').val();
						if(biayaUmum){
							aoData.push(
									{"name": 'sCriteria_biayaUmum', "value": biayaUmum}
							);
						}
	
						var claimRepair = $('#filter_claimRepair input').val();
						if(claimRepair){
							aoData.push(
									{"name": 'sCriteria_claimRepair', "value": claimRepair}
							);
						}
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}
	
						var creditPaymentRate = $('#filter_creditPaymentRate input').val();
						if(creditPaymentRate){
							aoData.push(
									{"name": 'sCriteria_creditPaymentRate', "value": creditPaymentRate}
							);
						}
	
						var gajiAdm = $('#filter_gajiAdm input').val();
						if(gajiAdm){
							aoData.push(
									{"name": 'sCriteria_gajiAdm', "value": gajiAdm}
							);
						}
	
						var gajiAdmLainLain = $('#filter_gajiAdmLainLain input').val();
						if(gajiAdmLainLain){
							aoData.push(
									{"name": 'sCriteria_gajiAdmLainLain', "value": gajiAdmLainLain}
							);
						}
	
						var gajiMekanik = $('#filter_gajiMekanik input').val();
						if(gajiMekanik){
							aoData.push(
									{"name": 'sCriteria_gajiMekanik', "value": gajiMekanik}
							);
						}
	
						var gajiMekanikLainLain = $('#filter_gajiMekanikLainLain input').val();
						if(gajiMekanikLainLain){
							aoData.push(
									{"name": 'sCriteria_gajiMekanikLainLain', "value": gajiMekanikLainLain}
							);
						}
	
						var insentifAdm = $('#filter_insentifAdm input').val();
						if(insentifAdm){
							aoData.push(
									{"name": 'sCriteria_insentifAdm', "value": insentifAdm}
							);
						}
	
						var insentifLemburMek = $('#filter_insentifLemburMek input').val();
						if(insentifLemburMek){
							aoData.push(
									{"name": 'sCriteria_insentifLemburMek', "value": insentifLemburMek}
							);
						}
	
						var istarget = $('#filter_istarget input').val();
						if(istarget){
							aoData.push(
									{"name": 'sCriteria_istarget', "value": istarget}
							);
						}
	
						var jasa = $('#filter_jasa input').val();
						if(jasa){
							aoData.push(
									{"name": 'sCriteria_jasa', "value": jasa}
							);
						}
	
						var jumlahStall = $('#filter_jumlahStall input').val();
						if(jumlahStall){
							aoData.push(
									{"name": 'sCriteria_jumlahStall', "value": jumlahStall}
							);
						}
	
						var lainLain = $('#filter_lainLain input').val();
						if(lainLain){
							aoData.push(
									{"name": 'sCriteria_lainLain', "value": lainLain}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}
	
						var mekanik = $('#filter_mekanik input').val();
						if(mekanik){
							aoData.push(
									{"name": 'sCriteria_mekanik', "value": mekanik}
							);
						}
	
						var monthYear = $('#filter_monthYear input').val();
						if(monthYear){
							aoData.push(
									{"name": 'sCriteria_monthYear', "value": monthYear}
							);
						}
	
						var parts = $('#filter_parts input').val();
						if(parts){
							aoData.push(
									{"name": 'sCriteria_parts', "value": parts}
							);
						}
	
						var paymentRate = $('#filter_paymentRate input').val();
						if(paymentRate){
							aoData.push(
									{"name": 'sCriteria_paymentRate', "value": paymentRate}
							);
						}
	
						var repairOrder = $('#filter_repairOrder input').val();
						if(repairOrder){
							aoData.push(
									{"name": 'sCriteria_repairOrder', "value": repairOrder}
							);
						}
	
						var repairUnit = $('#filter_repairUnit input').val();
						if(repairUnit){
							aoData.push(
									{"name": 'sCriteria_repairUnit', "value": repairUnit}
							);
						}
	
						var serviceType = $('#filter_serviceType input').val();
						if(serviceType){
							aoData.push(
									{"name": 'sCriteria_serviceType', "value": serviceType}
							);
						}
	
						var spooring = $('#filter_spooring input').val();
						if(spooring){
							aoData.push(
									{"name": 'sCriteria_spooring', "value": spooring}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
