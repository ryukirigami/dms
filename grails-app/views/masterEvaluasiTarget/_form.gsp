<%@ page import="com.kombos.finance.MasterEvaluasiTarget" %>


<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'companyDealer', 'error')} ">
    <label class="control-label" for="companyDealer">
        <g:message code="masterEvaluasiTarget.companyDealer.label" default="Company Dealer" />

    </label>
    <div class="controls">
        <g:if test="${masterEvaluasiTargetInstance?.companyDealer?.id}">
            <g:select id="companyDealer" name="companyDealer.id" from="${com.kombos.administrasi.CompanyDealer.list()}" optionKey="id" required="" value="${masterEvaluasiTargetInstance?.companyDealer?.id}" class="many-to-one"/>
        </g:if><g:else>
            <g:select name="companyDealer.id" id="companyDealer" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
        </g:else>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'monthYear', 'error')} ">
    <label class="control-label" for="monthYear">
        <g:message code="masterEvaluasiTarget.monthYear.label" default="Periode" />

    </label>
    <div class="controls">
        %{--<g:textField name="monthYear" value="${tahunPeriode}" />--}%
        <select name="bulanPeriode" id="bulanPeriode" value="${bulanPeriode}" >
            <option value="01" ${bulanPeriode=="01" ? "selected" : ""}>Januari</option>
            <option value="02" ${bulanPeriode=="02" ? "selected" : ""}>Februari</option>
            <option value="03" ${bulanPeriode=="03" ? "selected" : ""}>Maret</option>
            <option value="04" ${bulanPeriode=="04" ? "selected" : ""}>April</option>
            <option value="05" ${bulanPeriode=="05" ? "selected" : ""}>Mei</option>
            <option value="06" ${bulanPeriode=="06" ? "selected" : ""}>Juni</option>
            <option value="07" ${bulanPeriode=="07" ? "selected" : ""}>Juli</option>
            <option value="08" ${bulanPeriode=="08" ? "selected" : ""}>Agustus</option>
            <option value="09" ${bulanPeriode=="09" ? "selected" : ""}>September</option>
            <option value="10" ${bulanPeriode=="10" ? "selected" : ""}>Oktober</option>
            <option value="11" ${bulanPeriode=="11" ? "selected" : ""}>November</option>
            <option value="12" ${bulanPeriode=="12" ? "selected" : ""}>Desember</option>
        </select> -
    <g:textField name="tahunPeriode" id="tahunPeriode" value="${tahunPeriode}" maxlength="4" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'serviceType', 'error')} ">
    <label class="control-label" for="serviceType">
        <g:message code="masterEvaluasiTarget.serviceType.label" default="Service Type" />

    </label>
    <div class="controls">
        %{--<g:textField name="serviceType" value="${masterEvaluasiTargetInstance?.serviceType}" />--}%
            <g:select id="serviceType" name="serviceType" from="${['GR', 'BP']}" required="" value="${masterEvaluasiTargetInstance?.serviceType}" class="many-to-one"/>
    </div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'antikarat', 'error')} ">
    <label class="control-label" for="antikarat">
        <g:message code="masterEvaluasiTarget.antikarat.label" default="Antikarat" />

    </label>
    <div class="controls">
        <g:field type="number" name="antikarat" value="${masterEvaluasiTargetInstance.antikarat}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'spooring', 'error')} ">
    <label class="control-label" for="spooring">
        <g:message code="masterEvaluasiTarget.spooring.label" default="Spooring" />

    </label>
    <div class="controls">
        <g:field type="number" name="spooring" value="${masterEvaluasiTargetInstance.spooring}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'jasa', 'error')} ">
    <label class="control-label" for="jasa">
        <g:message code="masterEvaluasiTarget.jasa.label" default="Jasa" />

    </label>
    <div class="controls">
        <g:field type="number" name="jasa" value="${masterEvaluasiTargetInstance.jasa}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'parts', 'error')} ">
    <label class="control-label" for="parts">
        <g:message code="masterEvaluasiTarget.parts.label" default="Parts" />

    </label>
    <div class="controls">
        <g:field type="number" name="parts" value="${masterEvaluasiTargetInstance.parts}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'bahan', 'error')} ">
    <label class="control-label" for="bahan">
        <g:message code="masterEvaluasiTarget.bahan.label" default="Bahan" />

    </label>
    <div class="controls">
        <g:field type="number" name="bahan" value="${masterEvaluasiTargetInstance.bahan}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'lainLain', 'error')} ">
    <label class="control-label" for="lainLain">
        <g:message code="masterEvaluasiTarget.lainLain.label" default="Lain Lain" />

    </label>
    <div class="controls">
        <g:field type="number" name="lainLain" value="${masterEvaluasiTargetInstance.lainLain}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'gajiMekanik', 'error')} ">
    <label class="control-label" for="gajiMekanik">
        <g:message code="masterEvaluasiTarget.gajiMekanik.label" default="Gaji Mekanik" />

    </label>
    <div class="controls">
        <g:field type="number" name="gajiMekanik" value="${masterEvaluasiTargetInstance.gajiMekanik}" />
    </div>
</div>

%{--<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'gajiMekanikLainLain', 'error')} ">--}%
    %{--<label class="control-label" for="gajiMekanikLainLain">--}%
        %{--<g:message code="masterEvaluasiTarget.gajiMekanikLainLain.label" default="Gaji Mekanik Lain Lain" />--}%

    %{--</label>--}%
    %{--<div class="controls">--}%
        %{--<g:field type="number" name="gajiMekanikLainLain" value="${masterEvaluasiTargetInstance.gajiMekanikLainLain}" />--}%
    %{--</div>--}%
%{--</div>--}%

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'insentifLemburMek', 'error')} ">
    <label class="control-label" for="insentifLemburMek">
        <g:message code="masterEvaluasiTarget.insentifLemburMek.label" default="Insentif Lembur Mek" />

    </label>
    <div class="controls">
        <g:field type="number" name="insentifLemburMek" value="${masterEvaluasiTargetInstance.insentifLemburMek}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'gajiAdm', 'error')} ">
    <label class="control-label" for="gajiAdm">
        <g:message code="masterEvaluasiTarget.gajiAdm.label" default="Gaji Adm" />

    </label>
    <div class="controls">
        <g:field type="number" name="gajiAdm" value="${masterEvaluasiTargetInstance.gajiAdm}" />
    </div>
</div>

%{--<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'gajiAdmLainLain', 'error')} ">--}%
    %{--<label class="control-label" for="gajiAdmLainLain">--}%
        %{--<g:message code="masterEvaluasiTarget.gajiAdmLainLain.label" default="Gaji Adm Lain Lain" />--}%

    %{--</label>--}%
    %{--<div class="controls">--}%
        %{--<g:field type="number" name="gajiAdmLainLain" value="${masterEvaluasiTargetInstance.gajiAdmLainLain}" />--}%
    %{--</div>--}%
%{--</div>--}%

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'insentifAdm', 'error')} ">
    <label class="control-label" for="insentifAdm">
        <g:message code="masterEvaluasiTarget.insentifAdm.label" default="Insentif Adm" />

    </label>
    <div class="controls">
        <g:field type="number" name="insentifAdm" value="${masterEvaluasiTargetInstance.insentifAdm}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'biayaParts', 'error')} ">
    <label class="control-label" for="biayaParts">
        <g:message code="masterEvaluasiTarget.biayaParts.label" default="Biaya Parts" />

    </label>
    <div class="controls">
        <g:field type="number" name="biayaParts" value="${masterEvaluasiTargetInstance.biayaParts}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'biayaBahan', 'error')} ">
    <label class="control-label" for="biayaBahan">
        <g:message code="masterEvaluasiTarget.biayaBahan.label" default="Biaya Bahan" />

    </label>
    <div class="controls">
        <g:field type="number" name="biayaBahan" value="${masterEvaluasiTargetInstance.biayaBahan}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'biayaPekLuar', 'error')} ">
    <label class="control-label" for="biayaPekLuar">
        <g:message code="masterEvaluasiTarget.biayaPekLuar.label" default="Biaya Pek Luar" />

    </label>
    <div class="controls">
        <g:field type="number" name="biayaPekLuar" value="${masterEvaluasiTargetInstance.biayaPekLuar}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'biayaSpooring', 'error')} ">
    <label class="control-label" for="biayaSpooring">
        <g:message code="masterEvaluasiTarget.biayaSpooring.label" default="Biaya Spooring" />

    </label>
    <div class="controls">
        <g:field type="number" name="biayaSpooring" value="${masterEvaluasiTargetInstance.biayaSpooring}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'biayaPerjalanan', 'error')} ">
    <label class="control-label" for="biayaPerjalanan">
        <g:message code="masterEvaluasiTarget.biayaPerjalanan.label" default="Biaya Perjalanan" />

    </label>
    <div class="controls">
        <g:field type="number" name="biayaPerjalanan" value="${masterEvaluasiTargetInstance.biayaPerjalanan}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'biayaUmum', 'error')} ">
    <label class="control-label" for="biayaUmum">
        <g:message code="masterEvaluasiTarget.biayaUmum.label" default="Biaya Umum" />

    </label>
    <div class="controls">
        <g:field type="number" name="biayaUmum" value="${masterEvaluasiTargetInstance.biayaUmum}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'biayaKomisi', 'error')} ">
    <label class="control-label" for="biayaKomisi">
        <g:message code="masterEvaluasiTarget.biayaKomisi.label" default="Biaya Komisi" />

    </label>
    <div class="controls">
        <g:field type="number" name="biayaKomisi" value="${masterEvaluasiTargetInstance.biayaKomisi}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'claimRepair', 'error')} ">
    <label class="control-label" for="claimRepair">
        <g:message code="masterEvaluasiTarget.claimRepair.label" default="Claim Repair" />

    </label>
    <div class="controls">
        <g:field type="number" name="claimRepair" value="${masterEvaluasiTargetInstance.claimRepair}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'biayaTakTerduga', 'error')} ">
    <label class="control-label" for="biayaTakTerduga">
        <g:message code="masterEvaluasiTarget.biayaTakTerduga.label" default="Biaya Tak Terduga" />

    </label>
    <div class="controls">
        <g:field type="number" name="biayaTakTerduga" value="${masterEvaluasiTargetInstance.biayaTakTerduga}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'biayaPenyusutan', 'error')} ">
    <label class="control-label" for="biayaPenyusutan">
        <g:message code="masterEvaluasiTarget.biayaPenyusutan.label" default="Biaya Penyusutan" />

    </label>
    <div class="controls">
        <g:field type="number" name="biayaPenyusutan" value="${masterEvaluasiTargetInstance.biayaPenyusutan}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'biayaSewaGedung', 'error')} ">
    <label class="control-label" for="biayaSewaGedung">
        <g:message code="masterEvaluasiTarget.biayaSewaGedung.label" default="Biaya Sewa Gedung" />

    </label>
    <div class="controls">
        <g:field type="number" name="biayaSewaGedung" value="${masterEvaluasiTargetInstance.biayaSewaGedung}" />
    </div>
</div>



<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'jumlahStall', 'error')} ">
    <label class="control-label" for="jumlahStall">
        <g:message code="masterEvaluasiTarget.jumlahStall.label" default="Jumlah Stall" />

    </label>
    <div class="controls">
        <g:field type="number" name="jumlahStall" value="${masterEvaluasiTargetInstance.jumlahStall}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'mekanik', 'error')} ">
    <label class="control-label" for="mekanik">
        <g:message code="masterEvaluasiTarget.mekanik.label" default="Mekanik" />

    </label>
    <div class="controls">
        <g:field type="number" name="mekanik" value="${masterEvaluasiTargetInstance.mekanik}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'repairUnit', 'error')} ">
    <label class="control-label" for="repairUnit">
        <g:message code="masterEvaluasiTarget.repairUnit.label" default="Repair Unit" />

    </label>
    <div class="controls">
        <g:field type="number" name="repairUnit" value="${masterEvaluasiTargetInstance.repairUnit}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'repairOrder', 'error')} ">
    <label class="control-label" for="repairOrder">
        <g:message code="masterEvaluasiTarget.repairOrder.label" default="Repair Order" />

    </label>
    <div class="controls">
        <g:field type="number" name="repairOrder" value="${masterEvaluasiTargetInstance.repairOrder}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'paymentRate', 'error')} ">
    <label class="control-label" for="paymentRate">
        <g:message code="masterEvaluasiTarget.paymentRate.label" default="Payment Rate" />

    </label>
    <div class="controls">
        <g:field type="number" name="paymentRate" value="${masterEvaluasiTargetInstance.paymentRate}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterEvaluasiTargetInstance, field: 'creditPaymentRate', 'error')} ">
	<label class="control-label" for="creditPaymentRate">
		<g:message code="masterEvaluasiTarget.creditPaymentRate.label" default="Credit Payment Rate" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="creditPaymentRate" value="${masterEvaluasiTargetInstance.creditPaymentRate}" />
	</div>
</div>

