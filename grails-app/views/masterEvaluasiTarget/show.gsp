

<%@ page import="com.kombos.finance.MasterEvaluasiTarget" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'masterEvaluasiTarget.label', default: 'MasterEvaluasiTarget')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteMasterEvaluasiTarget;

$(function(){ 
	deleteMasterEvaluasiTarget=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/masterEvaluasiTarget/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadMasterEvaluasiTargetTable();
   				expandTableLayout('masterEvaluasiTarget');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-masterEvaluasiTarget" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="masterEvaluasiTarget"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${masterEvaluasiTargetInstance?.antikarat}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="antikarat-label" class="property-label"><g:message
					code="masterEvaluasiTarget.antikarat.label" default="Antikarat" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="antikarat-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="antikarat"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter antikarat" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="antikarat"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.bahan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="bahan-label" class="property-label"><g:message
					code="masterEvaluasiTarget.bahan.label" default="Bahan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="bahan-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="bahan"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter bahan" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="bahan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.biayaBahan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="biayaBahan-label" class="property-label"><g:message
					code="masterEvaluasiTarget.biayaBahan.label" default="Biaya Bahan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="biayaBahan-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="biayaBahan"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter biayaBahan" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="biayaBahan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.biayaKomisi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="biayaKomisi-label" class="property-label"><g:message
					code="masterEvaluasiTarget.biayaKomisi.label" default="Biaya Komisi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="biayaKomisi-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="biayaKomisi"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter biayaKomisi" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="biayaKomisi"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.biayaParts}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="biayaParts-label" class="property-label"><g:message
					code="masterEvaluasiTarget.biayaParts.label" default="Biaya Parts" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="biayaParts-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="biayaParts"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter biayaParts" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="biayaParts"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.biayaPekLuar}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="biayaPekLuar-label" class="property-label"><g:message
					code="masterEvaluasiTarget.biayaPekLuar.label" default="Biaya Pek Luar" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="biayaPekLuar-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="biayaPekLuar"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter biayaPekLuar" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="biayaPekLuar"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.biayaPenyusutan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="biayaPenyusutan-label" class="property-label"><g:message
					code="masterEvaluasiTarget.biayaPenyusutan.label" default="Biaya Penyusutan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="biayaPenyusutan-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="biayaPenyusutan"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter biayaPenyusutan" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="biayaPenyusutan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.biayaPerjalanan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="biayaPerjalanan-label" class="property-label"><g:message
					code="masterEvaluasiTarget.biayaPerjalanan.label" default="Biaya Perjalanan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="biayaPerjalanan-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="biayaPerjalanan"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter biayaPerjalanan" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="biayaPerjalanan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.biayaSewaGedung}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="biayaSewaGedung-label" class="property-label"><g:message
					code="masterEvaluasiTarget.biayaSewaGedung.label" default="Biaya Sewa Gedung" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="biayaSewaGedung-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="biayaSewaGedung"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter biayaSewaGedung" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="biayaSewaGedung"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.biayaSpooring}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="biayaSpooring-label" class="property-label"><g:message
					code="masterEvaluasiTarget.biayaSpooring.label" default="Biaya Spooring" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="biayaSpooring-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="biayaSpooring"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter biayaSpooring" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="biayaSpooring"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.biayaTakTerduga}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="biayaTakTerduga-label" class="property-label"><g:message
					code="masterEvaluasiTarget.biayaTakTerduga.label" default="Biaya Tak Terduga" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="biayaTakTerduga-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="biayaTakTerduga"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter biayaTakTerduga" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="biayaTakTerduga"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.biayaUmum}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="biayaUmum-label" class="property-label"><g:message
					code="masterEvaluasiTarget.biayaUmum.label" default="Biaya Umum" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="biayaUmum-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="biayaUmum"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter biayaUmum" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="biayaUmum"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.claimRepair}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="claimRepair-label" class="property-label"><g:message
					code="masterEvaluasiTarget.claimRepair.label" default="Claim Repair" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="claimRepair-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="claimRepair"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter claimRepair" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="claimRepair"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.companyDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="companyDealer-label" class="property-label"><g:message
					code="masterEvaluasiTarget.companyDealer.label" default="Company Dealer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="companyDealer-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="companyDealer"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter companyDealer" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:link controller="companyDealer" action="show" id="${masterEvaluasiTargetInstance?.companyDealer?.id}">${masterEvaluasiTargetInstance?.companyDealer?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="masterEvaluasiTarget.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="createdBy"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.creditPaymentRate}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="creditPaymentRate-label" class="property-label"><g:message
					code="masterEvaluasiTarget.creditPaymentRate.label" default="Credit Payment Rate" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="creditPaymentRate-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="creditPaymentRate"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter creditPaymentRate" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="creditPaymentRate"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="masterEvaluasiTarget.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="dateCreated"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:formatDate date="${masterEvaluasiTargetInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.gajiAdm}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="gajiAdm-label" class="property-label"><g:message
					code="masterEvaluasiTarget.gajiAdm.label" default="Gaji Adm" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="gajiAdm-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="gajiAdm"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter gajiAdm" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="gajiAdm"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.gajiAdmLainLain}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="gajiAdmLainLain-label" class="property-label"><g:message
					code="masterEvaluasiTarget.gajiAdmLainLain.label" default="Gaji Adm Lain Lain" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="gajiAdmLainLain-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="gajiAdmLainLain"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter gajiAdmLainLain" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="gajiAdmLainLain"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.gajiMekanik}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="gajiMekanik-label" class="property-label"><g:message
					code="masterEvaluasiTarget.gajiMekanik.label" default="Gaji Mekanik" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="gajiMekanik-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="gajiMekanik"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter gajiMekanik" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="gajiMekanik"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.gajiMekanikLainLain}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="gajiMekanikLainLain-label" class="property-label"><g:message
					code="masterEvaluasiTarget.gajiMekanikLainLain.label" default="Gaji Mekanik Lain Lain" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="gajiMekanikLainLain-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="gajiMekanikLainLain"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter gajiMekanikLainLain" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="gajiMekanikLainLain"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.insentifAdm}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="insentifAdm-label" class="property-label"><g:message
					code="masterEvaluasiTarget.insentifAdm.label" default="Insentif Adm" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="insentifAdm-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="insentifAdm"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter insentifAdm" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="insentifAdm"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.insentifLemburMek}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="insentifLemburMek-label" class="property-label"><g:message
					code="masterEvaluasiTarget.insentifLemburMek.label" default="Insentif Lembur Mek" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="insentifLemburMek-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="insentifLemburMek"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter insentifLemburMek" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="insentifLemburMek"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.istarget}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="istarget-label" class="property-label"><g:message
					code="masterEvaluasiTarget.istarget.label" default="Istarget" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="istarget-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="istarget"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter istarget" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="istarget"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.jasa}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="jasa-label" class="property-label"><g:message
					code="masterEvaluasiTarget.jasa.label" default="Jasa" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="jasa-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="jasa"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter jasa" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="jasa"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.jumlahStall}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="jumlahStall-label" class="property-label"><g:message
					code="masterEvaluasiTarget.jumlahStall.label" default="Jumlah Stall" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="jumlahStall-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="jumlahStall"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter jumlahStall" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="jumlahStall"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.lainLain}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lainLain-label" class="property-label"><g:message
					code="masterEvaluasiTarget.lainLain.label" default="Lain Lain" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lainLain-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="lainLain"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter lainLain" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="lainLain"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="masterEvaluasiTarget.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="lastUpdProcess"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="masterEvaluasiTarget.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="lastUpdated"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:formatDate date="${masterEvaluasiTargetInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.mekanik}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="mekanik-label" class="property-label"><g:message
					code="masterEvaluasiTarget.mekanik.label" default="Mekanik" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="mekanik-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="mekanik"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter mekanik" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="mekanik"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.monthYear}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="monthYear-label" class="property-label"><g:message
					code="masterEvaluasiTarget.monthYear.label" default="Month Year" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="monthYear-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="monthYear"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter monthYear" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="monthYear"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.parts}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="parts-label" class="property-label"><g:message
					code="masterEvaluasiTarget.parts.label" default="Parts" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="parts-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="parts"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter parts" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="parts"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.paymentRate}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="paymentRate-label" class="property-label"><g:message
					code="masterEvaluasiTarget.paymentRate.label" default="Payment Rate" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="paymentRate-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="paymentRate"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter paymentRate" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="paymentRate"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.repairOrder}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="repairOrder-label" class="property-label"><g:message
					code="masterEvaluasiTarget.repairOrder.label" default="Repair Order" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="repairOrder-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="repairOrder"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter repairOrder" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="repairOrder"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.repairUnit}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="repairUnit-label" class="property-label"><g:message
					code="masterEvaluasiTarget.repairUnit.label" default="Repair Unit" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="repairUnit-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="repairUnit"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter repairUnit" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="repairUnit"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.serviceType}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="serviceType-label" class="property-label"><g:message
					code="masterEvaluasiTarget.serviceType.label" default="Service Type" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="serviceType-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="serviceType"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter serviceType" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="serviceType"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.spooring}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="spooring-label" class="property-label"><g:message
					code="masterEvaluasiTarget.spooring.label" default="Spooring" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="spooring-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="spooring"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter spooring" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="spooring"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterEvaluasiTargetInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="masterEvaluasiTarget.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${masterEvaluasiTargetInstance}" field="updatedBy"
								url="${request.contextPath}/MasterEvaluasiTarget/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadMasterEvaluasiTargetTable();" />--}%
							
								<g:fieldValue bean="${masterEvaluasiTargetInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('masterEvaluasiTarget');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${masterEvaluasiTargetInstance?.id}"
					update="[success:'masterEvaluasiTarget-form',failure:'masterEvaluasiTarget-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteMasterEvaluasiTarget('${masterEvaluasiTargetInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
