<%@ page import="com.kombos.administrasi.CompanyDealer"%>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript disappointmentGrition="head">
	$(function(){



	    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    });
    var checkin = $('#search_tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
                checkin.hide();
                $('#search_tanggal2')[0].focus();
            }).data('datepicker');

    var checkout = $('#search_tanggal2').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
                checkout.hide();
    }).data('datepicker');

    previewData = function(){

		var namaReport = $('#namaReport').val();
		var workshop = $('#workshop').val();
		var tanggal  = $('#search_tanggal').val();
		var tanggal2 = $('#search_tanggal2').val();
		var format = $('input[name="formatReport"]:checked').val();

        if(namaReport==null)
        {
        alert("Pilih Salah satu report")
        }
        else
		{window.location = "${request.contextPath}/businessTransaction/previewData?namaReport="+namaReport+"&workshop="+workshop+"&tanggal="+tanggal+"&format="+format+"&tanggal2="+tanggal2;
    }}


    </g:javascript>

</head>
<body>
<div class="navbar box-header no-border">Report – Business Transaction
</div>
<br>
<div class="box">
    <div class="span12" id="appointmentGr-table">
        <div class="row-fluid form-horizontal">
            <div id="kiri" class="span6">


                <div class="control-group">
                    <label class="control-label" for="filter_periode" style="text-align: left;">
                        Format Report
                    </label>
                    <div id="filter_format" class="controls">
                        <g:radioGroup name="formatReport" id="formatReport" values="['x','p']" labels="['Excel','PDF']" value="x" required="" >
                            ${it.radio} ${it.label}
                        </g:radioGroup>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="t951Tanggal"
                           style="text-align: left;"> Workshop </label>
                    <div id="filter_wo" class="controls">
                        <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                            <g:select name="workshop" id="workshop" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                        </g:if>
                        <g:else>
                            <g:select name="workshop" id="workshop" readonly="" disabled="" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                        </g:else>
                        %{--<g:select name="workshop" id="workshop"--}%
                        %{--from="${CompanyDealer.list()}" optionKey="id"--}%
                        %{--optionValue="m011NamaWorkshop" style="width: 44%" />--}%
                    </div>
                </div>


                <div class="control-group">
                    <label class="control-label" for="t951Tanggal"
                           style="text-align: left;"> Nama Report </label>
                    <div id="filter_nama" class="controls">
                        <select name="namaReport" id="namaReport" size="8"
                                style="width: 100%; height: 230px; font-size: 12px;">

                            <option value="01">01. Part Transaction</option>
                            <option value="02">02. Business Transaction</option>

                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="t951Tanggal"
                           style="text-align: left;"> Periode </label>
                    <div id="filter_periode" class="controls">
                        <ba:datePicker id="search_tanggal" name="search_tanggal" precision="day" format="dd-MM-yyyy"  value="${new Date()}" />
                        &nbsp;&nbsp;s.d.&nbsp;&nbsp;
                        <ba:datePicker id="search_tanggal2" name="search_tanggal2" precision="day" format="dd-MM-yyyy"  value="${new Date()+1}"  />
                    </div>



                    </div>

                </div>
            </div>

        </div>
        <g:field type="button" onclick="previewData()"
                 class="btn btn-primary create" name="preview"
                 value="${message(code: 'default.button.upload.label', default: 'Preview')}" />
        <g:field type="button" onclick="window.location.replace('#/home')"
                 class="btn btn-cancel cancel" name="cancel"
                 value="${message(code: 'default.button.upload.label', default: 'Close')}" />
    </div>
</div>
</body>
</html>