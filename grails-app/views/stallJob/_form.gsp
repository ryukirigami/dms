<%@ page import="com.kombos.administrasi.Operation; com.kombos.administrasi.Stall; com.kombos.administrasi.StallJob" %>



<div class="control-group fieldcontain ${hasErrors(bean: stallJobInstance, field: 'stall', 'error')} required">
	<label class="control-label" for="stall">
		<g:message code="stallJob.stall.label" default="Stall" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="stall" name="stall.id" from="${Stall.createCriteria().list {eq("staDel", "0");order("m022NamaStall", "asc")}}" optionKey="id" required="" value="${stallJobInstance?.stall?.id}" class="many-to-one"/>
	</div>
</div>

<g:javascript>
    document.getElementById('stall').focus();
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: stallJobInstance, field: 'm024Tanggal', 'error')} required">
	<label class="control-label" for="m024Tanggal">
		<g:message code="stallJob.m024Tanggal.label" default="Tanggal Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:datePicker name="m024Tanggal" precision="day"  value="${stallJobInstance?.m024Tanggal}"  />--}%
    <ba:datePicker name="m024Tanggal" precision="day" value="${stallJobInstance?.m024Tanggal}" format="dd/mm/yyyy" required="true"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stallJobInstance, field: 'operation', 'error')} required">
	<label class="control-label" for="operation">
		<g:message code="stallJob.operation.label" default="Operation" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="operation" name="operation.id" from="${Operation.createCriteria().list {eq("staDel", "0");order("m053NamaOperation", "asc")}}" optionKey="id" required="" value="${stallJobInstance?.operation?.id}" class="many-to-one"/>
	</div>
</div>
%{--
<div class="control-group fieldcontain ${hasErrors(bean: stallJobInstance, field: 'staDel', 'error')} required">
	<label class="control-label" for="staDel">
		<g:message code="stallJob.staDel.label" default="Sta Del" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="staDel" required="" value="${stallJobInstance?.staDel}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stallJobInstance, field: 'createdBy', 'error')} ">
	<label class="control-label" for="createdBy">
		<g:message code="stallJob.createdBy.label" default="Created By" />
		
	</label>
	<div class="controls">
	<g:textField name="createdBy" value="${stallJobInstance?.createdBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stallJobInstance, field: 'updatedBy', 'error')} ">
	<label class="control-label" for="updatedBy">
		<g:message code="stallJob.updatedBy.label" default="Updated By" />
		
	</label>
	<div class="controls">
	<g:textField name="updatedBy" value="${stallJobInstance?.updatedBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: stallJobInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="stallJob.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${stallJobInstance?.lastUpdProcess}"/>
	</div>
</div>
--}%