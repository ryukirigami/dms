

<%@ page import="com.kombos.administrasi.StallJob" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'stallJob.label', default: 'Stall Job')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteStallJob;

$(function(){ 
	deleteStallJob=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/stallJob/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadStallJobTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-stallJob" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="stallJob"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${stallJobInstance?.stall}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="stall-label" class="property-label"><g:message
					code="stallJob.stall.label" default="Stall" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="stall-label">
						%{--<ba:editableValue
								bean="${stallJobInstance}" field="stall"
								url="${request.contextPath}/StallJob/updatefield" type="text"
								title="Enter stall" onsuccess="reloadStallJobTable();" />--}%
							
								${stallJobInstance?.stall?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stallJobInstance?.m024Tanggal}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m024Tanggal-label" class="property-label"><g:message
					code="stallJob.m024Tanggal.label" default="Tanggal Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m024Tanggal-label">
						%{--<ba:editableValue
								bean="${stallJobInstance}" field="m024Tanggal"
								url="${request.contextPath}/StallJob/updatefield" type="text"
								title="Enter m024Tanggal" onsuccess="reloadStallJobTable();" />--}%
							
								<g:formatDate date="${stallJobInstance?.m024Tanggal}" type="date" style="LONG" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stallJobInstance?.operation}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="operation-label" class="property-label"><g:message
					code="stallJob.operation.label" default="Operation" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="operation-label">
						%{--<ba:editableValue
								bean="${stallJobInstance}" field="operation"
								url="${request.contextPath}/StallJob/updatefield" type="text"
								title="Enter operation" onsuccess="reloadStallJobTable();" />--}%
							
								%{--<g:link controller="operation" action="show" id="${stallJobInstance?.operation?.id}">${stallJobInstance?.operation?.encodeAsHTML()}</g:link>--}%
                        ${stallJobInstance?.operation?.m053JobsId+"."+stallJobInstance?.operation?.serial?.m052NamaSerial+" "+stallJobInstance?.operation?.m053NamaOperation}
						</span></td>
					
				</tr>
				</g:if>
            %{--
                <g:if test="${stallJobInstance?.staDel}">
                <tr>
                <td class="span2" style="text-align: right;"><span
                    id="staDel-label" class="property-label"><g:message
                    code="stallJob.staDel.label" default="Sta Del" />:</span></td>

                    <td class="span3"><span class="property-value"
                        aria-labelledby="staDel-label">
                        %{--<ba:editableValue
                                bean="${stallJobInstance}" field="staDel"
                                url="${request.contextPath}/StallJob/updatefield" type="text"
                                title="Enter staDel" onsuccess="reloadStallJobTable();" />
							
								<g:fieldValue bean="${stallJobInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
            --}%
				<g:if test="${stallJobInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="stallJob.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${stallJobInstance}" field="createdBy"
								url="${request.contextPath}/StallJob/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadStallJobTable();" />--}%
							
								<g:fieldValue bean="${stallJobInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stallJobInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="stallJob.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${stallJobInstance}" field="updatedBy"
								url="${request.contextPath}/StallJob/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadStallJobTable();" />--}%
							
								<g:fieldValue bean="${stallJobInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stallJobInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="stallJob.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${stallJobInstance}" field="lastUpdProcess"
								url="${request.contextPath}/StallJob/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadStallJobTable();" />--}%
							
								<g:fieldValue bean="${stallJobInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stallJobInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="stallJob.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${stallJobInstance}" field="dateCreated"
								url="${request.contextPath}/StallJob/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadStallJobTable();" />--}%
							
								<g:formatDate date="${stallJobInstance?.dateCreated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${stallJobInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="stallJob.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${stallJobInstance}" field="lastUpdated"
								url="${request.contextPath}/StallJob/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadStallJobTable();" />--}%
							
								<g:formatDate date="${stallJobInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${stallJobInstance?.id}"
					update="[success:'stallJob-form',failure:'stallJob-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteStallJob('${stallJobInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
