
<%@ page import="com.kombos.administrasi.StallJob" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="stallJob_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stallJob.stall.label" default="Stall" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stallJob.m024Tanggal.label" default="Tanggal Berlaku" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stallJob.operation.label" default="Operation" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stallJob.staDel.label" default="Sta Del" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stallJob.createdBy.label" default="Created By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stallJob.updatedBy.label" default="Updated By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="stallJob.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_stall" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_stall" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m024Tanggal" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m024Tanggal" value="date.struct">
					<input type="hidden" name="search_m024Tanggal_day" id="search_m024Tanggal_day" value="">
					<input type="hidden" name="search_m024Tanggal_month" id="search_m024Tanggal_month" value="">
					<input type="hidden" name="search_m024Tanggal_year" id="search_m024Tanggal_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m024Tanggal_dp" value="" id="search_m024Tanggal" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_operation" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_operation" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_staDel" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_createdBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_updatedBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var stallJobTable;
var reloadStallJobTable;
$(function(){
	
	reloadStallJobTable = function() {
		stallJobTable.fnDraw();
	}

	var recordsStallJobPerPage = [];
    var anStallJobSelected;
    var jmlRecStallJobPerPage=0;
    var id;
    
	$('#search_m024Tanggal').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m024Tanggal_day').val(newDate.getDate());
			$('#search_m024Tanggal_month').val(newDate.getMonth()+1);
			$('#search_m024Tanggal_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			stallJobTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	stallJobTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	stallJobTable = $('#stallJob_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsStallJob = $("#stallJob_datatables tbody .row-select");
            var jmlStallJobCek = 0;
            var nRow;
            var idRec;
            rsStallJob.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsStallJobPerPage[idRec]=="1"){
                    jmlStallJobCek = jmlStallJobCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsStallJobPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecStallJobPerPage = rsStallJob.length;
            if(jmlStallJobCek==jmlRecStallJobPerPage && jmlRecStallJobPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "stall",
	"mDataProp": "stall",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m024Tanggal",
	"mDataProp": "m024Tanggal",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "operation",
	"mDataProp": "operation",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "staDel",
	"mDataProp": "staDel",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "createdBy",
	"mDataProp": "createdBy",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "updatedBy",
	"mDataProp": "updatedBy",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var stall = $('#filter_stall input').val();
						if(stall){
							aoData.push(
									{"name": 'sCriteria_stall', "value": stall}
							);
						}

						var m024Tanggal = $('#search_m024Tanggal').val();
						var m024TanggalDay = $('#search_m024Tanggal_day').val();
						var m024TanggalMonth = $('#search_m024Tanggal_month').val();
						var m024TanggalYear = $('#search_m024Tanggal_year').val();
						
						if(m024Tanggal){
							aoData.push(
									{"name": 'sCriteria_m024Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_m024Tanggal_dp', "value": m024Tanggal},
									{"name": 'sCriteria_m024Tanggal_day', "value": m024TanggalDay},
									{"name": 'sCriteria_m024Tanggal_month', "value": m024TanggalMonth},
									{"name": 'sCriteria_m024Tanggal_year', "value": m024TanggalYear}
							);
						}
	
						var operation = $('#filter_operation input').val();
						if(operation){
							aoData.push(
									{"name": 'sCriteria_operation', "value": operation}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
	$('.select-all').click(function(e) {

        $("#stallJob_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsStallJobPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsStallJobPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#stallJob_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsStallJobPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anStallJobSelected = stallJobTable.$('tr.row_selected');
            if(jmlRecStallJobPerPage == anStallJobSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsStallJobPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
