
<%@ page import="com.kombos.parts.SalesRetur" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'pendingMasuk.label', default: 'Jurnal Pending Masuk')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			$(function(){
                $("#tahun").autoNumeric({
                    vMin:'0',
                    vMax:'9999',
                    mDec: null,
                    aSep:''
                });

                doSave = function(){
                    var form = $('#pendingMasuk-table').find('form');
                    var checkGoods =[];
                    $("#pendingMasuk-table tbody .row-select").each(function() {
                            var id = $(this).val();
                            checkGoods.push(id);
                    });
                    $("#ids").val(JSON.stringify(checkGoods));
                    $.ajax({
                        url:'${request.contextPath}/jurnalPendingMasuk/save',
                        type: "POST", // Always use POST when deleting data
                        data : form.serialize(),
                        success : function(data,textStatus){
                           toastr.success("<div>"+data.berhasil+" of "+data.totalSave+" : Success Save <div>");
                        }
                     });
                }
            });
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">

			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="pendingMasuk-table">
            <form id="form-pending" class="form-horizontal">
                <input type="hidden" name="ids" id="ids" value="">
                <div class="control-group">
                    <label class="control-label" for="t951Tanggal" style="text-align: left;">
                        Periode
                    </label>
                    <div id="filter_nomor" class="controls">
                        <select name="src_bulan" id="bulan">
                            <option value="01">Januari</option>
                            <option value="02">Februari</option>
                            <option value="03">Maret</option>
                            <option value="04">April</option>
                            <option value="05">Mei</option>
                            <option value="06">Juni</option>
                            <option value="07">Juli</option>
                            <option value="08">Agustus</option>
                            <option value="09">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>
                        <input type="text" maxlength="4" name="src_tahun" id="tahun" />
                    </div>
                </div>
            </form>
            <table>
                <tr>
                    <td>
                        <div class="controls" style="right: 0">
                            <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-primary" name="view" id="view" >Search</button>
                            <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-cancel" name="clear" id="clear" >Clear</button>
                        </div>
                    </td>
                </tr>
            </table>
			<g:render template="dataTables" />
            <fieldset class="buttons controls" style="padding-top: 10px;">
                <button class="btn btn-primary" name="save" onclick="doSave();" >Save</button>
            </fieldset>
		</div>
	</div>
</body>
</html>
