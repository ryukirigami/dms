
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>


<table id="pendingMasuk_datatables_${idTable}" cellpadding="0" cellspacing="0" border="0"
       class="display table table-striped table-bordered table-hover" style="table-layout: fixed; ">
    <thead>
    <tr>
        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="pendingMasuk.sorNumber.label" default="No." /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="pendingMasuk.sorNumber.label" default="Nomor Wo" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="pendingMasuk.sorNumber.label" default="Nomor Polisi" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="pendingMasuk.deliveryDate.label" default="Tanggal SO" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="pendingMasuk.deliveryDate.label" default="Kode Parts" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="pendingMasuk.deliveryDate.label" default="Nama Parts" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="pendingMasuk.deliveryDate.label" default="Qty" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="pendingMasuk.deliveryDate.label" default="Harga Beli Satuan" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="pendingMasuk.deliveryDate.label" default="Harga Total" /></div>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
var pendingMasukTable;
var reloadSalesReturTable;
$(function(){

	reloadSalesReturTable = function() {
		pendingMasukTable.fnDraw();
	}
     $('#view').click(function(e){
        e.stopPropagation();
		pendingMasukTable.fnDraw();
	});
    pendingMasukTable = $('#pendingMasuk_datatables_${idTable}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "id",
	"mDataProp": "no",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="hidden" class="pull-left row-select" value="'+row['id']+'" id="idPartsRCP">' + data;
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"20px",
	"bVisible": true
},{
	"sName": "id",
	"mDataProp": "noWo",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
}
,

{
	"sName": "id",
	"mDataProp": "noPol",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},

{
	"sName": "id",
	"mDataProp": "tglWo",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},

{
	"sName": "id",
	"mDataProp": "m111ID",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"120px",
	"bVisible": true
},

{
	"sName": "id",
	"mDataProp": "m111Nama",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"180px",
	"bVisible": true
},

{
	"sName": "id",
	"mDataProp": "qty",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"80px",
	"bVisible": true
},

{
	"sName": "id",
	"mDataProp": "harga",
	"aTargets": [6],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        if(data != null)
        return '<span class="pull-right numeric">'+data+'</span>';
        else
        return '<span></span>';
    },
	"bSortable": false,
	"sWidth":"130px",
	"bVisible": true
},

{
	"sName": "id",
	"mDataProp": "total",
	"aTargets": [7],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        if(data != null)
        return '<span class="pull-right numeric">'+data+'</span>';
        else
        return '<span></span>';
    },
	"bSortable": false,
	"sWidth":"130px",
	"bVisible": true
}

],
    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                 var tahun = $('#tahun').val();
                 var bulan = $('#bulan').val();
                 if(bulan){
					   aoData.push(
									{"name": 'bulan', "value": bulan}
			            );
			     }

                 if(tahun){
					   aoData.push(
									{"name": 'tahun', "value": tahun}
			            );
			     }
                $.ajax({ "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData ,
                    "success": function (json) {
                        fnCallback(json);
                       },
                    "complete": function () {
                       }
                });
            }
});
});

</g:javascript>
