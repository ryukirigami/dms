

<%@ page import="com.kombos.example.Contoh" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'contoh.label', default: 'Contoh')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteContoh;

$(function(){ 
	deleteContoh=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/contoh/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadContohTable();
   				expandTableLayout('contoh');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-contoh" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="contoh"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${contohInstance?.alamat}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="alamat-label" class="property-label"><g:message
					code="contoh.alamat.label" default="Alamat" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="alamat-label">
						%{--<ba:editableValue
								bean="${contohInstance}" field="alamat"
								url="${request.contextPath}/Contoh/updatefield" type="text"
								title="Enter alamat" onsuccess="reloadContohTable();" />--}%
							
								<g:fieldValue bean="${contohInstance}" field="alamat"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${contohInstance?.jumlahKendaraan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="jumlahKendaraan-label" class="property-label"><g:message
					code="contoh.jumlahKendaraan.label" default="Jumlah Kendaraan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="jumlahKendaraan-label">
						%{--<ba:editableValue
								bean="${contohInstance}" field="jumlahKendaraan"
								url="${request.contextPath}/Contoh/updatefield" type="text"
								title="Enter jumlahKendaraan" onsuccess="reloadContohTable();" />--}%
							
								<g:fieldValue bean="${contohInstance}" field="jumlahKendaraan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${contohInstance?.kota}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kota-label" class="property-label"><g:message
					code="contoh.kota.label" default="Kota" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kota-label">
						%{--<ba:editableValue
								bean="${contohInstance}" field="kota"
								url="${request.contextPath}/Contoh/updatefield" type="text"
								title="Enter kota" onsuccess="reloadContohTable();" />--}%
							
								<g:fieldValue bean="${contohInstance}" field="kota"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${contohInstance?.nama}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="nama-label" class="property-label"><g:message
					code="contoh.nama.label" default="Nama" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="nama-label">
						%{--<ba:editableValue
								bean="${contohInstance}" field="nama"
								url="${request.contextPath}/Contoh/updatefield" type="text"
								title="Enter nama" onsuccess="reloadContohTable();" />--}%
							
								<g:fieldValue bean="${contohInstance}" field="nama"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${contohInstance?.tanggalLahir}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tanggalLahir-label" class="property-label"><g:message
					code="contoh.tanggalLahir.label" default="Tanggal Lahir" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tanggalLahir-label">
						%{--<ba:editableValue
								bean="${contohInstance}" field="tanggalLahir"
								url="${request.contextPath}/Contoh/updatefield" type="text"
								title="Enter tanggalLahir" onsuccess="reloadContohTable();" />--}%
							
								<g:formatDate date="${contohInstance?.tanggalLahir}" />
							
						</span></td>
					
				</tr>
				</g:if>
				<g:if test="${contohInstance?.catatanPersetujuan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="nama-label" class="property-label"><g:message
					code="contoh.catatanPersetujuan.label" default="Catatan Persetujuan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="nama-label">
						%{--<ba:editableValue
								bean="${contohInstance}" field="nama"
								url="${request.contextPath}/Contoh/updatefield" type="text"
								title="Enter nama" onsuccess="reloadContohTable();" />--}%
							
								<g:fieldValue bean="${contohInstance}" field="catatanPersetujuan"/>
							
						</span></td>
					
				</tr>
				</g:if>
				
				<g:if test="${contohInstance?.catatanImplementasi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="nama-label" class="property-label"><g:message
					code="contoh.catatanImplementasi.label" default="Catatan Implementasi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="nama-label">
						%{--<ba:editableValue
								bean="${contohInstance}" field="nama"
								url="${request.contextPath}/Contoh/updatefield" type="text"
								title="Enter nama" onsuccess="reloadContohTable();" />--}%
							
								<g:fieldValue bean="${contohInstance}" field="catatanImplementasi"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('contoh');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${contohInstance?.id}"
					update="[success:'contoh-form',failure:'contoh-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteContoh('${contohInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
