
<%@ page import="com.kombos.example.Contoh" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="contoh_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="contoh.alamat.label" default="Alamat" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="contoh.jumlahKendaraan.label" default="Jumlah Kendaraan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="contoh.kota.label" default="Kota" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="contoh.nama.label" default="Nama" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="contoh.tanggalLahir.label" default="Tanggal Lahir" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_alamat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_alamat" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_jumlahKendaraan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_jumlahKendaraan" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_kota" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_kota" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_nama" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_nama" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_tanggalLahir" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_tanggalLahir" value="date.struct">
					<input type="hidden" name="search_tanggalLahir_day" id="search_tanggalLahir_day" value="">
					<input type="hidden" name="search_tanggalLahir_month" id="search_tanggalLahir_month" value="">
					<input type="hidden" name="search_tanggalLahir_year" id="search_tanggalLahir_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_tanggalLahir_dp" value="" id="search_tanggalLahir" class="search_init">
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var contohTable;
var reloadContohTable;
$(function(){
	
	reloadContohTable = function() {
		contohTable.fnDraw();
	}

	
	$('#search_tanggalLahir').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tanggalLahir_day').val(newDate.getDate());
			$('#search_tanggalLahir_month').val(newDate.getMonth()+1);
			$('#search_tanggalLahir_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			contohTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	contohTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	contohTable = $('#contoh_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "alamat",
	"mDataProp": "alamat",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "jumlahKendaraan",
	"mDataProp": "jumlahKendaraan",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "kota",
	"mDataProp": "kota",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "nama",
	"mDataProp": "nama",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "tanggalLahir",
	"mDataProp": "tanggalLahir",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var alamat = $('#filter_alamat input').val();
						if(alamat){
							aoData.push(
									{"name": 'sCriteria_alamat', "value": alamat}
							);
						}
	
						var jumlahKendaraan = $('#filter_jumlahKendaraan input').val();
						if(jumlahKendaraan){
							aoData.push(
									{"name": 'sCriteria_jumlahKendaraan', "value": jumlahKendaraan}
							);
						}
	
						var kota = $('#filter_kota input').val();
						if(kota){
							aoData.push(
									{"name": 'sCriteria_kota', "value": kota}
							);
						}
	
						var nama = $('#filter_nama input').val();
						if(nama){
							aoData.push(
									{"name": 'sCriteria_nama', "value": nama}
							);
						}

						var tanggalLahir = $('#search_tanggalLahir').val();
						var tanggalLahirDay = $('#search_tanggalLahir_day').val();
						var tanggalLahirMonth = $('#search_tanggalLahir_month').val();
						var tanggalLahirYear = $('#search_tanggalLahir_year').val();
						
						if(tanggalLahir){
							aoData.push(
									{"name": 'sCriteria_tanggalLahir', "value": "date.struct"},
									{"name": 'sCriteria_tanggalLahir_dp', "value": tanggalLahir},
									{"name": 'sCriteria_tanggalLahir_day', "value": tanggalLahirDay},
									{"name": 'sCriteria_tanggalLahir_month', "value": tanggalLahirMonth},
									{"name": 'sCriteria_tanggalLahir_year', "value": tanggalLahirYear}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
