
<%@ page import="com.kombos.hrd.TrainingClassRoom" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'trainingClassRoom.label', default: 'TrainingClassRoom')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <style>
    .form-horizontal input + .help-block, .form-horizontal select + .help-block, .form-horizontal textarea + .help-block, .form-horizontal .uneditable-input + .help-block, .form-horizontal .input-prepend + .help-block, .form-horizontal .input-append + .help-block {
        font-size: 10px !important;
        font-style: italic !important;
        margin-top: 6px !important;
    }
    .modal.fade.in {
        left: 25.5% !important;
        width: 1205px !important;
    }
    .modal.fade {
        top: -100%;
    }
    .dataTables_length {
        display: none;
    }
    </style>
    <r:require modules="baseapplayout, baseapplist, autoNumeric" />
    <g:javascript>
			var show;
			var edit;
			var oFormService;
			var deletedKaryawan;
			var selectedTraining = {};
			$(function(){
			    oFormService= {
			        fnInit: function() {
			            var that = this;
			            $("#btnTambahPeserta").click(function() {
			                that.fnTambahPeserta();
			            });
			            $("#btnInputHasil").click(function() {
			                that.fnInputHasilPeserta();
			            })
			        },
			        fnInputHasilPeserta: function() {
                        var that = this;
                        var trainingId = -1;
                        $("#trainingClassRoom_datatables tbody").find("tr").each(function() {
                            var radio = $(this).find("td:eq(1) input[type='radio']");
                            if (radio.is(":checked")) {
                               var id = radio.data("id");
                                    var nama = radio.data("nama");
                                  <g:remoteFunction action="inputHasilPeserta"
                                                    onLoading="jQuery('#spinner').fadeIn(1);"
                                                    onSuccess="oFormService.fnLoadPesertaForm(data);"
                                                    onComplete="jQuery('#spinner').fadeOut();"
                                                    params="'id='+id"
                                    />
                            }
                        });
			        },
			        fnTambahPeserta: function() {
			            var that = this;
			            var trainingId = -1;
                        $("#trainingClassRoom_datatables tbody").find("tr").each(function() {
                            var radio = $(this).find("td:eq(1) input[type='radio']");
                            if (radio.is(":checked")) {
                                    var id = radio.data("id");
                                    var nama = radio.data("nama");
                                      <g:remoteFunction action="loadKaryawanPesertaTraining"
                                                        onLoading="jQuery('#spinner').fadeIn(1);"
                                                        onSuccess="oFormService.fnLoadPesertaForm(data);"
                                                        onComplete="jQuery('#spinner').fadeOut();"
                                                        params="'id='+id"
                                        />
                                    }
                                })
                    },
                    fnLoadPesertaForm: function(data) {
                        $("#box-table").fadeOut();
                        $("#box-search").fadeOut();
                        $("#navbar").fadeOut();
                        $("#box-tambahPeserta").fadeIn();
                        deletedKaryawan = [];
                        $("#peserta-form").html(data);

                    },
                    fnLoadPesertaTrainingForm: function() {
                        $("#box-table").fadeIn();
                        $("#box-search").fadeIn();
                        $("#box-tambahPeserta").fadeOut();
                        $("#navbar").fadeIn();
                    },

			        fnShowKaryawanDataTable: function() {
                        $("#karyawanModal").modal("show");
			        },
			        fnHideKaryawanDataTable: function() {
			            $("#karyawanModal").modal("hide");
			        }
			    }
			    oFormService.fnInit();
			    $("#btnCari").click(function() {
			        reloadTrainingClassRoomTable();
			    })

$('.box-action').click(function(){
switch($(this).attr('target')){
case '_CREATE_' :
shrinkTableLayout('trainingClassRoom');
        <g:remoteFunction action="create"
                          onLoading="jQuery('#spinner').fadeIn(1);"
                          onSuccess="loadFormInstance('trainingClassRoom', data, textStatus);"
                          onComplete="jQuery('#spinner').fadeOut();" />
        break;
    case '_DELETE_' :
        bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
								function(result){
									if(result){
										massDelete('trainingClassRoom', '${request.contextPath}/trainingClassRoom/massdelete', reloadTrainingClassRoomTable);
									}
								});

							break;
				   }
				   return false;
				});

				show = function(id) {
					showInstance('trainingClassRoom','${request.contextPath}/trainingClassRoom/show/'+id);
				};
				
				edit = function(id) {
					editInstance('trainingClassRoom','${request.contextPath}/trainingClassRoom/edit/'+id);
				};

});
    </g:javascript>
</head>
<body>
<div class="box" id="box-search">
    <div class="span12">
        <fieldset>
            <table>
                <tr style="display:table-row;">
                    <td style="width: 130px; display:table-cell; padding:5px;">
                        <label class="control-label" for="sCriteria_namaTraining">Nama Training</label>
                    </td>
                    <td>
                        <input type="text" id="sCriteria_namaTraining">
                    </td>
                </tr>
                <tr>
                    <td style="width: 130px; display:table-cell; padding:5px;">
                        <label class="control-label" for="search_dateBegin">Tanggal Mulai Training</label>
                    </td>
                    <td>
                        <ba:datePicker name="search_dateBegin" precision="day" format="yyyy-MM-dd"/>
                    </td>
                    <td>
                        <span style="padding: 10px; position: relative; top: -6px; left: 3px;">-</span>
                    </td>
                    <td style="width: 130px; display:table-cell; padding:5px;">
                        <ba:datePicker name="search_dateFinish" precision="day" format="yyyy-MM-dd"/>
                    </td>
                </tr>
                <tr style="display:table-row;">
                    <td style="width: 130px; display:table-cell; padding:5px;">
                        <label class="control-label" for="sCriteria_namaInstruktur">Instruktur</label>
                    </td>
                    <td>
                        <input type="text" id="sCriteria_namaInstruktur">
                    </td>
                </tr>
                <tr style="display:table-row;">
                    <td style="width: 130px; display:table-cell; padding:5px;">
                        <label class="control-label" for="sCriteria_tipeTraining">Tipe Training</label>
                    </td>
                    <td>
                        <g:select name="sCriteria_tipeTraining" from="${com.kombos.hrd.TrainingType.list()}"
                                  optionKey="id" noSelection="['':'']" optionValue="tipeTraining"/>
                    </td>
                </tr>
                <tr>
                    <td style="width: 130px; display:table-cell; padding:5px;">
                    </td>
                    <td colspan="2">
                        <a id="btnCari" class="btn btn-primary" href="javascript:void(0);">
                            Cari
                        </a>
                      %{--  <a id="btnTambah" class="btn btn-primary" href="javascript:void(0);">
                            Tambah
                        </a>--}%
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
</div>
<div class="navbar box-header no-border" id="navbar">
    <span class="pull-left">Peserta Training</span>
    <ul class="nav pull-right">
        <li><a class="pull-right box-action" href="javascript:void(0);"
               style="display: block;" title="Tambah Peserta" id="btnTambahPeserta">&nbsp;&nbsp;<i
                    class="icon-plus"></i>&nbsp;&nbsp;
        </a></li>
        <li><a class="pull-right box-action" href="#"  id="btnInputHasil"
               style="display: block;" title="Input Hasil">&nbsp;&nbsp;<i
                    class="icon-pencil"></i>&nbsp;&nbsp;
        </a></li>
        <li class="separator"></li>
    </ul>
</div>
<div class="box" id="box-table" style="margin-top: 6px;">
    <div class="span12" id="trainingClassRoom-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <g:render template="dataTablesPesertaTraining" />
    </div>
    <div class="span7" id="trainingClassRoom-form" style="display: none;padding: 10px; box-shadow: 0px 1px 2px gray;"></div>
</div>

<div class="span12" id="box-tambahPeserta" style="display: none; margin-left: 0;">
     <div id="peserta-form">

     </div>
</div>

</body>
</html>
