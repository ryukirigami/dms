<%@ page import="com.kombos.administrasi.CompanyDealer; com.kombos.hrd.TrainingType; com.kombos.hrd.TrainingInstructor; com.kombos.hrd.TrainingClassRoom" %>

<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: trainingClassRoomInstance, field: 'namaTraining', 'error')} ">
    <label class="control-label" for="namaTraining">
        <g:message code="trainingClassRoom.namaTraining.label" default="Nama Training"/>

    </label>

    <div class="controls">
        <g:textField name="namaTraining" value="${trainingClassRoomInstance?.namaTraining}"/>
        <span class="help-block">
            <g:fieldError field="namaTraining" bean="${trainingClassRoomInstance}"/>
        </span>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: trainingClassRoomInstance, field: 'batch', 'error')} ">
    <label class="control-label" for="batch">
        <g:message code="trainingClassRoom.batch.label" default="Batch"/>

    </label>

    <div class="controls">
        <g:textField name="batch" value="${trainingClassRoomInstance?.batch}" onkeypress="return isNumberKey(event);" />
        <span class="help-block">
            <g:fieldError field="batch" bean="${trainingClassRoomInstance}"/>
        </span>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: trainingClassRoomInstance, field: 'dateBegin', 'error')} ">
    <label class="control-label" for="dateBegin">
        <g:message code="trainingClassRoom.dateBegin.label" default="Tanggal Mulai Training"/>

    </label>

    <div class="controls">
        <ba:datePicker name="dateBegin" precision="day" value="${trainingClassRoomInstance?.dateBegin}"
                       format="yyyy-MM-dd"/>
        <span class="help-block">
            <g:fieldError field="dateBegin" bean="${trainingClassRoomInstance}"/>
        </span>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: trainingClassRoomInstance, field: 'dateFinish', 'error')} ">
    <label class="control-label" for="dateFinish">
        <g:message code="trainingClassRoom.dateFinish.label" default="Tanggal Akhir Training"/>

    </label>

    <div class="controls">
        <ba:datePicker name="dateFinish" precision="day" value="${trainingClassRoomInstance?.dateFinish}"
                       format="yyyy-MM-dd"/>
        <span class="help-block">
            <g:fieldError field="dateFinish" bean="${trainingClassRoomInstance}"/>
        </span>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: trainingClassRoomInstance, field: 'trainingType', 'error')} ">
    <label class="control-label" for="trainingType">
        <g:message code="trainingClassRoom.trainingType.label" default="Tipe Training"/>

    </label>

    <div class="controls">
        <g:select id="trainingType" name="trainingType.id" from="${TrainingType.list()}" optionKey="id" optionValue="tipeTraining"
                   value="${trainingClassRoomInstance?.trainingType?.id}" class="many-to-one"/>
        <span class="help-block">
            <g:fieldError field="trainingType" bean="${trainingClassRoomInstance}"/>
        </span>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: trainingClassRoomInstance, field: 'branch', 'error')} ">
    <label class="control-label" for="branch">
        <g:message code="trainingClassRoom.trainingType.label" default="Cabang"/>

    </label>

    <div class="controls">
        <g:select id="branch" name="branch.id" from="${CompanyDealer.list()}" optionKey="id" optionValue="m011NamaWorkshop"
                  value="${trainingClassRoomInstance?.branch?.id}" class="many-to-one"/>
        <span class="help-block">
            <g:fieldError field="branch" bean="${trainingClassRoomInstance}"/>
        </span>
    </div>
</div>

<div class="control-group ${hasErrors(bean: trainingClassRoomInstance, field: 'certificationType', 'error')}">
    <label class="control-label" for="description">
        Sertifikasi
    </label>

    <div class="controls">
        <g:select name="certificationType.id" from="${com.kombos.hrd.CertificationType.list()}" optionKey="id" optionValue="namaSertifikasi"/>
        <span class="help-block">
            <g:fieldError field="certificationType" bean="${trainingClassRoomInstance}"/>
        </span>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: trainingClassRoomInstance, field: 'firstInstructor', 'error')} ">
    <label class="control-label" for="firstInstructor">
        <g:message code="trainingClassRoom.firstInstructor.label" default="Instruktur 1"/>

    </label>

    <div class="controls">
        <g:select id="firstInstructor" name="firstInstructor.id" from="${TrainingInstructor.list()}"
                  optionKey="id"  value="${trainingClassRoomInstance?.firstInstructor?.id}" optionValue="namaInstruktur" noSelection="['':'']"
                  class="many-to-one"/>
        <span class="help-block">
            <g:fieldError field="firstInstructor" bean="${trainingClassRoomInstance}"/>
        </span>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: trainingClassRoomInstance, field: 'secondInstructor', 'error')} ">
    <label class="control-label" for="secondInstructor">
        <g:message code="trainingClassRoom.secondInstructor.label" default="Instruktur 2"/>

    </label>

    <div class="controls">
        <g:select id="secondInstructor" name="secondInstructor.id" from="${TrainingInstructor.list()}"
                  optionKey="id"  value="${trainingClassRoomInstance?.secondInstructor?.id}" optionValue="namaInstruktur" noSelection="['':'']"
                  class="many-to-one"/>
        <span class="help-block">
            <g:fieldError field="secondInstructor" bean="${trainingClassRoomInstance}"/>
        </span>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: trainingClassRoomInstance, field: 'thirdInstructor', 'error')} ">
    <label class="control-label" for="thirdInstructor">
        <g:message code="trainingClassRoom.thirdInstructor.label" default="Instruktur 3"/>

    </label>

    <div class="controls">
        <g:select id="thirdInstructor" name="thirdInstructor.id" from="${TrainingInstructor.list()}"
                  optionKey="id"  value="${trainingClassRoomInstance?.thirdInstructor?.id}"  optionValue="namaInstruktur" noSelection="['':'']"
                  class="many-to-one"/>
        <span class="help-block">
            <g:fieldError field="thirdInstructor" bean="${trainingClassRoomInstance}"/>
        </span>
    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: trainingClassRoomInstance, field: 'pointMin', 'error')} ">
    <label class="control-label" for="pointMin">
        <g:message code="trainingClassRoom.pointMin.label" default="Nilai Minimal Lulus"/>

    </label>

    <div class="controls">
        <g:textField class="numeric" name="pointMin" value="${trainingClassRoomInstance.pointMin}"/>
        <span class="help-block">
            <g:fieldError field="pointMin" bean="${trainingClassRoomInstance}"/>
        </span>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: trainingClassRoomInstance, field: 'description', 'error')} ">
    <label class="control-label" for="description">
        <g:message code="trainingClassRoom.description.label" default="Description"/>

    </label>

    <div class="controls">
        <g:textField name="description" value="${trainingClassRoomInstance?.description}"/>
        <span class="help-block">
            <g:fieldError field="description" bean="${trainingClassRoomInstance}"/>
        </span>
    </div>
</div>


<g:javascript>
    $(function() {
        $(".numeric").autoNumeric("init", {
            vMin: '0',
            vMax: '99'
        });
    })

</g:javascript>



