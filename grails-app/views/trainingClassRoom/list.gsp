
<%@ page import="com.kombos.hrd.TrainingClassRoom" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'trainingClassRoom.label', default: 'TrainingClassRoom')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
        <style>
        .form-horizontal input + .help-block, .form-horizontal select + .help-block, .form-horizontal textarea + .help-block, .form-horizontal .uneditable-input + .help-block, .form-horizontal .input-prepend + .help-block, .form-horizontal .input-append + .help-block {
            font-size: 10px !important;
            font-style: italic !important;
            margin-top: 6px !important;
        }
        </style>
		<r:require modules="baseapplayout, baseapplist, autoNumeric" />
		<g:javascript>
			var show;
			var edit;
			$(function(){
			    $("#btnCari").click(function() {
			        reloadTrainingClassRoomTable();
			    })
			    $("#btnTambah").click(function() {
                    shrinkTableLayout('trainingClassRoom');
							<g:remoteFunction action="create"
                                              onLoading="jQuery('#spinner').fadeIn(1);"
                                              onSuccess="loadFormInstance('trainingClassRoom', data, textStatus);"
                                              onComplete="jQuery('#spinner').fadeOut();" />

                    })
$('.box-action').click(function(){
    switch($(this).attr('target')){
        case '_CREATE_' :
            shrinkTableLayout('trainingClassRoom');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('trainingClassRoom', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
								function(result){
									if(result){
										massDelete('trainingClassRoom', '${request.contextPath}/trainingClassRoom/massdelete', reloadTrainingClassRoomTable);
									}
								});

							break;
				   }
				   return false;
				});

				show = function(id) {
					showInstance('trainingClassRoom','${request.contextPath}/trainingClassRoom/show/'+id);
				};
				
				edit = function(id) {
					editInstance('trainingClassRoom','${request.contextPath}/trainingClassRoom/edit/'+id);
				};

});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left">Data Kelas Training</span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
    <div class="box" id="box-search">
        <div class="span12">
            <fieldset>
                <table>
                    <tr style="display:table-row;">
                        <td style="width: 130px; display:table-cell; padding:5px;">
                            <label class="control-label" for="sCriteria_namaTraining">Nama Training</label>
                        </td>
                        <td>
                            <input type="text" id="sCriteria_namaTraining">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 130px; display:table-cell; padding:5px;">
                            <label class="control-label" for="search_dateBegin">Tanggal Mulai Training</label>
                        </td>
                        <td>
                            <ba:datePicker name="search_dateBegin" precision="day" format="yyyy-MM-dd"/>
                        </td>
                        <td>
                            <span style="padding: 10px; position: relative; top: -6px; left: 3px;">-</span>
                        </td>
                        <td style="width: 130px; display:table-cell; padding:5px;">
                           <ba:datePicker name="search_dateFinish" precision="day" format="yyyy-MM-dd"/>
                        </td>
                    </tr>
                    <tr style="display:table-row;">
                        <td style="width: 130px; display:table-cell; padding:5px;">
                            <label class="control-label" for="sCriteria_namaInstruktur">Instruktur</label>
                        </td>
                        <td>
                            <input type="text" id="sCriteria_namaInstruktur">
                        </td>
                    </tr>
                    <tr style="display:table-row;">
                        <td style="width: 130px; display:table-cell; padding:5px;">
                            <label class="control-label" for="sCriteria_tipeTraining">Tipe Training</label>
                        </td>
                        <td>
                            <g:select name="sCriteria_tipeTraining" from="${com.kombos.hrd.TrainingType.list()}"
                                optionKey="id" noSelection="['':'']" optionValue="tipeTraining"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 130px; display:table-cell; padding:5px;">
                        </td>
                        <td colspan="2">
                            <a id="btnCari" class="btn btn-primary" href="javascript:void(0);">
                                Cari
                            </a>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
    </div>
	<div class="box" id="box-table">
		<div class="span12" id="trainingClassRoom-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <g:render template="dataTables" />
		</div>
		<div class="span7" id="trainingClassRoom-form" style="display: none;padding: 10px; box-shadow: 0px 1px 2px gray;"></div>
	</div>
</body>
</html>
