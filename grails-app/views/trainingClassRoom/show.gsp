

<%@ page import="com.kombos.hrd.TrainingClassRoom" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'trainingClassRoom.label', default: 'TrainingClassRoom')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteTrainingClassRoom;

$(function(){ 
	deleteTrainingClassRoom=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/trainingClassRoom/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadTrainingClassRoomTable();
   				expandTableLayout('trainingClassRoom');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-trainingClassRoom" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="trainingClassRoom"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${trainingClassRoomInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="trainingClassRoom.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${trainingClassRoomInstance}" field="createdBy"
								url="${request.contextPath}/TrainingClassRoom/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadTrainingClassRoomTable();" />--}%
							
								<g:fieldValue bean="${trainingClassRoomInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${trainingClassRoomInstance?.dateBegin}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateBegin-label" class="property-label"><g:message
					code="trainingClassRoom.dateBegin.label" default="Date Begin" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateBegin-label">
						%{--<ba:editableValue
								bean="${trainingClassRoomInstance}" field="dateBegin"
								url="${request.contextPath}/TrainingClassRoom/updatefield" type="text"
								title="Enter dateBegin" onsuccess="reloadTrainingClassRoomTable();" />--}%
							
								<g:formatDate date="${trainingClassRoomInstance?.dateBegin}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${trainingClassRoomInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="trainingClassRoom.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${trainingClassRoomInstance}" field="dateCreated"
								url="${request.contextPath}/TrainingClassRoom/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadTrainingClassRoomTable();" />--}%
							
								<g:formatDate date="${trainingClassRoomInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${trainingClassRoomInstance?.dateFinish}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateFinish-label" class="property-label"><g:message
					code="trainingClassRoom.dateFinish.label" default="Date Finish" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateFinish-label">
						%{--<ba:editableValue
								bean="${trainingClassRoomInstance}" field="dateFinish"
								url="${request.contextPath}/TrainingClassRoom/updatefield" type="text"
								title="Enter dateFinish" onsuccess="reloadTrainingClassRoomTable();" />--}%
							
								<g:formatDate date="${trainingClassRoomInstance?.dateFinish}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${trainingClassRoomInstance?.description}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="description-label" class="property-label"><g:message
					code="trainingClassRoom.description.label" default="Description" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="description-label">
						%{--<ba:editableValue
								bean="${trainingClassRoomInstance}" field="description"
								url="${request.contextPath}/TrainingClassRoom/updatefield" type="text"
								title="Enter description" onsuccess="reloadTrainingClassRoomTable();" />--}%
							
								<g:fieldValue bean="${trainingClassRoomInstance}" field="description"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${trainingClassRoomInstance?.firstInstructor}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="firstInstructor-label" class="property-label"><g:message
					code="trainingClassRoom.firstInstructor.label" default="First Instructor" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="firstInstructor-label">
						%{--<ba:editableValue
								bean="${trainingClassRoomInstance}" field="firstInstructor"
								url="${request.contextPath}/TrainingClassRoom/updatefield" type="text"
								title="Enter firstInstructor" onsuccess="reloadTrainingClassRoomTable();" />--}%
							
								<g:link controller="trainingInstructor" action="show" id="${trainingClassRoomInstance?.firstInstructor?.id}">${trainingClassRoomInstance?.firstInstructor?.namaInstruktur}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${trainingClassRoomInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="trainingClassRoom.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${trainingClassRoomInstance}" field="lastUpdProcess"
								url="${request.contextPath}/TrainingClassRoom/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadTrainingClassRoomTable();" />--}%
							
								<g:fieldValue bean="${trainingClassRoomInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${trainingClassRoomInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="trainingClassRoom.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${trainingClassRoomInstance}" field="lastUpdated"
								url="${request.contextPath}/TrainingClassRoom/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadTrainingClassRoomTable();" />--}%
							
								<g:formatDate date="${trainingClassRoomInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${trainingClassRoomInstance?.namaTraining}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="namaTraining-label" class="property-label"><g:message
					code="trainingClassRoom.namaTraining.label" default="Nama Training" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="namaTraining-label">
						%{--<ba:editableValue
								bean="${trainingClassRoomInstance}" field="namaTraining"
								url="${request.contextPath}/TrainingClassRoom/updatefield" type="text"
								title="Enter namaTraining" onsuccess="reloadTrainingClassRoomTable();" />--}%
							
								<g:fieldValue bean="${trainingClassRoomInstance}" field="namaTraining"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${trainingClassRoomInstance?.pointAverage}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="pointAverage-label" class="property-label"><g:message
					code="trainingClassRoom.pointAverage.label" default="Point Average" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="pointAverage-label">
						%{--<ba:editableValue
								bean="${trainingClassRoomInstance}" field="pointAverage"
								url="${request.contextPath}/TrainingClassRoom/updatefield" type="text"
								title="Enter pointAverage" onsuccess="reloadTrainingClassRoomTable();" />--}%
							
								<g:fieldValue bean="${trainingClassRoomInstance}" field="pointAverage"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${trainingClassRoomInstance?.pointMax}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="pointMax-label" class="property-label"><g:message
					code="trainingClassRoom.pointMax.label" default="Point Max" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="pointMax-label">
						%{--<ba:editableValue
								bean="${trainingClassRoomInstance}" field="pointMax"
								url="${request.contextPath}/TrainingClassRoom/updatefield" type="text"
								title="Enter pointMax" onsuccess="reloadTrainingClassRoomTable();" />--}%
							
								<g:fieldValue bean="${trainingClassRoomInstance}" field="pointMax"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${trainingClassRoomInstance?.pointMin}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="pointMin-label" class="property-label"><g:message
					code="trainingClassRoom.pointMin.label" default="Point Min" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="pointMin-label">
						%{--<ba:editableValue
								bean="${trainingClassRoomInstance}" field="pointMin"
								url="${request.contextPath}/TrainingClassRoom/updatefield" type="text"
								title="Enter pointMin" onsuccess="reloadTrainingClassRoomTable();" />--}%
							
								<g:fieldValue bean="${trainingClassRoomInstance}" field="pointMin"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${trainingClassRoomInstance?.secondInstructor}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="secondInstructor-label" class="property-label"><g:message
					code="trainingClassRoom.secondInstructor.label" default="Second Instructor" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="secondInstructor-label">
						%{--<ba:editableValue
								bean="${trainingClassRoomInstance}" field="secondInstructor"
								url="${request.contextPath}/TrainingClassRoom/updatefield" type="text"
								title="Enter secondInstructor" onsuccess="reloadTrainingClassRoomTable();" />--}%
							
								<g:link controller="trainingInstructor" action="show" id="${trainingClassRoomInstance?.secondInstructor?.id}">${trainingClassRoomInstance?.secondInstructor?.namaInstruktur.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${trainingClassRoomInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="trainingClassRoom.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${trainingClassRoomInstance}" field="staDel"
								url="${request.contextPath}/TrainingClassRoom/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadTrainingClassRoomTable();" />--}%
							
								<g:fieldValue bean="${trainingClassRoomInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${trainingClassRoomInstance?.thirdInstructor}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="thirdInstructor-label" class="property-label"><g:message
					code="trainingClassRoom.thirdInstructor.label" default="Third Instructor" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="thirdInstructor-label">
						%{--<ba:editableValue
								bean="${trainingClassRoomInstance}" field="thirdInstructor"
								url="${request.contextPath}/TrainingClassRoom/updatefield" type="text"
								title="Enter thirdInstructor" onsuccess="reloadTrainingClassRoomTable();" />--}%
							
								<g:link controller="trainingInstructor" action="show" id="${trainingClassRoomInstance?.thirdInstructor?.id}">${trainingClassRoomInstance?.thirdInstructor?.namaInstruktur.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${trainingClassRoomInstance?.totalTrainingDay}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="totalTrainingDay-label" class="property-label"><g:message
					code="trainingClassRoom.totalTrainingDay.label" default="Total Training Day" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="totalTrainingDay-label">
						%{--<ba:editableValue
								bean="${trainingClassRoomInstance}" field="totalTrainingDay"
								url="${request.contextPath}/TrainingClassRoom/updatefield" type="text"
								title="Enter totalTrainingDay" onsuccess="reloadTrainingClassRoomTable();" />--}%
							
								<g:fieldValue bean="${trainingClassRoomInstance}" field="totalTrainingDay"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${trainingClassRoomInstance?.trainingMember}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="trainingMember-label" class="property-label"><g:message
					code="trainingClassRoom.trainingMember.label" default="Training Member" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="trainingMember-label">
						%{--<ba:editableValue
								bean="${trainingClassRoomInstance}" field="trainingMember"
								url="${request.contextPath}/TrainingClassRoom/updatefield" type="text"
								title="Enter trainingMember" onsuccess="reloadTrainingClassRoomTable();" />--}%
							
								<g:each in="${trainingClassRoomInstance.trainingMember}" var="t">
								<g:link controller="trainingMember" action="show" id="${t.id}">${t?.karyawan?.nama.encodeAsHTML()}</g:link>
								</g:each>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${trainingClassRoomInstance?.trainingType}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="trainingType-label" class="property-label"><g:message
					code="trainingClassRoom.trainingType.label" default="Training Type" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="trainingType-label">
						%{--<ba:editableValue
								bean="${trainingClassRoomInstance}" field="trainingType"
								url="${request.contextPath}/TrainingClassRoom/updatefield" type="text"
								title="Enter trainingType" onsuccess="reloadTrainingClassRoomTable();" />--}%
							
								<g:link controller="trainingType" action="show" id="${trainingClassRoomInstance?.trainingType?.id}">${trainingClassRoomInstance?.trainingType?.tipeTraining.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>

                <g:if test="${trainingClassRoomInstance?.branch}">
                    <tr>
                        <td class="span2" style="text-align: right;"><span
                                id="branch-label" class="property-label"><g:message
                                    code="trainingClassRoom.branch.label" default="Cabang" />:</span></td>

                        <td class="span3"><span class="property-value"
                                                aria-labelledby="branch-label">
                            %{--<ba:editableValue
                                    bean="${trainingClassRoomInstance}" field="trainingType"
                                    url="${request.contextPath}/TrainingClassRoom/updatefield" type="text"
                                    title="Enter trainingType" onsuccess="reloadTrainingClassRoomTable();" />--}%

                            <g:link controller="companyDealer" action="show" id="${trainingClassRoomInstance?.branch?.id}">${trainingClassRoomInstance?.branch?.m011NamaWorkshop.encodeAsHTML()}</g:link>

                        </span></td>

                    </tr>
                </g:if>
			
				<g:if test="${trainingClassRoomInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="trainingClassRoom.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${trainingClassRoomInstance}" field="updatedBy"
								url="${request.contextPath}/TrainingClassRoom/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadTrainingClassRoomTable();" />--}%
							
								<g:fieldValue bean="${trainingClassRoomInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('trainingClassRoom');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${trainingClassRoomInstance?.id}"
					update="[success:'trainingClassRoom-form',failure:'trainingClassRoom-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteTrainingClassRoom('${trainingClassRoomInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
