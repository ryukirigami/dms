<%@ page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'trainingClassRoom.label', default: 'TrainingClassRoom')}" />
    <title><g:message code="default.create.label" args="[entityName]" /></title>
</head>
<body>
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<div class="box" id="box-search-pesertaTraining">
    <div class="span12">
        <fieldset>
            <table>
                <input type="hidden" id="trainingId" value="${training?.id}">
                <tr style="display:table-row;">
                    <td style="width: 130px; display:table-cell; padding:5px;">
                        <label class="control-label" for="namaTraining">Nama Training</label>
                    </td>
                    <td>
                        <span id="namaTraining" style="font-style: italic;">${training?.namaTraining}</span>
                    </td>
                </tr>
                <tr>
                    <td style="width: 130px; display:table-cell; padding:5px;">
                        <label class="control-label" for="dateBegin">Tanggal Training</label>
                    </td>
                    <td>
                        <span style="font-style: italic" id="dateBegin">
                            ${training?.dateBegin.format("dd/MM/yyyy")} - ${training?.dateFinish.format("dd/MM/yyyy")}
                        </span>
                    </td>
                </tr>
                <tr style="display:table-row;">
                    <td style="width: 130px; display:table-cell; padding:5px;">
                        <label class="control-label" for="namaInstruktur">Instruktur</label>
                    </td>
                    <td>
                        <span id="namaInstruktur" style="font-style: italic;">${training?.firstInstructor?.namaInstruktur}, ${training?.secondInstructor?.namaInstruktur}, ${training?.thirdInstructor?.namaInstruktur}</span>
                    </td>
                </tr>
                <tr style="display:table-row;">
                    <td style="width: 130px; display:table-cell; padding:5px;">
                        <label class="control-label" for="tanggalLulus">Tanggal Lulus</label>
                    </td>
                    <td>
                        <ba:datePicker name="tanggalLulus" precision="day" format="dd/MM/yyyy" value="${training?.dateFinish}" />
                    </td>
                </tr>
                <tr style="display:table-row;">
                    <td style="width: 130px; display:table-cell; padding:5px;">
                        <label class="control-label" for="rataKelas">Nilai Rata-rata Kelas</label>
                    </td>
                    <td>
                        <g:textField name="rataKelas" id="rataKelas" class="hasil numeric"></g:textField>
                    </td>
                </tr>
                <tr style="display:table-row;">
                    <td style="width: 130px; display:table-cell; padding:5px;">
                        <label class="control-label" for="nilaiNgajar">Nilai Mengajar</label>
                    </td>
                    <td>
                        <g:textField name="nilaiNgajar" id="nilaiNgajar" class="hasil numeric"></g:textField>
                    </td>
                </tr>
                <tr>
                    <td style="width: 130px; display:table-cell; padding:5px;">
                    </td>
                    <td colspan="2">
                        <a id="btnKembali" class="btn cancel" href="javascript:void(0);" onclick="oFormService.fnLoadPesertaTrainingForm();">
                            Kembali
                        </a>
                        <a id="btnSimpanPeserta" class="btn btn-primary" href="javascript:void(0);">
                            Simpan
                        </a>
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
</div>

<div class="navbar box-header no-border">
    <span class="pull-left">List Peserta - Input Hasil Peserta</span>
    <ul class="nav pull-right">
        <li><a class="pull-right box-action" href="#"
               style="display: block;" title="Hapus Peserta" id="btnHapusPeserta">&nbsp;&nbsp;<i
                    class="icon-remove"></i>&nbsp;&nbsp;
        </a></li>
        <li class="separator"></li>
    </ul>
</div>

<div class="box">
    <div class="span12">
        <table id="pesertaTraining_datatables" cellpadding="0" cellspacing="0"
               border="0"
               class="display table table-striped table-bordered table-hover"
               width="100%">
            <thead>
            <th><input type="checkbox" name="child" class="selectAll">&nbsp;&nbsp;Nomor Karyawan</th>
            <th>Nama Karyawan</th>
            <th>Nilai Training</th>
            <th>Nilai Instruktur</th>
            <th>Tanggal Lulus</th>
            </thead>
            <tbody>
            <g:each in="${members}" var="i">
                <tr>
                    <td><input type="checkbox" class="row-select" data-id="${i.karyawan?.id}" data-mid="${i.id}">&nbsp;&nbsp;${i.karyawan?.nomorPokokKaryawan}</td>
                    <td>${i.karyawan?.nama}</td>
                    <td><input type="text" class="hasil numeric" value="${i.point}"></td>
                    <td><input type="text" class="hasil numeric" value="${i.pointInstruktur}"></td>
                    <td>${i.tglGraduate}</td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
</div>

<g:javascript>
    var pesertaKaryawanDataTables;
    $(function() {
        $(".numeric").autoNumeric("init", {
            vMax: 100,
            vMin: 0
        });

        $("#btnSimpanPeserta").click(function() {
            var tgl = $("#tanggalLulus").val();
            var rataKelas = $("#rataKelas").val();
            var nilaiNgajar = $("#nilaiNgajar").val();
            if (tgl && rataKelas && nilaiNgajar) {
                bootbox.confirm('Anda sudah yakin?',
								function(result){
									if(result){
									    var dataPeserta = [];
									    var trainingId = $("#trainingId").val();

                                        $("#pesertaTraining_datatables tbody").find("tr").each(function(k,v) {
                                            var id = $(this).find("td:eq(0) input[type='checkbox']").data("id");
                                            var mid = $(this).find("td:eq(0) input[type='checkbox']").data("mid");
                                            var point = $(this).find("td:eq(2) input.hasil").autoNumeric("get");
                                            var pointInstruktur = $(this).find("td:eq(3) input.hasil").autoNumeric("get");
                                            var tglLulus = $('#tanggalLulus').val();
                                            var rtKelas = $('#rataKelas').val();
                                            var nilaiNg = $('#nilaiNgajar').val();

                                            dataPeserta.push({
                                                karyawanId: id,
                                                point: point,
                                                pointInstruktur: pointInstruktur,
                                                trainingId: trainingId,
                                                memberId: mid,
                                                tglLulus: tglLulus,
                                                pointAvg: rtKelas,
                                                pointMax: nilaiNg
                                            })
                                        })
                                        var params = {
                                            inputNilaiPeserta: JSON.stringify(dataPeserta),
                                            deletedPeserta: deletedKaryawan.length > 0 ? JSON.stringify(deletedKaryawan) : -1
                                        };
                                        $.post("${request.getContextPath()}/trainingClassRoom/updatePesertaTraining", params, function(data) {
                                            oFormService.fnLoadPesertaTrainingForm();
                                        })
									}
								});

            } else {
                alert("Mohon masukan tanggal lulus");
                $("#tglLulus").focus();
            }

        })
        $("#btnHapusPeserta").click(function() {
            bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
								function(result){
									if(result){
										 $("#pesertaTraining_datatables tbody").find("tr").each(function() {
                                            var check = $(this).find("td:eq(0) input[type='checkbox']");
                                            if (check.is(":checked")) {
                                                if (check.data("id")) {
                                                    // kalo row ini punya id, delete juga di table
                                                    deletedKaryawan.push(check.data("id"));
                                                }
                                                var rowIndex = pesertaKaryawanDataTables.fnGetPosition(this);
                                                pesertaKaryawanDataTables.fnDeleteRow(rowIndex);
                                            }
                                        })
									}
								});


        })

        pesertaKaryawanDataTables = $("#pesertaTraining_datatables").dataTable({
            "bPaginate": true,
            "bFilter": true,
            "bStateSave": true,
            "bSort": true,
            "bProcessing": true
        });

        $('#pesertaTraining_datatables tbody tr').live('click', function () {
            if($(this).find('.row-select').is(":checked")){
                $(this).find('.row-select').parent().parent().addClass('row_selected');
            } else {
                $(this).find('.row-select').parent().parent().removeClass('row_selected');
            }
        });

    })
</g:javascript>
</body>
</html>
