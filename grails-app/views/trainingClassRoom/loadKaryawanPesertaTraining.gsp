<%@ page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'trainingClassRoom.label', default: 'TrainingClassRoom')}" />
    <title><g:message code="default.create.label" args="[entityName]" /></title>
</head>
<body>
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<div class="box" id="box-search-pesertaTraining">
    <div class="span12">
        <fieldset>
            <table>
                <input type="hidden" id="trainingId" value="${training?.id}">
                <tr style="display:table-row;">
                    <td style="width: 130px; display:table-cell; padding:5px;">
                        <label class="control-label" for="namaTraining">Nama Training</label>
                    </td>
                    <td>
                        <span id="namaTraining" style="font-style: italic;">${training?.namaTraining}</span>
                    </td>
                </tr>
                <tr>
                    <td style="width: 130px; display:table-cell; padding:5px;">
                        <label class="control-label" for="dateBegin">Tanggal Training</label>
                    </td>
                    <td>
                        <span style="font-style: italic" id="dateBegin">
                            ${training?.dateBegin} - ${training?.dateFinish}
                        </span>
                    </td>
                </tr>
                <tr style="display:table-row;">
                    <td style="width: 130px; display:table-cell; padding:5px;">
                        <label class="control-label" for="namaInstruktur">Instruktur</label>
                    </td>
                    <td>
                        <span id="namaInstruktur" style="font-style: italic;">${training?.firstInstructor?.namaInstruktur}, ${training?.secondInstructor?.namaInstruktur}, ${training?.thirdInstructor?.namaInstruktur}</span>
                    </td>
                </tr>
                <tr>
                    <td style="width: 130px; display:table-cell; padding:5px;">
                    </td>
                    <td colspan="2">
                        <a id="btnKembali" class="btn cancel" href="javascript:void(0);" onclick="oFormService.fnLoadPesertaTrainingForm();">
                            Kembali
                        </a>
                        <a id="btnSimpanPeserta" class="btn btn-primary" href="javascript:void(0);">
                            Simpan
                        </a>
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
</div>

<div class="navbar box-header no-border">
    <span class="pull-left">List Peserta</span>
    <ul class="nav pull-right">
        <li><a class="pull-right box-action" href="javascript:void(0);"
               style="display: block;" title="Tambah Peserta" onclick="oFormService.fnShowKaryawanDataTable();">&nbsp;&nbsp;<i
                    class="icon-plus"></i>&nbsp;&nbsp;
        </a></li>
        <li><a class="pull-right box-action" href="#"
               style="display: block;" title="Hapus Peserta" id="btnHapusPeserta">&nbsp;&nbsp;<i
                    class="icon-remove"></i>&nbsp;&nbsp;
        </a></li>
        <li class="separator"></li>
    </ul>
</div>

<div class="box">
    <div class="span12">
        <table id="pesertaTraining_datatables" cellpadding="0" cellspacing="0"
               border="0"
               class="display table table-striped table-bordered table-hover"
               width="100%">
            <thead>
            <th><input type="checkbox" name="child" class="selectAll">&nbsp;&nbsp;Nomor Karyawan</th>
            <th>Nama Karyawan</th>
            </thead>
            <tbody>
                <g:each in="${members}" var="i">
                    <tr>
                        <td><input type="checkbox" class="row-select" data-mid="${i.id}" data-id="${i.karyawan?.id}">&nbsp;&nbsp;${i.karyawan?.nomorPokokKaryawan}</td>
                        <td>${i.karyawan?.nama}</td>
                    </tr>
                </g:each>
            </tbody>
        </table>
    </div>
</div>

<!-- Karyawan Modal -->

<div id="karyawanModal" class="modal fade">
    <div class="modal-dialog" style="width: 1200px; top: -17px;;">
        <div class="modal-content" style="width: 1200px;">
            <div class="modal-header">
                <a class="close" href="javascript:void(0);" data-dismiss="modal">×</a>
                <h4>Data Karyawan - List</h4>
            </div>
            <!-- dialog body -->
            <div class="modal-body" id="karyawanModal-body" style="max-height: 1200px;">
                <div class="box">
                    <div class="span12">
                        <fieldset>
                            <table>
                                <tr style="display:table-row;">
                                    <td style="width: 130px; display:table-cell; padding:5px;">
                                        <label class="control-label" for="sCriteria_nama">Nama Karyawan</label>
                                    </td>
                                    <td style="width: 130px; display:table-cell; padding:5px;">
                                        <input type="text" id="sCriteria_nama">
                                    </td>
                                    <td >
                                        <a id="btnSearchKaryawan" class="btn btn-primary" href="javascript:void(0);" style="position: relative; top: -5px;">
                                            Cari
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                </div>
                <g:render template="dataTablesKaryawan" />
            </div>

            <div class="modal-footer">
                <a href="javascript:void(0);" class="btn cancel">
                    Tutup
                </a>
                <a href="javascript:void(0);" id="btnPilihKaryawan" class="btn btn-primary">
                    Pilih Karyawan
                </a>
            </div>
        </div>
    </div>
</div>
<g:javascript>
    var pesertaKaryawanDataTables;
    $(function() {
        $("#btnSimpanPeserta").click(function() {
            bootbox.confirm('Anda sudah yakin?',
								function(result){
									if(result){
									    var dataPeserta = [];
									    var trainingId = $("#trainingId").val();
                                        var allPesertaData = pesertaKaryawanDataTables.fnGetData();
                                        if (allPesertaData.length > 0) {
                                            $(allPesertaData).each(function(k,v) {
                                                var pid = $(v[0]).data("pid");
                                                if (pid) {
                                                    dataPeserta.push({
                                                        karyawanId: pid,
                                                        trainingId: trainingId
                                                    });
                                                }
                                            })
                                        }

                                        var params = {
                                            newPeserta: JSON.stringify(dataPeserta),
                                            deletedPeserta: deletedKaryawan.length > 0 ? JSON.stringify(deletedKaryawan) : -1
                                        };
                                        $.post("${request.getContextPath()}/trainingClassRoom/updatePesertaTraining", params, function(data) {
                                            oFormService.fnLoadPesertaTrainingForm();
                                        })
									}
								});

        })
        $("#btnHapusPeserta").click(function() {
            bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
								function(result){
									if(result){
										 $("#pesertaTraining_datatables tbody").find("tr").each(function() {
                                            var check = $(this).find("td:eq(0) input[type='checkbox']");
                                            if (check.is(":checked")) {
                                                if (check.data("id")) {
                                                    // kalo row ini punya id, delete juga di table
                                                    deletedKaryawan.push(check.data("mid"));
                                                }
                                                var rowIndex = pesertaKaryawanDataTables.fnGetPosition(this);
                                                pesertaKaryawanDataTables.fnDeleteRow(rowIndex);
                                            }
                                        })
									}
								});


        })

        pesertaKaryawanDataTables = $("#pesertaTraining_datatables").dataTable({
            "bPaginate": true,
            "bFilter": true,
            "bStateSave": true,
            "bSort": true,
            "bProcessing": true
        });

        $('#pesertaTraining_datatables tbody tr').live('click', function () {
            if($(this).find('.row-select').is(":checked")){
                $(this).find('.row-select').parent().parent().addClass('row_selected');
            } else {
                $(this).find('.row-select').parent().parent().removeClass('row_selected');
            }
        });

    })
</g:javascript>
</body>
</html>
