<%@ page import="com.kombos.hrd.TrainingClassRoom" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'trainingClassRoom.label', default: 'TrainingClassRoom')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="edit-trainingClassRoom" class="content scaffold-edit" role="main">
			<legend>Ubah Data Training</legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			%{--<g:hasErrors bean="${trainingClassRoomInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${trainingClassRoomInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>--}%
			%{--<g:form method="post" >--}%
			<g:formRemote class="form-horizontal" name="edit" on404="alert('not found!');" onSuccess="reloadTrainingClassRoomTable();" update="trainingClassRoom-form"
				url="[controller: 'trainingClassRoom', action:'update']">
				<g:hiddenField name="id" value="${trainingClassRoomInstance?.id}" />
				<g:hiddenField name="version" value="${trainingClassRoomInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons controls">
					<g:actionSubmit class="btn btn-primary save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                    <a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout('trainingClassRoom');"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
				</fieldset>
			</g:formRemote>
			%{--</g:form>--}%
		</div>
	</body>
</html>
