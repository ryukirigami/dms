
<%@ page import="com.kombos.hrd.KeluargaKaryawan" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="keluargaKaryawan_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="keluargaKaryawan.hubungan.label" default="Hubungan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="keluargaKaryawan.jabatan.label" default="Jabatan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="keluargaKaryawan.jenisKelamin.label" default="Jenis Kelamin" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="keluargaKaryawan.karyawan.label" default="Karyawan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="keluargaKaryawan.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="keluargaKaryawan.nama.label" default="Nama" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="keluargaKaryawan.pekerjaan.label" default="Pekerjaan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="keluargaKaryawan.tanggalLahirKel.label" default="Tanggal Lahir" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="keluargaKaryawan.tempatLahir.label" default="Tempat Lahir" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_hubungan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_hubungan" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_jabatan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_jabatan" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_jenisKelamin" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_jenisKelamin" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_karyawan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_karyawan" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_nama" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_nama" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_pekerjaan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_pekerjaan" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_tanggalLahir" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_tanggalLahir" value="date.struct">
					<input type="hidden" name="search_tanggalLahir_day" id="search_tanggalLahir_day" value="">
					<input type="hidden" name="search_tanggalLahir_month" id="search_tanggalLahir_month" value="">
					<input type="hidden" name="search_tanggalLahir_year" id="search_tanggalLahir_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_tanggalLahir_dp" value="" id="search_tanggalLahir" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_tempatLahir" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_tempatLahir" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var keluargaKaryawanTable;
var reloadKeluargaKaryawanTable;
$(function(){
	
	reloadKeluargaKaryawanTable = function() {
		keluargaKaryawanTable.fnDraw();
	}

	
	$('#search_tanggalLahir').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tanggalLahir_day').val(newDate.getDate());
			$('#search_tanggalLahir_month').val(newDate.getMonth()+1);
			$('#search_tanggalLahir_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			keluargaKaryawanTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	keluargaKaryawanTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	keluargaKaryawanTable = $('#keluargaKaryawan_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "hubungan",
	"mDataProp": "hubungan",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "jabatan",
	"mDataProp": "jabatan",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "jenisKelamin",
	"mDataProp": "jenisKelamin",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "karyawan",
	"mDataProp": "karyawan",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "nama",
	"mDataProp": "nama",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "pekerjaan",
	"mDataProp": "pekerjaan",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "tanggalLahirKel",
	"mDataProp": "tanggalLahirKel",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "tempatLahir",
	"mDataProp": "tempatLahir",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var hubungan = $('#filter_hubungan input').val();
						if(hubungan){
							aoData.push(
									{"name": 'sCriteria_hubungan', "value": hubungan}
							);
						}
	
						var jabatan = $('#filter_jabatan input').val();
						if(jabatan){
							aoData.push(
									{"name": 'sCriteria_jabatan', "value": jabatan}
							);
						}
	
						var jenisKelamin = $('#filter_jenisKelamin input').val();
						if(jenisKelamin){
							aoData.push(
									{"name": 'sCriteria_jenisKelamin', "value": jenisKelamin}
							);
						}
	
						var karyawan = $('#filter_karyawan input').val();
						if(karyawan){
							aoData.push(
									{"name": 'sCriteria_karyawan', "value": karyawan}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}
	
						var nama = $('#filter_nama input').val();
						if(nama){
							aoData.push(
									{"name": 'sCriteria_nama', "value": nama}
							);
						}
	
						var pekerjaan = $('#filter_pekerjaan input').val();
						if(pekerjaan){
							aoData.push(
									{"name": 'sCriteria_pekerjaan', "value": pekerjaan}
							);
						}

						var tanggalLahir = $('#search_tanggalLahir').val();
						var tanggalLahirDay = $('#search_tanggalLahir_day').val();
						var tanggalLahirMonth = $('#search_tanggalLahir_month').val();
						var tanggalLahirYear = $('#search_tanggalLahir_year').val();
						
						if(tanggalLahir){
							aoData.push(
									{"name": 'sCriteria_tanggalLahir', "value": "date.struct"},
									{"name": 'sCriteria_tanggalLahir_dp', "value": tanggalLahir},
									{"name": 'sCriteria_tanggalLahir_day', "value": tanggalLahirDay},
									{"name": 'sCriteria_tanggalLahir_month', "value": tanggalLahirMonth},
									{"name": 'sCriteria_tanggalLahir_year', "value": tanggalLahirYear}
							);
						}
	
						var tempatLahir = $('#filter_tempatLahir input').val();
						if(tempatLahir){
							aoData.push(
									{"name": 'sCriteria_tempatLahir', "value": tempatLahir}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
