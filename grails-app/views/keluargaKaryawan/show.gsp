

<%@ page import="com.kombos.hrd.KeluargaKaryawan" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'keluargaKaryawan.label', default: 'KeluargaKaryawan')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteKeluargaKaryawan;

$(function(){ 
	deleteKeluargaKaryawan=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/keluargaKaryawan/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadKeluargaKaryawanTable();
   				expandTableLayout('keluargaKaryawan');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-keluargaKaryawan" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="keluargaKaryawan"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${keluargaKaryawanInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="keluargaKaryawan.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${keluargaKaryawanInstance}" field="createdBy"
								url="${request.contextPath}/KeluargaKaryawan/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadKeluargaKaryawanTable();" />--}%
							
								<g:fieldValue bean="${keluargaKaryawanInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${keluargaKaryawanInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="keluargaKaryawan.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${keluargaKaryawanInstance}" field="dateCreated"
								url="${request.contextPath}/KeluargaKaryawan/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadKeluargaKaryawanTable();" />--}%
							
								<g:formatDate date="${keluargaKaryawanInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${keluargaKaryawanInstance?.hubungan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="hubungan-label" class="property-label"><g:message
					code="keluargaKaryawan.hubungan.label" default="Hubungan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="hubungan-label">
						%{--<ba:editableValue
								bean="${keluargaKaryawanInstance}" field="hubungan"
								url="${request.contextPath}/KeluargaKaryawan/updatefield" type="text"
								title="Enter hubungan" onsuccess="reloadKeluargaKaryawanTable();" />--}%
							
								<g:fieldValue bean="${keluargaKaryawanInstance}" field="hubungan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${keluargaKaryawanInstance?.jabatan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="jabatan-label" class="property-label"><g:message
					code="keluargaKaryawan.jabatan.label" default="Jabatan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="jabatan-label">
						%{--<ba:editableValue
								bean="${keluargaKaryawanInstance}" field="jabatan"
								url="${request.contextPath}/KeluargaKaryawan/updatefield" type="text"
								title="Enter jabatan" onsuccess="reloadKeluargaKaryawanTable();" />--}%
							
								<g:fieldValue bean="${keluargaKaryawanInstance}" field="jabatan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${keluargaKaryawanInstance?.jenisKelamin}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="jenisKelamin-label" class="property-label"><g:message
					code="keluargaKaryawan.jenisKelamin.label" default="Jenis Kelamin" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="jenisKelamin-label">
						%{--<ba:editableValue
								bean="${keluargaKaryawanInstance}" field="jenisKelamin"
								url="${request.contextPath}/KeluargaKaryawan/updatefield" type="text"
								title="Enter jenisKelamin" onsuccess="reloadKeluargaKaryawanTable();" />--}%
							
								<g:fieldValue bean="${keluargaKaryawanInstance}" field="jenisKelamin"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${keluargaKaryawanInstance?.karyawan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="karyawan-label" class="property-label"><g:message
					code="keluargaKaryawan.karyawan.label" default="Karyawan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="karyawan-label">
						%{--<ba:editableValue
								bean="${keluargaKaryawanInstance}" field="karyawan"
								url="${request.contextPath}/KeluargaKaryawan/updatefield" type="text"
								title="Enter karyawan" onsuccess="reloadKeluargaKaryawanTable();" />--}%
							
								<g:link controller="karyawan" action="show" id="${keluargaKaryawanInstance?.karyawan?.id}">${keluargaKaryawanInstance?.karyawan?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${keluargaKaryawanInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="keluargaKaryawan.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${keluargaKaryawanInstance}" field="lastUpdProcess"
								url="${request.contextPath}/KeluargaKaryawan/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadKeluargaKaryawanTable();" />--}%
							
								<g:fieldValue bean="${keluargaKaryawanInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${keluargaKaryawanInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="keluargaKaryawan.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${keluargaKaryawanInstance}" field="lastUpdated"
								url="${request.contextPath}/KeluargaKaryawan/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadKeluargaKaryawanTable();" />--}%
							
								<g:formatDate date="${keluargaKaryawanInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${keluargaKaryawanInstance?.nama}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="nama-label" class="property-label"><g:message
					code="keluargaKaryawan.nama.label" default="Nama" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="nama-label">
						%{--<ba:editableValue
								bean="${keluargaKaryawanInstance}" field="nama"
								url="${request.contextPath}/KeluargaKaryawan/updatefield" type="text"
								title="Enter nama" onsuccess="reloadKeluargaKaryawanTable();" />--}%
							
								<g:fieldValue bean="${keluargaKaryawanInstance}" field="nama"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${keluargaKaryawanInstance?.pekerjaan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="pekerjaan-label" class="property-label"><g:message
					code="keluargaKaryawan.pekerjaan.label" default="Pekerjaan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="pekerjaan-label">
						%{--<ba:editableValue
								bean="${keluargaKaryawanInstance}" field="pekerjaan"
								url="${request.contextPath}/KeluargaKaryawan/updatefield" type="text"
								title="Enter pekerjaan" onsuccess="reloadKeluargaKaryawanTable();" />--}%
							
								<g:fieldValue bean="${keluargaKaryawanInstance}" field="pekerjaan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${keluargaKaryawanInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="keluargaKaryawan.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${keluargaKaryawanInstance}" field="staDel"
								url="${request.contextPath}/KeluargaKaryawan/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadKeluargaKaryawanTable();" />--}%
							
								<g:fieldValue bean="${keluargaKaryawanInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${keluargaKaryawanInstance?.tanggalLahirKel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tanggalLahirKel-label" class="property-label"><g:message
					code="keluargaKaryawan.tanggalLahirKel.label" default="Tanggal Lahir" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tanggalLahirKel-label">
						%{--<ba:editableValue
								bean="${keluargaKaryawanInstance}" field="tanggalLahir"
								url="${request.contextPath}/KeluargaKaryawan/updatefield" type="text"
								title="Enter tanggalLahir" onsuccess="reloadKeluargaKaryawanTable();" />--}%
							
								<g:formatDate date="${keluargaKaryawanInstance?.tanggalLahirKel}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${keluargaKaryawanInstance?.tempatLahir}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tempatLahir-label" class="property-label"><g:message
					code="keluargaKaryawan.tempatLahir.label" default="Tempat Lahir" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tempatLahir-label">
						%{--<ba:editableValue
								bean="${keluargaKaryawanInstance}" field="tempatLahir"
								url="${request.contextPath}/KeluargaKaryawan/updatefield" type="text"
								title="Enter tempatLahir" onsuccess="reloadKeluargaKaryawanTable();" />--}%
							
								<g:fieldValue bean="${keluargaKaryawanInstance}" field="tempatLahir"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${keluargaKaryawanInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="keluargaKaryawan.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${keluargaKaryawanInstance}" field="updatedBy"
								url="${request.contextPath}/KeluargaKaryawan/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadKeluargaKaryawanTable();" />--}%
							
								<g:fieldValue bean="${keluargaKaryawanInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('keluargaKaryawan');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${keluargaKaryawanInstance?.id}"
					update="[success:'keluargaKaryawan-form',failure:'keluargaKaryawan-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteKeluargaKaryawan('${keluargaKaryawanInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
