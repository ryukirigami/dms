<%@ page import="com.kombos.hrd.KeluargaKaryawan" %>

<g:if test="${params.onDataKaryawan}">
    <g:hiddenField name="karyawanId" value="${params.karyawanId}"/>
</g:if>

<div class="control-group fieldcontain ${hasErrors(bean: keluargaKaryawanInstance, field: 'nama', 'error')} ">
    <label class="control-label" for="nama">
        <g:message code="keluargaKaryawan.nama.label" default="Nama"/>

    </label>

    <div class="controls">
        <g:textField name="nama" value="${keluargaKaryawanInstance?.nama}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: keluargaKaryawanInstance, field: 'hubungan', 'error')} ">
    <label class="control-label" for="hubungan">
        <g:message code="keluargaKaryawan.hubungan.label" default="Hubungan"/>

    </label>

    <div class="controls">

        <g:select name="hubungan" from="${com.kombos.hrd.HubunganKeluarga.values()}" optionKey="value"
                  optionValue="key"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: keluargaKaryawanInstance, field: 'jabatan', 'error')} ">
    <label class="control-label" for="jabatan">
        <g:message code="keluargaKaryawan.jabatan.label" default="Jabatan"/>

    </label>

    <div class="controls">
        <g:textField name="jabatan" value="${keluargaKaryawanInstance?.jabatan}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: keluargaKaryawanInstance, field: 'jenisKelamin', 'error')} ">
    <label class="control-label" for="jenisKelamin">
        <g:message code="keluargaKaryawan.jenisKelamin.label" default="Jenis Kelamin"/>

    </label>

    <div class="controls">

        <g:radioGroup value="${keluargaKaryawanInstance?.jenisKelamin}" name="jenisKelamin"
                      labels="['Pria', 'Wanita']" values="['Pria', 'Wanita']">
            <p>${it.radio} ${it.label}</p>
        </g:radioGroup>

    </div>
</div>

%{--<div class="control-group fieldcontain ${hasErrors(bean: keluargaKaryawanInstance, field: 'karyawan', 'error')} ">--}%
%{--<label class="control-label" for="karyawan">--}%
%{--<g:message code="keluargaKaryawan.karyawan.label" default="Karyawan" />--}%
%{----}%
%{--</label>--}%
%{--<div class="controls">--}%
%{--<g:select id="karyawan" name="karyawan.id" from="${com.kombos.hrd.Karyawan.list()}" optionKey="id" required="" value="${keluargaKaryawanInstance?.karyawan?.id}" class="many-to-one"/>--}%
%{--</div>--}%
%{--</div>--}%



<div class="control-group fieldcontain ${hasErrors(bean: keluargaKaryawanInstance, field: 'pekerjaan', 'error')} ">
    <label class="control-label" for="pekerjaan">
        <g:message code="keluargaKaryawan.pekerjaan.label" default="Pekerjaan"/>

    </label>

    <div class="controls">
        <g:textField name="pekerjaan" value="${keluargaKaryawanInstance?.pekerjaan}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: keluargaKaryawanInstance, field: 'tanggalLahirKel', 'error')} ">
    <label class="control-label" for="tanggalLahirKel">
        <g:message code="keluargaKaryawan.tanggalLahirKel.label" default="Tanggal Lahir"/>

    </label>

    <div class="controls">

        <ba:datePicker name="tanggalLahirKel" precision="day" value="${keluargaKaryawanInstance?.tanggalLahirKel}"
                       format="yyyy-MM-dd"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: keluargaKaryawanInstance, field: 'tempatLahir', 'error')} ">
    <label class="control-label" for="tempatLahir">
        <g:message code="keluargaKaryawan.tempatLahir.label" default="Tempat Lahir"/>

    </label>

    <div class="controls">
        <g:textField name="tempatLahir" value="${keluargaKaryawanInstance?.tempatLahir}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: keluargaKaryawanInstance, field: 'pendidikan', 'error')} ">
    <label class="control-label" for="pendidikan">
        <g:message code="keluargaKaryawan.pendidikan.label" default="Pendidikan"/>

    </label>

    <div class="controls">

        <g:select name="pendidikan.id" from="${com.kombos.hrd.Pendidikan.list()}" optionKey="id" optionValue="pendidikan"/>
    </div>
</div>

