
<%@ page import="com.kombos.maintable.Retention" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="retention_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="retention.m207NamaRetention.label" default="M207 Nama Retention" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="retention.m207FormatSms.label" default="M207 Format Sms" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="retention.m207StaOtomatis.label" default="Metode Pengiriman SMS" /></div>
			</th>
		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m207NamaRetention" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m207NamaRetention" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m207FormatSms" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m207FormatSms" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m207StaOtomatis" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <g:select name="search_m207StaOtomatis" class="search_init"
                              optionKey="id" optionValue="nama"
                        onchange="reloadRetentionTable()"
                              from="${[[id: 1, nama: "Otomatis"], [id: 2, nama: "Manual"], [id: 3, nama: "Manual & Otomatis"]]}"
                              noSelection="['': 'Semua']"/>
				</div>
			</th>
	

		</tr>
	</thead>
</table>

<g:javascript>
var retentionTable;
var reloadRetentionTable;
$(function(){
	
	reloadRetentionTable = function() {
		retentionTable.fnDraw();
	}

	var recordsGoodsPerPage = [];
    var anGoodsSelected;
    var jmlRecGoodsPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	retentionTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	retentionTable = $('#retention_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsGoods = $("#retention_datatables tbody .row-select");
            var jmlGoodsCek = 0;
            var nRow;
            var idRec;
            rsGoods.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsGoodsPerPage[idRec]=="1"){
                    jmlGoodsCek = jmlGoodsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsGoodsPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecGoodsPerPage = rsGoods.length;
            if(jmlGoodsCek==jmlRecGoodsPerPage && jmlRecGoodsPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m207NamaRetention",
	"mDataProp": "m207NamaRetention",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
	    if(data=='Ulang Tahun'){
		    return '<input type="checkbox" disabled="disabled"  aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	    }else if(data=='STNK Jatuh Tempo'){
		    return '<input type="checkbox" disabled="disabled"  aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	    }else if(data=='Pemberitahuan Selesai Invoice'){
		    return '<input type="checkbox" disabled="disabled"  aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	    }else{
		    return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	    }
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m207FormatSms",
	"mDataProp": "m207FormatSms",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m207StaOtomatis",
	"mDataProp": "m207StaOtomatis",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m207NamaRetention = $('#filter_m207NamaRetention input').val();
						if(m207NamaRetention){
							aoData.push(
									{"name": 'sCriteria_m207NamaRetention', "value": m207NamaRetention}
							);
						}
	
						var m207FormatSms = $('#filter_m207FormatSms input').val();
						if(m207FormatSms){
							aoData.push(
									{"name": 'sCriteria_m207FormatSms', "value": m207FormatSms}
							);
						}
	
						var m207StaOtomatis = $('#filter_m207StaOtomatis select').val();
						if(m207StaOtomatis){
							aoData.push(
									{"name": 'sCriteria_m207StaOtomatis', "value": m207StaOtomatis}
							);
						}
	


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

	$('.select-all').click(function(e) {

        $("#retention_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsGoodsPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsGoodsPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#retention_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsGoodsPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anGoodsSelected = retentionTable.$('tr.row_selected');

            if(jmlRecGoodsPerPage == anGoodsSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsGoodsPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
