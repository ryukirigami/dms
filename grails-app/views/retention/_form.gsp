<%@ page import="com.kombos.maintable.Retention" %>
<g:javascript>
    $(document).ready(function() {
        if($('#m207FormatSms').val().length>0){
            var len = $('#m207FormatSms').val().length;
            var jmlSms = parseInt(len / 150) + 1
            $('#jmlSMS').text(jmlSms);
            $('#hitung').text(len);
        }

        $('#m207FormatSms').keyup(function() {
            var len = this.value.length;
            var jmlSms = parseInt(len / 150) + 1
            $('#jmlSMS').text(jmlSms);
            $('#hitung').text(len);
        });
    });
</g:javascript>


<div class="control-group fieldcontain ${hasErrors(bean: retentionInstance, field: 'm207NamaRetention', 'error')} required">
    <label class="control-label" for="m207NamaRetention">
        <g:message code="retention.m207NamaRetention.label" default="M207 Nama Retention"/>
        <span class="required-indicator">*</span>
    </label>

    <div class="controls">
        <g:if test="${retentionInstance?.m207NamaRetention==Retention.ULANG_TAHUN}">
            <g:textField name="m207NamaRetention" maxlength="50" disabled="disabled"
                         value="${retentionInstance?.m207NamaRetention}"/>
        </g:if>
        <g:elseif test="${retentionInstance?.m207NamaRetention== Retention.STNK_JATUH_TEMPO}">
            <g:textField name="m207NamaRetention" maxlength="50" disabled="disabled"
                         value="${retentionInstance?.m207NamaRetention}"/>
        </g:elseif>
        <g:elseif test="${retentionInstance?.m207NamaRetention== Retention.SELESAI_INVOICE}">
            <g:textField name="m207NamaRetention" maxlength="50" disabled="disabled"
                         value="${retentionInstance?.m207NamaRetention}"/>
        </g:elseif>
        <g:else>
            <g:textField name="m207NamaRetention" maxlength="50" required=""
                         value="${retentionInstance?.m207NamaRetention}"/>
        </g:else>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: retentionInstance, field: 'm207FormatSms', 'error')} required">
    <label class="control-label" for="m207FormatSms">
        <g:message code="retention.m207FormatSms.label" default="M207 Format Sms"/>
        <span class="required-indicator">*</span>
    </label>

    <div class="controls">
        <g:textArea rows="8" name="m207FormatSms" id="m207FormatSms" maxlength="1000" required="" value="${retentionInstance?.m207FormatSms}"/>
        <br/>
        Jumlah Karakter : <span id="hitung">0</span><br>
        Jumlah SMS : <span id="jmlSMS">1</span>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: retentionInstance, field: 'm207StaOtomatis', 'error')} ">
    %{--<label class="control-label" for="m207StaOtomatis">--}%
        %{--<g:message code="retention.m207StaOtomatis.label" default="Metode Pengiriman SMS"/>--}%

    %{--</label>--}%

    %{--<div class="controls">--}%
        %{--<g:if test="${retentionInstance?.m207NamaRetention== Retention.ULANG_TAHUN}">--}%
            %{--<g:if test="${retentionInstance?.m207StaOtomatis=='1'}">--}%
                %{--Otomatis--}%
            %{--</g:if>--}%
            %{--<g:elseif test="${retentionInstance?.m207StaOtomatis=='2'}">--}%
                %{--Manual--}%
            %{--</g:elseif>--}%
            %{--<g:elseif test="${retentionInstance?.m207StaOtomatis=='3'}">--}%
                %{--Otomatis & Manual--}%
            %{--</g:elseif>--}%
        %{--</g:if>--}%
        %{--<g:elseif test="${retentionInstance?.m207NamaRetention== Retention.STNK_JATUH_TEMPO}">--}%
            %{--<g:if test="${retentionInstance?.m207StaOtomatis=='1'}">--}%
                %{--Otomatis--}%
            %{--</g:if>--}%
            %{--<g:elseif test="${retentionInstance?.m207StaOtomatis=='2'}">--}%
                %{--Manual--}%
            %{--</g:elseif>--}%
            %{--<g:elseif test="${retentionInstance?.m207StaOtomatis=='3'}">--}%
                %{--Otomatis & Manual--}%
            %{--</g:elseif>--}%
        %{--</g:elseif>--}%
        %{--<g:else>--}%
            %{--<g:select name="m207StaOtomatis"--}%
                      %{--optionKey="id" optionValue="nama"--}%
                      %{--from="${[[id: 1, nama: "Otomatis"], [id: 2, nama: "Manual"], [id: 3, nama: "Manual & Otomatis"]]}"--}%
                      %{--value="${retentionInstance?.m207StaOtomatis}" valueMessagePrefix="retention.m207StaOtomatis"--}%
                      %{--noSelection="['': '']"/>--}%
        %{--</g:else>--}%
    %{--</div>--}%
</div>

