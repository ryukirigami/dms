

<%@ page import="com.kombos.maintable.Retention" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'retention.label', default: 'Retention')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteRetention;

$(function(){ 
	deleteRetention=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/retention/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadRetentionTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-retention" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="retention"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${retentionInstance?.m207NamaRetention}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m207NamaRetention-label" class="property-label"><g:message
					code="retention.m207NamaRetention.label" default="M207 Nama Retention" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m207NamaRetention-label">
						%{--<ba:editableValue
								bean="${retentionInstance}" field="m207NamaRetention"
								url="${request.contextPath}/Retention/updatefield" type="text"
								title="Enter m207NamaRetention" onsuccess="reloadRetentionTable();" />--}%
							
								<g:fieldValue bean="${retentionInstance}" field="m207NamaRetention"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${retentionInstance?.m207FormatSms}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m207FormatSms-label" class="property-label"><g:message
					code="retention.m207FormatSms.label" default="M207 Format Sms" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m207FormatSms-label">
						%{--<ba:editableValue
								bean="${retentionInstance}" field="m207FormatSms"
								url="${request.contextPath}/Retention/updatefield" type="text"
								title="Enter m207FormatSms" onsuccess="reloadRetentionTable();" />--}%
							
								<g:fieldValue bean="${retentionInstance}" field="m207FormatSms"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${retentionInstance?.m207StaOtomatis}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m207StaOtomatis-label" class="property-label"><g:message
					code="retention.m207StaOtomatis.label" default="Metode Pengiriman SMS" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m207StaOtomatis-label">
						%{--<ba:editableValue
								bean="${retentionInstance}" field="m207StaOtomatis"
								url="${request.contextPath}/Retention/updatefield" type="text"
								title="Enter m207StaOtomatis" onsuccess="reloadRetentionTable();" />--}%
                                <g:if test="${retentionInstance?.m207StaOtomatis=='1'}">
                                    Otomatis
                                </g:if>
                                <g:elseif test="${retentionInstance?.m207StaOtomatis=='2'}">
                                    Manual
                                </g:elseif>
                                <g:elseif test="${retentionInstance?.m207StaOtomatis=='3'}">
                                    Manual & Otomatis
                                </g:elseif>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${retentionInstance?.m207Ket}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m207Ket-label" class="property-label"><g:message
					code="retention.m207Ket.label" default="M207 Ket" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m207Ket-label">
						%{--<ba:editableValue
								bean="${retentionInstance}" field="m207Ket"
								url="${request.contextPath}/Retention/updatefield" type="text"
								title="Enter m207Ket" onsuccess="reloadRetentionTable();" />--}%
							
								<g:fieldValue bean="${retentionInstance}" field="m207Ket"/>
							
						</span></td>
					
				</tr>
				</g:if>

				<g:if test="${retentionInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="retention.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${retentionInstance}" field="createdBy"
								url="${request.contextPath}/Retention/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadRetentionTable();" />--}%
							
								<g:fieldValue bean="${retentionInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${retentionInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="retention.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${retentionInstance}" field="updatedBy"
								url="${request.contextPath}/Retention/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadRetentionTable();" />--}%
							
								<g:fieldValue bean="${retentionInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${retentionInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="retention.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${retentionInstance}" field="lastUpdProcess"
								url="${request.contextPath}/Retention/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadRetentionTable();" />--}%
							
								<g:fieldValue bean="${retentionInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${retentionInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="retention.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${retentionInstance}" field="dateCreated"
								url="${request.contextPath}/Retention/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadRetentionTable();" />--}%
							
								<g:formatDate date="${retentionInstance?.dateCreated}" format="EEEE, dd MMMM yyyy HH:mm" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${retentionInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="retention.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${retentionInstance}" field="lastUpdated"
								url="${request.contextPath}/Retention/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadRetentionTable();" />--}%
							
								<g:formatDate date="${retentionInstance?.lastUpdated}" format="EEEE, dd MMMM yyyy HH:mm" />
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${retentionInstance?.id}"
					update="[success:'retention-form',failure:'retention-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
                <g:if test="${retentionInstance?.m207NamaRetention==Retention.ULANG_TAHUN}">

                </g:if>
                <g:elseif test="${retentionInstance?.m207NamaRetention== Retention.STNK_JATUH_TEMPO}">

                </g:elseif>
                <g:else>
                    <ba:confirm id="delete" class="btn cancel"
                                message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
                                onsuccess="deleteRetention('${retentionInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
                </g:else>

			</fieldset>
		</g:form>
	</div>
</body>
</html>
