

<%@ page import="com.kombos.hrd.StatusKaryawan" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'statusKaryawan.label', default: 'StatusKaryawan')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteStatusKaryawan;

$(function(){ 
	deleteStatusKaryawan=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/statusKaryawan/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadStatusKaryawanTable();
   				expandTableLayout('statusKaryawan');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-statusKaryawan" role="main">
		<legend>
			Show Status Karyawan
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="statusKaryawan"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${statusKaryawanInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="statusKaryawan.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${statusKaryawanInstance}" field="createdBy"
								url="${request.contextPath}/StatusKaryawan/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadStatusKaryawanTable();" />--}%
							
								<g:fieldValue bean="${statusKaryawanInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${statusKaryawanInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="statusKaryawan.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${statusKaryawanInstance}" field="dateCreated"
								url="${request.contextPath}/StatusKaryawan/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadStatusKaryawanTable();" />--}%
							
								<g:formatDate date="${statusKaryawanInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${statusKaryawanInstance?.keterangan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="keterangan-label" class="property-label"><g:message
					code="statusKaryawan.keterangan.label" default="Keterangan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="keterangan-label">
						%{--<ba:editableValue
								bean="${statusKaryawanInstance}" field="keterangan"
								url="${request.contextPath}/StatusKaryawan/updatefield" type="text"
								title="Enter keterangan" onsuccess="reloadStatusKaryawanTable();" />--}%
							
								<g:fieldValue bean="${statusKaryawanInstance}" field="keterangan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${statusKaryawanInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="statusKaryawan.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${statusKaryawanInstance}" field="lastUpdProcess"
								url="${request.contextPath}/StatusKaryawan/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadStatusKaryawanTable();" />--}%
							
								<g:fieldValue bean="${statusKaryawanInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${statusKaryawanInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="statusKaryawan.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${statusKaryawanInstance}" field="lastUpdated"
								url="${request.contextPath}/StatusKaryawan/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadStatusKaryawanTable();" />--}%
							
								<g:formatDate date="${statusKaryawanInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${statusKaryawanInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="statusKaryawan.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${statusKaryawanInstance}" field="staDel"
								url="${request.contextPath}/StatusKaryawan/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadStatusKaryawanTable();" />--}%
							
								<g:fieldValue bean="${statusKaryawanInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${statusKaryawanInstance?.statusKaryawan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="statusKaryawan-label" class="property-label"><g:message
					code="statusKaryawan.statusKaryawan.label" default="Status Karyawan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="statusKaryawan-label">
						%{--<ba:editableValue
								bean="${statusKaryawanInstance}" field="statusKaryawan"
								url="${request.contextPath}/StatusKaryawan/updatefield" type="text"
								title="Enter statusKaryawan" onsuccess="reloadStatusKaryawanTable();" />--}%
							
								<g:fieldValue bean="${statusKaryawanInstance}" field="statusKaryawan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${statusKaryawanInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="statusKaryawan.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${statusKaryawanInstance}" field="updatedBy"
								url="${request.contextPath}/StatusKaryawan/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadStatusKaryawanTable();" />--}%
							
								<g:fieldValue bean="${statusKaryawanInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('statusKaryawan');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${statusKaryawanInstance?.id}"
					update="[success:'statusKaryawan-form',failure:'statusKaryawan-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteStatusKaryawan('${statusKaryawanInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
