<%@ page import="com.kombos.hrd.StatusKaryawan" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'statusKaryawan.label', default: 'StatusKaryawan')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="create-statusKaryawan" class="content scaffold-create" role="main">
			<legend>Create Status Karyawan</legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${statusKaryawanInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${statusKaryawanInstance}" var="error">
                    <li>Field '${error.field}' harus unique!</li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form action="save" >--}%
			<g:formRemote class="form-horizontal" name="create" on404="alert('not found!');" onSuccess="reloadStatusKaryawanTable();" update="statusKaryawan-form"
              url="[controller: 'statusKaryawan', action:'save']">	
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons controls">
					<a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout('statusKaryawan');"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
					<g:submitButton class="btn btn-primary create" name="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
				</fieldset>
			</g:formRemote>
			%{--</g:form>--}%
		</div>
	</body>
</html>
