
<%@ page import="com.kombos.hrd.StatusKaryawan" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="statusKaryawan_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
    <tr>
        <th colspan="2">
            <input type="checkbox" id="checkAll">
        </th>
    </tr>
		<tr>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="statusKaryawan.statusKaryawan.label" default="Status Karyawan" /></div>
			</th>
            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="statusKaryawan.keterangan.label" default="Keterangan" /></div>
            </th>
		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_keterangan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_keterangan" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_statusKaryawan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_statusKaryawan" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var statusKaryawanTable;
var reloadStatusKaryawanTable;
var bIsManualCheck;
$(function(){
	bIsCheckAllClicked = false;
	bIsManualCheck = false;
	
	reloadStatusKaryawanTable = function() {
		statusKaryawanTable.fnDraw();
	}

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	statusKaryawanTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	statusKaryawanTable = $('#statusKaryawan_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [


{
	"sName": "statusKaryawan",
	"mDataProp": "statusKaryawan",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true,
     "mRender": function ( data, type, row ) {
		var _isC;
        var _c = false;
        var _checkbox = "";

        if (!bIsManualCheck) {
            _c = $("#checkAll").is(":checked");
        } else {
           if (aoCheckedValues[row['id']] !== undefined) {
                _c = aoCheckedValues[row['id']].value;
            } else {
                _c = $("#checkAll").is(":checked");
            }
        }

        if (_c)
            _checkbox = "checked";

		return '<input type="checkbox"  '+_checkbox+' class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	}

},
{
	"sName": "keterangan",
	"mDataProp": "keterangan",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var keterangan = $('#filter_keterangan input').val();
						if(keterangan){
							aoData.push(
									{"name": 'sCriteria_keterangan', "value": keterangan}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}
	
						var statusKaryawan = $('#filter_statusKaryawan input').val();
						if(statusKaryawan){
							aoData.push(
									{"name": 'sCriteria_statusKaryawan', "value": statusKaryawan}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

	aoCheckedValues = {};
	aoUncheckedValues = {};

	statusKaryawanTable.bind("draw", function() {

        $("#statusKaryawan_datatables tbody tr td:first-child input[type='checkbox']").click(function() {
            var _id = $(this).parent().find("input[type='hidden']").val();
            var _checked = $(this).is(":checked");

            aoCheckedValues[_id] = {
                code: _id,
                value : _checked
            }

            if (!_checked) {
                aoUncheckedValues[_id] = {
                    code: _id,
                    value: false
                };
            } else {
                aoUncheckedValues[_id] = {
                    code: _id,
                    value: true
                };
            }

//            bIsCheckAllClicked = false;
            bIsManualCheck = true;
            console.log(aoCheckedValues)

        })

        $("#checkAll").click(function() {
            var that = this;
            $("#statusKaryawan_datatables tbody").find("tr").each(function() {
                var _td = $(this).find("td:first-child");
                var _id = _td.find("input[type='hidden']").val();
                var _checkbox = _td.find("input[type='checkbox']");

                _checkbox.prop("checked", $(that).is(":checked"));

                aoCheckedValues[_id] = {
                    code: _id,
                    value: $(that).is(":checked")
                };

                bIsCheckAllClicked = $(that).is(":checked");
                bIsManualCheck = false;
            })
        })

    });

});
</g:javascript>


			
