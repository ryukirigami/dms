<%@ page import="com.kombos.hrd.StatusKaryawan" %>


<div class="control-group fieldcontain ${hasErrors(bean: statusKaryawanInstance, field: 'statusKaryawan', 'error')} ">
    <label class="control-label" for="statusKaryawan">
        <g:message code="statusKaryawan.statusKaryawan.label" default="Status Karyawan" />

    </label>
    <div class="controls">
        <g:textField name="statusKaryawan" value="${statusKaryawanInstance?.statusKaryawan}" maxlength="16" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: statusKaryawanInstance, field: 'keterangan', 'error')} ">
	<label class="control-label" for="keterangan">
		<g:message code="statusKaryawan.keterangan.label" default="Keterangan" />
		
	</label>
	<div class="controls">
	<g:textArea rows="5" cols="3" maxlength="128" name="keterangan" value="${statusKaryawanInstance?.keterangan}" />
	</div>
</div>

<g:javascript>
    $(function() {
        oFormUtils.fnInit();
    })
</g:javascript>



