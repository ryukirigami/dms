

<%@ page import="com.kombos.baseapp.CommonCode" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'commonCode.label', default: 'CommonCode')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteCommonCode;

$(function(){ 
	deleteCommonCode=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/commonCode/delete/'+id,
			data: { id: id },
   			success:function(data,textStatus){
   				reloadCommonCodeTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-commonCode" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="commonCode"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${commonCodeInstance?.commoncode}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="commoncode-label" class="property-label"><g:message
					code="commonCode.commoncode.label" default="Commoncode" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="commoncode-label">
						<ba:editableValue
								bean="${commonCodeInstance}" field="commoncode"
								url="${request.contextPath}/CommonCode/updatefield" type="text"
								title="Enter commoncode" onsuccess="reloadCommonCodeTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${commonCodeInstance?.commonname}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="commonname-label" class="property-label"><g:message
					code="commonCode.commonname.label" default="Commonname" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="commonname-label">
						<ba:editableValue
								bean="${commonCodeInstance}" field="commonname"
								url="${request.contextPath}/CommonCode/updatefield" type="text"
								title="Enter commonname" onsuccess="reloadCommonCodeTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${commonCodeInstance?.commondesc}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="commondesc-label" class="property-label"><g:message
					code="commonCode.commondesc.label" default="Commondesc" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="commondesc-label">
						<ba:editableValue
								bean="${commonCodeInstance}" field="commondesc"
								url="${request.contextPath}/CommonCode/updatefield" type="text"
								title="Enter commondesc" onsuccess="reloadCommonCodeTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${commonCodeInstance?.commonusage}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="commonusage-label" class="property-label"><g:message
					code="commonCode.commonusage.label" default="Commonusage" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="commonusage-label">
						<ba:editableValue
								bean="${commonCodeInstance}" field="commonusage"
								url="${request.contextPath}/CommonCode/updatefield" type="text"
								title="Enter commonusage" onsuccess="reloadCommonCodeTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${commonCodeInstance?.codetype}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="codetype-label" class="property-label"><g:message
					code="commonCode.codetype.label" default="Codetype" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="codetype-label">
						<ba:editableValue
								bean="${commonCodeInstance}" field="codetype"
								url="${request.contextPath}/CommonCode/updatefield" type="text"
								title="Enter codetype" onsuccess="reloadCommonCodeTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${commonCodeInstance?.parentcode}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="parentcode-label" class="property-label"><g:message
					code="commonCode.parentcode.label" default="Parentcode" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="parentcode-label">
						<ba:editableValue
								bean="${commonCodeInstance}" field="parentcode"
								url="${request.contextPath}/CommonCode/updatefield" type="text"
								title="Enter parentcode" onsuccess="reloadCommonCodeTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${commonCodeInstance?.createdby}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdby-label" class="property-label"><g:message
					code="commonCode.createdby.label" default="Createdby" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdby-label">
						<ba:editableValue
								bean="${commonCodeInstance}" field="createdby"
								url="${request.contextPath}/CommonCode/updatefield" type="text"
								title="Enter createdby" onsuccess="reloadCommonCodeTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${commonCodeInstance?.createddate}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createddate-label" class="property-label"><g:message
					code="commonCode.createddate.label" default="Createddate" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createddate-label">
						<ba:editableValue
								bean="${commonCodeInstance}" field="createddate"
								url="${request.contextPath}/CommonCode/updatefield" type="text"
								title="Enter createddate" onsuccess="reloadCommonCodeTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${commonCodeInstance?.createdhost}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdhost-label" class="property-label"><g:message
					code="commonCode.createdhost.label" default="Createdhost" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdhost-label">
						<ba:editableValue
								bean="${commonCodeInstance}" field="createdhost"
								url="${request.contextPath}/CommonCode/updatefield" type="text"
								title="Enter createdhost" onsuccess="reloadCommonCodeTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${commonCodeInstance?.lasteditby}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lasteditby-label" class="property-label"><g:message
					code="commonCode.lasteditby.label" default="Lasteditby" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lasteditby-label">
						<ba:editableValue
								bean="${commonCodeInstance}" field="lasteditby"
								url="${request.contextPath}/CommonCode/updatefield" type="text"
								title="Enter lasteditby" onsuccess="reloadCommonCodeTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${commonCodeInstance?.lasteditdate}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lasteditdate-label" class="property-label"><g:message
					code="commonCode.lasteditdate.label" default="Lasteditdate" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lasteditdate-label">
						<ba:editableValue
								bean="${commonCodeInstance}" field="lasteditdate"
								url="${request.contextPath}/CommonCode/updatefield" type="text"
								title="Enter lasteditdate" onsuccess="reloadCommonCodeTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${commonCodeInstance?.lastedithost}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastedithost-label" class="property-label"><g:message
					code="commonCode.lastedithost.label" default="Lastedithost" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastedithost-label">
						<ba:editableValue
								bean="${commonCodeInstance}" field="lastedithost"
								url="${request.contextPath}/CommonCode/updatefield" type="text"
								title="Enter lastedithost" onsuccess="reloadCommonCodeTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${commonCodeInstance?.issyncronize}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="issyncronize-label" class="property-label"><g:message
					code="commonCode.issyncronize.label" default="Issyncronize" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="issyncronize-label">
						<ba:editableValue
								bean="${commonCodeInstance}" field="issyncronize"
								url="${request.contextPath}/CommonCode/updatefield" type="text"
								title="Enter issyncronize" onsuccess="reloadCommonCodeTable();" />
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${commonCodeInstance?.commoncode}"
					update="[success:'commonCode-form',failure:'commonCode-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteCommonCode('${commonCodeInstance?.commoncode}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
