<%@ page import="com.kombos.baseapp.CommonCode" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'commonCode.label', default: 'CommonCode')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
        <g:javascript disposition="head">
            var deleteCommonCode;

            $(function(){
                deleteCommonCode=function(id){
                    $.ajax({type:'POST', url:'${request.contextPath}/commonCode/delete/'+id,
                        data: { id: id },
                        success:function(data,textStatus){
                            reloadCommonCodeTable();
                            expandTableLayout();
                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
                }
            });

        </g:javascript>
	</head>
	<body>
		<div id="edit-commonCode" class="content scaffold-edit" role="main">
			<legend><g:message code="default.edit.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${commonCodeInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${commonCodeInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form method="post" >--}%
			<g:formRemote class="form-horizontal" name="edit" on404="alert('not found!');" onSuccess="reloadCommonCodeTable();" update="commonCode-form"
				url="[controller: 'commonCode', action:'update']">
				<g:hiddenField name="id" value="${commonCodeInstance?.commoncode}" />
				<g:hiddenField name="version" value="${commonCodeInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
					<input type="hidden" id="codetype" value="" name="codetype">
				</fieldset>
                <fieldset class="buttons controls">
                    <g:actionSubmit class="btn btn-primary save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                    <a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout();"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
                </fieldset>
			</g:formRemote>
			%{--</g:form>--}%
		</div>
	</body>
</html>
