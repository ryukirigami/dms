<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>CommonCode</title>
    <script type="text/javascript">

        function initCommonCodeGrid() {
            jQuery("#CommonCode-Grid").jqGrid({
                url:'${request.getContextPath()}/commonCode/getList',
                editurl:'${request.getContextPath()}/commonCode/edit',
                datatype:'json',
                mtype:'POST',
                colNames:[

                    '<g:message code="app.commonCode.key.label"/>',
                    '<g:message code="app.commonCode.value.label"/>'

                ],
                colModel:[

                    {name:'key', index:'key', width:200, editable:true, edittype:'text', editrules:{required:true}, searchoptions:{sopt:['eq', 'ne', 'cn', 'nc']}}  ,
                    {name:'value', index:'value', width:200, editable:true, edittype:'text', editrules:{required:true}, searchoptions:{sopt:['eq', 'ne', 'cn', 'nc']}}
                ],
                height:'auto',
                pager:'#CommonCode-Pager',
                rowNum:10,
                rowList:[10, 20, 30],
                viewrecords:true,
                sortable:true,
                gridview:true,
                forceFit:true,
                loadui:'block',
                rownumbers:true,
                caption:'CommonCode',
                onSelectRow:function (rowid) {
                    var rd = jQuery("#CommonCode-Grid").jqGrid('getRowData', rowid);
                    //TODO
                },
                gridComplete:function () {

                },
                sortname:'id',
                sortorder:'asc'
            });

            jQuery('#CommonCode-Grid').jqGrid(
                    'navGrid', '#CommonCode-Pager',
                    {edit:true, add:true, del:true, view:true}, // options
                    {
                        afterSubmit:afterSubmitForm
                    }, // edit options
                    {
                        afterSubmit:afterSubmitForm
                    }, // add options
                    {
                        afterSubmit:afterSubmitForm,
                        onclickSubmit:function (params, posdata) {
                            var id = jQuery("#CommonCode-Grid").jqGrid('getGridParam', 'selrow');
                            var rd = jQuery("#CommonCode-Grid").jqGrid('getRowData', id);
                            return {key:rd.key};
                        }
                    }, // del options
                    {}, // search options
                    {} // view options
            );
        }

        function afterSubmitForm(response, postdata) {
            var res = jQuery.parseJSON(response.responseText);
            alert(res.message);
            return [true, ""];
        }

        jQuery(function () {
            initCommonCodeGrid();
        });

    </script>
</head>

<body>

<table id="CommonCode-Grid"></table>

<div id="CommonCode-Pager"></div>

</body>
</html>