
<%@ page import="com.kombos.baseapp.CommonCode" %>
<grid:grid name='commonCodeDatatables' 

		columns.commoncode.dataTables.sWidth='"8%"'
	
		columns.commonname.dataTables.sWidth='"8%"'
	
		columns.commondesc.dataTables.sWidth='"8%"'
	
		columns.commonusage.dataTables.sWidth='"8%"'
	
		columns.codetype.dataTables.sWidth='"8%"'
	
		columns.parentcode.dataTables.sWidth='"8%"'
	
		columns.createdby.dataTables.sWidth='"8%"'
	
		columns.createddate.dataTables.sWidth='"5%"'
	
		columns.createdhost.dataTables.sWidth='"8%"'
	
		columns.lasteditby.dataTables.sWidth='"8%"'
	
		columns.lasteditdate.dataTables.sWidth='"5%"'
	
		columns.lastedithost.dataTables.sWidth='"8%"'
	
		columns.issyncronize.dataTables.sWidth='"8%"'
	
		
></grid:grid>