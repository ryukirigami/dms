<%@ page import="com.kombos.baseapp.CommonCode" %>



<div class="control-group fieldcontain ${hasErrors(bean: commonCodeInstance, field: 'commoncode', 'error')} required">
	<label class="control-label" for="commoncode">
		<g:message code="commonCode.commoncode.label" default="Commoncode" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <%
            if(request.requestURI.contains('edit.dispatch') || request.requestURI.contains('update.dispatch')){
        %>
        <g:textField name="commoncode" required="" maxlength="5" disabled="" value="${commonCodeInstance?.commoncode}"/>
        <%
            } else {
        %>
        <g:textField name="commoncode" required="" maxlength="5" value="${commonCodeInstance?.commoncode}"/>
        <%
            }
        %>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: commonCodeInstance, field: 'commonname', 'error')} ">
	<label class="control-label" for="commonname">
		<g:message code="commonCode.commonname.label" default="Commonname" />
		
	</label>
	<div class="controls">
	<g:textField name="commonname" value="${commonCodeInstance?.commonname}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: commonCodeInstance, field: 'commondesc', 'error')} ">
	<label class="control-label" for="commondesc">
		<g:message code="commonCode.commondesc.label" default="Commondesc" />
		
	</label>
	<div class="controls">
	<g:textField name="commondesc" value="${commonCodeInstance?.commondesc}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: commonCodeInstance, field: 'commonusage', 'error')} ">
	<label class="control-label" for="commonusage">
		<g:message code="commonCode.commonusage.label" default="Commonusage" />
		
	</label>
	<div class="controls">
	<g:textField name="commonusage" value="${commonCodeInstance?.commonusage}"/>
	</div>
</div>



<div class="control-group fieldcontain ${hasErrors(bean: commonCodeInstance, field: 'parentcode', 'error')} ">
	<label class="control-label" for="parentcode">
		<g:message code="commonCode.parentcode.label" default="Parentcode" />
		
	</label>
	<div class="controls">
	<g:textField name="parentcode" value="${commonCodeInstance?.parentcode}"/>
	</div>
</div>
%{--
<div class="control-group fieldcontain ${hasErrors(bean: commonCodeInstance, field: 'createdby', 'error')} ">
	<label class="control-label" for="createdby">
		<g:message code="commonCode.createdby.label" default="Createdby" />
		
	</label>
	<div class="controls">
	<g:textField name="createdby" value="${commonCodeInstance?.createdby}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: commonCodeInstance, field: 'createddate', 'error')} ">
	<label class="control-label" for="createddate">
		<g:message code="commonCode.createddate.label" default="Createddate" />
		
	</label>
	<div class="controls">
	<g:datePicker name="createddate" precision="day"  value="${commonCodeInstance?.createddate}" default="none" noSelection="['': '']" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: commonCodeInstance, field: 'createdhost', 'error')} ">
	<label class="control-label" for="createdhost">
		<g:message code="commonCode.createdhost.label" default="Createdhost" />
		
	</label>
	<div class="controls">
	<g:textField name="createdhost" value="${commonCodeInstance?.createdhost}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: commonCodeInstance, field: 'lasteditby', 'error')} ">
	<label class="control-label" for="lasteditby">
		<g:message code="commonCode.lasteditby.label" default="Lasteditby" />
		
	</label>
	<div class="controls">
	<g:textField name="lasteditby" value="${commonCodeInstance?.lasteditby}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: commonCodeInstance, field: 'lasteditdate', 'error')} ">
	<label class="control-label" for="lasteditdate">
		<g:message code="commonCode.lasteditdate.label" default="Lasteditdate" />
		
	</label>
	<div class="controls">
	<g:datePicker name="lasteditdate" precision="day"  value="${commonCodeInstance?.lasteditdate}" default="none" noSelection="['': '']" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: commonCodeInstance, field: 'lastedithost', 'error')} ">
	<label class="control-label" for="lastedithost">
		<g:message code="commonCode.lastedithost.label" default="Lastedithost" />
		
	</label>
	<div class="controls">
	<g:textField name="lastedithost" value="${commonCodeInstance?.lastedithost}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: commonCodeInstance, field: 'issyncronize', 'error')} ">
	<label class="control-label" for="issyncronize">
		<g:message code="commonCode.issyncronize.label" default="Issyncronize" />
		
	</label>
	<div class="controls">
	<g:checkBox name="issyncronize" value="${commonCodeInstance?.issyncronize}" />
	</div>
</div>
--}%
