<%@ page import="com.kombos.baseapp.CommonCode" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <%
        String codetype = params.codetype;
        if(codetype.equalsIgnoreCase('B')){
    %>
    <g:set var="entityName" value="${message(code: 'commonCode.label', default: 'Common Code')}"/>
    <%}else{%>
    <g:set var="entityName" value="${message(code: 'systemParameter.label', default: 'System Parameter')}"/>
    <%}%>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <r:require modules="baseapplayout" />
    %{--<r:require modules="baseapp"/>--}%
    <g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){

	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
                                   onLoading="jQuery('#spinner').fadeIn(1);"
                                   onSuccess="loadForm(data, textStatus);"
                                   onComplete="jQuery('#spinner').fadeOut();"/>
        break;
               case '_DELETE_' :
                   bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});

         		break;
       }
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/commonCode/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };

    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/commonCode/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };

    showChildren = function(id){
        $('#main-content').empty();
        $('#spinner').fadeIn(1);
        $.ajax({url: '${request.contextPath}/commonCodeDetail/list?codetype=${params.codetype}&commoncode='+id,type: "POST",dataType: "html",
        	complete : function (req, err) {
            	$('#main-content').append(req.responseText);
            	 $('#spinner').fadeOut();
           	}
        });
    }

    loadForm = function(data, textStatus){
		$('#commonCode-form').empty();
    	$('#commonCode-form').append(data);
    	jQuery('#codetype').val('${params.codetype}');
   	}

    shrinkTableLayout = function(){
    	if($("#commonCode-table").hasClass("span12")){
   			$("#commonCode-table").toggleClass("span12 span5");
        }
        $("#commonCode-form").css("display","block");
       	shrinkCommonCodeTable();
   	}

   	expandTableLayout = function(){
   		if($("#commonCode-table").hasClass("span5")){
   			$("#commonCode-table").toggleClass("span5 span12");
   		}
        $("#commonCode-form").css("display","none");
       	expandCommonCodeTable();
   	}

   	massDelete = function() {
   		var recordsToDelete = [];
		$("#commonCode-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});

		var json = JSON.stringify(recordsToDelete);

		$.ajax({
    		url:'${request.contextPath}/commonCode/massdelete',
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadCommonCodeTable();
    		}
		});

   	}

});
    </g:javascript>
</head>

<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]"/></span>
    <ul class="nav pull-right">
        <li><a class="pull-right box-action" href="#"
               style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
                    class="icon-plus"></i>&nbsp;&nbsp;
        </a></li>
        <li class="separator"></li>
    </ul>
</div>
%{--<div class="box">--}%
%{--<div class="span12" id="commonCode-table">--}%
%{--<g:if test="${flash.message}">--}%
%{--<div class="message" role="status">--}%
%{--${flash.message}--}%
%{--</div>--}%
%{--</g:if>--}%

%{--<g:render template="dataTables" />--}%
%{--</div>--}%
%{--<div class="span7" id="commonCode-form" style="display: none;"></div>--}%
%{--</div>--}%
<div class="box">
<div id="commonCode-table" class="span12">
<g:render template="../menu/maxLineDisplay"/>
<script type="text/javascript">
    var shrinkCommonCodeTable;
    var reloadCommonCodeTable;
    jQuery(function () {

        var oTable = $('#commonCodeDatatables_datatable').dataTable({
            "bScrollCollapse": true,
            "bAutoWidth" : true,
            "bPaginate" : true,
            "sDom":"<'row-fluid'r>t<'row-fluid'<'span6'i><p>>",
            bFilter:true,
            "bStateSave":false,
            'sPaginationType':'bootstrap',
            "fnInitComplete":function () {
            	var dataTables_wrapper_height = $('.dataTables_wrapper').height();
				 dataTables_wrapper_height += 50;
				 $('.dataTables_wrapper').height(dataTables_wrapper_height);
            },
            "fnRowCallback":function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            },
            "fnServerParams": function ( aoData ) {
        		aoData.push( {"name": "sFilter_1", "value": $('#operator_commoncode li.active').attr('name')} );
            	aoData.push( {"name": "sFilter_2", "value": $('#operator_commonname li.active').attr('name')} );
            	aoData.push( {"name": "sFilter_3", "value": $('#operator_commondesc li.active').attr('name')} );
            	aoData.push( {"name": "sFilter_4", "value": $('#operator_commonusage li.active').attr('name')} );
            	aoData.push( {"name": "sFilter_5", "value": $('#operator_codetype li.active').attr('name')} );
            	aoData.push( {"name": "sFilter_6", "value": $('#operator_parentcode li.active').attr('name')} );
        	},
            "bSort":true,
            "bProcessing":true,
            "bServerSide":true,
            "iDisplayLength":maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
            "sAjaxSource":"${request.getContextPath()}/commonCode/getList?codetype=${params.codetype}",
            "aoColumnDefs":[
                {   "sName":"commoncode",
                    "aTargets":[0],
                    "bSearchable":false,
                    "mRender":function (data, type, row) {
                        return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row[0]+'" title="Select this"><input type="hidden" value="'+row[0]+'">&nbsp;&nbsp;<a href="#" onclick="show('+"'"+row[0]+"'"+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+"'"+row[0]+"'"+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
                    },
                    "sWidth":"",
                    "bSortable":true,
                  	"sWidth":'100%',
                    "sClass":'',
                    "bVisible":false
                }  ,
                {   "sName":"commoncode",
                    "aTargets":[1],
                    "mRender":function (data, type, row) {
                        return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row[0]+'" title="Select this"><input type="hidden" value="'+row[0]+'">&nbsp;&nbsp;<a href="#" onclick="show('+"'"+row[0]+"'"+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+"'"+row[0]+"'"+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="showChildren('+"'"+row[0]+"'"+');">&nbsp;&nbsp;Show Children&nbsp;&nbsp;</a>';
                    },
                    "bSearchable":true,
                    "sWidth":"",
                    "bSortable":true,
                    "sClass":'',
                    "bVisible":true
                }  ,
                {   "sName":"commonname",
                    "aTargets":[2],
                    "bSearchable":true,
                    "sWidth":"",
                    "bSortable":true,
                    "sClass":'',
                    "bVisible":true
                }  ,
                {   "sName":"commondesc",
                    "aTargets":[3],
                    "bSearchable":true,
                    "sWidth":"",
                    "bSortable":true,
                    "sClass":'',
                    "bVisible":true
                }  ,
                {   "sName":"commonusage",
                    "aTargets":[4],
                    "bSearchable":true,
                    "sWidth":"",
                    "bSortable":true,
                    "sClass":'',
                    "bVisible":true
                }  ,
                {   "sName":"codetype",
                    "aTargets":[5],
                    "bSearchable":true,
                    "sWidth":"",
                    "bSortable":true,
                    "sClass":'',
                    "bVisible":true
                }  ,
                {   "sName":"parentcode",
                    "aTargets":[6],
                    "bSearchable":true,
                    "sWidth":"",
                    "bSortable":true,
                    "sClass":'',
                    "bVisible":true
                }
            ]
        });

        $("#commonCodeDatatables-filters tbody")
                .append("&lt;tr&gt;&lt;td align=\"center\"&gt;app.commonCode.commonCode.label&amp;nbsp;&amp;nbsp;&lt;/td&gt;&lt;td align=\"center\" id=\"filter_commoncode\"&gt; &lt;input type=\"text\" name=\"search_commoncode\" class=\"search_init\" size=\"10\"/&gt;&lt;/td&gt;&lt;/tr&gt;")
                .append("&lt;tr&gt;&lt;td align=\"center\"&gt;app.commonCode.commonname.label&amp;nbsp;&amp;nbsp;&lt;/td&gt;&lt;td align=\"center\" id=\"filter_commonname\"&gt; &lt;input type=\"text\" name=\"search_commonname\" class=\"search_init\" size=\"10\"/&gt;&lt;/td&gt;&lt;/tr&gt;")
                .append("&lt;tr&gt;&lt;td align=\"center\"&gt;app.commonCode.commondesc.label&amp;nbsp;&amp;nbsp;&lt;/td&gt;&lt;td align=\"center\" id=\"filter_commondesc\"&gt; &lt;input type=\"text\" name=\"search_commondesc\" class=\"search_init\" size=\"10\"/&gt;&lt;/td&gt;&lt;/tr&gt;")
                .append("&lt;tr&gt;&lt;td align=\"center\"&gt;app.commonCode.commonusage.label&amp;nbsp;&amp;nbsp;&lt;/td&gt;&lt;td align=\"center\" id=\"filter_commonusage\"&gt; &lt;input type=\"text\" name=\"search_commonusage\" class=\"search_init\" size=\"10\"/&gt;&lt;/td&gt;&lt;/tr&gt;")
                .append("&lt;tr&gt;&lt;td align=\"center\"&gt;app.commonCode.codetype.label&amp;nbsp;&amp;nbsp;&lt;/td&gt;&lt;td align=\"center\" id=\"filter_codetype\"&gt; &lt;input type=\"text\" name=\"search_codetype\" class=\"search_init\" size=\"10\"/&gt;&lt;/td&gt;&lt;/tr&gt;")
                .append("&lt;tr&gt;&lt;td align=\"center\"&gt;app.commonCode.parentcode.label&amp;nbsp;&amp;nbsp;&lt;/td&gt;&lt;td align=\"center\" id=\"filter_parentcode\"&gt; &lt;input type=\"text\" name=\"search_parentcode\" class=\"search_init\" size=\"10\"/&gt;&lt;/td&gt;&lt;/tr&gt;")
        ;


        $("#filter_commoncode input").bind('keypress', function (e) {
            /* Filter on the column (the index) of this element */
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) { //Enter keycode
                oTable.fnFilter(this.value, 1);
                e.stopPropagation();
            }
        });

        $('table .dropdown-menu li').click(function(){
             $(this).addClass('active').siblings().removeClass('active');
             $(this).closest('span').toggleClass('open');
         });

        $("#filter_commonname input").bind('keypress', function (e) {
            /* Filter on the column (the index) of this element */
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) { //Enter keycode
                oTable.fnFilter(this.value, 2);
                e.stopPropagation();
            }
        });


        $("#filter_commondesc input").bind('keypress', function (e) {
            /* Filter on the column (the index) of this element */
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) { //Enter keycode
                oTable.fnFilter(this.value, 3);
                e.stopPropagation();
            }
        });

        $("#filter_commonusage input").bind('keypress', function (e) {
            /* Filter on the column (the index) of this element */
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) { //Enter keycode
                oTable.fnFilter(this.value, 4);
                e.stopPropagation();
            }
        });


        $("#filter_codetype input").bind('keypress', function (e) {
            /* Filter on the column (the index) of this element */
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) { //Enter keycode
                oTable.fnFilter(this.value, 5);
                e.stopPropagation();
            }
        });


        $("#filter_parentcode input").bind('keypress', function (e) {
            /* Filter on the column (the index) of this element */
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) { //Enter keycode
                oTable.fnFilter(this.value, 6);
                e.stopPropagation();
            }
        });


        $("#filter_createdby input").bind('keypress', function (e) {
            /* Filter on the column (the index) of this element */
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) { //Enter keycode
                oTable.fnFilter(this.value, 7);
                e.stopPropagation();
            }
        });


        $("#filter_createddate input").bind('keypress', function (e) {
            /* Filter on the column (the index) of this element */
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) { //Enter keycode
                oTable.fnFilter(this.value, 8);
                e.stopPropagation();
            }
        });


        $("#filter_createdhost input").bind('keypress', function (e) {
            /* Filter on the column (the index) of this element */
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) { //Enter keycode
                oTable.fnFilter(this.value, 9);
                e.stopPropagation();
            }
        });


        $("#filter_lasteditby input").bind('keypress', function (e) {
            /* Filter on the column (the index) of this element */
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) { //Enter keycode
                oTable.fnFilter(this.value, 10);
                e.stopPropagation();
            }
        });

        $("#filter_lasteditdate input").bind('keypress', function (e) {
            /* Filter on the column (the index) of this element */
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) { //Enter keycode
                oTable.fnFilter(this.value, 11);
                e.stopPropagation();
            }
        });


        $("#filter_lastedithost input").bind('keypress', function (e) {
            /* Filter on the column (the index) of this element */
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) { //Enter keycode
                oTable.fnFilter(this.value, 12);
                e.stopPropagation();
            }
        });


        $("#filter_issyncronize input").bind('keypress', function (e) {
            /* Filter on the column (the index) of this element */
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) { //Enter keycode
                oTable.fnFilter(this.value, 13);
                e.stopPropagation();
            }
        });


        shrinkCommonCodeTable = function () {
            var oTable = $('#commonCodeDatatables_datatable').dataTable();
            oTable.fnSetColumnVis( 2, false );
            oTable.fnSetColumnVis( 3, false );
            oTable.fnSetColumnVis( 4, false );
            oTable.fnSetColumnVis( 5, false );
            oTable.fnSetColumnVis( 6, false );
        }

        expandCommonCodeTable = function () {
            var oTable = $('#commonCodeDatatables_datatable').dataTable();
            oTable.fnSetColumnVis( 2, true );
            oTable.fnSetColumnVis( 3, true );
            oTable.fnSetColumnVis( 4, true );
            oTable.fnSetColumnVis( 5, true );
            oTable.fnSetColumnVis( 6, true );
        }

        reloadCommonCodeTable = function () {
            var oTable = $('#commonCodeDatatables_datatable').dataTable();
            oTable.fnReloadAjax();
        }

        $('#commonCodeDatatables_datatable th .select-all').click(function (e) {
            var tc = $('#commonCodeDatatables_datatable tbody .row-select').attr('checked', this.checked);

            if (this.checked)
                tc.parent().parent().addClass('row_selected');
            else
                tc.parent().parent().removeClass('row_selected');
            e.stopPropagation();
        });

        $('#commonCodeDatatables_datatable tbody tr .row-select').live('click', function (e) {
            if (this.checked)
                $(this).parent().parent().addClass('row_selected');
            else
                $(this).parent().parent().removeClass('row_selected');
            e.stopPropagation();
        });

        $('#commonCodeDatatables_datatable tbody tr a').live('click', function (e) {
            e.stopPropagation();
        });

        $('#commonCodeDatatables_datatable tbody td').live('click', function (e) {
            if (e.target.tagName.toUpperCase() != "INPUT") {
                var $tc = $(this).parent().find('input:checkbox'),
                        tv = $tc.attr('checked');
                $tc.attr('checked', !tv);
                $(this).parent().toggleClass('row_selected');
            }
        });

        $(".search_init").click(function (e) {
            e.stopPropagation();
        });

        $(".dropdown-menu").click(function (e) {
            e.stopPropagation();
        });
    });
    
   

</script>

<table id="commonCodeDatatables_datatable" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover" width="100%">
    <thead>
    <tr>
        <th>
            <g:message code="app.commonCode.commoncode.label" default="Common&nbsp;Code"/>
        </th>
        <th>
            <g:message code="app.commonCode.commoncode.label" default="Common&nbsp;Code"/>
            <div id="filter_commoncode" style="margin-top: 38px;">
                <input type="text" name="search_commoncode"
                       class="search_init" />
                <span class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"> <i class="icon-filter"></i>	</a>
                    <ul id="operator_commoncode" class="dropdown-menu" role="menu"
                        aria-labelledby="dLabel">
                        <li name="beginwith"><a href="#">Begins with</a></li>
                        <li name="contains" ><a href="#">Contains</a></li>
                        <li name="endswith"><a href="#">Ends with</a></li>
                        <li name="equals" class="active"><a href="#">Equals</a></li>
                        <li name="doesntequal"><a href="#">Doesn't equal</a></li>
                        <li name="islessthan"><a href="#">Is less than</a></li>
                        <li name="islessthanorequalto"><a href="#">Is less than or equal to</a></li>
                        <li name="isgreaterthan"><a href="#">Is greater than</a></li>
                        <li name="isgreaterthanorequalto"><a href="#">Is greater than or equal to</a></li>
                    </ul>
                </span>
            </div>
        </th>
        <th>
            <g:message code="app.commonCode.commonname.label" default="Common&nbsp;Code&nbsp;Name"/>
            <div id="filter_commonname" style="margin-top: 38px;">
                <input type="text" name="search_commonname"
                       class="search_init" />
                <span class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"> <i class="icon-filter"></i>	</a>
                    <ul id="operator_commonname" class="dropdown-menu" role="menu"
                        aria-labelledby="dLabel">
                        <li name="beginwith"><a href="#">Begins with</a></li>
                        <li name="contains" ><a href="#">Contains</a></li>
                        <li name="endswith"><a href="#">Ends with</a></li>
                        <li name="equals" class="active"><a href="#">Equals</a></li>
                        <li name="doesntequal"><a href="#">Doesn't equal</a></li>
                        <li name="islessthan"><a href="#">Is less than</a></li>
                        <li name="islessthanorequalto"><a href="#">Is less than or equal to</a></li>
                        <li name="isgreaterthan"><a href="#">Is greater than</a></li>
                        <li name="isgreaterthanorequalto"><a href="#">Is greater than or equal to</a></li>
                    </ul>
                </span>
            </div>
        </th>
        <th>
            <g:message code="app.commonCode.commondesc.label" default="Common&nbsp;Code&nbsp;Description"/>
            <div id="filter_commondesc" style="margin-top: 38px;">
                <input type="text" name="search_commondesc"
                       class="search_init" />
                <span class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"> <i class="icon-filter"></i>	</a>
                    <ul id="operator_commondesc" class="dropdown-menu" role="menu"
                        aria-labelledby="dLabel">
                        <li name="beginwith"><a href="#">Begins with</a></li>
                        <li name="contains" ><a href="#">Contains</a></li>
                        <li name="endswith"><a href="#">Ends with</a></li>
                        <li name="equals" class="active"><a href="#">Equals</a></li>
                        <li name="doesntequal"><a href="#">Doesn't equal</a></li>
                        <li name="islessthan"><a href="#">Is less than</a></li>
                        <li name="islessthanorequalto"><a href="#">Is less than or equal to</a></li>
                        <li name="isgreaterthan"><a href="#">Is greater than</a></li>
                        <li name="isgreaterthanorequalto"><a href="#">Is greater than or equal to</a></li>
                    </ul>
                </span>
            </div>
        </th>
        <th>
            <g:message code="app.commonCode.commonusage.label" default="Common&nbsp;Usage"/>
            <div id="filter_commonusage" style="margin-top: 38px;">
                <input type="text" name="search_commonusage"
                       class="search_init" />
                <span class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"> <i class="icon-filter"></i>	</a>
                    <ul id="operator_commonusage" class="dropdown-menu" role="menu"
                        aria-labelledby="dLabel">
                        <li name="beginwith"><a href="#">Begins with</a></li>
                        <li name="contains" ><a href="#">Contains</a></li>
                        <li name="endswith"><a href="#">Ends with</a></li>
                        <li name="equals" class="active"><a href="#">Equals</a></li>
                        <li name="doesntequal"><a href="#">Doesn't equal</a></li>
                        <li name="islessthan"><a href="#">Is less than</a></li>
                        <li name="islessthanorequalto"><a href="#">Is less than or equal to</a></li>
                        <li name="isgreaterthan"><a href="#">Is greater than</a></li>
                        <li name="isgreaterthanorequalto"><a href="#">Is greater than or equal to</a></li>
                    </ul>
                </span>
            </div>
        </th>
        <th>
            <g:message code="app.commonCode.codetype.label" default="Code&nbsp;Type"/>
            <div id="filter_codetype" style="margin-top: 38px;">
                <input type="text" name="search_codetype"
                       class="search_init" />
                <span class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"> <i class="icon-filter"></i>	</a>
                    <ul id="operator_codetype" class="dropdown-menu" role="menu"
                        aria-labelledby="dLabel">
                        <li name="beginwith"><a href="#">Begins with</a></li>
                        <li name="contains" ><a href="#">Contains</a></li>
                        <li name="endswith"><a href="#">Ends with</a></li>
                        <li name="equals" class="active"><a href="#">Equals</a></li>
                        <li name="doesntequal"><a href="#">Doesn't equal</a></li>
                        <li name="islessthan"><a href="#">Is less than</a></li>
                        <li name="islessthanorequalto"><a href="#">Is less than or equal to</a></li>
                        <li name="isgreaterthan"><a href="#">Is greater than</a></li>
                        <li name="isgreaterthanorequalto"><a href="#">Is greater than or equal to</a></li>
                    </ul>
                </span>
            </div>
        </th>
        <th>
            <g:message code="app.commonCode.parentcode.label" default="Parent&nbsp;Code"/>
            <div id="filter_parentcode" style="margin-top: 38px;">
                <input type="text" name="search_parentcode"
                       class="search_init" />
                <span class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"> <i class="icon-filter"></i>	</a>
                    <ul id="operator_parentcode" class="dropdown-menu" role="menu"
                        aria-labelledby="dLabel">
                        <li name="beginwith"><a href="#">Begins with</a></li>
                        <li name="contains" ><a href="#">Contains</a></li>
                        <li name="endswith"><a href="#">Ends with</a></li>
                        <li name="equals" class="active"><a href="#">Equals</a></li>
                        <li name="doesntequal"><a href="#">Doesn't equal</a></li>
                        <li name="islessthan"><a href="#">Is less than</a></li>
                        <li name="islessthanorequalto"><a href="#">Is less than or equal to</a></li>
                        <li name="isgreaterthan"><a href="#">Is greater than</a></li>
                        <li name="isgreaterthanorequalto"><a href="#">Is greater than or equal to</a></li>
                    </ul>
                </span>
            </div>
        </th>
    </tr>
    </thead>
</table>
</div>
<div class="span7" id="commonCode-form" style="display: none;"></div>
</div>
</body>
</html>
