<%@ page import="com.kombos.administrasi.CompanyDealer" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="baseapplayout" />
<g:javascript disappointmentGrition="head">
	$(function(){
	    $('#detail').attr('disabled',true);
        $('#filter_periode2').hide();
        $('#filter_periode').show();
        $('#bulan1').change(function(){
        $('#bulan2').val($('#bulan1').val());
        });
        $('#bulan2').change(function(){
            if(parseInt($('#bulan1').val()) > parseInt($('#bulan2').val())){
                $('#bulan2').val($('#bulan1').val());
            }
        });
        $('#namaReport').change(function(){
             if($('#namaReport').val()=="01" || $('#namaReport').val()=="06" || $('#namaReport').val()=="11" || $('#namaReport').val()=="14" || $('#namaReport').val()=="15"){
                $('#detail').prop('disabled',true);
                $('#detail').attr('checked',false);
                $('#filter_periode').hide();
                $('#filter_periode2').show();
             }else if($('#namaReport').val()=="10"){
                $('#detail').prop('disabled',false);
                $('#detail').attr('checked',false);
                $('#filter_periode').show();
                $('#filter_periode2').hide();
             }else if($('#namaReport').val()=="12" || $('#namaReport').val()=="13"){
                      $('#filter_periode2').show();
                      $('#filter_periode').hide();
                      $('#detail').prop('disabled',false);
                      $('#detail').attr('checked',false);
             }else{
                     $('#detail').prop('disabled',false);
                     $('#detail').attr('checked',false);
                     $('#filter_periode').show();
                     $('#filter_periode2').hide();
             }
//                 $('#detail').prop('disabled',true);
//                 $('#detail').attr('checked',false);//$('#detail').show();
             });

	    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    });
    var checkin = $('#search_tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
                checkin.hide();
                $('#search_tanggal2')[0].focus();
            }).data('datepicker');

    var checkout = $('#search_tanggal2').datepicker({
        onRender: function(date) {
            return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
                checkout.hide();
    }).data('datepicker');

     previewData = function(){
            var namaReport = $('#namaReport').val();
            var workshop = $('#workshop').val();
            var search_tanggal = $('#search_tanggal').val();
            var search_tanggal2 = $('#search_tanggal2').val();
            var bulan1 = $('#bulan1').val();
            var bulan2= $('#bulan2').val();
            var tahun = $('#tahun').val();
            var detail = "";
            if(document.getElementById("detail").checked){detail = "1"}else{detail = "0"}
            if(namaReport == "" || namaReport == null){
                alert('Harap Isi Data Dengan Lengkap');
                return;
            }
           window.location = "${request.contextPath}/krMrs/previewData?namaReport="+namaReport+"&workshop="+workshop+"&bulan1="+bulan1+"&bulan2="+bulan2+"&tahun="+tahun+"&tanggal="+search_tanggal+"&tanggal2="+search_tanggal2+"&detail="+detail;
    }
</g:javascript>

</head>
<body>
<div class="navbar box-header no-border">
    Report – MRS
</div><br>
<div class="box">
    <div class="span12" id="appointmentGr-table">
        <div id="form-appointmentGr" class="form-horizontal">
            <fieldset>
                <div class="control-group">
                    <label class="control-label" for="workshop" style="text-align: left;">
                        Workshop
                    </label>
                    <div id="filter_wo" class="controls">
                        <g:select name="workshop" id="workshop" from="${CompanyDealer.createCriteria().list{eq('staDel','0');order('m011NamaWorkshop')}}" optionKey="id" optionValue="m011NamaWorkshop" style="width: 44%" value="${cd}" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="filter_nama" style="text-align: left;">
                        Nama Report
                    </label>
                    <div id="filter_nama" class="controls">
                        <select name="namaReport" id="namaReport" size="17" style="width: 44%; font-size: 12px;">
                            <option value="01">01. KPI Process</option>
                            <option value="02">02. KPI Direct Mail</option>
                            <option value="03">03. KPI SMS</option>
                            <option value="04">04. KPI Call</option>
                            <option value="05">05. KPI Appointment</option>
                            <option value="06">06. KPI Result</option>
                            <option value="07">07. Appointment Contribution</option>
                            <option value="08">08. SBE Contribution</option>
                            <option value="09">09. Unit Entry Contribution</option>
                            <option value="10">10. KPI Process Detail</option>
                            <option value="11">11. KPI Total</option>
                            <option value="12">12. SB Retention First Service</option>
                            <option value="13">13. SB Retention Period</option>
                            <option value="14">14. Customize Report</option>
                            <option value="15">15. SERVICE & FILL RATE</option>

                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="filter_periode" style="text-align: left;">
                        Periode
                    </label>
                    <div id="filter_periode" class="controls">
                        <ba:datePicker id="search_tanggal" name="search_tanggal" precision="day" format="dd-MM-yyyy"  value="${new Date()}" />
                         &nbsp;&nbsp;s.d.&nbsp;&nbsp;
                        <ba:datePicker id="search_tanggal2" name="search_tanggal2" precision="day" format="dd-MM-yyyy"  value="${new Date()+1}"  />
                    </div>
                    <div id="filter_periode2" class="controls">
                        %{
                            def listBulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
                        }%
                        <select name="bulan1" id="bulan1" style="width: 170px">
                            %{
                                for(int i=0;i<12;i++){
                                    out.print('<option value="'+i+'">'+listBulan.get(i)+'</option>');
                                }
                            }%
                        </select> &nbsp; - &nbsp;
                        <select name="bulan2" id="bulan2" style="width: 170px">
                            %{
                                for(int i=0;i<12;i++){
                                    out.print('<option value="'+i+'">'+listBulan.get(i)+'</option>');
                                }
                            }%
                        </select> &nbsp;
                       <select name="tahun" id="tahun" style="width: 106px">
                           %{
                            for(def a = (new Date().format("yyyy")).toInteger();a >= 1940 ;a--){
                                out.print('<option value="'+a+'">'+a+'</option>');
                            }
                           }%

                       </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="t951Tanggal" style="text-align: left;">
                        Report Detail
                    </label>
                    <div id="filter_detail" class="controls">
                        <input type="checkbox" name="detail" id="detail" value="1" /> Ya
                    </div>
                </div>
            </fieldset>
        </div>
           <g:field type="button" onclick="previewData()" class="btn btn-primary create" name="preview"  value="${message(code: 'default.preview.label', default: 'Preview')}" />
           <g:field type="button" onclick="window.location.replace('#/home')" class="btn btn-cancel cancel" name="cancel"  value="${message(code: 'default.button.close.label', default: 'Close')}" />
    </div>
</div>
</body>
</html>
