

<%@ page import="com.kombos.maintable.Reminder" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'reminder.label', default: 'Reminder')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteReminder;

$(function(){ 
	deleteReminder=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/reminder/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadReminderTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-reminder" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="reminder"
			class="table table-bordered table-hover">
			<tbody>

            <g:if test="${reminderInstance?.customerVehicle}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="customerVehicle-label" class="property-label"><g:message
                                code="reminder.customerVehicle.label" default="VinCode" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="customerVehicle-label">

                        <g:fieldValue field="t103VinCode" bean="${reminderInstance?.customerVehicle}" />
                    </span></td>

                </tr>
            </g:if>


            <g:if test="${reminderInstance?.m201ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m201ID-label" class="property-label"><g:message
					code="reminder.m201ID.label" default="Jenis Reminder" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m201ID-label">

							    <g:fieldValue field="m201NamaJenisReminder" bean="${reminderInstance?.m201ID}" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${reminderInstance?.m202ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m202ID-label" class="property-label"><g:message
					code="reminder.m202ID.label" default="Status Reminder" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m202ID-label">

                                <g:fieldValue field="m202NamaStatusReminder" bean="${reminderInstance?.m202ID}" />

                        </span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${reminderInstance?.operation}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="operation-label" class="property-label"><g:message
					code="reminder.operation.label" default="Operation" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="operation-label">

                            <g:fieldValue field="m053NamaOperation" bean="${reminderInstance?.operation}" />
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${reminderInstance?.t201LastKM}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t201LastKM-label" class="property-label"><g:message
					code="reminder.t201LastKM.label" default="KM" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t201LastKM-label">
						%{--<ba:editableValue
								bean="${reminderInstance}" field="t201LastKM"
								url="${request.contextPath}/Reminder/updatefield" type="text"
								title="Enter t201LastKM" onsuccess="reloadReminderTable();" />--}%
							
								<g:fieldValue bean="${reminderInstance}" field="t201LastKM"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${reminderInstance?.t201LastServiceDate}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t201LastServiceDate-label" class="property-label"><g:message
					code="reminder.t201LastServiceDate.label" default="Last Service Date" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t201LastServiceDate-label">
						%{--<ba:editableValue
								bean="${reminderInstance}" field="t201LastServiceDate"
								url="${request.contextPath}/Reminder/updatefield" type="text"
								title="Enter t201LastServiceDate" onsuccess="reloadReminderTable();" />--}%
							
								<g:formatDate date="${reminderInstance?.t201LastServiceDate}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${reminderInstance?.t201TglJatuhTempo}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t201TglJatuhTempo-label" class="property-label"><g:message
					code="reminder.t201TglJatuhTempo.label" default="Tgl. Jatuh Tempo" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t201TglJatuhTempo-label">
						%{--<ba:editableValue
								bean="${reminderInstance}" field="t201TglJatuhTempo"
								url="${request.contextPath}/Reminder/updatefield" type="text"
								title="Enter t201TglJatuhTempo" onsuccess="reloadReminderTable();" />--}%
							
								<g:formatDate date="${reminderInstance?.t201TglJatuhTempo}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${reminderInstance?.t201TglDM}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t201TglDM-label" class="property-label"><g:message
					code="reminder.t201TglDM.label" default="Tgl. DM" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t201TglDM-label">
						%{--<ba:editableValue
								bean="${reminderInstance}" field="t201TglDM"
								url="${request.contextPath}/Reminder/updatefield" type="text"
								title="Enter t201TglDM" onsuccess="reloadReminderTable();" />--}%
							
								<g:formatDate date="${reminderInstance?.t201TglDM}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${reminderInstance?.t201TglSMS}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t201TglSMS-label" class="property-label"><g:message
					code="reminder.t201TglSMS.label" default="Tgl. SMS" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t201TglSMS-label">
						%{--<ba:editableValue
								bean="${reminderInstance}" field="t201TglSMS"
								url="${request.contextPath}/Reminder/updatefield" type="text"
								title="Enter t201TglSMS" onsuccess="reloadReminderTable();" />--}%
							
								<g:formatDate date="${reminderInstance?.t201TglSMS}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${reminderInstance?.t201TglEmail}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t201TglEmail-label" class="property-label"><g:message
					code="reminder.t201TglEmail.label" default="Tgl. Email" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t201TglEmail-label">
						%{--<ba:editableValue
								bean="${reminderInstance}" field="t201TglEmail"
								url="${request.contextPath}/Reminder/updatefield" type="text"
								title="Enter t201TglEmail" onsuccess="reloadReminderTable();" />--}%
							
								<g:formatDate date="${reminderInstance?.t201TglEmail}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${reminderInstance?.t201TglCall}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t201TglCall-label" class="property-label"><g:message
					code="reminder.t201TglCall.label" default="Tgl. Call" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="t201TglCall-label">
						%{--<ba:editableValue
								bean="${reminderInstance}" field="t201TglCall"
								url="${request.contextPath}/Reminder/updatefield" type="text"
								title="Enter t201TglCall" onsuccess="reloadReminderTable();" />--}%

								<g:formatDate date="${reminderInstance?.t201TglCall}" />

						</span></td>

				</tr>
				</g:if>

				<g:if test="${reminderInstance?.t201Ket}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t201Ket-label" class="property-label"><g:message
					code="reminder.t201Ket.label" default="Keterangan" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="t201Ket-label">
						%{--<ba:editableValue
								bean="${reminderInstance}" field="t201Ket"
								url="${request.contextPath}/Reminder/updatefield" type="text"
								title="Enter t201Ket" onsuccess="reloadReminderTable();" />--}%

								<g:fieldValue bean="${reminderInstance}" field="t201Ket"/>

						</span></td>

				</tr>
				</g:if>

            <g:if test="${reminderInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="reminder.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${reminderInstance}" field="dateCreated"
                                url="${request.contextPath}/Reminder/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadReminderTable();" />--}%

                        <g:formatDate date="${reminderInstance?.dateCreated}" />

                    </span></td>

                </tr>
            </g:if>


            <g:if test="${reminderInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="reminder.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${reminderInstance}" field="createdBy"
								url="${request.contextPath}/Reminder/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadReminderTable();" />--}%
							
								<g:fieldValue bean="${reminderInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${reminderInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="reminder.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${reminderInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/Reminder/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadReminderTable();" />--}%

                        <g:fieldValue bean="${reminderInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${reminderInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="reminder.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${reminderInstance}" field="lastUpdated"
                                url="${request.contextPath}/Reminder/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadReminderTable();" />--}%

                        <g:formatDate date="${reminderInstance?.lastUpdated}" />

                    </span></td>

                </tr>
            </g:if>

				<g:if test="${reminderInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="reminder.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${reminderInstance}" field="updatedBy"
								url="${request.contextPath}/Reminder/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadReminderTable();" />--}%
							
								<g:fieldValue bean="${reminderInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			

			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.close.label" default="Kembali" /></a>
				%{--<g:remoteLink class="btn btn-primary edit" action="edit"--}%
					%{--id="${reminderInstance?.id}"--}%
					%{--update="[success:'reminder-form',failure:'reminder-form']"--}%
					%{--on404="alert('not found');">--}%
					%{--<g:message code="default.button.edit.label" default="Edit" />--}%
				%{--</g:remoteLink>--}%
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteReminder('${reminderInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
