
<%@ page import="com.kombos.maintable.Reminder" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="reminder_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reminder.label" default="M201 ID" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div>Nomor Polisi</div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div>Customer</div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div>Alamat</div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div>Telepon</div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div>HP</div>
            </th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reminder.t201LastKM.label" default="T201 Last KM" /></div>
			</th>


            <th style="border-bottom: none;padding: 5px;">
                <div>Next KM</div>
            </th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reminder.t201TglJatuhTempo.label" default="T201 Tgl Jatuh Tempo" /></div>
			</th>


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="reminder.t201TglDM.label" default="T201 Tgl DM" /></div>--}%
			%{--</th>--}%


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reminder.t201TglSMS.label" default="T201 Tgl SMS" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reminder.t201TglEmail.label" default="T201 Tgl Email" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px">
				<div><g:message code="reminder.staKirim.label" default="STS Kirim" /></div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var reminderTable;
var reloadReminderTable;
$(function(){
	
	reloadReminderTable = function() {
		reminderTable.fnDraw();
	}
    var recordsReminderPerPage = [];//new Array();
    var anReminderSelected;
    var jmlRecReminderPerPage=0;
    var id;


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	reminderTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	reminderTable = $('#reminder_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsReminder = $("#reminder_datatables tbody .row-select");
            var jmlReminderCek = 0;
            var nRow;
            var idRec;
            rsReminder.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsReminderPerPage[idRec]=="1"){
                    jmlReminderCek = jmlReminderCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsReminderPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecReminderPerPage = rsReminder.length;
            if(jmlReminderCek==jmlRecReminderPerPage && jmlRecReminderPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
        "fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m201ID",
	"mDataProp": "m201ID",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "nopol",
	"mDataProp": "nopol",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "nama",
	"mDataProp": "nama",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "alamat",
	"mDataProp": "alamat",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"400px",
	"bVisible": true
}

,

{
	"sName": "telp",
	"mDataProp": "telp",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "hp",
	"mDataProp": "hp",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t201LastKM",
	"mDataProp": "t201LastKM",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "nextKM",
	"mDataProp": "nextKM",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t201TglJatuhTempo",
	"mDataProp": "t201TglJatuhTempo",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

// {
// 	"sName": "t201TglDM",
// 	"mDataProp": "t201TglDM",
// 	"aTargets": [9],
// 	"bSearchable": false,
// 	"bSortable": false,
// 	"sWidth":"200px",
// 	"bVisible": false
// }
//
// ,

{
	"sName": "t201TglSMS",
	"mDataProp": "t201TglSMS",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t201TglEmail",
	"mDataProp": "t201TglEmail",
	"aTargets": [11],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t201STSkirim",
	"mDataProp": "t201STSkirim",
	"aTargets": [12],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {




						var searchReminder = document.getElementsByName('searchReminder');
                        var checkRetreiveValues = new Array()
                        for(var i = 0; i < searchReminder.length; i++){
                            if(searchReminder[i].checked){
                                checkRetreiveValues.push(searchReminder[i].value)
                            }
                        }
                        aoData.push(
                            {"name": 'searchReminder', "value": checkRetreiveValues}
                        );


						var checkKm = document.getElementById('checkKm').checked;
						if(checkKm){
							aoData.push(
									{"name": 'checkKm', "value": checkKm}
							);
						}
	
						var checkJatuhTempo = document.getElementById('checkJatuhTempo').checked;
						if(checkJatuhTempo){
							aoData.push(
									{"name": 'checkJatuhTempo', "value": checkJatuhTempo}
							);
						}
	
						var checkNextKm = document.getElementById('checkNextKm').checked;
						if(checkNextKm){
							aoData.push(
									{"name": 'checkNextKm', "value": checkNextKm}
							);
						}
	
						// var checkDM = document.getElementById('checkDM').checked;
						// if(checkDM){
						// 	aoData.push(
						// 			{"name": 'checkDM', "value": checkDM}
						// 	);
						// }
	
						var checkCall = document.getElementById('checkCall').checked;
						if(checkCall){
							aoData.push(
									{"name": 'checkCall', "value": checkCall}
							);
						}

						var checkSMS = document.getElementById('checkSMS').checked;
						if(checkSMS){
							aoData.push(
									{"name": 'checkSMS', "value": checkSMS}
							);
						}

						var checkEmail = document.getElementById('checkEmail').checked;
						if(checkEmail){
							aoData.push(
									{"name": 'checkEmail', "value": checkEmail}
							);
						}

                        if(document.getElementById('checkNextKm').checked == true){
                            var nextKm = $('#nextKm').val();
                            if(nextKm){
                                aoData.push(
                                        {"name": 'nextKm', "value": nextKm}
                                );
                            }

                            var oprNextKm = $('#oprNextKm').val();
                            if(oprNextKm){
                                aoData.push(
                                        {"name": 'oprNextKm', "value": oprNextKm}
                                );
                            }
                        }

                        if(document.getElementById('checkKm').checked == true){
                            var oprKm = $('#oprKm').val();
                            if(oprKm){
                                aoData.push(
                                        {"name": 'oprKm', "value": oprKm}
                                );
                            }


                            var km = $('#km').val();
                            if(km){
                                aoData.push(
                                        {"name": 'km', "value": km}
                                );
                            }
						}


                        if(document.getElementById('checkRetreive').checked == true){
                            var startRetreive = $('#startRetreive').val();
                            var startRetreiveDay = $('#startRetreive_day').val();
                            var startRetreiveMonth = $('#startRetreive_month').val();
                            var startRetreiveYear = $('#startRetreive_year').val();

                            if(startRetreive){
                                aoData.push(
                                        {"name": 'startRetreive', "value": "date.struct"},
                                        {"name": 'startRetreive_dp', "value": startRetreive},
                                        {"name": 'startRetreive_day', "value": startRetreiveDay},
                                        {"name": 'startRetreive_month', "value": startRetreiveMonth},
                                        {"name": 'startRetreive_year', "value": startRetreiveYear}
                                );
                            }

                            var endRetreive = $('#endRetreive').val();
                            var endRetreiveDay = $('#endRetreive_day').val();
                            var endRetreiveMonth = $('#endRetreive_month').val();
                            var endRetreiveYear = $('#endRetreive_year').val();

                            if(endRetreive){
                                aoData.push(
                                        {"name": 'endRetreive', "value": "date.struct"},
                                        {"name": 'endRetreive_dp', "value": endRetreive},
                                        {"name": 'endRetreive_day', "value": endRetreiveDay},
                                        {"name": 'endRetreive_month', "value": endRetreiveMonth},
                                        {"name": 'endRetreive_year', "value": endRetreiveYear}
                                );
                            }
						}


                        if(document.getElementById('checkJatuhTempo').checked == true){
                            var startJatuhTempo = $('#startJatuhTempo').val();
                            var startJatuhTempoDay = $('#startJatuhTempo_day').val();
                            var startJatuhTempoMonth = $('#startJatuhTempo_month').val();
                            var startJatuhTempoYear = $('#startJatuhTempo_year').val();

                            if(startJatuhTempo){
                                aoData.push(
                                        {"name": 'startJatuhTempo', "value": "date.struct"},
                                        {"name": 'startJatuhTempo_dp', "value": startJatuhTempo},
                                        {"name": 'startJatuhTempo_day', "value": startJatuhTempoDay},
                                        {"name": 'startJatuhTempo_month', "value": startJatuhTempoMonth},
                                        {"name": 'startJatuhTempo_year', "value": startJatuhTempoYear}
                                );
                            }

                            var endJatuhTempo = $('#endJatuhTempo').val();
                            var endJatuhTempoDay = $('#endJatuhTempo_day').val();
                            var endJatuhTempoMonth = $('#endJatuhTempo_month').val();
                            var endJatuhTempoYear = $('#endJatuhTempo_year').val();

                            if(endJatuhTempo){
                                aoData.push(
                                        {"name": 'endJatuhTempo', "value": "date.struct"},
                                        {"name": 'endJatuhTempo_dp', "value": endJatuhTempo},
                                        {"name": 'endJatuhTempo_day', "value": endJatuhTempoDay},
                                        {"name": 'endJatuhTempo_month', "value": endJatuhTempoMonth},
                                        {"name": 'endJatuhTempo_year', "value": endJatuhTempoYear}
                                );
                            }
						}

                        // if(document.getElementById('checkDM').checked == true){
                        //     var startDM = $('#startDM').val();
                        //     var startDMDay = $('#startDM_day').val();
                        //     var startDMMonth = $('#startDM_month').val();
                        //     var startDMYear = $('#startDM_year').val();
                        //
                        //     if(startDM){
                        //         aoData.push(
                        //                 {"name": 'startDM', "value": "date.struct"},
                        //                 {"name": 'startDM_dp', "value": startDM},
                        //                 {"name": 'startDM_day', "value": startDMDay},
                        //                 {"name": 'startDM_month', "value": startDMMonth},
                        //                 {"name": 'startDM_year', "value": startDMYear}
                        //         );
                        //     }

                        //     var endDM = $('#endDM').val();
                        //     var endDMDay = $('#endDM_day').val();
                        //     var endDMMonth = $('#endDM_month').val();
                        //     var endDMYear = $('#endDM_year').val();
                        //
                        //     if(endDM){
                        //         aoData.push(
                        //                 {"name": 'endDM', "value": "date.struct"},
                        //                 {"name": 'endDM_dp', "value": endDM},
                        //                 {"name": 'endDM_day', "value": endDMDay},
                        //                 {"name": 'endDM_month', "value": endDMMonth},
                        //                 {"name": 'endDM_year', "value": endDMYear}
                        //         );
                        //     }
                        // }

                        if(document.getElementById('checkCall').checked == true){
                            var startCall = $('#startCall').val();
                            var startCallDay = $('#startCall_day').val();
                            var startCallMonth = $('#startCall_month').val();
                            var startCallYear = $('#startCall_year').val();

                            if(startCall){
                                aoData.push(
                                        {"name": 'startCall', "value": "date.struct"},
                                        {"name": 'startCall_dp', "value": startCall},
                                        {"name": 'startCall_day', "value": startCallDay},
                                        {"name": 'startCall_month', "value": startCallMonth},
                                        {"name": 'startCall_year', "value": startCallYear}
                                );
                            }





                            var endCall = $('#endCall').val();
                            var endCallDay = $('#endCall_day').val();
                            var endCallMonth = $('#endCall_month').val();
                            var endCallYear = $('#endCall_year').val();

                            if(endCall){
                                aoData.push(
                                        {"name": 'endCall', "value": "date.struct"},
                                        {"name": 'endCall_dp', "value": endCall},
                                        {"name": 'endCall_day', "value": endCallDay},
                                        {"name": 'endCall_month', "value": endCallMonth},
                                        {"name": 'endCall_year', "value": endCallYear}
                                );
                            }
						}
						
						
						
						
                        if(document.getElementById('checkSMS').checked == true){
                            var startSMS = $('#startSMS').val();
                            var startSMSDay = $('#startSMS_day').val();
                            var startSMSMonth = $('#startSMS_month').val();
                            var startSMSYear = $('#startSMS_year').val();

                            if(startSMS){
                                aoData.push(
                                        {"name": 'startSMS', "value": "date.struct"},
                                        {"name": 'startSMS_dp', "value": startSMS},
                                        {"name": 'startSMS_day', "value": startSMSDay},
                                        {"name": 'startSMS_month', "value": startSMSMonth},
                                        {"name": 'startSMS_year', "value": startSMSYear}
                                );
                            }





                            var endSMS = $('#endSMS').val();
                            var endSMSDay = $('#endSMS_day').val();
                            var endSMSMonth = $('#endSMS_month').val();
                            var endSMSYear = $('#endSMS_year').val();

                            if(endSMS){
                                aoData.push(
                                        {"name": 'endSMS', "value": "date.struct"},
                                        {"name": 'endSMS_dp', "value": endSMS},
                                        {"name": 'endSMS_day', "value": endSMSDay},
                                        {"name": 'endSMS_month', "value": endSMSMonth},
                                        {"name": 'endSMS_year', "value": endSMSYear}
                                );
                            }
						
						}
						
						
                        if(document.getElementById('checkEmail').checked == true){
                            var startEmail = $('#startEmail').val();
                            var startEmailDay = $('#startEmail_day').val();
                            var startEmailMonth = $('#startEmail_month').val();
                            var startEmailYear = $('#startEmail_year').val();

                            if(startEmail){
                                aoData.push(
                                        {"name": 'startEmail', "value": "date.struct"},
                                        {"name": 'startEmail_dp', "value": startEmail},
                                        {"name": 'startEmail_day', "value": startEmailDay},
                                        {"name": 'startEmail_month', "value": startEmailMonth},
                                        {"name": 'startEmail_year', "value": startEmailYear}
                                );
                            }



                            var endEmail = $('#endEmail').val();
                            var endEmailDay = $('#endEmail_day').val();
                            var endEmailMonth = $('#endEmail_month').val();
                            var endEmailYear = $('#endEmail_year').val();

                            if(endEmail){
                                aoData.push(
                                        {"name": 'endEmail', "value": "date.struct"},
                                        {"name": 'endEmail_dp', "value": endEmail},
                                        {"name": 'endEmail_day', "value": endEmailDay},
                                        {"name": 'endEmail_month', "value": endEmailMonth},
                                        {"name": 'endEmail_year', "value": endEmailYear}
                                );
                            }
	                    }


	

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
	$('.select-all').click(function(e) {

        $("#reminder_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsReminderPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsReminderPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#reminder_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsReminderPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anReminderSelected = reminderTable.$('tr.row_selected');
            if(jmlRecReminderPerPage == anReminderSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsReminderPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
