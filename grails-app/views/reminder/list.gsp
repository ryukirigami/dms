<%@ page import="com.kombos.maintable.JenisReminder; com.kombos.maintable.Reminder" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'reminder.label', default: 'Reminder')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <r:require modules="baseapplayout"/>
    <g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 

	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
                                   onLoading="jQuery('#spinner').fadeIn(1);"
                                   onSuccess="loadForm(data, textStatus);"
                                   onComplete="jQuery('#spinner').fadeOut();"/>
        break;
    case '_DELETE_' :
        bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/reminder/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/reminder/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#reminder-form').empty();
    	$('#reminder-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#reminder-table").hasClass("span12")){
   			$("#reminder-table").toggleClass("span12 span5");
        }
        $("#reminder-form").css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
   		if($("#reminder-table").hasClass("span5")){
   			$("#reminder-table").toggleClass("span5 span12");
   		}
        $("#reminder-form").css("display","none");
   	}

   	clearSearch = function(){
        var inputs, index;

        inputs = document.getElementsByTagName('input');
        for (index = 0; index < inputs.length; ++index) {
            inputs[index].value = '';
            inputs[index].checked = false;
        }
        reloadReminderTable();
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#reminder-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/reminder/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadReminderTable();
    		}
		});
		
   	}

});
    </g:javascript>
    <style>
    input {
        /* font-size: xx-small; */
        width: 100px;
        border: 1px solid #c4c4c4;
        border-top-color: #aaa;
        -webkit-box-shadow: rgba(255, 255, 255, 0.9) 0 1px 0;
        -moz-box-shadow: rgba(255, 255, 255, 0.9) 0 1px 0;
        box-shadow: rgba(255, 255, 255, 0.9) 0 1px 0;
    }

    legend {
        display: block;
        width: 100%;
        padding: 0;
        /* margin-bottom: 20px; */
        font-size: 21px;
        line-height: 20px;
        color: #333333;
        border: 0;
        border-bottom: 1px solid #e5e5e5;
    }
    </style>

</head>

<body>

<div class="box">
    <legend style="font-size: small">Search Criteria:</legend>
    <fieldset class="form">

        <table style="width: 100%;border: 0px">
            <tr>
                <td style="vertical-align: top;padding-right: 30px">

                    <div class="box">

                        <table style="width: 100%;border: 0px">
                            <tr>
                                <td>
                                    Reminder
                                </td>
                                <td style="width: 50px"></td>
                                <td>
                                    <g:each in="${JenisReminder.listOrderByM201NamaJenisReminder()}">
                                        <input name="searchReminder" id="searchReminder_${it?.id}" type="checkbox"
                                               value="${it?.id}" checked> ${it?.m201NamaJenisReminder}
                                    </g:each>
                                </td>

                            </tr>

                            <tr>
                                <td>
                                    KM
                                </td>
                                <td style="width: 50px">&nbsp;&nbsp;&nbsp;<g:checkBox name="checkKm"/></td>
                                <td>
                                    <g:select from="${["=", "<", ">", "<=", ">=", "!="]}"
                                              style="width:50px"
                                              required="" name="oprKm"/>
                                    <g:textField name="km"/>
                                </td>

                            </tr>

                            <tr>
                                <td>
                                    Next KM
                                </td>
                                <td style="width: 50px">&nbsp;&nbsp;&nbsp;<g:checkBox name="checkNextKm"/></td>
                                <td>
                                    <g:select from="${["=", "<", ">", "<=", ">=", "!="]}"
                                              style="width:50px"
                                              required="" name="oprNextKm"/>
                                    <g:textField name="nextKm"/>
                                </td>

                            </tr>


                            <tr>
                                <td>
                                    Tanggal Call
                                </td>
                                <td style="width: 50px">&nbsp;&nbsp;&nbsp;<g:checkBox name="checkCall"/></td>
                                <td>
                                    <ba:datePicker name="startCall" precision="day" format="dd/MM/yyyy"/>
                                    s.d
                                    <ba:datePicker name="endCall" precision="day" format="dd/MM/yyyy"/>
                                </td>

                            </tr>

                        </table>

                    </div>

                </td>

                <td style="vertical-align: top;width: 50%">

                    <div class="box">

                        <table style="width: 100%;border: 0px">
                            <tr>
                                <td style="width: 15%">
                                    Tanggal Retreive
                                </td>
                                <td style="width: 50px">&nbsp;&nbsp;&nbsp;<g:checkBox name="checkRetreive"/></td>
                                <td>
                                    <ba:datePicker name="startRetreive" precision="day" format="dd/MM/yyyy"/>
                                    s.d
                                    <ba:datePicker name="endRetreive" precision="day" format="dd/MM/yyyy"/>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%">
                                    Tanggal Est. Jatuh Tempo
                                </td>
                                <td style="width: 50px">&nbsp;&nbsp;&nbsp;<g:checkBox name="checkJatuhTempo"/></td>
                                <td>
                                    <ba:datePicker name="startJatuhTempo" precision="day" format="dd/MM/yyyy"/>
                                    s.d
                                    <ba:datePicker name="endJatuhTempo" precision="day" format="dd/MM/yyyy"/>
                                </td>
                            </tr>
                            %{--<tr>--}%
                                %{--<td style="width: 15%">--}%
                                    %{--Tanggal Kirim DM--}%
                                %{--</td>--}%
                                %{--<td style="width: 50px">&nbsp;&nbsp;&nbsp;<g:checkBox name="checkDM"/></td>--}%
                                %{--<td>--}%
                                    %{--<ba:datePicker name="startDM" precision="day" format="dd/MM/yyyy"/>--}%
                                    %{--s.d--}%
                                    %{--<ba:datePicker name="endDM" precision="day" format="dd/MM/yyyy"/>--}%
                                %{--</td>--}%
                            %{--</tr>--}%
                            <tr>
                                <td style="width: 15%">
                                    Tanggal Kirim SMS
                                </td>
                                <td style="width: 50px">&nbsp;&nbsp;&nbsp;<g:checkBox name="checkSMS"/></td>
                                <td>
                                    <ba:datePicker name="startSMS" precision="day" format="dd/MM/yyyy"/>
                                    s.d
                                    <ba:datePicker name="endSMS" precision="day" format="dd/MM/yyyy"/>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%">
                                    Tanggal Kirim Email
                                </td>
                                <td style="width: 50px">&nbsp;&nbsp;&nbsp;<g:checkBox name="checkEmail"/></td>
                                <td>
                                    <ba:datePicker name="startEmail" precision="day" format="dd/MM/yyyy"/>
                                    s.d
                                    <ba:datePicker name="endEmail" precision="day" format="dd/MM/yyyy"/>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%">
                                    STS Kirim
                                </td>
                                <td style="width: 50px">&nbsp;&nbsp;&nbsp;<g:checkBox name="checkSTSkirim"/></td>
                                <td>
                                    <ba:datePicker name="startSTSkirim" precision="day" format="dd/MM/yyyy"/>
                                    s.d
                                    <ba:datePicker name="endSTSkirim" precision="day" format="dd/MM/yyyy"/>
                                </td>
                            </tr>
                        </table>
                    </div>

                </td>
            </tr>

        </table>

    </fieldset>


    <fieldset class="buttons controls">

        %{--<button class="btn btn-primary" id="buttonEdit" onclick="onEdit()">Edit</button>--}%
        <button class="btn btn-primary" id="buttonSave" onclick="reloadReminderTable();">Search</button>
        %{--<button class="btn btn-primary" id="buttonCancel" onclick="onCancel()">Cancel</button>--}%

        <button class="btn btn-primary" id="buttonClose" onclick="clearSearch()">Clear Search</button>

    </fieldset>
</div>

<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]"/></span>
    <ul class="nav pull-right">
        <li><a class="pull-right box-action" href="#"
               style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
                    class="icon-plus"></i>&nbsp;&nbsp;
        </a></li>
        <li><a class="pull-right box-action" href="#"
               style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
                    class="icon-remove"></i>&nbsp;&nbsp;
        </a></li>
        <li class="separator"></li>
    </ul>
</div>





<div class="box">
    <div class="span12" id="reminder-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>

        <g:render template="dataTables"/>
    </div>

    <div class="span7" id="reminder-form" style="display: none;"></div>
</div>
<fieldset class="buttons controls" style="text-align: right;">

    <button class="btn btn-primary" onclick="window.location.replace('#/home');">Close</button>

</fieldset>
</body>
</html>
