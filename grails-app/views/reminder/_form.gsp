<%@ page import="com.kombos.maintable.Reminder" %>
<g:javascript>
    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57)){
            return false;
        }
        else{
            return true;
        }
    }

    $(function(){
        $('#customerVehicles').typeahead({
            source: function (query, process) {
                var word = $('#customerVehicles').val();
                return $.get('${request.contextPath}/historyCustomerVehicle/getVincodes?word='+word, { query: query }, function (data) {
                    return process(data.options);
                });
            }
        });
    });

    function disableSpace(event){
        var charCode = (event.which) ? event.which : event.keyCode
        if (charCode == 32) {
            return false;
        }
    }
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: reminderInstance, field: 'customerVehicle', 'error')} ">
	<label class="control-label" for="customerVehicle">
		<g:message code="reminder.customerVehicle.label" default="VinCode" />
        <span class="required-indicator">*</span>
    </label>
	<div class="controls">
        <g:textField name="customerVehicles" onkeypress="return disableSpace(event);" id="customerVehicles" required="" class="typeahead" value="${reminderInstance?.customerVehicle?.t103VinCode}" autocomplete="off"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: reminderInstance, field: 'm201ID', 'error')} ">
    <label class="control-label" for="m201ID">
        <g:message code="reminder.m201ID.label" default="Jenis Reminder" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select required="" id="m201ID" name="m201ID.id" from="${com.kombos.maintable.JenisReminder.createCriteria().list{eq("m201StaDel","0");order("m201NamaJenisReminder")}}" optionValue="m201NamaJenisReminder" optionKey="id" value="${reminderInstance?.m201ID?.id}" class="many-to-one" noSelection="['': '']"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: reminderInstance, field: 'm202ID', 'error')} ">
	<label class="control-label" for="m202ID">
		<g:message code="reminder.m202ID.label" default="Status Reminder" />
		
	</label>
	<div class="controls">
	<g:select id="m202ID" name="m202ID.id" from="${com.kombos.maintable.StatusReminder.createCriteria().list{eq("m202StaDel","0");order("m202NamaStatusReminder")}}" optionKey="id" value="${reminderInstance?.m202ID?.id}" class="many-to-one" noSelection="['': '']"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: reminderInstance, field: 't201LastKM', 'error')} ">
	<label class="control-label" for="t201LastKM">
		<g:message code="reminder.t201LastKM.label" default="KM" />
		
	</label>
	<div class="controls">
    <g:textField name="t201LastKM" onkeypress="return isNumberKey(event);" value="${reminderInstance.t201LastKM}" />
	%{--<g:field name="t201LastKM" type="number" value="${reminderInstance.t201LastKM}"/>--}%
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: reminderInstance, field: 't201LastServiceDate', 'error')} ">
	<label class="control-label" for="t201LastServiceDate">
		<g:message code="reminder.t201LastServiceDate.label" default="Last Service Date" />
		
	</label>
	<div class="controls">
	<ba:datePicker format="dd/MM/yyyy" name="t201LastServiceDate" precision="day"  value="${reminderInstance?.t201LastServiceDate}" default="none" noSelection="['': '']" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: reminderInstance, field: 't201TglJatuhTempo', 'error')} ">
	<label class="control-label" for="t201TglJatuhTempo">
		<g:message code="reminder.t201TglJatuhTempo.label" default="Tgl Jatuh Tempo" />
		
	</label>
	<div class="controls">
	<ba:datePicker format="dd/MM/yyyy" name="t201TglJatuhTempo" precision="day"  value="${reminderInstance?.t201TglJatuhTempo}" default="none" noSelection="['': '']" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: reminderInstance, field: 't201TglDM', 'error')} ">
	<label class="control-label" for="t201TglDM">
		<g:message code="reminder.t201TglDM.label" default="Tgl DM" />
		
	</label>
	<div class="controls">
	<ba:datePicker format="dd/MM/yyyy" name="t201TglDM" precision="day"  value="${reminderInstance?.t201TglDM}" default="none" noSelection="['': '']" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: reminderInstance, field: 't201TglSMS', 'error')} ">
	<label class="control-label" for="t201TglSMS">
		<g:message code="reminder.t201TglSMS.label" default="T201 Tgl SMS" />
		
	</label>
	<div class="controls">
	<ba:datePicker format="dd/MM/yyyy" name="t201TglSMS" precision="day"  value="${reminderInstance?.t201TglSMS}" default="none" noSelection="['': '']" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: reminderInstance, field: 't201TglEmail', 'error')} ">
	<label class="control-label" for="t201TglEmail">
		<g:message code="reminder.t201TglEmail.label" default="T201 Tgl Email" />
		
	</label>
	<div class="controls">
	<ba:datePicker format="dd/MM/yyyy" name="t201TglEmail" precision="day"  value="${reminderInstance?.t201TglEmail}" default="none" noSelection="['': '']" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: reminderInstance, field: 't201TglCall', 'error')} ">
	<label class="control-label" for="t201TglCall">
		<g:message code="reminder.t201TglCall.label" default="Tgl. Call" />
		
	</label>
	<div class="controls">
	<ba:datePicker format="dd/MM/yyyy" name="t201TglCall" precision="day"  value="${reminderInstance?.t201TglCall}" default="none" noSelection="['': '']" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: reminderInstance, field: 't201Ket', 'error')} ">
	<label class="control-label" for="t201Ket">
		<g:message code="reminder.t201Ket.label" default="Keterangan" />
		
	</label>
	<div class="controls">
	<g:textArea name="t201Ket" maxlength="50" value="${reminderInstance?.t201Ket}"/>
	</div>
</div>

