

<%@ page import="com.kombos.administrasi.ErrorTranslation" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'errorTranslation.label', default: 'Error Translation')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteErrorTranslation;

$(function(){ 
	deleteErrorTranslation=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/errorTranslation/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadErrorTranslationTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-errorTranslation" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="errorTranslation"
			class="table table-bordered table-hover">
			<tbody>

				
				%{--<g:if test="${errorTranslationInstance?.m780Id}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="m780Id-label" class="property-label"><g:message--}%
					%{--code="errorTranslation.m780Id.label" default="M780 Id" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="m780Id-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${errorTranslationInstance}" field="m780Id"--}%
								%{--url="${request.contextPath}/ErrorTranslation/updatefield" type="text"--}%
								%{--title="Enter m780Id" onsuccess="reloadErrorTranslationTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${errorTranslationInstance}" field="m780Id"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${errorTranslationInstance?.m780Tahun}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="m780Tahun-label" class="property-label"><g:message--}%
					%{--code="errorTranslation.m780Tahun.label" default="M780 Tahun" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="m780Tahun-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${errorTranslationInstance}" field="m780Tahun"--}%
								%{--url="${request.contextPath}/ErrorTranslation/updatefield" type="text"--}%
								%{--title="Enter m780Tahun" onsuccess="reloadErrorTranslationTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${errorTranslationInstance}" field="m780Tahun"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			
				<g:if test="${errorTranslationInstance?.m780ErrorMsg}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m780ErrorMsg-label" class="property-label"><g:message
					code="errorTranslation.m780ErrorMsg.label" default="Error Message" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m780ErrorMsg-label">
						%{--<ba:editableValue
								bean="${errorTranslationInstance}" field="m780ErrorMsg"
								url="${request.contextPath}/ErrorTranslation/updatefield" type="text"
								title="Enter m780ErrorMsg" onsuccess="reloadErrorTranslationTable();" />--}%
							
								<g:fieldValue bean="${errorTranslationInstance}" field="m780ErrorMsg"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${errorTranslationInstance?.m780ErrTranslation}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m780ErrTranslation-label" class="property-label"><g:message
					code="errorTranslation.m780ErrTranslation.label" default="Error Translation" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m780ErrTranslation-label">
						%{--<ba:editableValue
								bean="${errorTranslationInstance}" field="m780ErrTranslation"
								url="${request.contextPath}/ErrorTranslation/updatefield" type="text"
								title="Enter m780ErrTranslation" onsuccess="reloadErrorTranslationTable();" />--}%
							
								<g:fieldValue bean="${errorTranslationInstance}" field="m780ErrTranslation"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${errorTranslationInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="errorTranslation.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${errorTranslationInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/errorTranslation/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloaderrorTranslationTable();" />--}%

                        <g:fieldValue bean="${errorTranslationInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${errorTranslationInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="errorTranslation.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${errorTranslationInstance}" field="dateCreated"
                                url="${request.contextPath}/errorTranslation/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloaderrorTranslationTable();" />--}%

                        <g:formatDate date="${errorTranslationInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${errorTranslationInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="errorTranslation.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${errorTranslationInstance}" field="createdBy"
                                url="${request.contextPath}/errorTranslation/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloaderrorTranslationTable();" />--}%

                        <g:fieldValue bean="${errorTranslationInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${errorTranslationInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="errorTranslation.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${errorTranslationInstance}" field="lastUpdated"
                                url="${request.contextPath}/errorTranslation/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloaderrorTranslationTable();" />--}%

                        <g:formatDate date="${errorTranslationInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${errorTranslationInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="errorTranslation.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${errorTranslationInstance}" field="updatedBy"
                                url="${request.contextPath}/errorTranslation/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloaderrorTranslationTable();" />--}%

                        <g:fieldValue bean="${errorTranslationInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			

			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${errorTranslationInstance?.id}"
					update="[success:'errorTranslation-form',failure:'errorTranslation-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteErrorTranslation('${errorTranslationInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
