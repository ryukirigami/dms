<%@ page import="com.kombos.administrasi.ErrorTranslation" %>



<div class="control-group fieldcontain ${hasErrors(bean: errorTranslationInstance, field: 'm780ErrorMsg', 'error')} required">
	<label class="control-label" for="m780ErrorMsg">
		<g:message code="errorTranslation.m780ErrorMsg.label" default="Error Message" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textArea name="m780ErrorMsg" style="resize: none" maxlength="255" required="" value="${errorTranslationInstance?.m780ErrorMsg}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: errorTranslationInstance, field: 'm780ErrTranslation', 'error')} required">
	<label class="control-label" for="m780ErrTranslation">
		<g:message code="errorTranslation.m780ErrTranslation.label" default="Error Translation" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:textArea name="m780ErrTranslation" style="resize: none" maxlength="255" required="" value="${errorTranslationInstance?.m780ErrTranslation}"/>
	</div>
</div>

