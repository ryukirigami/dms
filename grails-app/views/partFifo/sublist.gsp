<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript>
var PartFifoSubTable;
$(function(){
var anOpen = [];

    if(PartFifoSubTable)
        PartFifoSubTable.dataTable().fnDestroy();

    PartFifoSubTable = $('#partsFifo_datatables_sub_${idTable}').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubList")}",
		"aoColumns": [

{
    "sName": "naon",
    "mDataProp": null,
    "bSortable": false,
    "sWidth":"1.9%",
    "sDefaultContent": ''
}

,

{
    "sName": "naon",
    "mDataProp": null,
    "bSortable": false,
    "sWidth":"1.9%",
    "sDefaultContent": ''
}

,

{
	"sName": "noSO",
	"mDataProp": "noSO",
	"aTargets": [0],
	"bSortable": false,
	"sWidth":"250px",
	"bVisible": true
}

,

{
	"sName": "tglSO",
	"mDataProp": "tglSO",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"250px",
	"bVisible": true
}

,

{
	"sName": "qty",
	"mDataProp": "qty",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"250px",
	"bVisible": true
}

,

{
	"sName": "hargaJual",
	"mDataProp": "hargaJual",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"250px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						%{--aoData.push(--}%
									%{--{"name": 'idTable', "value": "${idTable}"}--}%
						%{--);--}%

						aoData.push(
									{"name": 'fifoId', "value": "${fifoId}"}
						);

						aoData.push(
									{"name": 'kodeGoods', "value": "${kodeGoods}"}
						);

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
    </g:javascript>

</head>
<body>
<div class="innerDetails">
    <table id="partsFifo_datatables_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
           class="display table table-striped table-bordered table-hover" style="width: 100%">
        <thead>
        <tr style="width: 100%">
            <th></th>
            <th></th>
            <th style="border-bottom: none; padding: 5px; width: 250px;">
                <div><g:message code="partFifo.noSO.label" default="Nomor SO" /></div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 250px;">
                <div><g:message code="partFifo.tanggalSO.label" default="Tanggal SO" /></div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 250px;">
                <div><g:message code="partFifo.qty.label" default="Qty" /></div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 250px;">
                <div><g:message code="partFifo.jual.label" default="Harga Jual" /></div>
            </th>
        </tr>
        </thead>
    </table>
</div>
</body>
</html>