
<%@ page import="com.kombos.parts.PartFifo" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="partFifo_datatables_${idTable}" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th rowspan="2" style="vertical-align: middle">
				<div> </div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="partFifo.po.label" default="Nomor Po" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="partFifo.po.label" default="Tanggal Po" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="partFifo.goods.label" default="Kode Goods" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="partFifo.goods.label" default="Nama Goods" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="partFifo.qtyBeli.label" default="Qty" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="partFifo.hargaBeli.label" default="Harga Beli" /></div>
			</th>
		
		</tr>

		<tr>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_po" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_tglPO" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_kodeGoods" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_goods" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_qtyBeli" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_hargaBeli" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
				</div>
			</th>

		</tr>

	</thead>
</table>

<g:javascript>
var partFifoTable;
var reloadPartFifoTable;
$(function(){

	reloadPartFifoTable = function() {
		partFifoTable.fnDraw();
	}

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	partFifoTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	var anOpen = [];
	$('#partFifo_datatables_${idTable} td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = partFifoTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${request.contextPath}/partFifo/sublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = partFifoTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});

  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			partFifoTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

	$('#view').click(function(e){
        e.stopPropagation();
        // staCari = "yes";
		partFifoTable.fnDraw();
//        reloadAuditTrailTable();
	});

	partFifoTable = $('#partFifo_datatables_${idTable}').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "po",
    "mDataProp": null,
    "sClass": "control center",
    "bSortable": false,
    "sWidth":"9px",
    "sDefaultContent": '<i class="icon-plus"></i>'
}

,

{
	"sName": "po",
	"mDataProp": "po",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}

,

{
	"sName": "tglPO",
	"mDataProp": "tglPO",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "kodeGoods",
	"mDataProp": "kodeGoods",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
}

,

{
	"sName": "goods",
	"mDataProp": "goods",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"250px",
	"bVisible": true
}

,

{
	"sName": "qtyBeli",
	"mDataProp": "qtyBeli",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"50px",
	"bVisible": true
}

,

{
	"sName": "hargaBeli",
	"mDataProp": "hargaBeli",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}



],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var goods = $('#goods').val();
						if(goods){
							aoData.push(
									{"name": 'sCriteria_goods', "value": goods}
							);
						}

						var tanggalAwal = $('#tanggalAwal').val();
						if(tanggalAwal){
							aoData.push(
									{"name": 'sCriteria_tanggalAwal', "value": tanggalAwal}
							);
						}

						var tanggalAkhir = $('#tanggalAkhir').val();
						if(tanggalAkhir){
							aoData.push(
									{"name": 'sCriteria_tanggalAkhir', "value": tanggalAkhir}
							);
						}

						//BATAS=================================================

						// var po = $('#filter_po input').val();
						// if(po){
						// 	aoData.push(
						// 			{"name": 'sCriteria_po', "value": po}
						// 	);
						// }
                        //
						// var hargaBeli = $('#filter_hargaBeli input').val();
						// if(hargaBeli){
						// 	aoData.push(
						// 			{"name": 'sCriteria_hargaBeli', "value": hargaBeli}
						// 	);
						// }
                        //
						// var qtyBeli = $('#filter_qtyBeli input').val();
						// if(qtyBeli){
						// 	aoData.push(
						// 			{"name": 'sCriteria_qtyBeli', "value": qtyBeli}
						// 	);
						// }

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
