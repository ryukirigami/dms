

<%@ page import="com.kombos.parts.PartFifo" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'partFifo.label', default: 'PartFifo')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deletePartFifo;

$(function(){ 
	deletePartFifo=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/partFifo/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadPartFifoTable();
   				expandTableLayout('partFifo');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-partFifo" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="partFifo"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${partFifoInstance?.companyDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="companyDealer-label" class="property-label"><g:message
					code="partFifo.companyDealer.label" default="Company Dealer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="companyDealer-label">
						%{--<ba:editableValue
								bean="${partFifoInstance}" field="companyDealer"
								url="${request.contextPath}/PartFifo/updatefield" type="text"
								title="Enter companyDealer" onsuccess="reloadPartFifoTable();" />--}%
							
								<g:link controller="companyDealer" action="show" id="${partFifoInstance?.companyDealer?.id}">${partFifoInstance?.companyDealer?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partFifoInstance?.po}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="po-label" class="property-label"><g:message
					code="partFifo.po.label" default="Po" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="po-label">
						%{--<ba:editableValue
								bean="${partFifoInstance}" field="po"
								url="${request.contextPath}/PartFifo/updatefield" type="text"
								title="Enter po" onsuccess="reloadPartFifoTable();" />--}%
							
								<g:link controller="PO" action="show" id="${partFifoInstance?.po?.id}">${partFifoInstance?.po?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partFifoInstance?.goods}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="goods-label" class="property-label"><g:message
					code="partFifo.goods.label" default="Goods" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="goods-label">
						%{--<ba:editableValue
								bean="${partFifoInstance}" field="goods"
								url="${request.contextPath}/PartFifo/updatefield" type="text"
								title="Enter goods" onsuccess="reloadPartFifoTable();" />--}%
							
								<g:link controller="goods" action="show" id="${partFifoInstance?.goods?.id}">${partFifoInstance?.goods?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partFifoInstance?.hargaBeli}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="hargaBeli-label" class="property-label"><g:message
					code="partFifo.hargaBeli.label" default="Harga Beli" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="hargaBeli-label">
						%{--<ba:editableValue
								bean="${partFifoInstance}" field="hargaBeli"
								url="${request.contextPath}/PartFifo/updatefield" type="text"
								title="Enter hargaBeli" onsuccess="reloadPartFifoTable();" />--}%
							
								<g:fieldValue bean="${partFifoInstance}" field="hargaBeli"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partFifoInstance?.qtyBeli}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="qtyBeli-label" class="property-label"><g:message
					code="partFifo.qtyBeli.label" default="Qty Beli" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="qtyBeli-label">
						%{--<ba:editableValue
								bean="${partFifoInstance}" field="qtyBeli"
								url="${request.contextPath}/PartFifo/updatefield" type="text"
								title="Enter qtyBeli" onsuccess="reloadPartFifoTable();" />--}%
							
								<g:fieldValue bean="${partFifoInstance}" field="qtyBeli"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partFifoInstance?.wo_do}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="wo_do-label" class="property-label"><g:message
					code="partFifo.wo_do.label" default="Wodo" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="wo_do-label">
						%{--<ba:editableValue
								bean="${partFifoInstance}" field="wo_do"
								url="${request.contextPath}/PartFifo/updatefield" type="text"
								title="Enter wo_do" onsuccess="reloadPartFifoTable();" />--}%
							
								<g:fieldValue bean="${partFifoInstance}" field="wo_do"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partFifoInstance?.qtyPakai}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="qtyPakai-label" class="property-label"><g:message
					code="partFifo.qtyPakai.label" default="Qty Pakai" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="qtyPakai-label">
						%{--<ba:editableValue
								bean="${partFifoInstance}" field="qtyPakai"
								url="${request.contextPath}/PartFifo/updatefield" type="text"
								title="Enter qtyPakai" onsuccess="reloadPartFifoTable();" />--}%
							
								<g:fieldValue bean="${partFifoInstance}" field="qtyPakai"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partFifoInstance?.hargaJual}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="hargaJual-label" class="property-label"><g:message
					code="partFifo.hargaJual.label" default="Harga Jual" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="hargaJual-label">
						%{--<ba:editableValue
								bean="${partFifoInstance}" field="hargaJual"
								url="${request.contextPath}/PartFifo/updatefield" type="text"
								title="Enter hargaJual" onsuccess="reloadPartFifoTable();" />--}%
							
								<g:fieldValue bean="${partFifoInstance}" field="hargaJual"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partFifoInstance?.sisa}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="sisa-label" class="property-label"><g:message
					code="partFifo.sisa.label" default="Sisa" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="sisa-label">
						%{--<ba:editableValue
								bean="${partFifoInstance}" field="sisa"
								url="${request.contextPath}/PartFifo/updatefield" type="text"
								title="Enter sisa" onsuccess="reloadPartFifoTable();" />--}%
							
								<g:fieldValue bean="${partFifoInstance}" field="sisa"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partFifoInstance?.isOpen}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="isOpen-label" class="property-label"><g:message
					code="partFifo.isOpen.label" default="Is Open" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="isOpen-label">
						%{--<ba:editableValue
								bean="${partFifoInstance}" field="isOpen"
								url="${request.contextPath}/PartFifo/updatefield" type="text"
								title="Enter isOpen" onsuccess="reloadPartFifoTable();" />--}%
							
								<g:fieldValue bean="${partFifoInstance}" field="isOpen"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partFifoInstance?.tglBeli}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tglBeli-label" class="property-label"><g:message
					code="partFifo.tglBeli.label" default="Tgl Beli" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tglBeli-label">
						%{--<ba:editableValue
								bean="${partFifoInstance}" field="tglBeli"
								url="${request.contextPath}/PartFifo/updatefield" type="text"
								title="Enter tglBeli" onsuccess="reloadPartFifoTable();" />--}%
							
								<g:formatDate date="${partFifoInstance?.tglBeli}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partFifoInstance?.tglJual}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tglJual-label" class="property-label"><g:message
					code="partFifo.tglJual.label" default="Tgl Jual" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tglJual-label">
						%{--<ba:editableValue
								bean="${partFifoInstance}" field="tglJual"
								url="${request.contextPath}/PartFifo/updatefield" type="text"
								title="Enter tglJual" onsuccess="reloadPartFifoTable();" />--}%
							
								<g:formatDate date="${partFifoInstance?.tglJual}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${partFifoInstance?.isWo}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="isWo-label" class="property-label"><g:message
					code="partFifo.isWo.label" default="Is Wo" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="isWo-label">
						%{--<ba:editableValue
								bean="${partFifoInstance}" field="isWo"
								url="${request.contextPath}/PartFifo/updatefield" type="text"
								title="Enter isWo" onsuccess="reloadPartFifoTable();" />--}%
							
								<g:fieldValue bean="${partFifoInstance}" field="isWo"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('partFifo');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${partFifoInstance?.id}"
					update="[success:'partFifo-form',failure:'partFifo-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deletePartFifo('${partFifoInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
