<%@ page import="com.kombos.finance.AccountNumber" %>



<div class="control-group fieldcontain ${hasErrors(bean: accountNumberInstance, field: 'accountName', 'error')} ">
	<label class="control-label" for="accountName">
		<g:message code="accountNumber.accountName.label" default="Account Name" />
	</label>
	<div class="controls">
	<g:textField name="accountName" value="${params.id && (params.action as String).equals("create") ?"":accountNumberInstance?.accountName}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: accountNumberInstance, field: 'accountNumber', 'error')} ">
	<label class="control-label" for="accountNumber">
		<g:message code="accountNumber.accountNumber.label" default="Account Number" />
		
	</label>
	<div class="controls">
	<g:textField name="accountNumber" value="${params.id && (params.action as String).equals("create")?"":accountNumberInstance?.accountNumber}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: accountNumberInstance, field: 'accountType', 'error')} ">
	<label class="control-label" for="accountType">
		<g:message code="accountNumber.accountType.label" default="Account Type" />
		
	</label>
	<div class="controls">
	<g:select id="accountType" name="accountType.id" from="${com.kombos.finance.AccountType.list()}" optionKey="id" required="" value="${accountNumberInstance?.accountType?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: accountNumberInstance, field: 'accountType', 'error')} ">
    <label class="control-label" for="accountMutationType">
        <g:message code="accountNumber.accountType.label" default="Account Mutation Type" />

    </label>
    <div class="controls">
        %{--<g:select name="accountMutationType" from="${com.kombos.finance.AccountNumber.accountMutationType()}" optionKey="" />--}%
        <select name="accountMutationType">

            <g:each in="${com.kombos.finance.AccountNumber.accountMutationType()}" status="o" var="type">
                    <g:if test="${accountNumberInstance.accountMutationType.equals(type)}">
                        <option selected="selected" value="${type}">${type}</option>
                    </g:if>
                    <g:else>
                        <option value="${type}">${type}</option>
                    </g:else>
            </g:each>
        </select>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: accountNumberInstance, field: 'tipeNeraca', 'error')} ">
    <label class="control-label" for="accountType">
        <g:message code="accountNumber.tipeNeraca.label" default="Tipe Neraca" />

    </label>
    <div class="controls">
        <g:select id="tipeNeraca" name="tipeNeraca" from="${["Current Asset","Other Asset","Fixed Asset","Liabilities","Long Term Debt","Equity"]}" required="" value="${accountNumberInstance?.tipeNeraca}" class="many-to-one"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: accountNumberInstance, field: 'tipeLabaRugi', 'error')} ">
    <label class="control-label" for="accountType">
        <g:message code="accountNumber.tipeLabaRugi.label" default="Tipe Laba Rugi" />

    </label>
    <div class="controls">
        <g:select id="tipeLabaRugi" name="tipeLabaRugi" from="${["Revenue","Cogs","Operating Income","Operating Expense"]}" required="" value="${accountNumberInstance?.tipeLabaRugi}" class="many-to-one"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: accountNumberInstance, field: 'tipeArusKas', 'error')} ">
    <label class="control-label" for="accountType">
        <g:message code="accountNumber.tipeArusKas.label" default="Tipe Arus Kas" />

    </label>
    <div class="controls">
        <g:select id="tipeArusKas" name="tipeArusKas" from="${["Non Operating Income","Non Operating Expense"]}" required="" value="${accountNumberInstance?.tipeArusKas}" class="many-to-one"/>
    </div>
</div>

<g:if test="${accountNumberInstance.level>=0}">

<div class="control-group fieldcontain ${hasErrors(bean: accountNumberInstance, field: 'level', 'error')} ">
	<label class="control-label" for="level">
		<g:message code="accountNumber.level.label" default="Level" />

	</label>
	<div class="controls">
    <g:if test="${parent}">
        <input type="text" name="level" value="${parent?.level+1}" readonly="readonly"/>
    </g:if>
    <g:else>
        <input type="text" name="level" value="${Integer.parseInt(accountNumberInstance.level as String)}" readonly/>
    </g:else>
	</div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: accountNumberInstance, field: 'parentAccount', 'error')} ">
	<label class="control-label" for="parentAccount">
		<g:message code="accountNumber.parentAccount.label" default="Parent Account" />
		
	</label>
	<div class="controls">
        <g:if test="${parent}">
            <input type="text" name="parentAccount" value="${parent?.accountNumber}" readonly="readonly"/>
        </g:if>
        <g:else>
            <input type="text" name="parentAccount" value="${accountNumberInstance.parentAccount}" readonly="readonly"/>
        </g:else>
	</div>
</div>

</g:if>

<g:if test="${accountNumberInstance.accountName}">
    <div class="control-group fieldcontain">
        <label for="parentAccountName" class="control-label">Parent Name</label>
        <div class="controls">
            <input type="text" name="parentAccountName" value="${accountNumberInstance.parentAccount ? AccountNumber.findByAccountNumberAndStaDel(accountNumberInstance.parentAccount.toString(),"0").accountName: ''}" readonly>
        </div>
    </div>
</g:if>


<input type="hidden" name="isParent" value="${params.isParent}">
