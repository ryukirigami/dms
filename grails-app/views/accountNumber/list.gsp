
<%@ page import="com.kombos.finance.AccountNumber" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'accountNumber.label', default: 'AccountNumber')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
            var TYPE_SHOW_PARENT = 0,
                TYPE_SHOW_CHILD  = 1;
			var show;
			var edit;
			$(function(){
				$('.box-action').click(function(){
					switch($(this).attr('target')){
					    case '_CREATE_PARENT_':
					        shrinkTableLayout('accountNumber');
							<g:remoteFunction action="create"
                                              params="[isParent: true]"
                                              onLoading="jQuery('#spinner').fadeIn(1);"
                                              onSuccess="loadFormInstance('accountNumber', data, textStatus);"
                                              onComplete="jQuery('#spinner').fadeOut();" />
					        break;
						case '_CREATE_' :
							shrinkTableLayout('accountNumber');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('accountNumber', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('accountNumber', '${request.contextPath}/accountNumber/massdelete', reloadAccountNumberTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id, type) {
                    showInstance('accountNumber','${request.contextPath}/accountNumber/show/'+id);
				};
				
				edit = function(id) {
					editInstance('accountNumber','${request.contextPath}/accountNumber/edit/'+id);
				};

				addChild = function(id) {
                    shrinkTableLayout('accountNumber');
                    $('#spinner').fadeIn(1);
                    $.get('${request.getContextPath()}/accountNumber/create/' +id, function(data){
                       loadFormInstance('accountNumber', data);
                    }).done(function() {
                       $('#spinner').fadeOut();
                    });
				};

});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left">Account Number List</span>
		<ul class="nav pull-right">
            %{--<li><a class="pull-right box-action" href="#"
                   style="display: block;" id="view-child">&nbsp;&nbsp;<i
                        class="icon-search"></i>&nbsp;View Account Number
            </a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" id="view-parent">&nbsp;&nbsp;<i
					class="icon-search"></i>&nbsp;View Parent Account
			</a></li>--}%
            <li><a class="pull-right box-action" href="#"
                   style="display: block;" target="_CREATE_PARENT_">&nbsp;&nbsp;<i
                        class="icon-plus"></i>&nbsp;Parent Account
            </a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="accountNumber-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
		</div>
		<div class="span7" id="accountNumber-form" style="display: none;"></div>
	</div>
</body>
</html>
