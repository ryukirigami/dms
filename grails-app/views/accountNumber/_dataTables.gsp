
<%@ page import="com.kombos.finance.AccountNumber" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="accountNumber_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="accountNumber.accountNumber.label" default="Account Number" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="accountNumber.parentAccount.label" default="Parent Account" /></div>
            </th>




			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="accountNumber.accountName.label" default="Account Name" /></div>
			</th>



			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="accountNumber.accountType.label" default="Account Type" /></div>
			</th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="accountNumber.accountMutationType.label" default="Account Mutation Type" /></div>
            </th>
		
		</tr>
		<tr>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_accountNumber" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_accountNumber" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_parentAccount" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_parentAccount" class="search_init" />
                </div>
            </th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_accountName" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_accountName" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_accountType" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_accountType" class="search_init" />
				</div>
			</th>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_accountMutationType" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_accountMutationType" class="search_init" />
                </div>
            </th>

		</tr>
	</thead>
</table>

<g:javascript>
var accountNumberTable;
var reloadAccountNumberTable;
$(function(){
	
	reloadAccountNumberTable = function() {
		accountNumberTable.fnDraw();
	}

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	accountNumberTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	accountNumberTable = $('#accountNumber_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
	"sName": "accountNumber",
	"mDataProp": "accountNumber",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true,
	"mRender": function(data, type, row) {
	return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this">' +
	       '<input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;' +
	       '<a href="#" onclick="show('+row['id']+');">'+data+'</a>' +
	       '<a href="#" class="pull-right cell-action" href="javascript:void(0);" onclick="addChild('+row['id']+');">' +
	       '<i class="icon-plus"></i>&nbsp;child</a>' +
	       '<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;' +
           '<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	}
}

,

{
	"sName": "parentAccount",
	"mDataProp": "parentAccount",
	"aTargets": [11],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
	/*,
	"mRender": function(data, type, row) {
	return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+(data?data:" ")+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="show('+row['id']+',\''+TYPE_SHOW_PARENT+'\');">&nbsp;&nbsp;<i class="icon-plus"></i>&nbsp;&nbsp;</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	}*/
}
,

{
	"sName": "accountName",
	"mDataProp": "accountName",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,


{
	"sName": "accountType",
	"mDataProp": "accountType",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "accountMutationType",
	"mDataProp": "accountMutationType",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var accountMutationType = $('#filter_accountMutationType input').val();
						if(accountMutationType){
							aoData.push(
									{"name": 'sCriteria_accountMutationType', "value": accountMutationType}
							);
						}
	
						var accountName = $('#filter_accountName input').val();
						if(accountName){
							aoData.push(
									{"name": 'sCriteria_accountName', "value": accountName}
							);
						}
	
						var accountNumber = $('#filter_accountNumber input').val();
						if(accountNumber){
							aoData.push(
									{"name": 'sCriteria_accountNumber', "value": accountNumber}
							);
						}
	
						var accountType = $('#filter_accountType input').val();
						if(accountType){
							aoData.push(
									{"name": 'sCriteria_accountType', "value": accountType}
							);
						}
	
//						var bankReconciliationDetail = $('#filter_bankReconciliationDetail input').val();
//						if(bankReconciliationDetail){
//							aoData.push(
//									{"name": 'sCriteria_bankReconciliationDetail', "value": bankReconciliationDetail}
//							);
//						}
	
//						var journalDetail = $('#filter_journalDetail input').val();
//						if(journalDetail){
//							aoData.push(
//									{"name": 'sCriteria_journalDetail', "value": journalDetail}
//							);
//						}
	
//						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
//						if(lastUpdProcess){
//							aoData.push(
//									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
//							);
//						}
	
//						var ledgerCardDetail = $('#filter_ledgerCardDetail input').val();
//						if(ledgerCardDetail){
//							aoData.push(
//									{"name": 'sCriteria_ledgerCardDetail', "value": ledgerCardDetail}
//							);
//						}
	
//						var level = $('#filter_level input').val();
//						if(level){
//							aoData.push(
//									{"name": 'sCriteria_level', "value": level}
//							);
//						}
	
//						var mappingJournalDetail = $('#filter_mappingJournalDetail input').val();
//						if(mappingJournalDetail){
//							aoData.push(
//									{"name": 'sCriteria_mappingJournalDetail', "value": mappingJournalDetail}
//							);
//						}
//
//						var monthlyBalance = $('#filter_monthlyBalance input').val();
//						if(monthlyBalance){
//							aoData.push(
//									{"name": 'sCriteria_monthlyBalance', "value": monthlyBalance}
//							);
//						}
//
						var parentAccount = $('#filter_parentAccount input').val();
						if(parentAccount){
							aoData.push(
									{"name": 'sCriteria_parentAccount', "value": parentAccount}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
