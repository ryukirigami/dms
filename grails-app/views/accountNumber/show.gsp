

<%@ page import="com.kombos.finance.AccountNumber" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'accountNumber.label', default: 'AccountNumber')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteAccountNumber;

$(function(){ 
	deleteAccountNumber=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/accountNumber/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadAccountNumberTable();
   				expandTableLayout('accountNumber');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-accountNumber" role="main">
        <legend>
            Show Account Number
            %{--${accountNumberInstance.level >0?:" Parent"}--}%
        </legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="accountNumber"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${accountNumberInstance?.accountMutationType}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="accountMutationType-label" class="property-label"><g:message
					code="accountNumber.accountMutationType.label" default="Account Mutation Type" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="accountMutationType-label">
						%{--<ba:editableValue
								bean="${accountNumberInstance}" field="accountMutationType"
								url="${request.contextPath}/AccountNumber/updatefield" type="text"
								title="Enter accountMutationType" onsuccess="reloadAccountNumberTable();" />--}%
							
								<g:fieldValue bean="${accountNumberInstance}" field="accountMutationType"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${accountNumberInstance?.accountName}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="accountName-label" class="property-label"><g:message
					code="accountNumber.accountName.label" default="Account Name" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="accountName-label">
						%{--<ba:editableValue
								bean="${accountNumberInstance}" field="accountName"
								url="${request.contextPath}/AccountNumber/updatefield" type="text"
								title="Enter accountName" onsuccess="reloadAccountNumberTable();" />--}%
							
								<g:fieldValue bean="${accountNumberInstance}" field="accountName"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${accountNumberInstance?.accountNumber}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="accountNumber-label" class="property-label"><g:message
					code="accountNumber.accountNumber.label" default="Account Number" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="accountNumber-label">
						%{--<ba:editableValue
								bean="${accountNumberInstance}" field="accountNumber"
								url="${request.contextPath}/AccountNumber/updatefield" type="text"
								title="Enter accountNumber" onsuccess="reloadAccountNumberTable();" />--}%
							
								<g:fieldValue bean="${accountNumberInstance}" field="accountNumber"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${accountNumberInstance?.accountType}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="accountType-label" class="property-label"><g:message
					code="accountNumber.accountType.label" default="Account Type" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="accountType-label">
						%{--<ba:editableValue
								bean="${accountNumberInstance}" field="accountType"
								url="${request.contextPath}/AccountNumber/updatefield" type="text"
								title="Enter accountType" onsuccess="reloadAccountNumberTable();" />--}%
							
								%{--<g:link controller="accountType"  id="${accountNumberInstance?.accountType?.id}">${accountNumberInstance?.accountType?.encodeAsHTML()}</g:link>--}%
                            <g:fieldValue bean="${accountNumberInstance}" field="accountType"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${accountNumberInstance?.bankReconciliationDetail}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="bankReconciliationDetail-label" class="property-label"><g:message
					code="accountNumber.bankReconciliationDetail.label" default="Bank Reconciliation Detail" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="bankReconciliationDetail-label">
						%{--<ba:editableValue
								bean="${accountNumberInstance}" field="bankReconciliationDetail"
								url="${request.contextPath}/AccountNumber/updatefield" type="text"
								title="Enter bankReconciliationDetail" onsuccess="reloadAccountNumberTable();" />--}%
							
								<g:each in="${accountNumberInstance.bankReconciliationDetail}" var="b">
								<g:link controller="bankReconciliationDetail" action="show" id="${b.id}">${b?.encodeAsHTML()}</g:link>
								</g:each>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${accountNumberInstance?.journalDetail}">
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="journalDetail-label" class="property-label"><g:message--}%
					%{--code="accountNumber.journalDetail.label" default="Journal Detail" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="journalDetail-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${accountNumberInstance}" field="journalDetail"--}%
								%{--url="${request.contextPath}/AccountNumber/updatefield" type="text"--}%
								%{--title="Enter journalDetail" onsuccess="reloadAccountNumberTable();" />--}%
							%{----}%
								%{--<g:each in="${accountNumberInstance.journalDetail}" var="j">--}%
								%{--<g:link controller="journalDetail" action="show" id="${j.id}">${j?.encodeAsHTML()}</g:link>--}%
								%{--</g:each>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				</g:if>

				<g:if test="${accountNumberInstance?.ledgerCardDetail}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="ledgerCardDetail-label" class="property-label"><g:message
					code="accountNumber.ledgerCardDetail.label" default="Ledger Card Detail" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="ledgerCardDetail-label">
						%{--<ba:editableValue
								bean="${accountNumberInstance}" field="ledgerCardDetail"
								url="${request.contextPath}/AccountNumber/updatefield" type="text"
								title="Enter ledgerCardDetail" onsuccess="reloadAccountNumberTable();" />--}%
							
								<g:each in="${accountNumberInstance.ledgerCardDetail}" var="l">
								<g:link controller="ledgerCardDetail" action="show" id="${l.id}">${l?.encodeAsHTML()}</g:link>
								</g:each>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${accountNumberInstance?.level}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="level-label" class="property-label"><g:message
					code="accountNumber.level.label" default="Level" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="level-label">
						%{--<ba:editableValue
								bean="${accountNumberInstance}" field="level"
								url="${request.contextPath}/AccountNumber/updatefield" type="text"
								title="Enter level" onsuccess="reloadAccountNumberTable();" />--}%
							
								<g:fieldValue bean="${accountNumberInstance}" field="level"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${accountNumberInstance?.mappingJournalDetail}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="mappingJournalDetail-label" class="property-label"><g:message--}%
					%{--code="accountNumber.mappingJournalDetail.label" default="Mapping Journal Detail" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="mappingJournalDetail-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${accountNumberInstance}" field="mappingJournalDetail"--}%
								%{--url="${request.contextPath}/AccountNumber/updatefield" type="text"--}%
								%{--title="Enter mappingJournalDetail" onsuccess="reloadAccountNumberTable();" />--}%
							%{----}%
								%{--<g:each in="${accountNumberInstance.mappingJournalDetail}" var="m">--}%
								%{--<g:link controller="mappingJournalDetail" action="show" id="${m.id}">${m?.encodeAsHTML()}</g:link>--}%
								%{--</g:each>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			
				%{--<g:if test="${accountNumberInstance?.monthlyBalance}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="monthlyBalance-label" class="property-label"><g:message--}%
					%{--code="accountNumber.monthlyBalance.label" default="Monthly Balance" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="monthlyBalance-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${accountNumberInstance}" field="monthlyBalance"--}%
								%{--url="${request.contextPath}/AccountNumber/updatefield" type="text"--}%
								%{--title="Enter monthlyBalance" onsuccess="reloadAccountNumberTable();" />--}%
							%{----}%
								%{--<g:each in="${accountNumberInstance.monthlyBalance}" var="m">--}%
								%{--<g:link controller="monthlyBalance" action="show" id="${m.id}">${m?.encodeAsHTML()}</g:link>--}%
								%{--</g:each>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			
				<g:if test="${accountNumberInstance?.parentAccount}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="parentAccount-label" class="property-label"><g:message
					code="accountNumber.parentAccount.label" default="Parent Account" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="parentAccount-label">
						%{--<ba:editableValue
								bean="${accountNumberInstance}" field="parentAccount"
								url="${request.contextPath}/AccountNumber/updatefield" type="text"
								title="Enter parentAccount" onsuccess="reloadAccountNumberTable();" />--}%
							
								<g:fieldValue bean="${accountNumberInstance}" field="parentAccount"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${accountNumberInstance?.staDel}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="staDel-label" class="property-label"><g:message--}%
					%{--code="accountNumber.staDel.label" default="Sta Del" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="staDel-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${accountNumberInstance}" field="staDel"--}%
								%{--url="${request.contextPath}/AccountNumber/updatefield" type="text"--}%
								%{--title="Enter staDel" onsuccess="reloadAccountNumberTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${accountNumberInstance}" field="staDel"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%

            <g:if test="${accountNumberInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="accountNumber.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${accountNumberInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/AccountNumber/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadAccountNumberTable();" />--}%

                        <g:fieldValue bean="${accountNumberInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${accountNumberInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="accountNumber.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${accountNumberInstance}" field="createdBy"
                                url="${request.contextPath}/AccountNumber/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadAccountNumberTable();" />--}%

                        <g:fieldValue bean="${accountNumberInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${accountNumberInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="accountNumber.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${accountNumberInstance}" field="dateCreated"
                                url="${request.contextPath}/AccountNumber/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadAccountNumberTable();" />--}%

                        <g:formatDate date="${accountNumberInstance?.dateCreated}" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${accountNumberInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="accountNumber.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${accountNumberInstance}" field="lastUpdated"
                                url="${request.contextPath}/AccountNumber/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadAccountNumberTable();" />--}%

                        <g:formatDate date="${accountNumberInstance?.lastUpdated}" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${accountNumberInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="accountNumber.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${accountNumberInstance}" field="updatedBy"
								url="${request.contextPath}/AccountNumber/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadAccountNumberTable();" />--}%
							
								<g:fieldValue bean="${accountNumberInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('accountNumber');"><g:message
						code="default.button.close.label" default="CLose" /></a>
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${accountNumberInstance?.id}"
					update="[success:'accountNumber-form',failure:'accountNumber-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteAccountNumber('${accountNumberInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
