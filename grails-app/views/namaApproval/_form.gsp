<%@ page import="com.kombos.baseapp.sec.shiro.Role; com.kombos.baseapp.sec.shiro.User; com.kombos.administrasi.KegiatanApproval; com.kombos.administrasi.NamaApproval" %>


<g:javascript>
    $(function(){
        $('#userProfile').typeahead({
            source: function (query, process) {
                return $.get('${request.contextPath}/namaApproval/getUser?roles='+$('#approvevRole').val(), { query: query }, function (data) {
                    return process(data.options);
                });
            }
        });
        $('#userProfileDelegate').typeahead({
            source: function (query, process) {
                return $.get('${request.contextPath}/namaApproval/getUser?roles='+$('#delegasiRole').val(), { query: query }, function (data) {
                    return process(data.options);
                });
            }
        });
    });

    function changeRole(from){
        $('#'+from).val('');
    }
    
    var checkin = $('#m771TglAwal').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
        var newDate = new Date(ev.date)
        newDate.setDate(newDate.getDate() + 1);
        checkout.setValue(newDate);
        checkin.hide();
        $('#m771TglAkhir')[0].focus();
    }).data('datepicker');
    
    var checkout = $('#m771TglAkhir').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: namaApprovalInstance, field: 'kegiatanApproval', 'error')} required">
	<label class="control-label" for="kegiatanApproval">
		<g:message code="namaApproval.kegiatanApproval.label" default="Nama Kegiatan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:select style="width:480px" id="kegiatanApproval" noSelection="['':'Pilih Kegiatan Approval']" name="kegiatanApproval.id" from="${KegiatanApproval.createCriteria().list{eq("staDel","0");order("m770KegiatanApproval")}}" optionKey="id" required="" value="${namaApprovalInstance?.kegiatanApproval?.id}" class="many-to-one"/>
	</div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: namaApprovalInstance, field: 'm771Level', 'error')} required">
    <label class="control-label" for="m771Level">
        <g:message code="namaApproval.m771Level.label" default="Level" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select style="width:480px"  noSelection="['':'Pilih Level']" required="" name="m771Level" from="${[1,2,3,4,5]}" value="${namaApprovalInstance?.m771Level}" />
    </div>
</div>


<div class="control-group required">
    <label class="control-label" for="approvevRole">
        <g:message code="namaApproval.approvevRole.label" default="Approver Role" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select style="width:480px" id="approvevRole" onchange="changeRole('userProfile');" name="approvelRole.id"  noSelection="['':'Pilih Role']" value="${namaApprovalInstance?.approvelRole?.id}" from="${Role.createCriteria().list(){eq("staDel","0");order("name")}}" optionKey="id" required=""  class="many-to-one"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: namaApprovalInstance, field: 'userProfile', 'error')} required">
	<label class="control-label" for="userProfile">
		<g:message code="namaApproval.userProfile.label" default="Approver Name" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:textField style="width:465px"  name="userProfile" id="userProfile" required=""  class="typeahead" value="${namaApprovalInstance?.userProfile?.fullname}" autocomplete="off"/>
    </div>
</div>


<div class="control-group required">
    <label class="control-label" for="delegasiRole">
        <g:message code="namaApproval.delegasiRole.label" default="Delegasi Role" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select style="width:480px" id="delegasiRole" name="delegasiRole.id" onchange="changeRole('userProfileDelegate');"  noSelection="['':'Pilih Role']" from="${Role.createCriteria().list(){eq("staDel","0");order("name")}}" optionKey="id" value="${namaApprovalInstance?.delegasiRole?.id}" required="" class="many-to-one"/>
    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: namaApprovalInstance, field: 'userProfileDelegate', 'error')} required">
	<label class="control-label" for="userProfileDelegate">
		<g:message code="namaApproval.userProfileDelegate.label" default="Delegasikan Kepada" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:textField style="width:465px"  name="userProfileDelegate" id="userProfileDelegate" required=""  class="typeahead" value="${namaApprovalInstance?.userProfileDelegate?.fullname}" autocomplete="off"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: namaApprovalInstance, field: 'm771TglAwal', 'error')} required">
	<label class="control-label" for="m771TglAwal">
		<g:message code="namaApproval.m771TglAwal.label" default="Tanggal Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<ba:datePicker name="m771TglAwal" id="m771TglAwal" precision="day"  value="${namaApprovalInstance?.m771TglAwal}" format="dd/MM/yyyy" required="true"/>&nbsp;&nbsp;s.d.&nbsp;&nbsp;
    <ba:datePicker id="m771TglAkhir" name="m771TglAkhir" precision="day"  value="${namaApprovalInstance?.m771TglAkhir}" format="dd/MM/yyyy" required="true" />
	</div>
</div>
