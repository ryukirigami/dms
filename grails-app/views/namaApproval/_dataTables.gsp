
<%@ page import="com.kombos.administrasi.NamaApproval" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="namaApproval_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="namaApproval.kegiatanApproval.label" default="Nama Kegiatan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="namaApproval.userProfile.label" default="Approver" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="namaApproval.m771Level.label" default="Level" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="namaApproval.userProfileDelegate.label" default="Delegasikan Kepada" /></div>
			</th>



		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_kegiatanApproval" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_kegiatanApproval" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_userProfile" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_userProfile" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m771Level" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <g:select name="search_m771Level" noSelection="['':'']" from="${[1,2,3,4,5]}" onchange="reloadNamaApprovalTable();" />
                </div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_userProfileDelegate" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_userProfileDelegate" class="search_init" />
				</div>
			</th>

		</tr>
	</thead>
</table>

<g:javascript>
var namaApprovalTable;
var reloadNamaApprovalTable;
$(function(){
	
	reloadNamaApprovalTable = function() {
		namaApprovalTable.fnDraw();
	}
	
    var recordsnamaApprovalperpage = [];
    var anNamaApprovalSelected;
    var jmlRecNamaApprovalPerPage=0;
    var id;
	
	$('#search_m771TglAwal').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m771TglAwal_day').val(newDate.getDate());
			$('#search_m771TglAwal_month').val(newDate.getMonth()+1);
			$('#search_m771TglAwal_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			namaApprovalTable.fnDraw();	
	});

	

	$('#search_m771TglAkhir').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m771TglAkhir_day').val(newDate.getDate());
			$('#search_m771TglAkhir_month').val(newDate.getMonth()+1);
			$('#search_m771TglAkhir_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			namaApprovalTable.fnDraw();	
	});

	


    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	namaApprovalTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	namaApprovalTable = $('#namaApproval_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsNamaApproval = $("#namaApproval_datatables tbody .row-select");
            var jmlNamaApprovalCek = 0;
            var nRow;
            var idRec;
            rsNamaApproval.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsnamaApprovalperpage[idRec]=="1"){
                    jmlNamaApprovalCek = jmlNamaApprovalCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsnamaApprovalperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecNamaApprovalPerPage = rsNamaApproval.length;
            if(jmlNamaApprovalCek==jmlRecNamaApprovalPerPage && jmlRecNamaApprovalPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "kegiatanApproval",
	"mDataProp": "kegiatanApproval",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}
,
{
	"sName": "userProfile",
	"mDataProp": "userProfile",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m771Level",
	"mDataProp": "m771Level",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "userProfileDelegate",
	"mDataProp": "userProfileDelegate",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var kegiatanApproval = $('#filter_kegiatanApproval input').val();
						if(kegiatanApproval){
							aoData.push(
									{"name": 'sCriteria_kegiatanApproval', "value": kegiatanApproval}
							);
						}
	
						var kondisiApproval = $('#filter_kondisiApproval input').val();
						if(kondisiApproval){
							aoData.push(
									{"name": 'sCriteria_kondisiApproval', "value": kondisiApproval}
							);
						}
	
						var userProfile = $('#filter_userProfile input').val();
						if(userProfile){
							aoData.push(
									{"name": 'sCriteria_userProfile', "value": userProfile}
							);
						}
	
						var m771Level = $('#filter_m771Level select').val();
						if(m771Level){
							aoData.push(
									{"name": 'sCriteria_m771Level', "value": m771Level}
							);
						}
	
						var userProfileDelegate = $('#filter_userProfileDelegate input').val();
						if(userProfileDelegate){
							aoData.push(
									{"name": 'sCriteria_userProfileDelegate', "value": userProfileDelegate}
							);
						}

						var m771TglAwal = $('#search_m771TglAwal').val();
						var m771TglAwalDay = $('#search_m771TglAwal_day').val();
						var m771TglAwalMonth = $('#search_m771TglAwal_month').val();
						var m771TglAwalYear = $('#search_m771TglAwal_year').val();
						
						if(m771TglAwal){
							aoData.push(
									{"name": 'sCriteria_m771TglAwal', "value": "date.struct"},
									{"name": 'sCriteria_m771TglAwal_dp', "value": m771TglAwal},
									{"name": 'sCriteria_m771TglAwal_day', "value": m771TglAwalDay},
									{"name": 'sCriteria_m771TglAwal_month', "value": m771TglAwalMonth},
									{"name": 'sCriteria_m771TglAwal_year', "value": m771TglAwalYear}
							);
						}

						var m771TglAkhir = $('#search_m771TglAkhir').val();
						var m771TglAkhirDay = $('#search_m771TglAkhir_day').val();
						var m771TglAkhirMonth = $('#search_m771TglAkhir_month').val();
						var m771TglAkhirYear = $('#search_m771TglAkhir_year').val();
						
						if(m771TglAkhir){
							aoData.push(
									{"name": 'sCriteria_m771TglAkhir', "value": "date.struct"},
									{"name": 'sCriteria_m771TglAkhir_dp', "value": m771TglAkhir},
									{"name": 'sCriteria_m771TglAkhir_day', "value": m771TglAkhirDay},
									{"name": 'sCriteria_m771TglAkhir_month', "value": m771TglAkhirMonth},
									{"name": 'sCriteria_m771TglAkhir_year', "value": m771TglAkhirYear}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {
        $("#namaApproval_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsnamaApprovalperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsnamaApprovalperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

    $('#namaApproval_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsnamaApprovalperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anNamaApprovalSelected = namaApprovalTable.$('tr.row_selected');
            if(jmlRecNamaApprovalPerPage == anNamaApprovalSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsnamaApprovalperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
