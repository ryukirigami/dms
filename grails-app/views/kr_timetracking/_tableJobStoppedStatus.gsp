<table cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="100%">
    <tr>
    	<td>
    		<input type="checkbox" id="selectAllJobStoppedStatus" onclick="selectAll(this);"/>
    	</td>
    </tr>
    <tr>
    	<td>
    		<input type="checkbox" name="jobStoppedStatus" value="0"/>&nbsp;WAITING FOR PARTS
    	</td>
    </tr>
    <tr>
    	<td>
    		<input type="checkbox" name="jobStoppedStatus" value="1"/>&nbsp;WAITING FOR APPROVAL
    	</td>
    </tr>
	<tr>
    	<td>
    		<input type="checkbox" name="jobStoppedStatus" value="2"/>&nbsp;WAITING FOR OTHERS
    	</td>
    </tr>
	<tr>
    	<td>
    		<input type="checkbox" name="jobStoppedStatus" value="3"/>&nbsp;WAITING FOR INSPECTION DURING REPAIR
    	</td>
    </tr>
	<tr>
    	<td>
    		<input type="checkbox" name="jobStoppedStatus" value="4"/>&nbsp;UNDER SUBLET
    	</td>
    </tr>
	<tr>
    	<td>
    		<input type="checkbox" name="jobStoppedStatus" value="5"/>&nbsp;RESET REPAIR ORDER
    	</td>
    </tr>
</table>
<g:javascript>
function selectAll(e){
	if(e.checked){
		$("input[name='jobStoppedStatus']").each(function (){
			$(this).attr("checked", "checked");
		});
	} else {
		$("input[name='jobStoppedStatus']").each(function (){
			$(this).removeAttr("checked");
		});
	}	
}
</g:javascript>