<%@ page import="com.kombos.customerprofile.Warna; com.kombos.administrasi.Section" %>


<g:if test="${sectionInstance?.m051ID}">
    <div class="control-group fieldcontain ${hasErrors(bean: sectionInstance, field: 'm051ID', 'error')} required">
        <label class="control-label" for="m051ID">
            <g:message code="section.m051ID.label" default="Kode Section" />
            <span class="required-indicator">*</span>
        </label>
        <div class="controls">
        ${sectionInstance?.m051ID}
        </div>
    </div>
</g:if>

<div class="control-group fieldcontain ${hasErrors(bean: sectionInstance, field: 'm051NamaSection', 'error')} required">
	<label class="control-label" for="m051NamaSection">
		<g:message code="section.m051NamaSection.label" default="Nama Section" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m051NamaSection" id="m051NamaSection" maxlength="50" required="" value="${sectionInstance?.m051NamaSection}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: sectionInstance, field: 'm051WarnaSection', 'error')} required">
	<label class="control-label" for="warna">
		<g:message code="section.m051WarnaSection.label" default="Warna pada board" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField id="m051WarnaSection" name="m051WarnaSection" required="" value="${sectionInstance?.m051WarnaSection}"/>
	</div>
</div>

%{--<div class="control-group fieldcontain ${hasErrors(bean: sectionInstance, field: 'staDel', 'error')} required">--}%
	%{--<label class="control-label" for="staDel">--}%
		%{--<g:message code="section.staDel.label" default="Sta Del" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="staDel" required="" value="${sectionInstance?.staDel}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: sectionInstance, field: 'createdBy', 'error')} ">--}%
	%{--<label class="control-label" for="createdBy">--}%
		%{--<g:message code="section.createdBy.label" default="Created By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="createdBy" value="${sectionInstance?.createdBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: sectionInstance, field: 'updatedBy', 'error')} ">--}%
	%{--<label class="control-label" for="updatedBy">--}%
		%{--<g:message code="section.updatedBy.label" default="Updated By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="updatedBy" value="${sectionInstance?.updatedBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: sectionInstance, field: 'lastUpdProcess', 'error')} ">--}%
	%{--<label class="control-label" for="lastUpdProcess">--}%
		%{--<g:message code="section.lastUpdProcess.label" default="Last Upd Process" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="lastUpdProcess" value="${sectionInstance?.lastUpdProcess}"/>--}%
	%{--</div>--}%
%{--</div>--}%

<g:javascript>
    document.getElementById('m051NamaSection').focus();
</g:javascript>