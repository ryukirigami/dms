

<%@ page import="com.kombos.administrasi.Section" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'section.label', default: 'Section')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteSection;

$(function(){ 
	deleteSection=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/section/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadSectionTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-section" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="section"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${sectionInstance?.m051ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m051ID-label" class="property-label"><g:message
					code="section.m051ID.label" default="Kode Section" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m051ID-label">
						%{--<ba:editableValue
								bean="${sectionInstance}" field="m051ID"
								url="${request.contextPath}/Section/updatefield" type="text"
								title="Enter m051ID" onsuccess="reloadSectionTable();" />--}%
							
								<g:fieldValue bean="${sectionInstance}" field="m051ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${sectionInstance?.m051NamaSection}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m051NamaSection-label" class="property-label"><g:message
					code="section.m051NamaSection.label" default="Nama Section" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m051NamaSection-label">
						%{--<ba:editableValue
								bean="${sectionInstance}" field="m051NamaSection"
								url="${request.contextPath}/Section/updatefield" type="text"
								title="Enter m051NamaSection" onsuccess="reloadSectionTable();" />--}%
							
								<g:fieldValue bean="${sectionInstance}" field="m051NamaSection"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${sectionInstance?.m051WarnaSection}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m051WarnaSection-label" class="property-label"><g:message
					code="section.m051WarnaSection.label" default="Warna" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m051WarnaSection-label">
						%{--<ba:editableValue
								bean="${sectionInstance}" field="warna"
								url="${request.contextPath}/Section/updatefield" type="text"
								title="Enter warna" onsuccess="reloadSectionTable();" />--}%

                        <g:fieldValue bean="${sectionInstance}" field="m051WarnaSection"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${sectionInstance?.staDel}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="staDel-label" class="property-label"><g:message--}%
					%{--code="section.staDel.label" default="Sta Del" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="staDel-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${sectionInstance}" field="staDel"--}%
								%{--url="${request.contextPath}/Section/updatefield" type="text"--}%
								%{--title="Enter staDel" onsuccess="reloadSectionTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${sectionInstance}" field="staDel"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			
				<g:if test="${sectionInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="section.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${sectionInstance}" field="lastUpdProcess"
								url="${request.contextPath}/Section/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadSectionTable();" />--}%
							
								<g:fieldValue bean="${sectionInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${sectionInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="section.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${sectionInstance}" field="dateCreated"
								url="${request.contextPath}/Section/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadSectionTable();" />--}%
							
								<g:formatDate date="${sectionInstance?.dateCreated}" type="datetime" style="MEDIUM"  />
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${sectionInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="section.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${sectionInstance}" field="createdBy"
                                url="${request.contextPath}/Section/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadSectionTable();" />--}%

                        <g:fieldValue bean="${sectionInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${sectionInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="section.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${sectionInstance}" field="lastUpdated"
								url="${request.contextPath}/Section/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadSectionTable();" />--}%
							
								<g:formatDate date="${sectionInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${sectionInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="section.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${sectionInstance}" field="updatedBy"
                                url="${request.contextPath}/Section/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadSectionTable();" />--}%

                        <g:fieldValue bean="${sectionInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${sectionInstance?.id}"
					update="[success:'section-form',failure:'section-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteSection('${sectionInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
