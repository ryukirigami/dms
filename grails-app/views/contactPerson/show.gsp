

<%@ page import="com.kombos.administrasi.ContactPerson" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'contactPerson.label', default: 'Contact Person')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteContactPerson;

$(function(){ 
	deleteContactPerson=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/contactPerson/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadContactPersonTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-contactPerson" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="contactPerson"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${contactPersonInstance?.companyDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="companyDealer-label" class="property-label"><g:message
					code="contactPerson.companyDealer.label" default="Company Dealer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="companyDealer-label">
						%{--<ba:editableValue
								bean="${contactPersonInstance}" field="companyDealer"
								url="${request.contextPath}/ContactPerson/updatefield" type="text"
								title="Enter companyDealer" onsuccess="reloadContactPersonTable();" />--}%
                                <g:fieldValue bean="${contactPersonInstance?.companyDealer}" field="m011NamaWorkshop"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${contactPersonInstance?.m013Tanggal}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m013Tanggal-label" class="property-label"><g:message
					code="contactPerson.m013Tanggal.label" default="Tanggal Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m013Tanggal-label">
						%{--<ba:editableValue
								bean="${contactPersonInstance}" field="m013Tanggal"
								url="${request.contextPath}/ContactPerson/updatefield" type="text"
								title="Enter m013Tanggal" onsuccess="reloadContactPersonTable();" />--}%
							
								<g:formatDate date="${contactPersonInstance?.m013Tanggal}" format="dd/MM/yyyy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${contactPersonInstance?.m013NamaCP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m013NamaCP-label" class="property-label"><g:message
					code="contactPerson.m013NamaCP.label" default="Nama Contact Person" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m013NamaCP-label">
						%{--<ba:editableValue
								bean="${contactPersonInstance}" field="m013NamaCP"
								url="${request.contextPath}/ContactPerson/updatefield" type="text"
								title="Enter m013NamaCP" onsuccess="reloadContactPersonTable();" />--}%
							
								<g:fieldValue bean="${contactPersonInstance}" field="m013NamaCP"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${contactPersonInstance?.m013JabatanCP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m013JabatanCP-label" class="property-label"><g:message
					code="contactPerson.m013JabatanCP.label" default="Jabatan Contact Person" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m013JabatanCP-label">
						%{--<ba:editableValue
								bean="${contactPersonInstance}" field="m013JabatanCP"
								url="${request.contextPath}/ContactPerson/updatefield" type="text"
								title="Enter m013JabatanCP" onsuccess="reloadContactPersonTable();" />--}%
							
								<g:fieldValue bean="${contactPersonInstance}" field="m013JabatanCP"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${contactPersonInstance?.m013Telp1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m013Telp1-label" class="property-label"><g:message
					code="contactPerson.m013Telp1.label" default="Nomor Telp 1" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m013Telp1-label">
						%{--<ba:editableValue
								bean="${contactPersonInstance}" field="m013Telp1"
								url="${request.contextPath}/ContactPerson/updatefield" type="text"
								title="Enter m013Telp1" onsuccess="reloadContactPersonTable();" />--}%
							
								<g:fieldValue bean="${contactPersonInstance}" field="m013Telp1"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${contactPersonInstance?.m013Telp2}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m013Telp2-label" class="property-label"><g:message
					code="contactPerson.m013Telp2.label" default="Nomor Telp 2" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m013Telp2-label">
						%{--<ba:editableValue
								bean="${contactPersonInstance}" field="m013Telp2"
								url="${request.contextPath}/ContactPerson/updatefield" type="text"
								title="Enter m013Telp2" onsuccess="reloadContactPersonTable();" />--}%
							
								<g:fieldValue bean="${contactPersonInstance}" field="m013Telp2"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${contactPersonInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="contactPerson.lastUpdProcess.label" default="last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${contactPersonInstance}" field="m013Telp2"
                                url="${request.contextPath}/ContactPerson/updatefield" type="text"
                                title="Enter m013Telp2" onsuccess="reloadContactPersonTable();" />--}%

                        <g:fieldValue field="lastUpdProcess" bean="${contactPersonInstance}" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${contactPersonInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="contactPerson.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${contactPersonInstance}" field="m013Telp2"
                                url="${request.contextPath}/ContactPerson/updatefield" type="text"
                                title="Enter m013Telp2" onsuccess="reloadContactPersonTable();" />--}%

                        <g:formatDate date="${contactPersonInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${contactPersonInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="contactPerson.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${contactPersonInstance}" field="m013Telp2"
                                url="${request.contextPath}/ContactPerson/updatefield" type="text"
                                title="Enter m013Telp2" onsuccess="reloadContactPersonTable();" />--}%

                        <g:fieldValue field="createdBy" bean="${contactPersonInstance}" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${contactPersonInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdate-label" class="property-label"><g:message
                                code="contactPerson.lastUpdate.label" default="Last Update" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdate-label">
                        %{--<ba:editableValue
                                bean="${contactPersonInstance}" field="m013Telp2"
                                url="${request.contextPath}/ContactPerson/updatefield" type="text"
                                title="Enter m013Telp2" onsuccess="reloadContactPersonTable();" />--}%

                        <g:formatDate date="${contactPersonInstance?.lastUpdated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${contactPersonInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="contactPerson.updatedBy.label" default="Update By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${contactPersonInstance}" field="m013Telp2"
                                url="${request.contextPath}/ContactPerson/updatefield" type="text"
                                title="Enter m013Telp2" onsuccess="reloadContactPersonTable();" />--}%

                        <g:fieldValue field="updatedBy" bean="${contactPersonInstance}" />

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${contactPersonInstance?.id}"
					update="[success:'contactPerson-form',failure:'contactPerson-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteContactPerson('${contactPersonInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
