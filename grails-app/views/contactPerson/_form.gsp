<%@ page import="com.kombos.administrasi.CompanyDealer; com.kombos.administrasi.ContactPerson; com.kombos.administrasi.ManPower" %>

<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: contactPersonInstance, field: 'companyDealer', 'error')} required">
	<label class="control-label" for="companyDealer">
		<g:message code="contactPerson.companyDealer.label" default="Company Dealer" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="companyDealer" name="companyDealer.id" noSelection="['':'Pilih Company Dealer']" from="${CompanyDealer.createCriteria().list(){eq("staDel","0");order('m011NamaWorkshop')}}" optionKey="id" required="" value="${contactPersonInstance?.companyDealer?.id}" class="many-to-one"/>
	</div>
</div>
<g:javascript>
    document.getElementById("companyDealer").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: contactPersonInstance, field: 'm013Tanggal', 'error')} required">
	<label class="control-label" for="m013Tanggal">
		<g:message code="contactPerson.m013Tanggal.label" default="Tanggal Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
            <ba:datePicker name="m013Tanggal" precision="day" value="${contactPersonInstance?.m013Tanggal}" format="dd/mm/yyyy" required="true"/>
        </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: contactPersonInstance, field: 'm013NamaCP', 'error')} required">
	<label class="control-label" for="m013NamaCP">
		<g:message code="contactPerson.m013NamaCP.label" default="Nama Contact Person" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m013NamaCP" maxlength="50" required="" value="${contactPersonInstance?.m013NamaCP}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: contactPersonInstance, field: 'm013JabatanCP', 'error')} required">
	<label class="control-label" for="m013JabatanCP">
		<g:message code="contactPerson.m013JabatanCP.label" default="Jabatan Contact Person" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:select id="m013JabatanCP" name="m013JabatanCP" noSelection="['':'Pilih Jabatan']" from="${ManPower.createCriteria().listDistinct{eq("staDel","0");order('m014JabatanManPower');projections {property('m014JabatanManPower','jabatan')}}}" required="" value="${contactPersonInstance?.m013JabatanCP?.m014JabatanManPower}" class="many-to-one"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: contactPersonInstance, field: 'm013Telp1', 'error')} ">
	<label class="control-label" for="m013Telp1">
		<g:message code="contactPerson.m013Telp1.label" default="Nomor Telp 1" />
		
	</label>
	<div class="controls">
	<g:textField name="m013Telp1" maxlength="12" onkeypress="return isNumberKey(event);" pattern="${contactPersonInstance.constraints.m013Telp1.matches}" value="${contactPersonInstance?.m013Telp1}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: contactPersonInstance, field: 'm013Telp2', 'error')} ">
	<label class="control-label" for="m013Telp2">
		<g:message code="contactPerson.m013Telp2.label" default="Nomor Telp 2" />
		
	</label>
	<div class="controls">
	<g:textField name="m013Telp2" maxlength="12" onkeypress="return isNumberKey(event);" pattern="${contactPersonInstance.constraints.m013Telp2.matches}" value="${contactPersonInstance?.m013Telp2}"/>
	</div>
</div>

