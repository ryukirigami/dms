
<%@ page import="com.kombos.administrasi.ContactPerson" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>
<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</g:javascript>

<table id="contactPerson_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="contactPerson.companyDealer.label" default="Company Dealer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="contactPerson.m013Tanggal.label" default="Tanggal Berlaku" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="contactPerson.m013NamaCP.label" default="Nama Contact Person" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="contactPerson.m013JabatanCP.label" default="Jabatan Contact Person" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="contactPerson.m013Telp1.label" default="Nomor Telp 1" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="contactPerson.m013Telp2.label" default="Nomor Telp 2" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_companyDealer" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m013Tanggal" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m013Tanggal" value="date.struct">
					<input type="hidden" name="search_m013Tanggal_day" id="search_m013Tanggal_day" value="">
					<input type="hidden" name="search_m013Tanggal_month" id="search_m013Tanggal_month" value="">
					<input type="hidden" name="search_m013Tanggal_year" id="search_m013Tanggal_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m013Tanggal_dp" value="" id="search_m013Tanggal" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m013NamaCP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m013NamaCP" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m013JabatanCP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m013JabatanCP" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m013Telp1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m013Telp1" onkeypress="return isNumberKey(event);" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m013Telp2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m013Telp2" onkeypress="return isNumberKey(event);" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var ContactPersonTable;
var reloadContactPersonTable;
$(function(){
	
	reloadContactPersonTable = function() {
		ContactPersonTable.fnDraw();
	}

    var recordscontactPersonperpage = [];
    var anContactPersonSelected;
    var jmlRecContactPersonPerPage=0;
    var id;

	$('#search_m013Tanggal').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m013Tanggal_day').val(newDate.getDate());
			$('#search_m013Tanggal_month').val(newDate.getMonth()+1);
			$('#search_m013Tanggal_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			ContactPersonTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	ContactPersonTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	ContactPersonTable = $('#contactPerson_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsContactPerson = $("#contactPerson_datatables tbody .row-select");
            var jmlContactPersonCek = 0;
            var nRow;
            var idRec;
            rsContactPerson.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordscontactPersonperpage[idRec]=="1"){
                    jmlContactPersonCek = jmlContactPersonCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordscontactPersonperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecContactPersonPerPage = rsContactPerson.length;
            if(jmlContactPersonCek==jmlRecContactPersonPerPage && jmlRecContactPersonPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "companyDealer",
	"mDataProp": "companyDealer",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m013Tanggal",
	"mDataProp": "m013Tanggal",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m013NamaCP",
	"mDataProp": "m013NamaCP",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m013JabatanCP",
	"mDataProp": "m013JabatanCP",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m013Telp1",
	"mDataProp": "m013Telp1",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m013Telp2",
	"mDataProp": "m013Telp2",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}

						var m013Tanggal = $('#search_m013Tanggal').val();
						var m013TanggalDay = $('#search_m013Tanggal_day').val();
						var m013TanggalMonth = $('#search_m013Tanggal_month').val();
						var m013TanggalYear = $('#search_m013Tanggal_year').val();
						
						if(m013Tanggal){
							aoData.push(
									{"name": 'sCriteria_m013Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_m013Tanggal_dp', "value": m013Tanggal},
									{"name": 'sCriteria_m013Tanggal_day', "value": m013TanggalDay},
									{"name": 'sCriteria_m013Tanggal_month', "value": m013TanggalMonth},
									{"name": 'sCriteria_m013Tanggal_year', "value": m013TanggalYear}
							);
						}
	
						var m013NamaCP = $('#filter_m013NamaCP input').val();
						if(m013NamaCP){
							aoData.push(
									{"name": 'sCriteria_m013NamaCP', "value": m013NamaCP}
							);
						}
	
						var m013JabatanCP = $('#filter_m013JabatanCP input').val();
						if(m013JabatanCP){
							aoData.push(
									{"name": 'sCriteria_m013JabatanCP', "value": m013JabatanCP}
							);
						}
	
						var m013Telp1 = $('#filter_m013Telp1 input').val();
						if(m013Telp1){
							aoData.push(
									{"name": 'sCriteria_m013Telp1', "value": m013Telp1}
							);
						}
	
						var m013Telp2 = $('#filter_m013Telp2 input').val();
						if(m013Telp2){
							aoData.push(
									{"name": 'sCriteria_m013Telp2', "value": m013Telp2}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
    $('.select-all').click(function(e) {
        $("#contactPerson_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordscontactPersonperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordscontactPersonperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

    $('#contactPerson_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordscontactPersonperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anContactPersonSelected = ContactPersonTable.$('tr.row_selected');
            if(jmlRecContactPersonPerPage == anContactPersonSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordscontactPersonperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
