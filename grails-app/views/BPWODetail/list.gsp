
<%@ page import="com.kombos.customerprofile.HistoryCustomerVehicle; com.kombos.administrasi.Stall;com.kombos.reception.Reception" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="bPWODetail.label" default="Body & Paint - WO Detail" /></title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>
			var show;
			var edit;
            var saveChange;
            var noWOPar = -1;
            var fromNoWO=false;
            var fromNoPol=false;
            var finalInspection;
            var adaga = '${receptionInstance?.t401NoWO}'
            if(adaga){
                isiData('${receptionInstance?.t401NoWO}');
            }


    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

        finalInspection = function() {
            var nRow = $('#search_noWO').val();
            if(nRow==null || nRow==""){
                alert("Anda belum memasukan Nomor WO");
            }else{
                shrinkTableLayout();
                $('#spinner').fadeIn(1);
                $.ajax({type:'POST', url:'${request.contextPath}/BPWODetail/finalInspection',
                    data : {noWo : nRow},
                    success:function(data,textStatus){
                        loadForm(data, textStatus);
                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){},
                    complete:function(XMLHttpRequest,textStatus){
                        $('#spinner').fadeOut();
                    }
                });
            }

        };

        shrinkTableLayout = function(){
            $("#bPWODetail-table").hide()
            $("#final-form").css("display","block");
        }

        loadForm = function(data, textStatus){
            $('#final-form').empty();
            $('#final-form').append(data);
        }

        expandTableLayout = function(){
            $("#bPWODetail-table").show()
            $("#final-form").css("display","none");
        }
        
        $("#revisiRateTeknisiModal").on("show", function() {
            $("#revisiRateTeknisiModal .btn").on("click", function(e) {
                $("#revisiRateTeknisiModal").modal('hide');
            });
        });
        $("#revisiRateTeknisiModal").on("hide", function() {
            $("#revisiRateTeknisiModal a.btn").off("click");
        });
        
        function showRate(id){
            var id = id;
            $("#revisiRateTeknisiContent").empty();
            $.ajax({type:'POST', url:'${request.contextPath}/BPRevisiRateTeknisi',
            data : {id : id},
                success:function(data,textStatus){
                        $("#revisiRateTeknisiContent").html(data);
                        $("#revisiRateTeknisiModal").modal({
                            "backdrop" : "static",
                            "keyboard" : true,
                            "show" : true
                        }).css({'width': '880px','margin-left': function () {return -($(this).width() / 2);}});

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
            }

        function isiData(noWo){
            var id=noWo;
            $.ajax({
                url:'${request.contextPath}/BPWODetail/getTableBiayaData',
                type: "POST",
                data: { noWO : id },
                success : function(data){
                    if(data.length>0){
                        $.each(data,function(i,item){
                            biayaTable.fnAddData({
                                'namaJob': item.namaJobParts,
                                'hargaJob': item.hargaJob,
                                'hargaParts': item.hargaParts,
                                'total': item.total
                            });
                        });
                    }
                }
            });

            $.ajax({
                url:'${request.contextPath}/BPWODetail/getTableRateData',
                type: "POST",
                data: { noWO : id },
                success : function(data){
                    $.each(data,function(i,item){
                       rateTeknisiTable.fnAddData({
                            'namaJob': item.namaJob,
                            'proses' : item.proses,
                            'teknisi': item.teknisi,
                            'rate': item.rate,
                            'id' : item.id
                        });
                    });
                }
            });


        }

        function findData(from){
            var from = from
            var noWO=null,cek = true
            if(from=='wo'){
                noWO = $('#search_noWO').val();
                if(noWO==null || noWO==""){
                    cek=false
                }
            }else{
                noWO = $('#woTerkait').val();
                if(noWO==null || noWO==""){
                    cek=false
                }
            }
            if(cek==true){
                $.ajax({
                    url:'${request.contextPath}/BPWODetail/findData',
                    type: "POST",
                    data : { noWO : noWO },
                    success : function(data){
                        $('#noPolAwal').val('');
                        $('#noPolTengah').val('');
                        $('#noPolAkhir').val('');
                        $('#nomorWO').val('');
                        $('#nomorPolisi').val('')
                        $('#modelKendaraan').val('')
                        $('#namaStall').val('')
                        $('#tanggalWO').val('')
                        $('#waktuPenyerahan').val('')
                        $('#namaCustomer').val('')
                        $('#telponCustomer').val('')
                        $('#alamatCustomer').val('')
                        $('#keluhan').val('')
                        $('#permintaan').val('')
                        if(data.length>0){
                        reloadBiayaTable();
                        reloadRateTeknisiTable();
                            isiData(data[0].t401NoWo);
                            $('#search_noWO').val(data[0].t401NoWo);
                            $('#noPolAwal').val(data[0].noPolAwal);
                            $('#noPolTengah').val(data[0].noPolTengah);
                            $('#noPolAkhir').val(data[0].noPolAkhir);
                            $('#nomorWO').val(data[0].t401NoWo);
                            $('#nomorPolisi').val(data[0].noPolisi)
                            $('#modelKendaraan').val(data[0].modelKendaraan)
                            $('#namaStall').val(data[0].namaStall)
                            $('#tanggalWO').val(data[0].tanggalWO)
                            $('#waktuPenyerahan').val(data[0].waktuPenyerahan)
                            $('#namaCustomer').val(data[0].namaCustomer)
                            $('#telponCustomer').val(data[0].telponCustomer)
                            $('#alamatCustomer').val(data[0].alamatCustomer)
                            $('#keluhan').val(data[0].keluhan)
                            $('#permintaan').val(data[0].permintaan)
                        }
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        alert('Internal Server Error');
                    }
                });

            $.ajax({
                url: '${request.contextPath}/BPWODetail/getSelectData?noWO='+noWO,
                type: 'GET',
                dataType: 'html',
                success: function(data, textStatus, xhr) {
                    if(data!=""){
                        $('#woTerkait').html(data);
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    alert(textStatus);
                },
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });


            }else{
                var selectobject=document.getElementById("woTerkait")
                if(from=="wo"){
                    alert('Nomor WO masih kosong');
                }else{
                    if(selectobject.length>0){
                        alert('Anda Belum Memilih WO Terkait');
                    }else{
                        alert('WO Terkait masih kosong');
                    }
                }
            }
        };

        function addJobParts(){
            var vNowo = $('#search_noWO').val();
            if(vNowo==""){
                alert('Data masih kosong');
                return
            }
            window.location.href='#/EditJobParts';
            $('#spinner').fadeIn(1);
            $.ajax({
                url: '${request.contextPath}/editJobParts?nowo='+vNowo,
                type: "GET",dataType:"html",
                complete : function (req, err) {
                    $('#main-content').html(req.responseText);
                    $('#spinner').fadeOut();
                }
            });
        }

        function backButton(){
            loadPath('jobForTeknisi/index');
        }
    </g:javascript>
</head>

<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="bPWODetail.label" default="Body & Paint - WO Detail" /></span>
    <ul class="nav pull-right">
    </ul>
</div>
<div class="box">
    <div class="span12" id="bPWODetail-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <fieldset>
            <legend style="font-size: 14px">
                Search
            </legend>
            <table>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.t401NoWo.label" default="Nomor WO" />
                    </td>
                    <td>
                        <g:textField style="width:280px" name="search_noWO" id="search_noWO" value="${receptionInstance?.t401NoWO}" maxlength="20" onblur="fromNoWO=true;" />
                    </td>
                    <td style="padding: 8px">
                        <g:field type="button" style="padding: 5px;width: 150px" class="btn cancel" onclick="findData('wo');"
                                 name="view" id="view" value="${message(code: 'default.search.label', default: 'Search')}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.noPolisi.label" default="Nomor Polisi" />
                    </td>
                    <td>
                        <g:textField style="width:40px" name="noPolAwal" id="noPolAwal" maxlength="2" value="${receptionInstance?.historyCustomerVehicle?.kodeKotaNoPol?.m116ID}" onblur="fromNoPol=true;" />
                        <g:textField style="width:100px" name="noPolTengah" id="noPolTengah" value="${receptionInstance?.historyCustomerVehicle?.t183NoPolTengah}" onkeypress="return isNumberKey(event);" onblur="fromNoPol=true;" maxlength="5" />
                        <g:textField style="width:40px" name="noPolAkhir" id="noPolAkhir" value="${receptionInstance?.historyCustomerVehicle?.t183NoPolBelakang}" maxlength="3" onblur="fromNoPol=true;" />
                    </td>
                    <td style="padding: 8px">
                        <g:field type="button" style="padding: 5px;width: 150px" class="btn cancel" onclick="findData('woterkait');"
                                 name="view" id="view" value="${message(code: 'default.search.label', default: 'Search WO Terkait')}" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="wo.terkait.label" default="WO Terkait" />
                    </td>
                    <td>
                        <g:select name="woTerkait" id="woTerkait" style="width:300px" from="${Reception.createCriteria().list(){eq("staDel","0");eq("staSave","0");eq("companyDealer",session?.userCompanyDealer);eq("historyCustomerVehicle",HistoryCustomerVehicle.findByFullNoPol("${receptionInstance?.historyCustomerVehicle?.fullNoPol}"))}}" size="5" />
                    </td>
                    <td></td>
                </tr>

            </table>
        </fieldset>


        <fieldset>
            <legend style="font-size: 14px">WO Detail</legend>
            <div class="row-fluid">

                <div id="kiri" class="span5">
                    <table style="width: 100%;">
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.t401NoWo.label" default="Nomor WO" />
                            </td>
                            <td>
                                <g:textField style="width:100%" value="${receptionInstance?.t401NoWO}" name="nomorWO" id="nomorWO" readonly="" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.noPolisi.label" default="Nomor Polisi" />
                            </td>
                            <td>
                                <g:textField style="width:100%" value="${receptionInstance?.historyCustomerVehicle?.fullNoPol}" name="nomorPolisi" readonly="" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.modelKendaraan.label" default="Model Kendaraan" />
                            </td>
                            <td>
                                <g:textField style="width:100%" name="modelKendaraan" value="${receptionInstance?.historyCustomerVehicle?.fullModelCode?.modelName?.m104NamaModelName}" readonly=""/>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.namaStall.label" default="Nama Stall" />
                            </td>
                            <td>
                                <g:textField style="width:100%" name="namaStall" value="${getStall}" readonly=""/>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.tanggalWO.label" default="Tanggal WO" />
                            </td>
                            <td>
                                <g:textField style="width:100%" name="tanggalWO" value="${tangalWO}" readonly="" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.tanggalPenyerahan.label" default="Tanggal Penyerahan" />
                            </td>
                            <td>
                                <g:textField style="width:100%" name="waktuPenyerahan" value="${tanggalPenyerahan}" readonly="" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.namaCustomer.label" default="Nama Customer" />
                            </td>
                            <td>
                                <g:textField style="width:100%" name="namaCustomer" value="${namaCustomer}" readonly="" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.alamatCustomer.label" default="Alamat Customer" />
                            </td>
                            <td>
                                <g:textArea style="width:100%;resize: none" name="alamatCustomer" value="${receptionInstance?.historyCustomer?.t182Alamat}" readonly="" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.teleponCustomer.label" default="Telepon Customer" />
                            </td>
                            <td>
                                <g:textField  style="width:100%" name="telponCustomer" value="${receptionInstance?.historyCustomer?.t182NoHp}" readonly="" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.keluhan.label" default="Keluhan" />
                            </td>
                            <td>
                                <g:textArea style="width:100%;resize: none" name="keluhan" value="${getKeluhan}" readonly="" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <g:message code="wo.permintaan.label" default="Permintaan" />
                            </td>
                            <td>
                                <g:textArea style="width:100%;resize: none" name="permintaan" value="${receptionInstance?.t401PermintaanCusts.each {it.permintaan}}" readonly="" />
                            </td>
                        </tr>
                    </table>
                </div>

                <div id="kanan" class="span7">
                    <fieldset>
                        <legend style="font-size: 14px">
                            <g:message code="wo.biaya.label" default="Biaya" />
                        </legend>
                        <div style="width: 100%" >
                            <g:render template="dataTablesBiaya" />
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend style="font-size: 14px">
                            <g:message code="wo.rateTeknisi.label" default="Rate Teknisi" />
                        </legend>
                        <div style="width: 100%" >
                            <g:render template="dataTablesRateTeknisi" />
                        </div>
                    </fieldset>
                </div>

            </div>
        </fieldset>
        <div>
            <g:field type="button" style="width: 150px" class="btn cancel" onclick="addJobParts();"
                     name="addJob" id="addJob" value="${message(code: 'default.addJob.label', default: 'Add New Job...')}" />
            &nbsp;&nbsp;&nbsp;
            <g:field type="button" style="width: 150px" class="btn cancel" onclick="addJobParts();"
                     name="addParts" id="addParts" value="${message(code: 'default.addParts.label', default: 'Add New Parts...')}" />
            &nbsp;&nbsp;&nbsp;
            <g:field type="button" style="width: 150px" class="btn cancel" onclick="finalInspection();"
                     name="finalInspection" id="finalInspection" value="${message(code: 'default.finalInspection.label', default: 'Final Inspection...')}" />
            &nbsp;&nbsp;&nbsp;
            <g:field type="button" style="width: 150px" class="btn cancel" onclick="backButton();"
                     name="jobforteknisi" id="jobforteknisi" value="${message(code: 'default.finalInspection.label', default: 'Job For Teknisi...')}" />

        </div>
    </div>

    <div class="span7" id="final-form" style="display: none; width:95%"></div>
</div>
<div id="revisiRateTeknisiModal" class="modal fade"style="width: 800px;">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content" style="width: 800px;">
            <!-- dialog body -->
            <div class="modal-body" style="max-height: 800px; width: 800px;">
                <div id="revisiRateTeknisiContent"/>
                %{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}%
                <div class="iu-content"></div>
            </div>

        </div>
    </div>
</div>

</body>
</html>
