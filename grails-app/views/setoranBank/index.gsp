<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'delivery.kwitansi.label', default: 'Setoran Bank')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript>

        $('#search_tglSetor').datepicker().on('changeDate', function(ev) {
       			var newDate = new Date(ev.date);
       			$('#search_tglSetor_day').val(newDate.getDate());
       			$('#search_tglSetor_month').val(newDate.getMonth()+1);
       			$('#search_tglSetor_year').val(newDate.getFullYear());
       			$(this).datepicker('hide');
       	});

       	$('#search_tglFrom').datepicker().on('changeDate', function(ev) {
       			var newDate = new Date(ev.date);
       			$('#search_tglFrom_day').val(newDate.getDate());
       			$('#search_tglFrom_month').val(newDate.getMonth()+1);
       			$('#search_tglFrom_year').val(newDate.getFullYear());
       			$(this).datepicker('hide');
       	});
        $('#search_tglTo').datepicker().on('changeDate', function(ev) {
       			var newDate = new Date(ev.date);
       			$('#search_tglTo_day').val(newDate.getDate());
       			$('#search_tglTo_month').val(newDate.getMonth()+1);
       			$('#search_tglTo_year').val(newDate.getFullYear());
       			$(this).datepicker('hide');
       	});
        $(function() {
            onClearSetoranBank = function () {
                $('#search_tglSetor').val('');
            }
            onClearSearch = function () {
                $('#search_tglFrom').val('');
                $('#search_tglTo').val('');
                reloadSetoranBankTable();
            }
            onSearch = function(){
                var search_tglFrom = $("#search_tglFrom").val();
                var search_tglTo = $("#search_tglTo").val();
                //var oTable = $('#send_approval_datatables').dataTable();
                if ((search_tglFrom.length == 0 && search_tglTo.length == 0) ) {
                    //oTable.fnReloadAjax();
                    reloadSetoranBankTable();
                } else if ((search_tglFrom.length == 10 && search_tglTo.length == 10)) {
                    var from = search_tglFrom.split("/");
                    var dfrom = new Date(from[2], from[1] - 1, from[0]);
                    console.log("dfrom=" + dfrom);
                    var to = search_tglTo.split("/");
                    var dto = new Date(to[2], to[1] - 1, to[0]);
                    console.log("dto=" + dto);
                    if (dfrom <= dto) {
                        //oTable.fnReloadAjax();
                        reloadSetoranBankTable();
                    } else {
                        alert("Tangal Awal Pencarian harus Sebelum Tanggal Akhir Pencarian");
                    }
                } else {
                    if (search_tglFrom.length == 10 && search_tglTo.length == 0) {
                        alert("Silahkan memilih Tanggal Akhir pencarian");
                    } else if (search_tglFrom.length == 0 && search_tglTo.length == 10) {
                        alert("Silahkan memilih Tanggal Awal pencarian");
                    }
                }
            }

            onSaveSetoran = function(){
                var search_tglSetor = $("#search_tglSetor").val();
                var jmlSetor = $("#jmlSetor").val();
                console.log("search_tglSetor=" + search_tglSetor + " jmlSetor=" + jmlSetor);

                if (false) {
                    alert("Is not complete");
                } else {
                    $.ajax({
                        url:'${request.contextPath}/setoranBank/saveSetor?1=1',
                        type: "POST", // Always use POST when deleting data
                        data : { search_tglSetor: search_tglSetor, jmlSetor: jmlSetor },
                        success : function(data){
                            alert('Save success');
                            reloadSetoranBankTable();
                            $('#spinner').fadeIn(1);
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            alert('Internal Server Error');
                        }
                    });
                }

            }

        });
    </g:javascript>
</head>

<body>

<div class="box">
    <legend>Saldo Kas</legend>
    <table style="width: 60%">
        <tr style="padding: 5px; height: 25px">
            <td style="width: 150px">Tanggal</td>
            <td></td>
            <td>
                <span class="property-value">
                    <g:fieldValue bean="${kasKasirInstance}" field="t707TanggalSetorBank" style="width: 125px;" />
                </span>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button class="btn btn-primary" id="buttonClose" onclick="reloadAntriCuciTable();">Close Transaksi</button>
            </td>
        </tr>
        <tr style="padding: 5px; height: 25px">
            <td>Saldo Awal</td>
            <td></td>
            <td>
                <span class="property-value">
                    <g:fieldValue bean="${kasKasirInstance}" field="t707SaldoAwal" style="width: 125px;" />
                </span>
            </td>
        </tr>
        <tr style="padding: 5px; height: 25px">
            <td>Saldo Akhir</td>
            <td></td>
            <td>
                <span class="property-value">
                    <g:fieldValue bean="${kasKasirInstance}" field="t707SaldoAkhir" style="width: 125px;" />
                </span>
            </td>
        </tr>
    </table>
</div>

<div class="box">
    <legend>Setoran Bank</legend>
    <table style="width: 60%">
        <tr style="padding: 5px; height: 25px">
            <td style="width: 150px">Tanggal Setor</td>
            <td></td>
            <td>
                <input type="hidden" name="search_tglSetor" value="date.struct">
                <input type="hidden" name="search_tglSetor_day" id="search_tglSetor_day" value="">
                <input type="hidden" name="search_tglSetor_month" id="search_tglSetor_month" value="">
                <input type="hidden" name="search_tglSetor_year" id="search_tglSetor_year" value="">
                <input type="text" data-date-format="dd/mm/yyyy" name="search_tglSetor_dp" value="" id="search_tglSetor" class="search_init" style="width: 120px">

                &nbsp;&nbsp;
                &nbsp;<button class="btn btn-primary" id="buttonSave" onclick="onSaveSetoran();">Save</button>
                &nbsp;<button class="btn btn-primary" id="buttonClearSave" onclick="onClearSetoranBank();">Clear</button>
            </td>
        </tr>
        <tr style="padding: 5px; height: 25px">
            <td>Nama Bank</td>
            <td></td>
            <td>
                <span class="property-value">
                    <g:fieldValue bean="${kasKasirInstance}" field="t707NamaBank" style="width: 125px;" />
                </span>
            </td>
        </tr>
        <tr style="padding: 5px; height: 25px">
            <td>Atas Nama</td>
            <td></td>
            <td>
                <span class="property-value">
                    <g:fieldValue bean="${kasKasirInstance}" field="t707AtasNama" style="width: 125px;" />
                </span>
            </td>
        </tr>
        <tr style="padding: 5px; height: 25px">
            <td>Nomor Rekening</td>
            <td></td>
            <td>
                <span class="property-value">
                    <g:fieldValue bean="${kasKasirInstance}" field="t707NoRekening" style="width: 125px;" />
                </span>
            </td>
        </tr>
        <tr style="padding: 5px; height: 25px">
            <td>Jumlah Setor</td>
            <td></td>
            <td>
                <span class="property-value">
                    <g:textField name="jmlSetor" id="jmlSetor" value="${kasKasirInstance?.t707JmlSetor}" style="width: 125px;" maxlength="15" />
                </span>
            </td>
        </tr>
        <tr style="padding: 5px; height: 25px">
            <td>Kasir</td>
            <td></td>
            <td>
                <span class="property-value">
                    <g:fieldValue bean="${kasKasirInstance}" field="t707xNamaUser" style="width: 125px;" />
                </span>
            </td>
        </tr>
    </table>
</div>

<div class="box">
    <legend>Setoran Bank List</legend>
    <table style="width: 80%">
        <tr style="padding: 5px; height: 25px">
            <td style="width: 150px">Tanggal Setor</td>
            <td></td>
            <td>
                <input type="hidden" name="search_tglFrom" value="date.struct">
                <input type="hidden" name="search_tglFrom_day" id="search_tglFrom_day" value="">
                <input type="hidden" name="search_tglFrom_month" id="search_tglFrom_month" value="">
                <input type="hidden" name="search_tglFrom_year" id="search_tglFrom_year" value="">
                <input type="text" data-date-format="dd/mm/yyyy" name="search_tglFrom_dp" value="" id="search_tglFrom" class="search_init" style="width: 120px">
                &nbsp;s.d&nbsp;
                <input type="hidden" name="search_tglTo" value="date.struct">
                <input type="hidden" name="search_tglTo_day" id="search_tglTo_day" value="">
                <input type="hidden" name="search_tglTo_month" id="search_tglTo_month" value="">
                <input type="hidden" name="search_tglTo_year" id="search_tglTo_year" value="">
                <input type="text" data-date-format="dd/mm/yyyy" name="search_tglTo_dp" value="" id="search_tglTo" class="search_init" style="width: 120px">

                <button class="btn btn-primary" id="buttonSearch" onclick="onSearch();">Search</button>
                <button class="btn btn-primary" id="buttonClear" onclick="onClearSearch();">Clear</button>
            </td>
        </tr>
    </table>
    <div class="span12" id="sendApproval-table" style="margin-left: -10px; margin-right: 10px;">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>

        <g:render template="setoranDataTables"/>
    </div>
</div>

</body>
</html>