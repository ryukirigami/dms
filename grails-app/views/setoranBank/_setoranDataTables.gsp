<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="setoran_bank_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Tgl Setor</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Setor Ke Bank</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Nama Account</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Nomor Account</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Jumlah Setor</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Kasir</div>
        </th>
    </tr>
    </thead>
</table>
<g:javascript>
var setoranBankdatatables;

function searchData() {
    reloadSetoranBankTable();
}

$(function() {
	reloadSetoranBankTable = function() {
		setoranBankdatatables.fnDraw();
	}

	setoranBankdatatables = $('#setoran_bank_datatables').dataTable({
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		},
	    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
	    },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "setoranBankDataTablesList")}",
		"aoColumns": [
			{
				"sName": "tglSetor",
				"mDataProp": "tglSetor",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			{
				"sName": "setorKeBank",
				"mDataProp": "setorKeBank",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			{
				"sName": "namaAccount",
				"mDataProp": "namaAccount",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			{
				"sName": "nomorAccount",
				"mDataProp": "nomorAccount",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			{
				"sName": "jumlahSetor",
				"mDataProp": "jumlahSetor",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			{
				"sName": "kasir",
				"mDataProp": "kasir",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			}
		],

        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

            var tglFrom = $('#search_tglFrom').val();
            var tglFromDay = $('#search_tglFrom_day').val();
            var tglFromMonth = $('#search_tglFrom_month').val();
            var tglFromYear = $('#search_tglFrom_year').val();
            if(tglFrom){
                aoData.push(
                        {"name": 'sCriteria_tglFrom', "value": "date.struct"},
                        {"name": 'sCriteria_tglFrom_dp', "value": tglFrom},
                        {"name": 'sCriteria_tglFrom_day', "value": tglFromDay},
                        {"name": 'sCriteria_tglFrom_month', "value": tglFromMonth},
                        {"name": 'sCriteria_tglFrom_year', "value": tglFromYear}
                );
            }

            var tglTo = $('#search_tglTo').val();
            var tglToDay = $('#search_tglTo_day').val();
            var tglToMonth = $('#search_tglTo_month').val();
            var tglToYear = $('#search_tglTo_year').val();
            if(tglTo){
                aoData.push(
                        {"name": 'sCriteria_tglTo', "value": "date.struct"},
                        {"name": 'sCriteria_tglTo_dp', "value": tglTo},
                        {"name": 'sCriteria_tglTo_day', "value": tglToDay},
                        {"name": 'sCriteria_tglTo_month', "value": tglToMonth},
                        {"name": 'sCriteria_tglTo_year', "value": tglToYear}
                );
            }

            $.ajax({ "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData ,
                "success": function (json) {
                    fnCallback(json);
                   },
                "complete": function () {
                   }
            });
        }

	});

});
</g:javascript>