<%@ page import="com.kombos.parts.Request" %>

<g:javascript>

    $(function(){
        $('#t162NoReff').typeahead({
            source: function (query, process) {
                return $.get('${request.contextPath}/request/kodeList', { query: query }, function (data) {
                    return process(data.options);
                });
            }
        });
    });

    function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
    }
    $(document).ready(function()
    {
        $('input:radio[name=t162StaFA]').click(function(){
            if($('input:radio[name=t162StaFA]:nth(0)').is(':checked')){
                $("#t162NoReff").prop('disabled', false);

            }else{
                $("#t162NoReff").val("")
                $("#t162NoReff").prop('disabled', true);

            }
        });


    });
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: requestInstance, field: 't162ID', 'error')} ">
	<label class="control-label" for="t162ID">
		<g:message code="request.t162ID.label" default="ID" />
		
	</label>
	<div class="controls">
	<g:textField name="t162ID" maxlength="20" value="${requestInstance?.t162ID}" readonly=""/>
	</div>
</div>
<g:javascript>
    document.getElementById("t162ID").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: requestInstance, field: 'goods', 'error')} ">
	<label class="control-label" for="goods">
		<g:message code="request.goods.label" default="Nama Parts" />
		
	</label>
	<div class="controls">
	%{--<g:select id="goods" name="goods.id" from="${com.kombos.parts.Goods.list()}" optionKey="id" value="${requestInstance?.goods?.id}" class="many-to-one" noSelection="['null': '']" rereadonly=""/>--}%
    <g:textField name="t162ID" maxlength="20" value="${requestInstance?.goods?.m111Nama}" readonly=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: requestInstance, field: 't162Qty1', 'error')} ">
	<label class="control-label" for="t162Qty1">
		<g:message code="request.t162Qty1.label" default="Jumlah" />
		
	</label>
	<div class="controls">
	<g:textField name="t162Qty1" value="${fieldValue(bean: requestInstance, field: 't162Qty1')}" onkeypress="return isNumberKey(event)" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: requestInstance, field: 'goods', 'error')} ">
    <label class="control-label" for="satuan1">
        <g:message code="request.satuan1.label" default="Satuan 1" />

    </label>
    <div class="controls">
        <g:select id="satuan1" name="satuan1.id" from="${com.kombos.parts.Satuan.list()}" optionKey="id" value="${requestInstance?.satuan1?.id}" class="many-to-one" noSelection="['null': '']" />

    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: requestInstance, field: 't162Qty2', 'error')} ">
	<label class="control-label" for="t162Qty2">
		<g:message code="request.t162Qty2.label" default="Jumlah 2" />
		
	</label>
	<div class="controls">
	<g:textField name="t162Qty2" value="${fieldValue(bean: requestInstance, field: 't162Qty2')}" onkeypress="return isNumberKey(event)" />
	</div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: requestInstance, field: 'goods', 'error')} ">
    <label class="control-label" for="satuan2">
        <g:message code="request.satuan2.label" default="Satuan 2" />

    </label>
    <div class="controls">
        <g:select id="satuan2" name="satuan2.id" from="${com.kombos.parts.Satuan.list()}" optionKey="id" value="${requestInstance?.satuan2?.id}" class="many-to-one" noSelection="['null': '']"  />

    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: requestInstance, field: 't162Qty1', 'error')} ">
    <label class="control-label" for="t162Qty1Available">
        <g:message code="request.t162Qty1Available.label" default="Jumlah Available 1" />

    </label>
    <div class="controls">
        <g:textField name="t162Qty1Available" value="${fieldValue(bean: requestInstance, field: 't162Qty1Available')}" onkeypress="return isNumberKey(event)" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: requestInstance, field: 't162Qty2Available', 'error')} ">
    <label class="control-label" for="t162Qty2Available">
        <g:message code="request.t162Qty2Available.label" default="Jumlah Available 2" />

    </label>
    <div class="controls">
        <g:textField name="t162Qty2Available" value="${fieldValue(bean: requestInstance, field: 't162Qty2Available')}" onkeypress="return isNumberKey(event)" />
    </div>
</div>
%{--<div class="control-group fieldcontain ${hasErrors(bean: requestInstance, field: 't162TglRequest', 'error')} ">--}%
	%{--<label class="control-label" for="t162TglRequest">--}%
		%{--<g:message code="request.t162TglRequest.label" default="T162 Tgl Request" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:datePicker name="t162TglRequest" precision="day"  value="${requestInstance?.t162TglRequest}" default="none" noSelection="['': '']" />--}%
	%{--</div>--}%
%{--</div>--}%

<div class="control-group fieldcontain ${hasErrors(bean: requestInstance, field: 't162StaFA', 'error')} ">
	<label class="control-label" for="t162StaFA">
		<g:message code="request.t162StaFA.label" default="Status" />

	</label>
	<div class="controls">

    <g:radioGroup name="t162StaFA" id="t162StaFA" values="['1','0']" labels="['Ya','Tidak']" value="${requestInstance?.t162StaFA}" >
        ${it.radio} ${it.label}
    </g:radioGroup>

	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: requestInstance, field: 't162NoReff', 'error')} ">
	<label class="control-label" for="t162NoReff">
		<g:message code="request.t162NoReff.label" default="No Referensi" />
		
	</label>
	<div class="controls">
        <g:if test="${requestInstance?.t162StaFA == '1'}">
            <g:textField name="t162NoReff" id="t162NoReff" class="typeahead"  maxlength="50" value="${requestInstance?.t162NoReff}" autocomplete="off" />
        </g:if>
    	<g:else>
            <g:textField name="t162NoReff" id="t162NoReff" class="typeahead"  maxlength="50" value="${requestInstance?.t162NoReff}" autocomplete="off" disabled="" />
    	</g:else>
	</div>
</div>
