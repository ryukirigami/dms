
<%@ page import="com.kombos.parts.Claim" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="claimInput_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;" rowspan="2">
				<div><g:message code="claim.goods.label" default="Kode Parts" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;" rowspan="2">
                <div><g:message code="claim.goods.label" default="Nama Parts" /></div>
            </th>


			<th style="border-bottom: none;padding: 5px;" rowspan="2">
				<div><g:message code="claim.t162Qty1.label" default="Tanggal dan Jam Receive" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;" rowspan="2">
                <div><g:message code="claim.satuan1.label" default="Nomor PO" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;" rowspan="2">
                <div><g:message code="claim.goods.label" default="Tanggal PO" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;" rowspan="2">
                <div><g:message code="claim.goods.label" default="Nomor Invoice" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;" rowspan="2">
                <div><g:message code="claim.t162Qty1.label" default="Tanggal Invoice" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px; text-align: center" colspan="4">
                <div><g:message code="claim.t162Qty1.label" default="QTy" /></div>
            </th>

		</tr>
        <tr>
            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="claim.goods.label" default="Invoice" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="claim.t162Qty1.label" default="Receive" /></div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="claim.goods.label" default="Rusak" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="claim.t162Qty1.label" default="Salah" /></div>
            </th>
        </tr>


	</thead>
</table>

<g:javascript>
var claimInputTable;
var reloadClaimTable;
$(function(){
	
	var recordsClaimPerPage = [];
    var anClaimSelected;
    var jmlRecClaimPerPage=0;
    var id;
    
	reloadClaimTable = function() {
		claimInputTable.fnDraw();
	}

	
	$('#search_t162TglClaim').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t162TglClaim_day').val(newDate.getDate());
			$('#search_t162TglClaim_month').val(newDate.getMonth()+1);
			$('#search_t162TglClaim_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			claimInputTable.fnDraw();
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	claimInputTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	claimInputTable = $('#claimInput_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsClaim = $("#claimInput_datatables tbody .row-select");
            var jmlClaimCek = 0;
            var nRow;
            var idRec;
            rsClaim.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsClaimPerPage[idRec]=="1"){
                    jmlClaimCek = jmlClaimCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsClaimPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecClaimPerPage = rsClaim.length;
            if(jmlClaimCek==jmlRecClaimPerPage && jmlRecClaimPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
//		 "bProcessing": true,
//		 "bServerSide": true,
//		 "sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
//	 "sAjaxSource": "${g.createLink(action: "datablesList")}",
		"aoColumns": [

{
	"sName": "goods",
	"mDataProp": "goods",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" id="idPopUp" value="'+row['id']+'">&nbsp;&nbsp;'+data ;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "goods",
	"mDataProp": "goods2",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "goodsReceive",
	"mDataProp": "tglReceive",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "goodsReceive",
	"mDataProp": "po",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "goodsReceive",
	"mDataProp": "tglpo",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,


{
	"sName": "invoice",
	"mDataProp": "nomorInvoice",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "invoice",
	"mDataProp": "tglInvoice",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "invoice",
	"mDataProp": "QInvoice",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "goodsReceive",
	"mDataProp": "Qreceive",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "goodsReceive",
	"mDataProp": "Qrusak",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "goodsReceive",
	"mDataProp": "Qsalah",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var t162ID = $('#filter_t162ID input').val();
						if(t162ID){
							aoData.push(
									{"name": 'sCriteria_t162ID', "value": t162ID}
							);
						}
	
						var goods = $('#filter_goods input').val();
						if(goods){
							aoData.push(
									{"name": 'sCriteria_goods', "value": goods}
							);
						}
	
						var t162Qty1 = $('#filter_t162Qty1 input').val();
						if(t162Qty1){
							aoData.push(
									{"name": 'sCriteria_t162Qty1', "value": t162Qty1}
							);
						}
	
						var t162Qty2 = $('#filter_t162Qty2 input').val();
						if(t162Qty2){
							aoData.push(
									{"name": 'sCriteria_t162Qty2', "value": t162Qty2}
							);
						}

						var t162TglClaim = $('#search_t162TglClaim').val();
						var t162TglClaimDay = $('#search_t162TglClaim_day').val();
						var t162TglClaimMonth = $('#search_t162TglClaim_month').val();
						var t162TglClaimYear = $('#search_t162TglClaim_year').val();
						
						if(t162TglClaim){
							aoData.push(
									{"name": 'sCriteria_t162TglClaim', "value": "date.struct"},
									{"name": 'sCriteria_t162TglClaim_dp', "value": t162TglClaim},
									{"name": 'sCriteria_t162TglClaim_day', "value": t162TglClaimDay},
									{"name": 'sCriteria_t162TglClaim_month', "value": t162TglClaimMonth},
									{"name": 'sCriteria_t162TglClaim_year', "value": t162TglClaimYear}
							);
						}
	
						var t162StaFA = $('#filter_t162StaFA input').val();
						if(t162StaFA){
							aoData.push(
									{"name": 'sCriteria_t162StaFA', "value": t162StaFA}
							);
						}
	
						var t162NoReff = $('#filter_t162NoReff input').val();
						if(t162NoReff){
							aoData.push(
									{"name": 'sCriteria_t162NoReff', "value": t162NoReff}
							);
						}
	
						var t162Qty1Available = $('#filter_t162Qty1Available input').val();
						if(t162Qty1Available){
							aoData.push(
									{"name": 'sCriteria_t162Qty1Available', "value": t162Qty1Available}
							);
						}
	
						var t162Qty2Available = $('#filter_t162Qty2Available input').val();
						if(t162Qty2Available){
							aoData.push(
									{"name": 'sCriteria_t162Qty2Available', "value": t162Qty2Available}
							);
						}
	
						var t162NamaPemohon = $('#filter_t162NamaPemohon input').val();
						if(t162NamaPemohon){
							aoData.push(
									{"name": 'sCriteria_t162NamaPemohon', "value": t162NamaPemohon}
							);
						}
	
						var t162xNamaUser = $('#filter_t162xNamaUser input').val();
						if(t162xNamaUser){
							aoData.push(
									{"name": 'sCriteria_t162xNamaUser', "value": t162xNamaUser}
							);
						}
	
						var t162xNamaDivisi = $('#filter_t162xNamaDivisi input').val();
						if(t162xNamaDivisi){
							aoData.push(
									{"name": 'sCriteria_t162xNamaDivisi', "value": t162xNamaDivisi}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
	$('.select-all').click(function(e) {

        $("#claimInput_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsClaimPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsClaimPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#claimInput_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsClaimPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anClaimSelected = claimInputTable.$('tr.row_selected');

            if(jmlRecClaimPerPage == anClaimSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsClaimPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

	if(cekStatus=="ubah"){
        var noClaim = "${noClaim}";
        $.ajax({
        url:'${request.contextPath}/claimInput/getTableData',
    		type: "POST", // Always use POST when deleting data
    		data: { id: noClaim },
    		success : function(data){
    		    $.each(data,function(i,item){
                    claimInputTable.fnAddData({
                        'id': item.id,
                        'goods': item.goods,
                        'goods2': item.goods2,
                        'po': item.po,
                        'tglpo': item.tglpo,
                        'nomorInvoice': item.nomorInvoice,
                        'tglReceive': item.tglReceive,
                        'QInvoice': item.qtyInvoice,
                        'Qreceive': item.qtyReceive,
                        'Qsalah': item.qtySalah,
                        'Qrusak': item.qtyRusak,
                        'tglInvoice': item.tglInvoice

                    });
                });
            }
		});
    }
});
</g:javascript>


			
