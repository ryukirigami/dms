<%@ page import="com.kombos.parts.Claim" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'claim.label', default: 'Input Parts Claim')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
    <g:javascript>
		$(function(){
			$('#m121Nama').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/claimInput/kodeList', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});
    </g:javascript>
		<g:javascript disposition="head">
        var m121Nama = "";
        var noUrut = 1;
	var show;
	var cekStatus = "${aksi}";
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){

	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/claim/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/claim/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#claimInput-form').empty();
    	$('#claimInput-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#claimInput-table").hasClass("span12")){
   			$("#claimInput-table").toggleClass("span12 span5");
        }
        $("#claimInput-form").css("display","block");
   	}
   	
   	expandTableLayout = function(){
   		if($("#claimInput-table").hasClass("span5")){
   			$("#claimInput-table").toggleClass("span5 span12");
   		}
        $("#claimInput-form").css("display","none");
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#claimInput-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/claimInput/massdelete',
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadClaimTable();
    		}
		});
		
   	}

});
            $(document).ready(function()
         {
             $('input:radio[name=search_t162StaFA]').click(function(){
                if($('input:radio[name=search_t162StaFA]:nth(0)').is(':checked')){
                    $("#search_t162NoReff").prop('disabled', false);

                }else{
                    $("#search_t162NoReff").val("")
                    $("#search_t162NoReff").prop('disabled', true);


                }
             });


         });

    //darisini
	$("#claimAddModal").on("show", function() {
		$("#claimAddModal .btn").on("click", function(e) {
			$("#claimAddModal").modal('hide');
		});
	});
	$("#claimAddModal").on("hide", function() {
		$("#claimAddModal a.btn").off("click");
	});
    loadClaimAddModal = function(){
            if(cekStatus !='ubah'){
                if(m121Nama != $('#m121Nama').val()){
                   $("#claimInput-table tbody .row-select").each(function() {
                        var id = $(this).next("input:hidden").val();
                        var row = $(this).closest("tr").get(0);
                        claimInputTable.fnDeleteRow(claimInputTable.fnGetPosition(row));
                    });
                }
            }
    	    m121Nama = $('#m121Nama').val();

		$("#claimAddContent").empty();
		$.ajax({type:'POST', url:'${request.contextPath}/claimAdd/list',
		data : {vendor : m121Nama},
   			success:function(data,textStatus){
					$("#claimAddContent").html(data);
				    $("#claimAddModal").modal({
						"backdrop" : "static",
						"keyboard" : true,
						"show" : true
					}).css({'width': '1100px','margin-left': function () {return -($(this).width() / 2);}});

   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});



    }

       tambahReq = function(){
           var checkGoods =[];
            $("#ClaimAdd-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    var nRow = $(this).parents('tr')[0];

					checkGoods.push(nRow);
                }
            });
            if(checkGoods.length<1){
                alert('Anda belum memilih data yang akan ditambahkan');
                loadClaimAddModal();
            }else {
				for (var i=0;i < checkGoods.length;i++){

					var aData = ClaimAddTable.fnGetData(checkGoods[i]);

					claimInputTable.fnAddData({
						'id': aData['id'],
						'norut': noUrut,
						'goods': aData['goods'],
						'goods2': aData['goods2'],
						'tglReceive': aData['tglReceive'],
						'po': aData['po'],
						'tglpo': aData['tglpo'],
						'nomorInvoice': aData['nomorInvoice'],
						'tglInvoice': aData['tglInvoice'],
						'QInvoice': aData['qtyInvoice'],
						'Qreceive': aData['qtyReceive'],
						'Qrusak': aData['qtyRusak'],
						'Qsalah': aData['qtySalah']

						});
					noUrut++;
					$('#qty-'+aData['id']).autoNumeric('init',{
						vMin:'0',
						vMax:'999999999999999999',
						mDec: null,
						aSep:''
					});
				}
                loadClaimAddModal();
			}

      }
      updateClaim = function(){
            var nomor = $("#nomorClaim").val();
            var formClaim = $('#claimInput-table').find('form');
            var checkGoods =[];
            $("#claimInput-table tbody .row-select").each(function() {
            if(this.checked){
                var id = $(this).next("input:hidden").val();
                var nRow = $(this).parents('tr')[0];
                var aData = claimInputTable.fnGetData(nRow);
                checkGoods.push(id);
            }
            });
            if(checkGoods.length<1){
                alert('Anda belum memilih data yang akan ditambahkan');
                 reloadClaimTable();
            }else{
                $("#ids").val(JSON.stringify(checkGoods));
                $.ajax({
                    url:'${request.contextPath}/claimInput/ubahData',
                    type: "POST", // Always use POST when deleting data
                    data : formClaim.serialize(),
                    success : function(data){
                        window.location.href = '#/claim' ;
                        toastr.success('<div>Claim Parts telah diUbah.</div>');
                        formClaim.find('.deleteafter').remove();
                        $("#ids").val('');
                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });

                checkGoods = [];
            }
       }
    deleteData = function(){
                $("#claimInput-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    var row = $(this).closest("tr").get(0);
                    claimInputTable.fnDeleteRow(claimInputTable.fnGetPosition(row));
                }
                });
      }

        createRequest = function(){
            var nomor = $("#nomorClaim").val();
            var tanggal = $("#tanggal").val();
            var jam = $("#jam").val();
            var menit = $("#menit").val();
            var vendor = $("#vendor").val();
            var formClaim = $('#claimInput-table').find('form');

            var checkGoods =[];
            $("#claimInput-table tbody .row-select").each(function() {
            if(this.checked){
                var id = $(this).next("input:hidden").val();
                var nRow = $(this).parents('tr')[0];
                var aData = claimInputTable.fnGetData(nRow);
                checkGoods.push(id);
            }
            });
            if(checkGoods.length<1){
                alert('Anda belum memilih data yang akan ditambahkan');
                 reloadClaimTable();
            }else if(tanggal==""){
                toastr.error('<div>Data Belum Lengkap</div>');
            }else{
             var r = confirm("Anda yakin data akan disimpan?");
             if(r==true){
                var rows = claimInputTable.fnGetNodes();
                $("#ids").val(JSON.stringify(checkGoods));
                $.ajax({
                    url:'${request.contextPath}/claimInput/req',

                    type: "POST", // Always use POST when deleting data
                    data : formClaim.serialize(),
                    success : function(data){
                      if(data=='fail'){
                        toastr.error('<div>Vendor Tidak Benar.</div>');
                      }else{
                        if(rows.length > 10){
                            $("#claimInput-table tbody .row-select").each(function() {
                                if(this.checked){
                                    var row = $(this).closest("tr").get(0);
                                    claimInputTable.fnDeleteRow(claimInputTable.fnGetPosition(row));
                                    reloadClaimTable();
                                }
                            });
                            formClaim.append('<input type="hidden" name="noClaimBefore" value="'+data+'" class="deleteafter">');
                            toastr.success('<div>Sukses</div>');
                        }else{
                            window.location.href = '#/claim' ;
                            toastr.success('<div>Claim Parts telah dibuat.</div>');
                            formClaim.find('.deleteafter').remove();
                            $("#ids").val('');
                        }

                      }

                    },
                error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
                }
                });

                checkGoods = [];
              }
            }
        };

</g:javascript>

	</head>
	<body>
	<div class="navbar box-header no-border">
        <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
        <ul class="nav pull-right">
            <li></li>
			%{--<li><a class="pull-right box-action" href="#"--}%
				%{--style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i--}%
					%{--class="icon-remove"></i>&nbsp;&nbsp;--}%
			%{--</a></li>--}%
			%{--<li class="separator"></li>--}%
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="claimInput-table">
            <form id="form-claim" class="form-horizontal">
                <input type="hidden" name="ids" id="ids" value="">
                    <fieldset>
                        <g:if test="${aksi=='ubah'}">
                            <div class="control-group">
                                <label class="control-label" for="t951Tanggal" style="text-align: left;">
                                    Nomor Claim
                                </label>
                                <div id="filter_nomor" class="controls">
                                    <g:hiddenField name="aksi" value="ubah" />
                                    <g:textField name="nomorClaim" id="nomorClaim" maxlength="20" value="${noClaim}" readonly="readonly"  />
                                </div>
                            </div>
                        </g:if>
                        <div class="control-group">
                            <label class="control-label" for="userRole" style="text-align: left;">
                                Tanggal Claim *
                            </label>
                            <div id="filter_status" class="controls">
                            <g:if test="${aksi=='ubah'}">
                                <g:textField name="tanggal" id="tanggal" maxlength="90" value="${tglClaim}" readonly="readonly"  />
                            </g:if>
                            <g:else>
                                <ba:datePicker id="tanggal" name="tanggal" precision="day" format="dd-MM-yyyy"  value="" required="true"  />
                                <select id="jam" name="jam" style="width: 60px" required="">
                                    %{
                                        for (int i=0;i<24;i++){
                                            if(i<10){

                                                out.println('<option value="'+i+'">0'+i+'</option>');


                                            } else {

                                                out.println('<option value="'+i+'">'+i+'</option>');

                                            }

                                        }
                                    }%
                                </select> H :
                                <select id="menit" name="menit" style="width: 60px" required="">
                                    %{
                                        for (int i=0;i<60;i++){
                                            if(i<10){

                                                out.println('<option value="'+i+'">0'+i+'</option>');


                                            } else {

                                                out.println('<option value="'+i+'">'+i+'</option>');

                                            }
                                        }
                                    }%
                                </select> m
                            </g:else>
                            </div>
                         </div>
                        <div class="control-group">
                            <label class="control-label" for="t951Tanggal" style="text-align: left;">
                                Vendor *
                            </label>
                            <div id="filter_vendor" class="controls">
                                <g:if test="${aksi=='ubah'}">
                                    <g:textField name="namaVendor" id="m121Nama" maxlength="20" value="${vendor}" readonly="readonly"  />
                                </g:if>
                                <g:else>
                                    <g:textField name="namaVendor" id="m121Nama" class="typeahead"  autocomplete="off" maxlength="220"  />
                                </g:else>
                            </div>
                        </div>

                    </fieldset>
                   </form>
                    <fieldset>
                        <table>
                            <tr>
                                <td>
                                    <button style="width: 170px;height: 30px; border-radius: 5px" class="btn btn-primary add" name="view" id="add" onclick="loadClaimAddModal()">Add Parts</button>
                                </td>
                            </tr>
                        </table>
                    </fieldset><br>
			<g:render template="dataTables" />
            <g:if test="${aksi=='ubah'}">
                <g:field type="button" onclick="updateClaim()" class="btn btn-primary create" name="Edit"  value="${message(code: 'default.button.upload.label', default: 'Update')}" />
            </g:if>
            <g:else>
                <g:field type="button" onclick="createRequest()" class="btn btn-primary create" name="tambahSave"  value="${message(code: 'default.button.upload.label', default: 'Save')}" />
            </g:else>
            <g:field type="button" onclick="window.location.replace('#/claim');" class="btn btn-cancel" name="cancel" id="cancel" value="Cancel" />
            <g:field type="button" onclick="deleteData()" class="btn btn-primary create" name="deleteData" id="delData" value="${message(code: 'default.button.upload.label', default: 'Delete')}" />
		</div>
		<div class="span7" id="claim-form" style="display: none;"></div>
	</div>
    <div id="claimAddModal" class="modal fade">
        <div class="modal-dialog" style="width: 1100px;">
            <div class="modal-content" style="width: 1100px;">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 1100px;">
                    <div id="claimAddContent"/>
                <div class="iu-content"></div>

                </div>
            </div>
        </div>
    </div>
</body>
</html>


<g:javascript>
  if(cekStatus !='ubah'){
    document.getElementById("tanggal").focus();
  }
</g:javascript>