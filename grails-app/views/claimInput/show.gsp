

<%@ page import="com.kombos.parts.Request" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'request.label', default: 'Request')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteRequest;

$(function(){ 
	deleteRequest=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/request/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadRequestTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-request" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="request"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${requestInstance?.t162ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t162ID-label" class="property-label"><g:message
					code="request.t162ID.label" default="ID" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t162ID-label">
						%{--<ba:editableValue
								bean="${requestInstance}" field="t162ID"
								url="${request.contextPath}/Request/updatefield" type="text"
								title="Enter t162ID" onsuccess="reloadRequestTable();" />--}%
							
								<g:fieldValue bean="${requestInstance}" field="t162ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${requestInstance?.goods}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="goods-label" class="property-label"><g:message
					code="request.goods.label" default="Nama Parts" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="goods-label">
						%{--<ba:editableValue
								bean="${requestInstance}" field="goods"
								url="${request.contextPath}/Request/updatefield" type="text"
								title="Enter goods" onsuccess="reloadRequestTable();" />--}%
							
								<g:link controller="goods" action="show" id="${requestInstance?.goods?.id}">${requestInstance?.goods?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${requestInstance?.t162Qty1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t162Qty1-label" class="property-label"><g:message
					code="request.t162Qty1.label" default="Jumlah" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t162Qty1-label">
						%{--<ba:editableValue
								bean="${requestInstance}" field="t162Qty1"
								url="${request.contextPath}/Request/updatefield" type="text"
								title="Enter t162Qty1" onsuccess="reloadRequestTable();" />--}%
							
								<g:fieldValue bean="${requestInstance}" field="t162Qty1"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${requestInstance?.t162Qty2}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t162Qty2-label" class="property-label"><g:message
					code="request.t162Qty2.label" default="Jumlah 2" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t162Qty2-label">
						%{--<ba:editableValue
								bean="${requestInstance}" field="t162Qty2"
								url="${request.contextPath}/Request/updatefield" type="text"
								title="Enter t162Qty2" onsuccess="reloadRequestTable();" />--}%
							
								<g:fieldValue bean="${requestInstance}" field="t162Qty2"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${requestInstance?.satuan1}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="satuan1-label" class="property-label"><g:message
                                code="goods.satuan.label" default="Satuan 1" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="satuan1-label">
                        %{--<ba:editableValue
                                bean="${requestInstance}" field="satuan"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter satuan" onsuccess="reloadGoodsTable();" />--}%

                        <g:link controller="satuan1" action="show" id="${requestInstance?.satuan1?.id}">${requestInstance?.satuan1?.encodeAsHTML()}</g:link>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${requestInstance?.satuan2}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="satuan-label" class="property-label"><g:message
                                code="goods.satuan2.label" default="Satuan 2" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="satuan2-label">
                        %{--<ba:editableValue
                                bean="${requestInstance}" field="satuan"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter satuan" onsuccess="reloadGoodsTable();" />--}%

                        <g:link controller="satuan2" action="show" id="${requestInstance?.satuan2?.id}">${requestInstance?.satuan2?.encodeAsHTML()}</g:link>

                    </span></td>

                </tr>
            </g:if>
				<g:if test="${requestInstance?.t162TglRequest}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t162TglRequest-label" class="property-label"><g:message
					code="request.t162TglRequest.label" default="Tanggal" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t162TglRequest-label">
						%{--<ba:editableValue
								bean="${requestInstance}" field="t162TglRequest"
								url="${request.contextPath}/Request/updatefield" type="text"
								title="Enter t162TglRequest" onsuccess="reloadRequestTable();" />--}%
							
								<g:formatDate date="${requestInstance?.t162TglRequest}" />
							
						</span></td>
					
				</tr>
				</g:if>
            <g:if test="${requestInstance?.t162Qty1Available}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t162Qty1Available-label" class="property-label"><g:message
                                code="request.t162Qty1Available.label" default="t162Qty1Available" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t162Qty1Available-label">
                        %{--<ba:editableValue
                                bean="${requestInstance}" field="t162Qty2"
                                url="${request.contextPath}/Request/updatefield" type="text"
                                title="Enter t162Qty2" onsuccess="reloadRequestTable();" />--}%

                        <g:fieldValue bean="${requestInstance}" field="t162Qty1Available"/>

                    </span></td>

                </tr>
            </g:if>
            <g:if test="${requestInstance?.t162Qty2Available}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t162Qty2Available-label" class="property-label"><g:message
                                code="request.t162Qty2Available.label" default="t162Qty2Available" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t162Qty2Available-label">
                        %{--<ba:editableValue
                                bean="${requestInstance}" field="t162Qty2"
                                url="${request.contextPath}/Request/updatefield" type="text"
                                title="Enter t162Qty2" onsuccess="reloadRequestTable();" />--}%

                        <g:fieldValue bean="${requestInstance}" field="t162Qty2Available"/>

                    </span></td>

                </tr>
            </g:if>

			%{----}%
				%{--<g:if test="${requestInstance?.t162StaFA}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="t162StaFA-label" class="property-label"><g:message--}%
					%{--code="request.t162StaFA.label" default="T162 Sta FA" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="t162StaFA-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${requestInstance}" field="t162StaFA"--}%
								%{--url="${request.contextPath}/Request/updatefield" type="text"--}%
								%{--title="Enter t162StaFA" onsuccess="reloadRequestTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${requestInstance}" field="t162StaFA"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				<g:if test="${requestInstance?.t162NoReff}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t162NoReff-label" class="property-label"><g:message
					code="request.t162NoReff.label" default="No Referensi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t162NoReff-label">
						%{--<ba:editableValue
								bean="${requestInstance}" field="t162NoReff"
								url="${request.contextPath}/Request/updatefield" type="text"
								title="Enter t162NoReff" onsuccess="reloadRequestTable();" />--}%
							
								<g:fieldValue bean="${requestInstance}" field="t162NoReff"/>
							
						</span></td>
					
				</tr>
				</g:if>


			
				<g:if test="${requestInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="request.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${requestInstance}" field="createdBy"
								url="${request.contextPath}/Request/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadRequestTable();" />--}%
							
								<g:fieldValue bean="${requestInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${requestInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="request.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${requestInstance}" field="updatedBy"
								url="${request.contextPath}/Request/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadRequestTable();" />--}%
							
								<g:fieldValue bean="${requestInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${requestInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="request.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${requestInstance}" field="lastUpdProcess"
								url="${request.contextPath}/Request/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadRequestTable();" />--}%
							
								<g:fieldValue bean="${requestInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${requestInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="request.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${requestInstance}" field="dateCreated"
								url="${request.contextPath}/Request/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadRequestTable();" />--}%
							
								<g:formatDate date="${requestInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${requestInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="request.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${requestInstance}" field="lastUpdated"
								url="${request.contextPath}/Request/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadRequestTable();" />--}%
							
								<g:formatDate date="${requestInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.close.label" default="Close" /></a>
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${requestInstance?.id}"
					update="[success:'request-form',failure:'request-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteRequest('${requestInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
