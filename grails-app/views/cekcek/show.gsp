

<%@ page import="com.kombos.example.Cekcek" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'cekcek.label', default: 'Cekcek')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteCekcek;

$(function(){ 
	deleteCekcek=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/cekcek/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadCekcekTable();
   				expandTableLayout('cekcek');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-cekcek" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="cekcek"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${cekcekInstance?.agama}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="agama-label" class="property-label"><g:message
					code="cekcek.agama.label" default="Agama" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="agama-label">
						%{--<ba:editableValue
								bean="${cekcekInstance}" field="agama"
								url="${request.contextPath}/Cekcek/updatefield" type="text"
								title="Enter agama" onsuccess="reloadCekcekTable();" />--}%
							
								<g:link controller="agama" action="show" id="${cekcekInstance?.agama?.id}">${cekcekInstance?.agama?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cekcekInstance?.alamat}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="alamat-label" class="property-label"><g:message
					code="cekcek.alamat.label" default="Alamat" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="alamat-label">
						%{--<ba:editableValue
								bean="${cekcekInstance}" field="alamat"
								url="${request.contextPath}/Cekcek/updatefield" type="text"
								title="Enter alamat" onsuccess="reloadCekcekTable();" />--}%
							
								<g:fieldValue bean="${cekcekInstance}" field="alamat"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cekcekInstance?.jumlahKendaraan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="jumlahKendaraan-label" class="property-label"><g:message
					code="cekcek.jumlahKendaraan.label" default="Jumlah Kendaraan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="jumlahKendaraan-label">
						%{--<ba:editableValue
								bean="${cekcekInstance}" field="jumlahKendaraan"
								url="${request.contextPath}/Cekcek/updatefield" type="text"
								title="Enter jumlahKendaraan" onsuccess="reloadCekcekTable();" />--}%
							
								<g:fieldValue bean="${cekcekInstance}" field="jumlahKendaraan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cekcekInstance?.kota}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kota-label" class="property-label"><g:message
					code="cekcek.kota.label" default="Kota" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kota-label">
						%{--<ba:editableValue
								bean="${cekcekInstance}" field="kota"
								url="${request.contextPath}/Cekcek/updatefield" type="text"
								title="Enter kota" onsuccess="reloadCekcekTable();" />--}%
							
								<g:fieldValue bean="${cekcekInstance}" field="kota"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cekcekInstance?.nama}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="nama-label" class="property-label"><g:message
					code="cekcek.nama.label" default="Nama" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="nama-label">
						%{--<ba:editableValue
								bean="${cekcekInstance}" field="nama"
								url="${request.contextPath}/Cekcek/updatefield" type="text"
								title="Enter nama" onsuccess="reloadCekcekTable();" />--}%
							
								<g:fieldValue bean="${cekcekInstance}" field="nama"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cekcekInstance?.tanggalLahir}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tanggalLahir-label" class="property-label"><g:message
					code="cekcek.tanggalLahir.label" default="Tanggal Lahir" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tanggalLahir-label">
						%{--<ba:editableValue
								bean="${cekcekInstance}" field="tanggalLahir"
								url="${request.contextPath}/Cekcek/updatefield" type="text"
								title="Enter tanggalLahir" onsuccess="reloadCekcekTable();" />--}%
							
								<g:formatDate date="${cekcekInstance?.tanggalLahir}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('cekcek');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${cekcekInstance?.id}"
					update="[success:'cekcek-form',failure:'cekcek-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteCekcek('${cekcekInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
