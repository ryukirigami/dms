<%@ page import="com.kombos.example.Cekcek" %>



<div class="control-group fieldcontain ${hasErrors(bean: cekcekInstance, field: 'agama', 'error')} ">
	<label class="control-label" for="agama">
		<g:message code="cekcek.agama.label" default="Agama" />
		
	</label>
	<div class="controls">
	<g:select id="agama" name="agama.id" from="${com.kombos.customerprofile.Agama.list()}" optionKey="id" required="" value="${cekcekInstance?.agama?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: cekcekInstance, field: 'alamat', 'error')} ">
	<label class="control-label" for="alamat">
		<g:message code="cekcek.alamat.label" default="Alamat" />
		
	</label>
	<div class="controls">
	<g:textField name="alamat" value="${cekcekInstance?.alamat}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: cekcekInstance, field: 'jumlahKendaraan', 'error')} ">
	<label class="control-label" for="jumlahKendaraan">
		<g:message code="cekcek.jumlahKendaraan.label" default="Jumlah Kendaraan" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="jumlahKendaraan" value="${cekcekInstance.jumlahKendaraan}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: cekcekInstance, field: 'kota', 'error')} ">
	<label class="control-label" for="kota">
		<g:message code="cekcek.kota.label" default="Kota" />
		
	</label>
	<div class="controls">
	<g:textField name="kota" value="${cekcekInstance?.kota}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: cekcekInstance, field: 'nama', 'error')} ">
	<label class="control-label" for="nama">
		<g:message code="cekcek.nama.label" default="Nama" />
		
	</label>
	<div class="controls">
	<g:textField name="nama" value="${cekcekInstance?.nama}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: cekcekInstance, field: 'tanggalLahir', 'error')} ">
	<label class="control-label" for="tanggalLahir">
		<g:message code="cekcek.tanggalLahir.label" default="Tanggal Lahir" />
		
	</label>
	<div class="controls">
	<ba:datePicker name="tanggalLahir" precision="day" value="${cekcekInstance?.tanggalLahir}" format="yyyy-MM-dd"/>
	</div>
</div>

