
<%@ page import="com.kombos.example.Cekcek" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'cekcek.label', default: 'Cekcek')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			$(function(){ 
			
				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('cekcek');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('cekcek', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('cekcek', '${request.contextPath}/cekcek/massdelete', reloadCekcekTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('cekcek','${request.contextPath}/cekcek/show/'+id);
				};
				
				edit = function(id) {
					editInstance('cekcek','${request.contextPath}/cekcek/edit/'+id);
				};

});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="cekcek-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
		</div>
		<div class="span7" id="cekcek-form" style="display: none;"></div>
	</div>
</body>
</html>
