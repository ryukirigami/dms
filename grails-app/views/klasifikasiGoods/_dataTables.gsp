
<%@ page import="com.kombos.parts.KlasifikasiGoods" %>

<r:require modules="baseapplayout" />


<g:render template="../menu/maxLineDisplay"/>

<table id="klasifikasiGoods_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover  table-selectable"
	width="100%">
	<thead>
		<tr>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="klasifikasiGoods.kodeparts.label" default="Kode Parts" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
				<div><g:message code="klasifikasiGoods.goods.label" default="Nama Parts" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="klasifikasiGoods.group.label" default="Group" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="klasifikasiGoods.franc.label" default="Franc" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="klasifikasiGoods.scc.label" default="Scc" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="klasifikasiGoods.location.label" default="Location" /></div>
			</th>



		
		</tr>
		<tr>
		

			<th style="border-top: none;padding: 5px;">
				<div id="filter_kodeParts" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_kodeParts" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_goods" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_goods" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
				<div id="filter_group" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_group" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_franc" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_franc" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_scc" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_scc" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_location" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_location" class="search_init" />
				</div>
			</th>
	



		</tr>
	</thead>
</table>

<g:javascript>
var klasifikasiGoodsTable;
var reloadKlasifikasiGoodsTable;

$(function(){

    var recordsklasifikasiGoodsperpage = [];
    var anKlasifikasiGoodsSelected;
    var jmlRecKlasifikasiGoodsPerPage=0;
    var id;

        
	reloadKlasifikasiGoodsTable = function() {
		klasifikasiGoodsTable.fnDraw();
	}

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	klasifikasiGoodsTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	klasifikasiGoodsTable = $('#klasifikasiGoods_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
	    "fnDrawCallback": function () {
            var rsKlasifikasiGoods = $("#klasifikasiGoods_datatables tbody .row-select");
            var jmlKlasifikasiGoodsCek = 0;
            var nRow;
            var idRec;
            rsKlasifikasiGoods.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsklasifikasiGoodsperpage[idRec]=="1"){
                    jmlKlasifikasiGoodsCek = jmlKlasifikasiGoodsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsklasifikasiGoodsperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecKlasifikasiGoodsPerPage = rsKlasifikasiGoods.length;
            if(jmlKlasifikasiGoodsCek==jmlRecKlasifikasiGoodsPerPage && jmlRecKlasifikasiGoodsPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
	    "fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
            //  showEditable();
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			    return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [


{
	"sName": "kodeParts",
	"mDataProp": "kodeParts",
	"aTargets": [1],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="javascript:void(0);" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "namaGoods",
	"mDataProp": "goods",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "group",
	"mDataProp": "group",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "franc",
	"mDataProp": "franc",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "scc",
	"mDataProp": "scc",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "location",
	"mDataProp": "location",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}
	
						var goods = $('#filter_goods input').val();
						if(goods){
							aoData.push(
									{"name": 'sCriteria_goods', "value": goods}
							);
						}

						var kodeParts = $('#filter_kodeParts input').val();
						if(kodeParts){
							aoData.push(
									{"name": 'sCriteria_kodeParts', "value": kodeParts}
							);
						}
	
						var group = $('#filter_group input').val();
						if(group){
							aoData.push(
									{"name": 'sCriteria_group', "value": group}
							);
						}
	
						var franc = $('#filter_franc input').val();
						if(franc){
							aoData.push(
									{"name": 'sCriteria_franc', "value": franc}
							);
						}
	
						var scc = $('#filter_scc input').val();
						if(scc){
							aoData.push(
									{"name": 'sCriteria_scc', "value": scc}
							);
						}
	
						var location = $('#filter_location input').val();
						if(location){
							aoData.push(
									{"name": 'sCriteria_location', "value": location}
							);
						}
	
						var parameterICC = $('#filter_parameterICC input').val();
						if(parameterICC){
							aoData.push(
									{"name": 'sCriteria_parameterICC', "value": parameterICC}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
	$('.select-all').click(function(e) {
        $("#klasifikasiGoods_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsklasifikasiGoodsperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsklasifikasiGoodsperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

    $('#klasifikasiGoods_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsklasifikasiGoodsperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anKlasifikasiGoodsSelected = klasifikasiGoodsTable.$('tr.row_selected');
            if(jmlRecKlasifikasiGoodsPerPage == anKlasifikasiGoodsSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsklasifikasiGoodsperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });



});



</g:javascript>


			
