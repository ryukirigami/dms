<%@ page import="com.kombos.parts.Location; com.kombos.parts.SCC; com.kombos.parts.Franc; com.kombos.parts.Group; com.kombos.parts.Goods; com.kombos.administrasi.CompanyDealer; com.kombos.parts.KlasifikasiGoods" %>

<g:javascript>
    var getIdGoods;
    var showGoods

    $(function(){
    	showGoods = function() {
    			$("#src_koGo").val("");
    			$("#src_naGo").val("");
    			$("#src_satGo").val("");

    			nomorDOTable.fnDraw();

                $("#pilihGoodsModal").modal({
                        "backdrop" : "dynamic",
                        "keyboard" : true,
                        "show" : true
                    }).css({'width': '800px','margin-left': function () {return -($(this).width() / 2);}});
            }


       $('#good').typeahead({
            source: function (query, process) {
                return $.get('${request.contextPath}/klasifikasiGoods/listGoods', { query: query }, function (data) {
                    return process(data.options);
                });
            }
        });

       getIdGoods = function(){
            var kodeGoods = $('#good').val();
            $.ajax({
                url:'${request.contextPath}/klasifikasiGoods/idGoods?kodeGoods='+kodeGoods,
                type: "POST", // Always use POST when deleting data
                success : function(data){
                    $('#idGoods').val(data);
                }
            })
       }

       $("#btnPilih").click(function() {
       		$("#pilihGoodsModel").modal("hide");
       })
    });
</g:javascript>


<div class="control-group fieldcontain ${hasErrors(bean: klasifikasiGoodsInstance, field: 'goodS', 'error')} ">
	<label class="control-label" for="good">
		<g:message code="klasifikasiGoods.goods.label" default="Goods" />
		<span class="-indicator">*</span>
	</label>
	<div class="controls">


%{--TESTER--}%

	%{--<g:select id="goods" name="goods.id" from="${Goods.createCriteria().list(){eq("staDel", "0");order("m111ID", "asc")}}" optionKey="id" value="${klasifikasiGoodsInstance?.goods?.id}" class="many-to-one"/>--}%
    %{--<g:textField name="good" id="good" required="" optionKey="id" class="typeahead" value="${klasifikasiGoodsInstance?.goods ? klasifikasiGoodsInstance?.goods?.m111ID?.trim()+" | "+klasifikasiGoodsInstance?.goods?.m111Nama?.trim():""}" onblur="getIdGoods();" autocomplete="off"/>--}%

	<g:hiddenField name="idGoods" id="idGoods" required="" value="${klasifikasiGoodsInstance?.goods?.id}" />

	<input id="good" type="text" readonly="readonly" value="${klasifikasiGoodsInstance?.goods ? klasifikasiGoodsInstance?.goods?.m111ID?.trim()+" | "+klasifikasiGoodsInstance?.goods?.m111Nama?.trim():""}"/>
		<div class="input-append">
				<g:if test="${!klasifikasiGoodsInstance?.goods?.id}">
					<button type="button" class="btn" onclick="showGoods();">
						<i class="icon-search"></i>
					</button>
				</g:if>
		</div>
	</div>
</div>

<g:javascript>
    document.getElementById("good").focus();
</g:javascript>


<div class="control-group fieldcontain ${hasErrors(bean: klasifikasiGoodsInstance, field: 'group', 'error')} ">
	<label class="control-label" for="group">
		<g:message code="klasifikasiGoods.group.label" default="Group" />
		<span class="-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="group" name="group.id" from="${Group.createCriteria().list(){eq("staDel", "0");order("m181NamaGroup", "asc")}}" optionKey="id"  value="${klasifikasiGoodsInstance?.group?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: klasifikasiGoodsInstance, field: 'franc', 'error')} ">
	<label class="control-label" for="franc">
		<g:message code="klasifikasiGoods.franc.label" default="Franc" />
		<span class="-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="franc" name="franc.id" from="${Franc.createCriteria().list(){order("m117NamaFranc", "asc")}}" optionKey="id"  value="${klasifikasiGoodsInstance?.franc?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: klasifikasiGoodsInstance, field: 'scc', 'error')} ">
	<label class="control-label" for="scc">
		<g:message code="klasifikasiGoods.scc.label" default="Scc" />
		<span class="-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="scc" name="scc.id" from="${SCC.createCriteria().list(){eq("staDel", "0");order("m113NamaSCC", "asc")}}" optionKey="id" value="${klasifikasiGoodsInstance?.scc?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: klasifikasiGoodsInstance, field: 'location', 'error')} ">
	<label class="control-label" for="location">
		<g:message code="klasifikasiGoods.location.label" default="Location" />
		<span class="-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="location" name="location.id" from="${Location.createCriteria().list(){eq("staDel", "0");order("m120NamaLocation", "asc")}}" optionKey="id"  value="${klasifikasiGoodsInstance?.location?.id}" class="many-to-one"/>
	</div>
</div>

<div id="pilihGoodsModal" class="modal hide">
	<div class="modal-content">
		<div class="modal-body">
			<g:render template="showGoods"/>
		</div>

		<div class="modal-footer">
			<a class="btn btn-primary" id="btnPilih" data-dismiss = "modal">Pilih</a>
		</div>
	</div>
</div>


