

<%@ page import="com.kombos.parts.KlasifikasiGoods" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'klasifikasiGoods.label', default: 'Klasifikasi Goods')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteKlasifikasiGoods;

$(function(){ 
	deleteKlasifikasiGoods=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/klasifikasiGoods/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadKlasifikasiGoodsTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-klasifikasiGoods" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="klasifikasiGoods"
			class="table table-bordered table-hover">
			<tbody>



            <g:if test="${klasifikasiGoodsInstance?.companyDealer}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="companyDealer-label" class="property-label"><g:message
                                code="klasifikasiGoods.companyDealer.label" default="Company Dealer" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="companyDealer-label">
                        %{--<ba:editableValue
                                bean="${klasifikasiGoodsInstance}" field="companyDealer"
                                url="${request.contextPath}/KlasifikasiGoods/updatefield" type="text"
                                title="Enter companyDealer" onsuccess="reloadKlasifikasiGoodsTable();" />--}%

                        ${klasifikasiGoodsInstance?.companyDealer?.encodeAsHTML()}

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${klasifikasiGoodsInstance?.goods}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="goods-label" class="property-label"><g:message
					code="klasifikasiGoods.goods.label" default="Parts" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="goods-label">
						%{--<ba:editableValue
								bean="${klasifikasiGoodsInstance}" field="goods"
								url="${request.contextPath}/KlasifikasiGoods/updatefield" type="text"
								title="Enter goods" onsuccess="reloadKlasifikasiGoodsTable();" />--}%
							
								${klasifikasiGoodsInstance?.goods?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${klasifikasiGoodsInstance?.group}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="group-label" class="property-label"><g:message
					code="klasifikasiGoods.group.label" default="Group" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="group-label">
						%{--<ba:editableValue
								bean="${klasifikasiGoodsInstance}" field="group"
								url="${request.contextPath}/KlasifikasiGoods/updatefield" type="text"
								title="Enter group" onsuccess="reloadKlasifikasiGoodsTable();" />--}%
							
								${klasifikasiGoodsInstance?.group?.encodeAsHTML()}
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${klasifikasiGoodsInstance?.franc}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="franc-label" class="property-label"><g:message
					code="klasifikasiGoods.franc.label" default="Franc" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="franc-label">
						%{--<ba:editableValue
								bean="${klasifikasiGoodsInstance}" field="franc"
								url="${request.contextPath}/KlasifikasiGoods/updatefield" type="text"
								title="Enter franc" onsuccess="reloadKlasifikasiGoodsTable();" />--}%
							
							${klasifikasiGoodsInstance?.franc?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${klasifikasiGoodsInstance?.scc}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="scc-label" class="property-label"><g:message
					code="klasifikasiGoods.scc.label" default="Scc" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="scc-label">
						%{--<ba:editableValue
								bean="${klasifikasiGoodsInstance}" field="scc"
								url="${request.contextPath}/KlasifikasiGoods/updatefield" type="text"
								title="Enter scc" onsuccess="reloadKlasifikasiGoodsTable();" />--}%
							
							${klasifikasiGoodsInstance?.scc?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${klasifikasiGoodsInstance?.location}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="location-label" class="property-label"><g:message
					code="klasifikasiGoods.location.label" default="Location" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="location-label">
						%{--<ba:editableValue
								bean="${klasifikasiGoodsInstance}" field="location"
								url="${request.contextPath}/KlasifikasiGoods/updatefield" type="text"
								title="Enter location" onsuccess="reloadKlasifikasiGoodsTable();" />--}%
							
								${klasifikasiGoodsInstance?.location?.encodeAsHTML()}
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${klasifikasiGoodsInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="franc.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${klasifikasiGoodsInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/Franc/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadFrancTable();" />--}%

                        <g:fieldValue bean="${klasifikasiGoodsInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${klasifikasiGoodsInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="franc.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${klasifikasiGoodsInstance}" field="dateCreated"
                                url="${request.contextPath}/Franc/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadFrancTable();" />--}%

                        <g:formatDate date="${klasifikasiGoodsInstance?.dateCreated}" type="datetime" style="MEDIUM"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${klasifikasiGoodsInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="franc.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${klasifikasiGoodsInstance}" field="createdBy"
                                url="${request.contextPath}/Franc/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadFrancTable();" />--}%

                        <g:fieldValue bean="${klasifikasiGoodsInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${klasifikasiGoodsInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="franc.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${klasifikasiGoodsInstance}" field="lastUpdated"
                                url="${request.contextPath}/Franc/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadFrancTable();" />--}%

                        <g:formatDate date="${klasifikasiGoodsInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${klasifikasiGoodsInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="franc.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${klasifikasiGoodsInstance}" field="updatedBy"
                                url="${request.contextPath}/Franc/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadFrancTable();" />--}%

                        <g:fieldValue bean="${klasifikasiGoodsInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>


            </tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.close.label" default="Close" /></a>
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${klasifikasiGoodsInstance?.id}"
					update="[success:'klasifikasiGoods-form',failure:'klasifikasiGoods-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteKlasifikasiGoods('${klasifikasiGoodsInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>

</body>
</html>
