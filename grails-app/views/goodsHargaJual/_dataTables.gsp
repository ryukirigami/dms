
<%@ page import="com.kombos.parts.GoodsHargaJual" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<g:javascript>
    function isNumber(evt){
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57)){
            return false;
        } else {
            return true;
        }
    }
</g:javascript>

<table id="goodsHargaJual_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="goodsHargaJual.t151TMT.label" default="Tangal Berlaku" /></div>
			</th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="goodsHargaJual.kategori.label" default="Kode Goods" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="goodsHargaJual.goods.label" default="Nama Goods" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
				<div><g:message code="goodsHargaJual.t151HargaTanpaPPN.label" default="Harga Jual" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="goodsHargaJual.t151HargaDenganPPN.label" default="Harga Jual PPN" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="goodsHargaJual.satuan.label" default="Satuan" /></div>
			</th>

		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t151TMT" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_t151TMT" value="date.struct">
					<input type="hidden" name="search_t151TMT_day" id="search_t151TMT_day" value="">
					<input type="hidden" name="search_t151TMT_month" id="search_t151TMT_month" value="">
					<input type="hidden" name="search_t151TMT_year" id="search_t151TMT_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t151TMT_dp" value="" id="search_t151TMT" class="search_init">
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_kategori" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_kategori" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_goods" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_goods" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
				<div id="filter_t151HargaTanpaPPN" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" onkeypress="return isNumber(event);" name="search_t151HargaTanpaPPN" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t151HargaDenganPPN" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" onkeypress="return isNumber(event);" name="search_t151HargaDenganPPN" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_satuan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_satuan" class="search_init" />
				</div>
			</th>
	

		</tr>
	</thead>
</table>

<g:javascript>
var goodsHargaJualTable;
var reloadGoodsHargaJualTable;
$(function(){
	
	reloadGoodsHargaJualTable = function() {
		goodsHargaJualTable.fnDraw();
	}

	var recordsGoodsHargaJualPerPage = [];
    var anGoodsHargaJualSelected;
    var jmlRecGoodsHargaJualPerPage=0;
    var id;
	
	$('#search_t151TMT').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t151TMT_day').val(newDate.getDate());
			$('#search_t151TMT_month').val(newDate.getMonth()+1);
			$('#search_t151TMT_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			goodsHargaJualTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	goodsHargaJualTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	goodsHargaJualTable = $('#goodsHargaJual_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsGoodsHargaJual = $("#goodsHargaJual_datatables tbody .row-select");
            var jmlGoodsHargaJualCek = 0;
            var nRow;
            var idRec;
            rsGoodsHargaJual.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsGoodsHargaJualPerPage[idRec]=="1"){
                    jmlGoodsHargaJualCek = jmlGoodsHargaJualCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsGoodsHargaJualPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecGoodsHargaJualPerPage = rsGoodsHargaJual.length;
            if(jmlGoodsHargaJualCek==jmlRecGoodsHargaJualPerPage && jmlRecGoodsHargaJualPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "t151TMT",
	"mDataProp": "t151TMT",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="javascript:void(0);" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "goods1",
	"mDataProp": "kode",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "goods2",
	"mDataProp": "goods",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "t151HargaTanpaPPN",
	"mDataProp": "t151HargaTanpaPPN",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"mRender": function ( data, type, row ) {
        if(data != null)
        return '<span class="pull-right numeric">'+data+'</span>';
        else
        return '<span></span>';
    },
	"bVisible": true
}

,

{
	"sName": "t151HargaDenganPPN",
	"mDataProp": "t151HargaDenganPPN",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"mRender": function ( data, type, row ) {
        if(data != null)
        return '<span class="pull-right numeric">'+data+'</span>';
        else
        return '<span></span>';
    },
	"bVisible": true
}

,

{
	"sName": "satuan",
	"mDataProp": "satuan",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}
	
						var kategori = $('#filter_kategori input').val();
						if(kategori){
							aoData.push(
									{"name": 'sCriteria_kategori', "value": kategori}
							);
						}

						var t151TMT = $('#search_t151TMT').val();
						var t151TMTDay = $('#search_t151TMT_day').val();
						var t151TMTMonth = $('#search_t151TMT_month').val();
						var t151TMTYear = $('#search_t151TMT_year').val();
						
						if(t151TMT){
							aoData.push(
									{"name": 'sCriteria_t151TMT', "value": "date.struct"},
									{"name": 'sCriteria_t151TMT_dp', "value": t151TMT},
									{"name": 'sCriteria_t151TMT_day', "value": t151TMTDay},
									{"name": 'sCriteria_t151TMT_month', "value": t151TMTMonth},
									{"name": 'sCriteria_t151TMT_year', "value": t151TMTYear}
							);
						}
	
						var t151HargaTanpaPPN = $('#filter_t151HargaTanpaPPN input').val();
						if(t151HargaTanpaPPN){
							aoData.push(
									{"name": 'sCriteria_t151HargaTanpaPPN', "value": t151HargaTanpaPPN}
							);
						}
	
						var t151HargaDenganPPN = $('#filter_t151HargaDenganPPN input').val();
						if(t151HargaDenganPPN){
							aoData.push(
									{"name": 'sCriteria_t151HargaDenganPPN', "value": t151HargaDenganPPN}
							);
						}
	
						var satuan = $('#filter_satuan input').val();
						if(satuan){
							aoData.push(
									{"name": 'sCriteria_satuan', "value": satuan}
							);
						}
	
						var goods = $('#filter_goods input').val();
						if(goods){
							aoData.push(
									{"name": 'sCriteria_goods', "value": goods}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							$('td span.numeric').text(function(index, text) {
                                    return $.number(text,0);
                                    });
							   }
						});
		}			
	});
		$('.select-all').click(function(e) {

        $("#goodsHargaJual_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsGoodsHargaJualPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsGoodsHargaJualPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#goodsHargaJual_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsGoodsHargaJualPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anGoodsHargaJualSelected = goodsHargaJualTable.$('tr.row_selected');
            if(jmlRecGoodsHargaJualPerPage == anGoodsHargaJualSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsGoodsHargaJualPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
