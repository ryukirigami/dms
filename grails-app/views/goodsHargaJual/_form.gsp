<%@ page import="com.kombos.parts.GoodsHargaJual" %>

<g:javascript>
var showGoods;

$(function(){

        showGoods = function() {
    			$("#src_koGo").val("");
    			$("#src_naGo").val("");
    			$("#src_satGo").val("");

    			nomorDOTable.fnDraw();

                $("#pilihGoodsModal").modal({
                        "backdrop" : "dynamic",
                        "keyboard" : true,
                        "show" : true
                    }).css({'width': '800px','margin-left': function () {return -($(this).width() / 2);}});
            }


			$('#namaGoods').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/goodsHargaJual/listParts', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});

		function detailParts(){
                var noGoods = $('#namaGoods').val();
                $.ajax({
                    url:'${request.contextPath}/goodsHargaJual/detailParts?noGoods='+noGoods,
                    type: "POST", // Always use POST when deleting data
                    success : function(data){
                        $('#idGoods').val("");
                        	console.log(data.hasil);
                        	console.log(data.id);
                        if(data.hasil=="ada"){
                            alert(data.hasil)
                            alert(data.id)
                            $('#idGoods').val(data.id);
                        }
                            console.log("idGoods : " + data.id)
                    }
                })
        }


    jQuery(function($) {
          $(".numeric").autoNumeric("init", {
            vMin: '-999999999.99',
             aSep:',',
            vMax: '99999999999.99'
        });
    });



    function isNumberKeyHargaTanpaPPN(evt){
        if($("#t151HargaTanpaPPN").val()!="" && $("#t151HargaTanpaPPN").val()!=null){
            $.ajax({
                url: '${request.contextPath}/goodsHargaJual/getPPN',
                type: 'GET',
                dataType: 'json',
                success: function(data, textStatus, xhr) {
                    if(data==-1){
                        alert("Data PPN di general parameter belum tersedia");
                    } else if(data==0){
                        var hargaNoPPN = $("#t151HargaTanpaPPN").val();
                        $("#t151HargaDenganPPN").attr("value", hargaNoPPN);
                    }else{
                        var hargaNoPPN = $("#t151HargaTanpaPPN").val().replace(/,/g,"");
                        var angka=parseFloat(hargaNoPPN)
                        var hargaPPN = angka+(angka*data/100);
                        $("#t151HargaDenganPPN").attr("value", hargaPPN);
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    alert(textStatus);
                }
            });
        }else{
            $("#t151HargaDenganPPN").attr("value", "");
            return false;
        }
    }

    function isNumberKeyHargaPPN(evt){
        if($("#t151HargaDenganPPN").val()!="" && $("#t151HargaDenganPPN").val()!=null){
            $.ajax({
                url: '${request.contextPath}/goodsHargaJual/getPPN',
                type: 'GET',
                dataType: 'json',
                success: function(data, textStatus, xhr) {
                    if(data==-1){
                        alert("Data PPN di general parameter belum tersedia");
                    } else if(data==0){
                        var hargaPPN = $("#t151HargaDenganPPN").val();
                        $("#t151HargaTanpaPPN").attr("value", hargaPPN);
                    }else{
                        var hargaPPN = $("#t151HargaDenganPPN").val().replace(/,/g,"");
                        var angka=parseFloat(hargaPPN)
                        var hargaNoPPN = angka - (angka*data/(100+data));
                        $("#t151HargaTanpaPPN").attr("value", hargaNoPPN);
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    alert(textStatus);
                }
            });
        } else {
            $("#t151HargaTanpaPPN").attr("value", "");
            return false;
        }
    }
</g:javascript>


<div class="control-group fieldcontain ${hasErrors(bean: goodsHargaJualInstance, field: 'goods', 'error')} required">
    <label class="control-label" for="goods">
        <g:message code="goodsHargaJual.goods.label" default="Goods" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        %{--<g:textField class="typeahead" autocomplete="off" id="goods" name="goods" optionKey="id" value="${goodsDiskonInstance?.goods?.m111Nama}"/>--}%
        %{--<g:textField name="namaGoods" id="namaGoods" class="typeahead" maxlength="11" value="${goodsHargaJualInstance?.goods?.m111ID?goodsHargaJualInstance?.goods?.m111ID?.trim()+' || '+goodsHargaJualInstance?.goods?.m111Nama:""}" autocomplete="off" onblur="detailParts();"/>--}%
        %{--<g:hiddenField name="goodsId" id="idGoods"  value="${goodsHargaJualInstance?.goods?.id}" />--}%

        %{--<g:hiddenField name="idGoods" id="idGoods" required="" value="${klasifikasiGoodsInstance?.goods?.id}" />--}%
        <g:hiddenField name="goodsId" id="idGoods" required="" value="${goodsHargaJualInstance?.goods?.id}" />

        <input id="good" type="text" readonly="readonly" value="${goodsHargaJualInstance?.goods ? goodsHargaJualInstance?.goods?.m111ID?.trim()+" | "+goodsHargaJualInstance?.goods?.m111Nama?.trim():""}"/>
        <div class="input-append">

            <g:if test="${!goodsHargaJualInstance?.goods?.id}">
                <button type="button" class="btn" onclick="showGoods();">
                    <i class="icon-search"></i>
                </button>
            </g:if>


        </div>

    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: goodsHargaJualInstance, field: 't151TMT', 'error')} required">
    <label class="control-label" for="t151TMT">
        <g:message code="goodsHargaJual.t151TMT.label" default="Tanggal Berlaku" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <ba:datePicker name="t151TMT" precision="day"  value="${goodsHargaJualInstance?.t151TMT}" required="true" format="dd/MM/yyyy"  />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: goodsHargaJualInstance, field: 't151HargaTanpaPPN', 'error')} required">
	<label class="control-label" for="t151HargaTanpaPPN">
		<g:message code="goodsHargaJual.t151HargaTanpaPPN.label" default="Harga Jual" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField id="t151HargaTanpaPPN" maxlength="8" class="numeric" name="t151HargaTanpaPPN" value="${fieldValue(bean: goodsHargaJualInstance, field: 't151HargaTanpaPPN')}" required=""/>
	</div>
</div>

%{--<div class="control-group fieldcontain ${hasErrors(bean: goodsHargaJualInstance, field: 't151HargaDenganPPN', 'error')} required">--}%
	%{--<label class="control-label" for="t151HargaDenganPPN">--}%
		%{--<g:message code="goodsHargaJual.t151HargaDenganPPN.label" default="Harga Jual PPN" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	<g:hiddenField id="t151HargaDenganPPN" maxlength="8" class="numeric" name="t151HargaDenganPPN" value="1" required=""/>
	%{--</div>--}%
%{--</div>--}%

<div id="pilihGoodsModal" class="modal hide">
    <div class="modal-content">
        <div class="modal-body">
            <g:render template="showGoods"/>
    </div>

        <div class="modal-footer">
            <a class="btn btn-primary" id="btnPilih" data-dismiss = "modal">Pilih</a>
        </div>
    </div>
</div>