
<%@ page import="com.kombos.parts.KlasifikasiGoods" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="klasifikasiGoods_datatables_${idTable}" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="stokOPName.goods.kode.label" default="Kode Part" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="stokOPName.goods.nama.label" default="Nama Part" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="stokOPName.goods.lokasi.label" default="Location" /></div>
        </th>

    </tr>
    <tr>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_kode_goods_popup" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_kode_goods_popup" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_nama_goods_popup" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_nama_goods_popup" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_lokasi_goods_popup" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input  type="text" name="search_lokasi_goods_popup" class="search_init" />
            </div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var klasifikasiGoodsTable;
var reloadKlasifikasiGoodsTable;
var tabelVar=klasifikasiGoodsTable;
$(function(){

	reloadKlasifikasiGoodsTable = function() {
		klasifikasiGoodsTable.fnDraw();
	}

    var recordsKlasifikasiGoodsPerPage = [];//new Array();
    var anKlasifikasiGoodsSelected;
    var jmlRecKlasifikasiGoodsPerPage=0;
    var id;
	
	    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	klasifikasiGoodsTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

    if(klasifikasiGoodsTable)
    klasifikasiGoodsTable.dataTable().fnDestroy();

    klasifikasiGoodsTable = $('#klasifikasiGoods_datatables_${idTable}').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsKlasifikasiGoods = $("#klasifikasiGoods_datatables_${idTable} tbody .row-select");
            var jmlKlasifikasiGoodsCek = 0;
            var nRow;
            var idRec;
            rsKlasifikasiGoods.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsKlasifikasiGoodsPerPage[idRec]=="1"){
                    jmlKlasifikasiGoodsCek = jmlKlasifikasiGoodsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsKlasifikasiGoodsPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecKlasifikasiGoodsPerPage = rsKlasifikasiGoods.length;
            if(jmlKlasifikasiGoodsCek==jmlRecKlasifikasiGoodsPerPage && jmlRecKlasifikasiGoodsPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [


{
    "sName": "goods",
    "mDataProp": "kodegoods_popup",
    "aTargets": [0],
    "mRender": function ( data, type, row ) {
        return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data//<a href="#" onclick="show('+row['id']+');">'+sCriteria_kode_goods+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
    },
    "bSearchable": true,
    "bSortable": true,
    "sWidth":"300px",
	"bVisible": true
}
,

{
	"sName": "goods",
	"mDataProp": "namagoods_popup",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}
,
{
	"sName": "location",
	"mDataProp": "location_popup",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var kodegoods_popup = $('#filter_kode_goods_popup input').val();
						if(kodegoods_popup){
							aoData.push(
									{"name": 'sCriteria_kode_goods_popup', "value": kodegoods_popup}
							);
						}

						var namagoods_popup = $('#filter_nama_goods_popup input').val();
						if(namagoods_popup){
							aoData.push(
									{"name": 'sCriteria_nama_goods_popup', "value": namagoods_popup}
							);
						}

						var lokasigoods_popup = $('#filter_lokasi_goods_popup input').val();
						if(lokasigoods_popup){
							aoData.push(
									{"name": 'sCriteria_lokasi_goods_popup', "value": lokasigoods_popup}
							);
						}

						var exist =[];
						$("#klasifikasiGoods_datatables_${idTable} tbody .row-select").each(function() {
						    var id = $(this).next("input:hidden").val();
							exist.push(id);
						});

						if(exist.length > 0){
							aoData.push(
										{"name": 'sCriteria_exist', "value": JSON.stringify(exist)}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
	
	$('.select-all').click(function(e) {

        $("#klasifikasiGoods_datatables_${idTable} tbody .row-select").each(function() {
            if(this.checked){
                recordsKlasifikasiGoodsPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsKlasifikasiGoodsPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#klasifikasiGoods_datatables_${idTable} tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsKlasifikasiGoodsPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anKlasifikasiGoodsSelected = klasifikasiGoodsTable.$('tr.row_selected');
            if(jmlRecKlasifikasiGoodsPerPage == anKlasifikasiGoodsSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsKlasifikasiGoodsPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
