
<%@ page import="com.kombos.parts.KlasifikasiGoods" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'pilihklasifikasiGoods.label', default: 'Part')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var showKlasifikasiGoods;
	var loadFormKlasifikasiGoods;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadFormKlasifikasiGoods(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});
});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
	</div>
	<div class="box">
		<div class="span12" id="klasifikasiGoods-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
            <g:field type="button" onclick="tambahKlasifikasiGoods();" class="btn btn-primary create" name="addSelected" id="addSelected" value="${message(code: 'default.button.upload.label', default: 'Add Selected')}" />
		</div>
		<div class="span7" id="klasifikasiGoods-form" style="display: none;"></div>
	</div>
</body>
</html>
