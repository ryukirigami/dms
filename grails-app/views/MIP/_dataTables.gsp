
<%@ page import="com.kombos.parts.MIP" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>
<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
        return true;
    }

</g:javascript>

<table id="MIP_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="MIP.m160TglBerlaku.label" default="Tanggal Berlaku" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="MIP.m160RangeDAD.label" default="Range Data DAD" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="MIP.m160OrderCycle.label" default="Order Cycle" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="MIP.m160LeadTime.label" default="Lead Time" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="MIP.m160SSLeadTime.label" default="SS Lead Time" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="MIP.m160SSDemand.label" default="SS Demand" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="MIP.m160DeadStock.label" default="Dead Stock" /></div>
			</th>


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="MIP.m160ID.label" default="M160 ID" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="MIP.companyDealer.label" default="Company Dealer" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="MIP.createdBy.label" default="Created By" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="MIP.updatedBy.label" default="Updated By" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="MIP.lastUpdProcess.label" default="Last Upd Process" /></div>--}%
			%{--</th>--}%

		
		</tr>
		<tr>
		

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m160TglBerlaku" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m160TglBerlaku" value="date.struct">
					<input type="hidden" name="search_m160TglBerlaku_day" id="search_m160TglBerlaku_day" value="">
					<input type="hidden" name="search_m160TglBerlaku_month" id="search_m160TglBerlaku_month" value="">
					<input type="hidden" name="search_m160TglBerlaku_year" id="search_m160TglBerlaku_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m160TglBerlaku_dp" value="" id="search_m160TglBerlaku" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m160RangeDAD" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m160RangeDAD" onkeypress="return isNumberKey(event)" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m160OrderCycle" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m160OrderCycle" onkeypress="return isNumberKey(event)" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m160LeadTime" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m160LeadTime" onkeypress="return isNumberKey(event)" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m160SSLeadTime" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m160SSLeadTime" onkeypress="return isNumberKey(event)" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m160SSDemand" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m160SSDemand" onkeypress="return isNumberKey(event)" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m160DeadStock" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m160DeadStock" onkeypress="return isNumberKey(event)" class="search_init" />
				</div>
			</th>
	
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_m160ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_m160ID" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_companyDealer" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_createdBy" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_updatedBy" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_lastUpdProcess" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%


		</tr>
	</thead>
</table>

<g:javascript>
var MIPTable;
var reloadMIPTable;
$(function(){
	
	reloadMIPTable = function() {
		MIPTable.fnDraw();
	}

	var recordsMIPPerPage = [];
    var anMIPSelected;
    var jmlRecMIPPerPage=0;
    var id;
    
	$('#search_m160TglBerlaku').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m160TglBerlaku_day').val(newDate.getDate());
			$('#search_m160TglBerlaku_month').val(newDate.getMonth()+1);
			$('#search_m160TglBerlaku_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			MIPTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	MIPTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	MIPTable = $('#MIP_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsMIP = $("#MIP_datatables tbody .row-select");
            var jmlMIPCek = 0;
            var nRow;
            var idRec;
            rsMIP.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsMIPPerPage[idRec]=="1"){
                    jmlMIPCek = jmlMIPCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsMIPPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecMIPPerPage = rsMIP.length;
            if(jmlMIPCek==jmlRecMIPPerPage && jmlRecMIPPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m160TglBerlaku",
	"mDataProp": "m160TglBerlaku",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m160RangeDAD",
	"mDataProp": "m160RangeDAD",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m160OrderCycle",
	"mDataProp": "m160OrderCycle",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m160LeadTime",
	"mDataProp": "m160LeadTime",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m160SSLeadTime",
	"mDataProp": "m160SSLeadTime",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m160SSDemand",
	"mDataProp": "m160SSDemand",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m160DeadStock",
	"mDataProp": "m160DeadStock",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
//
//,
//
//{
//	"sName": "m160ID",
//	"mDataProp": "m160ID",
//	"aTargets": [7],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "companyDealer",
//	"mDataProp": "companyDealer",
//	"aTargets": [8],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "createdBy",
//	"mDataProp": "createdBy",
//	"aTargets": [9],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "updatedBy",
//	"mDataProp": "updatedBy",
//	"aTargets": [10],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "lastUpdProcess",
//	"mDataProp": "lastUpdProcess",
//	"aTargets": [11],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var m160TglBerlaku = $('#search_m160TglBerlaku').val();
						var m160TglBerlakuDay = $('#search_m160TglBerlaku_day').val();
						var m160TglBerlakuMonth = $('#search_m160TglBerlaku_month').val();
						var m160TglBerlakuYear = $('#search_m160TglBerlaku_year').val();
						
						if(m160TglBerlaku){
							aoData.push(
									{"name": 'sCriteria_m160TglBerlaku', "value": "date.struct"},
									{"name": 'sCriteria_m160TglBerlaku_dp', "value": m160TglBerlaku},
									{"name": 'sCriteria_m160TglBerlaku_day', "value": m160TglBerlakuDay},
									{"name": 'sCriteria_m160TglBerlaku_month', "value": m160TglBerlakuMonth},
									{"name": 'sCriteria_m160TglBerlaku_year', "value": m160TglBerlakuYear}
							);
						}
	
						var m160RangeDAD = $('#filter_m160RangeDAD input').val();
						if(m160RangeDAD){
							aoData.push(
									{"name": 'sCriteria_m160RangeDAD', "value": m160RangeDAD}
							);
						}
	
						var m160OrderCycle = $('#filter_m160OrderCycle input').val();
						if(m160OrderCycle){
							aoData.push(
									{"name": 'sCriteria_m160OrderCycle', "value": m160OrderCycle}
							);
						}
	
						var m160LeadTime = $('#filter_m160LeadTime input').val();
						if(m160LeadTime){
							aoData.push(
									{"name": 'sCriteria_m160LeadTime', "value": m160LeadTime}
							);
						}
	
						var m160SSLeadTime = $('#filter_m160SSLeadTime input').val();
						if(m160SSLeadTime){
							aoData.push(
									{"name": 'sCriteria_m160SSLeadTime', "value": m160SSLeadTime}
							);
						}
	
						var m160SSDemand = $('#filter_m160SSDemand input').val();
						if(m160SSDemand){
							aoData.push(
									{"name": 'sCriteria_m160SSDemand', "value": m160SSDemand}
							);
						}
	
						var m160DeadStock = $('#filter_m160DeadStock input').val();
						if(m160DeadStock){
							aoData.push(
									{"name": 'sCriteria_m160DeadStock', "value": m160DeadStock}
							);
						}
	
						var m160ID = $('#filter_m160ID input').val();
						if(m160ID){
							aoData.push(
									{"name": 'sCriteria_m160ID', "value": m160ID}
							);
						}
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
	$('.select-all').click(function(e) {

        $("#MIP_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsMIPPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsMIPPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#MIP_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsMIPPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anMIPSelected = MIPTable.$('tr.row_selected');
            if(jmlRecMIPPerPage == anMIPSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsMIPPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
