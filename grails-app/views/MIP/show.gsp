

<%@ page import="com.kombos.parts.MIP" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'MIP.label', default: 'MIP')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteMIP;

$(function(){ 
	deleteMIP=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/MIP/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadMIPTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-MIP" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="MIP"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${MIPInstance?.m160TglBerlaku}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m160TglBerlaku-label" class="property-label"><g:message
					code="MIP.m160TglBerlaku.label" default="Tanggal Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m160TglBerlaku-label">
						%{--<ba:editableValue
								bean="${MIPInstance}" field="m160TglBerlaku"
								url="${request.contextPath}/MIP/updatefield" type="text"
								title="Enter m160TglBerlaku" onsuccess="reloadMIPTable();" />--}%
							
								<g:formatDate date="${MIPInstance?.m160TglBerlaku}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${MIPInstance?.m160RangeDAD}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m160RangeDAD-label" class="property-label"><g:message
					code="MIP.m160RangeDAD.label" default="Range Data DAD" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m160RangeDAD-label">
						%{--<ba:editableValue
								bean="${MIPInstance}" field="m160RangeDAD"
								url="${request.contextPath}/MIP/updatefield" type="text"
								title="Enter m160RangeDAD" onsuccess="reloadMIPTable();" />--}%
							
								<g:fieldValue bean="${MIPInstance}" field="m160RangeDAD"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${MIPInstance?.m160OrderCycle}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m160OrderCycle-label" class="property-label"><g:message
					code="MIP.m160OrderCycle.label" default="Order Cycle" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m160OrderCycle-label">
						%{--<ba:editableValue
								bean="${MIPInstance}" field="m160OrderCycle"
								url="${request.contextPath}/MIP/updatefield" type="text"
								title="Enter m160OrderCycle" onsuccess="reloadMIPTable();" />--}%
							
								<g:fieldValue bean="${MIPInstance}" field="m160OrderCycle"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${MIPInstance?.m160LeadTime}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m160LeadTime-label" class="property-label"><g:message
					code="MIP.m160LeadTime.label" default="Lead Time" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m160LeadTime-label">
						%{--<ba:editableValue
								bean="${MIPInstance}" field="m160LeadTime"
								url="${request.contextPath}/MIP/updatefield" type="text"
								title="Enter m160LeadTime" onsuccess="reloadMIPTable();" />--}%
							
								<g:fieldValue bean="${MIPInstance}" field="m160LeadTime"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${MIPInstance?.m160SSLeadTime}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m160SSLeadTime-label" class="property-label"><g:message
					code="MIP.m160SSLeadTime.label" default="SS Lead Time" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m160SSLeadTime-label">
						%{--<ba:editableValue
								bean="${MIPInstance}" field="m160SSLeadTime"
								url="${request.contextPath}/MIP/updatefield" type="text"
								title="Enter m160SSLeadTime" onsuccess="reloadMIPTable();" />--}%
							
								<g:fieldValue bean="${MIPInstance}" field="m160SSLeadTime"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${MIPInstance?.m160SSDemand}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m160SSDemand-label" class="property-label"><g:message
					code="MIP.m160SSDemand.label" default="SS Demand" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m160SSDemand-label">
						%{--<ba:editableValue
								bean="${MIPInstance}" field="m160SSDemand"
								url="${request.contextPath}/MIP/updatefield" type="text"
								title="Enter m160SSDemand" onsuccess="reloadMIPTable();" />--}%
							
								<g:fieldValue bean="${MIPInstance}" field="m160SSDemand"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${MIPInstance?.m160DeadStock}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m160DeadStock-label" class="property-label"><g:message
					code="MIP.m160DeadStock.label" default="Dead Stock" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m160DeadStock-label">
						%{--<ba:editableValue
								bean="${MIPInstance}" field="m160DeadStock"
								url="${request.contextPath}/MIP/updatefield" type="text"
								title="Enter m160DeadStock" onsuccess="reloadMIPTable();" />--}%
							
								<g:fieldValue bean="${MIPInstance}" field="m160DeadStock"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${MIPInstance?.m160ID}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="m160ID-label" class="property-label"><g:message--}%
					%{--code="MIP.m160ID.label" default="M160 ID" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="m160ID-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${MIPInstance}" field="m160ID"--}%
								%{--url="${request.contextPath}/MIP/updatefield" type="text"--}%
								%{--title="Enter m160ID" onsuccess="reloadMIPTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${MIPInstance}" field="m160ID"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${MIPInstance?.companyDealer}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="companyDealer-label" class="property-label"><g:message--}%
					%{--code="MIP.companyDealer.label" default="Company Dealer" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="companyDealer-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${MIPInstance}" field="companyDealer"--}%
								%{--url="${request.contextPath}/MIP/updatefield" type="text"--}%
								%{--title="Enter companyDealer" onsuccess="reloadMIPTable();" />--}%
							%{----}%
								%{--<g:link controller="companyDealer" action="show" id="${MIPInstance?.companyDealer?.id}">${MIPInstance?.companyDealer?.encodeAsHTML()}</g:link>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%

            <g:if test="${MIPInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="MIP.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${MIPInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/MIP/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadMIPTable();" />--}%

                        <g:fieldValue bean="${MIPInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${MIPInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="MIP.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${MIPInstance}" field="dateCreated"
								url="${request.contextPath}/MIP/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadMIPTable();" />--}%
							
								<g:formatDate date="${MIPInstance?.dateCreated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${MIPInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="MIP.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${MIPInstance}" field="createdBy"
                                url="${request.contextPath}/MIP/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadMIPTable();" />--}%

                        <g:fieldValue bean="${MIPInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${MIPInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="MIP.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${MIPInstance}" field="lastUpdated"
								url="${request.contextPath}/MIP/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadMIPTable();" />--}%
							
								<g:formatDate date="${MIPInstance?.lastUpdated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${MIPInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="MIP.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${MIPInstance}" field="updatedBy"
                                url="${request.contextPath}/MIP/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadMIPTable();" />--}%

                        <g:fieldValue bean="${MIPInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${MIPInstance?.id}"
					update="[success:'MIP-form',failure:'MIP-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteMIP('${MIPInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
