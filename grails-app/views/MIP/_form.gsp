<%@ page import="com.kombos.parts.MIP" %>
<r:require modules="autoNumeric" />
<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</g:javascript>


<div class="control-group fieldcontain ${hasErrors(bean: MIPInstance, field: 'm160TglBerlaku', 'error')} required">
	<label class="control-label" for="m160TglBerlaku">
		<g:message code="MIP.m160TglBerlaku.label" default="Tanggal Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<ba:datePicker name="m160TglBerlaku" precision="day"  value="${MIPInstance?.m160TglBerlaku}"  format="dd/MM/yyyy" required="true"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: MIPInstance, field: 'm160RangeDAD', 'error')} required">
	<label class="control-label" for="m160RangeDAD">
		<g:message code="MIP.m160RangeDAD.label" default="Range Data DAD" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:field name="m160RangeDAD" value="${fieldValue(bean: MIPInstance, field: 'm160RangeDAD')}" required=""/>--}%
    <g:textField name="m160RangeDAD" maxlength="4" onkeypress="return isNumberKey(event)" value="${fieldValue(bean: MIPInstance, field: 'm160RangeDAD')}" required=""/>&nbsp;Hari
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: MIPInstance, field: 'm160OrderCycle', 'error')} required">
	<label class="control-label" for="m160OrderCycle">
		<g:message code="MIP.m160OrderCycle.label" default="Order Cycle" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m160OrderCycle" maxlength="4" onkeypress="return isNumberKey(event)" value="${fieldValue(bean: MIPInstance, field: 'm160OrderCycle')}" required=""/>&nbsp;Hari
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: MIPInstance, field: 'm160LeadTime', 'error')} required">
	<label class="control-label" for="m160LeadTime">
		<g:message code="MIP.m160LeadTime.label" default="Lead Time" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m160LeadTime" maxlength="4" onkeypress="return isNumberKey(event)" value="${fieldValue(bean: MIPInstance, field: 'm160LeadTime')}" required=""/>&nbsp;Hari
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: MIPInstance, field: 'm160SSLeadTime', 'error')} required">
	<label class="control-label" for="m160SSLeadTime">
		<g:message code="MIP.m160SSLeadTime.label" default="SS Lead Time" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m160SSLeadTime" maxlength="4" onkeypress="return isNumberKey(event)" value="${fieldValue(bean: MIPInstance, field: 'm160SSLeadTime')}" required=""/>&nbsp;Hari
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: MIPInstance, field: 'm160SSDemand', 'error')} required">
	<label class="control-label" for="m160SSDemand">
		<g:message code="MIP.m160SSDemand.label" default="SS Demand" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m160SSDemand" maxlength="4" onkeypress="return isNumberKey(event)" value="${fieldValue(bean: MIPInstance, field: 'm160SSDemand')}" required=""/>&nbsp;Hari
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: MIPInstance, field: 'm160DeadStock', 'error')} required">
	<label class="control-label" for="m160DeadStock">
		<g:message code="MIP.m160DeadStock.label" default="Dead Stock" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m160DeadStock" maxlength="4" onkeypress="return isNumberKey(event)" value="${fieldValue(bean: MIPInstance, field: 'm160DeadStock')}" required=""/>&nbsp;Tahun
	</div>
</div>

%{--<div class="control-group fieldcontain ${hasErrors(bean: MIPInstance, field: 'm160ID', 'error')} ">--}%
	%{--<label class="control-label" for="m160ID">--}%
		%{--<g:message code="MIP.m160ID.label" default="M160 ID" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:field name="m160ID" type="number" value="${MIPInstance.m160ID}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: MIPInstance, field: 'companyDealer', 'error')} ">--}%
	%{--<label class="control-label" for="companyDealer">--}%
		%{--<g:message code="MIP.companyDealer.label" default="Company Dealer" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:select id="companyDealer" name="companyDealer.id" from="${com.kombos.baseapp.maintable.CompanyDealer.list()}" optionKey="id" value="${MIPInstance?.companyDealer?.id}" class="many-to-one" noSelection="['null': '']"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: MIPInstance, field: 'createdBy', 'error')} ">--}%
	%{--<label class="control-label" for="createdBy">--}%
		%{--<g:message code="MIP.createdBy.label" default="Created By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="createdBy" value="${MIPInstance?.createdBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: MIPInstance, field: 'updatedBy', 'error')} ">--}%
	%{--<label class="control-label" for="updatedBy">--}%
		%{--<g:message code="MIP.updatedBy.label" default="Updated By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="updatedBy" value="${MIPInstance?.updatedBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: MIPInstance, field: 'lastUpdProcess', 'error')} ">--}%
	%{--<label class="control-label" for="lastUpdProcess">--}%
		%{--<g:message code="MIP.lastUpdProcess.label" default="Last Upd Process" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="lastUpdProcess" value="${MIPInstance?.lastUpdProcess}"/>--}%
	%{--</div>--}%
%{--</div>--}%

<g:javascript>
    document.getElementById("m160TglBerlaku").focus();
</g:javascript>