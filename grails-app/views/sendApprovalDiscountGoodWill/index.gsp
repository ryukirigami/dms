<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'delivery.kwitansi.label', default: 'Delivery - Kwitansi')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript>

        $('#search_tglFrom').datepicker().on('changeDate', function(ev) {
       			var newDate = new Date(ev.date);
       			$('#search_tglFrom_day').val(newDate.getDate());
       			$('#search_tglFrom_month').val(newDate.getMonth()+1);
       			$('#search_tglFrom_year').val(newDate.getFullYear());
       			$(this).datepicker('hide');
       	});

       	$('#search_tglTo').datepicker().on('changeDate', function(ev) {
       			var newDate = new Date(ev.date);
       			$('#search_tglTo_day').val(newDate.getDate());
       			$('#search_tglTo_month').val(newDate.getMonth()+1);
       			$('#search_tglTo_year').val(newDate.getFullYear());
       			$(this).datepicker('hide');
       	});

        $(function() {
            $('input[type=radio]').change(function(e) {
                console.log("this.value=" + this.value);
                if (this.value == '0' ) {
                    $('#persenRequest').prop('disabled',false);
                    $('#nominalRequest').val("");
                    $('#nominalRequest').prop('disabled',true);
                } else {
                    $('#persenRequest').val("");
                    $('#persenRequest').prop('disabled',true);
                    $('#nominalRequest').prop('disabled',false);
                }
            });

            onClearSearch = function () {
                $('#checkStatusApproval').prop('checked',false);
                $('#checkNomorInvoice').prop('checked',false);
                $('#checkNomorPolisi').prop('checked',false);
                $('#search_tglFrom').val('');
                $('#search_tglTo').val('');
                $('#nomorInvoiceSearch').val('');
                $('#nomorPolisiSearch').val('');
                $('#nomorInvoiceSearch').prop('disabled',true);
                $('#nomorPolisiSearch').prop('disabled',true);
                reloadSendApprovalTable();
            }

            selectCheckStatusApproval = function() {
           	    if ($('#checkStatusApproval').is(':checked')) {
           	        $('#status_approval').prop('disabled', false);
           	    } else {
           	        $('#status_approval').val('');
           	        $('#status_approval').prop('disabled', true);
           	    }
           	}
            selectCheckNomorInvoice = function() {
                if ($('#checkNomorInvoice').is(':checked')) {
                    $('#nomorInvoiceSearch').prop('disabled', false);
                } else {
                    $('#nomorInvoiceSearch').val('');
                    $('#nomorInvoiceSearch').prop('disabled', true);
                }
            }
            selectCheckNomorPolisi = function() {
                if ($('#checkNomorPolisi').is(':checked')) {
                    $('#nomorPolisiSearch').prop('disabled', false);
                } else {
                    $('#nomorPolisiSearch').val('');
                    $('#nomorPolisiSearch').prop('disabled', true);
                }
            }

            onSendApproval = function(){
                var nomorInvoice = $("#nomorInvoice").val();
                var persenRequest = $("#persenRequest").val();
                var nominalRequest = $("#nominalRequest").val();
                console.log("nomorInvoice=" + nomorInvoice + " persenRequest=" + persenRequest + " nominalRequest=" + nominalRequest);
                if (!nomorInvoice) {
                    alert("Silakan Mengisi Nomor Invoice");
                } else if (!persenRequest && !nominalRequest) {
                    alert("Silahkan Mengisi Persen Request Atau Nominal Request");
                } else {
                    $.ajax({
                        url:'${request.contextPath}/sendApprovalDiscountGoodWill/sendApproval?1=1',
                        type: "POST", // Always use POST when deleting data
                        data : { nomorInvoice: nomorInvoice, persenRequest: persenRequest, nominalRequest: nominalRequest},
                        success : function(data){
                            alert('Save success');
                            var oTable = $('#send_approval_datatables').dataTable();
                            oTable.fnReloadAjax();
                            $('#spinner').fadeIn(1);
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            alert('Internal Server Error');
                        }
                    });
                }
            }

            onSearch = function(){
                var search_tglFrom = $("#search_tglFrom").val();
                var search_tglTo = $("#search_tglTo").val();
                var oTable = $('#send_approval_datatables').dataTable();
                if ((search_tglFrom.length == 0 && search_tglTo.length == 0) ) {
                    oTable.fnReloadAjax();
                } else if ((search_tglFrom.length == 10 && search_tglTo.length == 10)) {
                    var from = search_tglFrom.split("/");
                    var dfrom = new Date(from[2], from[1] - 1, from[0]);
                    console.log("dfrom=" + dfrom);
                    var to = search_tglTo.split("/");
                    var dto = new Date(to[2], to[1] - 1, to[0]);
                    console.log("dto=" + dto);
                    if (dfrom <= dto) {
                        oTable.fnReloadAjax();
                    } else {
                        alert("Tangal Awal Pencarian harus Sebelum Tanggal Akhir Pencarian");
                    }
                } else {
                    if (search_tglFrom.length == 10 && search_tglTo.length == 0) {
                        alert("Silahkan memilih Tanggal Akhir pencarian");
                    } else if (search_tglFrom.length == 0 && search_tglTo.length == 10) {
                        alert("Silahkan memilih Tanggal Awal pencarian");
                    }
                }
            }

        });
    </g:javascript>
</head>

<body>

<div class="box">
    <legend>Send Approval Discount Good Will</legend>
    <table style="width: 60%">
        <tr>
            <td>Nomor Invoice</td>
            <td></td>
            <td>
                <g:textField name="nomorInvoice" id="nomorInvoice"/>
                &nbsp;&nbsp;&nbsp;<button class="btn btn-primary" id="buttonSend" onclick="onSendApproval();">Send Approval Discount Good Will</button>
            </td>
        </tr>
        <tr>
            <td>
                <label class="control-label" for="lbl_RequestBy">
                    Request By
                </label>
            </td>
            <td colspan="2">
                <div id="lbl_RequestBy" class="controls">
                    <g:radioGroup name="requestBy" values="['0','1']" value="0" labels="['Persen','Nominal']">
                        ${it.radio} <g:message code="${it.label}" />
                    </g:radioGroup>
                </div>
            </td>
        </tr>
        <tr>
            <td>Persen Request Discount</td>
            <td></td>
            <td>
                <g:textField name="persenRequest" id="persenRequest" style="width: 70px;"  />&nbsp;%&nbsp;&nbsp;&nbsp;(Maks. 10%)
            </td>
        </tr>
        <tr>
            <td>Nominal Request Discount</td>
            <td></td>
            <td>
                <g:textField name="nominalRequest" id="nominalRequest" style="width: 125px;" disabled="disabled" />&nbsp;(Maks. Rp.500.000)
            </td>
        </tr>
    </table>
</div>

<div class="box">
    <legend>Approval Discount Good Will</legend>
    <table style="width: 80%">
        <tr>
            <td>Tanggal Request</td>
            <td></td>
            <td>
                <input type="hidden" name="search_tglFrom" value="date.struct">
                <input type="hidden" name="search_tglFrom_day" id="search_tglFrom_day" value="">
                <input type="hidden" name="search_tglFrom_month" id="search_tglFrom_month" value="">
                <input type="hidden" name="search_tglFrom_year" id="search_tglFrom_year" value="">
                <input type="text" data-date-format="dd/mm/yyyy" name="search_tglFrom_dp" value="" id="search_tglFrom" class="search_init" style="width: 120px">
                &nbsp;s.d&nbsp;
                <input type="hidden" name="search_tglTo" value="date.struct">
                <input type="hidden" name="search_tglTo_day" id="search_tglTo_day" value="">
                <input type="hidden" name="search_tglTo_month" id="search_tglTo_month" value="">
                <input type="hidden" name="search_tglTo_year" id="search_tglTo_year" value="">
                <input type="text" data-date-format="dd/mm/yyyy" name="search_tglTo_dp" value="" id="search_tglTo" class="search_init" style="width: 120px">

                <button class="btn btn-primary" id="buttonSearch" onclick="onSearch();">Search</button>
                <button class="btn btn-primary" onclick="onClearSearch();">Clear</button>
            </td>
        </tr>
        <tr>
            <td>Status Approval</td>
            <td><g:checkBox name="checkStatusApproval" id="checkStatusApproval" value="checkStatusApproval" onclick="selectCheckStatusApproval();"/></td>
            <td>
                %{--<g:select name="status" from="${com.kombos.parts.StatusApproval?.values()}" keys="${com.kombos.parts.StatusApproval.values()*.name()}" required="" />--}%
                <g:select id="status_approval" name="status_approval" from="${['', 'WAIT_TO_SEND', 'WAIT_FOR_APPROVAL', 'APPROVED', 'REJECTED']}" />
                %{--<g:select style="width:90%" name="namaWasher" from="${statusAprovals}"
                                                                              optionKey="id"
                                                                              optionValue="t015NamaLengkap"/>--}%
            </td>
        </tr>
        <tr>
            <td>Nomor Invoice</td>
            <td><g:checkBox name="checkNomorInvoice" id="checkNomorInvoice" value="checkNomorInvoice" onclick="selectCheckNomorInvoice();"/></td>
            <td>
                <div id="filter_nomorInvoice" style="width: 220px;">
                    <input type="text" name="search_nomorInvoice" class="search_init" />
                </div>
            </td>
        </tr>
        <tr>
            <td>Nomor Polisi</td>
            <td><g:checkBox name="checkNomorPolisi" id="checkNomorPolisi" value="checkNomorPolisi" onclick="selectCheckNomorPolisi();"/></td>
            <td>
                <g:textField name="nomorPolisiSearch" id="nomorPolisiSearch"/>
            </td>
        </tr>
    </table>
    <div class="span12" id="sendApproval-table" style="margin-left: -10px; margin-right: 10px;">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>

        <g:render template="sendApprovalDataTables"/>
    </div>
</div>

</body>
</html>