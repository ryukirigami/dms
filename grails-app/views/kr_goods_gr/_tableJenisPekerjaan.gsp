
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<g:javascript>
</g:javascript>

<table id="jpekerjaan_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="100%">
    <thead>
    <tr>
        <th style="border-bottom: none;padding: 5px;">
            <div>&nbsp;<input type="checkbox" class="select-all" />&nbsp;
                <g:message code="timeTrackingGR.sa.label" default="Jenis Pekerjaan" /></div>
        </th>
    </tr>
    <tr>
        <th style="border-top: none;padding: 5px;">
            <div id="filter_pekerjaan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_pekerjaan" class="search_init" />
            </div>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
var pekerjaanTable;
var reloadPekerjaanTable;
$(function(){
    
    reloadPekerjaanTable = function() {
        pekerjaanTable.fnDraw();
    }

    $("th div input").bind('keypress', function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code == 13) { 
            e.stopPropagation();
            pekerjaanTable.fnDraw();
        }
    });

    $("th div input").click(function (e) {
        e.stopPropagation();
    });

    pekerjaanTable = $('#jpekerjaan_datatables').dataTable({
        "sScrollX": "100%",
        "bScrollCollapse": true,
        "bAutoWidth" : false,
        "bPaginate" : true,
        "sInfo" : "",
        "sInfoEmpty" : "",
        "sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
        bFilter: true,
        "bStateSave": false,
        'sPaginationType': 'bootstrap',
        "fnInitComplete": function () {
            this.fnAdjustColumnSizing(true);
           },
           "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            return nRow;
           },
        "bSort": true,
        "bProcessing": true,
        "bServerSide": true,
        "sServerMethod": "POST",
        "iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
        "sAjaxSource": "${g.createLink(action: "dataTablesJenisPekerjaan")}",
        "aoColumns": [

{
    "sName": "m401NamaTipeKerusakan",
    "mDataProp": "namaPekerjaan",
    "aTargets": [0],
    "mRender": function ( data, type, row ) {
        return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this" name="jenisPekerjaan" value="'+row['id']+'">&nbsp;&nbsp;'+data;
    },
    "bSearchable": true,
    "bSortable": true,
    "sWidth":"100%",
    "bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
            var pekerjaan = $('#filter_pekerjaan input').val();
            if(pekerjaan){
                aoData.push(
                        {"name": 'pekerjaan', "value": pekerjaan}
                );
            }
    
            $.ajax({ "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData ,
                "success": function (json) {
                    fnCallback(json);
                   },
                "complete": function () {
                   }
                    });
        }           
    });
    
});
</g:javascript>


            
