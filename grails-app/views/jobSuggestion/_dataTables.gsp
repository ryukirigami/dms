
<%@ page import="com.kombos.maintable.JobSuggestion" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="jobSuggestion_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="jobSuggestion.t503ID.label" default="ID" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
				<div><g:message code="jobSuggestion.customerVehicle.label" default="Nama Customer" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="jobSuggestion.lastUpdProcess.label" default="No. Polisi" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="jobSuggestion.t503TglJobSuggest.label" default="Tgl Job Suggest" /></div>
            </th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="jobSuggestion.t503KetJobSuggest.label" default="Ket Job Suggest" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="jobSuggestion.t503staJob.label" default="Sta Job" /></div>
			</th>

		</tr>
		<tr>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t503ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t503ID" class="search_init" />
                </div>
            </th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_customerVehicle" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_customerVehicle" class="search_init" />
                </div>
            </th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t503TglJobSuggest" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_t503TglJobSuggest" value="date.struct">
					<input type="hidden" name="search_t503TglJobSuggest_day" id="search_t503TglJobSuggest_day" value="">
					<input type="hidden" name="search_t503TglJobSuggest_month" id="search_t503TglJobSuggest_month" value="">
					<input type="hidden" name="search_t503TglJobSuggest_year" id="search_t503TglJobSuggest_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t503TglJobSuggest_dp" value="" id="search_t503TglJobSuggest" class="search_init">
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t503KetJobSuggest" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t503KetJobSuggest" class="search_init" />
                </div>
            </th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t503staJob" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t503staJob" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var jobSuggestionTable;
var reloadJobSuggestionTable;
$(function(){
	
	reloadJobSuggestionTable = function() {
		jobSuggestionTable.fnDraw();
	}

	
	$('#search_t503TglJobSuggest').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t503TglJobSuggest_day').val(newDate.getDate());
			$('#search_t503TglJobSuggest_month').val(newDate.getMonth()+1);
			$('#search_t503TglJobSuggest_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			jobSuggestionTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	jobSuggestionTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	jobSuggestionTable = $('#jobSuggestion_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () { this.fnAdjustColumnSizing(true); },
		"fnRowCallback" : function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {return nRow;},
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "t503ID",
	"mDataProp": "t503ID",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "namaCustomer",
	"mDataProp": "namaCustomer",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "noPol",
	"mDataProp": "noPol",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}
,
{
	"sName": "t503TglJobSuggest",
	"mDataProp": "t503TglJobSuggest",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "t503KetJobSuggest",
	"mDataProp": "t503KetJobSuggest",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "t503staJob",
	"mDataProp": "t503staJob",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var customerVehicle = $('#filter_customerVehicle input').val();
						if(customerVehicle){
							aoData.push(
									{"name": 'sCriteria_customerVehicle', "value": customerVehicle}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}
	
						var t503ID = $('#filter_t503ID input').val();
						if(t503ID){
							aoData.push(
									{"name": 'sCriteria_t503ID', "value": t503ID}
							);
						}
	
						var t503KetJobSuggest = $('#filter_t503KetJobSuggest input').val();
						if(t503KetJobSuggest){
							aoData.push(
									{"name": 'sCriteria_t503KetJobSuggest', "value": t503KetJobSuggest}
							);
						}

						var t503TglJobSuggest = $('#search_t503TglJobSuggest').val();
						var t503TglJobSuggestDay = $('#search_t503TglJobSuggest_day').val();
						var t503TglJobSuggestMonth = $('#search_t503TglJobSuggest_month').val();
						var t503TglJobSuggestYear = $('#search_t503TglJobSuggest_year').val();
						
						if(t503TglJobSuggest){
							aoData.push(
									{"name": 'sCriteria_t503TglJobSuggest', "value": "date.struct"},
									{"name": 'sCriteria_t503TglJobSuggest_dp', "value": t503TglJobSuggest},
									{"name": 'sCriteria_t503TglJobSuggest_day', "value": t503TglJobSuggestDay},
									{"name": 'sCriteria_t503TglJobSuggest_month', "value": t503TglJobSuggestMonth},
									{"name": 'sCriteria_t503TglJobSuggest_year', "value": t503TglJobSuggestYear}
							);
						}
	
						var t503staJob = $('#filter_t503staJob input').val();
						if(t503staJob){
							aoData.push(
									{"name": 'sCriteria_t503staJob', "value": t503staJob}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
