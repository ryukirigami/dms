<%@ page import="com.kombos.maintable.JobSuggestion" %>

<div class="control-group fieldcontain ${hasErrors(bean: jobSuggestionInstance, field: 't503ID', 'error')} ">
    <label class="control-label" for="t503ID">
        <g:message code="jobSuggestion.t503ID.label" default="No." />

    </label>
    <div class="controls">
        <g:field type="number" name="t503ID" value="${jobSuggestionInstance.t503ID}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: jobSuggestionInstance, field: 'customerVehicle', 'error')} ">
	<label class="control-label" for="customerVehicle">
		<g:message code="jobSuggestion.customerVehicle.label" default="Nama Costumer" />
	</label>
	<div class="controls">
    <g:textField id="customerVehicle" name="customerVehicle.id" value="${jobSuggestionInstance?.customerVehicle}" class="many-to-one"/>
	%{--<g:select id="customerVehicle" name="customerVehicle.id" from="${com.kombos.customerprofile.CustomerVehicle.list()}" optionKey="id" required="" value="${jobSuggestionInstance?.customerVehicle?.id}" class="many-to-one"/>--}%
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: jobSuggestionInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="jobSuggestion.lastUpdProcess.label" default="No. Pol." />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${jobSuggestionInstance?.lastUpdProcess}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: jobSuggestionInstance, field: 't503TglJobSuggest', 'error')} ">
	<label class="control-label" for="t503TglJobSuggest">
		<g:message code="jobSuggestion.t503TglJobSuggest.label" default="Tgl. Service" />
	</label>
	<div class="controls">
	<ba:datePicker name="t503TglJobSuggest" precision="day" value="${jobSuggestionInstance?.t503TglJobSuggest}" format="yyyy-MM-dd"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: jobSuggestionInstance, field: 't503KetJobSuggest', 'error')} ">
    <label class="control-label" for="t503KetJobSuggest">
        <g:message code="jobSuggestion.t503KetJobSuggest.label" default="Job Suggest" />

    </label>
    <div class="controls">
        <g:textField name="t503KetJobSuggest" value="${jobSuggestionInstance?.t503KetJobSuggest}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: jobSuggestionInstance, field: 't503staJob', 'error')} ">
	<label class="control-label" for="t503staJob">
		<g:message code="jobSuggestion.t503staJob.label" default="Status Job" />
		
	</label>
	<div class="controls">
	<g:textField name="t503staJob" value="${jobSuggestionInstance?.t503staJob}" />
	</div>
</div>

