

<%@ page import="com.kombos.maintable.JobSuggestion" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'jobSuggestion.label', default: 'JobSuggestion')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteJobSuggestion;

$(function(){ 
	deleteJobSuggestion=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/jobSuggestion/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadJobSuggestionTable();
   				expandTableLayout('jobSuggestion');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-jobSuggestion" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>

		<table id="jobSuggestion" class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${jobSuggestionInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="jobSuggestion.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
								<g:fieldValue bean="${jobSuggestionInstance}" field="createdBy"/>
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${jobSuggestionInstance?.t503ID}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t503ID-label" class="property-label"><g:message
                                code="jobSuggestion.t503ID.label" default="No." />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t503ID-label">

                        <g:fieldValue bean="${jobSuggestionInstance}" field="t503ID"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${jobSuggestionInstance?.customerVehicle}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="customerVehicle-label" class="property-label"><g:message
					code="jobSuggestion.customerVehicle.label" default="Nama Customer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="customerVehicle-label">
						%{--<ba:editableValue
								bean="${jobSuggestionInstance}" field="customerVehicle"
								url="${request.contextPath}/JobSuggestion/updatefield" type="text"
								title="Enter customerVehicle" onsuccess="reloadJobSuggestionTable();" />--}%
							
								<g:link controller="customerVehicle" action="show" id="${jobSuggestionInstance?.customerVehicle?.id}">${jobSuggestionInstance?.customerVehicle?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${jobSuggestionInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="jobSuggestion.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${jobSuggestionInstance}" field="dateCreated"
								url="${request.contextPath}/JobSuggestion/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadJobSuggestionTable();" />--}%
							
								<g:formatDate date="${jobSuggestionInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${jobSuggestionInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="jobSuggestion.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${jobSuggestionInstance}" field="lastUpdProcess"
								url="${request.contextPath}/JobSuggestion/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadJobSuggestionTable();" />--}%
							
								<g:fieldValue bean="${jobSuggestionInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${jobSuggestionInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="jobSuggestion.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${jobSuggestionInstance}" field="lastUpdated"
								url="${request.contextPath}/JobSuggestion/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadJobSuggestionTable();" />--}%
							
								<g:formatDate date="${jobSuggestionInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${jobSuggestionInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;">
                  <span id="staDel-label" class="property-label">
                      <g:message code="jobSuggestion.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3">
                        <span class="property-value" aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${jobSuggestionInstance}" field="staDel"
								url="${request.contextPath}/JobSuggestion/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadJobSuggestionTable();" />--}%
							
								<g:fieldValue bean="${jobSuggestionInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${jobSuggestionInstance?.t503KetJobSuggest}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t503KetJobSuggest-label" class="property-label"><g:message
					code="jobSuggestion.t503KetJobSuggest.label" default="Ket Job Suggest" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t503KetJobSuggest-label">
						%{--<ba:editableValue
								bean="${jobSuggestionInstance}" field="t503KetJobSuggest"
								url="${request.contextPath}/JobSuggestion/updatefield" type="text"
								title="Enter t503KetJobSuggest" onsuccess="reloadJobSuggestionTable();" />--}%
							
								<g:fieldValue bean="${jobSuggestionInstance}" field="t503KetJobSuggest"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${jobSuggestionInstance?.t503TglJobSuggest}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t503TglJobSuggest-label" class="property-label"><g:message
					code="jobSuggestion.t503TglJobSuggest.label" default="Tgl Job Suggest" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t503TglJobSuggest-label">
						%{--<ba:editableValue
								bean="${jobSuggestionInstance}" field="t503TglJobSuggest"
								url="${request.contextPath}/JobSuggestion/updatefield" type="text"
								title="Enter t503TglJobSuggest" onsuccess="reloadJobSuggestionTable();" />--}%
							
								<g:formatDate date="${jobSuggestionInstance?.t503TglJobSuggest}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${jobSuggestionInstance?.t503staJob}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t503staJob-label" class="property-label"><g:message
					code="jobSuggestion.t503staJob.label" default="Sta Job" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t503staJob-label">
						%{--<ba:editableValue
								bean="${jobSuggestionInstance}" field="t503staJob"
								url="${request.contextPath}/JobSuggestion/updatefield" type="text"
								title="Enter t503staJob" onsuccess="reloadJobSuggestionTable();" />--}%
							
								<g:fieldValue bean="${jobSuggestionInstance}" field="t503staJob"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${jobSuggestionInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="jobSuggestion.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${jobSuggestionInstance}" field="updatedBy"
								url="${request.contextPath}/JobSuggestion/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadJobSuggestionTable();" />--}%
							
								<g:fieldValue bean="${jobSuggestionInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>

		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('jobSuggestion');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${jobSuggestionInstance?.id}"
					update="[success:'jobSuggestion-form',failure:'jobSuggestion-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteJobSuggestion('${jobSuggestionInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
