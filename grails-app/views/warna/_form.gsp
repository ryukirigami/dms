<%@ page import="com.kombos.customerprofile.Warna" %>



<div class="control-group fieldcontain ${hasErrors(bean: warnaInstance, field: 'm092ID', 'error')} required">
	<label class="control-label" for="m092ID">
		<g:message code="warna.m092ID.label" default="Kode Warna" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:textField name="m092ID" maxlength="4" required="" value="${warnaInstance?.m092ID}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: warnaInstance, field: 'm092NamaWarna', 'error')} required">
	<label class="control-label" for="m092NamaWarna">
		<g:message code="warna.m092NamaWarna.label" default="Nama Warna" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m092NamaWarna" maxlength="20" required="" value="${warnaInstance?.m092NamaWarna}"/>
	</div>
</div>

<g:javascript>
    document.getElementById("m092ID").focus();
</g:javascript>

%{--<div class="control-group fieldcontain ${hasErrors(bean: warnaInstance, field: 'staDel', 'error')} required">--}%
	%{--<label class="control-label" for="staDel">--}%
		%{--<g:message code="warna.staDel.label" default="Sta Del" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="staDel" required="" value="${warnaInstance?.staDel}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: warnaInstance, field: 'createdBy', 'error')} ">--}%
	%{--<label class="control-label" for="createdBy">--}%
		%{--<g:message code="warna.createdBy.label" default="Created By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="createdBy" value="${warnaInstance?.createdBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: warnaInstance, field: 'updatedBy', 'error')} ">--}%
	%{--<label class="control-label" for="updatedBy">--}%
		%{--<g:message code="warna.updatedBy.label" default="Updated By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="updatedBy" value="${warnaInstance?.updatedBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: warnaInstance, field: 'lastUpdProcess', 'error')} ">--}%
	%{--<label class="control-label" for="lastUpdProcess">--}%
		%{--<g:message code="warna.lastUpdProcess.label" default="Last Upd Process" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="lastUpdProcess" value="${warnaInstance?.lastUpdProcess}"/>--}%
	%{--</div>--}%
%{--</div>--}%

