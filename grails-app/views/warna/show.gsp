

<%@ page import="com.kombos.customerprofile.Warna" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'warna.label', default: 'Warna')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteWarna;

$(function(){ 
	deleteWarna=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/warna/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadWarnaTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-warna" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="warna"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${warnaInstance?.m092ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m092ID-label" class="property-label"><g:message
					code="warna.m092ID.label" default="Kode Warna" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m092ID-label">
						%{--<ba:editableValue
								bean="${warnaInstance}" field="m092ID"
								url="${request.contextPath}/Warna/updatefield" type="text"
								title="Enter m092ID" onsuccess="reloadWarnaTable();" />--}%
							
								<g:fieldValue bean="${warnaInstance}" field="m092ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${warnaInstance?.m092NamaWarna}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m092NamaWarna-label" class="property-label"><g:message
					code="warna.m092NamaWarna.label" default="Nama Warna" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m092NamaWarna-label">
						%{--<ba:editableValue
								bean="${warnaInstance}" field="m092NamaWarna"
								url="${request.contextPath}/Warna/updatefield" type="text"
								title="Enter m092NamaWarna" onsuccess="reloadWarnaTable();" />--}%
							
								<g:fieldValue bean="${warnaInstance}" field="m092NamaWarna"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${warnaInstance?.staDel}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="staDel-label" class="property-label"><g:message--}%
					%{--code="warna.staDel.label" default="Sta Del" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="staDel-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${warnaInstance}" field="staDel"--}%
								%{--url="${request.contextPath}/Warna/updatefield" type="text"--}%
								%{--title="Enter staDel" onsuccess="reloadWarnaTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${warnaInstance}" field="staDel"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%

            <g:if test="${warnaInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="warna.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${warnaInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/Warna/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadWarnaTable();" />--}%

                        <g:fieldValue bean="${warnaInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${warnaInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="warna.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${warnaInstance}" field="dateCreated"
								url="${request.contextPath}/Warna/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadWarnaTable();" />--}%
							
								<g:formatDate date="${warnaInstance?.dateCreated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${warnaInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="warna.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${warnaInstance}" field="createdBy"
                                url="${request.contextPath}/Warna/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadWarnaTable();" />--}%

                        <g:fieldValue bean="${warnaInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${warnaInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="warna.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${warnaInstance}" field="lastUpdated"
								url="${request.contextPath}/Warna/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadWarnaTable();" />--}%
							
								<g:formatDate date="${warnaInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${warnaInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="warna.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${warnaInstance}" field="updatedBy"
                                url="${request.contextPath}/Warna/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadWarnaTable();" />--}%

                        <g:fieldValue bean="${warnaInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${warnaInstance?.id}"
					update="[success:'warna-form',failure:'warna-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteWarna('${warnaInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
