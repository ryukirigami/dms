<%@ page import="com.kombos.administrasi.CompanyDealer; com.kombos.administrasi.JenisJamKerja" %>

<g:javascript>


$(document).ready(function()
{
    $("#m030JamMulai1_hour").change(function(){
        if(parseInt($('#m030JamSelesai1_hour').val()) <= parseInt($('#m030JamMulai1_hour').val()) && parseInt($('#m030JamMulai1_minute').val()) == '59' ){
            $("#m030JamSelesai1_hour").val(parseInt($('#m030JamMulai1_hour').val())+1);
            $('#m030JamSelesai1_minute').val('00');
        }
        else if(parseInt($('#m030JamSelesai1_hour').val()) < parseInt($('#m030JamMulai1_hour').val())){
            $("#m030JamSelesai1_hour").val($('#m030JamMulai1_hour').val());
            $('#m030JamSelesai1_minute').val($('#m030JamMulai1_minute').val());
        }
        else if(parseInt($("#m030JamSelesai1_hour").val()) == parseInt($("#m030JamMulai1_hour").val())
                && parseInt($("#m030JamSelesai1_minute").val()) < parseInt($("#m030JamMulai1_minute").val())){
            $('#m030JamSelesai1_minute').val(parseInt($('#m030JamMulai1_minute').val())+1);
        }
    });

    $("#m030JamMulai1_minute").change(function(){
        if(parseInt($('#m030JamSelesai1_hour').val()) == parseInt($('#m030JamMulai1_hour').val()) && parseInt($('#m030JamMulai1_minute').val()) == '59' ){
            $("#m030JamSelesai1_hour").val(parseInt($('#m030JamMulai1_hour').val())+1);
            $('#m030JamSelesai1_minute').val('00');
        }else if(parseInt($('#m030JamSelesai1_hour').val()) == parseInt($('#m030JamMulai1_hour').val())
                && parseInt($('#m030JamSelesai1_minute').val()) <= parseInt($('#m030JamMulai1_minute').val())){
            $('#m030JamSelesai1_minute').val(parseInt($('#m030JamMulai1_minute').val()));
        }
    });

    $('#m030JamSelesai1_hour').change(function(){
        if(parseInt($('#m030JamSelesai1_hour').val()) <= parseInt($('#m030JamMulai1_hour').val()) && parseInt($('#m030JamMulai1_minute').val()) == '59' ){
            $("#m030JamSelesai1_hour").val(parseInt($('#m030JamMulai1_hour').val())+1);
            $('#m030JamSelesai1_minute').val('00');
        }
        else if(parseInt($('#m030JamSelesai1_hour').val()) < parseInt($('#m030JamMulai1_hour').val())){
            $("#m030JamSelesai1_hour").val($('#m030JamMulai1_hour').val());
            $('#m030JamSelesai1_minute').val(parseInt($('#m030JamMulai1_minute').val()));
        }
    });

    $('#m030JamSelesai1_minute').change(function(){
        if(parseInt($('#m030JamSelesai1_hour').val()) == parseInt($('#m030JamMulai1_hour').val()) && parseInt($('#m030JamMulai1_minute').val()) == '59' ){
            $("#m030JamSelesai1_hour").val(parseInt($('#m030JamMulai1_hour').val())+1);
            $('#m030JamSelesai1_minute').val('00');
        }else if(parseInt($('#m030JamSelesai1_hour').val()) == parseInt($('#m030JamMulai1_hour').val())
                && parseInt($('#m030JamSelesai1_minute').val()) <= parseInt($('#m030JamMulai1_minute').val())){
            $('#m030JamSelesai1_minute').val(parseInt($('#m030JamMulai1_minute').val()));
        }
    });

    $("#m030JamMulai2_hour").change(function(){
        if(parseInt($('#m030JamMulai2_hour').val()) < parseInt($('#m030JamSelesai1_hour').val())){
            $("#m030JamMulai2_hour").val($('#m030JamSelesai1_hour').val());
            $("#m030JamSelesai2_hour").val($('#m030JamMulai2_hour').val());
        }
        else if(parseInt($('#m030JamSelesai2_hour').val()) <= parseInt($('#m030JamMulai2_hour').val()) && parseInt($('#m030JamMulai2_minute').val()) == '59' ){
            $("#m030JamSelesai2_hour").val(parseInt($('#m030JamMulai2_hour').val())+1);
            $('#m030JamSelesai2_minute').val('00');
        }
        else if(parseInt($('#m030JamSelesai2_hour').val()) < parseInt($('#m030JamMulai2_hour').val())){
            $("#m030JamSelesai2_hour").val($('#m030JamMulai2_hour').val());
            $('#m030JamSelesai2_minute').val($('#m030JamMulai2_minute').val());
        }
        else if(parseInt($("#m030JamSelesai2_hour").val()) == parseInt($("#m030JamMulai2_hour").val())
                && parseInt($("#m030JamSelesai2_minute").val()) < parseInt($("#m030JamMulai2_minute").val())){
            $('#m030JamSelesai2_minute').val(parseInt($('#m030JamMulai2_minute').val())+1);
        }
    });

    $("#m030JamMulai2_minute").change(function(){
        if(parseInt($('#m030JamSelesai2_hour').val()) == parseInt($('#m030JamMulai2_hour').val()) && parseInt($('#m030JamMulai2_minute').val()) == '59' ){
            $("#m030JamSelesai2_hour").val(parseInt($('#m030JamMulai2_hour').val())+1);
            $('#m030JamSelesai2_minute').val('00');
        }else if(parseInt($('#m030JamSelesai2_hour').val()) == parseInt($('#m030JamMulai2_hour').val())
                && parseInt($('#m030JamSelesai2_minute').val()) <= parseInt($('#m030JamMulai2_minute').val())){
            $('#m030JamSelesai2_minute').val(parseInt($('#m030JamMulai2_minute').val()));
        }
    });

    $('#m030JamSelesai2_hour').change(function(){
        if(parseInt($('#m030JamSelesai2_hour').val()) <= parseInt($('#m030JamMulai2_hour').val())){
            $("#m030JamSelesai2_hour").val(parseInt($('#m030JamMulai2_hour').val()));
        }
        else if(parseInt($('#m030JamSelesai2_hour').val()) <= parseInt($('#m030JamMulai2_hour').val()) && parseInt($('#m030JamMulai2_minute').val()) == '59' ){
            $("#m030JamSelesai2_hour").val(parseInt($('#m030JamMulai2_hour').val())+1);
            $('#m030JamSelesai2_minute').val('00');
        }
        else if(parseInt($('#m030JamSelesai2_hour').val()) < parseInt($('#m030JamMulai2_hour').val())){
            $("#m030JamSelesai2_hour").val($('#m030JamMulai2_hour').val());
            $('#m030JamSelesai2_minute').val(parseInt($('#m030JamMulai2_minute').val()));
        }
    });

    $('#m030JamSelesai2_minute').change(function(){
        if(parseInt($('#m030JamSelesai2_hour').val()) == parseInt($('#m030JamMulai2_hour').val()) && parseInt($('#m030JamMulai2_minute').val()) == '59' ){
            $("#m030JamSelesai2_hour").val(parseInt($('#m030JamMulai2_hour').val())+1);
            $('#m030JamSelesai2_minute').val('00');
        }else if(parseInt($('#m030JamSelesai2_hour').val()) == parseInt($('#m030JamMulai2_hour').val())
                && parseInt($('#m030JamSelesai2_minute').val()) <= parseInt($('#m030JamMulai2_minute').val())){
            $('#m030JamSelesai2_minute').val(parseInt($('#m030JamMulai2_minute').val()));
        }
    });

    $("#m030JamMulai3_hour").change(function(){
        if(parseInt($('#m030JamMulai3_hour').val()) < parseInt($('#m030JamSelesai2_hour').val())){
            $("#m030JamMulai3_hour").val($('#m030JamSelesai2_hour').val());
            $("#m030JamSelesai3_hour").val($('#m030JamMulai3_hour').val());
        }
        else if(parseInt($('#m030JamSelesai3_hour').val()) <= parseInt($('#m030JamMulai3_hour').val()) && parseInt($('#m030JamMulai3_minute').val()) == '59' ){
            $("#m030JamSelesai3_hour").val(parseInt($('#m030JamMulai3_hour').val())+1);
            $('#m030JamSelesai3_minute').val('00');
        }
        else if(parseInt($('#m030JamSelesai3_hour').val()) < parseInt($('#m030JamMulai3_hour').val())){
            $("#m030JamSelesai3_hour").val($('#m030JamMulai3_hour').val());
            $('#m030JamSelesai3_minute').val(parseInt($('#m030JamMulai3_minute').val()));
        }
        else if(parseInt($("#m030JamSelesai3_hour").val()) == parseInt($("#m030JamMulai3_hour").val())
                && parseInt($("#m030JamSelesai3_minute").val()) < parseInt($("#m030JamMulai3_minute").val())){
            $('#m030JamSelesai3_minute').val($('#m030JamMulai3_minute').val()+1);
        }
    });

    $("#m030JamMulai3_minute").change(function(){
        if(parseInt($('#m030JamSelesai3_hour').val()) == parseInt($('#m030JamMulai3_hour').val()) && parseInt($('#m030JamMulai3_minute').val()) == '59' ){
            $("#m030JamSelesai3_hour").val(parseInt($('#m030JamMulai3_hour').val())+1);
            $('#m030JamSelesai3_minute').val('00');
        }else if(parseInt($('#m030JamSelesai3_hour').val()) == parseInt($('#m030JamMulai3_hour').val())
                && parseInt($('#m030JamSelesai3_minute').val()) <= parseInt($('#m030JamMulai3_minute').val())){
            $('#m030JamSelesai3_minute').val(parseInt($('#m030JamMulai3_minute').val()));
        }
    });

    $('#m030JamSelesai3_hour').change(function(){
        if(parseInt($('#m030JamSelesai3_hour').val()) <= parseInt($('#m030JamMulai3_hour').val()) && parseInt($('#m030JamMulai3_minute').val()) == '59' ){
            $("#m030JamSelesai3_hour").val(parseInt($('#m030JamMulai3_hour').val())+1);
            $('#m030JamSelesai3_minute').val('00');
        }
        else if(parseInt($('#m030JamSelesai3_hour').val()) < parseInt($('#m030JamMulai3_hour').val())){
            $("#m030JamSelesai3_hour").val($('#m030JamMulai3_hour').val());
            $('#m030JamSelesai3_minute').val(parseInt($('#m030JamMulai3_minute').val()));
        }
    });

    $('#m030JamSelesai3_minute').change(function(){
        if(parseInt($('#m030JamSelesai3_hour').val()) == parseInt($('#m030JamMulai3_hour').val()) && parseInt($('#m030JamMulai3_minute').val()) == '59' ){
            $("#m030JamSelesai3_hour").val(parseInt($('#m030JamMulai3_hour').val())+1);
            $('#m030JamSelesai3_minute').val('00');
        }else if(parseInt($('#m030JamSelesai3_hour').val()) == parseInt($('#m030JamMulai3_hour').val())
                && parseInt($('#m030JamSelesai3_minute').val()) <= parseInt($('#m030JamMulai3_minute').val())){
            $('#m030JamSelesai3_minute').val(parseInt($('#m030JamMulai3_minute').val()));
        }
    });

    $("#m030JamMulai4_hour").change(function(){
        if(parseInt($('#m030JamMulai4_hour').val()) < parseInt($('#m030JamSelesai3_hour').val())){
            $("#m030JamMulai4_hour").val($('#m030JamSelesai3_hour').val());
            $("#m030JamSelesai4_hour").val($('#m030JamMulai4_hour').val());
        }
        else if(parseInt($('#m030JamSelesai4_hour').val()) <= parseInt($('#m030JamMulai4_hour').val()) && parseInt($('#m030JamMulai4_minute').val()) == '59' ){
            $("#m030JamSelesai4_hour").val(parseInt($('#m030JamMulai4_hour').val())+1);
            $('#m030JamSelesai4_minute').val('00');
        }
        else if(parseInt($('#m030JamSelesai4_hour').val()) < parseInt($('#m030JamMulai4_hour').val())){
            $("#m030JamSelesai4_hour").val($('#m030JamMulai4_hour').val());
            $('#m030JamSelesai4_minute').val(parseInt($('#m030JamMulai4_minute').val()));
        }
        else if(parseInt($("#m030JamSelesai4_hour").val()) == parseInt($("#m030JamMulai4_hour").val())
                && parseInt($("#m030JamSelesai4_minute").val()) < parseInt($("#m030JamMulai4_minute").val())){
            $('#m030JamSelesai4_minute').val($('#m030JamMulai4_minute').val()+1);
        }
    });

    $("#m030JamMulai4_minute").change(function(){
        if(parseInt($('#m030JamSelesai4_hour').val()) == parseInt($('#m030JamMulai4_hour').val()) && parseInt($('#m030JamMulai4_minute').val()) == '59' ){
            $("#m030JamSelesai4_hour").val(parseInt($('#m030JamMulai4_hour').val())+1);
            $('#m030JamSelesai4_minute').val('00');
        }else if(parseInt($('#m030JamSelesai4_hour').val()) == parseInt($('#m030JamMulai4_hour').val())
                && parseInt($('#m030JamSelesai4_minute').val()) <= parseInt($('#m030JamMulai4_minute').val())){
            $('#m030JamSelesai4_minute').val(parseInt($('#m030JamMulai4_minute').val()));
        }
    });

    $('#m030JamSelesai4_hour').change(function(){
        if(parseInt($('#m030JamSelesai4_hour').val()) <= parseInt($('#m030JamMulai4_hour').val()) && parseInt($('#m030JamMulai4_minute').val()) == '59' ){
            $("#m030JamSelesai4_hour").val(parseInt($('#m030JamMulai4_hour').val())+1);
            $('#m030JamSelesai4_minute').val('00');
        }
        else if(parseInt($('#m030JamSelesai4_hour').val()) < parseInt($('#m030JamMulai4_hour').val())){
            $("#m030JamSelesai4_hour").val($('#m030JamMulai4_hour').val());
            $('#m030JamSelesai4_minute').val(parseInt($('#m030JamMulai4_minute').val()));
        }
    });

    $('#m030JamSelesai4_minute').change(function(){
        if(parseInt($('#m030JamSelesai4_hour').val()) == parseInt($('#m030JamMulai4_hour').val()) && parseInt($('#m030JamMulai4_minute').val()) == '59' ){
            $("#m030JamSelesai4_hour").val(parseInt($('#m030JamMulai4_hour').val())+1);
            $('#m030JamSelesai4_minute').val('00');
        }else if(parseInt($('#m030JamSelesai4_hour').val()) == parseInt($('#m030JamMulai4_hour').val())
                && parseInt($('#m030JamSelesai4_minute').val()) <= parseInt($('#m030JamMulai4_minute').val())){
            $('#m030JamSelesai4_minute').val(parseInt($('#m030JamMulai4_minute').val()));
        }
    });


});

</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: jenisJamKerjaInstance, field: 'companyDealer', 'error')} required">
	<label class="control-label" for="companyDealer">
		<g:message code="jenisJamKerja.companyDealer.label" default="Company Dealer" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="companyDealer" name="companyDealer.id" from="${CompanyDealer.createCriteria().list {eq("staDel", "0");order("m011NamaWorkshop", "asc")}}" optionKey="id" required="" value="${jenisJamKerjaInstance?.companyDealer?.id}" class="many-to-one"/>
	</div>
</div>

<g:javascript>
    document.getElementById("companyDealer").focus();
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: jenisJamKerjaInstance, field: 'm030ID', 'error')} required">
	<label class="control-label" for="m030ID">
		<g:message code="jenisJamKerja.m030ID.label" default="Tipe Jam Kerja" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m030ID" maxlength="2" required="" value="${jenisJamKerjaInstance?.m030ID}"/>
	</div>
</div>
<fieldset>
    <legend>Jam kerja 1</legend>
    <div class="control-group fieldcontain ${hasErrors(bean: jenisJamKerjaInstance, field: 'm030JamMulai1', 'error')} required">
        <label class="control-label" for="m030JamMulai1">
            <g:message code="jenisJamKerja.m030JamMulai1.label" default="Jam Mulai" />
            <span class="required-indicator">*</span>
        </label>
        <div class="controls">
            %{--
                <g:datePicker name="m030JamMulai1" precision="day"  value="${jenisJamKerjaInstance?.m030JamMulai1}"  />
            --}%
            <select id="m030JamMulai1_hour" name="m030JamMulai1_hour" style="width: 60px" required="">
                %{
                    for (int i=0;i<24;i++){
                        if(i<10){
                            if(i==jenisJamKerjaInstance?.m030JamMulai1?.getHours()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==jenisJamKerjaInstance?.m030JamMulai1?.getHours()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }

                    }
                }%
            </select> H :
            <select id="m030JamMulai1_minute" name="m030JamMulai1_minute" style="width: 60px" required="">
                %{
                    for (int i=0;i<60;i++){
                        if(i<10){
                            if(i==jenisJamKerjaInstance?.m030JamMulai1?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==jenisJamKerjaInstance?.m030JamMulai1?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }
                    }
                }%
            </select> m
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: jenisJamKerjaInstance, field: 'm030JamSelesai1', 'error')} required">
        <label class="control-label" for="m030JamSelesai1">
            <g:message code="jenisJamKerja.m030JamSelesai1.label" default="Jam Selesai" />
            <span class="required-indicator">*</span>
        </label>
        <div class="controls">
            %{--
                <g:datePicker name="m030JamSelesai1" precision="day"  value="${jenisJamKerjaInstance?.m030JamSelesai1}"  />
            --}%
            <select id="m030JamSelesai1_hour" name="m030JamSelesai1_hour" style="width: 60px" required="">
                %{
                    for (int i=0;i<24;i++){
                        if(i<10){
                            if(i==jenisJamKerjaInstance?.m030JamSelesai1?.getHours()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==jenisJamKerjaInstance?.m030JamSelesai1?.getHours()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }
                    }
                }%
            </select> H :
            <select id="m030JamSelesai1_minute" name="m030JamSelesai1_minute" style="width: 60px" required="">
                %{
                    for (int i=0;i<60;i++){
                        if(i<10){
                            if(i==jenisJamKerjaInstance?.m030JamSelesai1?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==jenisJamKerjaInstance?.m030JamSelesai1?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }
                    }
                }%
            </select> m
        </div>
    </div>
</fieldset>

<fieldset>
    <legend>Jam kerja 2</legend>
    <div class="control-group fieldcontain ${hasErrors(bean: jenisJamKerjaInstance, field: 'm030JamMulai2', 'error')} required">
        <label class="control-label" for="m030JamMulai2">
            <g:message code="jenisJamKerja.m030JamMulai2.label" default="Jam Mulai" />
        </label>
        <div class="controls">
            %{--
                <g:datePicker name="m030JamMulai2" precision="day"  value="${jenisJamKerjaInstance?.m030JamMulai2}"  />
            --}%
            <select id="m030JamMulai2_hour" name="m030JamMulai2_hour" style="width: 60px">
                %{
                    for (int i=0;i<24;i++){
                        if(i<10){
                            if(i==jenisJamKerjaInstance?.m030JamMulai2?.getHours()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==jenisJamKerjaInstance?.m030JamMulai2?.getHours()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }

                    }
                }%
            </select> H :
            <select id="m030JamMulai2_minute" name="m030JamMulai2_minute" style="width: 60px">
                %{
                    for (int i=0;i<60;i++){
                        if(i<10){
                            if(i==jenisJamKerjaInstance?.m030JamMulai2?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==jenisJamKerjaInstance?.m030JamMulai2?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }
                    }
                }%
            </select> m
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: jenisJamKerjaInstance, field: 'm030JamSelesai2', 'error')} required">
        <label class="control-label" for="m030JamSelesai2">
            <g:message code="jenisJamKerja.m030JamSelesai2.label" default="Jam Selesai" />
        </label>
        <div class="controls">
            %{--
                <g:datePicker name="m030JamSelesai2" precision="day"  value="${jenisJamKerjaInstance?.m030JamSelesai2}"  />
            --}%
            <select id="m030JamSelesai2_hour" name="m030JamSelesai2_hour" style="width: 60px">
                %{
                    for (int i=0;i<24;i++){
                        if(i<10){
                            if(i==jenisJamKerjaInstance?.m030JamSelesai2?.getHours()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==jenisJamKerjaInstance?.m030JamSelesai2?.getHours()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }
                    }
                }%
            </select> H :
            <select id="m030JamSelesai2_minute" name="m030JamSelesai2_minute" style="width: 60px">
                %{
                    for (int i=0;i<60;i++){
                        if(i<10){
                            if(i==jenisJamKerjaInstance?.m030JamSelesai2?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==jenisJamKerjaInstance?.m030JamSelesai2?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }
                    }
                }%
            </select> m
        </div>
    </div>
</fieldset>

<fieldset>
    <legend>Jam kerja 3</legend>
    <div class="control-group fieldcontain ${hasErrors(bean: jenisJamKerjaInstance, field: 'm030JamMulai3', 'error')} required">
        <label class="control-label" for="m030JamMulai3">
            <g:message code="jenisJamKerja.m030JamMulai3.label" default="Jam Mulai" />
        </label>
        <div class="controls">
            %{--
            <g:datePicker name="m030JamMulai3" precision="day"  value="${jenisJamKerjaInstance?.m030JamMulai3}"  />
            --}%
            <select id="m030JamMulai3_hour" name="m030JamMulai3_hour" style="width: 60px">
                %{
                    for (int i=0;i<24;i++){
                        if(i<10){
                            if(i==jenisJamKerjaInstance?.m030JamMulai3?.getHours()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==jenisJamKerjaInstance?.m030JamMulai3?.getHours()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }
                    }
                }%
            </select> H :
            <select id="m030JamMulai3_minute" name="m030JamMulai3_minute" style="width: 60px">
                %{
                    for (int i=0;i<60;i++){
                        if(i<10){
                            if(i==jenisJamKerjaInstance?.m030JamMulai3?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==jenisJamKerjaInstance?.m030JamMulai3?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }
                    }
                }%
            </select> m
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: jenisJamKerjaInstance, field: 'm030JamSelesai3', 'error')} required">
        <label class="control-label" for="m030JamSelesai3">
            <g:message code="jenisJamKerja.m030JamSelesai3.label" default="Jam Selesai" />
        </label>
        <div class="controls">
            %{--
                <g:datePicker name="m030JamSelesai3" precision="day"  value="${jenisJamKerjaInstance?.m030JamSelesai3}"  />
            --}%
            <select id="m030JamSelesai3_hour" name="m030JamSelesai3_hour" style="width: 60px">
                %{
                    for (int i=0;i<24;i++){
                        if(i<10){
                            if(i==jenisJamKerjaInstance?.m030JamSelesai3?.getHours()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==jenisJamKerjaInstance?.m030JamSelesai3?.getHours()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }
                    }
                }%
            </select> H :
            <select id="m030JamSelesai3_minute" name="m030JamSelesai3_minute" style="width: 60px">
                %{
                    for (int i=0;i<60;i++){
                        if(i<10){
                            if(i==jenisJamKerjaInstance?.m030JamSelesai3?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==jenisJamKerjaInstance?.m030JamSelesai3?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }
                    }
                }%
            </select> m
        </div>
    </div>
</fieldset>

<fieldset>
    <legend>Jam kerja 4</legend>
    <div class="control-group fieldcontain ${hasErrors(bean: jenisJamKerjaInstance, field: 'm030JamMulai4', 'error')} required">
        <label class="control-label" for="m030JamMulai4">
            <g:message code="jenisJamKerja.m030JamMulai4.label" default="Jam Mulai" />
        </label>
        <div class="controls">
            %{--
                <g:datePicker name="m030JamMulai4" precision="day"  value="${jenisJamKerjaInstance?.m030JamMulai4}"  />
            --}%
            <select id="m030JamMulai4_hour" name="m030JamMulai4_hour" style="width: 60px">
                %{
                    for (int i=0;i<24;i++){
                        if(i<10){
                            if(i==jenisJamKerjaInstance?.m030JamMulai4?.getHours()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==jenisJamKerjaInstance?.m030JamMulai4?.getHours()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }
                    }
                }%
            </select> H :
            <select id="m030JamMulai4_minute" name="m030JamMulai4_minute" style="width: 60px">
                %{
                    for (int i=0;i<60;i++){
                        if(i<10){
                            if(i==jenisJamKerjaInstance?.m030JamMulai4?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==jenisJamKerjaInstance?.m030JamMulai4?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }
                    }
                }%
            </select> m
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: jenisJamKerjaInstance, field: 'm030JamSelesai4', 'error')} required">
        <label class="control-label" for="m030JamSelesai4">
            <g:message code="jenisJamKerja.m030JamSelesai4.label" default="Jam Selesai" />
        </label>
        <div class="controls">
            %{--
                <g:datePicker name="m030JamSelesai4" precision="day"  value="${jenisJamKerjaInstance?.m030JamSelesai4}"  />
            --}%
            <select id="m030JamSelesai4_hour" name="m030JamSelesai4_hour" style="width: 60px">
                %{
                    for (int i=0;i<24;i++){
                        if(i<10){
                            if(i==jenisJamKerjaInstance?.m030JamSelesai4?.getHours()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==jenisJamKerjaInstance?.m030JamSelesai4?.getHours()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }
                    }
                }%
            </select> H :
            <select id="m030JamSelesai4_minute" name="m030JamSelesai4_minute" style="width: 60px">
                %{
                    for (int i=0;i<60;i++){
                        if(i<10){
                            if(i==jenisJamKerjaInstance?.m030JamSelesai4?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">0'+i+'</option>');
                            }

                        } else {
                            if(i==jenisJamKerjaInstance?.m030JamSelesai4?.getMinutes()){
                                out.println('<option value="'+i+'" selected="">'+i+'</option>');
                            } else {
                                out.println('<option value="'+i+'">'+i+'</option>');
                            }
                        }
                    }
                }%
            </select> m
        </div>
    </div>
</fieldset>

%{--
<div class="control-group fieldcontain ${hasErrors(bean: jenisJamKerjaInstance, field: 'm030JamMulai1', 'error')} required">
	<label class="control-label" for="m030JamMulai1">
		<g:message code="jenisJamKerja.m030JamMulai1.label" default="Jam Mulai1" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:datePicker name="m030JamMulai1" precision="minute"  value="${jenisJamKerjaInstance?.m030JamMulai1}"  />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: jenisJamKerjaInstance, field: 'm030JamSelesai1', 'error')} required">
	<label class="control-label" for="m030JamSelesai1">
		<g:message code="jenisJamKerja.m030JamSelesai1.label" default="Jam Selesai1" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:datePicker name="m030JamSelesai1" precision="minute"  value="${jenisJamKerjaInstance?.m030JamSelesai1}"  />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: jenisJamKerjaInstance, field: 'm030JamMulai2', 'error')} ">
	<label class="control-label" for="m030JamMulai2">
		<g:message code="jenisJamKerja.m030JamMulai2.label" default="Jam Mulai2" />
		
	</label>
	<div class="controls">
	<g:datePicker name="m030JamMulai2" precision="minute"  value="${jenisJamKerjaInstance?.m030JamMulai2}" default="none" noSelection="['': '']" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: jenisJamKerjaInstance, field: 'm030JamSelesai2', 'error')} ">
	<label class="control-label" for="m030JamSelesai2">
		<g:message code="jenisJamKerja.m030JamSelesai2.label" default="Jam Selesai2" />
		
	</label>
	<div class="controls">
	<g:datePicker name="m030JamSelesai2" precision="minute"  value="${jenisJamKerjaInstance?.m030JamSelesai2}" default="none" noSelection="['': '']" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: jenisJamKerjaInstance, field: 'm030JamMulai3', 'error')} ">
	<label class="control-label" for="m030JamMulai3">
		<g:message code="jenisJamKerja.m030JamMulai3.label" default="Jam Mulai3" />
		
	</label>
	<div class="controls">
	<g:datePicker name="m030JamMulai3" precision="minute"  value="${jenisJamKerjaInstance?.m030JamMulai3}" default="none" noSelection="['': '']" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: jenisJamKerjaInstance, field: 'm030JamSelesai3', 'error')} ">
	<label class="control-label" for="m030JamSelesai3">
		<g:message code="jenisJamKerja.m030JamSelesai3.label" default="Jam Selesai3" />
		
	</label>
	<div class="controls">
	<g:datePicker name="m030JamSelesai3" precision="minute"  value="${jenisJamKerjaInstance?.m030JamSelesai3}" default="none" noSelection="['': '']" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: jenisJamKerjaInstance, field: 'm030JamMulai4', 'error')} ">
	<label class="control-label" for="m030JamMulai4">
		<g:message code="jenisJamKerja.m030JamMulai4.label" default="Jam Mulai4" />
		
	</label>
	<div class="controls">
	<g:datePicker name="m030JamMulai4" precision="minute"  value="${jenisJamKerjaInstance?.m030JamMulai4}" default="none" noSelection="['': '']" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: jenisJamKerjaInstance, field: 'm030JamSelesai4', 'error')} ">
	<label class="control-label" for="m030JamSelesai4">
		<g:message code="jenisJamKerja.m030JamSelesai4.label" default="Jam Selesai4" />
		
	</label>
	<div class="controls">
	<g:datePicker name="m030JamSelesai4" precision="minute"  value="${jenisJamKerjaInstance?.m030JamSelesai4}" default="none" noSelection="['': '']" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: jenisJamKerjaInstance, field: 'staDel', 'error')} required">
	<label class="control-label" for="staDel">
		<g:message code="jenisJamKerja.staDel.label" default="Sta Del" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="staDel" required="" value="${jenisJamKerjaInstance?.staDel}"/>
	</div>
</div>
--}%
