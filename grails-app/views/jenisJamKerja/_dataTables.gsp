
<%@ page import="com.kombos.administrasi.JenisJamKerja" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="jenisJamKerja_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="jenisJamKerja.companyDealer.label" default="Company Dealer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="jenisJamKerja.m030ID.label" default="Tipe Jam Kerja" /></div>
			</th>


            <th style="border-bottom: none;padding: 5px;text-align: center;" colspan="2">
                <div><g:message code="jenisJamKerja.m030JamKerja1.label" default="Jam Kerja 1" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;text-align: center;" colspan="2">
                <div><g:message code="jenisJamKerja.m030JamKerja2.label" default="Jam Kerja 2" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;text-align: center;" colspan="2">
                <div><g:message code="jenisJamKerja.m030JamKerja3.label" default="Jam Kerja 3" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;text-align: center;" colspan="2">
                <div><g:message code="jenisJamKerja.m030JamKerja4.label" default="Jam Kerja 4" /></div>
            </th>

%{--
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="jenisJamKerja.staDel.label" default="Sta Del" /></div>
			</th>
--}%
		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_companyDealer" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m030ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m030ID" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m030JamMulai1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m030JamMulai1" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m030JamSelesai1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m030JamSelesai1" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m030JamMulai2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m030JamMulai2" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m030JamSelesai2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m030JamSelesai2" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m030JamMulai3" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m030JamMulai3" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m030JamSelesai3" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m030JamSelesai3" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m030JamMulai4" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m030JamMulai4" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m030JamSelesai4" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m030JamSelesai4" class="search_init" />
				</div>
			</th>
%{--
			<th style="border-top: none;padding: 5px;">
				<div id="filter_staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_staDel" class="search_init" />
				</div>
			</th>
--}%

		</tr>
	</thead>
</table>

<g:javascript>
var JenisJamKerjaTable;
var reloadJenisJamKerjaTable;
$(function(){
	reloadJenisJamKerjaTable = function() {
		JenisJamKerjaTable.fnDraw();
	}

	var recordsJenisJamKerjaPerPage = [];
    var anJenisJamKerjaSelected;
    var jmlRecJenisJamKerjaPerPage=0;
    var id;

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	JenisJamKerjaTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	JenisJamKerjaTable = $('#jenisJamKerja_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsJenisJamKerja = $("#jenisJamKerja_datatables tbody .row-select");
            var jmlJenisJamKerjaCek = 0;
            var nRow;
            var idRec;
            rsJenisJamKerja.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsJenisJamKerjaPerPage[idRec]=="1"){
                    jmlJenisJamKerjaCek = jmlJenisJamKerjaCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsJenisJamKerjaPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecJenisJamKerjaPerPage = rsJenisJamKerja.length;
            if(jmlJenisJamKerjaCek==jmlRecJenisJamKerjaPerPage && jmlRecJenisJamKerjaPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

            {
                "sName": "companyDealer",
                "mDataProp": "companyDealer",
                "aTargets": [0],
                "mRender": function ( data, type, row ) {
                    return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
                },
                "bSearchable": true,
                "bSortable": true,
                "sWidth":"300px",
                "bVisible": true
            }

            ,

            {
                "sName": "m030ID",
                "mDataProp": "m030ID",
                "aTargets": [1],
                "bSearchable": true,
                "bSortable": true,
                "sWidth":"200px",
                "bVisible": true
            }

            ,

            {
                "sName": "m030JamMulai1",
                "mDataProp": "m030JamMulai1",
                "aTargets": [2],
                "bSearchable": true,
                "bSortable": true,
                "sWidth":"200px",
                "bVisible": true
            }

            ,

            {
                "sName": "m030JamSelesai1",
                "mDataProp": "m030JamSelesai1",
                "aTargets": [3],
                "bSearchable": true,
                "bSortable": true,
                "sWidth":"200px",
                "bVisible": true
            }

            ,

            {
                "sName": "m030JamMulai2",
                "mDataProp": "m030JamMulai2",
                "aTargets": [4],
                "bSearchable": true,
                "bSortable": true,
                "sWidth":"200px",
                "bVisible": true
            }

            ,

            {
                "sName": "m030JamSelesai2",
                "mDataProp": "m030JamSelesai2",
                "aTargets": [5],
                "bSearchable": true,
                "bSortable": true,
                "sWidth":"200px",
                "bVisible": true
            }

            ,

            {
                "sName": "m030JamMulai3",
                "mDataProp": "m030JamMulai3",
                "aTargets": [6],
                "bSearchable": true,
                "bSortable": true,
                "sWidth":"200px",
                "bVisible": true
            }

            ,

            {
                "sName": "m030JamSelesai3",
                "mDataProp": "m030JamSelesai3",
                "aTargets": [7],
                "bSearchable": true,
                "bSortable": true,
                "sWidth":"200px",
                "bVisible": true
            }

            ,

            {
                "sName": "m030JamMulai4",
                "mDataProp": "m030JamMulai4",
                "aTargets": [8],
                "bSearchable": true,
                "bSortable": true,
                "sWidth":"200px",
                "bVisible": true
            }

            ,

            {
                "sName": "m030JamSelesai4",
                "mDataProp": "m030JamSelesai4",
                "aTargets": [9],
                "bSearchable": true,
                "bSortable": true,
                "sWidth":"200px",
                "bVisible": true
            }

            ,

            {
                "sName": "staDel",
                "mDataProp": "staDel",
                "aTargets": [10],
                "bSearchable": true,
                "bSortable": true,
                "sWidth":"200px",
                "bVisible": false
            }

        ],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
            var companyDealer = $('#filter_companyDealer input').val();
            if(companyDealer){
                aoData.push(
                        {"name": 'sCriteria_companyDealer', "value": companyDealer}
                );
            }

            var m030ID = $('#filter_m030ID input').val();
            if(m030ID){
                aoData.push(
                        {"name": 'sCriteria_m030ID', "value": m030ID}
                );
            }

            var m030JamMulai1 = $('#filter_m030JamMulai1 input').val();
            if(m030JamMulai1){
                aoData.push(
                        {"name": 'sCriteria_m030JamMulai1', "value": m030JamMulai1}
                );
            }

            var m030JamSelesai1 = $('#filter_m030JamSelesai1 input').val();
            if(m030JamSelesai1){
                aoData.push(
                        {"name": 'sCriteria_m030JamSelesai1', "value": m030JamSelesai1}
                );
            }

            var m030JamMulai2 = $('#filter_m030JamMulai2 input').val();
            if(m030JamMulai2){
                aoData.push(
                        {"name": 'sCriteria_m030JamMulai2', "value": m030JamMulai2}
                );
            }

            var m030JamSelesai2 = $('#filter_m030JamSelesai2 input').val();
            if(m030JamSelesai2){
                aoData.push(
                        {"name": 'sCriteria_m030JamSelesai2', "value": m030JamSelesai2}
                );
            }

            var m030JamMulai3 = $('#filter_m030JamMulai3 input').val();
            if(m030JamMulai3){
                aoData.push(
                        {"name": 'sCriteria_m030JamMulai3', "value": m030JamMulai3}
                );
            }

            var m030JamSelesai3 = $('#filter_m030JamSelesai3 input').val();
            if(m030JamSelesai3){
                aoData.push(
                        {"name": 'sCriteria_m030JamSelesai3', "value": m030JamSelesai3}
                );
            }

            var m030JamMulai4 = $('#filter_m030JamMulai4 input').val();
            if(m030JamMulai4){
                aoData.push(
                        {"name": 'sCriteria_m030JamMulai4', "value": m030JamMulai4}
                );
            }

            var m030JamSelesai4 = $('#filter_m030JamSelesai4 input').val();
            if(m030JamSelesai4){
                aoData.push(
                        {"name": 'sCriteria_m030JamSelesai4', "value": m030JamSelesai4}
                );
            }

            var staDel = $('#filter_staDel input').val();
            if(staDel){
                aoData.push(
                        {"name": 'sCriteria_staDel', "value": staDel}
                );
            }

            $.ajax({ "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData ,
                "success": function (json) {
                    fnCallback(json);
                   },
                "complete": function () {
                   }
            });
		}			
	});
	
	$('.select-all').click(function(e) {

        $("#jenisJamKerja_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsJenisJamKerjaPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsJenisJamKerjaPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#jenisJamKerja_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsJenisJamKerjaPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anJenisJamKerjaSelected = JenisJamKerjaTable.$('tr.row_selected');
            if(jmlRecJenisJamKerjaPerPage == anJenisJamKerjaSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsJenisJamKerjaPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
