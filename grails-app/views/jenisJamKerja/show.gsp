

<%@ page import="com.kombos.administrasi.JenisJamKerja" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'jenisJamKerja.label', default: 'Tipe Jam Kerja')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteJenisJamKerja;

$(function(){ 
	deleteJenisJamKerja=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/jenisJamKerja/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadJenisJamKerjaTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-jenisJamKerja" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="jenisJamKerja"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${jenisJamKerjaInstance?.companyDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="companyDealer-label" class="property-label"><g:message
					code="jenisJamKerja.companyDealer.label" default="Company Dealer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="companyDealer-label">
						%{--<ba:editableValue
								bean="${jenisJamKerjaInstance}" field="companyDealer"
								url="${request.contextPath}/JenisJamKerja/updatefield" type="text"
								title="Enter companyDealer" onsuccess="reloadJenisJamKerjaTable();" />--}%
							
								${jenisJamKerjaInstance?.companyDealer?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${jenisJamKerjaInstance?.m030ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m030ID-label" class="property-label"><g:message
					code="jenisJamKerja.m030ID.label" default="Tipe Jam Kerja" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m030ID-label">
						%{--<ba:editableValue
								bean="${jenisJamKerjaInstance}" field="m030ID"
								url="${request.contextPath}/JenisJamKerja/updatefield" type="text"
								title="Enter m030ID" onsuccess="reloadJenisJamKerjaTable();" />--}%
							
								<g:fieldValue bean="${jenisJamKerjaInstance}" field="m030ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${jenisJamKerjaInstance?.m030JamMulai1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m030JamMulai1-label" class="property-label"><g:message
					code="jenisJamKerja.m030JamMulai1.label" default="Jam Mulai 1" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m030JamMulai1-label">
						%{--<ba:editableValue
								bean="${jenisJamKerjaInstance}" field="m030JamMulai1"
								url="${request.contextPath}/JenisJamKerja/updatefield" type="text"
								title="Enter m030JamMulai1" onsuccess="reloadJenisJamKerjaTable();" />
							
								<g:formatDate date="${jenisJamKerjaInstance?.m030JamMulai1}" />--}%
                        ${jenisJamKerjaInstance?.m030JamMulai1}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${jenisJamKerjaInstance?.m030JamSelesai1}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m030JamSelesai1-label" class="property-label"><g:message
					code="jenisJamKerja.m030JamSelesai1.label" default="Jam Selesai 1" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m030JamSelesai1-label">
						%{--<ba:editableValue
								bean="${jenisJamKerjaInstance}" field="m030JamSelesai1"
								url="${request.contextPath}/JenisJamKerja/updatefield" type="text"
								title="Enter m030JamSelesai1" onsuccess="reloadJenisJamKerjaTable();" />
							
								<g:formatDate date="${jenisJamKerjaInstance?.m030JamSelesai1}" />--}%
                        ${jenisJamKerjaInstance?.m030JamSelesai1}
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${jenisJamKerjaInstance?.m030JamMulai2}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m030JamMulai2-label" class="property-label"><g:message
					code="jenisJamKerja.m030JamMulai2.label" default="Jam Mulai 2" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m030JamMulai2-label">
						%{--<ba:editableValue
								bean="${jenisJamKerjaInstance}" field="m030JamMulai2"
								url="${request.contextPath}/JenisJamKerja/updatefield" type="text"
								title="Enter m030JamMulai2" onsuccess="reloadJenisJamKerjaTable();" />
							
								<g:formatDate date="${jenisJamKerjaInstance?.m030JamMulai2}" />--}%
                        ${jenisJamKerjaInstance?.m030JamMulai2}
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${jenisJamKerjaInstance?.m030JamSelesai2}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m030JamSelesai2-label" class="property-label"><g:message
					code="jenisJamKerja.m030JamSelesai2.label" default="Jam Selesai 2" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m030JamSelesai2-label">
						%{--<ba:editableValue
								bean="${jenisJamKerjaInstance}" field="m030JamSelesai2"
								url="${request.contextPath}/JenisJamKerja/updatefield" type="text"
								title="Enter m030JamSelesai2" onsuccess="reloadJenisJamKerjaTable();" />
							
								<g:formatDate date="${jenisJamKerjaInstance?.m030JamSelesai2}" />--}%
                        ${jenisJamKerjaInstance?.m030JamSelesai2}
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${jenisJamKerjaInstance?.m030JamMulai3}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m030JamMulai3-label" class="property-label"><g:message
					code="jenisJamKerja.m030JamMulai3.label" default="Jam Mulai 3" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m030JamMulai3-label">
						%{--<ba:editableValue
								bean="${jenisJamKerjaInstance}" field="m030JamMulai3"
								url="${request.contextPath}/JenisJamKerja/updatefield" type="text"
								title="Enter m030JamMulai3" onsuccess="reloadJenisJamKerjaTable();" />
							
								<g:formatDate date="${jenisJamKerjaInstance?.m030JamMulai3}" />--}%
                        ${jenisJamKerjaInstance?.m030JamMulai3}
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${jenisJamKerjaInstance?.m030JamSelesai3}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m030JamSelesai3-label" class="property-label"><g:message
					code="jenisJamKerja.m030JamSelesai3.label" default="Jam Selesai 3" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m030JamSelesai3-label">
						%{--<ba:editableValue
								bean="${jenisJamKerjaInstance}" field="m030JamSelesai3"
								url="${request.contextPath}/JenisJamKerja/updatefield" type="text"
								title="Enter m030JamSelesai3" onsuccess="reloadJenisJamKerjaTable();" />
							
								<g:formatDate date="${jenisJamKerjaInstance?.m030JamSelesai3}" />--}%
                        ${jenisJamKerjaInstance?.m030JamSelesai3}
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${jenisJamKerjaInstance?.m030JamMulai4}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m030JamMulai4-label" class="property-label"><g:message
					code="jenisJamKerja.m030JamMulai4.label" default="Jam Mulai 4" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m030JamMulai4-label">
						%{--<ba:editableValue
								bean="${jenisJamKerjaInstance}" field="m030JamMulai4"
								url="${request.contextPath}/JenisJamKerja/updatefield" type="text"
								title="Enter m030JamMulai4" onsuccess="reloadJenisJamKerjaTable();" />
							
								<g:formatDate date="${jenisJamKerjaInstance?.m030JamMulai4}" />--}%
                        ${jenisJamKerjaInstance?.m030JamMulai4}
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${jenisJamKerjaInstance?.m030JamSelesai4}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m030JamSelesai4-label" class="property-label"><g:message
					code="jenisJamKerja.m030JamSelesai4.label" default="Jam Selesai 4" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m030JamSelesai4-label">
						%{--<ba:editableValue
								bean="${jenisJamKerjaInstance}" field="m030JamSelesai4"
								url="${request.contextPath}/JenisJamKerja/updatefield" type="text"
								title="Enter m030JamSelesai4" onsuccess="reloadJenisJamKerjaTable();" />
							
								<g:formatDate date="${jenisJamKerjaInstance?.m030JamSelesai4}" />--}%
                        ${jenisJamKerjaInstance?.m030JamSelesai4}
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${jenisJamKerjaInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="jenisJamKerja.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${liftInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/Lift/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadLiftTable();" />--}%

                        <g:fieldValue bean="${jenisJamKerjaInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${jenisJamKerjaInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="jenisJamKerja.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${liftInstance}" field="dateCreated"
                                url="${request.contextPath}/Lift/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadLiftTable();" />--}%

                        <g:formatDate date="${jenisJamKerjaInstance?.dateCreated}"  type="datetime" style="MEDIUM"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${jenisJamKerjaInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="jenisJamKerja.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${liftInstance}" field="createdBy"
                                url="${request.contextPath}/Lift/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadLiftTable();" />--}%

                        <g:fieldValue bean="${jenisJamKerjaInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${jenisJamKerjaInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="jenisJamKerja.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${liftInstance}" field="lastUpdated"
                                url="${request.contextPath}/Lift/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadLiftTable();" />--}%

                        <g:formatDate date="${jenisJamKerjaInstance?.lastUpdated}"  type="datetime" style="MEDIUM"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${jenisJamKerjaInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="jenisJamKerja.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${liftInstance}" field="updatedBy"
                                url="${request.contextPath}/Lift/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadLiftTable();" />--}%

                        <g:fieldValue bean="${jenisJamKerjaInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>


            </tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${jenisJamKerjaInstance?.id}"
					update="[success:'jenisJamKerja-form',failure:'jenisJamKerja-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteJenisJamKerja('${jenisJamKerjaInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
