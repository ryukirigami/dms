<%@ page import="com.kombos.maintable.VehicleFA" %>


<div class="box">
    <legend style="font-size: small">Vehice List</legend>
    <fieldset class="buttons controls" >
        <button class="btn btn-primary" onclick="onSelectAll();">Select All</button>
        <button class="btn btn-primary" onclick="onUnSelectAll();">Unselect All</button>
    </fieldset>

    <div id="partsDataTables_tabs-1">
        <g:render template="vehicleListDataTables"/>
    </div>
</div>


<div class="box">
    <legend style="font-size: small">Field Action Category</legend>

    <div class="control-group fieldcontain ${hasErrors(bean: vehicleFAInstance, field: 'fa', 'error')} required">

        <label class="control-label" for="fa">
            <g:message code="vehicleFA.fa.label" default="Fa"/>
            <span class="required-indicator">*</span>
        </label>

        <div class="controls">
            <g:select id="fa" name="fa.id" from="${com.kombos.customerprofile.FA.list()}" optionKey="id" required=""
                      value="${vehicleFAInstance?.fa?.id}" class="many-to-one"/>
        </div>
    </div>
</div>

