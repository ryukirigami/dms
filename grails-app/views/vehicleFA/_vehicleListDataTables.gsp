
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="vehicleList_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>
        <th style="border-bottom: none;padding: 5px;">
            <div>Nomor Polisi</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>VinCode</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Full Model Code</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Model</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Tanggal DEC</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Nomor Mesin</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Warna</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Nomor Kunci</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Tanggal STNK</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Bulan & Tahun Rakit</div>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
var vehicleListTable;
var reloadvehicleListTable;
$(function(){

    reloadvehicleListTable = function() {
		vehicleListTable.fnDraw();
	}

	vehicleListTable = $('#vehicleList_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "vehicleListDatatablesList")}",
		"aoColumns": [

{
	"sName": "nopol",
	"mDataProp": "nopol",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input id="checkBoxVendor" type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "vinCode",
	"mDataProp": "vinCode",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "fullModel",
	"mDataProp": "fullModel",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "model",
	"mDataProp": "model",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "tanggalDEC",
	"mDataProp": "tanggalDEC",
	"aTargets": [4],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "nomorMesin",
	"mDataProp": "nomorMesin",
	"aTargets": [5],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "warna",
	"mDataProp": "warna",
	"aTargets": [6],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "nomorKunci",
	"mDataProp": "nomorKunci",
	"aTargets": [7],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "tanggalSTNK",
	"mDataProp": "tanggalSTNK",
	"aTargets": [8],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "tahunDanBulanRakit",
	"mDataProp": "tahunDanBulanRakit",
	"aTargets": [9],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
        $.ajax({ "dataType": 'json',
            "type": "POST",
            "url": sSource,
            "data": aoData ,
            "success": function (json) {
                fnCallback(json);
               },
            "complete": function () {
               }
        });
}
});
});
</g:javascript>



