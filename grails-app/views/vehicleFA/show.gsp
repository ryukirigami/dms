

<%@ page import="com.kombos.maintable.VehicleFA" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'vehicleFA.label', default: 'VehicleFA')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteVehicleFA;

$(function(){ 
	deleteVehicleFA=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/vehicleFA/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadVehicleFATable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-vehicleFA" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="vehicleFA"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${vehicleFAInstance?.customerVehicle}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="customerVehicle-label" class="property-label"><g:message
					code="vehicleFA.customerVehicle.label" default="Customer Vehicle" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="customerVehicle-label">
						%{--<ba:editableValue
								bean="${vehicleFAInstance}" field="customerVehicle"
								url="${request.contextPath}/VehicleFA/updatefield" type="text"
								title="Enter customerVehicle" onsuccess="reloadVehicleFATable();" />--}%
							
								<g:link controller="customerVehicle" action="show" id="${vehicleFAInstance?.customerVehicle?.id}">${vehicleFAInstance?.customerVehicle?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vehicleFAInstance?.historyCustomerVehicle}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="historyCustomerVehicle-label" class="property-label"><g:message
					code="vehicleFA.historyCustomerVehicle.label" default="History Customer Vehicle" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="historyCustomerVehicle-label">
						%{--<ba:editableValue
								bean="${vehicleFAInstance}" field="historyCustomerVehicle"
								url="${request.contextPath}/VehicleFA/updatefield" type="text"
								title="Enter historyCustomerVehicle" onsuccess="reloadVehicleFATable();" />--}%
							
								<g:link controller="historyCustomerVehicle" action="show" id="${vehicleFAInstance?.historyCustomerVehicle?.id}">${vehicleFAInstance?.historyCustomerVehicle?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vehicleFAInstance?.fa}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="fa-label" class="property-label"><g:message
					code="vehicleFA.fa.label" default="Fa" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="fa-label">
						%{--<ba:editableValue
								bean="${vehicleFAInstance}" field="fa"
								url="${request.contextPath}/VehicleFA/updatefield" type="text"
								title="Enter fa" onsuccess="reloadVehicleFATable();" />--}%
							
								<g:link controller="FA" action="show" id="${vehicleFAInstance?.fa?.id}">${vehicleFAInstance?.fa?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vehicleFAInstance?.t185StaReminderFA}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t185StaReminderFA-label" class="property-label"><g:message
					code="vehicleFA.t185StaReminderFA.label" default="T185 Sta Reminder FA" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t185StaReminderFA-label">
						%{--<ba:editableValue
								bean="${vehicleFAInstance}" field="t185StaReminderFA"
								url="${request.contextPath}/VehicleFA/updatefield" type="text"
								title="Enter t185StaReminderFA" onsuccess="reloadVehicleFATable();" />--}%
							
								<g:fieldValue bean="${vehicleFAInstance}" field="t185StaReminderFA"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vehicleFAInstance?.t185StaSudahDiperiksa}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t185StaSudahDiperiksa-label" class="property-label"><g:message
					code="vehicleFA.t185StaSudahDiperiksa.label" default="T185 Sta Sudah Diperiksa" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t185StaSudahDiperiksa-label">
						%{--<ba:editableValue
								bean="${vehicleFAInstance}" field="t185StaSudahDiperiksa"
								url="${request.contextPath}/VehicleFA/updatefield" type="text"
								title="Enter t185StaSudahDiperiksa" onsuccess="reloadVehicleFATable();" />--}%
							
								<g:fieldValue bean="${vehicleFAInstance}" field="t185StaSudahDiperiksa"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vehicleFAInstance?.t185StaSudahDikerjakan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t185StaSudahDikerjakan-label" class="property-label"><g:message
					code="vehicleFA.t185StaSudahDikerjakan.label" default="T185 Sta Sudah Dikerjakan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t185StaSudahDikerjakan-label">
						%{--<ba:editableValue
								bean="${vehicleFAInstance}" field="t185StaSudahDikerjakan"
								url="${request.contextPath}/VehicleFA/updatefield" type="text"
								title="Enter t185StaSudahDikerjakan" onsuccess="reloadVehicleFATable();" />--}%
							
								<g:fieldValue bean="${vehicleFAInstance}" field="t185StaSudahDikerjakan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vehicleFAInstance?.companyDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="companyDealer-label" class="property-label"><g:message
					code="vehicleFA.companyDealer.label" default="Company Dealer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="companyDealer-label">
						%{--<ba:editableValue
								bean="${vehicleFAInstance}" field="companyDealer"
								url="${request.contextPath}/VehicleFA/updatefield" type="text"
								title="Enter companyDealer" onsuccess="reloadVehicleFATable();" />--}%
							
								<g:link controller="companyDealer" action="show" id="${vehicleFAInstance?.companyDealer?.id}">${vehicleFAInstance?.companyDealer?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vehicleFAInstance?.t185TanggalDikerjakan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t185TanggalDikerjakan-label" class="property-label"><g:message
					code="vehicleFA.t185TanggalDikerjakan.label" default="T185 Tanggal Dikerjakan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t185TanggalDikerjakan-label">
						%{--<ba:editableValue
								bean="${vehicleFAInstance}" field="t185TanggalDikerjakan"
								url="${request.contextPath}/VehicleFA/updatefield" type="text"
								title="Enter t185TanggalDikerjakan" onsuccess="reloadVehicleFATable();" />--}%
							
								<g:formatDate date="${vehicleFAInstance?.t185TanggalDikerjakan}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vehicleFAInstance?.t185Lokasi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t185Lokasi-label" class="property-label"><g:message
					code="vehicleFA.t185Lokasi.label" default="T185 Lokasi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t185Lokasi-label">
						%{--<ba:editableValue
								bean="${vehicleFAInstance}" field="t185Lokasi"
								url="${request.contextPath}/VehicleFA/updatefield" type="text"
								title="Enter t185Lokasi" onsuccess="reloadVehicleFATable();" />--}%
							
								<g:fieldValue bean="${vehicleFAInstance}" field="t185Lokasi"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vehicleFAInstance?.t185MainDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t185MainDealer-label" class="property-label"><g:message
					code="vehicleFA.t185MainDealer.label" default="T185 Main Dealer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t185MainDealer-label">
						%{--<ba:editableValue
								bean="${vehicleFAInstance}" field="t185MainDealer"
								url="${request.contextPath}/VehicleFA/updatefield" type="text"
								title="Enter t185MainDealer" onsuccess="reloadVehicleFATable();" />--}%
							
								<g:fieldValue bean="${vehicleFAInstance}" field="t185MainDealer"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vehicleFAInstance?.t185Dealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t185Dealer-label" class="property-label"><g:message
					code="vehicleFA.t185Dealer.label" default="T185 Dealer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t185Dealer-label">
						%{--<ba:editableValue
								bean="${vehicleFAInstance}" field="t185Dealer"
								url="${request.contextPath}/VehicleFA/updatefield" type="text"
								title="Enter t185Dealer" onsuccess="reloadVehicleFATable();" />--}%
							
								<g:fieldValue bean="${vehicleFAInstance}" field="t185Dealer"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vehicleFAInstance?.t185Ket}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t185Ket-label" class="property-label"><g:message
					code="vehicleFA.t185Ket.label" default="T185 Ket" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t185Ket-label">
						%{--<ba:editableValue
								bean="${vehicleFAInstance}" field="t185Ket"
								url="${request.contextPath}/VehicleFA/updatefield" type="text"
								title="Enter t185Ket" onsuccess="reloadVehicleFATable();" />--}%
							
								<g:fieldValue bean="${vehicleFAInstance}" field="t185Ket"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vehicleFAInstance?.t185xNamaUser}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t185xNamaUser-label" class="property-label"><g:message
					code="vehicleFA.t185xNamaUser.label" default="T185x Nama User" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t185xNamaUser-label">
						%{--<ba:editableValue
								bean="${vehicleFAInstance}" field="t185xNamaUser"
								url="${request.contextPath}/VehicleFA/updatefield" type="text"
								title="Enter t185xNamaUser" onsuccess="reloadVehicleFATable();" />--}%
							
								<g:fieldValue bean="${vehicleFAInstance}" field="t185xNamaUser"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vehicleFAInstance?.t185xNamaDivisi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t185xNamaDivisi-label" class="property-label"><g:message
					code="vehicleFA.t185xNamaDivisi.label" default="T185x Nama Divisi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t185xNamaDivisi-label">
						%{--<ba:editableValue
								bean="${vehicleFAInstance}" field="t185xNamaDivisi"
								url="${request.contextPath}/VehicleFA/updatefield" type="text"
								title="Enter t185xNamaDivisi" onsuccess="reloadVehicleFATable();" />--}%
							
								<g:fieldValue bean="${vehicleFAInstance}" field="t185xNamaDivisi"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vehicleFAInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="vehicleFA.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${vehicleFAInstance}" field="createdBy"
								url="${request.contextPath}/VehicleFA/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadVehicleFATable();" />--}%
							
								<g:fieldValue bean="${vehicleFAInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vehicleFAInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="vehicleFA.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${vehicleFAInstance}" field="updatedBy"
								url="${request.contextPath}/VehicleFA/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadVehicleFATable();" />--}%
							
								<g:fieldValue bean="${vehicleFAInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vehicleFAInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="vehicleFA.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${vehicleFAInstance}" field="lastUpdProcess"
								url="${request.contextPath}/VehicleFA/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadVehicleFATable();" />--}%
							
								<g:fieldValue bean="${vehicleFAInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vehicleFAInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="vehicleFA.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${vehicleFAInstance}" field="dateCreated"
								url="${request.contextPath}/VehicleFA/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadVehicleFATable();" />--}%
							
								<g:formatDate date="${vehicleFAInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${vehicleFAInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="vehicleFA.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${vehicleFAInstance}" field="lastUpdated"
								url="${request.contextPath}/VehicleFA/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadVehicleFATable();" />--}%
							
								<g:formatDate date="${vehicleFAInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${vehicleFAInstance?.id}"
					update="[success:'vehicleFA-form',failure:'vehicleFA-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteVehicleFA('${vehicleFAInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
