
<%@ page import="com.kombos.maintable.VehicleFA" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="vehicleFA_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vehicleFA.customerVehicle.label" default="Customer Vehicle" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vehicleFA.historyCustomerVehicle.label" default="History Customer Vehicle" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vehicleFA.fa.label" default="Fa" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vehicleFA.t185StaReminderFA.label" default="T185 Sta Reminder FA" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vehicleFA.t185StaSudahDiperiksa.label" default="T185 Sta Sudah Diperiksa" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vehicleFA.t185StaSudahDikerjakan.label" default="T185 Sta Sudah Dikerjakan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vehicleFA.companyDealer.label" default="Company Dealer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vehicleFA.t185TanggalDikerjakan.label" default="T185 Tanggal Dikerjakan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vehicleFA.t185Lokasi.label" default="T185 Lokasi" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vehicleFA.t185MainDealer.label" default="T185 Main Dealer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vehicleFA.t185Dealer.label" default="T185 Dealer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vehicleFA.t185Ket.label" default="T185 Ket" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vehicleFA.t185xNamaUser.label" default="T185x Nama User" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vehicleFA.t185xNamaDivisi.label" default="T185x Nama Divisi" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vehicleFA.createdBy.label" default="Created By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vehicleFA.updatedBy.label" default="Updated By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="vehicleFA.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_customerVehicle" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_customerVehicle" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_historyCustomerVehicle" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_historyCustomerVehicle" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_fa" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_fa" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t185StaReminderFA" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t185StaReminderFA" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t185StaSudahDiperiksa" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t185StaSudahDiperiksa" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t185StaSudahDikerjakan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t185StaSudahDikerjakan" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_companyDealer" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t185TanggalDikerjakan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_t185TanggalDikerjakan" value="date.struct">
					<input type="hidden" name="search_t185TanggalDikerjakan_day" id="search_t185TanggalDikerjakan_day" value="">
					<input type="hidden" name="search_t185TanggalDikerjakan_month" id="search_t185TanggalDikerjakan_month" value="">
					<input type="hidden" name="search_t185TanggalDikerjakan_year" id="search_t185TanggalDikerjakan_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t185TanggalDikerjakan_dp" value="" id="search_t185TanggalDikerjakan" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t185Lokasi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t185Lokasi" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t185MainDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t185MainDealer" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t185Dealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t185Dealer" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t185Ket" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t185Ket" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t185xNamaUser" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t185xNamaUser" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t185xNamaDivisi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t185xNamaDivisi" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_createdBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_updatedBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var vehicleFATable;
var reloadVehicleFATable;
$(function(){
	
	reloadVehicleFATable = function() {
		vehicleFATable.fnDraw();
	}

	
	$('#search_t185TanggalDikerjakan').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t185TanggalDikerjakan_day').val(newDate.getDate());
			$('#search_t185TanggalDikerjakan_month').val(newDate.getMonth()+1);
			$('#search_t185TanggalDikerjakan_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			vehicleFATable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	vehicleFATable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	vehicleFATable = $('#vehicleFA_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "customerVehicle",
	"mDataProp": "customerVehicle",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "historyCustomerVehicle",
	"mDataProp": "historyCustomerVehicle",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "fa",
	"mDataProp": "fa",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t185StaReminderFA",
	"mDataProp": "t185StaReminderFA",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t185StaSudahDiperiksa",
	"mDataProp": "t185StaSudahDiperiksa",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t185StaSudahDikerjakan",
	"mDataProp": "t185StaSudahDikerjakan",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "companyDealer",
	"mDataProp": "companyDealer",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t185TanggalDikerjakan",
	"mDataProp": "t185TanggalDikerjakan",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t185Lokasi",
	"mDataProp": "t185Lokasi",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t185MainDealer",
	"mDataProp": "t185MainDealer",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t185Dealer",
	"mDataProp": "t185Dealer",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t185Ket",
	"mDataProp": "t185Ket",
	"aTargets": [11],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t185xNamaUser",
	"mDataProp": "t185xNamaUser",
	"aTargets": [12],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t185xNamaDivisi",
	"mDataProp": "t185xNamaDivisi",
	"aTargets": [13],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "createdBy",
	"mDataProp": "createdBy",
	"aTargets": [14],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "updatedBy",
	"mDataProp": "updatedBy",
	"aTargets": [15],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [16],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var customerVehicle = $('#filter_customerVehicle input').val();
						if(customerVehicle){
							aoData.push(
									{"name": 'sCriteria_customerVehicle', "value": customerVehicle}
							);
						}
	
						var historyCustomerVehicle = $('#filter_historyCustomerVehicle input').val();
						if(historyCustomerVehicle){
							aoData.push(
									{"name": 'sCriteria_historyCustomerVehicle', "value": historyCustomerVehicle}
							);
						}
	
						var fa = $('#filter_fa input').val();
						if(fa){
							aoData.push(
									{"name": 'sCriteria_fa', "value": fa}
							);
						}
	
						var t185StaReminderFA = $('#filter_t185StaReminderFA input').val();
						if(t185StaReminderFA){
							aoData.push(
									{"name": 'sCriteria_t185StaReminderFA', "value": t185StaReminderFA}
							);
						}
	
						var t185StaSudahDiperiksa = $('#filter_t185StaSudahDiperiksa input').val();
						if(t185StaSudahDiperiksa){
							aoData.push(
									{"name": 'sCriteria_t185StaSudahDiperiksa', "value": t185StaSudahDiperiksa}
							);
						}
	
						var t185StaSudahDikerjakan = $('#filter_t185StaSudahDikerjakan input').val();
						if(t185StaSudahDikerjakan){
							aoData.push(
									{"name": 'sCriteria_t185StaSudahDikerjakan', "value": t185StaSudahDikerjakan}
							);
						}
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}

						var t185TanggalDikerjakan = $('#search_t185TanggalDikerjakan').val();
						var t185TanggalDikerjakanDay = $('#search_t185TanggalDikerjakan_day').val();
						var t185TanggalDikerjakanMonth = $('#search_t185TanggalDikerjakan_month').val();
						var t185TanggalDikerjakanYear = $('#search_t185TanggalDikerjakan_year').val();
						
						if(t185TanggalDikerjakan){
							aoData.push(
									{"name": 'sCriteria_t185TanggalDikerjakan', "value": "date.struct"},
									{"name": 'sCriteria_t185TanggalDikerjakan_dp', "value": t185TanggalDikerjakan},
									{"name": 'sCriteria_t185TanggalDikerjakan_day', "value": t185TanggalDikerjakanDay},
									{"name": 'sCriteria_t185TanggalDikerjakan_month', "value": t185TanggalDikerjakanMonth},
									{"name": 'sCriteria_t185TanggalDikerjakan_year', "value": t185TanggalDikerjakanYear}
							);
						}
	
						var t185Lokasi = $('#filter_t185Lokasi input').val();
						if(t185Lokasi){
							aoData.push(
									{"name": 'sCriteria_t185Lokasi', "value": t185Lokasi}
							);
						}
	
						var t185MainDealer = $('#filter_t185MainDealer input').val();
						if(t185MainDealer){
							aoData.push(
									{"name": 'sCriteria_t185MainDealer', "value": t185MainDealer}
							);
						}
	
						var t185Dealer = $('#filter_t185Dealer input').val();
						if(t185Dealer){
							aoData.push(
									{"name": 'sCriteria_t185Dealer', "value": t185Dealer}
							);
						}
	
						var t185Ket = $('#filter_t185Ket input').val();
						if(t185Ket){
							aoData.push(
									{"name": 'sCriteria_t185Ket', "value": t185Ket}
							);
						}
	
						var t185xNamaUser = $('#filter_t185xNamaUser input').val();
						if(t185xNamaUser){
							aoData.push(
									{"name": 'sCriteria_t185xNamaUser', "value": t185xNamaUser}
							);
						}
	
						var t185xNamaDivisi = $('#filter_t185xNamaDivisi input').val();
						if(t185xNamaDivisi){
							aoData.push(
									{"name": 'sCriteria_t185xNamaDivisi', "value": t185xNamaDivisi}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
