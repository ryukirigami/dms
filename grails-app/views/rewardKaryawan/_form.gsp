<%@ page import="com.kombos.hrd.RewardKaryawan" %>
<div class="control-group fieldcontain ${hasErrors(bean: rewardKaryawanInstance, field: 'rewardKaryawan', 'error')} ">
	<label class="control-label" for="rewardKaryawan">
		<g:message code="rewardKaryawan.rewardKaryawan.label" default="Reward Karyawan" />
		
	</label>
	<div class="controls">
	<g:textField name="rewardKaryawan" value="${rewardKaryawanInstance?.rewardKaryawan}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: rewardKaryawanInstance, field: 'keterangan', 'error')} ">
    <label class="control-label" for="keterangan">
        <g:message code="rewardKaryawan.keterangan.label" default="Keterangan" />

    </label>
    <div class="controls">
        <g:textArea  rows="5" cols="3" name="keterangan" value="${rewardKaryawanInstance?.keterangan}" />
    </div>
</div>

<g:javascript>
    $(function() {
        oFormUtils.fnInit();
    })
</g:javascript>