<%@ page import="com.kombos.hrd.RewardKaryawan" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'rewardKaryawan.label', default: 'RewardKaryawan')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="edit-rewardKaryawan" class="content scaffold-edit" role="main">
			<legend>Edit Reward Karyawan</legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${rewardKaryawanInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${rewardKaryawanInstance}" var="error">
                    <li>Field '${error.field}' harus unique!</li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form method="post" >--}%
			<g:formRemote class="form-horizontal" name="edit" on404="alert('not found!');" onSuccess="reloadRewardKaryawanTable();" update="rewardKaryawan-form"
				url="[controller: 'rewardKaryawan', action:'update']">
				<g:hiddenField name="id" value="${rewardKaryawanInstance?.id}" />
				<g:hiddenField name="version" value="${rewardKaryawanInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons controls">
					<g:actionSubmit class="btn btn-primary save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                    <a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout('rewardKaryawan');"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
				</fieldset>
			</g:formRemote>
			%{--</g:form>--}%
		</div>
	</body>
</html>
