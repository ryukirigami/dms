

<%@ page import="com.kombos.hrd.RewardKaryawan" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'rewardKaryawan.label', default: 'RewardKaryawan')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteRewardKaryawan;

$(function(){ 
	deleteRewardKaryawan=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/rewardKaryawan/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadRewardKaryawanTable();
   				expandTableLayout('rewardKaryawan');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-rewardKaryawan" role="main">
		<legend>
			Show Reward Karyawan
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="rewardKaryawan"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${rewardKaryawanInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="rewardKaryawan.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${rewardKaryawanInstance}" field="createdBy"
								url="${request.contextPath}/RewardKaryawan/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadRewardKaryawanTable();" />--}%
							
								<g:fieldValue bean="${rewardKaryawanInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${rewardKaryawanInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="rewardKaryawan.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${rewardKaryawanInstance}" field="dateCreated"
								url="${request.contextPath}/RewardKaryawan/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadRewardKaryawanTable();" />--}%
							
								<g:formatDate date="${rewardKaryawanInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${rewardKaryawanInstance?.historyRewardKaryawan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="historyRewardKaryawan-label" class="property-label"><g:message
					code="rewardKaryawan.historyRewardKaryawan.label" default="History Reward Karyawan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="historyRewardKaryawan-label">
						%{--<ba:editableValue
								bean="${rewardKaryawanInstance}" field="historyRewardKaryawan"
								url="${request.contextPath}/RewardKaryawan/updatefield" type="text"
								title="Enter historyRewardKaryawan" onsuccess="reloadRewardKaryawanTable();" />--}%
							
								<g:each in="${rewardKaryawanInstance.historyRewardKaryawan}" var="h">
								<g:link controller="historyRewardKaryawan" action="show" id="${h.id}">${h?.encodeAsHTML()}</g:link>
								</g:each>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${rewardKaryawanInstance?.keterangan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="keterangan-label" class="property-label"><g:message
					code="rewardKaryawan.keterangan.label" default="Keterangan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="keterangan-label">
						%{--<ba:editableValue
								bean="${rewardKaryawanInstance}" field="keterangan"
								url="${request.contextPath}/RewardKaryawan/updatefield" type="text"
								title="Enter keterangan" onsuccess="reloadRewardKaryawanTable();" />--}%
							
								<g:fieldValue bean="${rewardKaryawanInstance}" field="keterangan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${rewardKaryawanInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="rewardKaryawan.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${rewardKaryawanInstance}" field="lastUpdProcess"
								url="${request.contextPath}/RewardKaryawan/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadRewardKaryawanTable();" />--}%
							
								<g:fieldValue bean="${rewardKaryawanInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${rewardKaryawanInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="rewardKaryawan.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${rewardKaryawanInstance}" field="lastUpdated"
								url="${request.contextPath}/RewardKaryawan/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadRewardKaryawanTable();" />--}%
							
								<g:formatDate date="${rewardKaryawanInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${rewardKaryawanInstance?.rewardKaryawan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="rewardKaryawan-label" class="property-label"><g:message
					code="rewardKaryawan.rewardKaryawan.label" default="Reward Karyawan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="rewardKaryawan-label">
						%{--<ba:editableValue
								bean="${rewardKaryawanInstance}" field="rewardKaryawan"
								url="${request.contextPath}/RewardKaryawan/updatefield" type="text"
								title="Enter rewardKaryawan" onsuccess="reloadRewardKaryawanTable();" />--}%
							
								<g:fieldValue bean="${rewardKaryawanInstance}" field="rewardKaryawan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${rewardKaryawanInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="rewardKaryawan.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${rewardKaryawanInstance}" field="staDel"
								url="${request.contextPath}/RewardKaryawan/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadRewardKaryawanTable();" />--}%
							
								<g:fieldValue bean="${rewardKaryawanInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${rewardKaryawanInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="rewardKaryawan.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${rewardKaryawanInstance}" field="updatedBy"
								url="${request.contextPath}/RewardKaryawan/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadRewardKaryawanTable();" />--}%
							
								<g:fieldValue bean="${rewardKaryawanInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('rewardKaryawan');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${rewardKaryawanInstance?.id}"
					update="[success:'rewardKaryawan-form',failure:'rewardKaryawan-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteRewardKaryawan('${rewardKaryawanInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
