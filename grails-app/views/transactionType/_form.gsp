<%@ page import="com.kombos.finance.TransactionType" %>

<div class="control-group fieldcontain ${hasErrors(bean: transactionTypeInstance, field: 'transactionType', 'error')} ">
    <label class="control-label" for="transactionType">
        <g:message code="transactionType.transactionType.label" default="Transaction Type" />

    </label>
    <div class="controls">
        <g:textField name="transactionType" value="${transactionTypeInstance?.transactionType}" />
    </div>
</div>



%{--<div class="control-group fieldcontain ${hasErrors(bean: transactionTypeInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="transactionType.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${transactionTypeInstance?.lastUpdProcess}" />
	</div>
</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: transactionTypeInstance, field: 'transaction', 'error')} ">
	<label class="control-label" for="transaction">
		<g:message code="transactionType.transaction.label" default="Transaction" />
		
	</label>
	<div class="controls">
	
<ul class="one-to-many">
<g:each in="${transactionTypeInstance?.transaction?}" var="t">
    <li><g:link controller="transaction" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="transaction" action="create" params="['transactionType.id': transactionTypeInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'transaction.label', default: 'Transaction')])}</g:link>
</li>
</ul>

	</div>
</div>--}%



<div class="control-group fieldcontain ${hasErrors(bean: transactionTypeInstance, field: 'description', 'error')} ">
    <label class="control-label" for="description">
        <g:message code="transactionType.description.label" default="Description" />

    </label>
    <div class="controls">

        <g:textArea name="description" value="${transactionTypeInstance?.description}" />
    </div>
</div>