

<%@ page import="com.kombos.finance.TransactionType" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'transactionType.label', default: 'TransactionType')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteTransactionType;

$(function(){ 
	deleteTransactionType=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/transactionType/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadTransactionTypeTable();
   				expandTableLayout('transactionType');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-transactionType" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="transactionType"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${transactionTypeInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="transactionType.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${transactionTypeInstance}" field="createdBy"
								url="${request.contextPath}/TransactionType/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadTransactionTypeTable();" />--}%
							
								<g:fieldValue bean="${transactionTypeInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionTypeInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="transactionType.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${transactionTypeInstance}" field="dateCreated"
								url="${request.contextPath}/TransactionType/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadTransactionTypeTable();" />--}%
							
								<g:formatDate date="${transactionTypeInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionTypeInstance?.description}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="description-label" class="property-label"><g:message
					code="transactionType.description.label" default="Description" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="description-label">
						%{--<ba:editableValue
								bean="${transactionTypeInstance}" field="description"
								url="${request.contextPath}/TransactionType/updatefield" type="text"
								title="Enter description" onsuccess="reloadTransactionTypeTable();" />--}%
							
								<g:fieldValue bean="${transactionTypeInstance}" field="description"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionTypeInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="transactionType.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${transactionTypeInstance}" field="lastUpdProcess"
								url="${request.contextPath}/TransactionType/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadTransactionTypeTable();" />--}%
							
								<g:fieldValue bean="${transactionTypeInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionTypeInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="transactionType.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${transactionTypeInstance}" field="lastUpdated"
								url="${request.contextPath}/TransactionType/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadTransactionTypeTable();" />--}%
							
								<g:formatDate date="${transactionTypeInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionTypeInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="transactionType.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${transactionTypeInstance}" field="staDel"
								url="${request.contextPath}/TransactionType/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadTransactionTypeTable();" />--}%
							
								<g:fieldValue bean="${transactionTypeInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionTypeInstance?.transaction}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="transaction-label" class="property-label"><g:message
					code="transactionType.transaction.label" default="Transaction" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="transaction-label">
						%{--<ba:editableValue
								bean="${transactionTypeInstance}" field="transaction"
								url="${request.contextPath}/TransactionType/updatefield" type="text"
								title="Enter transaction" onsuccess="reloadTransactionTypeTable();" />--}%
							
								<g:each in="${transactionTypeInstance.transaction}" var="t">
								<g:link controller="transaction" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link>
								</g:each>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionTypeInstance?.transactionType}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="transactionType-label" class="property-label"><g:message
					code="transactionType.transactionType.label" default="Transaction Type" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="transactionType-label">
						%{--<ba:editableValue
								bean="${transactionTypeInstance}" field="transactionType"
								url="${request.contextPath}/TransactionType/updatefield" type="text"
								title="Enter transactionType" onsuccess="reloadTransactionTypeTable();" />--}%
							
								<g:fieldValue bean="${transactionTypeInstance}" field="transactionType"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${transactionTypeInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="transactionType.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${transactionTypeInstance}" field="updatedBy"
								url="${request.contextPath}/TransactionType/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadTransactionTypeTable();" />--}%
							
								<g:fieldValue bean="${transactionTypeInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('transactionType');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${transactionTypeInstance?.id}"
					update="[success:'transactionType-form',failure:'transactionType-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteTransactionType('${transactionTypeInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
