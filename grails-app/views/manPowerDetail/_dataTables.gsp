
<%@ page import="com.kombos.administrasi.ManPowerDetail" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="manPowerDetail_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="manPowerDetail.manPower.label" default="Jabatan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="manPowerDetail.m015LevelManPower.label" default="Level Man Power" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="manPowerDetail.m015Inisial.label" default="Inisial Level" /></div>
			</th>

%{--
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="manPowerDetail.staDel.label" default="Sta Del" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="manPowerDetail.m015ID.label" default="M015 ID" /></div>
			</th>
--}%
		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_manPower" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_manPower" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m015LevelManPower" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m015LevelManPower" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m015Inisial" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m015Inisial" class="search_init" />
				</div>
			</th>
%{--	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_staDel" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m015ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m015ID" class="search_init" />
				</div>
			</th>
--}%

		</tr>
	</thead>
</table>

<g:javascript>
var manPowerDetailTable;
var reloadManPowerDetailTable;
$(function(){
	
	reloadManPowerDetailTable = function() {
		manPowerDetailTable.fnDraw();
	}

	var recordsManPowerDetailPerPage = [];
    var anManPowerDetailSelected;
    var jmlRecManPowerDetailPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	manPowerDetailTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	manPowerDetailTable = $('#manPowerDetail_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		
		"fnDrawCallback": function () {
            var rsManPowerDetail = $("#manPowerDetail_datatables tbody .row-select");
            var jmlManPowerDetailCek = 0;
            var nRow;
            var idRec;
            rsManPowerDetail.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsManPowerDetailPerPage[idRec]=="1"){
                    jmlManPowerDetailCek = jmlManPowerDetailCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsManPowerDetailPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecManPowerDetailPerPage = rsManPowerDetail.length;
            if(jmlManPowerDetailCek==jmlRecManPowerDetailPerPage && jmlRecManPowerDetailPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "manPower",
	"mDataProp": "manPower",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m015LevelManPower",
	"mDataProp": "m015LevelManPower",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m015Inisial",
	"mDataProp": "m015Inisial",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "staDel",
	"mDataProp": "staDel",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "m015ID",
	"mDataProp": "m015ID",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var manPower = $('#filter_manPower input').val();
						if(manPower){
							aoData.push(
									{"name": 'sCriteria_manPower', "value": manPower}
							);
						}
	
						var m015LevelManPower = $('#filter_m015LevelManPower input').val();
						if(m015LevelManPower){
							aoData.push(
									{"name": 'sCriteria_m015LevelManPower', "value": m015LevelManPower}
							);
						}
	
						var m015Inisial = $('#filter_m015Inisial input').val();
						if(m015Inisial){
							aoData.push(
									{"name": 'sCriteria_m015Inisial', "value": m015Inisial}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var m015ID = $('#filter_m015ID input').val();
						if(m015ID){
							aoData.push(
									{"name": 'sCriteria_m015ID', "value": m015ID}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

	$('.select-all').click(function(e) {

        $("#manPowerDetail_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsManPowerDetailPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsManPowerDetailPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#manPowerDetail_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsManPowerDetailPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anManPowerDetailSelected = manPowerDetailTable.$('tr.row_selected');
            if(jmlRecManPowerDetailPerPage == anManPowerDetailSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsManPowerDetailPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
