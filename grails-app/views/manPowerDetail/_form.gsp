<%@ page import="com.kombos.administrasi.ManPower; com.kombos.administrasi.ManPowerDetail" %>



<div class="control-group fieldcontain ${hasErrors(bean: manPowerDetailInstance, field: 'manPower', 'error')} required">
	<label class="control-label" for="manPower">
		<g:message code="manPowerDetail.manPower.label" default="Jabatan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="manPower" name="manPower.id" from="${ManPower.createCriteria().list{eq("staDel", "0");order("m014JabatanManPower","asc")}}" optionKey="id" required="" value="${manPowerDetailInstance?.manPower?.id}" class="many-to-one"/>
	</div>
</div>

<g:javascript>
    document.getElementById("manPower").focus();
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: manPowerDetailInstance, field: 'm015LevelManPower', 'error')} required">
	<label class="control-label" for="m015LevelManPower">
		<g:message code="manPowerDetail.m015LevelManPower.label" default="Level Man Power" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m015LevelManPower" maxlength="50" required="" value="${manPowerDetailInstance?.m015LevelManPower}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: manPowerDetailInstance, field: 'm015Inisial', 'error')} required">
	<label class="control-label" for="m015Inisial">
		<g:message code="manPowerDetail.m015Inisial.label" default="Inisial Level" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m015Inisial" maxlength="4" required="" value="${manPowerDetailInstance?.m015Inisial}"/>
	</div>
</div>
%{--
<div class="control-group fieldcontain ${hasErrors(bean: manPowerDetailInstance, field: 'staDel', 'error')} required">
	<label class="control-label" for="staDel">
		<g:message code="manPowerDetail.staDel.label" default="Sta Del" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="staDel" maxlength="1" required="" value="${manPowerDetailInstance?.staDel}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: manPowerDetailInstance, field: 'm015ID', 'error')} ">
	<label class="control-label" for="m015ID">
		<g:message code="manPowerDetail.m015ID.label" default="M015 ID" />
		
	</label>
	<div class="controls">
	<g:textField name="m015ID" maxlength="2" value="${manPowerDetailInstance?.m015ID}"/>
	</div>
</div>
--}%
