

<%@ page import="com.kombos.administrasi.ManPowerDetail" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'manPowerDetail.label', default: 'Level Man Power')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteManPowerDetail;

$(function(){ 
	deleteManPowerDetail=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/manPowerDetail/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadManPowerDetailTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-manPowerDetail" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="manPowerDetail"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${manPowerDetailInstance?.manPower}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="manPower-label" class="property-label"><g:message
					code="manPowerDetail.manPower.label" default="Jabatan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="manPower-label">
						%{--<ba:editableValue
								bean="${manPowerDetailInstance}" field="manPower"
								url="${request.contextPath}/ManPowerDetail/updatefield" type="text"
								title="Enter manPower" onsuccess="reloadManPowerDetailTable();" />--}%
							
								${manPowerDetailInstance?.manPower?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${manPowerDetailInstance?.m015LevelManPower}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m015LevelManPower-label" class="property-label"><g:message
					code="manPowerDetail.m015LevelManPower.label" default="Level Man Power" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m015LevelManPower-label">
						%{--<ba:editableValue
								bean="${manPowerDetailInstance}" field="m015LevelManPower"
								url="${request.contextPath}/ManPowerDetail/updatefield" type="text"
								title="Enter m015LevelManPower" onsuccess="reloadManPowerDetailTable();" />--}%
							
								<g:fieldValue bean="${manPowerDetailInstance}" field="m015LevelManPower"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${manPowerDetailInstance?.m015Inisial}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m015Inisial-label" class="property-label"><g:message
					code="manPowerDetail.m015Inisial.label" default="Inisial Level" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m015Inisial-label">
						%{--<ba:editableValue
								bean="${manPowerDetailInstance}" field="m015Inisial"
								url="${request.contextPath}/ManPowerDetail/updatefield" type="text"
								title="Enter m015Inisial" onsuccess="reloadManPowerDetailTable();" />--}%
							
								<g:fieldValue bean="${manPowerDetailInstance}" field="m015Inisial"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${manPowerDetailInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="manPowerDetail.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${manPowerDetailInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/manPowerDetail/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadmanPowerDetailTable();" />--}%

                        <g:fieldValue bean="${manPowerDetailInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${manPowerDetailInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="manPowerDetail.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${manPowerDetailInstance}" field="dateCreated"
                                url="${request.contextPath}/manPowerDetail/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadmanPowerDetailTable();" />--}%

                        <g:formatDate date="${manPowerDetailInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${manPowerDetailInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="manPowerDetail.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${manPowerDetailInstance}" field="createdBy"
                                url="${request.contextPath}/manPowerDetail/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadmanPowerDetailTable();" />--}%

                        <g:fieldValue bean="${manPowerDetailInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${manPowerDetailInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="manPowerDetail.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${manPowerDetailInstance}" field="lastUpdated"
                                url="${request.contextPath}/manPowerDetail/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadmanPowerDetailTable();" />--}%

                        <g:formatDate date="${manPowerDetailInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${manPowerDetailInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="manPowerDetail.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${manPowerDetailInstance}" field="updatedBy"
                                url="${request.contextPath}/manPowerDetail/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadmanPowerDetailTable();" />--}%

                        <g:fieldValue bean="${manPowerDetailInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
<!--			
				<g:if test="${manPowerDetailInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="manPowerDetail.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${manPowerDetailInstance}" field="staDel"
								url="${request.contextPath}/ManPowerDetail/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadManPowerDetailTable();" />--}%
							
								<g:fieldValue bean="${manPowerDetailInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${manPowerDetailInstance?.m015ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m015ID-label" class="property-label"><g:message
					code="manPowerDetail.m015ID.label" default="M015 ID" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m015ID-label">
						%{--<ba:editableValue
								bean="${manPowerDetailInstance}" field="m015ID"
								url="${request.contextPath}/ManPowerDetail/updatefield" type="text"
								title="Enter m015ID" onsuccess="reloadManPowerDetailTable();" />--}%
							
								<g:fieldValue bean="${manPowerDetailInstance}" field="m015ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
-->			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${manPowerDetailInstance?.id}"
					update="[success:'manPowerDetail-form',failure:'manPowerDetail-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteManPowerDetail('${manPowerDetailInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
