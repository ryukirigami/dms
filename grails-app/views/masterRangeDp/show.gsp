

<%@ page import="com.kombos.maintable.MasterRangeDp" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'masterRangeDp.label', default: 'Master Maintenance Range DP')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteMasterRangeDp;

$(function(){ 
	deleteMasterRangeDp=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/masterRangeDp/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadMasterRangeDpTable();
   				expandTableLayout('masterRangeDp');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-masterRangeDp" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="masterRangeDp"
			class="table table-bordered table-hover">
			<tbody>

            <g:if test="${masterRangeDpInstance?.nilaiParts2}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="nilaiParts2-label" class="property-label"><g:message
                                code="masterRangeDp.nilaiParts2.label" default="Range Harga Parts" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="nilaiParts2-label">
                        %{--<ba:editableValue
                                bean="${masterRangeDpInstance}" field="nilaiParts2"
                                url="${request.contextPath}/MasterRangeDp/updatefield" type="text"
                                title="Enter nilaiParts2" onsuccess="reloadMasterRangeDpTable();" />--}%

                        <g:fieldValue bean="${masterRangeDpInstance}" field="nilaiParts1"/> -
                        <g:fieldValue bean="${masterRangeDpInstance}" field="nilaiParts2"/> Juta

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${masterRangeDpInstance?.persen}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="persen-label" class="property-label"><g:message
                                code="masterRangeDp.persen.label" default="Persen" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="persen-label">
                        %{--<ba:editableValue
                                bean="${masterRangeDpInstance}" field="persen"
                                url="${request.contextPath}/MasterRangeDp/updatefield" type="text"
                                title="Enter persen" onsuccess="reloadMasterRangeDpTable();" />--}%

                        <g:fieldValue bean="${masterRangeDpInstance}" field="persen"/> %

                    </span></td>

                </tr>
            </g:if>

				<g:if test="${masterRangeDpInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="masterRangeDp.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${masterRangeDpInstance}" field="createdBy"
								url="${request.contextPath}/MasterRangeDp/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadMasterRangeDpTable();" />--}%
							
								<g:fieldValue bean="${masterRangeDpInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterRangeDpInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="masterRangeDp.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${masterRangeDpInstance}" field="dateCreated"
								url="${request.contextPath}/MasterRangeDp/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadMasterRangeDpTable();" />--}%
							
								<g:formatDate date="${masterRangeDpInstance?.dateCreated}" format="EEEE, dd MMMM yyyy HH:mm"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterRangeDpInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="masterRangeDp.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${masterRangeDpInstance}" field="lastUpdProcess"
								url="${request.contextPath}/MasterRangeDp/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadMasterRangeDpTable();" />--}%
							
								<g:fieldValue bean="${masterRangeDpInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterRangeDpInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="masterRangeDp.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${masterRangeDpInstance}" field="lastUpdated"
								url="${request.contextPath}/MasterRangeDp/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadMasterRangeDpTable();" />--}%
							
								<g:formatDate date="${masterRangeDpInstance?.lastUpdated}" format="EEEE, dd MMMM yyyy HH:mm"/>
							
						</span></td>
					
				</tr>
				</g:if>
			

			
				<g:if test="${masterRangeDpInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="masterRangeDp.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${masterRangeDpInstance}" field="updatedBy"
								url="${request.contextPath}/MasterRangeDp/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadMasterRangeDpTable();" />--}%
							
								<g:fieldValue bean="${masterRangeDpInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('masterRangeDp');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${masterRangeDpInstance?.id}"
					update="[success:'masterRangeDp-form',failure:'masterRangeDp-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteMasterRangeDp('${masterRangeDpInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
