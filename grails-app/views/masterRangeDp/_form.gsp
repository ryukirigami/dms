<%@ page import="com.kombos.maintable.MasterRangeDp" %>
<script type="text/javascript">
    jQuery(function($) {
        $('.angka').autoNumeric('init',{
            vMin:'0',
            vMax:'100',
            aSep:'',
            mDec: '0'
        });
    });
 </script>

<div class="control-group fieldcontain ${hasErrors(bean: masterRangeDpInstance, field: 'nilaiParts1', 'error')} ">
	<label class="control-label" for="nilaiParts1">
		<g:message code="masterRangeDp.nilaiParts1.label" default="Range Harga Parts" />
		
	</label>
	<div class="controls">
	<g:textField name="nilaiParts1" style="width:32px" value="${masterRangeDpInstance.nilaiParts1}" class="angka" maxlength="3" /> -
    <g:textField name="nilaiParts2" style="width:32px" value="${masterRangeDpInstance.nilaiParts2}" class="angka" maxlength="3" /> Juta
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterRangeDpInstance, field: 'persen', 'error')} ">
	<label class="control-label" for="persen">
		<g:message code="masterRangeDp.persen.label" default="Persen" />
		
	</label>
	<div class="controls">
	<g:textField name="persen" style="width:75px" value="${masterRangeDpInstance.persen}" class="angka" maxlength="3" /> Persen
	</div>
</div>

