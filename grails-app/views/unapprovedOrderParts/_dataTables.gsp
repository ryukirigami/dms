
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay" />
<fieldset id="uop-search" class="buttons controls">
    <table style="padding-right: 10px">
        <tr>
            <td style="width: 130px">
                <label class="control-label" for="search_tanggalFrom">Tanggal&nbsp;</label>
            </td>
            <td>
                <div id="filter_tanggal" class="controls">
                    <ba:datePicker id="search_tanggalFrom" name="search_tanggalFrom"
                                   precision="day" format="dd/MM/yyyy" value="" />
                    s.d.
                    <ba:datePicker id="search_tanggalTo" name="search_tanggalTo"
                                   precision="day" format="dd/MM/yyyy" value="" />
                </div>
            </td>
        </tr>
        <tr>
            <td style="width: 130px">
                <label class="control-label" for="search_column">Cari di kolom&nbsp; </label>
            </td>
            <td>

                <div id="filter_status" class="controls">
                    <g:select name="search_column" id="search_column"
                              from="['noReferensi': 'Nomor Referensi', 'kodePart': 'Kode Part', 'namaPart': 'Nama Part', 'qty': 'Qty', 'satuan': 'Satuan', 'vendor': 'Vendor', 'hargaSatuan': 'Harga Satuan', 'totalHarga': 'Total Harga']"
                              optionKey="key" optionValue="value"
                              noSelection="['null': '[Pilih Kolom]']" />
                </div>
            </td>
        </tr>
        <tr>
            <td style="width: 130px">
                <label class="control-label" for="search_value">Data yang dicari&nbsp;</label>
            </td>
            <td>
                <div class="controls">
                    <g:textField name="search_value" id="search_value" />
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" >
                <div class="controls" style="right: 0">
                    <button style="width: 70px; height: 30px; border-radius: 5px"
                            class="btn btn-primary view" name="view" id="view" onClick="search();">Search</button>
                    <button style="width: 70px; height: 30px; border-radius: 5px"
                            class="btn btn-cancel view" name="clear" id="clear" onClick="clearAll();">Clear</button>
                </div>
            </td>
        </tr>
    </table>
</fieldset>
<table id="vop_datatables_${idTable}" cellpadding="0" cellspacing="0" border="0"
	class="display table table-striped table-bordered table-hover" 
	width="100%">
	<thead>
		<tr>
			<th style="border-bottom: none; padding: 5px;">
				<div>Tanggal Request</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Nomor Referensi</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Kode Part</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Nama Part</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Qty</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Satuan</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Vendor</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Harga Satuan (Rp)</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Total Harga (Rp)</div>
			</th>
		</tr>
		
	</thead>
</table>

<g:javascript>
var vopTable;
var search;
$(function(){ 
	search = function() {
		vopTable.fnDraw();
	}

	clearAll = function(){
	    $('#search_tanggalFrom').val('');
	    $('#search_tanggalTo').val('');
	    $('#search_value').val('');
	    document.getElementById("search_column").selectedIndex = 0 ;
	    vopTable.fnDraw();
	}

 	vopTable = $('#vop_datatables_${idTable}').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
	"sName": "tanggalRequest",
	"mDataProp": "tanggalRequest",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return data;
	},
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "noReferensi",
	"mDataProp": "noReferensi",
	"aTargets": [1],
	"bSortable": true,
	"sWidth":"190px",
	"bVisible": true
},
{
	"sName": "kodePart",
	"mDataProp": "kodePart",
	"aTargets": [2],
	"bSortable": true,
	"sWidth":"190px",
	"bVisible": true
},
{
	"sName": "namaPart",
	"mDataProp": "namaPart",
	"aTargets": [3],
	"bSortable": true,
	"sWidth":"190px",
	"bVisible": true
},
{
	"sName": "qty",
	"mDataProp": "qty",
	"aTargets": [4],
	"bSortable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "satuan",
	"mDataProp": "satuan",
	"aTargets": [5],
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "vendor",
	"mDataProp": "vendor",
	"aTargets": [6],
	"bSortable": true,
	"sWidth":"190px",
	"bVisible": true
},
{
	"sName": "hargaSatuan",
	"mDataProp": "hargaSatuan",
	"aTargets": [7],
	"bSortable": true,
	"sWidth":"190px",
	"bVisible": true
},
{
	"sName": "totalHarga",
	"mDataProp": "totalHarga",
	"aTargets": [8],
	"bSortable": true,
	"sWidth":"190px",
	"bVisible": true
}],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						var tanggalFrom = $('#search_tanggalFrom').val();
						var tanggalFromDay = $('#search_tanggalFrom_day').val();
						var tanggalFromMonth = $('#search_tanggalFrom_month').val();
						var tanggalFromYear = $('#search_tanggalFrom_year').val();
						
						if(tanggalFrom){
							aoData.push(
									{"name": 'sCriteria_tanggalFrom', "value": "date.struct"},
									{"name": 'sCriteria_tanggalFrom_dp', "value": tanggalFrom},
									{"name": 'sCriteria_tanggalFrom_day', "value": tanggalFromDay},
									{"name": 'sCriteria_tanggalFrom_month', "value": tanggalFromMonth},
									{"name": 'sCriteria_tanggalFrom_year', "value": tanggalFromYear}
							);
						}
						
						var tanggalTo = $('#search_tanggalTo').val();
						var tanggalToDay = $('#search_tanggalTo_day').val();
						var tanggalToMonth = $('#search_tanggalTo_month').val();
						var tanggalToYear = $('#search_tanggalTo_year').val();
						
						if(tanggalTo){
							aoData.push(
									{"name": 'sCriteria_tanggalTo', "value": "date.struct"},
									{"name": 'sCriteria_tanggalTo_dp', "value": tanggalTo},
									{"name": 'sCriteria_tanggalTo_day', "value": tanggalToDay},
									{"name": 'sCriteria_tanggalTo_month', "value": tanggalToMonth},
									{"name": 'sCriteria_tanggalTo_year', "value": tanggalToYear}
							);
						}

						var column = $('#search_column').val();
						if(column){
							var val = $('#search_value').val();
							if(val){
								aoData.push(
										{"name": 'sCriteria_column', "value": column}
								);
								aoData.push(
										{"name": 'sCriteria_value', "value": val}
								);
							}
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});



</g:javascript>



