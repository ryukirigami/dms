

<%@ page import="com.kombos.parts.Request" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<title>Unapproved Order Parts</title>
</head>
<body>
	<div id="show-unapproved-order-parts" role="main">
		<legend>
			Unapproved Order Parts
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="unapproved-order-parts-detail"
			class="table table-bordered table-hover">
			<tbody>				
				<tr><td class="span2" style="text-align: right;">
					<span id="tanggal-label" class="property-label">Tanggal Request:</span></td>
					<td class="span3"><span class="property-value"
						aria-labelledby="tanggal-label">
								<g:formatDate format="${dateTimeFormat}" date="${requestInstance.t162TglRequest}"/>
						</span></td></tr>
				<tr><td class="span2" style="text-align: right;">
					<span id="pemohon-label" class="property-label">Pemohon:</span></td>
					<td class="span3"><span class="property-value"
						aria-labelledby="pemohon-label">
								<g:fieldValue bean="${requestInstance}" field="t162NamaPemohon"/>
						</span></td></tr>
				<tr><td class="span2" style="text-align: right;">
					<span id="noReferensi-label" class="property-label">Nomor Referensi:</span></td>
					<td class="span3"><span class="property-value"
						aria-labelledby="noReferensi-label">
								<g:fieldValue bean="${requestInstance}" field="t162NoReff"/>
						</span></td></tr>
				<tr><td class="span2" style="text-align: right;">
					<span id="kodePart-label" class="property-label">Kode Part:</span></td>
					<td class="span3"><span class="property-value"
						aria-labelledby="kodePart-label">
								<g:fieldValue bean="${requestInstance}" field="goods.m111ID"/>
						</span></td></tr>
				<tr><td class="span2" style="text-align: right;">
					<span id="namaPart-label" class="property-label">Nama Part:</span></td>
					<td class="span3"><span class="property-value"
						aria-labelledby="namaPart-label">
								<g:fieldValue bean="${requestInstance}" field="goods.m111Nama"/>
						</span></td></tr>
				<tr><td class="span2" style="text-align: right;">
					<span id="qty-label" class="property-label">Qty:</span></td>
					<td class="span3"><span class="property-value"
						aria-labelledby="qty-label">
								${requestInstance.validasiOrder?.t163Qty1}
						</span></td></tr>
				<tr><td class="span2" style="text-align: right;">
					<span id="satuan-label" class="property-label">Satuan:</span></td>
					<td class="span3"><span class="property-value"
						aria-labelledby="satuan-label">
								<g:fieldValue bean="${requestInstance}" field="goods.satuan.m118Satuan1"/>
						</span></td></tr>
				<tr><td class="span2" style="text-align: right;">
					<span id="vendor-label" class="property-label">Vendor:</span></td>
					<td class="span3"><span class="property-value"
						aria-labelledby="vendor-label">
								${requestInstance.validasiOrder?.vendor?.m121Nama}
						</span></td></tr>
				<tr><td class="span2" style="text-align: right;">
					<span id="hargaSatuan-label" class="property-label">Harga Satuan (Rp):</span></td>
					<td class="span3"><span class="property-value"
						aria-labelledby="hargaSatuan-label">
								${requestInstance.validasiOrder?.t163HargaSatuan}
						</span></td></tr>
				<tr><td class="span2" style="text-align: right;">
					<span id="totalHarga-label" class="property-label">Total Harga (Rp):</span></td>
					<td class="span3"><span class="property-value"
						aria-labelledby="totalHarga-label">
								${requestInstance.validasiOrder?.t163TotalHarga}
						</span></td></tr>
				<tr><td class="span2" style="text-align: right;">
					<span id="tanggalUnapproved-label" class="property-label">Tanggal UnApproved:</span></td>
					<td class="span3"><span class="property-value"
						aria-labelledby="tanggalUnapproved-label">
								<g:formatDate format="${dateTimeFormat}" date="${requestInstance.validasiOrder?.t163TglApproveUnApprove}"/>
						</span></td></tr>
				<tr><td class="span2" style="text-align: right;">
					<span id="keterangan-label" class="property-label">Keterangan Tidak Disetujui:</span></td>
					<td class="span3"><span class="property-value"
						aria-labelledby="keterangan-label">
								${requestInstance.validasiOrder?.t163AlasanUnApprove}
						</span></td></tr>
										
			</tbody>
		</table>
		<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
		</fieldset>
	</div>
</body>
</html>
