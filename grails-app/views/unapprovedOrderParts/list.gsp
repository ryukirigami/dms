<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="title" value="UnApproved Order Parts" />
		<title>${title}</title>
		<r:require modules="baseapplayout" />
		<g:javascript>
	var show;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){    
	    shrinkTableLayout = function(){
	    	if($("#uop-table").hasClass("span12")){
	   			$("#uop-table").toggleClass("span12 span5");
	        }
	        $("#uop-form").css("display","block"); 
	        $('#uop-search').css("display","none"); 
	   	}
	   	
	   	expandTableLayout = function(){
	   		if($("#uop-table").hasClass("span5")){
	   			$("#uop-table").toggleClass("span5 span12");
	   		}
	        $("#uop-form").css("display","none");
	        $('#uop-search').css("display","block");
	   	}
	   	show = function(id) {
    		shrinkTableLayout();
       		$('#spinner').fadeIn(1);
   			$.ajax({type:'POST', url:'${request.contextPath}/unapprovedOrderParts/show/'+id,
   				success:function(data,textStatus){
   					$('#uop-form').html(data);   					
   				},
   				error:function(XMLHttpRequest,textStatus,errorThrown){},
   				complete:function(XMLHttpRequest,textStatus){
   					$('#spinner').fadeOut();
   				}
   			});
    	};
   	});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left">${title}</span>
		<ul class="nav pull-right">
			
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="uop-table">
		   	<g:render template="dataTables" />
         </div>
		<div class="span7" id="uop-form" style="display: none;"></div>
	</div>
</body>
</html>
