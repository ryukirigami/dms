

<%@ page import="com.kombos.administrasi.UserPrinter" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'userPrinter.label', default: 'User Printer')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteUserPrinter;

$(function(){ 
	deleteUserPrinter=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/userPrinter/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadUserPrinterTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-userPrinter" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="userPrinter"
			class="table table-bordered table-hover">
			<tbody>

            <g:if test="${userPrinterInstance?.userProfile?.companyDealer}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="companyDealer-label" class="property-label"><g:message
                                code="userPrinter.companyDealer.label" default="Workshop / Office" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="companyDealer-label">
                        <g:fieldValue field="m011NamaWorkshop" bean="${userPrinterInstance?.userProfile?.companyDealer}" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${userPrinterInstance?.userProfile?.divisi}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="divisi-label" class="property-label"><g:message
                                code="userPrinter.divisi.label" default="Divisi" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="divisi-label">
                        <g:fieldValue field="m012NamaDivisi" bean="${userPrinterInstance?.userProfile?.divisi}" />

                    </span></td>

                </tr>
            </g:if>


            <g:if test="${userPrinterInstance?.userProfile}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="userProfile-label" class="property-label"><g:message
					code="userPrinter.userProfile.label" default="Nama Pegawai" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="userProfile-label">
						<g:fieldValue field="t001NamaPegawai" bean="${userPrinterInstance?.userProfile}" />

						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${userPrinterInstance?.t005PathPrinter}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t005PathPrinter-label" class="property-label"><g:message
					code="userPrinter.t005PathPrinter.label" default="Path Printer" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="t005PathPrinter-label">
						<g:fieldValue field="t005PathPrinter" bean="${userPrinterInstance}" />

						</span></td>

				</tr>
				</g:if>

            <g:if test="${userPrinterInstance?.namaDokumen}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="namaDokumen-label" class="property-label"><g:message
					code="userPrinter.namaDokumen.label" default="Nama Pegawai" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="namaDokumen-label">
						<g:fieldValue field="m007NamaDokumen" bean="${userPrinterInstance?.namaDokumen}" />

						</span></td>

				</tr>
				</g:if>

            <g:if test="${userPrinterInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="userPrinter.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${userPrinterInstance}" field="dateCreated"
                                url="${request.contextPath}/UserPrinter/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadUserPrinterTable();" />--}%

                        <g:formatDate date="${userPrinterInstance?.dateCreated}" format="dd MMMM yyyy, hh:mm" />

                    </span></td>

                </tr>
            </g:if>


            <g:if test="${userPrinterInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="userPrinter.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${userPrinterInstance}" field="createdBy"
								url="${request.contextPath}/UserPrinter/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadUserPrinterTable();" />--}%
							
								<g:fieldValue bean="${userPrinterInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${userPrinterInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="userPrinter.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${userPrinterInstance}" field="lastUpdated"
                                url="${request.contextPath}/UserPrinter/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadUserPrinterTable();" />--}%

                        <g:formatDate date="${userPrinterInstance?.lastUpdated}" format="dd MMMM yyyy, hh:mm"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${userPrinterInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="userPrinter.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${userPrinterInstance}" field="updatedBy"
								url="${request.contextPath}/UserPrinter/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadUserPrinterTable();" />--}%
							
								<g:fieldValue bean="${userPrinterInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
            <g:if test="${userPrinterInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="userPrinter.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${userPrinterInstance}" field="lastUpdProcess"
								url="${request.contextPath}/UserPrinter/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadUserPrinterTable();" />--}%

								<g:fieldValue bean="${userPrinterInstance}" field="lastUpdProcess"/>

						</span></td>

				</tr>
				</g:if>


			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${userPrinterInstance?.id}"
					update="[success:'userPrinter-form',failure:'userPrinter-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteUserPrinter('${userPrinterInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
