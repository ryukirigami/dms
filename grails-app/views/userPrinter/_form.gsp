<%@ page import="com.kombos.administrasi.NamaDokumen; com.kombos.administrasi.Divisi; com.kombos.baseapp.sec.shiro.User; com.kombos.administrasi.UserPrinter" %>

<script language="Javascript">
    function loadDivision(idInit)
    {
        var root="${resource()}";
        var companyDealerId=document.getElementById('companyDealer').value;

        if(idInit){
            companyDealerId = idInit;
        }

        var url = root+'/userPrinter/findDivisionByCompanyDealer?companyDealer.id='+companyDealerId;

        jQuery('#divisi').load(url);
    }

    function loadUser(idInit)
    {
        var root="${resource()}";
        var divisiId=document.getElementById('divisi').value;

        if(idInit){
            divisiId = idInit;
        }

        var url = root+'/userPrinter/findUserByDivisi?divisi.id='+divisiId;

        jQuery('#userProfile').load(url);
    }

    $(function() {
        var cek = '${userPrinterInstance?.id}'
        var root="${resource()}";
        if(cek==''){
            var url = root+'/userPrinter/getPrinter';
        }else{
            var url = root+'/userPrinter/getPrinter?id='+cek;
        }
        jQuery('#t005PathPrinter').load(url);
    });
</script>


<div class="control-group fieldcontain ${hasErrors(bean: userPrinterInstance?.userProfile, field: 'companyDealer', 'error')} required">
    <label class="control-label" for="companyDealer">
        <g:message code="app.user.companyDealer.label" default="Workshop / Office" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select required="" name="companyDealer.id" noSelection="['-1':'Pilih Workshop']" id="companyDealer" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list{eq("staDel","0");order("m011NamaWorkshop")}}" optionKey="id" onchange='loadDivision();' value="${userPrinterInstance?.userProfile?.companyDealer?.id}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userPrinterInstance?.userProfile, field: 'divisi', 'error')} required">
    <label class="control-label" for="divisi">
        <g:message code="app.user.divisi.label" default="Divisi" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:if test="${userPrinterInstance?.userProfile}">
            <g:select id="divisi" name="divisi.id" from="${Divisi.createCriteria().list {eq("staDel","0");companyDealer{eq("id",userPrinterInstance?.userProfile?.companyDealer?.id)};order("m012NamaDivisi")}}" noSelection="['-1':'Pilih Divisi']" optionKey="id" optionValue="m012NamaDivisi" required="" onchange="loadUser();" value="${userPrinterInstance?.userProfile?.divisi?.id}" />
        </g:if>
        <g:else>
            <g:select id="divisi" name="divisi.id" from="${[]}" noSelection="['-1':'Pilih Divisi']" value="${userPrinterInstance?.userProfile?.divisi?.id}" optionKey="id" required="" onchange="loadUser();"/>
        </g:else>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userPrinterInstance, field: 'userProfile', 'error')} required">
	<label class="control-label" for="userProfile">
		<g:message code="userPrinter.userProfile.label" default="Nama Pegawai" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:if test="${userPrinterInstance?.userProfile}">
            <g:select id="userProfile" name="userProfile.id" noSelection="['':'Pilih Pegawai']" from="${User.createCriteria().list{eq("staDel","0");companyDealer{eq("id",userPrinterInstance?.userProfile?.companyDealer?.id)};divisi{eq("id",userPrinterInstance?.userProfile?.divisi?.id)};order("t001NamaPegawai")}}" optionKey="username" optionValue="t001NamaPegawai" required="" value="${userPrinterInstance?.userProfile?.username}" class="many-to-one"/>
        </g:if>
        <g:else>
            <g:select noSelection="['':'Pilih Pegawai']" id="userProfile" name="userProfile.id" from="${[]}" optionKey="username" required="" value="${userPrinterInstance?.userProfile?.id}" class="many-to-one" />
        </g:else>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userPrinterInstance, field: 't005PathPrinter', 'error')} required">
	<label class="control-label" for="t005PathPrinter">
		<g:message code="userPrinter.t005PathPrinter.label" default="Path Printer" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:select noSelection="['':'Pilih Path Printer']" id="t005PathPrinter" name="t005PathPrinter" from="${[]}" required="" value="${userPrinterInstance?.t005PathPrinter}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userPrinterInstance, field: 'namaDokumen', 'error')} required">
    <label class="control-label" for="namaDokumen">
        <g:message code="userPrinter.namaDokumen.label" default="Nama Dokumen" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="namaDokumen" name="namaDokumen.id" noSelection="['':'Pilih Dokumen']" from="${NamaDokumen.createCriteria().list{eq("staDel","0");order("m007NamaDokumen")}}" optionKey="id" required="" value="${userPrinterInstance?.namaDokumen?.id}" class="many-to-one"/>
    </div>
</div>
