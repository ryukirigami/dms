
<%@ page import="com.kombos.administrasi.UserPrinter" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="userPrinter_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="userPrinter.companyDealer.label" default="Workshop / Office" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="userPrinter.divisi.label" default="Divisi" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="userPrinter.namaPegawai.label" default="Nama Pegawai" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="userPrinter.t005PathPrinter.label" default="Path Printer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="userPrinter.namaDokumen.label" default="Nama Dokumen" /></div>
			</th>


		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_companyDealer" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_divisi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_divisi" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_userProfile" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_userProfile" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_t005PathPrinter" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t005PathPrinter" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
				<div id="filter_namaDokumen" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_namaDokumen" class="search_init" />
				</div>
			</th>

		</tr>
	</thead>
</table>

<g:javascript>
var userPrinterTable;
var reloadUserPrinterTable;
$(function(){
	
	reloadUserPrinterTable = function() {
		userPrinterTable.fnDraw();
	}

    var recordsuserPrinterperpage = [];
    var anUserPrinterSelected;
    var jmlRecUserPrinterPerPage=0;
    var id;

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	userPrinterTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	userPrinterTable = $('#userPrinter_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsUserPrinter = $("#userPrinter_datatables tbody .row-select");
            var jmlUserPrinterCek = 0;
            var nRow;
            var idRec;
            rsUserPrinter.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsuserPrinterperpage[idRec]=="1"){
                    jmlUserPrinterCek = jmlUserPrinterCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsuserPrinterperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecUserPrinterPerPage = rsUserPrinter.length;
            if(jmlUserPrinterCek==jmlRecUserPrinterPerPage && jmlRecUserPrinterPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "companyDealer",
	"mDataProp": "companyDealer",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "divisi",
	"mDataProp": "divisi",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "userProfile",
	"mDataProp": "userProfile",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t005PathPrinter",
	"mDataProp": "t005PathPrinter",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "namaDokumen",
	"mDataProp": "namaDokumen",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}
	
						var divisi = $('#filter_divisi input').val();
						if(divisi){
							aoData.push(
									{"name": 'sCriteria_divisi', "value": divisi}
							);
						}
	
						var t005PathPrinter = $('#filter_t005PathPrinter input').val();
						if(t005PathPrinter){
							aoData.push(
									{"name": 'sCriteria_t005PathPrinter', "value": t005PathPrinter}
							);
						}

						var userProfile = $('#filter_userProfile input').val();
						if(userProfile){
							aoData.push(
									{"name": 'sCriteria_userProfile', "value": userProfile}
							);
						}
	
						var namaDokumen = $('#filter_namaDokumen input').val();
						if(namaDokumen){
							aoData.push(
									{"name": 'sCriteria_namaDokumen', "value": namaDokumen}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
    $('.select-all').click(function(e) {
        $("#userPrinter_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsuserPrinterperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsuserPrinterperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

    $('#userPrinter_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsuserPrinterperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anUserPrinterSelected = userPrinterTable.$('tr.row_selected');
            if(jmlRecUserPrinterPerPage == anUserPrinterSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsuserPrinterperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
