

<%@ page import="com.kombos.parts.RPP" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'RPP.label', default: 'RPP')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteRPP;

$(function(){ 
	deleteRPP=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/RPP/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadRPPTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-RPP" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="RPP"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${RPPInstance?.m161TglBerlaku}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m161TglBerlaku-label" class="property-label"><g:message
					code="RPP.m161TglBerlaku.label" default="Tanggal Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m161TglBerlaku-label">
						%{--<ba:editableValue
								bean="${RPPInstance}" field="m161TglBerlaku"
								url="${request.contextPath}/RPP/updatefield" type="text"
								title="Enter m161TglBerlaku" onsuccess="reloadRPPTable();" />--}%
							
								<g:formatDate date="${RPPInstance?.m161TglBerlaku}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${RPPInstance?.m161MasaPengajuanRPP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m161MasaPengajuanRPP-label" class="property-label"><g:message
					code="RPP.m161MasaPengajuanRPP.label" default="Masa Pengajuan RPP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m161MasaPengajuanRPP-label">
						%{--<ba:editableValue
								bean="${RPPInstance}" field="m161MasaPengajuanRPP"
								url="${request.contextPath}/RPP/updatefield" type="text"
								title="Enter m161MasaPengajuanRPP" onsuccess="reloadRPPTable();" />--}%
							
								<g:fieldValue bean="${RPPInstance}" field="m161MasaPengajuanRPP"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${RPPInstance?.m161MaxDapatRPP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m161MaxDapatRPP-label" class="property-label"><g:message
					code="RPP.m161MaxDapatRPP.label" default="Harga Max Syarat Mendapat RPP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m161MaxDapatRPP-label">
						%{--<ba:editableValue
								bean="${RPPInstance}" field="m161MaxDapatRPP"
								url="${request.contextPath}/RPP/updatefield" type="text"
								title="Enter m161MaxDapatRPP" onsuccess="reloadRPPTable();" />--}%
							
								<g:fieldValue bean="${RPPInstance}" field="m161MaxDapatRPP"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${RPPInstance?.companyDealer}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="companyDealer-label" class="property-label"><g:message--}%
					%{--code="RPP.companyDealer.label" default="Company Dealer" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="companyDealer-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${RPPInstance}" field="companyDealer"--}%
								%{--url="${request.contextPath}/RPP/updatefield" type="text"--}%
								%{--title="Enter companyDealer" onsuccess="reloadRPPTable();" />--}%
							%{----}%
								%{--<g:link controller="companyDealer" action="show" id="${RPPInstance?.companyDealer?.id}">${RPPInstance?.companyDealer?.encodeAsHTML()}</g:link>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%

            <g:if test="${RPPInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="RPP.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${RPPInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/RPP/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadRPPTable();" />--}%

                        <g:fieldValue bean="${RPPInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
			

			
				<g:if test="${RPPInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="RPP.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${RPPInstance}" field="dateCreated"
								url="${request.contextPath}/RPP/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadRPPTable();" />--}%
							
								<g:formatDate date="${RPPInstance?.dateCreated}" type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${RPPInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="RPP.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${RPPInstance}" field="createdBy"
                                url="${request.contextPath}/RPP/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadRPPTable();" />--}%

                        <g:fieldValue bean="${RPPInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${RPPInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="RPP.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${RPPInstance}" field="lastUpdated"
                                url="${request.contextPath}/RPP/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadRPPTable();" />--}%

                        <g:formatDate date="${RPPInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${RPPInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="RPP.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${RPPInstance}" field="updatedBy"
                                url="${request.contextPath}/RPP/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadRPPTable();" />--}%

                        <g:fieldValue bean="${RPPInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${RPPInstance?.id}"
					update="[success:'RPP-form',failure:'RPP-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteRPP('${RPPInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
