
<%@ page import="com.kombos.parts.RPP" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>
<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</g:javascript>


<table id="RPP_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="RPP.m161TglBerlaku.label" default="Tanggal Berlaku" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="RPP.m161MasaPengajuanRPP.label" default="Masa Pengajuan RPP" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="RPP.m161MaxDapatRPP.label" default="Harga Max Syarat Mendapat RPP" /></div>
			</th>


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="RPP.companyDealer.label" default="Company Dealer" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="RPP.createdBy.label" default="Created By" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="RPP.updatedBy.label" default="Updated By" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="RPP.lastUpdProcess.label" default="Last Upd Process" /></div>--}%
			%{--</th>--}%

		
		</tr>
		<tr>
		

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m161TglBerlaku" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m161TglBerlaku" value="date.struct">
					<input type="hidden" name="search_m161TglBerlaku_day" id="search_m161TglBerlaku_day" value="">
					<input type="hidden" name="search_m161TglBerlaku_month" id="search_m161TglBerlaku_month" value="">
					<input type="hidden" name="search_m161TglBerlaku_year" id="search_m161TglBerlaku_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m161TglBerlaku_dp" value="" id="search_m161TglBerlaku" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m161MasaPengajuanRPP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input onkeypress="return isNumberKey(event)" type="text" name="search_m161MasaPengajuanRPP" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m161MaxDapatRPP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input onkeypress="return isNumberKey(event)" type="text" name="search_m161MaxDapatRPP" class="search_init" />
				</div>
			</th>
	
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_companyDealer" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_createdBy" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_updatedBy" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_lastUpdProcess" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%


		</tr>
	</thead>
</table>

<g:javascript>
var RPPTable;
var reloadRPPTable;
$(function(){
	
	reloadRPPTable = function() {
		RPPTable.fnDraw();
	}

	
	$('#search_m161TglBerlaku').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m161TglBerlaku_day').val(newDate.getDate());
			$('#search_m161TglBerlaku_month').val(newDate.getMonth()+1);
			$('#search_m161TglBerlaku_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			RPPTable.fnDraw();	
	});

	var recordsRPPPerPage = [];
    var anRPPSelected;
    var jmlRecRPPPerPage=0;
    var id;


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	RPPTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	RPPTable = $('#RPP_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsRPP = $("#RPP_datatables tbody .row-select");
            var jmlRPPCek = 0;
            var nRow;
            var idRec;
            rsRPP.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsRPPPerPage[idRec]=="1"){
                    jmlRPPCek = jmlRPPCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsRPPPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecRPPPerPage = rsRPP.length;
            if(jmlRPPCek==jmlRecRPPPerPage && jmlRecRPPPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m161TglBerlaku",
	"mDataProp": "m161TglBerlaku",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m161MasaPengajuanRPP",
	"mDataProp": "m161MasaPengajuanRPP",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m161MaxDapatRPP",
	"mDataProp": "m161MaxDapatRPP",
	"aTargets": [2],
	"mRender": function ( data, type, row ) {
				            			if(data != null)
				            				return '<span class="pull-right numeric">'+data+'</span>';
				            			else
				            				return '<span></span>';
								},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
//
//,
//
//{
//	"sName": "companyDealer",
//	"mDataProp": "companyDealer",
//	"aTargets": [3],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "createdBy",
//	"mDataProp": "createdBy",
//	"aTargets": [4],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "updatedBy",
//	"mDataProp": "updatedBy",
//	"aTargets": [5],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "lastUpdProcess",
//	"mDataProp": "lastUpdProcess",
//	"aTargets": [6],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var m161TglBerlaku = $('#search_m161TglBerlaku').val();
						var m161TglBerlakuDay = $('#search_m161TglBerlaku_day').val();
						var m161TglBerlakuMonth = $('#search_m161TglBerlaku_month').val();
						var m161TglBerlakuYear = $('#search_m161TglBerlaku_year').val();
						
						if(m161TglBerlaku){
							aoData.push(
									{"name": 'sCriteria_m161TglBerlaku', "value": "date.struct"},
									{"name": 'sCriteria_m161TglBerlaku_dp', "value": m161TglBerlaku},
									{"name": 'sCriteria_m161TglBerlaku_day', "value": m161TglBerlakuDay},
									{"name": 'sCriteria_m161TglBerlaku_month', "value": m161TglBerlakuMonth},
									{"name": 'sCriteria_m161TglBerlaku_year', "value": m161TglBerlakuYear}
							);
						}
	
						var m161MasaPengajuanRPP = $('#filter_m161MasaPengajuanRPP input').val();
						if(m161MasaPengajuanRPP){
							aoData.push(
									{"name": 'sCriteria_m161MasaPengajuanRPP', "value": m161MasaPengajuanRPP}
							);
						}
	
						var m161MaxDapatRPP = $('#filter_m161MaxDapatRPP input').val();
						if(m161MaxDapatRPP){
							aoData.push(
									{"name": 'sCriteria_m161MaxDapatRPP', "value": m161MaxDapatRPP}
							);
						}
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							        $('td span.numeric').text(function(index, text) {
												return $.number(text,0);
									});
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#RPP_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsRPPPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsRPPPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#RPP_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsRPPPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anRPPSelected = RPPTable.$('tr.row_selected');
            if(jmlRecRPPPerPage == anRPPSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsRPPPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
