<%@ page import="com.kombos.parts.RPP" %>
<r:require modules="autoNumeric" />
<script type="text/javascript">
    jQuery(function($) {
        $('.hari').autoNumeric('init',{
            vMin:'0',
            vMax:'9999',
            mDec:null,
            aSep:''
        });
    });
    jQuery(function($) {
        $('.harga').autoNumeric('init',{
            vMin:'0',
            vMax:'999999999',
            mDec:null,
            aSep:''
        });
    });
</script>


<div class="control-group fieldcontain ${hasErrors(bean: RPPInstance, field: 'm161TglBerlaku', 'error')} required">
	<label class="control-label" for="m161TglBerlaku">
		<g:message code="RPP.m161TglBerlaku.label" default="Tanggal Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<ba:datePicker name="m161TglBerlaku" required="true" precision="day"  value="${RPPInstance?.m161TglBerlaku}" format="dd/MM/yyyy" style="width:300px" />
	</div>
</div>

<g:javascript>
    document.getElementById("m161TglBerlaku").focus();
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: RPPInstance, field: 'm161MasaPengajuanRPP', 'error')} required">
	<label class="control-label" for="m161MasaPengajuanRPP">
		<g:message code="RPP.m161MasaPengajuanRPP.label" default="Masa Pengajuan RPP" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField class="hari" style="width:100px" name="m161MasaPengajuanRPP" type="number" value="${RPPInstance.m161MasaPengajuanRPP}" required=""/>&nbsp;Hari Dari Parts Datang
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: RPPInstance, field: 'm161MaxDapatRPP', 'error')} required">
	<label class="control-label" for="m161MaxDapatRPP">
		<g:message code="RPP.m161MaxDapatRPP.label" default="Harga Max Syarat Mendapat RPP" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField class="harga" style="width:100px" name="m161MaxDapatRPP" value="${fieldValue(bean: RPPInstance, field: 'm161MaxDapatRPP')}" required=""/>
	</div>
</div>

%{--<div class="control-group fieldcontain ${hasErrors(bean: RPPInstance, field: 'companyDealer', 'error')} required">--}%
	%{--<label class="control-label" for="companyDealer">--}%
		%{--<g:message code="RPP.companyDealer.label" default="Company Dealer" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:select id="companyDealer" name="companyDealer.id" from="${com.kombos.baseapp.maintable.CompanyDealer.list()}" optionKey="id" required="" value="${RPPInstance?.companyDealer?.id}" class="many-to-one"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: RPPInstance, field: 'createdBy', 'error')} ">--}%
	%{--<label class="control-label" for="createdBy">--}%
		%{--<g:message code="RPP.createdBy.label" default="Created By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="createdBy" value="${RPPInstance?.createdBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: RPPInstance, field: 'updatedBy', 'error')} ">--}%
	%{--<label class="control-label" for="updatedBy">--}%
		%{--<g:message code="RPP.updatedBy.label" default="Updated By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="updatedBy" value="${RPPInstance?.updatedBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: RPPInstance, field: 'lastUpdProcess', 'error')} ">--}%
	%{--<label class="control-label" for="lastUpdProcess">--}%
		%{--<g:message code="RPP.lastUpdProcess.label" default="Last Upd Process" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="lastUpdProcess" value="${RPPInstance?.lastUpdProcess}"/>--}%
	%{--</div>--}%
%{--</div>--}%

