

<%@ page import="com.kombos.hrd.HistoryWarningKaryawan" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'historyWarningKaryawan.label', default: 'HistoryWarningKaryawan')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteHistoryWarningKaryawan;

$(function(){ 
	deleteHistoryWarningKaryawan=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/historyWarningKaryawan/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadHistoryWarningKaryawanTable();
   				expandTableLayout('historyWarningKaryawan');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-historyWarningKaryawan" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="historyWarningKaryawan"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${historyWarningKaryawanInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="historyWarningKaryawan.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${historyWarningKaryawanInstance}" field="createdBy"
								url="${request.contextPath}/HistoryWarningKaryawan/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadHistoryWarningKaryawanTable();" />--}%
							
								<g:fieldValue bean="${historyWarningKaryawanInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyWarningKaryawanInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="historyWarningKaryawan.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${historyWarningKaryawanInstance}" field="dateCreated"
								url="${request.contextPath}/HistoryWarningKaryawan/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadHistoryWarningKaryawanTable();" />--}%
							
								<g:formatDate date="${historyWarningKaryawanInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyWarningKaryawanInstance?.karyawan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="karyawan-label" class="property-label"><g:message
					code="historyWarningKaryawan.karyawan.label" default="Karyawan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="karyawan-label">
						%{--<ba:editableValue
								bean="${historyWarningKaryawanInstance}" field="karyawan"
								url="${request.contextPath}/HistoryWarningKaryawan/updatefield" type="text"
								title="Enter karyawan" onsuccess="reloadHistoryWarningKaryawanTable();" />--}%
							
								<g:link controller="karyawan" action="show" id="${historyWarningKaryawanInstance?.karyawan?.id}">${historyWarningKaryawanInstance?.karyawan?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyWarningKaryawanInstance?.keterangan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="keterangan-label" class="property-label"><g:message
					code="historyWarningKaryawan.keterangan.label" default="Keterangan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="keterangan-label">
						%{--<ba:editableValue
								bean="${historyWarningKaryawanInstance}" field="keterangan"
								url="${request.contextPath}/HistoryWarningKaryawan/updatefield" type="text"
								title="Enter keterangan" onsuccess="reloadHistoryWarningKaryawanTable();" />--}%
							
								<g:fieldValue bean="${historyWarningKaryawanInstance}" field="keterangan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyWarningKaryawanInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="historyWarningKaryawan.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${historyWarningKaryawanInstance}" field="lastUpdProcess"
								url="${request.contextPath}/HistoryWarningKaryawan/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadHistoryWarningKaryawanTable();" />--}%
							
								<g:fieldValue bean="${historyWarningKaryawanInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyWarningKaryawanInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="historyWarningKaryawan.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${historyWarningKaryawanInstance}" field="lastUpdated"
								url="${request.contextPath}/HistoryWarningKaryawan/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadHistoryWarningKaryawanTable();" />--}%
							
								<g:formatDate date="${historyWarningKaryawanInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyWarningKaryawanInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="historyWarningKaryawan.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${historyWarningKaryawanInstance}" field="staDel"
								url="${request.contextPath}/HistoryWarningKaryawan/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadHistoryWarningKaryawanTable();" />--}%
							
								<g:fieldValue bean="${historyWarningKaryawanInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyWarningKaryawanInstance?.tanggalWarning}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tanggalWarning-label" class="property-label"><g:message
					code="historyWarningKaryawan.tanggalWarning.label" default="Tanggal Warning" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tanggalWarning-label">
						%{--<ba:editableValue
								bean="${historyWarningKaryawanInstance}" field="tanggalWarning"
								url="${request.contextPath}/HistoryWarningKaryawan/updatefield" type="text"
								title="Enter tanggalWarning" onsuccess="reloadHistoryWarningKaryawanTable();" />--}%
							
								<g:formatDate date="${historyWarningKaryawanInstance?.tanggalWarning}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyWarningKaryawanInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="historyWarningKaryawan.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${historyWarningKaryawanInstance}" field="updatedBy"
								url="${request.contextPath}/HistoryWarningKaryawan/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadHistoryWarningKaryawanTable();" />--}%
							
								<g:fieldValue bean="${historyWarningKaryawanInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyWarningKaryawanInstance?.warningKaryawan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="warningKaryawan-label" class="property-label"><g:message
					code="historyWarningKaryawan.warningKaryawan.label" default="Warning Karyawan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="warningKaryawan-label">
						%{--<ba:editableValue
								bean="${historyWarningKaryawanInstance}" field="warningKaryawan"
								url="${request.contextPath}/HistoryWarningKaryawan/updatefield" type="text"
								title="Enter warningKaryawan" onsuccess="reloadHistoryWarningKaryawanTable();" />--}%
							
								<g:link controller="warningKaryawan" action="show" id="${historyWarningKaryawanInstance?.warningKaryawan?.id}">${historyWarningKaryawanInstance?.warningKaryawan?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('historyWarningKaryawan');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${historyWarningKaryawanInstance?.id}"
					update="[success:'historyWarningKaryawan-form',failure:'historyWarningKaryawan-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteHistoryWarningKaryawan('${historyWarningKaryawanInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
