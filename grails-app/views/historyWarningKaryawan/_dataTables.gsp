
<%@ page import="com.kombos.administrasi.CompanyDealer; com.kombos.hrd.HistoryWarningKaryawan" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="historyWarningKaryawan_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="historyWarningKaryawan.warningKaryawan.label" default="Karyawan" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="historyWarningKaryawan.tanggalWarning.label" default="Tanggal Warning" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
				<div><g:message code="historyWarningKaryawan.companyDealer.label" default="Company Dealer" /></div>
			</th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="historyWarningKaryawan.workshop.label" default="Warning Karyawan" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="historyWarningKaryawan.keterangan.label" default="Keterangan" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="historyWarningKaryawan.keterangan.label" default="Approval" /></div>
            </th>

        </tr>
		<tr>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_warningKaryawan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_warningKaryawan" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_tanggalWarning" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
                    <input type="hidden" name="search_tanggalWarning" value="date.struct">
                    <input type="hidden" name="search_tanggalWarning_day" id="search_tanggalWarning_day" value="">
                    <input type="hidden" name="search_tanggalWarning_month" id="search_tanggalWarning_month" value="">
                    <input type="hidden" name="search_tanggalWarning_year" id="search_tanggalWarning_year" value="">
                    <input type="text" data-date-format="dd/mm/yyyy" name="search_tanggalWarning_dp" value="" id="search_tanggalWarning" class="search_init">
                </div>
            </th>


            <th style="border-top: none;padding: 5px;">
                <div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 185px;">
                    <g:if test="${params.companyDealer.toString().contains("HO")}">
                        <g:select name="search_companyDealer" id="workshop" from="${CompanyDealer.createCriteria().list{eq('staDel','0');order('m011NamaWorkshop')}}" optionKey="id" optionValue="m011NamaWorkshop" style="width: 100%" noSelection="['':'SEMUA']" onchange="reloadHistoryWarningKaryawanTable()" />
                    </g:if>
                </div>
			</th>
            <th style="border-top: none;padding: 5px;">
                <div id="filter_karyawan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_karyawan" class="search_init" />
                </div>
            </th>



            <th style="border-top: none;padding: 5px;">
                <div id="filter_keterangan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_keterangan" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_approval" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_approval" class="search_init" />
                </div>
            </th>

		</tr>
	</thead>
</table>

<g:javascript>
var historyWarningKaryawanTable;
var reloadHistoryWarningKaryawanTable;
$(function(){
	
	reloadHistoryWarningKaryawanTable = function() {
		historyWarningKaryawanTable.fnDraw();
	}

	
	$('#search_tanggalWarning').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tanggalWarning_day').val(newDate.getDate());
			$('#search_tanggalWarning_month').val(newDate.getMonth()+1);
			$('#search_tanggalWarning_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			historyWarningKaryawanTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	historyWarningKaryawanTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	historyWarningKaryawanTable = $('#historyWarningKaryawan_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "karyawan",
	"mDataProp": "karyawan",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "tanggalWarning",
	"mDataProp": "tanggalWarning",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,
{
	"sName": "workshop",
	"mDataProp": "workshop",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "warningKaryawan",
	"mDataProp": "warningKaryawan",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "keterangan",
	"mDataProp": "keterangan",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},

{
	"sName": "staApproval",
	"mDataProp": "approval",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var karyawan = $('#filter_karyawan input').val();
						if(karyawan){
							aoData.push(
									{"name": 'sCriteria_karyawan', "value": karyawan}
							);
						}

                        var companyDealer = $("#workshop").val();
                        if (companyDealer) {
                            aoData.push({"name": "sCriteria_companyDealer", "value": companyDealer});
                        }

						var keterangan = $('#filter_keterangan input').val();
						if(keterangan){
							aoData.push(
									{"name": 'sCriteria_keterangan', "value": keterangan}
							);
						}



						var tanggalWarning = $('#search_tanggalWarning').val();
						var tanggalWarningDay = $('#search_tanggalWarning_day').val();
						var tanggalWarningMonth = $('#search_tanggalWarning_month').val();
						var tanggalWarningYear = $('#search_tanggalWarning_year').val();
						
						if(tanggalWarning){
							aoData.push(
									{"name": 'sCriteria_tanggalWarning', "value": "date.struct"},
									{"name": 'sCriteria_tanggalWarning_dp', "value": tanggalWarning},
									{"name": 'sCriteria_tanggalWarning_day', "value": tanggalWarningDay},
									{"name": 'sCriteria_tanggalWarning_month', "value": tanggalWarningMonth},
									{"name": 'sCriteria_tanggalWarning_year', "value": tanggalWarningYear}
							);
						}
	
						var warningKaryawan = $('#filter_warningKaryawan input').val();
						if(warningKaryawan){
							aoData.push(
									{"name": 'sCriteria_warningKaryawan', "value": warningKaryawan}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
