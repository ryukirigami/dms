
<%@ page import="com.kombos.hrd.HistoryWarningKaryawan" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'historyWarningKaryawan.label', default: 'HistoryWarningKaryawan')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
        <style>
        .modal.fade.in {
            left: 25.5% !important;
            width: 1205px !important;
        }
        .modal.fade {
            top: -100%;
        }
        </style>
		<g:javascript>
			var show;
			var edit;
			var oFormService;
			$(function(){ 

			    oFormService = {
			         fnShowKaryawanDataTable: function() {
                        $("#karyawanModal").modal("show");
                        $("#karyawanModal").fadeIn();
			        },
			        fnHideKaryawanDataTable: function() {
			            $("#karyawanModal").modal("hide");
			        }
			    }

			    oFormService.fnHideKaryawanDataTable();

				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('historyWarningKaryawan');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('historyWarningKaryawan', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('historyWarningKaryawan', '${request.contextPath}/historyWarningKaryawan/massdelete', reloadHistoryWarningKaryawanTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('historyWarningKaryawan','${request.contextPath}/historyWarningKaryawan/show/'+id);
				};
				
				edit = function(id) {
					editInstance('historyWarningKaryawan','${request.contextPath}/historyWarningKaryawan/edit/'+id);
				};

});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="historyWarningKaryawan-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
		</div>
		<div class="span7" id="historyWarningKaryawan-form" style="display: none;"></div>
	</div>


</body>
</html>
