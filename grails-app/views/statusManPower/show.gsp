

<%@ page import="com.kombos.administrasi.StatusManPower" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'statusManPower.label', default: 'Man Power Status')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteStatusManPower;

$(function(){ 
	deleteStatusManPower=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/statusManPower/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadStatusManPowerTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-statusManPower" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="statusManPower"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${statusManPowerInstance?.namaManPower}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="namaManPower-label" class="property-label"><g:message
					code="statusManPower.namaManPower.label" default="Nama Teknisi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="namaManPower-label">
						%{--<ba:editableValue
								bean="${statusManPowerInstance}" field="namaManPower"
								url="${request.contextPath}/StatusManPower/updatefield" type="text"
								title="Enter namaManPower" onsuccess="reloadStatusManPowerTable();" />--}%
							
                        <g:fieldValue bean="${statusManPowerInstance?.namaManPower}" field="t015NamaLengkap"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${statusManPowerInstance?.t023Tmt}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t023Tmt-label" class="property-label"><g:message
					code="statusManPower.t023Tmt.label" default="Tgl Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t023Tmt-label">
						%{--<ba:editableValue
								bean="${statusManPowerInstance}" field="t023Tmt"
								url="${request.contextPath}/StatusManPower/updatefield" type="text"
								title="Enter t023Tmt" onsuccess="reloadStatusManPowerTable();" />--}%
							
								<g:formatDate date="${statusManPowerInstance?.t023Tmt}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${statusManPowerInstance?.t023Sta}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t023Sta-label" class="property-label"><g:message
					code="statusManPower.t023Sta.label" default="Status" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t023Sta-label">
						%{--<ba:editableValue
								bean="${statusManPowerInstance}" field="t023Sta"
								url="${request.contextPath}/StatusManPower/updatefield" type="text"
								title="Enter t023Sta" onsuccess="reloadStatusManPowerTable();" />--}%
							
								<g:if test="${statusManPowerInstance.t023Sta == '0'}">
			                        <g:message code="statusManPowerInstance.t023Sta.label" default="Booking"/>
			                    </g:if>
			                    <g:else>
			                        <g:message code="statusManPowerInstance.t023Sta.label" default="Booking Web"/>
			                    </g:else>		
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${statusManPowerInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="perhitunganNerxtService.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${statusManPowerInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/perhitunganNerxtService/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadperhitunganNerxtServiceTable();" />--}%

                        <g:fieldValue bean="${statusManPowerInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${statusManPowerInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="jenisStall.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${statusManPowerInstance}" field="dateCreated"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadJenisStallTable();" />--}%

                        <g:formatDate date="${statusManPowerInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>


            <g:if test="${statusManPowerInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="perhitunganNerxtService.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${statusManPowerInstance}" field="createdBy"
                                url="${request.contextPath}/perhitunganNerxtService/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadperhitunganNerxtServiceTable();" />--}%

                        <g:fieldValue bean="${statusManPowerInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

        <g:if test="${statusManPowerInstance?.lastUpdated}">
            <tr>
                <td class="span2" style="text-align: right;"><span
                        id="lastUpdated-label" class="property-label"><g:message
                            code="perhitunganNerxtService.lastUpdated.label" default="Last Updated" />:</span></td>

                <td class="span3"><span class="property-value"
                                        aria-labelledby="lastUpdated-label">
                    %{--<ba:editableValue
                            bean="${statusManPowerInstance}" field="lastUpdated"
                            url="${request.contextPath}/perhitunganNerxtService/updatefield" type="text"
                            title="Enter lastUpdated" onsuccess="reloadperhitunganNerxtServiceTable();" />--}%

                    <g:formatDate date="${statusManPowerInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />

                </span></td>

            </tr>
        </g:if>

        <g:if test="${statusManPowerInstance?.updatedBy}">
            <tr>
                <td class="span2" style="text-align: right;"><span
                        id="updatedBy-label" class="property-label"><g:message
                            code="perhitunganNerxtService.updatedBy.label" default="Updated By" />:</span></td>

                <td class="span3"><span class="property-value"
                                        aria-labelledby="updatedBy-label">
                    %{--<ba:editableValue
                            bean="${statusManPowerInstance}" field="updatedBy"
                            url="${request.contextPath}/perhitunganNerxtService/updatefield" type="text"
                            title="Enter updatedBy" onsuccess="reloadperhitunganNerxtServiceTable();" />--}%

                    <g:fieldValue bean="${statusManPowerInstance}" field="updatedBy"/>

                </span></td>

            </tr>
        </g:if>
            
            
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${statusManPowerInstance?.id}"
					update="[success:'statusManPower-form',failure:'statusManPower-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteStatusManPower('${statusManPowerInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
