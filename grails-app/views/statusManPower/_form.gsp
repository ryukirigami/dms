<%@ page import="com.kombos.administrasi.NamaManPower; com.kombos.administrasi.StatusManPower" %>



<div class="control-group fieldcontain ${hasErrors(bean: statusManPowerInstance, field: 'namaManPower', 'error')} required">
	<label class="control-label" for="namaManPower">
		<g:message code="statusManPower.namaManPower.label" default="Nama Teknisi" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="namaManPower" name="namaManPower.id" from="${NamaManPower.createCriteria().list{eq("staDel", "0");order("t015NamaLengkap", "asc")}}" optionValue="t015NamaLengkap" optionKey="id" required="required" value="${statusManPowerInstance?.namaManPower?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: statusManPowerInstance, field: 't023Tmt', 'error')} required">
	<label class="control-label" for="t023Tmt">
		<g:message code="statusManPower.t023Tmt.label" default="Tgl Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<ba:datePicker  name="t023Tmt" precision="day"  value="${statusManPowerInstance?.t023Tmt}" format="dd/mm/yyyy" required="true" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: statusManPowerInstance, field: 't023Sta', 'error')} required">
	<label class="control-label" for="t023Sta">
		<g:message code="statusManPower.t023Sta.label" default="Status" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">	
	  <g:radioGroup name="t023Sta" values="['0','1']" labels="['Booking','Booking Web']" value="${statusManPowerInstance?.t023Sta}" required="true" >
            ${it.radio} ${it.label}
        </g:radioGroup>
	</div>
</div>

