
<%@ page import="com.kombos.administrasi.StatusManPower" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="statusManPower_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="statusManPower.namaManPower.label" default="Nama Teknisi" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="statusManPower.t023Tmt.label" default="Tgl Berlaku" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="statusManPower.t023Sta.label" default="Status" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_namaManPower" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_namaManPower" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t023Tmt" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_t023Tmt" value="date.struct">
					<input type="hidden" name="search_t023Tmt_day" id="search_t023Tmt_day" value="">
					<input type="hidden" name="search_t023Tmt_month" id="search_t023Tmt_month" value="">
					<input type="hidden" name="search_t023Tmt_year" id="search_t023Tmt_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t023Tmt_dp" value="" id="search_t023Tmt" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t023Sta" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
				
							<select name="search_t023Sta" id="search_t023Sta" style="width:100%">
                                    <option value=""></option>
                                    <option value="0">Booking</option>
                                    <option value="1">Booking Web</option>

                             </select>
				</div>
			</th>
			 

		</tr>
	</thead>
</table>

<g:javascript>
var statusManPowerTable;
var reloadStatusManPowerTable;
$(function(){
	
	reloadStatusManPowerTable = function() {
		statusManPowerTable.fnDraw();
	}

	var recordsStatusManPowerPerPage = [];
    var anStatusManPowerSelected;
    var jmlRecStatusManPowerPerPage=0;
    var id;
	
	$('#search_t023Tmt').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t023Tmt_day').val(newDate.getDate());
			$('#search_t023Tmt_month').val(newDate.getMonth()+1);
			$('#search_t023Tmt_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			statusManPowerTable.fnDraw();	
	});

	
    $('#search_t023Sta').change(function(){
        statusManPowerTable.fnDraw();
    });

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	statusManPowerTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	statusManPowerTable = $('#statusManPower_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsStatusManPower = $("#statusManPower_datatables tbody .row-select");
            var jmlStatusManPowerCek = 0;
            var nRow;
            var idRec;
            rsStatusManPower.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsStatusManPowerPerPage[idRec]=="1"){
                    jmlStatusManPowerCek = jmlStatusManPowerCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsStatusManPowerPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecStatusManPowerPerPage = rsStatusManPower.length;
            if(jmlStatusManPowerCek==jmlRecStatusManPowerPerPage && jmlRecStatusManPowerPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "namaManPower",
	"mDataProp": "namaManPower",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "t023Tmt",
	"mDataProp": "t023Tmt",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t023Sta",
	"mDataProp": "t023Sta",
	"aTargets": [2],
	"mRender": function ( data, type, row ) {
          
            if(data=="0"){
                  return "Booking";
             }else
             {
                  return "Booking Web";
             }
        
        },
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var namaManPower = $('#filter_namaManPower input').val();
						if(namaManPower){
							aoData.push(
									{"name": 'sCriteria_namaManPower', "value": namaManPower}
							);
						}

						var t023Tmt = $('#search_t023Tmt').val();
						var t023TmtDay = $('#search_t023Tmt_day').val();
						var t023TmtMonth = $('#search_t023Tmt_month').val();
						var t023TmtYear = $('#search_t023Tmt_year').val();
						
						if(t023Tmt){
							aoData.push(
									{"name": 'sCriteria_t023Tmt', "value": "date.struct"},
									{"name": 'sCriteria_t023Tmt_dp', "value": t023Tmt},
									{"name": 'sCriteria_t023Tmt_day', "value": t023TmtDay},
									{"name": 'sCriteria_t023Tmt_month', "value": t023TmtMonth},
									{"name": 'sCriteria_t023Tmt_year', "value": t023TmtYear}
							);
						}
	
						var t023Sta = $('#filter_t023Sta select').val();
						if(t023Sta){
							aoData.push(
									{"name": 'sCriteria_t023Sta', "value": t023Sta}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#statusManPower_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsStatusManPowerPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsStatusManPowerPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#statusManPower_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsStatusManPowerPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anStatusManPowerSelected = statusManPowerTable.$('tr.row_selected');

            if(jmlRecStatusManPowerPerPage == anStatusManPowerSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsStatusManPowerPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
