
<%@ page import="com.kombos.parts.BlockStok" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'blockStok.label', default: 'Block Stock')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist, autoNumeric" />
		<g:javascript>
			var show;
			var edit;
			var addPickingSlip;
            var addEditPickingSlip;
			var printBlockStokSlip;
			var printBlockStokSlipCont;
			var showBlockStokDetail;
			var saveBlockStok;
			var loadBlockStokEditModal;
			var massDeleteBlockStok;
			var konfirmasiSaveUpdate;
			var showButton;
			var hideButton;
			var loadAutoScript;

			$(function(){

            loadAutoScript = function(){
                $('.auto').autoNumeric('init',{vMin:'0',
                    vMax:'9999999999999',
                    mDec: null,
                    aSep:''

                });

            }


			showButton = function(){
			    $("#btnCreate").show();
			    $("#btnDelete").show();
			}

			hideButton = function(){
			    $("#btnCreate").hide();
			    $("#btnDelete").hide();
			}


            konfirmasiSaveUpdate = function(){
                 return confirm('${message(code: 'default.button.save.confirm.message', default: 'Anda yakin data akan disimpan?')}');
            }


             showBlockStokDetail = function(){
                        $(".span7").css("display","none");
                        $(".span12").css("display","block");
                    }

				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout();
					//	showBlockStokDetail();
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('blockStok', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_MINIMIZE_' :

                            <g:remoteFunction action="list"
                              onLoading="jQuery('#spinner').fadeIn(1);"
                              onSuccess="bukaTableLayout()"
                              onComplete="jQuery('#spinner').fadeOut();" />
							break;
					    case '_REMOVE_' :
                         bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
                            function(result){
                                if(result){
                                    massDeleteBlockStok();
                                }
         			});

         		break;
				   }    
				   return false;
				});

				massDeleteBlockStok = function() {
                    var recordsToDelete = [];

                    $("#blockStok-table tbody .row-select").each(function() {
                        if(this.checked){
                            var id = $(this).next("input:hidden").val();
                   		    recordsToDelete.push(id);
                        }
                    });

                    var json = JSON.stringify(recordsToDelete);

                    $.ajax({
                        url:'${request.contextPath}/blockStok/massdelete',
                        type: "POST", // Always use POST when deleting data
                        data: { ids: json },
                        complete: function(xhr, status) {
                            blockStokTable.fnDraw();
                             toastr.success('<div>Block Stock Telah dihapus.</div>');

                        }
                    });

                }


				show = function(id) {
					showInstance('blockStok','${request.contextPath}/blockStok/show/'+id);
				};
				
				edit = function(id) {
					editInstance('blockStok','${request.contextPath}/blockStok/edit/'+id);
				};

				shrinkTableLayout = function(){
                    $("#blockStok-table").hide();
                    hideButton();
                    $("#blockStok-form").show();
                }



                bukaTableLayout = function(){
                    $("#blockStok-table").show();
                    showButton();
                     $("#blockStok-form").hide();
                    blockStokTable.fnDraw();
                }



                  addPickingSlip = function(){
                       checkGoods =[];
                        $("#pickingSlip_datatables .row-select").each(function() {
                            if(this.checked){
                              var nRow = $(this).parents('tr')[0];
                                checkGoods.push(nRow);

                            }
                        });
                        if(checkGoods.length<1){
                            alert('Anda belum memilih data yang akan ditambahkan');
                            return;
                         //   loadPilihJobModal();
                        }else{
                          //  alert("lenght : "+checkGoods.length);
                            for (var i=0;i<checkGoods.length;i++){
                         //   alert("var i "+i);
                                var aData = pickingSlipTable.fnGetData(checkGoods[i]);
                                blockStokDetailTable.fnAddData({
                                    'id' : aData['id'],
                                    'kodeParts': aData['m111ID_modal'],
                                    'namaParts': aData['m111Nama_modal'],
                                    'qtyPicking': aData['qtyPicking'],
                                    'satuan1': aData['satuan'],
                                    'qtyBlockStock': '0',
                                    'satuan2':  aData['satuan'],
                                    'location':'0'});
                                    $('#qtyBlockStok'+aData['id']).autoNumeric('init',{
                                                            vMin:'0',
                                                            vMax:'9999999999999',
                                                            mDec: null,
                                                            aSep:''
                                                        });

                            }
                        }

                    checkGoods = [];

                }

                 addEditPickingSlip = function(){
                       checkGoods =[];
                        $("#pickingSlipEdit_datatables .row-select").each(function() {
                            if(this.checked){
                              var nRow = $(this).parents('tr')[0];
                                checkGoods.push(nRow);

                            }
                        });
                        if(checkGoods.length<1){
                            alert('Anda belum memilih data yang akan ditambahkan');
                            return;
                         //   loadPilihJobModal();
                        }else{
                          //  alert("lenght : "+checkGoods.length);
                            for (var i=0;i<checkGoods.length;i++){
                               var aData = pickingSlipTable.fnGetData(checkGoods[i]);

                                blockStokDetailTableEdit.fnAddData({
                                    'id' : aData['id'],
                                    'kodeParts': aData['m111ID_modal'],
                                    'namaParts': aData['m111Nama_modal'],
                                    'qtyPicking': aData['qtyPicking'],
                                    'satuan1': aData['satuan'],
                                    'qtyBlockStock': '0',
                                    'satuan2':  aData['satuan'],
                                    'location':'0'});
                                    $('#qtyBlockStok'+aData['id']).autoNumeric('init',{
                                                            vMin:'0',
                                                            vMax:'999999999999',
                                                            mDec: null,
                                                            aSep:''
                                                        });
                                blockStokDetailTableEdit.fnDraw();

                            }
                        }

                     loadAutoScript();

                    checkGoods = [];

                }

                saveBlockStok = function(id){

                    if(!konfirmasiSaveUpdate){
                        return;
                    }


                    var isiTanggal = $("#tanggal").val();

                    if(isiTanggal == ""){
                        toastr.error("Harap isi tanggal block stok");
                        $("#tanggal").focus();
                        return;
                    }

                    var formBlockStok = $("#form-blockStokDetail");

                    var checkGoods =[];

                    $("#blockStokDetail_datatables tbody .row-select").each(function() {
                    if(this.checked){
                        var id = $(this).next("input:hidden").val();
                        var nRow = $(this).parents('tr')[0];
                        var aData = blockStokDetailTable.fnGetData(nRow);
                        var qtyInput = $('#qtyBlockStok'+id).val();

                        if(qtyInput == 0 || qtyInput == ""){
                            toastr.error("QTY tidak boleh 0 atau kosong ");
                            $("#qtyBlockStok"+id).focus();
                            return;
                        }

                        var location = $('#location'+id).val();
                        var tanggalBS = $('#tanggal').val();
                        var receptionBS = $('#reception').val();

                        checkGoods.push(id);

                        formBlockStok.append('<input type="hidden" id="tanggal"  value="'+tanggalBS+'" class="deleteafter">')
                        formBlockStok.append('<input type="hidden" id="reception"  value="'+receptionBS+'" class="deleteafter">')
                        formBlockStok.append('<input type="hidden" id="qtyBlockStok'+id+'" name="qtyBlockStok'+id + '" value="'+qtyInput+'" class="deleteafter">');
                        formBlockStok.append('<input type="hidden" name="location'+id + '" id="location'+id +'" value="'+location+'" class="deleteafter">')
                    }
                    });

                     if(checkGoods.length<1){
                        alert('Anda belum memilih data yang akan ditambahkan');
                         return;
                    }else{
                        $("#ids").val(JSON.stringify(checkGoods));


                        $.ajax({
                            url:'${request.contextPath}/blockStok/save',
                            type: "POST", // Always use POST when deleting data
                            data : formBlockStok.serialize(),
                            async : false,
                            success : function(data){
                                toastr.success('<div>Block Stock Telah dibuat.</div>');
                                formBlockStok.find('.deleteafter').remove();
                                $("#ids").val('');
                                bukaTableLayout();
                            },
                        error: function(xhr, textStatus, errorThrown) {
                        alert('Internal Server Error');
                        }
                        });

                        checkGoods = []
                    }


                }

                updateBlockStok = function(){

                    if(!konfirmasiSaveUpdate){
                        return;
                    }

                    var formBlockStok = $("#form-blockStokDetail");

                    var checkGoods =[];

                    $("#blockStokDetailEdit_datatables tbody .row-select").each(function() {
                    if(this.checked){
                        var id = $(this).next("input:hidden").val();
                        var nRow = $(this).parents('tr')[0];
                        var aData = blockStokDetailTableEdit.fnGetData(nRow);
                        var qtyInput = $('#qtyBlockStok'+id).val();
                        var location = $('#location'+id).val();
                        var tanggalBS = $('#tanggal').val();
                        var receptionBS = $('#reception').val();


                        if(qtyInput == 0 || qtyInput == ""){
                            toastr.error("QTY tidak boleh 0 atau kosong ");
                            $("#qtyBlockStok"+id).focus();
                            return;
                        }

                        checkGoods.push(id);

                        formBlockStok.append('<input type="hidden" id="tanggal"  value="'+tanggalBS+'" class="deleteafter">')
                        formBlockStok.append('<input type="hidden" id="reception"  value="'+receptionBS+'" class="deleteafter">')
                        formBlockStok.append('<input type="hidden" id="qtyBlockStok'+id+'" name="qtyBlockStok'+id + '" value="'+qtyInput+'" class="deleteafter">');
                        formBlockStok.append('<input type="hidden" name="location'+id + '" id="location'+id +'" value="'+location+'" class="deleteafter">')
                    }
                    });

                     if(checkGoods.length<1){
                        alert('Anda belum memilih data valid yang akan ditambahkan');
                         return;
                    }else{
                        $("#ids").val(JSON.stringify(checkGoods));

                        $.ajax({
                            url:'${request.contextPath}/blockStok/update',
                            type: "POST", // Always use POST when deleting data
                            data : formBlockStok.serialize(),
                            async : false,
                            success : function(data){
                                toastr.success('<div>Block Stock Telah diupdate</div>');
                                formBlockStok.find('.deleteafter').remove();
                                $("#ids").val('');
                                bukaTableLayout();
                            },
                        error: function(xhr, textStatus, errorThrown) {
                        alert('Internal Server Error');
                        }
                        });

                        checkGoods = []
                    }


                }

                printBlockStokSlip = function(){
                       checkBlockStok =[];
                       var idBlockStok;
                        $("#blockStok-table tbody .row-select").each(function() {
                            if(this.checked){
                                idBlockStok = $(this).next("input:hidden").val();
                               checkBlockStok.push(idBlockStok);
                            }
                        });
                        if(checkBlockStok.length<1){
                            alert('Anda belum memilih data valid yang akan diprint');
                            return;

                        }

                        var idBlockStoks = JSON.stringify(checkBlockStok);

                        window.location = "${request.contextPath}/blockStok/printBlockStokSlip?idBlockStoks="+idBlockStoks;

                }



});
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
        <ul class="nav pull-right">
            <li><a class="pull-right box-action" href="#"
                   style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
                        class="icon-plus" id="btnCreate"></i>&nbsp;&nbsp;
            </a></li>
            <li><a class="pull-right box-action" href="#"
                   style="display: block;" target="_REMOVE_">&nbsp;&nbsp;<i
                        class="icon-remove" id="btnDelete"></i>&nbsp;&nbsp;
            </a></li>
            <li class="separator"></li>
        </ul>
	</div>
	<div class="box">
		<div class="span12" id="blockStok-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>


            <table>
                <tr>
                    <td style="padding: 5px">
                        <g:message code="blockStok.t157TglBlockStok.label" default="Tanggal Block Stock"/>
                    </td>
                    <td style="padding: 5px">
                        <ba:datePicker style="width:70px" name="search_t157TglBlockStok" precision="day" value="" format="dd-MM-yyyy"/>&nbsp;s.d.&nbsp;
                        <ba:datePicker style="width:70px" name="search_t157TglBlockStokAkhir" precision="day" value="" format="dd-MM-yyyy"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <a class="pull-right box-action" style="display: block;" >
                            &nbsp;&nbsp;
                            <i>
                                <g:field type="button" style="padding: 5px" class="btn btn-primary pull-right" name="clear" id="clear" value="${message(code: 'blockStok.clearsearch.label', default: 'Clear Search')}" />
                            </i>
                            &nbsp;&nbsp;
                        </a>
                        <a class="pull-right box-action" style="display: block;" >
                            &nbsp;&nbsp;
                            <i>
                                <g:field type="button" style="padding: 5px" class="btn btn-primary pull-right" name="view" id="view" value="${message(code: 'blockStok.search.label', default: 'Search')}" />
                            </i>
                            &nbsp;&nbsp;
                        </a>
                    </td>
                </tr>
            </table>&nbsp;&nbsp;
			 <g:render template="dataTables" />
            <button id='printBlockStok' onclick="printBlockStokSlip();" type="button" class="btn btn-primary">Print Block Stock Slip</button>

    </div>
        <form id="form-blockStokDetail" class="form-horizontal">
            <div class="span12" id="blockStok-form" hidden=""></div>
        </form>


	</div>
    <div id="blockStokInputModal" class="modal fade">
        <div class="modal-dialog" style="width: 600px;">
            <div class="modal-content" style="width: 600px; height: 2000">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 1200px;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="iu-content"></div>
                    <div id="blockStokInputContent"/>
                </div>
                <!-- dialog buttons -->
                <div class="modal-footer">
                <button id='addGoods' onclick="addPickingSlip();" type="button" class="btn btn-primary">Add Selected</button></div>
            </div>
        </div>
    </div>
    <div id="blockStokEditModal" class="modal fade">
        <div class="modal-dialog" style="width: 1200px;">
            <div class="modal-content" style="width: 1200px; height: 2000">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 1200px;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="iu-content"></div>
                    <div id="blockStokEditContent"/>
                </div>
                <!-- dialog buttons -->
                <div class="modal-footer">
                    <button id='addGoods' onclick="addEditPickingSlip();" type="button" class="btn btn-primary">Add Selected</button></div>
            </div>
        </div>
    </div>

    </body>
</html>
