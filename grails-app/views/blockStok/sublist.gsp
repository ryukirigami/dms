
<%@ page import="com.kombos.parts.BlockStok" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
        <r:require modules="baseapplayout" />
		<g:javascript>
			var blockStokSubTable
			$(function(){
			if(blockStokSubTable)
			{
			    blockStokSubTable.fnDestroy();
			}

			blockStokSubTable = $('#blockStok_datatables_sub_${idTable}').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubList")}",
		"aoColumns": [

{
   "mDataProp": null,
   "sClass": "subcontrol center",
   "bSortable": false,
   "sWidth":"34px",
   "sDefaultContent": ''
}

,

{
	"sName": "goods",
	"mDataProp": "kodeparts",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="hidden" value="'+row['id']+'">'+data+'';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"191px",
	"bVisible": true
}

,

{
	"sName": "goods",
	"mDataProp": "namaparts",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"191px",
	"bVisible": true
}

,

{
	"sName": "pickingSlipDetail",
	"mDataProp": "qtyPicking",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	
	"bVisible": true
}

,

{
	"sName": "satuan",
	"mDataProp": "satuan1",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	
	"bVisible": true
}

,

{
	"sName": "blokStokDetail",
	"mDataProp": "qtyblockstok",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	
	"bVisible": true
}

,

{
	"sName": "satuan",
	"mDataProp": "satuan2",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	
	"bVisible": true
}

,

{
	"sName": "location",
	"mDataProp": "lokasi",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						aoData.push(
									{"name": 'idBlockStok', "value": "${idBlockStok}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
        </g:javascript>

    </head>
<body>
<div class="innerDetails">
    <table id="blockStok_datatables_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
           class="display table table-striped table-bordered table-hover" style="table-layout:fixed">
        <thead>
        <tr>
            <th></th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="blockStok.kode.part.label" default="Kode Part" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="blockStok.nama.part.label" default="Nama Part" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="blockStok.qty.picking.label" default="Qty Picking" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="blockStok.satuan1.label" default="Satuan" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="blockStok.qty.label" default="Qty Block Stock" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="blockStok.satuan2.label" default="Satuan" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="blockStok.lokasi.label" default="Lokasi" /></div>
            </th>

        </tr>
        </thead>
    </table>

</div>
</body>
</html>