
<%@ page import="com.kombos.parts.BlokStokDetail; com.kombos.parts.Location; com.kombos.parts.BlockStok" %>

<r:require modules="baseapplayout" />

<r:require modules="autoNumeric" />
<g:render template="../menu/maxLineDisplay"/>
<br/><br/><br/>
<table id="blockStokDetailEdit_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="100%">
    <thead>
    <tr>
        <th></th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:checkBox name="" class="select-all"/> &nbsp;<g:message code="blockStok.kode.part.label" default="Kode Part" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="blockStok.nama.part.label" default="Nama Part" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="blockStok.qty.picking.label" default="Qty Picking" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="blockStok.satuan1.label" default="Satuan" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="blockStok.qty.label" default="Qty Block Stock" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="blockStok.satuan2.label" default="Satuan" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="blockStok.lokasi.label" default="Lokasi" /></div>
        </th>



    </tr>
    </thead>
</table>

<g:javascript>
var blockStokDetailTableEdit;
var reloadBlockStokFormTable;
var selectLocationEditable;
var qtyBlockStokCek;
$(function(){


    var recordsblockStokDetailTableEditperpage = [];//new Array();
    var anCountrySelected;
    var jmlRecCountryPerPage=0;
    var id;

	reloadBlockStokFormTable = function() {
		blockStokDetailTableEdit.fnDraw();
	}


qtyBlockStokCek = function(rowId){
    var value = jQuery('#qtyBlockStok'+rowId).val();

    if(value==""){
        alert("Mohon QTY Block Stok diisi");
        return false;
    }
    else
        return true;
}

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	blockStokDetailTableEdit.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	blockStokDetailTableEdit = $('#blockStokDetailEdit_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsCountry = $("#blockStokDetailTableEdit_datatables tbody .row-select");
            var jmlCountryCek = 0;
            var nRow;
            var idRec;
            rsCountry.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsblockStokDetailTableEditperpage[idRec]=="1"){
                    jmlCountryCek = jmlCountryCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsblockStokDetailTableEditperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecCountryPerPage = rsCountry.length;
            if(jmlCountryCek==jmlRecCountryPerPage && jmlRecCountryPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);

		   loadAutoScript();
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": false,
	    "bServerSide": false,
///		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "dTBlockStokDetailEdit")}",
		"aoColumns": [

{
	"sName": "id",
	"mDataProp": "id",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}
,

{
	"sName": "kodeParts",
	"mDataProp": "kodeParts",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<label>&nbsp;&nbsp;'+data+'</label><input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this" checked="true"><input type="hidden" value="'+row['id']+'" id="idBlockStokDetail'+row['id']+'">&nbsp;&nbsp;&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "namaParts",
	"mDataProp": "namaParts",
	"aTargets": [1],
	"mRender": function ( data, type, row ) {
                        return '<label>'+data+'</label><input type="hidden" value="'+data+'" id="kodeParts'+row['id']+'"><input id="namaParts'+row['id']+'" type="hidden" value="'+data+'"/>';
    },
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "qtyPicking",
	"mDataProp": "qtyPicking",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "satuan1",
	"mDataProp": "satuan1",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "qtyBlockStock",
	"mDataProp": "qtyBlockStock",
	"aTargets": [4],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
                        return '<input type="text" id="qtyBlockStok'+row['id']+'" class="inline-edit auto" value="'+data+'"/>';
    },
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "satuan2",
	"mDataProp": "satuan2",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "location",
	"mDataProp": "location",
	"mRender": function ( data, type, row ) {
        return '<select id="location'+row['id']+'" />';
    },
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
        selectLocationEditable(oData['id'],oData['location']);
      },
	"sWidth":"200px",
	"bVisible": true
}




],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                        aoData.push(
									{"name": 'idBlockStok', "value": "${blockStokInstance?.id}"}
						);


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
	
	
	$('.select-all').click(function(e) {

        $("#blockStokDetailTableEdit_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordsblockStokDetailTableEditperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsblockStokDetailTableEditperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#blockStokDetailTableEdit_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsblockStokDetailTableEditperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anCountrySelected = blockStokDetailTableEdit.$('tr.row_selected');
            if(jmlRecCountryPerPage == anCountrySelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsblockStokDetailTableEditperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });



});

selectLocationEditable = function (id,location) {
    jQuery.getJSON('${request.contextPath}/blockStok/getLocationSelected?location='+location, function (data) {
        if (data) {
            jQuery.each(data, function (index, value) {
                $('#location'+id).append("<option value='" + value.id + "' selected>" + value.namaLokasi + "</option>");
            });


        }
    });


    jQuery.getJSON('${request.contextPath}/blockStok/getLocationsOthers?location='+location, function (data) {
        if (data) {
            jQuery.each(data, function (index, value) {
                $('#location'+id).append("<option value='" + value.id + "'>" + value.namaLokasi + "</option>");
            });


        }
    });




 }


$(function(){


        $('.auto').autoNumeric('init',{vMin:'0',
            vMax:'999999999999999999',
            mDec: null,
            aSep:''
        });


});


</g:javascript>



