
<%@ page import="com.kombos.parts.BlockStok" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="blockStok_datatables_${idTable}" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable">
	<thead>
		<tr>
            <th style="vertical-align: middle">
                <div> </div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
				<div><g:message code="blockStok.lastUpdProcess.label" default="Nomor Block Stok" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="blockStok.reception.label" default="Tanggal Block Stok" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="blockStok.staData.label" default="Nomor WO" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="blockStok.t157ID.label" default="Tanggal WO" /></div>
			</th>


		
		</tr>

	</thead>
</table>

<g:javascript>
var blockStokTable;
var reloadBlockStokTable;
$(function(){

    var recordsblockStokperpage = [];//new Array();
    var anblockStokSelected;
    var jmlRecblockStokPerPage=0;
    var id;


$('#clear').click(function(e){
        $('#search_t157TglBlockStok').val("");
        $('#search_t157TglBlockStokAkhir').val("");
        e.stopPropagation();
        blockStokTable.fnDraw();

	});
    $('#view').click(function(e){
        e.stopPropagation();
		blockStokTable.fnDraw();
//        reloadAuditTrailTable();
	});

var anOpen = [];
	$('#blockStok_datatables_${idTable} td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = blockStokTable.fnGetData(nTr);

    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${request.contextPath}/blockStok/sublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = blockStokTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});

  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			blockStokTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});


	reloadBlockStokTable = function() {
		blockStokTable.fnDraw();
	}


	$('#search_t157TglBlockStok').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t157TglBlockStok_day').val(newDate.getDate());
			$('#search_t157TglBlockStok_month').val(newDate.getMonth()+1);
			$('#search_t157TglBlockStok_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			blockStokTable.fnDraw();
	});




$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	blockStokTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	blockStokTable = $('#blockStok_datatables_${idTable}').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsblockStok = $("#blockStok_datatables tbody .row-select");
            var jmlblockStokCek = 0;
            var nRow;
            var idRec;
            rsblockStok.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsblockStokperpage[idRec]=="1"){
                    jmlblockStokCek = jmlblockStokCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsblockStokperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecblockStokPerPage = rsblockStok.length;
            if(jmlblockStokCek==jmlRecblockStokPerPage && jmlRecblockStokPerPage>0){
                $('#selectAll').attr('checked', true);
            } else {
                $('#selectAll').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
               "sName": "id",
               "mDataProp": null,
               "sClass": "control center",
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "lastUpdProcess",
	"mDataProp": "idBlockStok",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data+'<a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "reception",
	"mDataProp": "tanggalBlockStok",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "staData",
	"mDataProp": "nomorWO",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t157ID",
	"mDataProp": "tanggalWO",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}



],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

	                    var t017Tanggal1 = $('#search_t157TglBlockStok').val();
						var t017Tanggal2 = $('#search_t157TglBlockStokAkhir').val();

						if(t017Tanggal1){
							aoData.push(
									{"name": 'sCriteria_t0157TglBlockStok', "value": t017Tanggal1},
									{"name": 'sCriteria_t0157TglBlockStokAkhir', "value": t017Tanggal2}
							);
						}


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
	$('#selectAll').click(function(e) {

        $("#blockStok_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordsblockStokperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsblockStokperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#blockStok_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsblockStokperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anblockStokSelected = blockStokTable.$('tr.row_selected');
            if(jmlRecblockStokPerPage == anblockStokSelected.length){
                $('#selectAll').attr('checked', true);
            }
        } else {
            recordsblockStokperpage[id] = "0";
            $('#selectAll').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
