<%@ page import="com.kombos.parts.BlockStok" %>
<!DOCTYPE html>
<html>
<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'blockStok.label', default: 'Block Stock')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
<g:javascript>
	    $("#blockStokInputModal").on("show", function() {
		$("#blockStokInputModal .btn").on("click", function(e) {
			$("#blockStokInputModal").modal('hide');
		});
	});
	$("#blockStokInputModal").on("hide", function() {
		$("#blockStokInputModal a.btn").off("click");
	});

   	loadBlockStokInputModal = function(){
        var noWO = $("#reception").val();
		$("#blockStokInputContent").empty();
		$.ajax({type:'POST', url:'${request.contextPath}/blockStokInput',
		    data : {type : "1",idRecep : noWO},
   			success:function(data,textStatus){
					$("#blockStokInputContent").html(data);
				    $("#blockStokInputModal").modal({
						"backdrop" : "static",
						"keyboard" : true,
						"show" : true
					}).css({'width': '610px','margin-left': function () {return -($(this).width() / 2);}});

   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    }

    tambahGoods = function(){
           checkGoods =[];
            $("#blokStokDetail-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    checkGoods.push(id);
                }
            });
            if(checkGoods.length<1){
                alert('Anda belum memilih data yang akan ditambahkan');
                loadBlockStokInputsModal();
            }
                var checkGoodsMap = JSON.stringify(checkGoods);
                $.ajax({
                url:'${request.contextPath}/blockStok/tambah',
                type: "POST", // Always use POST when deleting data
                data : { ids: checkGoodsMap },
                success : function(data){
                    $('#result_search').empty();
                    reloadBlockStokFormTable();
                },
                error: function(xhr, textStatus, errorThrown) {
                    alert('Internal Server Error');
                }
                });

        checkGoods = [];

    }


    function saveStokFunc(){
     delStok =[];
           changeStaData = [];
           var a=0;
           var kode = $('#t157ID').val();
           var tanggal = $('#t157TglBlockStok').val();
           var nowo = $('#reception').val();
           var tanggalwo = $('#receptionTanggalWO').val();
            $("#blockStokTable-table tbody .row-select").each(function() {
                a++;
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    changeStaData.push(id);
                }else{
                    var id = $(this).next("input:hidden").val();
                    delStok.push(id);
                }
            });
            if(changeStaData.length<1){
                alert('Anda belum memilih data yang akan disimpan');
                return;
            }


            if(tanggal!="" && nowo!="" && tanggalwo!="" && changeStaData.length > 0){
            //    alert('masuk if');
                var delStokMap = JSON.stringify(delStok);
                var changeStokMap = JSON.stringify(changeStaData);
                $.ajax({
                url:'${request.contextPath}/blockStok/saveStok',
                type: "POST", // Always use POST when deleting data
                data : { del: delStokMap, change : changeStokMap , tanggal : tanggal, kode : kode, noWO : nowo },
                success : function(data){

                },
                error: function(xhr, textStatus, errorThrown) {
                    alert('Internal Server Error');
                }
                });
            }



        delStok = [];
        changeStaData = [];

    }

    $('#saveStok').click(function(){
    expandTableLayout('blockStok');
           delStok =[];
           changeStaData = [];
           var a=0;
           var kode = $('#t157ID').val();
           var tanggal = $('#t157TglBlockStok').val();
           var nowo = $('#reception').val();
           var tanggalwo = $('#receptionTanggalWO').val();
            $("#blockStokTable-table tbody .row-select").each(function() {
                a++;
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    changeStaData.push(id);
                }else{
                    var id = $(this).next("input:hidden").val();
                    delStok.push(id);
                }
            });
            if(changeStaData.length<1){
                alert('Anda belum memilih data yang akan disimpan');
            }


            if(tanggal!="" && nowo!="" && tanggalwo!="" && changeStaData.length > 0){
            //    alert('masuk if');
                var delStokMap = JSON.stringify(delStok);
                var changeStokMap = JSON.stringify(changeStaData);
                $.ajax({
                url:'${request.contextPath}/blockStok/saveStok',
                type: "POST", // Always use POST when deleting data
                data : { del: delStokMap, change : changeStokMap , tanggal : tanggal, kode : kode, noWO : nowo },
                success : function(data){
                var hasil = data
                    if(hasil=="sukses"){
                        console.log('masuk sukses')
                    $('#t157ID').val("");
                    $('#t157TglBlockStok').val("");
                    $('#receptionNowo').val("");
                    $('#receptionTanggalWO').val("");
                    expandTableLayout('blockStok');
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    alert('Internal Server Error');
                }
                });
            }

        delStok = [];
        changeStaData = [];

    });

    $('#cancel2').click(function(){
        delStok =[];
        changeStaData = [];
        $("#blockStokTable-table tbody .row-select").each(function() {
            if(this.checked){
                var id = $(this).next("input:hidden").val();
                changeStaData.push(id);
            }else{
                var id = $(this).next("input:hidden").val();
                delStok.push(id);
            }
        });
        var delStokMap = JSON.stringify(delStok);
        var changeStokMap = JSON.stringify(changeStaData);
        $.ajax({
        url:'${request.contextPath}/blockStok/delStok',
        type: "POST", // Always use POST when deleting data
        data : { del: delStokMap, change : changeStokMap},
        success : function(data){
            $('#t157ID').val("");
            $('#t157TglBlockStok').val("");
            $('#receptionNowo').val("");
            $('#receptionTanggalWO').val("");
            //reloadStokOPNameTable();
        },
        error: function(xhr, textStatus, errorThrown) {
            alert('Internal Server Error');
        }
        });
        delStok = [];
        changeStaData = [];
    });

    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
        </g:javascript>
	</head>
	<body>


    <div id="create-blockStok" class="content scaffold-create" role="main">
			<legend>Input Block Stok</legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${blockStokInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${blockStokInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form action="save" >--}%
             <fieldset class="form">
                    <g:render template="form"/>
                 <g:field style="" type="button" onclick="loadBlockStokInputModal();" class="btn btn-primary create"
                          name="cari" id="cari" value="${message(code: 'blockStok.button.cariGoods.label', default: 'Add Parts')}" />
                 </div></div>
                        <g:if test="${flash.message}">
                            <div class="message" role="status">
                                ${flash.message}
                            </div>
                        </g:if>
                    <input type="hidden" name="ids" id="ids" value="">

                  <g:render template="dataTablesBlockStokDetail" />
                 <a class="btn cancel" id="cancel"  href="javascript:void(0);"
                    onclick="bukaTableLayout()" ><g:message
                         code="default.button.cancel.label" default="Cancel" /></a>
                 <g:field type="button" onclick="saveBlockStok();" class="btn btn-primary create" name="tambah" id="tambah" value="${message(code: 'default.button.upload.label', default: 'Save')}"/>

             </fieldset>
               		</div>
	</body>
</html>
