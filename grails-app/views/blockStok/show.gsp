

<%@ page import="com.kombos.parts.BlockStok" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'blockStok.label', default: 'Block Stock')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteBlockStok;

$(function(){ 
	deleteBlockStok=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/blockStok/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadBlockStokTable();
   				expandTableLayout('blockStok');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-blockStok" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="blockStok"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${blockStokInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="blockStok.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${blockStokInstance}" field="createdBy"
								url="${request.contextPath}/BlockStok/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadBlockStokTable();" />--}%
							
								<g:fieldValue bean="${blockStokInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${blockStokInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="blockStok.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${blockStokInstance}" field="dateCreated"
								url="${request.contextPath}/BlockStok/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadBlockStokTable();" />--}%
							
								<g:formatDate date="${blockStokInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${blockStokInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="blockStok.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${blockStokInstance}" field="lastUpdProcess"
								url="${request.contextPath}/BlockStok/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadBlockStokTable();" />--}%
							
								<g:fieldValue bean="${blockStokInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${blockStokInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="blockStok.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${blockStokInstance}" field="lastUpdated"
								url="${request.contextPath}/BlockStok/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadBlockStokTable();" />--}%
							
								<g:formatDate date="${blockStokInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${blockStokInstance?.reception}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="reception-label" class="property-label"><g:message
					code="blockStok.reception.label" default="Reception" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="reception-label">
						%{--<ba:editableValue
								bean="${blockStokInstance}" field="reception"
								url="${request.contextPath}/BlockStok/updatefield" type="text"
								title="Enter reception" onsuccess="reloadBlockStokTable();" />--}%
							
								<g:link controller="reception" action="show" id="${blockStokInstance?.reception?.id}">${blockStokInstance?.reception?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${blockStokInstance?.staData}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staData-label" class="property-label"><g:message
					code="blockStok.staData.label" default="Sta Data" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staData-label">
						%{--<ba:editableValue
								bean="${blockStokInstance}" field="staData"
								url="${request.contextPath}/BlockStok/updatefield" type="text"
								title="Enter staData" onsuccess="reloadBlockStokTable();" />--}%
							
								<g:fieldValue bean="${blockStokInstance}" field="staData"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${blockStokInstance?.t157ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t157ID-label" class="property-label"><g:message
					code="blockStok.t157ID.label" default="T157 ID" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t157ID-label">
						%{--<ba:editableValue
								bean="${blockStokInstance}" field="t157ID"
								url="${request.contextPath}/BlockStok/updatefield" type="text"
								title="Enter t157ID" onsuccess="reloadBlockStokTable();" />--}%
							
								<g:fieldValue bean="${blockStokInstance}" field="t157ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${blockStokInstance?.t157TglBlockStok}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t157TglBlockStok-label" class="property-label"><g:message
					code="blockStok.t157TglBlockStok.label" default="T157 Tgl Block Stok" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t157TglBlockStok-label">
						%{--<ba:editableValue
								bean="${blockStokInstance}" field="t157TglBlockStok"
								url="${request.contextPath}/BlockStok/updatefield" type="text"
								title="Enter t157TglBlockStok" onsuccess="reloadBlockStokTable();" />--}%
							
								<g:formatDate date="${blockStokInstance?.t157TglBlockStok}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${blockStokInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="blockStok.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${blockStokInstance}" field="updatedBy"
								url="${request.contextPath}/BlockStok/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadBlockStokTable();" />--}%
							
								<g:fieldValue bean="${blockStokInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('blockStok');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${blockStokInstance?.id}"
					update="[success:'blockStok-form',failure:'blockStok-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteBlockStok('${blockStokInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
