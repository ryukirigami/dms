<%@ page import="com.kombos.reception.Reception; com.kombos.parts.BlockStok" %>
<g:javascript>
			$('#noWo').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/pickingSlipInput/woList', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});

            detailWo = function(){
                    var noWo = $('#noWo').val();
                    $.ajax({
                        url:'${request.contextPath}/blockStok/detailWo?noWo='+noWo,
                        type: "POST", // Always use POST when deleting data
                        success : function(data){
                            $('#reception').val(data);
                        }
                    })
            }
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: blockStokInstance, field: 't157TglBlockStok', 'error')} ">
    <label class="control-label" for="t157TglBlockStok">
        <g:message code="blockStok.t157TglBlockStok.label" default="Tanggal Block Stock" />

    </label>
    <div class="controls">
        <ba:datePicker name="tanggal" id="tanggal" precision="day" value="${blockStokInstance?.t157TglBlockStok}" format="dd-MM-yyyy"/>
    </div>
</div>
<g:javascript>
    document.getElementById("tanggal").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: blockStokInstance, field: 'reception', 'error')} ">
	<label class="control-label" for="reception">
		<g:message code="blockStok.reception.label" default="Nomor WO" />

	</label>
	<div class="controls">
	%{--<g:select id="reception" name="reception.id" from="${Reception.list()}" optionKey="id" required="" value="${blockStokInstance?.reception?.id}" class="many-to-one"/>--}%
        <g:textField name="noWo" id="noWo" maxlength="20"  class="typeahead" onblur="detailWo()" />
        <g:hiddenField id="reception" name="reception.id" />
    </div>
</div>
