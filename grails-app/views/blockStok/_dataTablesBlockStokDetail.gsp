
<%@ page import="com.kombos.parts.BlokStokDetail; com.kombos.parts.Location; com.kombos.parts.BlockStok" %>

<r:require modules="baseapplayout" />

<r:require modules="autoNumeric" />
<g:render template="../menu/maxLineDisplay"/>
<br/><br/><br/>
<table id="blockStokDetail_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>
            <th></th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:checkBox name="" class="select-all"/> &nbsp;<g:message code="blockStok.kode.part.label" default="Kode Part" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="blockStok.nama.part.label" default="Nama Part" /></div>
            </th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="blockStok.qty.picking.label" default="Qty Picking" /></div>
			</th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="blockStok.satuan1.label" default="Satuan" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="blockStok.qty.label" default="Qty Block Stock" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="blockStok.satuan2.label" default="Satuan" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="blockStok.lokasi.label" default="Lokasi" /></div>
            </th>

		</tr>
	</thead>
</table>

<g:javascript>
var blockStokDetailTable;
var reloadBlockStokFormTable;
var selectLocation;
var qtyBlockStokCek;
$(function(){

	 var recordsWarrantyClaimperpage = [];//new Array();
    var anWarrantySelected;
    var jmlRecWarrantyPerPage=0;
    //var activePage = $('li.active').text();
    var id;


	reloadBlockStokFormTable = function() {
		blockStokDetailTable.fnDraw();
	}


qtyBlockStokCek = function(rowId){
    var value = jQuery('#qtyBlockStok'+rowId).val();

    if(value==""){
        alert("Mohon QTY Block Stok diisi");
        return false;
    }
    else
        return true;
}

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	blockStokDetailTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	blockStokDetailTable = $('#blockStokDetail_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsblockStokDetail = $("#blockStokDetail_datatables tbody .row-select");
            var jmlblockStokDetailCek = 0;
            var nRow;
            var idRec;
            rsblockStokDetail.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsWarrantyClaimperpage[idRec]=="1"){
                    jmlblockStokDetailCek = jmlblockStokDetailCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsWarrantyClaimperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecblockStokDetailPerPage = rsblockStokDetail.length;
            if(jmlblockStokDetailCek==jmlRecblockStokDetailPerPage && jmlRecblockStokDetailPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
	//	"bProcessing": true,
	//	"bServerSide": true,
	//	"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
	//	"sAjaxSource": "${g.createLink(action: "datatablesBlockStokDetail")}",
		"aoColumns": [

{
	"sName": "id",
	"mDataProp": "id",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}
,

{
	"sName": "kodeParts",
	"mDataProp": "kodeParts",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<label>&nbsp;&nbsp;'+data+'</label><input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'" id="idBlockStokDetail'+row['id']+'">&nbsp;&nbsp;&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "namaParts",
	"mDataProp": "namaParts",
	"aTargets": [1],
	"mRender": function ( data, type, row ) {
                        return '<label>'+data+'</label><input type="hidden" value="'+data+'" id="kodeParts'+row['id']+'"><input id="namaParts'+row['id']+'" type="hidden" value="'+data+'"/>';
    },
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "qtyPicking",
	"mDataProp": "qtyPicking",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "satuan1",
	"mDataProp": "satuan1",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "qtyBlockStock",
	"mDataProp": "qtyBlockStock",
	"aTargets": [4],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
                        return '<input type="text" id="qtyBlockStok'+row['id']+'" maxlength="8" class="inline-edit" value="0"/>';
    },
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "satuan2",
	"mDataProp": "satuan2",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "location",
	"mDataProp": "location",
	"mRender": function ( data, type, row ) {
        console.log("nilai row : "+row['id']);
        return '<select id="location'+row['id']+'" />';
    },
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
       // selectLocation(irow['location']);
      // alert("nilai irow "+iRow);
        console.log("nilai iRow "+oData['id']);
        selectLocation(oData['id']);
      },
	"sWidth":"200px",
	"bVisible": true
}




],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                        aoData.push(
									{"name": 'blockStokDetailList', "value": "${blockStokDetailList}"}
						);


						var t157ID = $('#filter_kode_goods input').val();
						if(t157ID){
							aoData.push(
									{"name": 'sCriteria_kode_goods', "value": t157ID}
							);
						}

						var reception = $('#filter_nama_goods input').val();
						if(reception){
							aoData.push(
									{"name": 'sCriteria_nama_goods', "value": reception}
							);
						}

						var reception = $('#filter_qtypicking input').val();
						if(reception){
							aoData.push(
									{"name": 'sCriteria_qtypicking', "value": reception}
							);
						}

						var t157ID = $('#filter_satuan1_satuan input').val();
						if(t157ID){
							aoData.push(
									{"name": 'sCriteria_satuan1_satuan', "value": t157ID}
							);
						}

						var reception = $('#filter_t158Qty1 input').val();
						if(reception){
							aoData.push(
									{"name": 'sCriteria_t158Qty1', "value": reception}
							);
						}

						var reception = $('#filter_satuan2_satuan input').val();
						if(reception){
							aoData.push(
									{"name": 'sCriteria_satuan2_satuan', "value": reception}
							);
						}

						var reception = $('#filter_location input').val();
						if(reception){
							aoData.push(
									{"name": 'sCriteria_location', "value": reception}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
	
    	 $('.select-all').click(function(e) {

        $("#blockStokDetail_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordsWarrantyClaimperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsWarrantyClaimperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });





});


            function selectLocation(id) {
                jQuery.getJSON('${request.contextPath}/blockStok/getLocations', function (data) {
                    if (data) {
                        jQuery.each(data, function (index, value) {
                            $('#location'+id).append("<option value='" + value.id + "'>" + value.namaLokasi + "</option>");
                        });
                    }
                });

 }

 function editRow (nRow)
    {
        var idTr = '#blockStokDetail_datatables tr:eq('+nRow+')';
        var trElm = $(idTr);
        trHtml = trElm.html(); //in case need of cancel button
        var id = jQuery('#idBlockStokDetail'+nRow).val();
        var oldSatuanSatu = jQuery('#satuanSatu'+nRow).val();
        var oldSatuanDua = jQuery('#satuanDua'+nRow).val();


        var kodeParts = jQuery('#kodeParts'+nRow).val();
        var namaParts = jQuery('#namaParts'+nRow).val();
        var qtyPicking = jQuery('#qtyPickingSlip'+nRow).val();
        var oldQtyBlockStok = jQuery('#qtyBlockStok'+nRow).val();
        var location = jQuery('#location'+nRow).val();

       var htmlEdit =
                '<td class="">&nbsp;&nbsp;' +kodeParts+
'<input type="checkbox" class="pull-left row-select" aria-label="Row '+id+'" title="Select this"><input type="hidden" value="'+id+'" id="idBlockStokDetail'+nRow+'"><input type="hidden" value="'+kodeParts+'" id="kodeParts'+id+'">&nbsp;&nbsp;&nbsp;&nbsp;</a>' +
'</td>' +
                        '<td class=""><label>'+namaParts+'</label><input id="namaParts'+id+'" type="hidden" value="'+namaParts+'"/></td>' +
                        '<td class=""><label>'+qtyPicking+'</label><input id="qtyPickingSlip'+id+'" type="hidden" value="'+qtyPicking+'"/></td>' +
                        '<td class=""><label>'+oldSatuanSatu+'</label><input id="satuanSatu'+id+'" type="hidden" value="'+oldSatuanSatu+'"/></td>' +
                        '<td class=""><input id="qtyBlockStok'+id+'"  type="text" size="3" value="'+oldQtyBlockStok+'"/></td>' +
                        '<td class=""><label>'+oldSatuanDua+'</label><input id="satuanDua'+id+'" type="hidden" value="'+oldSatuanDua+'"/></td>' +
                         '<td class=""><input id="location'+id+'" type="hidden" value="'+location+'"/><select id="locationSelect"/></td>' +
                        '<td class=""><a class="edit" href="javascript:saveEditRow('+id+')">Save</a>&nbsp;/&nbsp;<a class="edit" href="javascript:cancelEditRow('+id+')">Cancel</a></td>';

        trElm.html(htmlEdit);

        selectLocation();


    }

    function cancelEditRow(nRow){
//        var idTr = '#rolePermissionDatatables_datatable tr:eq('+nRow+')';
//        var trElm = $(idTr);
//        trElm.html(trHtml);
        reloadBlockStokFormTable();
    }

    function saveEditRow(nRow){
        //do ajax
        var cekQty = qtyBlockStokCek(nRow);
        if(cekQty == false)
            return;

        var idTr = '#blockStokDetail_datatables tr:eq('+nRow+')';
        var trElm = $(idTr);
        trHtml = trElm.html(); //in case need of cancel button
        var id = jQuery('#idBlockStokDetail'+nRow).val();

		var oldSatuanSatu = jQuery('#satuanSatu'+nRow).val();
        var oldSatuanDua = jQuery('#satuanDua'+nRow).val();


        var kodeParts = jQuery('#kodeParts'+nRow).val();
        var namaParts = jQuery('#namaParts'+nRow).val();
        var qtyPicking = jQuery('#qtyPickingSlip'+nRow).val();
        var oldQtyBlockStok = jQuery('#qtyBlockStok'+nRow).val();
        var location = jQuery('#locationSelect').val();

        jQuery.ajax({
            type: 'POST',
            url: '${request.getContextPath()}/blockStok/editBlockStokDetail',
            data: {
                idBlockStokDetail : id,
				qtyBlockStok : oldQtyBlockStok,
				location : location

            },
            async: false,
            success: function(data) {
                if(data.message)
                	alert(data.message);
                else
                    alert("Your request will be submitted to request for approval");
            },
            error: function(){
                alert('Server Response Error');
            }
        });

        reloadBlockStokFormTable();
    }


</g:javascript>


			
