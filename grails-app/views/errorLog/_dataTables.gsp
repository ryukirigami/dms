
<%@ page import="com.kombos.administrasi.ErrorLog" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="errorLog_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="errorLog.m779Tanggal.label" default="Tanggal" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="errorLog.m779Jam.label" default="Jam" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="errorLog.userProfile.label" default="Username" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="errorLog.namaForm.label" default="Location" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="errorLog.m779Event.label" default="Event" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="errorLog.m779ErrorMsg.label" default="Pesan Kesalahan" /></div>
			</th>


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="errorLog.m779Id.label" default="M779 Id" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="errorLog.m779xNamaUser.label" default="M779x Nama User" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="errorLog.m779xNamaDivisi.label" default="M779x Nama Divisi" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="errorLog.staDel.label" default="Sta Del" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="errorLog.createdBy.label" default="Created By" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="errorLog.updatedBy.label" default="Updated By" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="errorLog.lastUpdProcess.label" default="Last Upd Process" /></div>--}%
			%{--</th>--}%

		
		</tr>
		%{--<tr>--}%
		%{----}%

			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_m779Tanggal" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >--}%
					%{--<input type="hidden" name="search_m779Tanggal" value="date.struct">--}%
					%{--<input type="hidden" name="search_m779Tanggal_day" id="search_m779Tanggal_day" value="">--}%
					%{--<input type="hidden" name="search_m779Tanggal_month" id="search_m779Tanggal_month" value="">--}%
					%{--<input type="hidden" name="search_m779Tanggal_year" id="search_m779Tanggal_year" value="">--}%
					%{--<input type="text" data-date-format="dd/mm/yyyy" name="search_m779Tanggal_dp" value="" id="search_m779Tanggal" class="search_init">--}%
				%{--</div>--}%
			%{--</th>--}%

			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_m779Jam" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
                    %{--<input type="text" name="search_m779Jam" class="search_init" />--}%
                %{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_userProfile" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_userProfile" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_namaForm" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_namaForm" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_m779Event" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_m779Event" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_m779ErrorMsg" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_m779ErrorMsg" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_m779Id" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_m779Id" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_m779xNamaUser" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_m779xNamaUser" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_m779xNamaDivisi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_m779xNamaDivisi" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_staDel" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_createdBy" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_updatedBy" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_lastUpdProcess" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%


		%{--</tr>--}%
	</thead>
</table>

<g:javascript>
var errorLogTable;
var reloadErrorLogTable;
$(function(){
	
	reloadErrorLogTable = function() {
		errorLogTable.fnDraw();
	}

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	errorLogTable.fnDraw();
		}
	});
	$('#clear').click(function(e){
        $('#search_m779Tanggal').val("");
        $('#search_m779Tanggal_day').val("");
        $('#search_m779Tanggal_month').val("");
        $('#search_m779Tanggal_year').val("");
        $('#search_m779TanggalAkhir').val("");
        $('#search_m779TanggalAkhir_day').val("");
        $('#search_m779TanggalAkhir_month').val("");
        $('#search_m779TanggalAkhir_year').val("");
        $('#filter_m779ErrorMsg input').val("");
        $('#filter_userProfile select      ').val("");
//        reloadAuditTrailTable();
	});
	$('#view').click(function(e){
        e.stopPropagation();
		errorLogTable.fnDraw();
//        reloadAuditTrailTable();
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	errorLogTable = $('#errorLog_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m779Tanggal",
	"mDataProp": "m779Tanggal",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m779Jam",
	"mDataProp": "m779Jam",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "userProfile",
	"mDataProp": "userProfile",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "namaForm",
	"mDataProp": "namaForm",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m779Event",
	"mDataProp": "m779Event",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m779ErrorMsg",
	"mDataProp": "m779ErrorMsg",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

//,
//
//{
//	"sName": "m779Id",
//	"mDataProp": "m779Id",
//	"aTargets": [6],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "m779xNamaUser",
//	"mDataProp": "m779xNamaUser",
//	"aTargets": [7],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "m779xNamaDivisi",
//	"mDataProp": "m779xNamaDivisi",
//	"aTargets": [8],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "staDel",
//	"mDataProp": "staDel",
//	"aTargets": [9],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "createdBy",
//	"mDataProp": "createdBy",
//	"aTargets": [10],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "updatedBy",
//	"mDataProp": "updatedBy",
//	"aTargets": [11],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "lastUpdProcess",
//	"mDataProp": "lastUpdProcess",
//	"aTargets": [12],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var m779Tanggal = $('#search_m779Tanggal').val();
						var m779TanggalDay = $('#search_m779Tanggal_day').val();
						var m779TanggalMonth = $('#search_m779Tanggal_month').val();
						var m779TanggalYear = $('#search_m779Tanggal_year').val();
						
						if(m779Tanggal){
							aoData.push(
									{"name": 'sCriteria_m779Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_m779Tanggal_dp', "value": m779Tanggal},
									{"name": 'sCriteria_m779Tanggal_day', "value": m779TanggalDay},
									{"name": 'sCriteria_m779Tanggal_month', "value": m779TanggalMonth},
									{"name": 'sCriteria_m779Tanggal_year', "value": m779TanggalYear}
							);
						}

						var m779TanggalAkhir = $('#search_m779TanggalAkhir').val();
						var m779TanggalAkhirDay = $('#search_m779TanggalAkhir_day').val();
						var m779TanggalAkhirMonth = $('#search_m779TanggalAkhir_month').val();
						var m779TanggalAkhirYear = $('#search_m779TanggalAkhir_year').val();

						if(m779TanggalAkhir){
							aoData.push(
									{"name": 'sCriteria_m779TanggalAkhir', "value": "date.struct"},
									{"name": 'sCriteria_m779TanggalAkhir_dp', "value": m779TanggalAkhir},
									{"name": 'sCriteria_m779TanggalAkhir_day', "value": m779TanggalAkhirDay},
									{"name": 'sCriteria_m779TanggalAkhir_month', "value": m779TanggalAkhirMonth},
									{"name": 'sCriteria_m779TanggalAkhir_year', "value": m779TanggalAkhirYear}
							);
						}

						var m779Jam = $('#m779Jam input').val();
                        if(m779Jam){
                            aoData.push(
                                    {"name": 'sCriteria_m779Jam', "value": m779Jam}
                            );
                        }
	
						var userProfile = $('#filter_userProfile select').val();
						if(userProfile){
							aoData.push(
									{"name": 'sCriteria_userProfile', "value": userProfile}
							);
						}
	
						var namaForm = $('#filter_namaForm input').val();
						if(namaForm){
							aoData.push(
									{"name": 'sCriteria_namaForm', "value": namaForm}
							);
						}
	
						var m779Event = $('#filter_m779Event input').val();
						if(m779Event){
							aoData.push(
									{"name": 'sCriteria_m779Event', "value": m779Event}
							);
						}
	
						var m779ErrorMsg = $('#filter_m779ErrorMsg input').val();
						if(m779ErrorMsg){
							aoData.push(
									{"name": 'sCriteria_m779ErrorMsg', "value": m779ErrorMsg}
							);
						}
	
						var m779Id = $('#filter_m779Id input').val();
						if(m779Id){
							aoData.push(
									{"name": 'sCriteria_m779Id', "value": m779Id}
							);
						}
	
						var m779xNamaUser = $('#filter_m779xNamaUser input').val();
						if(m779xNamaUser){
							aoData.push(
									{"name": 'sCriteria_m779xNamaUser', "value": m779xNamaUser}
							);
						}
	
						var m779xNamaDivisi = $('#filter_m779xNamaDivisi input').val();
						if(m779xNamaDivisi){
							aoData.push(
									{"name": 'sCriteria_m779xNamaDivisi', "value": m779xNamaDivisi}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
