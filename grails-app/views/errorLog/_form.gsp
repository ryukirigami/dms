<%@ page import="com.kombos.administrasi.NamaForm; com.kombos.administrasi.UserProfile; com.kombos.administrasi.ErrorLog" %>



<div class="control-group fieldcontain ${hasErrors(bean: errorLogInstance, field: 'm779Tanggal', 'error')} required">
	<label class="control-label" for="m779Tanggal">
		<g:message code="errorLog.m779Tanggal.label" default="M779 Tanggal" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<ba:datePicker format="dd/MM/yyyy" name="m779Tanggal" precision="day"  value="${errorLogInstance?.m779Tanggal}"  />
	</div>
</div>
<g:javascript>
    document.getElementById("m779Tanggal").focus();
</g:javascript>
%{--<div class="control-group fieldcontain ${hasErrors(bean: errorLogInstance, field: 'm779Jam', 'error')} ">--}%
	%{--<label class="control-label" for="m779Jam">--}%
		%{--<g:message code="errorLog.m779Jam.label" default="M779 Jam" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:datePicker name="m779Jam" precision="day"  value="${errorLogInstance?.m779Jam}" default="none" noSelection="['': '']" />--}%
	%{--</div>--}%
%{--</div>--}%

<div class="control-group fieldcontain ${hasErrors(bean: errorLogInstance, field: 'userProfile', 'error')} required">
	<label class="control-label" for="userProfile">
		<g:message code="errorLog.userProfile.label" default="Username" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="userProfile" name="userProfile.id" from="${UserProfile.list()}" optionKey="id" required="" value="${errorLogInstance?.userProfile?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: errorLogInstance, field: 'namaForm', 'error')} ">
	<label class="control-label" for="namaForm">
		<g:message code="errorLog.namaForm.label" default="Location" />
		
	</label>
	<div class="controls">
	<g:select id="namaForm" name="namaForm.id" from="${NamaForm.list()}" optionKey="id" value="${errorLogInstance?.namaForm?.id}" class="many-to-one" noSelection="['null': '']"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: errorLogInstance, field: 'm779Event', 'error')} ">
	<label class="control-label" for="m779Event">
		<g:message code="errorLog.m779Event.label" default="Event" />
		
	</label>
	<div class="controls">
	<g:textField name="m779Event" maxlength="50" value="${errorLogInstance?.m779Event}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: errorLogInstance, field: 'm779ErrorMsg', 'error')} ">
	<label class="control-label" for="m779ErrorMsg">
		<g:message code="errorLog.m779ErrorMsg.label" default="Pesan Kesalahan" />
		
	</label>
	<div class="controls">
	<g:textArea name="m779ErrorMsg" cols="40" rows="5" maxlength="255" value="${errorLogInstance?.m779ErrorMsg}"/>
	</div>
</div>

%{--<div class="control-group fieldcontain ${hasErrors(bean: errorLogInstance, field: 'm779Id', 'error')} ">--}%
	%{--<label class="control-label" for="m779Id">--}%
		%{--<g:message code="errorLog.m779Id.label" default="M779 Id" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:field name="m779Id" type="number" value="${errorLogInstance.m779Id}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: errorLogInstance, field: 'm779xNamaUser', 'error')} ">--}%
	%{--<label class="control-label" for="m779xNamaUser">--}%
		%{--<g:message code="errorLog.m779xNamaUser.label" default="M779x Nama User" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="m779xNamaUser" maxlength="20" value="${errorLogInstance?.m779xNamaUser}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: errorLogInstance, field: 'm779xNamaDivisi', 'error')} ">--}%
	%{--<label class="control-label" for="m779xNamaDivisi">--}%
		%{--<g:message code="errorLog.m779xNamaDivisi.label" default="M779x Nama Divisi" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="m779xNamaDivisi" maxlength="20" value="${errorLogInstance?.m779xNamaDivisi}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: errorLogInstance, field: 'staDel', 'error')} required">--}%
	%{--<label class="control-label" for="staDel">--}%
		%{--<g:message code="errorLog.staDel.label" default="Sta Del" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="staDel" maxlength="1" required="" value="${errorLogInstance?.staDel}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: errorLogInstance, field: 'createdBy', 'error')} ">--}%
	%{--<label class="control-label" for="createdBy">--}%
		%{--<g:message code="errorLog.createdBy.label" default="Created By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="createdBy" value="${errorLogInstance?.createdBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: errorLogInstance, field: 'updatedBy', 'error')} ">--}%
	%{--<label class="control-label" for="updatedBy">--}%
		%{--<g:message code="errorLog.updatedBy.label" default="Updated By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="updatedBy" value="${errorLogInstance?.updatedBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: errorLogInstance, field: 'lastUpdProcess', 'error')} ">--}%
	%{--<label class="control-label" for="lastUpdProcess">--}%
		%{--<g:message code="errorLog.lastUpdProcess.label" default="Last Upd Process" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="lastUpdProcess" value="${errorLogInstance?.lastUpdProcess}"/>--}%
	%{--</div>--}%
%{--</div>--}%

