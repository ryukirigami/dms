

<%@ page import="com.kombos.administrasi.ErrorLog" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'errorLog.label', default: 'Error Log')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteErrorLog;

$(function(){ 
	deleteErrorLog=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/errorLog/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadErrorLogTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-errorLog" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="errorLog"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${errorLogInstance?.m779Tanggal}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m779Tanggal-label" class="property-label"><g:message
					code="errorLog.m779Tanggal.label" default="Tanggal" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m779Tanggal-label">
						%{--<ba:editableValue
								bean="${errorLogInstance}" field="m779Tanggal"
								url="${request.contextPath}/ErrorLog/updatefield" type="text"
								title="Enter m779Tanggal" onsuccess="reloadErrorLogTable();" />--}%
							
								<g:formatDate date="${errorLogInstance?.m779Tanggal}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${errorLogInstance?.m779Jam}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m779Jam-label" class="property-label"><g:message
					code="errorLog.m779Jam.label" default="Jam" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m779Jam-label">
						%{--<ba:editableValue
								bean="${errorLogInstance}" field="m779Jam"
								url="${request.contextPath}/ErrorLog/updatefield" type="text"
								title="Enter m779Jam" onsuccess="reloadErrorLogTable();" />--}%
							
								<g:formatDate date="${errorLogInstance?.m779Jam}" format="HH:mm:ss"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${errorLogInstance?.userProfile}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="userProfile-label" class="property-label"><g:message
					code="errorLog.userProfile.label" default="Username" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="userProfile-label">
						%{--<ba:editableValue
								bean="${errorLogInstance}" field="userProfile"
								url="${request.contextPath}/ErrorLog/updatefield" type="text"
								title="Enter userProfile" onsuccess="reloadErrorLogTable();" />--}%
							
								<g:link controller="userProfile" action="show" id="${errorLogInstance?.userProfile?.id}">${errorLogInstance?.userProfile?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${errorLogInstance?.namaForm}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="namaForm-label" class="property-label"><g:message
					code="errorLog.namaForm.label" default="Location" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="namaForm-label">
						%{--<ba:editableValue
								bean="${errorLogInstance}" field="namaForm"
								url="${request.contextPath}/ErrorLog/updatefield" type="text"
								title="Enter namaForm" onsuccess="reloadErrorLogTable();" />--}%
							
								<g:link controller="namaForm" action="show" id="${errorLogInstance?.namaForm?.id}">${errorLogInstance?.namaForm?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${errorLogInstance?.m779Event}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m779Event-label" class="property-label"><g:message
					code="errorLog.m779Event.label" default="Event" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m779Event-label">
						%{--<ba:editableValue
								bean="${errorLogInstance}" field="m779Event"
								url="${request.contextPath}/ErrorLog/updatefield" type="text"
								title="Enter m779Event" onsuccess="reloadErrorLogTable();" />--}%
							
								<g:fieldValue bean="${errorLogInstance}" field="m779Event"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${errorLogInstance?.m779ErrorMsg}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m779ErrorMsg-label" class="property-label"><g:message
					code="errorLog.m779ErrorMsg.label" default="Pesan Kesalahan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m779ErrorMsg-label">
						%{--<ba:editableValue
								bean="${errorLogInstance}" field="m779ErrorMsg"
								url="${request.contextPath}/ErrorLog/updatefield" type="text"
								title="Enter m779ErrorMsg" onsuccess="reloadErrorLogTable();" />--}%
							
								<g:fieldValue bean="${errorLogInstance}" field="m779ErrorMsg"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${errorLogInstance?.m779Id}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="m779Id-label" class="property-label"><g:message--}%
					%{--code="errorLog.m779Id.label" default="M779 Id" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="m779Id-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${errorLogInstance}" field="m779Id"--}%
								%{--url="${request.contextPath}/ErrorLog/updatefield" type="text"--}%
								%{--title="Enter m779Id" onsuccess="reloadErrorLogTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${errorLogInstance}" field="m779Id"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${errorLogInstance?.m779xNamaUser}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="m779xNamaUser-label" class="property-label"><g:message--}%
					%{--code="errorLog.m779xNamaUser.label" default="M779x Nama User" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="m779xNamaUser-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${errorLogInstance}" field="m779xNamaUser"--}%
								%{--url="${request.contextPath}/ErrorLog/updatefield" type="text"--}%
								%{--title="Enter m779xNamaUser" onsuccess="reloadErrorLogTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${errorLogInstance}" field="m779xNamaUser"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${errorLogInstance?.m779xNamaDivisi}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="m779xNamaDivisi-label" class="property-label"><g:message--}%
					%{--code="errorLog.m779xNamaDivisi.label" default="M779x Nama Divisi" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="m779xNamaDivisi-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${errorLogInstance}" field="m779xNamaDivisi"--}%
								%{--url="${request.contextPath}/ErrorLog/updatefield" type="text"--}%
								%{--title="Enter m779xNamaDivisi" onsuccess="reloadErrorLogTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${errorLogInstance}" field="m779xNamaDivisi"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${errorLogInstance?.staDel}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="staDel-label" class="property-label"><g:message--}%
					%{--code="errorLog.staDel.label" default="Sta Del" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="staDel-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${errorLogInstance}" field="staDel"--}%
								%{--url="${request.contextPath}/ErrorLog/updatefield" type="text"--}%
								%{--title="Enter staDel" onsuccess="reloadErrorLogTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${errorLogInstance}" field="staDel"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%

            <g:if test="${errorLogInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="errorLog.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${errorLogInstance}" field="dateCreated"
                                url="${request.contextPath}/errorLog/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloaderrorLogTable();" />--}%

                        <g:formatDate date="${errorLogInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${errorLogInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="errorLog.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${errorLogInstance}" field="createdBy"
                                url="${request.contextPath}/errorLog/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloaderrorLogTable();" />--}%

                        <g:fieldValue bean="${errorLogInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${errorLogInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="errorLog.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${errorLogInstance}" field="lastUpdated"
                                url="${request.contextPath}/errorLog/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloaderrorLogTable();" />--}%

                        <g:formatDate date="${errorLogInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${errorLogInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="errorLog.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${errorLogInstance}" field="updatedBy"
                                url="${request.contextPath}/errorLog/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloaderrorLogTable();" />--}%

                        <g:fieldValue bean="${errorLogInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${errorLogInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="errorLog.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${errorLogInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/errorLog/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloaderrorLogTable();" />--}%

                        <g:fieldValue bean="${errorLogInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${errorLogInstance?.id}"
					update="[success:'errorLog-form',failure:'errorLog-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteErrorLog('${errorLogInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
