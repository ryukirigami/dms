

<%@ page import="com.kombos.administrasi.MasterPanel" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'masterPanel.label', default: 'Panel')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteMasterPanel;

$(function(){ 
	deleteMasterPanel=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/masterPanel/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadMasterPanelTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-masterPanel" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="masterPanel"
			class="table table-bordered table-hover">
			<tbody>

                <g:if test="${masterPanelInstance?.m094ID}">
                <tr>
                <td class="span2" style="text-align: right;"><span
                id="m094ID-label" class="property-label"><g:message
                code="masterPanel.m094ID.label" default="Kode Panel" />:</span></td>

                <td class="span3"><span class="property-value"
                aria-labelledby="m094ID-label">
                %{--<ba:editableValue
					bean="${masterPanelInstance}" field="m094ID"
					url="${request.contextPath}/MasterPanel/updatefield" type="text"
					title="Enter m094ID" onsuccess="reloadMasterPanelTable();" />--}%

                <g:fieldValue bean="${masterPanelInstance}" field="m094ID"/>

                </span></td>

                </tr>
                </g:if>
				
				<g:if test="${masterPanelInstance?.m094NamaPanel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m094NamaPanel-label" class="property-label"><g:message
					code="masterPanel.m094NamaPanel.label" default="Nama Panel" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m094NamaPanel-label">
						%{--<ba:editableValue
								bean="${masterPanelInstance}" field="m094NamaPanel"
								url="${request.contextPath}/MasterPanel/updatefield" type="text"
								title="Enter m094NamaPanel" onsuccess="reloadMasterPanelTable();" />--}%
							
								<g:fieldValue bean="${masterPanelInstance}" field="m094NamaPanel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterPanelInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="masterPanel.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${masterPanelInstance}" field="lastUpdProcess"
								url="${request.contextPath}/MasterPanel/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadMasterPanelTable();" />--}%
							
								<g:fieldValue bean="${masterPanelInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${masterPanelInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="masterPanel.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${masterPanelInstance}" field="dateCreated"
								url="${request.contextPath}/MasterPanel/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadMasterPanelTable();" />--}%
							
								<g:formatDate date="${masterPanelInstance?.dateCreated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>

                <g:if test="${masterPanelInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="masterPanel.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${masterPanelInstance}" field="createdBy"
                                url="${request.contextPath}/MasterPanel/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadMasterPanelTable();" />--}%

                        <g:fieldValue bean="${masterPanelInstance}" field="createdBy"/>

                    </span></td>

                </tr>
                </g:if>
			
				<g:if test="${masterPanelInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="masterPanel.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${masterPanelInstance}" field="lastUpdated"
								url="${request.contextPath}/MasterPanel/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadMasterPanelTable();" />--}%
							
								<g:formatDate date="${masterPanelInstance?.lastUpdated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>

                <g:if test="${masterPanelInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="masterPanel.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${masterPanelInstance}" field="updatedBy"
                                url="${request.contextPath}/MasterPanel/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadMasterPanelTable();" />--}%

                        <g:fieldValue bean="${masterPanelInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
                </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${masterPanelInstance?.id}"
					update="[success:'masterPanel-form',failure:'masterPanel-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteMasterPanel('${masterPanelInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
