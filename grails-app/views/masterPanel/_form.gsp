<%@ page import="com.kombos.administrasi.MasterPanel" %>

<g:if test="${masterPanelInstance.m094ID}">
    <div class="control-group fieldcontain ${hasErrors(bean: masterPanelInstance, field: 'm094ID', 'error')} ">
        <label class="control-label" for="m094ID">
        <g:message code="masterPanel.m094ID.label" default="Kode Panel" />

        </label>
        <div class="controls">
        ${masterPanelInstance.m094ID}
        </div>
    </div>
</g:if>

<div class="control-group fieldcontain ${hasErrors(bean: masterPanelInstance, field: 'm094NamaPanel', 'error')} required">
	<label class="control-label" for="m094NamaPanel">
		<g:message code="masterPanel.m094NamaPanel.label" default="Nama Panel" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m094NamaPanel" id="m094NamaPanel" required="" value="${masterPanelInstance?.m094NamaPanel}" maxlength="50"/>
	</div>
</div>

<g:javascript>
    document.getElementById('m094NamaPanel').focus();
</g:javascript>


%{--<div class="control-group fieldcontain ${hasErrors(bean: masterPanelInstance, field: 'createdBy', 'error')} ">--}%
	%{--<label class="control-label" for="createdBy">--}%
		%{--<g:message code="masterPanel.createdBy.label" default="Created By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="createdBy" value="${masterPanelInstance?.createdBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: masterPanelInstance, field: 'updatedBy', 'error')} ">--}%
	%{--<label class="control-label" for="updatedBy">--}%
		%{--<g:message code="masterPanel.updatedBy.label" default="Updated By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="updatedBy" value="${masterPanelInstance?.updatedBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: masterPanelInstance, field: 'lastUpdProcess', 'error')} ">--}%
	%{--<label class="control-label" for="lastUpdProcess">--}%
		%{--<g:message code="masterPanel.lastUpdProcess.label" default="Last Upd Process" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="lastUpdProcess" value="${masterPanelInstance?.lastUpdProcess}"/>--}%
	%{--</div>--}%
%{--</div>--}%

