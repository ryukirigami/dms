
<%@ page import="com.kombos.finance.MappingJournal" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="mappingJournal_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="mappingJournal.mappingCode.label" default="Mapping Code" /></div>
			</th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="mappingJournal.mappingName.label" default="Mapping Name" /></div>
			</th>

            %{--tester--}%
            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="mappingJournal.mappingName.label" default="Description" /></div>
            </th>
            %{--tester--}%
        </tr>
		<tr>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_mappingCode" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_mappingCode" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_mappingName" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_mappingName" class="search_init" />
				</div>
			</th>
            %{--tester--}%
            <th style="border-top: none;padding: 5px;">
                <div id="filter_description" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_description" class="search_init" />
                </div>
            </th>
            %{--tester--}%
		</tr>
	</thead>
</table>

<g:javascript>
var mappingJournalTable;
var reloadMappingJournalTable;
$(function(){
	
	reloadMappingJournalTable = function() {
		mappingJournalTable.fnDraw();
	}

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	mappingJournalTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	mappingJournalTable = $('#mappingJournal_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
	"sName": "mappingCode",
	"mDataProp": "mappingCode",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true,
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="edit('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	}
}
,

{
	"sName": "mappingName",
	"mDataProp": "mappingName",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "description",
	"mDataProp": "description",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var mappingCode = $('#filter_mappingCode input').val();
						if(mappingCode){
							aoData.push(
									{"name": 'sCriteria_mappingCode', "value": mappingCode}
							);
						}

						var mappingName = $('#filter_mappingName input').val();
						if(mappingName){
							aoData.push(
									{"name": 'sCriteria_mappingName', "value": mappingName}
							);
						}

                        var description = $('#filter_description input').val();
						if(description){
							aoData.push(
									{"name": 'sCriteria_description', "value": description}
							);
						}


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
