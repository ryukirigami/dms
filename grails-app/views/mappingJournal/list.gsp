
<%@ page import="com.kombos.finance.MappingJournal" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'mappingJournal.label', default: 'MappingJournal')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			var addDetailRow;
			var showDetail;
			var showModalAccount;
			var putAccount;
			var performSave;
			var performDeletion;
			var refresh;
			$(function(){
				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							/*shrinkTableLayout('mappingJournal');*/
							var table = $('#mappingJournal-table');
		                    table.fadeOut();
                            $('#mappingJournal-form').css("display","block");
                            $('#mappingJournalDetail-table').css("display","block");
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('mappingJournal', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('mappingJournal', '${request.contextPath}/mappingJournal/massdelete', reloadMappingJournalTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});
				show = function(id) {
					showInstance('mappingJournal','${request.contextPath}/mappingJournal/show/'+id);
				};
				edit = function(id) {
					//editInstance('mappingJournal','${request.contextPath}/mappingJournal/edit/'+id);
					var table = $('#mappingJournal-table');
                    table.fadeOut();
                    $('#mappingJournal-form').css("display","block");
                    $('#mappingJournalDetail-table').css("display","block");
                    jQuery('#spinner').fadeIn(1);
                    $.get('${request.getContextPath()}/mappingJournal/edit/' + id, function(data) {
                        loadFormInstance('mappingJournal', data);

                    }).done(function() {
                        jQuery('#spinner').fadeOut();
                    });
				};
				addDetailRow = function() {
				    var $tbl = $("#mappingJournalDetail-table table tbody");
				    var count = new Number(0);

				    if ($tbl.html().length > 20) {
				        count = new Number($tbl.find('tr:last-child').attr("id").split("_")[1]);
				    }
				    var colDetailId = '<td id="detailid_'+(count + 1)+'" style="display:none;"></td>',
				        colNomor = '<td id="nomorurut_'+(count + 1)+'">'+(count + 1)+' <a href="javascript:void(0);" onClick="performDeletion('+(count+1)+');" style="magin: 5px;"><i class="icon-trash"></i></a></td>',
				        colAkunId = '<td style="display:none;" id="accountnumberid_'+(count+1)+'"></td>',
				        colNomorRekening = '<td>' +'<input id="nomorrekening_'+(count + 1)+'" type="text" readOnly><a href="javascript:void(0);" onClick="showModalAccount('+(count+1)+');" style="margin: 5px;">' +'<i class="icon-search"></i></a></td>',
				        colNamaRekening = '<td id="namarekening_'+(count+1)+'"></td>',
                        colTipeTransaksi = '<td><select id="tipetransaksi_'+(count + 1)+'"><option value="Debet">Debet</option><option value="Kredit">Kredit</option></select></td>';


                    $tbl.append('<tr id="row_'+(count+1)+'">'+colDetailId + colNomor + colAkunId + colNomorRekening + colNamaRekening + colTipeTransaksi +'</tr>');
				};

				showModalAccount = function(idx) {
                    $("#popupAccountNumberContent").empty();
                    $.ajax({type:'POST', url:'${request.contextPath}/mappingJurnalModal',
                        success:function(data,textStatus){
                                $("#popupAccountNumberContent").html(data);
                                $("#popupAccountNumberModal").modal({
                                    "backdrop" : "dynamic",
                                    "keyboard" : true,
                                    "show" : true
                                }).css({'width': '1000px','margin-left': function () {return -($(this).width() / 2);}});

                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
                    $("#idx").val(idx);
				};

				putAccount = function() {
				    var idx = $('#idx').val();
                   $('#accountNumber-table tbody').find('tr').each(function(k,v) {
				       if ($(this).find('td:first-child input[type="radio"]').is(":checked")) {
                            var aData = accountNumberTable.fnGetData(k);
				            var accountId = aData.id;
				            var accountNumber = aData.accountNumber;
				            var accountName = aData.accountName;
				            $('#accountnumberid_'+idx).html(accountId);
                            $('#nomorrekening_'+idx).val(accountNumber);
                            $('#namarekening_' +idx).html(accountName);
				            toastr.success("Data Added");
                       }
				    });
				}

                performSave = function(id) {
                    var jsonObj = [];
                    var mappingCode = $('#mappingCode').val();
                    var mappingName = $('#mappingName').val();
                    var count = 1;
                    if (mappingCode && mappingName) {
                        $("#mappingJournalDetail-table table tbody").find('tr').each(function(k,v) {
                           count = $(this).attr("id").split("_")[1];
                           var map = {};
                           var idAkun = $(this).find('td#accountnumberid_' + count).html().trim();
                           var tipeTransaksi = $(this).find('td select#tipetransaksi_' + count).val();
                           var detailId = null;
                           if (id) {
                                 detailId = $(this).find('td#detailid_' + count).html().trim();
                                 if (detailId.length > 0)
                                    map['detailId'] = detailId;
                           }
                           map['id'] = idAkun;
                           map['accountTypeTransaction'] = tipeTransaksi;
                           map['nomorUrut'] = count;
                           jsonObj.push(map);
                           count++;
                        });
                        var action = id?"update":"save";
                        $.post('${request.contextPath}/mappingJournal/'+action+'/', {
                            id: id?id:null,
                            mappingCode: mappingCode,
                            mappingName: mappingName,
                            mappingDetails: JSON.stringify(jsonObj)
                        }, function(data) {
                            refresh();
                        });
                    }
                };

                refresh = function() {
                    var table = $('#mappingJournal-table');
		                    table.fadeIn('fast');
                            $('#mappingJournal-form').fadeOut('fast')
                            $('#mappingJournalDetail-table').fadeOut('fast')
                            reloadMappingJournalTable();
                }

                performDeletion = function(rowid, detailid, mappingjournalid) {
                    var sure = confirm("Are you sure?");
                    if (sure) {
                        if (mappingjournalid && detailid) {
                        jQuery('#spinner').fadeIn(1);
                        $.post('${request.getContextPath()}/mappingJournal/removeDetail', {
                            detailid: detailid,
                            mappingJournalId: mappingjournalid
                        }, function(data) {
                        }).done(function() {
                            jQuery('#spinner').fadeOut(1);
                        });
                    }
                    $('#row_' + rowid).remove();
                    }
                };

});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="mappingJournal-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <input type="hidden" name="idx" id="idx" />
			<g:render template="dataTables" />
		</div>
		<div class="span10" id="mappingJournal-form" style="display: none;"></div>
	</div>

    <div id="popupAccountNumberModal" class="modal fade">
        <div class="modal-dialog" style="width: 1000px;">
            <div class="modal-content" style="width: 1000px;">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 480px;">
                    %{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}%
                    <div id="popupAccountNumberContent">
                        <div class="iu-content"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="control-group">
                        <a href="javascript:void(0);" id="btnPilihKaryawan" onclick="putAccount();" class="btn btn-primary">
                            Save
                        </a>
                        <a href="javascript:void(0);" class="btn cancel" data-dismiss="modal">
                            Close
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
