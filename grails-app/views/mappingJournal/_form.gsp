<%@ page import="com.kombos.finance.MappingJournal" %>

<div class="control-group fieldcontain ${hasErrors(bean: mappingJournalInstance, field: 'mappingCode', 'error')} ">
	<label class="control-label" for="mappingCode">
		<g:message code="mappingJournal.mappingCode.label" default="Mapping Code" />
		
	</label>
	<div class="controls">
	<g:textField name="mappingCode" value="${mappingJournalInstance?.mappingCode}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: mappingJournalInstance, field: 'mappingName', 'error')} ">
	<label class="control-label" for="mappingName">
		<g:message code="mappingJournal.mappingName.label" default="Mapping Name" />
		
	</label>
	<div class="controls">
	<g:textField name="mappingName" value="${mappingJournalInstance?.mappingName}" />
	</div>
</div>

%{--TESTER--}%
<div class="control-group fieldcontain ${hasErrors(bean: mappingJournalInstance, field: 'description', 'error')} ">
    <label class="control-label" for="description">
        <g:message code="mappingJournal.mappingName.label" default="Description" />

    </label>
    <div class="controls">
        <g:textArea name="description" value="${mappingJournalInstance?.description}" />
    </div>
</div>
%{--TESTER--}%

<div class="span12" id="mappingJournalDetail-table" style="display: block; margin-top: 20px;">
    <legend>Journal Memorial Detail</legend>
    <table class="table table-bordered">
        <thead>
        <th style="display: none;">Detail ID</th>
        <th>No</th><th style="display: none;">Id Akun</th><th>Nomor Rekening</th>
        <th>Nama Rekening</th><th>Tipe Transaksi</th>
        </thead>
        <tbody>
        <g:if test="${dataDetail}">
            <g:each in="${dataDetail}" status="i" var="detail">
                <tr id="row_${i+1}">
                    <td id="detailid_${i +1}" style="display: none;">
                        ${detail.id}
                    </td>
                    <td id="nomorurut_${i +1}">
                        ${i+1}
                                                             %{-- rowid, detailid--}%
                        <a href="javascript:void(0);" onClick="performDeletion(${i +1},${detail.id},${mappingJournalInstance.id});" style="margin: 5px;">
                            <i class="icon-trash"></i>
                        </a>
                    </td>
                    <td id="accountnumberid_${i +1}" style="display: none;">
                        ${detail.accountNumber.id}
                    </td>
                    <td>
                        <input id="nomorrekening_${i}" type="text" readonly="readonly" value="${detail.accountNumber.accountNumber}">
                        <a style="margin: 5px;" onclick="showModalAccount(${detail.nomorUrut});" href="javascript:void(0);">
                            <i class="icon-search"></i>
                        </a>
                    </td>
                    <td id="namarekening_${i +1}">
                        ${detail?.accountNumber?.accountName}
                    </td>
                    <td>
                        <select id="tipetransaksi_${i +1}">
                            %{--<option value="${detail?.accountTransactionType}">${detail?.accountTransactionType}</option>--}%
                            %{--<option value="${detail?.accountTransactionType?.equals("Kredit")?"Debet": "Kredit"}">--}%
                                %{--${detail.accountTransactionType.equals("Kredit")?"Debet": "Kredit"}--}%
                            %{--</option>--}%
                            
                            <option value="${detail?.accountTransactionType}">${detail?.accountTransactionType}</option>--}%
                            <option value="${detail?.accountTransactionType?.equals("Debet")?"Kredit": "Debet"}">
                                ${detail.accountTransactionType.equals("Debet")?"Kredit": "Debet"}
                            </option>
                        </select>
                    </td>
                </tr>
            </g:each>
        </g:if>
        </tbody>
    </table>
</div>

