<%@ page import="com.kombos.hrd.TrainingClassRoom; com.kombos.hrd.TrainingMember; com.kombos.hrd.TrainingInstructor; com.kombos.administrasi.Divisi; com.kombos.administrasi.CompanyDealer" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <style>
    .form-horizontal input + .help-block, .form-horizontal select + .help-block, .form-horizontal textarea + .help-block, .form-horizontal .uneditable-input + .help-block, .form-horizontal .input-prepend + .help-block, .form-horizontal .input-append + .help-block {
        font-size: 10px !important;
        font-style: italic !important;
        margin-top: 6px !important;
    }
    .modal.fade.in {
        left: 25.5% !important;
        width: 1205px !important;
    }
    .modal.fade {
        top: -100%;
    }
    </style>
    <r:require modules="baseapplayout" />
    <g:javascript disappointmentGrition="head">
	$(function(){
        $("#btnCariKaryawan").click(function() {
            oFormService.fnShowKaryawanDataTable();
        })
        oFormService = {
            fnShowKaryawanDataTable: function() {
                $("#karyawanModal").modal("show");
                $("#karyawanModal").fadeIn();
            },
            fnHideKaryawanDataTable: function() {
                $("#karyawanModal").modal("hide");
            }
        }
            $('#divkaryawan').hide();
            $('#divdivisi').hide()
            $('#divperiode1').hide();
            $('#divperiode2').hide();
            $('#filter_periode').hide();
            $('#filter_periode2').hide();
            $('#namaInstruktur').hide();

        $('#namaReport').change(function(){
            $('#divkaryawan').hide();
            $('#workshop').show();
            $('#divdivisi').hide()
            $('#divperiode1').hide();
            $('#divperiode2').hide();
            $('#filter_periode').hide();
            $('#filter_periode2').hide();
            $('#namaInstruktur').hide();

             if($('#namaReport').val()=="01" ){
                $('#divkaryawan').show();
             }else if($('#namaReport').val()=="02"){
                $('#divdivisi').show()
                $('#divperiode2').show()
                 $('#namaTraining').hide();
             }else if($('#namaReport').val()=="04"){
                $('#divperiode1').show()
                $('#filter_periode').show();
                 $('#namaTraining').hide();
             }else if($('#namaReport').val()=="05"){
                $('#divperiode1').show();
                $('#filter_periode2').show();
                 $('#namaTraining').hide();
             }else if($('#namaReport').val()=="06"){
                $('#divperiode1').show();
                $('#filter_periode2').show();
                $('#namaTraining').hide();
             }else if($('#namaReport').val()=="10"){
                $('#divperiode1').show();
                $('#filter_periode').show();
                $('#namaTraining').hide();
             }else if($('#namaReport').val()=="11" ){
                $('#divperiode1').show();
                $('#filter_periode2').show();
             }else if($('#namaReport').val()=="12" || $('#namaReport').val()=="14" || $('#namaReport').val()=="16" || $('#namaReport').val()=="17"){
                $('#divperiode1').show();
                $('#filter_periode').show();
                 $('#namaTraining').hide();
             }else if($('#namaReport').val()=="13"){
                $('#divkaryawan').show();
                $('#divperiode1').show();
                $('#filter_periode').show();
                 $('#namaTraining').hide();
             }else if($('#namaReport').val()=="15"){
                $('#divkaryawan').show();
                $('#divperiode1').show();
                $('#filter_periode').show();
                 $('#namaTraining').hide();
             }else if($('#namaReport').val()=="18"){
                $('#divperiode1').show();
                $('#filter_periode2').show();
                 $('#namaTraining').hide();
             }else if($('#namaReport').val()=="19"){
                $('#divperiode1').show();
                $('#filter_periode').show();
                $('#namaInstruktur').show();
                $('#namaTraining').hide();
             }else if($('#namaReport').val()=="20"){
                $('#divperiode1').show();
                $('#filter_periode').show();
                $('#namaTraining').show()
             }else{}
        });

	    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    });
    var checkin = $('#search_tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
                checkin.hide();
                $('#search_tanggal2')[0].focus();
            }).data('datepicker');

    var checkout = $('#search_tanggal2').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
                checkout.hide();
    }).data('datepicker');

     previewData = function(){
            var namaReport = $('#namaReport').val();
            var workshop = $('#workshop').val();
            var search_tanggal = $('#search_tanggal').val();
            var search_tanggal2 = $('#search_tanggal2').val();
            var bulan = $('#bulan').val();
            var tahunperiode = $('#txtperiode').val();
            var divisi = $("#selectdivisi").val();
            var tahun = $('#tahun').val();
            var namaInstrukture = $('#selectinstruktur').val();
            var namaTraininge = $('#selecttraining').val();
            var training = $('#training_id').val();
            var detail = "";
            var karyawan = $("#karyawan_id").val();
            var format = $('input[name="formatReport"]:checked').val();
            if(namaReport == "" || namaReport == null){
                alert('Harap Isi Data Dengan Lengkap');
                return;
            }
            if(workshop == "" || workshop == null || workshop == "Pilih Cabang"){
                alert('Worshop belum dipilih!');
                return;
            }
            if(karyawan == "" || karyawan == null){
                alert('Belum Pilih Karyawan!');
                return;
            }
           window.location = "${request.contextPath}/reportHRD/previewData?namaReport="+namaReport+"&workshop="+workshop+"&bulan="+bulan+"&tahun="+tahun+"&tanggal="+search_tanggal+"&tanggal2="+search_tanggal2+"&detail="+detail+"&tahunperiode="+tahunperiode+"&divisi="+divisi+"&selectinstruktur="+namaInstrukture+"&karyawan.id="+karyawan+"&selecttraining="+namaTraininge+"&format="+format;
    }

    </g:javascript>

</head>
<body>
<div class="navbar box-header no-border">
    Report – HRD

</div><br>
<div class="box">
    <div class="span12" id="appointmentGr-table">
        <div id="form-appointmentGr" class="form-horizontal">
            <fieldset>
                <div class="control-group">
                    <label class="control-label" for="workshop" style="text-align: left;">
                        Format File
                    </label>
                    <div id="filter_format" class="controls">
                        <g:radioGroup name="formatReport" id="formatReport" values="['x','p']" labels="['Excel','PDF']" value="x" required="" >
                            ${it.radio} ${it.label}
                        </g:radioGroup>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="workshop" style="text-align: left;">
                        Workshop
                    </label>
                    <div id="filter_wo" class="controls">
                        <g:if test="${dealer.toString().contains("HO")}">
                            <g:select name="workshop" id="workshop" from="${CompanyDealer.createCriteria().list{eq('staDel','0');order('m011NamaWorkshop')}}" optionKey="id" optionValue="m011NamaWorkshop" style="width: 44%" noSelection="['':'Pilih Cabang']" />
                        </g:if>
                        <g:else>
                            ${dealer}
                            <g:hiddenField name="dealer" id="workshop" value="${session.userCompanyDealerId}" />
                        </g:else>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="filter_nama" style="text-align: left;">
                        Nama Report
                    </label>
                    <div id="filter_nama" class="controls">
                        <select name="namaReport" id="namaReport" size="17" style="width: 44%; font-size: 12px;">
                            <option value="01">01. Biodata Karyawan</option>
                            <option value="02">02. Kondite Karyawan</option>
                            <option value="03">03. Data Karyawan Per Cabang</option>
                            <option value="04">04. Data Karyawan Keluar,Masuk, Mutasi</option>
                            <option value="05">05. Evaluasi Karyawan</option>
                            <option value="06">06. Daftar Hadir Karyawan</option>
                            <option value="11">07. Fasility</option>
                            <option value="12">08. Rekap Training History</option>
                            <option value="13">09. Training History Karyawan</option>
                            <option value="14">10. Rekap History Sertifikasi</option>
                            <option value="15">11. Sertifikasi History Karyawan</option>
                            <option value="16">12. History Kelas Training</option>
                            <option value="17">13. History Sertifikasi</option>
                            <option value="18">14. Daftar Instruktur </option>
                            <option value="19">15. Detail Instruktur </option>
                            <option value="20">16. Detail Sertifikasi </option>
                        </select>
                    </div>
                </div>
                <div class="control-group" id="divkaryawan"style="display: none">
                    <label class="control-label" for="filter_nama" style="text-align: left;">
                        Karyawan
                    </label>
                    <div id="filter_divisi" class="controls">
                        <g:textField name="karyawan.name" id="karyawan_nama" readonly="" />
                        <span class="add-on">
                            <a href="javascript:void(0);" id="btnCariKaryawan">
                                <i class="icon-search"/>
                            </a>
                        </span>
                        <g:hiddenField name="karyawan.id" id="karyawan_id" />
                    </div>
                </div>
                <div class="control-group" id="divdivisi"style="display: none">
                    <label class="control-label" for="filter_nama" style="text-align: left;">
                        Divisi/Bagian
                    </label>
                    <div id="filter_karyawan" class="controls">
                        <g:select name="selectdivisi" id="selectdivisi" from="${Divisi.createCriteria().list {eq("staDel","0");eq("companyDealer",session.userCompanyDealer);order("m012NamaDivisi")}}" optionKey="id" optionValue="m012NamaDivisi" />
                    </div>
                </div>
                <div class="control-group" id="namaInstruktur"style="display: none">
                    <label class="control-label" for="filter_nama" style="text-align: left;">
                        Nama Instruktur
                    </label>
                    <div id="filter_instruktur" class="controls">
                        <g:select name="selectinstruktur" id="selectinstruktur" from="${TrainingInstructor.createCriteria().list {eq("staDel","0");order("namaInstruktur")}}" optionKey="id" optionValue="namaInstruktur" />
                    </div>
                </div>

                <div class="control-group" id="namaTraining"style="display: none">
                    <label class="control-label" for="filter_nama" style="text-align: left;">
                        Nama Training
                    </label>
                    <div id="filter_training" class="controls">
                        <g:select name="selecttraining" id="selecttraining" from="${TrainingClassRoom?.createCriteria().list {eq("staDel","0");order("namaTraining")}}" optionKey="id" optionValue="namaTraining" />
                    </div>
                </div>


                <div id="divperiode1" class="control-group">
                    <label class="control-label" for="filter_periode" style="text-align: left;">
                        Periode
                    </label>
                    <div id="filter_periode" class="controls">
                        <ba:datePicker id="search_tanggal" name="search_tanggal" precision="day" format="dd-MM-yyyy"  value="${new Date()}" />
                        &nbsp;&nbsp;s.d.&nbsp;&nbsp;
                        <ba:datePicker id="search_tanggal2" name="search_tanggal2" precision="day" format="dd-MM-yyyy"  value="${new Date()+1}"  />
                    </div>
                    <div id="filter_periode2" class="controls">
                        %{
                            def listBulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
                        }%
                        <select name="bulan" id="bulan" style="width: 170px">
                            %{
                                for(int i=0;i<12;i++){
                                    out.print('<option value="'+i+'">'+listBulan.get(i)+'</option>');
                                }
                            }%
                        </select>
                        <select name="tahun" id="tahun" style="width: 106px">
                            %{
                                for(def a = (new Date().format("yyyy")).toInteger();a >= 2000 ;a--){
                                    out.print('<option value="'+a+'">'+a+'</option>');
                                }
                            }%

                        </select>
                    </div>
                </div>
                <div id="divperiode2" class="control-group">
                    <label class="control-label" for="filter_periode" style="text-align: left;">
                        Periode
                    </label>
                    <div id="filter_tahunperiode" class="controls">
                        <g:textField name="txtperiode" id="txtperiode" maxlength="4" />
                    </div>
                </div>
            </fieldset>
        </div>
        <g:field type="button" onclick="previewData()" class="btn btn-primary create" name="preview"  value="${message(code: 'default.preview.label', default: 'Preview')}" />
        <g:field type="button" onclick="window.location.replace('#/home')" class="btn btn-cancel cancel" name="cancel"  value="${message(code: 'default.button.close.label', default: 'Close')}" />
    </div>
</div>
<div id="karyawanModal" class="modal fade">
    <div class="modal-dialog" style="width: 1200px;">
        <div class="modal-content" style="width: 1200px;">
            <div class="modal-header">
                <a class="close" href="javascript:void(0);" data-dismiss="modal">×</a>
                <h4>Data Karyawan - List</h4>
            </div>
            <!-- dialog body -->
            <div class="modal-body" id="karyawanModal-body" style="max-height: 1200px;">
                <div class="box">
                    <div class="span12">
                        <fieldset>
                            <table>
                                <tr style="display:table-row;">
                                    <td style="width: 130px; display:table-cell; padding:5px;">
                                        <label class="control-label" for="sCriteria_nama">Nama Karyawan</label>
                                    </td>
                                    <td style="width: 130px; display:table-cell; padding:5px;">
                                        <input type="text" id="sCriteria_nama">
                                    </td>
                                    <td >
                                        <a id="btnSearchKaryawan" class="btn btn-primary" href="javascript:void(0);">
                                            Cari
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                </div>
                <g:render template="dataTablesKaryawan" />
            </div>

            <div class="modal-footer">
                <a href="javascript:void(0);" class="btn cancel" onclick="oFormService.fnHideKaryawanDataTable();">
                    Tutup
                </a>
                <a href="javascript:void(0);" id="btnPilihKaryawan" class="btn btn-primary">
                    Pilih Karyawan
                </a>
            </div>
        </div>
    </div>
</div>
</body>
</html>
