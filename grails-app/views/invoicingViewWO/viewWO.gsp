
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'delivery.invoicing.label', default: 'Delivery - Invoicing')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>
        var cekGeInv;
        var loadForm;

        var checkin = $('#tanggalWO_start').datepicker({
            onRender: function(date) {
                return '';
            }
        }).on('changeDate', function(ev) {
                    var newDate = new Date(ev.date)
                    newDate.setDate(newDate.getDate() + 1);
                    checkout.setValue(newDate);
                    checkin.hide();
                    $('#tanggalWO_end')[0].focus();
                }).data('datepicker');

        var checkout = $('#tanggalWO_end').datepicker({
            onRender: function(date) {
                return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
                    checkout.hide();
                }).data('datepicker');

        $(function(){
            cekGeInv = function(){
				    var checkID =[];
                    $('#wo-table tbody .row-select').each(function() {
                        if(this.checked){
                            var id = $(this).next("input:hidden").val();
                            checkID.push(id);
                        }
                    });

                    if(checkID.length!=1){
                        if(checkID.length<1){
                            alert('Pilih salah satu data untuk melihat WO (Sudah JOC)');
                        }else{
                            alert('Anda hanya boleh memilih 1 data');
                        }
                        return;
                    }else{
                        var idUbah=checkID[0];
                        $.ajax({url:'${request.contextPath}/invoicingGenerateInvoices/generateInvoices?idUbah='+idUbah,
                            type:"GET", datatype: "html",
   			                complete : function(req,err){
   			                $('#main-content').html(req.responseText);
   				            $('#spinner').fadeOut();
   				            loading = false;
   			                }
   		                });
                    }
            }
		});


    </g:javascript>
</head>

<body>
    <div class="navbar box-header no-border">
        <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
    </div>
    <div>&nbsp;</div>
    <div class="box">

        <!-- Start Tab -->
        <ul class="nav nav-tabs" style="margin-bottom: 0;">
            <li class="active"><a href="#">View WO (Sudah JOC)</a></li>
            <li><a href="javascript:loadPath('invoicingGenerateInvoices/generateInvoices');">Generate Invoices</a></li>
        </ul>
        <!-- End Tab -->

        <!-- Start Kriteria Search -->
        <div class="box" style="padding-top: 5px; padding-left: 10px;">
            <div class="span12">
                <legend style="font-size: small;">
                    <g:message code="delivery.invoicing.kriteria.search.label" default="Kriteria Search WO (Sudah JOC dan Washing)"/>
                </legend>
                <g:if test="${flash.message}">
                    <div class="alert alert-error">
                        ${flash.message}
                    </div>
                </g:if>
            </div>
            <div class="span12" id="ivwo-table" style="padding-left: 0;">
                <table width="95%;">
                    <tr align="left">
                        <td align="left" width="100 px;">
                            <label class="control-label">
                                <g:message code="delivery.invoicing.tanggal.wo.label" default="Tanggal WO"/>
                            </label>
                        </td>
                        <td align="left" width="25 px;">
                            <ba:datePicker id="tanggalWO_start" name="tanggalWO_start" precision="day" format="dd/MM/yyyy"  value="" />
                        </td>
                        <td align="left" width="5 px;">
                            <label class="control-label">
                                &nbsp;<g:message code="delivery.invoicing.until.label" default="s/d"/>&nbsp;
                            </label>
                        </td>
                        <td align="left" width="25 px;">
                            <ba:datePicker id="tanggalWO_end" name="tanggalWO_end" precision="day" format="dd/MM/yyyy"  value="" />
                        </td>
                        <td>
                            &nbsp;
                            <g:field type="button" style="width: 90px" class="btn btn-primary search" onclick="reloadViewWOTable();"
                                     name="search" id="search" value="${message(code: 'default.search.label', default: 'Search')}" />
                        </td>
                    </tr>
                    <tr align="left">
                        <td align="left">
                            <label class="control-label">
                                <g:message code="delivery.invoicing.kategori.label" default="Kategori"/>
                            </label>
                        </td>
                        <td align="left" colspan="3">
                            <g:select style="width:100%" name="kategoriSelect" id="kategoriSelect" from="${['nomorWO': 'NOMOR WO','noPol': 'NOMOR POLISI']}"
                                      optionKey="key" optionValue="value"
                                      noSelection="['null': '[Pilih Kategori]']"
                            />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr align="left">
                        <td align="left">
                            <label class="control-label">
                                <g:message code="delivery.invoicing.keyWord.label" default="Kata Kunci"/>
                            </label>
                        </td>
                        <td align="left" colspan="3">
                            <g:textField name="keyword" id="keyword" style="width:97%"/>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </div>
        </div>
        <!-- End Kriteria Search -->

        <!-- Start Kontent -->
        <div class="box" style="padding-top: 5px; padding-left: 10px;">
            <div class="span12" style="height: 40px;">
                <legend style="font-size: small;">
                    <g:message code="delivery.invoicing.wo.list.label" default="WO Sudah JOC dan Washing"/>
                </legend>
            </div>
            <div class="span12" id="wo-table" style="padding-left: 0; margin-left: 0;">
                <g:render template="woDataTables"/>
                <br/>
                <g:field type="button" style="width: 170px" class="btn btn-primary search" onclick="return cekGeInv();"
                         name="generate" id="generate" value="${message(code: 'default.generate.label', default: 'Generate Invoice + Part Slip')}" />
            </div>
            <div class="span12" id="generateInv-form" style="display: none;margin-left: 0;"></div>
        </div>
        <!-- End Kontent -->
    </div>
</body>
</html>