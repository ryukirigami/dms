
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>
<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</g:javascript>

<table id="viewWO_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="100%">
    <thead>
    <tr>
        <th style="border-bottom: none;padding: 5px;">
            <g:message code="wo.tanggalWO.label" default="Tanggal WO"/>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <g:message code="wo.t401NoWo.label" default="Nomor WO"/>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <g:message code="wo.noPolisi.label" default="Nomor Polisi"/>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <g:message code="wo.namaCustomer.label" default="Nama Customer"/>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <g:message code="wo.kategoriJob.label" default="Tipe"/>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <g:message code="wo.statusInv.label" default="Status Inv"/>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
var ViewWOTable;
var reloadViewWOTable;
$(function(){
	
	reloadViewWOTable = function() {
		ViewWOTable.fnDraw();
	}

    var recordsviewWOperpage = [];
    var anViewWOSelected;
    var jmlRecViewWOPerPage=0;
    var id;

	ViewWOTable = $('#viewWO_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsViewWO = $("#viewWO_datatables tbody .row-select");
            var jmlViewWOCek = 0;
            var nRow;
            var idRec;
            rsViewWO.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsviewWOperpage[idRec]=="1"){
                    jmlViewWOCek = jmlViewWOCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsviewWOperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecViewWOPerPage = rsViewWO.length;
            if(jmlViewWOCek==jmlRecViewWOPerPage && jmlRecViewWOPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
	"sName": "tanggalWO",
	"mDataProp": "tanggalWO",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
	    return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSortable": false,
	"sWidth":"125px",
	"bVisible": true
},
{
	"sName": "noWo",
	"mDataProp": "noWo",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"230px",
	"bVisible": true
},
{
	"sName": "noPolisi",
	"mDataProp": "noPolisi",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"95px",
	"bVisible": true
},
{
	"sName": "namaCustomer",
	"mDataProp":"namaCustomer",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "kategoriJob",
	"mDataProp": "kategoriJob",
	"aTargets": [4],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "statusInv",
	"mDataProp": "statusInv",
	"aTargets": [5],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        var Tanggal = $('#tanggalWO_start').val();

                        if(Tanggal){
                            aoData.push(
                                    {"name": 'sCriteria_Tanggal', "value": Tanggal}
                            );
                        }

                        var Tanggalakhir = $('#tanggalWO_end').val();

                        if(Tanggalakhir){
                            aoData.push(
                                    {"name": 'sCriteria_Tanggalakhir', "value": Tanggalakhir}
                            );
                        }

                        var column = $('#kategoriSelect').val();
						if(column){
							var val = $('#keyword').val();
							if(val){
								aoData.push(
										{"name": 'sCriteria_column', "value": column}
								);
								aoData.push(
										{"name": 'sCriteria_value', "value": val}
								);
							}
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
    $('.select-all').click(function(e) {
        $("#viewWO_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsviewWOperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsviewWOperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

    $('#viewWO_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsviewWOperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anViewWOSelected = ViewWOTable.$('tr.row_selected');
            if(jmlRecViewWOPerPage == anViewWOSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsviewWOperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
