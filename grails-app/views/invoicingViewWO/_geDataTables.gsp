
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="genInvoicePopUp_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 130px"><g:message code="genInvoice.noInv.label" default="No. Invoice" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 125px"><g:message code="genInvoice.invReff.label" default="Inv. Ref" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 115px"><g:message code="genInvoice.partslip.label" default="Part Slip" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 110px"><g:message code="genInvoice.tanggal.label" default="Tanggal" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="genInvoice.nomorWO.label" default="Nomor WO" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="genInvoice.kepada.label" default="Kepada" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="genInvoice.alamat.label" default="Alamat" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="genInvoice.npwp.label" default="NPWP" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="genInvoice.tipe.label" default="Tipe" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="genInvoice.jenis.label" default="Jenis" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="genInvoice.warranty.label" default="Warranty" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="genInvoice.jasalabor.label" default="Jasa/Labor" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="genInvoice.sublet.label" default="Sublet" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="genInvoice.parts.label" default="Parts" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="genInvoice.oil.label" default="Oil" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="genInvoice.subTotal.label" default="Sub Total" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="genInvoice.Discount.label" default="Discount" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="genInvoice.subTotalStlhDisc.label" default="Sub Total Stlh Disc" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="genInvoice.ppn.label" default="PPN 10%" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="genInvoice.totalInv.label" default="Materai" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="genInvoice.t921TglKasusDiterima.label" default="Total Invoice" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="genInvoice.bf.label" default="BF" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="genInvoice.onRisk.label" default="On Risk" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div style="width: 120px"><g:message code="genInvoice.tobar.label" default="Total Yang Dibayar" /></div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var genInvoicePopUpTable;
var reloadComplaintPopUpTable;

function showDetailOnList(id){
    $("#dialog").modal('hide');
    $('#search_id').val(id);
    expandTableLayout();
    $('#search_id').val('');
}

$(function(){
	
	reloadComplaintPopUpTable = function() {
		genInvoicePopUpTable.fnDraw();
	}


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	genInvoicePopUpTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	$('#clear').click(function(e){
        $('#search_t921TglComplainPopUp').val("");
        $('#search_t921TglComplainPopUp_day').val("");
        $('#search_t921TglComplainPopUp_month').val("");
        $('#search_t921TglComplainPopUp_year').val("");
        $('#search_t921TglComplainAkhirPopUp').val("");
        $('#search_t921TglComplainAkhirPopUp_day').val("");
        $('#search_t921TglComplainAkhirPopUp_month').val("");
        $('#search_t921TglComplainAkhirPopUp_year').val("");
    });

    $('#viewPopUp').click(function(){
        genInvoicePopUpTable.fnDraw();
	});


	genInvoicePopUpTable = $('#genInvoicePopUp_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "t921TglComplain",
	"mDataProp": "t921TglComplain",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '&nbsp;&nbsp;<a href="#" onclick="showDetailOnList('+row['id']+');">'+data+'</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t921NoReff",
	"mDataProp": "t921NoReff",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "partslip",
	"mDataProp": "partslip",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "tanggal",
	"mDataProp": "tanggal",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "nomorWO",
	"mDataProp": "nomorWO",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "kepada",
	"mDataProp": "kepada",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "alamat",
	"mDataProp": "alamat",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "npwp",
	"mDataProp": "npwp",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "tipe",
	"mDataProp": "tipe",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "jenis",
	"mDataProp": "jenis",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "warranty",
	"mDataProp": "warranty",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "jasalabor",
	"mDataProp": "jasalabor",
	"aTargets": [11],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "sublet",
	"mDataProp": "sublet",
	"aTargets": [12],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "parts",
	"mDataProp": "parts",
	"aTargets": [13],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "oil",
	"mDataProp": "oil",
	"aTargets": [14],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "subTotal",
	"mDataProp": "subTotal",
	"aTargets": [15],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "Discount",
	"mDataProp": "Discount",
	"aTargets": [16],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "subTotalStlhDisc",
	"mDataProp": "subTotalStlhDisc",
	"aTargets": [17],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "ppn",
	"mDataProp": "ppn",
	"aTargets": [18],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "totalInv",
	"mDataProp": "totalInv",
	"aTargets": [19],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t921TglKasusDiterima",
	"mDataProp": "t921TglKasusDiterima",
	"aTargets": [20],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "bf",
	"mDataProp": "bf",
	"aTargets": [21],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "onRisk",
	"mDataProp": "onRisk",
	"aTargets": [22],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "tobar",
	"mDataProp": "tobar",
	"aTargets": [24],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var alamat = $('#filter_alamat input').val();
						if(alamat){
							aoData.push(
									{"name": 'sCriteria_alamat', "value": alamat}
							);
						}

						var t921ID = $('#filter_t921ID input').val();
						if(t921ID){
							aoData.push(
									{"name": 'sCriteria_t921ID', "value": t921ID}
							);
						}

						var t921TglComplain = $('#search_t921TglComplainPopUp').val();
						var t921TglComplainDay = $('#search_t921TglComplainPopUp_day').val();
						var t921TglComplainMonth = $('#search_t921TglComplainPopUp_month').val();
						var t921TglComplainYear = $('#search_t921TglComplainPopUp_year').val();

						if(t921TglComplain){
							aoData.push(
									{"name": 'sCriteria_t921TglComplain', "value": "date.struct"},
									{"name": 'sCriteria_t921TglComplain_dp', "value": t921TglComplain},
									{"name": 'sCriteria_t921TglComplain_day', "value": t921TglComplainDay},
									{"name": 'sCriteria_t921TglComplain_month', "value": t921TglComplainMonth},
									{"name": 'sCriteria_t921TglComplain_year', "value": t921TglComplainYear}
							);
						}

						var t921TglComplainAkhir = $('#search_t921TglComplainAkhirPopUp').val();
						var t921TglComplainAkhirDay = $('#search_t921TglComplainAkhirPopUp_day').val();
						var t921TglComplainAkhirMonth = $('#search_t921TglComplainAkhirPopUp_month').val();
						var t921TglComplainAkhirYear = $('#search_t921TglComplainAkhirPopUp_year').val();

						if(t921TglComplainAkhir){
							aoData.push(
									{"name": 'sCriteria_t921TglComplainAkhir', "value": "date.struct"},
									{"name": 'sCriteria_t921TglComplainAkhir_dp', "value": t921TglComplainAkhir},
									{"name": 'sCriteria_t921TglComplainAkhir_day', "value": t921TglComplainAkhirDay},
									{"name": 'sCriteria_t921TglComplainAkhir_month', "value": t921TglComplainAkhirMonth},
									{"name": 'sCriteria_t921TglComplainAkhir_year', "value": t921TglComplainAkhirYear}
							);
						}

						var npwp = $('#filter_npwp input').val();
						if(npwp){
							aoData.push(
									{"name": 'sCriteria_npwp', "value": npwp}
							);
						}

						var tipe = $('#filter_tipe input').val();
						if(tipe){
							aoData.push(
									{"name": 'sCriteria_tipe', "value": tipe}
							);
						}

						var jenis = $('#filter_jenis input').val();
						if(jenis){
							aoData.push(
									{"name": 'sCriteria_jenis', "value": jenis}
							);
						}

						var warranty = $('#filter_warranty input').val();
						if(warranty){
							aoData.push(
									{"name": 'sCriteria_warranty', "value": warranty}
							);
						}

						var jasalabor = $('#filter_jasalabor input').val();
						if(jasalabor){
							aoData.push(
									{"name": 'sCriteria_jasalabor', "value": jasalabor}
							);
						}

						var sublet = $('#filter_sublet input').val();
						if(sublet){
							aoData.push(
									{"name": 'sCriteria_sublet', "value": sublet}
							);
						}

						var parts = $('#filter_parts input').val();
						if(parts){
							aoData.push(
									{"name": 'sCriteria_parts', "value": parts}
							);
						}

						var oil = $('#filter_oil input').val();
						if(oil){
							aoData.push(
									{"name": 'sCriteria_oil', "value": oil}
							);
						}

						var subTotal = $('#filter_subTotal input').val();
						if(subTotal){
							aoData.push(
									{"name": 'sCriteria_subTotal', "value": subTotal}
							);
						}

						var Discount = $('#filter_Discount input').val();
						if(Discount){
							aoData.push(
									{"name": 'sCriteria_Discount', "value": Discount}
							);
						}

						var subTotalStlhDisc = $('#filter_subTotalStlhDisc input').val();
						if(subTotalStlhDisc){
							aoData.push(
									{"name": 'sCriteria_subTotalStlhDisc', "value": subTotalStlhDisc}
							);
						}

						var ppn = $('#filter_ppn input').val();
						if(ppn){
							aoData.push(
									{"name": 'sCriteria_ppn', "value": ppn}
							);
						}

						var totalInv = $('#filter_totalInv input').val();
						if(totalInv){
							aoData.push(
									{"name": 'sCriteria_totalInv', "value": totalInv}
							);
						}

						var t921NoReff = $('#filter_t921NoReff input').val();
						if(t921NoReff){
							aoData.push(
									{"name": 'sCriteria_t921NoReff', "value": t921NoReff}
							);
						}

						var t921TglKasusDiterima = $('#search_t921TglKasusDiterima').val();
						var t921TglKasusDiterimaDay = $('#search_t921TglKasusDiterima_day').val();
						var t921TglKasusDiterimaMonth = $('#search_t921TglKasusDiterima_month').val();
						var t921TglKasusDiterimaYear = $('#search_t921TglKasusDiterima_year').val();

						if(t921TglKasusDiterima){
							aoData.push(
									{"name": 'sCriteria_t921TglKasusDiterima', "value": "date.struct"},
									{"name": 'sCriteria_t921TglKasusDiterima_dp', "value": t921TglKasusDiterima},
									{"name": 'sCriteria_t921TglKasusDiterima_day', "value": t921TglKasusDiterimaDay},
									{"name": 'sCriteria_t921TglKasusDiterima_month', "value": t921TglKasusDiterimaMonth},
									{"name": 'sCriteria_t921TglKasusDiterima_year', "value": t921TglKasusDiterimaYear}
							);
						}

						var bf = $('#search_bf').val();
						var bfDay = $('#search_bf_day').val();
						var bfMonth = $('#search_bf_month').val();
						var bfYear = $('#search_bf_year').val();

						if(bf){
							aoData.push(
									{"name": 'sCriteria_bf', "value": "date.struct"},
									{"name": 'sCriteria_bf_dp', "value": bf},
									{"name": 'sCriteria_bf_day', "value": bfDay},
									{"name": 'sCriteria_bf_month', "value": bfMonth},
									{"name": 'sCriteria_bf_year', "value": bfYear}
							);
						}

						var partslip = $('#filter_partslip input').val();
						if(partslip){
							aoData.push(
									{"name": 'sCriteria_partslip', "value": partslip}
							);
						}

						var kepada = $('#filter_kepada input').val();
						if(kepada){
							aoData.push(
									{"name": 'sCriteria_kepada', "value": kepada}
							);
						}

						var tanggal = $('#filter_tanggal input').val();
						if(tanggal){
							aoData.push(
									{"name": 'sCriteria_tanggal', "value": tanggal}
							);
						}

						var onRisk = $('#filter_onRisk input').val();
						if(onRisk){
							aoData.push(
									{"name": 'sCriteria_onRisk', "value": onRisk}
							);
						}

						var nomorWO = $('#filter_nomorWO input').val();
						if(nomorWO){
							aoData.push(
									{"name": 'sCriteria_nomorWO', "value": nomorWO}
							);
						}

						var tobar = $('#filter_tobar input').val();
						if(tobar){
							aoData.push(
									{"name": 'sCriteria_tobar', "value": tobar}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
