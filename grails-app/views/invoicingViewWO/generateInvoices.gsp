<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'delivery.invoicing.label', default: 'Delivery - Invoicing')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">

    </g:javascript>
</head>

<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
</div>
<div>&nbsp;</div>

<div class="box">

    <!-- Start Tab -->
    <ul class="nav nav-tabs" style="margin-bottom: 0;">
        <li><a href="javascript:loadPath('invoicingViewWO/viewWO');">View WO (Sudah JOC)</a></li>
        <li class="active"><a href="#">Generate Invoices</a></li>
    </ul>
    <!-- End Tab -->

    <!-- Start Kriteria Search -->
    <div class="box" style="padding-top: 5px; padding-left: 10px;">
        <div class="span12">
            <legend style="font-size: small;">
                <g:message code="delivery.invoicing.kriteria.search.label" default="Kriteria Search WO (Sudah JOC dan Washing)"/>
            </legend>
            <g:if test="${flash.message}">
                <div class="alert alert-error">
                    ${flash.message}
                </div>
            </g:if>
        </div>
        <div class="span7" id="search-table" style="padding-left: 0;">
            <table width="95%;">
                <tr align="left">
                    <td align="left">
                        <label class="control-label">
                            <g:message code="delivery.invoicing.kategori.label" default="Kategori"/>
                        </label>
                    </td>
                    <td align="left" colspan="3">
                        <g:select style="width:100%" name="kategoriSelect" id="kategoriSelect" onchange="changeKategori();" from="${['NOMOR WO','NOMOR POLISI']}"/>
                        <input type="hidden" id="kategori" name="kategori" value="1">
                    </td>
                    <td align="left" rowspan="2">
                        &nbsp;
                        <g:field type="button" style="width: 80px; height: 55px" class="btn btn-primary search" onclick="search();"
                                 name="ok" id="ok" value="${message(code: 'default.ok.label', default: 'OK')}" />
                    </td>
                </tr>
                <tr align="left">
                    <td align="left">
                        <label class="control-label">
                            <g:message code="delivery.invoicing.keyWord.label" default="Kata Kunci"/>
                        </label>
                    </td>
                    <td align="left" colspan="3">
                        <g:textField name="keyword" id="keyword" style="width:97%"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
    </div>
    <!-- End Kriteria Search -->

    <!-- Start Invoicing -->
    <div class="box" style="padding-top: 5px; padding-left: 10px;">
        <div class="span12" style="height: 40px;">
            <legend style="font-size: small;">
                <g:message code="delivery.invoicing.wo.list.label" default="Invoicing"/>
            </legend>
            <g:if test="${flash.message}">
                <div class="alert alert-error">
                    ${flash.message}
                </div>
            </g:if>
        </div>
        <div class="span12" id="user-table" style="padding-left: 0; margin-left: 0;">
            <table width="95%" class="table table-bordered">
                <tr>
                    <td>Tanggal WO</td>
                    <td></td>
                    <td>Nomor Mesin</td>
                    <td></td>
                    <td>Nama Customer</td>
                    <td></td>
                    <td rowspan="3">Alamat</td>
                    <td rowspan="3"></td>
                    <td rowspan="3">
                        <g:field type="button" style="width: 220px; height: 50px;" class="btn btn-primary search" onclick="genInvoicePartSlipFaktur();"
                                             name="geninvoicepartslipfaktur" id="geninvoicepartslipfaktur"
                                             value="${message(code: 'default.generate.label', default: 'Generate Invoice + Part Slip + Faktur')}" />
                    </td>
                </tr>
                <tr>
                    <td>Nomor WO</td>
                    <td></td>
                    <td>Nomor Rangka</td>
                    <td></td>
                    <td>Service Advisorr</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Nomor Polisi</td>
                    <td></td>
                    <td>Model</td>
                    <td></td>
                    <td>Billing</td>
                    <td></td>
                </tr>
            </table>
            <br/>
            <g:field type="button" style="width: 120px" class="btn btn-primary search" onclick="generateInvoice();"
                     name="generate" id="generate" value="${message(code: 'default.generate.label', default: 'Customer Profil')}" />
            <g:field type="button" style="width: 120px" class="btn btn-primary search" onclick="generateInvoice();"
                     name="generate" id="generate" value="${message(code: 'default.generate.label', default: 'WO Information')}" />
            <br>

            <g:render template="geDataTables"/>
            <g:field type="button" style="width: 120px" class="btn btn-primary search" onclick="generateInvoice();"
                     name="generate" id="generate" value="${message(code: 'default.generate.label', default: 'Discount Information')}" />

        </div>
    </div>
    <!-- End Invoicing -->







        <div class="span5" id="user-table" style="padding-left: 0; margin-left: 0;">

            <div class="box" style="padding-top: 5px; padding-left: 5px;">
                <legend style="font-size: small;">
                    <g:message code="delivery.invoicing.wo.list.label" default="Jasa/Labor Detail"/>
                </legend>
                <g:if test="${flash.message}">
                    <div class="alert alert-error">
                        ${flash.message}
                    </div>
                </g:if>


                <table width="95%" class="table table-bordered">


                    <tr>
                        <td>No Invoice/Credit Notes </td>
                        <td>:</td>
                        <td></td>
                        <td rowspan="2"><g:field type="button" style="width: 180px; height: 25px;" class="btn btn-primary search" onclick="generateInvoice();"
                                                 name="generate" id="generate" value="${message(code: 'default.generate.label', default: 'Customer Profil')}" /></td>
                    </tr>
                    <tr>
                        <td>No Part Slip :</td>
                        <td>:</td>
                        <td></td>
                    </tr>

                    <g:render template="jaDataTables"/>


            </div>

        </br>


        </div>

        </div>
        <!-- jasa labor detail -->
        </div>



      </div>
</body>
</html>