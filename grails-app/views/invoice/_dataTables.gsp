
<%@ page import="com.kombos.parts.Invoice" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="invoice_datatables_${idTable}" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover" style="table-layout: fixed; ">
    <thead>
        <tr>
            <th>&nbsp;<input type="checkbox" name="checkSelect" onclick="selectAll();"/>&nbsp;</th>
            <th></th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div>Vendor</div>
            </th>
            <th style="border-bottom: none; padding: 5px; width: 200px;"/>
            <th style="border-bottom: none; padding: 5px; width: 200px;"/>
            <th style="border-bottom: none; padding: 5px; width: 200px;"/>
            <th style="border-bottom: none; padding: 5px; width: 200px;"/>
            <th style="border-bottom: none; padding: 5px; width: 200px;"/>
            <th style="border-bottom: none; padding: 5px; width: 200px;"/>
            <th style="border-bottom: none; padding: 5px; width: 200px;"/>
            <th style="border-bottom: none; padding: 5px; width: 200px;"/>
            <th style="border-bottom: none; padding: 5px; width: 200px;"/>
        </tr>
    </thead>
</table>

<g:javascript>
var invoiceTable;
var reloadInvoiceTable;

function searchInvoice(){
    reloadInvoiceTable();
}

$(function(){

	reloadInvoiceTable = function() {
		invoiceTable.fnDraw();
	}

	$('#clear').click(function(){
        $('#search_noInv').val('');
        $('#search_t156Tanggal_start').val('');
        $('#search_t156Tanggal_end').val('');
       document.getElementById("vendor").selectedIndex = 0 ;
        $('#cek1').prop('checked',false);
        $('#cek2').prop('checked',false);
        reloadInvoiceTable();
	});
	$('#search_t166TglInv').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t166TglInv_day').val(newDate.getDate());
			$('#search_t166TglInv_month').val(newDate.getMonth()+1);
			$('#search_t166TglInv_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			invoiceTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	invoiceTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});


    var anOpen = [];
    $('#invoice_datatables_${idTable} td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = invoiceTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${request.contextPath}/invoice/sublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = invoiceTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});

  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			invoiceTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

	invoiceTable = $('#invoice_datatables_${idTable}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "sClass": "control center",
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "id",
	"mDataProp": "id",
	"bSortable": false,
	"sWidth":"100px",
	"sDefaultContent": '',
	"bVisible": false
},
{
	"sName": "vendor",
	"mDataProp": "vendor",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select-vendor" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'"> &nbsp; &nbsp;' + data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
},
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"100px",
	"sDefaultContent": '',
	"bVisible": true
},
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"100px",
	"sDefaultContent": '',
	"bVisible": true
},
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"100px",
	"sDefaultContent": '',
	"bVisible": true
},
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"100px",
	"sDefaultContent": '',
	"bVisible": true
},
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"100px",
	"sDefaultContent": '',
	"bVisible": true
},
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"100px",
	"sDefaultContent": '',
	"bVisible": true
} ,
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"100px",
	"sDefaultContent": '',
	"bVisible": true
},
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"100px",
	"sDefaultContent": '',
	"bVisible": true
},
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"100px",
	"sDefaultContent": '',
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						var tanggalStart = $("#search_t156Tanggal_start").val();
						var tanggalStartDay = $('#search_t156Tanggal_start_day').val();
						var tanggalStartMonth = $('#search_t156Tanggal_start_month').val();
						var tanggalStartYear = $('#search_t156Tanggal_start_year').val();
                        if(tanggalStart){
							aoData.push(
									{"name": 'tanggalStart', "value": "date.struct"},
									{"name": 'tanggalStart_dp', "value": tanggalStart},
									{"name": 'tanggalStart_day', "value": tanggalStartDay},
									{"name": 'tanggalStart_month', "value": tanggalStartMonth},
									{"name": 'tanggalStart_year', "value": tanggalStartYear}
							);
						}

                        var tanggalEnd = $("#search_t156Tanggal_start").val();
                        var tanggalEndDay = $('#search_t156Tanggal_end_day').val();
						var tanggalEndMonth = $('#search_t156Tanggal_end_month').val();
						var tanggalEndYear = $('#search_t156Tanggal_end_year').val();
						if(tanggalEnd){
							aoData.push(
									{"name": 'tanggalEnd', "value": "date.struct"},
									{"name": 'tanggalEnd_dp', "value": tanggalEnd},
									{"name": 'tanggalEnd_day', "value": tanggalEndDay},
									{"name": 'tanggalEnd_month', "value": tanggalEndMonth},
									{"name": 'tanggalEnd_year', "value": tanggalEndYear}
							);
						}

                        var search_noInv = $("#search_noInv").val();
                        if(search_noInv){
							aoData.push(
									{"name": 'search_noInv', "value": search_noInv}
							);
						}
						var vendor = $("#vendor").find(":selected").val();
                        if(vendor){
							aoData.push(
									{"name": 'vendor', "value": vendor}
							);
						}

                        var staSPLD1 = $('input[name=staSPLD1]').val();
                        if($('input[name=staSPLD1]').is(':checked')){
							aoData.push(
									{"name": 'staSPLD1', "value": staSPLD1}
							);
						}

                        var staSPLD2 = $('input[name=staSPLD2]').val();
                        if($('input[name=staSPLD2]').is(':checked')){
							aoData.push(
									{"name": 'staSPLD2', "value": staSPLD2}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
