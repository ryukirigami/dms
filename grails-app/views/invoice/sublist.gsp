<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
		<g:javascript>
var vopSubTable_${idTable};
$(function(){ 
var anOpen = [];
	$('#vop_datatables_sub_${idTable} td.subcontrol').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
  		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = vopSubTable_${idTable}.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST', 
	   			data : oData,
	   			url:'${request.contextPath}/invoice/subsublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = vopSubTable_${idTable}.fnOpen(nTr,data,'details');
    				$('div.innerinnerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
    		
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerinnerDetails', $(nTr).next()[0]).slideUp( function () {
      			vopSubTable_${idTable}.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

    $('#vop_datatables_sub_${idTable} a.edit').live('click', function (e) {
        e.preventDefault();
         
        var nRow = $(this).parents('tr')[0];
         
        if ( nEditing !== null && nEditing != nRow ) {
            restoreRow( vopSubTable_${idTable}, nEditing );
            editRow( vopSubTable_${idTable}, nRow );
            nEditing = nRow;
        }
        else if ( nEditing == nRow && this.innerHTML == "Save" ) {
            saveRow( vopSubTable_${idTable}, nEditing );
            nEditing = null;
        }
        else {
            editRow( vopSubTable_${idTable}, nRow );
            nEditing = nRow;
        }
    } );
    if(vopSubTable_${idTable})
    	vopSubTable_${idTable}.dataTable().fnDestroy();
vopSubTable_${idTable} = $('#vop_datatables_sub_${idTable}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "t166NoInv",
	"mDataProp": "t166NoInv",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"> <input type="hidden" value="'+data+'">&nbsp;&nbsp;' + data;
	},
	"bSortable": false,
	"sWidth":"274px",
	"bVisible": true
},
{
	"sName": "t166TglInv",
	"mDataProp": "t166TglInv",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "t166NetSalesPrice",
	"mDataProp": "t166NetSalesPrice",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"100px",
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"100px",
	"sDefaultContent": '',
	"bVisible": true
},
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"100px",
	"sDefaultContent": '',
	"bVisible": true
} ,
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"100px",
	"sDefaultContent": '',
	"bVisible": true
},
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"100px",
	"sDefaultContent": '',
	"bVisible": true
},
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"100px",
	"sDefaultContent": '',
	"bVisible": true
} ,
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"100px",
	"sDefaultContent": '',
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						var tanggalStart = $("#search_t156Tanggal_start").val();
						var tanggalStartDay = $('#search_t156Tanggal_start_day').val();
						var tanggalStartMonth = $('#search_t156Tanggal_start_month').val();
						var tanggalStartYear = $('#search_t156Tanggal_start_year').val();
                        if(tanggalStart){
							aoData.push(
									{"name": 'tanggalStart', "value": "date.struct"},
									{"name": 'tanggalStart_dp', "value": tanggalStart},
									{"name": 'tanggalStart_day', "value": tanggalStartDay},
									{"name": 'tanggalStart_month', "value": tanggalStartMonth},
									{"name": 'tanggalStart_year', "value": tanggalStartYear}
							);
						}

                        var tanggalEnd = $("#search_t156Tanggal_start").val();
                        var tanggalEndDay = $('#search_t156Tanggal_end_day').val();
						var tanggalEndMonth = $('#search_t156Tanggal_end_month').val();
						var tanggalEndYear = $('#search_t156Tanggal_end_year').val();
						if(tanggalEnd){
							aoData.push(
									{"name": 'tanggalEnd', "value": "date.struct"},
									{"name": 'tanggalEnd_dp', "value": tanggalEnd},
									{"name": 'tanggalEnd_day', "value": tanggalEndDay},
									{"name": 'tanggalEnd_month', "value": tanggalEndMonth},
									{"name": 'tanggalEnd_year', "value": tanggalEndYear}
							);
						}

                        var staSPLD1 = $('input[name=staSPLD1]').val();
                        if($('input[name=staSPLD1]').is(':checked')){
							aoData.push(
									{"name": 'staSPLD1', "value": staSPLD1}
							);
						}

                        var search_noInv = $("#search_noInv").val();
                        if(search_noInv){
							aoData.push(
									{"name": 'search_noInv', "value": search_noInv}
							);
						}
                        aoData.push(
									{"name": 'id', "value": ${id}}
							);
                        aoData.push(
                                {"name": 'vendorId', "value": ${vendorId}}
                        );

                        var staSPLD2 = $('input[name=staSPLD2]').val();
                        if($('input[name=staSPLD2]').is(':checked')){
							aoData.push(
									{"name": 'staSPLD2', "value": staSPLD2}
							);
						}
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
		</g:javascript>
	</head>
	<body>
<div class="innerDetails">
<table id="vop_datatables_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
	class="display table table-striped table-bordered table-hover">
    <thead>
        <tr>
           <th></th>
           <th></th>
			<th style="border-bottom: none; padding: 5px; width: 200px;">
				<div>No Invoice</div>
			</th>
			<th style="border-bottom: none; padding: 5px; width: 200px;">
				<div>Invoice Date</div>
			</th>
			<th style="border-bottom: none; padding: 5px; width: 200px;">
				<div>Total Harga (Rp)</div>
			</th>
			<th style="border-bottom: none; padding: 5px;"/>
			<th style="border-bottom: none; padding: 5px;"/>
			<th style="border-bottom: none; padding: 5px;"/>
            <th style="border-bottom: none; padding: 5px;"/>
            <th style="border-bottom: none; padding: 5px;"/>
            <th style="border-bottom: none; padding: 5px;"/>
            <th style="border-bottom: none; padding: 5px;"/>
         </tr>
			    </thead>
			    <tbody></tbody>
			</table>
</div>
</body>
</html>
