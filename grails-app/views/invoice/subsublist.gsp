<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
		<g:javascript>
var vopSubSubTable_${idTable};
$(function(){ 
vopSubSubTable_${idTable} = $('#vop_datatables_sub_sub_${idTable}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubSubList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": ''
},
{
	"sName": "kodePart",
	"mDataProp": "kodePart",
	"aTargets": [1],
	"mRender": function ( data, type, row ) {
		return '<input type="hidden" value="'+row[0]+'">&nbsp;&nbsp;' + data;
	},
	"bSortable": false,
	"sWidth":"241px",
	"bVisible": true
},
{
	"sName": "namaPart",
	"mDataProp": "namaPart",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"183px",
	"bVisible": true
},
{
	"sName": "noPO",
	"mDataProp": "noPO",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"85px",
	"bVisible": true
},
{
	"sName": "tanggalPO",
	"mDataProp": "tanggalPO",
	"aTargets": [4],
	"bSortable": false,
	"sWidth":"89px",
	"bVisible": true
},
{
	"sName": "tipeOrder",
	"mDataProp": "tipeOrder",
	"aTargets": [5],
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "qtyReceive",
	"mDataProp": "qtyReceive",
	"aTargets": [6],
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "retailPrice",
	"mDataProp": "retailPrice",
	"aTargets": [7],
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "discount",
	"mDataProp": "discount",
	"aTargets": [8],
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "netSalePrice",
	"mDataProp": "netSalePrice",
	"aTargets": [9],
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
}



],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						var tanggalStart = $("#search_t156Tanggal_start").val();
						var tanggalStartDay = $('#search_t156Tanggal_start_day').val();
						var tanggalStartMonth = $('#search_t156Tanggal_start_month').val();
						var tanggalStartYear = $('#search_t156Tanggal_start_year').val();
                        if(tanggalStart){
							aoData.push(
									{"name": 'tanggalStart', "value": "date.struct"},
									{"name": 'tanggalStart_dp', "value": tanggalStart},
									{"name": 'tanggalStart_day', "value": tanggalStartDay},
									{"name": 'tanggalStart_month', "value": tanggalStartMonth},
									{"name": 'tanggalStart_year', "value": tanggalStartYear}
							);
						}

                        var tanggalEnd = $("#search_t156Tanggal_start").val();
                        var tanggalEndDay = $('#search_t156Tanggal_end_day').val();
						var tanggalEndMonth = $('#search_t156Tanggal_end_month').val();
						var tanggalEndYear = $('#search_t156Tanggal_end_year').val();
						if(tanggalEnd){
							aoData.push(
									{"name": 'tanggalEnd', "value": "date.struct"},
									{"name": 'tanggalEnd_dp', "value": tanggalEnd},
									{"name": 'tanggalEnd_day', "value": tanggalEndDay},
									{"name": 'tanggalEnd_month', "value": tanggalEndMonth},
									{"name": 'tanggalEnd_year', "value": tanggalEndYear}
							);
						}

                        aoData.push(
									{"name": 'invoiceNo', "value": '${invoiceNo}'}
							);

                        var staSPLD1 = $('input[name=staSPLD1]').val();
                        if($('input[name=staSPLD1]').is(':checked')){
							aoData.push(
									{"name": 'staSPLD1', "value": staSPLD1}
							);
						}

                        var staSPLD2 = $('input[name=staSPLD2]').val();
                        if($('input[name=staSPLD2]').is(':checked')){
							aoData.push(
									{"name": 'staSPLD2', "value": staSPLD2}
							);
						}
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
		</g:javascript>
	</head>
	<body>
	<div class="innerinnerDetails">
<table id="vop_datatables_sub_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
	class="display table table-striped table-bordered table-hover">
    <thead>
        <tr>
           <th></th>
           <th></th>
           <th></th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Kode Part</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Nama Part</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>No PO</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Tanggal PO</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Tipe Order</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Qty Receive</div>
			</th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Retail Price</div>
            </th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Discount</div>
			</th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Net Sale Price</div>
            </th>
        </tr>
			    </thead>
			    <tbody></tbody>
			</table>
	</div>
</body>
</html>
