
<%@ page import="com.kombos.parts.Vendor; com.kombos.parts.Invoice" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'invoice.label', default: 'Invoice')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    klik = function(){

        $('#spinner').fadeIn(1);
        $.ajax({
            url: '${request.contextPath}/inputInvoice',
            type: "GET",dataType:"html",
            complete : function (req, err) {
                $('#main-content').html(req.responseText);
                $('#spinner').fadeOut();
            }
        });
    }
    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/invoice/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/invoice/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#invoice-form').empty();
    	$('#invoice-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#invoice-table").hasClass("span12")){
   			$("#invoice-table").toggleClass("span12 span5");
        }
        $("#invoice-form").css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
   		if($("#invoice-table").hasClass("span5")){
   			$("#invoice-table").toggleClass("span5 span12");
   		}
        $("#invoice-form").css("display","none");
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#invoice-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/invoice/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadInvoiceTable();
    		}
		});
		
   	}

});

function deleteBatch(){
    var recordsToDeleteVendor = [];
    $("#invoice-table tbody .row-select-vendor").each(function() {
        if(this.checked){
            var id = $(this).next("input:hidden").val();
            recordsToDeleteVendor.push(id);
        }
    });

    var jsonVendor = JSON.stringify(recordsToDeleteVendor);

    var recordsToDelete = [];
    $("#invoice-table tbody .row-select").each(function() {
        if(this.checked){
            var id = $(this).next("input:hidden").val();
            recordsToDelete.push(id);
        }
    });

    var json = JSON.stringify(recordsToDelete);

    $.ajax({
        url:'${request.contextPath}/invoice/massdelete',
        type: "POST", // Always use POST when deleting data
        data: {
            idsVendor: jsonVendor,
            idsInv: json
        },
        complete: function(xhr, status) {
            reloadInvoiceTable();
        }
    });
}

function selectAll(){
    if($('input[name=checkSelect]').is(':checked')){
        $(".row-select").attr("checked",true);
        $(".row-select-vendor").attr("checked",true);
    }else {
        $(".row-select").attr("checked",false);
        $(".row-select-vendor").attr("checked",false);
    }
}

</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
            <li><a class="pull-right" href="#/inputInvoice" onclick="klik()" style="display: block;" >&nbsp;&nbsp;<i class="icon-plus"></i>&nbsp;&nbsp;</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="invoice-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <fieldset>
                <table style="padding-right: 10px">
                    <tr>
                        <td style="width: 130px">
                            <label class="control-label" for="tanggal_invoice">
                                <g:message code="invoice.tanggal.label" default="No Invoice" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="no_invoice" class="controls">
                                <g:textField name="search_noInv" id="search_noInv" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 130px">
                            <label class="control-label" for="tanggal_invoice">
                                <g:message code="invoice.tanggal.label" default="Tanggal Invoice" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="tanggal_invoice" class="controls">
                                    <ba:datePicker id="search_t156Tanggal_start" name="search_t156Tanggal_start" precision="day" format="dd/MM/yyyy"  value="" />
                                &nbsp;-&nbsp;
                                <ba:datePicker id="search_t156Tanggal_end" name="search_t156Tanggal_end" precision="day" format="dd/MM/yyyy"  value="" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label" for="vendor">
                                <g:message code="invoice.vendro.label" default="Vendor" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="vendors" class="controls">
                                <g:select id="vendor" name="vendor.id" from="${Vendor.createCriteria().list {eq('staDel','0'); order('m121Nama','asc')}}" optionKey="id" required="" class="many-to-one" noSelection="['':'']"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label" for="vendorType">
                                <g:message code="invoice.vendorType.label" default="Vendor Type" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="vendorType" class="controls">
                                <input type="checkbox" id="cek1" name="staSPLD1" value="0"> &nbsp;SPLD&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="checkbox" id="cek2" name="staSPLD2" value="1"> &nbsp;Non SPLD
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" >
                            <div class="controls" style="right: 0">
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-primary view" name="view" id="view" onclick="searchInvoice()" >Search</button>
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-cancel clear" name="clear" id="clear" >Clear</button>
                            </div>
                        </td>
                    </tr>
                </table>
            </fieldset>
			<g:render template="dataTables" />

		</div>
		<div class="span7" id="invoice-form" style="display: none;"></div>
	</div>
</body>
</html>
