

<%@ page import="com.kombos.parts.ICC" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'ICC.label', default: 'ICC')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteICC;

$(function(){ 
	deleteICC=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/ICC/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadICCTable();
   				expandTableLayout('ICC');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-ICC" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="ICC"
			class="table table-bordered table-hover">
			<tbody>

				
				%{--<g:if test="${ICCInstance?.companyDealer}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="companyDealer-label" class="property-label"><g:message--}%
					%{--code="ICC.companyDealer.label" default="Company Dealer" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="companyDealer-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${ICCInstance}" field="companyDealer"--}%
								%{--url="${request.contextPath}/ICC/updatefield" type="text"--}%
								%{--title="Enter companyDealer" onsuccess="reloadICCTable();" />--}%
							%{----}%
								%{--<g:link controller="companyDealer" action="show" id="${ICCInstance?.companyDealer?.id}">${ICCInstance?.companyDealer?.encodeAsHTML()}</g:link>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%

            <g:if test="${ICCInstance?.goods}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="goods-label" class="property-label"><g:message
                                code="ICC.goods.label" default="Goods" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="goods-label">
                        %{--<ba:editableValue
                                bean="${ICCInstance}" field="goods"
                                url="${request.contextPath}/ICC/updatefield" type="text"
                                title="Enter goods" onsuccess="reloadICCTable();" />--}%

                        <g:link controller="goods" action="show" id="${ICCInstance?.goods?.id}">${ICCInstance?.goods?.encodeAsHTML()}</g:link>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${ICCInstance?.parameterICC}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="parameterICC-label" class="property-label"><g:message
                                code="ICC.parameterICC.label" default="Parameter ICC" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="parameterICC-label">
                        %{--<ba:editableValue
                                bean="${ICCInstance}" field="parameterICC"
                                url="${request.contextPath}/ICC/updatefield" type="text"
                                title="Enter parameterICC" onsuccess="reloadICCTable();" />--}%

                        <g:link controller="parameterICC" action="show" id="${ICCInstance?.parameterICC?.id}">${ICCInstance?.parameterICC?.encodeAsHTML()}</g:link>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${ICCInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="ICC.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${ICCInstance}" field="createdBy"
								url="${request.contextPath}/ICC/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadICCTable();" />--}%
							
								<g:fieldValue bean="${ICCInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${ICCInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="ICC.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${ICCInstance}" field="dateCreated"
								url="${request.contextPath}/ICC/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadICCTable();" />--}%
							
								<g:formatDate date="${ICCInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			

			
				<g:if test="${ICCInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="ICC.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${ICCInstance}" field="lastUpdProcess"
								url="${request.contextPath}/ICC/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadICCTable();" />--}%
							
								<g:fieldValue bean="${ICCInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${ICCInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="ICC.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${ICCInstance}" field="lastUpdated"
								url="${request.contextPath}/ICC/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadICCTable();" />--}%
							
								<g:formatDate date="${ICCInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${ICCInstance?.t155DAD}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="t155DAD-label" class="property-label"><g:message--}%
					%{--code="ICC.t155DAD.label" default="T155 DAD" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="t155DAD-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${ICCInstance}" field="t155DAD"--}%
								%{--url="${request.contextPath}/ICC/updatefield" type="text"--}%
								%{--title="Enter t155DAD" onsuccess="reloadICCTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${ICCInstance}" field="t155DAD"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${ICCInstance?.t155StaDel}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="t155StaDel-label" class="property-label"><g:message--}%
					%{--code="ICC.t155StaDel.label" default="T155 Sta Del" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="t155StaDel-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${ICCInstance}" field="t155StaDel"--}%
								%{--url="${request.contextPath}/ICC/updatefield" type="text"--}%
								%{--title="Enter t155StaDel" onsuccess="reloadICCTable();" />--}%
							%{----}%
								%{--<g:fieldValue bean="${ICCInstance}" field="t155StaDel"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			%{----}%
				%{--<g:if test="${ICCInstance?.t155Tanggal}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="t155Tanggal-label" class="property-label"><g:message--}%
					%{--code="ICC.t155Tanggal.label" default="T155 Tanggal" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="t155Tanggal-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${ICCInstance}" field="t155Tanggal"--}%
								%{--url="${request.contextPath}/ICC/updatefield" type="text"--}%
								%{--title="Enter t155Tanggal" onsuccess="reloadICCTable();" />--}%
							%{----}%
								%{--<g:formatDate date="${ICCInstance?.t155Tanggal}" />--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			
				<g:if test="${ICCInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="ICC.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${ICCInstance}" field="updatedBy"
								url="${request.contextPath}/ICC/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadICCTable();" />--}%
							
								<g:fieldValue bean="${ICCInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('ICC');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${ICCInstance?.id}"
					update="[success:'ICC-form',failure:'ICC-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteICC('${ICCInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
