<%@ page import="com.kombos.parts.ICC" %>





<div class="control-group fieldcontain ${hasErrors(bean: ICCInstance, field: 'goods', 'error')} ">
	<label class="control-label" for="goods">
		<g:message code="ICC.kode.goods.label" default="Kode Parts" />
		
	</label>
	<div class="controls">
	<g:select id="goods" name="goods.id" from="${com.kombos.parts.Goods.list()}" optionKey="id" required="" value="${ICCInstance?.goods?.m111ID}" class="many-to-one"/>
	</div>
</div>
<g:javascript>
    document.getElementById("goods").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: ICCInstance, field: 'goods', 'error')} ">
    <label class="control-label" for="goods">
        <g:message code="ICC.nama.goods.label" default="Nama Parts" />

    </label>
    <div class="controls">
        <g:select id="goods" name="goods.id" from="${com.kombos.parts.Goods.list()}" optionKey="id" required="" value="${ICCInstance?.goods?.m111Nama}" class="many-to-one"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: ICCInstance, field: 'parameterICC', 'error')} ">
	<label class="control-label" for="parameterICC">
		<g:message code="ICC.gudang.parameterICC.label" default="ICC Gudang" />
		
	</label>
	<div class="controls">
	<g:select id="parameterICC" name="parameterICC.id" from="${com.kombos.parts.ParameterICC.list()}" optionKey="id" required="" value="${ICCInstance?.parameterICC?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: ICCInstance, field: 'parameterICC', 'error')} ">
    <label class="control-label" for="parameterICC">
        <g:message code="ICC.suggest.parameterICC.label" default="ICC Suggest" />

    </label>
    <div class="controls">
        <g:select id="parameterICC" name="parameterICC.id" from="${com.kombos.parts.ParameterICC.list()}" optionKey="id" required="" value="${ICCInstance?.parameterICC?.id}" class="many-to-one"/>
    </div>
</div>

%{--<div class="control-group fieldcontain ${hasErrors(bean: ICCInstance, field: 'companyDealer', 'error')} ">--}%
    %{--<label class="control-label" for="companyDealer">--}%
        %{--<g:message code="ICC.companyDealer.label" default="Company Dealer" />--}%

    %{--</label>--}%
    %{--<div class="controls">--}%
        %{--<g:select id="companyDealer" name="companyDealer.id" from="${com.kombos.administrasi.CompanyDealer.list()}" optionKey="id" required="" value="${ICCInstance?.companyDealer?.id}" class="many-to-one"/>--}%
    %{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: ICCInstance, field: 'lastUpdProcess', 'error')} ">--}%
    %{--<label class="control-label" for="lastUpdProcess">--}%
        %{--<g:message code="ICC.lastUpdProcess.label" default="Last Upd Process" />--}%

    %{--</label>--}%
    %{--<div class="controls">--}%
        %{--<g:textField name="lastUpdProcess" value="${ICCInstance?.lastUpdProcess}" />--}%
    %{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: ICCInstance, field: 't155DAD', 'error')} ">--}%
	%{--<label class="control-label" for="t155DAD">--}%
		%{--<g:message code="ICC.t155DAD.label" default="T155 DAD" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:field type="number" name="t155DAD" value="${ICCInstance.t155DAD}" />--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: ICCInstance, field: 't155StaDel', 'error')} ">--}%
	%{--<label class="control-label" for="t155StaDel">--}%
		%{--<g:message code="ICC.t155StaDel.label" default="T155 Sta Del" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="t155StaDel" value="${ICCInstance?.t155StaDel}" />--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: ICCInstance, field: 't155Tanggal', 'error')} ">--}%
	%{--<label class="control-label" for="t155Tanggal">--}%
		%{--<g:message code="ICC.t155Tanggal.label" default="T155 Tanggal" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<ba:datePicker name="t155Tanggal" precision="day" value="${ICCInstance?.t155Tanggal}" format="yyyy-MM-dd"/>--}%
	%{--</div>--}%
%{--</div>--}%

