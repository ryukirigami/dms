
<%@ page import="com.kombos.parts.ICC" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'ICC.label', default: 'ICC')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			var hitungICC;

			$(function(){



                $('#t155Tanggal_check').click(function(){
                    var checkStatus = $("#t155Tanggal_check").is(":checked");
                    if(checkStatus){
                        $("#btnHitung").removeAttr("disabled");
                        $("#t155Tanggal").prop('disabled', false);

                    }else{
                        $("#btnHitung").attr("disabled","disabled");
                        $("#t155Tanggal").val("");
                        $("#t155Tanggal").prop('disabled', true);

                    }
                });

				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('ICC');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('ICC', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('ICC', '${request.contextPath}/ICC/massdelete', reloadICCTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('ICC','${request.contextPath}/ICC/show/'+id);
				};
				
				edit = function(id) {
					editInstance('ICC','${request.contextPath}/ICC/edit/'+id);
				};

});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
	</div>
	<div class="box">
		<div class="span12" id="ICC-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <fieldset>
                <table style="padding-right: 10px">
                <legend>Perhitungan ICC</legend>

                    <div class="control-group fieldcontain ${hasErrors(bean: ICCInstance, field: 't155Tanggal', 'error')} ">
                        <label class="control-label" for="t155Tanggal">
                            <g:message code="ICC.t155Tanggal.label" default="Tanggal Perhitungan" />

                        </label>
                        <div class="controls">
                            <g:checkBox name="t155Tanggal_check" id="t155Tanggal_check" />
                            <ba:datePicker name="t155Tanggal" id="t155Tanggal" disabled="true" precision="day" value="${ICCInstance?.t155Tanggal}" format="dd/MM/yyyy"/>&nbsp;&nbsp;&nbsp;
                            <button style="width: 70px;height: 30px; border-radius: 5px" class="btn-primary view" name="btnHitung" id="btnHitung" onclick="hitungICC();" disabled>Hitung ICC</button>
                        </div>
                    </div>
                </table>
            </fieldset>
<g:javascript>

    $("#t155Tanggal").prop('disabled', true);

    $('#t155Tanggal_check').click(function(){
        var checkStatus = $("#t155Tanggal_check").is(":checked");
        if(checkStatus){
            $("#btnHitung").removeAttr("disabled");
            $("#t155Tanggal").prop('disabled', false);

        }else{
            $("#btnHitung").attr("disabled","disabled");
            $("#t155Tanggal").val("");
            $("#t155Tanggal").prop('disabled', true);

        }
    });

</g:javascript>

        </div>

			<g:render template="dataTables" />

<g:javascript>
$(function(){
     hitungICC = function(){

				     var tanggal = $('#t155Tanggal').val();

                     if(tanggal == ""){
                        toastr.error("Tanggal tidak boleh kosong");
                        $('#t155Tanggal').focus();
                        return;
                     }
                      jQuery.getJSON('${request.contextPath}/ICC/hitungICC?tanggal=' + tanggal, function (data) {

                        if (data) {
                            ICCTable.fnClearTable();
                            jQuery.each(data, function (index, value) {
                              ICCTable.fnAddData({
                                    'kodePart' : value.kodePart,
                                    'namaPart': value.namaPart,
                                    'iccGudang': value.iccGudang,
                                    'iccSuggest': value.iccSuggest});

                            });

                        }
                    });

                }

})
</g:javascript>
		</div>
		<div class="span7" id="ICC-form" style="display: none;"></div>
	</div>
</body>
</html>
