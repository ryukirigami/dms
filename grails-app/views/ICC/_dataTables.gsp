
<%@ page import="com.kombos.parts.ICC" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="ICC_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="ICC.kode.goods.label" default="Kode Parts" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="ICC.nama.goods.label" default="Nama Parts" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="ICC.gudang.parameterICC.label" default="ICC Gudang" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="ICC.suggest.parameterICC.label" default="ICC Suggest" /></div>
            </th>


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="ICC.companyDealer.label" default="Company Dealer" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="ICC.lastUpdProcess.label" default="Last Upd Process" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="ICC.t155DAD.label" default="T155 DAD" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="ICC.t155StaDel.label" default="T155 Sta Del" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="ICC.t155Tanggal.label" default="T155 Tanggal" /></div>--}%
			%{--</th>--}%

		
		</tr>

	</thead>
</table>

<g:javascript>
var ICCTable;
var reloadICCTable;
$(function(){
	
	reloadICCTable = function() {
		ICCTable.fnDraw();


	}

	
	$('#search_t155Tanggal').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t155Tanggal_day').val(newDate.getDate());
			$('#search_t155Tanggal_month').val(newDate.getMonth()+1);
			$('#search_t155Tanggal_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			ICCTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	ICCTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	ICCTable = $('#ICC_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
	//	"bProcessing": true,
	//	"bServerSide": true,
//		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
//		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [


{
	"sName": "kodePart",
	"mDataProp": "kodePart",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "namaPart",
	"mDataProp": "namaPart",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "iccGudang",
	"mDataProp": "iccGudang",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "iccSuggest",
	"mDataProp": "iccSuggest",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var goods = $('#filter_kode_goods input').val();
						if(goods){
							aoData.push(
									{"name": 'sCriteria_kode_goods', "value": goods}
							);
						}

						var goods = $('#filter_nama_goods input').val();
						if(goods){
							aoData.push(
									{"name": 'sCriteria_nama_goods', "value": goods}
							);
						}
	
						var parameterICC = $('#filter_gudang_parameterICC input').val();
						if(parameterICC){
							aoData.push(
									{"name": 'sCriteria_gudang_parameterICC', "value": parameterICC}
							);
						}

						var parameterICC = $('#filter_suggest_parameterICC input').val();
						if(parameterICC){
							aoData.push(
									{"name": 'sCriteria_suggest_parameterICC', "value": parameterICC}
							);
						}

						%{--var companyDealer = $('#filter_companyDealer input').val();--}%
						%{--if(companyDealer){--}%
							%{--aoData.push(--}%
									%{--{"name": 'sCriteria_companyDealer', "value": companyDealer}--}%
							%{--);--}%
						%{--}--}%
	%{----}%
						%{--var t155DAD = $('#filter_t155DAD input').val();--}%
						%{--if(t155DAD){--}%
							%{--aoData.push(--}%
									%{--{"name": 'sCriteria_t155DAD', "value": t155DAD}--}%
							%{--);--}%
						%{--}--}%
	%{----}%
						%{--var t155StaDel = $('#filter_t155StaDel input').val();--}%
						%{--if(t155StaDel){--}%
							%{--aoData.push(--}%
									%{--{"name": 'sCriteria_t155StaDel', "value": t155StaDel}--}%
							%{--);--}%
						%{--}--}%

						%{--var t155Tanggal = $('#search_t155Tanggal').val();--}%
						%{--var t155TanggalDay = $('#search_t155Tanggal_day').val();--}%
						%{--var t155TanggalMonth = $('#search_t155Tanggal_month').val();--}%
						%{--var t155TanggalYear = $('#search_t155Tanggal_year').val();--}%
						%{----}%
						%{--if(t155Tanggal){--}%
							%{--aoData.push(--}%
									%{--{"name": 'sCriteria_t155Tanggal', "value": "date.struct"},--}%
									%{--{"name": 'sCriteria_t155Tanggal_dp', "value": t155Tanggal},--}%
									%{--{"name": 'sCriteria_t155Tanggal_day', "value": t155TanggalDay},--}%
									%{--{"name": 'sCriteria_t155Tanggal_month', "value": t155TanggalMonth},--}%
									%{--{"name": 'sCriteria_t155Tanggal_year', "value": t155TanggalYear}--}%
							%{--);--}%
						%{--}--}%
						%{----}%
						%{--var lastUpdProcess = $('#filter_lastUpdProcess input').val();--}%
						%{--if(lastUpdProcess){--}%
							%{--aoData.push(--}%
									%{--{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}--}%
							%{--);--}%
						%{--}--}%

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
