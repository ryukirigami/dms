

<%@ page import="com.kombos.parts.HargaJualNonSPLD" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'hargaJualNonSPLD.label', default: 'Semua Parts')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteHargaJualNonSPLD;

$(function(){ 
	deleteHargaJualNonSPLD=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/hargaJualNonSPLD/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadHargaJualNonSPLDTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-hargaJualNonSPLD" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="hargaJualNonSPLD"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${hargaJualNonSPLDInstance?.t159TMT}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t159TMT-label" class="property-label"><g:message
					code="hargaJualNonSPLD.t159TMT.label" default="Tanggal Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t159TMT-label">
						%{--<ba:editableValue
								bean="${hargaJualNonSPLDInstance}" field="t159TMT"
								url="${request.contextPath}/HargaJualNonSPLD/updatefield" type="text"
								title="Enter t159TMT" onsuccess="reloadHargaJualNonSPLDTable();" />--}%
							
								<g:formatDate date="${hargaJualNonSPLDInstance?.t159TMT}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${hargaJualNonSPLDInstance?.t159PersenKenaikanHarga}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t159PersenKenaikanHarga-label" class="property-label"><g:message
					code="hargaJualNonSPLD.t159PersenKenaikanHarga.label" default="Persen Kenaikan Harga" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t159PersenKenaikanHarga-label">
						%{--<ba:editableValue
								bean="${hargaJualNonSPLDInstance}" field="t159PersenKenaikanHarga"
								url="${request.contextPath}/HargaJualNonSPLD/updatefield" type="text"
								title="Enter t159PersenKenaikanHarga" onsuccess="reloadHargaJualNonSPLDTable();" />--}%
							
								<g:fieldValue bean="${hargaJualNonSPLDInstance}" field="t159PersenKenaikanHarga"/> %
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${hargaJualNonSPLDInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="hargaJualNonSPLD.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${hargaJualNonSPLDInstance}" field="createdBy"
								url="${request.contextPath}/HargaJualNonSPLD/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadHargaJualNonSPLDTable();" />--}%
							
								<g:fieldValue bean="${hargaJualNonSPLDInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${hargaJualNonSPLDInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="hargaJualNonSPLD.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${hargaJualNonSPLDInstance}" field="updatedBy"
								url="${request.contextPath}/HargaJualNonSPLD/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadHargaJualNonSPLDTable();" />--}%
							
								<g:fieldValue bean="${hargaJualNonSPLDInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${hargaJualNonSPLDInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="hargaJualNonSPLD.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${hargaJualNonSPLDInstance}" field="lastUpdProcess"
								url="${request.contextPath}/HargaJualNonSPLD/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadHargaJualNonSPLDTable();" />--}%
							
								<g:fieldValue bean="${hargaJualNonSPLDInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${hargaJualNonSPLDInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="hargaJualNonSPLD.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${hargaJualNonSPLDInstance}" field="dateCreated"
								url="${request.contextPath}/HargaJualNonSPLD/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadHargaJualNonSPLDTable();" />--}%
							
								<g:formatDate date="${hargaJualNonSPLDInstance?.dateCreated}" format="EEEE, dd MMMM yyyy HH:mm"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${hargaJualNonSPLDInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="hargaJualNonSPLD.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${hargaJualNonSPLDInstance}" field="lastUpdated"
								url="${request.contextPath}/HargaJualNonSPLD/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadHargaJualNonSPLDTable();" />--}%
							
								<g:formatDate date="${hargaJualNonSPLDInstance?.lastUpdated}" format="EEEE, dd MMMM yyyy HH:mm" />
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${hargaJualNonSPLDInstance?.id}"
					update="[success:'hargaJualNonSPLD-form',failure:'hargaJualNonSPLD-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteHargaJualNonSPLD('${hargaJualNonSPLDInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
