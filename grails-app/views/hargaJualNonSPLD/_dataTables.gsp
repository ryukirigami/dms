
<%@ page import="com.kombos.parts.HargaJualNonSPLD" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="hargaJualNonSPLD_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="hargaJualNonSPLD.t159TMT.label" default="Tanggal Berlaku" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="hargaJualNonSPLD.t159PersenKenaikanHarga.label" default="Persen Kenaikan Harga (%)" /></div>
			</th>



		
		</tr>
		<tr>
		

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t159TMT" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_t159TMT" value="date.struct">
					<input type="hidden" name="search_t159TMT_day" id="search_t159TMT_day" value="">
					<input type="hidden" name="search_t159TMT_month" id="search_t159TMT_month" value="">
					<input type="hidden" name="search_t159TMT_year" id="search_t159TMT_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t159TMT_dp" value="" id="search_t159TMT" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t159PersenKenaikanHarga" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t159PersenKenaikanHarga" class="search_init persen"/>
				</div>
			</th>
            <g:javascript>
                $('.persen').autoNumeric('init',{
                    vMin:'0',
                    vMax:'100',
                    aSep:'',
                    mDec: '2'
                });

            </g:javascript>



        </tr>
	</thead>
</table>

<g:javascript>
var hargaJualNonSPLDTable;
var reloadHargaJualNonSPLDTable;
$(function(){

    var recordshargaJualNonSPLDperpage = [];//new Array();
    var anhargaJualNonSPLDSelected;
    var jmlRechargaJualNonSPLDPerPage=0;
    var id;

	
	reloadHargaJualNonSPLDTable = function() {
		hargaJualNonSPLDTable.fnDraw();
	}

	
	$('#search_t159TMT').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t159TMT_day').val(newDate.getDate());
			$('#search_t159TMT_month').val(newDate.getMonth()+1);
			$('#search_t159TMT_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			hargaJualNonSPLDTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	hargaJualNonSPLDTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	hargaJualNonSPLDTable = $('#hargaJualNonSPLD_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rshargaJualNonSPLD = $("#hargaJualNonSPLD_datatables tbody .row-select");
            var jmlhargaJualNonSPLDCek = 0;
            var nRow;
            var idRec;
            rshargaJualNonSPLD.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordshargaJualNonSPLDperpage[idRec]=="1"){
                    jmlhargaJualNonSPLDCek = jmlhargaJualNonSPLDCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordshargaJualNonSPLDperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRechargaJualNonSPLDPerPage = rshargaJualNonSPLD.length;
            if(jmlhargaJualNonSPLDCek==jmlRechargaJualNonSPLDPerPage && jmlRechargaJualNonSPLDPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "t159TMT",
	"mDataProp": "t159TMT",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "t159PersenKenaikanHarga",
	"mDataProp": "t159PersenKenaikanHarga",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var t159TMT = $('#search_t159TMT').val();
						var t159TMTDay = $('#search_t159TMT_day').val();
						var t159TMTMonth = $('#search_t159TMT_month').val();
						var t159TMTYear = $('#search_t159TMT_year').val();
						
						if(t159TMT){
							aoData.push(
									{"name": 'sCriteria_t159TMT', "value": "date.struct"},
									{"name": 'sCriteria_t159TMT_dp', "value": t159TMT},
									{"name": 'sCriteria_t159TMT_day', "value": t159TMTDay},
									{"name": 'sCriteria_t159TMT_month', "value": t159TMTMonth},
									{"name": 'sCriteria_t159TMT_year', "value": t159TMTYear}
							);
						}
	
						var t159PersenKenaikanHarga = $('#filter_t159PersenKenaikanHarga input').val();
						if(t159PersenKenaikanHarga){
							aoData.push(
									{"name": 'sCriteria_t159PersenKenaikanHarga', "value": t159PersenKenaikanHarga}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

	 $('.select-all').click(function(e) {

        $("#hargaJualNonSPLD_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordshargaJualNonSPLDperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordshargaJualNonSPLDperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#hargaJualNonSPLD_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordshargaJualNonSPLDperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anhargaJualNonSPLDSelected = hargaJualNonSPLDTable.$('tr.row_selected');
            if(jmlRechargaJualNonSPLDPerPage == anhargaJualNonSPLDSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordshargaJualNonSPLDperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });


});
</g:javascript>



			
