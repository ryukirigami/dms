<%@ page import="com.kombos.parts.HargaJualNonSPLD" %>



<div class="control-group fieldcontain ${hasErrors(bean: hargaJualNonSPLDInstance, field: 't159TMT', 'error')} required">
	<label class="control-label" for="t159TMT">
		<g:message code="hargaJualNonSPLD.t159TMT.label" default="Tanggal Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<ba:datePicker name="t159TMT" precision="day"  value="${hargaJualNonSPLDInstance?.t159TMT}" format="dd/mm/yyyy" required="true" />
	</div>
</div>
<g:javascript>
    document.getElementById("t159TMT").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: hargaJualNonSPLDInstance, field: 't159PersenKenaikanHarga', 'error')} required">
	<label class="control-label" for="t159PersenKenaikanHarga">
		<g:message code="hargaJualNonSPLD.t159PersenKenaikanHarga.label" default="Persen Kenaikan Harga" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t159PersenKenaikanHarga" value="${fieldValue(bean: hargaJualNonSPLDInstance, field: 't159PersenKenaikanHarga')}" class="persen" required=""/> %
	</div>
</div>
<g:javascript>
    $('.persen').autoNumeric('init',{
        vMin:'0',
        vMax:'100',
        aSep:'',
        mDec: '2'
    });
</g:javascript>
