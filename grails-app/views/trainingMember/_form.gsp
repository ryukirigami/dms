<%@ page import="com.kombos.hrd.TrainingMember" %>



<div class="control-group fieldcontain ${hasErrors(bean: trainingMemberInstance, field: 'karyawan', 'error')} ">
	<label class="control-label" for="karyawan">
		<g:message code="trainingMember.karyawan.label" default="Karyawan" />
		
	</label>
	<div class="controls">
	<g:select id="karyawan" name="karyawan.id" from="${com.kombos.hrd.Karyawan.list()}" optionKey="id" required="" value="${trainingMemberInstance?.karyawan?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: trainingMemberInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="trainingMember.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${trainingMemberInstance?.lastUpdProcess}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: trainingMemberInstance, field: 'point', 'error')} ">
	<label class="control-label" for="point">
		<g:message code="trainingMember.point.label" default="Point" />
		
	</label>
	<div class="controls">
	<g:field type="number" name="point" value="${trainingMemberInstance.point}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: trainingMemberInstance, field: 'pointInstruktur', 'error')} ">
    <label class="control-label" for="pointInstruktur">
        <g:message code="trainingMember.pointInstruktur.label" default="Point Instruktur" />

    </label>
    <div class="controls">
        <g:field type="number" name="pointInstruktur" value="${trainingMemberInstance.pointInstruktur}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: trainingMemberInstance, field: 'staGraduate', 'error')} ">
	<label class="control-label" for="staGraduate">
		<g:message code="trainingMember.staGraduate.label" default="Sta Graduate" />
		
	</label>
	<div class="controls">
	<g:textField name="staGraduate" value="${trainingMemberInstance?.staGraduate}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: trainingMemberInstance, field: 'tglGraduate', 'error')} ">
	<label class="control-label" for="tglGraduate">
		<g:message code="trainingMember.tglGraduate.label" default="Tgl Graduate" />
		
	</label>
	<div class="controls">
	<ba:datePicker name="tglGraduate" precision="day" value="${trainingMemberInstance?.tglGraduate}" format="yyyy-MM-dd"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: trainingMemberInstance, field: 'training', 'error')} ">
	<label class="control-label" for="training">
		<g:message code="trainingMember.training.label" default="Training" />
		
	</label>
	<div class="controls">
	<g:select id="training" name="training.id" from="${com.kombos.hrd.TrainingClassRoom.list()}" optionKey="id" required="" value="${trainingMemberInstance?.training?.id}" class="many-to-one"/>
	</div>
</div>

