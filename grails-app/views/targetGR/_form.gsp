<%@ page import="com.kombos.administrasi.CompanyDealer; com.kombos.administrasi.TargetGR" %>


<g:javascript disposition="head">
//    $(function(){
//        $("#m037TglBerlaku_date").datepicker();
//    });
</g:javascript>

<div class="span6 row-fluid">
<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037TglBerlaku', 'error')} required">
	<label class="control-label" for="m037TglBerlaku">
		<g:message code="targetGR.m037TglBerlaku.label" default="Tanggal Berlaku(Unit)" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:datePicker name="m037TglBerlaku" precision="day"  value="${targetGRInstance?.m037TglBerlaku}"  />--}%
    %{
        if(request.getRequestURI().contains("edit")){
    }%
    <input type="hidden" name="m037TglBerlaku" id="m037TglBerlaku" value="${targetGRInstance?.m037TglBerlaku}" />
    <input type="text" disabled="" name="m037TglBerlaku_temp" id="m037TglBerlaku_temp" value="${targetGRInstance?.m037TglBerlaku}" />
    %{
        }else{
    }%
    <input type="hidden" name="m037TglBerlaku" id="m037TglBerlaku" value="${targetGRInstance?.m037TglBerlaku}" />
        %{--<input type="text" id="m037TglBerlaku_date" name="m037TglBerlaku_date"/>--}%
        <ba:datePicker name="m037TglBerlaku_date" id="m037TglBerlaku_date" precision="day" format="dd/mm/yyyy" required="true"/>
    %{
        }
    }%
    </div>
</div>
    <g:javascript>
        document.getElementById("m037TglBerlaku").focus();
    </g:javascript>
%{--<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'companyDealer', 'error')} required">--}%
	%{--<label class="control-label" for="companyDealer">--}%
		%{--<g:message code="targetGR.companyDealer.label" default="Company Dealer" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:select id="companyDealer" name="companyDealer.id" from="${CompanyDealer.list()}" optionKey="id" required="" value="${targetGRInstance?.companyDealer?.id}" class="many-to-one"/>--}%
	%{--</div>--}%
%{--</div>--}%

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037TargetUnitSales', 'error')} required">
	<label class="control-label" for="m037TargetUnitSales">
		<g:message code="targetGR.m037TargetUnitSales.label" default="Target Unit Sales(Unit)" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037TargetUnitSales" value="${fieldValue(bean: targetGRInstance, field: 'm037TargetUnitSales')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037TargetAmountSales', 'error')} required">
    <label class="control-label" for="m037TargetAmountSales">
        <g:message code="targetGR.m037TargetAmountSales.label" default="Target Unit(Amount)" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:field type="text" name="m037TargetAmountSales" value="${fieldValue(bean: targetGRInstance, field: 'm037TargetAmountSales')}" required=""/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037TargetCPUS', 'error')} required">
	<label class="control-label" for="m037TargetCPUS">
		<g:message code="targetGR.m037TargetCPUS.label" default="Target CPUS(Unit)" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037TargetCPUS" value="${fieldValue(bean: targetGRInstance, field: 'm037TargetCPUS')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037TargetSBI', 'error')} required">
	<label class="control-label" for="m037TargetSBI">
		<g:message code="targetGR.m037TargetSBI.label" default="Target SBI(Unit)" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037TargetSBI" value="${fieldValue(bean: targetGRInstance, field: 'm037TargetSBI')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037TargetSBE', 'error')} required">
	<label class="control-label" for="m037TargetSBE">
		<g:message code="targetGR.m037TargetSBE.label" default="Target SBE(Unit)" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037TargetSBE" value="${fieldValue(bean: targetGRInstance, field: 'm037TargetSBE')}" required=""/>
	</div>
</div>
<fieldset>
    <legend>COGS</legend>
<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037COGSPart', 'error')} required">
	<label class="control-label" for="m037COGSPart">
		<g:message code="targetGR.m037COGSPart.label" default="Part(Rp)" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037COGSPart" value="${fieldValue(bean: targetGRInstance, field: 'm037COGSPart')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037COGSOil', 'error')} required">
	<label class="control-label" for="m037COGSOil">
		<g:message code="targetGR.m037COGSOil.label" default="Oil(Rp)" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037COGSOil" value="${fieldValue(bean: targetGRInstance, field: 'm037COGSOil')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037COGSSublet', 'error')} required">
	<label class="control-label" for="m037COGSSublet">
		<g:message code="targetGR.m037COGSSublet.label" default="Sublet(Rp)" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037COGSSublet" value="${fieldValue(bean: targetGRInstance, field: 'm037COGSSublet')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037COGSLabor', 'error')} required">
	<label class="control-label" for="m037COGSLabor">
		<g:message code="targetGR.m037COGSLabor.label" default="Labor(Rp)" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037COGSLabor" value="${fieldValue(bean: targetGRInstance, field: 'm037COGSLabor')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037COGSOverHead', 'error')} required">
	<label class="control-label" for="m037COGSOverHead">
		<g:message code="targetGR.m037COGSOverHead.label" default="Over Head(Rp)" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037COGSOverHead" value="${fieldValue(bean: targetGRInstance, field: 'm037COGSOverHead')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037COGSDeprecation', 'error')} required">
	<label class="control-label" for="m037COGSDeprecation">
		<g:message code="targetGR.m037COGSDeprecation.label" default="Deprecation(Rp)" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037COGSDeprecation" value="${fieldValue(bean: targetGRInstance, field: 'm037COGSDeprecation')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037COGSTotalSGA', 'error')} required">
	<label class="control-label" for="m037COGSTotalSGA">
		<g:message code="targetGR.m037COGSTotalSGA.label" default="Total SGA(Rp)" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037COGSTotalSGA" value="${fieldValue(bean: targetGRInstance, field: 'm037COGSTotalSGA')}" required=""/>
	</div>
</div>
</fieldset>
<fieldset>
    <legend>Technical & Warranty</legend>
<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037TotalClaimItem', 'error')} required">
	<label class="control-label" for="m037TotalClaimItem">
		<g:message code="targetGR.m037TotalClaimItem.label" default="Total Claim SW 103 (Items)" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037TotalClaimItem" value="${fieldValue(bean: targetGRInstance, field: 'm037TotalClaimItem')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037TotalClaimAmount', 'error')} required">
	<label class="control-label" for="m037TotalClaimAmount">
		<g:message code="targetGR.m037TotalClaimAmount.label" default="Total Claim SW 103 (Amount)" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037TotalClaimAmount" value="${fieldValue(bean: targetGRInstance, field: 'm037TotalClaimAmount')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037TechReport', 'error')} required">
	<label class="control-label" for="m037TechReport">
		<g:message code="targetGR.m037TechReport.label" default="Technical Report (Items)" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037TechReport" value="${fieldValue(bean: targetGRInstance, field: 'm037TechReport')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037TWCLeadTime', 'error')} required">
	<label class="control-label" for="m037TWCLeadTime">
		<g:message code="targetGR.m037TWCLeadTime.label" default="TWC Load Time (Day)" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037TWCLeadTime" value="${fieldValue(bean: targetGRInstance, field: 'm037TWCLeadTime')}" required=""/>
	</div>
</div>
</fieldset>
</div>
<div class="span6 row-fluid">
    <div id="companyPICForm" class="span12 row-fluid">
    <fieldset>
        <legend>Customer Satisfaction Level</legend>
    </fieldset>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037YTDS', 'error')} required">
	<label class="control-label" for="m037YTDS">
		<g:message code="targetGR.m037YTDS.label" default="YTD : Satisfied" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037YTDS" value="${fieldValue(bean: targetGRInstance, field: 'm037YTDS')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037YTDD', 'error')} required">
	<label class="control-label" for="m037YTDD">
		<g:message code="targetGR.m037YTDD.label" default="YTD : Dissatisfied" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037YTDD" value="${fieldValue(bean: targetGRInstance, field: 'm037YTDD')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037ThisMonthS', 'error')} required">
	<label class="control-label" for="m037ThisMonthS">
		<g:message code="targetGR.m037ThisMonthS.label" default="This Month : Satisfied" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037ThisMonthS" value="${fieldValue(bean: targetGRInstance, field: 'm037ThisMonthS')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037ThisMonthD', 'error')} required">
	<label class="control-label" for="m037ThisMonthD">
		<g:message code="targetGR.m037ThisMonthD.label" default="This Month : Dissatisfied" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037ThisMonthD" value="${fieldValue(bean: targetGRInstance, field: 'm037ThisMonthD')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037Q1S', 'error')} required">
	<label class="control-label" for="m037Q1S">
		<g:message code="targetGR.m037Q1S.label" default="Q1 : Satisfied" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037Q1S" value="${fieldValue(bean: targetGRInstance, field: 'm037Q1S')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037Q1D', 'error')} required">
	<label class="control-label" for="m037Q1D">
		<g:message code="targetGR.m037Q1D.label" default="Q1 : Dissatisfied" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037Q1D" value="${fieldValue(bean: targetGRInstance, field: 'm037Q1D')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037Q2S', 'error')} required">
	<label class="control-label" for="m037Q2S">
		<g:message code="targetGR.m037Q2S.label" default="Q2 : Satisfied" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037Q2S" value="${fieldValue(bean: targetGRInstance, field: 'm037Q2S')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037Q2D', 'error')} required">
	<label class="control-label" for="m037Q2D">
		<g:message code="targetGR.m037Q2D.label" default="Q2 : Dissatisfied" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037Q2D" value="${fieldValue(bean: targetGRInstance, field: 'm037Q2D')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037Q3S', 'error')} required">
	<label class="control-label" for="m037Q3S">
		<g:message code="targetGR.m037Q3S.label" default="Q3 : Satisfied" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037Q3S" value="${fieldValue(bean: targetGRInstance, field: 'm037Q3S')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037Q3D', 'error')} required">
	<label class="control-label" for="m037Q3D">
		<g:message code="targetGR.m037Q3D.label" default="Q3 : Dissatisfied" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037Q3D" value="${fieldValue(bean: targetGRInstance, field: 'm037Q3D')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037Q4S', 'error')} required">
	<label class="control-label" for="m037Q4S">
		<g:message code="targetGR.m037Q4S.label" default="Q4 : Satisfied" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037Q4S" value="${fieldValue(bean: targetGRInstance, field: 'm037Q4S')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037Q4D', 'error')} required">
	<label class="control-label" for="m037Q4D">
		<g:message code="targetGR.m037Q4D.label" default="Q4 : Dissatisfied" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037Q4D" value="${fieldValue(bean: targetGRInstance, field: 'm037Q4D')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037Q5S', 'error')} required">
	<label class="control-label" for="m037Q5S">
		<g:message code="targetGR.m037Q5S.label" default="Q5 : Satisfied" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037Q5S" value="${fieldValue(bean: targetGRInstance, field: 'm037Q5S')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037Q5D', 'error')} required">
	<label class="control-label" for="m037Q5D">
		<g:message code="targetGR.m037Q5D.label" default="Q5 : Dissatisfied" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037Q5D" value="${fieldValue(bean: targetGRInstance, field: 'm037Q5D')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037Q6S', 'error')} required">
	<label class="control-label" for="m037Q6S">
		<g:message code="targetGR.m037Q6S.label" default="Q6 : Satisfied" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037Q6S" value="${fieldValue(bean: targetGRInstance, field: 'm037Q6S')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetGRInstance, field: 'm037Q6D', 'error')} required">
	<label class="control-label" for="m037Q6D">
		<g:message code="targetGR.m037Q6D.label" default="Q6 : Dissatisfied" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m037Q6D" value="${fieldValue(bean: targetGRInstance, field: 'm037Q6D')}" required=""/>
	</div>
</div>
    </div>
    </div>