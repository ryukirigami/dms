
<%@ page import="com.kombos.administrasi.TargetGR" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="targetGR_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div>&nbsp;<input type="checkbox" name="checkSelect" id="checkSelect" onclick="selectAll();"/>&nbsp;<g:message code="targetGR.m037TglBerlaku.label" default="Tanggal Berlaku" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037TargetUnitSales.label" default="Unit Sales" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037TargetCPUS.label" default="CPUS" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037TargetSBI.label" default="SBI" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037TargetSBE.label" default="SBE" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037TargetAmountSales.label" default="Sales Amount" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037COGSPart.label" default="Part" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037COGSOil.label" default="Oil" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037COGSSublet.label" default="Sublet" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037COGSLabor.label" default="Labor" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037COGSOverHead.label" default="Overhead" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037COGSDeprecation.label" default="Deprecation" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037COGSTotalSGA.label" default="Total SGA" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037TotalClaimItem.label" default="SW103 (Item)" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037TotalClaimAmount.label" default="SW103 (Amount)" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037TechReport.label" default="Tech Report" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037TWCLeadTime.label" default="TWC Load Time" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037YTDS.label" default="YTD-S" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037YTDD.label" default="YTD-D" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037ThisMonthS.label" default="This Month-S" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037ThisMonthD.label" default="This Month-D" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037Q1S.label" default="Q1-S" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037Q1D.label" default="Q1-D" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037Q2S.label" default="Q2-S" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037Q2D.label" default="Q2-D" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037Q3S.label" default="Q3-S" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037Q3D.label" default="Q3-D" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037Q4S.label" default="Q4-S" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037Q4D.label" default="Q4-D" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037Q5S.label" default="Q5-S" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037Q5D.label" default="Q5-D" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037Q6S.label" default="Q6-S" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetGR.m037Q6D.label" default="Q6-D" /></div>
			</th>

		
		</tr>
		<tr>
		

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037TglBerlaku" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m037TglBerlaku" value="date.struct">
					<input type="hidden" name="search_m037TglBerlaku_day" id="search_m037TglBerlaku_day" value="">
					<input type="hidden" name="search_m037TglBerlaku_month" id="search_m037TglBerlaku_month" value="">
					<input type="hidden" name="search_m037TglBerlaku_year" id="search_m037TglBerlaku_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m037TglBerlaku_dp" value="" id="search_m037TglBerlaku" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037TargetUnitSales" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037TargetUnitSales" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037TargetCPUS" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037TargetCPUS" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037TargetSBI" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037TargetSBI" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037TargetSBE" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037TargetSBE" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037TargetAmountSales" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037TargetAmountSales" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037COGSPart" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037COGSPart" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037COGSOil" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037COGSOil" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037COGSSublet" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037COGSSublet" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037COGSLabor" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037COGSLabor" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037COGSOverHead" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037COGSOverHead" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037COGSDeprecation" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037COGSDeprecation" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037COGSTotalSGA" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037COGSTotalSGA" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037TotalClaimItem" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037TotalClaimItem" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037TotalClaimAmount" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037TotalClaimAmount" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037TechReport" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037TechReport" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037TWCLeadTime" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037TWCLeadTime" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037YTDS" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037YTDS" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037YTDD" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037YTDD" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037ThisMonthS" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037ThisMonthS" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037ThisMonthD" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037ThisMonthD" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037Q1S" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037Q1S" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037Q1D" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037Q1D" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037Q2S" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037Q2S" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037Q2D" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037Q2D" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037Q3S" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037Q3S" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037Q3D" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037Q3D" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037Q4S" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037Q4S" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037Q4D" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037Q4D" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037Q5S" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037Q5S" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037Q5D" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037Q5D" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037Q6S" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037Q6S" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m037Q6D" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m037Q6D" class="search_init" />
				</div>
			</th>
		</tr>
	</thead>
</table>

<g:javascript>
var targetGRTable;
var reloadTargetGRTable;
$(function(){
	
	reloadTargetGRTable = function() {
		targetGRTable.fnDraw();
	}

	
	$('#search_m037TglBerlaku').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m037TglBerlaku_day').val(newDate.getDate());
			$('#search_m037TglBerlaku_month').val(newDate.getMonth()+1);
			$('#search_m037TglBerlaku_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			targetGRTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	targetGRTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	targetGRTable = $('#targetGR_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m037TglBerlaku",
	"mDataProp": "m037TglBerlaku",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037TargetUnitSales",
	"mDataProp": "m037TargetUnitSales",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037TargetCPUS",
	"mDataProp": "m037TargetCPUS",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037TargetSBI",
	"mDataProp": "m037TargetSBI",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037TargetSBE",
	"mDataProp": "m037TargetSBE",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037TargetAmountSales",
	"mDataProp": "m037TargetAmountSales",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037COGSPart",
	"mDataProp": "m037COGSPart",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037COGSOil",
	"mDataProp": "m037COGSOil",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037COGSSublet",
	"mDataProp": "m037COGSSublet",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037COGSLabor",
	"mDataProp": "m037COGSLabor",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037COGSOverHead",
	"mDataProp": "m037COGSOverHead",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037COGSDeprecation",
	"mDataProp": "m037COGSDeprecation",
	"aTargets": [11],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037COGSTotalSGA",
	"mDataProp": "m037COGSTotalSGA",
	"aTargets": [12],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037TotalClaimItem",
	"mDataProp": "m037TotalClaimItem",
	"aTargets": [13],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037TotalClaimAmount",
	"mDataProp": "m037TotalClaimAmount",
	"aTargets": [14],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037TechReport",
	"mDataProp": "m037TechReport",
	"aTargets": [15],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037TWCLeadTime",
	"mDataProp": "m037TWCLeadTime",
	"aTargets": [16],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037YTDS",
	"mDataProp": "m037YTDS",
	"aTargets": [17],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037YTDD",
	"mDataProp": "m037YTDD",
	"aTargets": [18],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037ThisMonthS",
	"mDataProp": "m037ThisMonthS",
	"aTargets": [19],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037ThisMonthD",
	"mDataProp": "m037ThisMonthD",
	"aTargets": [20],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037Q1S",
	"mDataProp": "m037Q1S",
	"aTargets": [21],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037Q1D",
	"mDataProp": "m037Q1D",
	"aTargets": [22],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037Q2S",
	"mDataProp": "m037Q2S",
	"aTargets": [23],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037Q2D",
	"mDataProp": "m037Q2D",
	"aTargets": [24],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037Q3S",
	"mDataProp": "m037Q3S",
	"aTargets": [25],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037Q3D",
	"mDataProp": "m037Q3D",
	"aTargets": [26],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037Q4S",
	"mDataProp": "m037Q4S",
	"aTargets": [27],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037Q4D",
	"mDataProp": "m037Q4D",
	"aTargets": [28],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037Q5S",
	"mDataProp": "m037Q5S",
	"aTargets": [29],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037Q5D",
	"mDataProp": "m037Q5D",
	"aTargets": [30],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037Q6S",
	"mDataProp": "m037Q6S",
	"aTargets": [31],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m037Q6D",
	"mDataProp": "m037Q6D",
	"aTargets": [32],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

//,
//
//{
//	"sName": "companyDealer",
//	"mDataProp": "companyDealer",
//	"aTargets": [33],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": false
//}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var m037TglBerlaku = $('#search_m037TglBerlaku').val();
						var m037TglBerlakuDay = $('#search_m037TglBerlaku_day').val();
						var m037TglBerlakuMonth = $('#search_m037TglBerlaku_month').val();
						var m037TglBerlakuYear = $('#search_m037TglBerlaku_year').val();
						
						if(m037TglBerlaku){
							aoData.push(
									{"name": 'sCriteria_m037TglBerlaku', "value": "date.struct"},
									{"name": 'sCriteria_m037TglBerlaku_dp', "value": m037TglBerlaku},
									{"name": 'sCriteria_m037TglBerlaku_day', "value": m037TglBerlakuDay},
									{"name": 'sCriteria_m037TglBerlaku_month', "value": m037TglBerlakuMonth},
									{"name": 'sCriteria_m037TglBerlaku_year', "value": m037TglBerlakuYear}
							);
						}
	
						var m037TargetUnitSales = $('#filter_m037TargetUnitSales input').val();
						if(m037TargetUnitSales){
							aoData.push(
									{"name": 'sCriteria_m037TargetUnitSales', "value": m037TargetUnitSales}
							);
						}
	
						var m037TargetCPUS = $('#filter_m037TargetCPUS input').val();
						if(m037TargetCPUS){
							aoData.push(
									{"name": 'sCriteria_m037TargetCPUS', "value": m037TargetCPUS}
							);
						}
	
						var m037TargetSBI = $('#filter_m037TargetSBI input').val();
						if(m037TargetSBI){
							aoData.push(
									{"name": 'sCriteria_m037TargetSBI', "value": m037TargetSBI}
							);
						}
	
						var m037TargetSBE = $('#filter_m037TargetSBE input').val();
						if(m037TargetSBE){
							aoData.push(
									{"name": 'sCriteria_m037TargetSBE', "value": m037TargetSBE}
							);
						}
	
						var m037TargetAmountSales = $('#filter_m037TargetAmountSales input').val();
						if(m037TargetAmountSales){
							aoData.push(
									{"name": 'sCriteria_m037TargetAmountSales', "value": m037TargetAmountSales}
							);
						}
	
						var m037COGSPart = $('#filter_m037COGSPart input').val();
						if(m037COGSPart){
							aoData.push(
									{"name": 'sCriteria_m037COGSPart', "value": m037COGSPart}
							);
						}
	
						var m037COGSOil = $('#filter_m037COGSOil input').val();
						if(m037COGSOil){
							aoData.push(
									{"name": 'sCriteria_m037COGSOil', "value": m037COGSOil}
							);
						}
	
						var m037COGSSublet = $('#filter_m037COGSSublet input').val();
						if(m037COGSSublet){
							aoData.push(
									{"name": 'sCriteria_m037COGSSublet', "value": m037COGSSublet}
							);
						}
	
						var m037COGSLabor = $('#filter_m037COGSLabor input').val();
						if(m037COGSLabor){
							aoData.push(
									{"name": 'sCriteria_m037COGSLabor', "value": m037COGSLabor}
							);
						}
	
						var m037COGSOverHead = $('#filter_m037COGSOverHead input').val();
						if(m037COGSOverHead){
							aoData.push(
									{"name": 'sCriteria_m037COGSOverHead', "value": m037COGSOverHead}
							);
						}
	
						var m037COGSDeprecation = $('#filter_m037COGSDeprecation input').val();
						if(m037COGSDeprecation){
							aoData.push(
									{"name": 'sCriteria_m037COGSDeprecation', "value": m037COGSDeprecation}
							);
						}
	
						var m037COGSTotalSGA = $('#filter_m037COGSTotalSGA input').val();
						if(m037COGSTotalSGA){
							aoData.push(
									{"name": 'sCriteria_m037COGSTotalSGA', "value": m037COGSTotalSGA}
							);
						}
	
						var m037TotalClaimItem = $('#filter_m037TotalClaimItem input').val();
						if(m037TotalClaimItem){
							aoData.push(
									{"name": 'sCriteria_m037TotalClaimItem', "value": m037TotalClaimItem}
							);
						}
	
						var m037TotalClaimAmount = $('#filter_m037TotalClaimAmount input').val();
						if(m037TotalClaimAmount){
							aoData.push(
									{"name": 'sCriteria_m037TotalClaimAmount', "value": m037TotalClaimAmount}
							);
						}
	
						var m037TechReport = $('#filter_m037TechReport input').val();
						if(m037TechReport){
							aoData.push(
									{"name": 'sCriteria_m037TechReport', "value": m037TechReport}
							);
						}
	
						var m037TWCLeadTime = $('#filter_m037TWCLeadTime input').val();
						if(m037TWCLeadTime){
							aoData.push(
									{"name": 'sCriteria_m037TWCLeadTime', "value": m037TWCLeadTime}
							);
						}
	
						var m037YTDS = $('#filter_m037YTDS input').val();
						if(m037YTDS){
							aoData.push(
									{"name": 'sCriteria_m037YTDS', "value": m037YTDS}
							);
						}
	
						var m037YTDD = $('#filter_m037YTDD input').val();
						if(m037YTDD){
							aoData.push(
									{"name": 'sCriteria_m037YTDD', "value": m037YTDD}
							);
						}
	
						var m037ThisMonthS = $('#filter_m037ThisMonthS input').val();
						if(m037ThisMonthS){
							aoData.push(
									{"name": 'sCriteria_m037ThisMonthS', "value": m037ThisMonthS}
							);
						}
	
						var m037ThisMonthD = $('#filter_m037ThisMonthD input').val();
						if(m037ThisMonthD){
							aoData.push(
									{"name": 'sCriteria_m037ThisMonthD', "value": m037ThisMonthD}
							);
						}
	
						var m037Q1S = $('#filter_m037Q1S input').val();
						if(m037Q1S){
							aoData.push(
									{"name": 'sCriteria_m037Q1S', "value": m037Q1S}
							);
						}
	
						var m037Q1D = $('#filter_m037Q1D input').val();
						if(m037Q1D){
							aoData.push(
									{"name": 'sCriteria_m037Q1D', "value": m037Q1D}
							);
						}
	
						var m037Q2S = $('#filter_m037Q2S input').val();
						if(m037Q2S){
							aoData.push(
									{"name": 'sCriteria_m037Q2S', "value": m037Q2S}
							);
						}
	
						var m037Q2D = $('#filter_m037Q2D input').val();
						if(m037Q2D){
							aoData.push(
									{"name": 'sCriteria_m037Q2D', "value": m037Q2D}
							);
						}
	
						var m037Q3S = $('#filter_m037Q3S input').val();
						if(m037Q3S){
							aoData.push(
									{"name": 'sCriteria_m037Q3S', "value": m037Q3S}
							);
						}
	
						var m037Q3D = $('#filter_m037Q3D input').val();
						if(m037Q3D){
							aoData.push(
									{"name": 'sCriteria_m037Q3D', "value": m037Q3D}
							);
						}
	
						var m037Q4S = $('#filter_m037Q4S input').val();
						if(m037Q4S){
							aoData.push(
									{"name": 'sCriteria_m037Q4S', "value": m037Q4S}
							);
						}
	
						var m037Q4D = $('#filter_m037Q4D input').val();
						if(m037Q4D){
							aoData.push(
									{"name": 'sCriteria_m037Q4D', "value": m037Q4D}
							);
						}
	
						var m037Q5S = $('#filter_m037Q5S input').val();
						if(m037Q5S){
							aoData.push(
									{"name": 'sCriteria_m037Q5S', "value": m037Q5S}
							);
						}
	
						var m037Q5D = $('#filter_m037Q5D input').val();
						if(m037Q5D){
							aoData.push(
									{"name": 'sCriteria_m037Q5D', "value": m037Q5D}
							);
						}
	
						var m037Q6S = $('#filter_m037Q6S input').val();
						if(m037Q6S){
							aoData.push(
									{"name": 'sCriteria_m037Q6S', "value": m037Q6S}
							);
						}
	
						var m037Q6D = $('#filter_m037Q6D input').val();
						if(m037Q6D){
							aoData.push(
									{"name": 'sCriteria_m037Q6D', "value": m037Q6D}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
