

<%@ page import="com.kombos.administrasi.TargetGR" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'targetGR.label', default: 'Target GR')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteTargetGR;

$(function(){ 
	deleteTargetGR=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/targetGR/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadTargetGRTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-targetGR" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="targetGR"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${targetGRInstance?.m037TglBerlaku}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037TglBerlaku-label" class="property-label"><g:message
					code="targetGR.m037TglBerlaku.label" default="Tanggal Berlaku(Unit)" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037TglBerlaku-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037TglBerlaku"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037TglBerlaku" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:formatDate date="${targetGRInstance?.m037TglBerlaku}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${targetGRInstance?.companyDealer}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="companyDealer-label" class="property-label"><g:message--}%
					%{--code="targetGR.companyDealer.label" default="Company Dealer" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="companyDealer-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${targetGRInstance}" field="companyDealer"--}%
								%{--url="${request.contextPath}/TargetGR/updatefield" type="text"--}%
								%{--title="Enter companyDealer" onsuccess="reloadTargetGRTable();" />--}%

                        %{--<g:fieldValue bean="${targetGRInstance}" field="companyDealer"/>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			
				<g:if test="${targetGRInstance?.m037TargetUnitSales}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037TargetUnitSales-label" class="property-label"><g:message
					code="targetGR.m037TargetUnitSales.label" default="Target Unit Sales(Unit)" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037TargetUnitSales-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037TargetUnitSales"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037TargetUnitSales" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037TargetUnitSales"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037TargetCPUS}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037TargetCPUS-label" class="property-label"><g:message
					code="targetGR.m037TargetCPUS.label" default="Target CPUS(Unit)" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037TargetCPUS-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037TargetCPUS"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037TargetCPUS" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037TargetCPUS"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037TargetSBI}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037TargetSBI-label" class="property-label"><g:message
					code="targetGR.m037TargetSBI.label" default="Target SBI(Unit)" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037TargetSBI-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037TargetSBI"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037TargetSBI" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037TargetSBI"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037TargetSBE}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037TargetSBE-label" class="property-label"><g:message
					code="targetGR.m037TargetSBE.label" default="Target SBE(Unit)" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037TargetSBE-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037TargetSBE"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037TargetSBE" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037TargetSBE"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037TargetAmountSales}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037TargetAmountSales-label" class="property-label"><g:message
					code="targetGR.m037TargetAmountSales.label" default="Target Sales(Amount)" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037TargetAmountSales-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037TargetAmountSales"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037TargetAmountSales" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037TargetAmountSales"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037COGSPart}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037COGSPart-label" class="property-label"><g:message
					code="targetGR.m037COGSPart.label" default="Part (Rp)" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037COGSPart-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037COGSPart"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037COGSPart" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037COGSPart"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037COGSOil}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037COGSOil-label" class="property-label"><g:message
					code="targetGR.m037COGSOil.label" default="Oil (Rp)" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037COGSOil-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037COGSOil"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037COGSOil" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037COGSOil"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037COGSSublet}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037COGSSublet-label" class="property-label"><g:message
					code="targetGR.m037COGSSublet.label" default="Sublet (Rp)" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037COGSSublet-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037COGSSublet"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037COGSSublet" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037COGSSublet"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037COGSLabor}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037COGSLabor-label" class="property-label"><g:message
					code="targetGR.m037COGSLabor.label" default="Labor (Rp)" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037COGSLabor-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037COGSLabor"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037COGSLabor" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037COGSLabor"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037COGSOverHead}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037COGSOverHead-label" class="property-label"><g:message
					code="targetGR.m037COGSOverHead.label" default="Over Head (Rp)" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037COGSOverHead-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037COGSOverHead"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037COGSOverHead" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037COGSOverHead"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037COGSDeprecation}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037COGSDeprecation-label" class="property-label"><g:message
					code="targetGR.m037COGSDeprecation.label" default="Deprecation (Rp)" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037COGSDeprecation-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037COGSDeprecation"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037COGSDeprecation" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037COGSDeprecation"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037COGSTotalSGA}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037COGSTotalSGA-label" class="property-label"><g:message
					code="targetGR.m037COGSTotalSGA.label" default="Total SGA (Rp)" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037COGSTotalSGA-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037COGSTotalSGA"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037COGSTotalSGA" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037COGSTotalSGA"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037TotalClaimItem}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037TotalClaimItem-label" class="property-label"><g:message
					code="targetGR.m037TotalClaimItem.label" default="Total Claim SW 103 (Items)" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037TotalClaimItem-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037TotalClaimItem"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037TotalClaimItem" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037TotalClaimItem"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037TotalClaimAmount}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037TotalClaimAmount-label" class="property-label"><g:message
					code="targetGR.m037TotalClaimAmount.label" default="Total Claim SW 103 (Amount)" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037TotalClaimAmount-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037TotalClaimAmount"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037TotalClaimAmount" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037TotalClaimAmount"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037TechReport}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037TechReport-label" class="property-label"><g:message
					code="targetGR.m037TechReport.label" default="Technical Report (Items)" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037TechReport-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037TechReport"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037TechReport" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037TechReport"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037TWCLeadTime}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037TWCLeadTime-label" class="property-label"><g:message
					code="targetGR.m037TWCLeadTime.label" default="TWC Lead Time (Day)" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037TWCLeadTime-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037TWCLeadTime"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037TWCLeadTime" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037TWCLeadTime"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037YTDS}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037YTDS-label" class="property-label"><g:message
					code="targetGR.m037YTDS.label" default="YTD : Statisfied" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037YTDS-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037YTDS"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037YTDS" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037YTDS"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037YTDD}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037YTDD-label" class="property-label"><g:message
					code="targetGR.m037YTDD.label" default="YTD : Disstatisfied" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037YTDD-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037YTDD"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037YTDD" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037YTDD"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037ThisMonthS}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037ThisMonthS-label" class="property-label"><g:message
					code="targetGR.m037ThisMonthS.label" default="This Month : Statisfied" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037ThisMonthS-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037ThisMonthS"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037ThisMonthS" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037ThisMonthS"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037ThisMonthD}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037ThisMonthD-label" class="property-label"><g:message
					code="targetGR.m037ThisMonthD.label" default="This Month : Disstatisfied" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037ThisMonthD-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037ThisMonthD"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037ThisMonthD" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037ThisMonthD"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037Q1S}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037Q1S-label" class="property-label"><g:message
					code="targetGR.m037Q1S.label" default="Q1 : Statisfied" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037Q1S-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037Q1S"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037Q1S" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037Q1S"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037Q1D}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037Q1D-label" class="property-label"><g:message
					code="targetGR.m037Q1D.label" default="Q1 : Disstatisfied" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037Q1D-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037Q1D"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037Q1D" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037Q1D"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037Q2S}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037Q2S-label" class="property-label"><g:message
					code="targetGR.m037Q2S.label" default="Q2 : Statisfied" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037Q2S-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037Q2S"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037Q2S" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037Q2S"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037Q2D}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037Q2D-label" class="property-label"><g:message
					code="targetGR.m037Q2D.label" default="Q2 : Disstatisfied" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037Q2D-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037Q2D"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037Q2D" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037Q2D"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037Q3S}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037Q3S-label" class="property-label"><g:message
					code="targetGR.m037Q3S.label" default="Q3 : Statisfied" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037Q3S-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037Q3S"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037Q3S" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037Q3S"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037Q3D}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037Q3D-label" class="property-label"><g:message
					code="targetGR.m037Q3D.label" default="Q3 : Disstatisfied" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037Q3D-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037Q3D"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037Q3D" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037Q3D"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037Q4S}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037Q4S-label" class="property-label"><g:message
					code="targetGR.m037Q4S.label" default="Q4 : Statisfied" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037Q4S-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037Q4S"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037Q4S" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037Q4S"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037Q4D}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037Q4D-label" class="property-label"><g:message
					code="targetGR.m037Q4D.label" default="Q4 : Disstatisfied" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037Q4D-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037Q4D"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037Q4D" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037Q4D"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037Q5S}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037Q5S-label" class="property-label"><g:message
					code="targetGR.m037Q5S.label" default="Q5 : Statisfied" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037Q5S-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037Q5S"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037Q5S" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037Q5S"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037Q5D}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037Q5D-label" class="property-label"><g:message
					code="targetGR.m037Q5D.label" default="Q5 : Disstatisfied" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037Q5D-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037Q5D"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037Q5D" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037Q5D"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037Q6S}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037Q6S-label" class="property-label"><g:message
					code="targetGR.m037Q6S.label" default="Q6 : Statisfied" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037Q6S-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037Q6S"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037Q6S" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037Q6S"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetGRInstance?.m037Q6D}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m037Q6D-label" class="property-label"><g:message
					code="targetGR.m037Q6D.label" default="Q6 : Disstatisfied" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m037Q6D-label">
						%{--<ba:editableValue
								bean="${targetGRInstance}" field="m037Q6D"
								url="${request.contextPath}/TargetGR/updatefield" type="text"
								title="Enter m037Q6D" onsuccess="reloadTargetGRTable();" />--}%
							
								<g:fieldValue bean="${targetGRInstance}" field="m037Q6D"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${targetGRInstance?.id}"
					update="[success:'targetGR-form',failure:'targetGR-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteTargetGR('${targetGRInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
