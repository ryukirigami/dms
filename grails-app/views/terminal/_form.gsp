<%@ page import="com.kombos.hrd.Terminal" %>



<div class="control-group fieldcontain ${hasErrors(bean: terminalInstance, field: 'companyDealer', 'error')} ">
	<label class="control-label" for="companyDealer">
		<g:message code="terminal.companyDealer.label" default="Company Dealer" />
		
	</label>
	<div class="controls">
	<g:select id="companyDealer" name="companyDealer.id" from="${com.kombos.administrasi.CompanyDealer.list()}" optionKey="id" required="" value="${terminalInstance?.companyDealer?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: terminalInstance, field: 'kodeTerminal', 'error')} ">
	<label class="control-label" for="kodeTerminal">
		<g:message code="terminal.kodeTerminal.label" default="Kode Terminal" />
		
	</label>
	<div class="controls">
	<g:textField name="kodeTerminal" value="${terminalInstance?.kodeTerminal}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: terminalInstance, field: 'namaTerminal', 'error')} ">
	<label class="control-label" for="namaTerminal">
		<g:message code="terminal.namaTerminal.label" default="Nama Terminal" />
		
	</label>
	<div class="controls">
	<g:textField name="namaTerminal" value="${terminalInstance?.namaTerminal}" />
	</div>
</div>

