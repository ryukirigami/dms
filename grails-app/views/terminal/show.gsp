

<%@ page import="com.kombos.hrd.Terminal" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'terminal.label', default: 'Terminal')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteTerminal;

$(function(){ 
	deleteTerminal=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/terminal/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadTerminalTable();
   				expandTableLayout('terminal');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-terminal" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="terminal"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${terminalInstance?.companyDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="companyDealer-label" class="property-label"><g:message
					code="terminal.companyDealer.label" default="Company Dealer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="companyDealer-label">
						%{--<ba:editableValue
								bean="${terminalInstance}" field="companyDealer"
								url="${request.contextPath}/Terminal/updatefield" type="text"
								title="Enter companyDealer" onsuccess="reloadTerminalTable();" />--}%
							
								<g:link controller="companyDealer" action="show" id="${terminalInstance?.companyDealer?.id}">${terminalInstance?.companyDealer?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${terminalInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="terminal.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${terminalInstance}" field="createdBy"
								url="${request.contextPath}/Terminal/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadTerminalTable();" />--}%
							
								<g:fieldValue bean="${terminalInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${terminalInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="terminal.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${terminalInstance}" field="dateCreated"
								url="${request.contextPath}/Terminal/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadTerminalTable();" />--}%
							
								<g:formatDate date="${terminalInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${terminalInstance?.kodeTerminal}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kodeTerminal-label" class="property-label"><g:message
					code="terminal.kodeTerminal.label" default="Kode Terminal" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kodeTerminal-label">
						%{--<ba:editableValue
								bean="${terminalInstance}" field="kodeTerminal"
								url="${request.contextPath}/Terminal/updatefield" type="text"
								title="Enter kodeTerminal" onsuccess="reloadTerminalTable();" />--}%
							
								<g:fieldValue bean="${terminalInstance}" field="kodeTerminal"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${terminalInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="terminal.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${terminalInstance}" field="lastUpdProcess"
								url="${request.contextPath}/Terminal/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadTerminalTable();" />--}%
							
								<g:fieldValue bean="${terminalInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${terminalInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="terminal.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${terminalInstance}" field="lastUpdated"
								url="${request.contextPath}/Terminal/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadTerminalTable();" />--}%
							
								<g:formatDate date="${terminalInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${terminalInstance?.namaTerminal}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="namaTerminal-label" class="property-label"><g:message
					code="terminal.namaTerminal.label" default="Nama Terminal" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="namaTerminal-label">
						%{--<ba:editableValue
								bean="${terminalInstance}" field="namaTerminal"
								url="${request.contextPath}/Terminal/updatefield" type="text"
								title="Enter namaTerminal" onsuccess="reloadTerminalTable();" />--}%
							
								<g:fieldValue bean="${terminalInstance}" field="namaTerminal"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${terminalInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="terminal.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${terminalInstance}" field="staDel"
								url="${request.contextPath}/Terminal/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadTerminalTable();" />--}%
							
								<g:fieldValue bean="${terminalInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${terminalInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="terminal.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${terminalInstance}" field="updatedBy"
								url="${request.contextPath}/Terminal/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadTerminalTable();" />--}%
							
								<g:fieldValue bean="${terminalInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('terminal');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${terminalInstance?.id}"
					update="[success:'terminal-form',failure:'terminal-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteTerminal('${terminalInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
