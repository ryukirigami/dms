

<%@ page import="com.kombos.finance.Journal" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'journal.label', default: 'Journal')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteJournal;

$(function(){ 
	deleteJournal=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/journal/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadJournalTable();
   				expandTableLayout('journal');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-journal" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="journal"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${journalInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="journal.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${journalInstance}" field="createdBy"
								url="${request.contextPath}/Journal/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadJournalTable();" />--}%
							
								<g:fieldValue bean="${journalInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="journal.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${journalInstance}" field="dateCreated"
								url="${request.contextPath}/Journal/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadJournalTable();" />--}%
							
								<g:formatDate date="${journalInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalInstance?.description}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="description-label" class="property-label"><g:message
					code="journal.description.label" default="Description" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="description-label">
						%{--<ba:editableValue
								bean="${journalInstance}" field="description"
								url="${request.contextPath}/Journal/updatefield" type="text"
								title="Enter description" onsuccess="reloadJournalTable();" />--}%
							
								<g:fieldValue bean="${journalInstance}" field="description"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalInstance?.docNumber}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="docNumber-label" class="property-label"><g:message
					code="journal.docNumber.label" default="Doc Number" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="docNumber-label">
						%{--<ba:editableValue
								bean="${journalInstance}" field="docNumber"
								url="${request.contextPath}/Journal/updatefield" type="text"
								title="Enter docNumber" onsuccess="reloadJournalTable();" />--}%
							
								<g:fieldValue bean="${journalInstance}" field="docNumber"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalInstance?.isApproval}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="isApproval-label" class="property-label"><g:message
					code="journal.isApproval.label" default="Is Approval" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="isApproval-label">
						%{--<ba:editableValue
								bean="${journalInstance}" field="isApproval"
								url="${request.contextPath}/Journal/updatefield" type="text"
								title="Enter isApproval" onsuccess="reloadJournalTable();" />--}%
							
								<g:fieldValue bean="${journalInstance}" field="isApproval"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalInstance?.journalCode}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="journalCode-label" class="property-label"><g:message
					code="journal.journalCode.label" default="Journal Code" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="journalCode-label">
						%{--<ba:editableValue
								bean="${journalInstance}" field="journalCode"
								url="${request.contextPath}/Journal/updatefield" type="text"
								title="Enter journalCode" onsuccess="reloadJournalTable();" />--}%
							
								<g:fieldValue bean="${journalInstance}" field="journalCode"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalInstance?.journalDate}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="journalDate-label" class="property-label"><g:message
					code="journal.journalDate.label" default="Journal Date" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="journalDate-label">
						%{--<ba:editableValue
								bean="${journalInstance}" field="journalDate"
								url="${request.contextPath}/Journal/updatefield" type="text"
								title="Enter journalDate" onsuccess="reloadJournalTable();" />--}%
							
								<g:formatDate date="${journalInstance?.journalDate}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalInstance?.journalType}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="journalType-label" class="property-label"><g:message
					code="journal.journalType.label" default="Journal Type" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="journalType-label">
						%{--<ba:editableValue
								bean="${journalInstance}" field="journalType"
								url="${request.contextPath}/Journal/updatefield" type="text"
								title="Enter journalType" onsuccess="reloadJournalTable();" />--}%
							
								<g:link controller="journalType" action="show" id="${journalInstance?.journalType?.id}">${journalInstance?.journalType?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="journal.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${journalInstance}" field="lastUpdProcess"
								url="${request.contextPath}/Journal/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadJournalTable();" />--}%
							
								<g:fieldValue bean="${journalInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="journal.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${journalInstance}" field="lastUpdated"
								url="${request.contextPath}/Journal/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadJournalTable();" />--}%
							
								<g:formatDate date="${journalInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="journal.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${journalInstance}" field="staDel"
								url="${request.contextPath}/Journal/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadJournalTable();" />--}%
							
								<g:fieldValue bean="${journalInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalInstance?.totalCredit}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="totalCredit-label" class="property-label"><g:message
					code="journal.totalCredit.label" default="Total Credit" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="totalCredit-label">
						%{--<ba:editableValue
								bean="${journalInstance}" field="totalCredit"
								url="${request.contextPath}/Journal/updatefield" type="text"
								title="Enter totalCredit" onsuccess="reloadJournalTable();" />--}%
							
								<g:fieldValue bean="${journalInstance}" field="totalCredit"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalInstance?.totalDebit}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="totalDebit-label" class="property-label"><g:message
					code="journal.totalDebit.label" default="Total Debit" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="totalDebit-label">
						%{--<ba:editableValue
								bean="${journalInstance}" field="totalDebit"
								url="${request.contextPath}/Journal/updatefield" type="text"
								title="Enter totalDebit" onsuccess="reloadJournalTable();" />--}%
							
								<g:fieldValue bean="${journalInstance}" field="totalDebit"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${journalInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="journal.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${journalInstance}" field="updatedBy"
								url="${request.contextPath}/Journal/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadJournalTable();" />--}%
							
								<g:fieldValue bean="${journalInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('journal');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${journalInstance?.id}"
					update="[success:'journal-form',failure:'journal-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteJournal('${journalInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
