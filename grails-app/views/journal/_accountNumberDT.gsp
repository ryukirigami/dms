<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<input type="hidden" id="accountNumber">

<table id="nomorKwitansi_table" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>

        %{--String accountNumber
        String accountName--}%

        <th style="border-bottom: none;padding: 5px;">
            Nomor Rekening
        </th>
        <th style="border-bottom: none;padding: 5px;">
            Nama Rekening
        </th>
    </tr>

    <tr>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_nomorRekening" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="nomorRekening" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_namaRekening" style="padding-top: 0px;position:relative; margin-top: 0px;width: 200px;">
                <input type="text" name="nomorRekening" class="search_init" />
            </div>
        </th>



    </tr>

    </thead>
</table>
<g:javascript>
var nomorAkunTable;
var reloadNomorAkunTable;
var setValues;
$(function(){

    setValues = function(el) {
        var noAkun = $(el).parent().parent().find("td.accountNumber span").html();

        $("#accountNumber").val(noAkun);
    }

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	nomorAkunTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

    if (nomorAkunTable) {

    } else {
        nomorAkunTable = $('#nomorKwitansi_table').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : true,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "accountNumbersDT")}",
		"aoColumns": [

{
	"sName": "accountNumber",
	"mDataProp": "accountNumber",
	"aTargets": [1],
	"bSortable": false,
	"bSearchable": true,
	"sWidth":"100px",
	"sClass": "accountNumber",
	"bVisible": true,
	"mRender": function ( data, type, row ) {
	    return "<input type='radio' checked='false' onclick='setValues(this);' name='accounts'><span>"+data+"</span>";
	}
},
{
    "sName": "accountName",
	"mDataProp": "accountName",
	"aTargets": [2],
	"bSortable": false,
	"bSearchable": true,
	"sWidth":"100px",
	"sClass": "accountName",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var kode = $('#filter_nomorRekening input').val();
                        var nama = $("#filter_namaRekening input").val();

						if(kode){
							aoData.push(
									{"name": 'sCriteria_nomorAkun', "value": kode}
							);
						}

						if (nama) {
						    aoData.push(
						        {"name": "sCriteria_namaAkun", "value": nama}
						    )
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
    }


});
</g:javascript>
