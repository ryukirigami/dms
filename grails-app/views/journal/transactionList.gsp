<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title>Jurnal Transaksi</title>
    <r:require modules="baseapplayout, baseapplist" />
    <r:require modules="baseapplayout" />
    <style>
    #filter_tanggalJurnal_to {margin-top: 10px;}
    </style>
    <g:render template="../menu/maxLineDisplay"/>
    <g:javascript>
        cetakJurnal = function(){
           var tanggal1 = $("#search_journalDate").val();
           var tanggal2 = $("#search_journalDate_to").val();
           if(!tanggal1 || !tanggal2){
            alert("Masukan Tanggal");
            return false
           }

           window.location = "${request.contextPath}/journal/printJurnalTransaksi?tanggal="+tanggal1+"&tanggal2="+tanggal2;

     }
    </g:javascript>
</head>

<body>
<div class="navbar box-header no-border">
    <span id="title" class="pull-left">Jurnal Transaksi</span>
</div>
<div class="box">
    <div class="span12" id="journal-table">
        <button id='cetakJurnal' onclick="cetakJurnal();" type="button" class="btn btn-primary">Cetak Jurnal</button>
    </div>
    <!-- END SPAN12 -->
    <div class="span12" id="journal-dataTable" style="margin-left: -10px;">
        <table id="journal_datatables2" cellpadding="0" cellspacing="0"
               border="0"
               class="display table table-striped table-bordered table-hover"
               width="100%">
            <!-- DataTables -->
            <thead>
            <tr>
                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="journal.journalCode.label" default="Journal Code" /></div>
                </th>
                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="journal.journalDate.label" default="Journal Date" /></div>
                </th>

                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="journal.description.label" default="Description" /></div>
                </th>


                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="journal.totalDebit.label" default="Total Debit" /></div>
                </th>

                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="journal.totalCredit.label" default="Total Credit" /></div>
                </th>
            </tr>
            <tr>
                <th style="border-top: none;padding: 5px;">
                    <div id="filter_journalCode" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                        <input type="text" name="search_journalCode" class="search_init" />
                    </div>
                </th>

                <th style="border-top: none;padding: 5px;">
                    <div id="filter_journalDate" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
                        <input type="hidden" name="search_journalDate" value="date.struct">
                        <input type="hidden" name="search_journalDate_day" id="search_journalDate_day" value="">
                        <input type="hidden" name="search_journalDate_month" id="search_journalDate_month" value="">
                        <input type="hidden" name="search_journalDate_year" id="search_journalDate_year" value="">
                        <input type="text" data-date-format="dd/mm/yyyy" name="search_journalDate_dp" value="" id="search_journalDate" placeholder="Date from.." class="search_init">

                        <input type="hidden" name="search_journalDate_to" value="date.struct">
                        <input type="hidden" name="search_journalDate_day_to" id="search_journalDate_day_to" value="">
                        <input type="hidden" name="search_journalDate_month_to" id="search_journalDate_month_to" value="">
                        <input type="hidden" name="search_journalDate_year_to" id="search_journalDate_year_to" value="">
                        <input type="text" data-date-format="dd/mm/yyyy" name="search_journalDate_dp_to" value="" id="search_journalDate_to" style="margin-top: 5px;" placeholder="Date to.." class="search_init">
                    </div>
                </th>

                <th style="border-top: none;padding: 5px;">
                    <div id="filter_description" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                        <input type="text" name="search_description" class="search_init" />
                    </div>
                </th>

                <th style="border-top: none;padding: 5px;">
                    <div id="filter_totalDebit" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                        <input type="text" name="search_totalDebit" class="search_init" />
                    </div>
                </th>

                <th style="border-top: none;padding: 5px;">
                    <div id="filter_totalCredit" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                        <input type="text" name="search_totalCredit" class="search_init" />
                    </div>
                </th>

            </tr>


            </thead>
        </table>
    </div>
    <div class="span12" id="journal-form" style="display: none;margin-left: 0px;border: none;"></div>
</div>

<g:javascript>
var journalTable;
var reloadJournalTable;
var show;
var closeWindow;
$(function(){
closeWindow = function() {
    $("#title").html("Jurnal Transaksi");
    $("#journal-dataTable").fadeIn("fast")
    $("#journal-table").fadeIn("fast");
    $('#journal-form').css("display","none");
    $('#journalDetail-table').css("display","none");
}
show = function(id) {
    var table = $('#journal-table');
        table.fadeOut();

    $("#title").html("Detail Jurnal Transaksi");
    $("#journal-dataTable").fadeOut()
    $('#journal-form').css("display","block");
    $('#journalDetail-table').css("display","block");
    jQuery("#spinner").fadeIn(1);
    $.get("${request.getContextPath()}/journal/showTransactionDetail", {
        id: id
    }, function(data) {
        loadFormInstance("journal", data);
    }).done(function() {
        jQuery("#spinner").fadeOut();
    })
}

reloadJournalTable = function() {
    journalTable.fnDraw();
}


$('#search_journalDate').datepicker().on('changeDate', function(ev) {
    var newDate = new Date(ev.date);
    $('#search_journalDate_day').val(newDate.getDate());
    $('#search_journalDate_month').val(newDate.getMonth()+1);
    $('#search_journalDate_year').val(newDate.getFullYear());
    $(this).datepicker('hide');
//    journalTable.fnDraw();
});

$('#search_journalDate_to').datepicker().on('changeDate', function(ev) {
    var newDate = new Date(ev.date);
    $('#search_journalDate_day_to').val(newDate.getDate());
    $('#search_journalDate_month_to').val(newDate.getMonth()+1);
    $('#search_journalDate_year_to').val(newDate.getFullYear());
    $(this).datepicker('hide');
    journalTable.fnDraw();
});


    $("th div input").bind('keypress', function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code == 13) {
        e.stopPropagation();
        journalTable.fnDraw();
        }
    });
    $("th div input").click(function (e) {
        e.stopPropagation();
    });

    journalTable = $('#journal_datatables2').dataTable({
    "sScrollX": "100%",
    "bScrollCollapse": true,
    "bAutoWidth" : false,
    "bPaginate" : true,
    "sInfo" : "",
    "sInfoEmpty" : "",
    "sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
    bFilter: true,
    "bStateSave": false,
    'sPaginationType': 'bootstrap',
    "fnInitComplete": function () {
    this.fnAdjustColumnSizing(true);
    },
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
    return nRow;
    },
    "bSort": true,
    "bProcessing": true,
    "bServerSide": true,
    "sServerMethod": "POST",
    "iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
    "sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
	"sName": "journalCode",
	"mDataProp": "journalCode",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true,
	"mRender": function ( data, type, row ) {
        return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="javascript:void(0);" onclick="show('+row['id']+');">'+data+'</a>';
	}
}
,



{
	"sName": "journalDate",
	"mDataProp": "journalDate",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "description",
	"mDataProp": "description",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "totalDebit",
	"mDataProp": "totalDebit",
	"aTargets": [8],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
	    if(data != null){
            return '<span class="pull-right">'+data+'</span>';
        }else{
            return '<span></span>';
        }
    },
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}
 ,
{
	"sName": "totalCredit",
	"mDataProp": "totalCredit",
	"aTargets": [7],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
        if(data != null)
        return '<span class="pull-right">'+data+'</span>';
        else
        return '<span></span>';
    },
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {


                        aoData.push(
                                {"name": 'journalTransaction', "value": true}
                        );
						var description = $('#filter_description input').val();
						if(description){
							aoData.push(
									{"name": 'sCriteria_description', "value": description}
							);
						}

						var docNumber = $('#filter_docNumber input').val();
						if(docNumber){
							aoData.push(
									{"name": 'sCriteria_docNumber', "value": docNumber}
							);
						}

						var journalCode = $('#filter_journalCode input').val();
						if(journalCode){
							aoData.push(
									{"name": 'sCriteria_journalCode', "value": journalCode}
							);
						}

						var journalDate = $('#search_journalDate').val();
						var journalDateDay = $('#search_journalDate_day').val();
						var journalDateMonth = $('#search_journalDate_month').val();
						var journalDateYear = $('#search_journalDate_year').val();

						if(journalDate){
							aoData.push(
									{"name": 'sCriteria_journalDate', "value": "date.struct"},
									{"name": 'sCriteria_journalDate_dp', "value": journalDate},
									{"name": 'sCriteria_journalDate_day', "value": journalDateDay},
									{"name": 'sCriteria_journalDate_month', "value": journalDateMonth},
									{"name": 'sCriteria_journalDate_year', "value": journalDateYear}
							);
						}

						var journalDateTo = $('#search_journalDate_to').val();
						var journalDateDayTo = $('#search_journalDate_day_to').val();
						var journalDateMonthTo = $('#search_journalDate_month_to').val();
						var journalDateYearTo = $('#search_journalDate_year_to').val();

						if (journalDateTo) {
						    aoData.push(
									{"name": 'sCriteria_journalDate_to', "value": "date.struct"},
									{"name": 'sCriteria_journalDate_dp_to', "value": journalDateTo},
									{"name": 'sCriteria_journalDate_day_to', "value": journalDateDayTo},
									{"name": 'sCriteria_journalDate_month_to', "value": journalDateMonthTo},
									{"name": 'sCriteria_journalDate_year_to', "value": journalDateYearTo}
							);
						}

						var totalCredit = $('#filter_totalCredit input').val();
						if(totalCredit){
							aoData.push(
									{"name": 'sCriteria_totalCredit', "value": totalCredit}
							);
						}

						var totalDebit = $('#filter_totalDebit input').val();
						if(totalDebit){
							aoData.push(
									{"name": 'sCriteria_totalDebit', "value": totalDebit}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							      $('td span.numeric').text(function(index, text) {
                                    return $.number(text,0);
                                    });
							   }
						});
		}
	});
});
</g:javascript>


</body>

</html>