<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title>Buku Besar</title>
    <r:require modules="baseapplayout" />
    <g:render template="../menu/maxLineDisplay"/>
    <g:javascript>
        var accountNumberDT;
        $(function() {

            var checkin = $('#filter_tanggalJurnal_from').datepicker({
                    onRender: function(date) {
                        return '';
                    }
                }).on('changeDate', function(ev) {
                            var newDate = new Date(ev.date)
                            newDate.setDate(newDate.getDate());
                            checkout.setValue(newDate);
                            checkin.hide();
                            $('#filter_tanggalJurnal_to')[0].focus();
                        }).data('datepicker');

                var checkout = $('#filter_tanggalJurnal_to').datepicker({
                    onRender: function(date) {
                        return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
                    }
                }).on('changeDate', function(ev) {
                            checkout.hide();
                        }).data('datepicker');


            accountNumberDT = function() {
                $("#accountNumberDT").modal("show");
            }

            $("#btnPilih").click(function() {
                $("#accountNumber").val($("#accountNumber").val());
                $("#accountNumberDT").modal("hide");
            })
            previewData = function(format){
               var tglAwal  = $('#filter_tanggalJurnal_from').val();
               var tglAkhir = $('#filter_tanggalJurnal_to').val();
               var nomorAkun = $('#accountNumber').val();
               var subType = $('#subType').val();
               var subLedger = $('#idSubledger').val();
               window.location = "${request.contextPath}/journal/printBukuBesar?format="+format+"&tglAwal="+tglAwal+"&tglAkhir="+tglAkhir+"&nomorAkun="+nomorAkun+"&subType="+subType+"&subLedger="+subLedger;
           }
        });

        function modalSubledgerBukuBesar() {
            var t = document.getElementById("subType");
            var cekST = t.options[t.selectedIndex].text;
            if(cekST=="Pilih Subtype"){
                alert("Pilih Subtype terlebih dahulu")
                return
            }
            $("#popupSubledgerContent").empty();
            $.ajax({type:'POST', url:'${request.contextPath}/subledgerModal?subtype='+cekST,
                success:function(data,textStatus){
                    $("#popupSubledgerContent").html(data);
                    $("#popupSubledgerModal").modal({
                        "backdrop" : "dynamic",
                        "keyboard" : true,
                        "show" : true
                    }).css({'width': '500px','margin-left': function () {return -($(this).width() / 2);}});
                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
        }

        function putSubledgerBB() {
            $('#subledger-table tbody').find('tr').each(function(k,v) {
                if ($(this).find('td:first-child input[type="radio"]').is(":checked")) {
                    var aData = subledgerTable.fnGetData(k);
                    var accountNumber = aData.accountNumber;
                    var id = aData.id;
                    $('#atasNama').val(accountNumber);
                    $('#idSubledger').val(id);
                }
            });
            $("#closeSubledger").click();
        }

        function changeSubtype(){
            $('#atasNama').val("");
            $('#idSubledger').val("");
        }
    </g:javascript>
</head>

<body>
<div class="navbar box-header no-border">
    <span id="title" class="pull-left">Buku Besar List</span>
</div><br>
<div class="box">
    <div class="span12" id="journal-table">
        <div class="row-fluid">
            <div class="span9">
                <div id="search-form" class="form-horizontal">
                    <fieldset class="form">
                        <div class="control-group fieldcontain">
                            <label for="filter_tanggalJurnal_from" class="control-label">
                                Tanggal Jurnal
                            </label>
                            <div class="controls">
                                <ba:datePicker id="filter_tanggalJurnal_from" name="filter_tanggalJurnal_from" precision="day" format="dd/MM/yyyy"  value="${params.dari != null ? params.sCriteria_journalDate_dp: new Date()}" />
                                &nbsp;&nbsp;&nbsp;s.d.&nbsp;&nbsp;&nbsp;
                                <ba:datePicker id="filter_tanggalJurnal_to" name="filter_tanggalJurnal_to" precision="day" format="dd/MM/yyyy"  value="${params.dari != null ? params.sCriteria_journalDate_dp_to: new Date()}" />

                            </div>

                        </div>
                        <div class="control-group fieldcontain">
                            <label for="accountNumber" class="control-label">
                                Nomor Akun
                            </label>
                            <div class="controls">
                                <g:textField name="accountNumber" id="accountNumber" value="${params.dari != null ? params.akun : ''}" style="width: 460px;" autocomplete="off"/>
                            </div>
                        </div>
                        <div class="control-group fieldcontain">
                            <label for="accountNumber" class="control-label">
                                Atas Nama
                            </label>
                            <div class="controls">
                                <g:select name="subType" id="subType" onchange="changeSubtype();" value="${params.dari != null ? params.subType : '' }" from="${com.kombos.finance.SubType.createCriteria().list {eq("staDel","0");order("description")}}" noSelection="['':'Pilih Subtype']" optionValue="description" optionKey="id" />
                                <g:textField name="atasNama" value="${params.dari != null ? params.nmSubLedger : ''}" id="atasNama" readonly="" /><a href="javascript:void(0);" onclick="modalSubledgerBukuBesar();"><i class="icon-search" id="btnSearchName"></i></a>
                                <g:hiddenField name="idSubledger" id="idSubledger" value="${params.dari != null ? params.subLedger : '' }" />
                            </div>
                        </div>
                        <div class="control-group fieldcontain">
                            <div class="controls">
                                <button onclick="findData();" class="btn btn-primary">Cari Data</button>
                                <button onclick="resetData();" class="btn btn-cancel">Reset Data</button>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div><br>
    <!-- END SPAN12 -->
    <div class="span12" id="journal-dataTable" style="margin-left: -5px; margin-top: 20px;">
        <table id="journal_datatables" cellpadding="0" cellspacing="0"
               border="0"
               class="display table table-striped table-bordered table-hover"
               width="100%">
            <!-- DataTables -->
            <thead>
            <tr>
                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="journal.journalCode.label" default="Journal Code" /></div>
                </th>
                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="journal.journalDate.label" default="Journal Date" /></div>
                </th>

                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="journal.description.label" default="Description" /></div>
                </th>


                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="journal.totalDebit.label" default="Debet" /></div>
                </th>

                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="journal.totalCredit.label" default="Credit" /></div>
                </th>

                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="journal.totalCredit.label" default="Total" /></div>
                </th>
            </tr>

        <tr>

            <th style="border-top: none;padding: 5px;">
            </th>

            <th style="border-top: none;padding: 5px;">
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_description" style="padding-top: 0px;position:relative; margin-top: 0px;width: 150px;">
                    <input type="text" name="search_description" class="search_init" value="${params.dari != null ? params.sCriteria_description : ''}" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_debet" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_debet" class="search_init" value="${params.dari != null ? params.sCriteria_debet : ''}" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_credit" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_credit" class="search_init" value="${params.dari != null ? params.sCriteria_credit : ''}" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
            </th>
        </tr>

        <tr>
            <th colspan="2"></th>
            <th>Saldo Awal</th>
            <th colspan="2"></th>
            <th id="saldoAwal" style="text-align: right">${params.saldoAwal}</th>
        </tr>
        <g:if test="${htmlData}">
            ${htmlData}
        </g:if>
        </thead>
            <tfoot>
                <tr>
                    <th></th>
                    <th></th>
                    <th>Saldo Akhir</th>
                    <th id="grandTotDebet" style="text-align: right">${params.grandTotDebet}</th>
                    <th id="grandTotKredit" style="text-align: right">${params.grandTotKredit}</th>
                    <th id="grandTot" style="text-align: right">${params.grandTot}</th>
                </tr>
            </tfoot>

        </table>
    </div>
    <div class="span12" id="journal-form" style="display: none;margin-left: 0px;border: none;"></div>
    <div>Jumlah : ${params.jumlahdata}</div><br>
    <g:field type="button" onclick="previewData('xls')" class="btn btn-primary create" name="preview"  value="${message(code: 'default.button.upload.label', default: 'Preview Excel')}" />
    <g:field type="button" onclick="previewData('pdf')" class="btn btn-primary create" name="preview"  value="${message(code: 'default.button.upload.label', default: 'Preview PDF')}" />
</div>

<g:javascript>
var closeWindowDetail;
var showDetail;
$(function(){

    $("th div input").bind('keypress', function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code == 13) {
        e.stopPropagation();
            findData();
        }
    });
    resetData = function() {
        toastr.success("Mohon Tunggu..");
        loadPath("journal/bukuBesar");
    }
    findData = function() {
            ${htmlData = ""};

            var url = "";
            $("#saldoAwal").html('0');
            $("#grandTotDebet").html('0');
            $("#grandTotKredit").html('0');
            $("#grandTot").html('0');

            var descriptionFilter = $('#filter_description input').val();
            if(descriptionFilter){
                url += "&sCriteria_description="+descriptionFilter;
            }

            var debetFilter = $('#filter_debet input').val();
            if(debetFilter){
                url += "&sCriteria_debet="+debetFilter;
            }

            var kreditFilter = $('#filter_credit input').val();
            if(kreditFilter){
                url += "&sCriteria_credit="+kreditFilter;
            }

            var journalDate = $('#filter_tanggalJurnal_from').val();

            if(journalDate){
                url += "&sCriteria_journalDate_dp="+journalDate;
            }

            var journalDateTo = $('#filter_tanggalJurnal_to').val();

            if (journalDateTo) {
                    url += "&sCriteria_journalDate_dp_to="+journalDateTo;
            }

            if (journalDate && journalDateTo && $("#accountNumber").val()) {
                    toastr.success("Mohon Tunggu Sebentar..");
                    var idSubledger = $('#idSubledger').val();
                    if(idSubledger){
                        url += "&subLedger="+idSubledger;
                        url += "&nmSubLedger="+$('#atasNama').val();
                    }
                    var subType = $('#subType').val();
                    if(subType){
                        url += "&subType="+subType;
                    }
                    url += "&akun="+$("#accountNumber").val();
                    loadPath("journal/bukuBesar?dari=cari"+url);
            }  else {
                alert("Mohon lengkapi field parameter pencarian terlebih dahulu");
                toastr.error("Data Belum Lengkap..");
            }
    }

    $('#accountNumber').typeahead({
        source: function (query, process) {
            return $.get('${request.contextPath}/franc/listAccount', { query: query }, function (data) {
                return process(data.options);
            });
        }
    });

    showDetail = function(id){
        $("#detailContent").empty();
        $.ajax({
            type:'POST',
            url:'${request.contextPath}/journal/showTransactionDetail',
            data : {id : id},
            success:function(data,textStatus){
                    $("#detailContent").html(data);
                    $("#detailModal").modal({
                        "backdrop" : "dynamic",
                        "keyboard" : true,
                        "show" : true
                    }).css({'width': '1100px','margin-left': function () {return -($(this).width() / 2);}});

            },
            error:function(XMLHttpRequest,textStatus,errorThrown){},
            complete:function(XMLHttpRequest,textStatus){
                $('#spinner').fadeOut();
            }
        });
    }

    closeWindowDetail = function() {
        $("#detailModal").hide();
    }

});

</g:javascript>
<div id="popupSubledgerModal" class="modal fade">
    <div class="modal-dialog" style="width: 500px;">
        <div class="modal-content" style="width: 500px;">
            <!-- dialog body -->
            <div class="modal-body" style="max-height: 480px;">
                %{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}%
                <div id="popupSubledgerContent">
                    <div class="iu-content"></div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="control-group">
                    <a href="javascript:void(0);" id="btnPilihSubledger" onclick="putSubledgerBB();" class="btn btn-primary">
                        Set
                    </a>
                    <a href="javascript:void(0);" class="btn cancel" id="closeSubledger" data-dismiss="modal">
                        Close
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="accountNumberDT" class="modal hide">
    <div class="modal-content">
        <div class="modal-header">

        </div>

        <div class="modal-body">
            <g:render template="accountNumberDT"/>
        </div>

        <div class="modal-footer">
            <a class="btn btn-primary" id="btnPilih">Pilih</a>
        </div>
    </div>
</div>
<div id="detailModal" class="modal fade">
    <div class="modal-dialog" style="width: 1090px;">
        <div class="modal-content" style="width: 1090px;">
            <div class="modal-body" style="max-height: 450px;">
                <div id="detailContent"/>
                <div class="iu-content"></div>

            </div>
        </div>
    </div>
</div>

</body>

</html>