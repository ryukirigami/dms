<%@ page import="com.kombos.hrd.Karyawan; com.kombos.customerprofile.HistoryCustomer; com.kombos.parts.Vendor; com.kombos.administrasi.VendorAsuransi; com.kombos.administrasi.CompanyDealer; com.kombos.maintable.Company; com.kombos.finance.Journal" %>

<div class="control-group fieldcontain ${hasErrors(bean: journalInstance, field: 'journalCode', 'error')} ">
    <label class="control-label" for="journalCode">
        <g:message code="journal.journalCode.label" default="Journal Code" />

    </label>
    <div class="controls">
        <g:textField readonly="readonly" name="journalCode" value="${journalInstance?.journalCode?journalInstance.journalCode:newCode}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: journalInstance, field: 'docNumber', 'error')} ">
    <label class="control-label" for="journalCode">
        <g:message code="journal.journalCode.label" default="Document Number" />

    </label>
    <div class="controls">
        <g:textField  name="docNumber" value="${journalInstance?.docNumber ? journalInstance?.docNumber : '-'}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: journalInstance, field: 'journalDate', 'error')} ">
    <label class="control-label" for="journalDate">
        <g:message code="journal.journalDate.label" default="Journal Date" />

    </label>
    <div class="controls">
        <ba:datePicker name="journalDate" precision="day" value="${journalInstance?.journalDate}" format="yyyy-MM-dd"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: journalInstance, field: 'description', 'error')} ">
	<label class="control-label" for="description">
		<g:message code="journal.description.label" default="Description" />
		
	</label>
	<div class="controls">
	<g:textArea name="description" value="${journalInstance?.description}" maxlength="1000" style="font-size: 15px; " rows="4" />
	</div>
</div>

<div class="control-group">
    <div class="controls">
        <a onclick="addDetailRow();" class="btn cancel" href="javascript:void(0);">Add</a>
    </div>
</div>


<div class="span11" id="journalDetail-table" style="display: block; margin-top: 20px;">
    <legend>
        <div class="pull-right">
            <span style="font-size: 11px;">Sisa Saldo
                <input type="text" class="numeric" style="padding: 2px; width: 80px; font-size: 11px; text-align: right;" readonly="" value="0" id="sisaSaldo">
            </span>
        </div>
    </legend>
    <table class="table table-bordered">
        <thead>
        <th style="display: none;">Detail ID</th>
        <th>No</th><th style="display: none;">Id Akun</th><th>Nomor Rekening</th>
        <th style="width: 170px;">Nama Rekening</th><th>Tipe Transaksi</th>
        <th>Jumlah</th><th style="display: none;">subType id</th>
        <th>Sub Type</th>
        <th>Sub Ledger</th>
        <th>Keterangan</th>
        <th>Aktifitas</th>
        </thead>
        <tbody>
        <g:if test="${journalInstance.journalDetail}">
            <g:each in="${journalInstance.journalDetail}" status="i" var="detail">
                <tr id="row_${i + 1}">
                    <td id="detailid_${i + 1}" style="display: none;">
                        ${detail.id}
                    </td>
                    <td id="nomorurut_${i + 1}">
                        ${i+1}
                    </td>
                    <td id="accountnumberid_${i + 1}" style="display: none;">
                        ${detail.accountNumber.id}
                    </td>
                    <td>
                        <input id="nomorrekening_${i + 1}" type="text" readonly="readonly" value="${detail.accountNumber.accountNumber}" style="width: 130px">
                        <a style="margin: 5px;" onclick="showDetail(${i + 1}, 'accountNumber');" href="javascript:void(0);">
                            <i class="icon-search"></i>
                        </a>
                    </td>
                    <td id="namarekening_${i + 1}" >
                        <span style="width: 170px;">${detail.accountNumber.accountName}</span>
                    </td>
                    <td>
                        <select id="tipetransaksi_${i + 1}" style="width: 100px">
                            %{
                                if(detail.journalTransactionType=="TJ"){
                                    if(detail.creditAmount>0){
                                        detail.journalTransactionType = 'Kredit'
                                    }else{
                                        detail.journalTransactionType = 'Debet'
                                    }
                                }
                            }%
                            <option value="${detail.journalTransactionType}">${detail.journalTransactionType}</option>
                            <option value="${detail.journalTransactionType.equals("Kredit")?"Debet": "Kredit"}">
                                ${detail.journalTransactionType.equals("Kredit")?"Debet": "Kredit"}
                            </option>
                        </select>
                    </td>
                    <td>
                        <input type="text" class="numeric" id="jumlah_${i + 1}" value="${detail.journalTransactionType.equals("Kredit")?detail.creditAmount:detail.debitAmount}" maxlength="12"  style="width: 130px">
                    </td>
                    <td>
                        <g:select name="subtypeid_" id="subtypeid_${i + 1}" noSelection="['':'Pilih SubType']" from="${com.kombos.finance.SubType.createCriteria().list {eq("staDel","0")}}" optionValue="subType" optionKey="subType" value="${detail.subType?.subType}"  style="width: 150px"/>
                    <td><input type="hidden" id="idSubledger_${i+1}" value="${detail?.subLedger}" /> <input id="subLedger_${i+1}" type="text" style="width: 170px" readonly value="${detail?.subLedger}"/><a href="javascript:void(0);" onclick="modalSubledger('${i+1}');"><i class="icon-search" id="btnSearchName"></i></a></td>
                    <td>
                        <textarea id="desc_${i + 1}" style="width: 150px; height: 18px;" >${detail?.description}</textarea>
                    </td>
                    <g:if test="${detail?.subType}">
                        <g:if test="${detail?.subType?.subType?.toString()?.toUpperCase()?.contains('CABANG')}">
                            <g:javascript>
                                <%
                                    def cbg = detail?.subLedger?.toString()?.replace(" ","")?.isNumber() ? CompanyDealer?.get(detail?.subLedger) : null
                                    String namacab = cbg ? cbg?.m011NamaWorkshop : ""
                                %>
                                $("#subLedger_${i+1}").val('<%print(namacab)%>')
                            </g:javascript>
                        </g:if>
                        <g:if test="${detail?.subType?.subType?.toString()?.toUpperCase()?.contains('VENDOR')}">
                            <g:javascript>
                                <%
                                    def vend = detail?.subLedger?.toString()?.replace(" ","")?.isNumber() ? Vendor?.get(detail?.subLedger) : null
                                    String namaven = vend ? vend?.m121Nama : ""
                                %>
                                $("#subLedger_${i+1}").val('<%print(namaven)%>')
                            </g:javascript>
                        </g:if>
                        <g:if test="${detail?.subType?.subType?.toString()?.toUpperCase()?.contains('ASURANSI')}">
                            <g:javascript>
                                <%
                                    def asr = detail?.subLedger?.toString()?.replace(" ","")?.isNumber() ? VendorAsuransi?.get(detail?.subLedger) : null
                                    String namaasr = asr ? asr?.m193Nama : ""
                                %>
                                $("#subLedger_${i+1}").val('<%print(namaasr)%>')
                            </g:javascript>
                        </g:if>
                        <g:if test="${detail?.subType?.subType?.toString()?.toUpperCase()==('CUSTOMER')}">
                            <g:javascript>
                                <%
                                    def cst = detail?.subLedger?.toString()?.replace(" ","")?.isNumber() ? HistoryCustomer?.get(detail?.subLedger) : null
                                    String namacst = cst ? cst?.fullNama : ""
                                %>
                                $("#subLedger_${i+1}").val('<%print(namacst)%>')
                            </g:javascript>
                        </g:if>
                        <g:if test="${detail?.subType?.subType?.toString()?.toUpperCase()?.contains('KARYAWAN')}">
                            <g:javascript>
                                <%
                                    def kry = detail?.subLedger?.toString()?.replace(" ","")?.isNumber() ? Karyawan?.get(detail?.subLedger) : null
                                    String namakar = kry ? kry?.nama : ""
                                %>
                                $("#subLedger_${i+1}").val('<%print(namakar)%>')
                            </g:javascript>
                        </g:if>
                        <g:if test="${detail?.subType?.subType?.toString()?.toUpperCase()?.contains('CUSTOMER COMPANY')}">
                            <g:javascript>
                                <%
                                    def cmp = detail?.subLedger?.toString()?.replace(" ","")?.isNumber() ? Company?.get(detail?.subLedger) : null
                                    String cmpname = cmp ? cmp?.namaPerusahaan : ""
                                %>
                                $("#subLedger_${i+1}").val('<%print(cmpname)%>')
                            </g:javascript>
                        </g:if>
                    </g:if>
                    <td style="text-align: center;">
                        <a href="javascript:void(0);" onClick="performDeletion(${i + 1},${detail.id},${journalInstance.id});">
                            <i class="icon-trash"></i>
                        </a>
                    </td>
                </tr>
            </g:each>
        </g:if>
        </tbody>
    </table>
</div>
