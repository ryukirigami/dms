<%@ page import="com.kombos.finance.Journal" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'journal.label', default: 'Journal Memorial')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<g:javascript>
		$(function() {
			recalculate = function(e) {
				if ($("#journalDetail-table table tbody").html().length > 20) {
					var totalDebit = new Number(0);
					var totalKredit = new Number(0);
					var count = 0;
					$("#journalDetail-table table tbody").find("tr").each(function(k,v) {
						count = $(this).attr("id").split("_")[1];
						if ($("#tipetransaksi_" + count).val() === "Debet") {
							totalDebit += new Number($("#jumlah_" + count).autoNumeric("get"));
						} else {
							totalKredit += new Number($("#jumlah_" + count).autoNumeric("get"));
						}
					});
					sisaSaldo = totalDebit - totalKredit;
					$("#sisaSaldo").autoNumeric("set", sisaSaldo);
				}
			};
			$(".numeric").autoNumeric("init", {
				vMin: '-999999999999.99',
				vMax: '999999999999.99'
			});

			$("#journalDetail-table table tbody").on("focusout", "tr td input.jumlah", function() {
				recalculate();
			});
		})
	</g:javascript>
	<body>
		<div id="edit-journal" class="content scaffold-edit" role="main">
			<legend><g:message code="default.edit.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${journalInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${journalInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form method="post" >--}%
			<g:formRemote class="form-horizontal" name="edit" on404="alert('not found!');" onSuccess="reloadJournalTable();" update="journal-form"
				url="[controller: 'journal', action:'update']">
				<g:hiddenField name="id" value="${journalInstance?.id}" />
				<g:hiddenField name="version" value="${journalInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons controls">
                    <a class="btn cancel" href="javascript:void(0);"
                       onclick="refresh();"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
                    <a onclick="performSave(${journalInstance?.id});" class="btn btn-primary create" href="javascript:void(0);">Update</a>
				</fieldset>
			</g:formRemote>
			%{--</g:form>--}%
		</div>
	</body>
</html>
