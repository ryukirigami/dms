<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title>Jurnal Transaksi</title>
    <r:require modules="baseapplayout" />
    <g:render template="../menu/maxLineDisplay"/>
    <g:javascript>
        $(function() {
            var checkin = $('#filter_tanggalJurnal_from').datepicker({
                    onRender: function(date) {
                        return '';
                    }
                }).on('changeDate', function(ev) {
                            var newDate = new Date(ev.date)
                            newDate.setDate(newDate.getDate());
                            checkout.setValue(newDate);
                            checkin.hide();
                            $('#filter_tanggalJurnal_to')[0].focus();
                        }).data('datepicker');

                var checkout = $('#filter_tanggalJurnal_to').datepicker({
                    onRender: function(date) {
                        return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
                    }
                }).on('changeDate', function(ev) {
                            checkout.hide();
                        }).data('datepicker');



            previewData = function(format){
               var tglAwal  = $('#filter_tanggalJurnal_from').val();
               var tglAkhir = $('#filter_tanggalJurnal_to').val();
               var journalCode = $('#journalCode').val();
               window.location = "${request.contextPath}/journal/printJurnalTransaksi?format="+format+"&tglAwal="+tglAwal+"&tglAkhir="+tglAkhir+"&journalCode="+journalCode;
           }
        });


    </g:javascript>
</head>

<body>
<div class="navbar box-header no-border">
    <span id="title" class="pull-left">Jurnal Transaksi List</span>
</div><br>
<div class="box">
    <div class="span12" id="journal-table">
        <div class="row-fluid">
            <div class="span9">
                <div id="search-form" class="form-horizontal">
                    <fieldset class="form">
                        <div class="control-group fieldcontain">
                            <label for="journalType" class="control-label">
                                Tipe Jurnal
                            </label>
                            <div class="controls">
                                <select name="journalType" id="journalType">
                                    <option value="TJ" ${params.sCriteria_journalType!='MJ'?'selected':''} >Jurnal Transaksi</option>
                                    <option value="MJ" ${params.sCriteria_journalType=='MJ'?'selected':''}>Jurnal Memorial</option>
                                </select>
                            </div>
                        </div>
                        <div class="control-group fieldcontain">
                            <label for="journalCode" class="control-label">
                                Kode Jurnal
                            </label>
                            <div class="controls">
                                <input type="text" name="journalCode" id="journalCode" style="width: 300px;" value="${params.dari != null ? params.journalCode : ''}" autocomplete="off"/>
                            </div>
                        </div>
                        <div class="control-group fieldcontain">
                            <label for="journalCode" class="control-label">
                                Tanggal Jurnal
                            </label>

                            <div class="controls">
                                <ba:datePicker id="filter_tanggalJurnal_from" name="filter_tanggalJurnal_from" precision="day" format="dd/MM/yyyy"  value="${params.dari != null ? params.sCriteria_journalDate_dp: ''}" />
                                &nbsp;&nbsp;&nbsp;s.d.&nbsp;&nbsp;&nbsp;
                                <ba:datePicker id="filter_tanggalJurnal_to" name="filter_tanggalJurnal_to" precision="day" format="dd/MM/yyyy"  value="${params.dari != null ? params.sCriteria_journalDate_dp_to: ''}" />
                            </div>
                        </div>

                         <div class="control-group fieldcontain">
                            <div class="controls">
                                <button onclick="findData();" class="btn btn-primary">Cari Jurnal</button>
                                <button onclick="resetData();" class="btn btn-cancel">Reset Data</button>
                            </div>
                        </div><br>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
    <!-- END SPAN12 -->
    <div class="span12" id="journal-dataTable" style="margin-left: -5px; margin-top: 10px;">
        <table id="journal_datatables" cellpadding="0" cellspacing="0"
               border="0"
               class="display table table-striped table-bordered table-hover"
               width="100%">
            <!-- DataTables -->
            <thead>
            <tr>
                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="journal.journalCode.label" default="Journal Code" /></div>
                </th>
                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="journal.journalDate.label" default="Journal Date" /></div>
                </th>

                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="journal.description.label" default="Description" /></div>
                </th>


                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="journal.totalDebit.label" default="Debet" /></div>
                </th>

                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="journal.totalCredit.label" default="Credit" /></div>
                </th>

            </tr>

        <tr>

            <th style="border-top: none;padding: 5px;">
            </th>

            <th style="border-top: none;padding: 5px;">
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_description" style="padding-top: 0px;position:relative; margin-top: 0px;width: 350px;">
                    <input type="text" name="search_description" class="search_init" value="${params.dari != null ? params.sCriteria_description : ''}" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_debet" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_debet" class="search_init" value="${params.dari != null ? params.sCriteria_debet : ''}" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_credit" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_credit" class="search_init" value="${params.dari != null ? params.sCriteria_credit : ''}" />
                </div>
            </th>

         </tr>

         <g:if test="${htmlData}">
            ${htmlData}
        </g:if>
        </thead>
        </table>
    </div>
    <div class="span12" id="journal-form" style="display: none;margin-left: 0px;border: none;"></div>
    <div>Jumlah : ${params.jumlahdata}</div><br>
    <div ${params.sCriteria_journalType=='MJ'?'style="display:none"':''} >
        <g:field type="button" onclick="previewData('xls')" class="btn btn-primary create" name="preview"  value="${message(code: 'default.button.upload.label', default: 'Preview Excel')}" />
        <g:field type="button" onclick="previewData('pdf')" class="btn btn-primary create" name="preview"  value="${message(code: 'default.button.upload.label', default: 'Preview PDF')}" />
    </div>
</div>

<g:javascript>
var closeWindowDetail;
var showDetail;
$(function(){

    $("div input").bind('keypress', function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code == 13) {
        e.stopPropagation();
            findData();
        }
    });

    resetData = function() {
        toastr.success("Mohon Tunggu..");
        loadPath("journal/jurnalTransaksi");
    }

    findData = function() {
            ${htmlData = ""};

            var url = "";

            var descriptionFilter = $('#filter_description input').val();
            if(descriptionFilter){
                url += "&sCriteria_description="+descriptionFilter;
            }

            var debetFilter = $('#filter_debet input').val();
            if(debetFilter){
                url += "&sCriteria_debet="+debetFilter;
            }

            var kreditFilter = $('#filter_credit input').val();
            if(kreditFilter){
                url += "&sCriteria_credit="+kreditFilter;
            }

            var journalDate = $('#filter_tanggalJurnal_from').val();

            if(journalDate){
                url += "&sCriteria_journalDate_dp="+journalDate;
            }

            var journalDateTo = $('#filter_tanggalJurnal_to').val();

            if (journalDateTo) {
                    url += "&sCriteria_journalDate_dp_to="+journalDateTo;
            }

            var journalCode = $('#journalCode').val();

            if (journalCode) {
                    url += "&journalCode="+journalCode;
            }


            if (url) {
                    var journalType = $('select[name=journalType] option:selected').val();
                    url += "&sCriteria_journalType="+journalType;
                    toastr.success("Mohon Tunggu Sebentar..");
                    loadPath("journal/jurnalTransaksi?dari=cari"+url);
            }  else {
                alert("Mohon lengkapi field parameter pencarian terlebih dahulu");
                toastr.error("Data Belum Lengkap..");
            }


    }

    showDetail = function(id){
        $("#detailContent").empty();
        $.ajax({
            type:'POST',
            url:'${request.contextPath}/journal/showTransactionDetail',
            data : {id : id},
            success:function(data,textStatus){
                    $("#detailContent").html(data);
                    $("#detailModal").modal({
                        "backdrop" : "dynamic",
                        "keyboard" : true,
                        "show" : true
                    }).css({'width': '1100px','margin-left': function () {return -($(this).width() / 2);}});

            },
            error:function(XMLHttpRequest,textStatus,errorThrown){},
            complete:function(XMLHttpRequest,textStatus){
                $('#spinner').fadeOut();
            }
        });
    }

    closeWindowDetail = function() {
        $("#detailModal").hide();
    }

});

</g:javascript>
 <div id="detailModal" class="modal fade">
    <div class="modal-dialog" style="width: 1090px;">
        <div class="modal-content" style="width: 1090px;">
            <div class="modal-body" style="max-height: 450px;">
                <div id="detailContent"/>
                <div class="iu-content"></div>

            </div>
        </div>
    </div>
</div>

</body>

</html>