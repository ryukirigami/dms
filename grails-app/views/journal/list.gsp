
<%@ page import="com.kombos.finance.Journal" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'journal.label', default: 'Journal Memorial')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist, autoNumeric" />
		<g:javascript>
			var show,edit,addDetailRow, showDetail, putAccount, performSave,cekSave, refresh, performDeletion, recalculate;
			var sisaSaldo = new Number(0);
			$(function(){
				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :
                            var table = $('#journal-table');
		                    table.fadeOut();
                            $('#journal-form').css("display","block");
                            $('#journalDetail-table').css("display","block");
                            <g:remoteFunction action="create"
                              onLoading="jQuery('#spinner').fadeIn(1);"
                              onSuccess="loadFormInstance('journal', data, textStatus);"
                              onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :
                                    bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
                                        function(result){
                                            if(result){
                                                massDelete();
                                            }
                                        });

                        break;
				   }    
				   return false;
				});
				show = function(id) {
                    var table = $('#journal-table');
                        table.fadeOut();

                    $("#title").html("Detail Jurnal Transaksi");
                    $("#journal-dataTable").fadeOut()
                    $('#journal-form').css("display","block");
                    $('#journalDetail-table').css("display","block");
                    jQuery("#spinner").fadeIn(1);
                    $.get("${request.getContextPath()}/journal/showTransactionDetail", {
                        id: id,dari : 3
                    }, function(data) {
                        loadFormInstance("journal", data);
                    }).done(function() {
                        jQuery("#spinner").fadeOut();
                    })
                }
                massDelete = function() {
                    var recordsToDelete = [];
                    $("#journal-table tbody .row-select").each(function() {
                        if(this.checked){
                            var id = $(this).next("input:hidden").val();
                            recordsToDelete.push(id);
                        }
                    });
                    var json = JSON.stringify(recordsToDelete);

                    $.ajax({
                        url:'${request.contextPath}/journal/deleteJurnal',
                        type: "POST", // Always use POST when deleting data
                        data: { ids: json },
                        success:function(data,textStatus){
                            if(data=="not"){
                                alert("GAGAL")
                            }else{
                                toastr.success("SUKSES");
                            }
                        },
                        complete: function(xhr, status) {
                            reloadJournalTable();
                        }
                    });

                }
                edit = function(id) {
                  var table = $('#journal-table');
                        table.fadeOut();
				    $("#journal-dataTable").fadeOut();
                    $('#journal-form').css("display","block");
                    $('#journalDetail-table').css("display","block");
                    jQuery('#spinner').fadeIn(1);
                    $.get('${request.getContextPath()}/journal/edit/' + id, function(data) {
                        loadFormInstance('journal', data);

                    }).done(function() {
                        jQuery('#spinner').fadeOut();
                    });
				};
				addDetailRow = function() {
				    var $tbl = $("#journalDetail-table table tbody");
				    var count = new Number(0);

				    if ($tbl.html().length > 20) {
				        count = new Number($tbl.find('tr:last-child').attr("id").split("_")[1]);
				    }

				    var colDetailId = '<td id="detailid_'+(count + 1)+'" style="display:none;"></td>',
				        colNomor = '<td id="nomorurut_'+(count + 1)+'">'+(count + 1)+' </td>',
				        colAkunId = '<td id="accountnumberid_'+(count+1)+'" style="display: none;"></td>',
				        colNomorRekening = '<td>' +'<input id="nomorrekening_'+(count + 1)+'" type="text" style="width: 130px;" readOnly ><a href="javascript:void(0);" onClick="showDetail('+(count+1)+', \'accountNumber\');" style="margin: 5px;">' +'<i class="icon-search"></i></a></td>',
				        colNamaRekening = '<td id="namarekening_'+(count+1)+'" style="width: 170px;"><span style="width: 170px;"></span></td>',
                        colTipeTransaksi = '<td><select id="tipetransaksi_'+(count + 1)+'"  style="width:100px"><option value="Debet">Debet</option><option value="Kredit">Kredit</option></select></td>',
                        colJumlah = '<td><input id="jumlah_'+(count + 1)+'" type="text" class="numeric jumlah" style="width: 130px;"/></td>',
                        subledger ='<td><input type="hidden" id="idSubledger'+(count + 1)+'" ><input id="subLedger_'+(count + 1)+'" readonly="" type="text" style="width: 170px;"><a href="javascript:void(0);" onclick="modalSubledger('+(count + 1)+');"><i class="icon-search" id="btnSearchName"></i></a></td>',
                        colSubType = '<td><select onchange="changeValue('+(count + 1)+')" id="subtypeid_'+(count + 1)+'" style="width: 150px;">' +
                            ' <option value="">-Pilih Subtype-</option>'
                                     <g:each in="${com.kombos.finance.SubType.createCriteria().list {eq("staDel","0");order("description")}}">
                                         +'<option value="${it.subType}">${it.subType}</option>'
                                     </g:each>
                                    +'</select>',
                        colDesc = '<td><textarea id="desc_'+(count + 1)+'" style="width: 150px; height:18px;" /></td>';
                    $tbl.append('<tr id="row_'+(count+1)+'">'+colDetailId + colNomor + colAkunId + colNomorRekening + colNamaRekening + colTipeTransaksi + colJumlah + colSubType +subledger+colDesc+  '<td style="text-align: center;"><a href="javascript:void(0);" onClick="performDeletion('+(count+1)+');" style="magin: 5px;"><i class="icon-trash"></i></a></td></tr>');
                    $(".numeric").autoNumeric("init", {
                        vMin: '-99999999999.99',
                        vMax: '9999999999999.99'
                    });
				};
                showDetail = function(idx, type) {
                    if(type=="accountNumber"){
                        showModalAccount(idx);
                    }else{
                    var html = "";
                    $.get("${request.contextPath}/journal/showDetail", {
                      type: type
                    }, function(data) {
                            for (var i = 0; i < data.length; i++) {
                                html += "<tr>";
                                html += '<td><input type="radio" name="radio"></td>';
                                html += '<td id="id">'+data[i].id+'</td>';
                                html += '<td id="subType">' + data[i].subType + '</td>';
                                html += '<td id="description">' + data[i].description + '</td>';
                                html +="</tr>";
                            }
                            $('#detailSubType-table tbody').html(html);
                            $("#idx").val(idx);
                            $('#detailSubType-modal').modal('show');
                         });
                    }
                };
                showModalAccount = function(idx) {
                    $("#popupAccountNumberContent").empty();
                    $.ajax({type:'POST', url:'${request.contextPath}/mappingJurnalModal',
                        success:function(data,textStatus){
                                $("#popupAccountNumberContent").html(data);
                                $("#popupAccountNumberModal").modal({
                                    "backdrop" : "dynamic",
                                    "keyboard" : true,
                                    "show" : true
                                }).css({'width': '600px','margin-left': function () {return -($(this).width() / 2);}});

                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
                    $("#idx").val(idx);
				};
                changeValue = function(id){
                     $('#subLedger_' + id).val("");
                     $('#idSubledger' + id).val("");
                }
				putAccount = function(type) {
                    if (type === "subType") {
                       $('#detailSubType-table tbody').find('tr').each(function(k,v) {
                           if ($(this).find('td:first-child input[type="radio"]').is(":checked")) {
                               var subTypeId = $(this).find('td#id').html();
                               var subType = $(this).find('td#subType').html();
                               var description = $(this).find('td#description').html();
                               var idx = $('#idx').val();

                               $('#subtypeid_'+idx).val(subTypeId);
                               $('#subtype_'+idx).val(subType);

                               $('#detailSubType-modal').modal("hide");
                            }
				        });
                    } else {
                       var akunNeedSubtype = "PIUTANG USAHA, PIUTANG LAIN-LAIN, PIUTANG SEWA, PIUTANG KARYAWAN, PIUTANG DAGANG, ";
                       akunNeedSubtype += "HUTANG USAHA, HUTANG LAIN-LAIN UMUM, ";
                       akunNeedSubtype += "U.M SERVICE PERBAIKAN UMUM, U.M SERVICE PERBAIKAN BODY, ";
                       akunNeedSubtype += "U.M BIAYA INSENTIF,  U.M BIAYA PENGGANTIAN CUTI, ";
                       akunNeedSubtype += "U.M.J.TRAINING";
                       var idx = $('#idx').val();
                       $('#accountNumber-table tbody').find('tr').each(function(k,v) {
                           if ($(this).find('td:first-child input[type="radio"]').is(":checked")) {
                                var aData = accountNumberTable.fnGetData(k);
                                var accountId = aData.id;
                                var accountNumber = aData.accountNumber;
                                var accountName = aData.accountName;

                                $('#accountnumberid_'+idx).html(accountId);
                                $('#nomorrekening_'+idx).val(accountNumber);
                                $('#namarekening_' +idx).html(accountName);
                                $('#closeAccount').click();
                                if(akunNeedSubtype.contains(accountName)){
                                    alert("HARAP TAMBAHKAN SUBTYPE & SUBLEDGER");
                                    toastr.info("HARAP TAMBAHKAN SUBTYPE & SUBLEDGER");
                                }else{
                                    toastr.success("Sukses Ditambahkan");
                                }
                           }
                        });
                    }

				};

				cekSave = function(){
				    var hasil = false
				    var docNumber = $('#docNumber').val();
				    $.post('${request.contextPath}/journal/cekSave/', {
                        docNumber: docNumber
                    }, function(data) {
                        if(data == 'true'){
                            alert("DOCNUMBER SUDAH PERNAH DIGUNAKAN");
                        }else{
                            performSave();
                        }
                    });
				}

				performSave = function(id) {
                    recalculate();
                    if (sisaSaldo === 0) {
                        var jsonObj = [];
                        var journalCode = $('#journalCode').val();
                        var docNumber = $('#docNumber').val();
                        var journalDate = $('#journalDate_day').val() + '/' + $('#journalDate_month').val() + '/' + $('#journalDate_year').val();
                        var description = $('#description').val();
                        var count = 1;
                        var totalKredit = new Number(0);
                        var totalDebet = new Number(0);
                        if (journalCode && journalDate) {
                            var confrm = confirm("Apakah Anda Yakin?");
                            if(confrm){
                                $("#journalDetail-table table tbody").find('tr').each(function(k,v) {
                                   count = $(this).attr("id").split("_")[1];
                                   var map = {};
                                   var idAkun = $(this).find('td#accountnumberid_' + count).html().trim();
                                   var tipeTransaksi = $(this).find('td select#tipetransaksi_' + count).val();
                                   var jumlah = new Number($(this).find('td input#jumlah_' + count).autoNumeric("get"));
                                   var subTypeId = $(this).find('td select#subtypeid_' + count).val();
                                   var subLedger = $(this).find('td input#idSubledger' + count).val();
                                   var desc = $(this).find('td textarea#desc_' + count).val();
                                   var detailId = null;
                                   if (id) {
                                         detailId = $(this).find('td#detailid_' + count).html().trim();
                                         if (detailId.length > 0)
                                            map['detailId'] = detailId;
                                   }
                                   if (tipeTransaksi === "Kredit")
                                        totalKredit+=jumlah;
                                   else
                                        totalDebet+=jumlah;
                                   map['id'] = idAkun;
                                   map['journalTypeTransaction'] = tipeTransaksi;
                                   map['nomorUrut'] = count;
                                   map['jumlah'] = jumlah;
                                   map['subTypeId'] = subTypeId;
                                   map['subLedger'] = subLedger;
                                   map['desc'] = desc;
                                   jsonObj.push(map);
                                   count++;
                                });
                                var action = id?"update":"save";
                                $.post('${request.contextPath}/journal/'+action+'/', {
                                    id: id?id:null,
                                    journalCode: journalCode,
                                    docNumber: docNumber,
                                    journalDate: journalDate,
                                    description: description ? description : "-",
                                    totalCredit: totalKredit,
                                    totalDebit: totalDebet,
                                    journalDetails: JSON.stringify(jsonObj)
                                }, function(data) {
                                    alert("Kode Jurnal : " + journalCode);
                                    refresh();
                                    reloadJournalTable();
                                });
                            }
                        }
                    } else {
                        alert("sisa saldo harus balance");
                    }

				};
				refresh = function() {
                    var table = $('#journal-table');
		            table.fadeIn('fast');
                    $('#journal-form').fadeOut('fast')
                    $('#journalDetail-table').fadeOut('fast')
                    // reloadJournalTable();
                };
                performDeletion = function(rowid, detailid, journalid) {
                    var sure = confirm("Are you sure?");
                    if (sure) {
                        if (journalid && detailid) {
                        jQuery('#spinner').fadeIn(1);
                        $.post('${request.getContextPath()}/journal/removeDetail', {
                            detailId: detailid,
                            journalId: journalid
                        }, function(data) {
                        }).done(function() {
                            jQuery('#spinner').fadeOut(1);
                        });
                    }
                    $('#row_' + rowid).remove();
                    }
                };

                recalculate = function(e) {
                     if ($("#journalDetail-table table tbody").html().length > 20) {
                        var totalDebit = new Number(0);
                        var totalKredit = new Number(0);
                        var count = 0;
                        $("#journalDetail-table table tbody").find("tr").each(function(k,v) {
                            count = $(this).attr("id").split("_")[1];
                            if ($("#tipetransaksi_" + count).val() === "Debet") {
                                totalDebit += new Number($("#jumlah_" + count).autoNumeric("get"));
                            } else {
                                totalKredit += new Number($("#jumlah_" + count).autoNumeric("get"));
                            }
                        });
                        sisaSaldo = totalDebit - totalKredit;
                        $("#sisaSaldo").autoNumeric("set", sisaSaldo);
                     }
                };
});
    function modalSubledger(urut) {
        var cekST = $("#subtypeid_"+urut).val();
        if(cekST==""){
            alert("Pilih Subtype terlebih dahulu")
            return
        }
        $("#popupSubledgerContent").empty();
            $.ajax({type:'POST', url:'${request.contextPath}/subledgerModal?subtype='+cekST,
                success:function(data,textStatus){
                        $("#popupSubledgerContent").html(data);
                        $("#popupSubledgerModal").modal({
                            "backdrop" : "dynamic",
                            "keyboard" : true,
                            "show" : true
                        }).css({'width': '500px','margin-left': function () {return -($(this).width() / 2);}});

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
            $("#idx").val(urut);
    }

    function putSubledger() {
        var idx = $('#idx').val();
       $('#subledger-table tbody').find('tr').each(function(k,v) {
           if ($(this).find('td:first-child input[type="radio"]').is(":checked")) {
                var aData = subledgerTable.fnGetData(k);
                var accountNumber = aData.accountNumber;
                var id = aData.id;
                $('#subLedger_'+idx).val(accountNumber);
                $('#idSubledger'+idx).val(id);
           }
        });
        $("#closeSubledger").click();
    }
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>

			<li class="separator"></li>
            <g:if test="${session.userAuthority.toString().contains("ROLE_KALINGGA")}">
                <li><a class="pull-right box-action" href="#"
                       style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
                            class="icon-remove"></i>&nbsp;&nbsp;
                </a></li>
            </g:if>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="journal-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <input type="hidden" name="idx" id="idx" />
			<g:render template="dataTables" />
		</div>
		<div class="span12" id="journal-form" style="display: none;"></div>
	</div>
    <!-- MODAL -->

    <!-- MODAL -->
    <div id="detailSubType-modal" class="modal fade">
        <div class="modal-content">
            <div class="modal-header">
                List of Sub Type
            </div>
            <div class="modal-body">
                <table id="detailSubType-table" class="table">
                    <thead>
                    <th></th>
                    <th>Id</th>
                    <th>Sub Type</th>
                    <th>Description</th>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-primary create" name="save" value="Confirm" onclick="putAccount('subType');">
            </div>
        </div>
    </div>
    <div id="popupAccountNumberModal" class="modal fade">
        <div class="modal-dialog" style="width: 600px;">
            <div class="modal-content" style="width: 600px;">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 400px;">
                    %{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}%
                    <div id="popupAccountNumberContent">
                        <div class="iu-content"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="control-group">
                        <a href="javascript:void(0);" id="btnPilihKaryawan" onclick="putAccount();" class="btn btn-primary">
                            Add
                        </a>
                        <a href="javascript:void(0);" class="btn cancel" id="closeAccount" data-dismiss="modal">
                            Close
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="popupSubledgerModal" class="modal fade">
        <div class="modal-dialog" style="width: 550px;">
            <div class="modal-content" style="width: 550px;">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 600px;">
                    %{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}%
                    <div id="popupSubledgerContent">
                        <div class="iu-content"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="control-group">
                        <a href="javascript:void(0);" id="btnPilih" onclick="putSubledger();" class="btn btn-primary">
                            Add
                        </a>
                        <a href="javascript:void(0);" class="btn cancel" id="closeSubledger" data-dismiss="modal">
                            Close
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>
