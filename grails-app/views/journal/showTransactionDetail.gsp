<%--
  Created by IntelliJ IDEA.
  User: DENDYSWARAN
  Date: 5/8/14
  Time: 11:50 PM
--%>

<%@ page import="com.kombos.maintable.Company; com.kombos.hrd.Karyawan; com.kombos.customerprofile.HistoryCustomer; com.kombos.administrasi.VendorAsuransi; com.kombos.parts.Vendor; com.kombos.administrasi.CompanyDealer; com.kombos.parts.Konversi" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title>Penerimaan Lain - Lain</title>
    <style>
    #detail-table thead th, #detail-table tbody tr td  {font-size: 10px; white-space: nowrap;vertical-align: middle;}
    #detail-table tbody tr td {text-align: center;}
    #detail-table tbody td input[type="text"] {font-size: 10px; width: 100%; margin-top: 10px;}
    #detail-form {margin-left: 0px;} #btnSearchAccount {position: relative; left: 9px; top: 2px;}
    </style>
    <r:require modules="baseapplayout, baseapplist"/>
    <g:javascript>
        function showTable(formBuka){
            if(formBuka=="memorial"){
                $("#journal-table").fadeIn()
            }else{
                $("#journal-dataTable").fadeIn()
            }
            $('#journal-form').css("display","none");
        }
    </g:javascript>
</head>

<body>

<div class="box" style="border: none;">
    <div class="span12">
        <div class="row-fluid">
            <div class="span6">
                <div id="penerimaan-form" class="form-horizontal">
                    <fieldset class="form">
                        <div class="control-group fieldcontain">
                            <label for="kodeJurnal" class="control-label">
                                Kode Jurnal
                            </label>
                            <div class="controls">
                                <input type="text" value="${journalInstance?.journalCode}" id="kodeJurnal" readonly>
                            </div>
                        </div>
                        <div class="control-group fieldcontain">
                            <label for="tanggalJurnal" class="control-label">
                                Tanggal Jurnal
                            </label>
                            <div class="controls">
                                <input id="tanggalJurnal" type="text" value="${journalInstance?.journalDate.format("dd/MM/yyyy")}" readonly>
                            </div>
                        </div>
                        <div class="control-group fieldcontain">
                            <label for="deskripsi" class="control-label">
                                Deskripsi
                            </label>
                            <div class="controls">
                                <g:textArea name="deskripsi" id="deskripsi" value="${journalInstance?.description}" readonly="" style="resize:none" />
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="span6">
                <div class="form-horizontal">
                    <%
                        def konversi = new Konversi()
                    %>
                    <fieldset class="form">
                        <div class="control-group fieldcontain">
                            <label for="totalDebet" class="control-label">Total Debet</label>
                            <div class="controls">
                                <input type="text"style="text-align: right" readonly value="${journalInstance?.totalDebit ? konversi?.toRupiah(journalInstance?.totalDebit) : 0}" id="totalDebet">
                            </div>
                        </div>
                        <div class="control-group fieldcontain">
                            <label for="totalKredit" class="control-label">Total Kredit</label>
                            <div class="controls">
                                <input type="text" style="text-align: right" readonly value="${journalInstance?.totalCredit ? konversi?.toRupiah(journalInstance?.totalCredit) : 0}" id="totalKredit">
                            </div>
                        </div>
                        <div class="control-group fieldcontain">
                            <label for="kodeTransaksi" class="control-label">Doc Number</label>
                            <div class="controls">
                                <input type="text" value="${journalInstance.docNumber}" id="kodeTransaksi" readonly="readonly">
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
    <div class="span12" id="detail-form" style="display: block; margin-top: 20px;">
        <legend>Detail</legend>
        <table id="detail-table" class="table table-bordered" width="100%">
            <thead>
            <th>No</th>
            <th>Nomor Rekening</th>
            <th>Nama Rekening</th>
            <th width="50%">Deskripsi</th>
            <th>Debet</th>
            <th>Kredit</th>
            </thead>
            <tbody>
                <g:if test="${journalInstance.journalDetail}">
                    <g:each in="${journalDetailInstance}" status="i" var="j" >
                        <%
                            String atasNama = "";
                        %>
                        <g:if test="${j?.subType && j?.subLedger}">
                            <%
                                if(j?.subType?.subType?.toString()?.toUpperCase()?.contains('CABANG')){
                                    def cbg = j?.subLedger?.toString()?.replace(" ","")?.isNumber() ? CompanyDealer?.get(j?.subLedger) : null
                                    if(cbg){
                                        atasNama = "a.n "+cbg?.m011NamaWorkshop
                                    }
                                }else if(j?.subType?.subType?.toString()?.toUpperCase()?.contains('VENDOR')){
                                    def vend = j?.subLedger?.toString()?.replace(" ","")?.isNumber() ? Vendor?.get(j?.subLedger) : null
                                    if(vend){
                                        atasNama = "a.n "+vend?.m121Nama
                                    }
                                }else if(j?.subType?.subType?.toString()?.toUpperCase()?.contains('ASURANSI')){
                                    def asr = j?.subLedger?.toString()?.replace(" ","")?.isNumber() ? VendorAsuransi?.get(j?.subLedger) : null
                                    if(asr){
                                        atasNama = "a.n "+asr?.m193Nama
                                    }
                                }else if(j?.subType?.subType?.toString()?.toUpperCase()==('CUSTOMER')){
                                    def cst = j?.subLedger?.toString()?.replace(" ","")?.isNumber() ? HistoryCustomer?.get(j?.subLedger) : null
                                    if(cst){
                                        atasNama = "a.n "+cst?.fullNama
                                    }
                                }else  if(j?.subType?.subType?.toString()?.toUpperCase()?.contains('KARYAWAN')){
                                    def kry = j?.subLedger?.toString()?.replace(" ","")?.isNumber() ? Karyawan?.get(j?.subLedger) : null
                                    if(kry){
                                        atasNama = "a.n "+kry?.nama
                                    }
                                }else  if(j?.subType?.subType?.toString()?.toUpperCase()?.contains('CUSTOMER COMPANY')){
                                    def cmpy = j?.subLedger?.toString()?.replace(" ","")?.isNumber() ? Company?.get(j?.subLedger) : null
                                    if(cmpy){
                                        atasNama = "a.n "+cmpy?.namaPerusahaan
                                    }
                                }
                            %>
                        </g:if>
                        <tr>
                            <td>${i + 1}</td>
                            <td>${j.accountNumber.accountNumber}</td>
                            <td style="white-space: normal;">${j.accountNumber.accountName} ${atasNama}</td>
                            <td width="50%" style="white-space: normal; text-align: left;">
                                    ${(!j.description || j.description=="-")? j.journal.description : j.description}
                            </td>
                            <td><span class="pull-right">${konversi?.toRupiah(j?.debitAmount)}</span></td>
                            <td><span class="pull-right">${konversi?.toRupiah(j?.creditAmount)}</span></td>
                        </tr>
                    </g:each>
                </g:if>
            </tbody>
        </table>
        <g:if test="${dari && dari==2}" >
            <button class="btn btn-primary create" name="doApprove" id="doApprove" onclick="doApprove('${journalInstance?.id}','approve');" style="margin-top: 13px;">Approve</button>
            <button class="btn btn-warning create" name="doApprove" id="doApprove" onclick="doApprove('${journalInstance?.id}','reject');" style="margin-top: 13px;">Reject</button>
            <button class="btn cancel" data-dismiss="modal" name="closeModal" id="closeModal" style="margin-top: 13px;">Tutup</button>
        </g:if>
        <g:elseif test="${dari && dari==3}">
            <button class="btn cancel" data-dismiss="modal" name="close" id="closeTrxDetail" onclick="showTable('memorial');" style="margin-top: 13px;">Tutup</button>
        </g:elseif>
        <g:else>
            <button class="btn cancel" data-dismiss="modal" name="close" id="closeTrxDetail" onclick="showTable('bukan');" style="margin-top: 13px;">Tutup</button>
        </g:else>
    </div>
</div>

</body>
</html>