<%--
  Created by IntelliJ IDEA.
  User: DENDYSWARAN
  Date: 7/2/14
  Time: 6:54 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'journal.label', default: 'Journal')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <r:require modules="baseapplayout, baseapplist"/>
    <g:javascript>
        var show;
        var doApprove;
        $(function() {
            doApprove = function(id,status) {
                var texttanya = "Setuju untuk approval?";
                if(status=="reject"){
                    texttanya = "Apakah anda yakin data akan di reject?"
                }
                var confirmation = confirm(texttanya);
                if (confirmation) {
                    $.post("${request.getContextPath()}/journal/journalApproval", {id: id,status : status}, function(data) {
                        if (data.error) {
                            alert(data.error)
                        } else {
                            $("#closeModal").click();
                              $('#need_'+id+'').hide();
                            if(status=="reject"){
                                toastr.success("Data Rejected");
                                $('#hasil_'+id+'').html("Rejected");
                            }else{
                                toastr.success("Data Approved");
                                $('#hasil_'+id+'').html("Approved");
                            }
//                            reloadJournalTable();
                        }
                    })
                }
            }

            showDetailApproval = function(id){
                $("#detailContent").empty();
                $.ajax({
                    type:'POST',
                    url:'${request.contextPath}/journal/showTransactionDetail',
                    data : {id : id,dari : 2},
                    success:function(data,textStatus){
                            $("#detailContent").html(data);
                            $("#detailModal").modal({
                                "backdrop" : "dynamic",
                                "keyboard" : true,
                                "show" : true
                            }).css({'width': '1100px','margin-left': function () {return -($(this).width() / 2);}});

                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){},
                    complete:function(XMLHttpRequest,textStatus){
                        $('#spinner').fadeOut();
                    }
                });
            }
        })
    </g:javascript>
</head>

<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]"/> - Approval</span>
</div>

<div class="box">
    <div class="span12" id="journal-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
            <table id="journal_datatables" cellpadding="0" cellspacing="0"
                   border="0"
                   class="display table table-striped table-bordered table-hover"
                   width="100%">
                <!-- DataTables -->
                <thead>
                <tr>
                        <th>No.</th>
                        <th>Journal Code</th>
                        <th>Journal Date</th>
                        <th>Description</th>
                        <th>Total Debit</th>
                        <th>Total Credit</th>
                        <th>DocNumber</th>
                        <th>Created By</th>
                        <th>Approval</th>
                </tr>
                </thead>
            ${htmlData}

        </table>
        %{--<g:render template="approvalDT"/>--}%
    </div>
</div>
<div id="detailModal" class="modal fade">
    <div class="modal-dialog" style="width: 1090px;">
        <div class="modal-content" style="width: 1090px;">
            <div class="modal-body" style="max-height: 450px;">
                <div id="detailContent"/>
                <div class="iu-content"></div>

            </div>
        </div>
    </div>
</div>

</body>
</html>