<%@ page import="com.kombos.finance.Journal" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'journal.label', default: 'Journal Memorial')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
    <g:javascript>
        $(function() {
            $(".numeric").autoNumeric("init", {
                vMin: '-999999999999.99',
                vMax: '999999999999.99'
            });

            $("#journalDetail-table table tbody").on("focusout", "tr td input.jumlah", function() {
                recalculate();
            });
        })
    </g:javascript>
	<body>
		<div id="create-journal" class="content scaffold-create" role="main">
			<legend><g:message code="default.create.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${journalInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${journalInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form action="save" >--}%
			<g:formRemote class="form-horizontal" name="create" on404="alert('not found!');" onSuccess="reloadJournalTable();" update="journal-form"
              url="[controller: 'journal', action:'save']">
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons controls">
					<a class="btn cancel" href="javascript:void(0);" style="margin-left: -152px;"
                       onclick="refresh();"><g:message
                            code="default.button.cancel.label" default="Cancel"  /></a>
                    <a onclick="cekSave();" class="btn btn-primary create" href="javascript:void(0);" style="margin-left: 1px;">Save</a>
				</fieldset>
			</g:formRemote>
			%{--</g:form>--}%
		</div>
	</body>
</html>
