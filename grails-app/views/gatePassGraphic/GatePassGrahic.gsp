<%@ page import="com.kombos.parts.Goods" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'goods.label', default: 'Graphic')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
//	$(function(){ });
    preview=function(){

        window.location = "${request.contextPath}/gatePassGraphic/preview"
    };

        function previewdetail(){
        window.location = "${request.contextPath}/gatePassGraphic/previewdetail"
        }
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
</div>
<div class="box">
    <div class="span12" id="goods-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <table style="width: 45%">
            <tr>
                <td>
                    <div class="box" style="width: 80%">
                    <table style="width: 100%">
                        <tr>
                            <td >Jumlah Customer in</td>
                            <td>=</td>
                            <td>${jmlcustomerin}</td>
                        </tr>
                    </tr>
                        <tr>
                            <td>Proses Service</td>
                            <td>=</td>
                            <td>${custsisa}</td>

                        </tr>
                    </tr>
                        <tr>
                            <td> Gate Pass</td>
                            <td>=</td>
                            <td>${satu}</td>

                        </tr>
                        <tr>
                            <td>Tes Jalan Kendaraan </td>
                            <td>=</td>
                            <td>${dua}</td>

                        </tr>

                        <tr>
                            <td>Rawat Jalan Kendaraan</td>
                            <td>=</td>
                            <td>${tiga}</td>

                        </tr>
                        <tr>
                            <td>Sublet</td>
                            <td>=</td>
                            <td>${empat}</td>

                        </tr>
                    </table>


                </div>
                </td>
                <td>
                    <div class="box" style="width: 80%">

                    <g:field type="button"   style="width: 150px; height: 30px;" onclick="preview()" class="btn btn-primary create" name="preview"  value="${message(code: 'default.button.upload.label', default: 'Preview')}" />
                    <br/>
                    <g:field type="button"  style="width: 150px; height: 30px;" onclick="previewdetail()" class="btn btn-primary create" name="preview"  value="${message(code: 'default.button.upload.label', default: 'Preview Detail')}" />


                </div>
                </td>
           </tr>
        </table>


<table style="width: 100%" >
    <tr>
        <td>
            <div class="box" style="width: 80%">
            Table Customer in
            <g:render template="Tablejmlcusin" />
        </div>
        </td>
        <td>
            <div class="box" style="width: 80%">
            Table Gate Pass
            <g:render template="tablegatepass" />
        </div>
        </td>

    </tr>
    <tr>
        <td>
            <div class="box" style="width: 80%">
            Tes Jalan Kendaraaan
            <g:render template="tablegatepasstes" />
        </div>
        </td>
        <td>
            <div class="box" style="width: 80%">
            Rawat jalan Kendaraan
            <g:render template="Tablegatepasssjl" />
        </div>
        </td>


    </tr>
    <tr>
        <td>
            <div class="box" style="width: 80%">
                Sublet
                <g:render template="TableSublet" />
            </div>
        </td>
        <td>

        </td>


    </tr>
</table>
    </div>
    <div class="span7" id="goods-form" style="display: none;"></div>
</div>
</body>
</html>
