
<%@ page import="com.kombos.parts.Satuan; com.kombos.parts.Goods" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>


<table id="gatepassT800_datatables3" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="gatepassT800.m111ID.label" default="No" /></div>
        </th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="gatepassT800.m111Nama.label" default="No Polisi" /></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="gatepassT800.satuan.label" default="Jam Masuk" /></div>
</th>


<th style="border-bottom: none;padding: 5px;">
    <div><g:message code="gatepassT800.satuan.label" default="Jam Gatepass" /></div>
</th>


</tr>
</thead>
</table>

<g:javascript>
var gatepassT800Table;
var reloadGatepassT800Table;
$(function(){
	
	reloadGatepassT800Table = function() {
		gatepassT800Table.fnDraw();
	}
    $('#search_satuan').change(function(){
        gatepassT800Table.fnDraw();
    });
	var recordsGatepassT800PerPage = [];
    var anGatepassT800Selected;
    var jmlRecGatepassT800PerPage=0;
    var id;

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	gatepassT800Table.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	gatepassT800Table = $('#gatepassT800_datatables3').dataTable({
		"sScrollX": "",
		"bScrollCollapse": false,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsGatepassT800 = $("#gatepassT800_datatables tbody .row-select");
            var jmlGatepassT800Cek = 0;
            var nRow;
            var idRec;
            rsGatepassT800.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsGatepassT800PerPage[idRec]=="1"){
                    jmlGatepassT800Cek = jmlGatepassT800Cek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsGatepassT800PerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecGatepassT800PerPage = rsGatepassT800.length;
            if(jmlGatepassT800Cek==jmlRecGatepassT800PerPage && jmlRecGatepassT800PerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList3")}",
		"aoColumns": [

{
	"sName": "id",
	"mDataProp": "id",
	"aTargets": [0],

	"bSearchable": true,
	"bSortable": true,
	"sWidth":"20px",
	"bVisible": true
}

,

{
	"sName": "nopol",
	"mDataProp": "nopol",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"50px",
	"bVisible": true
}

,

{
	"sName": "jammasuk",
	"mDataProp": "jammasuk",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"20px",
	"bVisible": true
},

{
	"sName": "jamkeluar",
	"mDataProp": "jamkeluar",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"20px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {



						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

//	$('.select-all').click(function(e) {
//
//        $("#gatepassT800_datatables tbody .row-select").each(function() {
//            if(this.checked){
//                recordsGatepassT800PerPage[$(this).next("input:hidden").val()] = "1";
//            } else {
//                recordsGatepassT800PerPage[$(this).next("input:hidden").val()] = "0";
//            }
//        });
//
//    });
//
//	$('#gatepassT800_datatables tbody tr').live('click', function () {
//
//        id = $(this).find('.row-select').next("input:hidden").val();
//        if($(this).find('.row-select').is(":checked")){
//            recordsGatepassT800PerPage[id] = "1";
//            $(this).find('.row-select').parent().parent().addClass('row_selected');
//            anGatepassT800Selected = gatepassT800Table.$('tr.row_selected');
//
//            if(jmlRecGatepassT800PerPage == anGatepassT800Selected.length){
//                $('.select-all').attr('checked', true);
//            }
//
//        } else {
//            recordsGatepassT800PerPage[id] = "0";
//            $('.select-all').attr('checked', false);
//            $(this).find('.row-select').parent().parent().removeClass('row_selected');
//        }
//    });
});
</g:javascript>