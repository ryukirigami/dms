<%@ page import="com.kombos.administrasi.BaseModel; com.kombos.administrasi.PerhitunganNextServiceBaseModel" %>
<script type="text/javascript">
    jQuery(function($) {
        $('.auto').autoNumeric('init', {mDec: '0'});
    });

    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

</script>


<div class="control-group fieldcontain ${hasErrors(bean: perhitunganNextServiceBaseModelInstance, field: 't115TglBerlaku', 'error')} required">
	<label class="control-label" for="t115TglBerlaku">
		<g:message code="perhitunganNextServiceBaseModel.t115TglBerlaku.label" default="Tanggal Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<ba:datePicker name="t115TglBerlaku" precision="day"  value="${perhitunganNextServiceBaseModelInstance?.t115TglBerlaku}" format= "dd-MM-yyyy"  required="true"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: perhitunganNextServiceBaseModelInstance, field: 't115KategoriService', 'error')} required">
	<label class="control-label" for="t115KategoriService">
		<g:message code="perhitunganNextServiceBaseModel.t115KategoriService.label" default="Kategori Service" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:radioGroup required="" name="t115KategoriService" values="['0','1']" labels="['SBI','SBE']" value="${perhitunganNextServiceBaseModelInstance?.t115KategoriService}" >
        ${it.radio} ${it.label}
    </g:radioGroup>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: perhitunganNextServiceBaseModelInstance, field: 'baseModel', 'error')} required">
	<label class="control-label" for="baseModel">
		<g:message code="perhitunganNextServiceBaseModel.baseModel.label" default="Base Model" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="baseModel" name="baseModel.id" from="${BaseModel.createCriteria().list(){eq("staDel", "0") order("m102NamaBaseModel", "asc")}}" optionKey="id" optionValue="m102NamaBaseModel" required="" value="${perhitunganNextServiceBaseModelInstance?.baseModel?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: perhitunganNextServiceBaseModelInstance, field: 't115Km', 'error')} required">
	<label class="control-label" for="t115Km">
		<g:message code="perhitunganNextServiceBaseModel.t115Km.label" default="Next Service" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t115Km"  required="" onkeypress="return isNumberKey(event)" maxlength="8" value="${fieldValue(bean: perhitunganNextServiceBaseModelInstance, field: 't115Km')}" />&nbsp;&nbsp;KM<br/><br/>
	</div>
	<div class="controls">
	<g:textField name="t115Bulan"  required="" onkeypress="return isNumberKey(event)" maxlength="8" value="${fieldValue(bean: perhitunganNextServiceBaseModelInstance, field: 't115Bulan')}"/>&nbsp;&nbsp;Bulan<br/><br/>
	</div>
</div>

