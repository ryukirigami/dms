
<%@ page import="com.kombos.administrasi.PerhitunganNextServiceBaseModel" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>
<script type="text/javascript">
    jQuery(function($) {
        $('.cek').autoNumeric('init', {mDec: '0'});
    });
</script>
<table id="perhitunganNextServiceBaseModel_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px; vertical-align:bottom;" rowspan="2">
				<div><g:message code="perhitunganNextServiceBaseModel.t115TglBerlaku.label" default="Tanggal Berlaku" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; vertical-align:bottom;" rowspan="2">
				<div><g:message code="perhitunganNextServiceBaseModel.t115KategoriService.label" default="Kategori Service" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; vertical-align:bottom;" rowspan="2">
				<div><g:message code="perhitunganNextServiceBaseModel.baseModel.label" default="Base Model" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; text-align: center" colspan="2">
				<div><g:message code="perhitunganNextServiceBaseModel.nextService.label" default="Next Service" /></div>
			</th>			
				
		</tr>
		<tr>
		
			<th style="border-bottom: none;padding: 5px;  text-align: center">
				<div><g:message code="perhitunganNextServiceBaseModel.t115Km.label" default="KM" /></div>
			</th>
            
            <th style="border-bottom: none;padding: 5px;  text-align: center">
				<div><g:message code="perhitunganNextServiceBaseModel.t115Bulan.label" default="Bulan" /></div>
			</th>
		</tr>
		<tr>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t115TglBerlaku" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_t115TglBerlaku" value="date.struct">
					<input type="hidden" name="search_t115TglBerlaku_day" id="search_t115TglBerlaku_day" value="">
					<input type="hidden" name="search_t115TglBerlaku_month" id="search_t115TglBerlaku_month" value="">
					<input type="hidden" name="search_t115TglBerlaku_year" id="search_t115TglBerlaku_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t115TglBerlaku_dp" value="" id="search_t115TglBerlaku" class="search_init">
				</div>
			</th>
			
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t115KategoriService" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<select name="search_t115KategoriService" id="search_t115KategoriService" onchange="reloadPerhitunganNextServiceBaseModelTable();" style="width:100%">
						<option value="">ALL</option>
						<option value="1">SBE</option>
						<option value="0">SBI</option>
					</select>
				</div>
			</th>
%{--	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t115KategoriService" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t115KategoriService" class="search_init" />
				</div>
			</th>
--}%	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_baseModel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_baseModel" class="search_init" />
				</div>
			</th>
			
			<th style="border-top: none;padding: 5px;text-align: center">
				<div id="filter_t115Km" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t115Km" class="search_init cek" />
				</div>
			</th>
			
			<th style="border-top: none;padding: 5px;text-align: center">
				<div id="filter_t115Bulan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t115Bulan" class="search_init cek" />
				</div>
			</th>		
			
		</tr>
	</thead>
</table>

<g:javascript>
var perhitunganNextServiceBaseModelTable;
var reloadPerhitunganNextServiceBaseModelTable;
$(function(){
	
	reloadPerhitunganNextServiceBaseModelTable = function() {
		perhitunganNextServiceBaseModelTable.fnDraw();
	}

	var recordsNsbmperpage = [];//new Array();
    var anNsbmSelected;
    var jmlRecNsbmPerPage=0;
    var id;

	$('#search_t115TglBerlaku').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t115TglBerlaku_day').val(newDate.getDate());
			$('#search_t115TglBerlaku_month').val(newDate.getMonth()+1);
			$('#search_t115TglBerlaku_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			perhitunganNextServiceBaseModelTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	perhitunganNextServiceBaseModelTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	perhitunganNextServiceBaseModelTable = $('#perhitunganNextServiceBaseModel_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            //alert( $('li.active').text() );
            var rsNsbm = $("#perhitunganNextServiceBaseModel_datatables tbody .row-select");
            var jmlNsbmCek = 0;
            var idRec;
            var nRow;
            rsNsbm.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsNsbmperpage[idRec]=="1"){
                    jmlNsbmCek = jmlNsbmCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsNsbmperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecNsbmPerPage = rsNsbm.length;
            if(jmlNsbmCek==jmlRecNsbmPerPage && jmlRecNsbmPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "t115TglBerlaku",
	"mDataProp": "t115TglBerlaku",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "t115KategoriService",
	"mDataProp": "t115KategoriService",
	"aTargets": [1],
	"mRender": function ( data, type, row ) {          
            if(data=="0"){
                  return "SBI";
             }else
             {
                  return "SBE";
             }
        
        },
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "baseModel",
	"mDataProp": "baseModel",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t115Km",
	"mDataProp": "t115Km",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t115Bulan",
	"mDataProp": "t115Bulan",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var t115TglBerlaku = $('#search_t115TglBerlaku').val();
						var t115TglBerlakuDay = $('#search_t115TglBerlaku_day').val();
						var t115TglBerlakuMonth = $('#search_t115TglBerlaku_month').val();
						var t115TglBerlakuYear = $('#search_t115TglBerlaku_year').val();
						
						if(t115TglBerlaku){
							aoData.push(
									{"name": 'sCriteria_t115TglBerlaku', "value": "date.struct"},
									{"name": 'sCriteria_t115TglBerlaku_dp', "value": t115TglBerlaku},
									{"name": 'sCriteria_t115TglBerlaku_day', "value": t115TglBerlakuDay},
									{"name": 'sCriteria_t115TglBerlaku_month', "value": t115TglBerlakuMonth},
									{"name": 'sCriteria_t115TglBerlaku_year', "value": t115TglBerlakuYear}
							);
						}
	
						var t115KategoriService = $('#search_t115KategoriService').val();
						if(t115KategoriService){
							aoData.push(
									{"name": 'sCriteria_t115KategoriService', "value": t115KategoriService}
							);
						}
	
						var baseModel = $('#filter_baseModel input').val();
						if(baseModel){
							aoData.push(
									{"name": 'sCriteria_baseModel', "value": baseModel}
							);
						}
	
						var t115Km = $('#filter_t115Km input').val();
						if(t115Km){
							aoData.push(
									{"name": 'sCriteria_t115Km', "value": t115Km}
							);
						}
	
						var t115Bulan = $('#filter_t115Bulan input').val();
						if(t115Bulan){
							aoData.push(
									{"name": 'sCriteria_t115Bulan', "value": t115Bulan}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

    $('.select-all').click(function(e) {
        $("#perhitunganNextServiceBaseModel_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsNsbmperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsNsbmperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

	$('#perhitunganNextServiceBaseModel_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsNsbmperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anNsbmSelected = perhitunganNextServiceBaseModelTable.$('tr.row_selected');
            if(jmlRecNsbmPerPage == anNsbmSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsNsbmperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
