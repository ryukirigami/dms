

<%@ page import="com.kombos.administrasi.PerhitunganNextServiceBaseModel" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'perhitunganNextServiceBaseModel.label', default: 'Perhitungan Next Service Per Base Model')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deletePerhitunganNextServiceBaseModel;

$(function(){ 
	deletePerhitunganNextServiceBaseModel=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/perhitunganNextServiceBaseModel/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadPerhitunganNextServiceBaseModelTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-perhitunganNextServiceBaseModel" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="perhitunganNextServiceBaseModel"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${perhitunganNextServiceBaseModelInstance?.t115TglBerlaku}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t115TglBerlaku-label" class="property-label"><g:message
					code="perhitunganNextServiceBaseModel.t115TglBerlaku.label" default="Tanggal Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t115TglBerlaku-label">
						%{--<ba:editableValue
								bean="${perhitunganNextServiceBaseModelInstance}" field="t115TglBerlaku"
								url="${request.contextPath}/PerhitunganNextServiceBaseModel/updatefield" type="text"
								title="Enter t115TglBerlaku" onsuccess="reloadPerhitunganNextServiceBaseModelTable();" />--}%
							
								<g:formatDate date="${perhitunganNextServiceBaseModelInstance?.t115TglBerlaku}" format="dd-MM-yyyy" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${perhitunganNextServiceBaseModelInstance?.t115KategoriService}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t115KategoriService-label" class="property-label"><g:message
					code="perhitunganNextServiceBaseModel.t115KategoriService.label" default="Kategori Service" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t115KategoriService-label">
						%{--<ba:editableValue
								bean="${perhitunganNextServiceBaseModelInstance}" field="t115KategoriService"
								url="${request.contextPath}/PerhitunganNextServiceBaseModel/updatefield" type="text"
								title="Enter t115KategoriService" onsuccess="reloadPerhitunganNextServiceBaseModelTable();" />--}%
							
								<g:if test="${perhitunganNextServiceBaseModelInstance.t115KategoriService == '0'}">
								<g:message code="perhitunganNextServiceBaseModel.t115KategoriService.label" default="SBI" />
              					</g:if>
              					<g:else>
                				<g:message code="perhitunganNextServiceBaseModel.t115KategoriService.label" default="SBE" />
              					</g:else>
            			</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${perhitunganNextServiceBaseModelInstance?.baseModel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="baseModel-label" class="property-label"><g:message
					code="perhitunganNextServiceBaseModel.baseModel.label" default="Base Model" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="baseModel-label">
						%{--<ba:editableValue
								bean="${perhitunganNextServiceBaseModelInstance}" field="baseModel"
								url="${request.contextPath}/PerhitunganNextServiceBaseModel/updatefield" type="text"
								title="Enter baseModel" onsuccess="reloadPerhitunganNextServiceBaseModelTable();" />--}%
							
								${perhitunganNextServiceBaseModelInstance?.baseModel?.m102NamaBaseModel}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${perhitunganNextServiceBaseModelInstance?.t115Km}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t115Km-label" class="property-label"><g:message
					code="perhitunganNextServiceBaseModel.t115Km.label" default="Next Service" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t115Km-label">
						%{--<ba:editableValue
								bean="${perhitunganNextServiceBaseModelInstance}" field="t115Km"
								url="${request.contextPath}/PerhitunganNextServiceBaseModel/updatefield" type="text"
								title="Enter t115Km" onsuccess="reloadPerhitunganNextServiceBaseModelTable();" />--}%
							
								<g:fieldValue bean="${perhitunganNextServiceBaseModelInstance}" field="t115Km"/>&nbsp;&nbsp;KM<br/>
              					<g:fieldValue bean="${perhitunganNextServiceBaseModelInstance}" field="t115Bulan"/>&nbsp;&nbsp;Bulan<br/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${perhitunganNextServiceBaseModelInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="perhitunganNextServiceBaseModel.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${perhitunganNextServiceBaseModelInstance}" field="dateCreated"
                                url="${request.contextPath}/perhitunganNextServiceBaseModel/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadperhitunganNextServiceBaseModelTable();" />--}%

                        <g:formatDate date="${perhitunganNextServiceBaseModelInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${perhitunganNextServiceBaseModelInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="perhitunganNextServiceBaseModel.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${perhitunganNextServiceBaseModelInstance}" field="createdBy"
                                url="${request.contextPath}/perhitunganNextServiceBaseModel/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadperhitunganNextServiceBaseModelTable();" />--}%

                        <g:fieldValue bean="${perhitunganNextServiceBaseModelInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${perhitunganNextServiceBaseModelInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="perhitunganNextServiceBaseModel.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${perhitunganNextServiceBaseModelInstance}" field="lastUpdated"
                                url="${request.contextPath}/perhitunganNextServiceBaseModel/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadperhitunganNextServiceBaseModelTable();" />--}%

                        <g:formatDate date="${perhitunganNextServiceBaseModelInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${perhitunganNextServiceBaseModelInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="perhitunganNextServiceBaseModel.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${perhitunganNextServiceBaseModelInstance}" field="updatedBy"
                                url="${request.contextPath}/perhitunganNextServiceBaseModel/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadperhitunganNextServiceBaseModelTable();" />--}%

                        <g:fieldValue bean="${perhitunganNextServiceBaseModelInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${perhitunganNextServiceBaseModelInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="perhitunganNextServiceBaseModel.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${perhitunganNextServiceBaseModelInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/perhitunganNextServiceBaseModel/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadperhitunganNextServiceBaseModelTable();" />--}%

                        <g:fieldValue bean="${perhitunganNextServiceBaseModelInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${perhitunganNextServiceBaseModelInstance?.id}"
					update="[success:'perhitunganNextServiceBaseModel-form',failure:'perhitunganNextServiceBaseModel-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deletePerhitunganNextServiceBaseModel('${perhitunganNextServiceBaseModelInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
