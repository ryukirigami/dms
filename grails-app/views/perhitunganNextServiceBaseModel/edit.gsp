<%@ page import="com.kombos.administrasi.PerhitunganNextServiceBaseModel" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'perhitunganNextServiceBaseModel.label', default: 'Perhitungan Next Service Per Base Model')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="edit-perhitunganNextServiceBaseModel" class="content scaffold-edit" role="main">
			<legend><g:message code="default.edit.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${perhitunganNextServiceBaseModelInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${perhitunganNextServiceBaseModelInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form method="post" >--}%
			<g:formRemote class="form-horizontal" name="edit" on404="alert('not found!');" onSuccess="reloadPerhitunganNextServiceBaseModelTable();" update="perhitunganNextServiceBaseModel-form"
				url="[controller: 'perhitunganNextServiceBaseModel', action:'update']">
				<g:hiddenField name="id" value="${perhitunganNextServiceBaseModelInstance?.id}" />
				<g:hiddenField name="version" value="${perhitunganNextServiceBaseModelInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
                <fieldset class="buttons controls">
                    <g:actionSubmit class="btn btn-primary save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                    <a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout();"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
                </fieldset>
			</g:formRemote>
			%{--</g:form>--}%
		</div>
	</body>
</html>
