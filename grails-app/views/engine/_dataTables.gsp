
<%@ page import="com.kombos.administrasi.Engine" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="engine_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="engine.baseModel.label" default="Base Model" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="engine.modelName.label" default="Model Name" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="engine.bodyType.label" default="Body Type" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="engine.gear.label" default="Gear" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="engine.grade.label" default="Grade" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="engine.m108KodeEngine.label" default="Kode Engine" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="engine.m108NamaEngine.label" default="Nama Engine" /></div>
			</th>


		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_baseModel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_baseModel" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_modelName" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_modelName" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_bodyType" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_bodyType" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_gear" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_gear" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_grade" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_grade" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m108KodeEngine" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m108KodeEngine" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m108NamaEngine" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m108NamaEngine" class="search_init" />
				</div>
			</th>
	


		</tr>
	</thead>
</table>

<g:javascript>
var engineTable;
var reloadEngineTable;
$(function(){
	
	reloadEngineTable = function() {
		engineTable.fnDraw();
	}

    var recordsengineperpage = [];
    var anEngineSelected;
    var jmlRecEnginePerPage=0;
    var id;


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	engineTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	engineTable = $('#engine_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsEngine = $("#engine_datatables tbody .row-select");
            var jmlEngineCek = 0;
            var nRow;
            var idRec;
            rsEngine.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsengineperpage[idRec]=="1"){
                    jmlEngineCek = jmlEngineCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsengineperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecEnginePerPage = rsEngine.length;
            if(jmlEngineCek==jmlRecEnginePerPage && jmlRecEnginePerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "baseModel",
	"mDataProp": "baseModel",
	"aTargets": [1],
        "mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "modelName",
	"mDataProp": "modelName",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "bodyType",
	"mDataProp": "bodyType",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "gear",
	"mDataProp": "gear",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "grade",
	"mDataProp": "grade",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m108KodeEngine",
	"mDataProp": "m108KodeEngine",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m108NamaEngine",
	"mDataProp": "m108NamaEngine",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m108ID = $('#filter_m108ID input').val();
						if(m108ID){
							aoData.push(
									{"name": 'sCriteria_m108ID', "value": m108ID}
							);
						}
	
						var baseModel = $('#filter_baseModel input').val();
						if(baseModel){
							aoData.push(
									{"name": 'sCriteria_baseModel', "value": baseModel}
							);
						}
	
						var modelName = $('#filter_modelName input').val();
						if(modelName){
							aoData.push(
									{"name": 'sCriteria_modelName', "value": modelName}
							);
						}
	
						var bodyType = $('#filter_bodyType input').val();
						if(bodyType){
							aoData.push(
									{"name": 'sCriteria_bodyType', "value": bodyType}
							);
						}
	
						var gear = $('#filter_gear input').val();
						if(gear){
							aoData.push(
									{"name": 'sCriteria_gear', "value": gear}
							);
						}
	
						var grade = $('#filter_grade input').val();
						if(grade){
							aoData.push(
									{"name": 'sCriteria_grade', "value": grade}
							);
						}
	
						var m108KodeEngine = $('#filter_m108KodeEngine input').val();
						if(m108KodeEngine){
							aoData.push(
									{"name": 'sCriteria_m108KodeEngine', "value": m108KodeEngine}
							);
						}
	
						var m108NamaEngine = $('#filter_m108NamaEngine input').val();
						if(m108NamaEngine){
							aoData.push(
									{"name": 'sCriteria_m108NamaEngine', "value": m108NamaEngine}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
    $('.select-all').click(function(e) {
        $("#engine_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsengineperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsengineperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

    $('#engine_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsengineperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anEngineSelected = engineTable.$('tr.row_selected');
            if(jmlRecEnginePerPage == anEngineSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsengineperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
