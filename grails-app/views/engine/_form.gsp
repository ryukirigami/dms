<%@ page import="com.kombos.administrasi.Grade; com.kombos.administrasi.Gear; com.kombos.administrasi.BodyType; com.kombos.administrasi.BaseModel; com.kombos.administrasi.ModelName; com.kombos.administrasi.Engine" %>

<g:javascript>
    var selectModelName;
    var selectBodyType;
    var selectGear;
    var selectGrade;

			$(function(){
			selectModelName = function () {
			var idBaseModel = $('#baseModel option:selected').val();
			    if(idBaseModel != undefined){
                    jQuery.getJSON('${request.contextPath}/bodyType/getModelNames?idBaseModel='+idBaseModel, function (data) {
                            $('#modelName').empty();
                            if (data) {
                                jQuery.each(data, function (index, value) {
                                    $('#modelName').append("<option value='" + value.id + "'>" + value.kodeModelName + "</option>");
                                });
                            }
                            selectBodyType();
                        });
                }else{
                     $('#modelName').empty();
                     $('#bodyType').empty();
                     $('#gear').empty();
                     $('#grade').empty();
                }
            };

			selectBodyType = function () {
			var idModelName = $('#modelName').val();
			    if(idModelName != undefined){
                    jQuery.getJSON('${request.contextPath}/bodyType/getBodyTypes?idModelName='+idModelName, function (data) {
                            $('#bodyType').empty();
                            if (data) {
                                jQuery.each(data, function (index, value) {
                                    $('#bodyType').append("<option value='" + value.id + "'>" + value.kodeBodyType + "</option>");
                                });
                            }
                            selectGear();
                        });
                }else{
                     $('#bodyType').empty();
                     $('#gear').empty();
                     $('#grade').empty();
                }
            };

            selectGear = function() {
            var idBodyType = $('#bodyType').val();
                if (idBodyType != undefined){
                    jQuery.getJSON('${request.contextPath}/bodyType/getGears?idBodyType='+idBodyType, function (data) {
                            $('#gear').empty();
                            if (data) {
                                jQuery.each(data, function (index, value) {
                                    $('#gear').append("<option value='" + value.id + "'>" + value.kodeGear + "</option>");
                                });
                            }
                            selectGrade();
                        });
                }else{
                     $('#gear').empty();
                     $('#grade').empty();
                }
            };

            selectGrade = function(){
            var idGear = $('#gear').val();
                if (idGear != undefined){
                    jQuery.getJSON('${request.contextPath}/bodyType/getGrades?idGear='+idGear, function (data) {
                            $('#grade').empty();
                            if (data) {
                                jQuery.each(data, function (index, value) {
                                    $('#grade').append("<option value='" + value.id + "'>" + value.kodeGrade + "</option>");
                                });
                            }
                        });
                }else{
                     $('#grade').empty();
                }
            };
    });
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: engineInstance, field: 'baseModel', 'error')} required">
	<label class="control-label" for="baseModel">
		<g:message code="engine.baseModel.label" default="Base Model" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="baseModel" onchange="selectModelName();" name="baseModel.id" noSelection="['':'Pilih Base Model']" from="${BaseModel.createCriteria().list(){eq("staDel","0");order("m102KodeBaseModel")}}" optionValue="${{it.m102KodeBaseModel + " - " + it.m102NamaBaseModel}}" optionKey="id" required="" value="${engineInstance?.baseModel?.id}" class="many-to-one"/>
	</div>
</div>
<g:javascript>
    document.getElementById("baseModel").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: engineInstance, field: 'modelName', 'error')} required">
	<label class="control-label" for="modelName">
		<g:message code="engine.modelName.label" default="Model Name" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="modelName" onchange="selectBodyType();" name="modelName.id" noSelection="['':'Pilih Model Name']" optionValue="${{it.m104KodeModelName + " - " + it.m104NamaModelName}}" from="${ModelName.createCriteria().list(){eq("staDel","0");order("m104KodeModelName")}}" optionKey="id" required="" value="${engineInstance?.modelName?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: engineInstance, field: 'bodyType', 'error')} required">
	<label class="control-label" for="bodyType">
		<g:message code="engine.bodyType.label" default="Body Type" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="bodyType" onchange="selectGear();" name="bodyType.id" optionValue="${{it.m105KodeBodyType + " - " + it.m105NamaBodyType}}" noSelection="['':'Pilih Body Type']" from="${BodyType.createCriteria().list(){eq("staDel","0");order("m105KodeBodyType")}}" optionKey="id" required="" value="${engineInstance?.bodyType?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: engineInstance, field: 'gear', 'error')} required">
	<label class="control-label" for="gear">
		<g:message code="engine.gear.label" default="Gear" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="gear" onchange="selectGrade();" name="gear.id" noSelection="['':'Pilih Gear']" optionValue="${{it.m106KodeGear + " - " + it.m106NamaGear}}" from="${Gear.createCriteria().list(){eq("staDel","0");order("m106KodeGear")}}" optionKey="id" required="" value="${engineInstance?.gear?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: engineInstance, field: 'grade', 'error')} required">
	<label class="control-label" for="grade">
		<g:message code="engine.grade.label" default="Grade" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="grade" name="grade.id" noSelection="['':'Pilih Grade']" optionValue="${{it.m107KodeGrade + " - " + it.m107NamaGrade}}" from="${Grade.createCriteria().list(){eq("staDel","0");order("m107KodeGrade")}}" optionKey="id" required="" value="${engineInstance?.grade?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: engineInstance, field: 'm108KodeEngine', 'error')} required">
	<label class="control-label" for="m108KodeEngine">
		<g:message code="engine.m108KodeEngine.label" default="Kode Engine" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m108KodeEngine" maxlength="2" required="" value="${engineInstance?.m108KodeEngine}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: engineInstance, field: 'm108NamaEngine', 'error')} required">
	<label class="control-label" for="m108NamaEngine">
		<g:message code="engine.m108NamaEngine.label" default="Nama Engine" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m108NamaEngine" maxlength="20" required="" value="${engineInstance?.m108NamaEngine}"/>
	</div>
</div>

