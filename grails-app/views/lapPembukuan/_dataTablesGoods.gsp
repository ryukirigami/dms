<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="goods_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">

</table>

<g:javascript>
var goodsTable;
var reloadGoodsTable;
$(function(){

    $("#btnPilihGoods").click(function() {
        $("#goods_datatables tbody").find("tr").each(function() {
            var radio = $(this).find("td:eq(0) input[type='radio']");
            if (radio.is(":checked")) {
                var id = radio.data("id");
                var nama = radio.data("nama");
                $("#goods_id").val(id);
                $("#goods_nama").val(nama);
                oFormService.fnHideGoodsDataTable();
            }
        })

    });


	reloadGoodsTable = function() {
		goodsTable.fnDraw();
		}

    $("#btnSearchGoods").click(function() {
	    reloadGoodsTable();
    });

	goodsTable = $('#goods_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : 5, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList", controller: "goods")}",
		"aoColumns": [

{
    "sTitle": "Kode Parts",
	"sName": "m111ID",
	"mDataProp": "m111ID",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return "<input type='radio' data-id='"+row["id"]+"' data-nama='"+row["m111Nama"]+"' name='radio' class='pull-left row-select' style='position: relative; top: 3px;' aria-label='Row " + row["id"] + "' title='Select this'><input type='hidden' value='"+row["id"]+"'>&nbsp;&nbsp;"+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}

,

{
	"sTitle": "Nama Goods",
	"sName": "m111Nama",
	"mDataProp": "m111Nama",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
    "sTitle": "Satuan",
	"sName": "satuan",
	"mDataProp": "satuan",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"80px",
	"bVisible": true
}

,

{
    "sTitle": "Jenis",
	"sName": "franc",
	"mDataProp": "franc",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                        var namaGoods = $("#sCriteria_nama").val();

                        if (namaGoods) {
                            aoData.push({"name": "sCriteria_m111Nama", "value": namaGoods});
                        }

                        var sCriteria_m111ID = $("#sCriteria_kode").val();

                        if (sCriteria_m111ID) {
                            aoData.push({"name": "sCriteria_m111ID", "value": sCriteria_m111ID});
                        }

$.ajax({ "dataType": 'json',
    "type": "POST",
    "url": sSource,
    "data": aoData ,
    "success": function (json) {
        fnCallback(json);
       },
    "complete": function () {
       }
});
}
});

});
</g:javascript>



