<%@ page import="com.kombos.administrasi.CompanyDealer" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
    <style>
    .form-horizontal input + .help-block, .form-horizontal select + .help-block, .form-horizontal textarea + .help-block, .form-horizontal .uneditable-input + .help-block, .form-horizontal .input-prepend + .help-block, .form-horizontal .input-append + .help-block {
        font-size: 10px !important;
        font-style: italic !important;
        margin-top: 6px !important;
    }
    .modal.fade.in {
        left: 40% !important;
        width: 805px !important;
    }
    .modal.fade {
        top: -100%;
    }
    </style>
<r:require modules="baseapplayout" />
<g:javascript disappointmentGrition="head">
 $("#btnCariGoods").click(function() {
        oFormService.fnShowGoodsDataTable();
        })
        oFormService = {
            fnShowGoodsDataTable: function() {
                $("#goodsModal").modal("show");
                $("#goodsModal").fadeIn();
            },
            fnHideGoodsDataTable: function() {
                $("#goodsModal").modal("hide");
            }
        }
    var cekJenis;
	$(function(){

        cekJenis =  function() {
            var tipe = $('input[name="formatReport"]:checked').val();

            $('#tahun').show();
            if(tipe=='b'){
                $('#bulan').show();
            }else{
                $('#bulan').hide();
            }
        };


        $('#filter_format').show();
        $('#divgoods').hide();
        $('#filter_tanggal').hide();
        $('#filter_tanggal1').hide();
        $('#bulan').hide();
        $('#tahun').hide();
        $('#memorial1').hide();
        $('#memorial2').hide();

        $('#filter_jenis').hide();
        $('#filter_level').hide();
        $('#namaReport').change(function(){
                $('#preview').val("Preview");
                $('#filter_format').show();
                $('#filter_tanggal1').hide();
                $('#divgoods').hide();
                $('#filter_tanggal').hide();
                $('#bulan').hide();
                $('#tahun').hide();
                $('#filter_jenis').hide();
                $('#filter_level').hide();
                $('#lblsd').hide();
                $('#filter_periode').show();
                $('#filter_tipeLR').hide();
                $('#memorial1').hide();
                $('#memorial2').hide();
             if($('#namaReport').val()=="NERACA" || $('#namaReport').val()=="RINCIAN_RUGILABA" || $('#namaReport').val()=="RINCIAN_NERACAPERCOBAAN"){
                $('#bulan').show();
                $('#tahun').show();
                $('#filter_level').show();
             }else if($('#namaReport').val()=="MEMORIAL"){
                $('#filter_tanggal1').show();
                 $('#memorial1').show();
                $('#memorial2').show();
             }else if($('#namaReport').val()=="RUGILABA"){
                $('#bulan').show();
                $('#tahun').show();
                $('#filter_level').show();
                $('#filter_tipeLR').show();
             }else if($('#namaReport').val()=="PENDING_MASUK_BAHAN" || $('#namaReport').val()=="PENDING_MASUK_PARTS" ||
              $('#namaReport').val()=="PENDING_MASUK_SO" || $('#namaReport').val()=="TMO" ||
              $('#namaReport').val()=="PENDING_KELUAR_BAHAN" || $('#namaReport').val()=="PENDING_KELUAR_PARTS" ||$('#namaReport').val()=="PENDING_KELUAR_SO"){
                $('#bulan').show();
                $('#tahun').show();
             }
             else if($('#namaReport').val()=="PENJUALAN_BARANG"){
                $('#bulan').show();
                $('#tahun').show();
                $('#divgoods').show();
             }
             else if($('#namaReport').val()=="HASS"){
                $('#filter_tanggal').show();
             }
             else if($('#namaReport').val()=="EVALUASI_TARGET"){
                $('#preview').val("Generate");
                $('#filter_format').hide();
                $('#bulan').show();
                $('#tahun').show();
             }
             else if($('#namaReport').val()=="NERACA_PERCOBAAN"){
                $('#filter_periode').hide();
                $('#filter_tanggal').show();
                $('#filter_level').show();
             }
             else if($('#namaReport').val()=="KONTRIBUSI_HASIL"){
                $('#tahun').show();
             }
             else if($('#namaReport').val()=="UNIT"){
                $('#bulan').show();
                $('#tahun').show();
                $('#filter_jenis').show();
             }
             else if($('#namaReport').val()=="RK_CABANG"){
                $('#bulan').show();
                $('#tahun').show();
             }
             else if($('#namaReport').val()=="RINCIAN_NERACA"){
                $('#bulan').show();
                $('#tahun').show();
                $('#lblsd').show();
                $('#filter_tanggal').show();
                $('#filter_level').show();

             }
         });
	    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    });

    previewData = function(){
        var formatReport = $('input[name="formatReport"]:checked').val();
        var bulan  = $('#bulan').val();
        var tahun  = $('#tahun').val();
        var dibuat  = $('#dibuat').val();
        var disetujui  = $('#disetujui').val();
        var tanggal  = $('#search_tanggal').val();
        var tanggal1  = $('#search_tanggal1').val();
        var tanggal2  = $('#search_tanggal2').val();
        var jenis  = $('#jenis').val();
        var tipeLR  = $('input[name="tipeLR"]:checked').val();
        var level  = $('#level').val();
        var companyDealer  = $('#companyDealer').val();
        if($('#namaReport').val()=="NERACA"){ //neraca
           window.location = "${request.contextPath}/neraca/previewData?bulan="+bulan+"&tahun="+tahun+"&level="+level+"&companyDealer="+companyDealer+"&format="+formatReport;
        }
        else if($('#namaReport').val()=="RINCIAN_NERACA"){ //rincian neraca
           window.location = "${request.contextPath}/rincianTransaksi/printNeraca?bulan="+bulan+"&tahun="+tahun+"&tanggal="+tanggal+"&level="+level+"&companyDealer="+companyDealer+"&format="+formatReport;
        }
        else if($('#namaReport').val()=="RUGILABA"){ //rugi Laba
            if(tipeLR=="b"){
               window.location = "${request.contextPath}/rugiLaba/previewData?bulan="+bulan+"&tahun="+tahun+"&level="+level+"&companyDealer="+companyDealer+"&tipe="+tipeLR+"&format="+formatReport;
            }else{
               window.location = "${request.contextPath}/rugiLaba/previewDataTahunan?bulan="+bulan+"&tahun="+tahun+"&level="+level+"&companyDealer="+companyDealer+"&tipe="+tipeLR+"&format="+formatReport;
            }
        }
        else if($('#namaReport').val()=="RINCIAN_RUGILABA"){ //perincian rugi laba
           window.location = "${request.contextPath}/rincianTransaksi/printRugiLaba?bulan="+bulan+"&tahun="+tahun+"&level="+level+"&companyDealer="+companyDealer+"&format="+formatReport;
        }
        else if($('#namaReport').val()=="PENDING_MASUK_BAHAN"){ //daftar pending masuk bahan
           window.location = "${request.contextPath}/pendingPartBahan/pendingMasuk?bulan="+bulan+"&tahun="+tahun+"&companyDealer="+companyDealer+"&format="+formatReport;
        }
        else if($('#namaReport').val()=="PENDING_MASUK_PARTS"){ //daftar pending masuk part
           window.location = "${request.contextPath}/pendingPart/pendingMasuk?bulan="+bulan+"&tahun="+tahun+"&companyDealer="+companyDealer+"&format="+formatReport;
        }
        else if($('#namaReport').val()=="PENDING_MASUK_SO"){ //daftar pending masuk So Per SA
           window.location = "${request.contextPath}/pendingSoPerSa/pendingMasuk?bulan="+bulan+"&tahun="+tahun+"&companyDealer="+companyDealer+"&format="+formatReport;
        }
        else if($('#namaReport').val()=="PENDING_KELUAR_BAHAN"){ //daftar pending keluar bahan
           window.location = "${request.contextPath}/pendingPartBahan/pendingKeluar?bulan="+bulan+"&tahun="+tahun+"&companyDealer="+companyDealer+"&format="+formatReport;
        }
        else if($('#namaReport').val()=="PENDING_KELUAR_PARTS"){ //daftar pending keluar part
           window.location = "${request.contextPath}/pendingPart/pendingKeluar?bulan="+bulan+"&tahun="+tahun+"&companyDealer="+companyDealer+"&format="+formatReport;
        }
        else if($('#namaReport').val()=="PENDING_KELUAR_SO"){ //daftar pending keluar So Per SA
           window.location = "${request.contextPath}/pendingSoPerSa/pendingKeluar?bulan="+bulan+"&tahun="+tahun+"&companyDealer="+companyDealer+"&format="+formatReport;
        }
        else if($('#namaReport').val()=="TMO"){ //tmo per sa
           window.location = "${request.contextPath}/laporanPerSa/printTmo?bulan="+bulan+"&tahun="+tahun+"&companyDealer="+companyDealer+"&format="+formatReport;
        }
        else if($('#namaReport').val()=="PENJUALAN_BARANG"){ //penjualan barang
           var namaGoods = $('#goods_id').val();
           if(namaGoods==""){
            alert("DATA BELUM LENGKAP");
            return;
           }
           window.location = "${request.contextPath}/laporanPerSa/printPenjualanBarang?bulan="+bulan+"&tahun="+tahun+"&namaGoods="+namaGoods+"&companyDealer="+companyDealer+"&format="+formatReport;
        }
        else if($('#namaReport').val()=="HASS"){ //hass
           window.location = "${request.contextPath}/laporanHass/previewData?tanggal="+tanggal+"&companyDealer="+companyDealer+"&format="+formatReport;
        }
        else if($('#namaReport').val()=="EVALUASI_TARGET"){ //evaluasi target
            window.location = "${request.contextPath}/evaluasiTarget/previewData?bulan="+bulan+"&tahun="+tahun+"&jenis="+jenis+"&companyDealer="+companyDealer+"&format="+formatReport;
        }
        else if($('#namaReport').val()=="NERACA_PERCOBAAN"){ //neraca percobaan
           window.location = "${request.contextPath}/neracaPercobaan/previewData?tanggal="+tanggal+"&level="+level+"&companyDealer="+companyDealer+"&format="+formatReport;
        }
        else if($('#namaReport').val()=="KONTRIBUSI_HASIL"){ //Kontribusi Hasil
           window.location = "${request.contextPath}/kontribusiHasil/previewData?tahun="+tahun+"&companyDealer="+companyDealer+"&format="+formatReport;
        }
        else if($('#namaReport').val()=="UNIT"){ //Laporan Unit Per Model
           if(jenis=="General Repair"){
                window.location = "${request.contextPath}/lapUnitPerModel/previewDataGR?bulan="+bulan+"&tahun="+tahun+"&jenis="+jenis+"&companyDealer="+companyDealer+"&format="+formatReport;
            }else{
                window.location = "${request.contextPath}/lapUnitPerModel/previewDataBP?bulan="+bulan+"&tahun="+tahun+"&jenis="+jenis+"&companyDealer="+companyDealer+"&format="+formatReport;
            }
        }else if($('#namaReport').val()=="RK_CABANG"){ //Laporan R/K
            window.location = "${request.contextPath}/lapRkCabang/previewData?bulan="+bulan+"&tahun="+tahun+"&companyDealer="+companyDealer+"&format="+formatReport;
        }else if($('#namaReport').val()=="RINCIAN_NERACAPERCOBAAN"){ //Laporan Perincian Neraca Percobaan
            window.location = "${request.contextPath}/rincianTransaksi/printNeracaPercobaan?bulan="+bulan+"&tahun="+tahun+"&level="+level+"&companyDealer="+companyDealer+"&format="+formatReport;
        }
        else if($('#namaReport').val()=="MEMORIAL"){ //print memorial
           window.location = "${request.contextPath}/journal/printJurnalMemorial?tanggal="+tanggal1+"&tanggal2="+tanggal2+"&companyDealer="+companyDealer+"&dibuat="+dibuat+"&disetujui="+disetujui+"&format="+formatReport;
        }
    }

	    var checkin = $('#search_tanggal1').datepicker({
            onRender: function(date) {
                return '';
            }
        }).on('changeDate', function(ev) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate());
            checkout.setValue(newDate);
            checkin.hide();
            $('#search_tanggal2')[0].focus();
        }).data('datepicker');

        var checkout = $('#search_tanggal2').datepicker({
            onRender: function(date) {
                return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');

</g:javascript>

</head>
<body>
<div class="navbar box-header no-border">
    Laporan Pembukuan
</div><br>
<div class="box">
    <div class="span12" id="appointmentGr-table">
        <div id="form-appointmentGr" class="form-horizontal">
            <fieldset>
                <div class="control-group" id="filter_format"style="display: none">
                    <label class="control-label" for="filter_periode" style="text-align: left;">
                        Format File
                    </label>
                    <div id="filter_format2" class="controls">
                        <g:radioGroup name="formatReport" id="formatReport" values="['x','p']" labels="['Excel','PDF']" value="x" required="" >
                            ${it.radio} ${it.label}
                        </g:radioGroup>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="filter_periode" style="text-align: left;">
                        Company Dealer
                    </label>
                    <div id="filter_company" class="controls">
                        <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                            <g:select name="companyDealer" id="companyDealer" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                        </g:if>
                        <g:else>
                            <g:select name="companyDealer" id="companyDealer" readonly="" disabled="" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                        </g:else>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="t951Tanggal" style="text-align: left;">
                        Periode
                    </label>
                    <div id="filter_tipeLR" style="display: none" class="controls">
                        <g:radioGroup name="tipeLR" id="tipeLR" values="['b','t']" labels="['Bulanan','Tahunan']" value="b" required="" onclick="cekJenis()">
                            ${it.radio} ${it.label}
                        </g:radioGroup>
                    </div>
                    <div id="filter_periode" class="controls">
                        %{
                            def listBulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
                        }%
                        <select name="bulan" id="bulan" style="width: 170px;margin-right: 10px">
                            %{
                                for(int i=0;i<12;i++){
                                    out.print('<option value="'+(i+1)+'">'+listBulan.get(i)+'</option>');
                                }
                            }%
                        </select>
                        <select name="tahun" id="tahun" style="width: 106px">
                            %{
                                for(def a = (new Date().format("yyyy")).toInteger();a >= 2014 ;a--){
                                    out.print('<option value="'+a+'">'+a+'</option>');
                                }
                            }%
                        </select>
                    </div>
                    <div id="lblsd" style="display: none; padding-bottom: 5px" class="controls">
                        &nbsp; &nbsp; sampai dengan
                    </div>
                    <div id="filter_tanggal" class="controls">
                        <ba:datePicker id="search_tanggal" name="search_tanggal" precision="day" format="dd/MM/yyyy"  value="${new Date()}" />
                    </div>
                    <div id="filter_tanggal1" class="controls">
                        <ba:datePicker id="search_tanggal1" name="search_tanggal1" precision="day" format="dd/MM/yyyy"  value="${new Date()}" />
                        <ba:datePicker id="search_tanggal2" name="search_tanggal2" precision="day" format="dd/MM/yyyy"  value="${new Date()}" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="t951Tanggal" style="text-align: left;">
                        Nama Report
                    </label>
                    <div id="filter_nama" class="controls">
                        <select name="namaReport" id="namaReport" size="21" style="width: 44%; font-size: 12px;">
                            <option value="NERACA">01. Neraca</option>
                            <option value="RINCIAN_NERACA">02. Perincian Transaksi Neraca </option>
                            <option value="RUGILABA">03. Rugi Laba</option>
                            <option value="RINCIAN_RUGILABA">04. Perincian Transaksi Rugi Laba</option>
                            <option value="NERACA_PERCOBAAN">05. Neraca Percobaan</option>
                            <option value="RINCIAN_NERACAPERCOBAAN">06. Laporan Perincian Neraca Percobaan</option>
                            <option value="HASS">07. Laporan HASS</option>
                            <option value="MEMORIAL">08. Jurnal Memorial</option>
                            <option value="PENDING_MASUK_BAHAN">09. Daftar Pending Masuk Bahan</option>
                            <option value="PENDING_MASUK_PARTS">10. Daftar Pending Masuk Parts</option>
                            <option value="PENDING_MASUK_SO">11. Daftar Pending Masuk SO Per SA</option>
                            <option value="PENDING_KELUAR_BAHAN">12. Daftar Pending Keluar Bahan</option>
                            <option value="PENDING_KELUAR_PARTS">13. Daftar Pending Keluar Parts</option>
                            <option value="PENDING_KELUAR_SO">14. Daftar Pending Keluar SO Per SA</option>
                            <option value="TMO">15. Laporan TMO Per SA</option>
                            <option value="PENJUALAN_BARANG">16. Laporan Penjualan Barang</option>
                            <option value="KONTRIBUSI_HASIL">17. Kontribusi Hasil</option>
                            <option value="EVALUASI_TARGET">18. Evaluasi Target</option>
                            <option value="UNIT">19. Laporan Unit Per Model</option>
                            <option value="RK_CABANG">20. Laporan R/K Cabang</option>
                        </select>
                    </div>
                </div>
                <div class="control-group" id="memorial1"style="display: none">
                    <label class="control-label" for="filter_nama" style="text-align: left;">
                        Dibuat Oleh
                    </label>
                    <div id="filter_divisi31" class="controls">
                        <g:textField name="dibuat" id="dibuat" style="width: 270px; text-transform: uppercase" value="${dibuat}" />
                    </div>
                </div>
                <div class="control-group" id="memorial2"style="display: none">
                    <label class="control-label" for="filter_nama" style="text-align: left;">
                        Disetujui Oleh
                    </label>
                    <div id="filter_divisi21" class="controls">
                        <g:textField name="disetujui" id="disetujui" style="width: 270px; text-transform: uppercase" value="${disetujui}" />
                    </div>
                </div>
                <div class="control-group" id="divgoods"style="display: none">
                    <label class="control-label" for="filter_nama" style="text-align: left;">
                        Goods
                    </label>
                    <div id="filter_divisi" class="controls">
                        <g:textField name="goods.name" id="goods_nama" readonly="" style="width: 270px;" />
                        <span class="add-on">
                            <a href="javascript:void(0);" id="btnCariGoods">
                                <i class="icon-search"/>
                            </a>
                        </span>
                        <g:hiddenField name="goods.id" id="goods_id" />
                    </div>
                </div>

                <div class="control-group" id="filter_jenis">
                    <label class="control-label" for="t951Tanggal" style="text-align: left;">
                        Jenis :
                    </label>
                    <div id="jeniss" class="controls">
                        <select name="jenis" id="jenis" style="width: 170px">
                            %{
                                out.print('<option value="General Repair">GR</option>');
                                out.print('<option value="Body Paint">BP</option>');
                            }%
                        </select>
                    </div>
                </div>
                <div class="control-group" id="filter_level">
                    <label class="control-label" for="t951Tanggal" style="text-align: left;">
                        Level :
                    </label>
                    <div id="filter_periodes" class="controls">
                        <select name="level" id="level" style="width: 170px">
                            %{
                                for(int i=1;i<=5;i++){
                                    out.print('<option value="'+i+'">Level '+i+'</option>');
                                }
                            }%
                        </select>
                    </div>
               </div>
            </fieldset>
        </div>
           <g:field type="button" onclick="previewData()" class="btn btn-primary create" name="preview" id="preview"  value="${message(code: 'default.button.upload.label', default: 'Preview')}" />
           <g:field type="button" onclick="window.location.replace('#/home')" class="btn btn-cancel cancel" name="cancel"  value="${message(code: 'default.button.upload.label', default: 'Close')}" />
    </div>
</div>
<div id="goodsModal" class="modal fade">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content" style="width: 800px;">
            <div class="modal-header">
                <a class="close" href="javascript:void(0);" data-dismiss="modal">×</a>
                <h4>Data Goods - List</h4>
            </div>
            <!-- dialog body -->
            <div class="modal-body" id="goodsModal-body" style="max-height: 800px;">
                <div class="box">
                    <div class="span12">
                        <fieldset>
                            <table>
                                <tr style="display:table-row;">
                                    <td style="width: 130px; display:table-cell; padding:5px;">
                                        <label class="control-label" for="sCriteria_nama">Kode Goods</label>
                                    </td>
                                    <td style="width: 130px; display:table-cell; padding:5px;">
                                        <input type="text" id="sCriteria_kode">
                                    </td>
                                    <td style="width: 130px; display:table-cell; padding:5px;">
                                        <label class="control-label" for="sCriteria_nama">Nama Goods</label>
                                    </td>
                                    <td style="width: 130px; display:table-cell; padding:5px;">
                                        <input type="text" id="sCriteria_nama">
                                    </td>
                                    <td >
                                        <a id="btnSearchGoods" class="btn btn-primary" href="javascript:void(0);">
                                            Cari
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                </div>
                <g:render template="dataTablesGoods" />
            </div>

            <div class="modal-footer">
                <a href="javascript:void(0);" class="btn cancel" onclick="oFormService.fnHideGoodsDataTable();">
                    Tutup
                </a>
                <a href="javascript:void(0);" id="btnPilihGoods" class="btn btn-primary">
                    Pilih Goods
                </a>
            </div>
        </div>
    </div>
</div>
</body>
</html>
