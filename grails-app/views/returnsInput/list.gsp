
<%@ page import="com.kombos.parts.Returns" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'returns.label', default: 'Input Parts Return')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
    <g:javascript>
		$(function(){
			$('#m121Nama').typeahead({
			    source: function (query, process) {
			   // alert("nilai query : "+query);
			       return $.get('${request.contextPath}/returnsInput/kodeList', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});
    </g:javascript>
		<g:javascript disposition="head">
 var m121Nama = "";
    var noUrut = 1;
    var cekStatus = "${aksi}";
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/returnsInput/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };

    loadForm = function(data, textStatus){
		$('#returnsInput-form').empty();
    	$('#returnsInput-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#returnsInput-table").hasClass("span12")){
   			$("#returnsInput-table").toggleClass("span12 span5");
        }
        $("#returnsInput-form").css("display","block");
   	}
   	
   	expandTableLayout = function(){
   		if($("#returnsInput-table").hasClass("span5")){
   			$("#returnsInput-table").toggleClass("span5 span12");
   		}
        $("#returnsInput-form").css("display","none");
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#returnsInput-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/returnsInput/massdelete',
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadRequestTable();
    		}
		});
		
   	}

});
            $(document).ready(function()
         {
             $('input:radio[name=search_t162StaFA]').click(function(){
                if($('input:radio[name=search_t162StaFA]:nth(0)').is(':checked')){
                    $("#search_t162NoReff").prop('disabled', false);

                }else{
                    $("#search_t162NoReff").val("")
                    $("#search_t162NoReff").prop('disabled', true);
                }
             });


         });

    //darisini
	$("#returnsAddModal").on("show", function() {
		$("#returnsAddModal .btn").on("click", function(e) {
			$("#returnsAddModal").modal('hide');
		});
	});
	$("#returnsAddModal").on("hide", function() {
		$("#returnsAddModal a.btn").off("click");
	});
    loadReturnsAddModal = function(){
	    if(m121Nama != $('#m121Nama').val()){
               $("#returnsInput-table tbody .row-select").each(function() {
                    var id = $(this).next("input:hidden").val();
                    var row = $(this).closest("tr").get(0);
                    returnsTable.fnDeleteRow(returnsTable.fnGetPosition(row));
                });
          }
    	    m121Nama = $('#m121Nama').val();
		$("#returnsAddContent").empty();
		$.ajax({type:'POST', url:'${request.contextPath}/returnsAdd',
		data : {vendor : m121Nama},
   			success:function(data,textStatus){
					$("#returnsAddContent").html(data);
				    $("#returnsAddModal").modal({
						"backdrop" : "static",
						"keyboard" : true,
						"show" : true
					}).css({'width': '1085px','margin-left': function () {return -($(this).width() / 2);}});

   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});



    }

    
       tambahReq = function(){

          var checkGoods =[];
            $("#ReturnsAdd-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    var nRow = $(this).parents('tr')[0];

					checkGoods.push(nRow);
                }
            });
            if(checkGoods.length<1){
                alert('Pilih Data Yang akan ditambahkan?');
                loadReturnsAddModal();
            } else {
				for (var i=0;i < checkGoods.length;i++){

					var aData = ReturnsAddTable.fnGetData(checkGoods[i]);

					returnsTable.fnAddData({
						'id': aData['id'],

						'goods': aData['goods'],
						'goods2': aData['goods2'],
						'tglReceive': aData['tglReceive'],
						'po': aData['po'],
						'tglpo': aData['tglpo'],
						'nomorInvoice': aData['nomorInvoice'],
						'tglInvoice': aData['tglInvoice'],
						'QInvoice': aData['QInvoice'],
						'Qreceive': aData['Qreceive'],
						'Qreturn': '0',
						'Qreff': aData['Qreff']

						});

					$('#qty-'+aData['id']).autoNumeric('init',{
						vMin:'0',
						vMax:'999999999999999999',
						mDec: null,
						aSep:''
					});
				}
				 loadReturnsAddModal();

			}

      }
      deleteData = function(){
                var checkGoods =[];
                $("#returnsInput-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    var row = $(this).closest("tr").get(0);
                    returnsTable.fnDeleteRow(returnsTable.fnGetPosition(row));
                }
                });
      }
      updateReturns = function(){
            var formReturns = $('#returnsInput-table').find('form');
                var qtyNol = false
                var checkGoods =[];
                $("#returnsInput-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    var nRow = $(this).parents('tr')[0];
                    var aData = returnsTable.fnGetData(nRow)
                    var qtyInput = $('#qty-' + aData['id'], nRow);
                    if(qtyInput.val()==0 || qtyInput.val()==""){
                       qtyNol = true
                    }
                    checkGoods.push(id);

                    formReturns.append('<input type="hidden" name="qty-'+aData['id'] + '" value="'+qtyInput[0].value+'" class="deleteafter">');
                }
                });
                if(checkGoods.length<1){
                    alert('Anda belum memilih data yang akan ditambahkan');
                     reloadReturnsTable();
                }else{
                    if($("#tanggal").val()=="" || $("#m121Nama").val()==""){
                        toastr.error('<div>Mohon Diisi Dengan Benar.</div>');
                    }else if(qtyNol == true){
                        toastr.error('<div>Quantity Tidak Boleh NOL</div>');
                        formReturns.find('.deleteafter').remove();
                    }
                    else{
                         var r = confirm("Anda yakin data akan disimpan?");
                         if(r==true){
                            var rows = returnsTable.fnGetNodes();
                            $("#ids").val(JSON.stringify(checkGoods));
                            $.ajax({
                                url:'${request.contextPath}/returnsInput/ubah',
                                type: "POST", // Always use POST when deleting data
                                data : formReturns.serialize(),
                                success : function(data){
                                  if(data=='fail'){
                                    toastr.error('<div>Vendor Tidak Benar.</div>');
                                  }else{
                                    if(rows.length > 10){
                                        $("#returnsInput-table tbody .row-select").each(function() {
                                            if(this.checked){
                                                var row = $(this).closest("tr").get(0);
                                                returnsTable.fnDeleteRow(returnsTable.fnGetPosition(row));
                                                reloadReturnsTable();
                                            }
                                        });
                                        formReturns.append('<input type="hidden" name="noReturnBefore" value="'+data+'" class="deleteafter">');
                                        toastr.success('<div>Sukses</div>');
                                    }else{
                                        window.location.href = '#/returns' ;
                                        toastr.success('<div>Parts Returns telah di Ubah.</div>');
                                        formReturns.find('.deleteafter').remove();
                                        $("#ids").val('');
                                    }

                                  }
                                },
                            error: function(xhr, textStatus, errorThrown) {
                            alert('Internal Server Error');
                            }
                            });

                            checkGoods = [];
                          }
                    }
                }
      };
      createRequest = function(){
                var formReturns = $('#returnsInput-table').find('form');
                var qtyNol = false
                var checkGoods =[];
                $("#returnsInput-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    var nRow = $(this).parents('tr')[0];
                    var aData = returnsTable.fnGetData(nRow)
                    var qtyInput = $('#qty-'+aData['id'], nRow);
                    if(qtyInput.val()==0 || qtyInput.val()==""){
                       qtyNol = true
                    }
                    checkGoods.push(id);

                    formReturns.append('<input type="hidden" name="qty-'+aData['id'] + '" value="'+qtyInput[0].value+'" class="deleteafter">');
                }
                });
                if(checkGoods.length<1){
                    alert('Anda belum memilih data yang akan ditambahkan');
                     reloadReturnsTable();
                }else{
                    if($("#tanggal").val()=="" || $("#m121Nama").val()==""){
                        toastr.error('<div>Mohon Diisi Dengan Benar.</div>');
                    }else if(qtyNol == true){
                        toastr.error('<div>Quantity Tidak Boleh NOL</div>');
                        formReturns.find('.deleteafter').remove();
                    }
                    else{
                         var r = confirm("Anda yakin data akan disimpan?");
                         if(r==true){
                            var rows = returnsTable.fnGetNodes();
                            $("#ids").val(JSON.stringify(checkGoods));
                            $.ajax({
                                url:'${request.contextPath}/returnsInput/req',
                                type: "POST", // Always use POST when deleting data
                                data : formReturns.serialize(),
                                success : function(data){
                                  if(data=='fail'){
                                    toastr.error('<div>Vendor Tidak Benar.</div>');
                                  }else{
                                    if(rows.length > 10){
                                        $("#returnsInput-table tbody .row-select").each(function() {
                                            if(this.checked){
                                                var row = $(this).closest("tr").get(0);
                                                returnsTable.fnDeleteRow(returnsTable.fnGetPosition(row));
                                                reloadReturnsTable();
                                            }
                                        });
                                        formReturns.append('<input type="hidden" name="noReturnBefore" value="'+data+'" class="deleteafter">');
                                        toastr.success('<div>Sukses</div>');
                                    }else{
                                        window.location.href = '#/returns' ;
                                        toastr.success('<div>Parts Returns telah dibuat.</div>');
                                        formReturns.find('.deleteafter').remove();
                                        $("#ids").val('');
                                    }

                                  }
                                },
                            error: function(xhr, textStatus, errorThrown) {
                            alert('Internal Server Error');
                            }
                            });

                            checkGoods = [];
                          }
                    }
                }

    };

</g:javascript>

	</head>
	<body>
    <div class="navbar box-header no-border">
        <span class="pull-left">
            <g:if test="${aksi=='ubah'}">
                Edit Parts Return
            </g:if>
            <g:else>
                Input Parts Return
            </g:else>
        </span>
        <ul class="nav pull-right">
            <li>&nbsp;</li>
        </ul>
    </div><br>
	<div class="box">
		<div class="span12" id="returnsInput-table">
            <div>${flash.message}</div>
            <fieldset>
                <form id="form-returns" class="form-horizontal">
                    <input type="hidden" name="ids" id="ids" value="">
                    <g:if test="${aksi=='ubah'}">
                        <div class="control-group">
                            <label class="control-label" for="t951Tanggal" style="text-align: left;">
                                Nomor Return
                            </label>
                            <div id="filter_wo" class="controls">
                                <g:textField name="noReturns" readonly="readonly" id="noReturns" value="${nomorReturns}" autocomplete="off" maxlength="220"  />
                            </div>
                        </div>
                    </g:if>
                        <div class="control-group">
                            <label class="control-label" for="userRole"  style="text-align: left;">Tanggal Return *
                            </label>
                            <div id="filter_status" class="controls">
                            <g:if test="${aksi=='ubah'}">
                                <ba:datePicker id="tanggal" name="tanggal" precision="day" format="dd-MM-yyyy" required="" value="${u_tanggal}"  />
                            </g:if>
                            <g:else>
                                <ba:datePicker id="tanggal" name="tanggal" precision="day" format="dd-MM-yyyy" required="" value="${new Date()}"  />
                            </g:else>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="t951Tanggal" style="text-align: left;">Vendor *
                            </label>
                            <div id="filter_vendor" class="controls">
                                <g:if test="${aksi=='ubah'}">
                                    <g:textField name="vendor" id="vendor" value="${u_vendor}" maxlength="220" readonly="readonly"  />
                                </g:if>
                                <g:else>
                                    <g:textField name="namaVendor" id="m121Nama" class="typeahead"  autocomplete="off" maxlength="220"  />
                                </g:else>
                            </div>
                        </div>
                </form>
            </fieldset>
            <g:if test="${aksi!='ubah'}">
            <fieldset>
                <table>
                    <tr>
                        <td>
                            <button style="width: 170px;height: 30px; border-radius: 5px" class="btn btn-primary" name="view" id="add" onclick="loadReturnsAddModal()">Add Parts</button>
                        </td>
                    </tr>
                </table>
            </fieldset>
            </g:if>
                <br>
			<g:render template="dataTables" />
            <g:if test="${aksi=='ubah'}">
                <g:field type="button" onclick="updateReturns()" class="btn btn-primary create" name="Edit"  value="${message(code: 'default.button.upload.label', default: 'Update')}" />
            </g:if>
            <g:else>
                <g:field type="button" onclick="deleteData()" class="btn btn-cancel create" name="deleteData" id="delData" value="${message(code: 'default.button.upload.label', default: 'Delete')}" />
                <g:field type="button" onclick="createRequest()" class="btn btn-primary create" name="tambahSave" id="tambahSave" value="${message(code: 'default.button.upload.label', default: 'Save')}" />
            </g:else>
            <g:field type="button" onclick="window.location.replace('#/returns');" class="btn btn-cancel" name="cancel" id="cancel" value="Cancel" />
		</div>
		<div class="span7" id="returns-form" style="display: none;"></div>
	</div>
    <div id="returnsAddModal" class="modal fade">
        <div class="modal-dialog" style="width: 1083px;">
            <div class="modal-content" style="width: 1083px;">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 1083px;">
                    <div id="returnsAddContent"/>
                <div class="iu-content"></div>

                </div>

            </div>
        </div>
    </div>
</body>
</html>

<g:javascript>
    <g:if test="${aksi=='ubah'}">
        %{--document.getElementById("tanggal").focus();--}%
    </g:if>
    <g:else>
        document.getElementById("m121Nama").focus();
    </g:else>
</g:javascript>

