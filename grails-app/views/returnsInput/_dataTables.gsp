
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="returnsInput_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;" rowspan="2">
				<div><g:message code="returnsInput.goods.label" default="Kode Parts" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;" rowspan="2">
                <div><g:message code="returnsInput.goods.label" default="Nama Parts" /></div>
            </th>



            <th style="border-bottom: none;padding: 5px;" rowspan="2">
                <div><g:message code="returnsInput.goods.label" default="Nomor Invoice" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;" rowspan="2">
                <div><g:message code="returnsInput.t162Qty1.label" default="Tanggal Invoice" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px; text-align: center" colspan="3">
                <div><g:message code="returnsInput.t162Qty1.label" default="QTy" /></div>
            </th>

		</tr>
        <tr>
            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="returnsInput.t162Qty1.label" default="Reff" /></div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="returnsInput.goods.label" default="Receive" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="returnsInput.t162Qty1.label" default="Return" /></div>
            </th>
        </tr>


	</thead>
</table>

<g:javascript>
var returnsTable;
var reloadReturnsTable;
$(function(){
	var recordsReturnsPerPage = [];
    var anReturnsSelected;
    var jmlRecReturnsPerPage=0;
    var id;
	reloadReturnsTable = function() {
		returnsTable.fnDraw();
	}

	
	$('#search_t162TglReturns').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t162TglReturns_day').val(newDate.getDate());
			$('#search_t162TglReturns_month').val(newDate.getMonth()+1);
			$('#search_t162TglReturns_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			returnsTable.fnDraw();
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	returnsTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	returnsTable = $('#returnsInput_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsReturns = $("#returnsInput_datatables tbody .row-select");
            var jmlReturnsCek = 0;
            var nRow;
            var idRec;
            rsReturns.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsReturnsPerPage[idRec]=="1"){
                    jmlReturnsCek = jmlReturnsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsReturnsPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecReturnsPerPage = rsReturns.length;
            if(jmlReturnsCek==jmlRecReturnsPerPage && jmlRecReturnsPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"aoColumns": [

{
	"sName": "goods",
	"mDataProp": "goods",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" id="idPopUp" value="'+row['id']+'">&nbsp;&nbsp'+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "goods2",
	"mDataProp": "goods2",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "nomorInvoice",
	"mDataProp": "nomorInvoice",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "tglInvoice",
	"mDataProp": "tglInvoice",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "Qreff",
	"mDataProp": "Qreff",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "Qreceive",
	"mDataProp": "Qreceive",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "Qreturn",
	"mDataProp": "Qreturn",
	"aTargets": [6],
	"bSearchable": true,
    "mRender": function ( data, type, row ) {
        return '<input id="qty-'+row['id']+'" class="inline-edit" type="text" style="width:218px;" value="'+data+'">';
    },
    "bSortable": true,
    "sWidth":"200px",
    "bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var t162ID = $('#filter_t162ID input').val();
						if(t162ID){
							aoData.push(
									{"name": 'sCriteria_t162ID', "value": t162ID}
							);
						}
	
						var goods = $('#filter_goods input').val();
						if(goods){
							aoData.push(
									{"name": 'sCriteria_goods', "value": goods}
							);
						}
	
						var t162Qty1 = $('#filter_t162Qty1 input').val();
						if(t162Qty1){
							aoData.push(
									{"name": 'sCriteria_t162Qty1', "value": t162Qty1}
							);
						}
	
						var t162Qty2 = $('#filter_t162Qty2 input').val();
						if(t162Qty2){
							aoData.push(
									{"name": 'sCriteria_t162Qty2', "value": t162Qty2}
							);
						}

						var t162TglReturns = $('#search_t162TglReturns').val();
						var t162TglReturnsDay = $('#search_t162TglReturns_day').val();
						var t162TglReturnsMonth = $('#search_t162TglReturns_month').val();
						var t162TglReturnsYear = $('#search_t162TglReturns_year').val();
						
						if(t162TglReturns){
							aoData.push(
									{"name": 'sCriteria_t162TglReturns', "value": "date.struct"},
									{"name": 'sCriteria_t162TglReturns_dp', "value": t162TglReturns},
									{"name": 'sCriteria_t162TglReturns_day', "value": t162TglReturnsDay},
									{"name": 'sCriteria_t162TglReturns_month', "value": t162TglReturnsMonth},
									{"name": 'sCriteria_t162TglReturns_year', "value": t162TglReturnsYear}
							);
						}
	
						var t162StaFA = $('#filter_t162StaFA input').val();
						if(t162StaFA){
							aoData.push(
									{"name": 'sCriteria_t162StaFA', "value": t162StaFA}
							);
						}
	
						var t162NoReff = $('#filter_t162NoReff input').val();
						if(t162NoReff){
							aoData.push(
									{"name": 'sCriteria_t162NoReff', "value": t162NoReff}
							);
						}
	
						var t162Qty1Available = $('#filter_t162Qty1Available input').val();
						if(t162Qty1Available){
							aoData.push(
									{"name": 'sCriteria_t162Qty1Available', "value": t162Qty1Available}
							);
						}
	
						var t162Qty2Available = $('#filter_t162Qty2Available input').val();
						if(t162Qty2Available){
							aoData.push(
									{"name": 'sCriteria_t162Qty2Available', "value": t162Qty2Available}
							);
						}
	
						var t162NamaPemohon = $('#filter_t162NamaPemohon input').val();
						if(t162NamaPemohon){
							aoData.push(
									{"name": 'sCriteria_t162NamaPemohon', "value": t162NamaPemohon}
							);
						}
	
						var t162xNamaUser = $('#filter_t162xNamaUser input').val();
						if(t162xNamaUser){
							aoData.push(
									{"name": 'sCriteria_t162xNamaUser', "value": t162xNamaUser}
							);
						}
	
						var t162xNamaDivisi = $('#filter_t162xNamaDivisi input').val();
						if(t162xNamaDivisi){
							aoData.push(
									{"name": 'sCriteria_t162xNamaDivisi', "value": t162xNamaDivisi}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#returnsInput_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsReturnsPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsReturnsPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#returnsInput_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsReturnsPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anReturnsSelected = returnsTable.$('tr.row_selected');

            if(jmlRecReturnsPerPage == anReturnsSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsReturnsPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
    if(cekStatus=="ubah"){
        var nomorReturns = "${nomorReturns}";
        $.ajax({
        url:'${request.contextPath}/returnsInput/getTableData',
    		type: "POST", // Always use POST when deleting data
    		data: { id: nomorReturns },
    		success : function(data){
    		    $.each(data,function(i,item){
                    returnsTable.fnAddData({
                        'id': item.id,
						'goods': item.kode,
						'goods2': item.nama,
						'tglReceive': item.tglReceive,
						'po': item.po,
						'tglpo': item.tglpo,
						'nomorInvoice': item.nomorInvoice,
						'tglInvoice': item.tglInvoice,
						'QInvoice': item.QInvoice,
						'Qreceive': item.Qreceive,
						'Qreturn': item.Qreturn,
						'Qreff': item.Qreff
                    });
                });
            }
		});
    }
});
</g:javascript>


			
