<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
        <style>
            table.fixed { table-layout:fixed; }
            table.fixed td { overflow: hidden; }
            .center {
                text-align:center;
            }
         </style>
		<g:javascript>
var goodsReceiveSubTable_${idTableSub};
$(function(){
    var recordspartsperpage = [];//new Array();
    var anPartsSelected;
    var jmlRecPartsPerPage=0;
    var id;
    var isSelected;
var anOpen = [];
	$('#goodsReceive_datatables_sub_${idTableSub} td.subcontrol').live('click',function () {
   		var nTr = this.parentNode;
   		console.log("nTr=" + nTr);
  		var i = $.inArray( nTr, anOpen );
  		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = goodsReceiveSubTable_${idTableSub}.fnGetData(nTr);
    		console.log("oData=" + oData);
    		console.log("oData.idGoodsReceive=" + oData.idGoodsReceive);
    		$('#spinner').fadeIn(1);
            isSelected = ${isSelected};
            console.log("oData=" + oData);
    		console.log("isSelected=" + isSelected + ", namaVendor=${namaVendor}");
	   		$.ajax({
	   		    type:'POST',
	   			//data : oData,
	   			data : {oData : oData, namaVendor : "${namaVendor}", idGoodsReceive : oData.idGoodsReceive, noReff : oData.noReff},
	   			url:'${request.contextPath}/goodsReceive/subsublist?isSelectedSub=' + isSelected,
	   			success:function(data,textStatus){
	   				var nDetailsRow = goodsReceiveSubTable_${idTableSub}.fnOpen(nTr,data,'details');
    				$('div.innerinnerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerinnerDetails', $(nTr).next()[0]).slideUp( function () {
      			goodsReceiveSubTable_${idTableSub}.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

    $('#goodsReceive_datatables_sub_${idTableSub} a.edit').live('click', function (e) {
        e.preventDefault();
         
        var nRow = $(this).parents('tr')[0];
         
        if ( nEditing !== null && nEditing != nRow ) {
            restoreRow( goodsReceiveSubTable_${idTableSub}, nEditing );
            editRow( goodsReceiveSubTable_${idTableSub}, nRow );
            nEditing = nRow;
        }
        else if ( nEditing == nRow && this.innerHTML == "Save" ) {
            saveRow( goodsReceiveSubTable_${idTableSub}, nEditing );
            nEditing = null;
        }
        else {
            editRow( goodsReceiveSubTable_${idTableSub}, nRow );
            nEditing = nRow;
        }
    } );
    if(goodsReceiveSubTable_${idTableSub})
    	goodsReceiveSubTable_${idTableSub}.dataTable().fnDestroy();
goodsReceiveSubTable_${idTableSub} = $('#goodsReceive_datatables_sub_${idTableSub}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
             var rsParts = $("#goodsReceive_datatables_sub_${idTableSub} tbody .row-select2");
             var jmlPartsCek = 0;
             var nRow;
             var idRec;
             rsParts.each(function() {
                 idRec = $(this).next("input:hidden").val();
                 nRow = $(this).parent().parent();//.addClass('row_selected');

                if(this.checked || recordspartsperpage[idRec]=="1"){
                    jmlPartsCek = jmlPartsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(!this.checked || recordspartsperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

             });
             jmlRecPartsPerPage = rsParts.length;
             %{--if(jmlPartsCek == jmlRecPartsPerPage && jmlRecPartsPerPage > 0){
                 $('.select-all2_${idTableSub}').attr('checked', true);
             } else {
                 $('.select-all2_${idTableSub}').attr('checked', false);
             }--}%
             //alert("apakah ini : "+rs.length);
         },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "noReff",
	"mDataProp": "noReff",
    "mRender": function ( data, type, row ) {
        return '<input id="goodsReceive-sub-row-'+row['id']+'" type="checkbox" class="pull-left row-select2" ${isSelected=='true'?'checked':''} > ' +
            '<input type="hidden" value="'+row['id']+'"><input type="hidden" value="${namaVendor}"><input type="hidden" value="'+ data +'">&nbsp;&nbsp;' + data;
    },
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "tipeBayar",
	"mDataProp": "tipeBayar",
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "tglReff",
	"mDataProp": "tglReff",
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "tglJamReceive",
	"mDataProp": "tglJamReceive",
	"bSortable": false,
	"bVisible": true
},
{
    "sName": "petugasReceive",
	"mDataProp": "petugasReceive",
	"bSortable": false,
	"sDefaultContent": '',
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        aoData.push(
                            {"name": 'namaVendor', "value": "${namaVendor}"},
                            {"name": 'VendorId', "value": "${VendorId}"},
                            {"name": 'isSelected', "value": "${isSelected}"}
						);

						var search_noReff = $('#search_noReff').val();
                        if(search_noReff){
                            aoData.push(
                                    {"name": 'search_noReff', "value": search_noReff}
                            );
                        }

                         var tglFrom = $('#search_tanggal').val();

                        if(tglFrom){
                            aoData.push(
                                    {"name": 'sCriteria_tglFrom', "value": tglFrom}
                            );
                        }

                        var tglTo = $('#search_tanggalAkhir').val();

                        if(tglTo){
                            aoData.push(
                                    {"name": 'sCriteria_tglTo', "value": tglTo}
                            );
                        }
                         var criteriaParts = $('input[name^=criteriaParts]:checked').val();
                         console.log("criteriaParts=" + criteriaParts);
                         if(criteriaParts){
                             aoData.push(
                                     {"name": 'criteriaParts', "value": criteriaParts}
                             );
                         }
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

    $('.select-all2_${idTableSub}').click(function(e) {
        var tw = $(this).parents(".dataTables_wrapper");
        var tc = tw.find('#goodsReceive_datatables_sub_${idTableSub} tbody').find('.row-select2').attr('checked', this.checked);
        if(this.checked)
            tc.parent().parent().addClass(' row_selected ');
        else
            tc.parent().parent().removeClass('row_selected');
        console.log("tc.parent().parent().html()=" + tc.parent().parent().html());
        e.stopPropagation();
        var namaVendorOuter = $(this).next("input:hidden").val();
        $("#goodsReceive_datatables_sub_${idTableSub} tbody .row-select2").each(function() {
            var next = $(this).find('.row-select3').next("input:hidden").val();
            console.log("next=" + next);
            var nextNext = $(this).find('.row-select3').next("input:hidden").next("input:hidden").val();
            console.log("nextNext=" + nextNext);

            var nRow3 = $('.row-select3').parent().parent();//.addClass('row_selected');
            console.log("nRow3=" + nRow3);
            console.log("this.checked=" + this.checked);
            if(this.checked){
                $('.sel-all3').each(function() {
                    var namaVendor = $(this).next("input:hidden").val();
                    var noReffInner = $(this).next("input:hidden").next("input:hidden").val();
                    console.log("sel-all3 namaVendor=" + namaVendor + " noReffInner=" + noReffInner);
                    if (namaVendorOuter == namaVendor) {
                        $(this).attr('checked', true);

                        var nRow = $(this).parent().parent().parent();//.addClass('row_selected');
                        console.log("select-all2 nRow.html()=" + nRow.html());
                        nRow.addClass('row_selected');
                    }
                });
                $('table[id^=goodsReceive_datatables_sub_sub_] tbody .row-select3').each(function() {
                    var namaVendor = $(this).next("input:hidden").next("input:hidden").val();
                    var noReffInner = $(this).next("input:hidden").next("input:hidden").next("input:hidden").val();
                    if (namaVendorOuter == namaVendor) {
                        $(this).attr('checked', true);

                        var nRow = $(this).parent().parent();//.addClass('row_selected');
                        console.log("row-select3 nRow.html()=" + nRow.html());
                        nRow.addClass('row_selected');
                    }
                });

            } else {
                $('.sel-all3').each(function() {
                    var namaVendor = $(this).next("input:hidden").val();
                    var noReffInnner = $(this).next("input:hidden").next("input:hidden").val();
                    if (namaVendorOuter == namaVendor) {
                        $(this).attr('checked', false);

                        var nRow = $(this).parent().parent().parent();//.addClass('row_selected');
                        nRow.removeClass('row_selected');
                    }
                });
                $('table[id^=goodsReceive_datatables_sub_sub_] tbody .row-select3').each(function() {
                    var namaVendor = $(this).next("input:hidden").next("input:hidden").val();
                    var noReffInner = $(this).next("input:hidden").next("input:hidden").next("input:hidden").val();
                    if (namaVendorOuter == namaVendor) {
                        $(this).attr('checked', false);
                        var nRow = $(this).parent().parent();//.addClass('row_selected');
                        nRow.removeClass('row_selected');
                    }
                });
            }
        });
    });

	$('#goodsReceive_datatables_sub_${idTableSub} tbody tr').live('click', function () {
        id = $(this).find('.row-select2').next("input:hidden").val();
        console.log("id=" + id);
        var vendorName = $(this).find('.row-select2').next("input:hidden").next("input:hidden").val();
        var noReff = $(this).find('.row-select2').next("input:hidden").next("input:hidden").next("input:hidden").val();
        console.log("vendorName=" + vendorName + " noReff=" + noReff);
        //if (vendorName) {   //untuk tree sublist saja
            if($(this).find('.row-select2').is(":checked"))
            {
                recordspartsperpage[id] = "1";
                $(this).find('.row-select2').parent().parent().addClass('row_selected');
                anPartsSelected = goodsReceiveSubTable_${idTableSub}.$('tr.row_selected');
                console.log("jmlRecPartsPerPage=" + jmlRecPartsPerPage + " anPartsSelected=" + anPartsSelected);
                if(jmlRecPartsPerPage == anPartsSelected.length){
                    $('.select-all2_${idTableSub}').attr('checked', true);
                }

                $('.sel-all3').each(function() {
                    var vendorNameInner = $(this).next("input:hidden").val();
                    var noReffInner = $(this).next("input:hidden").next("input:hidden").val();
                    console.log("sel-all3 vendorName=" + vendorName + " vendorNameInner=" + vendorNameInner + " noReffInner=" + noReffInner);
                    if (noReff == noReffInner) {
                        $(this).attr('checked', true);

                        var nRow = $(this).parent().parent().parent();//.addClass('row_selected');
                        console.log("sel-all3 nRow.html()=" + nRow.html());
                        nRow.addClass('row_selected');
                    }
                });

                $('table[id^=goodsReceive_datatables_sub_sub_] tbody .row-select3').each(function() {
                    var vendorNameInner = $(this).next("input:hidden").next("input:hidden").val();
                    var noReffInner = $(this).next("input:hidden").next("input:hidden").next("input:hidden").val();
                    console.log("row-select3 vendorNameInner=" + vendorNameInner + " vendorName=" + vendorName + " noReffInner=" + noReffInner);
                    if (noReff == noReffInner) {
                        $(this).attr('checked', true);

                        var nRow = $(this).parent().parent();//.addClass('row_selected');
                        console.log("row-select3 nRow.html()=" + nRow.html());
                        nRow.addClass('row_selected');
                    }
                });
            }
            else
            {
                recordspartsperpage[id] = "0";
                $('.select-all2_${idTableSub}').attr('checked', false);
                $('.select-all').attr('checked', false);
                $(this).find('.row-select2').parent().parent().removeClass('row_selected');

                $('.sel-all3').each(function() {
                    var vendorNameInner = $(this).next("input:hidden").val();
                    var noReffInner = $(this).next("input:hidden").next("input:hidden").val();
                    console.log("sel-all3 vendorName=" + vendorName + " vendorNameInner=" + vendorNameInner + " noReffInner=" + noReffInner);
                    if (noReff == noReffInner) {
                        $(this).attr('checked', false);

                        var nRow = $(this).parent().parent().parent();//.addClass('row_selected');
                        nRow.removeClass('row_selected');
                    }
                });
                $('table[id^=goodsReceive_datatables_sub_sub_] tbody .row-select3').each(function() {
                    var vendorNameInner = $(this).next("input:hidden").next("input:hidden").val();
                    var noReffInner = $(this).next("input:hidden").next("input:hidden").next("input:hidden").val();
                    console.log("row-select3 vendorName=" + vendorName + " vendorNameInner=" + vendorNameInner + " noReffInner=" + noReffInner);
                    if (noReff == noReffInner) {
                        $(this).attr('checked', false);

                        var nRow = $(this).parent().parent();//.addClass('row_selected');
                        nRow.removeClass('row_selected');
                    }
                });
            }
        //}
    });

    var checkbox_col2 = $(".table-selectable").find("thead").find("tr").first().find("th").first().find("div");
    if(checkbox_col2 && checkbox_col2.html()){
           if(checkbox_col2.html().search('type="checkbox"')==-1){
               checkbox_col2.prepend('<input type="checkbox" class="pull-left select-all2_${idTableSub} sel-all2" aria-label="Select all" title="Select all"/>&nbsp;&nbsp;')
           }

    }
                                                                                      

});
		</g:javascript>
	</head>
	<body>
<div class="innerDetails">
<table id="goodsReceive_datatables_sub_${idTableSub}" cellpadding="0" cellspacing="0" border="0"
	class="display table table-striped table-bordered table-hover fixed table-selectable fixed">
    <col width="24px" />
    <col width="25px" />
    <col width="150px" />
    <col width="150px" />
    <col width="175px" />
    <col width="200px" />
    <col width="500px" />
    <thead>
        <tr>
            <th></th>
            <th></th>
			<th style="border-bottom: none; padding: 5px; width: 200px;">
				<div class="center"><input type="checkbox" class="pull-left select-all2_${idTableSub} sel-all2" aria-label="Select all" title="Select all" ${isSelected=='true'?'checked':''} />
                    <input type="hidden" value="${namaVendor}">&nbsp;&nbsp;Nomor Referensi</div>
			</th>
			<th style="border-bottom: none; padding: 5px; width: 140px;">
				<div class="center">Tipe Bayar</div>
			</th>
            <th style="border-bottom: none; padding: 5px; width: 140px;">
                <div class="center">Tanggal Referensi</div>
            </th>
			<th style="border-bottom: none; padding: 5px; width: 200px;">
				<div class="center">Tanggal Dan Jam Receive</div>
			</th>
            <th style="border-bottom: none; padding: 5px; width: 200px;">
                <div class="center">Petugas Receive</div>
            </th>
         </tr>
			    </thead>
			    <tbody></tbody>
			</table>
</div>
</body>
</html>
