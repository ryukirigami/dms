<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
        <style>
            table.fixed { table-layout:fixed; }
            table.fixed td { overflow: hidden; }
            .center {
                text-align:center;
            }
         </style>
		<g:javascript>
var goodsReceiveSubSubTable_${idTableSubSub};
$(function(){
    var recordspartsperpage = [];//new Array();
    var anPartsSelected;
    var jmlRecPartsPerPage=0;
    var id;
    var isSelectedSub;
var anOpen = [];
goodsReceiveSubSubTable_${idTableSubSub} = $('#goodsReceive_datatables_sub_sub_${idTableSubSub}').dataTable({
		"sScrollX": "1396px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnDrawCallback": function () {
             var rsParts = $("#goodsReceive_datatables_sub_sub_${idTableSubSub} tbody .row-select3");
             var jmlPartsCek = 0;
             var nRow;
             var idRec;
             rsParts.each(function() {
                 idRec = $(this).next("input:hidden").val();
                 nRow = $(this).parent().parent();//.addClass('row_selected');

                 if(this.checked || recordspartsperpage[idRec]=="1"){
                     jmlPartsCek = jmlPartsCek + 1;
                     $(this).attr('checked', true);
                     nRow.addClass('row_selected');
                 } else if(!this.checked || recordspartsperpage[idRec]=="0"){
                     $(this).attr('checked', false);
                     nRow.removeClass('row_selected');
                 }

             });

             jmlRecPartsPerPage = rsParts.length;
             %{--if(jmlPartsCek == jmlRecPartsPerPage && jmlRecPartsPerPage > 0){--}%
                 %{--$('.select-all3_${idTableSubSub}').attr('checked', true);--}%
             %{--} else {--}%
                 %{--$('.select-all3_${idTableSubSub}').attr('checked', false);--}%
             %{--}--}%
             //alert("apakah ini : "+rs.length);
         },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubSubList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sDefaultContent": ''
},
{
	"sName": "partsCode",
	"mDataProp": "partsCode",
	"mRender": function ( data, type, row ) {
	    return '<input id="goodsReceive-sub-sub-row-'+row['id']+'" type="checkbox" class="pull-left row-select3" ${isSelectedSub=='true'?'checked':''} > ' +
	    '<input type="hidden" value="'+row['id']+'"><input type="hidden" value="${namaVendor}"><input type="hidden" value="${noReff}"><input type="hidden" value="${idGoodsReceive}"><input type="hidden" value="'+ data +'">&nbsp;&nbsp;' + data;
	},
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "partsName",
	"mDataProp": "partsName",
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "poNumber",
	"mDataProp": "poNumber",
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "poDate",
	"mDataProp": "poDate",
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "orderType",
	"mDataProp": "orderType",
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "qtyRef",
	"mDataProp": "qtyRef",
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "qtyRcv",
	"mDataProp": "qtyRcv",
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "qtyRusak",
	"mDataProp": "qtyRusak",
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "qtySalah",
	"mDataProp": "qtySalah",
	"bSortable": false,
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        console.log("fnServerData namaVendor=${namaVendor}");
                        console.log("fnServerData idGoodsReceive=${idGoodsReceive}");
                        aoData.push(
                            {"name": 'noReff', "value": "${noReff}"},
                            {"name": 'tglReff', "value": "${tglReff}"},
                            {"name": 'tglJamReceive', "value": "${tglJamReceive}"},
                            {"name": 'petugasReceive', "value": "${petugasReceive}"},
                            {"name": 'namaVendor', "value": "${namaVendor}"},
                            {"name": 'isSelectedSub', "value": "${isSelectedSub}"},
                            {"name": 'idGoodsReceive', "value": "${idGoodsReceive}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

    $('.select-all3_${idTableSubSub}').click(function(e) {
        var tw = $(this).parents(".dataTables_wrapper");
        console.log("tw=" + tw);
        console.log("idTableSubSub=" + ${idTableSubSub} + " this.checked=" + this.checked);
        var tc = tw.find('#goodsReceive_datatables_sub_sub_${idTableSubSub} tbody').find('.row-select3').attr('checked', this.checked);
        console.log("tc.html()=" + tc.html());
        console.log("this.checked=" + this.checked);
        if(this.checked)
            tc.parent().parent().addClass(' row_selected ');
        else
            tc.parent().parent().removeClass('row_selected');
        console.log("tc.html()=" + tc.html());
        console.log("tc.parent().parent().html()=" + tc.parent().parent().html());
        e.stopPropagation();
    });


	$('#goodsReceive_datatables_sub_sub_${idTableSubSub} tbody tr').live('click', function () {
        id = $(this).find('.row-select3').next("input:hidden").val();
        if($(this).find('.row-select3').is(":checked")){
            recordspartsperpage[id] = "1";
            $(this).find('.row-select3').parent().parent().addClass('row_selected');
            anPartsSelected = goodsReceiveSubSubTable_${idTableSubSub}.$('tr.row_selected');
            if(jmlRecPartsPerPage == anPartsSelected.length){
                $('.select-all3_${idTableSubSub}').attr('checked', true);
            }
        } else {
            recordspartsperpage[id] = "0";
            $('.select-all3_${idTableSubSub}').attr('checked', false);
            $(this).find('.row-select3').parent().parent().removeClass('row_selected');
        }
    });

    var checkbox_col3 = $(".table-selectable").find("thead").find("tr").first().find("th").first().find("div");
    if(checkbox_col3 && checkbox_col3.html()){
           if(checkbox_col3.html().search('type="checkbox"')==-1){
               checkbox_col3.prepend('<input type="checkbox" class="pull-left select-all3_${idTableSubSub} sel-all3" aria-label="Select all" title="Select all"/>&nbsp;&nbsp;')
           }

    }
});
		</g:javascript>
	</head>
	<body>
	<div class="innerinnerDetails">
<table id="goodsReceive_datatables_sub_sub_${idTableSubSub}" cellpadding="0" cellspacing="0" border="0"
	class="display table table-striped table-bordered table-hover fixed table-selectable ">
    <col width="23px" />
    <col width="24px" />
    <col width="25px" />
    <col width="163px" />
    <col width="169px" />
    <col width="109px" />
    <col width="109px" />
    <col width="100px" />
    <col width="100px" />
    <col width="100px" />
    <col width="100px" />
    <col width="100px" />
    <thead>
        <tr>
           <th></th>
            <th></th>
            <th></th>
			<th style="border-bottom: none; padding: 5px;">
				<div class="center"><input type="checkbox" class="pull-left select-all3_${idTableSubSub} sel-all3" aria-label="Select all" title="Select all" ${isSelectedSub=='true'?'checked':''} />
                    <input type="hidden" value="${namaVendor}"><input type="hidden" value="${noReff}"><input type="hidden" value="${idGoodsReceive}">&nbsp;Kode Part</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div class="center">Nama Part</div>
			</th>
            <th style="border-bottom: none; padding: 5px;">
                <div class="center">Nomor PO</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div class="center">Tanggal PO</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div class="center">Tipe Order</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div class="center">Qty Refr</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div class="center">Qty Receive</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div class="center">Qty Rusak</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div class="center">Qty Salah</div>
            </th>

         </tr>
			    </thead>
			    <tbody></tbody>
			</table>
	</div>
</body>
</html>
