
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="goodsReceive_datatables_${idTable}" cellpadding="0" cellspacing="0" border="0"
	class="display table table-striped table-bordered table-hover table-selectable fixed" style="table-layout: fixed; ">
    <col width="25px" />
	<col width="1200px" />
	<thead>
		<tr>
            <th></th>
			<th style="border-bottom: none;padding: 5px;">
				<div class="center"><input type="checkbox" class="pull-left select-all" aria-label="Select all" title="Select all"/>&nbsp;&nbsp;Vendor / Company Dealer</div>
			</th>
		</tr>
	</thead>
</table>

<g:javascript>
var goodsReceiveTable;
var reloadGoodsReceiveTable;
var isSelected;

$(function(){
    var recordspartsperpage = [];//new Array();
    var anPartsSelected;
    var jmlRecPartsPerPage=0;
    var id;
    var anOpen = [];

    $('#goodsReceive_datatables_${idTable} td.control').live( 'click', function () {
        var nTr = this.parentNode;
        console.log("nTr=" + nTr );
        var i = $.inArray( nTr, anOpen );

        if ( i === -1 ) {
            $('i', this).attr( 'class', "icon-minus" );
            var vendorId = $(this).closest('tr').find("input:hidden").val();
            var sname = "goodsReceive-row-"  + vendorId;
            console.log("sname=" + sname);
            if ($('#' + sname).is(':checked')) {
                console.log("is checked");
                isSelected = "true";
            } else {
                console.log("is unchecked");
                isSelected = "false";
            }

    		var oData = goodsReceiveTable.fnGetData(nTr);
            $('#spinner').fadeIn(1);
            $.ajax({type:'POST',
                data : oData,
                url:'${request.contextPath}/goodsReceive/sublist?isSelected=' + isSelected,
                success:function(data,textStatus){
                    var nDetailsRow = goodsReceiveTable.fnOpen(nTr,data,'details');
                    $('div.innerDetails', nDetailsRow).slideDown();
                    anOpen.push( nTr );
                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
        }
        else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			goodsReceiveTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
        }
    } );

    reloadGoodsReceiveTable = function() {
		goodsReceiveTable.fnDraw();
	}

	goodsReceiveTable = $('#goodsReceive_datatables_${idTable}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsParts = $("#goodsReceive_datatables_${idTable} tbody .row-select");
            var jmlPartsCek = 0;
            var nRow;
            var idRec;
            rsParts.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordspartsperpage[idRec]=="1"){
                    jmlPartsCek = jmlPartsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordspartsperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecPartsPerPage = rsParts.length;
            if(jmlPartsCek == jmlRecPartsPerPage && jmlRecPartsPerPage > 0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		 "fnInitComplete": function () {
        this.fnAdjustColumnSizing(true);
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        return nRow;
        },
        "bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "sClass": "control center",
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "namaVendor",
	"mDataProp": "namaVendor",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
	    return '<input id="goodsReceive-row-'+row['id']+'" type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"> ' +
		 '<input type="hidden" value="'+row['id']+'"><input type="hidden" value="'+data+'"><input type="hidden" id="checkbox-'+row['id']+'" >&nbsp;&nbsp;' + data;
	},
	"bSortable": false  ,
	"sWidth":"1200px",
	"bVisible": true
},
{
    "sName": "idVendor",
	"mDataProp": "idVendor",
	"aTargets": [1],
	"bSortable": false,
	"sDefaultContent": '',
	"bVisible": false
}
],
	        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	            var tglFrom = $('#search_tanggal').val();

            if(tglFrom){
                aoData.push(
                        {"name": 'sCriteria_tglFrom', "value": tglFrom}
                );
            }

			var search_noReff = $('#search_noReff').val();
			if(search_noReff){
				aoData.push(
						{"name": 'search_noReff', "value": search_noReff}
				);
			}

            var tglTo = $('#search_tanggalAkhir').val();

            if(tglTo){
                aoData.push(
                        {"name": 'sCriteria_tglTo', "value": tglTo}
                );
            }
                var criteriaParts = $('input[name^=criteriaParts]:checked').val();
                console.log("criteriaParts=" + criteriaParts);
                if(criteriaParts){
                    aoData.push(
                            {"name": 'criteriaParts', "value": criteriaParts}
                    );
                }
                $.ajax({ "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData ,
                    "success": function (json) {
                        fnCallback(json);
                       },
                    "complete": function () {
                       }
                });
			}
		});

    $('#goodsReceive_datatables_${idTable} a.edit').live('click', function (e) {
        e.preventDefault();

        var nRow = $(this).parents('tr')[0];

        if ( nEditing !== null && nEditing != nRow ) {
            restoreRow( goodsReceiveTable, nEditing );
            editRow( goodsReceiveTable, nRow );
            nEditing = nRow;
        }
        else if ( nEditing == nRow && this.innerHTML == "Save" ) {
            saveRow( goodsReceiveTable, nEditing );
            nEditing = null;
        }
        else {
            editRow( goodsReceiveTable, nRow );
            nEditing = nRow;
        }
    } );

    $('.select-all').click(function(e) {
        $("#goodsReceive_datatables_${idTable} tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            var nRow = $('.row-select2').parent().parent();//.addClass('row_selected');
            var nRow3 = $('.row-select3').parent().parent();//.addClass('row_selected');
            if(this.checked){
                $('.row-select3').prop('checked', true);
                $('.row-select2').prop('checked', true);
                $('.row-select').prop('checked', true);
                $('.sel-all3').prop('checked', true);
                $('.sel-all2').prop('checked', true);
                nRow.addClass('row_selected');
                nRow3.addClass('row_selected');

                recordspartsperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                $('.row-select3').prop('checked', false);
                $('.row-select2').prop('checked', false);
                $('.row-select').prop('checked', false);
                $('.sel-all3').prop('checked', false);
                $('.sel-all2').prop('checked', false);
                nRow.removeClass('row_selected');
                nRow3.removeClass('row_selected');

                recordspartsperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#goodsReceive_datatables_${idTable} tbody tr').live('click', function () {
	    id = $(this).find('.row-select').next("input:hidden").val();
	    console.log("this=" + $(this));
	    console.log("this.find('.row-select')=" + $(this).find('.row-select'));
        console.log("id=" + id);
        var vendorName = $(this).find('.row-select').next("input:hidden").next("input:hidden").val();
        console.log("vendorName=" + vendorName);
        if (vendorName) {   //untuk tree yg atas saja
            if($(this).find('.row-select').is(":checked"))
            {
                recordspartsperpage[id] = "1";
                $(this).find('.row-select').parent().parent().addClass('row_selected');
                anPartsSelected = goodsReceiveTable.$('tr.row_selected');
                console.log("jmlRecPartsPerPage=" + jmlRecPartsPerPage + " anPartsSelected=" + anPartsSelected);
                if(jmlRecPartsPerPage == anPartsSelected.length){
                    $('.select-all').attr('checked', true);
                }

                $('.sel-all2').each(function() {
                    var vendorNameInner = $(this).next("input:hidden").val();
                    console.log("1 sel-all2 vendorName=" + vendorName + " vendorNameInner=" + vendorNameInner);
                    if (vendorName == vendorNameInner) {
                        $(this).attr('checked', true);

                        var nRow = $(this).parent().parent().parent();//.addClass('row_selected');
                        console.log("select-all2 nRow.html()=" + nRow.html());
                        nRow.addClass('row_selected');
                    }
                });
                $('.sel-all3').each(function() {
                    var noGRInner3 = $(this).next("input:hidden").val();
                    console.log("sel-all3 vendorName=" + vendorName + " noGRInner3=" + noGRInner3);
                    if (vendorName == noGRInner3) {
                        $(this).attr('checked', true);

                        var nRow = $(this).parent().parent().parent();//.addClass('row_selected');
                        console.log("select-all3 nRow.html()=" + nRow.html());
                        nRow.addClass('row_selected');
                    }
                });
                $('table[id^=goodsReceive_datatables_sub_] tbody .row-select2').each(function() {
                    var vendorNameInner = $(this).next("input:hidden").next("input:hidden").val();
                    console.log("1 sel-all2 vendorName=" + vendorName + " vendorNameInner=" + vendorNameInner);
                    if (vendorName == vendorNameInner) {
                        $(this).attr('checked', true);

                        var nRow = $(this).parent().parent();//.addClass('row_selected');
                        console.log("row-select2 nRow.html()=" + nRow.html());
                        nRow.addClass('row_selected');
                    }
                });
                $('table[id^=goodsReceive_datatables_sub_sub_] tbody .row-select3').each(function() {
                    var noGRInner3 = $(this).next("input:hidden").next("input:hidden").val();
                    console.log("row-select3 noGRInner3=" + noGRInner3 + " vendorName=" + vendorName);
                    if (vendorName == noGRInner3) {
                        $(this).attr('checked', true);

                        var nRow = $(this).parent().parent();//.addClass('row_selected');
                        console.log("row-select3 nRow.html()=" + nRow.html());
                        nRow.addClass('row_selected');
                    }
                });
            }
            else
            {
                console.log("row-select is checked=" + $(this).find('.row-select').is(":checked"));
                recordspartsperpage[id] = "0";
                console.log("2. select-all false")
                $('.select-all').attr('checked', false);
                $(this).find('.row-select').parent().parent().removeClass('row_selected');

                $('.sel-all2').each(function() {
                    var vendorNameInner = $(this).next("input:hidden").val();
                    console.log("2 sel-all2 vendorName=" + vendorName + " vendorNameInner=" + vendorNameInner);
                    if (vendorName == vendorNameInner) {
                        $(this).attr('checked', false);

                        var nRow = $(this).parent().parent().parent();//.addClass('row_selected');
                        nRow.removeClass('row_selected');
                    }
                });
                $('.sel-all3').each(function() {
                    var noGRInner3 = $(this).next("input:hidden").val();
                    console.log("sel-all3 vendorName=" + vendorName + " noGRInner3=" + noGRInner3);
                    if (vendorName == noGRInner3) {
                        $(this).attr('checked', false);

                        var nRow = $(this).parent().parent().parent();//.addClass('row_selected');
                        nRow.removeClass('row_selected');
                    }
                });
                $('table[id^=goodsReceive_datatables_sub_] tbody .row-select2').each(function() {
                    var vendorNameInner = $(this).next("input:hidden").next("input:hidden").val();
                    console.log("2 sel-all2 vendorName=" + vendorName + " vendorNameInner=" + vendorNameInner);
                    if (vendorName == vendorNameInner) {
                        $(this).attr('checked', false);

                        var nRow = $(this).parent().parent();//.addClass('row_selected');
                        nRow.removeClass('row_selected');
                    }
                });
                $('table[id^=goodsReceive_datatables_sub_sub_] tbody .row-select3').each(function() {
                    var noGRInner3 = $(this).next("input:hidden").next("input:hidden").val();
                    console.log("row-select3 vendorName=" + vendorName + " noGRInner3=" + noGRInner3);
                    if (vendorName == noGRInner3) {
                        $(this).attr('checked', false);

                        var nRow = $(this).parent().parent();//.addClass('row_selected');
                        nRow.removeClass('row_selected');
                    }
                });
            }
        }
    });

});

</g:javascript>


			
