
<%@ page import="com.kombos.parts.GoodsReceive" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="title" value="Goods Receive" />
		<title>${title}</title>
		<r:require modules="baseapplayout" />
        <style>
            table.fixed { table-layout:fixed; }
            table.fixed td { overflow: hidden; }
            .center {
                text-align:center;
            }
         </style>
		<g:javascript>
	var orderParts;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	var printBinningInstruction;

	$(function(){ 

	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :
                shrinkTableLayout();
         		$('#spinner').fadeIn(1);
                $.ajax({type:'POST', url:'${request.contextPath}/inputGoodsReceive/index/',
                    success:function(data,textStatus){
                        loadForm(data, textStatus);
                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){},
                    complete:function(XMLHttpRequest,textStatus){
                        $('#spinner').fadeOut();
                    }
                });
         		break;
         	case '_DELETE_' :
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
         			function(result){
         				if(result){
         					massDeleteGoodsReceive();
         				}
         			});

         		break;
       }
       return false;
	});

    massDeleteGoodsReceive = function() {
        var recordsToDelete = [];
        var goodsReceiveToDelete = [];
        var goodsReceiveDetailsToDelete = [];
        $('#goodsReceive_datatables_${idTable} tbody .row-select').each(function() {
            if(this.checked){
                var id = $(this).next("input:hidden").val();
                recordsToDelete.push(id);
            }
        });

        $('table[id^=goodsReceive_datatables_sub_] tbody .row-select2').each(function() {
            console.log("this=" + this);

            if(this.checked){
                var id = $(this).next("input:hidden").val();
                var goodsReceiveNumber = $(this).next("input:hidden").next("input:hidden").val();
                console.log("id=" + id + " goodsReceiveNumber=" + goodsReceiveNumber);
                goodsReceiveToDelete.push(id);
            }
        });
        $('table[id^=goodsReceive_datatables_sub_sub_] tbody .row-select3').each(function() {
            console.log("this=" + this);

            if(this.checked){
                var id = $(this).next("input:hidden").val();
                var goodsReceiveDetailsNumber = $(this).next("input:hidden").next("input:hidden").val();
                console.log("id=" + id + " goodsReceiveDetailsNumber=" + goodsReceiveDetailsNumber);
                goodsReceiveDetailsToDelete.push(id);
            }
        });
        console.log("recordsToDelete=" + recordsToDelete);
        console.log("goodsReceiveToDelete=" + goodsReceiveToDelete);
        console.log("goodsReceiveDetailsToDelete=" + goodsReceiveDetailsToDelete);
        var json = JSON.stringify(recordsToDelete);
        var jsonDetail = JSON.stringify(goodsReceiveToDelete);
        var jsonDetailDetail = JSON.stringify(goodsReceiveDetailsToDelete);
        console.log("json=" + json);
        console.log("jsonDetail=" + jsonDetail);
        console.log("jsonDetailDetail=" + jsonDetailDetail);
        if (recordsToDelete.length > 0) {
            $.ajax({
                url:'${request.contextPath}/goodsReceive/massdeleteVendor',
                type: "POST", // Always use POST when deleting data
                data: { ids: json },
                complete: function(xhr, status) {
                    goodsReceiveTable.fnDraw();
                     toastr.success('<div>Goods Receive Telah Dihapus.</div>');

                }
            });
        } else if (goodsReceiveToDelete > 0) {
            $.ajax({
                url:'${request.contextPath}/goodsReceive/massdeleteGoodsReceive',
                type: "POST", // Always use POST when deleting data
                data: { ids: jsonDetail },
                complete: function(xhr, status) {
                    goodsReceiveTable.fnDraw();
                     toastr.success('<div>Goods Receive Telah Dihapus.</div>');

                }
            });
        } else if (goodsReceiveDetailsToDelete.length > 0) {
            $.ajax({
                url:'${request.contextPath}/goodsReceive/massdeleteGoodsReceiveDetail',
                type: "POST", // Always use POST when deleting data
                data: { ids: jsonDetailDetail },
                complete: function(xhr, status) {
                    goodsReceiveTable.fnDraw();
                     toastr.success('<div>Goods Receive Detail Telah Dihapus.</div>');
                }
            });
        }
    }


    orderParts = function(id) {
    	var checkParts = [];
        $("#goodsReceive-table tbody .row-select").each(function() {
        if(this.checked){
            var id = $(this).next("input:hidden").val();
            checkParts.push(id);
        }
        });
        if(checkParts.length<1){
             alert('Anda belum memilih data yang akan ditambahkan');
        }
        var checkPartsMap = JSON.stringify(checkParts);
    	
    	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', 
   			data : { ids: checkPartsMap },
   			url:'${request.contextPath}/cekKetersediaanParts/orderParts',
   			success:function(data,textStatus){
   				reloadGoodsReceiveTable();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };

    onClearSearch = function () {
        $('input[name=criteriaParts]:radio').removeAttr('checked');
         $('#search_tanggal').val("");
         $('#search_tanggalAkhir').val("");
         $('#search_noReff').val("");
        reloadGoodsReceiveTable();
    }

   	onSearch = function(){
       reloadGoodsReceiveTable();
   	}


    edit = function(id) {
        console.log("inside edit id=" + id);
        shrinkTableLayout();
        window.location.replace('#/inputGoodsReceive/index/' + id);
    	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/inputGoodsReceive/index/'+id,
   			success:function(data,textStatus){
   			    loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };

    edit = function(id){
        console.log("didalam edit");
        shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/inputGoodsReceive/index?1=1',
   		    data : { id: id},
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
        $(".row-select").each(function() {
            this.checked = false
        });
    }

    shrinkTableLayout = function(){
        $("#goodsReceive-table").hide();
        $("#goodsReceive-form").css("display","block");
   	}

    loadForm = function(data, textStatus){
		$('#goodsReceive-form').empty();
    	$('#goodsReceive-form').append(data);
   	}

   	expandTableLayout = function(){
   	    var oTable = $('#binning_datatables_${idTable}').dataTable();
        oTable.fnReloadAjax();
   		$("#goodsReceive-table").show();
   		$("#goodsReceive-form").css("display","none");
   	}
          
    printBinningInstruction = function(){
        //window.location = "${request.contextPath}/goodsReceive/printBinningInstruction";

        var recordsToPrint = [];
        var goodsReceiveToPrint = [];
        var goodsReceiveDetailsToPrint = [];
        $('#goodsReceive_datatables_${idTable} tbody .row-select').each(function() {
            if(this.checked){
                var id = $(this).next("input:hidden").val();
                recordsToPrint.push(id);
            }
        });
        $('table[id^=goodsReceive_datatables_sub_] tbody .row-select2').each(function() {
            if(this.checked){
                var id = $(this).next("input:hidden").val();
                var goodsReceiveNumber = $(this).next("input:hidden").next("input:hidden").val();
                goodsReceiveToPrint.push(id);
            }
        });
        $('table[id^=goodsReceive_datatables_sub_sub_] tbody .row-select3').each(function() {
            if(this.checked){
                var id = $(this).next("input:hidden").val();
                var goodsReceiveDetailsNumber = $(this).next("input:hidden").next("input:hidden").val();
                goodsReceiveDetailsToPrint.push(id);
            }
        });
        console.log("recordsToPrint=" + recordsToPrint);
        console.log("goodsReceiveToPrint=" + goodsReceiveToPrint);
        console.log("goodsReceiveDetailsToPrint=" + goodsReceiveDetailsToPrint);
        var json = JSON.stringify();
        var jsonDetail = JSON.stringify(goodsReceiveToPrint);
        var jsonDetailDetail = JSON.stringify(goodsReceiveDetailsToPrint);

        console.log("jsonDetail=" + jsonDetail);
        console.log("jsonDetailDetail=" + jsonDetailDetail);
        if (recordsToPrint.length > 0) {
            var idVendors =  JSON.stringify(recordsToPrint);
            console.log("idVendors=" + idVendors);
            window.location = "${request.contextPath}/goodsReceive/printBinningInstruction?idVendors="+idVendors;
        } else if (goodsReceiveToPrint.length > 0) {
            var idGoodsReceives =  JSON.stringify(goodsReceiveToPrint);
            console.log("idGoodsReceives=" + idGoodsReceives);
            window.location = "${request.contextPath}/goodsReceive/printBinningInstruction?idGoodsReceives="+idGoodsReceives;
        } else if (goodsReceiveDetailsToPrint.length > 0) {
            var idGoodsReceiveDetails =  JSON.stringify(goodsReceiveDetailsToPrint);
            console.log("idGoodsReceiveDetails=" + idGoodsReceiveDetails);
            window.location = "${request.contextPath}/goodsReceive/printBinningInstruction?idGoodsReceiveDetails="+idGoodsReceiveDetails;
        }
    }
        var checkin = $('#search_tanggal').datepicker({
            onRender: function(date) {
                return '';
            }
        }).on('changeDate', function(ev) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate());
            checkout.setValue(newDate);
            checkin.hide();
            $('#search_tanggalAkhir')[0].focus();
        }).data('datepicker');

        var checkout = $('#search_tanggalAkhir').datepicker({
            onRender: function(date) {
                return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');
   	});
</g:javascript>

	</head>
	<body>
    <div class="span12" id="goodsReceive-table">
    <div class="box" style="margin-left: -40px; margin-right: 40px;">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <legend style="font-size: small">Search Criteria</legend>
        <fieldset>
        <table style="padding-right: 10px; display:table; border-collapse:separate; border-spacing:5px;" cellpadding="15" cellspacing="15">
            <tr style="display:table-row;">
                <td style="width: 130px; display:table-cell; padding:5px;">
                    <label class="control-label" for="lbl_rcvDate">
                        No Reff
                    </label>
                </td>
                <td>
                    <div id="lbl_noreff" class="controls">
                        <g:textField name="search_noReff" id="search_noReff" />
                    </div>
                </td>
            </tr>
            <tr style="display:table-row;">
                <td style="width: 130px; display:table-cell; padding:5px;">
                    <label class="control-label" for="lbl_rcvDate">
                        Tanggal Receive
                    </label>
                </td>

                <td>
                    <div id="lbl_rcvDate" class="controls">
                        <ba:datePicker id="search_tanggal" name="search_tanggal" precision="day" format="dd/MM/yyyy"  value="${new Date()}" /> S.d
                        <ba:datePicker id="search_tanggalAkhir" name="search_tanggalAkhir" precision="day" format="dd/MM/yyyy" style="display: none" value="${new Date()}" />
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width: 130px; display:table-cell; padding:5px;">
                    <label class="control-label" for="lbl_criteriaParts">
                        Kriteria Parts
                    </label>
                </td>
                <td colspan="2">
                    <div id="lbl_criteriaParts" class="controls">
                        <g:radioGroup name="criteriaParts" values="['0','1']" labels="['Parts Belum Binning','Parts Sudah Binning']">
                            ${it.radio} <g:message code="${it.label}" />
                        </g:radioGroup>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="width: 130px; display:table-cell; padding:5px;">
                    <button class="btn btn-primary" id="buttonSearch" onclick="onSearch();">Search</button>
                    <button class="btn btn-cancel" onclick="onClearSearch();">Clear Search</button>
                </td>
            </tr>
        </table>
        </fieldset>
    </div>

    <div class="navbar box-header no-border"  style="margin-left: -40px; margin-right: 40px;" >
        <span class="pull-left">Parts Receive</span>
        <ul class="nav pull-right">
          <li><a class="pull-right box-action" href="#"
              style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
                  class="icon-plus"></i>&nbsp;&nbsp;
          </a></li>
          <li><a class="pull-right box-action" href="#"
              style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
                  class="icon-remove"></i>&nbsp;&nbsp;
          </a></li>
        </ul>
    </div>

    <div class="box" style="margin-left: -40px; margin-right: 40px;">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <g:render template="dataTables" />
        <div class="modal-footer">
            <button class="btn btn-primary" data-dismiss="modal" onclick="printBinningInstruction();">Print Binning Instruction</button>
        </div>
	</div>
    </div>

    <div class="span12" id="goodsReceive-form" style="display: none;margin-left: 0;"></div>
</body>
</html>
