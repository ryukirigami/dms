<%--
  Created by IntelliJ IDEA.
  User: DENDYSWARAN
  Date: 5/8/14
  Time: 11:50 PM
--%>

<%@ page import="com.kombos.maintable.Company; com.kombos.hrd.Karyawan; com.kombos.customerprofile.HistoryCustomer; com.kombos.administrasi.VendorAsuransi; com.kombos.parts.Vendor; com.kombos.administrasi.CompanyDealer; com.kombos.parts.Konversi" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title>DATA</title>
    <style>
    #detail-table thead th, #detail-table tbody tr td  {font-size: 10px; white-space: nowrap;vertical-align: middle;}
    #detail-table tbody tr td {text-align: center;}
    #detail-table tbody td input[type="text"] {font-size: 10px; width: 100%; margin-top: 10px;}
    #detail-form {margin-left: 0px;} #btnSearchAccount {position: relative; left: 9px; top: 2px;}
    </style>
    <r:require modules="baseapplayout, baseapplist"/>
    <g:javascript>
        $(function(){
            $("input[type=radio][value='1']").attr('checked', 'checked');

            saveApproval = function(){
                var arr = new Array()
                var staAprove = ""
                var dataid = ""
                var conf = confirm("Apakah Anda Yakin?")
                if(conf){
                    $("#detail-table tbody .row-select").each(function() {
                        if(this.checked){
                            var sta = this.value
                            var dataid = $(this).next("input:hidden").val()
                            var map = {
                                staApprove : sta,
                                dataid : dataid
                            }
                            arr.push(map)
                        }
                    });
                    var params = {
                        arr : JSON.stringify(arr)
                    }
                    $.post("${request.getContextPath()}/Revisi/saveApproval", params, function(data) {
                    if (!data.error){
                        toastr.success("Save Approval Success...");
                        reloadEFakturTable();
                    }
                    else
                        alert("Internal Server Error");
                    });
                    console.log(arr);
                }
            }
        });
    </g:javascript>
</head>

<body>
<div class="box">
    <div class="span12" id="showDetail-table">
        <div class="row-fluid" style="overflow: auto">
            <table id="detail-table" class="display table table-striped table-bordered table-hover table-selectable fixed">
                <thead>
                <th>No.</th>
                <th>Approve</th>
                <th>Rejected</th>
                <th>FK</th>
                <th>KD_JENIS_TRANSAKSI</th>
                <th>FG_PENGGANTI</th>
                <th>NOMOR_FAKTUR</th>
                <th>MASA_PAJAK</th>
                <th>TAHUN_PAJAK</th>
                <th>TANGGAL_FAKTUR</th>
                <th>NPWP</th>
                <th>NAMA</th>
                <th>ALAMAT_LENGKAP</th>
                <th>JUMLAH_DPP</th>
                <th>JUMLAH_PPNBM</th>
                <th>ID_KETERANGAN_TAMBAHAN</th>
                <th>FG_UANG_MUKA</th>
                <th>UANG_MUKA_DPP</th>
                <th>UANG_MUKA_PPN</th>
                <th>UANG_MUKA_PPNBM</th>
                <th>REFERENSI</th>
                <th>LT</th>
                <th>NPWP2</th>
                <th>NAMA2</th>
                <th>JALAN</th>
                <th>BLOK</th>
                <th>NOMOR</th>
                <th>RT</th>
                <th>RW</th>
                <th>KECAMATAN</th>
                <th>KELURAHAN</th>
                <th>KABUPATEN</th>
                <th>PROPINSI</th>
                <th>KODE POS</th>
                <th>NOMOR_TELEPON</th>
                <th>OF</th>
                <th>KODE_OBJEK</th>
                <th>NAMA_OBJEK</th>
                <th>HARGA_SATUAN</th>
                <th>JUMLAH_BARANG</th>
                <th>HARGA_TOTAL</th>
                <th>DISKON</th>
                <th>DPP</th>
                <th>PPN</th>
                <th>TARIF_PPNB</th>
                <th>PPNB</th>
                </thead>
                <tbody>
                <g:if test="${dataFakturDetail}">
                    <g:each in="${dataFakturDetail}" status="i" var="j" >
                        <tr>
                            <td>${i + 1}</td>
                            <td><input type="radio" class="row-select" name="radio_${j.id}" value="1"><input type="hidden" name="data_${j.id}" value="${j.id}"></td>
                            <td><input type="radio" class="row-select" name="radio_${j.id}" value="2"><input type="hidden" name="data_${j.id}" value="${j.id}"></td>
                            <td>${j.getFK()}</td>
                            <td>${j.getKD_JENIS_TRANSAKSI()}</td>
                            <td>${j.getFG_PENGGANTI()}</td>
                            <td>${j.getNOMOR_FAKTUR()}</td>
                            <td>${j.getMASA_PAJAK()}</td>
                            <td>${j.getTAHUN_PAJAK()}</td>
                            <td>${j.getTANGGAL_FAKTUR()}</td>
                            <td>${j.getNPWP()}</td>
                            <td>${j.getNAMA_OBJEK()}</td>
                            <td>${j.getALAMAT_LENGKAP()}</td>
                            <td>${j.getJUMLAH_DPP()}</td>
                            <td>${j.getJUMLAH_PPNBM()}</td>
                            <td>${j.getID_KETERANGAN_TAMBAHAN()}</td>
                            <td>${j.getFG_UANG_MUKA()}</td>
                            <td>${j.getUANG_MUKA_DPP()}</td>
                            <td>${j.getUANG_MUKA_PPN()}</td>
                            <td>${j.getUANG_MUKA_PPNBM()}</td>
                            <td>${j.getREFERENSI()}</td>
                            <td>${j.getLT()}</td>
                            <td>${j.getNPWP2()}</td>
                            <td>${j.getNAMA2()}</td>
                            <td>${j.getJALAN()}</td>
                            <td>${j.getBLOK()}</td>
                            <td>${j.getNOMOR()}</td>
                            <td>${j.getRT()}</td>
                            <td>${j.getRW()}</td>
                            <td>${j.getKECAMATAN()}</td>
                            <td>${j.getKELURAHAN()}</td>
                            <td>${j.getKABUPATEN()}</td>
                            <td>${j.getPROPINSI()}</td>
                            <td>${j.getKODE_POS()}</td>
                            <td>${j.getNOMOR_TELEPON()}</td>
                            <td>${j.getOF_()}</td>
                            <td>${j.getKODE_OBJEK()}</td>
                            <td>${j.getNAMA()}</td>
                            <td>${j.getHARGA_SATUAN()}</td>
                            <td>${j.getJUMLAH_BARANG()}</td>
                            <td>${j.getHARGA_TOTAL()}</td>
                            <td>${j.getDISKON()}</td>
                            <td>${j.getDPP()}</td>
                            <td>${j.getPPN()}</td>
                            <td>${j.getTARIF_PPNBM()}</td>
                            <td>${j.getPPNBM()}</td>
                        </tr>
                    </g:each>
                </g:if>
                </tbody>
            </table>
        </div>
        <button class="btn btn-primary save" data-dismiss="modal" name="SAVE" id="saveTrxDetail" onclick="saveApproval()" style="margin-top: 13px;">Save</button>
        <button class="btn cancel" data-dismiss="modal" name="close" id="closeTrxDetail" style="margin-top: 13px;">Tutup</button>
    </div>
</div>

</body>
</html>