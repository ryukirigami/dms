<%@ page import="com.kombos.maintable.EFaktur" %>

%{--Baris 1--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'FK', 'error')} ">
    <label class="control-label" for="FK">
        <g:message code="EFaktur.FK.label" default="FK"/>

    </label>

    <div class="controls">
        <g:textField name="FK" value="${EFakturInstance?.FK}"/>
    </div>
</div>

%{--Baris 2--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'KD_JENIS_TRANSAKSI', 'error')} ">
    <label class="control-label" for="KD_JENIS_TRANSAKSI">
        <g:message code="EFaktur.KD_JENIS_TRANSAKSI.label" default="KDJENISTRANSAKSI"/>

    </label>

    <div class="controls">
        <g:textField name="KD_JENIS_TRANSAKSI" value="${EFakturInstance?.KD_JENIS_TRANSAKSI}"/>
    </div>
</div>

%{--Baris 3--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'FG_PENGGANTI', 'error')} ">
    <label class="control-label" for="FG_PENGGANTI">
        <g:message code="EFaktur.FG_PENGGANTI.label" default="FGPENGGANTI"/>

    </label>

    <div class="controls">
        <g:textField name="FG_PENGGANTI" value="${EFakturInstance?.FG_PENGGANTI}"/>
    </div>
</div>

%{--Baris 4--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'NOMOR_FAKTUR', 'error')} ">
    <label class="control-label" for="NOMOR_FAKTUR">
        <g:message code="EFaktur.NOMOR_FAKTUR.label" default="NOMORFAKTUR"/>

    </label>

    <div class="controls">
        <g:textField name="NOMOR_FAKTUR" value="${EFakturInstance?.NOMOR_FAKTUR}"/>
    </div>
</div>

%{--Baris 5--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'MASA_PAJAK', 'error')} ">
    <label class="control-label" for="MASA_PAJAK">
        <g:message code="EFaktur.MASA_PAJAK.label" default="MASAPAJAK"/>

    </label>

    <div class="controls">
        <g:textField name="MASA_PAJAK" value="${EFakturInstance?.MASA_PAJAK}"/>
    </div>
</div>

%{--Baris 6--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'TAHUN_PAJAK', 'error')} ">
    <label class="control-label" for="TAHUN_PAJAK">
        <g:message code="EFaktur.TAHUN_PAJAK.label" default="TAHUNPAJAK"/>

    </label>

    <div class="controls">
        <g:textField name="TAHUN_PAJAK" value="${EFakturInstance?.TAHUN_PAJAK}"/>
    </div>
</div>

%{--Baris 7--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'TANGGAL_FAKTUR', 'error')} ">
    <label class="control-label" for="TANGGAL_FAKTUR">
        <g:message code="EFaktur.TANGGAL_FAKTUR.label" default="TANGGALFAKTUR"/>

    </label>

    <div class="controls">
        <g:textField name="TANGGAL_FAKTUR" value="${EFakturInstance?.TANGGAL_FAKTUR}"/>
    </div>
</div>

%{--Baris 8--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'NPWP', 'error')} ">
    <label class="control-label" for="NPWP">
        <g:message code="EFaktur.NPWP.label" default="NPWP"/>

    </label>

    <div class="controls">
        <g:textField name="NPWP" value="${EFakturInstance?.NPWP}"/>
    </div>
</div>

%{--Baris 9--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'NAMA_OBJEK', 'error')} ">
    <label class="control-label" for="NAMAOBJEK">
        <g:message code="EFaktur.NAMA_OBJEK.label" default="NAMA"/>

    </label>

    <div class="controls">
        <g:textField name="NAMAOBJEK" value="${EFakturInstance?.NAMA_OBJEK}"/>
    </div>
</div>

%{--Baris 10--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'ALAMAT_LENGKAP', 'error')} ">
    <label class="control-label" for="ALAMAT_LENGKAP">
        <g:message code="EFaktur.ALAMAT_LENGKAP.label" default="ALAMATLENGKAP"/>

    </label>

    <div class="controls">
        <g:textField name="ALAMAT_LENGKAP" value="${EFakturInstance?.ALAMAT_LENGKAP}"/>
    </div>
</div>

%{--Baris 11--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'JUMLAH_DPP', 'error')} ">
    <label class="control-label" for="JUMLAH_DPP">
        <g:message code="EFaktur.JUMLAH_DPP.label" default="JUMLAHDPP"/>

    </label>

    <div class="controls">
        <g:textField name="JUMLAH_DPP" value="${EFakturInstance?.JUMLAH_DPP}"/>
    </div>
</div>

%{--Baris 12--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'JUMLAH_PPN', 'error')} ">
    <label class="control-label" for="JUMLAH_PPN">
        <g:message code="EFaktur.JUMLAH_PPN.label" default="JUMLAHPPN"/>

    </label>

    <div class="controls">
        <g:textField name="JUMLAH_PPN" value="${EFakturInstance?.JUMLAH_PPN}"/>
    </div>
</div>

%{--Baris 13--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'JUMLAH_PPNBM', 'error')} ">
    <label class="control-label" for="JUMLAH_PPNBM">
        <g:message code="EFaktur.JUMLAH_PPNBM.label" default="JUMLAHPPNBM"/>

    </label>

    <div class="controls">
        <g:textField name="JUMLAH_PPNBM" value="${EFakturInstance?.JUMLAH_PPNBM}"/>
    </div>
</div>

%{--Baris 14--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'ID_KETERANGAN_TAMBAHAN', 'error')} ">
    <label class="control-label" for="ID_KETERANGAN_TAMBAHAN">
        <g:message code="EFaktur.ID_KETERANGAN_TAMBAHAN.label" default="IDKETERANGANTAMBAHAN"/>

    </label>

    <div class="controls">
        <g:textField name="ID_KETERANGAN_TAMBAHAN" value="${EFakturInstance?.ID_KETERANGAN_TAMBAHAN}"/>
    </div>
</div>

%{--Baris 15--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'FG_UANG_MUKA', 'error')} ">
    <label class="control-label" for="FG_UANG_MUKA">
        <g:message code="EFaktur.FG_UANG_MUKA.label" default="FGUANGMUKA"/>

    </label>

    <div class="controls">
        <g:textField name="FG_UANG_MUKA" value="${EFakturInstance?.FG_UANG_MUKA}"/>
    </div>
</div>

%{--Baris 16--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'UANG_MUKA_DPP', 'error')} ">
    <label class="control-label" for="UANG_MUKA_DPP">
        <g:message code="EFaktur.UANG_MUKA_DPP.label" default="UANGMUKADPP"/>

    </label>

    <div class="controls">
        <g:textField name="UANG_MUKA_DPP" value="${EFakturInstance?.UANG_MUKA_DPP}"/>
    </div>
</div>

%{--Baris 17--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'UANG_MUKA_PPN', 'error')} ">
    <label class="control-label" for="UANG_MUKA_PPN">
        <g:message code="EFaktur.UANG_MUKA_PPN.label" default="UANGMUKAPPN"/>

    </label>

    <div class="controls">
        <g:textField name="UANG_MUKA_PPN" value="${EFakturInstance?.UANG_MUKA_PPN}"/>
    </div>
</div>

%{--Baris 18--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'UANG_MUKA_PPNBM', 'error')} ">
    <label class="control-label" for="UANG_MUKA_PPNBM">
        <g:message code="EFaktur.UANG_MUKA_PPNBM.label" default="UANGMUKAPPNBM"/>

    </label>

    <div class="controls">
        <g:textField name="UANG_MUKA_PPNBM" value="${EFakturInstance?.UANG_MUKA_PPNBM}"/>
    </div>
</div>

%{--Baris 19--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'REFERENSI', 'error')} ">
    <label class="control-label" for="REFERENSI">
        <g:message code="EFaktur.REFERENSI.label" default="REFERENSI"/>

    </label>

    <div class="controls">
        <g:textField name="REFERENSI" value="${EFakturInstance?.REFERENSI}"/>
    </div>
</div>

%{--Baris 20--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'LT', 'error')} ">
    <label class="control-label" for="LT">
        <g:message code="EFaktur.LT.label" default="LT"/>

    </label>

    <div class="controls">
        <g:textField name="LT" value="${EFakturInstance?.LT}"/>
    </div>
</div>

%{--Baris 21--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'NPWP2', 'error')} ">
    <label class="control-label" for="NPWP2">
        <g:message code="EFaktur.NPWP2.label" default="NPWP 2"/>

    </label>

    <div class="controls">
        <g:textField name="NPWP2" value="${EFakturInstance?.NPWP2}"/>
    </div>
</div>

%{--Baris 22--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'NAMA2', 'error')} ">
    <label class="control-label" for="NAMA2">
        <g:message code="EFaktur.NAMA.label" default="NAMA 2"/>

    </label>

    <div class="controls">
        <g:textField name="NAMA2" value="${EFakturInstance?.NAMA2}"/>
    </div>
</div>

%{--Baris 23--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'JALAN', 'error')} ">
    <label class="control-label" for="JALAN">
        <g:message code="EFaktur.JALAN.label" default="JALAN"/>

    </label>

    <div class="controls">
        <g:textField name="JALAN" value="${EFakturInstance?.JALAN}"/>
    </div>
</div>

%{--Baris 24--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'BLOK', 'error')} ">
    <label class="control-label" for="BLOK">
        <g:message code="EFaktur.BLOK.label" default="BLOK"/>

    </label>

    <div class="controls">
        <g:textField name="BLOK" value="${EFakturInstance?.BLOK}"/>
    </div>
</div>

%{--Baris 25--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'NOMOR', 'error')} ">
    <label class="control-label" for="NOMOR">
        <g:message code="EFaktur.NOMOR.label" default="NOMOR"/>

    </label>

    <div class="controls">
        <g:textField name="NOMOR" value="${EFakturInstance?.NOMOR}"/>
    </div>
</div>

%{--Baris 26--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'RT', 'error')} ">
    <label class="control-label" for="RT">
        <g:message code="EFaktur.RT.label" default="RT"/>

    </label>

    <div class="controls">
        <g:textField name="RT" value="${EFakturInstance?.RT}"/>
    </div>
</div>

%{--Baris 27--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'RW', 'error')} ">
    <label class="control-label" for="RW">
        <g:message code="EFaktur.RW.label" default="RW"/>

    </label>

    <div class="controls">
        <g:textField name="RW" value="${EFakturInstance?.RW}"/>
    </div>
</div>

%{--Baris 28--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'KECAMATAN', 'error')} ">
    <label class="control-label" for="KECAMATAN">
        <g:message code="EFaktur.KECAMATAN.label" default="KECAMATAN"/>

    </label>

    <div class="controls">
        <g:textField name="KECAMATAN" value="${EFakturInstance?.KECAMATAN}"/>
    </div>
</div>

%{--Baris 29--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'KELURAHAN', 'error')} ">
    <label class="control-label" for="KELURAHAN">
        <g:message code="EFaktur.KELURAHAN.label" default="KELURAHAN"/>

    </label>

    <div class="controls">
        <g:textField name="KELURAHAN" value="${EFakturInstance?.KELURAHAN}"/>
    </div>
</div>

%{--Baris 30--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'KABUPATEN', 'error')} ">
    <label class="control-label" for="KABUPATEN">
        <g:message code="EFaktur.KABUPATEN.label" default="KABUPATEN"/>

    </label>

    <div class="controls">
        <g:textField name="KABUPATEN" value="${EFakturInstance?.KABUPATEN}"/>
    </div>
</div>

%{--Baris 31--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'PROPINSI', 'error')} ">
    <label class="control-label" for="PROPINSI">
        <g:message code="EFaktur.PROPINSI.label" default="PROPINSI"/>

    </label>

    <div class="controls">
        <g:textField name="PROPINSI" value="${EFakturInstance?.PROPINSI}"/>
    </div>
</div>

%{--Baris 32--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'KODE_POS', 'error')} ">
    <label class="control-label" for="KODE_POS">
        <g:message code="EFaktur.KODE_POS.label" default="KODEPOS"/>

    </label>

    <div class="controls">
        <g:textField name="KODE_POS" value="${EFakturInstance?.KODE_POS}"/>
    </div>
</div>

%{--Baris 32--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'NOMOR_TELEPON', 'error')} ">
    <label class="control-label" for="NOMOR_TELEPON">
        <g:message code="EFaktur.NOMOR_TELEPON.label" default="NOMORTELEPON"/>

    </label>

    <div class="controls">
        <g:textField name="NOMOR_TELEPON" value="${EFakturInstance?.NOMOR_TELEPON}"/>
    </div>
</div>

%{--Baris 33--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'OF_', 'error')} ">
    <label class="control-label" for="OF_">
        <g:message code="EFaktur.OF_.label" default="OF"/>

    </label>

    <div class="controls">
        <g:textField name="OF_" value="${EFakturInstance?.OF_}"/>
    </div>
</div>

%{--Baris 34--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'KODE_OBJEK', 'error')} ">
    <label class="control-label" for="KODE_OBJEK">
        <g:message code="EFaktur.KODE_OBJEK.label" default="KODEOBJEK"/>

    </label>

    <div class="controls">
        <g:textField name="KODE_OBJEK" value="${EFakturInstance?.KODE_OBJEK}"/>
    </div>
</div>

%{--Baris 35--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'NAMA', 'error')} ">
    <label class="control-label" for="NAMA">
        <g:message code="EFaktur.NAMA2.label" default="NAMAOBJEK"/>

    </label>

    <div class="controls">
        <g:textField name="NAMA" value="${EFakturInstance?.NAMA}"/>
    </div>
</div>

%{--Baris 36--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'HARGA_SATUAN', 'error')} ">
    <label class="control-label" for="HARGA_SATUAN">
        <g:message code="EFaktur.HARGA_SATUAN.label" default="HARGASATUAN"/>

    </label>

    <div class="controls">
        <g:textField name="HARGA_SATUAN" value="${EFakturInstance?.HARGA_SATUAN}"/>
    </div>
</div>

%{--Baris 37--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'JUMLAH_BARANG', 'error')} ">
    <label class="control-label" for="JUMLAH_BARANG">
        <g:message code="EFaktur.JUMLAH_BARANG.label" default="JUMLAHBARANG"/>

    </label>

    <div class="controls">
        <g:textField name="JUMLAH_BARANG" value="${EFakturInstance?.JUMLAH_BARANG}"/>
    </div>
</div>

%{--Baris 38--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'HARGA_TOTAL', 'error')} ">
    <label class="control-label" for="HARGA_TOTAL">
        <g:message code="EFaktur.HARGA_TOTAL.label" default="HARGATOTAL"/>

    </label>

    <div class="controls">
        <g:textField name="HARGA_TOTAL" value="${EFakturInstance?.HARGA_TOTAL}"/>
    </div>
</div>

%{--Baris 39--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'DISKON', 'error')} ">
    <label class="control-label" for="DISKON">
        <g:message code="EFaktur.DISKON.label" default="DISKON"/>

    </label>

    <div class="controls">
        <g:textField name="DISKON" value="${EFakturInstance?.DISKON}"/>
    </div>
</div>

%{--Baris 40--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'DPP', 'error')} ">
    <label class="control-label" for="DPP">
        <g:message code="EFaktur.DPP.label" default="DPP"/>

    </label>

    <div class="controls">
        <g:textField name="DPP" value="${EFakturInstance?.DPP}"/>
    </div>
</div>

%{--Baris 41--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'PPN', 'error')} ">
    <label class="control-label" for="PPN">
        <g:message code="EFaktur.PPN.label" default="PPN"/>

    </label>

    <div class="controls">
        <g:textField name="PPN" value="${EFakturInstance?.PPN}"/>
    </div>
</div>

%{--Baris 42--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'TARIF_PPNBM', 'error')} ">
    <label class="control-label" for="TARIF_PPNBM">
        <g:message code="EFaktur.TARIF_PPNBM.label" default="TARIFPPNBM"/>

    </label>

    <div class="controls">
        <g:textField name="TARIF_PPNBM" value="${EFakturInstance?.TARIF_PPNBM}"/>
    </div>
</div>

%{--Baris 43--}%
<div class="control-group fieldcontain ${hasErrors(bean: EFakturInstance, field: 'PPNBM', 'error')} ">
    <label class="control-label" for="PPNBM">
        <g:message code="EFaktur.PPNBM.label" default="PPNBM"/>

    </label>

    <div class="controls">
        <g:textField name="PPNBM" value="${EFakturInstance?.PPNBM}"/>
    </div>
</div>
%{--Baris Akhir--}%

