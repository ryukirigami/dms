<%@ page import="com.kombos.maintable.EFaktur" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="bootstrapeditable"/>
    <g:set var="entityName"
           value="${message(code: 'EFaktur.label', default: 'EFaktur - REVISI PPN MASUKAN DETIL')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <g:javascript disposition="head">
var deleteEFaktur;

$(function(){ 
	deleteEFaktur=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/Revisi/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadEFakturTable();
   				expandTableLayout('EFaktur');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

    </g:javascript>
</head>

<body>
<div id="show-EFaktur" role="main">
<legend>
    <g:message code="default.show.label" args="[entityName]"/>
</legend>
<g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
</g:if>
<table id="EFaktur"
       class="table table-bordered table-hover">
<tbody>

<g:if test="${EFakturInstance?.docNumber}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="docNumber-label" class="property-label"><g:message
                    code="EFaktur.docNumber.label" default="Doc Number"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="docNumber-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="docNumber"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter docNumber" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="docNumber"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.companyDealer}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="companyDealer-label" class="property-label"><g:message
                    code="EFaktur.companyDealer.label" default="Company Dealer"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="companyDealer-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="companyDealer"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter companyDealer" onsuccess="reloadEFakturTable();" />--}%

            %{--<g:link controller="companyDealer" action="show"--}%
            %{--id="${EFakturInstance?.companyDealer?.id}">${EFakturInstance?.companyDealer?.encodeAsHTML()}</g:link>--}%
            <g:fieldValue bean="${EFakturInstance}" field="companyDealer"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.tanggal}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="tanggal-label" class="property-label"><g:message
                    code="EFaktur.tanggal.label" default="TanggalUpload"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="tanggal-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="tanggal"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter tanggal" onsuccess="reloadEFakturTable();" />--}%

            <g:formatDate date="${EFakturInstance?.tanggal}"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.FK}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="FK-label" class="property-label"><g:message
                    code="EFaktur.FK.label" default="FK"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="FK-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="FK"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter FK" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="FK"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.KD_JENIS_TRANSAKSI}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="KD_JENIS_TRANSAKSI-label" class="property-label"><g:message
                    code="EFaktur.KD_JENIS_TRANSAKSI.label" default="KDJENISTRANSAKSI"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="KD_JENIS_TRANSAKSI-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="KD_JENIS_TRANSAKSI"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter KD_JENIS_TRANSAKSI" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="KD_JENIS_TRANSAKSI"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.FG_PENGGANTI}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="FG_PENGGANTI-label" class="property-label"><g:message
                    code="EFaktur.FG_PENGGANTI.label" default="FGPENGGANTI"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="FG_PENGGANTI-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="FG_PENGGANTI"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter FG_PENGGANTI" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="FG_PENGGANTI"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.NOMOR_FAKTUR}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="NOMOR_FAKTUR-label" class="property-label"><g:message
                    code="EFaktur.NOMOR_FAKTUR.label" default="NOMORFAKTUR"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="NOMOR_FAKTUR-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="NOMOR_FAKTUR"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter NOMOR_FAKTUR" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="NOMOR_FAKTUR"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.MASA_PAJAK}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="MASA_PAJAK-label" class="property-label"><g:message
                    code="EFaktur.MASA_PAJAK.label" default="MASAPAJAK"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="MASA_PAJAK-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="MASA_PAJAK"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter MASA_PAJAK" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="MASA_PAJAK"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.TAHUN_PAJAK}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="TAHUN_PAJAK-label" class="property-label"><g:message
                    code="EFaktur.TAHUN_PAJAK.label" default="TAHUNPAJAK"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="TAHUN_PAJAK-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="TAHUN_PAJAK"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter TAHUN_PAJAK" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="TAHUN_PAJAK"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.TANGGAL_FAKTUR}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="TANGGAL_FAKTUR-label" class="property-label"><g:message
                    code="EFaktur.TANGGAL_FAKTUR.label" default="TANGGALFAKTUR"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="TANGGAL_FAKTUR-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="TANGGAL_FAKTUR"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter TANGGAL_FAKTUR" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="TANGGAL_FAKTUR"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.NPWP}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="NPWP-label" class="property-label"><g:message
                    code="EFaktur.NPWP.label" default="NPWP"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="NPWP-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="NPWP"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter NPWP" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="NPWP"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.NAMA_OBJEK}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="NAMA-label" class="property-label"><g:message
                    code="EFaktur.NAMA.label" default="NAMA"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="NAMA-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="NAMA"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter NAMA" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="NAMA_OBJEK"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.ALAMAT_LENGKAP}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="ALAMAT_LENGKAP-label" class="property-label"><g:message
                    code="EFaktur.ALAMAT_LENGKAP.label" default="ALAMATLENGKAP"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="ALAMAT_LENGKAP-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="ALAMAT_LENGKAP"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter ALAMAT_LENGKAP" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="ALAMAT_LENGKAP"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.JUMLAH_DPP}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="JUMLAH_DPP-label" class="property-label"><g:message
                    code="EFaktur.JUMLAH_DPP.label" default="JUMLAHDPP"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="JUMLAH_DPP-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="JUMLAH_DPP"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter JUMLAH_DPP" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="JUMLAH_DPP"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.JUMLAH_PPN}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="JUMLAH_PPN-label" class="property-label"><g:message
                    code="EFaktur.JUMLAH_PPN.label" default="JUMLAHPPN"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="JUMLAH_PPN-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="JUMLAH_PPN"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter JUMLAH_PPN" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="JUMLAH_PPN"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.JUMLAH_PPNBM}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="JUMLAH_PPNBM-label" class="property-label"><g:message
                    code="EFaktur.JUMLAH_PPNBM.label" default="JUMLAHPPNBM"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="JUMLAH_PPNBM-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="JUMLAH_PPNBM"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter JUMLAH_PPNBM" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="JUMLAH_PPNBM"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.ID_KETERANGAN_TAMBAHAN}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="ID_KETERANGAN_TAMBAHAN-label" class="property-label"><g:message
                    code="EFaktur.ID_KETERANGAN_TAMBAHAN.label" default="IDKETERANGANTAMBAHAN"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="ID_KETERANGAN_TAMBAHAN-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="ID_KETERANGAN_TAMBAHAN"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter ID_KETERANGAN_TAMBAHAN" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="ID_KETERANGAN_TAMBAHAN"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.FG_UANG_MUKA}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="FG_UANG_MUKA-label" class="property-label"><g:message
                    code="EFaktur.FG_UANG_MUKA.label" default="FGUANGMUKA"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="FG_UANG_MUKA-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="FG_UANG_MUKA"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter FG_UANG_MUKA" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="FG_UANG_MUKA"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.UANG_MUKA_DPP}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="UANG_MUKA_DPP-label" class="property-label"><g:message
                    code="EFaktur.UANG_MUKA_DPP.label" default="UANGMUKADPP"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="UANG_MUKA_DPP-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="UANG_MUKA_DPP"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter UANG_MUKA_DPP" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="UANG_MUKA_DPP"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.UANG_MUKA_PPN}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="UANG_MUKA_PPN-label" class="property-label"><g:message
                    code="EFaktur.UANG_MUKA_PPN.label" default="UANGMUKAPPN"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="UANG_MUKA_PPN-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="UANG_MUKA_PPN"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter UANG_MUKA_PPN" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="UANG_MUKA_PPN"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.UANG_MUKA_PPNBM}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="UANG_MUKA_PPNBM-label" class="property-label"><g:message
                    code="EFaktur.UANG_MUKA_PPNBM.label" default="UANGMUKAPPNBM"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="UANG_MUKA_PPNBM-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="UANG_MUKA_PPNBM"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter UANG_MUKA_PPNBM" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="UANG_MUKA_PPNBM"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.REFERENSI}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="REFERENSI-label" class="property-label"><g:message
                    code="EFaktur.REFERENSI.label" default="REFERENSI"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="REFERENSI-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="REFERENSI"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter REFERENSI" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="REFERENSI"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.LT}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="LT-label" class="property-label"><g:message
                    code="EFaktur.LT.label" default="LT"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="LT-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="LT"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter LT" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="LT"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.NPWP2}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="NPWP2-label" class="property-label"><g:message
                    code="EFaktur.NPWP2.label" default="NPWP 2"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="NPWP2-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="NPWP2"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter NPWP2" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="NPWP2"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.NAMA2}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="NAMA2-label" class="property-label"><g:message
                    code="EFaktur.NAMA2.label" default="NAMA 2"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="NAMA2-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="NAMA2"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter NAMA2" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="NAMA2"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.JALAN}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="JALAN-label" class="property-label"><g:message
                    code="EFaktur.JALAN.label" default="JALAN"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="JALAN-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="JALAN"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter JALAN" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="JALAN"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.BLOK}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="BLOK-label" class="property-label"><g:message
                    code="EFaktur.BLOK.label" default="BLOK"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="BLOK-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="BLOK"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter BLOK" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="BLOK"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.NOMOR}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="NOMOR-label" class="property-label"><g:message
                    code="EFaktur.NOMOR.label" default="NOMOR"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="NOMOR-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="NOMOR"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter NOMOR" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="NOMOR"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.RT}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="RT-label" class="property-label"><g:message
                    code="EFaktur.RT.label" default="RT"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="RT-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="RT"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter RT" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="RT"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.RW}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="RW-label" class="property-label"><g:message
                    code="EFaktur.RW.label" default="RW"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="RW-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="RW"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter RW" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="RW"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.KECAMATAN}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="KECAMATAN-label" class="property-label"><g:message
                    code="EFaktur.KECAMATAN.label" default="KECAMATAN"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="KECAMATAN-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="KECAMATAN"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter KECAMATAN" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="KECAMATAN"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.KELURAHAN}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="KELURAHAN-label" class="property-label"><g:message
                    code="EFaktur.KELURAHAN.label" default="KELURAHAN"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="KELURAHAN-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="KELURAHAN"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter KELURAHAN" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="KELURAHAN"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.KABUPATEN}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="KABUPATEN-label" class="property-label"><g:message
                    code="EFaktur.KABUPATEN.label" default="KABUPATEN"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="KABUPATEN-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="KABUPATEN"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter KABUPATEN" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="KABUPATEN"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.PROPINSI}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="PROPINSI-label" class="property-label"><g:message
                    code="EFaktur.PROPINSI.label" default="PROPINSI"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="PROPINSI-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="PROPINSI"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter PROPINSI" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="PROPINSI"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.KODE_POS}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="KODE_POS-label" class="property-label"><g:message
                    code="EFaktur.KODE_POS.label" default="KODEPOS"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="KODE_POS-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="KODE_POS"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter KODE_POS" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="KODE_POS"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.NOMOR_TELEPON}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="NOMOR_TELEPON-label" class="property-label"><g:message
                    code="EFaktur.NOMOR_TELEPON.label" default="NOMORTELEPON"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="NOMOR_TELEPON-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="NOMOR_TELEPON"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter NOMOR_TELEPON" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="NOMOR_TELEPON"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.OF_}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="OF_-label" class="property-label"><g:message
                    code="EFaktur.OF_.label" default="OF"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="OF_-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="OF_"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter OF_" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="OF_"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.KODE_OBJEK}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="KODE_OBJEK-label" class="property-label"><g:message
                    code="EFaktur.KODE_OBJEK.label" default="KODEOBJEK"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="KODE_OBJEK-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="KODE_OBJEK"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter KODE_OBJEK" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="KODE_OBJEK"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.NAMA}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="NAMA_OBJEK-label" class="property-label"><g:message
                    code="EFaktur.NAMA_OBJEK.label" default="NAMAOBJEK"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="NAMA_OBJEK-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="NAMA_OBJEK"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter NAMA_OBJEK" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="NAMA"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.HARGA_SATUAN}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="HARGA_SATUAN-label" class="property-label"><g:message
                    code="EFaktur.HARGA_SATUAN.label" default="HARGASATUAN"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="HARGA_SATUAN-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="HARGA_SATUAN"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter HARGA_SATUAN" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="HARGA_SATUAN"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.JUMLAH_BARANG}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="JUMLAH_BARANG-label" class="property-label"><g:message
                    code="EFaktur.JUMLAH_BARANG.label" default="JUMLAHBARANG"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="JUMLAH_BARANG-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="JUMLAH_BARANG"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter JUMLAH_BARANG" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="JUMLAH_BARANG"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.HARGA_TOTAL}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="HARGA_TOTAL-label" class="property-label"><g:message
                    code="EFaktur.HARGA_TOTAL.label" default="HARGATOTAL"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="HARGA_TOTAL-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="HARGA_TOTAL"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter HARGA_TOTAL" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="HARGA_TOTAL"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.DISKON}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="DISKON-label" class="property-label"><g:message
                    code="EFaktur.DISKON.label" default="DISKON"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="DISKON-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="DISKON"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter DISKON" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="DISKON"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.DPP}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="DPP-label" class="property-label"><g:message
                    code="EFaktur.DPP.label" default="DPP"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="DPP-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="DPP"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter DPP" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="DPP"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.PPN}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="PPN-label" class="property-label"><g:message
                    code="EFaktur.PPN.label" default="PPN"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="PPN-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="PPN"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter PPN" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="PPN"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.TARIF_PPNBM}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="TARIF_PPNBM-label" class="property-label"><g:message
                    code="EFaktur.TARIF_PPNBM.label" default="TARIFPPNBM"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="TARIF_PPNBM-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="TARIF_PPNBM"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter TARIF_PPNBM" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="TARIF_PPNBM"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.PPNBM}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="PPNBM-label" class="property-label"><g:message
                    code="EFaktur.PPNBM.label" default="PPNBM"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="PPNBM-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="PPNBM"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter PPNBM" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="PPNBM"/>

        </span></td>

    </tr>
</g:if>


<g:if test="${EFakturInstance?.dateCreated}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="dateCreated-label" class="property-label"><g:message
                    code="EFaktur.dateCreated.label" default="Date Created"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="dateCreated-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="dateCreated"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter dateCreated" onsuccess="reloadEFakturTable();" />--}%

            <g:formatDate date="${EFakturInstance?.dateCreated}"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.lastUpdated}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="lastUpdated-label" class="property-label"><g:message
                    code="EFaktur.lastUpdated.label" default="Last Updated"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="lastUpdated-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="lastUpdated"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter lastUpdated" onsuccess="reloadEFakturTable();" />--}%

            <g:formatDate date="${EFakturInstance?.lastUpdated}"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.lastUpdProcess}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="lastUpdProcess-label" class="property-label"><g:message
                    code="EFaktur.lastUpdProcess.label" default="Last Upd Process"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="lastUpdProcess-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="lastUpdProcess"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter lastUpdProcess" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="lastUpdProcess"/>

        </span></td>

    </tr>
</g:if>

<g:if test="${EFakturInstance?.createdBy}">
    <tr>
        <td class="span2" style="text-align: right;"><span
                id="createdBy-label" class="property-label"><g:message
                    code="EFaktur.createdBy.label" default="Created By"/>:</span></td>

        <td class="span3"><span class="property-value"
                                aria-labelledby="createdBy-label">
            %{--<ba:editableValue
                    bean="${EFakturInstance}" field="createdBy"
                    url="${request.contextPath}/EFaktur/updatefield" type="text"
                    title="Enter createdBy" onsuccess="reloadEFakturTable();" />--}%

            <g:fieldValue bean="${EFakturInstance}" field="createdBy"/>

        </span></td>

    </tr>
</g:if>



</tbody>
</table>
<g:form class="form-horizontal">
    <fieldset class="buttons controls">
        <a class="btn cancel" href="javascript:void(0);"
           onclick="expandTableLayout('EFaktur');"><g:message
                code="default.button.back.label" default="Close"/></a>
        %{--<g:remoteLink class="btn btn-primary edit" action="edit"--}%
        %{--id="${EFakturInstance?.id}"--}%
        %{--update="[success: 'EFaktur-form', failure: 'EFaktur-form']"--}%
        %{--on404="alert('not found');">--}%
        %{--?            --}%%{--<g:message code="default.button.edit.label" default="Edit"/>--}%
        %{--</g:remoteLink>--}%
        %{--<ba:confirm id="delete" class="btn cancel"--}%
        %{--message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"--}%
        %{--onsuccess="deleteEFaktur('${EFakturInstance?.id}')"--}%
        %{--label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>--}%
    </fieldset>
</g:form>
</div>
</body>
</html>
