<%@ page import="com.kombos.parts.Vendor" %>

<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode != 45  && charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</g:javascript>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="vendor_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px; vertical-align:bottom;" rowspan="2">
				<div><g:message code="vendor.m121ID.label" default="Nomor" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; vertical-align:bottom;" rowspan="2">
				<div><g:message code="vendor.m121Nama.label" default="Nama Vendor" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; vertical-align:bottom;" rowspan="2">
				<div><g:message code="vendor.m121staSPLD.label" default="SPLD" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; vertical-align:bottom;" rowspan="2">
				<div><g:message code="vendor.m121Alamat.label" default="Alamat Office" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; vertical-align:bottom;" rowspan="2">
				<div><g:message code="vendor.m121AlamatNPWP.label" default="Alamat NPWP" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; vertical-align:bottom;" rowspan="2">
				<div><g:message code="vendor.m121Telp.label" default="Telp" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; vertical-align:bottom;" rowspan="2">
				<div><g:message code="vendor.m121NPWP.label" default="NPWP" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; vertical-align:bottom;" rowspan="2">
				<div><g:message code="vendor.m121eMail.label" default="Email" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; vertical-align:bottom;" rowspan="2">
				<div><g:message code="vendor.m121ContactPerson.label" default="Contact Person" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; vertical-align:bottom;" rowspan="2">
				<div><g:message code="vendor.m121TelpCP.label" default="Nomor Telp CP" /></div>
			</th>


            <th style="border-bottom: none;padding: 5px;  text-align: center" colspan="3">
                <div><g:message code="vendor.bank1.label" default="Bank 1" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;  text-align: center" colspan="3">
                <div><g:message code="vendor.bank2.label" default="Bank 2" /></div>
            </th>

        </tr>
        <tr>


			<th style="border-bottom: none;padding: 5px; text-align: center">
				<div><g:message code="vendor.m121NamaBank1.label" default="Nama Bank" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; text-align: center">
				<div><g:message code="vendor.m121NoRekBank1.label" default="No Rekening" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; text-align: center">
				<div><g:message code="vendor.m121AtasNamaBank1.label" default="Rekening a.n" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; text-align: center">
				<div><g:message code="vendor.m121NamaBank2.label" default="Nama Bank" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; text-align: center">
				<div><g:message code="vendor.m121NoRekBank2.label" default="No Rekening" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; text-align: center">
				<div><g:message code="vendor.m121AtasNamaBank2.label" default="Rekening a.n" /></div>
			</th>


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="vendor.m121staDel.label" default="M121sta Del" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="vendor.createdBy.label" default="Created By" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="vendor.updatedBy.label" default="Updated By" /></div>--}%
			%{--</th>--}%


			%{--<th style="border-bottom: none;padding: 5px;">--}%
				%{--<div><g:message code="vendor.lastUpdProcess.label" default="Last Upd Process" /></div>--}%
			%{--</th>--}%

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m121ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m121ID" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m121Nama" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m121Nama" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_m121staSPLD" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">

                    <select name="search_m121staSPLD" id="search_m121staSPLD" style="width:100%" onchange="reloadVendorTable();">
                        <option value=""></option>
                        <option value="0">SPLD</option>
                        <option value="1">Non SPLD</option>
                    </select>
                </div>
            </th>
	
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_m121staSPLD" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_m121staSPLD" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m121Alamat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m121Alamat" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m121AlamatNPWP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m121AlamatNPWP" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m121Telp" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m121Telp" onkeypress="return isNumberKey(event)" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m121NPWP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m121NPWP" onkeypress="return isNumberKey(event)" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m121eMail" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m121eMail" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m121ContactPerson" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m121ContactPerson" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m121TelpCP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m121TelpCP" onkeypress="return isNumberKey(event)" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m121NamaBank1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m121NamaBank1" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m121NoRekBank1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m121NoRekBank1" onkeypress="return isNumberKey(event)" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m121AtasNamaBank1" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m121AtasNamaBank1" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m121NamaBank2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m121NamaBank2" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m121NoRekBank2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m121NoRekBank2" onkeypress="return isNumberKey(event)" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m121AtasNamaBank2" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m121AtasNamaBank2" class="search_init" />
				</div>
			</th>
	
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_m121staDel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_m121staDel" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_createdBy" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_updatedBy" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%
	%{----}%
			%{--<th style="border-top: none;padding: 5px;">--}%
				%{--<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">--}%
					%{--<input type="text" name="search_lastUpdProcess" class="search_init" />--}%
				%{--</div>--}%
			%{--</th>--}%


		</tr>
	</thead>
</table>

<g:javascript>
var vendorTable;
var reloadVendorTable;
$(function(){
	
	reloadVendorTable = function() {
		vendorTable.fnDraw();
	}

	var recordsVendorPerPage = [];
    var anVendorSelected;
    var jmlRecVendorPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	vendorTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	vendorTable = $('#vendor_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsVendor = $("#vendor_datatables tbody .row-select");
            var jmlVendorCek = 0;
            var nRow;
            var idRec;
            rsVendor.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsVendorPerPage[idRec]=="1"){
                    jmlVendorCek = jmlVendorCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsVendorPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecVendorPerPage = rsVendor.length;
            if(jmlVendorCek==jmlRecVendorPerPage && jmlRecVendorPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m121ID",
	"mDataProp": "m121ID",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m121Nama",
	"mDataProp": "m121Nama",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m121staSPLD",
	"mDataProp": "m121staSPLD",
	"aTargets": [2],
	"mRender": function ( data, type, row ) {
            if(data=="0"){
                  return "SPLD";
             }else
             {
                  return "Non SPLD";
             }

        },
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "m121Alamat",
	"mDataProp": "m121Alamat",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m121AlamatNPWP",
	"mDataProp": "m121AlamatNPWP",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m121Telp",
	"mDataProp": "m121Telp",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m121NPWP",
	"mDataProp": "m121NPWP",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m121eMail",
	"mDataProp": "m121eMail",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "m121ContactPerson",
	"mDataProp": "m121ContactPerson",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "m121TelpCP",
	"mDataProp": "m121TelpCP",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "m121NamaBank1",
	"mDataProp": "m121NamaBank1",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m121NoRekBank1",
	"mDataProp": "m121NoRekBank1",
	"aTargets": [11],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m121AtasNamaBank1",
	"mDataProp": "m121AtasNamaBank1",
	"aTargets": [12],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m121NamaBank2",
	"mDataProp": "m121NamaBank2",
	"aTargets": [13],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m121NoRekBank2",
	"mDataProp": "m121NoRekBank2",
	"aTargets": [14],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m121AtasNamaBank2",
	"mDataProp": "m121AtasNamaBank2",
	"aTargets": [15],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
//
//,
//
//{
//	"sName": "m121staDel",
//	"mDataProp": "m121staDel",
//	"aTargets": [16],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "createdBy",
//	"mDataProp": "createdBy",
//	"aTargets": [17],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "updatedBy",
//	"mDataProp": "updatedBy",
//	"aTargets": [18],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}
//
//,
//
//{
//	"sName": "lastUpdProcess",
//	"mDataProp": "lastUpdProcess",
//	"aTargets": [19],
//	"bSearchable": true,
//	"bSortable": true,
//	"sWidth":"200px",
//	"bVisible": true
//}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m121ID = $('#filter_m121ID input').val();
						if(m121ID){
							aoData.push(
									{"name": 'sCriteria_m121ID', "value": m121ID}
							);
						}
	
						var m121Nama = $('#filter_m121Nama input').val();
						if(m121Nama){
							aoData.push(
									{"name": 'sCriteria_m121Nama', "value": m121Nama}
							);
						}
	
						var m121staSPLD = $('#search_m121staSPLD').val();
						if(m121staSPLD){
							aoData.push(
									{"name": 'sCriteria_m121staSPLD', "value": m121staSPLD}
							);
						}
	
						var m121Alamat = $('#filter_m121Alamat input').val();
						if(m121Alamat){
							aoData.push(
									{"name": 'sCriteria_m121Alamat', "value": m121Alamat}
							);
						}
	
						var m121AlamatNPWP = $('#filter_m121AlamatNPWP input').val();
						if(m121AlamatNPWP){
							aoData.push(
									{"name": 'sCriteria_m121AlamatNPWP', "value": m121AlamatNPWP}
							);
						}
	
						var m121Telp = $('#filter_m121Telp input').val();
						if(m121Telp){
							aoData.push(
									{"name": 'sCriteria_m121Telp', "value": m121Telp}
							);
						}
	
						var m121NPWP = $('#filter_m121NPWP input').val();
						if(m121NPWP){
							aoData.push(
									{"name": 'sCriteria_m121NPWP', "value": m121NPWP}
							);
						}
	
						var m121eMail = $('#filter_m121eMail input').val();
						if(m121eMail){
							aoData.push(
									{"name": 'sCriteria_m121eMail', "value": m121eMail}
							);
						}
	
						var m121ContactPerson = $('#filter_m121ContactPerson input').val();
						if(m121ContactPerson){
							aoData.push(
									{"name": 'sCriteria_m121ContactPerson', "value": m121ContactPerson}
							);
						}
	
						var m121TelpCP = $('#filter_m121TelpCP input').val();
						if(m121TelpCP){
							aoData.push(
									{"name": 'sCriteria_m121TelpCP', "value": m121TelpCP}
							);
						}
	
						var m121NamaBank1 = $('#filter_m121NamaBank1 input').val();
						if(m121NamaBank1){
							aoData.push(
									{"name": 'sCriteria_m121NamaBank1', "value": m121NamaBank1}
							);
						}
	
						var m121NoRekBank1 = $('#filter_m121NoRekBank1 input').val();
						if(m121NoRekBank1){
							aoData.push(
									{"name": 'sCriteria_m121NoRekBank1', "value": m121NoRekBank1}
							);
						}
	
						var m121AtasNamaBank1 = $('#filter_m121AtasNamaBank1 input').val();
						if(m121AtasNamaBank1){
							aoData.push(
									{"name": 'sCriteria_m121AtasNamaBank1', "value": m121AtasNamaBank1}
							);
						}
	
						var m121NamaBank2 = $('#filter_m121NamaBank2 input').val();
						if(m121NamaBank2){
							aoData.push(
									{"name": 'sCriteria_m121NamaBank2', "value": m121NamaBank2}
							);
						}
	
						var m121NoRekBank2 = $('#filter_m121NoRekBank2 input').val();
						if(m121NoRekBank2){
							aoData.push(
									{"name": 'sCriteria_m121NoRekBank2', "value": m121NoRekBank2}
							);
						}
	
						var m121AtasNamaBank2 = $('#filter_m121AtasNamaBank2 input').val();
						if(m121AtasNamaBank2){
							aoData.push(
									{"name": 'sCriteria_m121AtasNamaBank2', "value": m121AtasNamaBank2}
							);
						}
	
						var m121staDel = $('#filter_m121staDel input').val();
						if(m121staDel){
							aoData.push(
									{"name": 'sCriteria_m121staDel', "value": m121staDel}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
	$('.select-all').click(function(e) {

        $("#vendor_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsVendorPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsVendorPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#vendor_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsVendorPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anVendorSelected = vendorTable.$('tr.row_selected');
            if(jmlRecVendorPerPage == anVendorSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsVendorPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
