<%@ page import="com.kombos.parts.Vendor" %>
<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode != 45  && charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</g:javascript>


<g:if test="${merkInstance?.m121ID}">
<div class="control-group fieldcontain ${hasErrors(bean: vendorInstance, field: 'm121ID', 'error')} required">
	<label class="control-label" for="m121ID">
		<g:message code="vendor.m121ID.label" default="No. Vendor" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	${vendorInstance?.m121ID}
	</div>
</div>
</g:if>

<div class="control-group fieldcontain ${hasErrors(bean: vendorInstance, field: 'm121Nama', 'error')} required">
	<label class="control-label" for="m121Nama">
		<g:message code="vendor.m121Nama.label" default="Nama" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m121Nama" maxlength="100" required="" value="${vendorInstance?.m121Nama}"/>
	</div>
    <div class="controls">
        <g:radioGroup name="m121staSPLD"  values="['0','1']" labels="['SPLD','Non SPLD']" value="${vendorInstance?.m121staSPLD}" >
            ${it.radio} ${it.label}
        </g:radioGroup>
     </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorInstance, field: 'm121Alamat', 'error')} required">
	<label class="control-label" for="m121Alamat">
		<g:message code="vendor.m121Alamat.label" default="Alamat Office" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textArea name="m121Alamat" maxlength="255" required="" value="${vendorInstance?.m121Alamat}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorInstance, field: 'm121AlamatNPWP', 'error')} required">
	<label class="control-label" for="m121AlamatNPWP">
		<g:message code="vendor.m121AlamatNPWP.label" default="Alamat NPWP" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textArea name="m121AlamatNPWP" maxlength="255" required="" value="${vendorInstance?.m121AlamatNPWP}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorInstance, field: 'm121Telp', 'error')} required">
	<label class="control-label" for="m121Telp">
		<g:message code="vendor.m121Telp.label" default="Telepon" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m121Telp"  onkeypress="return isNumberKey(event)" maxlength="20" required="" value="${vendorInstance?.m121Telp}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorInstance, field: 'm121NPWP', 'error')} required">
	<label class="control-label" for="m121NPWP">
		<g:message code="vendor.m121NPWP.label" default="Nomor NPWP" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m121NPWP" onkeypress="return isNumberKey(event)" maxlength="20" required="" value="${vendorInstance?.m121NPWP}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorInstance, field: 'm121eMail', 'error')} required">
	<label class="control-label" for="m121eMail">
		<g:message code="vendor.m121eMail.label" default="Email" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m121eMail" maxlength="20" required="" value="${vendorInstance?.m121eMail}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorInstance, field: 'm121ContactPerson', 'error')} required">
	<label class="control-label" for="m121ContactPerson">
		<g:message code="vendor.m121ContactPerson.label" default="Contact Person" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m121ContactPerson" maxlength="20" required="" value="${vendorInstance?.m121ContactPerson}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorInstance, field: 'm121TelpCP', 'error')} required">
	<label class="control-label" for="m121TelpCP">
		<g:message code="vendor.m121TelpCP.label" default="Nomor Telp CP" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m121TelpCP" onkeypress="return isNumberKey(event)" maxlength="50" required="" value="${vendorInstance?.m121TelpCP}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorInstance, field: 'm121NamaBank1', 'error')} required">
	<label class="control-label" for="m121NamaBank1">
		<g:message code="vendor.m121NamaBank1.label" default="Nama Bank 1" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m121NamaBank1" maxlength="20" required="" value="${vendorInstance?.m121NamaBank1}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorInstance, field: 'm121NoRekBank1', 'error')} required">
	<label class="control-label" for="m121NoRekBank1">
		<g:message code="vendor.m121NoRekBank1.label" default="No Rekening Bank 1" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m121NoRekBank1" onkeypress="return isNumberKey(event)" maxlength="20" required="" value="${vendorInstance?.m121NoRekBank1}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorInstance, field: 'm121AtasNamaBank1', 'error')} required">
	<label class="control-label" for="m121AtasNamaBank1">
		<g:message code="vendor.m121AtasNamaBank1.label" default="Atas Nama Bank 1" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m121AtasNamaBank1" maxlength="20" required="" value="${vendorInstance?.m121AtasNamaBank1}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorInstance, field: 'm121NamaBank2', 'error')} ">
	<label class="control-label" for="m121NamaBank2">
		<g:message code="vendor.m121NamaBank2.label" default="Nama Bank 2" />
		
	</label>
	<div class="controls">
	<g:textField name="m121NamaBank2" maxlength="20" value="${vendorInstance?.m121NamaBank2}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorInstance, field: 'm121NoRekBank2', 'error')} ">
	<label class="control-label" for="m121NoRekBank2">
		<g:message code="vendor.m121NoRekBank2.label" default="No Rekening Bank 2" />
		
	</label>
	<div class="controls">
	<g:textField name="m121NoRekBank2" onkeypress="return isNumberKey(event)" maxlength="20" value="${vendorInstance?.m121NoRekBank2}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: vendorInstance, field: 'm121AtasNamaBank2', 'error')} ">
	<label class="control-label" for="m121AtasNamaBank2">
		<g:message code="vendor.m121AtasNamaBank2.label" default="Atas Nama Bank 2" />
		
	</label>
	<div class="controls">
	<g:textField name="m121AtasNamaBank2" maxlength="20" value="${vendorInstance?.m121AtasNamaBank2}"/>
	</div>
</div>


<g:javascript>
    document.getElementById("m121Nama").focus();
</g:javascript>