<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
		<g:javascript>
		</g:javascript>
	</head>
	<body>
		<div class="box">
		    <legend style="font-size: small; line-height: 15px;">
                <table width="100%">
                    <tr>
                        <td colspan="4" width="100%"><h4>PT TOYOTA - ASTRA MOTOR</h4></td>
                    </tr>
                    <tr>
                        <td colspan="2" width="50%">${data?.namaWorkshop}</td>
                        <td width="20%">&nbsp;</td>
                        <td width="30%" align="center" rowspan="2" valign="midle"><h4>REFUND FORM</h4></td>
                    </tr>
                    <tr>
                        <td colspan="2" width="50%">${data?.alamatWorkshop}</td>
                        <td width="20%">&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="3%">TLP</td>
                        <td width="47%">${data?.teleponWorkshop} Ext. ${data?.extWorkshop}</td>
                        <td width="20%">&nbsp;</td>
                        <td width="30%" align="center">(${data?.jenisRefund})</td>
                    </tr>
                    <tr>
                        <td width="3%">&nbsp;</td>
                        <td width="47%">${data?.teleponWorkshop} (DIRECT)</td>
                        <td width="20%">&nbsp;</td>
                        <td width="30%">&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="3%">Fax</td>
                        <td width="47%">${data?.faxWorkshop}</td>
                        <td width="20%">&nbsp;</td>
                        <td width="30%" align="left">No. ${data?.nomorRefund}</td>
                    </tr>
                </table>
                <br/>
		    </legend>
            <table width="100%">
                <tr>
                    <td width="19%">WO / Police No.</td>
                    <td width="1%">:</td>
                    <td width="80%">${data?.nomorWO}/${data?.nomorPolisi}</td>
                </tr>
                <tr>
                    <td width="19%">Customer</td>
                    <td width="1%">:</td>
                    <td width="80%">${data?.customer}</td>
                </tr>
                <tr>
                    <td width="19%">Invoice</td>
                    <td width="1%">:</td>
                    <td width="80%">${data?.invoice}</td>
                </tr>
                <tr>
                    <td width="19%">Amount</td>
                    <td width="1%">:</td>
                    <td width="80%">${data?.amount}</td>
                </tr>
                <tr>
                    <td width="19%">Say in Word</td>
                    <td width="1%">:</td>
                    <td width="80%">${data?.sayInWord}</td>
                </tr>
                <tr>
                    <td width="19%">Reason</td>
                    <td width="1%">:</td>
                    <td width="80%">${data?.reason}${data?.reasonOther}</td>
                </tr>
                <tr>
                    <td width="19%">Bank - A/C Name - A/C No.</td>
                    <td width="1%">:</td>
                    <td width="80%">${data?.namaBank} - ${data?.actName} - ${data?.actNo}</td>
                </tr>
            </table>
			<br/>
            <br/>
            <span>Jakarta, ${data?.tanggalRefund}</span>
            <table width="100%" border="1">
                <tr>
                    <td width="17%" align="center">Payment</td>
                    <td width="17%" align="center">Requestor</td>
                    <td width="49%" align="center" colspan="3" >Acknowledge</td>
                    <td width="17%" align="center">Approved By</td>
                </tr>
                <tr>
                    <td width="17%" align="center">Method</td>
                    <td width="17%" align="center">(Customer)</td>
                    <td width="16%" align="center">Serv. Adv</td>
                    <td width="16%" align="center">Cashier</td>
                    <td width="17%" align="center">FAD Rep</td>
                    <td width="17%" align="center">Workshop Head</td>
                </tr>
                <tr>
                    <td width="17%" height="60">&nbsp;</td>
                    <td width="17%">&nbsp;</td>
                    <td width="16%">&nbsp;</td>
                    <td width="16%">&nbsp;</td>
                    <td width="17%">&nbsp;</td>
                    <td width="17%">&nbsp;</td>
                </tr>
            </table>
		</div>
	</body>
</html>