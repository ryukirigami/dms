<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="faktur_pajak_datatables_${idTable}" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	style="table-layout: fixed;"
	width="100%">
	<thead>
    	<tr>
			<th>
				<div style="width: 10px;">&nbsp;</div>
			</th>
        	<th style="border-bottom: none;">
            	<div style="width: 100px;">Tgl Faktur</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 125px;">No. Faktur Pajak</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 125px;">Invoice</div>
			</th>			
			<th style="border-bottom: none;">
            	<div style="width: 125px;">Penjual</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 150px;">Alamat Penjual</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 100px;">NPWP</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 125px;">Penerima BKP/JKP</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 150px;">Alamat</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 100px;">NPWP</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 125px;">Jasa</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 125px;">Parts</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 125px;">DPP</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 125px;">PPN 10%</div>
			</th>
		</tr>
	</thead>
</table>
<g:javascript>
var fakturTable;
var reloadFakturTable;
var msg='';

function searchData() {
    if(isValid()){
        reloadFakturTable();
    }else{
        alert(msg);
        return false;
    }
}

$(function() {

    isValid = function(){
        msg='';
        var chkTanggal = $('input[name=chkTanggalFaktur]').is(':checked');
        if(chkTanggal){
            var tanggalFakturStart = $("#tanggalFaktur_start").val();
            var tanggalFakturEnd = $("#tanggalFaktur_end").val();
            if(tanggalFakturStart==''){msg+='- Silahkan pilih tanggal awal faktur \n';}
            if(tanggalFakturEnd==''){msg+='- Silahkan pilih tanggal akhir faktur \n';}
        }

        var chkKategori = $('input[name=chkKategoriFaktur]').is(':checked');
        if(chkKategori){
            var kategoriFaktur = $('input[name=kategoriFaktur]').val();
            if(kategoriFaktur==''){msg+='- Silahkan pilih kategori faktur \n';}
        }

        var chkKataKunci = $('input[name=chkKataKunci]').is(':checked');
        if(chkKataKunci){
            var kataKunci = $('input[name=kataKunci]').val();
            if(kataKunci==''){msg+='- Silahkan masukkan kata kunci faktur \n';}
        }

        return msg=='';
    };

	reloadFakturTable = function() {
		fakturTable.fnDraw();
	};
	
	fakturTable = $('#faktur_pajak_datatables_${idTable}').dataTable({
		"sScrollX": "1024px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		},
	    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
	    },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "fakturPajakDataTablesList")}",
		"aoColumns": [
			//radioButton
			{
				"mDataProp": "noFaktur",
		        "sClass": "control center",
		        "bSortable": false,
		        "sWidth":"10px",
				"mRender": function ( data, type, row ) {
					return '<input type="radio" name="radioButton" class="pull-left row-select" value="'+row['noFaktur']+'" style="margin-top: 4px;"><input type="hidden" value="'+row['noFaktur']+'">';
				}
			},	
			//tanggalFaktur
			{
				"sName": "tanggalFaktur",
				"mDataProp": "tanggalFaktur",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			//noFaktur
			{
				"sName": "noFaktur",
				"mDataProp": "noFaktur",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//invoice
			{
				"sName": "invoice",
				"mDataProp": "invoice",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//penjual
			{
				"sName": "penjual",
				"mDataProp": "penjual",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//alamatPenjual
			{
				"sName": "alamatPenjual",
				"mDataProp": "alamatPenjual",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"150px",
				"bVisible": true
			},
			//npwpPenjual
			{
				"sName": "npwpPenjual",
				"mDataProp": "npwpPenjual",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			//penerima
			{
				"sName": "penerima",
				"mDataProp": "penerima",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//alamatPenerima
			{
				"sName": "alamatPenerima",
				"mDataProp": "alamatPenerima",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"150px",
				"bVisible": true
			},
			//npwpPenerima
			{
				"sName": "npwpPenerima",
				"mDataProp": "npwpPenerima",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			//jasa
			{
				"sName": "jasa",
				"mDataProp": "jasa",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//parts
			{
				"sName": "parts",
				"mDataProp": "parts",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//dpp
			{
				"sName": "dpp",
				"mDataProp": "dpp",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//ppn
			{
				"sName": "ppn",
				"mDataProp": "ppn",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			}
		],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			var tanggalFakturStart = $("#tanggalFaktur_start").val();
			var tanggalFakturStartDay = $('#tanggalFaktur_start_day').val();
			var tanggalFakturStartMonth = $('#tanggalFaktur_start_month').val();
			var tanggalFakturStartYear = $('#tanggalFaktur_start_year').val();
			var chkTanggal = $('input[name=chkTanggalFaktur]').is(':checked');
            if(tanggalFakturStart && chkTanggal) {
				aoData.push(
						{"name": 'tanggalFakturStart', "value": "date.struct"},
						{"name": 'tanggalFakturStart_dp', "value": tanggalFakturStart},
						{"name": 'tanggalFakturStart_day', "value": tanggalFakturStartDay},
						{"name": 'tanggalFakturStart_month', "value": tanggalFakturStartMonth},
						{"name": 'tanggalFakturStart_year', "value": tanggalFakturStartYear},
						{"name": 'chkTanggalFaktur', "value": true}
				);
			}
			
            var tanggalFakturEnd = $("#tanggalFaktur_end").val();
            var tanggalFakturEndDay = $('#tanggalFaktur_end_day').val();
			var tanggalFakturEndMonth = $('#tanggalFaktur_end_month').val();
			var tanggalFakturEndYear = $('#tanggalFaktur_end_year').val();
			if(tanggalFakturEnd && chkTanggal) {
				aoData.push(
						{"name": 'tanggalFakturEnd', "value": "date.struct"},
						{"name": 'tanggalFakturEnd_dp', "value": tanggalFakturEnd},
						{"name": 'tanggalFakturEnd_day', "value": tanggalFakturEndDay},
						{"name": 'tanggalFakturEnd_month', "value": tanggalFakturEndMonth},
						{"name": 'tanggalFakturEnd_year', "value": tanggalFakturEndYear}
				);
			}
			
            var kategoriFaktur = $('input[name=kategoriFaktur]').val();
            var chkKategori = $('input[name=chkKategoriFaktur]').is(':checked');
            if(kategoriFaktur && chkKategori) {
				aoData.push(
						{"name": 'kategoriFaktur', "value": kategoriFaktur},
						{"name": 'chkKategoriFaktur', "value": true}
				);
			}
            
			var kataKunci = $('input[name=kataKunci]').val();
			var chkKataKunci = $('input[name=chkKataKunci]').is(':checked');
            if(kataKunci && chkKataKunci) {
				aoData.push(
						{"name": 'kataKunci', "value": kataKunci},
						{"name": 'chkKataKunci', "value": true}
				);
			}			
			
			$.ajax({ "dataType": 'json',
				"type": "POST",
				"url": sSource,
				"data": aoData ,
				"success": function (json) {
					fnCallback(json);
				},
				"complete": function () {}
			});
			
		}
	});
		
});
</g:javascript>