<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
		<g:javascript>
		</g:javascript>
	</head>
	<body>
		<div class="box">
		    <table width="100%">
                <tr>
                    <td width="50%"><h5><span style="color: red;">PT. TOYOTA - ASTRA MOTOR</span></h5></td>
                    <td width="50%" align="center">No. ${data?.slipNo}</td>
                </tr>
                <tr>
                    <td width="100%" colspan="2">TECHNICAL SERVICE DIVISION - SUNTER WORKSHOP</td>
                </tr>
                <tr>
                    <td width="100%" align="center" colspan="2"><h4>PARTS SLIP</h4></td>
                </tr>
                <tr>
                    <td width="50%">
                        <table width="100%">
                            <tr>
                                <td width="8%">POL</td>
                                <td width="2%">:</td>
                                <td width="90%">${data?.nomorPolisi}</td>
                            </tr>
                            <tr>
                                <td width="8%">CUST</td>
                                <td width="2%">:</td>
                                <td width="90%">${data?.customer}</td>
                            </tr>
                            <tr>
                                <td width="8%">ADRS</td>
                                <td width="2%">:</td>
                                <td width="90%">${data?.addess}</td>
                            </tr>
                        </table>
                    </td>
                    <td width="50%" align="right">
                        <span>Page : 1</span>
                        <table width="100%" border="1">
                            <tr bgcolor="#a9a9a9">
                                <td width="35%" align="center">WO. NO.</td>
                                <td width="25%" align="center">SLIP NO.</td>
                                <td width="20%" align="center">DATE</td>
                                <td width="20%" align="center">TIME</td>
                            </tr>
                            <tr style="border-bottom: 0;">
                                <td width="35%" align="center">${data?.nomorWO}</td>
                                <td width="25%" align="center">${data?.slipNo}</td>
                                <td width="20%" align="center"><g:formatDate format="dd-MM-yyyy" date="${data?.tanggal}"/></td>
                                <td width="20%" align="center"><g:formatDate format="HH:mm:ss" date="${data?.tanggal}"/></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="100%" colspan="2">
                        <table width="100%" border="1">
                            <tr bgcolor="#a9a9a9">
                                <td width="5%" align="center">NO</td>
                                <td width="20%" align="center">PARTS NO</td>
                                <td width="25%" align="center">PARTS NAME</td>
                                <td width="7%" align="center">QTY</td>
                                <td width="14%" align="center">UNIT PRICE</td>
                                <td width="7%" align="center">DSC</td>
                                <td width="12%" align="center">EXTD. AMOUNT</td>
                                <td width="10%" align="center">RMK</td>
                            </tr>
                            <g:each var="row" in="${data?.details}" status="i">
                            <tr>
                                <td width="5%" align="center">${row?.no?.encodeAsHTML()}</td>
                                <td width="20%" align="center">${row?.partNo?.encodeAsHTML()}</td>
                                <td width="25%" align="left">${row?.partName?.encodeAsHTML()}</td>
                                <td width="7%" align="center">${row?.qty?.encodeAsHTML()}</td>
                                <td width="14%" align="right">${row?.unitPrice?.encodeAsHTML()}</td>
                                <td width="7%" align="center">${row?.discount?.encodeAsHTML()}</td>
                                <td width="12%" align="right">${row?.extAmount?.encodeAsHTML()}</td>
                                <td width="10%" align="center">${row?.remark?.encodeAsHTML()}</td>
                            </tr>
                            </g:each>
                        </table>
                    </td>
                </tr>
                <tr style="height: 5px;">
                    <td width="100%" colspan="2" height="5px;"></td>
                </tr>
                <tr>
                    <td width="50%">
                        <table width="100%">
                            <tr>
                                <td width="10%">&nbsp;</td>
                                <td width="40%">
                                    <img src="${request.contextPath}/images/toyota_quality_service.jpg" height="40" width="220">
                                </td>
                                <td width="50%">
                                    <img src="${request.contextPath}/images/toyota_genuine_parts.jpg" height="40" width="245">
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="50%">
                        <table width="100%" border="1">
                            <tr>
                                <td width="25%" align="center" valign="bottom"><br/><br/><br/>PART MAN</td>
                                <td width="25%" align="center" valign="bottom"><br/><br/><br/>SRV. ADVISOR</td>
                                <td width="25%" align="center" valign="bottom"><br/><br/><br/>BILLING</td>
                                <td width="25%" align="center" valign="bottom"><br/><br/><br/>NOTE</td>
                            </tr>
                        </table>
                    </td>
                </tr>
		    </table>
		</div>
	</body>
</html>