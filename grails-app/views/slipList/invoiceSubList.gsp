<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
		<g:javascript>
			var invoiceSubTable;
			$(function() {
			    if(invoiceSubTable){
					invoiceSubTable.dataTable().fnDestroy();
				}
				
				invoiceSubTable = $('#invoice_datatables_sub_${idTable}').dataTable({
					"sScrollX": "1024px",
					"bScrollCollapse": true,
					"bAutoWidth" : false,
					"bPaginate" : false,
					"bInfo" : false,
					"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
					"bFilter": false,
					"bStateSave": false,
					'sPaginationType': 'bootstrap',
					"fnInitComplete": function () {
						this.fnAdjustColumnSizing(true);
					},
				    "fnRowCallback": function (nRow) {
						return nRow;
				    },
					"bSort": true,
					"bProcessing": true,
					"bServerSide": true,
					"sServerMethod": "POST",
					"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
					"sAjaxSource": '${request.contextPath}/slipList/invoiceDataTablesSubList?id=${id}',					
					"aoColumns": [
						//blank
						{
					    	"mDataProp": null,
					        "bSortable": false,
					        "sWidth":"264px",
					        "sDefaultContent": ''
						},					
						//tanggalSettlement
						{
							"sName": "tanggalSettlement",
							"mDataProp": "tanggalSettlement",
							"bSearchable": false,
							"bSortable": false,
							"sWidth":"125px",
							"bVisible": true
						},
						//metodeBayar
						{
							"sName": "metodeBayar",
							"mDataProp": "metodeBayar",
							"bSearchable": false,
							"bSortable": false,
							"sWidth":"125px",
							"bVisible": true
						},
						//namaBank
						{
							"sName": "namaBank",
							"mDataProp": "namaBank",
							"bSearchable": false,
							"bSortable": false,
							"sWidth":"125px",
							"bVisible": true
						},
						//mesinEdc
						{
							"sName": "mesinEdc",
							"mDataProp": "mesinEdc",
							"bSearchable": false,
							"bSortable": false,
							"sWidth":"125px",
							"bVisible": true
						},
						//nomorKartu
						{
							"sName": "nomorKartu",
							"mDataProp": "nomorKartu",
							"bSearchable": false,
							"bSortable": false,
							"sWidth":"267px",
							"bVisible": true
						},
						//bankCharge
						{
							"sName": "bankCharge",
							"mDataProp": "bankCharge",
							"bSearchable": false,
							"bSortable": false,
							"sWidth":"217px",
							"bVisible": true
						},
						//nomorWbs
						{
							"sName": "nomorWbs",
							"mDataProp": "nomorWbs",
							"bSearchable": false,
							"bSortable": false,
							"sWidth":"125px",
							"bVisible": true
						},
						//jumlahSettlement
						{
							"sName": "jumlahSettlement",
							"mDataProp": "jumlahSettlement",
							"bSearchable": false,
							"bSortable": false,
							"sWidth":"125px",
							"bVisible": true
						}
					],
					"fnServerData": function ( sSource, aoData, fnCallback ) {
												
						var tanggalInvoiceStart = $("#tanggalInvoice_start").val();
						var tanggalInvoiceStartDay = $('#tanggalInvoice_start_day').val();
						var tanggalInvoiceStartMonth = $('#tanggalInvoice_start_month').val();
						var tanggalInvoiceStartYear = $('#tanggalInvoice_start_year').val();
						var chkTanggal = $('input[name=chkTanggalInvoice]').is(':checked');
			            if(tanggalInvoiceStart && chkTanggal) {
							aoData.push(
									{"name": 'tanggalInvoiceStart', "value": "date.struct"},
									{"name": 'tanggalInvoiceStart_dp', "value": tanggalInvoiceStart},
									{"name": 'tanggalInvoiceStart_day', "value": tanggalInvoiceStartDay},
									{"name": 'tanggalInvoiceStart_month', "value": tanggalInvoiceStartMonth},
									{"name": 'tanggalInvoiceStart_year', "value": tanggalInvoiceStartYear},
									{"name": 'chkTanggalInvoice', "value": true}
							);
						}
			
			            var tanggalInvoiceEnd = $("#tanggalInvoice_end").val();
			            var tanggalInvoiceEndDay = $('#tanggalInvoice_end_day').val();
						var tanggalInvoiceEndMonth = $('#tanggalInvoice_end_month').val();
						var tanggalInvoiceEndYear = $('#tanggalInvoice_end_year').val();
						if(tanggalInvoiceEnd && chkTanggal) {
							aoData.push(
									{"name": 'tanggalInvoiceEnd', "value": "date.struct"},
									{"name": 'tanggalInvoiceEnd_dp', "value": tanggalInvoiceEnd},
									{"name": 'tanggalInvoiceEnd_day', "value": tanggalInvoiceEndDay},
									{"name": 'tanggalInvoiceEnd_month', "value": tanggalInvoiceEndMonth},
									{"name": 'tanggalInvoiceEnd_year', "value": tanggalInvoiceEndYear}
							);
						}
			
						var tanggalSettlementStart = $("#tanggalSettlement_start").val();
						var tanggalSettlementStartDay = $('#tanggalSettlement_start_day').val();
						var tanggalSettlementStartMonth = $('#tanggalSettlement_start_month').val();
						var tanggalSettlementStartYear = $('#tanggalSettlement_start_year').val();
						var chkSettlement = $('input[name=chkTanggalSettlement]').is(':checked');
			            if(tanggalSettlementStart && chkSettlement) {
							aoData.push(
									{"name": 'tanggalSettlementStart', "value": "date.struct"},
									{"name": 'tanggalSettlementStart_dp', "value": tanggalSettlementStart},
									{"name": 'tanggalSettlementStart_day', "value": tanggalSettlementStartDay},
									{"name": 'tanggalSettlementStart_month', "value": tanggalSettlementStartMonth},
									{"name": 'tanggalSettlementStart_year', "value": tanggalSettlementStartYear},
									{"name": 'chkTanggalSettlement', "value": true}
							);
						}
			
			            var tanggalSettlementEnd = $("#tanggalSettlement_end").val();
			            var tanggalSetllementEndDay = $('#tanggalSettlement_end_day').val();
						var tanggalSettlementEndMonth = $('#tanggalSettlement_end_month').val();
						var tanggalSettlementEndYear = $('#tanggalSettlement_end_year').val();
						if(tanggalSettlementEnd && chkSettlement) {
							aoData.push(
									{"name": 'tanggalSettlementEnd', "value": "date.struct"},
									{"name": 'tanggalSettlementEnd_dp', "value": tanggalSettlementEnd},
									{"name": 'tanggalSettlementEnd_day', "value": tanggalSetllementEndDay},
									{"name": 'tanggalSettlementEnd_month', "value": tanggalSettlementEndMonth},
									{"name": 'tanggalSettlementEnd_year', "value": tanggalSettlementEndYear}
							);
						}
			
			            var statusInvoice = $('input[name=statusInvoice]').val();
						if(statusInvoice == '')statusInvoice='SEMUA';
						var chkStatus = $('input[name=chkStatusInvoice]').is(':checked');
			            if(statusInvoice && chkStatus) {
							aoData.push(
									{"name": 'statusInvoice', "value": statusInvoice},
									{"name": 'chkStatusInvoice', "value": true}
							);
						}
            
						var customerSPK = $('input[name=customerSPK]').val();
						if(customerSPK == '')customerSPK='SEMUA';
						var chkSPK = $('input[name=chkCustomerSPK]').is(':checked');
			            if(customerSPK && chkSPK) {
							aoData.push(
									{"name": 'customerSPK', "value": customerSPK},
									{"name": 'chkCustomerSPK', "value": true}
							);
						}
			
						var metodeBayar = $('input[name=metodeBayar]').val();
						if(metodeBayar == '')metodeBayar='SEMUA';
						var chkMetode = $('input[name=chkMetodeBayar]').is(':checked');
			            if(metodeBayar && chkMetode) {
							aoData.push(
									{"name": 'metodeBayar', "value": metodeBayar},
									{"name": 'chkMetodeBayar', "value": true}
							);
						}
			
						var nomorInvoice = $('input[name=nomorInvoice]').val();
						var chkInvoice = $('input[name=chkNomorInvoice]').is(':checked');
			            if(nomorInvoice && chkInvoice) {
							aoData.push(
									{"name": 'nomorInvoice', "value": nomorInvoice},
									{"name": 'chkNomorInvoice', "value": true}
							);
						}
			
						var tipeInvoice = $('input[name=tipeInvoice]').val();
						if(tipeInvoice == '')tipeInvoice='SEMUA';
						var chkTipe = $('input[name=chkTipeInvoice]').is(':checked');
			            if(tipeInvoice && chkTipe) {
							aoData.push(
									{"name": 'tipeInvoice', "value": tipeInvoice},
									{"name": 'chkTipeInvoice', "value": true}
							);
						}
			
						var customerName = $('input[name=customerName]').val();
						var chkCustomer = $('input[name=chkCustomerName]').is(':checked');
			            if(customerName && chkCustomer) {
							aoData.push(
									{"name": 'customerName', "value": customerName},
									{"name": 'chkCustomerName', "value": true}
							);
						}
			
						var startAging = $('input[name=startAging]').val();
						var endAging = $('input[name=endAging]').val();
						var chkAging = $('input[name=chkAging]').is(':checked');
			            if(startAging && endAging && chkAging) {
							aoData.push(
									{"name": 'startAging', "value": startAging},
									{"name": 'endAging', "value": endAging},
									{"name": 'chkAging', "value": true}
							);
						}
			
						var nomorPolisi = $('input[name=nomorPolisi]').val();
						var chkNomorPolisi = $('input[name=chkNomorPolisi]').is(':checked');
			            if(nomorPolisi && chkNomorPolisi) {
							aoData.push(
									{"name": 'nomorPolisi', "value": nomorPolisi},
									{"name": 'chkNomorPolisi', "value": true}
							);
						}
			
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							},
							"complete": function () {}
						});
					}
				});
			});
		</g:javascript>
	</head>
	<body>
		<div class="innerDetails">
			<table id="invoice_datatables_sub_${idTable}" cellpadding="0" cellspacing="0" 
				border="0"
				class="display table table-striped table-bordered table-hover"
				style="table-layout: fixed;"
				width="100%">
				<thead>
					<tr>
						<th>
							<div style="width: 264px;">&nbsp;</div>
						</th>							        
						<th style="border-bottom: none;">
			            	<div style="width: 125px;">Tgl Settlement</div>
						</th>			
						<th style="border-bottom: none;">
			            	<div style="width: 100px;">Metode Bayar</div>
						</th>
						<th style="border-bottom: none;">
			            	<div style="width: 125px;">Nama Bank</div>
						</th>
						<th style="border-bottom: none;">
			            	<div style="width: 125px;">Mesin EDC</div>
						</th>
						<th style="border-bottom: none;">
			            	<div style="width: 267px;">Nomor Kartu</div>
						</th>
						<th style="border-bottom: none;">
			            	<div style="width: 217px;">Bank Charge</div>
						</th>
						<th style="border-bottom: none;">
			            	<div style="width: 125px;">Nomor WBS</div>
						</th>
						<th style="border-bottom: none;">
			            	<div style="width: 125px;">Jumlah Settlement</div>
						</th>
					</tr>
				</thead>
			</table>
		</div>
	</body>
</html>
