<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
		<g:javascript>
		</g:javascript>
	</head>
	<body>
		<div class="box" style="padding-top: 10px; height: 450px; overflow: auto;">
		    <legend style="font-size: small;line-height: 15px; margin-bottom: 0;padding-bottom: 10px;">
                <table width="100%"><tr><td align="center"><h4>FAKTUR PAJAK</h4></td></tr></table>
		    </legend>
            <legend style="font-size: small;line-height: 15px; margin-bottom: 0; margin-top: 10px; padding-bottom: 10px;">
                <table width="100%">
                    <tr>
                        <td width="20%">Kode dan Nomor Seri Faktur Pajak</td>
                        <td width="1%">:</td>
                        <td width="79%">${data?.noFakturPajak}</td>
                    </tr>
                </table>
            </legend>
            <legend style="font-size: small;line-height: 15px; margin-bottom: 0; margin-top: 10px; padding-bottom: 10px;">
                <table width="100%">
                    <tr>
                        <td width="20%">Pengusaha Kena Pajak</td>
                        <td width="1%">:</td>
                        <td width="79%"></td>
                    </tr>
                    <tr>
                        <td width="20%">Nama</td>
                        <td width="1%">:</td>
                        <td width="79%"><b>${data?.namaPenjual}</b></td>
                    </tr>
                    <tr>
                        <td width="20%">Alamat</td>
                        <td width="1%">:</td>
                        <td width="79%">${data?.alamatWorkshop}</td>
                    </tr>
                    <tr>
                        <td width="20%">NPWP</td>
                        <td width="1%">:</td>
                        <td width="79%">${data?.npwpPenjual}</td>
                    </tr>
                    <tr>
                        <td width="20%">Tanggal Pengukuhan PKP</td>
                        <td width="1%">:</td>
                        <td width="79%">${data?.tanggalPKP}</td>
                    </tr>
                </table>
            </legend>
            <legend style="font-size: small;line-height: 15px; margin-bottom: 0; margin-top: 10px; padding-bottom: 10px;">
                <table width="100%">
                    <tr>
                        <td width="100%" colspan="3">Pembeli Barang Kena Pajak / Penerima Jasa Kena Pajak :</td>
                    </tr>
                    <tr>
                        <td width="20%">Nama</td>
                        <td width="1%">:</td>
                        <td width="79%">${data?.namaPembeli}</td>
                    </tr>
                    <tr>
                        <td width="20%">Alamat</td>
                        <td width="1%">:</td>
                        <td width="79%">${data?.alamatPembeli}</td>
                    </tr>
                    <tr>
                        <td width="20%">NPWP</td>
                        <td width="1%">:</td>
                        <td width="79%">${data?.npwpPembeli}</td>
                    </tr>
                    <tr>
                        <td colspan="3" width="100%">
                            <table width="100%" border="1">
                                <tr>
                                    <td width="15%" align="center" valign="midle">No Urut</td>
                                    <td width="65%" align="center" valign="midle">Nama Barang Kena Pajak / Jasa Kena Pajak</td>
                                    <td width="20%" align="center" valign="midle">Harga Jual/Penggantian/<br/>Uang Muka/Termijin (Rp)</td>
                                </tr>
                                <tr>
                                    <td width="15%" align="center" valign="top">1</td>
                                    <td width="65%" align="left" valign="top">BIAYA - JASA</td>
                                    <td width="20%" align="right" valign="top">${data?.biayaJasa}</td>
                                </tr>
                                <tr>
                                    <td width="15%" align="center" valign="top">2</td>
                                    <td width="65%" align="left" valign="top">BIAYA (PARTS/MATERIAL/OIL/SUBLET)<br/>SESUAI INVOICE NO : ${data?.invoice}</td>
                                    <td width="20%" align="right" valign="top">${data?.biayaGoods}</td>
                                </tr>
                                <tr>
                                    <td width="15%" align="center" valign="top">&nbsp;</td>
                                    <td width="65%" align="left" valign="top"></td>
                                    <td width="20%" align="right" valign="top"></td>
                                </tr>
                                <tr>
                                    <td width="15%" align="center" valign="top">&nbsp;</td>
                                    <td width="65%" align="left" valign="top"></td>
                                    <td width="20%" align="right" valign="top"></td>
                                </tr>
                                <tr>
                                    <td width="15%" align="center" valign="top">&nbsp;</td>
                                    <td width="65%" align="left" valign="top"></td>
                                    <td width="20%" align="right" valign="top"></td>
                                </tr>
                                <tr>
                                    <td width="15%" align="center" valign="top">&nbsp;</td>
                                    <td width="65%" align="left" valign="top"></td>
                                    <td width="20%" align="right" valign="top"></td>
                                </tr>
                                <tr>
                                    <td width="15%" align="center" valign="top">&nbsp;</td>
                                    <td width="65%" align="left" valign="top"></td>
                                    <td width="20%" align="right" valign="top"></td>
                                </tr>
                                <tr>
                                    <td width="15%" align="center" valign="top">&nbsp;</td>
                                    <td width="65%" align="left" valign="top"></td>
                                    <td width="20%" align="right" valign="top"></td>
                                </tr>
                                <tr>
                                    <td width="15%" align="center" valign="top">&nbsp;</td>
                                    <td width="65%" align="left" valign="top"></td>
                                    <td width="20%" align="right" valign="top"></td>
                                </tr>
                                <tr>
                                    <td width="15%" align="center" valign="top">&nbsp;</td>
                                    <td width="65%" align="left" valign="top"></td>
                                    <td width="20%" align="right" valign="top"></td>
                                </tr>
                                <tr>
                                    <td width="80%" colspan="2">Jumlah Harga Jual / Penggantian / Uang Muka / Termijin *)</td>
                                    <td width="20%" align="right">${data?.harga}</td>
                                </tr>
                                <tr>
                                    <td width="80%" colspan="2">Dikurangi Potongan Harga</td>
                                    <td width="20%" align="right">${data?.potongan}</td>
                                </tr>
                                <tr>
                                    <td width="80%" colspan="2">Dikurangi Uang Muka yang telah diterima</td>
                                    <td width="20%" align="right">${data?.uangMuka}</td>
                                </tr>
                                <tr>
                                    <td width="80%" colspan="2">Dasar Pengenaan Pajak</td>
                                    <td width="20%" align="right">${data?.dasarKenaPajak}</td>
                                </tr>
                                <tr>
                                    <td width="80%" colspan="2">PPN = 10 % x Dasar Pengenaan Pajak</td>
                                    <td width="20%" align="right">${data?.ppn}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" width="100%">
                            <table width="100%">
                                <tr>
                                    <td width="70%">Pajak Penjualan Atas Barang Mewah</td>
                                    <td width="30%" align="center">Jakarta, ${data?.tanggalFakturPajak}</td>
                                </tr>
                                <tr>
                                    <td width="70%">
                                        <table width="100%" border="1">
                                            <tr>
                                                <td width="30%" align="center">TARIF</td>
                                                <td width="30%" align="center">DPP</td>
                                                <td width="40%" align="center">PPn BM</td>
                                            </tr>
                                            <tr>
                                                <td width="30%" align="right">%</td>
                                                <td width="30%" align="center"></td>
                                                <td width="40%" align="left">Rp</td>
                                            </tr>
                                            <tr>
                                                <td width="30%" align="right">%</td>
                                                <td width="30%" align="center"></td>
                                                <td width="40%" align="left">Rp</td>
                                            </tr>
                                            <tr>
                                                <td width="30%" align="right">%</td>
                                                <td width="30%" align="center"></td>
                                                <td width="40%" align="left">Rp</td>
                                            </tr>
                                            <tr>
                                                <td width="30%" align="right">%</td>
                                                <td width="30%" align="center"></td>
                                                <td width="40%" align="left">Rp</td>
                                            </tr>
                                            <tr>
                                                <td width="30%" align="right">%</td>
                                                <td width="30%" align="center"></td>
                                                <td width="40%" align="left">Rp</td>
                                            </tr>
                                            <tr>
                                                <td width="60%" align="center" colspan="2">JUMLAH</td>
                                                <td width="40%" align="left">Rp</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="30%" align="center" valign="bottom">(${data?.namaPembeli})<br/>Nama</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </legend>
            * Coret yang tidak perlu
		</div>
	</body>
</html>