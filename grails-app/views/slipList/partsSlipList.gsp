<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'delivery.slipList.label', default: 'Delivery - Slip')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
			$(function() {												
				clearData = function() {
					$('#search-table').find(':input').each(function() {
						switch(this.type) {
							case 'password':
							case 'select-multiple':
							case 'select-one':
							case 'text':
							case 'hidden':
							case 'textarea':
								$(this).val('');
							    break;
							case 'checkbox':
							case 'radio':
								this.checked = false;
						}
					});
				};
				
				closeData = function() {
					var url = '${request.contextPath}/#/home';    
					$(location).attr('href',url);
				};

				printData = function(){
                    alert("Untuk Cetak partSlip, Harap Ke menu Invoicing");
				    %{--var row = 0;--}%
				    %{--var rowSelect = 0;--}%
                    %{--$("#parts_datatables_${idTable} tbody .row-select").each(function() {--}%
                         %{--if(this.checked){--}%
                            %{--var id = $(this).next("input:hidden").val();--}%
                            %{--doPrint(id);--}%
                            %{--rowSelect=rowSelect+1;--}%
                         %{--}--}%
                         %{--row=row+1;--}%
                    %{--});--}%
                    %{--if(row==0){--}%
                        %{--alert('Data tidak ditemukan');--}%
                        %{--return false;--}%
                    %{--}else{--}%
                        %{--if(rowSelect==0)alert('Silahkan pilih terlebih dahulu');--}%
                        %{--return false;--}%
                    %{--}--}%
				};

				doPrint = function(refNo){
				    if(refNo){
                        window.location = "${request.contextPath}/slipList/printPartsSlip?id="+refNo;
                    }
				}
				
				previewData = function() {
                    var row = 0;
				    var rowSelect = 0;
                    $("#parts_datatables_${idTable} tbody .row-select").each(function() {
                         if(this.checked){
                            var id = $(this).next("input:hidden").val();
                            doPreview(id);
                            rowSelect=rowSelect+1;
                         }
                         row=row+1;
                    });
                    if(row==0){
                        alert('Data tidak ditemukan');
                        return false;
                    }else{
                        if(rowSelect==0)alert('Silahkan pilih terlebih dahulu');
                        return false;
                    }
	            };

	            doPreview = function(a) {
	                $("#partsSlipListPreviewContent").empty();
	                $.ajax({
						type: 'POST',
						url: '${request.contextPath}/slipList/previewPartsSlip',
						data:{noPartsSlip:a},
	                    success: function (data) {
	                        $("#partsSlipListPreviewContent").html(data);
	                        $("#partsSlipListPreviewModal").modal({
	                            "backdrop": "static",
	                            "keyboard": true,
	                            "show": true
	                        }).css({
								'width': '1200px',
								'margin-left': function () {
	                            	return - ( $(this).width() / 2 );
	                            }
							});
	                    },
	                    error: function () {
	                        alert('Data not found');
	                        return false;
	                    },
	                    complete: function () {
	                        $('#spinner').fadeOut();
	                    }
	                });
	            };
			});
		</g:javascript>
	</head>
	<body>
		<div class="navbar box-header no-border">
			<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>		    
		</div>
		<div>&nbsp;</div>
		<div class="box">
			<!-- Start Tab -->
		    <ul class="nav nav-tabs" style="margin-bottom: 0;">
		        <li><a href="javascript:loadPath('slipList/invoiceList');">Invoice List</a></li>
		        <li class="active"><a href="#">Parts Slip List</a></li>
		        <li><a href="javascript:loadPath('slipList/fakturPajakList');">Faktur Pajak List</a></li>
		        <li><a href="javascript:loadPath('slipList/notaReturList');">Nota Retur List</a></li>
		        <li><a href="javascript:loadPath('slipList/kuitansiList');">Kuitansi List</a></li>
				<li><a href="javascript:loadPath('slipList/refundFormList');">Refund Form List</a></li>
		    </ul>
			<!-- End Tab -->
			
			<!-- Start Kriteria Search -->
			<div class="box" style="padding-top: 5px; padding-left: 10px;">
				<div class="span12">
                    <legend style="font-size: small;">
					    <g:message code="delivery.slipList.kriteria.search.parts.label" default="Kriteria Search Parts Slip"/>
                    </legend>
					<g:if test="${flash.message}">
						<div class="alert alert-error">
							${flash.message}
						</div>
					</g:if>
				</div>
				<div class="span12" id="search-table" style="padding-left: 0;">
					<table width="47%;">
						<tr align="left">
							<td align="left">
		                    	<label class="control-label">
									<g:message code="delivery.slipList.tanggal.parts.label" default="Tanggal Parts Slip"/>
		                    	</label>	
							</td>
							<td align="left">
								<input type="checkbox" name="chkTanggalParts"/>&nbsp;
							</td>
							<td align="left">
								<ba:datePicker id="tanggalParts_start" name="tanggalParts_start" precision="day" format="dd/MM/yyyy"  value="" />
							</td>
							<td align="left">
		                    	<label class="control-label">
									&nbsp;<g:message code="delivery.slipList.until.label" default="s/d"/>&nbsp;
		                    	</label>
							</td>
							<td align="left">
								<ba:datePicker id="tanggalParts_end" name="tanggalParts_end" precision="day" format="dd/MM/yyyy"  value="" />
							</td>							
						</tr>
						<tr align="left">
							<td align="left">
		                    	<label class="control-label">
									<g:message code="delivery.slipList.nomor.parts.label" default="Nomor Parts Slip"/>
		                    	</label>	
							</td>
							<td align="left">
								<input type="checkbox" name="chkNomorParts"/>&nbsp;
							</td>
							<td align="left" colspan="3">
								<g:textField name="nomorParts" id="nomorParts" style="width:97%"/>
							</td>							
						</tr>
						<tr align="left">
							<td align="left">
		                    	<label class="control-label">
									<g:message code="delivery.slipList.nomor.wo.label" default="Nomor WO"/>
		                    	</label>	
							</td>
							<td align="left">
								<input type="checkbox" name="chkNomorWO"/>&nbsp;
							</td>
							<td align="left" colspan="3">
								<g:textField name="nomorWO" id="nomorWO" style="width:97%"/>
							</td>							
						</tr>
						<tr align="left">
							<td align="left">
		                    	<label class="control-label">
									<g:message code="delivery.slipList.nomor.polisi.label" default="Nomor Polisi"/>
		                    	</label>	
							</td>
							<td align="left">
								<input type="checkbox" name="chkNomorPolisi"/>&nbsp;
							</td>
							<td align="left" colspan="3">
								<g:textField name="nomorPolisi" id="nomorPolisi" style="width:97%"/>
							</td>							
						</tr>						
					</table>
				</div>
				<div class="span12" style="padding-left: 0;">
                    <g:field type="button" style="width: 90px" class="btn btn-primary search" onclick="searchData();"
                             name="search" id="search" value="${message(code: 'default.search.label', default: 'Search')}" />
                    <g:field type="button" style="width: 90px" class="btn btn-primary clear" onclick="clearData();"
                             name="clear" id="clear" value="${message(code: 'default.clear.label', default: 'Clear')}" />
				</div>
			</div>
			<!-- End Kriteria Search -->
			
			<!-- Start Data List -->
			<div class="box" style="padding-top: 5px; padding-left: 10px;">
				<div class="span12" style="height: 40px;">
                    <legend style="font-size: small;">
					    <g:message code="delivery.slipList.parts.list.label" default="Parts Slip List"/>
                    </legend>
				</div>
				<div class="span12" id="user-table" style="padding-left: 0; margin-left: 0;">
					<g:render template="partsSlipDataTables"/>
					<br/>
	                <g:field type="button" style="width: 90px" class="btn btn-primary search" onclick="printData();"
	                             name="print" id="print" value="${message(code: 'default.print.label', default: 'Print')}" />
	                <g:field type="button" style="width: 90px" class="btn btn-primary clear" onclick="previewData();"
	                             name="preview" id="preview" value="${message(code: 'default.preview.label', default: 'Preview')}" />
				</div>
			</div>
			<!-- End Data List -->
		</div>		
		<ul class="nav pull-right">
            <g:field type="button" style="width: 90px" class="btn btn-primary" onclick="closeData();"
                     name="close" id="close" value="${message(code: 'default.close.label', default: 'Close')}" />
		</ul>
		
		<!-- Start Modal Preview -->
		<div id="partsSlipListPreviewModal" class="modal fade">
		    <div class="modal-dialog" style="width: 1200px;">
		        <div class="modal-content" style="width: 1200px;">
		            <!-- dialog body -->
		            <div class="modal-body" style="max-height: 1200px;">
		                <div id="partsSlipListPreviewContent"></div>
		                <div class="iu-content"></div>
		            </div>
		            <!-- dialog buttons -->
		            <div class="modal-footer">
			            <g:field type="button" style="width: 90px" class="btn btn-primary" name="close_modal" id="close_modal"
						value="${message(code: 'default.close.label', default: 'Close')}" data-dismiss="modal"/>
		            </div>
		        </div>
		    </div>
		</div>			
		<!-- End Modal Preview -->
	</body>
</html>