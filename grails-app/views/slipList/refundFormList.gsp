<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'delivery.slipList.label', default: 'Delivery - Slip')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript disposition="head">
		    $(function() {

				changeJenisRefund = function() {
					var jenisRefund = $('select[name=jenisRefundSelect] option:selected').val();
					$('input[name=jenisRefund]').val(jenisRefund);
				};
				
				changeMetodeRefund = function() {
					var metodeRefund = $('select[name=metodeRefundSelect] option:selected').val();
					$('input[name=metodeRefund]').val(metodeRefund);
				};
				
				clearData = function() {
					$('#search-table').find(':input').each(function() {
						switch(this.type) {
							case 'password':
							case 'select-multiple':
							case 'select-one':
							case 'text':
							case 'hidden':
							case 'textarea':
								$(this).val('');
							    break;
							case 'checkbox':
							case 'radio':
								this.checked = false;
						}
					});
				};
				
				closeData = function() {
					var url = '${request.contextPath}/#/home';    
					$(location).attr('href',url);
				};

				printData = function() {
				    var row = 0;
				    var rowSelect = 0;
				    $("#refund_form_datatables_${idTable} tbody .row-select").each(function() {
				        if(this.checked){
				            var id = $(this).next("input:hidden").val();
                            doPrint(id);
                            rowSelect=rowSelect+1;
				        }
				        row=row+1;
				    });
				    if(row==0){
                        alert('Data tidak ditemukan');
                        return false;
                    }else{
                        if(rowSelect==0)alert('Silahkan pilih terlebih dahulu');
                        return false;
                    }
				};

				doPrint = function(refNo) {
                    if(refNo){
                        window.location = "${request.contextPath}/refund/print?nomorRefund="+refNo;
                    }
                };
				
				previewData = function() {
				    var row = 0;
				    var rowSelect = 0;
				    $("#refund_form_datatables_${idTable} tbody .row-select").each(function() {
                         if(this.checked){
                            var id = $(this).next("input:hidden").val();
                            doPreview(id);
                            rowSelect=rowSelect+1;
                         }
                         row=row+1;
                    });
                    if(row==0){
                        alert('Data tidak ditemukan');
                        return false;
                    }else{
                        if(rowSelect==0)alert('Silahkan pilih terlebih dahulu');
                        return false;
                    }
				};

				doPreview = function(a) {
	                $("#refundFormListPreviewContent").empty();
	                $.ajax({
						type: 'POST', 
						url: '${request.contextPath}/slipList/previewRefundForm',
						data:{nomorRefund:a},
	                    success: function (data) {
                            $("#refundFormListPreviewContent").html(data);
                            $("#refundFormListPreviewModal").modal({
                                "backdrop": "static",
                                "keyboard": true,
                                "show": true
                            }).css({
                                'width': '1200px',
                                'margin-left': function () {
                                    return - ( $(this).width() / 2 );
                                }
                            });
	                    },
	                    error: function () {
	                        alert('Data not found');
                            return false;
	                    },
	                    complete: function () {
	                        $('#spinner').fadeOut();
	                    }
	                });	                
	            };
			});
		</g:javascript>
	</head>
	<body>
		<div class="navbar box-header no-border">
			<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>		    
		</div>
		<div>&nbsp;</div>
		<div class="box">
			<!-- Start Tab -->
		    <ul class="nav nav-tabs" style="margin-bottom: 0;">
		        <li><a href="javascript:loadPath('slipList/invoiceList');">Invoice List</a></li>
		        <li><a href="javascript:loadPath('slipList/partsSlipList');">Parts Slip List</a></li>
		        <li><a href="javascript:loadPath('slipList/fakturPajakList');">Faktur Pajak List</a></li>
		        <li><a href="javascript:loadPath('slipList/notaReturList');">Nota Retur List</a></li>
		        <li><a href="javascript:loadPath('slipList/kuitansiList');">Kuitansi List</a></li>
				<li class="active"><a href="#">Refund Form List</a></li>
		    </ul>
			<!-- End Tab -->
			
			<!-- Start Kriteria Search -->
			<div class="box" style="padding-top: 5px; padding-left: 10px;">
				<div class="span12">
                    <legend style="font-size: small;">
					    <g:message code="delivery.slipList.kriteria.search.refund.label" default="Kriteria Search Refund Form"/>
                    </legend>
					<g:if test="${flash.message}">
						<div class="alert alert-error">
							${flash.message}
						</div>
					</g:if>
				</div>
				<div class="span12" id="search-table" style="padding-left: 0;">
					<table width="47%;">
						<tr align="left">
							<td align="left">
		                    	<label class="control-label">
									<g:message code="delivery.slipList.tanggal.refund.label" default="Tanggal Refund"/>
		                    	</label>	
							</td>
							<td align="left">
								<input type="checkbox" name="chkTanggalRefund"/>&nbsp;
							</td>
							<td align="left">
								<ba:datePicker id="tanggalRefund_start" name="tanggalRefund_start" precision="day" format="dd/MM/yyyy"  value="" />
							</td>
							<td align="left">
		                    	<label class="control-label">
									&nbsp;<g:message code="delivery.slipList.until.label" default="s/d"/>&nbsp;
		                    	</label>
							</td>
							<td align="left">
								<ba:datePicker id="tanggalRefund_end" name="tanggalRefund_end" precision="day" format="dd/MM/yyyy"  value="" />
							</td>							
						</tr>
						<tr align="left">
							<td align="left">
		                    	<label class="control-label">
									<g:message code="delivery.slipList.jenis.refund.label" default="Jenis Refund"/>
		                    	</label>	
							</td>
							<td align="left">
								<input type="checkbox" name="chkJenisRefund"/>&nbsp;
							</td>
							<td align="left" colspan="3">
								<g:select style="width:100%" name="jenisRefundSelect" id="jenisRefundSelect" from="${jenisRefund}" optionKey="tipeKey" optionValue="tipeValue" noSelection="${['SEMUA':'SEMUA']}" onchange="changeJenisRefund();"/>
								<input type="hidden" id="jenisRefund" name="jenisRefund" value="SEMUA">
							</td>							
						</tr>
						<tr align="left">
							<td align="left">
		                    	<label class="control-label">
									<g:message code="delivery.slipList.metode.refund.label" default="Metode Refund"/>
		                    	</label>	
							</td>
							<td align="left">
								<input type="checkbox" name="chkMetodeRefund"/>&nbsp;
							</td>
							<td align="left" colspan="3">
								<g:select style="width:100%" name="metodeRefundSelect" id="metodeRefundSelect" from="${metodeRefund}" optionKey="tipeKey" optionValue="tipeValue" noSelection="${['SEMUA':'SEMUA']}" onchange="changeMetodeRefund();"/>
								<input type="hidden" id="metodeRefund" name="metodeRefund" value="SEMUA">
							</td>							
						</tr>						
						<tr align="left">
							<td align="left">
		                    	<label class="control-label">
									<g:message code="delivery.slipList.nomor.refund.label" default="Nomor Refund Form"/>
		                    	</label>	
							</td>
							<td align="left">
								<input type="checkbox" name="chkNomorRefund"/>&nbsp;
							</td>
							<td align="left" colspan="3">
								<g:textField name="nomorRefund" id="nomorRefund" style="width:97%"/>
							</td>							
						</tr>																	
					</table>
				</div>
				<div class="span12" style="padding-left: 0;">
                    <g:field type="button" style="width: 90px" class="btn btn-primary search" onclick="searchData();"
                             name="search" id="search" value="${message(code: 'default.search.label', default: 'Search')}" />
                    <g:field type="button" style="width: 90px" class="btn btn-primary clear" onclick="clearData();"
                             name="clear" id="clear" value="${message(code: 'default.clear.label', default: 'Clear')}" />
				</div>
			</div>
			<!-- End Kriteria Search -->
			
			<!-- Start Data List -->
			<div class="box" style="padding-top: 5px; padding-left: 10px;">
				<div class="span12" style="height: 40px;">
                    <legend style="font-size: small;">
					    <g:message code="delivery.slipList.refund.list.label" default="Refund Form List"/>
                    </legend>
				</div>
				<div class="span12" style="padding-left: 0; margin-left: 0;">
					<g:render template="refundFormDataTables"/>
					<br/>
	                <g:field type="button" style="width: 90px" class="btn btn-primary search" onclick="printData();"
	                             name="print" id="print" value="${message(code: 'default.print.label', default: 'Print')}" />
	                <g:field type="button" style="width: 90px" class="btn btn-primary clear" onclick="previewData();"
	                             name="preview" id="preview" value="${message(code: 'default.preview.label', default: 'Preview')}" />
				</div>
			</div>
			<!-- End Data List -->
		</div>		
		<ul class="nav pull-right">
            <g:field type="button" style="width: 90px" class="btn btn-primary" onclick="closeData();"
                     name="close" id="close" value="${message(code: 'default.close.label', default: 'Close')}" />
		</ul>
		
		<!-- Start Modal Preview -->
		<div id="refundFormListPreviewModal" class="modal fade">
		    <div class="modal-dialog" style="width: 1200px;">
		        <div class="modal-content" style="width: 1200px;">
		            <!-- dialog body -->
		            <div class="modal-body" style="max-height: 1200px;">
		                <div id="refundFormListPreviewContent"></div>
		                <div class="iu-content"></div>
		            </div>
		            <!-- dialog buttons -->
		            <div class="modal-footer">
			            <g:field type="button" style="width: 90px" class="btn btn-primary" name="close_modal" id="close_modal"
						value="${message(code: 'default.close.label', default: 'Close')}" data-dismiss="modal"/>
		            </div>
		        </div>
		    </div>
		</div>			
		<!-- End Modal Preview -->
	</body>
</html>