<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
		<g:javascript>
		</g:javascript>
	</head>
	<body>
		<div class="box">
		    <legend style="font-size: small;line-height: 15px;">
                <table width="100%">
                    <tr>
                        <td width="7%" rowspan="4" valign="top" align="center"><img src="${request.contextPath}/images/tam.jpg"></td>
                        <td width="43%" colspan="2" align="left"><h4>PT TOYOTA - ASTRA MOTOR</h4></td>
                        <td width="15%">&nbsp;</td>
                        <td width="35%" align="center">No ISO FORM-001/PRO-FINTAM-001</td>
                    </tr>
                    <tr>
                        <td width="43%" colspan="2" align="left">Jl. Jenderal Sudirman No.5</td>
                        <td width="15%">&nbsp;</td>
                        <td width="35%" rowspan="6" style="border: 1px solid black;">
                            <table width="100%">
                                <tr><td align="center"><b style="text-decoration: underline;">PERHATIAN</b></td></tr>
                                <tr><td align="center">SESUAI DENGAN PROSEDUR ADMINISTRASI</td></tr>
                                <tr><td align="center">BENGKEL, SETIAP PEMBATALAN ATAU</td></tr>
                                <tr><td align="center">PENGEMBALIAN SELISIH DOWN PAYMENT</td></tr>
                                <tr><td align="center">DIBAYARKAN MIN 2 MINGGU SETELAH</td></tr>
                                <tr><td align="center">TRANSAKSI</td></tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="43%" colspan="2">Karet Tengsing, Tanah Abang</td>
                        <td width="15%" rowspan="2" align="center" valign="bottom"><h4>KUITANSI</h4></td>
                    </tr>
                    <tr>
                        <td width="43%" colspan="2">Jakarta Pusat 10220</td>
                    </tr>
                    <tr>
                        <td width="7%">&nbsp;</td>
                        <td width="3%">Telp.</td>
                        <td width="40%">(021) 6515551</td>
                        <td width="15%" align="center">(${data?.jenisBayar})</td>
                    </tr>
                    <tr>
                        <td width="7%">&nbsp;</td>
                        <td width="3%">Fax.</td>
                        <td width="40%">(021) 6512238</td>
                        <td width="15%">&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="7%">&nbsp;</td>
                        <td width="3%">&nbsp;</td>
                        <td width="40%">&nbsp;</td>
                        <td width="15%">&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="10%" colspan="2" align="right">Nomor WO :</td>
                        <td width="40%" align="left">${data?.nomorWO}</td>
                        <td width="15%">&nbsp;</td>
                        <td width="35%" align="center">No. ${data?.nomorKuitansi}</td>
                    </tr>
                </table>
                <br/>
		    </legend>
            <table width="100%">
                <tr>
                    <td width="30%">Telah diterima dari<br/>(Received from)</td>
                    <td width="70%">${data?.terimaDari}</td>
                </tr>
                <tr>
                    <td width="30%">Terbilang<br/>(In Word)</td>
                    <td width="70%">${data?.terbilang}</td>
                </tr>
                <tr>
                    <td width="30%">Untuk Pembayaran<br/>(Payment detail)</td>
                    <td width="70%">${data?.pembayaran} / ${data?.nomorPolisi} / ${data?.metodeBayar}</td>
                </tr>
                <tr>
                    <td width="30%">Uang Sejumlah<br/>(In Amount)</td>
                    <td width="70%">Rp. ${data?.jumlah}</td>
                </tr>
            </table>
            <br/>
            <table width="100%">
                <tr>
                    <td width="30%" colspan="3">Printed Date : ${data?.printDate}</td>
                    <td width="40%" colspan="2">${data?.namaWorkshop}</td>
                    <td width="30%" align="center">Jakarta, ${data?.tanggalKuitansi}</td>
                </tr>
                <tr>
                    <td width="30%" colspan="3">&nbsp;</td>
                    <td width="40%" colspan="2">${data?.alamatWorkshop}</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="30%" colspan="3">&nbsp;</td>
                    <td width="3%">Telp.</td>
                    <td width="37%">${data?.teleponWorkshop}, Ext. ${data?.extWorkshop}</td>
                    <td width="30%">&nbsp;</td>
                </tr>
                <tr>
                    <td width="10%">Distribusi :</td>
                    <td width="10%">Putih dan Biru</td>
                    <td width="10%">-> Customer</td>
                    <td width="3%">&nbsp;</td>
                    <td width="37%">${data?.teleponWorkshop} (Direct)</td>
                    <td width="30%" align="center" style="text-decoration: underline;">${data?.kasir}</td>
                </tr>
                <tr>
                    <td width="10%">&nbsp;</td>
                    <td width="10%">Kuning</td>
                    <td width="10%">-> Divisi User</td>
                    <td width="3%">Fax.</td>
                    <td width="37%">${data?.faxWorkshop}</td>
                    <td width="30%" align="center">CASHIER</td>
                </tr>
                <tr>
                    <td width="10%">&nbsp;</td>
                    <td width="10%">Merah</td>
                    <td width="10%">-> Finance</td>
                    <td width="3%">&nbsp;</td>
                    <td width="37%">&nbsp;</td>
                    <td width="30%">&nbsp;</td>
                </tr>
            </table>
		</div>
	</body>
</html>