<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'delivery.slipList.label', default: 'Delivery - Slip')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">			
			$(function() {
				changeStatusInvoice = function() {
					var statusInvoice = $('select[name=statusInvoiceSelect] option:selected').val();
					$('input[name=statusInvoice]').val(statusInvoice);
				};
				
				changeCustomerSPK = function() {
					var statusSPK = $('select[name=customerSPKSelect] option:selected').val();
					$('input[name=customerSPK]').val(statusSPK);
				};
				
				changeMetodeBayar = function() {
					var metodeBayar = $('select[name=metodeBayarSelect] option:selected').val();
					$('input[name=metodeBayar]').val(metodeBayar);
				};
				
				changeTipeInvoice = function() {
					var tipeInvoice = $('select[name=tipeInvoiceSelect] option:selected').val();
					$('input[name=tipeInvoice]').val(tipeInvoice);
				};
				
				clearData = function() {
					$('#search-table').find(':input').each(function() {
						switch(this.type) {
							case 'password':
							case 'select-multiple':
							case 'select-one':
							case 'text':
							case 'hidden':
							case 'textarea':
								$(this).val('');
							    break;
							case 'checkbox':
							case 'radio':
								this.checked = false;
						}
					});
				};
				
				closeData = function() {
					var url = '${request.contextPath}/#/home';    
					$(location).attr('href',url);
				};

				printData = function() {
				    var row = 0;
				    var rowSelect = 0;
                    $("#invoice_datatables_${idTable} tbody .row-select").each(function() {
                         if(this.checked){
                            var id = $(this).next("input:hidden").val();
                            doPrint(id);
                            rowSelect=rowSelect+1;
                         }
                         row=row+1;
                    });
                    if(row==0){
                        alert('Data tidak ditemukan');
                        return false;
                    }else{
                        if(rowSelect==0)alert('Silahkan pilih terlebih dahulu');
                        return false;
                    }
                    //doPrint('12345');
				}

				doPrint = function(refNo){
				    if(refNo){
                        window.location = "${request.contextPath}/slipList/printInvoice?id="+refNo;
                    }
				}
				
				previewData = function() {
				    var row = 0;
				    var rowSelect = 0;
                    $("#invoice_datatables_${idTable} tbody .row-select").each(function() {
                         if(this.checked){
                            var id = $(this).next("input:hidden").val();
                            doPreview(id);
                            rowSelect=rowSelect+1;
                         }
                         row=row+1;
                    });
                    if(row==0){
                        alert('Data tidak ditemukan');
                        return false;
                    }else{
                        if(rowSelect==0)alert('Silahkan pilih terlebih dahulu');
                        return false;
                    }
	            };

	            doPreview = function(a) {
	                $("#invoiceListPreviewContent").empty();
	                $.ajax({
						type: 'POST',
						url: '${request.contextPath}/slipList/previewInvoice',
						data: {noInvoice:a},
	                    success: function (data) {
	                        $("#invoiceListPreviewContent").html(data);
	                        $("#invoiceListPreviewModal").modal({
	                            "backdrop": "static",
	                            "keyboard": true,
	                            "show": true
	                        }).css({
								'width': '1200px',
								'margin-left': function () {
	                            	return - ( $(this).width() / 2 );
	                            }
							});
	                    },
	                    error: function () {
	                        alert('Data not found');
                            return false;
	                    },
	                    complete: function () {
	                        $('#spinner').fadeOut();
	                    }
	                });
	            };
			});
		</g:javascript>
	</head>
	<body>
		<div class="navbar box-header no-border">
			<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>		    
		</div>
		<div>&nbsp;</div>
		<div class="box">
			
			<!-- Start Tab -->
		    <ul class="nav nav-tabs" style="margin-bottom: 0;">
		        <li class="active"><a href="#">Invoice List</a></li>
		        <li><a href="javascript:loadPath('slipList/partsSlipList');">Parts Slip List</a></li>
		        <li><a href="javascript:loadPath('slipList/fakturPajakList');">Faktur Pajak List</a></li>
		        <li><a href="javascript:loadPath('slipList/notaReturList');">Nota Retur List</a></li>
		        <li><a href="javascript:loadPath('slipList/kuitansiList');">Kuitansi List</a></li>
				<li><a href="javascript:loadPath('slipList/refundFormList');">Refund Form List</a></li>
		    </ul>
			<!-- End Tab -->
			
			<!-- Start Kriteria Search -->
			<div class="box" style="padding-top: 5px; padding-left: 10px;">
				<div class="span12">
					<legend style="font-size: small;">
						<g:message code="delivery.slipList.kriteria.search.label" default="Kriteria Search Invoice"/>
					</legend>
					<g:if test="${flash.message}">
						<div class="alert alert-error">
							${flash.message}
						</div>
					</g:if>
				</div>
				
				<div class="span12" id="search-table" style="padding-left: 0;">
					<table width="95%;">
						<tr align="left">
							<td align="left">
		                    	<label class="control-label">
									<g:message code="delivery.slipList.tanggal.invoice.label" default="Tanggal Invoice"/>
		                    	</label>	
							</td>
							<td align="left">
								<input type="checkbox" name="chkTanggalInvoice"/>&nbsp;
							</td>
							<td align="left">
								<ba:datePicker id="tanggalInvoice_start" name="tanggalInvoice_start" precision="day" format="dd/MM/yyyy"  value="" />
							</td>
							<td align="left">
		                    	<label class="control-label">
									&nbsp;<g:message code="delivery.slipList.until.label" default="s/d"/>&nbsp;
		                    	</label>
							</td>
							<td align="left">
								<ba:datePicker id="tanggalInvoice_end" name="tanggalInvoice_end" precision="day" format="dd/MM/yyyy"  value="" />
							</td>
							<td>&nbsp;</td>
							<td align="left">
		                    	<label class="control-label">
									<g:message code="delivery.slipList.tanggal.settlement.label" default="Tanggal Settlement"/>
		                    	</label>
							</td>
							<td align="left">
								<input type="checkbox" name="chkTanggalSettlement"/>&nbsp;
							</td>
							<td align="left">
								<ba:datePicker id="tanggalSettlement_start" name="tanggalSettlement_start" precision="day" format="dd/MM/yyyy"  value="" />
							</td>
							<td align="left">
		                    	<label class="control-label">
									&nbsp;<g:message code="delivery.slipList.until.label" default="s/d"/>&nbsp;
		                    	</label>
							</td>
							<td align="left">
								<ba:datePicker id="tanggalSettlement_end" name="tanggalSettlement_end" precision="day" format="dd/MM/yyyy"  value="" />
							</td>
						</tr>
						<tr align="left">
							<td align="left">
		                    	<label class="control-label">
									<g:message code="delivery.slipList.status.invoice.label" default="Status Invoice"/>
		                    	</label>	
							</td>
							<td align="left">
								<input type="checkbox" name="chkStatusInvoice"/>&nbsp;
							</td>
							<td align="left" colspan="3">
								<g:select style="width:100%" name="statusInvoiceSelect" id="statusInvoiceSelect" onchange="changeStatusInvoice();" from="${['SEMUA','CLOSE','OPEN']}"/>
								<input type="hidden" id="statusInvoice" name="statusInvoice" value="SEMUA">
							</td>
							<td>&nbsp;</td>
							<td align="left">
		                    	<label class="control-label">
									<g:message code="delivery.slipList.customer.spk.label" default="Customer SPK ?"/>
		                    	</label>	
							</td>
							<td align="left">
								<input type="checkbox" name="chkCustomerSPK"/>&nbsp;
							</td>
							<td align="left" colspan="3">
								<g:select style="width:100%" name="customerSPKSelect" id="customerSPKSelect" onchange="changeCustomerSPK();" from="${customerSPK}" optionKey="tipeKey" optionValue="tipeValue" noSelection="${['SEMUA':'SEMUA']}"/>
								<input type="hidden" id="customerSPK" name="customerSPK" value="SEMUA">
							</td>
						</tr>
						<tr align="left">
							<td align="left">
		                    	<label class="control-label">
									<g:message code="delivery.slipList.metode.bayar.label" default="Metode Bayar"/>
		                    	</label>	
							</td>
							<td align="left">
								<input type="checkbox" name="chkMetodeBayar"/>&nbsp;
							</td>
							<td align="left" colspan="3">
								<g:select style="width:100%" name="metodeBayarSelect" id="metodeBayarSelect" onchange="changeMetodeBayar();" from="${metodeBayar}" optionKey="m701Id" optionValue="m701MetodeBayar" noSelection="${['SEMUA':'SEMUA']}"/>
								<input type="hidden" id="metodeBayar" name="metodeBayar" value="SEMUA">
							</td>
							<td>&nbsp;</td>
							<td align="left">
		                    	<label class="control-label">
									<g:message code="delivery.slipList.nomor.invoice.label" default="Nomor Invoice"/>
		                    	</label>	
							</td>
							<td align="left">
								<input type="checkbox" name="chkNomorInvoice"/>&nbsp;
							</td>
							<td align="left" colspan="3">
								<g:textField name="nomorInvoice" id="nomorInvoice" style="width:97%"/>
							</td>
						</tr>
						<tr align="left">
							<td align="left">
		                    	<label class="control-label">
									<g:message code="delivery.slipList.tipe.invoice.label" default="Tipe Invoice"/>
		                    	</label>	
							</td>
							<td align="left">
								<input type="checkbox" name="chkTipeInvoice"/>&nbsp;
							</td>
							<td align="left" colspan="3">
								<g:select style="width:100%" name="tipeInvoiceSelect" id="tipeInvoiceSelect" onchange="changeTipeInvoice();" from="${tipeInvoice}" optionKey="tipeKey" optionValue="tipeValue" noSelection="${['SEMUA':'SEMUA']}"/>
								<input type="hidden" id="tipeInvoice" name="tipeInvoice" value="SEMUA">
							</td>
							<td>&nbsp;</td>
							<td align="left">
		                    	<label class="control-label">
									<g:message code="delivery.slipList.customer.name.label" default="Nama Customer"/>
		                    	</label>	
							</td>
							<td align="left">
								<input type="checkbox" name="chkCustomerName"/>&nbsp;
							</td>
							<td align="left" colspan="3">
								<g:textField name="customerName" id="customerName" style="width:97%"/>
							</td>
						</tr>
						<tr align="left">
							<td align="left">
		                    	<label class="control-label">
									<g:message code="delivery.slipList.aging.label" default="Aging"/>
		                    	</label>	
							</td>
							<td align="left">
								<input type="checkbox" name="chkAging"/>&nbsp;
							</td>
							<td align="left">
								<g:textField name="startAging" id="startAging"/>
							</td>
							<td align="left">
		                    	<label class="control-label">
									&nbsp;<g:message code="delivery.slipList.until.label" default="s/d"/>&nbsp;
		                    	</label>
							</td>
							<td align="left">
								<g:textField name="endAging" id="endAging"/>
							</td>
							<td>&nbsp;</td>
							<td align="left">
		                    	<label class="control-label">
									<g:message code="delivery.slipList.nomor.polisi.label" default="Nomor Polisi"/>
		                    	</label>
							</td>
							<td align="left">
								<input type="checkbox" name="chkNomorPolisi"/>&nbsp;
							</td>
							<td align="left" colspan="3">
								<g:textField name="nomorPolisi" id="nomorPolisi" style="width:97%"/>
							</td>							
						</tr>
					</table>
				</div>
				<div class="span12" style="padding-left: 0;">
                    <g:field type="button" style="width: 90px" class="btn btn-primary search" onclick="searchData();"
                             name="search" id="search" value="${message(code: 'default.search.label', default: 'Search')}" />
                    <g:field type="button" style="width: 90px" class="btn clear" onclick="clearData();"
                             name="clear" id="clear" value="${message(code: 'default.clear.label', default: 'Clear')}" />
				</div>
			</div>
			<!-- End Kriteria Search -->
			
			<!-- Start Data List -->
			<div class="box" style="padding-top: 5px; padding-left: 10px;">
				<div class="span12" style="height: 40px;">
                    <legend style="font-size: small;">
					    <g:message code="delivery.slipList.invoice.list.label" default="Invoice List"/>
                    </legend>
				</div>
				<div class="span12" id="user-table" style="padding-left: 0; margin-left: 0;">
					<g:render template="invoiceDataTables"/>
					<br/>
	                <g:field type="button" style="width: 90px" class="btn btn-primary search" onclick="printData();"
	                             name="print" id="print" value="${message(code: 'default.print.label', default: 'Print')}" />
	                <g:field type="button" style="width: 90px" class="btn btn-primary clear" onclick="previewData();"
	                             name="preview" id="preview" value="${message(code: 'default.preview.label', default: 'Preview')}" />
				</div>
			</div>
			<!-- End Data List -->			
		</div>
				
		<ul class="nav pull-right">
            <g:field type="button" style="width: 90px" class="btn btn-primary" onclick="closeData();"
                     name="close" id="close" value="${message(code: 'default.close.label', default: 'Close')}" />
		</ul>
		
		<!-- Start Modal Preview -->
		<div id="invoiceListPreviewModal" class="modal fade">
		    <div class="modal-dialog" style="width: 1200px;">
		        <div class="modal-content" style="width: 1200px;">
		            <!-- dialog body -->
		            <div class="modal-body" style="max-height: 450px;">
		                <div id="invoiceListPreviewContent"></div>
		                <div class="iu-content"></div>
		            </div>
		            <!-- dialog buttons -->
		            <div class="modal-footer">
			            <g:field type="button" style="width: 90px" class="btn btn-primary" name="close_modal" id="close_modal"
						value="${message(code: 'default.close.label', default: 'Close')}" data-dismiss="modal"/>
		            </div>
		        </div>
		    </div>
		</div>			
		<!-- End Modal Preview -->
	</body>
</html>