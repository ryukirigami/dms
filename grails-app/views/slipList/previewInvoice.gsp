<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript>
		</g:javascript>
</head>
<body>
<div class="box" style="height: 400px; overflow: auto;">
    <table width="100%" style="border-collapse:separate; border-spacing:1px;">
        <tr>
            <td colspan="7" width="100%"><img src="${request.contextPath}/images/toyota.png" width="130" height="40"></td>
            <td width="20%">${data?.noInvoice}</td>
        </tr>
        <tr>
            <td colspan="8" width="100%"><h4>${data?.namaWorkshop}</h4></td>
        </tr>
        <tr>
            <td colspan="6" width="100%">TECHNICAL SERVICE DIVISION</td>
            <td width="30%">No. Invoice</td>
            <td width="20%">${data?.noInvoice}</td>
        </tr>
        <tr>
            <td colspan="6" width="100%"></td>
            <td width="30%">Jakarta</td>
            <td width="20%">${data?.tanggalCetak}</td>
        </tr>
        <tr>
            <td colspan="6" width="100%">${data?.alamatWorkshop}</td>
            <td width="30%">NPWP</td>
            <td width="20%">${data?.npwpPenjual}</td>
        </tr>
        <tr>
            <td colspan="6" width="100%">Telp Pelayanan : ${data?.teleponWorkshop}</td>
            <td width="30%">PKP</td>
            <td width="20%">${data?.pkp}</td>
        </tr>
        <tr valign="top">
            <td colspan="6" width="100%">Fax : </td>
            <td width="30%" colspan="2"></td>
        </tr>
        <tr>
            <td colspan="8" width="100%" align="center"><h4>SERVICE INVOICE</h4></td>
        </tr>
<tr>
    <td colspan="8" width="100%">
        <table width="100%" style="border: 1px solid #000000;">
            <tr>
                <td width="60%">
                    <table width="100%">
                        <tr>
                            <td width="15%" style="padding: 5px;"><span style="text-decoration: underline;">Pelanggan</span><br/>Customer</td>
                            <td width="85%">: ${data?.pelanggan}</td>
                        </tr>
                        <tr>
                            <td width="15%" style="padding: 5px;"><span style="text-decoration: underline;">Alamat</span><br/>Address</td>
                            <td width="85%">: ${data?.alamatPelanggan}</td>
                        </tr>
                        <tr>
                            <td width="15%" style="padding: 5px;">NPWP</td>
                            <td width="85%">: ${data?.npwpPelanggan}</td>
                        </tr>
                    </table>
                </td>
                <td width="40%">
                    <table width="100%">
                        <tr>
                            <td width="15%"><span style="text-decoration: underline;">No Polisi</span><br/>Police No</td>
                            <td width="85%">: ${data?.nomorPolisi}</td>
                        </tr>
                        <tr>
                            <td width="15%">Model</td>
                            <td width="85%">: ${data?.model}</td>
                        </tr>
                        <tr>
                            <td width="15%"><span style="text-decoration: underline;">No Mesin</span><br/>Engine No</td>
                            <td width="85%">: ${data?.nomorMesin}</td>
                        </tr>
                        <tr>
                            <td width="15%"><span style="text-decoration: underline;">No Rangka</span><br/>Chassis No</td>
                            <td width="85%">: ${data?.nomorRangka}</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td colspan="8" width="100%">
        <table width="100%" style="border: 1px solid #000000;">
            <tr>
                <td width="10%" style="padding: 5px;">No Order</td>
                <td width="30%">: ${data?.nomorOrder}</td>
                <td width="8%">Int / Ext</td>
                <td width="23%">: ${data?.intExt}</td>
                <td width="12%">Dicetak / Printed</td>
                <td width="18%">: ${data?.tanggalCetak}</td>
            </tr>
            <tr>
                <td width="10%" style="padding: 5px;">Service Advisor</td>
                <td width="30%">: ${data?.serviceAdv}</td>
                <td width="8%">Technician</td>
                <td width="23%">: ${data?.teknisi}</td>
                <td width="12%">Administrasi / Billing</td>
                <td width="18%">: ${data?.admBilling}</td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td colspan="8" width="100%">
        <table width="100%">
            <tr>
                <td colspan="3" width="55%" align="center" style="border: 1px solid #000000">Total Jasa / Labor</td>
                <td colspan="2" width="45%" align="center" style="border: 1px solid #000000">Total Biaya Cost</td>
            </tr>
            <tr>
                <td width="30%" align="center" style="border: 1px solid #000000">Uraian / Description</td>
                <td width="10%" align="center" style="border: 1px solid #000000">Rate</td>
                <td width="15%" align="center" style="border: 1px solid #000000">Rupiah</td>
                <td width="30%" align="center" style="border: 1px solid #000000">Uraian / Description</td>
                <td width="15%" align="center" style="border: 1px solid #000000">Rupiah</td>
            </tr>
            <tr style="border-bottom: none">
                <td style="padding-left: 5px;border-left: 1px solid #000000;" rowspan="17" valign="top">
                    <table>
                        <g:each in="${data?.detailsJasa}" status="i1" var="det1">
                            <tr>
                                <td>${det1?.desc}</td>
                            </tr>
                        </g:each>
                    </table>                                </td>
                <td align="center" style="border-left: 1px solid #000000;" rowspan="17" valign="top">
                    <table>
                        <g:each in="${data?.detailsJasa}" status="i2" var="det2">
                            <tr>
                                <td>${det2?.rate}</td>
                            </tr>
                        </g:each>
                    </table>                                </td>
                <td align="right" style="border-left: 1px solid #000000;padding-right: 5px" rowspan="17" valign="top">
                    <table>
                        <g:each in="${data?.detailsJasa}" status="i3" var="det3">
                            <tr>
                                <td>${det3?.rupiah}</td>
                            </tr>
                        </g:each>
                    </table>                                </td>
                <td style="padding-left: 5px; border-left: 1px solid #000000;">Biaya Jasa</td>
                <td align="right" style="border-left: 1px solid #000000;border-right: 1px solid #000000;padding-right: 5px">${data?.jasaLabor}</td>
            </tr>
            <tr style="border-bottom: none">
                <td style="padding-left: 5px; border-left: 1px solid #000000;">Biaya Part</td>
                <td align="right" style="border-left: 1px solid #000000;border-right: 1px solid #000000;padding-right: 5px">${data?.sukuCadang}</td>
            </tr>
            <tr style="border-bottom: none">
                <td style="padding-left: 5px; border-left: 1px solid #000000;">Biaya Oli</td>
                <td align="right" style="border-left: 1px solid #000000;border-right: 1px solid #000000;padding-right: 5px">${data?.oil}</td>
            </tr>
            <tr style="border-bottom: none">
                <td style="padding-left: 5px; border-left: 1px solid #000000;">Biaya Material</td>
                <td align="right" style="border-left: 1px solid #000000;border-right: 1px solid #000000; padding-right: 5px;">${data?.material}</td>
            </tr>
            <tr style="border-bottom: none">
                <td style="padding-left: 5px; border-left: 1px solid #000000;">Biaya Sublet</td>
                <td align="right" style="border-left: 1px solid #000000;border-right: 1px solid #000000; padding-right: 5px">${data?.sublet}</td>
            </tr>
            <tr style="border-bottom: none">
                <td style="padding-left: 5px; border-left: 1px solid #000000;">Biaya Administrasi</td>
                <td align="right" style="border-left: 1px solid #000000;border-right: 1px solid #000000; padding-right: 5px;">${data?.adm}</td>
            </tr>
            <tr style="border-bottom: none">
                <td style="padding-left: 5px; border-left: 1px solid #000000;">&nbsp;</td>
                <td align="right" style="border-left: 1px solid #000000;border-right: 1px solid #000000; padding-right: 5px">&nbsp;</td>
            </tr>
            <tr style="border-bottom: none">
                <td style="padding-left: 5px; border-left: 1px solid #000000;">Sub Total 1</td>
                <td align="right" style="border-left: 1px solid #000000;border-right: 1px solid #000000; padding-right: 5px">${data?.subTot1}</td>
            </tr>
            <tr style="border-bottom: none">
                <td style="padding-left: 5px; border-left: 1px solid #000000;">Discount Jasa</td>
                <td align="right" style="border-left: 1px solid #000000;border-right: 1px solid #000000; padding-right: 5px">${data?.discJasa}</td>
            </tr>
            <tr style="border-bottom: none">
                <td style="padding-left: 5px; border-left: 1px solid #000000;">Discount Parts</td>
                <td align="right" style="border-left: 1px solid #000000;border-right: 1px solid #000000; padding-right: 5px">${data?.discPart}</td>
            </tr>
            <tr style="border-bottom: none">
                <td style="padding-left: 5px; border-left: 1px solid #000000;">&nbsp;</td>
                <td align="right" style="border-left: 1px solid #000000;border-right: 1px solid #000000; padding-right: 5px">&nbsp;</td>
            </tr>
            <tr style="border-bottom: none">
                <td style="padding-left: 5px; border-left: 1px solid #000000;">Sub Total 2</td>
                <td align="right" style="border-left: 1px solid #000000;border-right: 1px solid #000000;padding-right: 5px">${data?.subTot2}</td>
            </tr>
            <tr style="border-bottom: none">
                <td style="padding-left: 5px; border-left: 1px solid #000000;">PPN (Tax) 10%</td>
                <td align="right" style="border-left: 1px solid #000000;border-right: 1px solid #000000;padding-right: 5px">${data?.ppn}</td>
            </tr>
            <tr style="border-bottom: none">
                <td style="padding-left: 5px; border-left: 1px solid #000000;">Biaya Materai</td>
                <td align="right" style="border-left: 1px solid #000000;border-right: 1px solid #000000;padding-right: 5px">${data?.materai}</td>
            </tr>
            <tr style="border-bottom: none">
                <td style="padding-left: 5px; border-left: 1px solid #000000;">&nbsp;</td>
                <td align="right" style="border-left: 1px solid #000000;border-right: 1px solid #000000;padding-right: 5px">&nbsp;</td>
            </tr>
            <tr style="border-bottom: none">
                <td style="padding-left: 5px; border-left: 1px solid #000000;">Total Biaya</td>
                <td align="right" style="border-left: 1px solid #000000;border-right: 1px solid #000000;padding-right: 5px">${data?.totalInvoice}</td>
            </tr>
            <tr style="border-bottom: none">
                <td style="padding-left: 5px; border-left: 1px solid #000000;">Uang Muka</td>
                <td align="right" style="border-left: 1px solid #000000;border-right: 1px solid #000000;padding-right: 5px">${data?.bookingFee}</td>
            </tr>
            <tr>
                <td rowspan="3" colspan="2" align="center" valign="middle" style="padding-left: 5px; border: 1px solid #000000;">Total Jasa / Labor</td>
                <td rowspan="3" align="right" valign="midle" style="border: 1px solid #000000; padding-right: 5px;">${data?.subTotalJasa}</td>
                <td valign="middle" align="center" style="padding-left: 5px; border: 1px solid #000000;">Total Biaya / Cost</td>
                <td valign="middle" align="right" style="border: 1px solid #000000; padding-right: 5px;">${data?.totalBayar}</td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td colspan="8" width="100%">
        <table width="100%">
            <tr>
                <td width="15%" style="padding: 5px;"><span
                        style="text-decoration: underline;">Terbilang</span><br/>Say In Word</td>
                <td width="5%">(Rupiah)</td>
                <td width="80%" colspan="6" style="padding: 5px;border: 1px solid #000000">${data?.terbilang}</td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td colspan="2" width="20%">
        <table width="100%" style="border: 1px solid #000000;">
            <tr>
                <td align="center" style="border-bottom:1px solid #000000; padding-left: 5px; font-weight: bold;">Keterangan/Supply Slip</td>
            </tr>
            <tr>
                <td style="border-bottom:1px solid #000000; padding-left: 5px;">${data?.supply}<br/>&nbsp;<br/>&nbsp;</td>
            </tr>
            <tr>
                <td align="center" style="border-bottom:1px solid #000000; padding-left: 5px; font-weight: bold;">Informasi Garansi</td>
            </tr>
            <tr>
                <td style="padding-left: 5px;"><br/>&nbsp;<br/>&nbsp;</td>
            </tr>
        </table>
    </td>
    <td colspan="4" width="40%">
        <table width="100%" style="border: 2px solid #000000;">
            <tr>
                <td align="center" valign="top" style="border: 2px solid #000000; padding-left: 5px; font-weight: bold;">Saran / Suggestion</td>
            </tr>
            <tr>
                <td style="padding-left: 5px;" valign="top">${data?.saran}<br/>&nbsp;<br/>&nbsp;</td>
            </tr>
            <tr style="border-bottom: 2px solid #000000;">
                <td rowspan="2">&nbsp;<br/></td>
            </tr>
        </table>
    </td>
    <td colspan="4" rowspan="3" width="40%" align="left" valign="top" style="padding-left: 5px;">
        <table style="margin-top: 100px; margin-left: 150px; border: 2px solid #000000; font-weight: bold;">
            <tr>
                <td style="padding-left: 30px; padding-right: 30px; padding-top: 15px;" align="center">
                    Dikeluarkan Oleh :
                </td>
            </tr>
            <tr>
                <td style="padding-left: 30px; padding-right: 30px;" align="center">
                    Issued By :
                </td>
            </tr>
            <tr>
                <td style="padding-left: 30px; padding-right: 30px;" valign="bottom" align="center">
                    <br/><br/><br/><br/><br/><br/> ${data?.kaBeng}
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td colspan="4" width="40%">
        <table width="100%" style="border: 1px solid #000000;">
            <tr>
                <td style="padding-left: 5px;">
                    <b style="text-decoration: underline;">GARANSI BENGKEL / WARRANTY</b>
                    <i style="font-size: 10px;">
                        <br/>15 HARI atau 1.000 Km untuk reparasi
                        <br/>15 days or 1.0000 Km for general repair
                        <br/>1 BULAN untuk Engine O.H dan 6 BULAN untuk pengecetan Body
                        <br/>1 Month for Engine O.H and 6 Month for Body Paint
                    </i>
                </td>
            </tr>
        </table>
    </td>
    <td colspan="2" width="20%">
        <table width="100%" style="border: 1px solid #000000;">
            <tr>
                <td style="padding-left: 5px;">
                    <b style="font-size: 10px;">Claim / Keluhan (....) Hubungi :</b><br/>Telp<br/>${data?.keluhan}<br/>&nbsp;<br/>&nbsp;
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td colspan="6" width="60%">
        <table width="100%" style="border: 1px solid #000000;">
            <tr>
                <td style="padding-left: 5px;">
                    <b style="text-decoration: underline;">PERHATIAN / ATTENTION</b>
                    <i style="font-size: 10px;">
                        <br/>* Pembayaran adalah sah, apabila pada SERVICE INVOICE ini telah dibubuhi cap 'LUNAS' dan ditanda tangani oleh kasir atau penagih
                        <br/>* Pembayaran ditransfer pada rekening CV. KOMBOS pada Bank Danamon Cabang Cikini, Jakarta No. Rek...
                        <br/>* Pembayaran dengan Cek / Bilyet Giro dianggap sah apabila Cek / Bilyet Giro tersebut dapat dicairkan dalam rekening Perusahan                    </i>
                </td>
            </tr>
        </table>
    </td>
</tr>
</table>
<br/>
</div>
</body>
</html>