<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
		<g:javascript>
		</g:javascript>
	</head>
	<body>
		<div class="box" style="padding-top: 10px; height: 450px; overflow: auto;">
		    <legend style="font-size: small;line-height: 15px; margin-bottom: 0;">
                <span style="text-decoration: underline;">PEMBELI</span>
                <table width="100%">
                    <tr>
                        <td width="15%">Nama</td>
                        <td width="1%">:</td>
                        <td width="84%">${data?.namaPembeli}</td>
                    </tr>
                    <tr>
                        <td width="15%">Alamat</td>
                        <td width="1%">:</td>
                        <td width="84%">${data?.alamatPembeli}</td>
                    </tr>
                    <tr>
                        <td width="15%">NPWP</td>
                        <td width="1%">:</td>
                        <td width="84%">${data?.npwpPembeli}</td>
                    </tr>
                </table>
                <br/>
            </legend>
            <legend style="font-size: small;line-height: 15px;margin-bottom: 0;margin-top: 10px;">
                <table width="100%">
                    <tr>
                        <td width="100%" colspan="4" align="center"><h4>NOTA RETUR</h4></td>
                    </tr>
                    <tr>
                        <td width="10%">Faktur Pajak No.</td>
                        <td width="1%">:</td>
                        <td width="74%">${data?.noFakturPajak}</td>
                        <td width="15%">No. ${data?.noFaktur}</td>
                    </tr>
                    <tr>
                        <td width="10%">Tanggal</td>
                        <td width="1%">:</td>
                        <td width="74%">${data?.tanggalFakturPajak}</td>
                        <td width="15%">${data?.tanggalFaktur}</td>
                    </tr>
                </table>
                <br/>
            </legend>
            <legend style="font-size: small;line-height: 15px; margin-top: 10px; margin-bottom: 0;">
                <span style="text-decoration: underline;">KEPADA PENJUAL</span>
                <table width="100%">
                    <tr>
                        <td width="15%">Nama</td>
                        <td width="1%">:</td>
                        <td width="84%">${data?.namaPenjual}</td>
                    </tr>
                    <tr>
                        <td width="15%">Alamat</td>
                        <td width="1%">:</td>
                        <td width="84%">${data?.alamatWorkshop}</td>
                    </tr>
                    <tr>
                        <td width="15%">NPWP</td>
                        <td width="1%">:</td>
                        <td width="84%">${data?.namaPenjual}</td>
                    </tr>
                    <tr>
                        <td width="15%">Tanggal Pengukuhan PKP</td>
                        <td width="1%">:</td>
                        <td width="84%">${data?.tanggalPKP}</td>
                    </tr>
                </table>
                <table width="100%" border="1">
                    <tr>
                        <td width="5%" align="center" valign="midle">No. Urut</td>
                        <td width="45%" align="center" valign="midle">Nama Barang Kena Pajak Yang Dikembalikan</td>
                        <td width="10%" align="center" valign="midle">Kwantum</td>
                        <td width="20%" align="center" valign="midle">Harga Satuan<br/>menurut Faktur<br/>Pajak (Rp)</td>
                        <td width="20%" align="center" valign="midle">Harga Jual Yang<br/>Dikembalikan<br/>(Rp)</td>
                    </tr>
                    <tr>
                        <td width="5%" align="center" valign="top">1</td>
                        <td width="45%" align="left" valign="top">BIAYA JASA</td>
                        <td width="10%" align="center" valign="top"></td>
                        <td width="20%" align="right" valign="top"></td>
                        <td width="20%" align="right" valign="top">${data?.biayaJasa}</td>
                    </tr>
                    <tr>
                        <td width="5%" align="center" valign="top">2</td>
                        <td width="45%" align="left" valign="top">BIAYA (PARTS/MATERIAL/OIL/SUBLET)</td>
                        <td width="10%" align="center" valign="top"></td>
                        <td width="20%" align="right" valign="top"></td>
                        <td width="20%" align="right" valign="top">${data?.biayaGoods}</td>
                    </tr>
                    <tr>
                        <td width="5%" align="center" valign="top">&nbsp;</td>
                        <td width="45%" align="left" valign="top"></td>
                        <td width="10%" align="center" valign="top"></td>
                        <td width="20%" align="right" valign="top"></td>
                        <td width="20%" align="right" valign="top"></td>
                    </tr>
                    <tr>
                        <td width="5%" align="center" valign="top">&nbsp;</td>
                        <td width="45%" align="left" valign="top"></td>
                        <td width="10%" align="center" valign="top"></td>
                        <td width="20%" align="right" valign="top"></td>
                        <td width="20%" align="right" valign="top"></td>
                    </tr>
                    <tr>
                        <td width="5%" align="center" valign="top">&nbsp;</td>
                        <td width="45%" align="left" valign="top"></td>
                        <td width="10%" align="center" valign="top"></td>
                        <td width="20%" align="right" valign="top"></td>
                        <td width="20%" align="right" valign="top"></td>
                    </tr>
                    <tr>
                        <td width="5%" align="center" valign="top">&nbsp;</td>
                        <td width="45%" align="left" valign="top"></td>
                        <td width="10%" align="center" valign="top"></td>
                        <td width="20%" align="right" valign="top"></td>
                        <td width="20%" align="right" valign="top"></td>
                    </tr>
                    <tr>
                        <td width="5%" align="center" valign="top">&nbsp;</td>
                        <td width="45%" align="left" valign="top"></td>
                        <td width="10%" align="center" valign="top"></td>
                        <td width="20%" align="right" valign="top"></td>
                        <td width="20%" align="right" valign="top"></td>
                    </tr>
                    <tr>
                        <td width="5%" align="center" valign="top">&nbsp;</td>
                        <td width="45%" align="left" valign="top"></td>
                        <td width="10%" align="center" valign="top"></td>
                        <td width="20%" align="right" valign="top"></td>
                        <td width="20%" align="right" valign="top"></td>
                    </tr>
                    <tr>
                        <td width="5%" align="center" valign="top">&nbsp;</td>
                        <td width="45%" align="left" valign="top"></td>
                        <td width="10%" align="center" valign="top"></td>
                        <td width="20%" align="right" valign="top"></td>
                        <td width="20%" align="right" valign="top"></td>
                    </tr>
                    <tr>
                        <td width="5%" align="center" valign="top">&nbsp;</td>
                        <td width="45%" align="left" valign="top"></td>
                        <td width="10%" align="center" valign="top"></td>
                        <td width="20%" align="right" valign="top"></td>
                        <td width="20%" align="right" valign="top"></td>
                    </tr>
                    <tr>
                        <td width="80" colspan="4">Jumlah Harga Jual Yang Dikembalikan</td>
                        <td width="20" align="right">${data?.harga}</td>
                    </tr>
                    <tr>
                        <td width="80" colspan="4">Jumlah Pajak Pertambahan Nilai (PPN) Yang Dikurangi</td>
                        <td width="20" align="right">${data?.ppn}</td>
                    </tr>
                    <tr>
                        <td width="50" colspan="2">&nbsp;</td>
                        <td width="50" colspan="3" align="center">PEMBELI<br/><br/><br/><br/><br/>(${data?.namaPembeli})</td>
                    </tr>
                </table>
                <br/>
            </legend>
            <legend style="font-size: small;line-height: 15px; margin-top: 10px;">
                <table width="100%">
                    <tr>
                        <td width="15%">Lembar ke-1</td>
                        <td width="1%">:</td>
                        <td width="84%">Untuk Pengusaha Pajak Yang Menerbitkan Faktur Pajak</td>
                    </tr>
                    <tr>
                        <td width="15%">Lembar ke-2</td>
                        <td width="1%">:</td>
                        <td width="84%">Untuk Pembeli</td>
                    </tr>
                    <tr>
                        <td width="15%">Lembar ke-3</td>
                        <td width="1%">:</td>
                        <td width="84%">Arsip</td>
                    </tr>
                </table>
                <br/>
            </legend>
		</div>
	</body>
</html>