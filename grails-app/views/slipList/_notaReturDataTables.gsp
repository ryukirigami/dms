<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="nota_retur_datatables_${idTable}" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	style="table-layout: fixed;"
	width="100%">
	<thead>
    	<tr>
			<th>
				<div style="width: 10px;">&nbsp;</div>
			</th>
        	<th style="border-bottom: none;">
            	<div style="width: 100px;">Tgl Nota Retur</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 125px;">No. Nota Retur</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 125px;">No. Faktur Pajak</div>
			</th>			
			<th style="border-bottom: none;">
            	<div style="width: 125px;">Penjual</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 150px;">Alamat Penjual</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 100px;">NPWP</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 125px;">Pembeli BKP/JKP</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 150px;">Alamat</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 100px;">NPWP</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 125px;">Jasa</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 125px;">Parts</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 125px;">Total Dikembalikan</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 125px;">PPN Dikurangi</div>
			</th>
		</tr>
	</thead>
</table>
<g:javascript>
var notaTable;
var reloadNotaTable;
var msg='';

function searchData() {
    if(isValid()){
        reloadNotaTable();
    }else{
        alert(msg);
        return false;
    }
}

$(function() {
    isValid = function(){
        msg = '';
        var chkTanggal = $('input[name=chkTanggalNota]').is(':checked');
        if(chkTanggal){
            var tanggalNotaStart = $("#tanggalNota_start").val();
            var tanggalNotaEnd = $("#tanggalNota_end").val();
            if(tanggalNotaStart==''){msg+='- Silahkan pilih tanggal awal nota \n';}
            if(tanggalNotaEnd==''){msg+='- Silahkan pilih tanggal akhir nota \n';}
        }

        var chkKategori = $('input[name=chkKategoriNota]').is(':checked');
        if(chkKategori){
            var kategoriNota = $('input[name=kategoriNota]').val();
            if(kategoriNota==''){msg+='- Silahkan pilih kategori nota \n';}
        }

        var chkKataKunci = $('input[name=chkKataKunci]').is(':checked');
        if(chkKataKunci){
            var kataKunci = $('input[name=kataKunci]').val();
            if(kataKunci==''){msg+='- Silahkan masukkan kata kunci nota \n';}
        }

        return msg=='';
    };

	reloadNotaTable = function() {
		notaTable.fnDraw();
	};
	
	notaTable = $('#nota_retur_datatables_${idTable}').dataTable({
		"sScrollX": "1024px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		},
	    "fnRowCallback": function (nRow) {
			return nRow;
	    },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "notaReturDataTablesList")}",
		"aoColumns": [
			//radioButton
			{
				"mDataProp": "noNotaRetur",
		        "sClass": "control center",
		        "bSortable": false,
		        "sWidth":"10px",
				"mRender": function ( data, type, row ) {
					return '<input type="radio" name="radioButton" class="pull-left row-select" value="'+row['noNotaRetur']+'" style="margin-top: 4px;"><input type="hidden" value="'+row['noNotaRetur']+'">';
				}
			},			
			//tanggalNotaRetur
			{
				"sName": "tanggalNotaRetur",
				"mDataProp": "tanggalNotaRetur",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			//noNotaRetur
			{
				"sName": "noNotaRetur",
				"mDataProp": "noNotaRetur",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//noFakturPajak
			{
				"sName": "noFakturPajak",
				"mDataProp": "noFakturPajak",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//penjual
			{
				"sName": "penjual",
				"mDataProp": "penjual",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//alamatPenjual
			{
				"sName": "alamatPenjual",
				"mDataProp": "alamatPenjual",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"150px",
				"bVisible": true
			},
			//npwpPenjual
			{
				"sName": "npwpPenjual",
				"mDataProp": "npwpPenjual",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			//pembeli
			{
				"sName": "pembeli",
				"mDataProp": "pembeli",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//alamatPembeli
			{
				"sName": "alamatPembeli",
				"mDataProp": "alamatPembeli",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"150px",
				"bVisible": true
			},
			//npwpPembeli
			{
				"sName": "npwpPembeli",
				"mDataProp": "npwpPembeli",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			//jasa
			{
				"sName": "jasa",
				"mDataProp": "jasa",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//parts
			{
				"sName": "parts",
				"mDataProp": "parts",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//totalDikembalikan
			{
				"sName": "totalDikembalikan",
				"mDataProp": "totalDikembalikan",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//ppn
			{
				"sName": "ppn",
				"mDataProp": "ppn",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			}
		],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			var tanggalNotaStart = $("#tanggalNota_start").val();
			var tanggalNotaStartDay = $('#tanggalNota_start_day').val();
			var tanggalNotaStartMonth = $('#tanggalNota_start_month').val();
			var tanggalNotaStartYear = $('#tanggalNota_start_year').val();
			var chkTanggal = $('input[name=chkTanggalNota]').is(':checked');
            if(tanggalNotaStart && chkTanggal) {
				aoData.push(
						{"name": 'tanggalNotaStart', "value": "date.struct"},
						{"name": 'tanggalNotaStart_dp', "value": tanggalNotaStart},
						{"name": 'tanggalNotaStart_day', "value": tanggalNotaStartDay},
						{"name": 'tanggalNotaStart_month', "value": tanggalNotaStartMonth},
						{"name": 'tanggalNotaStart_year', "value": tanggalNotaStartYear},
						{"name": 'chkTanggalNota', "value": true}
				);
			}
			
            var tanggalNotaEnd = $("#tanggalNota_end").val();
            var tanggalNotaEndDay = $('#tanggalNota_end_day').val();
			var tanggalNotaEndMonth = $('#tanggalNota_end_month').val();
			var tanggalNotaEndYear = $('#tanggalNota_end_year').val();
			if(tanggalNotaEnd && chkTanggal) {
				aoData.push(
						{"name": 'tanggalNotaEnd', "value": "date.struct"},
						{"name": 'tanggalNotaEnd_dp', "value": tanggalNotaEnd},
						{"name": 'tanggalNotaEnd_day', "value": tanggalNotaEndDay},
						{"name": 'tanggalNotaEnd_month', "value": tanggalNotaEndMonth},
						{"name": 'tanggalNotaEnd_year', "value": tanggalNotaEndYear}
				);
			}
			
            var kategoriNota = $('input[name=kategoriNota]').val();
            var chkKategori = $('input[name=chkKategoriNota]').is(':checked');
            if(kategoriNota && chkKategori) {
				aoData.push(
						{"name": 'kategoriNota', "value": kategoriNota},
						{"name": 'chkKategoriNota', "value": true}
				);
			}
            
			var kataKunci = $('input[name=kataKunci]').val();
			var chkKataKunci = $('input[name=chkKataKunci]').is(':checked');
            if(kataKunci && chkKataKunci) {
				aoData.push(
						{"name": 'kataKunci', "value": kataKunci},
						{"name": 'chkKataKunci', "value": true}
				);
			}			
			
			$.ajax({ "dataType": 'json',
				"type": "POST",
				"url": sSource,
				"data": aoData ,
				"success": function (json) {
					fnCallback(json);
				},
				"complete": function () {}
			});
			
		}
	});
		
});
</g:javascript>