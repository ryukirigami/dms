<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="invoice_datatables_${idTable}" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
    	<tr>
			<th>
				<div style="width: 20px;">&nbsp;</div>
			</th>
        	<th style="border-bottom: none;">
            	<div style="width: 80px;">Tgl Invoice</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 60px;">Jenis Invoice</div>
			</th>
            <th style="border-bottom: none;">
                <div style="width: 60px;">Tipe Inv</div>
            </th>
            <th style="border-bottom: none;">
                <div style="width: 60px;">Status</div>
            </th>
			<th style="border-bottom: none;">
            	<div style="width: 125px;">No. Invoice</div>
			</th>			
			<th style="border-bottom: none;">
            	<div style="width: 90px;">Nomor WO</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 80px;">Nomor Polisi</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 125px;">Nama Customer</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 40px;">SPK</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 40px;">Jml Print</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 100px;">Total Invoice</div>
			</th>
			<th style="border-bottom: none;">
				<div style="width: 80px;">Metode Bayar</div>
			</th>
		</tr>
	</thead>
</table>
<g:javascript>
var invoiceTable;
var reloadInvoiceTable;
var msg='';
    //fungsi enter
	$("#nomorInvoice,#customerName,#nomorPolisi").bind('keypress', function(e) {
	var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			reloadInvoiceTable();
		}
	});

	function searchData() {
		if(isValid()){
			reloadInvoiceTable();
		}else{
			alert(msg);
			return false;
		}
	}

$(function() {

    isValid = function() {
        msg='';
        var chkTanggal = $('input[name=chkTanggalInvoice]').is(':checked');
        if(chkTanggal){
            var tanggalInvoiceStart = $("#tanggalInvoice_start").val();
            var tanggalInvoiceEnd = $("#tanggalInvoice_end").val();
            if(tanggalInvoiceStart==''){msg+='- Silahkan pilih tanggal awal invoice \n';}
            if(tanggalInvoiceEnd==''){msg+='- Silahkan pilih tanggal akhir invoice \n';}
        }

        var chkTanggalSettlement = $('input[name=chkTanggalSettlement]').is(':checked');
        if(chkTanggalSettlement){
            var tanggalSettlementStart = $("#tanggalSettlement_start").val();
            var tanggalSettlementEnd = $("#tanggalSettlement_end").val();
            if(tanggalSettlementStart==''){msg+='- Silahkan pilih tanggal awal settlement \n';}
            if(tanggalSettlementEnd==''){msg+='- Silahkan pilih tanggal akhir settlement \n';}
        }

        var chkStatus = $('input[name=chkStatusInvoice]').is(':checked');
        if(chkStatus){
            var statusInvoice = $('input[name=statusInvoice]').val();
            if(statusInvoice==''){msg+='- Silahkan pilih status invoice \n';}
        }

        var chkSPK = $('input[name=chkCustomerSPK]').is(':checked');
        if(chkSPK){
            var customerSPK = $('input[name=customerSPK]').val();
            if(customerSPK==''){msg+='- Silahkan pilih customer spk \n';}
        }

        var chkMetode = $('input[name=chkMetodeBayar]').is(':checked');
        if(chkMetode){
            var metodeBayar = $('input[name=metodeBayar]').val();
            if(metodeBayar==''){msg+='- Silahkan pilih metode bayar \n';}
        }

        var chkInvoice = $('input[name=chkNomorInvoice]').is(':checked');
        if(chkInvoice){
            var nomorInvoice = $('input[name=nomorInvoice]').val();
            if(nomorInvoice==''){msg+='- Silahkan masukkan nomor invoice \n';}
        }

        var chkJenis = $('input[name=chkJenisInvoice]').is(':checked');
        if(chkJenis){
            var jenisInvoice = $('input[name=jenisInvoice]').val();
            if(jenisInvoice==''){msg+='- Silahkan pilih jenis invoice \n';}
        }

        var chkTipe = $('input[name=chkTipeInvoice]').is(':checked');
        if(chkTipe){
            var tipeInvoice = $('input[name=tipeInvoice]').val();
            if(nomorInvoice==''){msg+='- Silahkan pilih tipe invoice \n';}
        }

        var chkCustomer = $('input[name=chkCustomerName]').is(':checked');
        if(chkCustomer){
            var customerName = $('input[name=customerName]').val();
            if(customerName==''){msg+='- Silahkan masukkan nama customer \n';}
        }

        var chkAging = $('input[name=chkAging]').is(':checked');
        if(chkAging){
            var startAging = $('input[name=startAging]').val();
			var endAging = $('input[name=endAging]').val();
			if(startAging==''){msg+='- Silahkan masukkan awal aging \n';}
			if(endAging==''){msg+='- Silahkan masukkan akhir aging \n';}
        }

        var chkNomorPolisi = $('input[name=chkNomorPolisi]').is(':checked');
        if(chkNomorPolisi){
            var nomorPolisi = $('input[name=nomorPolisi]').val();
            if(nomorPolisi==''){msg+='- Silahkan masukkan nomor polisi \n';}
        }

        return msg=='';
    };

	reloadInvoiceTable = function() {
		invoiceTable.fnDraw();
	};
	
	invoiceTable = $('#invoice_datatables_${idTable}').dataTable({
	    "sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		},
	    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
	    },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "invoiceDataTablesList")}",
		"aoColumns": [
			//radioButton
			{
				"mDataProp": "noInvoice",
		        "sClass": "control center",
		        "bSortable": false,
		        "sWidth":"20px",
				"mRender": function ( data, type, row ) {
					return '<input type="radio" name="radioButton" class="pull-left row-select" value="'+row['noInvoice']+'" style="margin-top: 4px;"><input name="id" id="id" type="hidden" value="'+row['noInvoice']+'">&nbsp;&nbsp;' + ((row['status']=='CLOSING')?'<i class="row-select icon-plus" style="cursor: pointer">':'&nbsp');
				}
			},
			//tanggalInvoice
			{
				"sName": "tanggalInvoice",
				"mDataProp": "tanggalInvoice",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"80px",
				"bVisible": true
			},
			//jenisInvoice
			{
				"sName": "jenisInvoice",
				"mDataProp": "jenisInvoice",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"60px",
				"bVisible": true
			},
			//tipeInv
			{
				"sName": "tipeInv",
				"mDataProp": "tipeInv",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"60px",
				"bVisible": true
			},

			//status
			{
				"sName": "status",
				"mDataProp": "status",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"60px",
				"bVisible": true
			},

			//noInvoice
			{
				"sName": "noInvoice",
				"mDataProp": "noInvoice",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//nomorWO
			{
				"sName": "nomorWO",
				"mDataProp": "nomorWO",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"90px",
				"bVisible": true
			},
			//nomorPolisi
			{
				"sName": "nomorPolisi",
				"mDataProp": "nomorPolisi",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"80px",
				"bVisible": true
			},
			//namaCustomer
			{
				"sName": "namaCustomer",
				"mDataProp": "namaCustomer",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//spk
			{
				"sName": "spk",
				"mDataProp": "spk",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"40px",
				"bVisible": true
			},
			//jml print
			{
				"sName": "jmlPrint",
				"mDataProp": "jmlPrint",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"40px",
				"bVisible": true
			},
			//totalInvoice
			{
				"sName": "totalInvoice",
				"mDataProp": "totalInvoice",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			//metode
			{
				"sName": "metode",
				"mDataProp": "metode",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"80px",
				"bVisible": true
			}
		],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			var tanggalInvoiceStart = $("#tanggalInvoice_start").val();
			var tanggalInvoiceStartDay = $('#tanggalInvoice_start_day').val();
			var tanggalInvoiceStartMonth = $('#tanggalInvoice_start_month').val();
			var tanggalInvoiceStartYear = $('#tanggalInvoice_start_year').val();
			var chkTanggal = $('input[name=chkTanggalInvoice]').is(':checked');
            if(tanggalInvoiceStart && chkTanggal) {
				aoData.push(
						{"name": 'tanggalInvoiceStart', "value": "date.struct"},
						{"name": 'tanggalInvoiceStart_dp', "value": tanggalInvoiceStart},
						{"name": 'tanggalInvoiceStart_day', "value": tanggalInvoiceStartDay},
						{"name": 'tanggalInvoiceStart_month', "value": tanggalInvoiceStartMonth},
						{"name": 'tanggalInvoiceStart_year', "value": tanggalInvoiceStartYear},
						{"name": 'chkTanggalInvoice', "value": true}
				);
			}
			
            var tanggalInvoiceEnd = $("#tanggalInvoice_end").val();
            var tanggalInvoiceEndDay = $('#tanggalInvoice_end_day').val();
			var tanggalInvoiceEndMonth = $('#tanggalInvoice_end_month').val();
			var tanggalInvoiceEndYear = $('#tanggalInvoice_end_year').val();
			if(tanggalInvoiceEnd && chkTanggal) {
				aoData.push(
						{"name": 'tanggalInvoiceEnd', "value": "date.struct"},
						{"name": 'tanggalInvoiceEnd_dp', "value": tanggalInvoiceEnd},
						{"name": 'tanggalInvoiceEnd_day', "value": tanggalInvoiceEndDay},
						{"name": 'tanggalInvoiceEnd_month', "value": tanggalInvoiceEndMonth},
						{"name": 'tanggalInvoiceEnd_year', "value": tanggalInvoiceEndYear}
				);
			}
			
			var tanggalSettlementStart = $("#tanggalSettlement_start").val();
			var tanggalSettlementStartDay = $('#tanggalSettlement_start_day').val();
			var tanggalSettlementStartMonth = $('#tanggalSettlement_start_month').val();
			var tanggalSettlementStartYear = $('#tanggalSettlement_start_year').val();
			var chkTanggalSettlement = $('input[name=chkTanggalSettlement]').is(':checked');
            if(tanggalSettlementStart && chkTanggalSettlement) {
				aoData.push(
						{"name": 'tanggalSettlementStart', "value": "date.struct"},
						{"name": 'tanggalSettlementStart_dp', "value": tanggalSettlementStart},
						{"name": 'tanggalSettlementStart_day', "value": tanggalSettlementStartDay},
						{"name": 'tanggalSettlementStart_month', "value": tanggalSettlementStartMonth},
						{"name": 'tanggalSettlementStart_year', "value": tanggalSettlementStartYear},
						{"name": 'chkTanggalSettlement', "value": true}
				);
			}
			
            var tanggalSettlementEnd = $("#tanggalSettlement_end").val();
            var tanggalSetllementEndDay = $('#tanggalSettlement_end_day').val();
			var tanggalSettlementEndMonth = $('#tanggalSettlement_end_month').val();
			var tanggalSettlementEndYear = $('#tanggalSettlement_end_year').val();
			if(tanggalSettlementEnd && chkTanggalSettlement) {
				aoData.push(
						{"name": 'tanggalSettlementEnd', "value": "date.struct"},
						{"name": 'tanggalSettlementEnd_dp', "value": tanggalSettlementEnd},
						{"name": 'tanggalSettlementEnd_day', "value": tanggalSetllementEndDay},
						{"name": 'tanggalSettlementEnd_month', "value": tanggalSettlementEndMonth},
						{"name": 'tanggalSettlementEnd_year', "value": tanggalSettlementEndYear}
				);
			}
			
            var statusInvoice = $('input[name=statusInvoice]').val();
			if(statusInvoice == '')statusInvoice = 'SEMUA';
			var chkStatus = $('input[name=chkStatusInvoice]').is(':checked');
            if(statusInvoice && chkStatus) {
				aoData.push(
						{"name": 'statusInvoice', "value": statusInvoice},
						{"name": 'chkStatusInvoice', "value": true}
				);
			}
            
			var customerSPK = $('input[name=customerSPK]').val();
			if(customerSPK == '')customerSPK = 'SEMUA';
			var chkSPK = $('input[name=chkCustomerSPK]').is(':checked');
            if(customerSPK && chkSPK) {
				aoData.push(
						{"name": 'customerSPK', "value": customerSPK},
						{"name": 'chkCustomerSPK', "value": true}
				);
			}
			
			var metodeBayar = $('input[name=metodeBayar]').val();
			if(metodeBayar == '')metodeBayar = 'SEMUA';
			var chkMetode = $('input[name=chkMetodeBayar]').is(':checked');
            if(metodeBayar && chkMetode) {
				aoData.push(
						{"name": 'metodeBayar', "value": metodeBayar},
						{"name": 'chkMetodeBayar', "value": true}
				);
			}
			
			var nomorInvoice = $('input[name=nomorInvoice]').val();
			var chkInvoice = $('input[name=chkNomorInvoice]').is(':checked');
            if(nomorInvoice && chkInvoice) {
				aoData.push(
						{"name": 'nomorInvoice', "value": nomorInvoice},
						{"name": 'chkNomorInvoice', "value": true}
				);
			}
			
			var jenisInvoice = $('input[name=jenisInvoice]').val();
			if(jenisInvoice == '')jenisInvoice = 'SEMUA';
			var chkJenis = $('input[name=chkJenisInvoice]').is(':checked');
            if(jenisInvoice && chkJenis) {
				aoData.push(
						{"name": 'jenisInvoice', "value": jenisInvoice},
						{"name": 'chkJenisInvoice', "value": true}
				);
			}

			var tipeInvoice = $('input[name=tipeInvoice]').val();
			if(tipeInvoice == '')tipeInvoice = 'SEMUA';
			var chkTipe = $('input[name=chkTipeInvoice]').is(':checked');
            if(tipeInvoice && chkTipe) {
				aoData.push(
						{"name": 'tipeInvoice', "value": tipeInvoice},
						{"name": 'chkTipeInvoice', "value": true}
				);
			}
			
			var customerName = $('input[name=customerName]').val();
			var chkCustomer = $('input[name=chkCustomerName]').is(':checked');
            if(customerName && chkCustomer) {
				aoData.push(
						{"name": 'customerName', "value": customerName},
						{"name": 'chkCustomerName', "value": true}
				);
			}
			
			var startAging = $('input[name=startAging]').val();
			var endAging = $('input[name=endAging]').val();
            if(startAging && endAging && chkAging) {
				aoData.push(
						{"name": 'startAging', "value": startAging},
						{"name": 'endAging', "value": endAging},
						{"name": 'chkAging', "value": true}
				);
			}
			
			var nomorPolisi = $('input[name=nomorPolisi]').val();
			var chkNomorPolisi = $('input[name=chkNomorPolisi]').is(':checked');
            if(nomorPolisi && chkNomorPolisi) {
				aoData.push(
						{"name": 'nomorPolisi', "value": nomorPolisi},
						{"name": 'chkNomorPolisi', "value": true}
				);
			}
			
			$.ajax({ "dataType": 'json',
				"type": "POST",
				"url": sSource,
				"data": aoData ,
				"success": function (json) {
					fnCallback(json);
				},
				"complete": function () {}
			});
			
		}
	});
	
    var anOpen = [];
	$("#invoice_datatables_${idTable} tbody").delegate("tr i", "click", function (e) {
		e.preventDefault();		
   		var nTr = this.parentNode.parentNode;		
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this.parentNode).attr( 'class', "icon-minus" );
    		var oData = invoiceTable.fnGetData(nTr);			
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${request.contextPath}/slipList/invoiceSubList',
	   			success:function(data){
	   				var nDetailsRow = invoiceTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(){
	   			    alert('Data not found');
	   			    return false;
	   			},
	   			complete:function(){
	   				$('#spinner').fadeOut();
	   			}
	   		});	
		} else {
    		$('i', this.parentNode).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			invoiceTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
		}
	});	
		
});
</g:javascript>