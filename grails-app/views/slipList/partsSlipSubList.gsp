<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
		<g:javascript>
			var partsSubTable;
			$(function() {
			    if(partsSubTable){
					partsSubTable.dataTable().fnDestroy();
				}
				
				partsSubTable = $('#parts_datatables_sub_${idTable}').dataTable({
					"bScrollCollapse": false,
					"bAutoWidth" : false,
					"bPaginate" : false,
					"bInfo" : false,
					"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
					"bFilter": false,
					"bStateSave": false,
					'sPaginationType': 'bootstrap',
					"fnInitComplete": function () {
						this.fnAdjustColumnSizing(true);
					},
				    "fnRowCallback": function (nRow) {
						return nRow;
				    },
					"bSort": true,
					"bProcessing": true,
					"bServerSide": true,
					"sServerMethod": "POST",
					"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
					"sAjaxSource": '${request.contextPath}/slipList/partsSlipDataTablesSubList?id=${id}&noInvoice=${noInvoice}',
					"aoColumns": [
						//radioButton
						{
							"mDataProp": "goodId",
				        	"sClass": "control center",
				        	"bSortable": false,
				        	"sWidth":"40px",
							"mRender": function () {
								return '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-plus" style="cursor: pointer;">';
							}
						},						
						//groupGood
						{
							"sName": "groupGood",
							"mDataProp": "groupGood",
							"bSearchable": false,
							"bSortable": false,
							"sWidth":"930px",
							"bVisible": true
						},
						//subTotal
						{
							"sName": "subTotal",
							"mDataProp": "subTotal",
							"bSearchable": false,
							"bSortable": false,
							"sWidth":"100px",
							"bVisible": true
						}
					],
					"fnServerData": function ( sSource, aoData, fnCallback ) {
						var tanggalStart = $("#tanggalParts_start").val();
						var tanggalStartDay = $('#tanggalParts_start_day').val();
						var tanggalStartMonth = $('#tanggalParts_start_month').val();
						var tanggalStartYear = $('#tanggalParts_start_year').val();
						var chkTanggal = $('input[name=chkTanggalParts]').is(':checked');
			            if(tanggalStart && chkTanggal) {
							aoData.push(
									{"name": 'tanggalStart', "value": "date.struct"},
									{"name": 'tanggalStart_dp', "value": tanggalStart},
									{"name": 'tanggalStart_day', "value": tanggalStartDay},
									{"name": 'tanggalStart_month', "value": tanggalStartMonth},
									{"name": 'tanggalStart_year', "value": tanggalStartYear},
									{"name": 'chkTanggalParts', "value": true}
							);
						}
			
			            var tanggalEnd = $("#tanggalParts_end").val();
			            var tanggalEndDay = $('#tanggalParts_end_day').val();
						var tanggalEndMonth = $('#tanggalParts_end_month').val();
						var tanggalEndYear = $('#tanggalParts_end_year').val();
						if(tanggalEnd && chkTanggal) {
							aoData.push(
									{"name": 'tanggalEnd', "value": "date.struct"},
									{"name": 'tanggalEnd_dp', "value": tanggalEnd},
									{"name": 'tanggalEnd_day', "value": tanggalEndDay},
									{"name": 'tanggalEnd_month', "value": tanggalEndMonth},
									{"name": 'tanggalEnd_year', "value": tanggalEndYear}
							);
						}			
			
			            var nomorParts = $('input[name=nomorParts]').val();
			            if(nomorParts && $('input[name=chkNomorParts]').is(':checked')) {
							aoData.push(
									{"name": 'nomorParts', "value": nomorParts},
									{"name": 'chkNomorParts', "value": true}
							);
						}
            
						var nomorWO = $('input[name=nomorWO]').val();
			            if(nomorWO && $('input[name=chkNomorWO]').is(':checked')) {
							aoData.push(
									{"name": 'nomorWO', "value": nomorWO},
									{"name": 'chkNomorWO', "value": true}
							);
						}
			
						var nomorPolisi = $('input[name=nomorPolisi]').val();
			            if(nomorPolisi && $('input[name=chkNomorPolisi]').is(':checked')) {
							aoData.push(
									{"name": 'nomorPolisi', "value": nomorPolisi},
									{"name": 'chkNomorPolisi', "value": true}
							);
						}					
			
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							},
							"complete": function () {}
						});
					}
				});
				
			    var anOpenGroup = [];
				$("#parts_datatables_sub_${idTable} tbody").delegate("tr i", "click", function (e) {
					e.preventDefault();		
			   		var nTrGroup = this.parentNode.parentNode;		
			  		var iGroup = $.inArray( nTrGroup, anOpenGroup );
			   		if ( iGroup === -1 ) {
			    		$('i', this.parentNode).attr( 'class', "icon-minus" );
			    		var oDataGroup = partsSubTable.fnGetData(nTrGroup);			
			    		$('#spinner').fadeIn(1);
				   		$.ajax({type:'POST',
				   			data : oDataGroup,
				   			url:'${request.contextPath}/slipList/partsSlipSubSubList',
				   			success:function(data){
				   				var nDetailsRow = partsSubTable.fnOpen(nTrGroup,data,'details');
			    				$('div.innerDetailsGroup', nDetailsRow).slideDown();
			    				anOpenGroup.push( nTrGroup );
				   			},
				   			error:function(){
				   			    alert('Data not found');
				   			    return false;
				   			},
				   			complete:function(){
				   				$('#spinner').fadeOut();
				   			}
				   		});	
					} else {
			    		$('i', this.parentNode).attr( 'class', 'icon-plus' );
			    		$('div.innerDetailsGroup', $(nTrGroup).next()[0]).slideUp( function () {
			      			partsSubTable.fnClose( nTrGroup );
			      			anOpenGroup.splice( iGroup, 1 );
			    		});
					}
				});
			});
		</g:javascript>
	</head>
	<body>
		<div class="innerDetails">
			<table id="parts_datatables_sub_${idTable}" cellpadding="0" cellspacing="0" 
				border="0"
				class="display table table-striped table-bordered table-hover"
				style="table-layout: fixed;"
				width="100%">
				<thead>
					<tr>
						<th>
							<div style="width: 40px;">&nbsp;</div>
						</th>							        
						<th style="border-bottom: none;">
			            	<div style="width: 930px;">Group Good</div>
						</th>			
						<th style="border-bottom: none;">
			            	<div style="width: 100px;">Sub Total</div>
						</th>						
					</tr>
				</thead>
			</table>
		</div>
	</body>
</html>
