<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="parts_datatables_${idTable}" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	style="table-layout: fixed;"
	width="100%">
	<thead>
    	<tr>
			<th>
				<div style="width: 40px;">&nbsp;</div>
			</th>
        	<th style="border-bottom: none;">
            	<div style="width: 100px;">Tgl Parts Slip</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 125px;">No. Parts Slip</div>
			</th>			
			<th style="border-bottom: none;">
            	<div style="width: 125px;">Nomor WO</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 125px;">Nopol</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 125px;">Customer</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 250px;">Alamat</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 100px;">Grand Total</div>
			</th>
		</tr>
	</thead>
</table>
<g:javascript>
var partsTable;
var reloadPartsTable;
var msg='';

function searchData() {
    if(isValid()){
        reloadPartsTable();
    }else{
        alert(msg);
        return false
    }

}

$(function() {
    isValid = function(){
        msg='';

        var chkTanggal = $('input[name=chkTanggalParts]').is(':checked');
        if(chkTanggal){
            var tanggalStart = $("#tanggalParts_start").val();
            var tanggalEnd = $("#tanggalParts_end").val();
            if(tanggalStart==''){msg+='- Silahkan pilih tanggal awal parts slip \n';}
            if(tanggalEnd==''){msg+='- Silahkan pilih tanggal akhir parts slip \n';}
        }

        var chkNomorParts = $('input[name=chkNomorParts]').is(':checked');
        if(chkNomorParts){
            var nomorParts = $('input[name=nomorParts]').val();
            if(nomorParts==''){msg+='- Silahkan masukkan nomor parts slip \n';}
        }

        var chkNomorWO = $('input[name=chkNomorWO]').is(':checked');
        if(chkNomorWO){
            var nomorWO = $('input[name=nomorWO]').val();
            if(nomorWO==''){msg+='- Silahkan masukkan nomor WO \n';}
        }

        var chkNomorPolisi = $('input[name=chkNomorPolisi]').is(':checked');
        if(chkNomorPolisi){
            var nomorPolisi = $('input[name=nomorPolisi]').val();
            if(nomorPolisi==''){msg+='- Silahkan masukkan nomor polisi \n';}
        }

        return msg=='';
    };

	reloadPartsTable = function() {
		partsTable.fnDraw();
	};
	
	partsTable = $('#parts_datatables_${idTable}').dataTable({
		"bScrollCollapse": false,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		},
	    "fnRowCallback": function (nRow) {
			return nRow;
	    },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "partsSlipDataTablesList")}",
		"aoColumns": [
			//radioButton
			{
				"mDataProp": "noPartsSlip",
		        "sClass": "control center",
		        "bSortable": false,
		        "sWidth":"40px",
				"mRender": function ( data, type, row ) {
					return '<input type="radio" name="radioButton" class="pull-left row-select" value="'+row['noPartsSlip']+'" style="margin-top: 4px;"><input type="hidden" value="'+row['noPartsSlip']+'">&nbsp;&nbsp;<i class="icon-plus" style="cursor: pointer;">';
				}
			},	
			//tanggalPartsSlip
			{
				"sName": "tanggalPartsSlip",
				"mDataProp": "tanggalPartsSlip",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			//noPartsSlip
			{
				"sName": "noPartsSlip",
				"mDataProp": "noPartsSlip",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//nomorWO
			{
				"sName": "nomorWO",
				"mDataProp": "nomorWO",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//noPol
			{
				"sName": "noPol",
				"mDataProp": "noPol",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},			
			//customer
			{
				"sName": "customer",
				"mDataProp": "customer",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//alamat
			{
				"sName": "alamat",
				"mDataProp": "alamat",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"250px",
				"bVisible": true
			},
			//grandTotal
			{
				"sName": "grandTotal",
				"mDataProp": "grandTotal",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			}		
		],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			var tanggalStart = $("#tanggalParts_start").val();
			var tanggalStartDay = $('#tanggalParts_start_day').val();
			var tanggalStartMonth = $('#tanggalParts_start_month').val();
			var tanggalStartYear = $('#tanggalParts_start_year').val();
			var chkTanggal = $('input[name=chkTanggalParts]').is(':checked');
            if(tanggalStart && chkTanggal) {
				aoData.push(
						{"name": 'tanggalStart', "value": "date.struct"},
						{"name": 'tanggalStart_dp', "value": tanggalStart},
						{"name": 'tanggalStart_day', "value": tanggalStartDay},
						{"name": 'tanggalStart_month', "value": tanggalStartMonth},
						{"name": 'tanggalStart_year', "value": tanggalStartYear},
						{"name": 'chkTanggalParts', "value": true}
				);
			}
			
            var tanggalEnd = $("#tanggalParts_end").val();
            var tanggalEndDay = $('#tanggalParts_end_day').val();
			var tanggalEndMonth = $('#tanggalParts_end_month').val();
			var tanggalEndYear = $('#tanggalParts_end_year').val();
			if(tanggalEnd && chkTanggal) {
				aoData.push(
						{"name": 'tanggalEnd', "value": "date.struct"},
						{"name": 'tanggalEnd_dp', "value": tanggalEnd},
						{"name": 'tanggalEnd_day', "value": tanggalEndDay},
						{"name": 'tanggalEnd_month', "value": tanggalEndMonth},
						{"name": 'tanggalEnd_year', "value": tanggalEndYear}
				);
			}			
			
            var nomorParts = $('input[name=nomorParts]').val();
            var chkNomorParts = $('input[name=chkNomorParts]').is(':checked');
            if(nomorParts && chkNomorParts) {
				aoData.push(
						{"name": 'nomorParts', "value": nomorParts},
						{"name": 'chkNomorParts', "value": true}
				);
			}
            
			var nomorWO = $('input[name=nomorWO]').val();
			var chkNomorWO = $('input[name=chkNomorWO]').is(':checked');
            if(nomorWO && chkNomorWO) {
				aoData.push(
						{"name": 'nomorWO', "value": nomorWO},
						{"name": 'chkNomorWO', "value": true}
				);
			}
			
			var nomorPolisi = $('input[name=nomorPolisi]').val();
			var chkNomorPolisi = $('input[name=chkNomorPolisi]').is(':checked');
            if(nomorPolisi && chkNomorPolisi) {
				aoData.push(
						{"name": 'nomorPolisi', "value": nomorPolisi},
						{"name": 'chkNomorPolisi', "value": true}
				);
			}					
			
			$.ajax({ "dataType": 'json',
				"type": "POST",
				"url": sSource,
				"data": aoData ,
				"success": function (json) {
					fnCallback(json);
				},
				"complete": function () {}
			});			
		}
	});
	
    var anOpen = [];
	$("#parts_datatables_${idTable} tbody").delegate("tr i", "click", function (e) {
		e.preventDefault();		
   		var nTr = this.parentNode.parentNode;		
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this.parentNode).attr( 'class', "icon-minus" );
    		var oData = partsTable.fnGetData(nTr);			
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${request.contextPath}/slipList/partsSlipSubList',
	   			success:function(data){
	   				var nDetailsRow = partsTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(){
                    alert('Data not found');
	   			    return false;
	   			},
	   			complete:function(){
	   				$('#spinner').fadeOut();
	   			}
	   		});	
		} else {
    		$('i', this.parentNode).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			partsTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
		}
	});	
});
</g:javascript>