<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="kuitansi_datatables_${idTable}" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	style="table-layout: fixed;"
	width="100%">
	<thead>
    	<tr>
			<th>
				<div style="width: 30px;">&nbsp;</div>
			</th>
        	<th style="border-bottom: none;">
            	<div style="width: 100px;">Tgl Kuitansi</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 125px;">No. Kuitansi</div>
			</th>			
			<th style="border-bottom: none;">
            	<div style="width: 125px;">Jenis Bayar</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 125px;">Nomor WO</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 125px;">No. Customer</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 125px;">Terima Dari</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 200px;">Untuk Pembayaran</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 100px;">Total Bayar</div>
			</th>
			<th style="border-bottom: none;">
            	<div style="width: 100px;">Kasir</div>
			</th>
		</tr>
	</thead>
</table>
<g:javascript>
var kuitansiTable;
var reloadKuitansiTable;

function searchData() {
    reloadKuitansiTable();
}

$(function() {
	reloadKuitansiTable = function() {
		kuitansiTable.fnDraw();
	}
	
	kuitansiTable = $('#kuitansi_datatables_${idTable}').dataTable({
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		},
	    "fnRowCallback": function (nRow) {
			return nRow;
	    },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "kuitansiDataTablesList")}",
		"aoColumns": [
			//radioButton
			{
				"mDataProp": "noKuitansi",
		        "sClass": "control center",
		        "bSortable": false,
		        "sWidth":"30px",
				"mRender": function ( data, type, row ) {
					return '<input type="radio" name="radioButton" class="pull-left row-select" value="'+row['noKuitansi']+'" style="margin-top: 4px;"><input type="hidden" value="'+row['noKuitansi']+'">&nbsp;&nbsp;<i class="icon-plus" style="cursor: pointer">';
				}
			},
			//tanggalKuitansi
			{
				"sName": "tanggalKuitansi",
				"mDataProp": "tanggalKuitansi",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			//noKuitansi
			{
				"sName": "noKuitansi",
				"mDataProp": "noKuitansi",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//jenisBayar
			{
				"sName": "jenisBayar",
				"mDataProp": "jenisBayar",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//nomorWO
			{
				"sName": "nomorWO",
				"mDataProp": "nomorWO",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//noCustomer
			{
				"sName": "noCustomer",
				"mDataProp": "noCustomer",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//terimaDari
			{
				"sName": "terimaDari",
				"mDataProp": "terimaDari",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"125px",
				"bVisible": true
			},
			//untukPembayaran
			{
				"sName": "untukPembayaran",
				"mDataProp": "untukPembayaran",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"200px",
				"bVisible": true
			},
			//totalBayar
			{
				"sName": "totalBayar",
				"mDataProp": "totalBayar",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			//kasir
			{
				"sName": "kasir",
				"mDataProp": "kasir",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			}
		],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			var tanggalKuitansiStart = $("#tanggalKuitansi_start").val();
			var tanggalKuitansiStartDay = $('#tanggalKuitansi_start_day').val();
			var tanggalKuitansiStartMonth = $('#tanggalKuitansi_start_month').val();
			var tanggalKuitansiStartYear = $('#tanggalKuitansi_start_year').val();
			var chkTanggal = $('input[name=chkTanggalKuitansi]').is(':checked');
            if(tanggalKuitansiStart && chkTanggal) {
				aoData.push(
						{"name": 'tanggalKuitansiStart', "value": "date.struct"},
						{"name": 'tanggalKuitansiStart_dp', "value": tanggalKuitansiStart},
						{"name": 'tanggalKuitansiStart_day', "value": tanggalKuitansiStartDay},
						{"name": 'tanggalKuitansiStart_month', "value": tanggalKuitansiStartMonth},
						{"name": 'tanggalKuitansiStart_year', "value": tanggalKuitansiStartYear},
						{"name": 'chkTanggalKuitansi', "value": true}
				);
			}

            var tanggalKuitansiEnd = $("#tanggalKuitansi_end").val();
            var tanggalKuitansiEndDay = $('#tanggalKuitansi_end_day').val();
			var tanggalKuitansiEndMonth = $('#tanggalKuitansi_end_month').val();
			var tanggalKuitansiEndYear = $('#tanggalKuitansi_end_year').val();
			if(tanggalKuitansiEnd && chkTanggal) {
				aoData.push(
						{"name": 'tanggalKuitansiEnd', "value": "date.struct"},
						{"name": 'tanggalKuitansiEnd_dp', "value": tanggalKuitansiEnd},
						{"name": 'tanggalKuitansiEnd_day', "value": tanggalKuitansiEndDay},
						{"name": 'tanggalKuitansiEnd_month', "value": tanggalKuitansiEndMonth},
						{"name": 'tanggalKuitansiEnd_year', "value": tanggalKuitansiEndYear}
				);
			}

            var jenisBayar = $('input[name=jenisBayar]').val();
			if(jenisBayar == '')jenisBayar='SEMUA';
            if(jenisBayar && $('input[name=chkJenisBayar]').is(':checked')) {
				aoData.push(
						{"name": 'jenisBayar', "value": jenisBayar},
						{"name": 'chkJenisBayar', "value": true}
				);
			}

			var metodeBayar = $('input[name=metodeBayar]').val();
			if(metodeBayar == '')metodeBayar='SEMUA';
            if(metodeBayar && $('input[name=chkMetodeBayar]').is(':checked')) {
				aoData.push(
						{"name": 'metodeBayar', "value": metodeBayar},
						{"name": 'chkMetodeBayar', "value": true}
				);
			}

			var nomorKuitansi = $('input[name=nomorKuitansi]').val();
            if(nomorKuitansi && $('input[name=chkNomorKuitansi]').is(':checked')) {
				aoData.push(
						{"name": 'nomorKuitansi', "value": nomorKuitansi},
						{"name": 'chkNomorKuitansi', "value": true}
				);
			}

			var namaKasir = $('input[name=namaKasir]').val();
            if(namaKasir && $('input[name=chkNamaKasir]').is(':checked')) {
				aoData.push(
						{"name": 'namaKasir', "value": namaKasir},
						{"name": 'chkNamaKasir', "value": true}
				);
			}

			$.ajax({
			    "dataType": 'json',
				"type": "POST",
				"url": sSource,
				"data": aoData ,
				"success": function (json) {
					fnCallback(json);
				},
				"complete": function () {}
			});

		}
	});

    var anOpen = [];
	$("#kuitansi_datatables_${idTable} tbody").delegate("tr i", "click", function (e) {
		e.preventDefault();		
   		var nTr = this.parentNode.parentNode;		
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this.parentNode).attr( 'class', "icon-minus" );
    		var oData = kuitansiTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${request.contextPath}/slipList/kuitansiSubList',
	   			success:function(data){
	   				var nDetailsRow = kuitansiTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(){
	   			    alert('Data not found');
	   			    return false;
	   			},
	   			complete:function(){
	   				$('#spinner').fadeOut();
	   			}
	   		});	
		} else {
    		$('i', this.parentNode).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			kuitansiTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
		}
	});
		
});
</g:javascript>