<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
		<g:javascript>
			var kuitansiSubTable;
			$(function() {
			    if(kuitansiSubTable){
					kuitansiSubTable.dataTable().fnDestroy();
				}
				
				kuitansiSubTable = $('#kuitansi_datatables_sub_${idTable}').dataTable({
					"sScrollX": "1024px",
					"bScrollCollapse": true,
					"bAutoWidth" : false,
					"bPaginate" : false,
					"bInfo" : false,
					"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
					"bFilter": false,
					"bStateSave": false,
					'sPaginationType': 'bootstrap',
					"fnInitComplete": function () {
						this.fnAdjustColumnSizing(true);
					},
				    "fnRowCallback": function (nRow) {
						return nRow;
				    },
					"bSort": true,
					"bProcessing": true,
					"bServerSide": true,
					"sServerMethod": "POST",
					"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
					"sAjaxSource": '${request.contextPath}/slipList/kuitansiDataTablesSubList?tglKui=${tglKui}&id=${id}',
					"aoColumns": [
						//blank
						{
					    	"mDataProp": null,
					        "bSortable": false,
					        "sWidth":"289px",
					        "sDefaultContent": ''
						},					
						//metodeBayar
						{
							"sName": "metodeBayar",
							"mDataProp": "metodeBayar",
							"bSearchable": false,
							"bSortable": false,
							"sWidth":"125px",
							"bVisible": true
						},
						//namaBank
						{
							"sName": "namaBank",
							"mDataProp": "namaBank",
							"bSearchable": false,
							"bSortable": false,
							"sWidth":"125px",
							"bVisible": true
						},
						//mesinEdc
						{
							"sName": "mesinEdc",
							"mDataProp": "mesinEdc",
							"bSearchable": false,
							"bSortable": false,
							"sWidth":"125px",
							"bVisible": true
						},
						//nomorKartu
						{
							"sName": "nomorKartu",
							"mDataProp": "nomorKartu",
							"bSearchable": false,
							"bSortable": false,
							"sWidth":"342px",
							"bVisible": true
						},
						//bankCharge
						{
							"sName": "bankCharge",
							"mDataProp": "bankCharge",
							"bSearchable": false,
							"bSortable": false,
							"sWidth":"100px",
							"bVisible": true
						},
						//jumlah
						{
							"sName": "jumlah",
							"mDataProp": "jumlah",
							"bSearchable": false,
							"bSortable": false,
							"sWidth":"100px",
							"bVisible": true
						}
					],
					"fnServerData": function ( sSource, aoData, fnCallback ) {
												
						var tanggalKuitansiStart = $("#tanggalKuitansi_start").val();
						var tanggalKuitansiStartDay = $('#tanggalKuitansi_start_day').val();
						var tanggalKuitansiStartMonth = $('#tanggalKuitansi_start_month').val();
						var tanggalKuitansiStartYear = $('#tanggalKuitansi_start_year').val();
						var chkTanggal = $('input[name=chkTanggalKuitansi]').is(':checked');
			            if(tanggalKuitansiStart && chkTanggal) {
							aoData.push(
									{"name": 'tanggalKuitansiStart', "value": "date.struct"},
									{"name": 'tanggalKuitansiStart_dp', "value": tanggalKuitansiStart},
									{"name": 'tanggalKuitansiStart_day', "value": tanggalKuitansiStartDay},
									{"name": 'tanggalKuitansiStart_month', "value": tanggalKuitansiStartMonth},
									{"name": 'tanggalKuitansiStart_year', "value": tanggalKuitansiStartYear},
									{"name": 'chkTanggalKuitansi', "value": true}
							);
						}
			
			            var tanggalKuitansiEnd = $("#tanggalKuitansi_end").val();
			            var tanggalKuitansiEndDay = $('#tanggalKuitansi_end_day').val();
						var tanggalKuitansiEndMonth = $('#tanggalKuitansi_end_month').val();
						var tanggalKuitansiEndYear = $('#tanggalKuitansi_end_year').val();
						if(tanggalKuitansiEnd && chkTanggal) {
							aoData.push(
									{"name": 'tanggalKuitansiEnd', "value": "date.struct"},
									{"name": 'tanggalKuitansiEnd_dp', "value": tanggalKuitansiEnd},
									{"name": 'tanggalKuitansiEnd_day', "value": tanggalKuitansiEndDay},
									{"name": 'tanggalKuitansiEnd_month', "value": tanggalKuitansiEndMonth},
									{"name": 'tanggalKuitansiEnd_year', "value": tanggalKuitansiEndYear}
							);
						}			
			
			            var jenisBayar = $('input[name=jenisBayar]').val();
						if(jenisBayar == '')jenisBayar='SEMUA';
			            if($('input[name=chkJenisBayar]').is(':checked')) {
							aoData.push(
									{"name": 'jenisBayar', "value": jenisBayar},
									{"name": 'chkJenisBayar', "value": true}
							);
						}
            
						var metodeBayar = $('input[name=metodeBayar]').val();
						if(metodeBayar == '')metodeBayar='SEMUA';
			            if($('input[name=chkMetodeBayar]').is(':checked')) {
							aoData.push(
									{"name": 'metodeBayar', "value": metodeBayar},
									{"name": 'chkMetodeBayar', "value": true}
							);
						}
			
						var nomorKuitansi = $('input[name=nomorKuitansi]').val();
			            if($('input[name=chkNomorKuitansi]').is(':checked')) {
							aoData.push(
									{"name": 'nomorKuitansi', "value": nomorKuitansi},
									{"name": 'chkNomorKuitansi', "value": true}
							);
						}
			
						var namaKasir = $('input[name=namaKasir]').val();
			            if($('input[name=chkNamaKasir]').is(':checked')) {
							aoData.push(
									{"name": 'namaKasir', "value": namaKasir},
									{"name": 'chkNamaKasir', "value": true}
							);
						}
			
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							},
							"complete": function () {}
						});
					}
				});
			});
		</g:javascript>
	</head>
	<body>
		<div class="innerDetails">
			<table id="kuitansi_datatables_sub_${idTable}" cellpadding="0" cellspacing="0" 
				border="0"
				class="display table table-striped table-bordered table-hover"
				style="table-layout: fixed;"
				width="100%">
				<thead>
					<tr>
						<th>
							<div style="width: 289px;">&nbsp;</div>
						</th>							        
						<th style="border-bottom: none;">
			            	<div style="width: 100px;">Metode Bayar</div>
						</th>
						<th style="border-bottom: none;">
			            	<div style="width: 125px;">Nama Bank</div>
						</th>
						<th style="border-bottom: none;">
			            	<div style="width: 125px;">Mesin EDC</div>
						</th>
						<th style="border-bottom: none;">
			            	<div style="width: 342px;">Nomor Kartu</div>
						</th>
						<th style="border-bottom: none;">
			            	<div style="width: 100px;">Bank Charge</div>
						</th>
						<th style="border-bottom: none;">
			            	<div style="width: 100px;">Jumlah</div>
						</th>
					</tr>
				</thead>
			</table>
		</div>
	</body>
</html>
