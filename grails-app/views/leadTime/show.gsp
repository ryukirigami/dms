

<%@ page import="com.kombos.administrasi.LeadTime" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'leadTime.label', default: 'Lead Time')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteLeadTime;

$(function(){ 
	deleteLeadTime=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/leadTime/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadLeadTimeTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-leadTime" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="leadTime"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${leadTimeInstance?.m035TglBerlaku}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m035TglBerlaku-label" class="property-label"><g:message
					code="leadTime.m035TglBerlaku.label" default="Tanggal Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m035TglBerlaku-label">
						%{--<ba:editableValue
								bean="${leadTimeInstance}" field="m035TglBerlaku"
								url="${request.contextPath}/LeadTime/updatefield" type="text"
								title="Enter m035TglBerlaku" onsuccess="reloadLeadTimeTable();" />--}%
							
								<g:formatDate date="${leadTimeInstance?.m035TglBerlaku}" />
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${leadTimeInstance?.m035LTReception}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m035LTReception-label" class="property-label"><g:message
                                code="leadTime.m035LTReception.label" default="Leadtime Reception" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m035LTReception-label">
                        %{--<ba:editableValue
                                bean="${leadTimeInstance}" field="m035LTReception"
                                url="${request.contextPath}/LeadTime/updatefield" type="text"
                                title="Enter m035LTReception" onsuccess="reloadLeadTimeTable();" />--}%

                        <g:fieldValue bean="${leadTimeInstance}" field="m035LTReception" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${leadTimeInstance?.m035LTPreDiagnose}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m035LTPreDiagnose-label" class="property-label"><g:message
                                code="leadTime.m035LTPreDiagnose.label" default="Leadtime Pre Diagnose" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m035LTPreDiagnose-label">
                        %{--<ba:editableValue
                                bean="${leadTimeInstance}" field="m035LTPreDiagnose"
                                url="${request.contextPath}/LeadTime/updatefield" type="text"
                                title="Enter m035LTPreDiagnose" onsuccess="reloadLeadTimeTable();" />--}%

                        <g:fieldValue bean="${leadTimeInstance}" field="m035LTPreDiagnose" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${leadTimeInstance?.m035LTProduction}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m035LTProduction-label" class="property-label"><g:message
                                code="leadTime.m035LTProduction.label" default="Leadtime Production" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m035LTProduction-label">
                        %{--<ba:editableValue
                                bean="${leadTimeInstance}" field="m035LTProduction"
                                url="${request.contextPath}/LeadTime/updatefield" type="text"
                                title="Enter m035LTProduction" onsuccess="reloadLeadTimeTable();" />--}%

                        <g:fieldValue bean="${leadTimeInstance}" field="m035LTProduction" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${leadTimeInstance?.m035LTIDR}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m035LTIDR-label" class="property-label"><g:message
                                code="leadTime.m035LTIDR.label" default="Leadtime Inspection During Repair (IDR)" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m035LTIDR-label">
                        %{--<ba:editableValue
                                bean="${leadTimeInstance}" field="m035LTIDR"
                                url="${request.contextPath}/LeadTime/updatefield" type="text"
                                title="Enter m035LTIDR" onsuccess="reloadLeadTimeTable();" />--}%

                        <g:fieldValue bean="${leadTimeInstance}" field="m035LTIDR" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${leadTimeInstance?.m035LTFinalInspection}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m035LTFinalInspection-label" class="property-label"><g:message
                                code="leadTime.m035LTFinalInspection.label" default="Leadtime Final Inspection" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m035LTFinalInspection-label">
                        %{--<ba:editableValue
                                bean="${leadTimeInstance}" field="m035LTFinalInspection"
                                url="${request.contextPath}/LeadTime/updatefield" type="text"
                                title="Enter m035LTFinalInspection" onsuccess="reloadLeadTimeTable();" />--}%

                        <g:fieldValue bean="${leadTimeInstance}" field="m035LTFinalInspection" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${leadTimeInstance?.m035Invoicing}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m035Invoicing-label" class="property-label"><g:message
                                code="leadTime.m035Invoicing.label" default="Leadtime Invoicing" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m035Invoicing-label">
                        %{--<ba:editableValue
                                bean="${leadTimeInstance}" field="m035Invoicing"
                                url="${request.contextPath}/LeadTime/updatefield" type="text"
                                title="Enter m035Invoicing" onsuccess="reloadLeadTimeTable();" />--}%

                        <g:fieldValue bean="${leadTimeInstance}" field="m035Invoicing" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${leadTimeInstance?.m035Washing}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m035Washing-label" class="property-label"><g:message
                                code="leadTime.m035Washing.label" default="Leadtime Washing" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m035Washing-label">
                        %{--<ba:editableValue
                                bean="${leadTimeInstance}" field="m035Washing"
                                url="${request.contextPath}/LeadTime/updatefield" type="text"
                                title="Enter m035Washing" onsuccess="reloadLeadTimeTable();" />--}%

                        <g:fieldValue bean="${leadTimeInstance}" field="m035Washing" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${leadTimeInstance?.m035Delivery}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m035Delivery-label" class="property-label"><g:message
                                code="leadTime.m035Delivery.label" default="Leadtime Delivery" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m035Delivery-label">
                        %{--<ba:editableValue
                                bean="${leadTimeInstance}" field="m035Delivery"
                                url="${request.contextPath}/LeadTime/updatefield" type="text"
                                title="Enter m035Delivery" onsuccess="reloadLeadTimeTable();" />--}%

                        <g:fieldValue bean="${leadTimeInstance}" field="m035Delivery" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${leadTimeInstance?.m035JobStopped}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m035JobStopped-label" class="property-label"><g:message
                                code="leadTime.m035JobStopped.label" default="Job Stopped" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m035JobStopped-label">
                        %{--<ba:editableValue
                                bean="${leadTimeInstance}" field="m035JobStopped"
                                url="${request.contextPath}/LeadTime/updatefield" type="text"
                                title="Enter m035JobStopped" onsuccess="reloadLeadTimeTable();" />--}%

                        <g:fieldValue bean="${leadTimeInstance}" field="m035JobStopped" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${leadTimeInstance?.m035WFReception}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m035WFReception-label" class="property-label"><g:message
                                code="leadTime.m035WFReception.label" default="Waiting For Reception" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m035WFReception-label">
                        %{--<ba:editableValue
                                bean="${leadTimeInstance}" field="m035WFReception"
                                url="${request.contextPath}/LeadTime/updatefield" type="text"
                                title="Enter m035WFReception" onsuccess="reloadLeadTimeTable();" />--}%

                        <g:fieldValue bean="${leadTimeInstance}" field="m035WFReception" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${leadTimeInstance?.m035WFJobDispatch}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m035WFJobDispatch-label" class="property-label"><g:message
                                code="leadTime.m035WFJobDispatch.label" default="Waiting For Job Dispatch" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m035WFJobDispatch-label">
                        %{--<ba:editableValue
                                bean="${leadTimeInstance}" field="m035WFJobDispatch"
                                url="${request.contextPath}/LeadTime/updatefield" type="text"
                                title="Enter m035WFJobDispatch" onsuccess="reloadLeadTimeTable();" />--}%

                        <g:fieldValue bean="${leadTimeInstance}" field="m035WFJobDispatch" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${leadTimeInstance?.m035WFClockOn}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m035WFClockOn-label" class="property-label"><g:message
                                code="leadTime.m035WFClockOn.label" default="Waiting For Clock On" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m035WFClockOn-label">
                        %{--<ba:editableValue
                                bean="${leadTimeInstance}" field="m035WFClockOn"
                                url="${request.contextPath}/LeadTime/updatefield" type="text"
                                title="Enter m035WFClockOn" onsuccess="reloadLeadTimeTable();" />--}%

                        <g:fieldValue bean="${leadTimeInstance}" field="m035WFClockOn" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${leadTimeInstance?.m035WFFinalInspection}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m035WFFinalInspection-label" class="property-label"><g:message
                                code="leadTime.m035WFFinalInspection.label" default="Waiting For Final Inspection" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m035WFFinalInspection-label">
                        %{--<ba:editableValue
                                bean="${leadTimeInstance}" field="m035WFFinalInspection"
                                url="${request.contextPath}/LeadTime/updatefield" type="text"
                                title="Enter m035WFFinalInspection" onsuccess="reloadLeadTimeTable();" />--}%

                        <g:fieldValue bean="${leadTimeInstance}" field="m035WFFinalInspection" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${leadTimeInstance?.m035WFInvoicing}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m035WFInvoicing-label" class="property-label"><g:message
                                code="leadTime.m035WFInvoicing.label" default="Waiting For Invoicing" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m035WFInvoicing-label">
                        %{--<ba:editableValue
                                bean="${leadTimeInstance}" field="m035WFInvoicing"
                                url="${request.contextPath}/LeadTime/updatefield" type="text"
                                title="Enter m035WFInvoicing" onsuccess="reloadLeadTimeTable();" />--}%

                        <g:fieldValue bean="${leadTimeInstance}" field="m035WFInvoicing" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${leadTimeInstance?.m035WFWashing}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m035WFWashing-label" class="property-label"><g:message
                                code="leadTime.m035WFWashing.label" default="Waiting For Washing" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m035WFWashing-label">
                        %{--<ba:editableValue
                                bean="${leadTimeInstance}" field="m035WFWashing"
                                url="${request.contextPath}/LeadTime/updatefield" type="text"
                                title="Enter m035WFWashing" onsuccess="reloadLeadTimeTable();" />--}%

                        <g:fieldValue bean="${leadTimeInstance}" field="m035WFWashing" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${leadTimeInstance?.m035WFNotifikasi}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m035WFNotifikasi-label" class="property-label"><g:message
                                code="leadTime.m035WFNotifikasi.label" default="Waiting For Notifikasi" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m035WFNotifikasi-label">
                        %{--<ba:editableValue
                                bean="${leadTimeInstance}" field="m035WFNotifikasi"
                                url="${request.contextPath}/LeadTime/updatefield" type="text"
                                title="Enter m035WFNotifikasi" onsuccess="reloadLeadTimeTable();" />--}%

                        <g:fieldValue bean="${leadTimeInstance}" field="m035WFNotifikasi" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${leadTimeInstance?.m035WFDelivery}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m035WFDelivery-label" class="property-label"><g:message
                                code="leadTime.m035WFDelivery.label" default="Waiting For Delivery" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m035WFDelivery-label">
                        %{--<ba:editableValue
                                bean="${leadTimeInstance}" field="m035WFDelivery"
                                url="${request.contextPath}/LeadTime/updatefield" type="text"
                                title="Enter m035WFDelivery" onsuccess="reloadLeadTimeTable();" />--}%

                        <g:fieldValue bean="${leadTimeInstance}" field="m035WFDelivery" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${leadTimeInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="leadTime.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${leadTimeInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/LeadTime/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadLeadTimeTable();" />--}%

                        <g:fieldValue bean="${leadTimeInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${leadTimeInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="leadTime.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${leadTimeInstance}" field="dateCreated"
                                url="${request.contextPath}/LeadTime/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadLeadTimeTable();" />--}%

                        <g:fieldValue bean="${leadTimeInstance}" field="dateCreated" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${leadTimeInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="leadTime.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${leadTimeInstance}" field="createdBy"
                                url="${request.contextPath}/LeadTime/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadLeadTimeTable();" />--}%

                        <g:fieldValue bean="${leadTimeInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${leadTimeInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="leadTime.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${leadTimeInstance}" field="lastUpdated"
                                url="${request.contextPath}/LeadTime/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadLeadTimeTable();" />--}%

                        <g:fieldValue bean="${leadTimeInstance}" field="lastUpdated" />

                    </span></td>

                </tr>
            </g:if>


            <g:if test="${leadTimeInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="leadTime.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${leadTimeInstance}" field="updatedBy"
                                url="${request.contextPath}/LeadTime/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadLeadTimeTable();" />--}%

                        <g:fieldValue bean="${leadTimeInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

            </tbody>
        </table>
    <g:form class="form-horizontal">
        <fieldset class="buttons controls">
            <a class="btn cancel" href="javascript:void(0);"
               onclick="expandTableLayout();"><g:message
                    code="default.button.cancel.label" default="Cancel" /></a>
            <g:remoteLink class="btn btn-primary edit" action="edit"
                          id="${leadTimeInstance?.id}"
                          update="[success:'leadTime-form',failure:'leadTime-form']"
                          on404="alert('not found');">
                <g:message code="default.button.edit.label" default="Edit" />
            </g:remoteLink>
            <ba:confirm id="delete" class="btn cancel"
                        message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
                        onsuccess="deleteLeadTime('${leadTimeInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
        </fieldset>
    </g:form>
    </div>
</body>
</html>
