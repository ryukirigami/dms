<%@ page import="com.kombos.administrasi.CompanyDealer; com.kombos.administrasi.LeadTime" %>


<g:javascript disposition="head">
    $(function(){
        $("#m035TglBerlaku_date").datepicker();
    });
</g:javascript>
<div class="span6">
<div class="control-group fieldcontain ${hasErrors(bean: leadTimeInstance, field: 'm035TglBerlaku', 'error')} required">
	<label class="control-label" for="m035TglBerlaku">
		<g:message code="leadTime.m035TglBerlaku.label" default="Tanggal Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:datePicker name="m035TglBerlaku" precision="day"  value="${leadTimeInstance?.m035TglBerlaku}"  />--}%
    %{
        if(request.getRequestURI().contains("edit")){
    }%
        <input type="hidden" name="m035TglBerlaku" id="m035TglBerlaku" value="${leadTimeInstance?.m035TglBerlaku}" />
        <input type="text" disabled="" name="m035TglBerlaku_temp" id="m035TglBerlaku_temp" value="${leadTimeInstance?.m035TglBerlaku}" />
    %{
        }else{
    }%
        <input type="hidden" name="m035TglBerlaku" id="m035TglBerlaku" value="${leadTimeInstance?.m035TglBerlaku}" />
        <input type="text" data-date-format="dd/mm/yyyy" id="m035TglBerlaku_date" name="m035TglBerlaku_date"/>
    %{
        }
    }%
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: leadTimeInstance, field: 'm035LTReception', 'error')} required">
	<label class="control-label" for="m035LTReception">
		<g:message code="leadTime.m035LTReception.label" default="Leadtime Reception" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:datePicker name="m035LTReception" precision="day"  value="${leadTimeInstance?.m035LTReception}"  />--}%
        <select id="m035LTReception_hour" name="m035LTReception_hour" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035LTReception?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035LTReception?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m035LTReception_minute" name="m035LTReception_minute" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035LTReception?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035LTReception?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: leadTimeInstance, field: 'm035LTPreDiagnose', 'error')} required">
	<label class="control-label" for="m035LTPreDiagnose">
		<g:message code="leadTime.m035LTPreDiagnose.label" default="Leadtime Pre Diagnose" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:datePicker name="m035LTPreDiagnose" precision="day"  value="${leadTimeInstance?.m035LTPreDiagnose}"  />--}%
        <select id="m035LTPreDiagnose_hour" name="m035LTPreDiagnose_hour" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035LTPreDiagnose?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035LTPreDiagnose?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m035LTPreDiagnose_minute" name="m035LTPreDiagnose_minute" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035LTPreDiagnose?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035LTPreDiagnose?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: leadTimeInstance, field: 'm035LTProduction', 'error')} required">
	<label class="control-label" for="m035LTProduction">
		<g:message code="leadTime.m035LTProduction.label" default="Leadtime Production" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:datePicker name="m035LTProduction" precision="day"  value="${leadTimeInstance?.m035LTProduction}"  />--}%
        <select id="m035LTProduction_hour" name="m035LTProduction_hour" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035LTProduction?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035LTProduction?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m035LTProduction_minute" name="m035LTProduction_minute" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035LTProduction?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035LTProduction?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: leadTimeInstance, field: 'm035LTIDR', 'error')} required">
	<label class="control-label" for="m035LTIDR">
		<g:message code="leadTime.m035LTIDR.label" default="Leadtime Inspection During Repair (IDR)" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:datePicker name="m035LTIDR" precision="day"  value="${leadTimeInstance?.m035LTIDR}"  />--}%
        <select id="m035LTIDR_hour" name="m035LTIDR_hour" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035LTIDR?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035LTIDR?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m035LTIDR_minute" name="m035LTIDR_minute" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035LTIDR?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035LTIDR?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: leadTimeInstance, field: 'm035LTFinalInspection', 'error')} required">
	<label class="control-label" for="m035LTFinalInspection">
		<g:message code="leadTime.m035LTFinalInspection.label" default="Leadtime Final Inspection" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:datePicker name="m035LTFinalInspection" precision="day"  value="${leadTimeInstance?.m035LTFinalInspection}"  />--}%
        <select id="m035LTFinalInspection_hour" name="m035LTFinalInspection_hour" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035LTFinalInspection?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035LTFinalInspection?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m035LTFinalInspection_minute" name="m035LTFinalInspection_minute" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035LTFinalInspection?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035LTFinalInspection?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: leadTimeInstance, field: 'm035Invoicing', 'error')} required">
	<label class="control-label" for="m035Invoicing">
		<g:message code="leadTime.m035Invoicing.label" default="Leadtime Invoicing" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:datePicker name="m035Invoicing" precision="day"  value="${leadTimeInstance?.m035Invoicing}"  />--}%
        <select id="m035Invoicing_hour" name="m035Invoicing_hour" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035Invoicing?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035Invoicing?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m035Invoicing_minute" name="m035Invoicing_minute" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035Invoicing?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035Invoicing?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: leadTimeInstance, field: 'm035Washing', 'error')} required">
	<label class="control-label" for="m035Washing">
		<g:message code="leadTime.m035Washing.label" default="Leadtime Washing" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:datePicker name="m035Washing" precision="day"  value="${leadTimeInstance?.m035Washing}"  />--}%
        <select id="m035Washing_hour" name="m035Washing_hour" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035Washing?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035Washing?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m035Washing_minute" name="m035Washing_minute" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035Washing?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035Washing?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: leadTimeInstance, field: 'm035Delivery', 'error')} required">
	<label class="control-label" for="m035Delivery">
		<g:message code="leadTime.m035Delivery.label" default="Leadtime Delivery" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:datePicker name="m035Delivery" precision="day"  value="${leadTimeInstance?.m035Delivery}"  />--}%
        <select id="m035Delivery_hour" name="m035Delivery_hour" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035Delivery?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035Delivery?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m035Delivery_minute" name="m035Delivery_minute" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035Delivery?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035Delivery?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
	</div>
</div>
</div>
<div class="span6">
<div class="control-group fieldcontain ${hasErrors(bean: leadTimeInstance, field: 'm035JobStopped', 'error')} required">
	<label class="control-label" for="m035JobStopped">
		<g:message code="leadTime.m035JobStopped.label" default="Job Stopped" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:datePicker name="m035JobStopped" precision="day"  value="${leadTimeInstance?.m035JobStopped}"  />--}%
        <select id="m035JobStopped_hour" name="m035JobStopped_hour" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035JobStopped?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035JobStopped?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m035JobStopped_minute" name="m035JobStopped_minute" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035JobStopped?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035JobStopped?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: leadTimeInstance, field: 'm035WFReception', 'error')} required">
	<label class="control-label" for="m035WFReception">
		<g:message code="leadTime.m035WFReception.label" default="Wait For Reception" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:datePicker name="m035WFReception" precision="day"  value="${leadTimeInstance?.m035WFReception}"  />--}%
        <select id="m035WFReception_hour" name="m035WFReception_hour" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035WFReception?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035WFReception?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m035WFReception_minute" name="m035WFReception_minute" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035WFReception?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035WFReception?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: leadTimeInstance, field: 'm035WFJobDispatch', 'error')} required">
	<label class="control-label" for="m035WFJobDispatch">
		<g:message code="leadTime.m035WFJobDispatch.label" default="Wait For Job Dispatch" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:datePicker name="m035WFJobDispatch" precision="day"  value="${leadTimeInstance?.m035WFJobDispatch}"  />--}%
        <select id="m035WFJobDispatch_hour" name="m035WFJobDispatch_hour" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035WFJobDispatch?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035WFJobDispatch?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m035WFJobDispatch_minute" name="m035WFJobDispatch_minute" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035WFJobDispatch?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035WFJobDispatch?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: leadTimeInstance, field: 'm035WFClockOn', 'error')} required">
	<label class="control-label" for="m035WFClockOn">
		<g:message code="leadTime.m035WFClockOn.label" default="Wait For Clock On" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:datePicker name="m035WFClockOn" precision="day"  value="${leadTimeInstance?.m035WFClockOn}"  />--}%
        <select id="m035WFClockOn_hour" name="m035WFClockOn_hour" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035WFClockOn?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035WFClockOn?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m035WFClockOn_minute" name="m035WFClockOn_minute" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035WFClockOn?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035WFClockOn?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: leadTimeInstance, field: 'm035WFFinalInspection', 'error')} required">
	<label class="control-label" for="m035WFFinalInspection">
		<g:message code="leadTime.m035WFFinalInspection.label" default="Wait For Final Inspection" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:datePicker name="m035WFFinalInspection" precision="day"  value="${leadTimeInstance?.m035WFFinalInspection}"  />--}%
        <select id="m035WFFinalInspection_hour" name="m035WFFinalInspection_hour" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035WFFinalInspection?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035WFFinalInspection?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m035WFFinalInspection_minute" name="m035WFFinalInspection_minute" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035WFFinalInspection?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035WFFinalInspection?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: leadTimeInstance, field: 'm035WFInvoicing', 'error')} required">
	<label class="control-label" for="m035WFInvoicing">
		<g:message code="leadTime.m035WFInvoicing.label" default="Wait For Invoicing" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:datePicker name="m035WFInvoicing" precision="day"  value="${leadTimeInstance?.m035WFInvoicing}"  />--}%
        <select id="m035WFInvoicing_hour" name="m035WFInvoicing_hour" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035WFInvoicing?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035WFInvoicing?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m035WFInvoicing_minute" name="m035WFInvoicing_minute" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035WFInvoicing?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035WFInvoicing?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: leadTimeInstance, field: 'm035WFWashing', 'error')} required">
	<label class="control-label" for="m035WFWashing">
		<g:message code="leadTime.m035WFWashing.label" default="Wait For Washing" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:datePicker name="m035WFWashing" precision="day"  value="${leadTimeInstance?.m035WFWashing}"  />--}%
        <select id="m035WFWashing_hour" name="m035WFWashing_hour" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035WFWashing?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035WFWashing?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m035WFWashing_minute" name="m035WFWashing_minute" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035WFWashing?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035WFWashing?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: leadTimeInstance, field: 'm035WFNotifikasi', 'error')} required">
	<label class="control-label" for="m035WFNotifikasi">
		<g:message code="leadTime.m035WFNotifikasi.label" default="Wait For Notifikasi" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:datePicker name="m035WFNotifikasi" precision="day"  value="${leadTimeInstance?.m035WFNotifikasi}"  />--}%
        <select id="m035WFNotifikasi_hour" name="m035WFNotifikasi_hour" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035WFNotifikasi?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035WFNotifikasi?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m035WFNotifikasi_minute" name="m035WFNotifikasi_minute" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035WFNotifikasi?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035WFNotifikasi?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: leadTimeInstance, field: 'm035WFDelivery', 'error')} required">
	<label class="control-label" for="m035WFDelivery">
		<g:message code="leadTime.m035WFDelivery.label" default="Wait For Delivery" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:datePicker name="m035WFDelivery" precision="day"  value="${leadTimeInstance?.m035WFDelivery}"  />--}%
        <select id="m035WFDelivery_hour" name="m035WFDelivery_hour" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<24;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035WFDelivery?.getHours()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035WFDelivery?.getHours()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }

                }
            }%
        </select> H :
        <select id="m035WFDelivery_minute" name="m035WFDelivery_minute" style="width: 60px" required="" class="jam_m031">
            %{
                for (int i=0;i<60;i++){
                    if(i<10){
                        if(i==leadTimeInstance?.m035WFDelivery?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">0'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">0'+i+'</option>');
                        }

                    } else {
                        if(i==leadTimeInstance?.m035WFDelivery?.getMinutes()){
                            out.println('<option value="'+i+'" selected="">'+i+'</option>');
                        } else {
                            out.println('<option value="'+i+'">'+i+'</option>');
                        }
                    }
                }
            }%
        </select> m
	</div>
</div>
</div>

