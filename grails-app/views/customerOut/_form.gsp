<%@ page import="com.kombos.reception.CustomerIn" %>

<g:javascript>
    $('#findDataCustomer').click(function(){
       if($('#t183NoPolTengah').val()=="" || $('#t183NoPolTengah').val()==null
               || $('#t183NoPolBelakang').val()=="" || $('#t183NoPolBelakang').val()==null){
           alert('Data No Polisi Masih Ada yang Kosong');
           return false
       }
       var kodeKota = $('#noPol').val();
       var noTengah = $('#t183NoPolTengah').val();
       var noBelakang = $('#t183NoPolBelakang').val();
       setNoPol(kodeKota,noTengah,noBelakang);
    });


    function setNoPol(kodeKota,noTengah,noBelakang){
        $('#btnSave').attr('disabled',true);
        document.getElementById('lblAtas').innerHTML='';
        document.getElementById('lblBawah').innerHTML='';
        $.ajax({
    		url:'${request.contextPath}/customerOut/findNoPol',
    		type: "POST", // Always use POST when deleting data
    		data: {kode : kodeKota, tengah : noTengah, belakang : noBelakang},
    		dataType: 'json',
    		success: function(data,textStatus,xhr){
    		    if(data.length>0){
    		        if(data[0].staGatePas!=-1){
                        $('#btnSave').attr('disabled',false);
                        if(data[0].staGatePas==1){
                            $('#statusGatePass').val('OK')
                        }else{
//                            $('#statusGatePass').val('OK (Test Drive)')
                            $('#statusGatePass').val('-')
                        }
    		        }else{
    		            $('#statusGatePass').val('Belum Ada Gate Pass')
    		        }
    		        var tpss=data[0].custSurvey;
                    document.getElementById('lblAtas').innerHTML='';
                    document.getElementById('lblBawah').innerHTML='';
                    $('#namaPemakai').val(data[0].namaCustomer);
                    $('#mobil').val(data[0].mobil);
                    $('#warna').val(data[0].warna);
                    $('#tahun').val(data[0].tahun);
                    $('#idCustomerVehicle').val(data[0].idCV);
                    $("#idReception").val(data[0].idReception);
    		    }else if(data!=0){
                    $('#namaPemakai').val("");
                    $('#mobil').val("");
                    $('#warna').val("");
                    $('#tahun').val("");
                    $('#statusGatePass').val("");
                    $('#idCustomerVehicle').val("");
                    $("#idReception").val("-1");
    		    }
//    		    else{
//    		          $('#namaPemakai').val("");
//                    $('#mobil').val("");
//                    $('#warna').val("");
//                    $('#tahun').val("");
//                    $('#statusGatePass').val("");
//                    $('#idCustomerVehicle').val("");
//                    $("#idReception").val("-1");
//                    alert('Data dengan nomor polisi tersebut tidak ditemukan');
//    		    }
    		},
    		error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(data,textStatus){
   			}
		});
    }
    var isCharOnly;
    var isNumberKey;
    var nomorBelakang = $('#t183NoPolBelakang');
    nomorBelakang.bind('keypress', inputNoPolisi).keyup(function() {
       var val = $(this).val()
       $(this).val(val.toUpperCase())
    });
    function inputNoPolisi(e){
        var code = (e.keyCode ? e.keyCode : e.which);
    }


    $(function(){
        isCharOnly = function(e) {
           e = e || event;
            return /[a-zA-Z-]/i.test(
                    String.fromCharCode(e.charCode || e.keyCode)
            ) || !e.charCode && e.keyCode  < 48;
        }

        isNumberKey = function(evt){
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)){
                return false;
            }
            else{
                return true;
            }
        }

        $(".charonly").bind('keypress',isCharOnly);

        $(".numberonly").bind('keypress',isNumberKey);
    });
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: customerInInstance, field: 'customerVehicle', 'error')} required">
    <label class="control-label" for="noPol" style="font-size: 20px; height: 50px; text-align: left;line-height: 50px;">
        <g:message code="customerIn.nopol.label" default="NOPOL" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <table style="width: 60%">
            <tr style="width: 80%">
                <td>
                    <g:select style="width: 90px; font-size: 30px; height: 61px; border-radius: 0;" id="noPol" name="noPol.id" from="${com.kombos.administrasi.KodeKotaNoPol.createCriteria().list(){eq("staDel","0");order("m116ID")}}" optionValue="m116ID" optionKey="m116ID" required="" value="${(!request.getRequestURI().contains("edit"))?"":customerInInstance?.customerVehicle.currentCond.kodeKotaNoPol}" class="many-to-one"/>
                    &nbsp;
                    <g:textField class="numberonly" name="t183NoPolTengah" id="t183NoPolTengah" maxlength="5" onkeypress="return isNumberKey(event);" style="width: 95px; font-size: 30px; height: 50px; border-radius: 0;" required="" value="${(!request.getRequestURI().contains("edit"))?"":customerInInstance?.customerVehicle.currentCondtion.t183NoPolTengah}"/>
                    &nbsp;
                    <g:textField class="charonly" name="t183NoPolBelakang" id="t183NoPolBelakang" maxlength="3" style="width: 70px; font-size: 30px; height: 50px; border-top-left-radius: 0; border-bottom-left-radius: 0;" required="" value="${(!request.getRequestURI().contains("edit"))?"":customerInInstance?.customerVehicle.currentCondtion.t183NoPolBelakang}"/>
                    &nbsp;
                    <a class="btn cancel" name="findDataCustomer" id="findDataCustomer" style="font-size: 20px; height: 50px;line-height: 50px;" ><g:message code="default.ok.label" default="OK"/></a>

                </td>
                <td>
                    <table style="padding-left: 20px">
                        <tr>
                            <td style="text-align: center">
                                <label id="lblAtas" style="font-size: 20px; height: 50px;"></label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label id="lblBawah" style="font-size: 16px; height: 50px;"></label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: customerInInstance, field: 'tujuanKedatangan', 'error')} required">
    <label class="control-label" for="tujuanKedatangan" style="text-align: left">
        <g:message code="customerIn.mobil.label" default="Mobil" />
    </label>
    <div class="controls">
        <input type="text" disabled id="mobil"/>
        <g:hiddenField name="idReception" id="idReception" />
    </div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: customerInInstance, field: 'tujuanKedatangan', 'error')} required">
    <label class="control-label" for="tujuanKedatangan" style="text-align: left">
        <g:message code="customerIn.tahun.label" default="Tahun" />
    </label>
    <div class="controls">
        <input type="text" disabled id="tahun"/>
    </div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: customerInInstance, field: 'tujuanKedatangan', 'error')} required">
    <label class="control-label" for="tujuanKedatangan" style="text-align: left">
        <g:message code="customerIn.warna.label" default="Warna" />
    </label>
    <div class="controls">
        <input type="text" disabled id="warna"/>
    </div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: customerInInstance, field: 'tujuanKedatangan', 'error')} required">
    <label class="control-label" for="tujuanKedatangan" style="text-align: left">
        <g:message code="customerIn.namaPemakai.label" default="Nama Customer" />
    </label>
    <div class="controls">
        <input type="text" disabled id="namaPemakai"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: customerInInstance, field: 'tujuanKedatangan', 'error')} required">
    <label class="control-label" for="tujuanKedatangan" style="text-align: left">
        <g:message code="customerIn.statusGatePass.label" default="Status" />
    </label>
    <div class="controls">
        <input type="text" disabled id="statusGatePass"/>
    </div>
</div>