<g:render template="../menu/maxLineDisplay"/>
<script type="text/javascript">
var shrink${gridConfig.domainClass.simpleName}Table;
var reload${gridConfig.domainClass.simpleName}Table;
jQuery(function () {

    var oTable = $('#${attrs.id}_datatable').dataTable({

        <g:each in="${gridConfig.dataTables}" var="property">
        "${property.key}":${property.value},
        </g:each>
        "sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
        bFilter: true,
        "bStateSave": false,
        'sPaginationType': 'bootstrap',
        "fnInitComplete": function () {
            <g:if test="${gridConfig.fixedColumns == 'true'}">
            new FixedColumns(oTable, {
                "iLeftColumns": ${gridConfig.noFixedColumns}
//                "iLeftWidth": 350
            });
            </g:if>
            var dataTables_wrapper_height = $('.dataTables_wrapper').height();
			 dataTables_wrapper_height += 50;
			 $('.dataTables_wrapper').height(dataTables_wrapper_height);

        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        },
        "bSort": true,
        "bProcessing": true,
        "bServerSide": true,
        "iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
        "sAjaxSource": "${g.createLink(action: "${gridConfig.id}Rows")}",
        "aoColumnDefs": [
            <g:each in="${gridConfig.columns}" var="col" status="idx">
            {   "sName": "${col.name}",
            	"aTargets": [${idx}],
            	
            	<g:if test="${idx == 1}">
            	"mRender": function ( data, type, row ) {
					return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row[0]+'" title="Select this"><input type="hidden" value="'+row[0]+'">&nbsp;&nbsp;<a href="#" onclick="show('+row[0]+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row[0]+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
				},
                </g:if>
                "bSearchable": ${col.enableFilter},
                <g:if test="${idx == 0}">
            	"mRender": function ( data, type, row ) {
					return '<a href="#" onclick="show('+row[0]+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row[0]+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
				},
                </g:if>
                "sWidth": "${col.sWidth}",
                "bSortable": ${col.sortable},
                <g:each in="${col.dataTables}" var="property">
                "${property.key}":${property.value},
                </g:each>
                <g:if test="${col.name == 'id' || col.name == 'pkid' }">
                "bVisible": false
                </g:if>
                <g:else>
                "bVisible": true
           		</g:else>
                %{--"sWidth": "${col.dataTables.sWidth}",--}%
                %{--"sClass": "${col.dataTables.sClass}"--}%
            }  <g:if test="${idx < gridConfig.columns.size() - 1}">,</g:if>
            </g:each>
        ]

    });

    $("#${attrs.id}-filters tbody")
    <g:each in="${gridConfig.columns}" var="col" status="idx">
    <g:if
	test="${gridConfig.enableFilter && col.enableFilter}">
    .append("<tr><td align=\"center\">${g.message(code: col.label, default: col.label)}&nbsp;&nbsp;</td><td align=\"center\" id=\"filter_${col.name}\"> <input type=\"text\" name=\"search_${col.name}\" class=\"search_init\" size=\"10\"/></td></tr>")
    </g:if>
    </g:each>
	;
	
	<g:set var="counter" value="${0}" />
    <g:each in="${gridConfig.columns}" var="col" status="idx">
    <g:if test="${gridConfig.enableFilter && col.enableFilter}">
    $("#filter_${col.name} input").bind('keypress', function(e) {
        /* Filter on the column (the index) of this element */
        var code = (e.keyCode ? e.keyCode : e.which);
 		if(code == 13) { //Enter keycode
	 		oTable.fnFilter(this.value, ${counter});
 		}
        
        <g:set var="counter" value="${counter + 1}" />
    }); 
    $("#filter_${col.name} input").click(function (e) {
    	 e.stopPropagation();  
    }); 
    </g:if>
    </g:each>

    shrink${gridConfig.domainClass.simpleName}Table = function(){
    	var oTable = $('#${attrs.id}_datatable').dataTable();
    	<g:each in="${gridConfig.columns}" var="col" status="idx"><g:if test="${col.hideOnShrink}">
    	oTable.fnSetColumnVis( ${idx}, false );</g:if></g:each>
    }

    expand${gridConfig.domainClass.simpleName}Table = function(){
    	var oTable = $('#${attrs.id}_datatable').dataTable();
    	<g:each in="${gridConfig.columns}" var="col" status="idx"><g:if test="${col.hideOnShrink}">
    	oTable.fnSetColumnVis( ${idx}, true );</g:if></g:each>
    }

    reload${gridConfig.domainClass.simpleName}Table = function(){
    	var oTable = $('#${attrs.id}_datatable').dataTable();
    	oTable.fnReloadAjax();
    }

    $('#${attrs.id}_datatable th .select-all').click(function(e) {
      	var tc = $('#${attrs.id}_datatable tbody .row-select').attr('checked', this.checked);
  
        if(this.checked)
          	tc.parent().parent().addClass('row_selected');
        else 
        	tc.parent().parent().removeClass('row_selected');
        e.stopPropagation();      
    });
    $('#${attrs.id}_datatable tbody tr .row-select').live('click', function (e) {
    	if(this.checked)
    		$(this).parent().parent().addClass('row_selected');
        else 
        	$(this).parent().parent().removeClass('row_selected');
    	e.stopPropagation();   
    	 
	} );

    $('#${attrs.id}_datatable tbody tr a').live('click', function (e) {
    	e.stopPropagation();       	 
	} );

    $('#${attrs.id}_datatable tbody td').live('click', function(e) {
        if (e.target.tagName.toUpperCase() != "INPUT") {
            var $tc = $(this).parent().find('input:checkbox'),
                tv = $tc.attr('checked');
            $tc.attr('checked', !tv);
            $(this).parent().toggleClass('row_selected');
        }
    });
    
});

</script>
		
<table id="${attrs.id}_datatable" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover" width="100%">
	<thead>
		<tr>
			<g:each in="${gridConfig.columns}" var="col" status="idx">
				<th>
					<g:if test="${idx == 1}">
            			<input type="checkbox" class="pull-left select-all" aria-label="Select all'" title="Select all">&nbsp;&nbsp;
				    </g:if>
					${g.message(code: col.label, default: col.name)}
					<div id="filter_${col.name}" style="margin-top: 38px;">
					<g:if test="${gridConfig.enableFilter && col.enableFilter}">
					<input type="text" name="search_${col.name}" class="search_init"/>
					</g:if>
					</div>
				</th>
			</g:each>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td colspan="${gridConfig.columns.size()}" class="dataTables_empty">Loading
				data from server</td>
		</tr>
	</tbody>
</table>


