

<%@ page import="com.kombos.administrasi.Provinsi" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'provinsi.label', default: 'Provinsi')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteProvinsi;

$(function(){ 
	deleteProvinsi=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/provinsi/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadProvinsiTable();
   				expandTableLayout('provinsi');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-provinsi" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="provinsi"
			class="table table-bordered table-hover">
			<tbody>

				

				<g:if test="${provinsiInstance?.m001ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m001ID-label" class="property-label"><g:message
					code="provinsi.m001ID.label" default="ID" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m001ID-label">
						%{--<ba:editableValue
								bean="${provinsiInstance}" field="m001ID"
								url="${request.contextPath}/Provinsi/updatefield" type="text"
								title="Enter m001ID" onsuccess="reloadProvinsiTable();" />--}%
							
								<g:fieldValue bean="${provinsiInstance}" field="m001ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${provinsiInstance?.m001NamaProvinsi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m001NamaProvinsi-label" class="property-label"><g:message
					code="provinsi.m001NamaProvinsi.label" default="Nama Provinsi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m001NamaProvinsi-label">
						%{--<ba:editableValue
								bean="${provinsiInstance}" field="m001NamaProvinsi"
								url="${request.contextPath}/Provinsi/updatefield" type="text"
								title="Enter m001NamaProvinsi" onsuccess="reloadProvinsiTable();" />--}%
							
								<g:fieldValue bean="${provinsiInstance}" field="m001NamaProvinsi"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${provinsiInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="provinsi.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${provinsiInstance}" field="createdBy"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadGoodsTable();" />--}%

                        <g:fieldValue bean="${provinsiInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${provinsiInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="provinsi.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${provinsiInstance}" field="updatedBy"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadGoodsTable();" />--}%

                        <g:fieldValue bean="${provinsiInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${provinsiInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="provinsi.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${provinsiInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadGoodsTable();" />--}%

                        <g:fieldValue bean="${provinsiInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${provinsiInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="provinsi.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${provinsiInstance}" field="dateCreated"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadGoodsTable();" />--}%

                        <g:formatDate date="${provinsiInstance?.dateCreated}" format="EEEE, dd MMMM yyyy HH:mm" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${provinsiInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="provinsi.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${provinsiInstance}" field="lastUpdated"
                                url="${request.contextPath}/Goods/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadGoodsTable();" />--}%

                        <g:formatDate date="${provinsiInstance?.lastUpdated}" format="EEEE, dd MMMM yyyy HH:mm"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('provinsi');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${provinsiInstance?.id}"
					update="[success:'provinsi-form',failure:'provinsi-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteProvinsi('${provinsiInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
