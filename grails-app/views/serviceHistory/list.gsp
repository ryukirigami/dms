
<%@ page import="com.kombos.administrasi.KodeKotaNoPol;"%>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="title" value="Service History" />
		<title>${title}</title>
		<r:require modules="baseapplayout" />
        <g:javascript disposition="head">
	var show;
	var loadForm;
	var noUrut = 1;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){


	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
                                   onLoading="jQuery('#spinner').fadeIn(1);"
                                   onSuccess="loadForm(data, textStatus);"
                                   onComplete="jQuery('#spinner').fadeOut();" />
            break;
        case '_DELETE_' :
            bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});

         		break;
       }
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/serviceHistory/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };


    loadForm = function(data, textStatus){
		$('#serviceHistory-form').empty();
    	$('#serviceHistory-form').append(data);
   	}

    shrinkTableLayout = function(){
    	$("#serviceHistory-table").hide()
//    	if($("#serviceHistory-table").hasClass("span12")){
//   			$("#serviceHistory-table").toggleClass("span12 span5");
//        }
        $("#serviceHistory-form").css("display","block");
   	}

   	expandTableLayout = function(){
//   		if($("#serviceHistory-table").hasClass("span5")){
//   			$("#serviceHistory-table").toggleClass("span5 span12");
//   		}
        $("#serviceHistory-table").show()
        $("#serviceHistory-form").css("display","none");
   	}

   	massDelete = function() {
   		var recordsToDelete = [];
		$("#serviceHistory-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    	//		alert(id)
    			recordsToDelete.push(id);
    		}
		});

		var json = JSON.stringify(recordsToDelete);

		$.ajax({
    		url:'${request.contextPath}/serviceHistory/massdelete',
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadServiceHistoryTable();
    		}
		});

   	}

});
     var checkin = $('#search_t017Tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
        var newDate = new Date(ev.date)
        newDate.setDate(newDate.getDate());
        checkout.setValue(newDate);
        checkin.hide();
        $('#search_t017Tanggal2')[0].focus();
    }).data('datepicker');

    var checkout = $('#search_t017Tanggal2').datepicker({
        onRender: function(date) {
            return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

    $(document).ready(function() {
        $("#nopol3").keyup(function(e) {
            var isi = $(e.target).val();
            $(e.target).val(isi.toUpperCase());
        });
    });
    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

        return true;
    }
 </g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left">${title}</span>
	</div>
	<div class="box">
		<div class="span12" id="serviceHistory-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <fieldset>
                <table style="padding-right: 10px">
                    <tr>
                        <td style="padding: 5px">
                                <g:message code="auditTrail.tanggal.label" default="Tanggal Service" />&nbsp;
                        </td>
                        <td style="padding: 5px">
                                <ba:datePicker id="search_t017Tanggal" name="search_t017Tanggal" precision="day" format="dd/MM/yyyy"  value="" />
                                &nbsp;&nbsp;&nbsp;s.d.&nbsp;&nbsp;&nbsp;
                                <ba:datePicker id="search_t017Tanggal2" name="search_t017Tanggal2" precision="day" format="dd/MM/yyyy"  value=""  />
                            </div>
                        </td>
                        <td style="padding: 5px">
                            <g:message code="serviceHistory.staJobTambah.label" default="Nomor Polisi"/>
                        </td>
                        <td style="padding: 5px">
                            <g:select name="nopol1" id="nopol1" from="${KodeKotaNoPol.createCriteria().list {eq("staDel",'0')}}" optionKey="id" style="width: 70px;"/>
                            <g:textField name="nopol2" id="nopol2" style="width: 70px" maxlength="5" onkeypress="return isNumberKey(event)" />
                            <g:textField name="nopol3" id="nopol3" style="width: 40px" maxlength="3" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <g:message code="serviceHistory.staCarryOver.label" default="Nomor WO"/>
                        </td>
                        <td style="padding: 5px">
                            <g:textField name="nomorWo" id="nomorWo" style="width:95%" />
                        </td>
                        <td style="padding: 5px">
                            <g:message code="serviceHistory.staCarryOver.label" default="Model Kendaraan"/>
                        </td>
                        <td style="padding: 5px">
                            <g:textField name="model" id="model"/>
                        </td>
                    </tr>

            <tr>
             <td style="padding: 5px">
                 <g:message code="serviceHistory.staCarryOver.label" default="Vincode"/>
             </td>

             <td style="padding: 5px">
                 <g:textField name="vincode" id="vincode" style="width:95%" />
             </td>
         </tr>
                    <tr>
                        <td colspan="4" >
                            <div class="controls" style="right: 0">
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-primary" name="view" id="view" >Filter</button>
                                <button style="width: 70px;height: 30px; border-radius: 5px" class="btn btn-cancel" name="clear" id="clear" >Clear</button>
                            </div>
                        </td>
                    </tr>
                </table>

            </fieldset>

           	<g:render template="dataTables" />
            <br>
            <fieldset>
                <legend style="font-size: 15px">Final Inspection Result</legend>
                <table>
                    <tr>
                        <td rowspan="3">
                            Catatan Final Inspection
                        </td>
                        <td rowspan="3" style="padding-left: 40px">
                            <g:textArea name="catatan" id="catatan" cols="5" rows="4" readonly="readonly" style="resize: none" />
                        </td>
                        <td rowspan="3" style="padding-left: 60px">
                            Job Suggest
                        </td>
                        <td rowspan="3" style="padding-left: 30px">
                            <g:textArea name="job" id="job" cols="5" rows="4" readonly="readonly" style="resize: none"  />
                        </td>
                        <td style="padding-left: 80px">
                            Kategori
                        </td>
                        <td style="padding-left: 30px" id="kategori">
                           : -
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 80px">
                            Tanggal Next Job
                        </td>
                        <td style="padding-left: 30px" id="tanggal">
                          : -
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 80px">
                            Status Final Inspection
                        </td>
                        <td style="padding-left: 30px" id="staFinal">
                           : -
                        </td>
                    </tr>
                </table>
            </fieldset>
		</div>
		<div class="span7" id="serviceHistory-form" style="display: none;"></div>
	</div>
</body>
</html>
