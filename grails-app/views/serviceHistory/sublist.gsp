<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
		<g:javascript>
var serviceHistorySubTable_${idTable};
$(function(){
var anOpen = [];
	$('#serviceHistory_datatables_sub_${idTable} td.subcontrol').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
  		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = serviceHistorySubTable_${idTable}.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST', 
	   			data : oData,
	   			url:'${request.contextPath}/serviceHistory/subsublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = serviceHistorySubTable_${idTable}.fnOpen(nTr,data,'details');
    				$('div.innerinnerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
    		
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerinnerDetails', $(nTr).next()[0]).slideUp( function () {
      			serviceHistorySubTable_${idTable}.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

    if(serviceHistorySubTable_${idTable})
    	serviceHistorySubTable_${idTable}.dataTable().fnDestroy();
serviceHistorySubTable_${idTable} = $('#serviceHistory_datatables_sub_${idTable}').dataTable({
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "t",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": false,
		"bProcessing": false,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "job",
	"mDataProp": "job",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return ' <input type="hidden" value="'+data+'">&nbsp;&nbsp;' + data;
	},
	"bSortable": false,
	"sWidth":"267px",
	"bVisible": true
},
{
	"sName": "hargaParts",
	"mDataProp": "hargaParts",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"141px",
	"bVisible": true
},
{
	"sName": "rateService",
	"mDataProp": "rateService",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"141px",
	"bVisible": true
},
{
	"sName": "hargaService",
	"mDataProp": "hargaService",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"145px",
	"bVisible": true
},
{
	"sName": "total",
	"mDataProp": "total",
	"aTargets": [4],
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true
},
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"140px",
	"sDefaultContent": '',
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        aoData.push(
									{"name": 'noWo', "value": "${noWo}"}

						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
		</g:javascript>
	</head>
	<body>
<div class="innerDetails">
<table id="serviceHistory_datatables_sub_${idTable}" cellpadding="0" cellspacing="0" border="0"
	class="display table table-striped table-bordered table-hover">
    <thead>
        <tr>
           <th></th>
           <th></th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Job</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Total Harga Parts</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Rate Service</div>
			</th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Harga Service</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Total</div>
            </th>
            <th style="border-bottom: none; padding: 5px;" />
        </tr>
			    </thead>
			    <tbody></tbody>
			</table>
</div>
</body>
</html>
