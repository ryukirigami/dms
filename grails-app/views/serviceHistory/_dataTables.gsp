
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay" />

<table id="serviceHistory_datatables_${idTable}" cellpadding="0" cellspacing="0" border="0"
	class="display table table-striped table-bordered table-hover" style="table-layout: fixed; ">
	<thead>
		<tr>
			<th></th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Nomor WO</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Nomor Polisi</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Model Kendaraan</div>
			</th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Nama Customer</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Alamat</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Telp</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Waktu Reception</div>
            </th>
            <th style="border-bottom: none; padding: 10px;">
                <div>Vincode</div>
            </th>

            <th style="border-bottom: none; padding: 5px;">
                <div>KM</div>
            </th>

            <th style="border-bottom: none; padding: 5px;">
                <div>SA</div>
            </th>

            <th style="border-bottom: none; padding: 5px;">
                <div>Teknisi</div>
            </th>

            <th style="border-bottom: none; padding: 5px;">
                <div>Foreman</div>
            </th>

            <th style="border-bottom: none; padding: 5px;">
                <div>Waktu Penyerahan</div>
            </th>

            <th style="border-bottom: none; padding: 5px;">
                <div>Keluhan</div>
            </th>

            <th style="border-bottom: none; padding: 5px;">
                <div>Cabang</div>
            </th>
        </tr>
		
	</thead>
</table>

<g:javascript>
var serviceHistoryTable;
var reloadServiceHistoryTable;
var oneChecked;
$(function(){
$('#vincode').typeahead({
            source: function (query, process) {
                var vincode = $('#vincode').val();
                return $.get('${request.contextPath}/serviceHistory/getVincodes?vincode='+vincode, { query: query }, function (data) {
                    return process(data.options);
                });
            }
        });


    oneChecked = function(comp){
       $(".row-select").prop("checked", false);
       $(comp).prop("checked", true);
       var noWo = $(comp).next("input:hidden").val();
       $.ajax({
    		url:'${request.contextPath}/serviceHistory/finalIns',
    		type: "POST", // Always use POST when deleting data
    		data: { noWo: noWo },
    		success : function(data){
                $("#catatan").html(data.catatan)
                $("#job").html(data.job)
                $("#kategori").html(data.kategori)
                $("#staFinal").html(data.staFI)
                $("#tanggal").html(data.tanggal)

            }
		});

    }
    reloadServiceHistoryTable = function() {
		serviceHistoryTable.fncDraw();
	}

    $('#clear').click(function(e){
        $('#search_t017Tanggal').val("");
        $('#search_t017Tanggal2').val("");
        $('#nopol1').val('');
	    $('#nopol2').val('');
	    $('#nopol3').val('');
	    $('#nomorWo').val('');
	    $('#model').val('');
	    $('#vincode').val('')
	    serviceHistoryTable.fnDraw();
	});

    $('#view').click(function(e){
        e.stopPropagation();
		serviceHistoryTable.fnDraw();
	});

	var anOpen = [];
	$('#serviceHistory_datatables_${idTable} td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = serviceHistoryTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST', 
	   			data : oData,
	   			url:'${request.contextPath}/serviceHistory/sublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = serviceHistoryTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
    		
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			serviceHistoryTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});
	
    $('#serviceHistory_datatables_${idTable} a.edit').live('click', function (e) {
        e.preventDefault();
         
        var nRow = $(this).parents('tr')[0];
         
        if ( nEditing !== null && nEditing != nRow ) {
            restoreRow( serviceHistoryTable, nEditing );
            editRow( serviceHistoryTable, nRow );
            nEditing = nRow;
        }
        else if ( nEditing == nRow && this.innerHTML == "Save" ) {
            saveRow( serviceHistoryTable, nEditing );
            nEditing = null;
        }
        else {
            editRow( serviceHistoryTable, nRow );
            nEditing = nRow;
        }
    } );
    
	reloadServiceHistoryTable = function() {
		serviceHistoryTable.fnDraw();
	}

 	serviceHistoryTable = $('#serviceHistory_datatables_${idTable}').dataTable({
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "sClass": "control center",
               "bSortable": false,
               "sWidth":"10px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "reception",
	"mDataProp": "noWo",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" onchange="oneChecked(this)" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"> <input type="hidden" value="'+data+'">&nbsp;&nbsp;' + data;
	},
	"bSortable": false,
	"sWidth":"180px",
	"bVisible": true
},
{
	"sName": "reception",
	"mDataProp": "noPol",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "reception",
	"mDataProp": "model",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "reception",
	"mDataProp": "nama",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "reception",
	"mDataProp": "alamat",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "reception",
	"mDataProp": "telp",
	"aTargets": [4],
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "reception",
	"mDataProp": "waktuReception",
	"aTargets": [5],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "vincode",
	"mDataProp": "vincode",
	"aTargets": [6],
	"bSortable": false,
	"sWidth":"150px",
	"bVisible": true
},
{
	"sName": "reception",
	"mDataProp": "kmSaatIni",
	"aTargets": [7],
	"bSortable": false,
	"sWidth":"75px",
	"bVisible": true
},
{
	"sName": "reception",
	"mDataProp": "SA",
	"aTargets": [8],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "teknisi",
	"mDataProp": "teknisi",
	"aTargets": [9],
	"bSortable": false,
	"sWidth":"75px",
	"bVisible": true
},
{
	"sName": "namaForeman",
	"mDataProp": "namaForeman",
	"aTargets": [10],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "reception",
	"mDataProp": "waktuPenyerahan",
	"aTargets": [11],
	"bSortable": false,
	"sWidth":"125px",
	"bVisible": true
}
,

{
	"sName": "keluhan",
	"mDataProp": "keluhan",
	"aTargets": [12],
	"bSortable": false,
	"sWidth":"125px",
	"bVisible": true
}
,

{
	"sName": "cabang",
	"mDataProp": "cabang",
	"aTargets": [13],
	"bSortable": false,
	"sWidth":"125px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        var t017Tanggal = $('#search_t017Tanggal').val();
						var t017TanggalDay = $('#search_t017Tanggal_day').val();
						var t017TanggalMonth = $('#search_t017Tanggal_month').val();
						var t017TanggalYear = $('#search_t017Tanggal_year').val();

						var t017Tanggal2 = $('#search_t017Tanggal2').val();
						var t017TanggalDay2 = $('#search_t017Tanggal2_day').val();
						var t017TanggalMonth2 = $('#search_t017Tanggal2_month').val();
						var t017TanggalYear2 = $('#search_t017Tanggal2_year').val();

						if(t017Tanggal){
							aoData.push(
									{"name": 'sCriteria_t017Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_t017Tanggal_dp', "value": t017Tanggal},
									{"name": 'sCriteria_t017Tanggal_day', "value": t017TanggalDay},
									{"name": 'sCriteria_t017Tanggal_month', "value": t017TanggalMonth},
									{"name": 'sCriteria_t017Tanggal_year', "value": t017TanggalYear}
							);
						}


						if(t017Tanggal2){
							aoData.push(
									{"name": 'sCriteria_t017Tanggal2', "value": "date.struct"},
									{"name": 'sCriteria_t017Tanggal2_dp', "value": t017Tanggal2},
									{"name": 'sCriteria_t017Tanggal2_day', "value": t017TanggalDay2},
									{"name": 'sCriteria_t017Tanggal2_month', "value": t017TanggalMonth2},
									{"name": 'sCriteria_t017Tanggal2_year', "value": t017TanggalYear2}
							);
						}

	                    var nopol1 = $('#nopol1').val();
						if(nopol1){
							aoData.push(
									{"name": 'nopol1', "value": nopol1}
							);
						}
						var nopol2 = $('#nopol2').val();
						if(nopol2){
							aoData.push(
									{"name": 'nopol2', "value": nopol2}
							);
						}
						var nopol3 = $('#nopol3').val();
						if(nopol3){
							aoData.push(
									{"name": 'nopol3', "value": nopol3}
							);
						}

						var model = $('#model').val();
						if(model){
							aoData.push(
									{"name": 'model', "value": model}
							);
						}
						var nomorWo = $('#nomorWo').val();
						if(nomorWo){
							aoData.push(
									{"name": 'nomorWo', "value": nomorWo}
							);
						}

							var vincode = $('#vincode').val();
						if(vincode){
							aoData.push(
									{"name": 'vincode', "value": vincode}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});



</g:javascript>



