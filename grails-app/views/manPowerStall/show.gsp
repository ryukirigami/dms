

<%@ page import="com.kombos.administrasi.ManPowerStall" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'manPowerStall.label', default: 'Man Power Stall')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteManPowerStall;

$(function(){ 
	deleteManPowerStall=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/manPowerStall/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadManPowerStallTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-manPowerStall" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="manPowerStall"
			class="table table-bordered table-hover">
			<tbody>

				<g:if test="${manPowerStallInstance?.namaManPower}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="namaManPower-label" class="property-label"><g:message
					code="manPowerStall.namaManPower.label" default="Nama Man Power" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="namaManPower-label">
						%{--<ba:editableValue
								bean="${manPowerStallInstance}" field="namaManPower"
								url="${request.contextPath}/ManPowerStall/updatefield" type="text"
								title="Enter namaManPower" onsuccess="reloadManPowerStallTable();" />--}%
							
								%{--<g:link controller="namaManPower" action="show" id="${manPowerStallInstance?.namaManPower?.id}">${manPowerStallInstance?.namaManPower?.encodeAsHTML()}</g:link>--}%
                        <g:fieldValue bean="${manPowerStallInstance?.namaManPower}" field="t015NamaLengkap"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${manPowerStallInstance?.stall}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="stall-label" class="property-label"><g:message
					code="manPowerStall.stall.label" default="Stall" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="stall-label">
						%{--<ba:editableValue
								bean="${manPowerStallInstance}" field="stall"
								url="${request.contextPath}/ManPowerStall/updatefield" type="text"
								title="Enter stall" onsuccess="reloadManPowerStallTable();" />--}%
							
								%{--<g:link controller="stall" action="show" id="${manPowerStallInstance?.stall?.id}">${manPowerStallInstance?.stall?.encodeAsHTML()}</g:link>--}%
                        <g:fieldValue bean="${manPowerStallInstance?.stall}" field="m022NamaStall"/>
						</span></td>
					
				</tr>
				</g:if>
			
			
				
				<g:if test="${manPowerStallInstance?.t019Tanggal}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t019Tanggal-label" class="property-label"><g:message
					code="manPowerStall.t019Tanggal.label" default="Tanggal Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t019Tanggal-label">
						%{--<ba:editableValue
								bean="${manPowerStallInstance}" field="t019Tanggal"
								url="${request.contextPath}/ManPowerStall/updatefield" type="text"
								title="Enter t019Tanggal" onsuccess="reloadManPowerStallTable();" />--}%
							
								<g:formatDate date="${manPowerStallInstance?.t019Tanggal}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
			<g:if test="${manPowerStallInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="jenisStall.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${manPowerStallInstance}" field="dateCreated"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:formatDate date="${manPowerStallInstance?.dateCreated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${manPowerStallInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="jenisStall.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${manPowerStallInstance}" field="createdBy"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:fieldValue bean="${manPowerStallInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${manPowerStallInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="jenisStall.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${manPowerStallInstance}" field="lastUpdated"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:formatDate date="${manPowerStallInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${manPowerStallInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="jenisStall.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${manPowerStallInstance}" field="updatedBy"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:fieldValue bean="${manPowerStallInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${manPowerStallInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="jenisStall.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${manPowerStallInstance}" field="lastUpdProcess"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:fieldValue bean="${manPowerStallInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${manPowerStallInstance?.id}"
					update="[success:'manPowerStall-form',failure:'manPowerStall-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteManPowerStall('${manPowerStallInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
