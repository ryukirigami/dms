<%@ page import="com.kombos.administrasi.NamaManPower; com.kombos.administrasi.Stall; com.kombos.administrasi.ManPowerStall" %>

<g:javascript>
		$(function(){
			$('#namaManPower').typeahead({
			    source: function (query, process) {
			        return $.get('${request.contextPath}/manPowerStall/kodeList', { query: query }, function (data) {
			            return process(data.options);
			        });
			    }
			});
		});
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: manPowerStallInstance, field: 'namaManPower', 'error')} required">
	<label class="control-label" for="namaManPower">
		<g:message code="manPowerStall.namaManPower.label" default="Nama ManPower" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:textField name="namaManPower" id="namaManPower" class="typeahead" maxlength="200" value="${manPowerStallInstance?.namaManPower?.t015NamaLengkap}" autocomplete="off" required="true"/>
    </div>
</div>
<g:javascript>
    document.getElementById("namaManPower").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: manPowerStallInstance, field: 'stall', 'error')} required">
	<label class="control-label" for="stall">
		<g:message code="manPowerStall.stall.label" default="Stall" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="stall" name="stall.id" from="${Stall.createCriteria().list(){eq("companyDealer", session.userCompanyDealer);order("m022NamaStall", "asc")}}" optionKey="id" required="" value="${manPowerStallInstance?.stall?.m022NamaStall}" class="many-to-one"/>
    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: manPowerStallInstance, field: 't019Tanggal', 'error')} required">
	<label class="control-label" for="t019Tanggal">
		<g:message code="manPowerStall.t019Tanggal.label" default="Tanggal Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<ba:datePicker name="t019Tanggal" precision="day"  value="${manPowerStallInstance?.t019Tanggal}"  format="dd-MM-yyyy" required="true" />
	</div>
</div>
<g:javascript>
    document.getElementById("namaManPower").focus();
</g:javascript>