
<%@ page import="com.kombos.administrasi.ManPowerStall" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="manPowerStall_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

		

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="manPowerStall.namaManPower.label" default="Nama Man Power" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="manPowerStall.stall.label" default="Stall" /></div>
			</th>
			
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="manPowerStall.t019Tanggal.label" default="Tanggal Berlaku" /></div>
			</th>

 
		</tr>
		<tr>
		

			<th style="border-top: none;padding: 5px;">
				<div id="filter_namaManPower" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_namaManPower" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_stall" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_stall" class="search_init" />
				</div>
			</th>
	
			
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t019Tanggal" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_t019Tanggal" value="date.struct">
					<input type="hidden" name="search_t019Tanggal_day" id="search_t019Tanggal_day" value="">
					<input type="hidden" name="search_t019Tanggal_month" id="search_t019Tanggal_month" value="">
					<input type="hidden" name="search_t019Tanggal_year" id="search_t019Tanggal_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t019Tanggal_dp" value="" id="search_t019Tanggal" class="search_init">
				</div>
			</th>
	

		</tr>
	</thead>
</table>

<g:javascript>
var ManPowerStallTable;
var reloadManPowerStallTable;
$(function(){
	
	reloadManPowerStallTable = function() {
		ManPowerStallTable.fnDraw();
	}

var recordsManPowerStallPerPage = [];
    var anManPowerStallSelected;
    var jmlRecManPowerStallPerPage=0;
    var id;
	
	$('#search_t019Tanggal').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t019Tanggal_day').val(newDate.getDate());
			$('#search_t019Tanggal_month').val(newDate.getMonth()+1);
			$('#search_t019Tanggal_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			ManPowerStallTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	ManPowerStallTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	ManPowerStallTable = $('#manPowerStall_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsManPowerStall = $("#manPowerStall_datatables tbody .row-select");
            var jmlManPowerStallCek = 0;
            var nRow;
            var idRec;
            rsManPowerStall.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsManPowerStallPerPage[idRec]=="1"){
                    jmlManPowerStallCek = jmlManPowerStallCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsManPowerStallPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecManPowerStallPerPage = rsManPowerStall.length;
            if(jmlManPowerStallCek==jmlRecManPowerStallPerPage && jmlRecManPowerStallPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [


{
	"sName": "namaManPower",
	"mDataProp": "namaManPower",
	"aTargets": [1],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "stall",
	"mDataProp": "stall",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t019Tanggal",
	"mDataProp": "t019Tanggal",
	"aTargets": [0],
	
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var t019Tanggal = $('#search_t019Tanggal').val();
						var t019TanggalDay = $('#search_t019Tanggal_day').val();
						var t019TanggalMonth = $('#search_t019Tanggal_month').val();
						var t019TanggalYear = $('#search_t019Tanggal_year').val();
						
						if(t019Tanggal){
							aoData.push(
									{"name": 'sCriteria_t019Tanggal', "value": "date.struct"},
									{"name": 'sCriteria_t019Tanggal_dp', "value": t019Tanggal},
									{"name": 'sCriteria_t019Tanggal_day', "value": t019TanggalDay},
									{"name": 'sCriteria_t019Tanggal_month', "value": t019TanggalMonth},
									{"name": 'sCriteria_t019Tanggal_year', "value": t019TanggalYear}
							);
						}
	
						var namaManPower = $('#filter_namaManPower input').val();
						if(namaManPower){
							aoData.push(
									{"name": 'sCriteria_namaManPower', "value": namaManPower}
							);
						}
	
						var stall = $('#filter_stall input').val();
						if(stall){
							aoData.push(
									{"name": 'sCriteria_stall', "value": stall}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

	$('.select-all').click(function(e) {

        $("#manPowerStall_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsManPowerStallPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsManPowerStallPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#manPowerStall_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsManPowerStallPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anManPowerStallSelected = ManPowerStallTable.$('tr.row_selected');
            if(jmlRecManPowerStallPerPage == anManPowerStallSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsManPowerStallPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
