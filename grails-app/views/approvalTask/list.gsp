<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<title>Approval Task</title>
<r:require modules="baseapplayout" />
<g:javascript>
   var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/task/getForm?taskId='+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#approvaltask-form').empty();
    	$('#approvaltask-form').append(data);
   	}
    
    shrinkTableLayout = function(){
   		if($("#approvaltask-table").hasClass('span12')){
   			$("#approvaltask-table").toggleClass('span12 span5');
   		}
        $("#approvaltask-form").css("display","block");
       	shrinkApprovalTaskTable();
   	}
   	
   	expandTableLayout = function(){
   		if($("#approvaltask-table").hasClass('span5')){
   			$("#approvaltask-table").toggleClass('span5 span12');
   		}
        $("#approvaltask-form").css("display","none");
       	expandApprovalTaskTable(); 
   	}

});
</g:javascript>
</head>
<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"> Approval Task </span>
		
	</div>
	<div class="box">
		<div id="approvaltask-table" class="span12">
				<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
				<table id="approvaltaskDatatables_datatable" cellpadding="0"
					cellspacing="0" border="0"
					class="display table table-striped table-bordered table-hover"
					width="100%">
					<thead>
						<tr>
							<th>Task Id</th>
							<th>Task Name</th>
							<th>Request By</th>
							<th>Request On</th>
							<th>Approval Id</th>
						</tr>
					</thead>
					<g:render template="../menu/maxLineDisplay" />
					<g:javascript>
					var shrinkApprovalTaskTable;
                	var reloadApprovalTaskTable;
                	var expandApprovalTaskTable;
                	jQuery(function () {
                	 var oTable = $('#approvaltaskDatatables_datatable').dataTable({
                	  	//"sScrollX" : "100%", //Scroll
						                	  	"sScrollXInner": "120%",
												"bScrollCollapse": true,
						"bAutoWidth" : false,
                        "bScrollCollapse" : true,
                        "bPaginate" : true,
                        "bSort" : false,
                        "sInfo" : "",
                        "sInfoEmpty" : "",

                        "sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
                        bFilter: true,
                        "bStateSave": false,
                        'sPaginationType': 'bootstrap',
                        "fnInitComplete": function () {
                        },
                        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        },
                        "bSort": true,
                        "bProcessing": true,
                        "bServerSide": true,
                        "sServerMethod": "POST",
                        "iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
                       
                        "sAjaxSource": "${request.getContextPath()}/ApprovalTask/getList",
						
                        "aoColumns": [
                        
				            {   "sName": "taskid",
				            	"mDataProp": "taskid",
				            	"aTargets": [0],
				                "bSearchable": true,
				                "bSortable": true,                
				                "bVisible": true,
				                "mRender": function ( data, type, row ) {
									return '<a href="#" onclick="show('+row['taskid']+');">'+data+'</a>';
								}
				            } ,
				            {   "sName": "taskname",
				            	"mDataProp": "taskname",
				            	"aTargets": [1],
				                "bSearchable": true,
				                "bSortable": true,                
				                "bVisible": true,				                
								"sWidth":"20%"
				            } ,
				            {   "sName": "requestby",
				            	"mDataProp": "requestby",
				            	"aTargets": [2],
				                "bSearchable": true,
				                "bSortable": true,                
				                "bVisible": true,
				                "sWidth":"16%"
				            } ,
				             {   "sName": "requeston",
				             	"mDataProp": "requeston",
				            	"aTargets": [3],
				                "bSearchable": true,
				                "bSortable": true,                
				                "bVisible": true,
				                "sWidth":"16%"
				            } ,
				             {   "sName": "approvalid",
				             	"mDataProp": "approvalid",
				            	"aTargets": [4],
				                "bSearchable": true,
				                "bSortable": true,                
				                "bVisible": true,
				                "sWidth":"16%"
				            }  
						
                        ]
                	 });
                	   
                	 //  new FixedColumns(oTable);   
                	 
                    

                    shrinkApprovalTaskTable = function(){
                        var oTable = $('#approvaltaskDatatables_datatable').dataTable();
                        oTable.fnSetColumnVis( 2, false );
                        oTable.fnSetColumnVis( 3, false );
                        oTable.fnSetColumnVis( 4, false );
                    }

                    expandApprovalTaskTable = function(){
                        var oTable = $('#approvaltaskDatatables_datatable').dataTable();
                        oTable.fnSetColumnVis( 2, true );
                        oTable.fnSetColumnVis( 3, true );
                        oTable.fnSetColumnVis( 4, true );
                    }

                    reloadApprovalTaskTable = function(){
                        var oTable = $('#approvaltaskDatatables_datatable').dataTable();
                        oTable.fnReloadAjax();
                    }                   

                    $('#approvaltaskDatatables_datatable tbody tr a').live('click', function (e) {
                        e.stopPropagation();
                    } );  	
				    
				    });
                	
                	
                	</g:javascript>
				</table>
			
		</div>
		<div class="span7" id="approvaltask-form" style="display: none;"></div>
	</div>
</body>
</html>
