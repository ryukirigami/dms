
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="stockIN_datatables_${idTable}" cellpadding="0" cellspacing="0" border="0"
	class="display table table-striped table-bordered table-hover table-selectable fixed" style="table-layout: fixed; ">
    <col width="25px" />
	<col width="325px" />
    <col width="250x" />
    <col width="600px" />
	<thead>
		<tr>
            <th></th>
			<th style="border-bottom: none;padding: 5px;" class="center">
				<div class="center"><input type="checkbox" class="pull-left select-all" aria-label="Select all" title="Select all"/>&nbsp;&nbsp;No. Stock In / No. Reff</div>
			</th>
            <th style="border-bottom: none;padding: 5px;" class="center">
                <div class="center">Tanggal Stock In</div>
            </th>
            <th style="border-bottom: none;padding: 5px;" class="center">
                <div class="center">Petugas Stock In</div>
            </th>
		</tr>
	</thead>
</table>

<g:javascript>
var stockINTable;
var reloadStockINTable;
var shrinkTableLayout;
var expandTableLayout;
var isSelected;

$(function(){
    var recordspartsperpage = [];//new Array();
    var anPartsSelected;
    var jmlRecPartsPerPage=0;
    var id;
	var anOpen = [];
	   
    $('#stockIN_datatables_${idTable} td.control').live( 'click', function () {
        var nTr = this.parentNode;
        var i = $.inArray( nTr, anOpen );
        if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
            var noStockINInner = $(this).closest('tr').find("input:hidden").val();
            var sname = "stockIN-row-" + noStockINInner;
            console.log("sname=" + sname);
            if ($('#' + sname).is(':checked')) {
            console.log("is checked");
            isSelected = "true";
            } else {
            console.log("is unchecked");
            isSelected = "false";
            }

    		var oData = stockINTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST', 
	   			data : oData,
	   			url:'${request.contextPath}/stockIN/sublist?isSelected=' + isSelected,
	   			success:function(data,textStatus){
	   				var nDetailsRow = stockINTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});
    		
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			stockINTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

    $('input[id^=stockIN-row-]').change(function(){
        if($(this).is(':checked')){
        console.log('Checked');
        } else {
        console.log('Not checked');
        }
        console.log("this.name()=" + this.name());
    });

	reloadStockINTable = function() {
		stockINTable.fnDraw();
	}

	stockINTable = $('#stockIN_datatables_${idTable}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsParts = $("#stockIN_datatables_${idTable} tbody .row-select");
            var jmlPartsCek = 0;
            var nRow;
            var idRec;
            rsParts.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordspartsperpage[idRec]=="1"){
                    jmlPartsCek = jmlPartsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordspartsperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecPartsPerPage = rsParts.length;
            if(jmlPartsCek == jmlRecPartsPerPage && jmlRecPartsPerPage > 0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "sClass": "control center",
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "nomorStockIN",
	"mDataProp": "nomorStockIN",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
	    %{--return '<input id="stockIN-row-'+row['id']+'" type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"> ' +--}%
	     %{--'<input type="hidden" value="'+row['id']+'"><input type="hidden" value="'+ data +'"><input type="hidden" id="checkbox-'+row['id']+'" >&nbsp;&nbsp;<a href="javascript:void(0);" onclick="edit(' + row['id'] + ');">' + data + '</a>';--}%
        return '<input id="stockIN-row-'+row['id']+'" type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"> ' +
        '<input type="hidden" value="'+row['id']+'"><input type="hidden" value="'+ data +'"><input type="hidden" id="checkbox-'+row['id']+'" >&nbsp;&nbsp;' + data;
	},
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "tanggalStockIN",
	"mDataProp": "tanggalStockIN",
	"aTargets": [1],
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "petugasStockIN",
	"mDataProp": "petugasStockIN",
	"aTargets": [2],
	"bSortable": false,
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
            var search_noReff = $('#search_noReff').val();

            if(search_noReff){
                aoData.push(
                        {"name": 'search_noReff', "value": search_noReff}
                );
            }
            var tglFrom = $('#search_tanggal').val();

            if(tglFrom){
                aoData.push(
                        {"name": 'sCriteria_tglFrom', "value": tglFrom}
                );
            }

            var tglTo = $('#search_tanggalAkhir').val();

            if(tglTo){
                aoData.push(
                        {"name": 'sCriteria_tglTo', "value": tglTo}
                );
            }
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});

    $('.select-all').click(function(e) {
        $("#stockIN_datatables_${idTable} tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                $('.row-select2').prop('checked', true);
                $('.row-select').prop('checked', true);
                $('.sel-all2').prop('checked', true);
                var nRow = $('.row-select2').parent().parent();//.addClass('row_selected');
                nRow.addClass('row_selected');
                recordspartsperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                $('.row-select2').prop('checked', false);
                $('.row-select').prop('checked', false);
                $('.sel-all2').prop('checked', false);
                var nRow = $('.row-select2').parent().parent();//.addClass('row_selected');
                nRow.removeClass('row_selected');
                recordspartsperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#stockIN_datatables_${idTable} tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        console.log("id=" + id);
        var noStockIN = $(this).find('.row-select').next("input:hidden").next("input:hidden").val();
        console.log("noStockIN=" + noStockIN);
        if($(this).find('.row-select').is(":checked"))
        {
            recordspartsperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anPartsSelected = stockINTable.$('tr.row_selected');
            if(jmlRecPartsPerPage == anPartsSelected.length){
                $('.select-all').attr('checked', true);
            }

            $('.sel-all2').each(function() {
                var noStockINInner = $(this).next("input:hidden").val();
                //console.log("select-all2 noStockIN=" + noStockIN + " noStockINInner=" + noStockINInner);
                if (noStockIN == noStockINInner) {
                    $(this).attr('checked', true);
                    var nRow = $(this).parent().parent().parent();//.addClass('row_selected');
                    console.log("select-all2 nRow.html()=" + nRow.html());
                    nRow.addClass('row_selected');
                }
            });
            $('table[id^=stockIN_datatables_sub_] tbody .row-select2').each(function() {
                var noStockINInner = $(this).next("input:hidden").next("input:hidden").val();
                //console.log("row-select2 noStockIN=" + noStockIN + " noStockINInner=" + noStockINInner);
                if (noStockIN == noStockINInner) {
                    $(this).attr('checked', true);
                    var nRow = $(this).parent().parent();//.addClass('row_selected');
                    console.log("row-select2 nRow.html()=" + nRow.html());
                    nRow.addClass('row_selected');
                }
            });

        }
        else {
            recordspartsperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');

            $('.sel-all2').each(function() {
                var noStockINInner = $(this).next("input:hidden").val();
                //console.log("select-all2 noStockIN=" + noStockIN + " noStockINInner=" + noStockINInner);
                if (noStockIN == noStockINInner) {
                    $(this).attr('checked', false);
                    var nRow = $(this).parent().parent().parent();//.addClass('row_selected');
                    nRow.removeClass('row_selected');
                }
            });
            $('table[id^=stockIN_datatables_sub_] tbody .row-select2').each(function() {
                var noStockINInner = $(this).next("input:hidden").next("input:hidden").val();
                //console.log("row-select2 noStockIN=" + noStockIN + " noStockINInner=" + noStockINInner);
                if (noStockIN == noStockINInner) {
                    $(this).attr('checked', false);
                    var nRow = $(this).parent().parent();//.addClass('row_selected');
                    nRow.removeClass('row_selected');
                }
            });
        }
    });

});
</g:javascript>
