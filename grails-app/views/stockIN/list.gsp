
<%@ page import="com.kombos.parts.StockIN" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
        <g:set var="title" value="Stock In" />
        <title>${title}</title>
		<r:require modules="baseapplayout" />
        <style>
            table.fixed { table-layout:fixed; }
            table.fixed td { overflow: hidden; }
            .center {
                text-align:center;
            }
         </style>
		<g:javascript>
	var orderParts;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){
    $("#stockIN-table").css("margin-left","-0px");
    $("#stockIN-table").css("margin-right","0px");

	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :
                shrinkTableLayout();
                <g:remoteFunction controller="inputStockIN" action="index"
                        onLoading="jQuery('#spinner').fadeIn(1);"
                        onSuccess="loadForm(data, textStatus);"
                        onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
         			function(result){
         				if(result){
         					massDeleteStockIN();
         				}
         			});

         		break;
       }
       return false;
	});

    massDeleteStockIN = function() {
        var recordsToDelete = [];
        var stockINDetailsToDelete = [];
        $('#stockIN_datatables_${idTable} tbody .row-select').each(function() {
            if(this.checked){
                var id = $(this).next("input:hidden").val();
                recordsToDelete.push(id);
            }
        });

        $('table[id^=stockIN_datatables_sub_] tbody .row-select2').each(function() {
            console.log("this=" + this);

            if(this.checked){
                var id = $(this).next("input:hidden").val();
                var stockINNumber = $(this).next("input:hidden").next("input:hidden").val();
                console.log("id=" + id + " stockINNumber=" + stockINNumber);
                stockINDetailsToDelete.push(id);
            }
        });
        console.log("recordsToDelete=" + recordsToDelete);
        console.log("stockINDetailsToDelete=" + stockINDetailsToDelete);
        var json = JSON.stringify(recordsToDelete);
        var jsonDetail = JSON.stringify(stockINDetailsToDelete);
        console.log("json=" + json);
        console.log("jsonDetail=" + jsonDetail);
        if (recordsToDelete.length > 0) {
            $.ajax({
                url:'${request.contextPath}/stockIN/massdeleteStockIN',
                type: "POST", // Always use POST when deleting data
                data: { ids: json },
                complete: function(xhr, status) {
                    stockINTable.fnDraw();
                     toastr.success('<div>Stock IN Telah Dihapus.</div>');

                }
            });
        } else if (stockINDetailsToDelete.length > 0) {
            $.ajax({
                url:'${request.contextPath}/stockIN/massdeleteStockINDetail',
                type: "POST", // Always use POST when deleting data
                data: { ids: jsonDetail },
                complete: function(xhr, status) {
                    stockINTable.fnDraw();
                     toastr.success('<div>Stock IN Detail Telah Dihapus.</div>');
                }
            });
        }
    }



    onClearSearch = function () {
         $('#search_tanggal').val("");
         $('#search_tanggalAkhir').val("");
         $('#search_noReff').val("");
        reloadStockINTable();
    }

   	onSearch = function(){
       reloadStockINTable();
   	}

    edit = function(id) {
        console.log("didalam edit");
        shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/inputStockIN/index?1=1',
   		    data : { id: id},
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
        $(".row-select").each(function() {
            this.checked = false
        });
    };

    shrinkTableLayout = function(){
        $("#stockIN-table").hide();
        console.log("afterHide");
        $("#stockIN-form").css("display","block");
        $("#stockIN-form").css("margin-left","-40px");
        $("#stockIN-form").css("margin-right","40px");
   	}

    loadForm = function(data, textStatus){
		$('#stockIN-form').empty();
    	$('#stockIN-form').append(data);
   	}

   	expandTableLayout = function(){
   	    var oTable = $('#stockIN_datatables_${idTable}').dataTable();
        oTable.fnReloadAjax();
   		$("#stockIN-table").show();
   		$("#stockIN-form").css("display","none");
   	}
   	var checkin = $('#search_tanggal').datepicker({
            onRender: function(date) {
                return '';
            }
        }).on('changeDate', function(ev) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate());
            checkout.setValue(newDate);
            checkin.hide();
            $('#search_tanggalAkhir')[0].focus();
        }).data('datepicker');

        var checkout = $('#search_tanggalAkhir').datepicker({
            onRender: function(date) {
                return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');
   	});
</g:javascript>
	</head>
	<body>
    <div class="span12" id="stockIN-table">
    <div class="box">
        <legend style="font-size: small">Search</legend>
        <table style="width: 50%;border: 0px">
            <tr>
                <td style="width: 130px">
                    <label class="control-label" for="lbl_rcvDat2e">
                        No Reff
                    </label>
                </td>
                <td>
                    <div id="lbl_noreff" class="controls">
                        <g:textField name="search_noReff" id="search_noReff" />
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width: 130px">
                    <label class="control-label" for="lbl_rcvDate">
                        Tanggal Stock In
                    </label>
                </td>
                <td>
                    <div id="lbl_rcvDate" class="controls">
                        <ba:datePicker id="search_tanggal" name="search_tanggal" precision="day" format="dd/MM/yyyy"  value="${new Date()}" /> S.d
                        <ba:datePicker id="search_tanggalAkhir" name="search_tanggalAkhir" precision="day" format="dd/MM/yyyy" style="display: none" value="${new Date()}" />
                    </div>
                </td>
            </tr>
        </table>
        <fieldset class="buttons controls">
            <button class="btn btn-primary" onclick="onSearch();">Search</button>
            <button class="btn btn-cancel" onclick="onClearSearch();">Clear Search</button>
        </fieldset>
    </div>

    <div class="navbar box-header no-border" >
        <span class="pull-left">Stock In</span>
        <ul class="nav pull-right">
          <li><a class="pull-right box-action" href="#"
              style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
                  class="icon-plus"></i>&nbsp;&nbsp;
          </a></li>
          %{--<li><a class="pull-right box-action" href="#"--}%
              %{--style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i--}%
                  %{--class="icon-remove"></i>&nbsp;&nbsp;--}%
          %{--</a></li>--}%
        </ul>
    </div>

    <div class="box" style="margin-left: -0px; margin-right: 0px;">
        <g:if test="${flash.message}">
           <div class="message" role="status">
               ${flash.message}
           </div>
        </g:if>
        <g:render template="dataTables" />
    </div>
    </div>

    <div class="span12" id="stockIN-form" style="display: none;"></div>
</body>
</html>
		