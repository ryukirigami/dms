<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
        <style>
            table.fixed { table-layout:fixed; }
            table.fixed td { overflow: hidden; }
            .center {
                text-align:center;
            }
        </style>
		<g:javascript>
var stockINSubTable_${idTableSub};
$(function(){
    var recordspartsperpage = [];//new Array();
    var anPartsSelected;
    var jmlRecPartsPerPage=0;
    var id;
    var isSelected;
var anOpen = [];
	$('#stockIN_datatables_sub_${idTableSub} a.edit').live('click', function (e) {
        e.preventDefault();
         
        var nRow = $(this).parents('tr')[0];
         
        if ( nEditing !== null && nEditing != nRow ) {
            restoreRow( stockINSubTable_${idTableSub}, nEditing );
            editRow( stockINSubTable_${idTableSub}, nRow );
            nEditing = nRow;
        }
        else if ( nEditing == nRow && this.innerHTML == "Save" ) {
            saveRow( stockINSubTable_${idTableSub}, nEditing );
            nEditing = null;
        }
        else {
            editRow( stockINSubTable_${idTableSub}, nRow );
            nEditing = nRow;
        }
    } );
    if(stockINSubTable_${idTableSub})
    	stockINSubTable_${idTableSub}.dataTable().fnDestroy();

    stockINSubTable_${idTableSub} = $('#stockIN_datatables_sub_${idTableSub}').dataTable({
		"sScrollX": "1398px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsParts = $("#stockIN_datatables_sub_${idTableSub} tbody .row-select2");
            var jmlPartsCek = 0;
            var nRow;
            var idRec;
            rsParts.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                console.log("nRow=" + nRow);
                console.log("idRec=" + idRec);
                console.log("recordspartsperpage[idRec]=" + recordspartsperpage[idRec]);

                if(this.checked || recordspartsperpage[idRec]=="1"){
                    jmlPartsCek = jmlPartsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(!this.checked || recordspartsperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecPartsPerPage = rsParts.length;
            %{--if(jmlPartsCek == jmlRecPartsPerPage && jmlRecPartsPerPage > 0){
                $('.select-all2_${idTableSub}').attr('checked', true);
            } else {
                $('.select-all2_${idTableSub}').attr('checked', false);
            }--}%
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		 },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSubList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sDefaultContent": ''
},
{
               "mDataProp": null,
               "sClass": "subcontrol center",
               "bSortable": false,
               "sDefaultContent": ''
},
{
	"sName": "kodeParts",
	"mDataProp": "kodeParts",
    "aTargets": [0],
	"mRender": function ( data, type, row ) {
	    //return '<input id="stockIN-sub-row-'+row['id']+'" type="checkbox" class="pull-left row-select2" aria-label="Row '+data+'" title="Select this"> <input type="hidden" value="'+data+'">&nbsp;&nbsp;' + data;
	     return '<input id="binning-sub-row-'+row['id']+'" type="checkbox" class="pull-left row-select2" ${isSelected=='true'?'checked':''} > ' +
        '<input type="hidden" value="'+row['id']+'"><input type="hidden" value="${nomorStockIN}">&nbsp;&nbsp;' + data;
	},
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "namaParts",
	"mDataProp": "namaParts",
    "aTargets": [1],
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "noReff",
	"mDataProp": "noReff",
    "aTargets": [2],
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "tglJamBinning",
	"mDataProp": "tglJamBinning",
    "aTargets": [3],
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "nomorBinning",
	"mDataProp": "nomorBinning",
    "aTargets": [4],
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "qtyReceive",
	"mDataProp": "qtyReceive",
    "aTargets": [5],
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "qtyBinning",
	"mDataProp": "qtyBinning",
    "aTargets": [6],
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "qtyRusak1",
	"mDataProp": "qtyRusak1",
    "aTargets": [7],
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "qtySalah1",
	"mDataProp": "qtySalah1",
    "aTargets": [8],
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "qtyStockIn1",
	"mDataProp": "qtyStockIn1",
    "aTargets": [9],
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "namaLocation",
	"mDataProp": "namaLocation",
    "aTargets": [10],
	"bSortable": false,
	"bVisible": true
},
{
	"sName": "namaLocationRusak",
	"mDataProp": "namaLocationRusak",
    "aTargets": [11],
	"bSortable": false,
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						aoData.push(
									{"name": 'nomorStockIN', "value": "${nomorStockIN}"},
									{"name": 'tglJamStockIN', "value": "${tglJamStockIN}"},
									{"name": 'petugasStockIN', "value": "${petugasStockIN}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

    $('.select-all2_${idTableSub}').click(function(e) {
        var tw = $(this).parents(".dataTables_wrapper");
        console.log("tw=" + tw);
        console.log("idTableSub" + ${idTableSub} + " this.checked=" + this.checked);
        var tc = tw.find('#stockIN_datatables_sub_${idTableSub} tbody').find('.row-select2').attr('checked', this.checked);
        console.log("tc.html()=" + tc.html());
        if(this.checked)
            tc.parent().parent().addClass(' row_selected ');
        else
            tc.parent().parent().removeClass('row_selected');
        console.log("tc.html()=" + tc.html());
        console.log("tc.parent().parent().html()=" + tc.parent().parent().html());
        e.stopPropagation();
    });

	$('#stockIN_datatables_sub_${idTableSub} tbody tr').live('click', function () {

        id = $(this).find('.row-select2').next("input:hidden").val();
        if($(this).find('.row-select2').is(":checked")){
            recordspartsperpage[id] = "1";
            $(this).find('.row-select2').parent().parent().addClass('row_selected');
            anPartsSelected = stockINSubTable_${idTableSub}.$('tr.row_selected');
            if(jmlRecPartsPerPage == anPartsSelected.length){
                $('.select-all2_${idTableSub}').attr('checked', true);
            }
        } else {
            recordspartsperpage[id] = "0";
            $('.select-all2_${idTableSub}').attr('checked', false);
            $(this).find('.row-select2').parent().parent().removeClass('row_selected');
        }
    });

    var checkbox_col2 = $(".table-selectable2").find("thead").find("tr").first().find("th").first().find("div");
    if(checkbox_col2 && checkbox_col2.html()){
           if(checkbox_col2.html().search('type="checkbox"')==-1){
               checkbox_col2.prepend('<input type="checkbox" class="pull-left select-all2_${idTableSub}" aria-label="Select all" title="Select all"/>&nbsp;&nbsp;')
           }

    }

});
		</g:javascript>
	</head>
	<body>
<div class="innerDetails">
<table id="stockIN_datatables_sub_${idTableSub}" cellpadding="0" cellspacing="0" border="0"
	class="display table table-striped table-bordered table-hover table-selectable fixed">
    <col width="24px" />
    <col width="25px" />
	<col width="150px" />
	<col width="200px" />
    <col width="100px" />
    <col width="150px" />
    <col width="100px" />
    <col width="100px" />
    <col width="100px" />
    <col width="100px" />
    <col width="100px" />
    <col width="100px" />
    <col width="100px" />
    <col width="100px" />
    <thead>
        <tr>
           <th rowspan="2"></th>
           <th rowspan="2"></th>
			<th style="border-bottom: none; padding: 5px;" rowspan="2">
				<div class="center"><input type="checkbox" class="pull-left select-all2_${idTableSub} sel-all2" aria-label="Select all" title="Select all"/>
                    <input type="hidden" value="${nomorStockIN}">&nbsp;&nbsp;Kode Part</div>
			</th>
			<th style="border-bottom: none; padding: 5px;" rowspan="2">
				<div class="center">Nama Part</div>
			</th>
			<th style="border-bottom: none; padding: 5px;" rowspan="2">
				<div class="center">No Reff</div>
			</th>
            <th style="border-bottom: none; padding: 5px;" rowspan="2">
				<div class="center">Tanggal Dan Jam Binning</div>
			</th>
            <th style="border-bottom: none; padding: 5px;" rowspan="2">
                <div class="center">Nomor Binning</div>
            </th>
            <th style="border-bottom: none; padding: 5px;" colspan="7">
                <div class="center">Qty</div>
            </th>
         </tr>
        <tr>

            <th style="border-bottom: none; padding: 5px;">
                <div>Receive</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Binning</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Rusak</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Salah</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Stock In</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Lokasi/Rak</div>
            </th>
            <th style="border-bottom: none; padding: 5px;">
                <div>Lokasi/Rak (Rusak/Salah)</div>
            </th>
         </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
</body>
</html>
