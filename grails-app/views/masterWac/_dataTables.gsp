
<%@ page import="com.kombos.administrasi.MasterWac" %>

<r:require modules="baseapplayout,autoNumeric" />
<g:render template="../menu/maxLineDisplay"/>

<table id="masterWac_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="masterWac.m403Nomor.label" default="Nomor" /></div>
			</th>




			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="masterWac.m403NamaPerlengkapan.label" default="Perlengkapan Manual" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="masterWac.m403JmlhMaksItemWAC.label" default="Jumlah Maks Item WAC GR Tercetak" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m403Nomor" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m403Nomor" class="search_init auto" />
				</div>
			</th>

	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m403NamaPerlengkapan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m403NamaPerlengkapan" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m403JmlhMaksItemWAC" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m403JmlhMaksItemWAC" class="search_init auto" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var MasterWacTable;
var reloadMasterWacTable;
$(function(){
	
	reloadMasterWacTable = function() {
		MasterWacTable.fnDraw();
	}

	var recordsWacperpage = [];
    var anWacSelected;
    var jmlRecWacPerPage=0;
    var id;

 $('.auto').autoNumeric('init',{
            vMin:'0',
            vMax:'999999999',
            mDec: '0',
            aSep:''
        });


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	MasterWacTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	MasterWacTable = $('#masterWac_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            //alert( $('li.active').text() );
            var rsWac = $("#masterWac_datatables tbody .row-select");
            var jmlWacCek = 0;
            var idRec;
            var nRow;
            rsWac.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsWacperpage[idRec]=="1"){
                    jmlWacCek = jmlWacCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsWacperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecWacPerPage = rsWac.length;
            if(jmlWacCek==jmlRecWacPerPage && jmlRecWacPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m403Nomor",
	"mDataProp": "m403Nomor",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m403NamaPerlengkapan",
	"mDataProp": "m403NamaPerlengkapan",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m403JmlhMaksItemWAC",
	"mDataProp": "m403JmlhMaksItemWAC",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m403Nomor = $('#filter_m403Nomor input').val();
						if(m403Nomor){
							aoData.push(
									{"name": 'sCriteria_m403Nomor', "value": m403Nomor}
							);
						}
	
						var m403KondisiWAC = $('#filter_m403KondisiWAC input').val();
						if(m403KondisiWAC){
							aoData.push(
									{"name": 'sCriteria_m403KondisiWAC', "value": m403KondisiWAC}
							);
						}
	
						var m403NamaPerlengkapan = $('#filter_m403NamaPerlengkapan input').val();
						if(m403NamaPerlengkapan){
							aoData.push(
									{"name": 'sCriteria_m403NamaPerlengkapan', "value": m403NamaPerlengkapan}
							);
						}
	
						var m403JmlhMaksItemWAC = $('#filter_m403JmlhMaksItemWAC input').val();
						if(m403JmlhMaksItemWAC){
							aoData.push(
									{"name": 'sCriteria_m403JmlhMaksItemWAC', "value": m403JmlhMaksItemWAC}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

    $('.select-all').click(function(e) {
        $("#masterWac_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsWacperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsWacperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

	$('#masterWac_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsWacperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anWacSelected = MasterWacTable.$('tr.row_selected');
            if(jmlRecWacPerPage == anWacSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsWacperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });


});
</g:javascript>


			
