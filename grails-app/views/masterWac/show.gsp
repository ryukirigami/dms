

<%@ page import="com.kombos.administrasi.MasterWac" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'masterWac.label', default: 'WAC')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteMasterWac;

$(function(){ 
	deleteMasterWac=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/masterWac/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadMasterWacTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-masterWac" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="masterWac"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${masterWacInstance?.m403Nomor}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m403Nomor-label" class="property-label"><g:message
					code="masterWac.m403Nomor.label" default="Nomor" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m403Nomor-label">
						%{--<ba:editableValue
								bean="${masterWacInstance}" field="m403Nomor"
								url="${request.contextPath}/MasterWac/updatefield" type="text"
								title="Enter m403Nomor" onsuccess="reloadMasterWacTable();" />--}%
							
								<g:fieldValue bean="${masterWacInstance}" field="m403Nomor"/>
							
						</span></td>
					
				</tr>
				</g:if>

                <g:if test="${masterWacInstance?.m403NamaPerlengkapan}">
                    <tr>
                        <td class="span2" style="text-align: right;"><span
                                id="m403NamaPerlengkapan-label" class="property-label"><g:message
                                    code="masterWac.m403NamaPerlengkapan.label" default="Perlengkapan Manual" />:</span></td>

                        <td class="span3"><span class="property-value"
                                                aria-labelledby="m403NamaPerlengkapan-label">
                            %{--<ba:editableValue
                                    bean="${masterWacInstance}" field="m403NamaPerlengkapan"
                                    url="${request.contextPath}/MasterWac/updatefield" type="text"
                                    title="Enter m403NamaPerlengkapan" onsuccess="reloadMasterWacTable();" />--}%

                            <g:fieldValue bean="${masterWacInstance}" field="m403NamaPerlengkapan"/>

                        </span></td>

                    </tr>
                </g:if>

				<g:if test="${masterWacInstance?.m403KondisiWAC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m403KondisiWAC-label" class="property-label"><g:message
					code="masterWac.m403KondisiWAC.label" default="Kondisi WAC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m403KondisiWAC-label">
						%{--<ba:editableValue
								bean="${masterWacInstance}" field="m403KondisiWAC"
								url="${request.contextPath}/MasterWac/updatefield" type="text"
								title="Enter m403KondisiWAC" onsuccess="reloadMasterWacTable();" />--}%
                         <img class="" src="${createLink(controller:'masterWac', action:'showKondisiWac', params : [id : masterWacInstance.id, random : logostamp]) }" />

                        </span></td>
					
				</tr>
				</g:if>
			

			
				<g:if test="${masterWacInstance?.m403JmlhMaksItemWAC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m403JmlhMaksItemWAC-label" class="property-label"><g:message
					code="masterWac.m403JmlhMaksItemWAC.label" default="Jumlh Maks Item WAC GR tercetak" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m403JmlhMaksItemWAC-label">
						%{--<ba:editableValue
								bean="${masterWacInstance}" field="m403JmlhMaksItemWAC"
								url="${request.contextPath}/MasterWac/updatefield" type="text"
								title="Enter m403JmlhMaksItemWAC" onsuccess="reloadMasterWacTable();" />--}%
							
								<g:fieldValue bean="${masterWacInstance}" field="m403JmlhMaksItemWAC"/>
							
						</span></td>
					
				</tr>
				</g:if>
            <g:if test="${masterWacInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="jenisStall.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${masterWacInstance}" field="dateCreated"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadJenisStallTable();" />--}%

                        <g:formatDate date="${masterWacInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${masterWacInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="jenisStall.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${masterWacInstance}" field="createdBy"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadJenisStallTable();" />--}%

                        <g:fieldValue bean="${masterWacInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${masterWacInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="jenisStall.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${masterWacInstance}" field="lastUpdated"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadJenisStallTable();" />--}%

                        <g:formatDate date="${masterWacInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${masterWacInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="jenisStall.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${masterWacInstance}" field="updatedBy"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadJenisStallTable();" />--}%

                        <g:fieldValue bean="${masterWacInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${masterWacInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="jenisStall.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${masterWacInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadJenisStallTable();" />--}%

                        <g:fieldValue bean="${masterWacInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${masterWacInstance?.id}"
					update="[success:'masterWac-form',failure:'masterWac-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteMasterWac('${masterWacInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
