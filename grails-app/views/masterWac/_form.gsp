<%@ page import="com.kombos.administrasi.MasterWac" %>

<script type="text/javascript">
    jQuery(function($) {
        $('.auto').autoNumeric('init', {mDec: '0'});
    });
</script>

<div class="control-group fieldcontain ${hasErrors(bean: masterWacInstance, field: 'm403Nomor', 'error')} required">
	<label class="control-label" for="m403Nomor">
		<g:message code="masterWac.m403Nomor.label" default="Nomor" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:hiddenField name="id" value="${masterWacInstance?.id}"/>
        <g:textField name="m403Nomor" id="m403Nomor" class="auto" value="${masterWacInstance?.m403Nomor}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterWacInstance, field: 'm403NamaPerlengkapan', 'error')} required">
    <label class="control-label" for="m403NamaPerlengkapan">
        <g:message code="masterWac.m403NamaPerlengkapan.label" default="Perlengkapan Manual" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="m403NamaPerlengkapan" maxlength="50" required="" value="${masterWacInstance?.m403NamaPerlengkapan}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterWacInstance, field: 'm403KondisiWAC', 'error')} ">
	<label class="control-label" for="m403KondisiWAC">
		<g:message code="masterWac.m403KondisiWAC.label" default="Kondisi WAC" />
		
	</label>
	<div class="controls">
	<input type="file" id="m403KondisiWAC" name="m403KondisiWAC" />
	</div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: masterWacInstance, field: 'm403JmlhMaksItemWAC', 'error')} required">
	<label class="control-label" for="m403JmlhMaksItemWAC">
		<g:message code="masterWac.m403JmlhMaksItemWAC.label" default="Jumlah Maks Item WAC GR" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:textField name="m403JmlhMaksItemWAC" maxlength="10" value="${fieldValue(bean: masterWacInstance, field: 'm403JmlhMaksItemWAC')}" required=""  class="auto" />
	</div>
</div>

<g:javascript>
    document.getElementById('m403Nomor').focus();
</g:javascript>