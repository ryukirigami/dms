<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'delivery.kwitansi.label', default: 'Delivery - Kwitansi')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript>

        $('#search_tglFrom').datepicker().on('changeDate', function(ev) {
       			var newDate = new Date(ev.date);
       			$('#search_tglFrom_day').val(newDate.getDate());
       			$('#search_tglFrom_month').val(newDate.getMonth()+1);
       			$('#search_tglFrom_year').val(newDate.getFullYear());
       			$(this).datepicker('hide');
       	});

       	$('#search_tglTo').datepicker().on('changeDate', function(ev) {
       			var newDate = new Date(ev.date);
       			$('#search_tglTo_day').val(newDate.getDate());
       			$('#search_tglTo_month').val(newDate.getMonth()+1);
       			$('#search_tglTo_year').val(newDate.getFullYear());
       			$(this).datepicker('hide');
       	});


        $(function() {
            $('input[type=radio]').change(function(e) {
                console.log("this.value=" + this.value);
                if (this.value == '0' ) {
                    $('#persenResponse').prop('disabled',false);
                    $('#nominalResponse').val("");
                    $('#nominalResponse').prop('disabled',true);
                } else {
                    $('#persenResponse').val("");
                    $('#persenResponse').prop('disabled',true);
                    $('#nominalResponse').prop('disabled',false);
                }
            });

            onClearSearch = function () {
                $('#checkStatusApproval').prop('checked',false);
                $('#checkNomorInvoice').prop('checked',false);
                $('#checkNomorPolisi').prop('checked',false);
                $('#search_tglFrom').val('');
                $('#search_tglTo').val('');
                $('#nomorInvoiceSearch').val('');
                $('#nomorPolisiSearch').val('');
                $('#nomorInvoiceSearch').prop('disabled',true);
                $('#nomorPolisiSearch').prop('disabled',true);
                reloadSendApprovalTable();
            }

            selectCheckStatusApproval = function() {
           	    if ($('#checkStatusApproval').is(':checked')) {
           	        $('#status_approval').prop('disabled', false);
           	    } else {
           	        $('#status_approval').val('');
           	        $('#status_approval').prop('disabled', true);
           	    }
           	}
            selectCheckNomorInvoice = function() {
                if ($('#checkNomorInvoice').is(':checked')) {
                    $('#nomorInvoiceSearch').prop('disabled', false);
                } else {
                    $('#nomorInvoiceSearch').val('');
                    $('#nomorInvoiceSearch').prop('disabled', true);
                }
            }
            selectCheckNomorPolisi = function() {
                if ($('#checkNomorPolisi').is(':checked')) {
                    $('#nomorPolisiSearch').prop('disabled', false);
                } else {
                    $('#nomorPolisiSearch').val('');
                    $('#nomorPolisiSearch').prop('disabled', true);
                }
            }

            onResponseApproval = function(){
                var checkJob =[];
                $("#response_approval_datatables tbody .row-select").each(function() {
                    if(this.checked){
                        var id = $(this).next("input:hidden").val();
                        checkJob.push(id);
                    }
                });
                console.log("checkJob.length=" + checkJob.length + " checkJob=" + checkJob);

                if(checkJob.length == 0){
                    alert('Anda belum memilih data yang akan diapprove');
                    return;
                }

                var checkBoxVendor = JSON.stringify(checkJob);
                console.log("checkBoxVendor=" + checkBoxVendor);

                var persenResponse = $("#persenResponse").val();
                var nominalResponse = $("#nominalResponse").val();
                var keterangan = $("#keterangan").val();
                if (!persenResponse && !nominalResponse) {
                    alert("Silahkan Mengisi Persen Response Atau Nominal Response");
                } else if (!keterangan) {
                    alert("Silahkan Mengisi Keterangan");
                } else {
                    $.ajax({
                        url:'${request.contextPath}/responseApprovalDiscountGoodWill/responseApproval?1=1',
                        type: "POST", // Always use POST when deleting data
                        data : { ids: checkBoxVendor, persenResponse: persenResponse, nominalResponse: nominalResponse, keterangan: keterangan},
                        success : function(data){
                            alert('Save success');
                            var oTable = $('#response_approval_datatables').dataTable();
                            oTable.fnReloadAjax();
                            $('#spinner').fadeIn(1);
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            alert('Internal Server Error');
                        }
                    });
                }
            }

            onResponseReject = function(){
                var checkJob =[];
                $("#response_approval_datatables tbody .row-select").each(function() {
                    if(this.checked){
                        var id = $(this).next("input:hidden").val();
                        checkJob.push(id);
                    }
                });
                console.log("checkJob.length=" + checkJob.length + " checkJob=" + checkJob);

                if(checkJob.length == 0){
                    alert('Anda belum memilih data yang akan direject');
                    return;
                }

                var checkBoxVendor = JSON.stringify(checkJob);
                console.log("checkBoxVendor=" + checkBoxVendor);

                var keterangan = $("#keterangan").val();
                if (!keterangan) {
                    alert("Silahkan Mengisi Keterangan");
                } else {
                    $.ajax({
                        url:'${request.contextPath}/responseApprovalDiscountGoodWill/responseReject?1=1',
                        type: "POST", // Always use POST when deleting data
                        data : { ids: checkBoxVendor, keterangan: keterangan},
                        success : function(data){
                            alert('Save success');
                            var oTable = $('#response_approval_datatables').dataTable();
                            oTable.fnReloadAjax();
                            $('#spinner').fadeIn(1);
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            alert('Internal Server Error');
                        }
                    });
                }
            }

            onSearch = function(){
                var search_tglFrom = $("#search_tglFrom").val();
                var search_tglTo = $("#search_tglTo").val();
                var oTable = $('#response_approval_datatables').dataTable();
                if ((search_tglFrom.length == 0 && search_tglTo.length == 0) ) {
                    oTable.fnReloadAjax();
                } else if ((search_tglFrom.length == 10 && search_tglTo.length == 10)) {
                    var from = search_tglFrom.split("/");
                    var dfrom = new Date(from[2], from[1] - 1, from[0]);
                    console.log("dfrom=" + dfrom);
                    var to = search_tglTo.split("/");
                    var dto = new Date(to[2], to[1] - 1, to[0]);
                    console.log("dto=" + dto);
                    if (dfrom <= dto) {
                        oTable.fnReloadAjax();
                    } else {
                        alert("Tangal Awal Pencarian harus Sebelum Tanggal Akhir Pencarian");
                    }
                } else {
                    if (search_tglFrom.length == 10 && search_tglTo.length == 0) {
                        alert("Silahkan memilih Tanggal Akhir pencarian");
                    } else if (search_tglFrom.length == 0 && search_tglTo.length == 10) {
                        alert("Silahkan memilih Tanggal Awal pencarian");
                    }
                }
            }

        });
    </g:javascript>
</head>

<body>

<div class="box">
    <legend>Approval Discount GoodWill</legend>
    <table style="width: 80%">
        <tr>
            <td>Tanggal Response</td>
            <td></td>
            <td>
                <input type="hidden" name="search_tglFrom" value="date.struct">
                <input type="hidden" name="search_tglFrom_day" id="search_tglFrom_day" value="">
                <input type="hidden" name="search_tglFrom_month" id="search_tglFrom_month" value="">
                <input type="hidden" name="search_tglFrom_year" id="search_tglFrom_year" value="">
                <input type="text" data-date-format="dd/mm/yyyy" name="search_tglFrom_dp" value="" id="search_tglFrom" class="search_init" style="width: 120px">
                &nbsp;s.d&nbsp;
                <input type="hidden" name="search_tglTo" value="date.struct">
                <input type="hidden" name="search_tglTo_day" id="search_tglTo_day" value="">
                <input type="hidden" name="search_tglTo_month" id="search_tglTo_month" value="">
                <input type="hidden" name="search_tglTo_year" id="search_tglTo_year" value="">
                <input type="text" data-date-format="dd/mm/yyyy" name="search_tglTo_dp" value="" id="search_tglTo" class="search_init" style="width: 120px">

                <button class="btn btn-primary" id="buttonSearch" onclick="onSearch();">Search</button>
                <button class="btn btn-primary" id="buttonClear" onclick="onClearSearch();">Clear</button>
            </td>
        </tr>
        <tr>
            <td>Status Approval</td>
            <td><g:checkBox name="checkStatusApproval" id="checkStatusApproval" value="checkStatusApproval" onclick="selectCheckStatusApproval();"/></td>
            <td>
                <g:select id="status_approval" name="status_approval" from="${com.kombos.parts.StatusApproval}" required="" class="many-to-one"/>
            </td>
        </tr>
        <tr>
            <td>Nomor Invoice</td>
            <td><g:checkBox name="checkNomorInvoice" id="checkNomorInvoice" value="checkNomorInvoice" onclick="selectCheckNomorInvoice();"/></td>
            <td>
                <g:textField name="nomorInvoiceSearch" id="nomorInvoiceSearch"/>
            </td>
        </tr>
        <tr>
            <td>Nomor Polisi</td>
            <td><g:checkBox name="checkNomorPolisi" id="checkNomorPolisi" value="checkNomorPolisi" onclick="selectCheckNomorPolisi();"/></td>
            <td>
                <g:textField name="nomorPolisiSearch" id="nomorPolisiSearch"/>
            </td>
        </tr>
    </table>
    <div class="span12" id="sendApproval-table" style="margin-left: -10px; margin-right: 10px;">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>

        <g:render template="responseApprovalDataTables"/>
    </div>
</div>

<div class="box">
    <legend>Response Approval Discount GoodWill</legend>
    <table style="width: 60%">
        <tr style="padding: 5px; height: 45px">
            <td>Request By</td>
            <td></td>
            <td>
                <span class="property-value">
                    <g:fieldValue bean="${invoiceInstance}" field="updatedBy" style="width: 125px;" />
                </span>
            </td>
            <td>
                <button class="btn btn-primary" id="buttonSend" onclick="onResponseApproval();" style="width: 250px;" >Approval Discount GoodWill</button>
            </td>
        </tr>
        <tr style="padding: 5px; height: 45px">
            <td>Tanggal Request</td>
            <td></td>
            <td>
                <span class="property-value">
                    <g:fieldValue bean="${invoiceInstance}" field="t701TglRequestApprovalGoodWill" style="width: 125px;" />
                </span>
            </td>
            <td>
                <button class="btn btn-primary" id="buttonReject" onclick="onResponseReject();" style="width: 250px;" >Reject Discount GoodWill</button>
            </td>
        </tr>
        <tr style="padding: 5px; height: 30px">
            <td>Nama Customer</td>
            <td></td>
            <td colspan="2">
                <span class="property-value">
                    <g:fieldValue bean="${invoiceInstance}" field="t701Customer" style="width: 125px;" />
                </span>
            </td>
        </tr>
        <tr style="padding: 5px; height: 30px">
            <td>Total Invoice</td>
            <td></td>
            <td colspan="2">
                <span class="property-value">
                    <g:fieldValue bean="${invoiceInstance}" field="t701TotalRp" style="width: 125px;" />
                </span>
            </td>
        </tr>
        <tr style="padding: 5px; height: 30px">
            <td>
                <label class="control-label" for="lbl_ResponseBy">
                    Response By
                </label>
            </td>
            <td colspan="3">
                <div id="lbl_ResponseBy" class="controls">
                    <g:radioGroup name="requestBy" values="['0','1']" value="0" labels="['Persen','Nominal']">
                        ${it.radio} <g:message code="${it.label}" />
                    </g:radioGroup>
                </div>
            </td>
        </tr>
        <tr style="padding: 5px; height: 30px">
            <td>Persen Response Discount</td>
            <td></td>
            <td colspan="2">
                <g:textField name="persenResponse" id="persenResponse" style="width: 70px;" />&nbsp;%&nbsp;&nbsp;&nbsp;(Maks. 10%)
            </td>
        </tr>
        <tr style="padding: 5px; height: 30px">
            <td>Nominal Response Discount</td>
            <td></td>
            <td colspan="2">
                <g:textField name="nominalResponse" id="nominalResponse" style="width: 125px;" disabled="disabled" />&nbsp;(Maks. Rp.500.000)
            </td>
        </tr>
        <tr style="padding: 5px; height: 30px">
            <td>Keterangan</td>
            <td></td>
            <td colspan="2">
                <g:textField name="keterangan" id="keterangan" />
            </td>
        </tr>
    </table>
</div>

</body>
</html>