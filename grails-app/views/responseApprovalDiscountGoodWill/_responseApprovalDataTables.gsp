<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="response_approval_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Tgl Request</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">No Invoice</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Customer</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Alamat</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Nopol</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Total Invoice</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Rp Disc Req</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Disc Req</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Status</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Tgl Respon</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Rp Disc Respon</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">% Disc Respon</div>
        </th>
        <th style="border-bottom: none;">
            <div style="width: 100px;">Keterangan</div>
        </th>
    </tr>
    </thead>
</table>
<g:javascript>
var responseApprovalTable;

function searchData() {
    reloadSendApprovalTable();
}

$(function() {
	reloadSendApprovalTable = function() {
		responseApprovalTable.fnDraw();
	}

	responseApprovalTable = $('#response_approval_datatables').dataTable({
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		},
	    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
	    },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "responseApprovalDataTablesList")}",
		"aoColumns": [
			{
				"sName": "tglRequest",
				"mDataProp": "tglRequest",
                "mRender": function ( data, type, row ) {
                    return '<input type="checkbox" style="width: 13px; height: 13px; padding: 0; margin:0; vertical-align: bottom; position: relative; top: -1px; *overflow: hidden;" ' +
                            'class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
                },
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			{
				"sName": "noInvoice",
				"mDataProp": "noInvoice",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			{
				"sName": "customer",
				"mDataProp": "customer",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			{
				"sName": "alamat",
				"mDataProp": "alamat",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			{
				"sName": "nopol",
				"mDataProp": "nopol",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
			{
				"sName": "totalInvoice",
				"mDataProp": "totalInvoice",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
            {
                "sName": "rpDiscReq",
                "mDataProp": "rpDiscReq",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"100px",
                "bVisible": true
            },
			{
				"sName": "persenDiscReq",
				"mDataProp": "persenDiscReq",
				"bSearchable": false,
				"bSortable": false,
				"sWidth":"100px",
				"bVisible": true
			},
            {
                "sName": "status",
                "mDataProp": "status",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"100px",
                "bVisible": true
            },
            {
                "sName": "tglRespon",
                "mDataProp": "tglRespon",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"100px",
                "bVisible": true
            },
            {
                "sName": "rpDiscRespon",
                "mDataProp": "rpDiscRespon",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"100px",
                "bVisible": true
            },
            {
                "sName": "persenDiscRespon",
                "mDataProp": "persenDiscRespon",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"100px",
                "bVisible": true
            },
            {
                "sName": "keterangan",
                "mDataProp": "keterangan",
                "bSearchable": false,
                "bSortable": false,
                "sWidth":"100px",
                "bVisible": true
            }
		],

        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

            var tglFrom = $('#search_tglFrom').val();
            var tglFromDay = $('#search_tglFrom_day').val();
            var tglFromMonth = $('#search_tglFrom_month').val();
            var tglFromYear = $('#search_tglFrom_year').val();
            if(tglFrom){
                aoData.push(
                        {"name": 'sCriteria_tglFrom', "value": "date.struct"},
                        {"name": 'sCriteria_tglFrom_dp', "value": tglFrom},
                        {"name": 'sCriteria_tglFrom_day', "value": tglFromDay},
                        {"name": 'sCriteria_tglFrom_month', "value": tglFromMonth},
                        {"name": 'sCriteria_tglFrom_year', "value": tglFromYear}
                );
            }

            var tglTo = $('#search_tglTo').val();
            var tglToDay = $('#search_tglTo_day').val();
            var tglToMonth = $('#search_tglTo_month').val();
            var tglToYear = $('#search_tglTo_year').val();
            if(tglTo){
                aoData.push(
                        {"name": 'sCriteria_tglTo', "value": "date.struct"},
                        {"name": 'sCriteria_tglTo_dp', "value": tglTo},
                        {"name": 'sCriteria_tglTo_day', "value": tglToDay},
                        {"name": 'sCriteria_tglTo_month', "value": tglToMonth},
                        {"name": 'sCriteria_tglTo_year', "value": tglToYear}
                );
            }
            var statusApproval = $('#status_approval').val('');
            if(statusApproval){
                aoData.push(
                        {"name": 'sCriteria_statusApproval', "value": statusApproval}
                );
            }

            var nomorInvoice = $('#filter_nomorInvoice input').val();
            if(nomorInvoice){
                aoData.push(
                        {"name": 'sCriteria_nomorInvoice', "value": nomorInvoice}
                );
            }
            var nomorPolisi = $('#nomorPolisiSearch').val('');
            if(nomorPolisi){
                aoData.push(
                        {"name": 'sCriteria_nomorPolisi', "value": nomorPolisi}
                );
            }
                        $.ajax({ "dataType": 'json',
                            "type": "POST",
                            "url": sSource,
                            "data": aoData ,
                            "success": function (json) {
                                fnCallback(json);
                               },
                            "complete": function () {
                               }
                        });
        }

	});

});
</g:javascript>