<%@ page import="com.kombos.administrasi.CompanyDealer; com.kombos.administrasi.Divisi" %>



<div class="control-group fieldcontain ${hasErrors(bean: divisiInstance, field: 'companyDealer', 'error')} required">
	<label class="control-label" for="companyDealer">
		<g:message code="divisi.companyDealer.label" default="Company Dealer" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="companyDealer" name="companyDealer.id" from="${CompanyDealer.createCriteria().list(){eq("staDel", "0");order("m011NamaWorkshop", "asc")}}" optionKey="id" required="" value="${divisiInstance?.companyDealer?.id}" class="many-to-one"/>
	</div>
</div>
<g:javascript>
    document.getElementById("companyDealer").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: divisiInstance, field: 'm012NamaDivisi', 'error')} required">
	<label class="control-label" for="m012NamaDivisi">
		<g:message code="divisi.m012NamaDivisi.label" default="Nama Divisi" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m012NamaDivisi" required="" maxlength="50" value="${divisiInstance?.m012NamaDivisi}"/>
	</div>
</div>
%{--
<div class="control-group fieldcontain ${hasErrors(bean: divisiInstance, field: 'm012ID', 'error')} ">
	<label class="control-label" for="m012ID">
		<g:message code="divisi.m012ID.label" default="M012 ID" />
		
	</label>
	<div class="controls">
	<g:textField name="m012ID" value="${divisiInstance?.m012ID}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: divisiInstance, field: 'staDel', 'error')} required">
	<label class="control-label" for="staDel">
		<g:message code="divisi.staDel.label" default="M012 Sta Del" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="staDel" required="" value="${divisiInstance?.staDel}"/>
	</div>
</div>
--}%
