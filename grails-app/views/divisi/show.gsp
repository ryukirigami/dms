

<%@ page import="com.kombos.administrasi.Divisi" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'divisi.label', default: 'Divisi')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteDivisi;

$(function(){ 
	deleteDivisi=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/divisi/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadDivisiTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-divisi" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="divisi"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${divisiInstance?.companyDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="companyDealer-label" class="property-label"><g:message
					code="divisi.companyDealer.label" default="Company Dealer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="companyDealer-label">
						%{--<ba:editableValue
								bean="${divisiInstance}" field="companyDealer"
								url="${request.contextPath}/Divisi/updatefield" type="text"
								title="Enter companyDealer" onsuccess="reloadDivisiTable();" />--}%
							
								${divisiInstance?.companyDealer?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${divisiInstance?.m012NamaDivisi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m012NamaDivisi-label" class="property-label"><g:message
					code="divisi.m012NamaDivisi.label" default="Nama Divisi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m012NamaDivisi-label">
						%{--<ba:editableValue
								bean="${divisiInstance}" field="m012NamaDivisi"
								url="${request.contextPath}/Divisi/updatefield" type="text"
								title="Enter m012NamaDivisi" onsuccess="reloadDivisiTable();" />--}%
							
								<g:fieldValue bean="${divisiInstance}" field="m012NamaDivisi"/>
							
						</span></td>
					
				</tr>
				</g:if>
<!--			
				<g:if test="${divisiInstance?.m012ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m012ID-label" class="property-label"><g:message
					code="divisi.m012ID.label" default="ID" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m012ID-label">
						%{--<ba:editableValue
								bean="${divisiInstance}" field="m012ID"
								url="${request.contextPath}/Divisi/updatefield" type="text"
								title="Enter m012ID" onsuccess="reloadDivisiTable();" />--}%
							
								<g:fieldValue bean="${divisiInstance}" field="m012ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${divisiInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="divisi.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${divisiInstance}" field="staDel"
								url="${request.contextPath}/Divisi/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadDivisiTable();" />--}%
							
								<g:fieldValue bean="${divisiInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
-->

            <g:if test="${divisiInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="divisi.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${divisiInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/divisi/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloaddivisiTable();" />--}%

                        <g:fieldValue bean="${divisiInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>


            <g:if test="${divisiInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="divisi.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${divisiInstance}" field="dateCreated"
                                url="${request.contextPath}/divisi/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloaddivisiTable();" />--}%

                        <g:formatDate date="${divisiInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${divisiInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="divisi.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${divisiInstance}" field="createdBy"
                                url="${request.contextPath}/divisi/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloaddivisiTable();" />--}%

                        <g:fieldValue bean="${divisiInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${divisiInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="divisi.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${divisiInstance}" field="lastUpdated"
                                url="${request.contextPath}/divisi/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloaddivisiTable();" />--}%

                        <g:formatDate date="${divisiInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${divisiInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="divisi.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${divisiInstance}" field="updatedBy"
                                url="${request.contextPath}/divisi/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloaddivisiTable();" />--}%

                        <g:fieldValue bean="${divisiInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
            
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${divisiInstance?.id}"
					update="[success:'divisi-form',failure:'divisi-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteDivisi('${divisiInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
