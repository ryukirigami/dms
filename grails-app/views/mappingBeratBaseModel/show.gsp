

<%@ page import="com.kombos.administrasi.MappingBeratBaseModel" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'mappingBeratBaseModel.label', default: 'Mapping Berat Base Model')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteMappingBeratBaseModel;

$(function(){ 
	deleteMappingBeratBaseModel=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/mappingBeratBaseModel/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadMappingBeratBaseModelTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-mappingBeratBaseModel" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="mappingBeratBaseModel"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${mappingBeratBaseModelInstance?.tipeBerat}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tipeBerat-label" class="property-label"><g:message
					code="mappingBeratBaseModel.tipeBerat.label" default="Tipe Berat" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tipeBerat-label">
						%{--<ba:editableValue
								bean="${mappingBeratBaseModelInstance}" field="tipeBerat"
								url="${request.contextPath}/MappingBeratBaseModel/updatefield" type="text"
								title="Enter tipeBerat" onsuccess="reloadMappingBeratBaseModelTable();" />--}%
							
								${mappingBeratBaseModelInstance?.tipeBerat?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${mappingBeratBaseModelInstance?.baseModel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="baseModel-label" class="property-label"><g:message
					code="mappingBeratBaseModel.baseModel.label" default="Base Model" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="baseModel-label">
						%{--<ba:editableValue
								bean="${mappingBeratBaseModelInstance}" field="baseModel"
								url="${request.contextPath}/MappingBeratBaseModel/updatefield" type="text"
								title="Enter baseModel" onsuccess="reloadMappingBeratBaseModelTable();" />--}%
							
								${mappingBeratBaseModelInstance?.baseModel?.m102NamaBaseModel?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${mappingBeratBaseModelInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="mappingBeratBaseModel.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${mappingBeratBaseModelInstance}" field="createdBy"
								url="${request.contextPath}/MappingBeratBaseModel/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadMappingBeratBaseModelTable();" />--}%
							
								<g:fieldValue bean="${mappingBeratBaseModelInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${mappingBeratBaseModelInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="mappingBeratBaseModel.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${mappingBeratBaseModelInstance}" field="updatedBy"
								url="${request.contextPath}/MappingBeratBaseModel/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadMappingBeratBaseModelTable();" />--}%
							
								<g:fieldValue bean="${mappingBeratBaseModelInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${mappingBeratBaseModelInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="mappingBeratBaseModel.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${mappingBeratBaseModelInstance}" field="lastUpdProcess"
								url="${request.contextPath}/MappingBeratBaseModel/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadMappingBeratBaseModelTable();" />--}%
							
								<g:fieldValue bean="${mappingBeratBaseModelInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${mappingBeratBaseModelInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="mappingBeratBaseModel.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${mappingBeratBaseModelInstance}" field="dateCreated"
								url="${request.contextPath}/MappingBeratBaseModel/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadMappingBeratBaseModelTable();" />--}%
							
								<g:formatDate date="${mappingBeratBaseModelInstance?.dateCreated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${mappingBeratBaseModelInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="mappingBeratBaseModel.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${mappingBeratBaseModelInstance}" field="lastUpdated"
								url="${request.contextPath}/MappingBeratBaseModel/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadMappingBeratBaseModelTable();" />--}%
							
								<g:formatDate date="${mappingBeratBaseModelInstance?.lastUpdated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${mappingBeratBaseModelInstance?.id}"
					update="[success:'mappingBeratBaseModel-form',failure:'mappingBeratBaseModel-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteMappingBeratBaseModel('${mappingBeratBaseModelInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
