<%@ page import="com.kombos.administrasi.BaseModel; com.kombos.administrasi.TipeBerat; com.kombos.administrasi.MappingBeratBaseModel" %>



<div class="control-group fieldcontain ${hasErrors(bean: mappingBeratBaseModelInstance, field: 'tipeBerat', 'error')} required">
	<label class="control-label" for="tipeBerat">
		<g:message code="mappingBeratBaseModel.tipeBerat.label" default="Tipe Berat" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="tipeBerat" name="tipeBerat.id" from="${TipeBerat.createCriteria().list {order("m026NamaTipeBerat", "asc")}}" optionKey="id" required="" value="${mappingBeratBaseModelInstance?.tipeBerat?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: mappingBeratBaseModelInstance, field: 'baseModel', 'error')} required">
	<label class="control-label" for="baseModel">
		<g:message code="mappingBeratBaseModel.baseModel.label" default="Base Model" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="baseModel" name="baseModel.id" from="${BaseModel.createCriteria().list {eq("staDel", "0");order("m102NamaBaseModel", "asc")}}" optionKey="id" required="" value="${mappingBeratBaseModelInstance?.baseModel?.id}" class="many-to-one"/>
	</div>
</div>

%{--<div class="control-group fieldcontain ${hasErrors(bean: mappingBeratBaseModelInstance, field: 'createdBy', 'error')} ">--}%
	%{--<label class="control-label" for="createdBy">--}%
		%{--<g:message code="mappingBeratBaseModel.createdBy.label" default="Created By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="createdBy" value="${mappingBeratBaseModelInstance?.createdBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: mappingBeratBaseModelInstance, field: 'updatedBy', 'error')} ">--}%
	%{--<label class="control-label" for="updatedBy">--}%
		%{--<g:message code="mappingBeratBaseModel.updatedBy.label" default="Updated By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="updatedBy" value="${mappingBeratBaseModelInstance?.updatedBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: mappingBeratBaseModelInstance, field: 'lastUpdProcess', 'error')} ">--}%
	%{--<label class="control-label" for="lastUpdProcess">--}%
		%{--<g:message code="mappingBeratBaseModel.lastUpdProcess.label" default="Last Upd Process" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="lastUpdProcess" value="${mappingBeratBaseModelInstance?.lastUpdProcess}"/>--}%
	%{--</div>--}%
%{--</div>--}%

