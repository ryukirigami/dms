<%@ page import="com.kombos.administrasi.CompanyDealer; com.kombos.administrasi.WorkItems" %>



<div class="control-group fieldcontain ${hasErrors(bean: workItemsInstance, field: 'm039WorkItems', 'error')} required">
	<label class="control-label" for="m039WorkItems">
		<g:message code="workItems.m039WorkItems.label" default="Work Items" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m039WorkItems" id="m039WorkItems" maxlength="16" required="" value="${workItemsInstance?.m039WorkItems}"/>
	</div>
</div>

<g:javascript>
    document.getElementById('m039WorkItems').focus();
</g:javascript>