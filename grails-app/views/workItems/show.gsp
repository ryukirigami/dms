

<%@ page import="com.kombos.administrasi.WorkItems" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'workItems.label', default: 'Work Items')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteWorkItems;

$(function(){ 
	deleteWorkItems=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/workItems/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadWorkItemsTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-workItems" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="workItems"
			class="table table-bordered table-hover">
			<tbody>

            <g:if test="${workItemsInstance?.companyDealer}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="companyDealer-label" class="property-label"><g:message
                                code="workItems.companyDealer.label" default="Company Dealer" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="companyDealer-label">
                        %{--<ba:editableValue
                                bean="${workItemsInstance}" field="companyDealer"
                                url="${request.contextPath}/WorkItems/updatefield" type="text"
                                title="Enter companyDealer" onsuccess="reloadWorkItemsTable();" />--}%
                        <g:fieldValue field="companyDealer" bean="${workItemsInstance}" />
                    </span></td>

                </tr>
            </g:if>

				<g:if test="${workItemsInstance?.m039WorkItems}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m039WorkItems-label" class="property-label"><g:message
					code="workItems.m039WorkItems.label" default="Work Items" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m039WorkItems-label">
						%{--<ba:editableValue
								bean="${workItemsInstance}" field="m039WorkItems"
								url="${request.contextPath}/WorkItems/updatefield" type="text"
								title="Enter m039WorkItems" onsuccess="reloadWorkItemsTable();" />--}%
							
								<g:fieldValue bean="${workItemsInstance}" field="m039WorkItems"/>
							
						</span></td>
					
				</tr>
				</g:if>

                <g:if test="${workItemsInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="workItems.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${workItemsInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/workItems/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadworkItemsTable();" />--}%

                        <g:fieldValue bean="${workItemsInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
                </g:if>
			
				<g:if test="${workItemsInstance?.m039WorkID}">
				<tr style="display: none">
				<td class="span2" style="text-align: right;"><span
					id="m039WorkID-label" class="property-label"><g:message
					code="workItems.m039WorkID.label" default="M039 Work ID" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m039WorkID-label">
						%{--<ba:editableValue
								bean="${workItemsInstance}" field="m039WorkID"
								url="${request.contextPath}/WorkItems/updatefield" type="text"
								title="Enter m039WorkID" onsuccess="reloadWorkItemsTable();" />--}%
							
								<g:fieldValue bean="${workItemsInstance}" field="m039WorkID"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${workItemsInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="workItems.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${workItemsInstance}" field="dateCreated"
                                url="${request.contextPath}/workItems/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadworkItemsTable();" />--}%

                        <g:formatDate date="${workItemsInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${workItemsInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="workItems.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${workItemsInstance}" field="createdBy"
                                url="${request.contextPath}/workItems/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadworkItemsTable();" />--}%

                        <g:fieldValue bean="${workItemsInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${workItemsInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="workItems.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${workItemsInstance}" field="lastUpdated"
                                url="${request.contextPath}/workItems/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadworkItemsTable();" />--}%

                        <g:formatDate date="${workItemsInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${workItemsInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="workItems.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${workItemsInstance}" field="updatedBy"
                                url="${request.contextPath}/workItems/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadworkItemsTable();" />--}%

                        <g:fieldValue bean="${workItemsInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>


			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
                        code="default.button.cancel.label" default="Cancel" /></a>
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${workItemsInstance?.id}"
					update="[success:'workItems-form',failure:'workItems-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteWorkItems('${workItemsInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
