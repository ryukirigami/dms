
<%@ page import="com.kombos.administrasi.WorkItems" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="workItems_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
    width="100%">
	<thead>
		<tr>

            <th style="border-bottom: none;padding: 5px;">
				<div><g:message code="workItems.m039WorkItems.label" default="Work Items" /></div>
			</th>

		
		</tr>
		<tr>


            <th style="border-top: none;padding: 5px;">
				<div id="filter_m039WorkItems" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m039WorkItems" class="search_init" />
				</div>
			</th>

		</tr>
	</thead>
</table>

<g:javascript>
var workItemsTable;
var reloadWorkItemsTable;
$(function(){
	
	reloadWorkItemsTable = function() {
		workItemsTable.fnDraw();
	}

    var recordsworkItemsperpage = [];
    var anWorkItemsSelected;
    var jmlRecWorkItemsPerPage=0;
    var id;

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	workItemsTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	workItemsTable = $('#workItems_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsWorkItems = $("#workItems_datatables tbody .row-select");
            var jmlWorkItemsCek = 0;
            var nRow;
            var idRec;
            rsWorkItems.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsworkItemsperpage[idRec]=="1"){
                    jmlWorkItemsCek = jmlWorkItemsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsworkItemsperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecWorkItemsPerPage = rsWorkItems.length;
            if(jmlWorkItemsCek==jmlRecWorkItemsPerPage && jmlRecWorkItemsPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
        "fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [


{
	"sName": "m039WorkItems",
	"mDataProp": "m039WorkItems",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"100%",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m039WorkItems = $('#filter_m039WorkItems input').val();
						if(m039WorkItems){
							aoData.push(
									{"name": 'sCriteria_m039WorkItems', "value": m039WorkItems}
							);
						}
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}
	
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
    $('.select-all').click(function(e) {
        $("#workItems_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsworkItemsperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsworkItemsperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

	$('#workItems_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsworkItemsperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anWorkItemsSelected = workItemsTable.$('tr.row_selected');
            if(jmlRecWorkItemsPerPage == anWorkItemsSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsworkItemsperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
