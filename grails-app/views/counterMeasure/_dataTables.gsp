
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>


<table id="counterMeasure_datatables_${idTable}" cellpadding="0" cellspacing="0" border="0"
       class="display table table-striped table-bordered table-hover" style="table-layout: fixed; ">
    <thead>
    <tr>
        <th style="border-bottom: none;padding: 5px;">
            <g:message code="wo.t401NoWo.label" default="Nomor Wo"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="counterMeasure.t802TglJamFU.label" default="Tanggal Follow Up"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="counterMeasure.t401NamaSA.label" default="SA"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="wo.noPolisi.label" default="Nomor Polisi"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="counterMeasure.q1.label" default="Q1"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="counterMeasure.q2.label" default="Q2"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="counterMeasure.q3.label" default="Q3"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="counterMeasure.q4.label" default="Q4"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="counterMeasure.q5.label" default="Q5"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="counterMeasure.t802StaCounterMeasure.label" default="Counter Measure"/>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <g:message code="counterMeasure.keterangan.label" default="Keterangan"/>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var counterMeasureTable;
var reloadCounterMeasureTable;
var followUp="";
$(function(){

 	$('#view').click(function(e){
 	    followUp=$('#statusCounterMeasure').val();
 	    if(followUp=='Invalid'){
 	        $('#followUp').prop('disabled',false)
 	    }else{
 	        $('#followUp').prop('disabled',true)
 	    }
        counterMeasureTable.fnDraw();
	});
	$('#clear').click(function(e){
	    followUp = "";
	    $('#search_TanggalFollowUp').val('');
	    $('#search_TanggalFollowUpAkhir').val('');
        $('#search_TanggalService').val('');
	    $('#search_TanggalServiceAkhir').val('');
	    $('#nopol1').val('');
	    $('#nopol2').val('');
	    $('#nopol3').val('');
	    $('#nomorWo').val('');
	    $('#inisialSA').val('');
	    $('#staCounterMeasure').val('');
	    $('#followUp').prop('disabled',true);
        counterMeasureTable.fnDraw();
	});
 
	reloadCounterMeasureTable = function() {

		counterMeasureTable.fnDraw();
	}


    counterMeasureTable = $('#counterMeasure_datatables_${idTable}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
               $(nRow).children().each(function(index, td) {
                        if(index === 9){
                            if (aData.status == "Valid") {
                                 $(td).html("<img src='images/followUp/ok.png' style='display: block; margin: 0 auto; text-align: center; '/>");
                            }else if (aData.status == "Invalid") {
                                 $(td).html("<img src='images/followUp/remove.png' style='display: block; margin: 0 auto; text-align: center; '/>");
                            }else{
                                 $(td).html("<img src='images/followUp/warning.png' style='display: block; margin: 0 auto; text-align: center; '/>");
                            }

                        }
                        if(index == 10){
                            if ($(td).html() == "Sudah") {
                                   $(td).css("background-color","#06B33A");
                            }else{
                                   $(td).css("background-color","#fbed50");
                            }

                        }
               });

			    return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"bDestroy": true,
		"aoColumns": [
 {
	"sName": "t802NamaSA_FU",
	"mDataProp": "nomorWo",
	"aTargets": [0],
    "mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select"  aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'"> &nbsp; '+data;
	},
	"bSortable": false,
	"sWidth":"110px",
	"bVisible": true
}
,
{
	"sName": "t802TglJamFU",
	"mDataProp": "t802TglJamFU",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"120px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "SA",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"75px",
	"bVisible": true
}
,
{
	"sName": "followUp",
	"mDataProp": "noPol",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"100px",
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "Q1",
	"aTargets": [4],
	"sWidth":"80px",
	"bSortable": false,
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "Q2",
	"aTargets": [5],
	"sWidth":"80px",
	"bSortable": false,
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "Q3",
	"aTargets": [6],
	"sWidth":"80px",
	"bSortable": false,
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "Q4",
	"aTargets": [7],
	"sWidth":"80px",
	"bSortable": false,
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "Q5",
	"aTargets": [8],
	"sWidth":"80px",
	"bSortable": false,
	"sDefaultContent": '',
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "status",
	"aTargets": [9],
	"bSortable": false,
	"sWidth":"130px",
	"bVisible": true
},
{
	"sName": "followUp",
	"mDataProp": "ket",
	"aTargets": [10],
	"bSortable": false,
	"sWidth":"130px",
	"bVisible": true
}

],
    "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
    
                        var Tanggal = $('#search_TanggalFollowUp').val();
						var TanggalDay = $('#search_TanggalFollowUp_day').val();
						var TanggalMonth = $('#search_TanggalFollowUp_month').val();
						var TanggalYear = $('#search_TanggalFollowUp_year').val();

	                    var Tanggalakhir = $('#search_TanggalFollowUpAkhir').val();
                        var TanggalDayakhir = $('#search_TanggalFollowUpAkhir_day').val();
                        var TanggalMonthakhir = $('#search_TanggalFollowUpAkhir_month').val();
                        var TanggalYearakhir = $('#search_TanggalFollowUpAkhir_year').val();


                        if(Tanggal){
                            aoData.push(
                                    {"name": 'search_TanggalFollowUp', "value": "date.struct"},
                                    {"name": 'search_TanggalFollowUp_dp', "value": Tanggal},
                                    {"name": 'search_TanggalFollowUp_day', "value": TanggalDay},
                                    {"name": 'search_TanggalFollowUp_month', "value": TanggalMonth},
                                    {"name": 'search_TanggalFollowUp_year', "value": TanggalYear}
                            );
                        }

                        if(Tanggalakhir){
                            aoData.push(
                                    {"name": 'search_TanggalFollowUpAkhir', "value": "date.struct"},
                                    {"name": 'search_TanggalFollowUpAkhir_dp', "value": Tanggalakhir},
                                    {"name": 'search_TanggalFollowUpAkhir_day', "value": TanggalDayakhir},
                                    {"name": 'search_TanggalFollowUpAkhir_month', "value":TanggalMonthakhir},
                                    {"name": 'search_TanggalFollowUpAkhir_year', "value": TanggalYearakhir}
                            );
                        }

                        var Tanggal2 = $('#search_TanggalService').val();
						var Tanggal2Day = $('#search_TanggalService_day').val();
						var Tanggal2Month = $('#search_TanggalService_month').val();
						var Tanggal2Year = $('#search_TanggalService_year').val();

	                    var Tanggal2akhir = $('#search_TanggalServiceAkhir').val();
                        var Tanggal2Dayakhir = $('#search_TanggalServiceAkhir_day').val();
                        var Tanggal2Monthakhir = $('#search_TanggalServiceAkhir_month').val();
                        var Tanggal2Yearakhir = $('#search_TanggalServiceAkhir_year').val();


                        if(Tanggal2){
                            aoData.push(
                                    {"name": 'search_TanggalService', "value": "date.struct"},
                                    {"name": 'search_TanggalService_dp', "value": Tanggal2},
                                    {"name": 'search_TanggalService_day', "value": Tanggal2Day},
                                    {"name": 'search_TanggalService_month', "value": Tanggal2Month},
                                    {"name": 'search_TanggalService_year', "value": Tanggal2Year}
                            );
                        }

                        if(Tanggal2akhir){
                            aoData.push(
                                    {"name": 'search_TanggalServiceAkhir', "value": "date.struct"},
                                    {"name": 'search_TanggalServiceAkhir_dp', "value": Tanggal2akhir},
                                    {"name": 'search_TanggalServiceAkhir_day', "value": Tanggal2Dayakhir},
                                    {"name": 'search_TanggalServiceAkhir_month', "value":Tanggal2Monthakhir},
                                    {"name": 'search_TanggalServiceAkhir_year', "value": Tanggal2Yearakhir}
                            );
                        }


						var statusCounterMeasure = $('#staCounterMeasure').val();
						if(statusCounterMeasure){
							aoData.push(
									{"name": 'statusCounterMeasure', "value": statusCounterMeasure}
							);
						}

						var nopol1 = $('#nopol1').val();
						if(nopol1){
							aoData.push(
									{"name": 'nopol1', "value": nopol1}
							);
						}
						var nopol2 = $('#nopol2').val();
						if(nopol2){
							aoData.push(
									{"name": 'nopol2', "value": nopol2}
							);
						}
						var nopol3 = $('#nopol3').val();
						if(nopol3){
							aoData.push(
									{"name": 'nopol3', "value": nopol3}
							);
						}

						var inisialSA = $('#inisialSA').val();
						if(inisialSA){
							aoData.push(
									{"name": 'inisialSA', "value": inisialSA}
							);
						}
						var nomorWo = $('#nomorWo').val();
						if(nomorWo){
							aoData.push(
									{"name": 'nomorWo', "value": nomorWo}
							);
						}



    $.ajax({ "dataType": 'json',
        "type": "POST",
        "url": sSource,
        "data": aoData ,
        "success": function (json) {
            fnCallback(json);
           },
        "complete": function () {
           }
    });
}
});
});


</g:javascript>
