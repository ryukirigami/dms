

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="edit_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>
        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="wo.t401NoWo.label" default="Nomor WO" /></div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="counterMeasure.alasanQ1.label" default="Alasan Q1" /></div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="counterMeasure.alasanQ2.label" default="Alasan Q2" /></div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="counterMeasure.alasanQ3.label" default="Alasan Q3" /></div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="counterMeasure.alasanQ4.label" default="Alasan Q4" /></div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="counterMeasure.alasanQ5.label" default="Alasan Q5" /></div>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
var EditTable;
var reloadEditTable;
$(function(){
	reloadEditTable = function() {
	    EditTable.fnClearTable();
		EditTable.fnDraw();
	}


	EditTable = $('#edit_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		bFilter: false,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            return nRow;
        },
		"bSort": true,
		"bProcessing": true,
		"bDestroy":true,
//		"bServerSide": true,
//		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
//        "sAjaxSource": "${g.createLink(action: "datatablesListEdit")}",
    "aoColumns": [



{
    "sName": "",
    "mDataProp": "noWO",
    "aTargets": [0],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"20%",
	"bVisible": true
}

,

{
	"sName": "",
	"mDataProp": "alasanQ1",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"16%",
	"bVisible": true
}

,

{
	"sName": "",
	"mDataProp": "alasanQ2",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"16%",
	"bVisible": true
}

,

{
	"sName": "",
	"mDataProp": "alasanQ3",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"16%",
	"bVisible": true
}
,

{
	"sName": "alasanQ4",
	"mDataProp": "alasanQ4",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"16%",
	"bVisible": true
}
,

{
	"sName": "alasanQ5",
	"mDataProp": "alasanQ5",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"16%",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
//            var idHide = $('#idHide').val();
//            console.log('id hide = '+idHide);
//            if(idHide){
//                aoData.push(
//                        {"name": 'id', "value": idHide}
//                );
//            }else{
//                aoData.push(
//                        {"name": 'id', "value": -1}
//                );
//            }

            $.ajax({ "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData ,
                "success": function (json) {
                    fnCallback(json);
                   },
                "complete": function () {
                   }
            });
		}
	});

});
</g:javascript>



