
<%@ page import="com.kombos.administrasi.KodeKotaNoPol;com.kombos.reception.Reception"%>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="counterMeasure.label" default="Counter Measure" /></title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>
        var ubah;
        $(function(){
            ubah = function(){
                var records = [];
                var idUbah;
                $('#counterMeasure-table tbody .row-select').each(function() {
                    if(this.checked){
                        var id = $(this).next("input:hidden").val();
                        records.push(id);
                        idUbah = id;
                    }
                });
                if(records.length<1){
                    alert('Anda belum memilih data');
                    return;
                }
                if(records.length>1){
                    alert('Anda hanya boleh memilih 1 data');
                    return;
                }

                $.ajax({
                    url:'${request.contextPath}/counterMeasure/cariData',
                    type: "POST", // Always use POST when deleting data
                    data: { idUbah: idUbah },
                    success: function(data){
                        var $radio1 = $('input:radio[name=t802StaCounterMeasure]');
                        if(data.t802StaCounterMeasure=='1'){
                            $radio1.filter('[value=1]').prop('checked', true);
                        }else{
                            $radio1.filter('[value=0]').prop('checked', true);
                        }
                        var $radio2 = $('input:radio[name=t802StaPotensiRTJ]');
                        if(data.t802StaPotensiRTJ=='1'){
                            $radio2.filter('[value=1]').prop('checked', true);
                        }else{
                            $radio2.filter('[value=0]').prop('checked', true);
                        }
                        $('#t802CatatanCounterMeasure').val(data.t802CatatanCounterMeasure);
                        $('#idHide').val(data.id);
                        var idReFu = data.id;
                        console.log(idReFu);
                        $.ajax({
                            url:'${request.contextPath}/counterMeasure/datatablesListEdit',
                            type: "POST", // Always use POST when deleting data
                            data: { id: idReFu },
                            success: function(data){
                                $.each(data,function(i,item){
                                    EditTable.fnAddData({
                                        'noWO': item.noWO,
                                        'alasanQ1': item.alasanQ1,
                                        'alasanQ2': item.alasanQ2,
                                        'alasanQ3': item.alasanQ3,
                                        'alasanQ4': item.alasanQ4,
                                        'alasanQ5': item.alasanQ5
                                    });
                                });
                            },error: function(xhr, textStatus, errorThrown) { alert(textStatus);}
                        });
                    },error: function(xhr, textStatus, errorThrown) { alert(textStatus);}
                });
                records = [];
                $('#save').prop('disabled',false);
                $('#cancel').prop('disabled',false);
                $('#edit').prop('disabled',true);
            }
        });

    function saveCM(){
        var idUbah = $('#idHide').val();
        var staCM;
        var staPotensiRTJ;
        if($('input:radio[name=t802StaCounterMeasure]:nth(0)').is(':checked')){
            staCM = '1';
        }else{
            staCM = '0';
        }
        if($('input:radio[name=t802StaPotensiRTJ]:nth(0)').is(':checked')){
            staPotensiRTJ = '1';
        }else{
            staPotensiRTJ = '0';
        }
        console.log('sta cm '+staCM+' stapotensi '+staPotensiRTJ);
        var t802CatatanCounterMeasure = $('#t802CatatanCounterMeasure').val();
        $.ajax({
            url:'${request.contextPath}/counterMeasure/saveData',
            type: "POST", // Always use POST when deleting data
            data: { idUbah: idUbah, t802StaCounterMeasure:staCM,t802CatatanCounterMeasure:t802CatatanCounterMeasure,t802StaPotensiRTJ:staPotensiRTJ},
            success: function(data){
                $('input:radio[name=t802StaCounterMeasure]').each(function(){
                   this.checked = false;
                });
                $('input:radio[name=t802StaPotensiRTJ]').each(function(){
                   this.checked = false;
                });
                $('#t802CatatanCounterMeasure').val('');
                $('#idHide').val('');
                reloadCounterMeasureTable();
                reloadEditTable();
            },error: function(xhr, textStatus, errorThrown) { alert(textStatus);}
        });
        $('#save').prop('disabled',true);
        $('#cancel').prop('disabled',true);
        $('#edit').prop('disabled',false);
    }

    function cancelCM(){
        $('input:radio[name=t802StaCounterMeasure]').each(function(){
           this.checked = false;
        });
        $('input:radio[name=t802StaPotensiRTJ]').each(function(){
           this.checked = false;
        });$('#t802CatatanCounterMeasure').val('');
        $('#idHide').val('');
        $('#save').prop('disabled',true);
        $('#cancel').prop('disabled',true);
        $('#edit').prop('disabled',false);
        reloadCounterMeasureTable();
        reloadEditTable();
    }

    var checkin = $('#search_TanggalFollowUp').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
        var newDate = new Date(ev.date)
        newDate.setDate(newDate.getDate() + 1);
        checkout.setValue(newDate);
        checkin.hide();
        $('#search_TanggalFollowUpAkhir')[0].focus();
    }).data('datepicker');
    
    var checkout = $('#search_TanggalFollowUpAkhir').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

    var checkin2 = $('#search_TanggalService').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
                var newDate2 = new Date(ev.date)
                newDate2.setDate(newDate2.getDate() + 1);
                checkout2.setValue(newDate2);
                checkin2.hide();
                $('#search_TanggalServiceAkhir')[0].focus();
    }).data('datepicker');

    var checkout2 = $('#search_TanggalServiceAkhir').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin2.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
                checkout2.hide();
    }).data('datepicker');


    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

        return true;
    }
    $(document).ready(function() {
        $("#nopol3").keyup(function(e) {
            var isi = $(e.target).val();
            $(e.target).val(isi.toUpperCase());
        });
    });
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="counterMeasure.label" default="Counter Measure" /></span>
    <ul class="nav pull-right">

    </ul>
</div>
<div class="box">
    <div class="span12" id="counterMeasure-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <fieldset>
            <legend style="font-size: 14px">
                <g:message code="filter.counterMeasure.label" default="Filter Counter Measure" />
            </legend>
        </fieldset>
        <table>
            <tr>
                <td style="padding: 5px">
                    <g:message code="wo.tanggalWOs.label" default="Tanggal Service"/>
                </td>
                <td style="padding: 5px">
                    <ba:datePicker name="search_TanggalService" id="search_TanggalService" precision="day" value="" disable="true" format="dd-MM-yyyy"/>&nbsp;s.d.&nbsp;
                    <ba:datePicker name="search_TanggalServiceAkhir" id="search_TanggalServiceAkhir" precision="day" value="" disable="true" format="dd-MM-yyyy"/>
                </td>
                <td style="padding: 5px">
                    <g:message code="wo.tanggalWOs.label" default="Tanggal Follow Up"/>
                </td>
                <td style="padding: 5px">
                    <ba:datePicker name="search_TanggalFollowUp" id="search_TanggalFollowUp" precision="day" value="" disable="true" format="dd-MM-yyyy"/>&nbsp;s.d.&nbsp;
                    <ba:datePicker name="search_TanggalFollowUpAkhir" id="search_TanggalFollowUpAkhir" precision="day" value="" disable="true" format="dd-MM-yyyy"/>
                </td>
                <td rowspan="3" style="vertical-align: top;">
                    <div class="controls" style="vertical-align: top;">
                        <button style="width: 70px;height: 30px; border-radius: 5px" class="btn-primary view" name="view" id="view" >Filter</button>
                        <button style="width: 70px;height: 30px; border-radius: 5px" class="btn cancel" name="clear" id="clear" >Clear</button>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    <g:message code="counterMeasure.staJobTambah.label" default="Nomor Polisi"/>
                </td>
                <td style="padding: 5px">
                    <g:select name="nopol1" id="nopol1" from="${KodeKotaNoPol.createCriteria().list {eq("staDel",'0')}}" optionKey="id" style="width: 70px;"/>
                    <g:textField name="nopol2" id="nopol2" style="width: 95px" maxlength="5" onkeypress="return isNumberKey(event)" />
                    <g:textField name="nopol3" id="nopol3" style="width: 40px" maxlength="3" />
                </td>
                <td style="padding: 5px">
                    <g:message code="counterMeasure.staProblemFinding.label" default="Inisial SA"/>
                </td>

                <td style="padding: 5px">
                    <g:select style="width:100%" name="inisialSA" id="inisialSA" from="${namaSA}" optionValue="username" optionKey="username" noSelection="${['':'Semua']}" />
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    <g:message code="counterMeasure.staCarryOver.label" default="Nomor WO"/>
                </td>
                <td style="padding: 5px">
                    <g:textField name="nomorWo" id="nomorWo" style="width:98%"/>
                </td>
                <td style="padding: 5px">
                    <g:message code="counterMeasure.staCounterMeasure.label" default="Status Counter Measure"/>
                </td>
                <td style="padding: 5px">
                    <select name="staCounterMeasure" id="staCounterMeasure" style="width:100%">
                        <option value="">Semua</option>
                        <option value="1">Sudah</option>
                        <option value="0">Belum</option>
                    </select>
                </td>
            </tr>
        </table>
        <br/>
        <fieldset>
            <legend style="font-size: 14px">
                Counter Measure
            </legend>
        </fieldset>
        <g:render template="dataTables" />
        <br/>

        <div class="row-fluid">
            <div id="kiri" class="span8">
                <g:render template="dataTablesEdit" />
            </div>

            <div id="kanan" class="span4">
                <input type="hidden" name="idHide" id="idHide" />
                <g:render template="formEdit"/>
            </div>
        </div>
        <div class="controls" style="right: 0">
            <button class="btn cancel" name="btnAppointment" id="btnAppointment" onclick="window.location.href='#/appointment'" >Appointment</button>
            <a class="pull-right">
                &nbsp;
                <i>
                    <button id='cancel' onclick="cancelCM();" style="width: 100px" disabled="" type="button" class="btn cancel">Cancel</button>
                </i>
                &nbsp;
            </a>
            <a class="pull-right">
                &nbsp;
                <i>
                    <button id='save' onclick="saveCM();" style="width: 100px" disabled="" type="button" class="btn btn-primary">Save</button>
                </i>
                &nbsp;
            </a>
            <a class="pull-right">
                &nbsp;
                <i>
                    <button id='edit' onclick="ubah();" style="width: 100px" type="button" class="btn cancel">Edit</button>
                </i>
                &nbsp;
            </a>
        </div>
    </div>

    <div class="span7" id="counterMeasure-form" style="display: none; width: 1200px;"></div>
</div>
</body>
</html>
