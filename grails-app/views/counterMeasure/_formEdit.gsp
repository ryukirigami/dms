
<table>
    <tr>
        <td style="padding: 5px">
            <g:message code="counterMeasure.label" default="Counter Measure" />
        </td>
        <td style="padding: 5px">
            <g:radioGroup name="t802StaCounterMeasure" id="t802StaCounterMeasure" values="['1','0']" labels="['Sudah','Belum']" required="" value="">
                ${it.radio} ${it.label}
            </g:radioGroup>
        </td>
    </tr>
    <tr>
        <td style="padding: 5px">
            <g:message code="counterMeasure.t802CatatanCounterMeasure.label" default="Catatan Counter Measure" />
        </td>
        <td style="padding: 5px">
            <g:textArea name="t802CatatanCounterMeasure" id="t802CatatanCounterMeasure" style="resize:none"/>
        </td>
    </tr>
    <tr>
        <td style="padding: 5px">
            <g:message code="counterMeasure.t802StaPotensiRTJ.label" default="Potensi Return Job" />
        </td>
        <td style="padding: 5px">
            <g:radioGroup name="t802StaPotensiRTJ" id="t802StaPotensiRTJ" values="['1','0']" labels="['Ya','Tidak']" required="" value="">
                ${it.radio} ${it.label}
            </g:radioGroup>
        </td>
    </tr>
</table>