<%@ page import="com.kombos.administrasi.JenisStall" %>



<div class="control-group fieldcontain ${hasErrors(bean: jenisStallInstance, field: 'm021NamaJenisStall', 'error')} required">
	<label class="control-label" for="m021NamaJenisStall">
		<g:message code="jenisStall.m021NamaJenisStall.label" default="Nama Jenis Stall" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m021NamaJenisStall" id="m021NamaJenisStall" maxlength="50" required="" value="${jenisStallInstance?.m021NamaJenisStall}"/>
	</div>
</div>

<g:javascript>
    document.getElementById('m021NamaJenisStall').focus();
</g:javascript>
%{--
<div class="control-group fieldcontain ${hasErrors(bean: jenisStallInstance, field: 'm021ID', 'error')} ">
	<label class="control-label" for="m021ID">
		<g:message code="jenisStall.m021ID.label" default="M021 ID" />
		
	</label>
	<div class="controls">
	<g:textField name="m021ID" maxlength="1" value="${jenisStallInstance?.m021ID}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: jenisStallInstance, field: 'staDel', 'error')} required">
	<label class="control-label" for="staDel">
		<g:message code="jenisStall.staDel.label" default="Sta Del" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="staDel" maxlength="1" required="" value="${jenisStallInstance?.staDel}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: jenisStallInstance, field: 'createdBy', 'error')} ">
	<label class="control-label" for="createdBy">
		<g:message code="jenisStall.createdBy.label" default="Created By" />
		
	</label>
	<div class="controls">
	<g:textField name="createdBy" value="${jenisStallInstance?.createdBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: jenisStallInstance, field: 'updatedBy', 'error')} ">
	<label class="control-label" for="updatedBy">
		<g:message code="jenisStall.updatedBy.label" default="Updated By" />
		
	</label>
	<div class="controls">
	<g:textField name="updatedBy" value="${jenisStallInstance?.updatedBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: jenisStallInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="jenisStall.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${jenisStallInstance?.lastUpdProcess}"/>
	</div>
</div>
--}%
