

<%@ page import="com.kombos.administrasi.JenisStall" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'jenisStall.label', default: 'Jenis Stall')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteJenisStall;

$(function(){ 
	deleteJenisStall=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/jenisStall/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadJenisStallTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-jenisStall" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="jenisStall"
			class="table table-bordered table-hover">
			<tbody>

            <g:if test="${jenisStallInstance?.m021ID}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m021ID-label" class="property-label"><g:message
                                code="jenisStall.m021ID.label" default="ID" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m021ID-label">
                        %{--<ba:editableValue
                                bean="${jenisStallInstance}" field="m021ID"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter m021ID" onsuccess="reloadJenisStallTable();" />--}%

                        <g:fieldValue bean="${jenisStallInstance}" field="m021ID"/>

                    </span></td>

                </tr>
            </g:if>

				<g:if test="${jenisStallInstance?.m021NamaJenisStall}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m021NamaJenisStall-label" class="property-label"><g:message
					code="jenisStall.m021NamaJenisStall.label" default="Nama Jenis Stall" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m021NamaJenisStall-label">
						%{--<ba:editableValue
								bean="${jenisStallInstance}" field="m021NamaJenisStall"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter m021NamaJenisStall" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:fieldValue bean="${jenisStallInstance}" field="m021NamaJenisStall"/>
							
						</span></td>
					
				</tr>
				</g:if>
			

			%{--
				<g:if test="${jenisStallInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="jenisStall.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${jenisStallInstance}" field="staDel"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadJenisStallTable();" />
							
								<g:fieldValue bean="${jenisStallInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>--}%

            <g:if test="${jenisStallInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="jenisStall.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${jenisStallInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadJenisStallTable();" />--}%

                        <g:fieldValue bean="${jenisStallInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${jenisStallInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="jenisStall.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${jenisStallInstance}" field="dateCreated"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:formatDate date="${jenisStallInstance?.dateCreated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${jenisStallInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="jenisStall.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${jenisStallInstance}" field="createdBy"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:fieldValue bean="${jenisStallInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${jenisStallInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="jenisStall.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${jenisStallInstance}" field="lastUpdated"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:formatDate date="${jenisStallInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${jenisStallInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="jenisStall.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${jenisStallInstance}" field="updatedBy"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:fieldValue bean="${jenisStallInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${jenisStallInstance?.id}"
					update="[success:'jenisStall-form',failure:'jenisStall-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteJenisStall('${jenisStallInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
