
<%@ page import="com.kombos.administrasi.CompanyDealer; com.kombos.hrd.HistoryRewardKaryawan" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="historyRewardKaryawan_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="historyRewardKaryawan.rewardKaryawan.label" default="Reward Karyawan" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="historyRewardKaryawan.tanggalReward.label" default="Tanggal Reward" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="historyRewardKaryawan.tanggalReward.label" default="Company Dealer" /></div>
            </th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="historyRewardKaryawan.karyawan.label" default="Karyawan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="historyRewardKaryawan.keterangan.label" default="Keterangan" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="historyRewardKaryawan.keterangan.label" default="Approval" /></div>
            </th>




		
		</tr>
		<tr>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_rewardKaryawan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_rewardKaryawan" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_tanggalReward" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
                    <input type="hidden" name="search_tanggalReward" value="date.struct">
                    <input type="hidden" name="search_tanggalReward_day" id="search_tanggalReward_day" value="">
                    <input type="hidden" name="search_tanggalReward_month" id="search_tanggalReward_month" value="">
                    <input type="hidden" name="search_tanggalReward_year" id="search_tanggalReward_year" value="">
                    <input type="text" data-date-format="dd/mm/yyyy" name="search_tanggalReward_dp" value="" id="search_tanggalReward" class="search_init">
                </div>
            </th>

  
            <th style="border-top: none;padding: 5px;">
                <div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 185px;">
                    <g:if test="${params.companyDealer.toString().contains("HO")}">
                        <g:select name="search_companyDealer" id="workshop" from="${CompanyDealer.createCriteria().list{eq('staDel','0');order('m011NamaWorkshop')}}" optionKey="id" optionValue="m011NamaWorkshop" style="width: 100%" noSelection="['':'SEMUA']" onchange="reloadHistoryRewardKaryawanTable()" />
                    </g:if>
                </div>
            </th>


            <th style="border-top: none;padding: 5px;">
				<div id="filter_karyawan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_karyawan" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_keterangan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_keterangan" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_approval" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_approval" class="search_init" />
                </div>
            </th>

		</tr>
	</thead>
</table>

<g:javascript>
var historyRewardKaryawanTable;
var reloadHistoryRewardKaryawanTable;
$(function(){
	
	reloadHistoryRewardKaryawanTable = function() {
		historyRewardKaryawanTable.fnDraw();
	}

	
	$('#search_tanggalReward').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tanggalReward_day').val(newDate.getDate());
			$('#search_tanggalReward_month').val(newDate.getMonth()+1);
			$('#search_tanggalReward_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			historyRewardKaryawanTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	historyRewardKaryawanTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	historyRewardKaryawanTable = $('#historyRewardKaryawan_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "rewardKaryawan",
	"mDataProp": "rewardKaryawan",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "tanggalReward",
	"mDataProp": "tanggalReward",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}



,

{
	"sName": "workshop",
	"mDataProp": "workshop",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "karyawan",
	"mDataProp": "karyawan",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "keterangan",
	"mDataProp": "keterangan",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "staApproval",
	"mDataProp": "Approval",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var karyawan = $('#filter_karyawan input').val();
						if(karyawan){
							aoData.push(
									{"name": 'sCriteria_karyawan', "value": karyawan}
							);
						}
	
						var keterangan = $('#filter_keterangan input').val();
						if(keterangan){
							aoData.push(
									{"name": 'sCriteria_keterangan', "value": keterangan}
							);
						}



                      var companyDealer = $("#workshop").val();

                        if (companyDealer) {
                            aoData.push({"name": "sCriteria_companyDealer", "value": companyDealer});
                        }

	
						var rewardKaryawan = $('#filter_rewardKaryawan input').val();
						if(rewardKaryawan){
							aoData.push(
									{"name": 'sCriteria_rewardKaryawan', "value": rewardKaryawan}
							);
						}

						var tanggalReward = $('#search_tanggalReward').val();
						var tanggalRewardDay = $('#search_tanggalReward_day').val();
						var tanggalRewardMonth = $('#search_tanggalReward_month').val();
						var tanggalRewardYear = $('#search_tanggalReward_year').val();
						
						if(tanggalReward){
							aoData.push(
									{"name": 'sCriteria_tanggalReward', "value": "date.struct"},
									{"name": 'sCriteria_tanggalReward_dp', "value": tanggalReward},
									{"name": 'sCriteria_tanggalReward_day', "value": tanggalRewardDay},
									{"name": 'sCriteria_tanggalReward_month', "value": tanggalRewardMonth},
									{"name": 'sCriteria_tanggalReward_year', "value": tanggalRewardYear}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
