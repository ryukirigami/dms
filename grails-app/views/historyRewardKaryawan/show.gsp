

<%@ page import="com.kombos.hrd.HistoryRewardKaryawan" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'historyRewardKaryawan.label', default: 'HistoryRewardKaryawan')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteHistoryRewardKaryawan;

$(function(){ 
	deleteHistoryRewardKaryawan=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/historyRewardKaryawan/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadHistoryRewardKaryawanTable();
   				expandTableLayout('historyRewardKaryawan');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-historyRewardKaryawan" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="historyRewardKaryawan"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${historyRewardKaryawanInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="historyRewardKaryawan.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${historyRewardKaryawanInstance}" field="createdBy"
								url="${request.contextPath}/HistoryRewardKaryawan/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadHistoryRewardKaryawanTable();" />--}%
							
								<g:fieldValue bean="${historyRewardKaryawanInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyRewardKaryawanInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="historyRewardKaryawan.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${historyRewardKaryawanInstance}" field="dateCreated"
								url="${request.contextPath}/HistoryRewardKaryawan/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadHistoryRewardKaryawanTable();" />--}%
							
								<g:formatDate date="${historyRewardKaryawanInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyRewardKaryawanInstance?.karyawan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="karyawan-label" class="property-label"><g:message
					code="historyRewardKaryawan.karyawan.label" default="Karyawan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="karyawan-label">
						%{--<ba:editableValue
								bean="${historyRewardKaryawanInstance}" field="karyawan"
								url="${request.contextPath}/HistoryRewardKaryawan/updatefield" type="text"
								title="Enter karyawan" onsuccess="reloadHistoryRewardKaryawanTable();" />--}%
							
								<g:link controller="karyawan" action="show" id="${historyRewardKaryawanInstance?.karyawan?.id}">${historyRewardKaryawanInstance?.karyawan?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyRewardKaryawanInstance?.keterangan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="keterangan-label" class="property-label"><g:message
					code="historyRewardKaryawan.keterangan.label" default="Keterangan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="keterangan-label">
						%{--<ba:editableValue
								bean="${historyRewardKaryawanInstance}" field="keterangan"
								url="${request.contextPath}/HistoryRewardKaryawan/updatefield" type="text"
								title="Enter keterangan" onsuccess="reloadHistoryRewardKaryawanTable();" />--}%
							
								<g:fieldValue bean="${historyRewardKaryawanInstance}" field="keterangan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyRewardKaryawanInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="historyRewardKaryawan.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${historyRewardKaryawanInstance}" field="lastUpdProcess"
								url="${request.contextPath}/HistoryRewardKaryawan/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadHistoryRewardKaryawanTable();" />--}%
							
								<g:fieldValue bean="${historyRewardKaryawanInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyRewardKaryawanInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="historyRewardKaryawan.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${historyRewardKaryawanInstance}" field="lastUpdated"
								url="${request.contextPath}/HistoryRewardKaryawan/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadHistoryRewardKaryawanTable();" />--}%
							
								<g:formatDate date="${historyRewardKaryawanInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyRewardKaryawanInstance?.rewardKaryawan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="rewardKaryawan-label" class="property-label"><g:message
					code="historyRewardKaryawan.rewardKaryawan.label" default="Reward Karyawan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="rewardKaryawan-label">
						%{--<ba:editableValue
								bean="${historyRewardKaryawanInstance}" field="rewardKaryawan"
								url="${request.contextPath}/HistoryRewardKaryawan/updatefield" type="text"
								title="Enter rewardKaryawan" onsuccess="reloadHistoryRewardKaryawanTable();" />--}%
							
								<g:link controller="rewardKaryawan" action="show" id="${historyRewardKaryawanInstance?.rewardKaryawan?.id}">${historyRewardKaryawanInstance?.rewardKaryawan?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyRewardKaryawanInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="historyRewardKaryawan.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${historyRewardKaryawanInstance}" field="staDel"
								url="${request.contextPath}/HistoryRewardKaryawan/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadHistoryRewardKaryawanTable();" />--}%
							
								<g:fieldValue bean="${historyRewardKaryawanInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyRewardKaryawanInstance?.tanggalReward}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tanggalReward-label" class="property-label"><g:message
					code="historyRewardKaryawan.tanggalReward.label" default="Tanggal Reward" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tanggalReward-label">
						%{--<ba:editableValue
								bean="${historyRewardKaryawanInstance}" field="tanggalReward"
								url="${request.contextPath}/HistoryRewardKaryawan/updatefield" type="text"
								title="Enter tanggalReward" onsuccess="reloadHistoryRewardKaryawanTable();" />--}%
							
								<g:formatDate date="${historyRewardKaryawanInstance?.tanggalReward}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${historyRewardKaryawanInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="historyRewardKaryawan.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${historyRewardKaryawanInstance}" field="updatedBy"
								url="${request.contextPath}/HistoryRewardKaryawan/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadHistoryRewardKaryawanTable();" />--}%
							
								<g:fieldValue bean="${historyRewardKaryawanInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>

		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
                %{--<g:if test="${params.onDataKaryawan}">
                    <a class="btn cancel" href="javascript:void(0);"
                       onclick="$(this).parent().parent().parent().parent().parent().parent().modal('hide');">Close</a>
                </g:if>
                <g:else>--}%
                    <a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout('historyRewardKaryawan');"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
                    <g:remoteLink class="btn btn-primary edit" action="edit"
                                  id="${historyRewardKaryawanInstance?.id}"
                                  update="[success:'historyRewardKaryawan-form',failure:'historyRewardKaryawan-form']"
                                  on404="alert('not found');">
                        <g:message code="default.button.edit.label" default="Edit" />
                    </g:remoteLink>
                    <ba:confirm id="delete" class="btn cancel"
                                message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
                                onsuccess="deleteHistoryRewardKaryawan('${historyRewardKaryawanInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
                %{--</g:else>--}%
			</fieldset>
		</g:form>
	</div>
</body>
</html>
