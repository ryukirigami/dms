<%@ page import="com.kombos.hrd.Karyawan" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="karyawan_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">

</table>

<g:javascript>
var karyawanTable;
var reloadKaryawanTable;
$(function(){

    $("#btnPilihKaryawan").click(function() {
        $("#karyawan_datatables tbody").find("tr").each(function() {
            var radio = $(this).find("td:eq(0) input[type='radio']");
            if (radio.is(":checked")) {
                var id = radio.data("id");
                var nama = radio.data("nama");
                $("#karyawan_id").val(id);
                $("#karyawan_nama").val(nama);
                oFormService.fnHideKaryawanDataTable();
            }
        })
    });


	reloadKaryawanTable = function() {
		karyawanTable.fnDraw();
		}

    $("#btnSearchKaryawan").click(function() {
	    reloadKaryawanTable();
    });

	$('#search_tanggalLahir').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tanggalLahir_day').val(newDate.getDate());
			$('#search_tanggalLahir_month').val(newDate.getMonth()+1);
			$('#search_tanggalLahir_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			karyawanTable.fnDraw();
	});



	$('#search_tanggalMasuk').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tanggalMasuk_day').val(newDate.getDate());
			$('#search_tanggalMasuk_month').val(newDate.getMonth()+1);
			$('#search_tanggalMasuk_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			karyawanTable.fnDraw();
	});

	karyawanTable = $('#karyawan_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : 4, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList", controller: "karyawan")}",
		"aoColumns": [

{
    "sTitle": "NPK",
	"sName": "nomorPokokKaryawan",
	"mDataProp": "nomorPokokKaryawan",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return "<input type='radio' data-id='"+row["id"]+"' data-nama='"+row["nama"]+"' data-npk='"+row["nomorPokokKaryawan"]+"' name='radio' class='pull-left row-select' style='position: relative; top: 3px;' aria-label='Row " + row["id"] + "' title='Select this'><input type='hidden' value='"+row["id"]+"'>&nbsp;&nbsp;"+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sTitle": "Nama Karyawan",
	"sName": "nama",
	"mDataProp": "nama",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
    "sTitle": "Tanggal Lahir",
	"sName": "tanggalLahir",
	"mDataProp": "tanggalLahir",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"150px",
	"bVisible": true
}

,

{
    "sTitle": "Alamat",
	"sName": "alamat",
	"mDataProp": "alamat",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"400px",
	"bVisible": true
}

,

{
    "sTitle": "Pekerjaan / Departemen",
	"sName": "jabatan",
	"mDataProp": "jabatan",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
    "sTitle": "Status Karyawan",
	"sName": "statusKaryawan",
	"mDataProp": "statusKaryawan",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},

{
    "sTitle": "Cabang",
	"sName": "cabang",
	"mDataProp": "cabang",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                        var namaKaryawan = $("#sCriteria_nama").val();

                        if (namaKaryawan) {
                            aoData.push({"name": "sCriteria_nama", "value": namaKaryawan});
                        }
                        var cd = $("#sCriteria_cabang").val();

                        if (cd) {
                            aoData.push({"name": "sCriteria_cabang", "value": cd});
                        }

$.ajax({ "dataType": 'json',
    "type": "POST",
    "url": sSource,
    "data": aoData ,
    "success": function (json) {
        fnCallback(json);
       },
    "complete": function () {
       }
});
}
});

});
</g:javascript>



