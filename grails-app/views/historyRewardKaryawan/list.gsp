
<%@ page import="com.kombos.hrd.HistoryRewardKaryawan" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'historyRewardKaryawan.label', default: 'HistoryRewardKaryawan')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
        <style>
        .modal.fade.in {
            left: 25.5% !important;
            width: 1205px !important;
        }
        .modal.fade {
            top: -100%;
        }
        </style>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			var oFormService;
			$(function(){ 

			    oFormService = {

			         fnShowKaryawanDataTable: function() {
                        $("#karyawanModal").modal("show");
                        $("#karyawanModal").fadeIn();
			        },
			        fnHideKaryawanDataTable: function() {
			            $("#karyawanModal").modal("hide");
			        }
			    }

				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('historyRewardKaryawan');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('historyRewardKaryawan', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('historyRewardKaryawan', '${request.contextPath}/historyRewardKaryawan/massdelete', reloadHistoryRewardKaryawanTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('historyRewardKaryawan','${request.contextPath}/historyRewardKaryawan/show/'+id);
				};
				
				edit = function(id) {
					editInstance('historyRewardKaryawan','${request.contextPath}/historyRewardKaryawan/edit/'+id);
				};

});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="historyRewardKaryawan-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>

			<g:render template="dataTables" />
		</div>
		<div class="span7" id="historyRewardKaryawan-form" style="display: none;"></div>
	</div>
</body>
</html>
