<%@ page import="com.kombos.hrd.HistoryRewardKaryawan" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'historyRewardKaryawan.label', default: 'HistoryRewardKaryawan')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="create-historyRewardKaryawan" class="content scaffold-create" role="main">
			<legend><g:message code="default.create.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${historyRewardKaryawanInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${historyRewardKaryawanInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form action="save" >--}%
            %{--<g:if test="${params.onDataKaryawan}">
                <g:formRemote class="form-horizontal" name="create" on404="alert('not found!');"  update="historyRewardKaryawan-form"
                              url="[controller: 'historyRewardKaryawan', action:'save']">
                    <fieldset class="form">
                        <g:hiddenField name="onDataKaryawan" value="true"/>
                        <g:hiddenField name="karyawanId" value="${params.karyawanId}"/>
                        <g:render template="form"/>
                    </fieldset>
                    <fieldset class="buttons controls">
                        <a class="btn cancel" href="javascript:void(0);"
                           onclick="$(this).parent().parent().parent().parent().parent().parent().modal('hide');"><g:message
                                code="default.button.cancel.label" default="Cancel" /></a>
                        <g:submitButton class="btn btn-primary create" name="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                    </fieldset>
                </g:formRemote>
            </g:if>
            <g:else>--}%
                <g:formRemote class="form-horizontal" name="create" on404="alert('not found!');" onSuccess="reloadHistoryRewardKaryawanTable();" update="historyRewardKaryawan-form"
                              url="[controller: 'historyRewardKaryawan', action:'save']">
                    <fieldset class="form">
                        <g:hiddenField name="karyawanId" value="${params.karyawanId}"/>
                        <g:render template="form"/>
                    </fieldset>
                    <fieldset class="buttons controls">
                        <a class="btn cancel" href="javascript:void(0);"
                           onclick="expandTableLayout('historyRewardKaryawan');"><g:message
                                code="default.button.cancel.label" default="Cancel" /></a>
                        <g:submitButton class="btn btn-primary create" name="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                    </fieldset>
                </g:formRemote>
            %{--</g:else>--}%
			%{--</g:form>--}%
		</div>
	</body>
</html>
