<%@ page import="com.kombos.hrd.HistoryRewardKaryawan" %>

<g:javascript>
$(function() {
    $("#btnCariKaryawan").click(function() {
        oFormService.fnShowKaryawanDataTable();
    })
})
</g:javascript>

<g:if test="${params.onDataKaryawan}">

<div class="control-group fieldcontain ${hasErrors(bean: historyRewardKaryawanInstance, field: 'rewardKaryawan', 'error')} ">
    <label class="control-label" for="rewardKaryawan">
        <g:message code="historyRewardKaryawan.rewardKaryawan.label" default="Reward Karyawan" />

    </label>
    <div class="controls">
        <g:select id="rewardKaryawan" name="rewardKaryawan.id" from="${com.kombos.hrd.RewardKaryawan.list()}" optionKey="id" required="" value="${historyRewardKaryawanInstance?.rewardKaryawan?.id}" class="many-to-one"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: historyRewardKaryawanInstance, field: 'tanggalReward', 'error')} ">
    <label class="control-label" for="tanggalReward">
        <g:message code="historyRewardKaryawan.tanggalReward.label" default="Tanggal Reward" />

    </label>
    <div class="controls">
        <ba:datePicker name="tanggalReward" precision="day" value="${historyRewardKaryawanInstance?.tanggalReward}" format="yyyy-MM-dd"/>
    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: historyRewardKaryawanInstance, field: 'keterangan', 'error')} ">
    <label class="control-label" for="keterangan">
        <g:message code="historyRewardKaryawan.keterangan.label" default="Keterangan" />

    </label>
    <div class="controls">
        <g:textField name="keterangan" value="${historyRewardKaryawanInstance?.keterangan}" />
    </div>
</div>
</g:if>

<g:if test="${!params.onDataKaryawan}">

    <div class="control-group fieldcontain ${hasErrors(bean: historyRewardKaryawanInstance, field: 'rewardKaryawan', 'error')} ">
        <label class="control-label" for="rewardKaryawan">
            <g:message code="historyRewardKaryawan.rewardKaryawan.label" default="Reward Karyawan" />

        </label>
        <div class="controls">
            <g:select id="rewardKaryawan" name="rewardKaryawan.id" from="${com.kombos.hrd.RewardKaryawan.list()}" optionKey="id" required="" value="${historyRewardKaryawanInstance?.rewardKaryawan?.id}" class="many-to-one"/>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: historyRewardKaryawanInstance, field: 'tanggalReward', 'error')} ">
        <label class="control-label" for="tanggalReward">
            <g:message code="historyRewardKaryawan.tanggalReward.label" default="Tanggal Reward" />

        </label>
        <div class="controls">
            <ba:datePicker name="tanggalReward" precision="day" value="${historyRewardKaryawanInstance?.tanggalReward}" format="yyyy-MM-dd"/>
        </div>
    </div>


    <div class="control-group fieldcontain ${hasErrors(bean: historyRewardKaryawanInstance, field: 'karyawan', 'error')} ">
        <label class="control-label" for="karyawan">
            <g:message code="historyRewardKaryawan.karyawan.label" default="Karyawan" />

        </label>
        <div class="controls">
        %{--<g:select id="karyawan" name="karyawan.id" from="${com.kombos.hrd.Karyawan.list()}" optionKey="id" required="" value="${historyRewardKaryawanInstance?.karyawan?.id}" class="many-to-one"/>--}%
            <div class="input-append">
            <g:hiddenField name="karyawan.id" readonly="readonly" id="karyawan_id" value="${historyRewardKaryawanInstance?.karyawan?.id}"/>
            <input type="text" value="${historyRewardKaryawanInstance?.karyawan?.nama}" readonly id="karyawan_nama"/>
                <span class="add-on">
                    <a href="javascript:void(0);" id="btnCariKaryawan">
                        <i class="icon-search"/>
                    </a>
                </span>
            </div>
        </div>
    </div>

    <div class="control-group fieldcontain ${hasErrors(bean: historyRewardKaryawanInstance, field: 'keterangan', 'error')} ">
        <label class="control-label" for="keterangan">
            <g:message code="historyRewardKaryawan.keterangan.label" default="Keterangan" />

        </label>
        <div class="controls">
            <g:textField name="keterangan" value="${historyRewardKaryawanInstance?.keterangan}" />
        </div>
    </div>

    <!-- Karyawan Modal -->

    <div id="karyawanModal" class="modal fade">
        <div class="modal-dialog" style="width: 1200px;">
            <div class="modal-content" style="width: 1200px;">
                <div class="modal-header">
                    <a class="close" href="javascript:void(0);" data-dismiss="modal">×</a>
                    <h4>Data Karyawan - List</h4>
                </div>
                <!-- dialog body -->
                <div class="modal-body" id="karyawanModal-body" style="max-height: 1200px;">
                    <div class="box">
                        <div class="span12">
                            <fieldset>
                                <table>
                                    <tr style="display:table-row;">
                                        <td style="width: 130px; display:table-cell; padding:5px;">
                                            <label class="control-label" for="sCriteria_nama">Nama Karyawan</label>
                                        </td>
                                        <td style="width: 130px; display:table-cell; padding:5px;">
                                            <input type="text" id="sCriteria_nama">
                                        </td>
                                        <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                            <td style="width: 130px; display:table-cell; padding:5px;">
                                                <g:select name="sCriteria_cabang" id="sCriteria_cabang" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" noSelection="['':'SEMUA CABANG']"/>
                                            </td>
                                        </g:if>
                                        <td >
                                            <a id="btnSearchKaryawan" class="btn btn-primary" href="javascript:void(0);">
                                                Cari
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </div>
                    </div>
                    <g:render template="dataTablesKaryawan" />
                </div>

                <div class="modal-footer">
                    <a href="javascript:void(0);" class="btn cancel" data-dismiss="modal">
                        Tutup
                    </a>
                    <a href="javascript:void(0);" id="btnPilihKaryawan" class="btn btn-primary">
                        Pilih Karyawan
                    </a>
                </div>
            </div>
        </div>
    </div>

</g:if>

