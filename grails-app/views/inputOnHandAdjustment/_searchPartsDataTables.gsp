<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="searchParts_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover fixed table-selectable" style="table-layout: fixed; ">
    <col width="425px" />
    <col width="425px" />
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:424px;">
            <div>&nbsp;<input type="checkbox" class="select-all">&nbsp;&nbsp;Kode Parts</div>
        </th>
        <th style="border-bottom: none;padding: 0px 0px 5px 0px; width:424px;">
            <div>&nbsp;&nbsp;Nama Parts</div>
        </th>
    </tr>

    <tr>

        <th style="border-top: none;padding: 0px 0px 5px 0px; width:424px;">
            <div id="filter_kode" style="width: 120px;">&nbsp;&nbsp;
                <input type="text" name="search_kode" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 0px 0px 5px 0px; width:424px;">
            <div id="filter_nama" style="width: 120px;">&nbsp;&nbsp;
                <input type="text" name="search_nama" class="search_init" />
            </div>
        </th>

    </tr>

    </thead>
</table>

<g:javascript>
var searchPartsTable;
var reloadsearchPartsTable;
$(function(){

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	searchPartsTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

    var recordssearchPartsperpage = [];
    var anSearchPartsSelected;
    var jmlRecSearchPartsPerPage=0;
    var id;


	searchPartsTable = $('#searchParts_datatables').dataTable({
		"sScrollX": "850px",
		"bScrollCollapse": true,
		"bAutoWidth" : true,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
				"fnDrawCallback": function () {
            var rsSearchParts = $("#searchParts_datatables tbody .row-select");
            var jmlSearchPartsCek = 0;
            var nRow;
            var idRec;
            rsSearchParts.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordssearchPartsperpage[idRec]=="1"){
                    jmlSearchPartsCek = jmlSearchPartsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordssearchPartsperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecSearchPartsPerPage = rsSearchParts.length;
            if(jmlSearchPartsCek==jmlRecSearchPartsPerPage && jmlRecSearchPartsPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "searchPartsDatatablesList")}",
		"aoColumns": [

{
	"sName": "kode",
	"mDataProp": "kode",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSortable": false,
	"bSearchable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "nama",
	"mDataProp": "nama",
	"aTargets": [1],
	"bSortable": false,
	"bSearchable": true,
	"sWidth":"100px",
	"bVisible": true
}],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var kode = $('#filter_kode input').val();

						if(kode){
							aoData.push(
									{"name": 'sCriteria_kode', "value": kode}
							);
						}

						var nama = $('#filter_nama input').val();
						if(nama){
							aoData.push(
									{"name": 'sCriteria_nama', "value": nama}
							);
						}

                        var exist =[];
						var rows = $("#parts_datatables").dataTable().fnGetNodes();
                        for(var i=0;i<rows.length;i++)
                        {
							var id = $(rows[i]).find("#idPopUp").val();
							exist.push(id);
						}
						if(exist.length > 0){
							aoData.push(
										{"name": 'sCriteria_exist', "value": JSON.stringify(exist)}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
	
	    $('.select-all').click(function(e) {
        $("#searchParts_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordssearchPartsperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordssearchPartsperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

    $('#searchParts_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordssearchPartsperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anSearchPartsSelected = searchPartsTable.$('tr.row_selected');
            if(jmlRecSearchPartsPerPage == anSearchPartsSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordssearchPartsperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>



