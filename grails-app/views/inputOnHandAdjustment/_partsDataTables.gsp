<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="parts_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="100%">
    <thead>
    <tr>
        <th style="border-bottom: none;padding: 5px;">
            <div>Kode Parts</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Nama Parts</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Qty Awal</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Satuan</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Qty Real</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Alasan</div>
        </th>
    </tr>
    </thead>
</table>

<g:javascript>
var partsTable;
var reloadpartsTable;
$(function(){

    reloadpartsTable = function() {
		partsTable.fnDraw();
	}
	
    var recordspartsperpage = [];
    var anPartsSelected;
    var jmlRecPartsPerPage=0;
    var id;



	partsTable = $('#parts_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsParts = $("#parts_datatables tbody .row-select");
            var jmlPartsCek = 0;
            var nRow;
            var idRec;
            rsParts.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordspartsperpage[idRec]=="1"){
                    jmlPartsCek = jmlPartsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordspartsperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecPartsPerPage = rsParts.length;
            if(jmlPartsCek==jmlRecPartsPerPage && jmlRecPartsPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
//		"bProcessing": true,
//		"bServerSide": true,
//		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"aoColumns": [
{
	"sName": "kode",
	"mDataProp": "kode",
	"mRender": function ( data, type, row ) {
	    if(row['ada']=='yes'){
            return '<input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	    }else {
	        return '<input id="checkBoxJob" type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" id="idPopUp" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	    }
	},
	"aTargets": [0],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "nama",
	"mDataProp": "nama",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "awal",
	"mDataProp": "awal",
	"aTargets": [2],
	"mRender": function ( data, type, row ) {
	    return '<input id="awal_'+row['id']+'" readonly="readonly"  class="pull-left row-awal" size=5 value="'+data+'" type="text">';
	},
	"bSortable": false,
	"sWidth":"70px",
	"bVisible": true
},
{
	"sName": "satuan",
	"mDataProp": "satuan",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"50px",
	"bVisible": true
},
{
	"sName": "real",
	"mDataProp": "real",
	"aTargets": [4],
	"mRender": function ( data, type, row ) {
	    return '<input id="real_'+row['id']+'" size=5 value="'+data+'" type="text">';
	},
	"bSortable": false,
	"sWidth":"70px",
	"bVisible": true
},
{
	"sName": "alasan",
	"mDataProp": "alasan",
	"aTargets": [5],
	"mRender": function ( data, type, row ) {
	    var select = '<select id="alasan_'+row['id']+'">';
        <g:each in="${com.kombos.parts.AlasanAdjusment.listOrderByM165AlasanAdjusment()}">
            if(data == ${it.id}){
                select += '<option selected value="${it.id}">${it.m165AlasanAdjusment}</option>';
            } else {
                select += '<option value="${it.id}">${it.m165AlasanAdjusment}</option>';
            }
        </g:each>
        select += '</select>'
        return select;
	},
	"bSortable": false,
	"sWidth":"80px",
	"bVisible": true
}],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
	    $('.select-all').click(function(e) {
        $("#parts_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordspartsperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordspartsperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

    $('#parts_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordspartsperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anPartsSelected = partsTable.$('tr.row_selected');
            if(jmlRecPartsPerPage == anPartsSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordspartsperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
<g:if test="${partsAdjustInstance?.t145ID}">
    var idUbah = '${partsAdjustInstance?.t145ID}'
        $.ajax({
    		url:'${request.contextPath}/inputOnHandAdjustment/getTableData',
    		type: "POST",
    		data: { id: idUbah },
    		success : function(data){
    		    $.each(data,function(i,item){
                    partsTable.fnAddData({
                        'id': item.id,
                        'kode': item.kode,
                        'nama': item.nama,
                        'satuan': item.satuan,
                        'awal': item.awal,
                        'real': item.real,
                        'alasan': item.alasan
                    });
                });
            }
		});

</g:if>
</g:javascript>



