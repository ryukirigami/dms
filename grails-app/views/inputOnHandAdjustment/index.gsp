<%--
  Created by IntelliJ IDEA.
  User: Mohammad Fauzi
  Date: 2/5/14
  Time: 12:25 AM
--%>

<%@ page import="com.kombos.administrasi.KegiatanApproval" contentType="text/html;charset=UTF-8" %>
<html>
<head>
<head>
<meta name="layout" content="main">
<title>Input On Hand Adjustment</title>
<r:require modules="baseapplayout"/>


<g:javascript disposition="head">

        $(function () {
            onDoSendToApproval = function() {
                var checkPart =[];
                    $("#parts_datatables tbody .row-select").each(function() {
                        if(this.checked){
                            var id = $(this).next("input:hidden").val();
                            checkPart.push(id);
                        }
                    });
                    if(checkPart.length == 0){
                        alert('Anda belum memilih data yang akan ditambahkan');
                        return;
                    }

                    $("#permohonanApprovalModal").modal({
                            "backdrop": "static",
                            "keyboard": true,
                            "show": true
                        }).css({'width': '1200px', 'margin-left': function () {
                                    return -($(this).width() / 2);
                                }});
            }

            onSearchAndAddParts = function() {
                $("#searchAndAddPartsContent").empty();
                $.ajax({type: 'POST', url: '${request.contextPath}/inputOnHandAdjustment/searchAndAddParts',
                    success: function (data, textStatus) {
                        $("#searchAndAddPartsContent").html(data);
                        $("#searchAndAddPartsModal").modal({
                            "backdrop": "static",
                            "keyboard": true,
                            "show": true
                        }).css({'width': '950px', 'margin-left': function () {
                                    return -($(this).width() / 2);
                                }});

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                    },
                    complete: function (XMLHttpRequest, textStatus) {
                        $('#spinner').fadeOut();
                    }
                });
            }


            tambahParts = function(){
                   var checkPart =[];
                    $("#searchParts_datatables tbody .row-select").each(function() {
                        if(this.checked){
                            var id = $(this).next("input:hidden").val();
                            var nRow = $(this).parents('tr')[0];
                            checkPart.push(nRow);
                        }
                    });
                    if(checkPart.length == 0){
                        alert('Anda belum memilih data yang akan ditambahkan');
                        return;
                    }
                    for (var i=0;i < checkPart.length;i++){
                        var aData = searchPartsTable.fnGetData(checkPart[i]);
                        partsTable.fnAddData({
                            'id': aData['id'],
                            'kode': aData['kode'],
                            'nama': aData['nama'],
                            'awal': aData['awalData'],
                            'satuan': aData['satuan'],
                            'real': '0',
                            'alasan': '',
                            'ada': 'no'
                            });

                        $('#real_-'+aData['id']).autoNumeric('init',{
                            vMin:'0',
                            vMax:'999999999999999999',
                            mDec: null,
                            aSep:''
                        });
                    }
                checkPart = [];
                onSearchAndAddParts();

             }

            onDeleteParts = function(){
                var checkID =[];
                var a=parseInt(-1);
                $('#parts_datatables tbody .row-select').each(function() {
                    a=parseInt(a)+1;
                    if(this.checked){
                        checkID.push(a);
                    }
                });

                if(checkID.length<1){
                    alert('Anda belum memilih data yang akan dihapus');
                }else{
                    console.log(checkID)
                    for(var b = (checkID.length-1) ; b >= 0 ; b--){
                        partsTable.fnDeleteRow(parseInt(checkID[b]));
                    }
                }
                checkID = [];
            }



        onSaveData = function(){

   try{
       var checkJob =[];
       var paramAwal = "";
       var paramReal = "";
       var paramAlasan = "";
           $("#parts_datatables tbody .row-select").each(function() {
               if(this.checked){
                   var id = $(this).next("input:hidden").val();
                   checkJob.push(id);
                   paramAwal += "&awal_"+id+"="+document.getElementById("awal_"+id).value
                   paramReal += "&real_"+id+"="+document.getElementById("real_"+id).value
                   paramAlasan += "&alasan_"+id+"="+document.getElementById("alasan_"+id).value
               }
           });
           if(checkJob.length == 0){
               alert('Anda belum memilih data yang akan disimpan');
               return;
           }
//                        alert(checkJob+" "+paramAwal+" "+paramReal+" "+paramAlasan)
           var checkBoxVendor = JSON.stringify(checkJob);
           $.ajax({
               url:'${request.contextPath}/inputOnHandAdjustment/doSave?1=1'+paramAwal+paramReal+paramAlasan,
                            type: "POST", // Always use POST when deleting data
                            data : { ids: checkBoxVendor },
                            success : function(data){
                                reloadpartsTable()
                                toastr.success("Data Berhasil Disimpan");
                                window.location.replace('#/partsAdjust');
                            },
                            error: function(xhr, textStatus, errorThrown) {
                                alert('Internal Server Error');
                            }
                        });

                    checkJob = [];
                }catch (e){
                    alert(e)
                }


             }



             onSendData = function(){

                try{
                    var checkJob =[];
                    var paramAwal = "";
                    var paramReal = "";
                    var paramAlasan = "";
                    var Pesan = document.getElementById("Pesan").value;
                    var dokumen = document.getElementById("dokumen").value;
                        $("#parts_datatables tbody .row-select").each(function() {
                            if(this.checked){
                                var id = $(this).next("input:hidden").val();
                                checkJob.push(id);
                                paramAwal += "&awal_"+id+"="+document.getElementById("awal_"+id).value
                                paramReal += "&real_"+id+"="+document.getElementById("real_"+id).value
                                paramAlasan += "&alasan_"+id+"="+document.getElementById("alasan_"+id).value
                            }
                        });
                        if(checkJob.length == 0){
                            alert('Anda belum memilih data yang akan disimpan');
                            return;
                        }
                        var checkBoxVendor = JSON.stringify(checkJob);
                        $.ajax({
                            url:'${request.contextPath}/inputOnHandAdjustment/doSend?1=1&paramAwal='+paramAwal+paramReal+paramAlasan,
                            type: "POST", // Always use POST when deleting data
                            data : { ids: checkBoxVendor,Pesan:Pesan, dokumen:dokumen},
                            success : function(data){
                                reloadpartsTable()
                                toastr.success("Send to approval success");
                                window.location.replace('#/partsAdjust');
                            },
                            error: function(xhr, textStatus, errorThrown) {
                                alert('Internal Server Error');
                            }
                        });

                    checkJob = [];
                }catch (e){
                    alert(e)
                }


             }


        });
</g:javascript>

</head>

<body>

    <div class="box">
    <div class="span12" id="input-table">
        <legend style="font-size: small">Input On Hand Adjument</legend>
        <form id="form-input">
            <g:if test="${partsAdjustInstance?.t145ID}">
                Nomor Adjustment : <input type="text" name="t145ID" id="t145ID" readonly value="${partsAdjustInstance?.t145ID}">
            </g:if>
        </form>
    </div>
        <fieldset class="buttons controls">
                 <button class="btn btn-primary" onclick="onSearchAndAddParts();">Add Parts...</button>
        </fieldset> <br>
         <g:render template="partsDataTables"/>



    <fieldset class="buttons controls">
            <button class="btn btn-primary" onclick="onDoSendToApproval()">Send To Approval</button>


        <button class="btn btn-primary" onclick="window.location.replace('#/partsAdjust');">Cancel</button>

    </fieldset>
    </div>
</div>


<div id="searchAndAddPartsModal" class="modal hide">
    <div class="modal-dialog" style="width: 950px;">
        <div class="modal-content" style="width: 950px;">
            <!-- dialog body -->
            <div class="modal-body" style="max-height: 1200px;">
                <div id="searchAndAddPartsContent"/>

                <div class="iu-content"></div>
            </div>
            <!-- dialog buttons -->
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" onclick="tambahParts()">Add Selected</button>
                <button id='closesearchAndAddParts' type="button" data-dismiss="modal"
                        class="btn btn-primary">Close</button>
            </div>
        </div>
    </div>
</div>
<div id="permohonanApprovalModal" class="modal hide">
    <div class="modal-dialog" style="width: 1200px;">
        <div class="modal-content" style="width: 1200px;">
            <!-- dialog body -->
            <div class="modal-body">
                <div class="iu-content">

                    Nama Kegiatan  :

                    <g:select name="kegiatanApproval" id="kegiatanApproval" from="${KegiatanApproval.list()}" optionKey="id"  value="${kegiatanApproval.id}" optionValue="m770KegiatanApproval" class="one-to-many" readonly="readonly"/>

                    <br>

                    Nomor Dokumen :
                    <g:textField name="dokumen" id="dokumen" value="${noDokumen}" />
                    <br>
                    Tanggal Dan Jam :

                    ${ new Date().format("dd MM yyyy")}

                    <br>
                    Approve(s)
                    <br>
                    <br>
                    ${approver}
                    <br>
                    <br>
                    Pesan :
                    <br>
                    <g:textArea name="Pesan" id="Pesan" style="width: 99%" />

                </div>
            </div>

            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" onclick="onSendData()">Send</button>
                <button id='closepermohonanApproval' type="button" data-dismiss="modal"
                        class="btn btn-primary">Close</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>