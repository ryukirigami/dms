<%@ page import="com.kombos.administrasi.ModelName; com.kombos.administrasi.BaseModel; com.kombos.administrasi.BodyType" %>

<g:javascript>

var selectModelName;
			$(function(){
			selectModelName = function () {
			var idBaseModel = $('#baseModel option:selected').val();
			    if(idBaseModel != undefined){
                    jQuery.getJSON('${request.contextPath}/bodyType/getModelNames?idBaseModel='+idBaseModel, function (data) {
                            $('#modelName').empty();
                            if (data) {
                                jQuery.each(data, function (index, value) {
                                    $('#modelName').append("<option value='" + value.id + "'>" + value.kodeModelName + "</option>");
                                });
                            }
                        });
                }else{
                     $('#modelName').empty();
                }
        };
    });
</g:javascript>
 

<div class="control-group fieldcontain ${hasErrors(bean: bodyTypeInstance, field: 'baseModel', 'error')} required">
	<label class="control-label" for="baseModel">
		<g:message code="bodyType.baseModel.label" default="Base Model" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="baseModel" onchange="selectModelName();" name="baseModel.id" noSelection="['':'Pilih Base Model']" from="${BaseModel.createCriteria().list(){eq("staDel", "0");order("m102KodeBaseModel", "asc")}}" optionKey="id" optionValue="${{it.m102KodeBaseModel + " - " + it.m102NamaBaseModel}}" required="" value="${bodyTypeInstance?.baseModel?.id}" class="many-to-one"/>
	</div>
</div>
<g:javascript>
    document.getElementById("baseModel").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: bodyTypeInstance, field: 'modelName', 'error')} required">
	<label class="control-label" for="modelName">
		<g:message code="bodyType.modelName.label" default="Model Name" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="modelName" name="modelName.id" noSelection="['':'Pilih Model Name']" from="${ModelName.createCriteria().list(){eq("staDel", "0");order("m104KodeModelName", "asc")}}" optionKey="id" optionValue="${{it.m104KodeModelName + " - " + it.m104NamaModelName}}" required="" value="${bodyTypeInstance?.modelName?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: bodyTypeInstance, field: 'm105KodeBodyType', 'error')} required">
	<label class="control-label" for="m105KodeBodyType">
		<g:message code="bodyType.m105KodeBodyType.label" default="Kode Body Type" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m105KodeBodyType" maxlength="2" required="" value="${bodyTypeInstance?.m105KodeBodyType}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: bodyTypeInstance, field: 'm105NamaBodyType', 'error')} required">
	<label class="control-label" for="m105NamaBodyType">
		<g:message code="bodyType.m105NamaBodyType.label" default="Nama Body Type" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m105NamaBodyType" maxlength="20" required="" value="${bodyTypeInstance?.m105NamaBodyType}"  />
	</div>
</div>


