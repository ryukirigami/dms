
<%@ page import="com.kombos.administrasi.BodyType" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="bodyType_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="bodyType.baseModel.label" default="Base Model" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="bodyType.modelName.label" default="Model Name" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="bodyType.m105KodeBodyType.label" default="Kode Body Type" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="bodyType.m105NamaBodyType.label" default="Nama Body Type" /></div>
			</th>


		
		</tr>
		<tr>
		

			<th style="border-top: none;padding: 5px;">
				<div id="filter_baseModel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_baseModel" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_modelName" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_modelName" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m105KodeBodyType" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m105KodeBodyType" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m105NamaBodyType" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m105NamaBodyType" class="search_init" />
				</div>
			</th>
	
		</tr>
	</thead>
</table>

<g:javascript>
var BodyTypeTable;
var reloadBodyTypeTable;
$(function(){
	
	reloadBodyTypeTable = function() {
		BodyTypeTable.fnDraw();
	}

	var recordsBodyTypeperpage = [];//new Array();
    var anBodyTypeSelected;
    var jmlRecBodyTypePerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	BodyTypeTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	BodyTypeTable = $('#bodyType_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            //alert( $('li.active').text() );
            var rsBodyType = $("#bodyType_datatables tbody .row-select");
            var jmlBodyTypeCek = 0;
            var nRow;
            var idRec;
            rsBodyType.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsBodyTypeperpage[idRec]=="1"){
                    jmlBodyTypeCek = jmlBodyTypeCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsBodyTypeperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecBodyTypePerPage = rsBodyType.length;
            if(jmlBodyTypeCek==jmlRecBodyTypePerPage && jmlRecBodyTypePerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

 
{
	"sName": "baseModel",
	"mDataProp": "baseModel",
	"aTargets": [1],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "modelName",
	"mDataProp": "modelName",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m105KodeBodyType",
	"mDataProp": "m105KodeBodyType",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m105NamaBodyType",
	"mDataProp": "m105NamaBodyType",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

 

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m105ID = $('#filter_m105ID input').val();
						if(m105ID){
							aoData.push(
									{"name": 'sCriteria_m105ID', "value": m105ID}
							);
						}
	
						var baseModel = $('#filter_baseModel input').val();
						if(baseModel){
							aoData.push(
									{"name": 'sCriteria_baseModel', "value": baseModel}
							);
						}
	
						var modelName = $('#filter_modelName input').val();
						if(modelName){
							aoData.push(
									{"name": 'sCriteria_modelName', "value": modelName}
							);
						}
	
						var m105KodeBodyType = $('#filter_m105KodeBodyType input').val();
						if(m105KodeBodyType){
							aoData.push(
									{"name": 'sCriteria_m105KodeBodyType', "value": m105KodeBodyType}
							);
						}
	
						var m105NamaBodyType = $('#filter_m105NamaBodyType input').val();
						if(m105NamaBodyType){
							aoData.push(
									{"name": 'sCriteria_m105NamaBodyType', "value": m105NamaBodyType}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#bodyType_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordsBodyTypeperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsBodyTypeperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#bodyType_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsBodyTypeperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anBodyTypeSelected = BodyTypeTable.$('tr.row_selected');
            if(jmlRecBodyTypePerPage == anBodyTypeSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsBodyTypeperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
