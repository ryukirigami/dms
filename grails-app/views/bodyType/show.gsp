

<%@ page import="com.kombos.administrasi.BodyType" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'bodyType.label', default: 'Body Type')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteBodyType;

$(function(){ 
	deleteBodyType=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/bodyType/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadBodyTypeTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-bodyType" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="bodyType"
			class="table table-bordered table-hover">
			<tbody>

				
				
				<g:if test="${bodyTypeInstance?.baseModel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="baseModel-label" class="property-label"><g:message
					code="bodyType.baseModel.label" default="Base Model" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="baseModel-label">
						%{--<ba:editableValue
								bean="${bodyTypeInstance}" field="baseModel"
								url="${request.contextPath}/BodyType/updatefield" type="text"
								title="Enter baseModel" onsuccess="reloadBodyTypeTable();" />--}%
							
								%{--<g:link controller="baseModel" action="show" id="${bodyTypeInstance?.baseModel?.id}">${bodyTypeInstance?.baseModel?.encodeAsHTML()}</g:link>--}%
                        <g:fieldValue bean="${bodyTypeInstance?.baseModel}" field="m102NamaBaseModel"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bodyTypeInstance?.modelName}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="modelName-label" class="property-label"><g:message
					code="bodyType.modelName.label" default="Model Name" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="modelName-label">
						%{--<ba:editableValue
								bean="${bodyTypeInstance}" field="modelName"
								url="${request.contextPath}/BodyType/updatefield" type="text"
								title="Enter modelName" onsuccess="reloadBodyTypeTable();" />--}%
							
								%{--<g:link controller="modelName" action="show" id="${bodyTypeInstance?.modelName?.id}">${bodyTypeInstance?.modelName?.encodeAsHTML()}</g:link>--}%
                        <g:fieldValue bean="${bodyTypeInstance?.modelName}" field="m104NamaModelName"/>
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bodyTypeInstance?.m105KodeBodyType}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m105KodeBodyType-label" class="property-label"><g:message
					code="bodyType.m105KodeBodyType.label" default="Kode Body Type" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m105KodeBodyType-label">
						%{--<ba:editableValue
								bean="${bodyTypeInstance}" field="m105KodeBodyType"
								url="${request.contextPath}/BodyType/updatefield" type="text"
								title="Enter m105KodeBodyType" onsuccess="reloadBodyTypeTable();" />--}%
							
								<g:fieldValue bean="${bodyTypeInstance}" field="m105KodeBodyType"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bodyTypeInstance?.m105NamaBodyType}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m105NamaBodyType-label" class="property-label"><g:message
					code="bodyType.m105NamaBodyType.label" default="Nama Body Type" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m105NamaBodyType-label">
						%{--<ba:editableValue
								bean="${bodyTypeInstance}" field="m105NamaBodyType"
								url="${request.contextPath}/BodyType/updatefield" type="text"
								title="Enter m105NamaBodyType" onsuccess="reloadBodyTypeTable();" />--}%
							
								<g:fieldValue bean="${bodyTypeInstance}" field="m105NamaBodyType"/>
							
						</span></td>
					
				</tr>
				</g:if>



            <g:if test="${bodyTypeInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="jenisStall.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${bodyTypeInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadJenisStallTable();" />--}%

                        <g:fieldValue bean="${bodyTypeInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
			
					<g:if test="${bodyTypeInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="jenisStall.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${bodyTypeInstance}" field="dateCreated"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:formatDate date="${bodyTypeInstance?.dateCreated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bodyTypeInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="jenisStall.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${bodyTypeInstance}" field="createdBy"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:fieldValue bean="${bodyTypeInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bodyTypeInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="jenisStall.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${bodyTypeInstance}" field="lastUpdated"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:formatDate date="${bodyTypeInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${bodyTypeInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="jenisStall.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${bodyTypeInstance}" field="updatedBy"
								url="${request.contextPath}/JenisStall/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadJenisStallTable();" />--}%
							
								<g:fieldValue bean="${bodyTypeInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${bodyTypeInstance?.id}"
					update="[success:'bodyType-form',failure:'bodyType-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteBodyType('${bodyTypeInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
