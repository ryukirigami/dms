

<%@ page import="com.kombos.administrasi.KelompokNPB" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'kelompokNPB.label', default: 'Kelompok NPB')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteKelompokNPB;

$(function(){ 
	deleteKelompokNPB=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/kelompokNPB/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadKelompokNPBTable();
   				expandTableLayout('kelompokNPB');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-kelompokNPB" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="kelompokNPB"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${kelompokNPBInstance?.kodeKelompok}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kodeKelompok-label" class="property-label"><g:message
					code="kelompokNPB.kodeKelompok.label" default="Kode Kelompok" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kodeKelompok-label">
						%{--<ba:editableValue
								bean="${kelompokNPBInstance}" field="kodeKelompok"
								url="${request.contextPath}/KelompokNPB/updatefield" type="text"
								title="Enter kodeKelompok" onsuccess="reloadKelompokNPBTable();" />--}%
							
								<g:fieldValue bean="${kelompokNPBInstance}" field="kodeKelompok"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${kelompokNPBInstance?.namaKelompok}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="namaKelompok-label" class="property-label"><g:message
                                code="kelompokNPB.namaKelompok.label" default="Nama Kelompok" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="namaKelompok-label">
                        %{--<ba:editableValue
                                bean="${kelompokNPBInstance}" field="namaKelompok"
                                url="${request.contextPath}/KelompokNPB/updatefield" type="text"
                                title="Enter namaKelompok" onsuccess="reloadKelompokNPBTable();" />--}%

                        <g:fieldValue bean="${kelompokNPBInstance}" field="namaKelompok"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${kelompokNPBInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="kelompokNPB.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${kelompokNPBInstance}" field="createdBy"
                                url="${request.contextPath}/KelompokNPB/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadKelompokNPBTable();" />--}%

                        <g:fieldValue bean="${kelompokNPBInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${kelompokNPBInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="kelompokNPB.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${kelompokNPBInstance}" field="updatedBy"
                                url="${request.contextPath}/KelompokNPB/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadKelompokNPBTable();" />--}%

                        <g:fieldValue bean="${kelompokNPBInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${kelompokNPBInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="kelompokNPB.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${kelompokNPBInstance}" field="dateCreated"
                                url="${request.contextPath}/KelompokNPB/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadKelompokNPBTable();" />--}%

                        %{--<g:formatDate date="${kelompokNPBInstance?.dateCreated}" />--}%
                        <g:formatDate date="${kelompokNPBInstance?.dateCreated}"  type="datetime" style="MEDIUM"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${kelompokNPBInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="kelompokNPB.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${kelompokNPBInstance}" field="lastUpdated"
                                url="${request.contextPath}/KelompokNPB/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadKelompokNPBTable();" />--}%

                        %{--<g:formatDate date="${kelompokNPBInstance?.lastUpdated}" />--}%
                        <g:formatDate date="${kelompokNPBInstance?.lastUpdated}"  type="datetime" style="MEDIUM"/>
                    </span></td>

                </tr>
            </g:if>

            <g:if test="${kelompokNPBInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="kelompokNPB.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${kelompokNPBInstance}" field="lastUpdProcess"
								url="${request.contextPath}/KelompokNPB/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadKelompokNPBTable();" />--}%
							
								<g:fieldValue bean="${kelompokNPBInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
			</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('kelompokNPB');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${kelompokNPBInstance?.id}"
					update="[success:'kelompokNPB-form',failure:'kelompokNPB-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteKelompokNPB('${kelompokNPBInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
