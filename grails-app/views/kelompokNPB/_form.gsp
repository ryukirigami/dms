<%@ page import="com.kombos.administrasi.KelompokNPB" %>



<div class="control-group fieldcontain ${hasErrors(bean: kelompokNPBInstance, field: 'kodeKelompok', 'error')} ">
	<label class="control-label" for="kodeKelompok">
		<g:message code="kelompokNPB.kodeKelompok.label" default="Kode Kelompok" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="kodeKelompok" id="kodeKelompok" maxlength="10" required=""  value="${kelompokNPBInstance?.kodeKelompok}" />
	</div>
</div>
<g:javascript>
    document.getElementById('kodeKelompok').focus();
</g:javascript>
%{--<div class="control-group fieldcontain ${hasErrors(bean: kelompokNPBInstance, field: 'lastUpdProcess', 'error')} ">--}%
	%{--<label class="control-label" for="lastUpdProcess">--}%
		%{--<g:message code="kelompokNPB.lastUpdProcess.label" default="Last Upd Process" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="lastUpdProcess" value="${kelompokNPBInstance?.lastUpdProcess}" />--}%
	%{--</div>--}%
%{--</div>--}%

<div class="control-group fieldcontain ${hasErrors(bean: kelompokNPBInstance, field: 'namaKelompok', 'error')} ">
	<label class="control-label" for="namaKelompok">
		<g:message code="kelompokNPB.namaKelompok.label" default="Nama Kelompok" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="namaKelompok" id="namaKelompok" maxlength="20" required=""  value="${kelompokNPBInstance?.namaKelompok}" />
	</div>
</div>

