<%--
  Created by IntelliJ IDEA.
  User: Mohammad Fauzi
  Date: 2/6/14
  Time: 4:50 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title>Search Job untuk Field Action</title>
    <r:require modules="baseapplayout"/>
</head>

<body>

<div class="content scaffold-edit" role="main">
    <fieldset class="form">

        <div class="control-group fieldcontain required">
            <div id="searchJobsDataTables_tabs">
                <div id="searchJobsDataTables_tabs-1">
                    <g:render template="searchJobsDataTables"/>
                </div>
            </div>
        </div>

    </fieldset>
</div>

<script>
    $(function () {
        $("#searchJobsDataTables_tabs").tabs();
    });
</script>

</body>
</html>