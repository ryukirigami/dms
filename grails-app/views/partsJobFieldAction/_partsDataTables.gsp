<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="parts_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>
			
			<th style="border-bottom: none;padding: 5px;">
				<div>Deskripsi Field Action</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Nama Parts</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Jumlah</div>
			</th>
            <th style="border-bottom: none;padding: 5px;">
                <div>Satuan</div>
            </th>
		</tr>
	</thead>
</table>

<g:javascript>
var partsTable;
var reloadpartsTable;
$(function(){

    reloadpartsTable = function() {
		partsTable.fnDraw();
	}

    var recordsPartsPerPage = [];//new Array();
    var anPartsSelected;
    var jmlRecPartsPerPage=0;
    var id;



	partsTable = $('#parts_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsParts = $("#parts_datatables tbody .row-select");
            var jmlPartsCek = 0;
            var nRow;
            var idRec;
            rsParts.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsPartsPerPage[idRec]=="1"){
                    jmlPartsCek = jmlPartsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsPartsPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecPartsPerPage = rsParts.length;
            if(jmlPartsCek==jmlRecPartsPerPage && jmlRecPartsPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "partsDatatablesList")}",
		"aoColumns": [

{
	"sName": "deskripsi",
	"mDataProp": "deskripsi",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input id="checkBoxJob" type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" id="idPopUp" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "nama",
	"mDataProp": "nama",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "jumlah",
	"mDataProp": "jumlah",
	"aTargets": [2],
	"mRender": function ( data, type, row ) {
	        return '<input id="jumlah_'+row['id']+'" onkeypress="return isNumberKey(event);" maxlength="4" class="inline-edit" type="text" style="width:70px;" value="'+data+'">';
    },
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "satuan",
	"mDataProp": "satuan",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        var idFA = $('#fa').val();
        				aoData.push(
                                {"name": 'fa', "value": idFA}
						);
	                    $.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
    $('.select-all').click(function(e) {

        $("#parts_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsPartsPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsPartsPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#parts_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsPartsPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anPartsSelected = partsTable.$('tr.row_selected');
            if(jmlRecPartsPerPage == anPartsSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsPartsPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
