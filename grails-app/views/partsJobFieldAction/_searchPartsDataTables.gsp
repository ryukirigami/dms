<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="searchParts_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div>&nbsp;<input type="checkbox" class="select-all"/>&nbsp;
            Kode Parts</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Nama Parts</div>
        </th>
    </tr>

    <tr>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_kode" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_kode" class="search_init" />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_nama" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_nama" class="search_init" />
            </div>
        </th>

    </tr>

    </thead>
</table>

<g:javascript>
var searchPartsTable;
var reloadsearchPartsTable;
$(function(){

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	searchPartsTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

    var recordsSearchJobsPerPage = [];//new Array();
    var anSearchJobsSelected;
    var jmlRecSearchJobsPerPage=0;
    var id;

	searchPartsTable = $('#searchParts_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : true,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsSearchJobs = $("#searchJobs_datatables tbody .row-select");
            var jmlSearchJobsCek = 0;
            var nRow;
            var idRec;
            rsSearchJobs.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsSearchJobsPerPage[idRec]=="1"){
                    jmlSearchJobsCek = jmlSearchJobsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsSearchJobsPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecSearchJobsPerPage = rsSearchJobs.length;
            if(jmlSearchJobsCek==jmlRecSearchJobsPerPage && jmlRecSearchJobsPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
        "fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "searchPartsDatatablesList")}",
		"aoColumns": [

{
	"sName": "kode",
	"mDataProp": "kode",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input id="checkBoxJob" type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data//<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSortable": false,
	"bSearchable": true,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "nama",
	"mDataProp": "nama",
	"aTargets": [1],
	"bSortable": false,
	"bSearchable": true,
	"sWidth":"100px",
	"bVisible": true
}],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
        				aoData.push(
									{"name": 'fa', "value": ${fa?.id}}
						);

						var kode = $('#filter_kode input').val();

						if(kode){
							aoData.push(
									{"name": 'sCriteria_kode', "value": kode}
							);
						}

						var nama = $('#filter_nama input').val();
						if(nama){
							aoData.push(
									{"name": 'sCriteria_nama', "value": nama}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
    $('.select-all').click(function(e) {

        $("#searchJobs_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsSearchJobsPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsSearchJobsPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#searchJobs_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsSearchJobsPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anSearchJobsSelected = searchSearchJobsTable.$('tr.row_selected');
            if(jmlRecSearchJobsPerPage == anSearchJobsSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsSearchJobsPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>



