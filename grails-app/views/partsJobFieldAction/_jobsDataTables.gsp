<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="jobs_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>
			
			<th style="border-bottom: none;padding: 5px;">
				<div>&nbsp;<input type="checkbox" class="select-all"/>&nbsp;
                Deskripsi Field Action</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Nama Job</div>
			</th>
		</tr>
	</thead>
</table>

<g:javascript>
var jobsTable;
var reloadjobsTable;
$(function(){

    reloadjobsTable = function() {
		jobsTable.fnDraw();
	}

    var recordsJobsPerPage = [];//new Array();
    var anJobsSelected;
    var jmlRecJobsPerPage=0;
    var id;


	jobsTable = $('#jobs_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsJobs = $("#jobs_datatables tbody .row-select");
            var jmlJobsCek = 0;
            var nRow;
            var idRec;
            rsJobs.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsJobsPerPage[idRec]=="1"){
                    jmlJobsCek = jmlJobsCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsJobsPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecJobsPerPage = rsJobs.length;
            if(jmlJobsCek==jmlRecJobsPerPage && jmlRecJobsPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "jobsDatatablesList")}",
		"aoColumns": [

{
	"sName": "deskripsi",
	"mDataProp": "deskripsi",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input id="checkBoxJob" type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},
{
	"sName": "nama",
	"mDataProp": "nama",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
}],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
        				var idFA = $('#fa').val();
        				aoData.push(
                                {"name": 'fa', "value": idFA}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
    $('.select-all').click(function(e) {

        $("#jobs_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsJobsPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsJobsPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#jobs_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsJobsPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anJobsSelected = jobsTable.$('tr.row_selected');
            if(jmlRecJobsPerPage == anJobsSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsJobsPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
});
</g:javascript>


			
