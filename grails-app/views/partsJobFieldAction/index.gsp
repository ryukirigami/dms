<%--
  Created by IntelliJ IDEA.
  User: Mohammad Fauzi
  Date: 2/5/14
  Time: 12:25 AM
--%>

<%@ page import="com.kombos.customerprofile.FA" contentType="text/html;charset=UTF-8" %>
<html>
<head>
<head>
    <meta name="layout" content="main">
    <title>Parts & Job untuk Field Action</title>
    <r:require modules="baseapplayout"/>


    <g:javascript>
        var closeModal;
        var addMore;
        function isNumberKey(evt)
        {
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        $(function () {
            addMore = function(){
                $("#addMoreContent").empty();
                $.ajax({
                    type:'POST',
                    url:'${request.contextPath}/FA',
                    success:function(data,textStatus){
                            $("#addMoreContent").html(data);
                            $("#addMoreModal").modal({
                                "backdrop" : "static",
                                "keyboard" : false,
                                "show" : true
                            }).css({'width': '1200px','margin-left': function () {return -($(this).width() / 2);}});
                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){},
                    complete:function(XMLHttpRequest,textStatus){
                        $('#spinner').fadeOut();
                    }
                });
            }



            onSave = function () {
                    alert('Save success');
                    window.location.replace('#/home');
            }

            onSearchAndAddParts = function () {
                var fa = $("#fa").val();
                if(fa<1){
                    alert('Pilih FA Terlebih Dahulu')
                    return
                }
                $("#searchAndAddPartsContent").empty();
                $.ajax({type: 'POST', url: '${request.contextPath}/partsJobFieldAction/searchAndAddParts?fa=' + fa,
                    success: function (data, textStatus) {
                        $("#searchAndAddPartsContent").html(data);
                        $("#searchAndAddPartsModal").modal({
                            "backdrop": "static",
                            "keyboard": true,
                            "show": true
                        }).css({'width': '1200px', 'margin-left': function () {
                                    return -($(this).width() / 2);
                                }});

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                    },
                    complete: function (XMLHttpRequest, textStatus) {
                        $('#spinner').fadeOut();
                    }
                });
            }

            onSearchAndAddJobs = function () {
                var fa = $("#fa").val();
                if(fa<1){
                    alert('Pilih FA Terlebih Dahulu')
                    return
                }
                $("#searchAndAddJobsContent").empty();
                $.ajax({type: 'POST', url: '${request.contextPath}/partsJobFieldAction/searchAndAddJobs?fa=' + fa,
                    success: function (data, textStatus) {
                        $("#searchAndAddJobsContent").html(data);
                        $("#searchAndAddJobsModal").modal({
                            "backdrop": "static",
                            "keyboard": true,
                            "show": true
                        }).css({'width': '1200px', 'margin-left': function () {
                                    return -($(this).width() / 2);
                                }});

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                    },
                    complete: function (XMLHttpRequest, textStatus) {
                        $('#spinner').fadeOut();
                    }
                });
            }

            tambahParts = function(){
                   var checkPart =[];
                    $("#searchParts_datatables tbody .row-select").each(function() {
                        if(this.checked){
                            var id = $(this).next("input:hidden").val();
                            checkPart.push(id);
                        }
                    });
                    if(checkPart.length == 0){
                        alert('Anda belum memilih data yang akan ditambahkan');
                        return;
                    }
                    var checkPartMap = JSON.stringify(checkPart);
                    var fa = document.getElementById("fa").value;
                    $.ajax({
                        url:'${request.contextPath}/partsJobFieldAction/tambahParts?fa=' + fa,
                        type: "POST", // Always use POST when deleting data
                        data : { ids: checkPartMap },
                        success : function(data){

                            reloadpartsTable();
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            alert('Internal Server Error');
                        }
                    });

                checkPart = [];

             }

            tambahJobs = function(){
                   var checkJob =[];
                    $("#searchJobs_datatables tbody .row-select").each(function() {
                        if(this.checked){
                            var id = $(this).next("input:hidden").val();
                            checkJob.push(id);
                        }
                    });
                    if(checkJob.length == 0){
                        alert('Anda belum memilih data yang akan ditambahkan');
                        return;
                    }
                    var checkJobMap = JSON.stringify(checkJob);
                    var fa = $('#fa').val();
                    $.ajax({
                        url:'${request.contextPath}/partsJobFieldAction/tambahJobs?fa=' + fa,
                        type: "POST", // Always use POST when deleting data
                        data : { ids: checkJobMap },
                        success : function(data){
                            reloadjobsTable();
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            alert('Internal Server Error');
                        }
                    });

                checkJob = [];

             }

            onDeleteParts = function(){
                   var checkPart =[];
                    $("#parts_datatables tbody .row-select").each(function() {
                        if(this.checked){
                            var id = $(this).next("input:hidden").val();
                            checkPart.push(id);
                        }
                    });
                    if(checkPart.length == 0){
                        alert('Anda belum memilih data yang akan dihapus');
                        return;
                    }
                    var checkPartMap = JSON.stringify(checkPart);
                    var fa = document.getElementById("fa").value;
                    $.ajax({
                        url:'${request.contextPath}/partsJobFieldAction/deleteParts?fa=' + fa,
                        type: "POST", // Always use POST when deleting data
                        data : { ids: checkPartMap },
                        success : function(data){
                            reloadpartsTable();
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            alert('Internal Server Error');
                        }
                    });

                checkPart = [];

             }

            onDeleteJobs = function(){
                   var checkJob =[];
                    $("#jobs_datatables tbody .row-select").each(function() {
                        if(this.checked){
                            var id = $(this).next("input:hidden").val();
                            checkJob.push(id);
                        }
                    });
                    if(checkJob.length == 0){
                        alert('Anda belum memilih data yang akan ditambahkan');
                        return;
                    }
                    var checkJobMap = JSON.stringify(checkJob);
                    var fa = document.getElementById("fa").value;
                    $.ajax({
                        url:'${request.contextPath}/partsJobFieldAction/deleteJobs?fa=' + fa,
                        type: "POST", // Always use POST when deleting data
                        data : { ids: checkJobMap },
                        success : function(data){

                            reloadjobsTable();
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            alert('Internal Server Error');
                        }
                    });

                checkJob = [];

             }

            lihatDetail = function(){
                reloadjobsTable();
                reloadpartsTable();
            }

            closeModal = function(modal){
                var root="${resource()}";
                var url = root+'/addDetailFieldAction/findList';
                jQuery('#fa').load(url);
                $('#addMoreModal').modal('hide');
            }

            onSaveData = function(){
                try{
                    var checkParts =[];
                    var jumlah = [];
                    var isNull = false;
                    var fa = $('#fa').val();
                    var rows = $("#parts_datatables").dataTable().fnGetNodes();
                    for(var i=0;i< rows.length;i++)
                    {
                        var id = $(rows[i]).find("#idPopUp").val();
                        checkParts.push(id);
                        var jum = document.getElementById("jumlah_"+id).value
                        if(jum=="" || jum==null){
                        isNull=true;
                        }
                        jumlah.push(jum)
                    }
                    if(isNull){
                        alert('Ada jumlah yang masih kosong. Harap isi semua jumlah');
                        return;
                    }
                    if(checkParts.length == 0){
                    }
                    var ids = JSON.stringify(checkParts);
                    var jumlahs = JSON.stringify(jumlah);
                    $.ajax({
                        url:'${request.contextPath}/partsJobFieldAction/doSave?',
                        type: "POST",
                        data : { ids: ids,jumlah : jumlahs,fa : fa },
                        success : function(data){
                            toastr.success("Data Berhasil Disimpan");
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            alert('Internal Server Error');
                        }
                    });
                    checkParts = [];
                }catch (e){
                    alert(e)
                }
            }

            });
    </g:javascript>

</head>

<body>

    <div class="box">
        <div class="span12" id="input-table">
        <legend style="font-size: small">Field Action Category</legend>

        <div class="controls">
            <table style="width: 50%">
                <tr>
                    <td style="width: 90%">
                        <form id="form-input">
                            Deskripsi Field Action:
                            <g:select id="fa" name="fa" style="width: 70%" from="${FA.list()}" onchange="lihatDetail();" optionKey="id" noSelection="[0:'Pilih FA']" required="" class="many-to-one"/>
                            &nbsp;
                        </form>
                    </td>
                    <td>
                        <button class="btn cancel pull-left" onclick="addMore();"> . . .</button>
                    </td>
                </tr>
            </table>
        </div>


        <legend style="font-size: small">Parts yang terlibat</legend>
        <fieldset class="buttons controls">
            <button class="btn btn-primary" onclick="onSearchAndAddParts();">Search and Add Parts...</button>
            <button class="btn btn-primary" onclick="onDeleteParts();">Delete</button>
        </fieldset>

        <div id="partsDataTables_tabs-1">
            <g:render template="partsDataTables"/>
        </div>

        <legend style="font-size: small">Jobs yang Terlibat</legend>
        <fieldset class="buttons controls">
            <button class="btn btn-primary" onclick="onSearchAndAddJobs();">Search and Add Jobs...</button>
            <button class="btn btn-primary" onclick="onDeleteJobs();">Delete</button>
        </fieldset>

        <div id="jobsDataTables_tabs-1">
            <g:render template="jobsDataTables"/>
        </div>
    </div>

        <div id="addMoreModal" class="modal hide" style="width: 1200px;">
            <div class="modal-dialog" style="width: 1200px;">
                <div class="modal-content" style="width: 1200px; height: 550">
                    <div class="modal-body" style="max-height: 550px;">
                        <div id="addMoreContent">
                            <div class="iu-content"></div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button name="closeModal" type="button" onclick="closeModal();" class="btn cancel">Close</button>
                    </div>
                </div>
            </div>
        </div>

    </div>

<div id="searchAndAddPartsModal" class="modal fade">
    <div class="modal-dialog" style="width: 1200px;">
        <div class="modal-content" style="width: 1200px;">
            <!-- dialog body -->
            <div class="modal-body" style="max-height: 450px;">
                <div id="searchAndAddPartsContent"/>

                <div class="iu-content"></div>
            </div>
            <!-- dialog buttons -->
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" onclick="tambahParts()">Add Selected</button>
                <button id='closesearchAndAddParts' type="button" data-dismiss="modal"
                        class="btn btn-primary">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="searchAndAddJobsModal" class="modal fade">
    <div class="modal-dialog" style="width: 1200px;">
        <div class="modal-content" style="width: 1200px;">
            <!-- dialog body -->
            <div class="modal-body" style="max-height: 450px;">
                <div id="searchAndAddJobsContent"/>

                <div class="iu-content"></div>
            </div>
            <!-- dialog buttons -->
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" onclick="tambahJobs()">Add Selected</button>
                <button id='closesearchAndAddJobs' type="button" data-dismiss="modal"
                        class="btn btn-primary">Close</button>
            </div>
        </div>
    </div>
</div>

<fieldset class="buttons controls">

    %{--<button class="btn btn-primary" id="buttonEdit" onclick="onEdit()">Edit</button>--}%
    <button class="btn btn-primary" onclick="onSaveData()" id="buttonSave">Save</button>
    %{--<button class="btn btn-primary" id="buttonCancel" onclick="onCancel()">Cancel</button>--}%

    <button class="btn btn-primary" id="buttonClose" onclick="window.location.replace('#/home');">Close</button>

</fieldset>

</body>
</html>