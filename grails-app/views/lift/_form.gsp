<%@ page import="com.kombos.administrasi.Lift" %>



<div class="control-group fieldcontain ${hasErrors(bean: liftInstance, field: 'm029NamaLift', 'error')} required">
	<label class="control-label" for="m029NamaLift">
		<g:message code="lift.m029NamaLift.label" default="Nama Lift" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m029NamaLift" maxlength="25" id="m029NamaLift" required="" value="${liftInstance?.m029NamaLift}"/>
	</div>
</div>

<g:javascript>
    document.getElementById('m029NamaLift').focus();
</g:javascript>

%{--<div class="control-group fieldcontain ${hasErrors(bean: liftInstance, field: 'm029ID', 'error')} required">--}%
	%{--<label class="control-label" for="m029ID">--}%
		%{--<g:message code="lift.m029ID.label" default="M029 ID" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:field name="m029ID" type="number" value="${liftInstance.m029ID}" required=""/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: liftInstance, field: 'createdBy', 'error')} ">--}%
	%{--<label class="control-label" for="createdBy">--}%
		%{--<g:message code="lift.createdBy.label" default="Created By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="createdBy" value="${liftInstance?.createdBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: liftInstance, field: 'updatedBy', 'error')} ">--}%
	%{--<label class="control-label" for="updatedBy">--}%
		%{--<g:message code="lift.updatedBy.label" default="Updated By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="updatedBy" value="${liftInstance?.updatedBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: liftInstance, field: 'lastUpdProcess', 'error')} ">--}%
	%{--<label class="control-label" for="lastUpdProcess">--}%
		%{--<g:message code="lift.lastUpdProcess.label" default="Last Upd Process" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="lastUpdProcess" value="${liftInstance?.lastUpdProcess}"/>--}%
	%{--</div>--}%
%{--</div>--}%

