

<%@ page import="com.kombos.administrasi.Lift" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'lift.label', default: 'Lift')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteLift;

$(function(){ 
	deleteLift=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/lift/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadLiftTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-lift" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="lift"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${liftInstance?.m029NamaLift}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m029NamaLift-label" class="property-label"><g:message
					code="lift.m029NamaLift.label" default="Nama Lift" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m029NamaLift-label">
						%{--<ba:editableValue
								bean="${liftInstance}" field="m029NamaLift"
								url="${request.contextPath}/Lift/updatefield" type="text"
								title="Enter m029NamaLift" onsuccess="reloadLiftTable();" />--}%
							
								<g:fieldValue bean="${liftInstance}" field="m029NamaLift"/>
							
						</span></td>
					
				</tr>
				</g:if>
			

				<g:if test="${liftInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="lift.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${liftInstance}" field="createdBy"
								url="${request.contextPath}/Lift/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadLiftTable();" />--}%
							
								<g:fieldValue bean="${liftInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${liftInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="lift.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${liftInstance}" field="updatedBy"
								url="${request.contextPath}/Lift/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadLiftTable();" />--}%
							
								<g:fieldValue bean="${liftInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${liftInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="lift.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${liftInstance}" field="lastUpdProcess"
								url="${request.contextPath}/Lift/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadLiftTable();" />--}%
							
								<g:fieldValue bean="${liftInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${liftInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="lift.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${liftInstance}" field="dateCreated"
								url="${request.contextPath}/Lift/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadLiftTable();" />--}%
							
								<g:formatDate date="${liftInstance?.dateCreated}"  type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${liftInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="lift.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${liftInstance}" field="lastUpdated"
								url="${request.contextPath}/Lift/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadLiftTable();" />--}%
							
								<g:formatDate date="${liftInstance?.lastUpdated}"  type="datetime" style="MEDIUM"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${liftInstance?.id}"
					update="[success:'lift-form',failure:'lift-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteLift('${liftInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
