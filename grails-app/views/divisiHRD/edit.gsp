<%@ page import="com.kombos.hrd.DivisiHRD" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'divisiHRD.label', default: 'DivisiHRD')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="edit-divisiHRD" class="content scaffold-edit" role="main">
            <legend>Edit Divisi (HRD Module)</legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${divisiHRDInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${divisiHRDInstance}" var="error">
                    <li>Field '${error.field}' harus unique!</li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form method="post" >--}%
			<g:formRemote class="form-horizontal" name="edit" on404="alert('not found!');" onSuccess="reloadDivisiHRDTable();" update="divisiHRD-form"
				url="[controller: 'divisiHRD', action:'update']">
				<g:hiddenField name="id" value="${divisiHRDInstance?.id}" />
				<g:hiddenField name="version" value="${divisiHRDInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons controls">
					<g:actionSubmit class="btn btn-primary save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                    <a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout('divisiHRD');"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
				</fieldset>
			</g:formRemote>
			%{--</g:form>--}%
		</div>
	</body>
</html>
