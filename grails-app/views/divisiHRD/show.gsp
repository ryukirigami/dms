

<%@ page import="com.kombos.hrd.DivisiHRD" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'divisiHRD.label', default: 'DivisiHRD')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteDivisiHRD;

$(function(){ 
	deleteDivisiHRD=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/divisiHRD/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadDivisiHRDTable();
   				expandTableLayout('divisiHRD');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-divisiHRD" role="main">
        <legend>Show Divisi (HRD Module)</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="divisiHRD"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${divisiHRDInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="divisiHRD.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${divisiHRDInstance}" field="createdBy"
								url="${request.contextPath}/DivisiHRD/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadDivisiHRDTable();" />--}%
							
								<g:fieldValue bean="${divisiHRDInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${divisiHRDInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="divisiHRD.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${divisiHRDInstance}" field="dateCreated"
								url="${request.contextPath}/DivisiHRD/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadDivisiHRDTable();" />--}%
							
								<g:formatDate date="${divisiHRDInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${divisiHRDInstance?.divisi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="divisi-label" class="property-label"><g:message
					code="divisiHRD.divisi.label" default="Divisi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="divisi-label">
						%{--<ba:editableValue
								bean="${divisiHRDInstance}" field="divisi"
								url="${request.contextPath}/DivisiHRD/updatefield" type="text"
								title="Enter divisi" onsuccess="reloadDivisiHRDTable();" />--}%
							
								<g:fieldValue bean="${divisiHRDInstance}" field="divisi"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${divisiHRDInstance?.keterangan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="keterangan-label" class="property-label"><g:message
					code="divisiHRD.keterangan.label" default="Keterangan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="keterangan-label">
						%{--<ba:editableValue
								bean="${divisiHRDInstance}" field="keterangan"
								url="${request.contextPath}/DivisiHRD/updatefield" type="text"
								title="Enter keterangan" onsuccess="reloadDivisiHRDTable();" />--}%
							
								<g:fieldValue bean="${divisiHRDInstance}" field="keterangan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${divisiHRDInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="divisiHRD.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${divisiHRDInstance}" field="lastUpdProcess"
								url="${request.contextPath}/DivisiHRD/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadDivisiHRDTable();" />--}%
							
								<g:fieldValue bean="${divisiHRDInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${divisiHRDInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="divisiHRD.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${divisiHRDInstance}" field="lastUpdated"
								url="${request.contextPath}/DivisiHRD/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadDivisiHRDTable();" />--}%
							
								<g:formatDate date="${divisiHRDInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${divisiHRDInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="divisiHRD.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${divisiHRDInstance}" field="staDel"
								url="${request.contextPath}/DivisiHRD/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadDivisiHRDTable();" />--}%
							
								<g:fieldValue bean="${divisiHRDInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${divisiHRDInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="divisiHRD.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${divisiHRDInstance}" field="updatedBy"
								url="${request.contextPath}/DivisiHRD/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadDivisiHRDTable();" />--}%
							
								<g:fieldValue bean="${divisiHRDInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('divisiHRD');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${divisiHRDInstance?.id}"
					update="[success:'divisiHRD-form',failure:'divisiHRD-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteDivisiHRD('${divisiHRDInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
