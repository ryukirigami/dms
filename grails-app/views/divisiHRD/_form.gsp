<%@ page import="com.kombos.hrd.DivisiHRD" %>



<div class="control-group fieldcontain ${hasErrors(bean: divisiHRDInstance, field: 'divisi', 'error')} ">
	<label class="control-label" for="divisi">
		<g:message code="divisiHRD.divisi.label" default="Divisi" />
		
	</label>
	<div class="controls">
	<g:textField name="divisi" value="${divisiHRDInstance?.divisi}" maxlength="16" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: divisiHRDInstance, field: 'keterangan', 'error')} ">
	<label class="control-label" for="keterangan">
		<g:message code="divisiHRD.keterangan.label" default="Keterangan" />
		
	</label>
	<div class="controls">
	<g:textArea rows="5" cols="3" name="keterangan" value="${divisiHRDInstance?.keterangan}" maxlength="128" />
	</div>
</div>

<g:javascript>
    $(function() {
        oFormUtils.fnInit();
    })
</g:javascript>
