
<%@ page import="com.kombos.customerprofile.FA; com.kombos.administrasi.Operation; com.kombos.parts.Returns;com.kombos.production.FinalInspection" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="bPQualityControl.inspectionDuringRepair.label" default="Inspection During Repair BP" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;
	var save;
	$(function(){

        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

        $('.box-action').click(function(){
            return false;
        });


   });
    $('input:radio[name=staRedo]').click(function(){
        if($('input:radio[name=staRedo]:nth(0)').is(':checked')){
            $("#redoJob").prop('disabled', false);
            $("#redoProses").prop('disabled', false);
        }else{
            $("#redoJob").prop('disabled', true);
            $("#redoProses").prop('disabled', true);
        }
    });
     $('input:radio[name=staIdr]').click(function(){
        if($('input:radio[name=staIdr]:nth(0)').is(':checked')){
            $("#alasanStaIdr").prop('disabled', false);

        }else{
            $("#alasanStaIdr").prop('disabled', true);

        }
    });

    save = function(){
            var staRedo=""
            var staIdr=""
            if($('input:radio[name=staRedo]:nth(0)').is(':checked')){
                staRedo="1";
            }else{
                staRedo="0";
            }
            if($('input:radio[name=staIdr]:nth(0)').is(':checked')){
                staIdr="1";
            }else{
                staIdr="0";
            }
            var nomorWO = $('#nomorWO').val();
            var alasanStaIdr = $('#alasanStaIdr').val();
            var tanggalUpdate = $('#tanggalUpdate').val();
            var redoJob = $('#redoJob').val();
            var redoProses = $('#redoProses').val();
            if(staIdr=="" || staIdr==null || alasanStaIdr=="" || alasanStaIdr == null || staRedo=="" || staRedo==null ){
                alert('Ada data yang masih kosong');
            }else{
                $.ajax({
                url:'${request.contextPath}/BPQualityControl/saveInspection',
                type: "POST", // Always use POST when deleting data
                data : {noWO : nomorWO ,tanggalUpdate:tanggalUpdate, staIdr : staIdr, alasanStaIdr : alasanStaIdr , staRedo : staRedo , redoJob : redoJob, redoProses : redoProses},
                success : function(data){
                    toastr.success('<div>Data berhasil disimpan.</div>');
                    expandTableLayout();
                },
            error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
            }
            });

            }

    }
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="bPQualityControl.inspectionDuringRepair.label" default="Inspection During Repair BP" /></span>
</div>
<div class="box">
    <table>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.t401NoWo.label" default="Nomor WO" />
            </td>
            <td>
                <g:textField  name="nomorWO" id="nomorWO" readonly="" value="${reception?.t401NoWO}" />
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.noPolisi.label" default="Nomor Polisi" />
            </td>
            <td>
                <g:textField  name="nomorPolisi" id="nomorPolisi" readonly="" value="${reception?.historyCustomerVehicle?.fullNoPol}"/>
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Model Kendaraan" />
            </td>
            <td>
                <g:textField  name="modelKendaraan" id="modelKendaraan" readonly="" value="${model}"/>
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Nama Stall" />
            </td>
            <td>
                <g:textField  name="stall" id="stall" readonly="" value="${stall}" />
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Teknisi" />
            </td>
            <td>
                <g:textField  name="teknisi" id="teknisi" readonly="" value="${teknisi}" />
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Tanggal Update" />
            </td>
            <td>
                <g:textField  name="tanggalUpdate" id="tanggalUpdate" readonly="" value="${tanggalUpdate}" />
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Status IDR" />
            </td>
            <td>

                <g:radioGroup name="staIdr" id="staIdr" values="['1','0']" labels="['OK','Not OK']" value="" >
                    ${it.radio} ${it.label}
                </g:radioGroup>
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Alasan Status IDR" />
            </td>
            <td>
                <g:textArea  name="alasanStaIdr" id="alasanStaIdr" value="" />
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Perlu Redo?" />
            </td>
            <td>
                <g:radioGroup name="staRedo" id="staRedo" values="['1','0']" labels="['Ya','Tidak']" value="" >
                    ${it.radio} ${it.label}
                </g:radioGroup>
            </td>
        </tr>
        <tr>
            <td>
                <b><span style="text-decoration: underline;">Redo</span></b>
                <br/><br/>
                &nbsp;&nbsp;&nbsp;&nbsp;Redo Ke Job
            </td>
            <td>
                <br/><br/><g:select name="redoJob" id="redoJob" optionKey="id" from="${Operation.createCriteria().list(){eq("staDel","0")}}" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;&nbsp;&nbsp;&nbsp;Redo Ke Proses
            </td>
            <td>
                <g:select name="redoProses" id="redoProses" optionKey="id" from="${Operation.createCriteria().list {eq("staDel","0")}}" />
            </td>
        </tr>
    </table>
    <div>
        <g:field type="button" style="width: 100px" class="btn cancel" onclick="window.location.href ='#/BPProblemFindingList';"
                 name="inputProblem" id="inputProblem" value="${message(code: 'default.inputProblem.label', default: 'Input Problem')}" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <g:field type="button" style="width: 100px" class="btn btn-primary create" onclick="save();"
                 name="btnOk" id="btnOk" value="${message(code: 'default.addJob.label', default: 'OK')}" />
        &nbsp;&nbsp;&nbsp;
        <g:field type="button" style="width: 100px" class="btn cancel" onclick="expandTableLayout();"
                 name="btnClose" id="btnClose" value="${message(code: 'default.addParts.label', default: 'Close')}" />
    </div>
</div>
</body>
</html>
