
<%@ page import="com.kombos.parts.StokOPName" %>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript>
    var bPQualityControlSub3QualityTable;
    var bPQualityControlSub3InspectionTable;
$(function(){

    bPQualityControlSub3QualityTable = $('#bPQualityControl_datatables_sub3Quality_${idTable}').dataTable({
		"sScrollX": "1205px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSub3QualityList")}",
		"aoColumns": [
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"12px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"12px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"12px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"11px",
   "sDefaultContent": ''
},
{
	"sName": "",
	"mDataProp": "quality",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"194px",
	"bVisible": true
}
,

{
	"sName": "",
	"mDataProp": "start",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"194px",
	"bVisible": true
}
,
{
    "sName": "",
	"mDataProp": "stop",
    "aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"180px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "lolos",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"209px",
	"bVisible": true
}
,

{
	"sName": "",
	"mDataProp": "alasan",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"194px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "redo",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"195px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						aoData.push(
									{"name": 't401NoWO', "value": "${t401NoWO}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});


    bPQualityControlSub3InspectionTable = $('#bPQualityControl_datatables_sub3Inspection_${idTable}').dataTable({
		"sScrollX": "1205px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSub3InspectionList")}",
		"aoColumns": [
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"12px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"12px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"13px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"11px",
   "sDefaultContent": ''
},
{
	"sName": "",
	"mDataProp": "inspect",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"194px",
	"bVisible": true
}
,

{
	"sName": "",
	"mDataProp": "start",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"194px",
	"bVisible": true
}
,
{
    "sName": "",
	"mDataProp": "stop",
    "aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"180px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "status",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"209px",
	"bVisible": true
}
,

{
	"sName": "",
	"mDataProp": "alasan",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"194px",
	"bVisible": true
}
,
{
	"sName": "",
	"mDataProp": "redo",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"195px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						aoData.push(
									{"name": 't401NoWO', "value": "${t401NoWO}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
    </g:javascript>

</head>
<body>
<div class="inner3Details">

    <g:if test="${inspect!=null}">
        <table id="bPQualityControl_datatables_sub3Inspection_${idTable}" cellpadding="0" cellspacing="0" border="0"
               class="display table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th></th>

                <th></th>

                <th></th>

                <th></th>

                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="bPQualityControl.roleKirim.label" default="Insp. Drg. Repair" /></div>
                </th>

                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="bPQualityControl.namaKirim.label" default="Start" /></div>
                </th>

                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="bPQualityControl.tanggalKirim.label" default="Stop" /></div>
                </th>

                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="bPQualityControl.deskripsiMasalah.label" default="Status IDR" /></div>
                </th>

                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="bPQualityControl.tanggalSolved.label" default="Alasan" /></div>
                </th>

                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="bPQualityControl.staProblem.label" default="Redo ke Job - Proses" /></div>
                </th>

            </tr>
            </thead>
        </table>
    </g:if>

    <g:if test="${quality!=null}">
        <table id="bPQualityControl_datatables_sub3Quality_${idTable}" cellpadding="0" cellspacing="0" border="0"
               class="display table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th></th>

                <th></th>

                <th></th>

                <th></th>

                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="bPQualityControl.roleKirim.label" default="Quality Control" /></div>
                </th>

                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="bPQualityControl.namaKirim.label" default="Start" /></div>
                </th>

                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="bPQualityControl.tanggalKirim.label" default="Stop" /></div>
                </th>

                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="bPQualityControl.deskripsiMasalah.label" default="Lolos QC?" /></div>
                </th>

                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="bPQualityControl.tanggalSolved.label" default="Alasan" /></div>
                </th>

                <th style="border-bottom: none;padding: 5px;">
                    <div><g:message code="bPQualityControl.staProblem.label" default="Redo ke Job - Proses" /></div>
                </th>

            </tr>
            </thead>
        </table>
    </g:if>

</div>
</body>
</html>


			
