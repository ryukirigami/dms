
<%@ page import="com.kombos.reception.Reception" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="bPQualityControl.label" default="Body & Paint - Quality Control & Inspection List"/></title>
    <r:require modules="baseapplayout, baseapplist" />
    <g:javascript>
			var WODetail;
			$(function(){ 
			
				WODetail = function(){
				    var checkID =[];
                    var id;
                    $('#bPQualityControl-table tbody .row-select').each(function() {
                        if(this.checked){
                            var id = $(this).next("input:hidden").val();
                            checkID.push(id);
                        }
                    });

                    if(checkID.length!=1){
                        if(checkID.length<1){
                            alert('Pilih salah satu data untuk melihat WO Detail');
                        }else{
                            alert('Anda hanya boleh memilih 1 data');
                        }
                        return;
                    }else{
                        var idUbah=checkID[0];
                        window.location.href='#/bpWODetail';
                        $.ajax({url: '${request.contextPath}/BPWODetail?idUbah='+idUbah,
                            type:"GET",dataType: "html",
                            complete : function (req, err) {
                                $('#main-content').html(req.responseText);
                                $('#spinner').fadeOut();
                                loading = false;
                            }
                        });
                    }

				};

                shrinkTableLayout = function(){
                    $("#bPQualityControl-table").hide();
                    $("#bPQualityControl-form").css("display","block");
                }

                expandTableLayout = function(){
                    $("#bPQualityControl-table").show();
                    $("#bPQualityControl-form").css("display","none");
                }


});
    shrinkTableLayout = function(){
        $("#bPQualityControl-table").hide()
        $("#bPQualityControl-form").css("display","block");
    }

    loadForm = function(data, textStatus){
        $('#bPQualityControl-form').empty();
        $('#bPQualityControl-form').append(data);
    }

    expandTableLayout = function(){
        $("#bPQualityControl-table").show()
        $("#bPQualityControl-form").css("display","none");
    }
    

    function quality(){
        var checkID =[];
        var id;
        $('#bPQualityControl-table tbody .row-select').each(function() {
            if(this.checked){
                var id = $(this).next("input:hidden").val();
                checkID.push(id);
            }
        });

        if(checkID.length!=1){
            if(checkID.length<1){
                alert('Pilih salah satu data');
            }else{
                alert('Anda hanya boleh memilih 1 data');
            }
            return;
        }else{
            var idUbah=checkID[0];
            shrinkTableLayout();
            $('#spinner').fadeIn(1);
            $.ajax({type:'POST', url:'${request.contextPath}/BPQualityControl/qualityControl',
                data : {noWo : idUbah},
                success:function(data,textStatus){
                    loadForm(data, textStatus);
                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
        }

    };
				
				
    function inspection(){
        var checkID =[];
        var id;
        $('#bPQualityControl-table tbody .row-select').each(function() {
            if(this.checked){
                var id = $(this).next("input:hidden").val();
                checkID.push(id);
            }
        });

        if(checkID.length!=1){
            if(checkID.length<1){
                alert('Pilih salah satu data');
            }else{
                alert('Anda hanya boleh memilih 1 data');
            }
            return;
        }else{
            var idUbah=checkID[0];
            shrinkTableLayout();
            $('#spinner').fadeIn(1);
            $.ajax({type:'POST', url:'${request.contextPath}/BPQualityControl/inspectionDuringRepair',
                data : {noWo : idUbah},
                success:function(data,textStatus){
                    loadForm(data, textStatus);
                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
        }
    };

    function finalInspection(){
        var checkID =[];
        var id;
        $('#bPQualityControl-table tbody .row-select').each(function() {
            if(this.checked){
                var id = $(this).next("input:hidden").val();
                checkID.push(id);
            }
        });

        if(checkID.length!=1){
            if(checkID.length<1){
                alert('Pilih salah satu data');
            }else{
                alert('Anda hanya boleh memilih 1 data');
            }
            return;
        }else{
            var idUbah=checkID[0];

            shrinkTableLayout();
            $('#spinner').fadeIn(1);
            $.ajax({type:'POST', url:'${request.contextPath}/BPQualityControl/finalInspection',
                data : {noWo : idUbah},
                success:function(data,textStatus){
                    loadForm(data, textStatus);
                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
        }
    };

    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

        return true;
    }

    var checkin = $('#search_Tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
        var newDate = new Date(ev.date)
        newDate.setDate(newDate.getDate() + 1);
        checkout.setValue(newDate);
        checkin.hide();
        $('#search_Tanggalakhir')[0].focus();
    }).data('datepicker');

    var checkout = $('#search_Tanggalakhir').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');


    function clearForm(){
        $('#search_Tanggal').val('')
        $('#search_Tanggalakhir').val('')
        $('#staRedoJob').val('')
        $('#staQualityControl').val('')
        $('#staLolosQC').val('')
        $('#staFinalInspection').val('')
        $('#staLolosFI').val('')
        $('#sa').val('')
        $('#foreman').val('')
        reloadBPQualityControlTable();
    }

    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="bPQualityControl.label" default="Body & Paint - Quality Control & Inspection List"/></span>
    <ul class="nav pull-right">
        <li class="separator"></li>
    </ul>
</div>
<div class="box">
    <div class="span12" id="bPQualityControl-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>

        <table>
            <tr>
                <td style="padding: 5px">
                    <g:message code="wo.tanggalWO.label" default="Tanggal WO"/>
                </td>
                <td style="padding: 5px">
                    <ba:datePicker name="search_Tanggal" precision="day" value="" format="dd-MM-yyyy"/>&nbsp;s.d.&nbsp;
                    <ba:datePicker name="search_Tanggalakhir" precision="day" value="" format="dd-MM-yyyy"/>
                </td>
                <td style="padding: 5px">
                    <g:message code="bPQualityControl.staRedoJob.label" default="Status Redo - Job Proses"/>
                </td>
                <td style="padding: 5px">
                    <g:select name="staRedoJob" id="staRedoJob" from="${['Ada Redo','Tidak Ada Redo']}" noSelection="['':'Pilih Status Redo']" />
                </td>
                <td rowspan="4">
                    <g:field type="button" onclick="reloadBPQualityControlTable();" style="padding: 5px; width: 120px"
                             class="btn cancel pull-right box-action" name="filter" id="filter" value="${message(code: 'default.button.filter.label', default: 'Filter')}" />
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <g:field type="button" onclick="clearForm();" style="padding: 5px; width: 120px"
                             class="btn cancel pull-right box-action" name="clear" id="clear" value="${message(code: 'default.button.clear.label', default: 'Clear Filter')}" />
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    <g:message code="bPQualityControl.staQualityControl.label" default="Status Quality Control"/>
                </td>
                <td style="padding: 5px">
                    <g:select name="staQualityControl" style="width:100%" id="staQualityControl" noSelection="['':'Pilih Status Quality Control']" from="${['Sudah Quality Control (QC)','Belum Quality Control (QC)']}" />
                </td>
                <td style="padding: 5px">
                    <g:message code="bPQualityControl.staLolosQC.label" default="Status Lolos QC"/>
                </td>
                <td style="padding: 5px">
                    <g:select name="staLolosQC" id="staLolosQC" noSelection="['':'Pilih Status Lolos QC']" from="${['Lolos QC','Tidak Lolos QC']}" />
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    <g:message code="bPQualityControl.staFinalInspection.label" default="Status Final Inspection"/>
                </td>
                <td style="padding: 5px">
                    <g:select style="width:100%" name="staFinalInspection" id="staFinalInspection" noSelection="['':'Pilih Status Final Inspection']" from="${['Sudah Final Inspection','Belum  Final Inspection']}" />
                </td>
                <td style="padding: 5px">
                    <g:message code="bPQualityControl.staLolosFI.label" default="Status Lolos FI"/>
                </td>
                <td style="padding: 5px">
                    <g:select style="width:100%" name="staLolosFI" id="staLolosFI" noSelection="['':'Pilih Status Lolos FI']" from="${['Lolos','Tidak']}" />
                </td>

            </tr>
            <tr>
                <td style="padding: 5px">
                    <g:message code="bPQualityControl.sa.label" default="SA"/>
                </td>
                <td style="padding: 5px">
                    <g:select style="width:100%" name="sa" id="sa" noSelection="['':'Pilih SA']" from="${Reception.createCriteria().list {eq("staDel","0");eq("staSave","0");eq("companyDealer",session?.userCompanyDealer);customerIn{tujuanKedatangan{eq("m400Tujuan","bp", [ignoreCase: true])}}}}" optionValue="t401NamaSA" />
                </td>
                <td style="padding: 5px">
                    <g:message code="bPQualityControl.staLolosFI.label" default="Foreman"/>
                </td>
                <td style="padding: 5px">
                    <g:select style="width:100%" name="foreman" id="foreman" noSelection="['':'Pilih Foreman']" from="${['(Semua)']}" />
                </td>

            </tr>
        </table>
        <br/><br/>
        <g:render template="dataTables" />
        <g:field type="button" onclick="return WODetail();" style="padding: 5px;"
                 class="btn cancel" name="woDetail" id="woDetail" value="${message(code: 'wo.woDetail.label', default: 'WO Detail')}" />
        &nbsp;&nbsp;
        <g:field type="button" onclick="quality(); " style="padding: 5px;"
                 class="btn cancel" name="runJPB" id="runJPB" value="${message(code: 'wo.QC.label', default: 'Quality Control')}" />
        &nbsp;&nbsp;
        <g:field type="button" onclick="inspection();" style="padding: 5px;"
                 class="btn cancel" name="idr" id="idr" value="${message(code: 'wo.inspectDUringRepair.label', default: 'Inspection During Repair')}" />
        &nbsp;&nbsp;
        <g:field type="button" onclick="finalInspection();" style="padding: 5px;"
                 class="btn cancel" name="fi" id="fi" value="${message(code: 'wo.fi.label', default: 'Final Inspection')}" />
        </div>
    <div class="span11" id="bPQualityControl-form" style="display: none;"></div>
</div>
</body>
</html>
