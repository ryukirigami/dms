
<%@ page import="com.kombos.production.Deffect; com.kombos.administrasi.Operation; com.kombos.parts.Returns;com.kombos.production.FinalInspection" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="bPQualityControl.qualityControl.label" default="Quality Control" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;
	var save;
	$(function(){

        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

        $('.box-action').click(function(){
            return false;
        });


   });
    $('input:radio[name=staLolosQC]').click(function(){
        if($('input:radio[name=staLolosQC]:nth(0)').is(':checked')){
            $("#alasanStaIdr").prop('disabled', true);
        }else{
            $("#alasanStaIdr").prop('disabled', false);
        }
    });

    save = function(){
            var staLolosQC=""
            if($('input:radio[name=staLolosQC]:nth(0)').is(':checked')){
                staLolosQC="1";
            }else{
                staLolosQC="0";
            }
            var nomorWO = $('#nomorWO').val();
            var alasanStaIdr = $('#alasanStaIdr').val();
            var redoJob = $('#redoJob').val();
            var redoProses = $('#redoProses').val();
            var catatan = $('#catatan').val();
            var tanggalUpdate = $('#tanggalUpdate').val();
            if(staLolosQC=="" || staLolosQC==null || catatan=="" || catatan==null){
                alert('Ada data yang masih kosong.')
            }else{
                console.log('========================= '+staLolosQC+' ++++++++++++++++++++ '+alasanStaIdr);
                $.ajax({
                url:'${request.contextPath}/BPQualityControl/saveQC',
                type: "POST", // Always use POST when deleting data
                data : {noWO : nomorWO ,staLolosQC : staLolosQC, tanggalUpdate:tanggalUpdate, alasanStaIdr : alasanStaIdr , redoJob : redoJob, redoProses : redoProses, catatan : catatan},
                success : function(data){
                    toastr.success('<div>Data berhasil disimpan.</div>');
                    expandTableLayout();
                },
            error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
            }
            });

            }

    }
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="bPQualityControl.qualityControl.label" default="Quality Control" /></span>
</div>
<div class="box">
    <table>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.t401NoWo.label" default="Nomor WO" />
            </td>
            <td>
                <g:textField  name="nomorWO" id="nomorWO" readonly="" value="${reception?.t401NoWO}" />
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.noPolisi.label" default="Nomor Polisi" />
            </td>
            <td>
                <g:textField  name="nomorPolisi" id="nomorPolisi" readonly="" value="${reception?.historyCustomerVehicle?.fullNoPol}"/>
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Model Kendaraan" />
            </td>
            <td>
                <g:textField  name="modelKendaraan" id="modelKendaraan" readonly="" value="${model}"/>
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Teknisi" />
            </td>
            <td>
                <g:textField  name="teknisi" id="teknisi" readonly="" value="${teknisi}" />
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Job" />
            </td>
            <td>
                <g:textArea  name="job" id="job" readonly="" value="${namaJob}" style="resize:none" />
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Proses" />
            </td>
            <td>
                <g:textField  name="proses" id="proses" readonly="" value="${proses}" />
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Lolos QC? *" />
            </td>
            <td>
                <input type="radio" name="staLolosQC" value="1" required="true"  />Ya  &nbsp;&nbsp;&nbsp;
                <input type="radio" name="staLolosQC" value="0" required="true"  />Tidak
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Alasan tidak lolos QC (defact)" />
                <span style="font-size: 8px"><br/>(Tekan ctrl lalu klik untuk<br/> memilih lebih dari satu)</span>
            </td>
            <td>
                %{--<g:select  name="alasanStaIdr" id="alasanStaIdr" from="${Deffect.createCriteria().list {eq("staDel","0")}}" size="5" multiple="true" value="" />--}%
                <g:textArea  name="alasanStaIdr" id="alasanStaIdr" value="" />
            </td>
        </tr>
        <tr>
            <td>
                <b><span style="text-decoration: underline;">Redo</span></b>
                <br/><br/>
                &nbsp;&nbsp;&nbsp;&nbsp;Redo Ke Job
            </td>
            <td>
                <br/><br/><g:select name="redoJob" id="redoJob" optionKey="id" noSelection="['':'Pilih Job Redo']" from="${Operation.createCriteria().list(){eq("staDel","0")}}" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;&nbsp;&nbsp;&nbsp;Redo Ke Proses
            </td>
            <td>
                <g:select name="redoProses" id="redoProses" optionKey="id" noSelection="['':'Pilih Proses Redo']" from="${Operation.createCriteria().list {eq("staDel","0")}}" />
                s%{--<g:select  name="redoProses" id="redoProses" readonly="" from="${jobRcp}" />--}%
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Catatan Tambahan" />
            </td>
            <td>
                <g:textArea style="resize:none"  name="catatan" id="catatan" value="" />
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Tanggal Update" />
            </td>
            <td>
                <g:textField  name="tanggalUpdate" id="tanggalUpdate" readonly="" value="${tanggalUpdate}" />
            </td>
        </tr>

    </table>
    <div>
        <g:field type="button" style="width: 100px" class="btn btn-primary create" onclick="save();"
                 name="btnOk" id="btnOk" value="${message(code: 'default.addJob.label', default: 'OK')}" />
        &nbsp;&nbsp;&nbsp;
        <g:field type="button" style="width: 100px" class="btn cancel" onclick="expandTableLayout();"
                 name="btnClose" id="btnClose" value="${message(code: 'default.addParts.label', default: 'Close')}" />
    </div>
</div>
</body>
</html>
