
<%@ page import="com.kombos.parts.StokOPName" %>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="baseapplayout" />
    <g:javascript>
    var bPQualityControlSub2Table;
$(function(){
    var anOpen = [];
	$('#bPQualityControl_datatables_sub2_${idTable} td.sub2control').live('click',function () {
	    var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
  		if ( i === -1 ) {
  		    var status=0;
  		    var nDetailsRow;
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = bPQualityControlSub2Table.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST', 
	   			data : oData,
	   			url:'${request.contextPath}/BPQualityControl/sub3list',

	   			success:function(data,textStatus){
	   			    status=1;
//	   			    alert('sukses 1');
	   				nDetailsRow = bPQualityControlSub2Table.fnOpen(nTr,data,'details');
    				$('div.inner3Details', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){

	   				$('#spinner').fadeOut();
	   			}
	   		});
  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.inner3QualityDetails', $(nTr).next()[0]).slideUp( function () {
      			bPQualityControlSub2Table.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

	bPQualityControlSub2Table = $('#bPQualityControl_datatables_sub2_${idTable}').dataTable({
		"sScrollX": "1205px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesSub2List")}",
		"aoColumns": [
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"11px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "bSortable": false,
   "sWidth":"11px",
   "sDefaultContent": ''
},
{
   "mDataProp": null,
   "sClass": "sub2control center",
   "bSortable": false,
   "sWidth":"11px",
   "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "",
	"mDataProp": "proses",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"586px",
	"bVisible": true
}
,

{
	"sName": "",
	"mDataProp": "foreman",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"589px",
	"bVisible": true
}
,

{
	"sName": "t401NoWO",
	"mDataProp": "t401NoWO",
	"aTargets": [2],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"0px",
	"bVisible": false
}],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						aoData.push(
									{"name": 't401NoWO', "value": "${t401NoWO}"}
						);
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
    </g:javascript>

</head>
<body>
<div class="inner2Details">
    <table id="bPQualityControl_datatables_sub2_${idTable}" cellpadding="0" cellspacing="0" border="0"
           class="display table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th></th>

            <th></th>

            <th></th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPQualityControl.proses.label" default="Proses" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="bPQualityControl.foreman.label" default="Foreman" /></div>
            </th>
        </tr>
        </thead>
    </table>

</div>
</body>
</html>


			
