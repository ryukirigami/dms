<%@ page import="com.kombos.administrasi.Operation" %>
<%--
  Created by IntelliJ IDEA.
  User: Ahmad Fawaz
  Date: 13/11/14
  Time: 15:05
--%>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'returnsAdd.label', default: 'Final Inspection')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
	var show;
	var loadForm;
	var editFInal;
	$(function(){

        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

        $('.box-action').click(function(){
            return false;
        });


   });
    $('input:radio[name=staFI]').click(function(){
        if($('input:radio[name=staFI]:nth(1)').is(':checked')){
            $("#redoJob").prop('disabled', false);
            $("#redoProses").prop('disabled', false);
        }else{
            $("#redoJob").prop('disabled', true);
            $("#redoProses").prop('disabled', true);
        }
    });

    editFinal = function(){
            var id = $('#id').val();
            var idV = $('#idV').val();
            var nomorWO = $('#nomorWO').val();
            var nomorPolisi = $('#nomorPolisi').val();
            var hasilFinal = $('#hasilFinal').val();
            var staFI = $('input[name="staFI"]:checked').val();
            var redoJob = $('#redoJob').val();
            // var redoProses = $('#redoProses').val();
            var jobSuggest = $('#jobSuggest').val();
            var tanggalNext = $('#tanggalNext').val();
            var tanggalUpdate = $('#tanggalUpdate').val();
            var jamMulai = $('#jamMulai').val();

            console.log(hasilFinal+staFI+tanggalNext);
            if(hasilFinal=="" || hasilFinal==null || staFI=="" || staFI == null || tanggalNext=="" || tanggalNext==null ){
                alert('Ada data yang masih kosong.')
            }else{
                $.ajax({
                url:'${request.contextPath}/BPQualityControl/editFinal',
                type: "POST", // Always use POST when deleting data
                data : {id:id,nomorWO : nomorWO, nomorPolisi: nomorPolisi, ket : hasilFinal ,staFI : staFI ,redoKeJob:redoJob,
                        jobSuggest:jobSuggest,tanggalNext:tanggalNext,tanggalUpdate:tanggalUpdate, jamMulai:jamMulai},

                success : function(data){
                    toastr.success('<div>Ubah Sukses</div>');
                    expandTableLayout();
                },
            error: function(xhr, textStatus, errorThrown) {
                alert('Internal Server Error');
            }
            });

            }

    }
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
    <ul class="nav pull-right">
        <li></li>
        <li></li>
        <li class="separator"></li>
    </ul>
</div>
<div class="box">
    <table>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.t401NoWo.label" default="Nomor WO" />
                <g:hiddenField name="id" id="id" value="${id}" />
                <g:hiddenField name="jamMulai" id="jamMulai" value="${jamMulai}" />
                <g:hiddenField name="idV" id="idV" value="${idV}" />
            </td>
            <td>
                <g:textField  name="nomorWO" id="nomorWO" readonly="" value="${noWo}" />
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.noPolisi.label" default="Nomor Polisi" />
            </td>
            <td>
                <g:textField  name="nomorPolisi" id="nomorPolisi" readonly="" value="${nopol}"/>
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Model Kendaraan" />
            </td>
            <td>
                <g:textField  name="modelKendaraan" id="modelKendaraan" readonly="" value="${model}"/>
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Nama Stall" />
            </td>
            <td>
                <g:textField  name="stall" id="stall" readonly="" value="${stall}" />
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Foreman" />
            </td>
            <td>
                <g:textField  name="foreman" id="foreman" readonly="" value="${nama}" />
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Status Kendaraan" />
            </td>
            <td>
                <g:textField  name="statusKendarann" id="statusKendarann" readonly="" value="${statusKendaraan}" />
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Hasil Final Inspection" />
            </td>
            <td>
                <g:textArea style="resize: none" required="" name="hasilFinal" id="hasilFinal" value="" />
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Lolos Final Inspection?" />
            </td>

            <td>
                <input type="radio" name="staFI" value="1" required="true"  ${staFI=='1'?'checked="checked"':''} />Ya   &nbsp;
                <input type="radio" name="staFI" value="0" required="true"   ${staFI=='1'?'':'checked="checked"'}  />Tidak
            </td>


        </tr>
        <tr>
            <td>
                <b><span style="text-decoration: underline;">Redo</span></b>
                <br/><br/>
                &nbsp;&nbsp;&nbsp;&nbsp;Redo Ke Job
            </td>
            <td>
                <g:select name="redoJob" id="redoJob" from="${jobRcp}" noSelection="['':'Pilih Job Untuk Redo']" />
            </td>
        </tr>
        %{--<tr>--}%
            %{--<td>--}%
                %{--&nbsp;&nbsp;&nbsp;&nbsp;Redo Ke Proses--}%
            %{--</td>--}%
            %{--<td>--}%
                %{--<g:select name="redoProses" id="redoProses" from="${Operation.createCriteria().list {}}" />--}%
            %{--</td>--}%
        %{--</tr>--}%
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Job Suggest (tidak Wajib)" />
            </td>
            <td>
                <g:textArea style="resize: none" name="jobSuggest" id="jobSuggest" value="${jobSugest}" />
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Tanggal Next Job" />
            </td>
            <td>
                <ba:datePicker name="tanggalNext" id="tanggalNext" required="true" precision="day" value="" format="dd/MM/yyyy"/>
            </td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <g:message code="wo.test.label" default="Tanggal Update" />
            </td>
            <td>
                <g:textField  name="tanggalUpdate" id="tanggalUpdate" readonly="" value="${tanggal}" />
            </td>
        </tr>
    </table>
    <g:hiddenField name="id" id="id" value="${id}" />
    <div>
        <g:field type="button" style="width: 100px" class="btn btn-primary create" onclick="editFinal();"
                 name="btnOk" id="btnOk" value="${message(code: 'default.addJob.label', default: 'OK')}" />
        &nbsp;&nbsp;&nbsp;
        <g:field type="button" style="width: 100px" class="btn cancel" onclick="expandTableLayout();"
                 name="btnClose" id="btnClose" value="${message(code: 'default.addParts.label', default: 'Close')}" />
    </div>
</div>
</body>
</html>
