<%@ page import="com.kombos.baseapp.sec.shiro.User" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
<%--		<r:require modules="baseapp" />--%>
		<g:set var="entityName" value="${message(code: 'app.user.label', default: 'User')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
        <r:require modules="export"></r:require>
        <g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	var edit;
	var unlock;
	$(function(){ 
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/user/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/user/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
     unlock = function(id) {
    	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/user/update/'+id,
   			data: {unlock: id},
   			success:function(data,textStatus){
   				oTable.fnDraw();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
     };

    loadForm = function(data, textStatus){
		$('#user-form').empty();
    	$('#user-form').append(data);
   	}

    shrinkTableLayout = function(){
   		if($("#user-table").hasClass('span12')){
   			$("#user-table").toggleClass('span12 span5');
   		}
        $("#user-form").css("display","block");
       	shrinkUserTable();
   	}

   	expandTableLayout = function(){
   		if($("#user-table").hasClass('span5')){
   			$("#user-table").toggleClass('span5 span12');
   		}
        $("#user-form").css("display","none");
       	expandUserTable();
   	}

   	massDelete = function() {
   		var recordsToDelete = [];
		$("#user-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});

		var json = JSON.stringify(recordsToDelete);

		$.ajax({
    		url:'${request.contextPath}/user/massdelete',
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadUserTable();
    		}
		});

   	}

});
</g:javascript>
	</head>
	<body>
	<h3 class="navbar box-header no-border">
		<g:message code="default.list.label" args="[entityName]" />
		<ul class="nav pull-right">
			<li class="separator"></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</h3>

    <ul class="nav nav-tabs">
        <li class="active"><a href="#">User Profile</a></li>
        <li><a href="javascript:loadPath('role/');">User Role</a></li>
        <li><a href="javascript:loadPath('rolePermissions/list');">User Access</a></li>
        <li><a href="javascript:loadPath('userPrinter/');">User Printer</a></li>
        <li><a href="javascript:loadPath('namaForm/');">User Form Name</a></li>
    </ul>

	<div class="box">
		<div class="span12" id="user-table">
            <g:render template="../menu/maxLineDisplay"/>
            <script type="text/javascript">
                var shrinkUserTable;
                var reloadUserTable;
                var expandUserTable;
                var oTable;
                jQuery(function () {

                    $('#search_tanggalExpired').datepicker().on('changeDate', function(ev) {
                        var newDate = new Date(ev.date);
                        $('#search_tanggalExpired_day').val(newDate.getDate());
                        $('#search_tanggalExpired_month').val(newDate.getMonth()+1);
                        $('#search_tanggalExpired_year').val(newDate.getFullYear());
                        $(this).datepicker('hide');
                        oTable.fnDraw();
                    });

                    oTable = $('#userDatatables_datatable').dataTable({
                        "sScrollX": "100%",
                        //"sScrollXInner": "210%",
                        "bScrollCollapse": true,
                        "bAutoWidth" : false,
                        //"bScrollCollapse" : true,
                        "bPaginate" : true,
                        //"bSort" : false,
                        "sInfo" : "",
                        "sInfoEmpty" : "",
                        //"sDom": "<'row-fluid'r>t<'row-fluid'<'span6'i><p>>",
                        "sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>","bFilter": false,
                        "bStateSave": false,
                        'sPaginationType': 'bootstrap',
                        "fnInitComplete": function () {
                        },
                        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        },
                        "bSort": true,
                        "bProcessing": true,
                        "bServerSide": true,
                        "iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
                        "sAjaxSource": "${request.getContextPath()}/user/datatablesList",
                        "aoColumns": [
                                        {   "sName": "companyDealer",
                                            "mDataProp": "companyDealer",
                                            "mRender": function ( data, type, row ) {
                                                return '<input type="hidden" value="'+data+'">&nbsp;&nbsp;<a href="#" onclick="show('+"'"+row['username']+"'"+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+"'"+row['username']+"'"+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="resetPassword('+"'"+row['username']+"'"+');">&nbsp;Reset&nbsp;Password&nbsp;&nbsp;</a>';
                                            },
                                            "bSearchable": true,
                                            "bSortable": true,
                                            "sWidth":"120px",
                                            "bVisible": true
                                        },
                                        {   "sName": "divisi",
                                            "mDataProp": "divisi",
                                            "bSearchable": true,
                                            "bSortable": true,
                                            "sWidth":"120px",
                                            "sClass":'',
                                            "bVisible": true
                                        },
                                        {   "sName": "roles",
                                            "mDataProp": "roles",
                                            "bSearchable": true,
                                            "bSortable": true,
                                            "sWidth":"120px",

                                            "sClass":'',
                                            "bVisible": true
                                        },
                                        {   "sName": "t001NamaPegawai",
                                            "mDataProp": "namaPegawai",
                                            "bSearchable": true,
                                            "bSortable": true,
                                            "sWidth":"120px",
                                            "sClass":'',
                                            "bVisible": true
                                        },
                                        {   "sName": "username",
                                            "mDataProp": "username",
                                            "bSearchable": true,
                                            "sWidth": "120px",
                                            "bSortable": true,
                                            "sClass":'',
                                            "bVisible": true
                                        },
                                         {   "sName": "t001Inisial",
                                         	"mDataProp": "inisial",
                                             "bSearchable": true,
                                             "sWidth": "120px",
                                             "bSortable": true,
                                             "sClass":'',
                                             "bVisible": true
                                         },
                                         {   "sName": "t001ExpiredAccountDate",
                                         	"mDataProp": "tanggalExpired",
                                             "bSearchable": true,
                                             "sWidth": "120px",
                                             "bSortable": true,
                                             "sClass":'',
                                             "bVisible": true
                                         },
                        ],
                        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        	//$('.search_init').each(function(idx){
                        		//aoData.push( {"name": "sSearch_"+(idx+1), "value": this.value} );
                            //});

                            var tanggalExpired = $('#search_tanggalExpired').val();
                            var tanggalExpiredDay = $('#search_tanggalExpired_day').val();
                            var tanggalExpiredMonth = $('#search_tanggalExpired_month').val();
                            var tanggalExpiredYear = $('#search_tanggalExpired_year').val();

                            if(tanggalExpired){
                                aoData.push(
                                        {"name": 'sCriteria_tanggalExpired', "value": "date.struct"},
                                        {"name": 'sCriteria_tanggalExpired_dp', "value": tanggalExpired},
                                        {"name": 'sCriteria_tanggalExpired_day', "value": tanggalExpiredDay},
                                        {"name": 'sCriteria_tanggalExpired_month', "value": tanggalExpiredMonth},
                                        {"name": 'sCriteria_tanggalExpired_year', "value": tanggalExpiredYear}
                                );
                            }

                            var companyDealer = $('#filter_companyDealer input').val();
                            if(companyDealer){
                                aoData.push(
                                        {"name": 'sCriteria_companyDealer', "value": companyDealer}
                                );
                            }

                            var divisi = $('#filter_divisi input').val();
                            if(divisi){
                                aoData.push(
                                        {"name": 'sCriteria_divisi', "value": divisi}
                                );
                            }

                            var namaPegawai = $('#filter_namaPegawai input').val();
                            if(namaPegawai){
                                aoData.push(
                                        {"name": 'sCriteria_namaPegawai', "value": namaPegawai}
                                );
                            }

                            var username = $('#filter_username input').val();
                            if(username){
                                aoData.push(
                                        {"name": 'sCriteria_username', "value": username}
                                );
                            }

                            var inisial = $('#filter_inisial input').val();
                            if(inisial){
                                aoData.push(
                                        {"name": 'sCriteria_inisial', "value": inisial}
                                );
                            }

                            var roles = $('#filter_roles input').val();
                            if(roles){
                                aoData.push(
                                        {"name": 'sCriteria_roles', "value": roles}
                                );
                            }

                            $.ajax({ "dataType": 'json',
					        	"type": "POST",
					        	"url": sSource,
					        	"data": aoData ,
					        	"success": function (json) {
					        		fnCallback(json);
					        	},
					        	"complete": function () {

					        	}
					        });
                    	}
                    });

<%--                    $("#roleDatatables-filters tbody")--%>
<%--                            .append("<tr><td align=\"center\">role.id.label&nbsp;&nbsp;</td><td align=\"center\" id=\"filter_id\"> <input type=\"text\" name=\"search_id\" class=\"search_init\" size=\"10\"/></td></tr>")--%>
<%--                            .append("<tr><td align=\"center\">Name&nbsp;&nbsp;</td><td align=\"center\" id=\"filter_name\"> <input type=\"text\" name=\"search_name\" class=\"search_init\" size=\"10\"/></td></tr>")--%>
<%--                            .append("<tr><td align=\"center\">Label&nbsp;&nbsp;</td><td align=\"center\" id=\"filter_permissions\"> <input type=\"text\" name=\"search_permissions\" class=\"search_init\" size=\"10\"/></td></tr>")--%>
<%--                            .append("<tr><td align=\"center\">Users&nbsp;&nbsp;</td><td align=\"center\" id=\"filter_users\"> <input type=\"text\" name=\"search_users\" class=\"search_init\" size=\"10\"/></td></tr>")--%>
<%--                    ;--%>


                    shrinkUserTable = function(){
//                        oTable.fnSetColumnVis( 2, false );
//                        oTable.fnSetColumnVis( 3, false );
//                        oTable.fnSetColumnVis( 4, false );
//                        oTable.fnSetColumnVis( 5, false );
//                        oTable.fnSetColumnVis( 6, false );
//                        oTable.fnSetColumnVis( 7, false );
                    }

                    expandUserTable = function(){
//                        oTable.fnSetColumnVis( 2, true );
//                        oTable.fnSetColumnVis( 3, true );
//                        oTable.fnSetColumnVis( 4, true );
//                        oTable.fnSetColumnVis( 5, true );
//                        oTable.fnSetColumnVis( 6, true );
//                        oTable.fnSetColumnVis( 7, true );
                    }

                    reloadUserTable = function(){
                        oTable.fnReloadAjax();
                    }

                    $('.search_init').each(function(idx){
                        $(this).keypress(function(e){
                        	/* Filter on the column (the index) of this element */
	                        var code = (e.keyCode ? e.keyCode : e.which);
	    		            if (code == 13) { //Enter keycode
	    		                //oTable.fnFilter(this.value, idx+1);
	    		                oTable.fnDraw();
	    		                e.stopPropagation();
	    		            }
                        });
                    });

                    $('table .dropdown-menu li').click(function(){
                    	$(this).addClass('active').siblings().removeClass('active');
                    	$(this).closest('span').toggleClass('open');
                    });

                    $('#userDatatables_datatable th .select-all').click(function(e) {
                        var tc = $('#userDatatables_datatable tbody .row-select').attr('checked', this.checked);

                        if(this.checked)
                            tc.parent().parent().addClass('row_selected');
                        else
                            tc.parent().parent().removeClass('row_selected');
                        e.stopPropagation();
                    });

                    $('#userDatatables_datatable tbody tr .row-select').live('click', function (e) {
                        if(this.checked)
                            $(this).parent().parent().addClass('row_selected');
                        else
                            $(this).parent().parent().removeClass('row_selected');
                        e.stopPropagation();

                    } );

                    $('#userDatatables_datatable tbody tr a').live('click', function (e) {
                        e.stopPropagation();
                    } );

                    $('#userDatatables_datatable tbody td').live('click', function(e) {
                        if (e.target.tagName.toUpperCase() != "INPUT") {
                            var $tc = $(this).parent().find('input:checkbox'),
                                    tv = $tc.attr('checked');
                            $tc.attr('checked', !tv);
                            $(this).parent().toggleClass('row_selected');
                        }
                    });

                });

                function resetPassword(userid){
                    jQuery.ajax({
                        type: 'POST',
                        url: '${request.getContextPath()}/user/resetPassword',
                        data: {
                            username : userid
                        },
                        async: false,
                        success: function(data) {
                            jQuery("#password_reset_info").html(data.message);
                        },
                        error: function(){
                            alert('Server Response Error, Cannot Get User Permission');
                        }
                    });
                }

                $("#filter_companyDealer input").click(function(e)
                {
                    e.stopPropagation();
                });

                $("#filter_divisi input").click(function(e)
                {
                    e.stopPropagation();
                });

                $("#filter_namaPegawai input").click(function(e)
                {
                    e.stopPropagation();
                });

                $("#filter_username input").click(function(e)
                {
                    e.stopPropagation();
                });

                $("#filter_inisial input").click(function(e)
                {
                    e.stopPropagation();
                });

                $("#filter_tanggalExpired input").click(function(e)
                {
                    e.stopPropagation();
                });

                $("#filter_roles input").click(function(e)
                {
                    e.stopPropagation();
                });

            </script>
            <div id="password_reset_info"></div>
            <table id="userDatatables_datatable" cellpadding="0" cellspacing="0"
                   border="0"
                   class="display table table-striped table-bordered table-hover table-selectable"
                   width="100%">
                <thead>
                <tr>

                    <th style="border-bottom: none;padding: 5px;">
                        <g:message code="app.user.companyDealer.label" default="Workshop / Office"/>
                    </th>
                    <th style="border-bottom: none;padding: 5px;">
                        <g:message code="app.user.divisi.label" default="Divisi"/>
                    </th>
                    <th style="border-bottom: none;padding: 5px;">
                        <g:message code="app.user.roles.label" default="Role"/>
                    </th>
                    <th style="border-bottom: none;padding: 5px;">
                        <g:message code="app.user.namaPegawai.label" default="Nama Pegawai"/>
                    </th>
                    <th style="border-bottom: none;padding: 5px;">
                        <g:message code="app.user.username.label" default="Username"/>
                    </th>
                    <th style="border-bottom: none;padding: 5px;">
                        <g:message code="app.user.inisial.label" default="Inisial"/>
                    </th>
                    <th style="border-bottom: none;padding: 5px;">
                        <g:message code="app.user.tanggalExpired.label" default="Tanggal Expired"/>
                    </th>
                </tr>
                <tr>
                    <th style="border-top: none;padding: 5px;">
                        <div id="filter_companyDealer" style="margin-top: 38px;">
                            <input type="text" name="search_companyDealer" class="search_init" />
                        </div>
                    </th>
                    <th style="border-top: none;padding: 5px;">
                        <div id="filter_divisi" style="margin-top: 38px;">
                            <input type="text" name="search_divisi" class="search_init" />
                        </div>
                    </th>
                    <th style="border-top: none;padding: 5px;">
                        <div id="filter_roles" style="margin-top: 38px;">
                            <input type="text" name="search_roles" class="search_init" />
                        </div>
                    </th>
                    <th style="border-top: none;padding: 5px;">
                        <div id="filter_namaPegawai" style="margin-top: 38px;">
                            <input type="text" name="search_namaPegawai" class="search_init" />
                        </div>
                    </th>
                    <th style="border-top: none;padding: 5px;">
                        <div id="filter_username" style="margin-top: 38px;">
                            <input type="text" name="search_username" class="search_init" />
                        </div>
                    </th>
                    <th style="border-top: none;padding: 5px;">
                        <div id="filter_inisial" style="margin-top: 38px;">
                            <input type="text" name="search_inisial" class="search_init" />
                        </div>
                    </th>
                    <th style="border-top: none;padding: 5px;">
                        <div id="filter_tanggalExpired" style="margin-top: 38px;">
                            <input type="hidden" name="search_tanggalExpired" value="date.struct">
                            <input type="hidden" name="search_tanggalExpired_day" id="search_tanggalExpired_day" value="">
                            <input type="hidden" name="search_tanggalExpired_month" id="search_tanggalExpired_month" value="">
                            <input type="hidden" name="search_tanggalExpired_year" id="search_tanggalExpired_year" value="">
                            <input type="text" data-date-format="dd/mm/yyyy" name="search_tanggalExpired_dp" value="" id="search_tanggalExpired" class="search_init">
                        </div>
                    </th>
                </tr>
            </thead>
        </table>
    </div>
		<div class="span7" id="user-form" style="display: none;"></div>
	</div>
</body>
</html>
