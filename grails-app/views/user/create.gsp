<%@ page import="com.kombos.baseapp.sec.shiro.User" %>
<%@ page import="com.kombos.baseapp.ApprovalStatus" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'app.user.label', default: 'User')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="create-user" class="content scaffold-create" role="main">
			<legend><g:message code="default.create.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${userInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${userInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form action="save" >--}%
			<form id="userForm" class="form-horizontal" action="${request.getContextPath()}/user/save" method="post" onsubmit="submitForm();return false;">
        
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons controls">
                    <a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout();"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
                    %{--<g:submitButton class="btn btn-primary create" name="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />--}%
					%{--<g:submitToRemote class="btn btn-primary create" name="draft" value="Save as draft" onSuccess="reloadUserTable();" update="user-form" url="[controller: 'user', action:'save']"/>--}%
<%--					<g:submitToRemote class="btn btn-primary create" name="save" value="Save" onSuccess="reloadUserTable();" update="user-form" url="[controller: 'user', action:'save']" before="jQuery('#create').validate();jQuery('#approvalStatus').val('${ApprovalStatus.PENDING}');"/>--%>
					<g:submitButton class="btn btn-primary create" name="save"
								value="Save" />		
				</fieldset>
				<g:hiddenField name="taskId" value="${params.taskId}" />
				<g:hiddenField name="approvalStatus" value="${ApprovalStatus.DRAFT}" />
			</form>
			%{--</g:form>--}%
			<g:javascript> 			
 			var submitForm;
 			var saveSuccess;
			var needApproval;
 			$(function(){ 
 			saveSuccess = function(){
		   		toastr.success("<div>The data is successfully added.</div>");
		   		reloadUserTable();
		   		expandTableLayout();
		   	};
		   	
		   	needApproval = function(){
		   		toastr.success("<div>The data is requested for approval.</div>");
		   		reloadUserTable();
		   		expandTableLayout();
		   	};
 			submitForm = function(){
 			   
 			if(jQuery('#userForm').valid()){
				jQuery.ajax({
					type:'POST',
					data:jQuery('#userForm').serialize(), 
					url:'${request.getContextPath()}/user/save',
					success:function(data,textStatus){
						if(data == 'Data saved') {
							saveSuccess();
						}else if(data == 'Your request will be submitted to request for approval'){
							//needApproval();
							loadForm(data);reloadUserTable();
						}else{
							loadForm(data);reloadUserTable();
						}
					},
					error:function(XMLHttpRequest,textStatus,errorThrown){}});
			}
			return false;
 			};
 			
 			$.validator.addMethod("uniqueUserName", function(value, element) {
 			  var isSuccess = false;
			  $.ajax({
			      type: "POST",
			      url: "${request.getContextPath()}/user/check/"+value,
			      async: false, 
			      dataType:"html",
			   success: function(msg)
			   {
			      // if the user exists, it returns a string "true"
			      if(msg == "found")
			         isSuccess = false;  // already exists
			      else
			      	isSuccess = true;      // username is free to use
			   }
			 })
			 return isSuccess;
			 }, "Username is Already Taken");
			
			 $("#userForm").validate({
				  rules: {				  
						username: "uniqueUserName"
				  },
				  submitHandler: function(form) {
					    return true;
				  },
				  onfocusout: false,
				  onkeyup: false,
				  onclick: false  				  
				});
			
			});	
			</g:javascript>
		</div>
	</body>
</html>
