<%@ page import="org.apache.commons.codec.binary.Base64; com.kombos.baseapp.sec.shiro.User" %>
<%@ page import="com.kombos.baseapp.ApprovalStatus" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'app.user.label', default: 'User')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteUser;

$(function(){ 
	deleteUser=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/user/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadUserTable();
   				expandTableLayout();
   				if(data == 'Your request will be submitted to request for approval')
   				toastr.success("<div>Your request will be submitted to request for approval</div>");
   				else
   				toastr.success("<div>User is successfully deleted.</div>");
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-user" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
            <g:if test="${flash.message.toString().toLowerCase().contains("password")}">
                <div class="message" style="color: red;font-size: 19px" role="status">${flash.message}</div>
            </g:if>
            <g:else>
                <div class="message" role="status">${flash.message}</div>
            </g:else>
            <br/>
		</g:if>

        %{--start here--}%
        <table id="user"
               class="table table-bordered table-hover">
            <tbody>

            <g:if test="${userInstance?.companyDealer}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="companyDealer-label" class="property-label"><g:message
                                code="app.user.companyDealer.label" default="Workshop / Office" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="companyDealer-label">
                        ${userInstance?.companyDealer.m011NamaWorkshop}
                    </span></td>

                </tr>
            </g:if>

            <g:if test="${userInstance?.divisi}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="divisi-label" class="property-label"><g:message
                                code="app.user.divisi.label" default="Divisi" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="divisi-label">
                        ${userInstance?.divisi.m012NamaDivisi}
                    </span></td>

                </tr>
            </g:if>

            <g:if test="${userInstance?.roles}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="roles-label" class="property-label"><g:message
                                code="app.user.roles.label" default="Roles" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="roles-label">
                        ${userInstance?.roles}
                    </span></td>

                </tr>
            </g:if>

            <g:if test="${userInstance?.t001NamaPegawai}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t001NamaPegawai-label" class="property-label"><g:message
                                code="app.user.t001NamaPegawai.label" default="Nama Pegawai" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t001NamaPegawai-label">
                        ${userInstance?.t001NamaPegawai}
                    </span></td>
                </tr>
            </g:if>

            <g:if test="${userInstance?.username}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="username-label" class="property-label"><g:message
                                code="app.user.username.label" default="Username" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="username-label">
                        ${userInstance?.username}
                    </span></td>

                </tr>
            </g:if>

            <g:if test="${userInstance?.effectiveStartDate}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t001ExpiredAccountDate-label" class="property-label"><g:message
                                code="user.t001ExpiredAccountDate.label" default="Tanggal Berlaku" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="effectiveStartDate-label">
                        ${effectiveStartDate}
                    </span></td>

                </tr>
            </g:if>

            <g:if test="${userInstance.t001ExpiredAccountDate}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="effectiveEndDate-label" class="property-label"><g:message
                                code="app.user.expired_date.label" default="Tanggal Expired" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="effectiveEndDate-label">
                        ${userInstance?.t001ExpiredAccountDate.format("dd/MM/yyyy")}
                    </span></td>

                </tr>
            </g:if>

            <g:if test="${userInstance?.t001Inisial}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t001Inisial-label" class="property-label"><g:message
                                code="app.user.t001Inisial.label" default="Inisial" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t001Inisial-label">
                        ${userInstance?.t001Inisial}
                    </span></td>

                </tr>
            </g:if>

            <g:if test="${userInstance?.t001Email}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t001Email-label" class="property-label"><g:message
                                code="app.user.t001Email.label" default="Email" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t001Email-label">
                        ${userInstance?.t001Email}
                    </span></td>

                </tr>
            </g:if>

            <g:if test="${userInstance?.lastLoggedIn}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastLoggedIn-label" class="property-label"><g:message
                                code="app.user.lastLoggedIn.label" default="Last LoggedIn" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastLoggedIn-label">
                        ${userInstance?.lastLoggedIn}
                    </span></td>

                </tr>
            </g:if>

            <g:if test="${userInstance?.canPasswordExpire}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="canPasswordExpire-label" class="property-label"><g:message
                                code="app.user.password_can_expire.label" default="Can Password Expire" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="canPasswordExpire-label">
                        ${userInstance?.canPasswordExpire}
                    </span></td>

                </tr>
            </g:if>

            <g:if test="${userInstance?.t001Foto}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="t001Foto-label" class="property-label"><g:message
                                code="app.user.t001Foto.label" default="Profile Image" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="t001Foto-label">
                <img width="250" id="t001Foto" name="t001Foto" src="data:jpg;base64,${new String(new Base64().encode(new File(userInstance.t001Foto).bytes), "UTF-8")}" />
            </span></td>
            </tr>
            </g:if>

            <g:if test="${userInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="user.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        <g:formatDate date="${userInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${userInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="user.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        <g:fieldValue field="createdBy" bean="${userInstance}" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${userInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdate-label" class="property-label"><g:message
                                code="user.lastUpdate.label" default="Last Update" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdate-label">
                        <g:formatDate date="${userInstance?.lastUpdated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${userInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="user.updatedBy.label" default="Update By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        <g:fieldValue field="updatedBy" bean="${userInstance}" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${userInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="user.lastUpdProcess.label" default="last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        <g:fieldValue field="lastUpdProcess" bean="${userInstance}" />

                    </span></td>

                </tr>
            </g:if>

            </tbody>
        </table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>	
				%{--<ba:confirm id="delete" class="btn cancel"--}%
					%{--message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"--}%
					%{--onsuccess="deleteUser('${userInstance?.username}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>--}%
				<g:if test="${params.approvalStatus == ApprovalStatus.DRAFT.toString() && params.taskId}">	
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${userInstance?.id}"
					update="[success:'user-form',failure:'user-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink> 
				
				</g:if>
				<g:elseif test="${params.approvalStatus == ApprovalStatus.PENDING.toString() && params.taskId}">
				<g:submitToRemote class="btn btn-primary edit" name="approve" value="Approve" update="user-form" url="[controller: 'user', action:'performApprove']"/>
				<g:hiddenField name="taskId" value="${params.taskId}" />
				<g:hiddenField name="approvalId" value="${params.approvalId}" />
				</g:elseif>				
			</fieldset>
		</g:form>
		
	</div>
</body>
</html>
