<%@ page import="java.text.SimpleDateFormat; com.kombos.baseapp.sec.shiro.User" %>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'username', 'error')} required">
    <label class="control-label" for="username">
        <g:message code="app.user.username.label" default="Username" />
    </label>
    <div class="controls">
        <g:textField name="username" readonly="readonly" value="${userInstance?.username}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'staffNo', 'error')} required">
    <label class="control-label" for="staffNo">
        <g:message code="app.user.staff_no.label" default="Staff No" />
    </label>
    <div class="controls">
        <g:textField name="staffNo" required="" value="${userInstance?.staffNo}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'firstName', 'error')} ">
    <label class="control-label" for="firstName">
        <g:message code="app.user.first_name.label" default="First Name" />
    </label>
    <div class="controls">
        <g:textField name="firstName" value="${userInstance?.firstName}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'lastName', 'error')} ">
    <label class="control-label" for="lastName">
        <g:message code="app.user.last_name.label" default="Last Name" />
    </label>
    <div class="controls">
        <g:textField name="lastName" value="${userInstance?.lastName}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'effectiveStartDate', 'error')} required">
    <label class="control-label" for="effectiveStartDate">
        <g:message code="app.user.effective_start_date.label" default="Effective Start Date" />
    </label>
    <div class="controls">
        %{--<g:datePicker name="effectiveStartDate" precision="day"  value="${userInstance?.effectiveStartDate}" default="none" noSelection="['': '']" />--}%
        <%
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        %>
        <input type="text" id="effectiveStartDate" name="effectiveStartDate" value="<%=userInstance?.effectiveStartDate?sdf.format(userInstance?.effectiveStartDate):""%>" data-date-format="dd/mm/yyyy">
        
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'effectiveEndDate', 'error')} required">
    <label class="control-label" for="effectiveEndDate">
        <g:message code="app.user.effective_end_date.label" default="Effective End Date" />
    </label>
    <div class="controls">
        <input type="text" id="effectiveEndDate" name="effectiveEndDate" value="<%=userInstance?.effectiveEndDate?sdf.format(userInstance?.effectiveEndDate):""%>" data-date-format="dd/mm/yyyy">
        
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'roles', 'error')} required">
    <label class="control-label" for="roles">
        <g:message code="app.user.roles.label" default="Roles" />
    </label>
    <div class="controls">
<%--        <g:select name="roles" from="${com.kombos.baseapp.sec.shiro.Role.list()}" multiple="multiple" optionKey="id" size="5" value="${userInstance?.roless}" class="many-to-many" style="display: none;"/>--%>
        <g:if test="${userInstance?.roless instanceof String}">
        <select id="roles" class="selectpicker valid" name="roles" style="display: none;">
			    <g:each in="${com.kombos.baseapp.sec.shiro.Role.list()}" status="i" var="cc">
			    <g:if test="${cc.id.toString() == userInstance?.roless?.toString()}"><option value="${cc.id}" selected="selected">${cc.toString()}</option></g:if>
			    <g:else><option value="${cc.id}">${cc.toString()}</option></g:else>
			    </g:each>
		</select>
        </g:if>
        <g:else>
         <select id="roles" class="many-to-many" size="${userInstance?.roless?.size()}" multiple="multiple" name="roles" style="display: none;">
			    <g:each in="${com.kombos.baseapp.sec.shiro.Role.list()}" status="i" var="cc">
			    <g:if test="${userInstance?.roless?.contains(cc.id)}"><option value="${cc.id}" selected="selected">${cc.toString()}</option></g:if>
			    <g:else><option value="${cc.id}">${cc.toString()}</option></g:else>
			    </g:each>
		</select>
		</g:else>
        <g:if test="${userInstance?.roless instanceof String}">
        <select id="_roles_" class="selectpicker valid" name="_roles_" disabled="disabled">
			    <g:each in="${com.kombos.baseapp.sec.shiro.Role.list()}" status="i" var="cc">
			    <g:if test="${cc.id.toString() == userInstance?.roless?.toString()}"><option value="${cc.id}" selected="selected">${cc.toString()}</option></g:if>
			    </g:each>
		</select>
        </g:if>
        <g:else>
         <select id="_roles_" class="many-to-many" size="${userInstance?.roless?.size()}" multiple="multiple" name="_roles_" disabled="disabled">
			    <g:each in="${com.kombos.baseapp.sec.shiro.Role.list()}" status="i" var="cc">
			    <g:if test="${userInstance?.roless?.contains(cc.id)}"><option value="${cc.id}">${cc.toString()}</option></g:if>
			    </g:each>
		</select>
		</g:else>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'groupLocalBranchId', 'error')} required">
    <label class="control-label" for="groupLocalBranchId">
        <g:message code="app.user.group_local_branch.label" default="Group Local Branch" />
    </label>
    <div class="controls">
<%--        <g:select name="groupLocalBranchId" from="${com.kombos.baseapp.GroupLocalBranch?.list()}" optionKey="pkid" size="1" value="${userInstance?.groupLocalBranchId}" default="none" noSelection="['': '']" style="display: none;"/>--%>
        <select id="groupLocalBranchId" class="selectpicker valid" name="groupLocalBranchId" style="display: none;">
			    <g:each in="${com.kombos.baseapp.GroupLocalBranch?.list()}" status="i" var="cc">
			    <g:if test="${cc.pkid.toString() == userInstance?.groupLocalBranchId?.toString()}"><option value="${cc.pkid}" selected="selected">${cc.toString()}</option></g:if>
			    <g:else><option value="${cc.pkid}">${cc.toString()}</option></g:else>
			    </g:each>
		</select>
        <select id="_groupLocalBranchId_" class="selectpicker valid" name="_groupLocalBranchId_" disabled="disabled">
			    <g:each in="${com.kombos.baseapp.GroupLocalBranch?.list()}" status="i" var="cc">
			    <g:if test="${cc.pkid.toString() == userInstance?.groupLocalBranchId?.toString()}"><option value="${cc.pkid}" selected="selected">${cc.toString()}</option></g:if>
			    </g:each>
		</select>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'groupFormId', 'error')} required">
    <label class="control-label" for="groupFormId">
        <g:message code="app.user.group_form.label" default="Group Form" />
    </label>
    <div class="controls">
<%--        <g:select name="groupFormId" from="${com.kombos.baseapp.GroupForm?.list()}" optionKey="pkid" size="1" value="${userInstance?.groupFormId}" default="none" noSelection="['': '']" style="display: none;"/>--%>
         <select id="groupFormId" class="selectpicker valid" name="groupFormId" style="display: none;">
			    <g:each in="${com.kombos.baseapp.GroupForm?.list()}" status="i" var="cc">
			    <g:if test="${cc.pkid.toString() == userInstance?.groupFormId?.toString()}"><option value="${cc.pkid}" selected="selected">${cc.toString()}</option></g:if>
			    <g:else><option value="${cc.pkid}">${cc.toString()}</option></g:else>
			    </g:each>
		</select>
         <select id="_groupFormId_" class="selectpicker valid" name="_groupFormId_" disabled="disabled">
			    <g:each in="${com.kombos.baseapp.GroupForm?.list()}" status="i" var="cc">
			    <g:if test="${cc.pkid.toString() == userInstance?.groupFormId?.toString()}"><option value="${cc.pkid}" selected="selected">${cc.toString()}</option></g:if>
			    </g:each>
		</select>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'reportTo', 'error')} required">
    <label class="control-label" for="reportTo">
        <g:message code="app.user.report_to.label" default="Report To" /> 
    </label>
    <div class="controls">
<%--        <g:select name="reportTo" from="${com.kombos.baseapp.sec.shiro.User.findAllByIsActive(true)}" optionKey="username" size="5" value="${userInstance?.reportToes}" style="display: none;"/>--%>
        <g:if test="${userInstance?.reportToes instanceof String}">
        <select id="reportTo" class="selectpicker valid" name="reportTo" style="display: none;">
			    <g:each in="${com.kombos.baseapp.sec.shiro.User.findAllByIsActive(true)}" status="i" var="cc">
			    <g:if test="${cc.username.toString() == userInstance?.reportToes?.toString()}"><option value="${cc.username}" selected="selected">${cc.toString()}</option></g:if>
			    <g:else><option value="${cc.username}">${cc.toString()}</option></g:else>
			    </g:each>
		</select>
        </g:if>
        <g:else>
        <select id="reportTo" class="many-to-many" size="${userInstance?.reportToes?.size()}" multiple="multiple" name="reportTo" style="display: none;">
			    <g:each in="${com.kombos.baseapp.sec.shiro.User.findAllByIsActive(true)}" status="i" var="cc">
			    <g:if test="${userInstance?.reportToes?.contains(cc.username)}"><option value="${cc.username}" selected="selected">${cc.toString()}</option></g:if>
			    <g:else><option value="${cc.username}">${cc.toString()}</option></g:else>
			    </g:each>
		</select>
		</g:else>
        <g:if test="${userInstance?.reportToes instanceof String}">
        <select id="_reportTo_" class="selectpicker valid" name="_reportTo_" disabled="disabled">
			    <g:each in="${com.kombos.baseapp.sec.shiro.User.findAllByIsActive(true)}" status="i" var="cc">
			    <g:if test="${cc.username.toString() == userInstance?.reportToes?.toString()}"><option value="${cc.username}" selected="selected">${cc.toString()}</option></g:if>
			    </g:each>
		</select>
        </g:if>
        <g:else>
        <select id="_reportTo_" class="many-to-many" size="${userInstance?.reportToes?.size()}" multiple="multiple" name="_reportTo_" disabled="disabled">
			    <g:each in="${com.kombos.baseapp.sec.shiro.User.findAllByIsActive(true)}" status="i" var="cc">
			    <g:if test="${userInstance?.reportToes?.contains(cc.username)}"><option value="${cc.username}">${cc.toString()}</option></g:if>
			    </g:each>
		</select>
		</g:else>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'canPasswordExpire', 'error')} ">
    <label class="control-label" for="canPasswordExpire">
        <g:message code="app.user.password_can_expire.label" default="Can Password Expire" />
    </label>
    <div class="controls">
        <g:checkBox name="canPasswordExpire" value="${userInstance?.canPasswordExpire}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'accountLocked', 'error')} ">
    <label class="control-label" for="accountLocked">
        <g:message code="app.user.account_locked.label" default="Locked" />
    </label>
    <div class="controls">
        <g:checkBox name="accountLocked" value="${userInstance?.accountLocked}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'isActive', 'error')} ">
    <label class="control-label" for="isActive">
        <g:message code="app.user.is_active.label" default="Active" />
    </label>
    <div class="controls">
        <g:checkBox name="isActive" value="${userInstance?.isActive}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'country', 'error')} ">
    <label class="control-label" for="country">
        <g:message code="app.user.country.label" default="Country" />
    </label>
    <div class="controls">
        <select id="country" class="selectpicker valid" name="country" style="display: none;">
			    <g:if test="${userInstance?.country == 'in'}"><option value="in" selected="selected">${message(code: 'app.login.language.in')}</option></g:if>
			    <g:elseif test="${userInstance?.country == 'en'}"><option value="en" selected="selected">${message(code: 'app.login.language.en')}</option></g:elseif>
		</select>
        <select id="_country_" class="selectpicker valid" name="_country_" disabled="disabled">
			    <g:if test="${userInstance?.country == 'in'}"><option value="in" selected="selected">${message(code: 'app.login.language.in')}</option></g:if>
			    <g:elseif test="${userInstance?.country == 'en'}"><option value="en" selected="selected">${message(code: 'app.login.language.en')}</option></g:elseif>
		</select>
    </div>
</div>

