<%--
  Created by IntelliJ IDEA.
  User: kombos
  Date: 2/11/13
  Time: 5:45 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>

<html>
    <head>
        <script type="text/javascript">
            function changePassword(){
                var old_password = jQuery('#old_password').val();
                var new_password = jQuery('#new_password').val();
                var confirm_new_password = jQuery('#confirm_new_password').val();

                jQuery.ajax({
                    type: 'POST',
                    url: '${request.getContextPath()}/user/editChangePassword',
                    data: {
                        oldPassword : old_password,
                        newPassword : new_password,
                        confirmNewPassword : confirm_new_password
                    },
                    async: false,
                    success: function(data) {
                        alert(data.message);
                        if(data.statusRelogin){
                            window.location.replace("${request.getContextPath()}/auth/login");
                        }
                    },
                    error: function(){
                        alert('Server Response Error');
                    }
                });
            }
        </script>
    </head>

    <body>
        %{--<table>--}%
            %{--<tbody>--}%
            %{--<tr>--}%
                %{--<td><g:message code="app.login.oldpassword.label" default="Old Password"/>:</td>--}%
                %{--<td><input id="old_password" type="password" name="password"/></td>--}%
            %{--</tr>--}%
            %{--<tr>--}%
                %{--<td><g:message code="app.login.newpassword.label" default="New Password"/>:</td>--}%
                %{--<td><input id="new_password" type="password" name="password"/></td>--}%
            %{--</tr>--}%
            %{--<tr>--}%
                %{--<td><g:message code="app.login.confirmnewpassword.label" default="Retype New Password"/>:</td>--}%
                %{--<td><input id="confirm_new_password" type="password" name="password"/></td>--}%
            %{--</tr>--}%
            %{--<tr>--}%
                %{--<td />--}%
                %{--<td>--}%
                    %{--<button type="button" onclick="changePassword()"><g:message code="app.login.button.changepassword.label" default="Change Password"/></button>--}%
                %{--</td>--}%
            %{--</tr>--}%
            %{--</tbody>--}%
        %{--</table>--}%
    <h3 class="navbar box-header no-border">
        <g:message code="app.changePassword.label"	default="Change Password" />
    </h3>
    <div class="box">
        <div id="changePasswordDiv" class="app-setting-div">
            <form class="form-horizontal" method="post">
                <fieldset class="form">
                    <div class="control-group fieldcontain">
                        <label for="old_password" class="control-label"><g:message code="app.login.oldpassword.label" default="Old Password"/>:</label>
                        <div class="controls">
                            <input id="old_password" type="password" name="old_password"/>
                        </div>
                    </div>
                    <div class="control-group fieldcontain  ">
                        <label for="new_password" class="control-label"><g:message code="app.login.newpassword.label" default="New Password"/>:</label>
                        <div class="controls">
                            <input id="new_password" type="password" name="new_password"/>
                        </div>
                    </div>
                    <div class="control-group fieldcontain  ">
                        <label for="confirm_new_password" class="control-label"><g:message code="app.login.confirmnewpassword.label" default="Confirm Password"/>:</label>
                        <div class="controls">
                            <input id="confirm_new_password" type="password" name="confirm_new_password"/>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="buttons controls">
                    <input type="button" value="<g:message code="app.login.button.changepassword.label" default="Change Password"/>" class="btn btn-primary save" onclick="javascript:changePassword();">
                </fieldset>
            </form>
        </div>
    </div>
    </body>
</html>
