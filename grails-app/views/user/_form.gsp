<%@ page import="org.apache.commons.codec.binary.Base64; java.text.SimpleDateFormat; com.kombos.baseapp.sec.shiro.User;com.kombos.administrasi.CompanyDealer;" %>

<script language="Javascript">
    function fileUpload(form, action_url, div_id) {
        if($('#myFile').val()=="" || $('#myFile').val()==null){
            alert('Anda belum memilih image yang akan di upload');
        }else{
//            Create the iframe...
            var iframe = document.createElement("iframe");
            iframe.setAttribute("id", "upload_iframe");
            iframe.setAttribute("name", "upload_iframe");
            iframe.setAttribute("width", "0");
            iframe.setAttribute("height", "0");
            iframe.setAttribute("border", "0");
            iframe.setAttribute("style", "width: 0; height: 0; border: none;");

            // Add to document...
            form.parentNode.appendChild(iframe);
            window.frames['upload_iframe'].name = "upload_iframe";

            iframeId = document.getElementById("upload_iframe");

            // Add event...
            var eventHandler = function () {

                if (iframeId.detachEvent) iframeId.detachEvent("onload", eventHandler);
                else iframeId.removeEventListener("load", eventHandler, false);

                // Message from server...
                if (iframeId.contentDocument) {
                    content = iframeId.contentDocument.body.innerHTML;
                } else if (iframeId.contentWindow) {
                    content = iframeId.contentWindow.document.body.innerHTML;
                } else if (iframeId.document) {
                    content = iframeId.document.body.innerHTML;
                }

                document.getElementById(div_id).innerHTML = content;

                // Del the iframe...
                setTimeout('iframeId.parentNode.removeChild(iframeId)', 250);
            }

            if (iframeId.addEventListener) iframeId.addEventListener("load", eventHandler, true);
            if (iframeId.attachEvent) iframeId.attachEvent("onload", eventHandler);

            // Set properties of form...
            form.setAttribute("target", "upload_iframe");
            form.setAttribute("action", action_url);
            form.setAttribute("method", "post");
            form.setAttribute("enctype", "multipart/form-data");
            form.setAttribute("encoding", "multipart/form-data");

            // Submit the form...
            form.submit();

            document.getElementById(div_id).innerHTML = "Uploading...";
        }
    }

    function loadDivision(idInit)
    {
        var root="${resource()}";
        var companyDealerId=document.getElementById('companyDealer').value;
        var username=document.getElementById('username').value;

        if(idInit){
            companyDealerId = idInit;
//            username = "";
        }

        var url = root+'/user/findDivisionByCompanyDealer?companyDealer.id='+companyDealerId+'&username='+username;

        jQuery('#divisi').load(url);

        var url1 = root+'/user/findRoleByCompanyDealer?companyDealer.id='+companyDealerId+'&username='+username;

        jQuery('#roles').load(url1);
    }

    $(function() {
        %{
            if(request.getRequestURI().contains("edit")){
        }%
        loadDivision(${userInstance?.companyDealer?.id});
        %{
            }else{
        }%
        $("#companyDealer").val($("#target option:first").val());
        loadDivision(${userInstance?.companyDealer?.id});
        %{
            }
        }%

        $('#t001NamaPegawai').typeahead({
            source: function (query, process) {
                return $.get('${request.contextPath}/user/listManPower', { query: query }, function (data) {
                    return process(data.options);
                });
            }
        });

        cariData = function(){
            var aoData =  new Array();
            $.ajax({
                url:'${request.contextPath}/user/listManPower',
                type: "POST", // Always use POST when deleting data
                data: { query: $("#listManPower").val()},
                success: function(data) {
                    aoData.push(
                        {"name": nama, "value": data.options},
                        {"name": id, "value": data.ids}
                    );
                }
            });
            return aoData
        }
    });
    function cekInput(evt){
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode ==8 || charCode ==20 || charCode ==13 || charCode ==190 || charCode ==173 || (charCode ==173 && evt.shiftKey) || charCode ==20 || charCode > 36 && charCode < 41 || charCode > 45 && charCode < 58 || charCode > 64 && charCode < 91)
            return true;
        return false;
    }
</script>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'companyDealer', 'error')} required">
    <label class="control-label" for="companyDealer">
        <g:message code="app.user.companyDealer.label" default="Workshop / Office" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select required="" name="companyDealer.id" id="companyDealer" noSelection="['-1':'Pilih Workshop']" from="${CompanyDealer.createCriteria().list{eq("staDel","0");order("m011NamaWorkshop")}}" value="${userInstance?.companyDealer?.id}" optionKey="id" onchange='loadDivision();'/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'divisi', 'error')} required">
    <label class="control-label" for="divisi">
        <g:message code="app.user.divisi.label" default="Divisi" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="divisi" name="divisi.id" from="${[]}" noSelection="['-1':'Pilih Divisi']" value="${userInstance?.divisi?.id}" optionKey="id" required=""/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'roles', 'error')} required">
    <label class="control-label" for="roles">
        <g:message code="app.user.roles.label" default="Roles" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select name="roles" id="roles" from="${com.kombos.baseapp.sec.shiro.Role.createCriteria().list{eq("staDel","0");order("name")}}" multiple="multiple" optionKey="id" size="5" value="${userInstance?.roles*.id}"  required="" class="many-to-many"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 't001NamaPegawai', 'error')} ">
    <label class="control-label" for="t001NamaPegawai">
        <g:message code="app.user.t001NamaPegawai.label" default="Nama Pegawai" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="t001NamaPegawai" id="t001NamaPegawai" value="${userInstance?.t001NamaPegawai}" maxlength="20" required="" class="typeahead" autocomplete="off"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'username', 'error')} required">
    <label class="control-label" for="username">
        <g:message code="app.user.username.label" default="Username" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:if test="${userInstance?.username}">
            <g:textField name="username" id="username" required="" readonly="" maxlength="20" value="${userInstance?.username}"/>
        </g:if>
        <g:else>
            <g:textField name="username" id="username" required="" onkeydown="return cekInput(event);" maxlength="20" value="${userInstance?.username}"/>
        </g:else>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 't001ExpiredAccountDate', 'error')} required">
    <label class="control-label" for="effectiveStartDate">
        <g:message code="app.user.effectiveStartDate.label" default="Tanggal Berlaku" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <ba:datePicker required="true" name="effectiveStartDate" value="${userInstance?.effectiveStartDate}" precision="day" format="dd-MM-yyyy"/>
        %{--<%--}%
            %{--SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");--}%
        %{--%>--}%
        %{--<input type="text" id="effectiveStartDate" name="effectiveStartDate" value="<%=userInstance?.effectiveStartDate==null?"":sdf.format(userInstance?.effectiveStartDate)%>" data-date-format="dd/mm/yyyy">--}%
        %{--<g:javascript>--}%
            %{--$('#effectiveStartDate').datepicker();--}%
        %{--</g:javascript>--}%
    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 'canPasswordExpire', 'error')} ">
    <label class="control-label" for="canPasswordExpire">
        <g:message code="app.user.password_can_expire.label" default="Can Password Expire" />
    </label>
    <div class="controls">
        <g:checkBox name="canPasswordExpire" value="${userInstance?.canPasswordExpire}" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 't001Inisial', 'error')} ">
    <label class="control-label" for="t001Inisial">
        <g:message code="app.user.t001Inisial.label" default="Inisial" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="t001Inisial" id="t001Inisial" required="" value="${userInstance?.t001Inisial}" maxlength="4"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 't001Email', 'error')} ">
    <label class="control-label" for="t001Email">
        <g:message code="app.user.t001Email.label" default="Email" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="t001Email" id="t001Email" required="" value="${userInstance?.t001Email}" maxlength="50"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userInstance, field: 't001Foto', 'error')} ">
    <label class="control-label" for="t001Foto">
        <g:message code="app.user.t001Foto.label" default="Foto" />
    </label>
    <div class="controls">
    %{--<g:form>--}%
        <g:if test="${userInstance?.t001Foto}">
            <img width="250" id="t001Foto" name="t001Foto" src="data:jpg;base64,${new String(new Base64().encode(new File(userInstance.t001Foto).bytes), "UTF-8")}" /></br></br>
        </g:if>
        <input type="file" name="myFile" id="myFile"/>
        <input type="button" style="width: 80px" value="Upload" onClick="fileUpload(this.form, '${g.createLink(controller: 'user', action: 'uploadImage')}', 'upload'); return false;">
        <div id="upload"></div>
        %{--</g:form>--}%
    </div>
</div>