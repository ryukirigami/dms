<%@ page import="com.kombos.parts.AlasanAdjusment" %>

<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: alasanAdjusmentInstance, field: 'm165ID', 'error')} required">
	<label class="control-label" for="m165ID">
		<g:message code="alasanAdjusment.m165ID.label" default="Kode" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="m165ID" type="number" value="${alasanAdjusmentInstance.m165ID}" maxlength="8" onkeypress="return isNumberKey(event)" required=""/>
	</div>
</div>
<g:javascript>
    document.getElementById("m165ID").focus();
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: alasanAdjusmentInstance, field: 'm165AlasanAdjusment', 'error')} required">
	<label class="control-label" for="m165AlasanAdjusment">
		<g:message code="alasanAdjusment.m165AlasanAdjusment.label" default="Alasan Adjustment" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textArea name="m165AlasanAdjusment" maxlength="255" required="" value="${alasanAdjusmentInstance?.m165AlasanAdjusment}"/>
	</div>
</div>
