
<%@ page import="com.kombos.parts.AlasanAdjusment" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</g:javascript>

<table id="alasanAdjusment_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="alasanAdjusment.m165ID.label" default="Kode" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="alasanAdjusment.m165AlasanAdjusment.label" default="Alasan Adjustment" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; display: hidden">
				<div><g:message code="alasanAdjusment.createdBy.label" default="Created By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; display: hidden">
				<div><g:message code="alasanAdjusment.updatedBy.label" default="Updated By" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px; display: hidden">
				<div><g:message code="alasanAdjusment.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m165ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m165ID" class="search_init" onkeypress="return isNumberKey(event)" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m165AlasanAdjusment" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m165AlasanAdjusment" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;  display: hidden">
				<div id="filter_createdBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_createdBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;  display: hidden">
				<div id="filter_updatedBy" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_updatedBy" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;  display: hidden">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var alasanAdjusmentTable;
var reloadAlasanAdjusmentTable;
$(function(){
	
	reloadAlasanAdjusmentTable = function() {
		alasanAdjusmentTable.fnDraw();
	}

    var recordsalasanAdjusmentperpage = [];
    var anAlasanAdjusmentSelected;
    var jmlRecAlasanAdjusmentPerPage=0;
    var id;


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	alasanAdjusmentTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	alasanAdjusmentTable = $('#alasanAdjusment_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsAlasanAdjusment = $("#alasanAdjusment_datatables tbody .row-select");
            var jmlAlasanAdjusmentCek = 0;
            var nRow;
            var idRec;
            rsAlasanAdjusment.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsalasanAdjusmentperpage[idRec]=="1"){
                    jmlAlasanAdjusmentCek = jmlAlasanAdjusmentCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsalasanAdjusmentperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecAlasanAdjusmentPerPage = rsAlasanAdjusment.length;
            if(jmlAlasanAdjusmentCek==jmlRecAlasanAdjusmentPerPage && jmlRecAlasanAdjusmentPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
        "fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m165ID",
	"mDataProp": "m165ID",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"20%",
	"bVisible": true
}

,

{
	"sName": "m165AlasanAdjusment",
	"mDataProp": "m165AlasanAdjusment",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"80%",
	"bVisible": true
}

,

{
	"sName": "createdBy",
	"mDataProp": "createdBy",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "updatedBy",
	"mDataProp": "updatedBy",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": false
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var m165ID = $('#filter_m165ID input').val();
						if(m165ID){
							aoData.push(
									{"name": 'sCriteria_m165ID', "value": m165ID}
							);
						}
	
						var m165AlasanAdjusment = $('#filter_m165AlasanAdjusment input').val();
						if(m165AlasanAdjusment){
							aoData.push(
									{"name": 'sCriteria_m165AlasanAdjusment', "value": m165AlasanAdjusment}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {
        $("#alasanAdjusment_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsalasanAdjusmentperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsalasanAdjusmentperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

    $('#alasanAdjusment_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsalasanAdjusmentperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anAlasanAdjusmentSelected = alasanAdjusmentTable.$('tr.row_selected');
            if(jmlRecAlasanAdjusmentPerPage == anAlasanAdjusmentSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsalasanAdjusmentperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
