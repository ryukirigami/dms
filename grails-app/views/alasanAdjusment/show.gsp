

<%@ page import="com.kombos.parts.AlasanAdjusment" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'alasanAdjusment.label', default: 'Alasan Adjustment')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteAlasanAdjusment;

$(function(){ 
	deleteAlasanAdjusment=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/alasanAdjusment/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadAlasanAdjusmentTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-alasanAdjusment" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="alasanAdjusment"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${alasanAdjusmentInstance?.m165ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m165ID-label" class="property-label"><g:message
					code="alasanAdjusment.m165ID.label" default="Kode" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m165ID-label">
						%{--<ba:editableValue
								bean="${alasanAdjusmentInstance}" field="m165ID"
								url="${request.contextPath}/AlasanAdjusment/updatefield" type="text"
								title="Enter m165ID" onsuccess="reloadAlasanAdjusmentTable();" />--}%
							
								<g:fieldValue bean="${alasanAdjusmentInstance}" field="m165ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${alasanAdjusmentInstance?.m165AlasanAdjusment}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m165AlasanAdjusment-label" class="property-label"><g:message
					code="alasanAdjusment.m165AlasanAdjusment.label" default="Alasan Adjustment" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m165AlasanAdjusment-label">
						%{--<ba:editableValue
								bean="${alasanAdjusmentInstance}" field="m165AlasanAdjusment"
								url="${request.contextPath}/AlasanAdjusment/updatefield" type="text"
								title="Enter m165AlasanAdjusment" onsuccess="reloadAlasanAdjusmentTable();" />--}%
							
								<g:fieldValue bean="${alasanAdjusmentInstance}" field="m165AlasanAdjusment"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${alasanAdjusmentInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="alasanAdjusment.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${alasanAdjusmentInstance}" field="dateCreated"
                                url="${request.contextPath}/alasanAdjusment/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadalasanAdjusmentTable();" />--}%

                        <g:formatDate date="${alasanAdjusmentInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${alasanAdjusmentInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="alasanAdjusment.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${alasanAdjusmentInstance}" field="createdBy"
                                url="${request.contextPath}/alasanAdjusment/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadalasanAdjusmentTable();" />--}%

                        <g:fieldValue bean="${alasanAdjusmentInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${alasanAdjusmentInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="alasanAdjusment.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${alasanAdjusmentInstance}" field="lastUpdated"
                                url="${request.contextPath}/alasanAdjusment/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadalasanAdjusmentTable();" />--}%

                        <g:formatDate date="${alasanAdjusmentInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${alasanAdjusmentInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="alasanAdjusment.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${alasanAdjusmentInstance}" field="updatedBy"
                                url="${request.contextPath}/alasanAdjusment/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadalasanAdjusmentTable();" />--}%

                        <g:fieldValue bean="${alasanAdjusmentInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${alasanAdjusmentInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="alasanAdjusment.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${alasanAdjusmentInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/alasanAdjusment/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadalasanAdjusmentTable();" />--}%

                        <g:fieldValue bean="${alasanAdjusmentInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${alasanAdjusmentInstance?.id}"
					update="[success:'alasanAdjusment-form',failure:'alasanAdjusment-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteAlasanAdjusment('${alasanAdjusmentInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
