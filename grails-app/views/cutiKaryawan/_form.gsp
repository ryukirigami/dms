<%@ page import="com.kombos.hrd.CutiKaryawan" %>


<g:javascript>
    $(function() {
        $("#btnCariKaryawan").click(function() {
            oFormService.fnShowKaryawanDataTable();
        })
        /*$("#jumlahHariCuti").keyup(function(e) {
            var v = $(this).val();
            if (v instanceof Number) {
                if (v > $("#sisaCutiTahunBerjalan").val()) {

                }
            }
        })*/
    })


    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }


</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: cutiKaryawanInstance, field: 'karyawan', 'error')} ">
    <label class="control-label" for="karyawan">
        <g:message code="cutiKaryawan.karyawan.label" default="Karyawan" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        %{--<g:select id="karyawan" name="karyawan.id" from="${com.kombos.hrd.Karyawan.list()}" optionKey="id" required="" value="${cutiKaryawanInstance?.karyawan?.id}" class="many-to-one"/>--}%
        <div class="input-append">
            <g:hiddenField name="karyawan.id" readonly="readonly" id="karyawan_id" value="${cutiKaryawanInstance?.karyawan?.id}"/>
            <input type="text" id="karyawan_nama" value="${cutiKaryawanInstance?.karyawan?.nama}" readonly/>
            <span class="add-on">
                <a href="javascript:void(0);" id="btnCariKaryawan">
                    <i class="icon-search"/>
                </a>
            </span>
        </div>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: cutiKaryawanInstance, field: 'tahunCuti', 'error')} ">
    <label class="control-label" for="tahunCuti">
        <g:message code="cutiKaryawan.tahunCuti.label" default="Tahun Cuti" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:field type="text" id="tahunCuti" name="tahunCuti" value="${cutiKaryawanInstance?.tahunCuti}" onkeypress="return isNumberKey(event);" placeholder="2016" maxlength="4" />
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="sisaCutiTahunBerjalan">
        Sisa Cuti Tahun Berjalan
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <input type="text" id="sisaCutiTahunBerjalan" name="sisaCutiTahunBerjalan" readonly/>
    </div>
</div>



<div class="control-group fieldcontain ${hasErrors(bean: cutiKaryawanInstance, field: 'tanggalMulaiCuti', 'error')} ">
    <label class="control-label" for="tanggalMulaiCuti">
        <g:message code="cutiKaryawan.tanggalMulaiCuti.label" default="Tanggal Mulai Cuti" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <ba:datePicker name="tanggalMulaiCuti" precision="day" value="${cutiKaryawanInstance?.tanggalMulaiCuti}" format="yyyy-MM-dd"/>
    </div>
</div>



<div class="control-group fieldcontain ${hasErrors(bean: cutiKaryawanInstance, field: 'tanggalAkhirCuti', 'error')} ">
    <label class="control-label" for="tanggalAkhirCuti">
        <g:message code="cutiKaryawan.tanggalAkhirCuti.label" default="Tanggal Akhir Cuti" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <ba:datePicker name="tanggalAkhirCuti" precision="day" value="${cutiKaryawanInstance?.tanggalAkhirCuti}" format="yyyy-MM-dd"/>
    </div>
</div>



<div class="control-group fieldcontain ${hasErrors(bean: cutiKaryawanInstance, field: 'jumlahHariCuti', 'error')} ">
	<label class="control-label" for="jumlahHariCuti">
		<g:message code="cutiKaryawan.jumlahHariCuti.label" default="Jumlah Hari Cuti" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="number" id="jumlahHariCuti" name="jumlahHariCuti" value="${cutiKaryawanInstance?.jumlahHariCuti}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: cutiKaryawanInstance, field: 'cutiKaryawan', 'error')} ">
    <label class="control-label" for="cutiKaryawan">
        <g:message code="cutiKaryawan.cutiKaryawan.label" default="Tipe Cuti" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select name="cutiKaryawan.id" from="${com.kombos.hrd.Cuti.list()}" optionKey="id" optionValue="cuti" value="${cutiKaryawanInstance?.cutiKaryawan?.id}" class="many-to-one"/>

    </div>
</div>



<div class="control-group fieldcontain ${hasErrors(bean: cutiKaryawanInstance, field: 'keterangan', 'error')} ">
	<label class="control-label" for="keterangan">
		<g:message code="cutiKaryawan.keterangan.label" default="Keterangan" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="keterangan" value="${cutiKaryawanInstance?.keterangan}" />
	</div>
</div>


<!-- Karyawan Modal -->

<div id="karyawanModal" class="modal fade">
    <div class="modal-dialog" style="width: 1200px;">
        <div class="modal-content" style="width: 1200px;">
            <div class="modal-header">
                <a class="close" href="javascript:void(0);" data-dismiss="modal">×</a>
                <h4>Data Karyawan - List</h4>
            </div>
            <!-- dialog body -->
            <div class="modal-body" id="karyawanModal-body" style="max-height: 450px;">
                <div class="box">
                    <div class="span12">
                        <fieldset>
                            <table>
                                <tr style="display:table-row;">
                                    <td style="width: 130px; display:table-cell; padding:5px;">
                                        <label class="control-label" for="sCriteria_npk">NPK</label>
                                    </td>
                                    <td style="width: 130px; display:table-cell; padding:5px;">
                                        <input type="text" id="sCriteria_npk">
                                    </td>
                                </tr>
                                <tr style="display:table-row;">
                                    <td style="width: 130px; display:table-cell; padding:5px;">
                                        <label class="control-label" for="sCriteria_nama">Nama Karyawan</label>
                                    </td>
                                    <td style="width: 130px; display:table-cell; padding:5px;">
                                        <input type="text" id="sCriteria_nama">
                                    </td>
                                <tr style="display:table-row;">
                                    <td style="width: 130px; display:table-cell; padding:5px;">
                                        <label class="control-label" for="sCriteria_pekerjaan">Pekerjaan / Departemen</label>
                                    </td>
                                    <td style="width: 130px; display:table-cell; padding:5px;">
                                        %{--<input type="text" id="sCriteria_pekerjaan">--}%
                                        <g:select name="sCriteria_pekerjaan" id="sCriteria_pekerjaan" from="${com.kombos.administrasi.ManPower.list()}"
                                                  optionKey="id" noSelection="['':'']" optionValue="m014JabatanManPower"/>
                                    </td>
                                </tr>
                                <tr style="display:table-row;">
                                    <td style="width: 130px; display:table-cell; padding:5px;">
                                        <label class="control-label" for="sCriteria_companyDealer">Cabang</label>
                                    </td>
                                    <td style="width: 130px; display:table-cell; padding:5px;">
                                        <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                            <g:select name="sCriteria_companyDealer" id="sCriteria_companyDealer" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}"  />
                                        </g:if>
                                        <g:else>
                                            <g:select name="sCriteria_companyDealer" id="sCriteria_companyDealer" readonly="" disabled="" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                                        </g:else>

                                    </td>
                                    <td >
                                        <a id="btnSearchKaryawan" class="btn btn-primary" href="javascript:void(0);">
                                            Cari
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                </div>
                <g:render template="dataTablesKaryawan" />
            </div>

            <div class="modal-footer">
                <a href="javascript:void(0);" class="btn cancel" data-dismiss="modal">
                    Tutup
                </a>
                <a href="javascript:void(0);" id="btnPilihKaryawan" class="btn btn-primary">
                    Pilih Karyawan
                </a>
            </div>
        </div>
    </div>
</div>

<g:javascript>
    oFormService.fnInit();
</g:javascript>
