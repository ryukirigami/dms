

<%@ page import="com.kombos.hrd.CutiKaryawan" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'cutiKaryawan.label', default: 'CutiKaryawan')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteCutiKaryawan;

$(function(){ 
	deleteCutiKaryawan=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/cutiKaryawan/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadCutiKaryawanTable();
   				expandTableLayout('cutiKaryawan');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-cutiKaryawan" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="cutiKaryawan"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${cutiKaryawanInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="cutiKaryawan.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${cutiKaryawanInstance}" field="createdBy"
								url="${request.contextPath}/CutiKaryawan/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadCutiKaryawanTable();" />--}%
							
								<g:fieldValue bean="${cutiKaryawanInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cutiKaryawanInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="cutiKaryawan.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${cutiKaryawanInstance}" field="dateCreated"
								url="${request.contextPath}/CutiKaryawan/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadCutiKaryawanTable();" />--}%
							
								<g:formatDate date="${cutiKaryawanInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cutiKaryawanInstance?.jumlahHariCuti}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="jumlahHariCuti-label" class="property-label"><g:message
					code="cutiKaryawan.jumlahHariCuti.label" default="Jumlah Hari Cuti" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="jumlahHariCuti-label">
						%{--<ba:editableValue
								bean="${cutiKaryawanInstance}" field="jumlahHariCuti"
								url="${request.contextPath}/CutiKaryawan/updatefield" type="text"
								title="Enter jumlahHariCuti" onsuccess="reloadCutiKaryawanTable();" />--}%
							
								<g:fieldValue bean="${cutiKaryawanInstance}" field="jumlahHariCuti"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cutiKaryawanInstance?.karyawan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="karyawan-label" class="property-label"><g:message
					code="cutiKaryawan.karyawan.label" default="Karyawan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="karyawan-label">
						%{--<ba:editableValue
								bean="${cutiKaryawanInstance}" field="karyawan"
								url="${request.contextPath}/CutiKaryawan/updatefield" type="text"
								title="Enter karyawan" onsuccess="reloadCutiKaryawanTable();" />--}%
							
								<g:link controller="karyawan" action="show" id="${cutiKaryawanInstance?.karyawan?.id}">${cutiKaryawanInstance?.karyawan?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cutiKaryawanInstance?.keterangan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="keterangan-label" class="property-label"><g:message
					code="cutiKaryawan.keterangan.label" default="Keterangan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="keterangan-label">
						%{--<ba:editableValue
								bean="${cutiKaryawanInstance}" field="keterangan"
								url="${request.contextPath}/CutiKaryawan/updatefield" type="text"
								title="Enter keterangan" onsuccess="reloadCutiKaryawanTable();" />--}%
							
								<g:fieldValue bean="${cutiKaryawanInstance}" field="keterangan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cutiKaryawanInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="cutiKaryawan.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${cutiKaryawanInstance}" field="lastUpdProcess"
								url="${request.contextPath}/CutiKaryawan/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadCutiKaryawanTable();" />--}%
							
								<g:fieldValue bean="${cutiKaryawanInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cutiKaryawanInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="cutiKaryawan.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${cutiKaryawanInstance}" field="lastUpdated"
								url="${request.contextPath}/CutiKaryawan/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadCutiKaryawanTable();" />--}%
							
								<g:formatDate date="${cutiKaryawanInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cutiKaryawanInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="cutiKaryawan.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${cutiKaryawanInstance}" field="staDel"
								url="${request.contextPath}/CutiKaryawan/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadCutiKaryawanTable();" />--}%
							
								<g:fieldValue bean="${cutiKaryawanInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cutiKaryawanInstance?.tanggalAkhirCuti}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tanggalAkhirCuti-label" class="property-label"><g:message
					code="cutiKaryawan.tanggalAkhirCuti.label" default="Tanggal Akhir Cuti" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tanggalAkhirCuti-label">
						%{--<ba:editableValue
								bean="${cutiKaryawanInstance}" field="tanggalAkhirCuti"
								url="${request.contextPath}/CutiKaryawan/updatefield" type="text"
								title="Enter tanggalAkhirCuti" onsuccess="reloadCutiKaryawanTable();" />--}%
							
								<g:formatDate date="${cutiKaryawanInstance?.tanggalAkhirCuti}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cutiKaryawanInstance?.tanggalMulaiCuti}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tanggalMulaiCuti-label" class="property-label"><g:message
					code="cutiKaryawan.tanggalMulaiCuti.label" default="Tanggal Mulai Cuti" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tanggalMulaiCuti-label">
						%{--<ba:editableValue
								bean="${cutiKaryawanInstance}" field="tanggalMulaiCuti"
								url="${request.contextPath}/CutiKaryawan/updatefield" type="text"
								title="Enter tanggalMulaiCuti" onsuccess="reloadCutiKaryawanTable();" />--}%
							
								<g:formatDate date="${cutiKaryawanInstance?.tanggalMulaiCuti}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cutiKaryawanInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="cutiKaryawan.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${cutiKaryawanInstance}" field="updatedBy"
								url="${request.contextPath}/CutiKaryawan/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadCutiKaryawanTable();" />--}%
							
								<g:fieldValue bean="${cutiKaryawanInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('cutiKaryawan');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${cutiKaryawanInstance?.id}"
					update="[success:'cutiKaryawan-form',failure:'cutiKaryawan-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteCutiKaryawan('${cutiKaryawanInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
