
<%@ page import="com.kombos.hrd.CutiKaryawan" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'cutiKaryawan.label', default: 'CutiKaryawan')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
        <style>
        .modal.fade.in {
            left: 25.5% !important;
            width: 1205px !important;
        }
        .modal.fade {
            top: -100%;
        }
        </style>
		<r:require modules="baseapplayout, baseapplist, autoNumeric" />
		<g:javascript>
			var show;
			var edit;
			var oFormService;
			$(function(){

			    oFormService = {
			        fnInit: function() {
			            $("#tahunCuti").focusout(function() {
			                $.get("${request.getContextPath()}/cutiKaryawan/sisaCutiTahunBerjalan", {id: $("#karyawan_id").val(), tahun: $(this).val()}, function(data) {
			                    $("#sisaCutiTahunBerjalan").val(data.sisaCutiTahunBerjalan);
			                })
			            })
			        },
			        fnShowKaryawanDataTable: function() {
                        $("#karyawanModal").modal("show");
                        $("#karyawanModal").fadeIn();
			        },
			        fnHideKaryawanDataTable: function() {
			            $("#karyawanModal").modal("hide");
			        }
			    }
			
				$('.box-action').click(function(){
					switch($(this).attr('target')){
						case '_CREATE_' :                      
							shrinkTableLayout('cutiKaryawan');
							<g:remoteFunction action="create"
								onLoading="jQuery('#spinner').fadeIn(1);"
								onSuccess="loadFormInstance('cutiKaryawan', data, textStatus);"
								onComplete="jQuery('#spinner').fadeOut();" />
							break;
						case '_DELETE_' :  
							bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
								function(result){
									if(result){
										massDelete('cutiKaryawan', '${request.contextPath}/cutiKaryawan/massdelete', reloadCutiKaryawanTable);
									}
								});                    
							
							break;
				   }    
				   return false;
				});

				show = function(id) {
					showInstance('cutiKaryawan','${request.contextPath}/cutiKaryawan/show/'+id);
				};
				
				edit = function(id) {
					editInstance('cutiKaryawan','${request.contextPath}/cutiKaryawan/edit/'+id);
				};

});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
					class="icon-plus"></i>&nbsp;&nbsp;
			</a></li>
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="cutiKaryawan-table">

			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>


                <fieldset class="form-horizontal">

                    <!-- Form Name -->
                    <legend>Pencarian Cuti Karyawan</legend>

                    <!-- Text input-->
                    <div class="control-group">
                        <label class="control-label" for="filter_karyawan">Nama Karyawan</label>
                        <div class="controls">
                            <input id="filter_karyawan" name="filter_karyawan" placeholder="" class="input-medium" type="text">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="control-group">
                        <label class="control-label" for="search_tanggalMulaiCuti">Tanggal Cuti (Mulai)</label>
                        <div class="controls">
                            %{--<input id="search_tanggalMulaiCuti" name="search_tanggalMulaiCuti" placeholder="" class="input-medium" type="text">--}%
                            <ba:datePicker name="search_tanggalMulaiCuti" format="dd-mm-yyyy" />
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="control-group">
                        <label class="control-label" for="search_tanggalAkhirCuti">Tanggal Cuti (Akhir)</label>
                        <div class="controls">
                            %{--<input id="search_tanggalAkhirCuti" name="search_tanggalAkhirCuti" placeholder="" class="input-medium" type="text">--}%
                            <ba:datePicker name="search_tanggalAkhirCuti" format="dd-mm-yyyy"  />
                        </div>
                    </div>

                    <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                        <div class="control-group">
                            <label class="control-label" for="filter_karyawan">Cabang</label>
                            <div class="controls">
                                <g:select name="sCriteria_dealer" id="sCriteria_dealer" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" noSelection="['':'SEMUA']" />
                            </div>
                        </div>
                    </g:if>

                    <!-- Select Basic -->
                    <div class="control-group">
                        <label class="control-label" for="filter_tipeCuti">Tipe Cuti</label>
                        <div class="controls">
                            %{--<g:select name="filter_tipeCuti" from="${com.kombos.hrd.Cuti.list()}" class="input-medium" noSelection="['':'']" />--}%
                            <g:select name="filter_tipeCuti" id="filter_tipeCuti" from="${com.kombos.hrd.Cuti.createCriteria().list{eq("staDel","0");order("cuti");}}" optionKey="id" optionValue="cuti" noSelection="['':'SEMUA']" />
                        </div>
                    </div>

                    <!-- Button -->
                    <div class="control-group">
                        <label class="control-label" for="btnCari"></label>
                        <div class="controls">
                            <button id="btnCari" name="btnCari" class="btn btn-primary" onclick="javascript:void(0);">Cari</button>
                        </div>
                    </div>
                </fieldset>


            <g:render template="dataTables" />
		</div>
		<div class="span7" id="cutiKaryawan-form" style="display: none;"></div>

	</div>
</body>
</html>
