
<%@ page import="com.kombos.hrd.CutiKaryawan" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="cutiKaryawan_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">

</table>

<g:javascript>
var cutiKaryawanTable;
var reloadCutiKaryawanTable;
$(function(){

	$("#btnCari").click(function() {
	    reloadCutiKaryawanTable();
	})

	reloadCutiKaryawanTable = function() {
		cutiKaryawanTable.fnDraw();
	}

	
	/*
	$('#search_tanggalAkhirCuti').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tanggalAkhirCuti_day').val(newDate.getDate());
			$('#search_tanggalAkhirCuti_month').val(newDate.getMonth()+1);
			$('#search_tanggalAkhirCuti_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			cutiKaryawanTable.fnDraw();	
	});

	

	$('#search_tanggalMulaiCuti').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tanggalMulaiCuti_day').val(newDate.getDate());
			$('#search_tanggalMulaiCuti_month').val(newDate.getMonth()+1);
			$('#search_tanggalMulaiCuti_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			cutiKaryawanTable.fnDraw();	
	});

	*/


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	cutiKaryawanTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	cutiKaryawanTable = $('#cutiKaryawan_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [


{
    "sTitle": "Nama Karyawan",
	"sName": "karyawan",
	"mDataProp": "karyawan",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true,
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	}
},


{
    "sTitle": "Tanggal",
	"sName": "tanggalMulaiCuti",
	"mDataProp": "tanggalMulaiCuti",
	"aTargets": [2],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"200px",
	"bVisible": true,
	"mRender": function ( data, type, row ) {
		return data + " - " + row['tanggalAkhirCuti'];
	}
},

{
    "sTitle": "Tipe Cuti",
	"sName": "cutiKaryawan",
	"mDataProp": "cutiKaryawan",
	"aTargets": [3],
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"100px",
	"bVisible": true
},

{
    "sTitle": "Jumlah Hari",
	"sName": "jumlahHariCuti",
	"mDataProp": "jumlahHariCuti",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"80px",
	"bVisible": true
}



,

{
    "sTitle": "Keterangan",
	"sName": "keterangan",
	"mDataProp": "keterangan",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
    "sTitle": "Cabang",
	"sName": "cabang",
	"mDataProp": "cabang",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
							var sCriteria_tipeCuti = $('#filter_tipeCuti').val();
						if(sCriteria_tipeCuti){
							aoData.push(
									{"name": 'filter_tipeCuti', "value": sCriteria_tipeCuti}
							);
						}

						var sCriteria_cabang = $('#sCriteria_dealer').val();
						if(sCriteria_cabang){
							aoData.push(
									{"name": 'sCriteria_dealer', "value": sCriteria_cabang}
							);
						}


						var jumlahHariCuti = $('#filter_jumlahHariCuti').val();
						if(jumlahHariCuti){
							aoData.push(
									{"name": 'sCriteria_jumlahHariCuti', "value": jumlahHariCuti}
							);
						}
	
						var karyawan = $('#filter_karyawan').val();
						if(karyawan){
							aoData.push(
									{"name": 'sCriteria_karyawan', "value": karyawan}
							);
						}
	
						var keterangan = $('#filter_keterangan').val();
						if(keterangan){
							aoData.push(
									{"name": 'sCriteria_keterangan', "value": keterangan}
							);
						}


						var tanggalAkhirCuti = $('#search_tanggalAkhirCuti').val();
						var tanggalAkhirCutiDay = $('#search_tanggalAkhirCuti_day').val();
						var tanggalAkhirCutiMonth = $('#search_tanggalAkhirCuti_month').val();
						var tanggalAkhirCutiYear = $('#search_tanggalAkhirCuti_year').val();
						
						if(tanggalAkhirCuti){
							aoData.push(
									{"name": 'sCriteria_tanggalAkhirCuti', "value": "date.struct"},
									{"name": 'sCriteria_tanggalAkhirCuti_dp', "value": tanggalAkhirCuti},
									{"name": 'sCriteria_tanggalAkhirCuti_day', "value": tanggalAkhirCutiDay},
									{"name": 'sCriteria_tanggalAkhirCuti_month', "value": tanggalAkhirCutiMonth},
									{"name": 'sCriteria_tanggalAkhirCuti_year', "value": tanggalAkhirCutiYear}
							);
						}

						var tanggalMulaiCuti = $('#search_tanggalMulaiCuti').val();
						var tanggalMulaiCutiDay = $('#search_tanggalMulaiCuti_day').val();
						var tanggalMulaiCutiMonth = $('#search_tanggalMulaiCuti_month').val();
						var tanggalMulaiCutiYear = $('#search_tanggalMulaiCuti_year').val();
						
						if(tanggalMulaiCuti){
							aoData.push(
									{"name": 'sCriteria_tanggalMulaiCuti', "value": "date.struct"},
									{"name": 'sCriteria_tanggalMulaiCuti_dp', "value": tanggalMulaiCuti},
									{"name": 'sCriteria_tanggalMulaiCuti_day', "value": tanggalMulaiCutiDay},
									{"name": 'sCriteria_tanggalMulaiCuti_month', "value": tanggalMulaiCutiMonth},
									{"name": 'sCriteria_tanggalMulaiCuti_year', "value": tanggalMulaiCutiYear}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>

