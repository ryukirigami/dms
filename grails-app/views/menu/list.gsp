<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<title></title>
		<r:require modules="baseapplayout" />
		<g:render template="../menu/maxLineDisplay"/>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left">${label}</span>
	</div>
	<div class="box">
		<div class="span12" id="material-table">
			<table id="${id}_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
			 <g:each in="${columns}" status="i" var="col">
				<th style="border-bottom: none;padding: 5px;">
					<div>${col.label}</div>
				</th>
			</g:each>			
		</tr>
	</thead>
</table>
<g:javascript>
var ${id}Table;
$(function(){
	
${id}Table = $('#${id}_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "${id}DatatablesList")}",
		"aoColumns": [
<g:each in="${columns}" status="i" var="col">
{
	"sName": "${col.field}",
	"mDataProp": "${col.field}",
	"aTargets": [${i}],
	"bSearchable": false,
	"bSortable": true,
	"sWidth":"${col.width}",
	"bVisible": true
}
<g:if test="${i < columns.size() - 1}">
,
</g:if>
</g:each>
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>

		</div>
	</div>
</body>
</html>
