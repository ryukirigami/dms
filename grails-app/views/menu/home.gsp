<html>
<head>
	<meta name="layout" content="main">
  	<title>Home</title>
</head>
<body>
<div class="row-fluid">
<div id="home-task" class="span4">
	<div class="home-task"><span><g:message code="home.mytask.label"/></span>
		<span><a href="javascript:void(0);" onclick="loadTask();">
			<span class="label label-warning" id="${taskCount}">${taskCount}</span>
		</a></span>
	</div>
	<div class="box" style="display: none;">
			<div class="span12">
				<ul id="task-list">
                    <g:each in="${masalah}" status="i" var="j" >
                         <br> <a href="#" onclick="openTask('${j?.jpb?.reception?.t401NoWO}');"> ${j?.jpb?.reception?.t401NoWO}</a>
                    </g:each>
                </ul>
	<%--			<g:render template="dataTables" />--%>
			</div>
	</div>
</div>
<g:javascript>
var loadTask;
var taskCollapsed = true;
var openTask;

$(function(){
	$.ajax({
        	type: 'GET',
            url: '${request.getContextPath()}/Task/tasksCount',
            success: function(data) {
            	var total = data.unassignedTasksCount + data.myTasksCount;
            	$("#tasksCount").html(total);
          	},
            error: function(){
            }
    });

	loadTask = function() {
		if(taskCollapsed){
			expandTask();
		} else {
			collapseTask();
		}
	};

	openTask = function(noWO){

        $('#main-content').empty();
        $('#spinner').fadeIn(1);
        $.ajax({url: '${request.contextPath}/responProblemFinding',
				type: "GET",
				data : {noWO : noWO},
				dataType: "html",
                complete : function (req, err) {
                    $('#main-content').append(req.responseText);
                     $('#spinner').fadeOut();
                   }
        });
    }


%{--openTask = function(){--}%
        %{--$('#main-content').empty();--}%
        %{--$('#spinner').fadeIn(1);--}%
        %{--$.ajax({url: '${request.contextPath}/problemFindingGr',--}%
%{--//				type: "GET",--}%
%{--//				data : {noWO : noWO},--}%
%{--//				dataType: "html",--}%
                %{--complete : function (req, err) {--}%
                    %{--$('#main-content').append(req.responseText);--}%
                     %{--$('#spinner').fadeOut();--}%
                   %{--}--}%
        %{--});--}%
    %{--}--}%

	function expandTask() {
	/*	$.ajax({
        	type: 'GET',
            url: '${request.getContextPath()}/task/tasksNotification',
            success: function(data) {
            	$("#task-list").html(data);
          	},
            error: function(){
                //alert('Server Response Error');
            }
       	});

	*/	taskCollapsed = false;
		$('#home-task').find('.box').slideDown(300,function() {
		});
		return false;
	}

	function collapseTask() {
		taskCollapsed = true;
		$('#home-task').find('.box').slideUp(300,function() {
		});
	}


});
</g:javascript>
<div id="home-shortcut" class="span5">
	<img src="${resource(dir: 'images',file: 'dashboard_shortcut.png')}" alt="shortcut">
</div>
<div id="home-iu" class="span3">
	<div class="home-iu"><span><g:message code="home.iu.label"/></span></div>
	<div class="box">
			<div class="span12" id="iu-list">
				<ul>
<%--					--%>
<%--					<li>--%>
<%--						<div class="iu-date">1 Januari 2014</div>--%>
<%--						<div class="iu-content"><a href="${request.contextPath}/informasiPenting/show/1" class="iu-link">Tahun Baru Semangat Baru, Semoga sukses terus tanpa ada halangan</a></div>--%>
<%--					</li>--%>
					<g:each in="${infos}" status="i" var="info">
						<li>
							<div class="iu-date">${info.tanggal}</div>
							%{--<div class="iu-content"><a href="${request.contextPath}/informasiPenting/show/${info.id}" class="iu-link">${info.content}</a></div>--}%
							<div class="iu-content">
                                <a href=" Detail Informasi : ${infos.content}" class="iu-link">
                                    ${info.judul}
                                </a>
                            </div>
						</li>
					</g:each>
				</ul>
			</div>
	</div>
</div>
<g:javascript>
$(function(){
	$('.iu-detail').click(function(event){
    	event.stopPropagation();
	});
	$('.iu-link').click(function(){
		alert($(this).attr('href'));
		$("#iuModal").modal({
			"backdrop" : "static",
			"keyboard" : true,
			"show" : true
		});
		return false;
	});
	$("#iuModal").on("show", function() {
		$("#iuModal .btn").on("click", function(e) {
			$("#iuModal").modal('hide');
		});
	});
	$("#iuModal").on("hide", function() {
		$("#iuModal a.btn").off("click");
	});
	$("#iuModal").on("hidden", function() {
<%--		$("#iuModal").remove();--%>
	});

});
</g:javascript>
</div>
<div class="row-fluid">
<div id="home-summary" class="span6">
	<div class="home-summary"><span><g:message code="home.summary.label"/></span></div>
	<div class="box">
			<div class="span12">
				<g:render template="todaySummary" />
			</div>
	</div>
</div>
<div id="home-approval" class="span6">
	<div class="home-approval"><span><g:message code="home.approval.label"/></span></div>
	<div class="box">
			<g:render template="approvalAndAlert" />
	</div>
</div>
</div>

<%--    <h3 class="box-header">&nbsp;</h3>--%>
<%--        <div class="box">--%>
<%--           <g:render template="welcomePage"/>--%>
<%--        </div>--%>
<!-- set up the modal to start hidden and fade in and out -->
<div id="iuModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- dialog body -->
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<div class="iu-content"></div>
			</div>
			<!-- dialog buttons -->
			<div class="modal-footer"><button type="button" class="btn btn-primary">OK</button></div>
		</div>
	</div>
</div>
</body>
</html>