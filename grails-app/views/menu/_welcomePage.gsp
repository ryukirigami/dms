<%@ page import="java.text.SimpleDateFormat; org.apache.shiro.SecurityUtils; com.kombos.baseapp.sec.shiro.User; com.kombos.baseapp.SecurityChecklist; com.kombos.baseapp.utils.SecurityChecklistUtilService; com.kombos.baseapp.AppSettingParamService; com.kombos.baseapp.AppSettingParam" %>
<label>
    <%
        AppSettingParam appSettingParam = AppSettingParam.findByCode(AppSettingParamService.WELCOME_TEXT)
        if(appSettingParam.value == null){
            println appSettingParam.defaultValue
        }else{
            if(appSettingParam.value.length()==0){
                println appSettingParam.defaultValue
            }else{
                println appSettingParam.value
            }
        }
    %>

    <%
        User user = User.findByUsername(SecurityUtils.subject?.principal.toString())
        AppSettingParam appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)
        AppSettingParam appSettingParamTimeFormat = AppSettingParam.findByCode(AppSettingParamService.TIME_FORMAT)
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        String timeFormat = appSettingParamTimeFormat.value?appSettingParamTimeFormat.value:appSettingParamTimeFormat.defaultValue
        String pattern = dateFormat + " " + timeFormat
        SimpleDateFormat sdf = new SimpleDateFormat(pattern)

        SecurityChecklistUtilService securityChecklist = new SecurityChecklistUtilService(SecurityChecklistUtilService.CODE_PREVIOUS_SUCCESSFUL_LOG_ON)
        if (securityChecklist.enablePreviousSuccessfulLogOn()){
            Date lastLoggedIn = user.lastLoggedIn
            if(lastLoggedIn != null){
                String lastLoggedInString = sdf.format(lastLoggedIn)
    %>
        <br/>
        <g:message code="app.previous_successfull_logon.label" args="[lastLoggedInString]" />
    <%
            }
        }
    %>

    <%
        securityChecklist = new SecurityChecklistUtilService(SecurityChecklistUtilService.CODE_PREVIOUS_UNSUCCESSFUL_LOG_ON)
        if (securityChecklist.enablePreviousSuccessfulLogOn()){
            Date lastUnsuccesfulLoggedIn = user.lastUnsuccessfullLoggedIn
            if(lastUnsuccesfulLoggedIn != null){
                String lastUnsuccessfulLoggedInString = sdf.format(lastUnsuccesfulLoggedIn)
    %>
        <br/>
        <g:message code="app.previous_unsuccessfull_logon.label" args="[lastUnsuccessfulLoggedInString]" />
    <%
            }
        }
    %>


    <%
        SecurityChecklistUtilService secUtil = new SecurityChecklistUtilService(SecurityChecklistUtilService.CODE_PASSWORD_CYCLES)
        if(secUtil.enablePasswordCycles()){
            def interval = secUtil.getIntervalPasswordExpiry(org.apache.shiro.SecurityUtils.subject.principal.toString())
            if(interval == 0){
    %>
        <br/>
		<br/>
		<br/>
        <span class="text-error"><g:message code="app.banner.today_password_expiry"/></span>
    <%
        }else{
            if(interval == 30 || interval == 15 || interval <= 7){
    %>
        <br/>
		<br/>
		<br/>
		<span class="text-error"><g:message code="app.banner.password_expiry" args="${[String.valueOf(interval)]}"/></span>
    <%
                }
            }
        }
    %>
</label>