<table id="approvalAndAlert_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
			<th style="border-bottom: none;padding: 5px;">
				<div>Tipe</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Jenis</div>
			</th>
		</tr>
	</thead>
</table>

<g:javascript>
var aaTable;
var reloadAaTable;
var open_alert_list;
var open_npb_approval;
var open_approval_list;
var open_approval_history_list;
$(function(){
	open_alert_list = function(){
        $('#main-content').empty();
        $('#spinner').fadeIn(1);
        $.ajax({url: '${request.contextPath}/alert',
				type: "GET",
				dataType: "html",
                complete : function (req, err) {
                    $('#main-content').append(req.responseText);
                     $('#spinner').fadeOut();
                   }
        });
    }

      open_npb_approval = function(){
                $('#main-content').empty();
                $('#spinner').fadeIn(1);
                $.ajax({url: '${request.contextPath}/notaPesananBarang/npbApprovalList',
                        type: "GET",
                        dataType: "html",
                        complete : function (req, err) {
                            $('#main-content').append(req.responseText);
                             $('#spinner').fadeOut();
                           }
                });
          }

	
	open_approval_list = function(data){
        $('#main-content').empty();
        $('#spinner').fadeIn(1);
        $.ajax({url: '${request.contextPath}/approvalT770',
				type: "GET",
				data: {
                    jenis: data
                },
                dataType: "html",
                complete : function (req, err) {
                    $('#main-content').append(req.responseText);
                     $('#spinner').fadeOut();
                   }
        });
    }
	
	open_approval_history_list = function(){
        $('#main-content').empty();
        $('#spinner').fadeIn(1);
        $.ajax({url: '${request.contextPath}/historyApproval',
				type: "GET",
				dataType: "html",
                complete : function (req, err) {
                    $('#main-content').append(req.responseText);
                     $('#spinner').fadeOut();
                   }
        });
    }
	reloadAaTable = function() {
		aaTable.fnDraw();
	}
	
	aaTable = $('#approvalAndAlert_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "t",
		"bFilter": false,
		"bStateSave": false,
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": false,
		"bProcessing": false,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : 1000, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "approvalAndAlertDatatablesList")}",
		"aoColumns": [

{
	"sName": "tipe",
	"mDataProp": "tipe",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		if(row['tipe'] === 'Approval')
			return '<a href="#" onclick="open_approval_history_list();">'+data+'</a>';
		else if (row['tipe'] === 'Aprroval NPB')
			return '<a href="#" onclick="open_npb_approval();">Approval</a>';
		else
			return '<a href="#" onclick="open_alert_list();">'+data+'</a>';
	},
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"250px",
	"bVisible": true
}

,

{
	"sName": "jenis",
	"mDataProp": "jenis",
	"aTargets": [1],
	"mRender": function ( data, type, row ) {
		if(row['tipe'] === 'Approval')
			return '<a href="#" onclick="open_approval_list(\''+row['kegiatan']+'\');">'+data+'</a>';
		else if (row['tipe'] === 'Approval NPB')
			return '<a href="#" onclick="open_npb_approval();">'+data+'</a>';
		else
			return '<a href="#" onclick="open_alert_list();">'+data+'</a>';
	},
	"bSearchable": false,
	"bSortable": false,
	"sWidth":"250px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>
