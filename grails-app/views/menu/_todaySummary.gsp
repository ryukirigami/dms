<table id="today_summary" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<tbody>
		<tr>
			<td class="span3">
				<div>Info Teknisi</div>
			</td>
			<td class="span1">
				<div><a id="infoTeknisi" href="javascript:void(0);" onclick="open_summary(this);">0</a></div>
			</td>
			<td class="span3">
				<div>Today Waiting for Delivery</div>
			</td>
			<td class="span1">
				<div><a id="todayWaitingForDelivery" href="javascript:void(0);" onclick="open_summary(this);">0</a></div>
			</td>
		</tr>
		<tr>
			<td class="span3">
				<div>Today Unit Entry</div>
			</td>
			<td class="span1">
				<div><a id="todayUnitEntry" href="javascript:void(0);" onclick="open_summary(this);">0</a></div>
			</td>
			<td class="span3">
				<div>Today Non-Ontime Delivery</div>
			</td>
			<td class="span1">
				<div><a id="todayNonOntimeDelivery" href="javascript:void(0);" onclick="open_summary(this);">0</a></div>
			</td>
		</tr>
		<tr>
			<td class="span3">
				<div>Today Appointment</div>
			</td>
			<td class="span1">
				<div><a id="todayAppointment" href="javascript:void(0);" onclick="open_summary(this);">0</a></div>
			</td>
			<td class="span3">
				<div>Today Unit Invoice</div>
			</td>
			<td class="span1">
				<div><a id="todayUnitInvoice" href="javascript:void(0);" onclick="open_summary(this);">0</a></div>
			</td>
		</tr>
		<tr>
			<td class="span3">
				<div>Today Walk In</div>
			</td>
			<td class="span1">
				<div><a id="todayWalkIn" href="javascript:void(0);" onclick="open_summary(this);">0</a></div>
			</td>
			<td class="span3">
				<div>Today Settlement</div>
			</td>
			<td class="span1">
				<div><a id="todaySettlement" href="javascript:void(0);" onclick="open_summary(this);">0</a></div>
			</td>
		</tr>
		<tr>
			<td class="span3">
				<div>Today No Show Appointment</div>
			</td>
			<td class="span1">
				<div><a id="todayNoShowAppointment" href="javascript:void(0);" onclick="open_summary(this);">0</a></div>
			</td>
			<td class="span3">
				<div>Tomorrow Appointment</div>
			</td>
			<td class="span1">
				<div><a id="tommorowAppointment" href="javascript:void(0);" onclick="open_summary(this);">0</a></div>
			</td>
		</tr>
        <tr>
            <td class="span3">
                <div>WO(BP) belum bayar On Risk</div>
            </td>
            <td class="span1">
                <div><a id="todayUnpaidOnRisk" href="javascript:void(0);" onclick="open_summary(this);">0</a></div>
            </td>
            <td class="span3">
                <div>Invoice tunai yang belum bayar</div>
            </td>
            <td class="span1">
                <div><a id="todayUnpaidInvoice" href="javascript:void(0);" onclick="open_summary(this);">0</a></div>
            </td>
        </tr>
	</tbody>
</table>

<g:javascript>
var open_summary;
$(function(){
	$('#spinner').fadeIn(1);
	$.ajax({type:'GET', url:'${request.contextPath}/menu/todaySummary',
   			success:function(data,textStatus){
				if(data.status == 'ok'){
					$('#infoTeknisi').html(data.infoTeknisi);
					$('#todayWaitingForDelivery').html(data.todayWaitingForDelivery);
					$('#todayUnitEntry').html(data.todayUnitEntry);
					$('#todayNonOntimeDelivery').html(data.todayNonOntimeDelivery);
					$('#todayAppointment').html(data.todayAppointment);
					$('#todayUnitInvoice').html(data.todayUnitInvoice);
					$('#todayWalkIn').html(data.todayWalkIn);
					$('#todaySettlement').html(data.todaySettlement);
					$('#todayNoShowAppointment').html(data.todayNoShowAppointment);
					$('#tommorowAppointment').html(data.tommorowAppointment);
					$('#todayUnpaidInvoice').html(data.todayUnpaidInvoice);
					$('#todayUnpaidOnRisk').html(data.todayUnpaidOnRisk);
				}
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
   		
   	open_summary = function(action){
   	    console.log(action)
        $('#main-content').empty();
        $('#spinner').fadeIn(1);
        $.ajax({url: '${request.contextPath}/menu/'+action.id,
				type: "GET",
				dataType: "html",
                complete : function (req, err) {
                    $('#main-content').append(req.responseText);
                     $('#spinner').fadeOut();
                     window.location.href='#/summaryDetail';
                   }
        });
    }
});
</g:javascript>
