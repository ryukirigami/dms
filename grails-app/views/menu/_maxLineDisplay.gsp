<%@ page import="com.kombos.baseapp.AppSettingParamService; com.kombos.baseapp.AppSettingParam" %>
    <%
        def maxLineDisplay
        AppSettingParam appSettingParam = AppSettingParam.findByCode(AppSettingParamService.MAX_NUMBER_LINES_DISPLAYED)
        if(appSettingParam.value == null){
            maxLineDisplay = appSettingParam.defaultValue
        }else{
            if(appSettingParam.value.length()==0){
                maxLineDisplay = appSettingParam.defaultValue
            }else{
                maxLineDisplay = appSettingParam.value
            }
        }
    %>
<script type="text/javascript">
    var maxLineDisplay = <%=maxLineDisplay%>;

    function checkNumber(n) {
        var number = !isNaN(parseFloat(n.value)) && isFinite(n.value);
        if(!number){
            // alert("tanggalFeedbackEnd !");
            n.focus();
            n.value = '';
        }
    }
</script>