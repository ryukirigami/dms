<%@ page import="com.kombos.administrasi.KategoriKendaraan; com.kombos.administrasi.Kategori; com.kombos.administrasi.CompanyDealer; com.kombos.administrasi.TarifCBUNTAM" %>

<g:javascript>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: tarifCBUNTAMInstance, field: 'companyDealer', 'error')} required">
	<label class="control-label" for="companyDealer">
		<g:message code="tarifCBUNTAM.companyDealer.label" default="Company Dealer" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="companyDealer" name="companyDealer.id" from="${CompanyDealer.createCriteria().list(){eq("staDel","0"); order("m011NamaWorkshop", "asc")}}" optionKey="id" required="" value="${tarifCBUNTAMInstance?.companyDealer?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: tarifCBUNTAMInstance, field: 'kategori', 'error')} required">
	<label class="control-label" for="kategori">
		<g:message code="tarifCBUNTAM.kategori.label" default="Kategori" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="kategori" name="kategori.id" from="${KategoriKendaraan.createCriteria().list(){order("m101NamaKategori","asc"); eq("staDel","0") }}" optionKey="id" required="" value="${tarifCBUNTAMInstance?.kategori?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: tarifCBUNTAMInstance, field: 't113bTMT', 'error')} required">
	<label class="control-label" for="t113bTMT">
		<g:message code="tarifCBUNTAM.t113bTMT.label" default="Tanggal Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<ba:datePicker name="t113bTMT" precision="day"  value="${tarifCBUNTAMInstance?.t113bTMT}"  format="yyyy-MM-dd" required="true"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: tarifCBUNTAMInstance, field: 't113bTarifPerjamCBUNTAMSB', 'error')} required">
	<label class="control-label" for="t113bTarifPerjamCBUNTAMSB">
		<g:message code="tarifCBUNTAM.t113bTarifPerjamCBUNTAMSB.label" default="Tarif Per Jam CBU NTAM SB" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t113bTarifPerjamCBUNTAMSB" maxlength="12" onkeypress="return isNumberKey(event)" value="${fieldValue(bean: tarifCBUNTAMInstance, field: 't113bTarifPerjamCBUNTAMSB')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: tarifCBUNTAMInstance, field: 't113bTarifPerjamCBUNTAMGR', 'error')} required">
	<label class="control-label" for="t113bTarifPerjamCBUNTAMGR">
		<g:message code="tarifCBUNTAM.t113bTarifPerjamCBUNTAMGR.label" default="Tarif Per Jam CBU NTAM GR" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t113bTarifPerjamCBUNTAMGR" maxlength="12" onkeypress="return isNumberKey(event)" value="${fieldValue(bean: tarifCBUNTAMInstance, field: 't113bTarifPerjamCBUNTAMGR')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: tarifCBUNTAMInstance, field: 't113bTarifPerjamCBUNTAMBP', 'error')} required">
	<label class="control-label" for="t113bTarifPerjamCBUNTAMBP">
		<g:message code="tarifCBUNTAM.t113bTarifPerjamCBUNTAMBP.label" default="Tarif Per Jam CB UNTAM BP" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t113bTarifPerjamCBUNTAMBP" onkeypress="return isNumberKey(event)" maxlength="12" value="${fieldValue(bean: tarifCBUNTAMInstance, field: 't113bTarifPerjamCBUNTAMBP')}" required=""/>
	</div>
</div>


<g:javascript>
    document.getElementById("companyDealer").focus();
</g:javascript>