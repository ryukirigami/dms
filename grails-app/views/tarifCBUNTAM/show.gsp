

<%@ page import="com.kombos.administrasi.TarifCBUNTAM" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'tarifCBUNTAM.label', default: 'Tarif Per Jam CBU NTAM')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteTarifCBUNTAM;

$(function(){ 
	deleteTarifCBUNTAM=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/tarifCBUNTAM/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadTarifCBUNTAMTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-tarifCBUNTAM" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="tarifCBUNTAM"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${tarifCBUNTAMInstance?.companyDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="companyDealer-label" class="property-label"><g:message
					code="tarifCBUNTAM.companyDealer.label" default="Company Dealer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="companyDealer-label">
						%{--<ba:editableValue
								bean="${tarifCBUNTAMInstance}" field="companyDealer"
								url="${request.contextPath}/TarifCBUNTAM/updatefield" type="text"
								title="Enter companyDealer" onsuccess="reloadTarifCBUNTAMTable();" />--}%
							
								${tarifCBUNTAMInstance?.companyDealer?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tarifCBUNTAMInstance?.kategori}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kategori-label" class="property-label"><g:message
					code="tarifCBUNTAM.kategori.label" default="Kategori" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kategori-label">
						%{--<ba:editableValue
								bean="${tarifCBUNTAMInstance}" field="kategori"
								url="${request.contextPath}/TarifCBUNTAM/updatefield" type="text"
								title="Enter kategori" onsuccess="reloadTarifCBUNTAMTable();" />--}%
							
								${tarifCBUNTAMInstance?.kategori?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tarifCBUNTAMInstance?.t113bTMT}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t113bTMT-label" class="property-label"><g:message
					code="tarifCBUNTAM.t113bTMT.label" default="Tanggal Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t113bTMT-label">
						%{--<ba:editableValue
								bean="${tarifCBUNTAMInstance}" field="t113bTMT"
								url="${request.contextPath}/TarifCBUNTAM/updatefield" type="text"
								title="Enter t113bTMT" onsuccess="reloadTarifCBUNTAMTable();" />--}%
							
								<g:formatDate date="${tarifCBUNTAMInstance?.t113bTMT}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tarifCBUNTAMInstance?.t113bTarifPerjamCBUNTAMSB}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t113bTarifPerjamCBUNTAMSB-label" class="property-label"><g:message
					code="tarifCBUNTAM.t113bTarifPerjamCBUNTAMSB.label" default="Tarif Per Jam CBU NTAM SB" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t113bTarifPerjamCBUNTAMSB-label">
						%{--<ba:editableValue
								bean="${tarifCBUNTAMInstance}" field="t113bTarifPerjamCBUNTAMSB"
								url="${request.contextPath}/TarifCBUNTAM/updatefield" type="text"
								title="Enter t113bTarifPerjamCBUNTAMSB" onsuccess="reloadTarifCBUNTAMTable();" />--}%
							
								<g:fieldValue bean="${tarifCBUNTAMInstance}" field="t113bTarifPerjamCBUNTAMSB"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tarifCBUNTAMInstance?.t113bTarifPerjamCBUNTAMGR}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t113bTarifPerjamCBUNTAMGR-label" class="property-label"><g:message
					code="tarifCBUNTAM.t113bTarifPerjamCBUNTAMGR.label" default="Tarif Per Jam CBU NTAM GR" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t113bTarifPerjamCBUNTAMGR-label">
						%{--<ba:editableValue
								bean="${tarifCBUNTAMInstance}" field="t113bTarifPerjamCBUNTAMGR"
								url="${request.contextPath}/TarifCBUNTAM/updatefield" type="text"
								title="Enter t113bTarifPerjamCBUNTAMGR" onsuccess="reloadTarifCBUNTAMTable();" />--}%
							
								<g:fieldValue bean="${tarifCBUNTAMInstance}" field="t113bTarifPerjamCBUNTAMGR"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${tarifCBUNTAMInstance?.t113bTarifPerjamCBUNTAMBP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t113bTarifPerjamCBUNTAMBP-label" class="property-label"><g:message
					code="tarifCBUNTAM.t113bTarifPerjamCBUNTAMBP.label" default="Tarif Per Jam CBU NTAM BP" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t113bTarifPerjamCBUNTAMBP-label">
						%{--<ba:editableValue
								bean="${tarifCBUNTAMInstance}" field="t113bTarifPerjamCBUNTAMBP"
								url="${request.contextPath}/TarifCBUNTAM/updatefield" type="text"
								title="Enter t113bTarifPerjamCBUNTAMBP" onsuccess="reloadTarifCBUNTAMTable();" />--}%
							
								<g:fieldValue bean="${tarifCBUNTAMInstance}" field="t113bTarifPerjamCBUNTAMBP"/>
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${tarifCBUNTAMInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="dealerPenjual.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${tarifCBUNTAMInstance}" field="dateCreated"
                                url="${request.contextPath}/dealerPenjual/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloaddealerPenjualTable();" />--}%

                        <g:formatDate date="${tarifCBUNTAMInstance?.dateCreated}" type="datetime" style="MEDIUM" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${tarifCBUNTAMInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="dealerPenjual.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${tarifCBUNTAMInstance}" field="createdBy"
                                url="${request.contextPath}/dealerPenjual/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloaddealerPenjualTable();" />--}%

                        <g:fieldValue bean="${tarifCBUNTAMInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${tarifCBUNTAMInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="dealerPenjual.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${tarifCBUNTAMInstance}" field="lastUpdated"
                                url="${request.contextPath}/dealerPenjual/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloaddealerPenjualTable();" />--}%

                        <g:formatDate date="${tarifCBUNTAMInstance?.lastUpdated}" type="datetime" style="MEDIUM"  />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${tarifCBUNTAMInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="dealerPenjual.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${tarifCBUNTAMInstance}" field="updatedBy"
                                url="${request.contextPath}/dealerPenjual/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloaddealerPenjualTable();" />--}%

                        <g:fieldValue bean="${tarifCBUNTAMInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${tarifCBUNTAMInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="dealerPenjual.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${tarifCBUNTAMInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/dealerPenjual/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloaddealerPenjualTable();" />--}%

                        <g:fieldValue bean="${tarifCBUNTAMInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>
			

			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${tarifCBUNTAMInstance?.id}"
					update="[success:'tarifCBUNTAM-form',failure:'tarifCBUNTAM-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteTarifCBUNTAM('${tarifCBUNTAMInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
