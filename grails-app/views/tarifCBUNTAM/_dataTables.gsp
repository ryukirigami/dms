
<%@ page import="com.kombos.administrasi.TarifCBUNTAM" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="tarifCBUNTAM_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="tarifCBUNTAM.companyDealer.label" default="Company Dealer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="tarifCBUNTAM.kategori.label" default="Kategori" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="tarifCBUNTAM.t113bTMT.label" default="Tanggal Berlaku" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="tarifCBUNTAM.t113bTarifPerjamCBUNTAMSB.label" default="Tarif Perjam CBU NTAM SB" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="tarifCBUNTAM.t113bTarifPerjamCBUNTAMGR.label" default="Tarif Perjam CBU NTAM GR" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="tarifCBUNTAM.t113bTarifPerjamCBUNTAMBP.label" default="Tarif Perjam CBU NTAM BP" /></div>
			</th>


		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_companyDealer" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_kategori" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_kategori" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t113bTMT" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_t113bTMT" value="date.struct">
					<input type="hidden" name="search_t113bTMT_day" id="search_t113bTMT_day" value="">
					<input type="hidden" name="search_t113bTMT_month" id="search_t113bTMT_month" value="">
					<input type="hidden" name="search_t113bTMT_year" id="search_t113bTMT_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t113bTMT_dp" value="" id="search_t113bTMT" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t113bTarifPerjamCBUNTAMSB" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t113bTarifPerjamCBUNTAMSB" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t113bTarifPerjamCBUNTAMGR" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t113bTarifPerjamCBUNTAMGR" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t113bTarifPerjamCBUNTAMBP" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t113bTarifPerjamCBUNTAMBP" class="search_init" />
				</div>
			</th>
	



		</tr>
	</thead>
</table>

<g:javascript>
var TarifCBUNTAMTable;
var reloadTarifCBUNTAMTable;
$(function(){
	
	reloadTarifCBUNTAMTable = function() {
		TarifCBUNTAMTable.fnDraw();
	}

	var recordstarifCBUNTAMperpage = [];//new Array();
    var antarifCBUNTAMSelected;
    var jmlRectarifCBUNTAMPerPage=0;
    var id;


	
	$('#search_t113bTMT').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t113bTMT_day').val(newDate.getDate());
			$('#search_t113bTMT_month').val(newDate.getMonth()+1);
			$('#search_t113bTMT_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			TarifCBUNTAMTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	TarifCBUNTAMTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	TarifCBUNTAMTable = $('#tarifCBUNTAM_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rstarifCBUNTAM = $("#tarifCBUNTAM_datatables tbody .row-select");
            var jmltarifCBUNTAMCek = 0;
            var nRow;
            var idRec;
            rstarifCBUNTAM.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordstarifCBUNTAMperpage[idRec]=="1"){
                    jmltarifCBUNTAMCek = jmltarifCBUNTAMCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordstarifCBUNTAMperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRectarifCBUNTAMPerPage = rstarifCBUNTAM.length;
            if(jmltarifCBUNTAMCek==jmlRectarifCBUNTAMPerPage && jmlRectarifCBUNTAMPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "companyDealer",
	"mDataProp": "companyDealer",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "kategori",
	"mDataProp": "kategori",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t113bTMT",
	"mDataProp": "t113bTMT",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t113bTarifPerjamCBUNTAMSB",
	"mDataProp": "t113bTarifPerjamCBUNTAMSB",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t113bTarifPerjamCBUNTAMGR",
	"mDataProp": "t113bTarifPerjamCBUNTAMGR",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t113bTarifPerjamCBUNTAMBP",
	"mDataProp": "t113bTarifPerjamCBUNTAMBP",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}
	
						var kategori = $('#filter_kategori input').val();
						if(kategori){
							aoData.push(
									{"name": 'sCriteria_kategori', "value": kategori}
							);
						}

						var t113bTMT = $('#search_t113bTMT').val();
						var t113bTMTDay = $('#search_t113bTMT_day').val();
						var t113bTMTMonth = $('#search_t113bTMT_month').val();
						var t113bTMTYear = $('#search_t113bTMT_year').val();
						
						if(t113bTMT){
							aoData.push(
									{"name": 'sCriteria_t113bTMT', "value": "date.struct"},
									{"name": 'sCriteria_t113bTMT_dp', "value": t113bTMT},
									{"name": 'sCriteria_t113bTMT_day', "value": t113bTMTDay},
									{"name": 'sCriteria_t113bTMT_month', "value": t113bTMTMonth},
									{"name": 'sCriteria_t113bTMT_year', "value": t113bTMTYear}
							);
						}
	
						var t113bTarifPerjamCBUNTAMSB = $('#filter_t113bTarifPerjamCBUNTAMSB input').val();
						if(t113bTarifPerjamCBUNTAMSB){
							aoData.push(
									{"name": 'sCriteria_t113bTarifPerjamCBUNTAMSB', "value": t113bTarifPerjamCBUNTAMSB}
							);
						}
	
						var t113bTarifPerjamCBUNTAMGR = $('#filter_t113bTarifPerjamCBUNTAMGR input').val();
						if(t113bTarifPerjamCBUNTAMGR){
							aoData.push(
									{"name": 'sCriteria_t113bTarifPerjamCBUNTAMGR', "value": t113bTarifPerjamCBUNTAMGR}
							);
						}
	
						var t113bTarifPerjamCBUNTAMBP = $('#filter_t113bTarifPerjamCBUNTAMBP input').val();
						if(t113bTarifPerjamCBUNTAMBP){
							aoData.push(
									{"name": 'sCriteria_t113bTarifPerjamCBUNTAMBP', "value": t113bTarifPerjamCBUNTAMBP}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
	  $('.select-all').click(function(e) {

        $("#tarifCBUNTAM_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordstarifCBUNTAMperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordstarifCBUNTAMperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#tarifCBUNTAM_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordstarifCBUNTAMperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            antarifCBUNTAMSelected = TarifCBUNTAMTable.$('tr.row_selected');
            if(jmlRectarifCBUNTAMPerPage == antarifCBUNTAMSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordstarifCBUNTAMperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
