<%@ page import="com.kombos.administrasi.Audio" %>



<div class="control-group fieldcontain ${hasErrors(bean: audioInstance, field: 'm405Text', 'error')} required">
	<label class="control-label" for="m405Text">
		<g:message code="audio.m405Text.label" default="Text" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
    <g:hiddenField name="id" id="id" value="${audioInstance?.id}"/>
	<g:textField name="m405Text" maxlength="50" required="" value="${audioInstance?.m405Text}"/>
	</div>
</div>
<g:javascript>
    document.getElementById("m405Text").focus();
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: audioInstance, field: 'audioPath', 'error')} required">
	<label class="control-label" for="audioPath">
		<g:message code="audio.audioPath.label" default="Suara" />
        <span class="required-indicator">*</span>
	</label>
	<div class="controls">
	  <input type="file" id="audioPath" name="audioPath"/> &nbsp;
        <g:if test="${audioInstance}">
        </g:if>
   	</div>
</div>


