<%@ page import="com.kombos.administrasi.CompanyDealer"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="baseapplayout" />
<g:javascript disappointmentGrition="head">
	$(function(){		
        $('#filter_periode2').hide();
        $('#filter_periode3').hide();
        $('#filter_periode').show();
        $('#bulan1').change(function(){
            $('#bulan2').val($('#bulan1').val());
        });
        $('#bulan2').change(function(){
            if(parseInt($('#bulan1').val()) > parseInt($('#bulan2').val())){
                $('#bulan2').val($('#bulan1').val());
            }
        });
        $('#namaReport').change(function(){        
        	if($('#namaReport').val() == "12" ||  $('#namaReport').val() == "13" ||
				$('#namaReport').val() == "20" || $('#namaReport').val() == "21" ||
				$('#namaReport').val() == "28" || $('#namaReport').val() == "29" ||
				$('#namaReport').val() == "34" || $('#namaReport').val() == "37"){
				$('#filter_periode2').show();
				$('#filter_periode3').hide();
				$('#filter_periode').hide();
        	} else
			if($('#namaReport').val() == "14" || $('#namaReport').val() == "15" ||
			   $('#namaReport').val() == "22" || $('#namaReport').val() == "23" ||
			   $('#namaReport').val() == "30" || $('#namaReport').val() == "31"){
				$('#filter_periode2').hide();
				$('#filter_periode3').show();
				$('#filter_periode').hide();
			} else {
				$('#filter_periode2').hide();
				$('#filter_periode3').hide();
				$('#filter_periode').show();
			}
        });


	    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    });
    var checkin = $('#search_tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
                checkin.hide();
                $('#search_tanggal2')[0].focus();
            }).data('datepicker');

    var checkout = $('#search_tanggal2').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
                checkout.hide();
    }).data('datepicker');

    previewData = function(){
            var namaReport = $('#namaReport').val();
            var workshop = $('#workshop').val();
            var search_tanggal = $('#search_tanggal').val();
            var search_tanggal2 = $('#search_tanggal2').val();
            var bulan1 = $('#bulan1').val();
            var bulan2= $('#bulan2').val();
            var tahun = $('#tahun').val();
			var jenisPekerjaan = "";
			var jenisProses = "";
    		var format = $('input[name="formatReport"]:checked').val();
            if(namaReport == "" || namaReport == null){
                alert('Harap Isi Data Dengan Lengkap');
                return;
            }
			
			$("input[name='jenisPekerjaan']").each(function (){
				if ($(this).attr("checked")) {
					jenisPekerjaan += $(this).val() + ',';
				}			
			});
			jenisPekerjaan = jenisPekerjaan.substr(0, jenisPekerjaan.length - 1);		
			
			$("input[name='jenisProses']").each(function (){
				if ($(this).attr("checked")) {
					jenisProses += $(this).val() + ',';
				}			
			});
			jenisProses = jenisProses.substr(0, jenisProses.length - 1);		
			
			window.location = "${request.contextPath}/production_bp/previewData?namaReport="+namaReport+"&workshop="+workshop+"&bulan1="+bulan1+"&bulan2="+bulan2+"&tahun="+tahun+
						"&tanggal="+search_tanggal+"&tanggal2="+search_tanggal2+"&jenisPekerjaan="+jenisPekerjaan+"&jenisProses="+jenisProses+"&format="+format;
    }


</g:javascript>

</head>
<body>
	<div class="navbar box-header no-border">Report – Production (BP)
	</div>
	<br>
	<div class="box">
		<div class="span12" id="appointmentGr-table">
			<div class="row-fluid form-horizontal">
				<div id="kiri" class="span6">
                    <div class="control-group">
                        <label class="control-label" for="filter_periode" style="text-align: left;">
                            Format Report
                        </label>
                        <div id="filter_format" class="controls">
                            <g:radioGroup name="formatReport" id="formatReport" values="['x','p']" labels="['Excel','PDF']" value="x" required="" >
                                ${it.radio} ${it.label}
                            </g:radioGroup>
                        </div>
                    </div>
					<div class="control-group">
						<label class="control-label" for="t951Tanggal"
							style="text-align: left;"> Workshop </label>
						%{--<div id="filter_wo" class="controls">--}%
							%{--<g:select name="workshop" id="workshop"--}%
								%{--from="${CompanyDealer.list()}" optionKey="id"--}%
								%{--optionValue="m011NamaWorkshop" style="width: 44%" />--}%
						%{--</div>--}%
                        <div id="filter_wo" class="controls">
                            <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                <g:select name="workshop" id="workshop" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                            </g:if>
                            <g:else>
                                <g:select name="workshop" id="workshop" readonly="" disabled="" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                            </g:else>
                        </div>

                    </div>
					<div class="control-group">
						<label class="control-label" for="t951Tanggal"
							style="text-align: left;"> Kategori Workshop </label>
						<div id="filter_workshop" class="controls">
							&nbsp; <b>Body & Preparation</b>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="t951Tanggal"
							style="text-align: left;"> Nama Report </label>
						<div id="filter_nama" class="controls">
							<select name="namaReport" id="namaReport" size="8"
								style="width: 100%; height: 230px; font-size: 12px;">
								<option value="01">01. BP - Production Lead Time</option>													
								<option value="02">02. BP - Redo Lead Time</option>													
								<option value="03">03. BP - Production Lead Time Detail (Body)</option>													
								<option value="04">04. BP - Production Lead Time Detail (Preparation)</option>													
								<option value="05">05. BP - Production Lead Time Detail (Painting)</option>													
								<option value="06">06. BP - Production Lead Time Detail (Polishing)</option>													
								<option value="07">07. BP - Production Lead Time Detail (Assembly)</option>													
								<option value="08">08. BP - Production Redo Body Daily (Unit)</option>													
								<option value="09">09. BP - Production Redo Body Daily (Panel)</option>													
								<option value="10">10. BP - Production Redo Body Weekly (Unit)</option>													
								<option value="11">11. BP - Production Redo Body Weekly (Panel)</option>													
								<option value="12">12. BP - Production Redo Body Monthly (Unit)</option>													
								<option value="13">13. BP - Production Redo Body Monthly (Panel)</option>													
								<option value="14">14. BP - Production Redo Body Yearly (Unit)</option>													
								<option value="15">15. BP - Production Redo Body Yearly (Panel)</option>		
								<option value="16">16. BP - Production Redo Preparation Daily (Unit)</option>				
								<option value="17">17. BP - Production Redo Preparation Daily (Panel)</option>				
								<option value="18">18. BP - Production Redo Preparation Weekly (Unit)</option>				
								<option value="19">19. BP - Production Redo Preparation Weekly (Panel)</option>				
								<option value="20">20. BP - Production Redo Preparation Monthly (Unit)</option>				
								<option value="21">21. BP - Production Redo Preparation Monthly (Panel)</option>				
								<option value="22">22. BP - Production Redo Preparation Yearly (Unit)</option>				
								<option value="23">23. BP - Production Redo Preparation Yearly (Panel)</option>				
								<option value="24">24. BP - Production Redo Painting Daily (Unit)</option>				
								<option value="25">25. BP - Production Redo Painting Daily (Panel)</option>				
								<option value="26">26. BP - Production Redo Painting Weekly (Unit)</option>				
								<option value="27">27. BP - Production Redo Painting Weekly (Panel)</option>				
								<option value="28">28. BP - Production Redo Painting Monthly (Unit)</option>				
								<option value="29">29. BP - Production Redo Painting Monthly (Panel)</option>				
								<option value="30">30. BP - Production Redo Painting Yearly (Unit)</option>				
								<option value="31">31. BP - Production Redo Painting Yearly (Panel)</option>				
								<option value="32">32. BP - Production Combiboth Productivity (Daily)</option>				
								<option value="33">33. BP - Production Combiboth Productivity (Weekly)</option>				
								<option value="34">34. BP - Production Combiboth Productivity (Monthly)</option>				
								<option value="35">35. BP - Production On Time in Each Process (Daily)</option>				
								<option value="36">36. BP - Production On Time in Each Process (Weekly)</option>				
								<option value="37">37. BP - Production On Time in Each Process (Monthly)</option>				
								<option value="38">38. BP - Technician Productivity</option>				
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="t951Tanggal"
							style="text-align: left;"> Periode </label>
						<div id="filter_periode" class="controls">
							<ba:datePicker id="search_tanggal" name="search_tanggal"
								precision="day" format="dd-MM-yyyy" value="${new Date()}" />
							&nbsp;&nbsp;s.d.&nbsp;&nbsp;
							<ba:datePicker id="search_tanggal2" name="search_tanggal2"
								precision="day" format="dd-MM-yyyy" value="${new Date()+1}" />
						</div>
						<div id="filter_periode2" class="controls">
							%{ def listBulan =
							['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
							}% <select name="bulan1" id="bulan1" style="width: 170px">
								%{ for(int i=0;i<12;i++){ out.print('
								<option value="'+(i+1)+'">'+listBulan.get(i)+'</option>'); } }%
							</select> &nbsp; - &nbsp; <select name="bulan2" id="bulan2"
								style="width: 170px"> %{ for(int i=0;i<12;i++){
								out.print('
								<option value="'+(i+1)+'">'+listBulan.get(i)+'</option>'); } }%
							</select> &nbsp; <select name="tahun" id="tahun" style="width: 106px">
								%{ for(def a = (new Date().format("yyyy")).toInteger();a >= 2000
								;a--){ out.print('
								<option value="'+a+'">'+a+'</option>'); } }%

							</select>
						</div>
						<div id="filter_periode3" class="controls">
							<select name="tahun" id="tahun" style="width: 170px"> %{
								for(def a = (new Date().format("yyyy")).toInteger(); a >= 2000;
								a--) { out.print('
								<option value="'+a+'">'+a+'</option>'); } }%
							</select>
						</div>
					</div>					
				</div>	
				<div id="kanan" class="span6">
					<div class="control-group">
						<label class="control-label" for="workshop"
							style="text-align: left;"> Jenis Pekerjaan </label>
						<div class="controls">
							<g:render template="tableJenisPekerjaan" />
						</div>
					</div>					
					<div class="control-group">
						<label class="control-label" for="workshop"
							style="text-align: left;"> Jenis Proses </label>
						<div class="controls">
							<g:render template="tableJenisProses" />
						</div>
					</div>					
				</div>					
			</div>
			<g:field type="button" onclick="previewData()"
				class="btn btn-primary create" name="preview"
				value="${message(code: 'default.button.upload.label', default: 'Preview')}" />
			<g:field type="button" onclick="window.location.replace('#/home')"
				class="btn btn-cancel cancel" name="cancel"
				value="${message(code: 'default.button.upload.label', default: 'Close')}" />
		</div>
	</div>
</body>
</html>
