

<%@ page import="com.kombos.administrasi.TargetBP" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'targetBP.label', default: 'Target BP')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteTargetBP;

$(function(){ 
	deleteTargetBP=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/targetBP/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadTargetBPTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-targetBP" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="targetBP"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${targetBPInstance?.m036TglBerlaku}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036TglBerlaku-label" class="property-label"><g:message
					code="targetBP.m036TglBerlaku.label" default="Tgl Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036TglBerlaku-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036TglBerlaku"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036TglBerlaku" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:formatDate date="${targetBPInstance?.m036TglBerlaku}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.companyDealer}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="companyDealer-label" class="property-label"><g:message
					code="targetBP.companyDealer.label" default="Company Dealer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="companyDealer-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="companyDealer"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter companyDealer" onsuccess="reloadTargetBPTable();" />--}%
                            <g:fieldValue bean="${targetBPInstance}" field="companyDealer"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="targetBP.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="createdBy"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="targetBP.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="updatedBy"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="targetBP.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="lastUpdProcess"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="targetBP.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="dateCreated"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:formatDate date="${targetBPInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="targetBP.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="lastUpdated"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:formatDate date="${targetBPInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036COGSDeprecation}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036COGSDeprecation-label" class="property-label"><g:message
					code="targetBP.m036COGSDeprecation.label" default="COGSD eprecation" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036COGSDeprecation-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036COGSDeprecation"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036COGSDeprecation" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036COGSDeprecation"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036COGSLabor}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036COGSLabor-label" class="property-label"><g:message
					code="targetBP.m036COGSLabor.label" default="COGSL abor" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036COGSLabor-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036COGSLabor"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036COGSLabor" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036COGSLabor"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036COGSOil}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036COGSOil-label" class="property-label"><g:message
					code="targetBP.m036COGSOil.label" default="COGSO il" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036COGSOil-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036COGSOil"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036COGSOil" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036COGSOil"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036COGSOverHead}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036COGSOverHead-label" class="property-label"><g:message
					code="targetBP.m036COGSOverHead.label" default="COGSO ver Head" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036COGSOverHead-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036COGSOverHead"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036COGSOverHead" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036COGSOverHead"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036COGSPart}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036COGSPart-label" class="property-label"><g:message
					code="targetBP.m036COGSPart.label" default="COGS Part" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036COGSPart-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036COGSPart"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036COGSPart" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036COGSPart"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036COGSSublet}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036COGSSublet-label" class="property-label"><g:message
					code="targetBP.m036COGSSublet.label" default="COGS Sublet" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036COGSSublet-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036COGSSublet"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036COGSSublet" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036COGSSublet"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036COGSTotalSGA}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036COGSTotalSGA-label" class="property-label"><g:message
					code="targetBP.m036COGSTotalSGA.label" default="COGS Total SGA" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036COGSTotalSGA-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036COGSTotalSGA"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036COGSTotalSGA" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036COGSTotalSGA"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036Q1D}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036Q1D-label" class="property-label"><g:message
					code="targetBP.m036Q1D.label" default="Q1 D" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036Q1D-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036Q1D"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036Q1D" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036Q1D"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036Q1S}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036Q1S-label" class="property-label"><g:message
					code="targetBP.m036Q1S.label" default="Q1 S" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036Q1S-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036Q1S"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036Q1S" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036Q1S"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036Q2D}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036Q2D-label" class="property-label"><g:message
					code="targetBP.m036Q2D.label" default="Q2 D" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036Q2D-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036Q2D"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036Q2D" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036Q2D"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036Q2S}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036Q2S-label" class="property-label"><g:message
					code="targetBP.m036Q2S.label" default="Q2 S" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036Q2S-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036Q2S"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036Q2S" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036Q2S"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036Q3D}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036Q3D-label" class="property-label"><g:message
					code="targetBP.m036Q3D.label" default="Q3 D" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036Q3D-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036Q3D"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036Q3D" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036Q3D"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036Q3S}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036Q3S-label" class="property-label"><g:message
					code="targetBP.m036Q3S.label" default="Q3 S" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036Q3S-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036Q3S"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036Q3S" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036Q3S"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036Q4D}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036Q4D-label" class="property-label"><g:message
					code="targetBP.m036Q4D.label" default="Q4 D" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036Q4D-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036Q4D"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036Q4D" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036Q4D"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036Q4S}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036Q4S-label" class="property-label"><g:message
					code="targetBP.m036Q4S.label" default="Q4 S" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036Q4S-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036Q4S"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036Q4S" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036Q4S"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036Q5D}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036Q5D-label" class="property-label"><g:message
					code="targetBP.m036Q5D.label" default="Q5 D" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036Q5D-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036Q5D"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036Q5D" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036Q5D"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036Q5S}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036Q5S-label" class="property-label"><g:message
					code="targetBP.m036Q5S.label" default="Q5 S" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036Q5S-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036Q5S"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036Q5S" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036Q5S"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036Q6D}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036Q6D-label" class="property-label"><g:message
					code="targetBP.m036Q6D.label" default="Q6 D" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036Q6D-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036Q6D"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036Q6D" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036Q6D"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036Q6S}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036Q6S-label" class="property-label"><g:message
					code="targetBP.m036Q6S.label" default="Q6 S" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036Q6S-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036Q6S"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036Q6S" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036Q6S"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036TWCLeadTime}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036TWCLeadTime-label" class="property-label"><g:message
					code="targetBP.m036TWCLeadTime.label" default="TWC Lead Time" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036TWCLeadTime-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036TWCLeadTime"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036TWCLeadTime" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036TWCLeadTime"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036TargetQty}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036TargetQty-label" class="property-label"><g:message
					code="targetBP.m036TargetQty.label" default="Target Qty" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036TargetQty-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036TargetQty"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036TargetQty" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036TargetQty"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036TargetSales}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036TargetSales-label" class="property-label"><g:message
					code="targetBP.m036TargetSales.label" default="Target Sales" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036TargetSales-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036TargetSales"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036TargetSales" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036TargetSales"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036TechReport}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036TechReport-label" class="property-label"><g:message
					code="targetBP.m036TechReport.label" default="Tech Report" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036TechReport-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036TechReport"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036TechReport" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036TechReport"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036ThisMonthD}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036ThisMonthD-label" class="property-label"><g:message
					code="targetBP.m036ThisMonthD.label" default="This Month D" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036ThisMonthD-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036ThisMonthD"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036ThisMonthD" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036ThisMonthD"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036ThisMonthS}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036ThisMonthS-label" class="property-label"><g:message
					code="targetBP.m036ThisMonthS.label" default="This Month S" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036ThisMonthS-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036ThisMonthS"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036ThisMonthS" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036ThisMonthS"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036TotalClaimAmount}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036TotalClaimAmount-label" class="property-label"><g:message
					code="targetBP.m036TotalClaimAmount.label" default="Total Claim Amount" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036TotalClaimAmount-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036TotalClaimAmount"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036TotalClaimAmount" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036TotalClaimAmount"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036TotalClaimItem}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036TotalClaimItem-label" class="property-label"><g:message
					code="targetBP.m036TotalClaimItem.label" default="Total Claim Item" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036TotalClaimItem-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036TotalClaimItem"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036TotalClaimItem" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036TotalClaimItem"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036YTDD}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036YTDD-label" class="property-label"><g:message
					code="targetBP.m036YTDD.label" default="YTDD" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036YTDD-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036YTDD"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036YTDD" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036YTDD"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${targetBPInstance?.m036YTDS}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m036YTDS-label" class="property-label"><g:message
					code="targetBP.m036YTDS.label" default="YTDS" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m036YTDS-label">
						%{--<ba:editableValue
								bean="${targetBPInstance}" field="m036YTDS"
								url="${request.contextPath}/TargetBP/updatefield" type="text"
								title="Enter m036YTDS" onsuccess="reloadTargetBPTable();" />--}%
							
								<g:fieldValue bean="${targetBPInstance}" field="m036YTDS"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${targetBPInstance?.id}"
					update="[success:'targetBP-form',failure:'targetBP-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteTargetBP('${targetBPInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
