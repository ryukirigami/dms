
<%@ page import="com.kombos.administrasi.TargetBP" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="targetBP_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div>&nbsp;<input type="checkbox" name="checkSelect" id="checkSelect" onclick="selectAll();"/>&nbsp;<g:message code="targetBP.m036TglBerlaku.label" default="Tgl Berlaku" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.companyDealer.label" default="Company Dealer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036COGSDeprecation.label" default="COGSD eprecation" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036COGSLabor.label" default="COGSL abor" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036COGSOil.label" default="COGSO il" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036COGSOverHead.label" default="COGSO ver Head" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036COGSPart.label" default="COGSP art" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036COGSSublet.label" default="COGSS ublet" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036COGSTotalSGA.label" default="COGST otal SGA" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036Q1D.label" default="Q1 D" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036Q1S.label" default="Q1 S" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036Q2D.label" default="Q2 D" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036Q2S.label" default="Q2 S" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036Q3D.label" default="Q3 D" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036Q3S.label" default="Q3 S" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036Q4D.label" default="Q4 D" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036Q4S.label" default="Q4 S" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036Q5D.label" default="Q5 D" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036Q5S.label" default="Q5 S" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036Q6D.label" default="Q6 D" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036Q6S.label" default="Q6 S" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036TWCLeadTime.label" default="TWCL ead Time" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036TargetQty.label" default="Target Qty" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036TargetSales.label" default="Target Sales" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036TechReport.label" default="Tech Report" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036ThisMonthD.label" default="This Month D" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036ThisMonthS.label" default="This Month S" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036TotalClaimAmount.label" default="Total Claim Amount" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036TotalClaimItem.label" default="Total Claim Item" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036YTDD.label" default="YTDD" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="targetBP.m036YTDS.label" default="YTDS" /></div>
			</th>

		
		</tr>
		<tr>
		

			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036TglBerlaku" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_m036TglBerlaku" value="date.struct">
					<input type="hidden" name="search_m036TglBerlaku_day" id="search_m036TglBerlaku_day" value="">
					<input type="hidden" name="search_m036TglBerlaku_month" id="search_m036TglBerlaku_month" value="">
					<input type="hidden" name="search_m036TglBerlaku_year" id="search_m036TglBerlaku_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_m036TglBerlaku_dp" value="" id="search_m036TglBerlaku" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_companyDealer" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036COGSDeprecation" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036COGSDeprecation" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036COGSLabor" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036COGSLabor" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036COGSOil" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036COGSOil" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036COGSOverHead" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036COGSOverHead" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036COGSPart" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036COGSPart" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036COGSSublet" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036COGSSublet" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036COGSTotalSGA" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036COGSTotalSGA" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036Q1D" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036Q1D" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036Q1S" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036Q1S" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036Q2D" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036Q2D" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036Q2S" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036Q2S" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036Q3D" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036Q3D" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036Q3S" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036Q3S" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036Q4D" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036Q4D" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036Q4S" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036Q4S" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036Q5D" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036Q5D" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036Q5S" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036Q5S" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036Q6D" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036Q6D" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036Q6S" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036Q6S" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036TWCLeadTime" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036TWCLeadTime" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036TargetQty" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036TargetQty" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036TargetSales" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036TargetSales" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036TechReport" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036TechReport" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036ThisMonthD" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036ThisMonthD" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036ThisMonthS" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036ThisMonthS" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036TotalClaimAmount" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036TotalClaimAmount" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036TotalClaimItem" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036TotalClaimItem" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036YTDD" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036YTDD" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_m036YTDS" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_m036YTDS" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var targetBPTable;
var reloadTargetBPTable;
$(function(){
	
	reloadTargetBPTable = function() {
		targetBPTable.fnDraw();
	}

	
	$('#search_m036TglBerlaku').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_m036TglBerlaku_day').val(newDate.getDate());
			$('#search_m036TglBerlaku_month').val(newDate.getMonth()+1);
			$('#search_m036TglBerlaku_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			targetBPTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	targetBPTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	targetBPTable = $('#targetBP_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "m036TglBerlaku",
	"mDataProp": "m036TglBerlaku",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "companyDealer",
	"mDataProp": "companyDealer",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036COGSDeprecation",
	"mDataProp": "m036COGSDeprecation",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036COGSLabor",
	"mDataProp": "m036COGSLabor",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036COGSOil",
	"mDataProp": "m036COGSOil",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036COGSOverHead",
	"mDataProp": "m036COGSOverHead",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036COGSPart",
	"mDataProp": "m036COGSPart",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036COGSSublet",
	"mDataProp": "m036COGSSublet",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036COGSTotalSGA",
	"mDataProp": "m036COGSTotalSGA",
	"aTargets": [11],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036Q1D",
	"mDataProp": "m036Q1D",
	"aTargets": [12],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036Q1S",
	"mDataProp": "m036Q1S",
	"aTargets": [13],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036Q2D",
	"mDataProp": "m036Q2D",
	"aTargets": [14],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036Q2S",
	"mDataProp": "m036Q2S",
	"aTargets": [15],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036Q3D",
	"mDataProp": "m036Q3D",
	"aTargets": [16],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036Q3S",
	"mDataProp": "m036Q3S",
	"aTargets": [17],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036Q4D",
	"mDataProp": "m036Q4D",
	"aTargets": [18],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036Q4S",
	"mDataProp": "m036Q4S",
	"aTargets": [19],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036Q5D",
	"mDataProp": "m036Q5D",
	"aTargets": [20],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036Q5S",
	"mDataProp": "m036Q5S",
	"aTargets": [21],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036Q6D",
	"mDataProp": "m036Q6D",
	"aTargets": [22],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036Q6S",
	"mDataProp": "m036Q6S",
	"aTargets": [23],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036TWCLeadTime",
	"mDataProp": "m036TWCLeadTime",
	"aTargets": [24],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036TargetQty",
	"mDataProp": "m036TargetQty",
	"aTargets": [25],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036TargetSales",
	"mDataProp": "m036TargetSales",
	"aTargets": [26],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036TechReport",
	"mDataProp": "m036TechReport",
	"aTargets": [27],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036ThisMonthD",
	"mDataProp": "m036ThisMonthD",
	"aTargets": [28],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036ThisMonthS",
	"mDataProp": "m036ThisMonthS",
	"aTargets": [29],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036TotalClaimAmount",
	"mDataProp": "m036TotalClaimAmount",
	"aTargets": [30],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036TotalClaimItem",
	"mDataProp": "m036TotalClaimItem",
	"aTargets": [31],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036YTDD",
	"mDataProp": "m036YTDD",
	"aTargets": [32],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m036YTDS",
	"mDataProp": "m036YTDS",
	"aTargets": [33],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var m036TglBerlaku = $('#search_m036TglBerlaku').val();
						var m036TglBerlakuDay = $('#search_m036TglBerlaku_day').val();
						var m036TglBerlakuMonth = $('#search_m036TglBerlaku_month').val();
						var m036TglBerlakuYear = $('#search_m036TglBerlaku_year').val();
						
						if(m036TglBerlaku){
							aoData.push(
									{"name": 'sCriteria_m036TglBerlaku', "value": "date.struct"},
									{"name": 'sCriteria_m036TglBerlaku_dp', "value": m036TglBerlaku},
									{"name": 'sCriteria_m036TglBerlaku_day', "value": m036TglBerlakuDay},
									{"name": 'sCriteria_m036TglBerlaku_month', "value": m036TglBerlakuMonth},
									{"name": 'sCriteria_m036TglBerlaku_year', "value": m036TglBerlakuYear}
							);
						}
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}
	
						var m036COGSDeprecation = $('#filter_m036COGSDeprecation input').val();
						if(m036COGSDeprecation){
							aoData.push(
									{"name": 'sCriteria_m036COGSDeprecation', "value": m036COGSDeprecation}
							);
						}
	
						var m036COGSLabor = $('#filter_m036COGSLabor input').val();
						if(m036COGSLabor){
							aoData.push(
									{"name": 'sCriteria_m036COGSLabor', "value": m036COGSLabor}
							);
						}
	
						var m036COGSOil = $('#filter_m036COGSOil input').val();
						if(m036COGSOil){
							aoData.push(
									{"name": 'sCriteria_m036COGSOil', "value": m036COGSOil}
							);
						}
	
						var m036COGSOverHead = $('#filter_m036COGSOverHead input').val();
						if(m036COGSOverHead){
							aoData.push(
									{"name": 'sCriteria_m036COGSOverHead', "value": m036COGSOverHead}
							);
						}
	
						var m036COGSPart = $('#filter_m036COGSPart input').val();
						if(m036COGSPart){
							aoData.push(
									{"name": 'sCriteria_m036COGSPart', "value": m036COGSPart}
							);
						}
	
						var m036COGSSublet = $('#filter_m036COGSSublet input').val();
						if(m036COGSSublet){
							aoData.push(
									{"name": 'sCriteria_m036COGSSublet', "value": m036COGSSublet}
							);
						}
	
						var m036COGSTotalSGA = $('#filter_m036COGSTotalSGA input').val();
						if(m036COGSTotalSGA){
							aoData.push(
									{"name": 'sCriteria_m036COGSTotalSGA', "value": m036COGSTotalSGA}
							);
						}
	
						var m036Q1D = $('#filter_m036Q1D input').val();
						if(m036Q1D){
							aoData.push(
									{"name": 'sCriteria_m036Q1D', "value": m036Q1D}
							);
						}
	
						var m036Q1S = $('#filter_m036Q1S input').val();
						if(m036Q1S){
							aoData.push(
									{"name": 'sCriteria_m036Q1S', "value": m036Q1S}
							);
						}
	
						var m036Q2D = $('#filter_m036Q2D input').val();
						if(m036Q2D){
							aoData.push(
									{"name": 'sCriteria_m036Q2D', "value": m036Q2D}
							);
						}
	
						var m036Q2S = $('#filter_m036Q2S input').val();
						if(m036Q2S){
							aoData.push(
									{"name": 'sCriteria_m036Q2S', "value": m036Q2S}
							);
						}
	
						var m036Q3D = $('#filter_m036Q3D input').val();
						if(m036Q3D){
							aoData.push(
									{"name": 'sCriteria_m036Q3D', "value": m036Q3D}
							);
						}
	
						var m036Q3S = $('#filter_m036Q3S input').val();
						if(m036Q3S){
							aoData.push(
									{"name": 'sCriteria_m036Q3S', "value": m036Q3S}
							);
						}
	
						var m036Q4D = $('#filter_m036Q4D input').val();
						if(m036Q4D){
							aoData.push(
									{"name": 'sCriteria_m036Q4D', "value": m036Q4D}
							);
						}
	
						var m036Q4S = $('#filter_m036Q4S input').val();
						if(m036Q4S){
							aoData.push(
									{"name": 'sCriteria_m036Q4S', "value": m036Q4S}
							);
						}
	
						var m036Q5D = $('#filter_m036Q5D input').val();
						if(m036Q5D){
							aoData.push(
									{"name": 'sCriteria_m036Q5D', "value": m036Q5D}
							);
						}
	
						var m036Q5S = $('#filter_m036Q5S input').val();
						if(m036Q5S){
							aoData.push(
									{"name": 'sCriteria_m036Q5S', "value": m036Q5S}
							);
						}
	
						var m036Q6D = $('#filter_m036Q6D input').val();
						if(m036Q6D){
							aoData.push(
									{"name": 'sCriteria_m036Q6D', "value": m036Q6D}
							);
						}
	
						var m036Q6S = $('#filter_m036Q6S input').val();
						if(m036Q6S){
							aoData.push(
									{"name": 'sCriteria_m036Q6S', "value": m036Q6S}
							);
						}
	
						var m036TWCLeadTime = $('#filter_m036TWCLeadTime input').val();
						if(m036TWCLeadTime){
							aoData.push(
									{"name": 'sCriteria_m036TWCLeadTime', "value": m036TWCLeadTime}
							);
						}
	
						var m036TargetQty = $('#filter_m036TargetQty input').val();
						if(m036TargetQty){
							aoData.push(
									{"name": 'sCriteria_m036TargetQty', "value": m036TargetQty}
							);
						}
	
						var m036TargetSales = $('#filter_m036TargetSales input').val();
						if(m036TargetSales){
							aoData.push(
									{"name": 'sCriteria_m036TargetSales', "value": m036TargetSales}
							);
						}
	
						var m036TechReport = $('#filter_m036TechReport input').val();
						if(m036TechReport){
							aoData.push(
									{"name": 'sCriteria_m036TechReport', "value": m036TechReport}
							);
						}
	
						var m036ThisMonthD = $('#filter_m036ThisMonthD input').val();
						if(m036ThisMonthD){
							aoData.push(
									{"name": 'sCriteria_m036ThisMonthD', "value": m036ThisMonthD}
							);
						}
	
						var m036ThisMonthS = $('#filter_m036ThisMonthS input').val();
						if(m036ThisMonthS){
							aoData.push(
									{"name": 'sCriteria_m036ThisMonthS', "value": m036ThisMonthS}
							);
						}
	
						var m036TotalClaimAmount = $('#filter_m036TotalClaimAmount input').val();
						if(m036TotalClaimAmount){
							aoData.push(
									{"name": 'sCriteria_m036TotalClaimAmount', "value": m036TotalClaimAmount}
							);
						}
	
						var m036TotalClaimItem = $('#filter_m036TotalClaimItem input').val();
						if(m036TotalClaimItem){
							aoData.push(
									{"name": 'sCriteria_m036TotalClaimItem', "value": m036TotalClaimItem}
							);
						}
	
						var m036YTDD = $('#filter_m036YTDD input').val();
						if(m036YTDD){
							aoData.push(
									{"name": 'sCriteria_m036YTDD', "value": m036YTDD}
							);
						}
	
						var m036YTDS = $('#filter_m036YTDS input').val();
						if(m036YTDS){
							aoData.push(
									{"name": 'sCriteria_m036YTDS', "value": m036YTDS}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
