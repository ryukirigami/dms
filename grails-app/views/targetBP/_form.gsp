<%@ page import="com.kombos.administrasi.CompanyDealer; com.kombos.administrasi.TargetBP" %>


<g:javascript disposition="head">
    $(function(){
        $("#m036TglBerlaku_date").datepicker();
    });
</g:javascript>
<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036TglBerlaku', 'error')} required">
	<label class="control-label" for="m036TglBerlaku">
		<g:message code="targetBP.m036TglBerlaku.label" default="Tgl Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	%{--<g:datePicker name="m036TglBerlaku" precision="day"  value="${targetBPInstance?.m036TglBerlaku}"  />--}%
        %{
            if(request.getRequestURI().contains("edit")){
        }%
        <input type="hidden" name="m036TglBerlaku" id="m036TglBerlaku" value="${targetBPInstance?.m036TglBerlaku}" />
        <input type="text" disabled="" name="m036TglBerlaku_temp" id="m036TglBerlaku_temp" value="${targetBPInstance?.m036TglBerlaku}" />
        %{
            }else{
        }%
        <input type="hidden" name="m036TglBerlaku" id="m036TglBerlaku" value="${targetBPInstance?.m036TglBerlaku}" />
        %{--<input type="text" id="m036TglBerlaku_date" name="m036TglBerlaku_date"/>--}%
        <ba:datePicker name="m036TglBerlaku_date" id="m036TglBerlaku_date" precision="day" format="dd/mm/yyyy" required="true"/>
        %{
            }
        }%
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'companyDealer', 'error')} required">
	<label class="control-label" for="companyDealer">
		<g:message code="targetBP.companyDealer.label" default="Company Dealer" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="companyDealer" name="companyDealer.id" from="${CompanyDealer.list()}" optionKey="id" required="" value="${targetBPInstance?.companyDealer?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036COGSDeprecation', 'error')} required">
	<label class="control-label" for="m036COGSDeprecation">
		<g:message code="targetBP.m036COGSDeprecation.label" default="COGS Deprecation" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036COGSDeprecation" value="${fieldValue(bean: targetBPInstance, field: 'm036COGSDeprecation')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036COGSLabor', 'error')} required">
	<label class="control-label" for="m036COGSLabor">
		<g:message code="targetBP.m036COGSLabor.label" default="COGS Labor" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036COGSLabor" value="${fieldValue(bean: targetBPInstance, field: 'm036COGSLabor')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036COGSOil', 'error')} required">
	<label class="control-label" for="m036COGSOil">
		<g:message code="targetBP.m036COGSOil.label" default="COGS Oil" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036COGSOil" value="${fieldValue(bean: targetBPInstance, field: 'm036COGSOil')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036COGSOverHead', 'error')} required">
	<label class="control-label" for="m036COGSOverHead">
		<g:message code="targetBP.m036COGSOverHead.label" default="COGS Over Head" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036COGSOverHead" value="${fieldValue(bean: targetBPInstance, field: 'm036COGSOverHead')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036COGSPart', 'error')} required">
	<label class="control-label" for="m036COGSPart">
		<g:message code="targetBP.m036COGSPart.label" default="COGS Part" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036COGSPart" value="${fieldValue(bean: targetBPInstance, field: 'm036COGSPart')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036COGSSublet', 'error')} required">
	<label class="control-label" for="m036COGSSublet">
		<g:message code="targetBP.m036COGSSublet.label" default="COGS Sublet" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036COGSSublet" value="${fieldValue(bean: targetBPInstance, field: 'm036COGSSublet')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036COGSTotalSGA', 'error')} required">
	<label class="control-label" for="m036COGSTotalSGA">
		<g:message code="targetBP.m036COGSTotalSGA.label" default="COGS Total SGA" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036COGSTotalSGA" value="${fieldValue(bean: targetBPInstance, field: 'm036COGSTotalSGA')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036Q1D', 'error')} required">
	<label class="control-label" for="m036Q1D">
		<g:message code="targetBP.m036Q1D.label" default="Q1 D" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036Q1D" value="${fieldValue(bean: targetBPInstance, field: 'm036Q1D')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036Q1S', 'error')} required">
	<label class="control-label" for="m036Q1S">
		<g:message code="targetBP.m036Q1S.label" default="Q1 S" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036Q1S" value="${fieldValue(bean: targetBPInstance, field: 'm036Q1S')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036Q2D', 'error')} required">
	<label class="control-label" for="m036Q2D">
		<g:message code="targetBP.m036Q2D.label" default="Q2 D" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036Q2D" value="${fieldValue(bean: targetBPInstance, field: 'm036Q2D')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036Q2S', 'error')} required">
	<label class="control-label" for="m036Q2S">
		<g:message code="targetBP.m036Q2S.label" default="Q2 S" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036Q2S" value="${fieldValue(bean: targetBPInstance, field: 'm036Q2S')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036Q3D', 'error')} required">
	<label class="control-label" for="m036Q3D">
		<g:message code="targetBP.m036Q3D.label" default="Q3 D" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036Q3D" value="${fieldValue(bean: targetBPInstance, field: 'm036Q3D')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036Q3S', 'error')} required">
	<label class="control-label" for="m036Q3S">
		<g:message code="targetBP.m036Q3S.label" default="Q3 S" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036Q3S" value="${fieldValue(bean: targetBPInstance, field: 'm036Q3S')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036Q4D', 'error')} required">
	<label class="control-label" for="m036Q4D">
		<g:message code="targetBP.m036Q4D.label" default="Q4 D" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036Q4D" value="${fieldValue(bean: targetBPInstance, field: 'm036Q4D')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036Q4S', 'error')} required">
	<label class="control-label" for="m036Q4S">
		<g:message code="targetBP.m036Q4S.label" default="Q4 S" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036Q4S" value="${fieldValue(bean: targetBPInstance, field: 'm036Q4S')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036Q5D', 'error')} required">
	<label class="control-label" for="m036Q5D">
		<g:message code="targetBP.m036Q5D.label" default="Q5 D" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036Q5D" value="${fieldValue(bean: targetBPInstance, field: 'm036Q5D')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036Q5S', 'error')} required">
	<label class="control-label" for="m036Q5S">
		<g:message code="targetBP.m036Q5S.label" default="Q5 S" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036Q5S" value="${fieldValue(bean: targetBPInstance, field: 'm036Q5S')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036Q6D', 'error')} required">
	<label class="control-label" for="m036Q6D">
		<g:message code="targetBP.m036Q6D.label" default="Q6 D" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036Q6D" value="${fieldValue(bean: targetBPInstance, field: 'm036Q6D')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036Q6S', 'error')} required">
	<label class="control-label" for="m036Q6S">
		<g:message code="targetBP.m036Q6S.label" default="Q6 S" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036Q6S" value="${fieldValue(bean: targetBPInstance, field: 'm036Q6S')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036TWCLeadTime', 'error')} required">
	<label class="control-label" for="m036TWCLeadTime">
		<g:message code="targetBP.m036TWCLeadTime.label" default="TWC Lead Time" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036TWCLeadTime" value="${fieldValue(bean: targetBPInstance, field: 'm036TWCLeadTime')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036TargetQty', 'error')} required">
	<label class="control-label" for="m036TargetQty">
		<g:message code="targetBP.m036TargetQty.label" default="Target Qty" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036TargetQty" value="${fieldValue(bean: targetBPInstance, field: 'm036TargetQty')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036TargetSales', 'error')} required">
	<label class="control-label" for="m036TargetSales">
		<g:message code="targetBP.m036TargetSales.label" default="Target Sales" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036TargetSales" value="${fieldValue(bean: targetBPInstance, field: 'm036TargetSales')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036TechReport', 'error')} required">
	<label class="control-label" for="m036TechReport">
		<g:message code="targetBP.m036TechReport.label" default="Tech Report" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036TechReport" value="${fieldValue(bean: targetBPInstance, field: 'm036TechReport')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036ThisMonthD', 'error')} required">
	<label class="control-label" for="m036ThisMonthD">
		<g:message code="targetBP.m036ThisMonthD.label" default="This Month D" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036ThisMonthD" value="${fieldValue(bean: targetBPInstance, field: 'm036ThisMonthD')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036ThisMonthS', 'error')} required">
	<label class="control-label" for="m036ThisMonthS">
		<g:message code="targetBP.m036ThisMonthS.label" default="This Month S" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036ThisMonthS" value="${fieldValue(bean: targetBPInstance, field: 'm036ThisMonthS')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036TotalClaimAmount', 'error')} required">
	<label class="control-label" for="m036TotalClaimAmount">
		<g:message code="targetBP.m036TotalClaimAmount.label" default="Total Claim Amount" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036TotalClaimAmount" value="${fieldValue(bean: targetBPInstance, field: 'm036TotalClaimAmount')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036TotalClaimItem', 'error')} required">
	<label class="control-label" for="m036TotalClaimItem">
		<g:message code="targetBP.m036TotalClaimItem.label" default="Total Claim Item" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036TotalClaimItem" value="${fieldValue(bean: targetBPInstance, field: 'm036TotalClaimItem')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036YTDD', 'error')} required">
	<label class="control-label" for="m036YTDD">
		<g:message code="targetBP.m036YTDD.label" default="YTDD" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036YTDD" value="${fieldValue(bean: targetBPInstance, field: 'm036YTDD')}" required=""/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: targetBPInstance, field: 'm036YTDS', 'error')} required">
	<label class="control-label" for="m036YTDS">
		<g:message code="targetBP.m036YTDS.label" default="YTDS" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:field type="text" name="m036YTDS" value="${fieldValue(bean: targetBPInstance, field: 'm036YTDS')}" required=""/>
	</div>
</div>

