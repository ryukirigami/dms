<%@ page import="com.kombos.reception.CustomerIn" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'customerIn.label', default: 'CustomerIn')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	    <g:javascript>
            function disabledAll(){
                toastr.success('Save Sukses');
                document.getElementById('tujuanKedatangan').disabled=true;
                document.getElementById('noPol').disabled=true;
                document.getElementById('t183NoPolTengah').disabled=true;
                document.getElementById('t183NoPolBelakang').disabled=true;
                document.getElementById('findDataCustomer').disabled=true;
                document.getElementById('t400StaComplain').disabled=true;
                document.getElementById('btnSave').disabled=true;
            }
	        function setPrintButton(){
				$("#buttonPrintNomorAntrian").show();
			}
			
			function printAntrian(){
                var kodeKota = $('#noPol').val();
                var noTengah = $('#t183NoPolTengah').val();
                var noBelakang = $('#t183NoPolBelakang').val();
                var idCV = $('#idCustomerVehicle').val();
                var tujuan = $('#tujuanKedatangan').val();
                if(kodeKota==null || kodeKota=="" || noTengah==null || noTengah==""){
                    alert('Data belum lengkap');
                    return
                }
                if(tujuan=="" || tujuan==null){
                    alert('Pilih Tujuan Kedatangan');
                    return
                }
                $.ajax({
                    url:'${request.contextPath}/customerIn/printNoAntrian',
                    type: "POST", // Always use POST when deleting data
                    data: {kode : kodeKota, tengah : noTengah, belakang : noBelakang,idCustomerVehicle : idCV,tujuan : tujuan},
                    success: function(data){
                            document.getElementById('customerPrint').innerHTML = data
                            constructNomorAntrian();
                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){alert('Internal Server Error')},
                    complete:function(data,textStatus){
                    }
                });
            }

            function constructNomorAntrian(){
                var iframe = document.createElement('iframe');

                iframe.onload = function() {
                    var doc = iframe.contentDocument ? iframe.contentDocument : iframe.contentWindow.document;
                    doc.getElementsByTagName('body')[0].innerHTML = $('#customerPrint').html();

                    iframe.contentWindow.focus(); // This is key, the iframe must have focus first
                    iframe.contentWindow.print();
                }

                $("#customerIn-Print").html("");
                document.getElementById("customerIn-Print").appendChild(iframe);
            }

            function checkTime(i) {
                if (i < 10) {
                    i = "0" + i;
                }
                return i;
            }

            function startTime() {
                var hari = ['Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu']
                var bulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
                var today = new Date();
                var tggl = hari[today.getDay()]+" , "+today.getDate()+" "+bulan[today.getMonth()]+" "+today.getFullYear();
                var h = today.getHours();
                var m = today.getMinutes();
                var s = today.getSeconds();
                // add a zero in front of numbers<10
                m = checkTime(m);
                s = checkTime(s);
                document.getElementById('time').innerHTML = tggl+" | "+h + ":" + m + ":" + s;
                t = setTimeout(function () {
                    startTime()
                }, 500);
            }
            startTime();
	    </g:javascript>
    </head>
	<body>
        <div id="customerIn-Print" style="background-color: white">

        </div>
		<div id="create-customerIn" class="content scaffold-create" role="main">
			<div class="box">
				<legend>Customer In</legend>
                <div id="time" style="font-style: italic" class="pull-right"></div>
                <g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
				</g:if>
				<g:hasErrors bean="${customerInInstance}">
				<ul class="errors" role="alert">
					<g:eachError bean="${customerInInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
					</g:eachError>
				</ul>
				</g:hasErrors>
				%{--<g:form action="save" >--}%
				<g:formRemote class="form-horizontal" name="create" on404="toastr.error('Save Failed');" onSuccess="disabledAll();setPrintButton();" update="customerIn-form"
				  url="[controller: 'customerIn', action:'save']">	
					<fieldset class="form">
						<g:render template="form"/>
					</fieldset>
					<fieldset class="buttons controls">
						<g:submitButton class="btn btn-primary create" id="btnSave" style="height: 50px;width: 100px;line-height: 50px;" name="save" value="${message(code: 'default.button.ok.label', default: 'SAVE')}" />
                        <a class="btn cancel" style="display : none;height: 50px;width: 200px; line-height: 50px;" href="javascript:void(0);" id="buttonPrintNomorAntrian"
                           onclick="printAntrian();" name="print"><g:message
                                code="button.print.nomor.antrian.label" default="Print Nomor Antrian" /></a>
					    <a class="btn cancel" style="height: 50px;width: 100px; line-height: 50px;" href="javascript:void(0);"
                           onclick="loadPath('customerIn/create');"><g:message
                                code="default.button.reset.label" default="Reset" /></a>
					</fieldset>
				</g:formRemote>
				%{--</g:form>--}%
				<div id="customerIn-form" style="display: none;">
				</div>
                <div id="customerPrint" style="display: none">

                </div>
			</div>	
		</div>
	</body>
</html>
