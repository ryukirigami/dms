
<%@ page import="com.kombos.reception.CustomerIn" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="customerIn_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerIn.t400ID.label" default="ID" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerIn.customerVehicle.label" default="Customer Vehicle" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerIn.tujuanKedatangan.label" default="Tujuan Kedatangan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerIn.t400StaComplain.label" default="Status Complain" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerIn.t400NomorAntrian.label" default="Nomor Antrian" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerIn.t400TglCetakNoAntrian.label" default="Tanggal Cetak No Antrian" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerIn.t400xNamaUserIn.label" default="Nama Driver" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerIn.t400StaApp.label" default="Status Booking" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerIn.reception.label" default="Reception" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerIn.t400StaReception.label" default="Status Reception" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerIn.t400TglJamReception.label" default="Tanggal Jam Reception" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerIn.companyDealer.label" default="Company Dealer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerIn.loket.label" default="Loket" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerIn.t400StaSelesaiReception.label" default="Status Selesai Reception" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerIn.t400TglJamSelesaiReception.label" default="Tanggal Jam Selesai Reception" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerIn.t400StaOut.label" default="Status Out" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerIn.t400TglJamOut.label" default="Tanggal Jam Out" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="customerIn.t400xNamaUserOut.label" default="Nama Driver Out" /></div>
			</th>
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t400ID" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t400ID" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_customerVehicle" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_customerVehicle" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_tujuanKedatangan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_tujuanKedatangan" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t400StaComplain" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t400StaComplain" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t400NomorAntrian" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t400NomorAntrian" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t400TglCetakNoAntrian" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_t400TglCetakNoAntrian" value="date.struct">
					<input type="hidden" name="search_t400TglCetakNoAntrian_day" id="search_t400TglCetakNoAntrian_day" value="">
					<input type="hidden" name="search_t400TglCetakNoAntrian_month" id="search_t400TglCetakNoAntrian_month" value="">
					<input type="hidden" name="search_t400TglCetakNoAntrian_year" id="search_t400TglCetakNoAntrian_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t400TglCetakNoAntrian_dp" value="" id="search_t400TglCetakNoAntrian" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t400xNamaUserIn" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t400xNamaUserIn" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t400StaApp" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t400StaApp" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_reception" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_reception" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t400StaReception" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t400StaReception" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t400TglJamReception" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_t400TglJamReception" value="date.struct">
					<input type="hidden" name="search_t400TglJamReception_day" id="search_t400TglJamReception_day" value="">
					<input type="hidden" name="search_t400TglJamReception_month" id="search_t400TglJamReception_month" value="">
					<input type="hidden" name="search_t400TglJamReception_year" id="search_t400TglJamReception_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t400TglJamReception_dp" value="" id="search_t400TglJamReception" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_companyDealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_companyDealer" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_loket" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_loket" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t400StaSelesaiReception" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t400StaSelesaiReception" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t400TglJamSelesaiReception" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_t400TglJamSelesaiReception" value="date.struct">
					<input type="hidden" name="search_t400TglJamSelesaiReception_day" id="search_t400TglJamSelesaiReception_day" value="">
					<input type="hidden" name="search_t400TglJamSelesaiReception_month" id="search_t400TglJamSelesaiReception_month" value="">
					<input type="hidden" name="search_t400TglJamSelesaiReception_year" id="search_t400TglJamSelesaiReception_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t400TglJamSelesaiReception_dp" value="" id="search_t400TglJamSelesaiReception" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t400StaOut" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t400StaOut" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t400TglJamOut" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_t400TglJamOut" value="date.struct">
					<input type="hidden" name="search_t400TglJamOut_day" id="search_t400TglJamOut_day" value="">
					<input type="hidden" name="search_t400TglJamOut_month" id="search_t400TglJamOut_month" value="">
					<input type="hidden" name="search_t400TglJamOut_year" id="search_t400TglJamOut_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_t400TglJamOut_dp" value="" id="search_t400TglJamOut" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t400xNamaUserOut" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t400xNamaUserOut" class="search_init" />
				</div>
			</th>
		</tr>
	</thead>
</table>

<g:javascript>
var customerInTable;
var reloadCustomerInTable;
$(function(){
	
	reloadCustomerInTable = function() {
		customerInTable.fnDraw();
	}

	
	$('#search_t400TglCetakNoAntrian').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t400TglCetakNoAntrian_day').val(newDate.getDate());
			$('#search_t400TglCetakNoAntrian_month').val(newDate.getMonth()+1);
			$('#search_t400TglCetakNoAntrian_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			customerInTable.fnDraw();	
	});

	

	$('#search_t400TglJamReception').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t400TglJamReception_day').val(newDate.getDate());
			$('#search_t400TglJamReception_month').val(newDate.getMonth()+1);
			$('#search_t400TglJamReception_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			customerInTable.fnDraw();	
	});

	

	$('#search_t400TglJamSelesaiReception').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t400TglJamSelesaiReception_day').val(newDate.getDate());
			$('#search_t400TglJamSelesaiReception_month').val(newDate.getMonth()+1);
			$('#search_t400TglJamSelesaiReception_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			customerInTable.fnDraw();	
	});

	

	$('#search_t400TglJamOut').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t400TglJamOut_day').val(newDate.getDate());
			$('#search_t400TglJamOut_month').val(newDate.getMonth()+1);
			$('#search_t400TglJamOut_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			customerInTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	customerInTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	customerInTable = $('#customerIn_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "t400ID",
	"mDataProp": "t400ID",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "customerVehicle",
	"mDataProp": "customerVehicle",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "tujuanKedatangan",
	"mDataProp": "tujuanKedatangan",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t400StaComplain",
	"mDataProp": "t400StaComplain",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t400NomorAntrian",
	"mDataProp": "t400NomorAntrian",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t400TglCetakNoAntrian",
	"mDataProp": "t400TglCetakNoAntrian",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t400xNamaUserIn",
	"mDataProp": "t400xNamaUserIn",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t400StaApp",
	"mDataProp": "t400StaApp",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "reception",
	"mDataProp": "reception",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t400StaReception",
	"mDataProp": "t400StaReception",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t400TglJamReception",
	"mDataProp": "t400TglJamReception",
	"aTargets": [10],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "companyDealer",
	"mDataProp": "companyDealer",
	"aTargets": [11],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "loket",
	"mDataProp": "loket",
	"aTargets": [12],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t400StaSelesaiReception",
	"mDataProp": "t400StaSelesaiReception",
	"aTargets": [13],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t400TglJamSelesaiReception",
	"mDataProp": "t400TglJamSelesaiReception",
	"aTargets": [14],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t400StaOut",
	"mDataProp": "t400StaOut",
	"aTargets": [15],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t400TglJamOut",
	"mDataProp": "t400TglJamOut",
	"aTargets": [16],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t400xNamaUserOut",
	"mDataProp": "t400xNamaUserOut",
	"aTargets": [17],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var t400ID = $('#filter_t400ID input').val();
						if(t400ID){
							aoData.push(
									{"name": 'sCriteria_t400ID', "value": t400ID}
							);
						}
	
						var customerVehicle = $('#filter_customerVehicle input').val();
						if(customerVehicle){
							aoData.push(
									{"name": 'sCriteria_customerVehicle', "value": customerVehicle}
							);
						}
	
						var tujuanKedatangan = $('#filter_tujuanKedatangan input').val();
						if(tujuanKedatangan){
							aoData.push(
									{"name": 'sCriteria_tujuanKedatangan', "value": tujuanKedatangan}
							);
						}
	
						var t400StaComplain = $('#filter_t400StaComplain input').val();
						if(t400StaComplain){
							aoData.push(
									{"name": 'sCriteria_t400StaComplain', "value": t400StaComplain}
							);
						}
	
						var t400NomorAntrian = $('#filter_t400NomorAntrian input').val();
						if(t400NomorAntrian){
							aoData.push(
									{"name": 'sCriteria_t400NomorAntrian', "value": t400NomorAntrian}
							);
						}

						var t400TglCetakNoAntrian = $('#search_t400TglCetakNoAntrian').val();
						var t400TglCetakNoAntrianDay = $('#search_t400TglCetakNoAntrian_day').val();
						var t400TglCetakNoAntrianMonth = $('#search_t400TglCetakNoAntrian_month').val();
						var t400TglCetakNoAntrianYear = $('#search_t400TglCetakNoAntrian_year').val();
						
						if(t400TglCetakNoAntrian){
							aoData.push(
									{"name": 'sCriteria_t400TglCetakNoAntrian', "value": "date.struct"},
									{"name": 'sCriteria_t400TglCetakNoAntrian_dp', "value": t400TglCetakNoAntrian},
									{"name": 'sCriteria_t400TglCetakNoAntrian_day', "value": t400TglCetakNoAntrianDay},
									{"name": 'sCriteria_t400TglCetakNoAntrian_month', "value": t400TglCetakNoAntrianMonth},
									{"name": 'sCriteria_t400TglCetakNoAntrian_year', "value": t400TglCetakNoAntrianYear}
							);
						}
	
						var t400xNamaUserIn = $('#filter_t400xNamaUserIn input').val();
						if(t400xNamaUserIn){
							aoData.push(
									{"name": 'sCriteria_t400xNamaUserIn', "value": t400xNamaUserIn}
							);
						}
	
						var t400StaApp = $('#filter_t400StaApp input').val();
						if(t400StaApp){
							aoData.push(
									{"name": 'sCriteria_t400StaApp', "value": t400StaApp}
							);
						}
	
						var reception = $('#filter_reception input').val();
						if(reception){
							aoData.push(
									{"name": 'sCriteria_reception', "value": reception}
							);
						}
	
						var t400StaReception = $('#filter_t400StaReception input').val();
						if(t400StaReception){
							aoData.push(
									{"name": 'sCriteria_t400StaReception', "value": t400StaReception}
							);
						}

						var t400TglJamReception = $('#search_t400TglJamReception').val();
						var t400TglJamReceptionDay = $('#search_t400TglJamReception_day').val();
						var t400TglJamReceptionMonth = $('#search_t400TglJamReception_month').val();
						var t400TglJamReceptionYear = $('#search_t400TglJamReception_year').val();
						
						if(t400TglJamReception){
							aoData.push(
									{"name": 'sCriteria_t400TglJamReception', "value": "date.struct"},
									{"name": 'sCriteria_t400TglJamReception_dp', "value": t400TglJamReception},
									{"name": 'sCriteria_t400TglJamReception_day', "value": t400TglJamReceptionDay},
									{"name": 'sCriteria_t400TglJamReception_month', "value": t400TglJamReceptionMonth},
									{"name": 'sCriteria_t400TglJamReception_year', "value": t400TglJamReceptionYear}
							);
						}
	
						var companyDealer = $('#filter_companyDealer input').val();
						if(companyDealer){
							aoData.push(
									{"name": 'sCriteria_companyDealer', "value": companyDealer}
							);
						}
	
						var loket = $('#filter_loket input').val();
						if(loket){
							aoData.push(
									{"name": 'sCriteria_loket', "value": loket}
							);
						}
	
						var t400StaSelesaiReception = $('#filter_t400StaSelesaiReception input').val();
						if(t400StaSelesaiReception){
							aoData.push(
									{"name": 'sCriteria_t400StaSelesaiReception', "value": t400StaSelesaiReception}
							);
						}

						var t400TglJamSelesaiReception = $('#search_t400TglJamSelesaiReception').val();
						var t400TglJamSelesaiReceptionDay = $('#search_t400TglJamSelesaiReception_day').val();
						var t400TglJamSelesaiReceptionMonth = $('#search_t400TglJamSelesaiReception_month').val();
						var t400TglJamSelesaiReceptionYear = $('#search_t400TglJamSelesaiReception_year').val();
						
						if(t400TglJamSelesaiReception){
							aoData.push(
									{"name": 'sCriteria_t400TglJamSelesaiReception', "value": "date.struct"},
									{"name": 'sCriteria_t400TglJamSelesaiReception_dp', "value": t400TglJamSelesaiReception},
									{"name": 'sCriteria_t400TglJamSelesaiReception_day', "value": t400TglJamSelesaiReceptionDay},
									{"name": 'sCriteria_t400TglJamSelesaiReception_month', "value": t400TglJamSelesaiReceptionMonth},
									{"name": 'sCriteria_t400TglJamSelesaiReception_year', "value": t400TglJamSelesaiReceptionYear}
							);
						}
	
						var t400StaOut = $('#filter_t400StaOut input').val();
						if(t400StaOut){
							aoData.push(
									{"name": 'sCriteria_t400StaOut', "value": t400StaOut}
							);
						}

						var t400TglJamOut = $('#search_t400TglJamOut').val();
						var t400TglJamOutDay = $('#search_t400TglJamOut_day').val();
						var t400TglJamOutMonth = $('#search_t400TglJamOut_month').val();
						var t400TglJamOutYear = $('#search_t400TglJamOut_year').val();
						
						if(t400TglJamOut){
							aoData.push(
									{"name": 'sCriteria_t400TglJamOut', "value": "date.struct"},
									{"name": 'sCriteria_t400TglJamOut_dp', "value": t400TglJamOut},
									{"name": 'sCriteria_t400TglJamOut_day', "value": t400TglJamOutDay},
									{"name": 'sCriteria_t400TglJamOut_month', "value": t400TglJamOutMonth},
									{"name": 'sCriteria_t400TglJamOut_year', "value": t400TglJamOutYear}
							);
						}
	
						var t400xNamaUserOut = $('#filter_t400xNamaUserOut input').val();
						if(t400xNamaUserOut){
							aoData.push(
									{"name": 'sCriteria_t400xNamaUserOut', "value": t400xNamaUserOut}
							);
						}
	
						var staDel = $('#filter_staDel input').val();
						if(staDel){
							aoData.push(
									{"name": 'sCriteria_staDel', "value": staDel}
							);
						}
	
						var createdBy = $('#filter_createdBy input').val();
						if(createdBy){
							aoData.push(
									{"name": 'sCriteria_createdBy', "value": createdBy}
							);
						}
	
						var updatedBy = $('#filter_updatedBy input').val();
						if(updatedBy){
							aoData.push(
									{"name": 'sCriteria_updatedBy', "value": updatedBy}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
