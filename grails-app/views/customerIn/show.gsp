<%@ page import="com.kombos.reception.CustomerIn" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'customerIn.label', default: 'CustomerIn')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<div id="show-customerIn" role="main">
        %{
            if(customerInInstance?.tujuanKedatangan?.m400Tujuan=="GR" || customerInInstance?.tujuanKedatangan?.m400Tujuan=="BP")
            {
        }%
		<table id="customerIn" class="table table-bordered table-hover">
			<tbody>
            <tr>
                <td class="span3">
                    <label>
                        %{out.print(customerInInstance.t400ID)}%
                    </label>
                </td>
            </tr>
            <tr>
                <td class="span3">
                    <label>
                        %{out.print(
                                customerInInstance.customerVehicle.getCurrentCondition().kodeKotaNoPol.m116ID+" "+
                                customerInInstance.customerVehicle.getCurrentCondition().t183NoPolTengah+" "+
                                customerInInstance.customerVehicle.getCurrentCondition().t183NoPolBelakang
                        )}%
                    </label>
                </td>
            </tr>
            <tr>
                <td class="span3">
                    <label>
                        %{out.print(new Date())}%
                    </label>
                </td>
            </tr>

            <tr>
                <td class="span3">
                    <label>
                        %{
                            if(customerInInstance?.tujuanKedatangan?.m400Tujuan=="BP"){
                                out.print("Body Paint")
                            }else if(customerInInstance?.tujuanKedatangan?.m400Tujuan=="GR"){
                                out.print("General Repair")
                            }else{
                                out.print(customerInInstance?.tujuanKedatangan?.m400Tujuan)
                            }
                        }%
                    </label>
                </td>
            </tr>
			</tbody>
		</table>
        %{
            }else{
        }%
        <table id="customerIn" class="table table-bordered table-hover">
            <tbody>
            <tr>
                <td class="span3">
                    <label>
                        %{out.print(
                                customerInInstance.customerVehicle.getCurrentCondition().kodeKotaNoPol.m116ID+" "+
                                        customerInInstance.customerVehicle.getCurrentCondition().t183NoPolTengah+" "+
                                        customerInInstance.customerVehicle.getCurrentCondition().t183NoPolBelakang
                        )}%
                    </label>
                </td>
            </tr>
            <tr>
                <td class="span3">
                    <label>
                        %{out.print(new Date())}%
                    </label>
                </td>
            </tr>

            <tr>
                <td class="span3">
                    <label>
                        %{
                            if(customerInInstance?.tujuanKedatangan?.m400Tujuan=="BP"){
                                out.print("Body Paint")
                            }else if(customerInInstance?.tujuanKedatangan?.m400Tujuan=="GR"){
                                out.print("General Repair")
                            }else{
                                out.print(customerInInstance?.tujuanKedatangan?.m400Tujuan)
                            }
                        }%
                    </label>
                </td>
            </tr>
            </tbody>
        </table>
        %{
            }
        }%
	</div>
</body>
</html>
