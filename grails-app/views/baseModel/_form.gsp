<%@ page import="com.kombos.administrasi.BahanBakar; com.kombos.administrasi.KategoriKendaraan; com.kombos.administrasi.BaseModel" %>



<div class="control-group fieldcontain ${hasErrors(bean: baseModelInstance, field: 'kategoriKendaraan', 'error')} required">
	<label class="control-label" for="kategoriKendaraan">
		<g:message code="baseModel.kategoriKendaraan.label" default="Kategori Kendaraan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:hiddenField name="id" id="id" value="${baseModelInstance?.id}"/>
        <g:select id="kategoriKendaraan" name="kategoriKendaraan.id" from="${KategoriKendaraan.createCriteria().list(){eq("staDel","0");order("m101NamaKategori","asc")}}" optionKey="id" required="" value="${baseModelInstance?.kategoriKendaraan?.id}" class="many-to-one"/>

	</div>
</div>
<div class="control-group fieldcontain ${hasErrors(bean: baseModelInstance, field: 'm102KodeBaseModel', 'error')} required">
    <label class="control-label" for="m102KodeBaseModel">
        <g:message code="baseModel.m102KodeBaseModel.label" default="Kode Base Model" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="m102KodeBaseModel" required="" maxlength="10" value="${baseModelInstance?.m102KodeBaseModel}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: baseModelInstance, field: 'm102NamaBaseModel', 'error')} required">
    <label class="control-label" for="m102NamaBaseModel">
        <g:message code="baseModel.m102NamaBaseModel.label" default="Nama Base Model" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="m102NamaBaseModel" required="" maxlength="20" value="${baseModelInstance?.m102NamaBaseModel}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: baseModelInstance, field: 'bahanBakar', 'error')} required">
	<label class="control-label" for="bahanBakar">
		<g:message code="baseModel.bahanBakar.label" default="Bahan Bakar" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="bahanBakar" name="bahanBakar.id" from="${BahanBakar.list()}" optionKey="id" required="" value="${baseModelInstance?.bahanBakar?.id}" class="many-to-one"/>
	</div>
</div>




<div class="control-group fieldcontain ${hasErrors(bean: baseModelInstance, field: 'm102Foto', 'error')} ">
	<label class="control-label" for="m102Foto">
		<g:message code="baseModel.m102Foto.label" default="Foto" />
		
	</label>
	<div class="controls">
    <g:if test="${baseModelInstance?.m102Foto}">
      <img class=""  height="70pt" width="70pt"  src="${createLink(controller:'baseModel', action:'showFoto', params : [id : baseModelInstance.id, random : new Random().nextDouble()] )}" />
    </g:if>
	<input type="file" id="m102Foto" name="m102Foto"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: baseModelInstance, field: 'm102FotoWAC', 'error')} ">
	<label class="control-label" for="m102FotoWAC">
		<g:message code="baseModel.m102FotoWAC.label" default="Foto WAC" />
		
	</label>
	<div class="controls">
    <g:if test="${baseModelInstance?.m102FotoWAC}">
        <img class=""  height="70pt" width="70pt"  src="${createLink(controller:'baseModel', action:'showFotoWAC', params : [id : baseModelInstance.id, random :new Random().nextDouble()] )}" />
    </g:if>

        <input type="file" id="m102FotoWAC" name="m102FotoWAC" />
	</div>
</div>

<g:javascript>
    document.getElementById("kategoriKendaraan").focus();
</g:javascript>