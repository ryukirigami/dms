
<%@ page import="com.kombos.administrasi.BaseModel" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="baseModel_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="baseModel.kategoriKendaraan.label" default="Kategori Kendaraan" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="baseModel.m102KodeBaseModel.label" default="Kode Base Model" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="baseModel.m102NamaBaseModel.label" default="Nama Base Model" /></div>
            </th>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="baseModel.bahanBakar.label" default="Bahan Bakar" /></div>
			</th>
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_kategoriKendaraan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_kategoriKendaraan" class="search_init" />
				</div>
			</th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_m102KodeBaseModel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_m102KodeBaseModel" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_m102NamaBaseModel" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_m102NamaBaseModel" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
				<div id="filter_bahanBakar" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_bahanBakar" class="search_init" />
				</div>
			</th>



		</tr>
	</thead>
</table>

<g:javascript>
var BaseModelTable;
var reloadBaseModelTable;
$(function(){
	
	reloadBaseModelTable = function() {
		BaseModelTable.fnDraw();
	}

	var recordsbaseModelperpage = [];//new Array();
    var anBaseModelSelected;
    var jmlRecBaseModelPerPage=0;
    var id;

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	BaseModelTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	BaseModelTable = $('#baseModel_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {
            var rsBaseModel = $("#baseModel_datatables tbody .row-select");
            var jmlBaseModelCek = 0;
            var nRow;
            var idRec;
            rsBaseModel.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsbaseModelperpage[idRec]=="1"){
                    jmlBaseModelCek = jmlBaseModelCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsbaseModelperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecBaseModelPerPage = rsBaseModel.length;
            if(jmlBaseModelCek==jmlRecBaseModelPerPage && jmlRecBaseModelPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
            //alert("apakah ini : "+rs.length);
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "kategoriKendaraan",
	"mDataProp": "kategoriKendaraan",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "m102KodeBaseModel",
	"mDataProp": "m102KodeBaseModel",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "m102NamaBaseModel",
	"mDataProp": "m102NamaBaseModel",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,


{
	"sName": "bahanBakar",
	"mDataProp": "bahanBakar",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var kategoriKendaraan = $('#filter_kategoriKendaraan input').val();
						if(kategoriKendaraan){
							aoData.push(
									{"name": 'sCriteria_kategoriKendaraan', "value": kategoriKendaraan}
							);
						}
	
						var bahanBakar = $('#filter_bahanBakar input').val();
						if(bahanBakar){
							aoData.push(
									{"name": 'sCriteria_bahanBakar', "value": bahanBakar}
							);
						}
	
						var m102KodeBaseModel = $('#filter_m102KodeBaseModel input').val();
						if(m102KodeBaseModel){
							aoData.push(
									{"name": 'sCriteria_m102KodeBaseModel', "value": m102KodeBaseModel}
							);
						}
	
						var m102NamaBaseModel = $('#filter_m102NamaBaseModel input').val();
						if(m102NamaBaseModel){
							aoData.push(
									{"name": 'sCriteria_m102NamaBaseModel', "value": m102NamaBaseModel}
							);
						}
	


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	
	  $('.select-all').click(function(e) {

        $("#baseModel_datatables tbody .row-select").each(function() {
            //alert(this.checked);
            //var id = $(this).next("input:hidden").val();
            if(this.checked){
                recordsbaseModelperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsbaseModelperpage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#baseModel_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsbaseModelperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anBaseModelSelected = BaseModelTable.$('tr.row_selected');
            if(jmlRecBaseModelPerPage == anBaseModelSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsbaseModelperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
