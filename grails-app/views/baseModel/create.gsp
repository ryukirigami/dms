<%@ page import="com.kombos.administrasi.BaseModel" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'baseModel.label', default: 'Base Model')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="create-baseModel" class="content scaffold-create" role="main">
			<legend><g:message code="default.create.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${baseModelInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${baseModelInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form action="save"  enctype="multipart/form-data">--}%

            <form id="BaseModel-save" class="form-horizontal" action="${request.getContextPath()}/baseModel/save" method="post" onsubmit="submitForm();return false;">
                <fieldset class="form">
                    <g:render template="form"/>
                </fieldset>
                <fieldset class="buttons controls">
                    <a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout();"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
                    <g:submitButton class="btn btn-primary create" name="save" value="${message(code: 'default.button.create.label', default: 'Create')}"
                                    onclick="return confirm('${message(code: 'default.button.save.confirm.message', default: 'Anda yakin data akan disimpan?')}');"/>
                </fieldset>
            </form>
            <g:javascript>
			var submitForm;
			$(function(){

			function progress(e){
		        if(e.lengthComputable){
		            //kalo mau pake progress bar
		            //$('progress').attr({value:e.loaded,max:e.total});
		        }
		    }

			submitForm = function() {

			var form = new FormData($('#BaseModel-save')[0]);

			$.ajax({
                url:'${request.getContextPath()}/baseModel/save',
                type: 'POST',
                xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){
                        myXhr.upload.addEventListener('progress',progress, false);
                    }
                    return myXhr;
                },
                //add beforesend handler to validate or something
                //beforeSend: functionname,
                success: function (res) {
     				$('#baseModel-form').empty();
					$('#baseModel-form').append(res);
					reloadBaseModelTable();
                },
                //add error handler for when a error occurs if you want!
                error: function (data, status, e){
					alert(e);
				},
                data: form,
                cache: false,
                contentType: false,
                processData: false
            });

				}
			});
            </g:javascript>
			%{--</g:form>--}%
		</div>
	</body>
</html>
