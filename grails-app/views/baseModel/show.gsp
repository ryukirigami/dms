

<%@ page import="com.kombos.administrasi.BaseModel" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'baseModel.label', default: 'Base Model')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteBaseModel;

$(function(){


	deleteBaseModel=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/baseModel/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadBaseModelTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-baseModel" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="baseModel"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${baseModelInstance?.kategoriKendaraan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kategoriKendaraan-label" class="property-label"><g:message
					code="baseModel.kategoriKendaraan.label" default="Kategori Kendaraan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kategoriKendaraan-label">
						%{--<ba:editableValue
								bean="${baseModelInstance}" field="kategoriKendaraan"
								url="${request.contextPath}/BaseModel/updatefield" type="text"
								title="Enter kategoriKendaraan" onsuccess="reloadBaseModelTable();" />--}%
							
								${baseModelInstance?.kategoriKendaraan?.encodeAsHTML()}
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${baseModelInstance?.m102KodeBaseModel}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m102KodeBaseModel-label" class="property-label"><g:message
                                code="baseModel.m102KodeBaseModel.label" default="Kode Base Model" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m102KodeBaseModel-label">
                        %{--<ba:editableValue
                                bean="${baseModelInstance}" field="m102KodeBaseModel"
                                url="${request.contextPath}/BaseModel/updatefield" type="text"
                                title="Enter m102KodeBaseModel" onsuccess="reloadBaseModelTable();" />--}%

                        <g:fieldValue bean="${baseModelInstance}" field="m102KodeBaseModel"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${baseModelInstance?.m102NamaBaseModel}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="m102NamaBaseModel-label" class="property-label"><g:message
                                code="baseModel.m102NamaBaseModel.label" default="Nama Base Model" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="m102NamaBaseModel-label">
                        %{--<ba:editableValue
                                bean="${baseModelInstance}" field="m102NamaBaseModel"
                                url="${request.contextPath}/BaseModel/updatefield" type="text"
                                title="Enter m102NamaBaseModel" onsuccess="reloadBaseModelTable();" />--}%

                        <g:fieldValue bean="${baseModelInstance}" field="m102NamaBaseModel"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${baseModelInstance?.bahanBakar}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="bahanBakar-label" class="property-label"><g:message
					code="baseModel.bahanBakar.label" default="Bahan Bakar" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="bahanBakar-label">
						%{--<ba:editableValue
								bean="${baseModelInstance}" field="bahanBakar"
								url="${request.contextPath}/BaseModel/updatefield" type="text"
								title="Enter bahanBakar" onsuccess="reloadBaseModelTable();" />--}%
							
								${baseModelInstance?.bahanBakar?.encodeAsHTML()}
							
						</span></td>
					
				</tr>
				</g:if>

				<g:if test="${baseModelInstance?.m102Foto}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m102Foto-label" class="property-label"><g:message
					code="baseModel.m102Foto.label" default="Foto" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m102Foto-label">
						%{--<ba:editableValue
								bean="${baseModelInstance}" field="m102Foto"
								url="${request.contextPath}/BaseModel/updatefield" type="text"
								title="Enter m102Foto" onsuccess="reloadBaseModelTable();" />--}%
                           <img class=".gambar"  src="${createLink(controller:'baseModel', action:'showFoto', params : [id : baseModelInstance.id, random : logostamp] )}" onload="" />
                        </span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${baseModelInstance?.m102FotoWAC}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m102FotoWAC-label" class="property-label"><g:message
					code="baseModel.m102FotoWAC.label" default="Foto WAC" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m102FotoWAC-label">
						%{--<ba:editableValue
								bean="${baseModelInstance}" field="m102FotoWAC"
								url="${request.contextPath}/BaseModel/updatefield" type="text"
								title="Enter m102FotoWAC" onsuccess="reloadBaseModelTable();" />--}%
                        <img class=".gambar" src="${createLink(controller:'baseModel', action:'showFotoWAC',  params : [id : baseModelInstance.id, random : logostamp] )}" />
						</span></td>
					
				</tr>
				</g:if>



            <g:if test="${baseModelInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="jenisStall.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">
                        %{--<ba:editableValue
                                bean="${jenisStallInstance}" field="lastUpdProcess"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter lastUpdProcess" onsuccess="reloadJenisStallTable();" />--}%

                        <g:fieldValue bean="${baseModelInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>


            <g:if test="${baseModelInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="jenisStall.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">
                        %{--<ba:editableValue
                                bean="${jenisStallInstance}" field="dateCreated"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter dateCreated" onsuccess="reloadJenisStallTable();" />--}%

                        <g:formatDate date="${baseModelInstance?.dateCreated}" type="datetime" style="MEDIUM"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${baseModelInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="jenisStall.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${jenisStallInstance}" field="createdBy"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadJenisStallTable();" />--}%

                        <g:fieldValue bean="${baseModelInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${baseModelInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="jenisStall.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        %{--<ba:editableValue
                                bean="${jenisStallInstance}" field="lastUpdated"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter lastUpdated" onsuccess="reloadJenisStallTable();" />--}%

                        <g:formatDate date="${baseModelInstance?.lastUpdated}" type="datetime" style="MEDIUM"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${baseModelInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="jenisStall.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${jenisStallInstance}" field="updatedBy"
                                url="${request.contextPath}/JenisStall/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadJenisStallTable();" />--}%

                        <g:fieldValue bean="${baseModelInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${baseModelInstance?.id}"
					update="[success:'baseModel-form',failure:'baseModel-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteBaseModel('${baseModelInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
