<%@ page import="com.kombos.hrd.Cuti" %>

   <g:javascript>
    $(function() {
        $("#cuti").focus();
    })
   </g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: cutiInstance, field: 'cuti', 'error')} ">
	<label class="control-label" for="cuti">
		<g:message code="cuti.cuti.label" default="Cuti" />
		
	</label>
	<div class="controls">
	<g:textField name="cuti" value="${cutiInstance?.cuti}" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: cutiInstance, field: 'keterangan', 'error')} ">
	<label class="control-label" for="keterangan">
		<g:message code="cuti.keterangan.label" default="Keterangan" />
		
	</label>
	<div class="controls">
	<g:textField name="keterangan" value="${cutiInstance?.keterangan}" />
	</div>
</div>


