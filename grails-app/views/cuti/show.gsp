

<%@ page import="com.kombos.hrd.Cuti" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'cuti.label', default: 'Cuti')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteCuti;

$(function(){ 
	deleteCuti=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/cuti/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadCutiTable();
   				expandTableLayout('cuti');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-cuti" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="cuti"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${cutiInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="cuti.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${cutiInstance}" field="createdBy"
								url="${request.contextPath}/Cuti/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadCutiTable();" />--}%
							
								<g:fieldValue bean="${cutiInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cutiInstance?.cuti}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="cuti-label" class="property-label"><g:message
					code="cuti.cuti.label" default="Cuti" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="cuti-label">
						%{--<ba:editableValue
								bean="${cutiInstance}" field="cuti"
								url="${request.contextPath}/Cuti/updatefield" type="text"
								title="Enter cuti" onsuccess="reloadCutiTable();" />--}%
							
								<g:fieldValue bean="${cutiInstance}" field="cuti"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cutiInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="cuti.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${cutiInstance}" field="dateCreated"
								url="${request.contextPath}/Cuti/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadCutiTable();" />--}%
							
								<g:formatDate date="${cutiInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cutiInstance?.keterangan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="keterangan-label" class="property-label"><g:message
					code="cuti.keterangan.label" default="Keterangan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="keterangan-label">
						%{--<ba:editableValue
								bean="${cutiInstance}" field="keterangan"
								url="${request.contextPath}/Cuti/updatefield" type="text"
								title="Enter keterangan" onsuccess="reloadCutiTable();" />--}%
							
								<g:fieldValue bean="${cutiInstance}" field="keterangan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cutiInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="cuti.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${cutiInstance}" field="lastUpdProcess"
								url="${request.contextPath}/Cuti/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadCutiTable();" />--}%
							
								<g:fieldValue bean="${cutiInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cutiInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="cuti.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${cutiInstance}" field="lastUpdated"
								url="${request.contextPath}/Cuti/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadCutiTable();" />--}%
							
								<g:formatDate date="${cutiInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cutiInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="cuti.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${cutiInstance}" field="staDel"
								url="${request.contextPath}/Cuti/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadCutiTable();" />--}%
							
								<g:fieldValue bean="${cutiInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${cutiInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="cuti.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${cutiInstance}" field="updatedBy"
								url="${request.contextPath}/Cuti/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadCutiTable();" />--}%
							
								<g:fieldValue bean="${cutiInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('cuti');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${cutiInstance?.id}"
					update="[success:'cuti-form',failure:'cuti-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteCuti('${cutiInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
