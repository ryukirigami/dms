

<%@ page import="com.kombos.parts.PengembalianDP" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'pengembalianDP.label', default: 'Pengembalian DP Parts')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deletePengembalianDP;

$(function(){ 
	deletePengembalianDP=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/pengembalianDP/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadPengembalianDPTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-pengembalianDP" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="pengembalianDP"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${pengembalianDPInstance?.m166TglBerlaku}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m166TglBerlaku-label" class="property-label"><g:message
					code="pengembalianDP.m166TglBerlaku.label" default="Tanggal Berlaku" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m166TglBerlaku-label">
						%{--<ba:editableValue
								bean="${pengembalianDPInstance}" field="m166TglBerlaku"
								url="${request.contextPath}/PengembalianDP/updatefield" type="text"
								title="Enter m166TglBerlaku" onsuccess="reloadPengembalianDPTable();" />--}%
							
								<g:formatDate date="${pengembalianDPInstance?.m166TglBerlaku}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${pengembalianDPInstance?.m166PersenKembaliDP}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m166PersenKembaliDP-label" class="property-label"><g:message
					code="pengembalianDP.m166PersenKembaliDP.label" default="Persen Pengembalian DP Parts" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m166PersenKembaliDP-label">
						%{--<ba:editableValue
								bean="${pengembalianDPInstance}" field="m166PersenKembaliDP"
								url="${request.contextPath}/PengembalianDP/updatefield" type="text"
								title="Enter m166PersenKembaliDP" onsuccess="reloadPengembalianDPTable();" />--}%
							
								<g:fieldValue bean="${pengembalianDPInstance}" field="m166PersenKembaliDP"/>&nbsp;%
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${pengembalianDPInstance?.m166MaxKembaliDiKasir}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m166MaxKembaliDiKasir-label" class="property-label"><g:message
					code="pengembalianDP.m166MaxKembaliDiKasir.label" default="Batas Max Pengembalian Di Kasir" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="m166MaxKembaliDiKasir-label">
						%{--<ba:editableValue
								bean="${pengembalianDPInstance}" field="m166MaxKembaliDiKasir"
								url="${request.contextPath}/PengembalianDP/updatefield" type="text"
								title="Enter m166MaxKembaliDiKasir" onsuccess="reloadPengembalianDPTable();" />--}%
							
								<g:fieldValue bean="${pengembalianDPInstance}" field="m166MaxKembaliDiKasir"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				%{--<g:if test="${pengembalianDPInstance?.companyDealer}">--}%
				%{--<tr>--}%
				%{--<td class="span2" style="text-align: right;"><span--}%
					%{--id="companyDealer-label" class="property-label"><g:message--}%
					%{--code="pengembalianDP.companyDealer.label" default="Company Dealer" />:</span></td>--}%
					%{----}%
						%{--<td class="span3"><span class="property-value"--}%
						%{--aria-labelledby="companyDealer-label">--}%
						%{--<ba:editableValue--}%
								%{--bean="${pengembalianDPInstance}" field="companyDealer"--}%
								%{--url="${request.contextPath}/PengembalianDP/updatefield" type="text"--}%
								%{--title="Enter companyDealer" onsuccess="reloadPengembalianDPTable();" />--}%
							%{----}%
								%{--<g:link controller="companyDealer" action="show" id="${pengembalianDPInstance?.companyDealer?.id}">${pengembalianDPInstance?.companyDealer?.encodeAsHTML()}</g:link>--}%
							%{----}%
						%{--</span></td>--}%
					%{----}%
				%{--</tr>--}%
				%{--</g:if>--}%
			
				<g:if test="${pengembalianDPInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="pengembalianDP.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${pengembalianDPInstance}" field="lastUpdProcess"
								url="${request.contextPath}/PengembalianDP/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadPengembalianDPTable();" />--}%
							
								<g:fieldValue bean="${pengembalianDPInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${pengembalianDPInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="pengembalianDP.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${pengembalianDPInstance}" field="dateCreated"
								url="${request.contextPath}/PengembalianDP/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadPengembalianDPTable();" />--}%
							
								<g:formatDate date="${pengembalianDPInstance?.dateCreated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${pengembalianDPInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="pengembalianDP.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">
                        %{--<ba:editableValue
                                bean="${pengembalianDPInstance}" field="createdBy"
                                url="${request.contextPath}/PengembalianDP/updatefield" type="text"
                                title="Enter createdBy" onsuccess="reloadPengembalianDPTable();" />--}%

                        <g:fieldValue bean="${pengembalianDPInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>
			
				<g:if test="${pengembalianDPInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="pengembalianDP.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${pengembalianDPInstance}" field="lastUpdated"
								url="${request.contextPath}/PengembalianDP/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadPengembalianDPTable();" />--}%
							
								<g:formatDate date="${pengembalianDPInstance?.lastUpdated}" type="datetime" style="MEDIUM" />
							
						</span></td>
					
				</tr>
				</g:if>

            <g:if test="${pengembalianDPInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="pengembalianDP.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        %{--<ba:editableValue
                                bean="${pengembalianDPInstance}" field="updatedBy"
                                url="${request.contextPath}/PengembalianDP/updatefield" type="text"
                                title="Enter updatedBy" onsuccess="reloadPengembalianDPTable();" />--}%

                        <g:fieldValue bean="${pengembalianDPInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.close.label" default="Close" /></a>
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${pengembalianDPInstance?.id}"
					update="[success:'pengembalianDP-form',failure:'pengembalianDP-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deletePengembalianDP('${pengembalianDPInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
