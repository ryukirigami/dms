<%@ page import="com.kombos.parts.PengembalianDP" %>
<script type="text/javascript">
    jQuery(function($) {
        $('.auto').autoNumeric('init',{
            mDec: ''
        });
    });
    jQuery(function($) {
        $('.persen').autoNumeric('init',{
            vMin:'0',
            vMax:'100',
            aSep:'',
            mDec: '2'
        });
    });
</script>


<div class="control-group fieldcontain ${hasErrors(bean: pengembalianDPInstance, field: 'm166TglBerlaku', 'error')} required">
	<label class="control-label" for="m166TglBerlaku">
		<g:message code="pengembalianDP.m166TglBerlaku.label" default="Tanggal Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<ba:datePicker name="m166TglBerlaku" precision="day"  value="${pengembalianDPInstance?.m166TglBerlaku}"  format="dd/MM/yyyy" required="true"/>
	</div>
</div>

<g:javascript>
    document.getElementById("m166TglBerlaku").focus();
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: pengembalianDPInstance, field: 'm166PersenKembaliDP', 'error')} required">
	<label class="control-label" for="m166PersenKembaliDP">
		<g:message code="pengembalianDP.m166PersenKembaliDP.label" default="Persen Pengembalian DP Parts" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField class="persen" name="m166PersenKembaliDP" value="${fieldValue(bean: pengembalianDPInstance, field: 'm166PersenKembaliDP')}" required=""/> %
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: pengembalianDPInstance, field: 'm166MaxKembaliDiKasir', 'error')} required">
	<label class="control-label" for="m166MaxKembaliDiKasir">
		<g:message code="pengembalianDP.m166MaxKembaliDiKasir.label" default="Batas Max Pengembalian Di Kasir" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField class="auto" name="m166MaxKembaliDiKasir" value="${fieldValue(bean: pengembalianDPInstance, field: 'm166MaxKembaliDiKasir')}" required=""/>
	</div>
</div>

%{--<div class="control-group fieldcontain ${hasErrors(bean: pengembalianDPInstance, field: 'companyDealer', 'error')} ">--}%
	%{--<label class="control-label" for="companyDealer">--}%
		%{--<g:message code="pengembalianDP.companyDealer.label" default="Company Dealer" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:select id="companyDealer" name="companyDealer.id" from="${com.kombos.baseapp.maintable.CompanyDealer.list()}" optionKey="id" value="${pengembalianDPInstance?.companyDealer?.id}" class="many-to-one" noSelection="['null': '']"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: pengembalianDPInstance, field: 'createdBy', 'error')} ">--}%
	%{--<label class="control-label" for="createdBy">--}%
		%{--<g:message code="pengembalianDP.createdBy.label" default="Created By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="createdBy" value="${pengembalianDPInstance?.createdBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: pengembalianDPInstance, field: 'updatedBy', 'error')} ">--}%
	%{--<label class="control-label" for="updatedBy">--}%
		%{--<g:message code="pengembalianDP.updatedBy.label" default="Updated By" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="updatedBy" value="${pengembalianDPInstance?.updatedBy}"/>--}%
	%{--</div>--}%
%{--</div>--}%

%{--<div class="control-group fieldcontain ${hasErrors(bean: pengembalianDPInstance, field: 'lastUpdProcess', 'error')} ">--}%
	%{--<label class="control-label" for="lastUpdProcess">--}%
		%{--<g:message code="pengembalianDP.lastUpdProcess.label" default="Last Upd Process" />--}%
		%{----}%
	%{--</label>--}%
	%{--<div class="controls">--}%
	%{--<g:textField name="lastUpdProcess" value="${pengembalianDPInstance?.lastUpdProcess}"/>--}%
	%{--</div>--}%
%{--</div>--}%

