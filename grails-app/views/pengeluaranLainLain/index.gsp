<%--
  Created by IntelliJ IDEA.
  User: DENDYSWARAN
  Date: 5/8/14
  Time: 11:50 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title>Pengeluaran Lain - Lain</title>
    <style>
    #detail-table thead th, #detail-table tbody tr td  {font-size: 10px; white-space: nowrap;vertical-align: middle;}
    #detail-table tbody tr td {text-align: center;}
    #detail-table tbody td input[type="text"] {font-size: 10px; width: 100%; margin-top: 10px;}
    #detail-form {margin-left: 0px;} #btnSearchAccount {position: relative; left: 9px; top: 2px;}
    </style>
    <r:require modules="baseapplayout, baseapplist, autoNumeric"/>
    <g:javascript>
        var putAccount;
        function changeSubtype(a){
            $('#subledger'+a).val("");
            $('#idSubledger'+a).val("");
        }
        function modalSubledger(urut) {
            var cekST = $("#subtypeid"+urut).val();
            if(cekST==""){
                alert("Pilih Subtype terlebih dahulu")
                return
            }
            $("#popupSubledgerContent").empty();
                $.ajax({type:'POST', url:'${request.contextPath}/subledgerModal?subtype='+cekST,
                    success:function(data,textStatus){
                            $("#popupSubledgerContent").html(data);
                            $("#popupSubledgerModal").modal({
                                "backdrop" : "dynamic",
                                "keyboard" : true,
                                "show" : true
                            }).css({'width': '500px','margin-left': function () {return -($(this).width() / 2);}});

                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){},
                    complete:function(XMLHttpRequest,textStatus){
                        $('#spinner').fadeOut();
                    }
                });
                $("#idx").val(urut);
        }

        putAccount = function() {
            var idx = $('#idx').val();
           $('#subledger-table tbody').find('tr').each(function(k,v) {
               if ($(this).find('td:first-child input[type="radio"]').is(":checked")) {
                    var aData = subledgerTable.fnGetData(k);
                    var accountNumber = aData.accountNumber;
                    var id = aData.id;
                    $('#subledger'+idx).val(accountNumber);
                    $('#idSubledger'+idx).val(id);
               }
            });
            $("#closeSubledger").click();
        }

        function showModalAccount(idx) {
            $("#popupAccountNumberContent").empty();
            $.ajax({type:'POST', url:'${request.contextPath}/mappingJurnalModal',
                success:function(data,textStatus){
                        $("#popupAccountNumberContent").html(data);
                        $("#popupAccountNumberModal").modal({
                            "backdrop" : "dynamic",
                            "keyboard" : true,
                            "show" : true
                        }).css({'width': '600px','margin-left': function () {return -($(this).width() / 2);}});

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){},
                complete:function(XMLHttpRequest,textStatus){
                    $('#spinner').fadeOut();
                }
            });
            $("#idx").val(idx);
        };

        function setAccount() {
            var idx = $('#idx').val();
            $('#accountNumber-table tbody').find('tr').each(function(k,v) {
               if ($(this).find('td:first-child input[type="radio"]').is(":checked")) {
                    var aData = accountNumberTable.fnGetData(k);
                    var accountNumber = aData.accountNumber;
                    var accountName = aData.accountName;
                    $('#nomorRekeningDetail'+idx).val(accountNumber);
                    $('#namaRekeningDetail'+idx).val(accountName);
                    $("#closeAccount").click();
                      // add
                       var akunNeedSubtype = "PIUTANG USAHA, PIUTANG LAIN-LAIN, PIUTANG SEWA, PIUTANG KARYAWAN, PIUTANG DAGANG, ";
                       akunNeedSubtype += "HUTANG USAHA, HUTANG LAIN-LAIN UMUM, ";
                       akunNeedSubtype += "U.M SERVICE PERBAIKAN UMUM, U.M SERVICE PERBAIKAN BODY, ";
                       akunNeedSubtype += "U.M BIAYA INSENTIF,  U.M BIAYA PENGGANTIAN CUTI, ";
                       akunNeedSubtype += "U.M.J.TRAINING";
                       if(akunNeedSubtype.contains(accountName)){
                            alert("HARAP TAMBAHKAN SUBTYPE & SUBLEDGER");
                            toastr.info("HARAP TAMBAHKAN SUBTYPE & SUBLEDGER");
                        }else{
                            toastr.success("Sukses Ditambahkan");
                        }
               }
            });
        }
    </g:javascript>
</head>

<body>
<div class="navbar box-header no-border">
    <span class="pull-left">Pengeluaran Lain Lain</span>
</div>
<div class="box">
    <div class="span12">
        <div class="row-fluid">
            <div class="span6">
                <div id="pengeluaran-form" class="form-horizontal">
                    <fieldset class="form">
                        <div class="control-group fieldcontain">
                            <label for="tanggalTransaksi" class="control-label">
                                Tanggal Transaksi
                            </label>
                            <div class="controls">
                                <ba:datePicker format="dd/mm/yyyy" name="tanggalTransaksi" required="true"/>
                                <g:hiddenField name="idx" id="idx" />
                            </div>
                        </div>

                        <div class="control-group fieldcontain">
                            <label for="tipeTransaksi" class="control-label">
                                Tipe Transaksi
                            </label>
                            <div class="controls">
                                <input type="radio" value="KAS" name="tipeTransaksi"><span>KAS</span>
                                <input type="radio" value="BANK" name="tipeTransaksi"><span>BANK</span>
                                <input type="radio" value="PDC" name="tipeTransaksi"><span>PDC</span>
                            </div>
                        </div>
                        <div class="control-group fieldcontain">
                            <label for="biayaOperasional" class="control-label">
                                Biaya Operasional?
                            </label>
                            <div class="controls">
                                <input type="radio" value="1" name="biayaOperasional"><span>Ya</span>
                                <input type="radio" value="0" name="biayaOperasional"><span>Tidak</span>
                            </div>
                        </div>
                        <div class="control-group fieldcontain">
                            <label for="setoranBank" class="control-label">
                                Setoran Bank?
                            </label>
                            <div class="controls">
                                <input type="radio" value="1" name="setoranBank"><span>Ya</span>
                                <input type="radio" value="0" name="setoranBank"><span>Tidak</span>
                            </div>
                        </div>
                        <div class="control-group fieldcontain">
                            <label for="namaBank" class="control-label">
                                Nama Bank
                            </label>
                            <div class="controls">
                                <select id="namaBank" readOnly></select>
                            </div>
                        </div>
                        <div class="control-group fieldcontain">
                            <label for="nomorRekening" class="control-label">
                                Nomor Rekening
                            </label>
                            <div class="controls">
                                <input type="text" id="nomorRekening" readonly>
                            </div>
                        </div>
                        <div class="control-group fieldcontain">
                            <label for="keterangan" class="control-label">
                                Keterangan
                            </label>
                            <div class="controls">
                                <textarea rows="5" cols="5" id="keterangan" style="font-size: 14px ; color: #000000" maxlength="255" ></textarea>
                            </div>
                        </div>
                        <div class="control-group fieldcontain">
                            <div class="controls">
                                <button id="btnAdd" class="btn btn-default">Tambah</button>
                                <button id="btnSave" class="btn btn-primary">Simpan</button>
                                <button id="btnReset" onclick="loadPath('pengeluaranLainLain');" class="btn btn-default" style="display: none">Reset</button>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="span6">
                <div class="form-horizontal">
                    <fieldset class="form">
                        <div class="control-group fieldcontain">
                            <label for="tanggalTransfer" class="control-label">Tanggal Transfer</label>
                            <div class="controls">
                                <ba:datePicker name="tanggalTransfer" format="dd/mm/yyyy" />
                            </div>
                        </div>
                        <div class="control-group fieldcontain">
                            <label for="nomorPDCKOMBOS" class="control-label">Nomor PDC KOMBOS</label>
                            <div class="controls">
                                <input type="text" id="nomorPDCKOMBOS" readonly>
                            </div>
                        </div>
                        <div class="control-group fieldcontain">
                            <label for="tanggalJthTempoPDC" class="control-label">Tanggal Jatuh Tempo PDC KOMBOS</label>
                            <div class="controls">
                                <ba:datePicker name="tanggalJthTempoPDC" format="dd/mm/yyyy" />
                            </div>
                        </div>
                        <div class="control-group fieldcontain">
                            <label for="jumlahPembayaran" class="control-label">Jumlah Pembayaran</label>
                            <div class="controls">
                                <input type="text" class="numeric" readonly="" id="jumlahPembayaran">
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
    <div class="span12" id="detail-form" style="display: block; margin-top: 20px;">
        <legend>Detail</legend>
        <table id="detail-table" class="table table-bordered">
            <thead>
            <th>No</th>
            <th>Nomor Rekening</th>
            <th>Nama Rekening</th>
            <th>Tipe Transaksi</th>
            <th>Jumlah</th>
            <th>SubType</th>
            <th>SubLedger</th>
            <th>Keterangan</th>
            <th>Aktivitas</th>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>

<div id="popupAccountNumberModal" class="modal fade">
    <div class="modal-dialog" style="width: 600px;">
        <div class="modal-content" style="width: 600px;">
            <!-- dialog body -->
            <div class="modal-body" style="max-height: 400px;">
                %{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}%
                <div id="popupAccountNumberContent">
                    <div class="iu-content"></div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="control-group">
                    <a href="javascript:void(0);" id="btnPilih" onclick="setAccount();" class="btn btn-primary">
                        Add
                    </a>
                    <a href="javascript:void(0);" class="btn cancel" id="closeAccount" data-dismiss="modal">
                        Close
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="popupSubledgerModal" class="modal fade">
    <div class="modal-dialog" style="width: 500px;">
        <div class="modal-content" style="width: 500px;">
            <!-- dialog body -->
            <div class="modal-body" style="max-height: 480px;">
                %{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}%
                <div id="popupSubledgerContent">
                    <div class="iu-content"></div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="control-group">
                    <a href="javascript:void(0);" id="btnPilihKaryawan" onclick="putAccount();" class="btn btn-primary">
                        Save
                    </a>
                    <a href="javascript:void(0);" class="btn cancel" id="closeSubledger" data-dismiss="modal">
                        Close
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<g:javascript>

    var SHOW_BANK_LIST    = 0;
    var SHOW_ACCOUNT_LIST = 1;
    var NOTE_SALDO = "${noteSaldo}";
    $(function() {
        if(NOTE_SALDO!=""){
            alert(NOTE_SALDO)
        }
        prepareCBBank();

        $(".numeric").autoNumeric("init", {
            vMin: '-99999999999.99',
            vMax: '9999999999999.99'
        });

        $("#btnAdd").click(function() {
            addRow();
        });

        $("#pengeluaran-form").on("click", "fieldset.form .control-group .controls input[type='radio']", function() {
            var checkedRadio = $(this).val();

            $("#namaBank").val("");
            $("#tanggalTransfer").val("");
            $("#nomorPDCKombos").val("");
            $("#tanggalJthTempoPDC").val("");
            if (checkedRadio === "KAS") {
                // HARDCODED
                $("#nomorRekening").val("1.10.10.01.001");
                // Disable elements
                $("#namaBank").attr("disabled", true);
                $("input[name=setoranBank]").removeAttr("disabled");
                $("#tanggalTransfer").attr("disabled", true);
                $("#nomorPDCKOMBOS").attr("disabled", "disabled");
                $("#tanggalJthTempoPDC").attr("disabled", "disabled");
            } else if (checkedRadio === "BANK") {
                // Enable elements
                $("input[name=setoranBank][value='0']").attr("checked",true);
                $("input[name=setoranBank]").attr("disabled", "disabled");
                $("#namaBank").removeAttr("readOnly");
                $("#namaBank").attr("disabled", false);
                $("#tanggalTransfer").removeAttr("disabled");
                $("#nomorPDCKOMBOS").attr("disabled", "disabled");
                $("#nomorRekening").val("");
                // Disable elements
                $("#tanggalJthTempoPDC").attr("disabled", "disabled");
            } else if (checkedRadio === "PDC") {
                // Enable elements
                $("#namaBank").removeAttr("readOnly");
                $("#nomorPDCKOMBOS").removeAttr("readOnly");
                $("#nomorPDCKOMBOS").removeAttr("disabled");
                $("#tanggalJthTempoPDC").removeAttr("disabled");
                $("#nomorRekening").val("");
                // Disable elements
                $("#tanggalTransfer").attr("disabled", false);
                $("#namaBank").attr("disabled", false);
            }
        });

        $("#namaBank").change(function() {
            if (typeof $(this).attr("readOnly") == "undefined") {
                // show bank modal
                var accountNumber = $(this).find("option:selected").data("account");
                $("#nomorRekening").val(accountNumber);
            }
        });

        //
        $("#btnPutAccount").click(function() {
            var row = $("#account-modal input[type='hidden']#row").val();
            putAccountDataToDetails(row, mapAccountData());
            $("#account-modal").modal("hide");
        });

        // Delete account
        $("#detail-table tbody").on("click", "tr td a i#btnDeleteAccount", function(evt) {
            var sure = confirm("Anda yakin ingin menghapus data ini?");
            if (sure) {
                $(this).parent().parent().parent().remove();
                performCalculation();
            }
        });

        // After input jumlah
        $("#detail-table tbody").on("focusout", "tr td input#jumlah", function(evt) {
            performCalculation();
        });

        $("#btnSave").click(function() {
            performSave();
        });

    });

    function addRow() {
        var mRow  = getLastRow();
        var count = new Number(mRow.count) + 1;

        $("#detail-table tbody").append(drawRowColumn(count));
        $(".numeric").autoNumeric("init", {
            vMin: '-999999999.99',
            vMax: '99999999999.99'
        });
    };

    function getLastRow() {
        if ($("#detail-table tbody").html().length <= 18)
            return {
                count: 0,
                hasRow: false
            }
        return {
            count: $("#detail-table tbody tr:last").data("row"),
            hasRow: true
        }
    }

    function drawRowColumn(iCount) {
        return "<tr data-row='"+iCount+"'>" +
                "<td ><span id='nomorUrut'>"+iCount+"</span></td>" +
                "<td><input type='text' id='nomorRekeningDetail"+iCount+"' readOnly style='width: 100px;'><a href='javascript:void(0);' onclick='showModalAccount("+iCount+")'><i class='icon-search' id='btnSearchAccount'></i></a></td>" +
                "<td><input type='text' id='namaRekeningDetail"+iCount+"' readOnly style='width: 100px;'></td>" +
                "<td><span id='tipeTransaksi'>Debet</span></td>" +
                "<td><input type='text' class='numeric' id='jumlah' style='width: 100px;'></td>" +
                '<td><select id="subtypeid'+iCount+'" onchange="changeSubtype('+iCount+');" style="width:150px">' +
                ' <option value="">-Pilih Subtype-</option>'
                    <g:each in="${com.kombos.finance.SubType.createCriteria().list {eq("staDel","0");order("description")}}">
                        + '<option value="${it?.subType}">${it?.subType}</option>'
                    </g:each>+'</select>' +'&nbsp;' +
                "<td><input type='hidden' id='idSubledger"+iCount+"' ><input type='text' id='subledger"+iCount+"' readOnly style='width: 100px;'><a href='javascript:void(0);' onclick='modalSubledger("+iCount+");'><i class='icon-search' id='btnSearchName'></i></a></td>" +
"<td style='background-color : #c4c4c4'><textarea id='deskripsi' style='width: 160px; height:45px; color:black;  border-style: solid;'></textarea></td>" +
                "<td><a href='javascript:void(0);'><i id='btnDeleteAccount' class='icon-trash'></i></a></td>" +
                "</tr>";
    }

    function countTableRows() {
        var count = 0;
        $("#detail-table tbody").find("tr").each(function(k,v) {
            count++;
        });
        return count;
    }

    function mapAccountData() {
        var accountNumber = null;
        var accountName   = null;
        var id            = null;
        var typeJurnal    = null;

        $("#detailAccountNumber-table tbody").find("tr").each(function() {
            if ($(this).find("td:first-child input[type='radio']").is(":checked")) {
                accountNumber = $(this).find("td#accountNumber").html();
                accountName   = $(this).find("td#accountName").html();
                typeJurnal    = "Debet";
                id            = $(this).find("td#id").html();
            }
        });

        return {
            id: id,
            accountNumber: accountNumber,
            accountName: accountName,
            typeJurnal:typeJurnal
        }
    }

    function prepareCBBank() {
        $.get("${request.getContextPath()}/pengeluaranLainLain/showDetails", {
            type: SHOW_BANK_LIST
        }, function(data) {
            var html = "<option></option>";
            for (var i = 0; i < data.length; i++)
                html += "<option data-account='"+data[i]["accountNumber"]+"' data-bank='"+data[i]["bankId"]+"' value='"+data[i]["bankName"]+"'>"+data[i]["bankName"]+" "+data[i]["bankCabang"]+" </option>";
            $("#namaBank").html(html);
        });
    }

    function performCalculation() {
        var jumlahPembayaran = new Number(0);

        $("#detail-table tbody").find("tr").each(function() {
            var sJumlah = $(this).find("td input#jumlah").autoNumeric("get");
            var iJumlah = new Number(sJumlah);

            jumlahPembayaran += iJumlah;
        });

        $("#jumlahPembayaran").autoNumeric("set", jumlahPembayaran);
    }

    function performSave() {
        var transactionType = $("input[name='tipeTransaksi']:checked").val();
        var biayaOperasional = $("input[name='biayaOperasional']:checked").val();
        var setoranBank = $("input[name='setoranBank']:checked").val();
        var amount          = $("#jumlahPembayaran").autoNumeric("get");
        var description     = $("#keterangan").val();
        var transactionDate = $("#tanggalTransaksi").val();
        var details         = crawlDetails();
        var canProceed      = false;
        var params;

        var bankId, accountNumber, transferDate, dueDate, nomorPDC;

        if (transactionType === "BANK") {
            //tanggaltransaksi, nama bank, nomorrekening, keterangan
            // tanggal transfer, jumlahpembayaran

            bankId        = $("#namaBank option:selected").data("bank");
            accountNumber = $("#nomorRekening").val();
            transferDate  = $("#tanggalTransfer").val();

            if (bankId && accountNumber && transferDate && amount && biayaOperasional)
                canProceed = true;

        } else if (transactionType === "PDC") {
            dueDate       = $("#tanggalJthTempoPDC").val();
            bankId        = $("#namaBank option:selected").data("bank");
            accountNumber = $("#nomorRekening").val();
            nomorPDC      = $("#nomorPDCKOMBOS").val();

            if (bankId && accountNumber && dueDate && amount && biayaOperasional)
                canProceed = true;

        } else if (transactionType === "KAS") {
            accountNumber = $("#nomorRekening").val();

            if (amount && biayaOperasional)
                canProceed = true;
        }

        if(transactionDate=="")
        {
        alert("Data Belum Lengkap")
        return
        }

        if(description=="") {
            alert("Keterangan Tidak Boleh Kosong");
            return
        }

        if(details=="salah") {
            return
        }

        if (canProceed) {
              var conf = confirm("Apakah Anda yakin untuk menyimpan?")
              if(conf){
                params = {
                    transactionType: transactionType,
                    biayaOperasional: biayaOperasional,
                    setoranBank: setoranBank,
                    amount: amount,
                    description: description,
                    transactionDate: transactionDate,
                    bankId: bankId,
                    accountNumber: accountNumber,
                    transferDate: transferDate,
                    details: details,
                    dueDate: dueDate,
                    nomorPDC: nomorPDC
                }
                $.post("${request.getContextPath()}/pengeluaranLainLain/insert", params, function(data) {
                    if (!data.error){
                        alert("Success. " + data.codeTrx);
                        $("#btnAdd").hide();
                        $("#btnSave").hide();
                        $("#btnReset").show();
                    }
                    else
                        alert(data.error);
                });
              }
        }else{
            alert('Data Belum Lengkap');
        }
    }

    function crawlDetails() {
        var status = ""
        var total           = $("#jumlahPembayaran").autoNumeric("get");
        var description     = $("#keterangan").val();
        var accountNumber   = $("#nomorRekening").val();
        var journalType     = "KREDIT";
        var arr             = new Array();

        arr.push({
             accountNumber: accountNumber,
             transactionType: journalType,
             total: total,
             description: description
        });

        var count = 0;
        $("#detail-table tbody").find("tr").each(function() {
            count++;

            var accountNumber   = $(this).find("td input[type='text']#nomorRekeningDetail"+count+"").val();
            var accountName     = $(this).find("td input[type='text']#namaRekeningDetail"+count+"").val();
            var transactionType = $(this).find("td span#tipeTransaksi").html();
            var total           = $(this).find("td input[type='text']#jumlah").autoNumeric("get");
            var description     = $(this).find("td textarea#deskripsi").val();
            var subtype         = $(this).find("td select#subtypeid"+count+"").val();
            var subledger       = $(this).find("td input[type='hidden']#idSubledger"+count+"").val();

            var map = {
                accountNumber: accountNumber,
                accountName: accountName,
                transactionType: transactionType,
                total: total,
                description: description ? description : "-",
                subtype : subtype,
                subledger : subledger
            }
            if((accountNumber == "4.10.10.01.001" || accountNumber == "4.10.30.01.001" ||
            accountNumber == "1.30.20.01.001" || accountNumber == "1.30.40.01.001"
            ) && (subledger=="" || !subledger)){
                alert("Apakah Benar, Subledger Tidak diisi?");
            }
            if(accountNumber==""){
                alert("Nomor akun tidak boleh KOSONG");
                status = "salah"
            }
            if(total==0 || total==""){
                alert("Jumlah tidak Boleh KOSONG atau NOL");
                status = "salah"
            }
            if(accountNumber!=null){
                arr.push(map);
            }
        });

        if(status==""){
            return JSON.stringify(arr);
        }else{
            return status
        }

    }

</g:javascript>
</body>
</html>