<%@ page import="com.kombos.administrasi.CompanyDealer"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="baseapplayout" />
<g:javascript disappointmentGrition="head">
	$(function(){		
        $('#filter_periode').show();        
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    });
    var checkin = $('#search_tanggal').datepicker({
        onRender: function(date) {
            return '';
        }
    }).on('changeDate', function(ev) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
                checkin.hide();
                $('#search_tanggal2')[0].focus();
            }).data('datepicker');

    var checkout = $('#search_tanggal2').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
                checkout.hide();
    }).data('datepicker');

    previewData = function(){
            var namaReport = $('#namaReport').val();
            var workshop = $('#workshop').val();
            var search_tanggal = $('#search_tanggal').val();
            var search_tanggal2 = $('#search_tanggal2').val();    
            if(namaReport == "" || namaReport == null){
                alert('Pilih report terlebih dahulu');
                return;
            }
            window.location = "${request.contextPath}/gate_pass/previewData?namaReport="+namaReport+"&workshop="+workshop+"&tanggal="+search_tanggal+"&tanggal2="+search_tanggal2;
    }
</g:javascript>

</head>
<body>
	<div class="navbar box-header no-border">Report – Gate Pass
	</div>
	<br>
	<div class="box">
		<div class="span12" id="appointmentGr-table">
			<div class="row-fluid form-horizontal">
				<div id="kiri" class="span6">
					<div class="control-group">
						<label class="control-label" for="t951Tanggal"
							style="text-align: left;"> Workshop </label>
						<div id="filter_wo" class="controls">
							<g:select name="workshop" id="workshop"
								from="${CompanyDealer.list()}" optionKey="id"
								optionValue="m011NamaWorkshop" style="width: 44%" />
						</div>
					</div>					
					<div class="control-group">
						<label class="control-label" for="t951Tanggal"
							style="text-align: left;"> Nama Report </label>
						<div id="filter_nama" class="controls">
							<select name="namaReport" id="namaReport" size="8"
								style="width: 100%; height: 230px; font-size: 12px;">
								<option value="01">01. Gate Pass</option>													
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="t951Tanggal"
							style="text-align: left;"> Periode </label>
						<div id="filter_periode" class="controls">
							<ba:datePicker id="search_tanggal" name="search_tanggal"
								precision="day" format="dd-MM-yyyy" value="${new Date()}" />
							&nbsp;&nbsp;s.d.&nbsp;&nbsp;
							<ba:datePicker id="search_tanggal2" name="search_tanggal2"
								precision="day" format="dd-MM-yyyy" value="${new Date()+1}" />
						</div>						
					</div>					
				</div>
				<div id="kanan" class="span6">					
					<div class="control-group">	
						<label class="control-label" style="text-align:left;">
						Jenis Pekerjaan
						</label>
						<div class="controls">
							<g:render template="tableJenisPekerjaan"></g:render>
						</div>
					</div>
					<div class="control-group">	
						<label class="control-label" style="text-align:left;">
						Jenis GatePass
						</label>
						<div class="controls">
							<g:render template="tableJenisGatePass"></g:render>
						</div>
					</div>
				</div>				
			</div>
			<g:field type="button" onclick="previewData()"
				class="btn btn-primary create" name="preview"
				value="${message(code: 'default.button.upload.label', default: 'Preview')}" />
			<g:field type="button" onclick="window.location.replace('#/home')"
				class="btn btn-cancel cancel" name="cancel"
				value="${message(code: 'default.button.upload.label', default: 'Close')}" />
		</div>
	</div>
</body>
</html>
