<%@ page import="com.kombos.administrasi.NamaDokumen" %>

<div class="control-group fieldcontain ${hasErrors(bean: namaDokumenInstance, field: 'm007NamaDokumen', 'error')} required ">
	<label class="control-label" for="m007NamaDokumen">
		<g:message code="namaDokumen.m007NamaDokumen.label" default="Nama Dokumen" /><span class="required-indicator">*</span>
		
	</label>
	<div class="controls">
	<g:textField name="m007NamaDokumen" value="${namaDokumenInstance?.m007NamaDokumen}" required="" maxlength="50"/>
	</div>
</div>

