
<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="partsTransferStokInput_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;"  >
				<div><g:message code="partsTransferStokInput.goods.label" default="Kode Parts" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;" >
                <div><g:message code="partsTransferStokInput.goods.label" default="Nama Parts" /></div>
            </th>


            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="partsTransferStokInput.t162Qty1.label" default="Nomor PO" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="partsTransferStokInput.t162Qty1.label" default="Harga Beli Parts" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="partsTransferStokInput.t162Qty1.label" default="Qty" /></div>
            </th>
            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="partsTransferStokInput.t162Qty1.label" default="Stock di Gudang" /></div>
            </th>

        </tr>


	</thead>
</table>

<g:javascript>
var partsTransferStokTable;
var reloadPartsTransferStokTable;
$(function(){
	var recordsPartsTransferStokPerPage = [];
    var anPartsTransferStokSelected;
    var jmlRecPartsTransferStokPerPage=0;
    var id;
	reloadPartsTransferStokTable = function() {
		partsTransferStokTable.fnDraw();
	}

	
	$('#search_t162TglPartsTransferStok').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t162TglPartsTransferStok_day').val(newDate.getDate());
			$('#search_t162TglPartsTransferStok_month').val(newDate.getMonth()+1);
			$('#search_t162TglPartsTransferStok_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			partsTransferStokTable.fnDraw();
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	partsTransferStokTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	partsTransferStokTable = $('#partsTransferStokInput_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnDrawCallback": function () {

            var rsPartsTransferStok = $("#partsTransferStokInput_datatables tbody .row-select");
            var jmlPartsTransferStokCek = 0;
            var nRow;
            var idRec;
            rsPartsTransferStok.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();
                if(recordsPartsTransferStokPerPage[idRec]=="1"){
                    jmlPartsTransferStokCek = jmlPartsTransferStokCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsPartsTransferStokPerPage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecPartsTransferStokPerPage = rsPartsTransferStok.length;
            if(jmlPartsTransferStokCek==jmlRecPartsTransferStokPerPage && jmlRecPartsTransferStokPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "goods",
	"mDataProp": "kode",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" id="idPopUp" value="'+row['id']+'">&nbsp;&nbsp'+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "goods2",
	"mDataProp": "nama",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "nomorPo",
	"mDataProp": "nomorPO",
	"aTargets": [2],
	"bSearchable": true,
    "bSortable": true,
    "sWidth":"200px",
    "bVisible": true
}
,
{
	"sName": "hargaBeli",
	"mDataProp": "hargaBeli",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
 ,

{
	"sName": "Qreturn",
	"mDataProp": "Qty",
	"aTargets": [4],
	"bSearchable": true,
    "bSortable": true,
    "sWidth":"200px",
    "bVisible": true
},

{
	"sName": "stokDigudang",
	"mDataProp": "stokDigudang",
	"aTargets": [4],
	"bSearchable": true,
    "bSortable": true,
    "sWidth":"200px",
    "bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

							aoData.push(
									{"name": 'nomorPO', "value": "${nomorPO}"}
							);

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {

        $("#partsTransferStokInput_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsPartsTransferStokPerPage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsPartsTransferStokPerPage[$(this).next("input:hidden").val()] = "0";
            }
        });

    });

	$('#partsTransferStokInput_datatables tbody tr').live('click', function () {

        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsPartsTransferStokPerPage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anPartsTransferStokSelected = partsTransferStokTable.$('tr.row_selected');

            if(jmlRecPartsTransferStokPerPage == anPartsTransferStokSelected.length){
                $('.select-all').attr('checked', true);
            }

        } else {
            recordsPartsTransferStokPerPage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });
    if(cekStatus=="ubah"){
        var nomorPartsTransferStok = "${noPartsTransferStok}";
        $.ajax({
        url:'${request.contextPath}/partsTransferStokInput/getTableData',
    		type: "POST", // Always use POST when deleting data
    		data: { id: nomorPartsTransferStok },
    		success : function(data){
    		    $.each(data,function(i,item){
                    partsTransferStokTable.fnAddData({
                        'id': item.id,
						'goods': item.kode,
						'goods2': item.nama,
						'hargaBeli': item.hargaBeli,
 						'Qreturn': item.Qty
                    });
                });
            }
		});
    }
});
</g:javascript>


			
