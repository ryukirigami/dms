
<%@ page import="com.kombos.parts.PartsTransferStok" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'partsTransferStok.label', default: 'Permintaan Transfer Stok')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
    var u_companyDealer = "${u_companyDealer}";
    var nomorPO = "${nomorPO}";
    var noUrut = 1;
    var cekStatus = "${aksi}";
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});


});
            $(document).ready(function()
         {
             $('input:radio[name=search_t162StaFA]').click(function(){
                if($('input:radio[name=search_t162StaFA]:nth(0)').is(':checked')){
                    $("#search_t162NoReff").prop('disabled', false);

                }else{
                    $("#search_t162NoReff").val("")
                    $("#search_t162NoReff").prop('disabled', true);
                }
             });


         });

      createRequest = function(){
                var formPartsTransferStok = $('#partsTransferStokInput-table').find('form');
                var checkGoods =[];
                var cGoods =[];
                $("#partsTransferStokInput-table tbody .row-select").each(function() {
                    var id = $(this).next("input:hidden").val();
                    if(this.checked){
                        var nRow = $(this).parents('tr')[0];
                        var aData = partsTransferStokTable.fnGetData(nRow)
                        checkGoods.push(id);
                    }
                    cGoods.push(id);

                });

                if($("#m121Nama").val()==""){
                    toastr.error('<div>Mohon Diisi Dengan Benar.</div>');
                }
                else{
                     var r = confirm("Anda yakin data akan disimpan?");
                     if(r==true){
                        var rows = partsTransferStokTable.fnGetNodes();
                        $("#ids").val(JSON.stringify(checkGoods));
                        $("#idss").val(JSON.stringify(cGoods));
                        $.ajax({
                            url:'${request.contextPath}/partsTransferStokCekInput/req',
                            type: "POST", // Always use POST when deleting data
                            data : formPartsTransferStok.serialize(),
                            success : function(data){
                              if(data=='fail'){
                                toastr.error('<div>Dealer Tidak Benar.</div>');
                              }else{
                                    window.location.href = '#/partsTransferStokCek' ;
                                    toastr.success('<div>Parts PartsTransferStok telah dibuat.</div>');
                                    formPartsTransferStok.find('.deleteafter').remove();
                                    $("#ids").val('');
                                    $("#idss").val('');
                              }
                            },
                        error: function(xhr, textStatus, errorThrown) {
                        alert('Internal Server Error');
                        }
                        });

                        checkGoods = [];
                        cGoods = [];
                      }
                }
    };

</g:javascript>

	</head>
	<body>
    <div class="navbar box-header no-border">
        <span class="pull-left">
            <g:if test="${aksi=='ubah'}">
                Edit
            </g:if>
            <g:else>
                Permintaan Transfer Stok
            </g:else>
        </span>
        <ul class="nav pull-right">
            <li>&nbsp;</li>
        </ul>
    </div><br>
	<div class="box">
		<div class="span12" id="partsTransferStokInput-table">
            <div>${flash.message}</div>
            <fieldset>
                <form id="form-partsTransferStok" class="form-horizontal">
                    <input type="hidden" name="ids" id="ids" value="">
                    <input type="hidden" name="idss" id="idss" value="">
                        <div class="control-group">
                            <label class="control-label" for="t951Tanggal" style="text-align: left;">Dealer Yang Membutuhkan *
                            </label>
                            <div id="filter_vendor" class="controls">
                                <label class="control-label" for="t951Tanggal" style="text-align: left;"><b>${u_companyDealer}</b>
                                </label>
                                <g:hiddenField name="namaCompanyDealer" value="${u_companyDealer}" />
                            </div>
                        </div>
                </form>
            </fieldset>
			<g:render template="dataTables" />
                 %{--<g:field type="button" onclick="deleteData()" class="btn btn-cancel create" name="deleteData" id="delData" value="${message(code: 'default.button.upload.label', default: 'Delete')}" />--}%
                <g:field type="button" onclick="createRequest()" class="btn btn-primary create" name="tambahSave" id="tambahSave" value="${message(code: 'default.button.upload.label', default: 'Save')}" />
              <g:field type="button" onclick="window.location.replace('#/partsTransferStokCek');" class="btn btn-cancel" name="cancel" id="cancel" value="Cancel" />
		</div>
		<div class="span7" id="partsTransferStok-form" style="display: none;"></div>
	</div>
</body>
</html>

<g:javascript>
</g:javascript>

