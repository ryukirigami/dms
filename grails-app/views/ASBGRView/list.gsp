
<%@ page import="com.kombos.board.ASB" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
	        var show;
			var edit;
			$(function(){
			    $('#asb_per_hari').load('${request.contextPath}/ASBGRView/perDay');

                $('#tab_asb_per_hari').click(function() {
                    $('#asb_per_hari').load('${request.contextPath}/ASBGRView/perDay');
                });

                $('#tab_asb_per_3hari').click(function() {
                    $('#asb_per_3hari').load('${request.contextPath}/ASBGRView/perThreeDays');
                });
                var reloadAll = function(){
                    var table = $('#ASB_datatables');
                    if(table.length > 0){
                        //asbView();
//                        setTimeout(function(){reloadAll()}, 10000);
                    } else {
                        var table3 = $('#ASB_datatablesDay1');
                        if(table3.length > 0){
                           // asbView3Hari();
//                            setTimeout(function(){reloadAll()}, 10000);
                        }
                    }
                }
//                setTimeout(function(){reloadAll()}, 10000);
            });

</g:javascript>
	</head>
	<body>
    <div class="navbar box-header no-border">
   		<span class="pull-left">Appointment Schedule Board (ASB) General Repair</span>
   	</div>

	<div class="box">
        <div class="tabbable control-group">
            <ul class="nav nav-tabs">
                <li id="tab_asb_per_hari" class="active">
                    <a href="#asb_per_hari" data-toggle="tab">Per Hari</a>
                </li>
                <li id="tab_asb_per_3hari" >
                    <a href="#asb_per_3hari" data-toggle="tab">3 Hari</a>
                </li>

            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="asb_per_hari">
                </div>

                <div class="tab-pane active" id="asb_per_3hari">

                 </div>

            </div>
        </div>
        </div>
    </body>
</html>
