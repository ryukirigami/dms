
<%@ page import="com.kombos.administrasi.KategoriJob; com.kombos.board.ASB" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'ASB.label', default: 'ASB GR')}" />
		%{--<title><g:message code="default.list.label" args="[entityName]" /></title>--}%
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			$(function(){

	$('#search_tglView3Hari').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tglView3Hari_day').val(newDate.getDate());
			$('#search_tglView3Hari_month').val(newDate.getMonth()+1);
			$('#search_tglView3Hari_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
//			var oTable = $('#ASB_datatables').dataTable();
//            oTable.fnReloadAjax();
	});

   	asbView3Hari = function(){
        var oTableDay1 = $('#ASB_datatablesDay1').dataTable();
        oTableDay1.fnReloadAjax();
        var oTableDay2 = $('#ASB_datatablesDay2').dataTable();
        oTableDay2.fnReloadAjax();
        var oTableDay3 = $('#ASB_datatablesDay3').dataTable();
        oTableDay3.fnReloadAjax();
        var from = $("#search_tglView3Hari").val().split("/");
        var theDate = new Date();
        theDate.setFullYear(from[2], from[1] - 1, from[0]);
        //alert("theDate=" + theDate);
        var m_names = new Array("January", "February", "March", "April", "Mei", "June", "July", "August", "September", "October", "November", "Desember");
        var formattedDate =  theDate.getDate() + " " + m_names[theDate.getMonth()] + " " + theDate.getFullYear();
        var formattedDatePlus1 =  (theDate.getDate() + 1) + " " + m_names[theDate.getMonth()] + " " + theDate.getFullYear();
        var formattedDatePlus2 =  (theDate.getDate() + 2) + " " + m_names[theDate.getMonth()] + " " + theDate.getFullYear();
        document.getElementById("lblTglViewPlus0").innerHTML = formattedDate;
        document.getElementById("lblTglViewPlus1").innerHTML = formattedDatePlus1;
        document.getElementById("lblTglViewPlus2").innerHTML = formattedDatePlus2;
   	}
});

</g:javascript>
	</head>
	<body>

	<div class="box">
        <table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">
            <tr>
                <td style="width: 100%;vertical-align: top;padding-left: 10px;">
                    <div class="box">
                        <legend style="font-size: small">View ASB</legend>
                        <table style="width: 50%;border: 0px">
                            <tr>
                                <td>
                                    <label class="control-label" for="lbl_companyDealer3Hari">
                                        Nama Workshop
                                    </label>
                                </td>
                                %{--<td>--}%
                                    %{--<div id="lbl_companyDealer3Hari" class="controls">--}%
                                        %{--<g:select id="input_companyDealer3Hari" disabled="" name="companyDealer.id" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop")}}" optionKey="id" required="" value="${session?.userCompanyDealer?.id}" class="many-to-one"/>--}%

                                    %{--</div>--}%
                                %{--</td>--}%
                                <td>
                                    <div id="lbl_companyDealer3Hari" class="controls">
                                        %{--<g:select id="input_companyDealer" disabled="" name="companyDealer.id" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop")}}" optionKey="id" required="" value="${session?.userCompanyDealer?.id}" class="many-to-one"/>--}%
                                        <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                            <g:select name="companyDealer.id" id="input_companyDealer3Hari" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                                        </g:if>
                                        <g:else>
                                            <g:select name="companyDealer.id" id="input_companyDealer3Hari" readonly="" disabled="" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                                        </g:else>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label class="control-label" for="lbl_tglView3Hari">
                                        Tanggal
                                    </label>
                                </td>
                                <td>
                                    <div id="lbl_tglView3Hari" class="controls">
                                        <div id="filter_tglView3Hari" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                                             <input type="hidden" name="search_tglView3Hari" value="date.struct">
                                             <input type="hidden" name="search_tglView3Hari_day" id="search_tglView3Hari_day" value="">
                                             <input type="hidden" name="search_tglView3Hari_month" id="search_tglView3Hari_month" value="">
                                             <input type="hidden" name="search_tglView3Hari_year" id="search_tglView3Hari_year" value="">
                                             <input type="text" data-date-format="dd/mm/yyyy" name="search_tglView3Hari_dp" value="" id="search_tglView3Hari" class="search_init">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <button class="btn btn-primary" id="buttonView3Hari" onclick="asbView3Hari()">View</button>
                                </td>
                            </tr>

                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="ASB-tableDay1">
                        <g:render template="dataTablesDay1" />
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="ASB-tableDay2">
                        <g:render template="dataTablesDay2" />
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="ASB-tableDay3">
                        <g:render template="dataTablesDay3" />
                    </div>
                </td>
            </tr>
            <tr>
                <table>
                    <tr>
                        <%
                            def cariData = KategoriJob.createCriteria().list {order("m055KategoriJob")};
                            cariData.each {
                        %>
                        <td style="padding: 10px;align-content: center">
                            <table class="table table-bordered" style="width: 10%;background-color: ${it?.m055WarnaBord}">
                                <tr><td>&nbsp;&nbsp;</td></tr>
                            </table>
                            <br>${it?.m055KategoriJob}
                        </td>
                        <%
                            }
                        %>
                    </tr>
                </table>
            </tr>
        </table>

	</div>
</body>
</html>
