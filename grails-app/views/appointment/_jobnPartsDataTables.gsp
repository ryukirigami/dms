<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="job_n_parts_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
			<th style="border-bottom: none;padding: 5px;">
				<div>Nama Job</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Rate</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Status Warranty</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Nominal</div>
			</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td><span>ESTIMASI WAKTU (Dalam Jam)</span></td>
			<td><span class="pull-right numeric" id="lblrate"></span></td>
            <td colspan="2"></td>
		</tr>
		<tr>
			<td colspan="3"><span>TOTAL SEBELUM DISCOUNT</span></td>
			<td><span class="pull-right numeric" id="lbltotal"></span></td>
		</tr>
	</tfoot>
</table>

<g:javascript>
var jobnPartsTable;
var reloadjobnPartsTable;
var selected_job;
$(function(){

	reloadjobnPartsTable = function() {
			jobnPartsTable.fnDraw();
	        reloadJoborderTable();
	}

	$('#job_n_parts_datatables tbody td').live('click', function(e) {
			$('#job_n_parts_datatables tbody .row_selected').removeClass('row_selected');
			var parent = $(this).parent()
            parent.toggleClass('row_selected');
			var selected_job = parent.find("input:hidden").val();
    });


	jobnPartsTable = $('#job_n_parts_datatables').dataTable({
		"sScrollX": "833px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t",
		bFilter: false,
		"bStateSave": false,
		//'sPaginationType': 'bootstrap',
		"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			var aData = jobnPartsTable.fnGetData(nRow);
			if(aData.staPart!="kosong"){
			var staSukses = false;
                var dataTemp ;
    			$.ajax({type:'POST',
                    data : aData,
                    url:'${g.createLink(action: "partList")}',
                    success:function(data,textStatus){
                    staSukses = true;
                        dataTemp = data;

//                        var nDetailsRow = jobnPartsTable.fnOpen(nRow,data,'details');
                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){},
                    complete:function(XMLHttpRequest,textStatus){
                    if(staSukses==true){
                            var nDetailsRow = jobnPartsTable.fnOpen(nRow,dataTemp,'details');
                        }
                        $('#spinner').fadeOut();
                    }
                });
            }
            $('#lblrate').text(aData.totalRate);
			$('#lbltotal').text(aData.totalNominal);

            return nRow;
		},
		"fnDrawCallback":function(oSettings){
			if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
				$(oSettings.nTableWrapper).find('.dataTables_paginate').hide();
			}
		},
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "jobnPartsDatatablesList")}",
		"aoColumns": [
{
	"sName": "namaJob",
	"mDataProp": "namaJob",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['idJobApp']+'" title="Select this"><input type="hidden" value="'+row['idJobApp']+'">&nbsp;&nbsp;'+data;
	},

	"bSearchable": false,
	"bSortable": true,
	"sWidth":"527px",
	"bVisible": true
},{
	"sName": "rate",
	"mDataProp": "rate",
	"aTargets": [1],
	"mRender": function ( data, type, row ) {
		if(data != 'null')
			return '<span class="pull-right numeric">'+data+'</span>';
		else
			return '<span></span>';
	},
	"bSearchable": false,
	"bSortable": true,
	"sWidth":"69px",
	"bVisible": true
},{
	"sName": "statusWarranty",
	"mDataProp": "statusWarranty",
	"aTargets": [2],
	"bSearchable": false,
	"bSortable": true,
	"sWidth":"73px",
	"bVisible": true
},{
	"sName": "nominal",
	"mDataProp": "nominal",
	"aTargets": [3],
	"mRender": function ( data, type, row ) {
		if(data != 'null')
			return '<span class="pull-right numeric">'+data+'</span>';
		else
			return '<span></span>';
	},
	"bSearchable": false,
	"bSortable": true,
	"sWidth":"119px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                        var kodeKota = $('#kodeKota');
			            var nomorTengah = $('#nomorTengah');
			            var nomorBelakang = $('#nomorBelakang');
			            if(kodeKota.val()){
			                aoData.push(
									{"name": 'sCriteria_kodeKota', "value": kodeKota.val()}
							);
			            }
			            if(nomorTengah.val()){
			                aoData.push(
									{"name": 'sCriteria_nomorTengah', "value": nomorTengah.val()}
							);
			            }
			            if(nomorBelakang.val()){
			                aoData.push(
									{"name": 'sCriteria_nomorBelakang', "value": nomorBelakang.val()}
							);
			            }

                        aoData.push(
                            {"name": 'sAppointmentId', "value": appointmentId}
                        );

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>


			
