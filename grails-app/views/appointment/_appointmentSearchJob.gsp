<g:javascript>
	var addJobs = function() {
   		var recordsToDelete = [];
		
		var idJob = "";
		var kodeJob = "";
		var namaJob = "";
		var rate = "";
		$("#operation_datatables_search tbody .row-select").each(function() {
			if(this.checked){
    			idJob = $(this).next("input:hidden").val();
				kodeJob = $($(this).next("input:hidden")).next("input:hidden").val();
				namaJob = $($($(this).next("input:hidden")).next("input:hidden")).next("input:hidden").val();
				rate = $($($($(this).next("input:hidden")).next("input:hidden")).next("input:hidden")).next("input:hidden").val();
                addJobAppointment(idJob,kodeJob,namaJob,rate);
               %{--$.ajax({--}%
                    %{--url:'${request.getContextPath()}/appointment/getRate?id='+idJob,--}%
                    %{--type: 'POST',--}%
                    %{--success: function (res) {--}%
                        %{--if(res.status == 'ok'){--}%
                        %{--rate = res.rate;--}%
                        %{--}--}%
                       %{--addJobAppointment(idJob,kodeJob,namaJob,rate);--}%
                    %{--},--}%
                    %{--error: function (data, status, e){--}%
                        %{--alert(e);--}%
                    %{--},--}%
                    %{--complete: function(xhr, status) {--}%

                    %{--},--}%
                    %{--cache: false,--}%
                    %{--contentType: false,--}%
                    %{--processData: false--}%
                %{--});--}%
    		}
		});
        openDialog('appointmentAddJobModal');
   	}	
</g:javascript>
<div class="modal-header">
    %{--<a class="close" data-dismiss="modal">×</a>--}%
    <h3>Appointment - Search Job</h3>
</div>
<div class="modal-body">
    <div class="dataTables_scroll">
        <div class="span12" id="companyDealerPopUp-table">
            <g:render template="datatablesJob"/>
        </div>
    </div>
</div>
<div class="modal-footer">
    <a onclick="addJobs();" href="javascript:void(0);" class="btn btn-success" id="add_part_btn">Add Job</a>
    <a href="#" class="btn" data-dismiss="modal" onclick="addJobs();">Close</a>
</div>