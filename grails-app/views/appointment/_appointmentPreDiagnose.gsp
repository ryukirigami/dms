<g:javascript>
	function getValueCheckbox(elementId){
		return ($(elementId).is(":checked"))?1:0;
	}

	function printPreDiagnose(){
		var href = $('#hrefPrintPreDiagnose').attr('href');
		window.location.href = href;
	}

	function savePreDiagnose(){
        alert("save");
		//saving Prediagnose


		var aodataPrediagnose =  new Array();

		var idx = 1;

		var inputSystemPA = "";
		var inputDTCPA = "";
		var inputDescPA = "";
		var inputStatusPA = "";
		var inputFreezePA = "";

		while (idx < countPemeriksaanAwal){
			inputSystemPA = 'inputSystemPA' + idx;
			inputDTCPA = 'inputDTCPA' + idx;
			inputDescPA = 'inputDescPA' + idx;
			inputStatusPA = 'inputStatusPA' + idx;
			inputFreezePA = 'inputFreezePA' + idx;

			//inputSystemPA
			aodataPrediagnose.push(
				{"name": inputSystemPA, "value": $('#'+inputSystemPA).val()}
			);
			//inputDTCPA
			aodataPrediagnose.push(
				{"name": inputDTCPA, "value": $('#'+inputDTCPA).val()}
			);
			//inputDescPA
			aodataPrediagnose.push(
				{"name": inputDescPA, "value": $('#'+inputDescPA).val()}
			);
			//inputStatusPA
			aodataPrediagnose.push(
				{"name": inputStatusPA, "value": $('#'+inputStatusPA).val()}
			);
			//inputFreezePA
			aodataPrediagnose.push(
				{"name": inputFreezePA, "value": $('#'+inputFreezePA).val()}
			);
			idx++;
		}

		aodataPrediagnose.push(
			{"name": 'countPemeriksaanAwal', "value": countPemeriksaanAwal}
		);

		idx = 1;
		var inputSystemCU = "";
		var inputDTCCU = "";
		var inputDescCU = "";
		var inputStatusCU = "";
		var inputFreezeCU = "";

		while (idx < countCekUlang){
			inputSystemCU = 'inputSystemCU' + idx;
			inputDTCCU = 'inputDTCCU' + idx;
			inputDescCU = 'inputDescCU' + idx;
			inputStatusCU = 'inputStatusCU' + idx;
			inputFreezeCU = 'inputFreezeCU' + idx;

			//inputSystemCU
			aodataPrediagnose.push(
				{"name": inputSystemCU, "value": $('#'+inputSystemCU).val()}
			);
			//inputDTCCU
			aodataPrediagnose.push(
				{"name": inputDTCCU, "value": $('#'+inputDTCCU).val()}
			);
			//inputDescCU
			aodataPrediagnose.push(
				{"name": inputDescCU, "value": $('#'+inputDescCU).val()}
			);
			//inputStatusCU
			aodataPrediagnose.push(
				{"name": inputStatusCU, "value": $('#'+inputStatusCU).val()}
			);
			//inputFreezeCU
			aodataPrediagnose.push(
				{"name": inputFreezeCU, "value": $('#'+inputFreezeCU).val()}
			);
			idx++;
		}

		aodataPrediagnose.push(
			{"name": 'countCekUlang', "value": countCekUlang}
		);

		aodataPrediagnose.push(
			{"name": 't406Km', "value": $('#lblkmdiagnose').html()}
		);

		aodataPrediagnose.push(
			{"name": 'receptionId', "value": $('#receptionId').val()}
		);

		aodataPrediagnose.push(
			{"name": 't406KeluhanCust', "value": $('#keluhanCust').val()}
		);

		aodataPrediagnose.push(
			{"name": 't406StaGejalaHariIni', "value": getValueCheckbox('#gejalaHariIni')}
		);

		aodataPrediagnose.push(
			{"name": 't406StaGejalaMingguLalu', "value": getValueCheckbox('#gejalaMingguLalu')}
		);

		aodataPrediagnose.push(
			{"name": 't406StaGejalaLainnya', "value": getValueCheckbox('#gejalaLainnya')}
		);

		aodataPrediagnose.push(
			{"name": 't406GejalaLainnya', "value": $('#gejalaLainnyaInput').val()}
		);

		aodataPrediagnose.push(
			{"name": 't406StaFrekSekali', "value":  getValueCheckbox('#frekuensiSekali')}
		);

		aodataPrediagnose.push(
			{"name": 't406StaFrekKadang', "value":  getValueCheckbox('#frekuensiKadang')}
		);

		aodataPrediagnose.push(
			{"name": 't406StaFrekSelalu', "value":  getValueCheckbox('#frekuensiSelalu')}
		);

		aodataPrediagnose.push(
			{"name": 't406StaFrekLainnya', "value":  getValueCheckbox('#frekuensiLainnya')}
		);

		aodataPrediagnose.push(
			{"name": 'frekuensiLainnyaInput', "value":  $('#frekuensiLainnyaInput').val()}
		);

		aodataPrediagnose.push(
			{"name": 't406StaMILOn', "value":  getValueCheckbox('#milOn')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaMILKedip', "value":  getValueCheckbox('#milKedip')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaMILOff', "value":  getValueCheckbox('#milOff')}
		);

		aodataPrediagnose.push(
			{"name": 't406StaMILIdling', "value":  getValueCheckbox('#kecepatanIdling')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaMILStarting', "value":  getValueCheckbox('#kecepatanStarting')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaMILKonstan', "value":  getValueCheckbox('#kecepatanKonstan')}
		);

		aodataPrediagnose.push(
			{"name": 't406StaMesinPanas', "value":  getValueCheckbox('#mesinPanas')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaMesinDingin', "value":  getValueCheckbox('#mesinDingin')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaPanasMesinLainnya', "value":  getValueCheckbox('#mesinLainnya')}
		);
		aodataPrediagnose.push(
			{"name": 't406PanasMesinLainnya', "value":  $('#mesinLainnyaInput').val()}
		);

		aodataPrediagnose.push(
			{"name": 't406StaGigi1', "value":  getValueCheckbox('#shiftLever1')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaGigi2', "value":  getValueCheckbox('#shiftLever2')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaGigi3', "value":  getValueCheckbox('#shiftLever3')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaGigi4', "value":  getValueCheckbox('#shiftLever4')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaGigi5', "value":  getValueCheckbox('#shiftLever5')}
		);

		aodataPrediagnose.push(
			{"name": 't406StaGigiP', "value":  getValueCheckbox('#shiftLeverP')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaGigiR', "value":  getValueCheckbox('#shiftLeverR')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaGigiN', "value":  getValueCheckbox('#shiftLeverN')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaGigiD', "value":  getValueCheckbox('#shiftLeverD')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaGigiS', "value":  getValueCheckbox('#shiftLeverS')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaGigiLainnya', "value":  getValueCheckbox('#shiftLeverL')}
		);

        aodataPrediagnose.push(
			{"name": 't406Kecepatan', "value":   $('#kecepatanKendaraan').val()}
		);
		aodataPrediagnose.push(
			{"name": 't406Beban', "value":   $('#bebanKendaraan').val()}
		);
		aodataPrediagnose.push(
			{"name": 't406RPM', "value":   $('#rpm').val()}
		);
		aodataPrediagnose.push(
			{"name": 't406JmlPenumpang', "value":   $('#jumlahPenumpang').val()}
		);
		aodataPrediagnose.push(
			{"name": 't406StaDalamKota', "value":  getValueCheckbox('#kondisiJalanDalkot')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaLuarKota', "value":   getValueCheckbox('#kondisiJalanLukot')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaTol', "value":  getValueCheckbox('#kondisiJalanTol')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaJalanLurus', "value":   getValueCheckbox('#kondisiJalanLurus')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaBelok', "value":   getValueCheckbox('#kondisiJalanBelok')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaDatar', "value":   getValueCheckbox('#kondisiJalanDatar')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaAkselerasi', "value":   getValueCheckbox('#kondisiJalanAkselerasi')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaTanjakan', "value":   getValueCheckbox('#kondisiJalanTanjakan')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaTurunan', "value":   getValueCheckbox('#kondisiJalanTurunan')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaRem', "value":   getValueCheckbox('#kondisiJalanRem')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaKondisiLainnya', "value":   getValueCheckbox('#kondisiJalanLain')}
		);
		aodataPrediagnose.push(
			{"name": 't406KondisiLainnya', "value": $('#kondisiJalanLainInput').val()}
		);

		aodataPrediagnose.push(
			{"name": 't406StaMacet', "value":   getValueCheckbox('#laluLintasMacet')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaLancar', "value":   getValueCheckbox('#laluLintasLancar')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaLalinLainnya', "value":   getValueCheckbox('#laluLintasLainnya')}
		);

		aodataPrediagnose.push(
			{"name": 't406LalinLainnya', "value": $('#laluLintasLainnyaInput').val()}
		);

		aodataPrediagnose.push(
			{"name": 't406StaCerah', "value":   getValueCheckbox('#cuacaCerah')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaBerawan', "value":   getValueCheckbox('#cuacaBerawan')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaHujan', "value":   getValueCheckbox('#cuacaHujan')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaPanas', "value":   getValueCheckbox('#cuacaPanas')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaLembab', "value":   getValueCheckbox('#cuacaLembab')}
		);
		aodataPrediagnose.push(
			{"name": 't406Temperatur', "value":   $('#cuacaTemperaturInput').val()}
		);

		aodataPrediagnose.push(
			{"name": 't406StaEG', "value":   getValueCheckbox('#problemEg')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaSuspensi', "value":   getValueCheckbox('#problemSuspensi')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaKlasifikasiRem', "value":   getValueCheckbox('#problemRem')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaLainnya', "value":   getValueCheckbox('#problemLainnya')}
		);
		aodataPrediagnose.push(
			{"name": 't406KlasifikasiLainnya', "value":  $('#problemLainnyaInput').val() }
		);

		aodataPrediagnose.push(
			{"name": 't406BlowerSpeed', "value":  $('#gejalaAcBlowerSpeed').val() }
		);
		aodataPrediagnose.push(
			{"name": 't406TempSetting', "value":  $('#gejalaAcTempSetting').val() }
		);

		aodataPrediagnose.push(
			{"name": 't406StaReci1', "value":   getValueCheckbox('#gejalaAcPos1')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaReci2', "value":   getValueCheckbox('#gejalaAcPos2')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaReci3', "value":   getValueCheckbox('#gejalaAcPos3')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaReci4', "value":   getValueCheckbox('#gejalaAcPos4')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaReci5', "value":   getValueCheckbox('#gejalaAcPos5')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaReci6', "value":   getValueCheckbox('#gejalaAcPosAuto')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaPerluTS', "value":   getValueCheckbox('#hasilPerluSupport')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaTidakPerluTS', "value":   getValueCheckbox('#hasilTidakPerluSupport')}
		);

		aodataPrediagnose.push(
			{"name": 't406DiagnosaAwal', "value":  $('#prediagnose').val() }
		);
		aodataPrediagnose.push(
			{"name": 't406DetailPekerjaan', "value":  $('#detilPekerjaan').val() }
		);
		aodataPrediagnose.push(
			{"name": 't406KonfirmasiAkhir', "value":  $('#konfirmasiAkhir').val() }
		);

		aodataPrediagnose.push(
			{"name": 't406StaOK', "value":   getValueCheckbox('#konfirmasiOk')}
		);
		aodataPrediagnose.push(
			{"name": 't406StaNG', "value":   getValueCheckbox('#konfirmasiNg')}
		);

        //t406StaOK : $('#').val(),
		//t406StaNG : $('#').val(),
		//t406StaNVH : $('#').val(),
		//t406GigiLainnya : $('#').val(),
		//t406MILLainnya : $('#').val(),
		//t406MILKecepatanLainnya : $('#').val(),
		//t406StaAppRcp : $('#').val(),
		//t406StaButuhDiagnose : $('#').val(),
		//t406StaSudahDiagnoseApp : $('#').val(),
		//t406StaSudahDiagnoseRcp : $('#').val(),
		//appointment : $('#').val(),
		//t406Km : $('#').val(),
		//t406TglDiagnosis : $('#').val(),
		//t406Suhu : $('#').val(),
		//t406StaPerluDTR : $('#').val(),
		//t406StaTidaKPerluDTR : $('#').val(),
		//t406StaKonfirmasiSelalu : $('#').val(),
		//t406StaKonfirmasiKadang : $('#').val(),
		//t406StaKonfirmasiTidakMuncul : $('#').val(),
		//t406StaKonfirmasiLainnya : $('#').val(),
		//t406KonfirmasiLainnya : $('#').val(),
		//t406KeteranganKonfirmasi : $('#').val(),
		//t406StaCustomer : $('#').val(),
		//t406JamMulai : $('#').val(),
		//t406JamSelesai : $('#').val(),
		//t406PICDiagnosis : $('#').val(),
		//t406TglJamMulai : $('#').val(),
		//t406TglJamSelesai : $('#').val(),
		//t406Foreman : $('#').val(),
		//t406Teknisi : $('#').val(),


		$.ajax({
			url:'${request.contextPath}/appointment/savePreDiagnose',
			type: "POST",
			async : false,
			data: aodataPrediagnose,
			success : function(data){
				if(data.id){
					initButtonPreDiagnose(data.id);
					toastr.success("Save Succes");
				}
				$('#spinner').fadeOut();
			},
			error: function(xhr, textStatus, errorThrown) {
				alert('Internal server error');
			}
		});
	    closeDialog('appointmentPreDiagnoseModal');
	}

	var countPemeriksaanAwal = 1;
		function addPemeriksaanAwal(){
			var inputSystemPA = $('#inputSystemPA').val();
			if(!inputSystemPA){
				alert('Silahkan isi System');
			}else{
				var inputDTCPA = $('#inputDTCPA').val();
				var inputDescPA = $('#inputDescPA').val();
				var inputStatusPA = $('#inputStatusPA').val();
				var inputFreezePA = $('#inputFreezePA').val();

				var newRow = "<tr><td><input id='countPemeriksaanAwal"+countPemeriksaanAwal+"' type='hidden' value='"+countPemeriksaanAwal+"'/><input id='inputSystemPA"+countPemeriksaanAwal+"' type='hidden' value='"+inputSystemPA+"'/><input id='inputDTCPA"+countPemeriksaanAwal+"' type='hidden' value='"+inputDTCPA+"'/><input id='inputDescPA"+countPemeriksaanAwal+"' type='hidden' value='"+inputDescPA+"'/><input id='inputStatusPA"+countPemeriksaanAwal+"' type='hidden' value='"+inputStatusPA+"'/><input id='inputFreezePA"+countPemeriksaanAwal+"' type='hidden' value='"+inputFreezePA+"'/>"+countPemeriksaanAwal+"</td><td>"+inputSystemPA+"</td><td>"+inputDescPA+"</td><td>"+inputDTCPA+"</td><td>"+inputStatusPA+"</td><td>"+inputFreezePA+"</td></tr>";

				countPemeriksaanAwal++;

				$('#pemeriksaanAwal > tbody:last').append(newRow);

				$('#pemeriksaanAwal').val("");
			}
		}

		var countCekUlang = 1;
		function addCekUlang(){
			var inputSystemCU = $('#inputSystemCU').val();
			if(!inputSystemCU){
				alert('Silahkan isi System');
			}else{
				var inputDTCCU = $('#inputDTCCU').val();
				var inputDescCU = $('#inputDescCU').val();
				var inputStatusCU = $('#inputStatusCU').val();
				var inputFreezeCU = $('#inputFreezeCU').val();

				var newRow = "<tr><td><input id='countCekUlang"+countCekUlang+"' type='hidden' value='"+countCekUlang+"'/><input id='inputSystemCU"+countCekUlang+"' type='hidden' value='"+inputSystemCU+"'/><input id='inputDTCCU"+countCekUlang+"' type='hidden' value='"+inputDTCCU+"'/><input id='inputDescCU"+countCekUlang+"' type='hidden' value='"+inputDescCU+"'/><input id='inputStatusCU"+countCekUlang+"' type='hidden' value='"+inputStatusCU+"'/><input id='inputFreezeCU"+countCekUlang+"' type='hidden' value='"+inputFreezeCU+"'/>"+countCekUlang+"</td><td>"+inputSystemCU+"</td><td>"+inputDescCU+"</td><td>"+inputDTCCU+"</td><td>"+inputStatusCU+"</td><td>"+inputFreezeCU+"</td></tr>";

				countCekUlang++;

				$('#cekUlang > tbody:last').append(newRow);

				$('#cekUlang').val("");
			}
		}
</g:javascript>
<div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Appointment Prediagnose</h3>
</div>
<div class="modal-body">
<div class="box">
<table class="table">
<tbody>
<tr>
<td style="width:50%;">
<table class="table">
<tr class="odd">
    <td colspan="7">
        Form Prediagnose
    </td>
</tr>
<tr>
    <td>
        Nomor Polisi
    </td>
    <td colspan="2">
        <label id="lblnopoldiagnose"></label>
    </td>
    <td colspan="2">
        Tgl Prediagnose
    </td>
    <td colspan="2">
        <label id="lbltggldiagnose">${new Date().format("dd-MM-yyyy")}</label>
    </td>
</tr>
<tr>
    <td>
        Km
    </td>
    <td colspan="2">
        <label id="lblkmdiagnose"></label>
    </td>
    <td colspan="2">
        Model
    </td>
    <td colspan="2">
        <label id="lblmobildiagnose"></label>
    </td>
</tr>
<tr>
    <td>
        Keluhan Customer
    </td>
    <td colspan="6">
        <textarea id="keluhanCust" name="keluhanCust" cols="300" rows="2"/>
    </td>
</tr>
<tr>
    <td>
        Gejala Terdeteksi Pertama Kali
    </td>
    <td>
        <input type="checkbox" id="gejalaHariIni"/>&nbsp;Hari Ini
    </td>
    <td>
        <input type="checkbox" id="gejalaMingguLalu"/>&nbsp;Minggu Lalu
    </td>
    <td colspan="4">
        <input type="checkbox" id="gejalaLainnya"/>&nbsp;<input type="text" id="gejalaLainnyaInput" class="input-small"/>
    </td>
</tr>
<tr>
    <td>
        Frekuensi Gejala Terjadi
    </td>
    <td>
        <input type="checkbox" id="frekuensiSekali"/>&nbsp;Sekali
    </td>
    <td>
        <input type="checkbox" id="frekuensiKadang"/>&nbsp;Kadang Kadang
    </td>
    <td>
        <input type="checkbox" id="frekuensiSelalu"/>&nbsp;Selalu
    </td>
    <td colspan="3">
        <input type="checkbox" id="frekeunsiLainnya"/>&nbsp;<input type="text" id="frekeunsiLainnyaInput" class="input-small"/>
    </td>
</tr>
<tr>
    <td>
        MIL Saat Gejala Terjadi
    </td>
    <td colspan="6">
        <input type="checkbox" id="milOn"/>&nbsp;On
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="milKedip"/>&nbsp;Berkedip
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="milOff"/>&nbsp;Off
    </td>
</tr>
<tr>
    <td>
        Kondisi Kendaraan Saat Gejala Terjadi
    </td>
    <td colspan="6">
        <input type="checkbox" id="kecepatanIdling"/>&nbsp;Idling
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="kecepatanStarting"/>&nbsp;Starting
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="kecepatanKonstan"/>&nbsp;Kecepatan Konstan
    </td>
</tr>
<tr>
    <td>
        Kondisi Mesin
    </td>
    <td colspan="6">
        <input type="checkbox" id="mesinPanas"/>&nbsp;Panas
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="mesinDingin"/>&nbsp;Dingin
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="mesinLainnya"/>&nbsp;Lainnya<input type="text" id="mesinLainnyaInput" class="input-small"/>C
    </td>
</tr>
<tr>
    <td>
        Posisi Shift Lever
    </td>
    <td colspan="6">
        <input type="checkbox" id="shiftLever1"/>&nbsp;1
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="shiftLever2"/>&nbsp;2
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="shiftLever3"/>&nbsp;3
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="shiftLever4"/>&nbsp;4
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="shiftLever5"/>&nbsp;5
    </td>
</tr>
<tr>
    <td>
        &nbsp;
    </td>
    <td colspan="6">
        <input type="checkbox" id="shiftLeverP"/>&nbsp;P
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="shiftLeverR"/>&nbsp;R
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="shiftLeverN"/>&nbsp;N
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="shiftLeverD"/>&nbsp;D
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="shiftLeverS"/>&nbsp;S
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="shiftLeverL"/>&nbsp;Lainnya
    </td>
</tr>
<tr class="odd">
    <td colspan="7">
        Kondisi Saat Gejala Terjadi (Pilih salah satu atau lebih untuk kondisi yang sesuai)
    </td>
</tr>
<tr>
    <td>
        Kecepatan Kendaraan
    </td>
    <td colspan="6">
        <input type="text" id="kecepatanKendaraan" class="input-small"/>Km/h
    &nbsp;&nbsp;&nbsp;
    Beban Kendaraan&nbsp;<input type="text" id="bebanKendaraan" class="input-small"/>Kg
    </td>
</tr>
<tr>
    <td>
        RPM Mesin
    </td>
    <td colspan="6">
        <input type="text" id="rpm" class="input-small"/>rpm
    &nbsp;&nbsp;&nbsp;
    Jumlah Penumpang&nbsp;<input type="text" id="jumlahPenumpang" class="input-small"/>Orang
    </td>
</tr>
<tr>
    <td>
        Kondisi Jalan dan Pengendara
    </td>
    <td colspan="6">
        <input type="checkbox" id="kondisiJalanDalkot"/>&nbsp;Dalam Kota
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="kondisiJalanLukot"/>&nbsp;Luar Kota
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="kondisiJalanTol"/>&nbsp;Tol
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="kondisiJalanBelok"/>&nbsp;Belok kiri-kanan
    </br>
        <input type="checkbox" id="kondisiJalanLurus"/>&nbsp;Jalan Lurus
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="kondisiJalanDatar"/>&nbsp;Datar
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="kondisiJalanTanjakan"/>&nbsp;Tanjakan
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="kondisiJalanTurunan"/>&nbsp;Turunan
    </br>
        <input type="checkbox" id="kondisiJalanAkselerasi"/>&nbsp;Akselerasi
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="kondisiJalanRem"/>&nbsp;Pengereman
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="kondisiJalanLain"/>&nbsp;Lainnya
        <input type="text" id="kondisiJalanLainInput" class="input-small"/>
    </td>
</tr>
<tr>
    <td>
        Lalu Lintas
    </td>
    <td colspan="6">
        <input type="checkbox" id="laluLintasMacet"/>&nbsp;Macet, Stop & Go
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="laluLintasLancar"/>&nbsp;Lancar
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="laluLintasLainnya"/>&nbsp;Lainnya
        <input type="text" id="laluLintasLainnyaInput" class="input-small"/>
    </td>
</tr>
<tr>
    <td>
        Kondisi Cuaca
    </td>
    <td colspan="6">
        <input type="checkbox" id="cuacaCerah"/>&nbsp;Cerah
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="cuacaBerawan"/>&nbsp;Berawan
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="cuacaHujan"/>&nbsp;Hujan
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="cuacaPanas"/>&nbsp;Panas
    </br>
        <input type="checkbox" id="cuacaLembab"/>&nbsp;Lembab
    &nbsp;&nbsp;&nbsp;
    Temperatur udara luar
        <input type="text" id="cuacaTemperaturInput" class="input-small"/>C
    </td>
</tr>
<tr>
    <td>
        Klasifikasi Problem
    </td>
    <td colspan="6">
        <input type="checkbox" id="problemEg"/>&nbsp;E/G
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="problemSuspensi"/>&nbsp;Suspensi
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="problemRem"/>&nbsp;Rem
    &nbsp;&nbsp;&nbsp;
        <input type="checkbox" id="problemLainnya"/>&nbsp; Lainnya<input type="text" id="problemLainnyaInput" class="input-small"/>
    </td>
</tr>
<tr>
    <td>
        &nbsp;
    </td>
    <td colspan="6">
        Gejala AC
    </br>
        Blower Speed&nbsp;&nbsp;&nbsp;<input type="text" id="gejalaAcBlowerSpeed" class="input-small"/>
        Temp Setting&nbsp;&nbsp;&nbsp;<input type="text" id="gejalaAcTempSetting" class="input-small"/>
    </br>
        Reci Position : &nbsp;&nbsp;&nbsp;</br>
        <input type="checkbox" id="gejalaAcPosAuto"/>&nbsp;Auto</br>
        <input type="checkbox" id="gejalaAcPos1"/>&nbsp;<img src="/dms/static/images/AcPos1.jpg"/></br>
        <input type="checkbox" id="gejalaAcPos2"/>&nbsp;<img src="/dms/static/images/AcPos2.jpg"/></br>
        <input type="checkbox" id="gejalaAcPos3"/>&nbsp;<img src="/dms/static/images/AcPos3.jpg"/></br>
        <input type="checkbox" id="gejalaAcPos4"/>&nbsp;<img src="/dms/static/images/AcPos4.jpg"/></br>
        <input type="checkbox" id="gejalaAcPos5"/>&nbsp;<img src="/dms/static/images/AcPos5.jpg"/>
    </td>
</tr>
</table>
</td>
<td style="width:50%;">
    <table class="table">
        <tr class="odd">
            <td>
                Pemeriksaan Awal
            </td>
        </tr>
        <tr>
            <td>
                <table class="display table table-striped table-bordered table-hover dataTable" width="100%"
                       cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px;">
                    <tr>
                        <th>
                            System
                        </th>
                        <th>
                            DTC
                        </th>
                        <th>
                            Description (from IT2)
                        </th>
                        <th>
                            Status (P,C or H)
                        </th>
                        <th>
                            Freeze Frame data saved
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" class="input-small" name="inputSystemPA" id="inputSystemPA"/>
                        </td>
                        <td>
                            <input type="text" class="input-small" name="inputDTCPA" id="inputDTCPA"/>
                        </td>
                        <td>
                            <input type="text" class="input-small" name="inputDescPA" id="inputDescPA"/>
                        </td>
                        <td>
                            <select class="input-small" name="inputStatusPA" id="inputStatusPA">
                                <option value="P">P</option>
                                <option value="C">C</option>
                                <option value="H">H</option>
                            </select>
                        </td>
                        <td>
                            <input class="input-small" type="text" name="inputFreezePA" id="inputFreezePA"/>
                        </td>
                        <td>
                            <input type="button" class="btn cancel" onclick="addPemeriksaanAwal();" value="Add"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table id="pemeriksaanAwal" class="display table table-striped table-bordered table-hover dataTable" width="100%"
                       cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px;">
                    <tr>
                        <th>
                            No
                        </th>
                        <th>
                            System
                        </th>
                        <th>
                            DTC
                        </th>
                        <th>
                            Description (from IT2)
                        </th>
                        <th>
                            Status (P,C or H)
                        </th>
                        <th>
                            Freeze Frame data saved
                        </th>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                Cek Ulang DTC
            </td>
        </tr>
        <tr>
            <td>
                <table class="display table table-striped table-bordered table-hover dataTable" width="100%"
                       cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px;">
                    <tr>
                        <th>
                            System
                        </th>
                        <th>
                            DTC
                        </th>
                        <th>
                            Description (from IT2)
                        </th>
                        <th>
                            Status (P,C or H)
                        </th>
                        <th>
                            Freeze Frame data saved
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <input class="input-small" type="text" name="inputSystemCU" id="inputSystemCU"/>
                        </td>
                        <td>
                            <input class="input-small" type="text" name="inputDTCCU" id="inputDTCCU"/>
                        </td>
                        <td>
                            <input class="input-small" type="text" name="inputDescCU" id="inputDescCU"/>
                        </td>
                        <td>
                            <select class="input-small" name="inputStatusCU" id="inputStatusCU">
                                <option value="P">P</option>
                                <option value="C">C</option>
                                <option value="H">H</option>
                            </select>
                        </td>
                        <td>
                            <input class="input-small" type="text" name="inputFreezeCU" id="inputFreezeCU"/>
                        </td>
                        <td>
                            <input type="button" class="btn cancel" onclick="addCekUlang();" value="Add"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table id="cekUlang" class="display table table-striped table-bordered table-hover dataTable" width="100%"
                       cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px;">
                    <tr>
                        <th>
                            No
                        </th>
                        <th>
                            System
                        </th>
                        <th>
                            DTC
                        </th>
                        <th>
                            Description (from IT2)
                        </th>
                        <th>
                            Status (P,C or H)
                        </th>
                        <th>
                            Freeze Frame data saved
                        </th>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                Konfirmasi Gejala (Lakukan informasi gejala sesuai informasi pelanggan)</br>
                Apakah keluhan pelanggan dapat dikonfirmasi?</br>
                Jelaskan hasil konfirmasi :</br>
                <textarea cols="100" rows="5" id="hasilKonfirmasi"/>
            </td>
        </tr>
    </table>
</td>
</tr>
<tr>
    <td colspan="2">
        <table class="table">
            <tr>
                <td colspan="2">
                    Catatan / Hasil Pre-Diagnose&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    Technical Support&nbsp;&nbsp;
                    <input type="checkbox" id="hasilPerluSupport"/>&nbsp;Perlu
                    <input type="checkbox" id="hasilTidakPerluSupport"/>&nbsp;Tidak Perlu
                </td>
                <td align="right" colspan="2">
                    <input type="text" id="hasilCustomer" class="input-small" style="width: 100px"/>&nbsp;Customer
                <g:hiddenField name="noWo" id="noWo" value="${noWo}" />
                </td>
            </tr>
            <tr>
                <td>
                    Prediagnose</br>
                    <textarea cols="100" rows="10" id="prediagnose"/>
                </td>
                <td>
                    Jam Mulai </br>
                    <div id="jamMulaiPrediagnose">-</div></br>
                    Jam Selesai </br>
                    <div id="jamSelesaiPrediagnose">-</div></br>
                    PIC Prediagnose </br>
                    <div id="picPrediagnose">PIC Prediagnose</div>
                </td>
                <td>
                    Detail Pekerjaan</br>
                    <textarea cols="100" rows="5" id="detilPekerjaan"/></br>
                    Konfirmasi Akhir</br>
                    <textarea cols="100" rows="5" id="konfirmasiAkhir"/>
                </td>
                <td>
                    Tanggal Dikerjakan &nbsp;&nbsp; <div id="tanggalDikerjakan"></div></br>
                    Jam Mulai &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jam Selesai</br>
                    <div id="jamMulai">-</div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div id="jamSelesai">-</div></br>
                    Foreman &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Teknisi</br>
                    <div id="foreman">-</div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div id="teknisi">-</div></br>
                    Konfirmasi Customer</br>
                    <input type="checkbox" id="konfirmasiOk"/>&nbsp;OK
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="checkbox" id="konfirmasiNg"/>&nbsp;NG
                </td>
            </tr>
        </table>
    </td>
</tr>
</tbody>
</table>
</div>
</div>
<div class="modal-footer">
    <div id="printPrediagnose" style="display:none;"/>
    <a id="btnPrintPrediagnose" onclick="printPreDiagnose();" href="javascript:void(0);" class="btn">Print</a>
    <a onclick="savePreDiagnose();" href="javascript:void(0);" class="btn">Save</a>
    <a href="#" class="btn" data-dismiss="modal">Close</a>
</div>