<table class="" width="100%" cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px;">
    <tr>
        <th>
            Keluhan
        </th>
        <th>
            Butuh Diagnose
        </th>
        <th>
            &nbsp;
        </th>
    </tr>
    <tr>
        <td>
            <input type="text" name="inputKeluhan" id="inputKeluhan"/>
        </td>
        <td>
            <input type="radio" name="inputDiagnose" value="1" checked="true"/> Ya
        </br>
            <input type="radio" name="inputDiagnose" value="2"/> Tidak
        </td>
        <td>
            <input type="button" class="btn cancel" onclick="addKeluhan();" value="Add"/>
        </td>
    </tr>
</table>
<table id="keluhan_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
            <th style="border-bottom: none;padding: 5px;">
                <div>No Urut</div>
            </th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Keluhan</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Butuh Diagnose</div>
			</th>
		</tr>
	</thead>
	<tfoot><tr><td colspan="3">
    <g:field type="button" onclick="alert('delete');" class="btn pull-left" name="delete_keluhan" id="delete_keluhan" value="Delete" />
	%{--<g:field type="button" onclick="openPreDiagnose();" class="btn pull-right" name="diagnose_keluhan" id="diagnose_keluhan" value="Diagnose" />--}%
	</td></tr></tfoot>
</table>

<g:javascript>
var addKeluhan;
var keluhanTable;
var reloadKeluhanTable;
var noUrut = 0;
$(function(){
    addKeluhan = function(){
		var inputKeluhan = $('#inputKeluhan').val();
		if(!inputKeluhan){
			alert('Silahkan isi keluhan');
		}else{
			var inputDiagnose = "";
			var selected = $("input[type='radio'][name='inputDiagnose']:checked");
			if (selected.length > 0) {
				inputDiagnose = selected.val();
			}


			noUrut++;

			$('#inputKeluhan').val("");

			keluhanTable.fnAddData({
						'noUrut': noUrut,
                    'id': '',
                    'keluhan': inputKeluhan,
                    'butuhDiagnose': inputDiagnose});
		}
	}
//	reloadKeluhanTable = function() {
//		keluhanTable.fnDraw();
//	}
	
	keluhanTable = $('#keluhan_datatables').dataTable({
		"bAutoWidth" : false,
		"bPaginate" : false,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "t",
		"bFilter": false,
		"bStateSave": false,
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": false,
		"bProcessing": false,
//		"bServerSide": true,
//		"sServerMethod": "POST",
		"iDisplayLength" : 1000, //maxLineDisplay is set in menu/maxLineDisplay.gsp
//		"sAjaxSource": "${g.createLink(action: "keluhanDatatablesList")}",
		"aoColumns": [

{
	"sName": "noUrut",
	"mDataProp": "noUrut",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this" checked="checked"> <input type="hidden" value="'+row['noUrut']+'">&nbsp;&nbsp;' + data;
	},
	
	"bSearchable": false,
	"bSortable": false,
	"bVisible": true
},

{
	"sName": "keluhan",
	"mDataProp": "keluhan",
	"aTargets": [1],
//	"mRender": function ( data, type, row ) {
//		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"> <input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;' + data;
//	},

	"bSearchable": false,
	"bSortable": false,
	"bVisible": true
}

,

{
	"sName": "butuhDiagnose",
	"mDataProp": "butuhDiagnose",
	"aTargets": [2],
	"mRender": function ( data, type, row ) {
		if(data == '1')
			return '<div class="controls">	<input type="radio" value="1" checked="checked" name="butuhDiagnose_'
						+row['noUrut']+'"> Ya <input type="radio" value="0" name="butuhDiagnose_'
						+row['noUrut']+'"> Tidak </div>';
		else 
			return '<div class="controls">	<input type="radio" value="1" name="butuhDiagnose_'
						+row['noUrut']+'"> Ya <input type="radio" value="0" checked="checked" name="butuhDiagnose_'
						+row['noUrut']+'"> Tidak </div>';
	},
	"bSearchable": false,
	"bSortable": false,
	"bVisible": true
}
]
//,
//        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
//						$.ajax({ "dataType": 'json',
//							"type": "POST",
//							"url": sSource,
//							"data": aoData ,
//							"success": function (json) {
//								fnCallback(json);
//							   },
//							"complete": function () {
//							   }
//						});
//		}
	});

	%{--$.ajax({ "dataType": 'json',--}%
							%{--"type": "POST",--}%
							%{--"url": "${g.createLink(action: "keluhanDatatablesList")}",--}%
							%{--"data": {--}%
							    %{--"sEcho": '1'--}%
							%{--} ,--}%
							%{--"success": function (json) {--}%

							   %{--},--}%
							%{--"complete": function () {--}%
							   %{--}--}%
						%{--});--}%
});
</g:javascript>
