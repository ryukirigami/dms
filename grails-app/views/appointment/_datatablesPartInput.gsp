<table id="inputPart_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover">
    <thead>
    <tr>
       <th style="border-bottom: none;padding: 5px;">
            <div>Kode Parts</div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div>Nama Parts</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Dibutuhkan pada proses</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Qty</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Satuan</div>
        </th>
        <th style="border-bottom: none;padding: 5px;">
            <div>Total Harga</div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var inputPartTable;
var reloadInputPartTable;
$(function(){

	reloadInputPartTable = function() {
		inputPartTable.fnDraw();
	}

//	$("th div input").bind('keypress', function(e) {
//		var code = (e.keyCode ? e.keyCode : e.which);
//		if(code == 13) {
//			e.stopPropagation();
//		 	inputPartTable.fnDraw();
//		}
//	});
//	$("th div input").click(function (e) {
//	 	e.stopPropagation();
//	});

	inputPartTable = $('#inputPart_datatables').dataTable({
//		"sScrollX": "100%",
//		"sScrollY": "400px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid't>",
		"bProcessing": false,
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": false,
		"bServerSide": false,
		"sServerMethod": "POST",
		"iDisplayLength" : 100, //maxLineDisplay is set in menu/maxLineDisplay.gsp
//		"sAjaxSource": "${g.createLink(action: "inputPartDatatablesList")}",
		"aoColumns": [
{
	"sName": "goods",
	"mDataProp": "goods",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "goods",
	"mDataProp": "goods2",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "proses",
	"mDataProp": "proses",
	"aTargets": [2],
	"bSearchable": true,
	"mRender": function ( data, type, row ) {
			var ps = [];
        <g:each in="${com.kombos.administrasi.NamaProsesBP.list()}">
            ps.push({id: ${it.id}, label: "${it?.m190NamaProsesBP}"});
        </g:each>

            var pCombo = '<select id="prosesBP'+row['id']+'" name="namaProsesBP.id" class="many-to-one inline-edit" style="width:105px;" >';
            pCombo += '<option value="1">[Dibutuhkan Pada Proses..]</option>';
            for(var i = 0; i < ps.length; i++){
            pCombo += '<option value="'+ps[i].id+'">'+ps[i].label+'</option>';
            }
            pCombo += '</select>';
            return pCombo;

	},
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "qty",
	"mDataProp": "qty",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"mRender": function ( data, type, row ) {
		return '<input type="text" style="width:100px;" value="1" class="pull-right inline-edit numeric" id="qty'+row['id']+'">';
	},
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "satuan1",
	"mDataProp": "satuan1",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
},
{
	"sName": "totalHarga",
	"mDataProp": "totalHarga",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

]
//,
//        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
//
//
//
//						var goods = $('#filter_goods input').val();
//						if(goods){
//							aoData.push(
//									{"name": 'sCriteria_goods', "value": goods}
//							);
//						}
//						var goods2 = $('#filter_goods2 input').val();
//						if(goods2){
//							aoData.push(
//									{"name": 'sCriteria_goods2', "value": goods2}
//							);
//						}
//
//
//
//
//						$.ajax({ "dataType": 'json',
//							"type": "POST",
//							"url": sSource,
//							"data": aoData ,
//							"success": function (json) {
//								fnCallback(json);
//							   },
//							"complete": function () {
//							   }
//						});
//		}
	});
});
</g:javascript>