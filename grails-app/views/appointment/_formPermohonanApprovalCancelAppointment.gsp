<%@ page import="com.kombos.administrasi.KegiatanApproval" %>
<g:javascript>
var approvalOrderCancelAppointment;
var sendApprovalCA;
var requestCancelAppointment;
$(function(){
sendApprovalCA = function(){
                    $.ajax({type:'POST',
                        data:$("#requestApprovalCancelAppointment").serialize(),
                        url:'/dms/appointment/requestApprovalCancelAppointment',
                        success:function(data,textStatus){
                            approvalOrderCancelAppointment(data);
                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){

                        }
                     });
                    return false;
            }
requestCancelAppointment = function(id) {
        $('#formAlasanCancelModal').modal('hide')
		var formPermohonanApproval = $('#formPermohonanApprovalCancelAppointmentModal').find('form');
		var checkParts = [];
        checkParts.push(appointmentId);
		if(checkParts.length < 1){
             alert('Anda belum memilih data yang akan ditambahkan');
        } else {
			$("#requestIds").val(JSON.stringify(checkParts));

			%{--$.ajax({type:'POST',--}%
				%{--url:'${request.contextPath}/appointment/generateNomorDokumenApprovalCancelAppointment',--}%
				%{--success:function(data,textStatus){--}%
					%{--$("#t770NoDokumen").val(data.value);--}%
				%{--},--}%
				%{--error:function(XMLHttpRequest,textStatus,errorThrown){},--}%
				%{--complete:function(XMLHttpRequest,textStatus){--}%
					%{--$('#spinner').fadeOut();--}%
				%{--}--}%
			%{--});--}%

			$("#formPermohonanApprovalCancelAppointmentModal").modal({
							"backdrop" : "static",
							"keyboard" : true,
							"show" : true
						});

		}
    };


	$("#formPermohonanApprovalCancelAppointmentModal").on("show", function() {
//			$("#formPermohonanApprovalCancelAppointmentModal").on("click", function(e) {
//				$("#formPermohonanApprovalCancelAppointmentModal").modal('hide');
//			});
		});
	$("#formPermohonanApprovalCancelAppointmentModal").on("hide", function() {
			            $("#formPermohonanApprovalCancelAppointmentModal a.btn").off("click");
		});

	approvalOrderCancelAppointment = function(data){
		$('#formPermohonanApprovalCancelAppointmentModal').modal('hide')
		toastr.success('<div>Cancel Booking Fee Approval has been sent.</div>');
		//vopTable.fnDraw();
	}
});
</g:javascript>
<div id="formPermohonanApprovalCancelAppointmentModal" class="modal hide">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" id="requestApprovalCancelAppointment">
                %{--<g:formRemote class="form-horizontal" name="requestApprovalCancelAppointment" on404="alert('not found!');" onSuccess="approvalOrderCancelAppointment(data);"--}%
                %{--url="[controller: 'appointment', action:'requestApprovalCancelAppointment']">--}%
                <input type="hidden" name="requestIds" id="requestIds" value="">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 500px;">
                    <div id="formPermohonanApprovalCancelAppointmentContent">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                        <div class="iu-content">
                            <div class="control-group fieldcontain">
                                <label class="control-label" for="kegiatanApproval">Nama Kegiatan</label>

                                <div class="controls">
                                    <g:select name="kegiatanApproval" id="kegiatanApproval"
                                              from="${KegiatanApproval.list()}" optionKey="id"
                                              value="${kegiatanApprovalCancelAppointment.id}"
                                              optionValue="m770KegiatanApproval" class="one-to-many"
                                              readonly="readonly"/>
                                </div>
                            </div>

                            <div class="control-group fieldcontain">
                                <label class="control-label" for="t770NoDokumen">
                                    Nomor Request
                                </label>

                                <div class="controls">
                                    <g:field type="text" name="t770NoDokumen" class="t770NoDokumen" value="${appointmentInstance?.reception?.t401NoAppointment}" readonly="readonly"/>
                                </div>
                            </div>

                            <div class="control-group fieldcontain">
                                <label class="control-label" for="t770TglJamSend">
                                    Tanggal dan Jam
                                </label>
                                <div class="controls">
                                    %{--<g:field type="text" name="t770TglJamSend" value="now" readonly="readonly"/>--}%
                                    <g:field type="text" name="t770TglJamSend" value="${new Date().format("dd-mm-yyyy HH:mm")}" readonly="readonly"/>
                                </div>
                            </div>

                            <div class="control-group fieldcontain">
                                <label class="control-label" for="approver">
                                    Approver(s)
                                </label>

                                <div class="controls">
                                    <g:textArea rows="3" cols="50" maxlength="20" name="approver" value="${approver}"
                                                readonly="readonly"/>
                                </div>
                            </div>

                            <div class="control-group fieldcontain">
                                <label class="control-label" for="pesan">
                                    Pesan
                                </label>

                                <div class="controls">
                                    <g:textArea rows="4" cols="50" maxlength="20" name="pesan" value=""/>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- dialog buttons -->
                <div class="modal-footer">
                    %{--<g:submitButton class="btn btn-primary create" name="send" value="${message(code: 'default.button.send.label', default: 'Send')}" />--}%
                    <g:field type="button" onclick="sendApprovalCA();" class="btn"
                             name="send" id="sendApprovalCancelAppointment"
                             value="Send"/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
            %{--</g:formRemote>--}%
        </div>
    </div>
</div>
