<%@ page import="com.kombos.maintable.AlasanCancel" %>
<g:javascript>
var selectAlasanCancelAppointment;
$(function(){

selectAlasanCancelAppointment = function(id) {
        $("#formAlasanCancelModal").modal({
							"backdrop" : "static",
							"keyboard" : true,
							"show" : true
						});

    };


	$("#formAlasanCancelModal").on("show", function() {
//			$("#formAlasanCancelModal").on("click", function(e) {
//				$("#formAlasanCancelModal").modal('hide');
//			});
		});
	$("#formAlasanCancelModal").on("hide", function() {
			            $("#formAlasanCancelModal a.btn").off("click");
		});


});
</g:javascript>
<div id="formAlasanCancelModal" class="modal hide">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" id="selectAlasanCancelForm">
                <div class="modal-body" style="max-height: 500px;">
                    <div>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                        <div class="iu-content">
                            <div class="control-group fieldcontain">
                                <label class="control-label" for="alasanCancel">Alasan Cancel</label>

                                <div class="controls">
                                    <g:select name="alasanCancel" id="alasanCancel"
                                              from="${AlasanCancel.list()}" optionKey="id"
                                              value=""
                                              optionValue="m402NamaAlasanCancel" class="one-to-many"
                                              />
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
                <!-- dialog buttons -->
                <div class="modal-footer">
                    %{--<g:submitButton class="btn btn-primary create" name="send" value="${message(code: 'default.button.send.label', default: 'Send')}" />--}%
                    <g:field type="button" onclick="requestCancelAppointment();" class="btn"
                             name="send" id="sendApprovalCancelAppointment"
                             value="Yes"/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </form>
            %{--</g:formRemote>--}%
        </div>
    </div>
</div>




