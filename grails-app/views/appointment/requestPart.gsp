<%@ page import="com.kombos.parts.RequestStatus" %>
<%@ page import="com.kombos.parts.SOQ" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="title" value="Request Parts" />
		<g:set var="permohonanId" value="RequestBookingFee" />
		<title>${title}</title>
		<r:require modules="baseapplayout, autoNumeric" />
		<g:javascript>

	var shrinkRequestPartTableLayout;
	var expandTableLayout;
	$(function(){ 

    orderParts = function(id) {
    	var checkParts = [];
		$("#ckp-table tbody .row-select").each(function() {
        if(this.checked){
            var id = $(this).next("input:hidden").val();

			checkParts.push(id);

        }
        });

        if(checkParts.length<1){
             alert('Anda belum memilih data yang akan ditambahkan');
        } else {
            var data = {};
            data["appointmentId"] = appointmentId;
			var checkPartsMap = JSON.stringify(checkParts);
            data["paIds"] =  checkPartsMap;
            for(var i = 0; i < checkParts.length; i++){
                data["tipeOrder_"+checkParts[i]] =  $("#tipeOrder"+checkParts[i])[0].value;
            }
			$('#spinner').fadeIn(1);
			$.ajax({type:'POST',
				data : data,
				url:'${request.contextPath}/appointment/orderPart',
				success:function(data,textStatus){
					 toastr.success("Order Part Sukses");
					 $('#appointmentRequestPartModal').modal('hide');
				},
				error:function(XMLHttpRequest,textStatus,errorThrown){},
				complete:function(XMLHttpRequest,textStatus){
					$('#spinner').fadeOut();
				}
			});
		}
    };

            shrinkRequestPartTableLayout = function(){
                if($("#ckp-table").hasClass("span12")){
                       $("#ckp-table").toggleClass("span12 span5");
                }
                $("#ckp-form").css("display","block");
               }

               expandRequestPartTableLayout = function(){
                   if($("#ckp-table").hasClass("span5")){
                       $("#ckp-table").toggleClass("span5 span12");
                   }
                $("#ckp-form").css("display","none");
               }

               });
        </g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left">${title}</span>
		<ul class="nav pull-right">
			
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="ckp-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
           	<g:render template="requestPartDataTables" />
           	<fieldset class="buttons controls" style="padding-top: 30px;">
					<g:field type="button" onclick="orderParts();" class="btn btn-primary" name="order-parts" id="order-parts" value="Order Parts" />
			</fieldset>
            
		</div>
		<div class="span7" id="ckp-form" style="display: none;"></div>
	</div>
	%{--<g:render template="formPermohonanApproval" />--}%
</body>
</html>
