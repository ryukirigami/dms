<g:javascript>

	function doOpenRequestPart(){
        $(".modal").modal('hide');
		openRequestPart();
	}

    function doDelete2(){
        var jobDelete = [];
        var partsDelete = []
        $("#job_order_datatables tbody .row-select").each(function() {
            if(this.checked){
                var idDelete = $(this).next("input:hidden").val()
                if(idDelete.indexOf("#")>-1){
                    idDelete = idDelete.substring(0,idDelete.indexOf("#"))
                    partsDelete.push(idDelete);
                }else{
                    jobDelete.push(idDelete);
                }
            }
        });
        if(jobDelete.length<1 && partsDelete.length<1){
            alert('Anda belum memilih data yang akan dihapus.')
            return
        }
        var json = JSON.stringify(jobDelete);
        var json2 = JSON.stringify(partsDelete);
        $.ajax({
            url:'${request.contextPath}/appointment/massdelete',
            type: "POST", // Always use POST when deleting data
            data: { jobs: json, parts : json2 },
            complete: function(xhr, status) {
                reloadjobnPartsTable();
//                reloadJoborderTable();
            }
        });
    }

</g:javascript>
<div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Appointment - Job Order</h3>
</div>
<div class="modal-body">
	<div class="box">
		<input type="button" class="btn cancel" onclick="openInputPart2();" value="Input Parts"/>
		<g:render template="jobnPartsDataTablesJobOrder" />
		<table class="display table" width="100%" cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px; width: 930px;">
			<tr>
				<td>
					%{--<input type="button" class="btn cancel" onclick="selectAllCheckbox('idKeluhan');" value="Select All"/>--}%
					%{--<input type="button" class="btn cancel" onclick="unselectAllCheckbox('idKeluhan');" value="Unselect All"/>--}%
					%{--<input type="button" class="btn cancel" onclick="deleteCheckbox('idKeluhan');" value="Delete"/>--}%
                    <input type="button" class="btn cancel" onclick="doDelete2();" value="Delete"/>
				</td>
			</tr>
		</table>
	</div>
</div>
<div class="modal-footer">
    <a onclick="doOpenRequestPart();" class="btn btn-success">Request Parts</a>
</div>