<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="cebfd_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
    <thead>
		<tr>
			<th style="border-bottom: none;padding: 5px;">
				<div>Kode Part</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Nama Part</div>
			</th>
			<th style="border-bottom: none;padding: 5px;" colspan="5">
				<div>Awal</div>
			</th>
			<th style="border-bottom: none;padding: 5px;" colspan="5">
				<div>Perubahan</div>
			</th>
			<th style="border-bottom: none;padding: 5px;">
				<div>Keterangan</div>
			</th>
		</tr>
		<tr>
			<th style="border-top: none; padding: 5px;"/>
			<th style="border-top: none; padding: 5px;"/>
			<th style="padding: 5px;">
				<div>Qty</div>
			</th>
			<th style="padding: 5px;">
				<div>Satuan</div>
			</th>
			<th style="padding: 5px;">
				<div>Min. DP</div>
			</th>
			<th style="padding: 5px;">
				<div>Harga</div>
			</th>
			<th style="padding: 5px;">
				<div>Total</div>
			</th>
			<th style="padding: 5px;">
				<div>Qty</div>
			</th>
			<th style="padding: 5px;">
				<div>Satuan</div>
			</th>
			<th style="padding: 5px;">
				<div>DP</div>
			</th>
			<th style="padding: 5px;">
				<div>Harga</div>
			</th>
			<th style="padding: 5px;">
				<div>Total</div>
			</th>
			<th style="border-top: none; padding: 5px;"/>
		</tr>
	</thead>
</table>

<g:javascript>
var checkOnValue;
    var cebfdTable;
    $(function(){
    checkOnValue = function() {
		var $this   = $(this);
		var value = $this.val();
		var nRow = $this.parents('tr')[0];
		var checkInput = $('input[type="checkbox"]', nRow);
		if(value){
			checkInput.attr("checked","checked");
		} else {
			checkInput.removeAttr("checked");
		}
	}
        cebfdTable = $('#cebfd_datatables').dataTable({
            "sScrollX": "984px%",
            "bScrollCollapse": true,
            "bAutoWidth" : false,
            "bPaginate" : true,
            "sInfo" : "",
            "sInfoEmpty" : "",
            "sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
            "bFilter": true,
            "bStateSave": false,
            "sPaginationType": 'bootstrap',
//        "bAutoWidth" : false,
//		"bPaginate" : false,
//		"sInfo" : "",
//		"sInfoEmpty" : "",
//		"sDom": "t",
//		"bFilter": false,
		"bStateSave": false,
            "fnInitComplete": function () {
                this.fnAdjustColumnSizing(true);
            },
            "bSort": true,
            "bSort": true,
            "bProcessing": true,
            "bServerSide": true,
            "sServerMethod": "POST",
            "iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
            "sAjaxSource": "${g.createLink(action: "cancelEditBookingFeeDatatablesList")}",
            "aoColumns": [
            {
                "sName": "kodePart",
                "mDataProp": "kodePart",
                "aTargets": [0],
                "mRender": function ( data, type, row ) {
                    return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+data+'" title="Select this"> <input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;' + data;
                },
                "bSortable": true,
                "sWidth":"200px",
                "bVisible": true
            },{
                "sName": "namaPart",
                "mDataProp": "namaPart",
                "aTargets": [1],
                "bSortable": true,
                "sWidth":"300px",
                "bVisible": true
            },{
                "sName": "requestQty",
                "mDataProp": "requestQty",
                "aTargets": [2],
//                "mRender": function ( data, type, row ) {
//                                                    if(data != null)
//                                                        return '<span class="pull-right numeric">'+data+'</span>';
//                                                    else
//                                                        return '<span></span>';
//                                            },
                "bSortable": true,
                "sWidth":"100px",
                "bVisible": true
            },{
                "sName": "requestSatuan",
                "mDataProp": "requestSatuan",
                "aTargets": [3],
                "bSortable": true,
                "sWidth":"150px",
                "bVisible": true
            },{
                "sName": "dp",
                "mDataProp": "dp",
                "aTargets": [4],
                "bSortable": true,
                "sWidth":"200px",
                "bVisible": true
//                "mRender": function ( data, type, row ) {
//                                                    if(data != null)
//                                                        return '<span class="pull-right numeric">'+data+'</span>';
//                                                    else
//                                                        return '<span></span>';
//                                            }
            },{
                "sName": "harga",
                "mDataProp": "harga",
                "aTargets": [5],
//                "mRender": function ( data, type, row ) {
//                                                    if(data != null)
//                                                        return '<span class="pull-right numeric">'+data+'</span>';
//                                                    else
//                                                        return '<span></span>';
//                                            },
                "bSortable": true,
                "sWidth":"200px",
                "bVisible": true
            },{
                "sName": "total",
                "mDataProp": "total",
                "aTargets": [6],
//                "mRender": function ( data, type, row ) {
//                                                    if(data != null)
//                                                        return '<span class="pull-right numeric">'+data+'</span>';
//                                                    else
//                                                        return '<span></span>';
//                                            },
                "bSortable": true,
                "sWidth":"200px",
                "bVisible": true
            },{
                "sName": "requestQty2",
                "mDataProp": "requestQty2",
                "aTargets": [7],
                "bSortable": true,
                "mRender": function ( data, type, row ) {
//                    if(data === null || data === 'null' || data === '' ){
                        return '<input type="text" style="width:100px;" value="'+data+'" class="pull-right inline-edit" id="qty'+row['id']+'">';
//                    } else {
//                        return '<span class="pull-right numeric">'+data+'</span>';
//                    }
                },
                "sWidth":"100px",
                "bVisible": true
            },{
                "sName": "requestSatuan2",
                "mDataProp": "requestSatuan2",
                "aTargets": [8],
                "bSortable": true,
                "sWidth":"150px",
                "bVisible": true
            },{
                "sName": "dp2",
                "mDataProp": "dp2",
                "aTargets": [9],
                "mRender": function ( data, type, row ) {
//                    if(data === null || data === 'null' || data === '' ){
                        return '<input type="text" style="width:100px;" value="'+data+'" class="pull-right inline-edit" id="dp'+row['id']+'">';
//                    } else {
//                        return '<span class="pull-right numeric">'+data+'</span>';
//                    }
                },
                "bSortable": true,
                "sWidth":"200px",
                "bVisible": true
            },{
                "sName": "harga2",
                "mDataProp": "harga2",
                "aTargets": [10],
                "bSortable": true,
                "sWidth":"200px",
                "bVisible": true
            },{
                "sName": "total2",
                "mDataProp": "total2",
                "aTargets": [11],
                "bSortable": true,
                "sWidth":"200px",
                "bVisible": true
            },{
                "sName": "keterangan",
                "mDataProp": "keterangan",
                "aTargets": [12],
                "mRender": function ( data, type, row ) {
//                    if(data === null || data === 'null' || data === '' ){
                        return '<input type="text" style="width:100px;" value="'+data+'" class="inline-edit" id="keterangan'+row['id']+'">';
//                    } else {
//                        return '<span class="pull-right numeric">'+data+'</span>';
//                    }
                },
                "bSortable": true,
                "sWidth":"200px",
                "bVisible": true
            }
            ],
            "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
                var selectAll = $('.select-all');
                selectAll.removeAttr('checked');
                $("#cebfd_datatables tbody .row-select").each(function() {
                    var id = $(this).next("input:hidden").val();
                    if(checked.indexOf(""+id)>=0){
                        checked[checked.indexOf(""+id)]="";
                    }
                    if(this.checked){
                        checked.push(""+id);
                    }
                });
			    aoData.push(
				    {"name": 'appointmentId', "value": appointmentId}
				);

				$.ajax({ "dataType": 'json',
					"type": "POST",
					"url": sSource,
					"data": aoData ,
					"success": function (json) {
						fnCallback(json);
						$('#cebfd_datatables').find('input.numeric').autoNumeric('init',{
							vMin:'0.00',
							vMax:'999999999999999999.00',
							aSep:''
						}).keypress(checkOnValue)
				    		.change(checkOnValue);
						$('#cebfd_datatables').find('select')
					    	.keypress(checkOnValue)
							.change(checkOnValue);
					    }
					});
		    }
        });
    })
</g:javascript>


			
