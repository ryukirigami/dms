<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-body" style="max-height: 415px;">
            <div id="appointmentAddJobContent">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="iu-content row-fluid">
                    <div class="span7 form-horizontal">
                        <div class="control-group">
                            <label class="control-label" for="kmSekarang">KM Sekarang<span class="required-indicator">*</span>
                            </label>
                            <div class="controls">
                                <g:textField name="kmSekarang" required=""/>
                            </div>
                        </div>
                        <span class="table-label">Keluhan & Permintaan Customer</span>
                        <g:render template="keluhanDataTables" />
                    </div>
                    <div class="span5 form-horizontal">
                        <div class="control-group">
                            <label class="control-label" for="petugasBooking">Petugas Booking
                            </label>
                            <div class="controls">
                                <g:textField name="petugasBooking" value="${petugasBooking}" disabled="disabled"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="jobSuggest">Job Suggest
                            </label>
                            <div class="controls">
                                <g:textArea name="jobSuggest" value="${jobSuggest}" disabled="disabled"/>
                            </div>
                        </div>
                    </div>
                    <div class="span11 form-horizontal">
                        <div class="control-group" style="margin-bottom: -4px;">
                            <label class="control-label" for="kategoriJob">Kategori Job<span class="required-indicator">*</span>
                            </label>
                            <div class="controls">
                                <g:select name="kategoriJob" from="${kategoriJobs}" optionKey="id"
                                          optionValue="m055KategoriJob" required=""
                                />
                            </div>
                        </div>
                        <div class="control-group" style="margin-bottom: -4px;">
                            <label class="control-label" for="workshopSumber">Workshop Sumber - Tujuan
                            </label>
                            <div class="controls">
                                <g:select id="workshopSumber" name="workshopSumber" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list(){eq("staDel","0");order("m011ID")}}" optionValue="${{it.m011NamaWorkshop}}" optionKey="id" required="" class="many-to-one"/> - <g:select id="workshopTujuan" name="workshopTujuan" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list(){eq("staDel","0");order("m011ID")}}" optionValue="${{it.m011NamaWorkshop}}" optionKey="id" required="" class="many-to-one"/>
                            </div>
                        </div>
                        <div class="control-group" style="margin-bottom: -4px;">
                            <label class="control-label" for="statusWarranty">Status Warranty<span class="required-indicator">*</span>
                            </label>
                            <div class="controls">
                                <g:select name="statusWarranty" from="${statusWarranties}" optionKey="id"

                                          optionValue="m058NamaStatusWarranty" required=""
                                />
                            </div>
                        </div>
                        <div class="control-group" style="margin-bottom: -4px;">
                            <label class="control-label" for="kriteriaPencarian">Kriteria Pencarian<span class="required-indicator">*</span>
                            </label>
                            <div class="controls">
                                <g:select name="kriteriaPencarian" from="${["Kode Job","Nama Job"]}"  required=""/>
                            </div>
                        </div>
                        <div class="control-group" style="margin-bottom: -4px;">
                            <label class="control-label" for="kataKunciPencarian">Kata Kunci Pencarian<span class="required-indicator">*</span>
                            </label>
                            <div class="controls">
                                <g:textField name="kataKunciPencarian" required=""/>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <fieldset class="buttons controls pull-right" style="padding-top: 10px;">
                            <g:field type="button" onclick="reloadAddJobTable();" class="btn" name="search_job" id="search_job" value="Search" />
                            <g:field type="button" onclick="openInputJob();" class="btn" name="input_job_baru" id="input_job_baru" value="Input Job Baru" />
                        </fieldset>
                    </div>
                    <g:render template="addJobDataTables" />
                </div>
            </div>

        </div>
        <!-- dialog buttons -->
        <div class="modal-footer">
            <button id='saveAppointmentAddJob' onclick="saveJobKeluhan();" type="button" class="btn btn-primary">Save</button>
            <button id='closeAppointmentAddJob' type="button" class="btn" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>