<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<r:require modules="baseapplayout" />
		<g:javascript>
var partTable_${idTable};
$(function(){ 
    if(partTable_${idTable})
    	partTable_${idTable}.dataTable().fnDestroy();
partTable_${idTable} = $('#part_datatables_${idTable}').dataTable({
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "t",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		 },
		 "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
		 	var aDataa = partTable_${idTable}.fnGetData(nRow);
		    if(aDataa.staNeedDP=="yes"){
			    $("#lblneedbookingfee").show();
			}
			return nRow;
		 },
		"bSort": false,
		"bProcessing": false,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "partDatatablesList")}",
		"aoColumns": [
{
               "mDataProp": null,
               "bSortable": false,
               "sWidth":"9px",
               "sDefaultContent": ''
},
{
	"sName": "kodePart",
	"mDataProp": "kodePart",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['idPart']+'" title="Select this"><input type="hidden" value="'+row['idPart']+'#">&nbsp;&nbsp;'+data;
	},
	"bSortable": false,
	"sWidth":"230px",
	"bVisible": true
},
{
	"sName": "namaPart",
	"mDataProp": "namaPart",
	"aTargets": [1],
	"bSortable": false,
	"sWidth":"420px",
	"bVisible": true
},
{
	"sName": "qty",
	"mDataProp": "qty",
	"aTargets": [2],
	"bSortable": false,
	"sWidth":"50px",
	"bVisible": true
},
{
	"sName": "satuan",
	"mDataProp": "satuan",
	"aTargets": [3],
	"bSortable": false,
	"sWidth":"50px",
	"bVisible": true
},
{
	"sName": "harga",
	"mDataProp": "harga",
	"aTargets": [3],
	"mRender": function ( data, type, row ) {
				            			if(data != null)
				            				return '<span class="pull-right numeric">'+data+'</span>';
				            			else
				            				return '<span></span>';
								},
	"bSortable": false,
	"sWidth":"153px",
	"bVisible": true
},
{
	"sName": "rate",
	"mDataProp": "rate",
	"aTargets": [4],
	"bSortable": false,
	"sWidth":"69px",
	"bVisible": true
},
{
	"sName": "statusWarranty",
	"mDataProp": "statusWarranty",
	"aTargets": [5],
	"bSortable": false,
	"sWidth":"73px",
	"bVisible": true
},
{
	"sName": "nominal",
	"mDataProp": "nominal",
	"aTargets": [6],
	"mRender": function ( data, type, row ) {
				            			if(data != null)
				            				return '<span class="pull-right numeric">'+data+'</span>';
				            			else 
				            				return '<span></span>';
								},
	"bSortable": false,
	"sWidth":"119px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						aoData.push(
									{"name": 'idJob', "value": "${idJob}"},
									{"name": 'idJobApp', "value": "${idJobApp}"}
						);
						
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
		</g:javascript>
	</head>
	<body>
<table id="part_datatables_${idTable}" cellpadding="0" cellspacing="0" border="0"
	class="display table table-striped table-bordered table-hover">
    <thead>
        <tr>
           <th></th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Kode Part</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Nama Part</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Qty</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Satuan</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">
				<div>Harga Satuan</div>
			</th>
			<th style="border-bottom: none; padding: 5px;">

			</th>
			%{--<th style="border-bottom: none; padding: 5px;">--}%

			%{--</th>--}%
			<th style="border-bottom: none; padding: 5px;">
				
			</th>			
         </tr>
			    </thead>
			    <tbody></tbody>
			</table>

</body>
</html>
