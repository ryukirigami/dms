<g:javascript>

	var countPembayaran = 1;
	function addPembayaran(){

		var inputMetodePembayaran = $('#inputMetodePembayaran').val();
		var inputMetodePembayaranText = $('#inputMetodePembayaran option:selected').text();
		if(!inputMetodePembayaran){
			alert('Silahkan isi Metode Pembayaran');
		}else{
			var inputJumlahPembayaran = $('#inputJumlahPembayaran').val();
			var inputKeteranganPembayaran = $('#inputKeteranganPembayaran').val();
			var noWo = $('#noWo').val();
			        $.ajax({type:'POST',
                            data: { jumlah: inputJumlahPembayaran , ket : inputKeteranganPembayaran , metodeBayar : inputMetodePembayaran, noWo : noWo},
                            url:'${request.contextPath}/appointment/addPembayaran',
                            success:function(data,textStatus){
                                  countPembayaran = (data.countPembayaran)

                            },
                            error:function(XMLHttpRequest,textStatus,errorThrown){},
                            complete:function(XMLHttpRequest,textStatus){
                                $('#spinner').fadeOut();
                            }
                        });
			var newRow = "<tr><td><input id='countPembayaran"+countPembayaran+"' type='hidden' value='"+countPembayaran+"'/><input id='metodePembayaran"+countPembayaran+"' type='hidden' value='"+inputMetodePembayaran+"'/><input id='jumlahPembayaran"+countPembayaran+"' type='hidden' value='"+inputJumlahPembayaran+"'/><input id='keteranganPembayaran"+countPembayaran+"' type='hidden' value='"+inputKeteranganPembayaran+"'/></td><td>"+inputMetodePembayaranText+"</td><td>"+inputJumlahPembayaran+"</td><td>"+inputKeteranganPembayaran+"</td></tr>";

			countPembayaran++;

			$('#tablePembayaranSummary > tbody:last').append(newRow);

			$('#inputMetodePembayaran').val("");
			$('#inputJumlahPembayaran').val("");
			$('#inputKeteranganPembayaran').val("");


		}
	}

	function saveSummary(){
		var aodataSummary =  new Array();
		var idx = 1;
		var nameMetodePembayaran = '';
		var nameJumlahPembayaran = '';
		var nameKeteranganPembayaran = '';

		var idMetodePembayaran = '';
		var idJumlahPembayaran = '';
		var idKeteranganPembayaran = '';

		aodataSummary.push(
			{"name": 'id', "value": $('#receptionId').val()}
		);

		aodataSummary.push(
			{"name": 'countRow', "value": countPembayaran}
		);


		var tanggalBayarBookingFee = $('#tanggalBayarBookingFee').val();
		tanggalBayarBookingFee += " "+$('#tanggalBayarBookingFee_hour').val();
		tanggalBayarBookingFee += ":"+$('#tanggalBayarBookingFee_minute').val();

		aodataSummary.push(
			{"name": 'tanggalBayarBookingFee', "value": tanggalBayarBookingFee}
		);

		var tanggalJamService = $('#tanggalJamService').val();
		tanggalJamService += " "+$('#tanggalJamService_hour').val();
		tanggalJamService += ":"+$('#tanggalJamService_minute').val();

		aodataSummary.push(
			{"name": 'tanggalJamService', "value": tanggalJamService}
		);

		var tanggalJamJanji = $('#tanggalJamJanji').val();
		tanggalJamJanji += " "+$('#tanggalJamJanji_hour').val();
		tanggalJamJanji += ":"+$('#tanggalJamJanji_minute').val();

		aodataSummary.push(
			{"name": 'tanggalJamJanji', "value": tanggalJamJanji}
		);

		var tanggalJamJanjiPenyerahan = $('#tanggalJamJanjiPenyerahan').val();
		tanggalJamJanjiPenyerahan += " "+$('#tanggalJamJanjiPenyerahan_hour').val();
		tanggalJamJanjiPenyerahan += ":"+$('#tanggalJamJanjiPenyerahan_minute').val();

		aodataSummary.push(
			{"name": 'tanggalJamJanjiPenyerahan', "value": tanggalJamJanjiPenyerahan}
		);

		while (idx < countPembayaran){
			nameMetodePembayaran = 'metodePembayaran' + idx;
			nameJumlahPembayaran = 'jumlahPembayaran' + idx;
			nameKeteranganPembayaran = 'keteranganPembayaran' + idx;

			idMetodePembayaran = '#metodePembayaran' + idx;
			idJumlahPembayaran = '#jumlahPembayaran' + idx;
			idKeteranganPembayaran = '#keteranganPembayaran' + idx;

			//nourut
			aodataSummary.push(
				{"name": nameMetodePembayaran, "value": $(idMetodePembayaran).val()}
			);
			//keluhan
			aodataSummary.push(
				{"name": nameJumlahPembayaran, "value": $(idJumlahPembayaran).val()}
			);
			//stadiagnose
			aodataSummary.push(
				{"name": nameKeteranganPembayaran, "value": $(idKeteranganPembayaran).val()}
			);
			idx++;
		}

		//saving Summary
		$('#spinner').fadeIn(1);
		$.ajax({
			url:'${request.contextPath}/reception/saveSummary',
			type: "POST",
			async : false,
			data: aodataSummary,
			success : function(data){
				$('#spinner').fadeOut();
				$("#dialog-reception-summary").modal("show");
			},
			error: function(xhr, textStatus, errorThrown) {
				alert('Internal server error');
			}
		});
	}
</g:javascript>
<div class="modal-header">
    %{--<a class="close" data-dismiss="modal">×</a>--}%
    <h3>Appointment Summary</h3>
</div>
<div class="modal-body">
    <div class="box">
        <table class="table">
            <tbody>
            <tr>
                <td>
                    <table class="table table-bordered table-dark table-hover">
                        <tbody>
                        <tr>
                            <td style="width: 30%"><span
                                    class="property-label">Nama Customer</span></td>
                            <td><span class="property-value" id="summaryNamaCustomer">
                                %{--Elizabeth--}%
                            </span></td>
                        </tr>
                        <tr>
                            <td><span
                                    class="property-label">Alamat</span></td>
                            <td><span class="property-value" id="summaryAlamat">
                                %{--Jl Rasuna Said--}%
                            </span></td>
                        </tr>
                        <tr>
                            <td><span
                                    class="property-label">Telepon</span></td>
                            <td><span class="property-value" id="summaryTelepon">
                                %{--089191919--}%
                            </span></td>
                        </tr>
                        <tr>
                            <td><span
                                    class="property-label">Mobil</span></td>
                            <td><span class="property-value" id="summaryMobil">
                                %{--Corolla Altis--}%
                            </span></td>
                        </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered table-dark table-hover">
                        <tbody>
                        <tr>
                            <td><span
                                    class="property-label">FIR</span></td>
                            <td><span class="property-value">
                                <input type="radio" name='fir' value="Ya"/>Ya
                                <input type="radio" name='fir' value="Tidak"/>Tidak
                            </span></td>
                        </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered table-dark table-hover">
                        <tbody>
                        <tr>
                            <td><span
                                    class="property-label">Keluhan</span></td>
                            <td><span class="property-value">
                                <textarea id="keluhanSummary" cols="300" rows="3"/>
                            </span></td>
                        </tr>
                        </tbody>
                    </table>
                    <!--table class="table table-bordered table-dark table-hover">
                        <tbody>
                        <tr>
                            <td><span
                                    class="property-label">Permintaan</span></td>
                            <td><span class="property-value">
                                <textarea cols="300" rows="3"/>
                            </span></td>
                        </tr>
                        </tbody>
                    </table-->
                </td>
                <td>
                    <table id="tableJobSummary" class="table table-bordered table-dark table-hover">
                        <tbody>
                        <tr>
                            <td><span class="property-label">No</span></td>
                            <td><span class="property-value">Nama Job</span></td>
                            <td><span class="property-value">Harga Job</span></td>
                            <td><span class="property-value">Harga Parts</span></td>
                            <td><span class="property-value">Total</span></td>
                        </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered table-dark table-hover">
                        <tbody>
                        <tr>
                            <td style="width: 45%"><span class="property-label">Tanggal Jam Bayar Booking Fee</span></td>
                            <td>
								<span class="property-value" id="summaryTanggalJamBayarBookingFee">
								%{--<ba:datePicker format="dd/mm/yyyy" required="false" name="tanggalBayarBookingFee" precision="day" style="width: 180px"/>--}%
								</span>
							</td>
                            %{--<td>--}%
								%{--<span class="property-value">--}%
									%{--<select id="tanggalBayarBookingFee_hour" name="tanggalBayarBookingFee_hour" style="width: 60px" required="">--}%
										%{--%{--}%
											%{--for (int i=0;i<24;i++){--}%
												%{--if(i<10){--}%
													%{--out.println('<option value="'+i+'">0'+i+'</option>');--}%
												%{--} else {--}%
													%{--out.println('<option value="'+i+'">'+i+'</option>');--}%
												%{--}--}%
											%{--}--}%
										%{--}%--}%
									%{--</select> H :--}%
									%{--<select id="tanggalBayarBookingFee_minute" name="tanggalBayarBookingFee_minute" style="width: 60px" required="">--}%
										%{--%{--}%
											%{--for (int i=0;i<60;i++){--}%
												%{--if(i<10){--}%
													%{--out.println('<option value="'+i+'">0'+i+'</option>');--}%
												%{--} else {--}%
													%{--out.println('<option value="'+i+'">'+i+'</option>');--}%
												%{--}--}%
											%{--}--}%
										%{--}%--}%
									%{--</select> m--}%
								%{--</span>--}%
							%{--</td>--}%
                        </tr>
                        <tr>
                            <td><span class="property-label">Tanggal Jam Service</span></td>
                            <td>
								<span class="property-value" id="summaryTanggalJamService">
								%{--<ba:datePicker format="dd/mm/yyyy" required="false" name="tanggalJamService" precision="day" style="width: 180px"/>--}%
								</span>
							</td>
                            %{--<td>--}%
								%{--<span class="property-value">--}%
									%{--<select id="tanggalJamService_hour" name="tanggalJamService_hour" style="width: 60px" required="">--}%
										%{--%{--}%
											%{--for (int i=0;i<24;i++){--}%
												%{--if(i<10){--}%
													%{--out.println('<option value="'+i+'">0'+i+'</option>');--}%
												%{--} else {--}%
													%{--out.println('<option value="'+i+'">'+i+'</option>');--}%
												%{--}--}%
											%{--}--}%
										%{--}%--}%
									%{--</select> H :--}%
									%{--<select id="tanggalJamService_minute" name="tanggalJamService_minute" style="width: 60px" required="">--}%
										%{--%{--}%
											%{--for (int i=0;i<60;i++){--}%
												%{--if(i<10){--}%
													%{--out.println('<option value="'+i+'">0'+i+'</option>');--}%
												%{--} else {--}%
													%{--out.println('<option value="'+i+'">'+i+'</option>');--}%
												%{--}--}%
											%{--}--}%
										%{--}%--}%
									%{--</select> m--}%
								%{--</span>--}%
							%{--</td>--}%
                        </tr>
                        <tr>
                            <td><span class="property-label">Tanggal Jam Janji Datang</span></td>
                            <td>
								<span class="property-value" id="summaryTanggalJamJanjiDatang">
								%{--<ba:datePicker format="dd/mm/yyyy" required="false" name="tanggalJamJanji" precision="day" style="width: 180px"/>--}%
								</span>
							</td>
                            %{--<td>--}%
								%{--<span class="property-value">--}%
									%{--<select id="tanggalJamJanji_hour" name="tanggalJamJanji_hour" style="width: 60px" required="">--}%
										%{--%{--}%
											%{--for (int i=0;i<24;i++){--}%
												%{--if(i<10){--}%
													%{--out.println('<option value="'+i+'">0'+i+'</option>');--}%
												%{--} else {--}%
													%{--out.println('<option value="'+i+'">'+i+'</option>');--}%
												%{--}--}%
											%{--}--}%
										%{--}%--}%
									%{--</select> H :--}%
									%{--<select id="tanggalJamJanji_minute" name="tanggalJamJanji_minute" style="width: 60px" required="">--}%
										%{--%{--}%
											%{--for (int i=0;i<60;i++){--}%
												%{--if(i<10){--}%
													%{--out.println('<option value="'+i+'">0'+i+'</option>');--}%
												%{--} else {--}%
													%{--out.println('<option value="'+i+'">'+i+'</option>');--}%
												%{--}--}%
											%{--}--}%
										%{--}%--}%
									%{--</select> m--}%
								%{--</span>--}%
							%{--</td>--}%
                        </tr>
                        <tr>
                            <td><span class="property-label">Tanggal Jam Janji Penyerahan</span></td>
                            <td>
								<span class="property-value" id="summaryTanggalJamJanjiPenyerahan">
								%{--<ba:datePicker format="dd/mm/yyyy" required="false" name="tanggalJamJanjiPenyerahan" precision="day" style="width: 180px"/>--}%
								</span>
							</td>
                            %{--<td>--}%
								%{--<span class="property-value">--}%
									%{--<select id="tanggalJamJanjiPenyerahan_hour" name="tanggalJamJanjiPenyerahan_hour" style="width: 60px" required="">--}%
										%{--%{--}%
											%{--for (int i=0;i<24;i++){--}%
												%{--if(i<10){--}%
													%{--out.println('<option value="'+i+'">0'+i+'</option>');--}%
												%{--} else {--}%
													%{--out.println('<option value="'+i+'">'+i+'</option>');--}%
												%{--}--}%
											%{--}--}%
										%{--}%--}%
									%{--</select> H :--}%
									%{--<select id="tanggalJamJanjiPenyerahan_minute" name="tanggalJamJanjiPenyerahan_minute" style="width: 60px" required="">--}%
										%{--%{--}%
											%{--for (int i=0;i<60;i++){--}%
												%{--if(i<10){--}%
													%{--out.println('<option value="'+i+'">0'+i+'</option>');--}%
												%{--} else {--}%
													%{--out.println('<option value="'+i+'">'+i+'</option>');--}%
												%{--}--}%
											%{--}--}%
										%{--}%--}%
									%{--</select> m--}%
								%{--</span>--}%
							%{--</td>--}%
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    <div class="dataTables_scroll">
        <table class="" width="100%" cellspacing="0" cellpadding="0" border="0" style="margin-left: 0px;">
            <tr>
                <th>
                    Metode Pembayaran
                </th>
                <th>
                    Jumlah Pembayaran
                </th>
                <th>
                    Keterangan
                </th>
                <th>
                    &nbsp;
                </th>
            </tr>
            <tr>
                <td>
                    <g:select id="inputMetodePembayaran" name="inputMetodePembayaran" from="${com.kombos.maintable.MetodeBayar.list()}" optionKey="id" class="many-to-one"/>
                </td>
                <td>
                    <input type="text" name="inputJumlahPembayaran" id="inputJumlahPembayaran"/>
                </td>
                <td>
                    <input type="text" name="inputKeteranganPembayaran" id="inputKeteranganPembayaran" value="BOOKING FEE" />
                </td>
                <td>
                    <input type="button" class="btn cancel" onclick="addPembayaran();" value="Add"/>
                </td>
            </tr>
        </table>
        <table id="tablePembayaranSummary" class="table table-bordered table-dark table-hover">
            <tbody>
            <tr>
                <td><span class="property-label">No</span></td>
                <td><span class="property-value">Metode Pembayaran</span></td>
                <td><span class="property-value">Jumlah Pembayaran</span></td>
                <td><span class="property-value">Keterangan</span></td>
            </tr>
            </tbody>
        </table>
        <%--
        <div class="span12" id="companyDealerPopUp-table">
            <g:render template="datatablesMetodePembayaran"/>
        </div>
        --%>
            </div>
    </div>
</div>
<div class="modal-footer">
    %{--<a onclick="saveSummary();" class="btn btn-success" id="add_part_btn">Close</a>--}%
    <a class="btn btn-success" id="add_part_btn" onclick="closeDialog('appointmentSummaryModal');">Close</a>
    %{--<a href="javascript:void(0);" onclick="closeDialog('appointmentSummaryModal');" class="btn btn-success" id="add_part_btn">Close</a>--}%
</div>