<%@ page import="com.kombos.baseapp.utils.DatatablesUtilService; com.kombos.reception.Appointment" %>
<%@ page import="com.kombos.administrasi.KegiatanApproval" %>
<%@ page import="com.kombos.administrasi.KategoriJob"%>
<%@ page import="com.kombos.administrasi.CompanyDealer"%>
<%@ page import="com.kombos.administrasi.Operation" %>
<%
    def datatablesUtilService = new DatatablesUtilService();
%>

<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName" value="Appointment"/>
<title><g:message code="default.list.label" args="[entityName]"/></title>
<r:require modules="baseapplayout, baseapplist"/>
%{--<r:require modules="modalBootstrap_patch"/>--}%
<g:javascript>
      //  var dirty = false;
        var closeModal;
        var appointmentId = "${appointmentInstance?.id}";
        var noAppointment="";
        var namaPemilik = "";
        var alamatPemilik = "";
        var telpPemilik = "";
        var mobilPemilik = "";
        var tglBayarBooking = "";
        var tgljanjiDatang = "";
        var hcId = null;
        var newAppointment;
        var openDataCustomer;
        var openCancelEditBookingFeeModal;
        var butuhDiagnose = false;
        var showAppSummary;
        var openAppSummary;
        var doDelete;
        var keluhanSummary = false;
        var jobSummary = false;
        var editAppointment;
        var openInputJobDanKeluhan;
        var openInputPart;
        var openRequestPart;
        var openPreDiagnose;
        var saveJobKeluhan;
        var savePartApp;
        var saveApp;
        var printAppointmentGR;
        var staValidate = 0;
        var printAppointmentBP;
        var idVincode = '-1';
        var selectAllCheckbox;
        var unselectAllCheckbox;
        var nilaiSpk = -1;
        var deleteCheckbox;
        var openDialog;
        var openJobKeluhan;
        var closeDialog;
        var staNewEdit = "";
        var idJobTempPart;
        var KM;
        var isValidated = false;
		var openCustomer;


        openJobKeluhan();
        $(function(){
        var cekCustLama = "${custLamaWeb?.id}";
        if(cekCustLama){
            showAppointment("${vehicleWeb?.kodeKotaNoPol?.m116ID}", "${vehicleWeb?.t183NoPolTengah}", "${vehicleWeb?.t183NoPolBelakang}");
        }
            openDialog = function(divModal){
                $(".modal").modal("hide");
                $("#"+divModal).modal({
                            "backdrop" : "static",
                            "keyboard" : true,
                            "show" : true
                        }).css({'width': '1100px','margin-left': function () {return -($(this).width() / 2);}});
            }

            closeDialog = function (divModal){
            $( "#"+divModal ).modal("hide");
            }
            selectAllCheckbox = function(className){
            $('.'+className).prop('checked', true);
            }

            unselectAllCheckbox = function(className){
            $('.'+className).prop('checked', false);
            }

            deleteCheckbox = function(className){
            $('.'+className).each(function() {
                if(this.checked){
                    $(this).closest("tr").remove();
                }
            });

}

            function initButtonPreDiagnose(id){
                $('#btnPrintPrediagnose').show();
                $('#printPrediagnose').html("<a id='hrefPrintPreDiagnose' href='${request.contextPath}/reception/printPrediagnosis?id="+id+"' >Print Prediagnose</a>");
		    }


            printAppointmentGR = function (){
                window.location.href = '${request.getContextPath()}/appointment/printAppointmentGR?id='+appointmentId+'&jenisApp=GR';
            }
            printAppointmentBP = function (){
                window.location.href = '${request.getContextPath()}/appointment/printAppointmentGR?id='+appointmentId+'&jenisApp=BP';
            }

            saveApp = function(e) {
                    var dataSave = 0;
                    var jum = $('#lblfooternominal').text();
                    $("#job_n_parts_datatables tbody .row-select").each(function() {
                         dataSave++;
                    });
                    if(dataSave<=0){
                       alert('Belum ada job yang diinputkan');
                       return;
                    }

                    if(!kodeKota.val())
                        kodeKota.focus();
                    else if(!nomorTengah.val())
                        nomorTengah.focus();
//                    else if(!nomorBelakang.val())
//                        nomorBelakang.focus();
                    else  {
                        if(staTHS==-1){
                            alert('Tentukan apakah THS?');
                            return
                        }
                        var almt = $('#alamatThs').val().replace(" ","");
                        if(staTHS!=1 && almt==""){
                            alert('Anda belum menambahkan alamat THS');
                            return
                        }
                        if(staTHS==1){
                            almt = "";
                        }
                        $('#spinner').fadeIn(1);
                        var idCustLamaWeb = $("#idCustLamaWeb").val();
                        var estRate = $("#lblrate").text();
                        var estTotal = $("#lbltotal").text();
                        var lblfooternominal =$('#lblfooternominal').text();
                        var jenisapp = $('#jenisApp').val();
                        var sumber = $('#workshopSumber').val();
                        var KM = $('#i_km').val();
                        var keluhan = $('#inputKeluhan').val();
                        $.ajax({url: '${request.contextPath}/appointment/saveApp',
                            type: "POST",
                            data: {
                                    appointmentId: appointmentId,
                                    kodeKota: kodeKota.val(),
                                    nomorTengah: nomorTengah.val(),
                                    nomorBelakang: nomorBelakang.val(),
                                    ths:staTHS,
                                    alamatTHS:almt,
                                    idCustLamaWeb: idCustLamaWeb,
                                    estRate:estRate,
                                    estTotal:estTotal,
                                    lblfooternominal:lblfooternominal,
                                    jumlah : jum,
                                    jenisApp : jenisapp,
                                    km : KM,
                                    keluhan : keluhan

                                },
                            success: function(data) {
                                if(data.status == 'ok'){
                                if(staNewEdit=="getNewAppointment"){
                                    initAfterSave();
                                }
                                toastr.success('<div>Appointment has been saved.</div>');
//                                    dirty = false;
                                }
                            },
                            complete : function (req, err) {
                                $('#spinner').fadeOut();
                                loading = false;
                            }
                        });
                }
            }

            savePartApp = function() {
                var partparams = {};
                partparams['appointmentId'] =  appointmentId;
                partparams['idJob'] =  idJobTempPart;
                var checkPart =[];
                var id = 1;
                $("#inputPart_datatables tbody .row-select").each(function() {
                    if(this.checked){
                        var nRow = $(this).parents('tr')[0];
                        var aData = inputPartTable.fnGetData(nRow);
                        partparams['part_' + id] = aData['id'];
                        partparams['prosesBP_' + id] = $('#prosesBP' + aData['id']).val();
                        partparams['qty_' + id] = $('#qty' + aData['id']).val();
                        checkPart.push(id);
                       id++;
                    }
                });

                partparams['countPart'] = id;
                $.ajax({url: '${request.contextPath}/appointment/savePart',
                    type: "POST",
                    data: partparams,
                    success: function(data) {
                        toastr.success('Data parts berhasil ditambahkan');
                        $('#appointmentInputPartModal').modal('hide');
                        reloadjobnPartsTable();
                    },
                    complete : function (req, err) {
                        $('#spinner').fadeOut();
                        loading = false;
                    }
                });
            }
            saveJobKeluhan = function() {
                var jobkeluhanparams = {};
                jobkeluhanparams['appointmentId'] =  appointmentId;

                var checkKeluhan =[];
                $("#keluhan_datatables tbody .row-select").each(function() {
                    if(this.checked){
                        var id = $(this).next("input:hidden").val();
                        var nRow = $(this).parents('tr')[0];
                        var aData = keluhanTable.fnGetData(nRow);
                        jobkeluhanparams['keluhan_' + id] = aData['keluhan']
                        jobkeluhanparams['butuhDiagnose_' + id] = aData['butuhDiagnose']
                        checkKeluhan.push(id);

                    }
                });
                jobkeluhanparams['keluhan_ids'] = JSON.stringify(checkKeluhan);

                var checkJob =[];
                var id = 1;
                $("#addJob_datatables tbody .row-select").each(function() {
                    if(this.checked){
                        var nRow = $(this).parents('tr')[0];
                        var aData = addJobTable.fnGetData(nRow);
                        jobkeluhanparams['operation_' + id] = aData['id'];
                        jobkeluhanparams['noKeluhan_' + id] = $('#noKeluhan_' + aData['kodeJob']).val();
                        checkJob.push(id);
                       id++;
                    }
                });

                jobkeluhanparams['job_ids'] = JSON.stringify(checkJob);
                jobkeluhanparams['statusWarranty'] = $("#staWarranty").val();
                $.ajax({url: '${request.contextPath}/appointment/saveJobKeluhan',
                    type: "POST",
                    data: jobkeluhanparams,
                    success: function(data) {

                        $('#appointmentAddJobModal').modal('hide');
                        reloadJoborderTable();
                        reloadjobnPartsTable();
                    },
                    complete : function (req, err) {
                        $('#spinner').fadeOut();
                        loading = false;
                    }
                });
            }

            //ian
            doDelete = function (){
            var jobDelete = [];
            var partsDelete = [];
            $("#job_n_parts_datatables tbody .row-select").each(function() {

                if(this.checked){
//                    .next("input:hidden")
                    var idDelete = $(this).next("input:hidden").val()
                    if(idDelete.indexOf("#")>-1){
                        idDelete = idDelete.substring(0,idDelete.indexOf("#"))
//                        alert(idDelete)
                        partsDelete.push(idDelete);
                    }else{
                        jobDelete.push(idDelete);
                    }
                }
            });
            if(jobDelete.length<1 && partsDelete.length<1){
                alert('Anda belum memilih data yang akan dihapus.')
                return
            }
            var json = JSON.stringify(jobDelete);
            var json2 = JSON.stringify(partsDelete);
            $.ajax({
                url:'${request.contextPath}/appointment/massdelete',
                type: "POST", // Always use POST when deleting data
                data: { jobs: json, parts : json2 },
                complete: function(xhr, status) {
                    reloadjobnPartsTable();
                    toastr.success('<div>Succes Delete.</div>');
                }
            });
        }

            openAppSummary = function() {
                var noDepan = $('#kodeKota').val();
                var noTengah = $('#nomorTengah').val();
                var noBelakang = $('#nomorBelakang').val();
                if(noDepan=="" || noDepan==null || noTengah=="" || noTengah==null ){
                    alert('Isi dan Lengkapi Nomor Polisinya Terlebih Dahulu');
                    return
                }
                if(jobSummary && keluhanSummary)
                $("#appointmentSummaryModal").modal({
                            "backdrop" : "static",
                            "keyboard" : true,
                            "show" : true
                        }).css({'width': '1100px','margin-left': function () {return -($(this).width() / 2);}});
            }
            showAppSummary = function() {
                $("#summaryNamaCustomer").text("");
                $("#summaryAlamat").text("");
                $("#summaryTelepon").text("");
                $("#summaryMobil").text("");
                $("#summaryNamaCustomer").text(namaPemilik);
                $("#summaryAlamat").text(alamatPemilik);
                $("#summaryTelepon").text(telpPemilik);
                $("#summaryMobil").text(mobilPemilik);

                var $radios = $('input:radio[name=fir]');
                if(butuhDiagnose){
                    $radios.filter('[value=Ya]').prop('checked', true);
                    $radios.filter('[value=Tidak]').prop('checked', false);
                }else{
                     $radios.filter('[value=Ya]').prop('checked', false);
                     $radios.filter('[value=Tidak]').prop('checked', true);
                }

                jobSummary = false;
                keluhanSummary = false;
                $.ajax({
                    url:'${request.getContextPath()}/appointment/getDataJobSummary?id='+appointmentId,
                    type: 'POST',
                    success: function (res) {
                        $('#tableJobSummary').find("tr:gt(0)").remove();
                        $('#tableJobSummary').append(res);
                        jobSummary = true;
                        openAppSummary();
                    },
                    error: function (data, status, e){
                        alert(e);
                    },
                    complete: function(xhr, status) {

                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

                $.ajax({
                    url:'${request.getContextPath()}/appointment/getDataKeluhanSummary?id='+appointmentId,
                    type: 'POST',
                    success: function (res) {
                        $('#keluhanSummary').val(res);
                        keluhanSummary = true;
                        openAppSummary();
                    },
                    error: function (data, status, e){
                        alert(e);
                    },
                    complete: function(xhr, status) {

                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

            }

            openInputJobDanKeluhan = function() {
                var depan = $('#kodeKota').val();
                var tengah=$('#nomorTengah').val();
                var belakang=$('#nomorBelakang').val();

                if(depan=="" || depan==null || tengah=="" || tengah==null ){
                    alert('Isi dan Lengkapi Data Nomor Polisi Terlebih Dahulu')
                    return
                }
                 $("#appointmentAddJobModal").modal({
                            "backdrop" : "static",
                            "keyboard" : true,
                            "show" : true
                        }).css({'width': '1100px','margin-left': function () {return -($(this).width() / 2);}});
            }






        openInputPart = function() {
        var length=0;
        var idJobTemp;
        $("#job_n_parts_datatables tbody .row-select").each(function() {
            if(this.checked){
                length++;
                idJobTemp = $(this).next("input:hidden").val();
            }
        });
        if(length==0){
            alert('Pilih satu job yang akan ditambahkan part');
            return;
        }
        if(length>1){
            alert('Anda hanya dapat memilih satu job');
            return;
        }
        idJobTempPart = idJobTemp;
        $("#appointmentInputPartModal").modal({
                    "backdrop" : "static",
                    "keyboard" : true,
                    "show" : true
                }).css({'width': '1100px','margin-left': function () {return -($(this).width() / 2);}});
        var inputPartTable = $('#inputPart_datatables').dataTable();
        if ( inputPartTable.length > 0 ) {
            inputPartTable.fnAdjustColumnSizing();
        }
        var addPartTable2 = $('#addPart_datatables').dataTable();
        if ( addPartTable2.length > 0 ) {
            addPartTable2.fnAdjustColumnSizing();
        }
       }


openInputPart2 = function() {
$('#tableInputParts').find("tr:gt(0)").remove();
var length=0;
var idJobTemp;
$("#job_order_datatables tbody .row-select").each(function() {
    if(this.checked){
        length++;
        idJobTemp = $(this).next("input:hidden").val();
    }
});
if(length==0){
    alert('Pilih satu job yang akan ditambahkan part');
    return;
}
if(length>1){
    alert('Anda hanya dapat memilih satu job');
    return;
}
idJobTempPart = idJobTemp;
$(".modal").modal('hide');
$("#appointmentInputPart2Modal").modal({
            "backdrop" : "static",
            "keyboard" : true,
            "show" : true
        }).css({'width': '1100px','margin-left': function () {return -($(this).width() / 2);}});
var inputPartTable = $('#inputPart_datatables').dataTable();
if ( inputPartTable.length > 0 ) {
    inputPartTable.fnAdjustColumnSizing();
}
var addPartTable2 = $('#addPart_datatables').dataTable();
if ( addPartTable2.length > 0 ) {
    addPartTable2.fnAdjustColumnSizing();
}
}

isCharOnly = function(e) {
e = e || event;
return /[a-zA-Z-]/i.test(
        String.fromCharCode(e.charCode || e.keyCode)
) || !e.charCode && e.keyCode  < 48;
}

isNumberKey = function(evt){
var charCode = (evt.which) ? evt.which : evt.keyCode
if (charCode > 31 && (charCode < 48 || charCode > 57)){
    return false;
}
else{
    return true;
}
}

$(".charonly").bind('keypress',isCharOnly);

$(".numberonly").bind('keypress',isNumberKey);

openRequestPart = function(){
    $("#requestPartContent").empty();
    $.ajax({type:'POST',
        url:'${request.contextPath}/appointment/requestPart',
                        success:function(data,textStatus){
                                $("#requestPartContent").html(data);
                                $("#appointmentRequestPartModal").modal({
                                    "backdrop" : "static",
                                    "keyboard" : true,
                                    "show" : true
                                }).css({'width': '1200px','margin-left': function () {return -($(this).width() / 2);}});

                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });

            }
            openInputJob = function() {
                $("#appointmentInputJobModal").modal({
                            "backdrop" : "static",
                            "keyboard" : true,
                            "show" : true
                        }).css({'width': '700px','margin-left': function () {return -($(this).width() / 2);}});
            }

            openPreDiagnose = function() {
              var km = $('#i_km').val();
              var kodeKota=$('#kodeKota').val();
              var noPolTengah=$('#nomorTengah').val();
              var noPolBelakang=$('#nomorBelakang').val();
              $('#lblnopoldiagnose').text(kodeKota+" "+ noPolTengah +" "+ noPolBelakang);
              $('#lblkmdiagnose').text(km);
              $('#lblmobildiagnose').text(mobilPemilik)
              $('#keluhanCust').text($('#inputKeluhan').val())
              if(km==""){
                    alert('Masukan KM sekarang terlebih dahulu');
                    return
              }
                $("#appointmentPreDiagnoseModal").modal({
                            "backdrop" : "static",
                            "keyboard" : true,
                            "show" : true
                        }).css({'width': '1200px','margin-left': function () {return -($(this).width() / 2);}});
            }


            newAppointment = function(){
                $("#lblneedbookingfee").hide();
                cekJob = [];
                var noDepan = $('#kodeKota').val();
                var noTengah = $('#nomorTengah').val();
                var noBelakang = $('#nomorBelakang').val();
                var appointmentIdTemp = $('#appointmentId').val()
                var dataUrl = '${request.contextPath}/appointment/'+staNewEdit
                $('#spinner').fadeIn(1);
                $.ajax({
                url: dataUrl,
                type: "POST",
                data: {kodeKotaNoPol : noDepan,noPolTengah : noTengah, noPolBelakang : noBelakang,idAppointment : appointmentIdTemp },
                success: function(data) {
                            var kosong = false;
                            if(data[0].hasil=="ada"){
                                $('#tanggalAppointment').show();
                                $('#noAppointment').show();
                                if(staNewEdit=="getAppointment"){
                                    initBeforeSave();
                                    initAfterSave()
                                }else{
                                    isValidated = true;
                                }
                                staValidate=1;
                                document.getElementById('lblfa').innerHTML = data[0].fa
                                document.getElementById('survey_tpss').innerHTML = data[0].tpss
                                document.getElementById('survey_follow_up').innerHTML = data[0].fu
                                document.getElementById('lblpks').innerHTML = data[0].pks
                                document.getElementById('survey_nama').innerHTML = data[0].pemakai
                                document.getElementById('survey_driver').innerHTML = data[0].driver
                                document.getElementById('survey_mobil').innerHTML = data[0].mobil
                                document.getElementById('lbltglbookingfee').innerHTML = data[0].tanggalBayar
                                document.getElementById('lbltgljanjidatang').innerHTML = data[0].tanggalDatang
                                document.getElementById('lbltgljanjiservice').innerHTML = data[0].tanggalMulai
                                document.getElementById('lbltgldelivery').innerHTML = data[0].tanggalDelivery
                                $('#txtJobSuggest').val(data[0].jobSuggest);
                                namaPemilik = data[0].pemakai;
                                alamatPemilik = data[0].alamat;
                                telpPemilik = data[0].telp;
                                mobilPemilik = data[0].mobil;
                                tglBayarBooking = data[0].tanggalBayar
                                tgljanjiDatang = data[0].tanggalDatang
                                if(data[0].idAppointment){
                                    $('#appointmentId').val(data[0].idAppointment);
                                    appointmentId = data[0].idAppointment;
                                }
                                if(data[0].tggl){
                                    $('#appointmentTggl').val(data[0].tgglAppointment);
                                    document.getElementById('tanggalAppointment').innerHTML=data[0].tgglAppointment
                                }
                                if(data[0].t770NoDokumen){
                                    $('#appointmentNoWO').val(data[0].t770NoDokumen);
                                    $(".t770NoDokumen").val(data[0].t770NoDokumen);
                                    document.getElementById('noAppointment').innerHTML = data[0].t770NoDokumen
                                }

                                if(data[0].jenisApp){
                                    var indCekApp = data[0].jenisApp-1;
                                    cekRadio = indCekApp;
                                    $('input:radio[name=jenisApp]:nth('+indCekApp+')').prop("checked",true);
                                }
                                if(data[0].staThs){
                                    staTHS = data[0].staThs;
                                    $('input:radio[name=staTHS]:nth('+data[0].staThs+')').prop("checked",true);
                                }
                                if(data[0].alamatThs){
                                    $('#alamatThs').val(data[0].alamatThs);
                                }

                                if(data[0].kmSekarang ){
                                    $('#i_km').val(data[0].kmSekarang)
                                }

                                <!-- keluhan -->
                                if(data[0].keluhan && data[0].keluhan.length>0 ){
                                var keluhan = data[0].keluhan;
                                countKeluhan = 1;
                                for(var i = 0;i< keluhan.length ; i++){
                                    var yaTidak = (keluhan[i][1]=='1' ? 'Ya' : 'Tidak');
                                    var KP = (keluhan[i][2]);
                                    var newRow = "<tr>" +
                                    "<td><input id='countKeluhan"+countKeluhan+"' type='hidden' value='"+countKeluhan+"'/><input id='keluhan"+countKeluhan+"' type='hidden' value='"+keluhan[i][0]+"'/><input id='staDiagnose"+countKeluhan+"' type='hidden' value='"+keluhan[i][1]+"'/><input id='staKP"+countKeluhan+"' type='hidden' value='"+keluhan[i][2]+"'/>" +
                                    "<input type='checkbox' class='idKeluhan' />"+countKeluhan+"</td>" +
                                    "<td>"+keluhan[i][0]+"</td>" +
                                    "<td>"+yaTidak+"</td>" +
                                    "<td>"+KP+"</td>" +
                                    "</tr>";
			                        $('#tableKeluhan > tbody:last').append(newRow);
			                        countKeluhan++;
                                }
                                    $('#i_km').val(data[0].kmSekarang)
                                }
                                <!-- keluhan -->

                                if(data[0].kategori ){
                                    $('#kategoriJob').val(data[0].kategori)
                                }
                                if(data[0].statusWarranty ){
                                    $('#staWarranty').val(data[0].statusWarranty)
                                }

                                idVincode=data[0].idCV;
                                nilaiSpk=data[0].spk;
                                reloadhistoryServiceTable();
                                reloadjobnPartsTable();
                            }else if(data[0].hasil=="nothing"){
                                kosong = true;
                                alert('Data dengan nomor polisi tersebut tidak ditemukan');
                                alert('Untuk Membuat Data Baru Silahkan Ke Menu \n  \tCustomer Profile --> Customer');

                            }else if(data[0].hasil=="noappointment"){
                                kosong = true;
                                alert('Belum ada data appointment untuk nomor polisi tersebut');
                            }
                            if(kosong){
                                idVincode='-1';
                                $('#lblfooternominal').text('');
                                $('#lblfooterrate').text('');
                                $('#noAppointment').text('${appointmentInstance?.reception?.t401NoAppointment}');
                                document.getElementById('tanggalAppointment').innerHTML=$('#appointmentTggl').val();
                                document.getElementById('noAppointment').innerHTML = $('#noAppointment').val()
                                document.getElementById('lblfa').innerHTML = ""
                                document.getElementById('survey_tpss').innerHTML = ""
                                document.getElementById('survey_follow_up').innerHTML = ""
                                document.getElementById('lblpks').innerHTML = ""
                                document.getElementById('survey_nama').innerHTML = ""
                                document.getElementById('survey_driver').innerHTML = ""
                                document.getElementById('survey_mobil').innerHTML = ""
                                document.getElementById('lbltglbookingfee').innerHTML = ""
                                document.getElementById('lbltgljanjidatang').innerHTML = ""
                                document.getElementById('lbltgljanjiservice').innerHTML = ""
                                document.getElementById('lbltgldelivery').innerHTML = ""
                                $('#lbljdpower').hide();
                                reloadhistoryServiceTable();
                                reloadjobnPartsTable();
                            }
                    },

                    complete : function (req, err) {
//                        $('#main-content').html(req.responseText);
                        $('#spinner').fadeOut();
                        loading = false;
//                    newAppointmentAtas();
                    }

                });
            }


            openCancelEditBookingFee = function() {
                $("#cancelEditBookingFeeContent").empty();
                    $.ajax({type:'POST',
                        url:'${request.contextPath}/appointment/cancelEditBookingFee',
                        success:function(data,textStatus){
                                $("#cancelEditBookingFeeContent").html(data);
                                $("#appointmentCancelEditBookingFeeModal").modal({
                                    "backdrop" : "static",
                                    "keyboard" : true,
                                    "show" : true
                                }).css({'width': '1200px','margin-left': function () {return -($(this).width() / 2);}});

                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){},
                        complete:function(XMLHttpRequest,textStatus){
                            $('#spinner').fadeOut();
                        }
                    });
            }

            openDataCustomer = function(){
                var kodeKota1 = $('#kodeKota').val();
                var noPolTengah2 = $('#nomorTengah').val();
                var noPolBelakang3 = $('#nomorBelakang').val();
                var btnCustomer = $('#datacuststomer');
                if(kodeKota1=="" || kodeKota1==null || noPolTengah2 =="" || noPolTengah2 ==null || noPolBelakang3=="" || noPolBelakang3==null
                    && btnCustomer.click()){
                        alert("Isi Dan Lengkapi Terlebih Dahulu Nomor Polisinya");
                }else{
                    $('#spinner').fadeIn(1);
                    var notNull = false;
                    var idKirim = []
                    $.ajax({
                        url:'${request.contextPath}/appointment/findNoPol',
                        type: "POST",
                        data: { kode: kodeKota1 , tengah : noPolTengah2 , belakang : noPolBelakang3 },
                        success : function(data){
                            if(data.length>2){
                                notNull = true;
                                idKirim.push(data[data.length-1].id)
                                idKirim.push(data[0].id)
                            }else{
                                alert('\t\t\tData customer untuk nomor polisi tersebut belum ada. \nSilahkan isi data customer di menu Customer Profile => Master Customer => Customer');
                            }
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            alert('Internal server error');
                        },
                        complete:function(XMLHttpRequest,textStatus){
                            openCustomer(notNull,idKirim);
                        }
                    });
                }
            }

            openCustomer = function(notNull,idKirim){
                if(notNull==true){
                    window.location.href='#/customer'
                    $('#spinner').fadeIn(1);
                    $.ajax({
                        url: '${request.contextPath}/customer',
                        data: { idCustomer: idKirim[0] , idVehicle : idKirim[1] },
                        type: "POST",dataType:"html",
                        complete : function (req, err) {
                            $('#main-content').html(req.responseText);
                            $('#spinner').fadeOut();
                        }
                    });
                }
            }


            var kodeKota = $('#kodeKota');
            var nomorTengah = $('#nomorTengah');
            var nomorBelakang = $('#nomorBelakang');

            kodeKota.keyup(function() {
                var val = $(this).val()
                $(this).val(val.toUpperCase())
            }).focus();
            kodeKota.bind('keypress', inputNoPolisi);
            nomorTengah.bind('keypress', inputNoPolisi);
            nomorBelakang.bind('keypress', inputNoPolisi).keyup(function() {
                var val = $(this).val()
                $(this).val(val.toUpperCase())
            });

            function inputNoPolisi(e){
//                dirty = true;
                var code = (e.keyCode ? e.keyCode : e.which);
                if(code == 13 && isValidated==false) {
                    if(!kodeKota.val())
                        kodeKota.focus();
                    else if(!nomorTengah.val())
                        nomorTengah.focus();
//                    else if(!nomorBelakang.val())
//                        nomorBelakang.focus();
                    else if(staNewEdit=="" ){
                        alert('Pilih New atau Edit');
                        return
                    }
                    else
                        newAppointment();
                }
}

        editAppointment = function(){
            if(!kodeKota.val())
                kodeKota.focus();
            else if(!nomorTengah.val())
                nomorTengah.focus();
            else if(!nomorBelakang.val())
                nomorBelakang.focus();
            else
              editAppointmentAtas();
        }
});

var staTHS = -1;
    function cekTHS(){
        if($('input:radio[name=staTHS]:nth(0)').is(':checked')){
            $('#kolomAlamat').show();
            staTHS=0;
        }else{
            $('#kolomAlamat').hide();
            staTHS=1;
        }
    }

var staTHS = -1;
var cekRadio =-1;
function cekRadioTHS(){
    if($('input:radio[name=jenisApp]:nth(0)').is(':checked')){
    cekRadio=0;
    $('#btnASB').attr("disabled",false)
    $('#btnJPB').attr("disabled",true)
    $('#print_service_appointment_bp').attr("disabled",true)
    $('#print_service_appointment_gr').attr("disabled",false)
    $('#barisTHS').show();
    }
    //                ==== kondisi ketika pilih BP AMBILISADOR ===
    else if( $('#btnASB').attr("disabled",true)&& $('#kolomAlamat').show() && $('#btnJPB').attr("disabled",false)&& $('input:radio[name=staTHS]:nth(0)').prop("checked",false)){
    cekRadio=1;
    $('#barisTHS').hide();
    $('#kolomAlamat').hide();
    $('#print_service_appointment_bp').attr("disabled",false)
    $('#print_service_appointment_gr').attr("disabled",true)
    }
    else {
    cekRadio=1;
    $('#btnASB').attr("disabled",true)
    $('#print_service_appointment_bp').attr("disabled",false)
    $('#btnJPB').attr("disabled",false)
    $('#barisTHS').hide();
    $('#kolomAlamat').hide();
    staTHS=1;
    }

    if($('input:radio[name=staTHS]:nth(0)').is(':checked')){
    $('#kolomAlamat').show();
    staTHS=0;
    }else{
    $('#kolomAlamat').hide();
    staTHS=1;
    }

}

var cekRadio = -1;
function cekJenis(){
    if($('input:radio[name=jenisApp]:nth(0)').is(':checked')){
        cekRadio=0;
        $('#btnASB').attr("disabled",false)
        $('#btnJPB').attr("disabled",true)
        $('#staTHS').show();

    }else {
        cekRadio=1;
        $('#btnASB').attr("disabled",true)
        $('#btnJPB').attr("disabled",false)
        $('#staTHS').hide();
    }
}

function newAppointmentAtas(){
    $("#lblAksiAppointment").text("New Appointment");
    staNewEdit = "getNewAppointment";
    $('#tanggalAppointment').show();
    $('#noAppointment').show();
    $('#new_appointment_atas').hide();
    $('#edit_appointment_atas').hide();
    $('#reset').show();
    initBeforeSave();
}




function initBeforeSave(){
    $('#input_job_dan_keluhan').show();
    $('#input_parts').show();
    $('#btnDelete2').show();
    $('#save_appointment').show();
}

function initAfterSave(){
    $('#request_parts').show();
    $('#cancel_edit_booking_fee').show();
    $('#btnASB').show();
    $('#btnJPB').show();
    $('#summary_appointment').show();
    $('#gatepass').show();
//    CARIAMAN
    $('#print_pre_diagnose').show();
//    CARIAMAN
    $('#print_service_appointment_gr').show();
    $('#print_service_appointment_bp').show();
    $('#reschedule_appointment').show();
    $('#cancel_appointment').show();
}

function editAppointmentAtas(){
    $("#lblAksiAppointment").text("Edit Appointment");
    staNewEdit = "getAppointment";
    $('#new_appointment_atas').hide();
    $('#edit_appointment_atas').hide();
    $('#reset').show();
}

    function showAppointment(kodeKota, nomorTengah, nomorBelakang) {
        $("#txtJobSuggest").val("");
        $('#spinner').fadeIn(1);
        $.ajax({url: '${request.contextPath}/appointment/showAppointment',
            type: "POST",
            data: {
                    kodeKota: kodeKota,
                    nomorTengah: nomorTengah,
                    nomorBelakang: nomorBelakang
                },
            success: function(data) {
                staValidate=1;
                $("idCustLamaWeb").val("");
                if(data.status == 'ok'){
                $("#noAppointment").css("display", "block");
                $("#txtJobSuggest").val(data.jobSuggest);
                    if(data.TPSS)
                        $("#survey_tpss").text(data.TPSS);

                    if(data.appointmentId){
                        appointmentId = data.appointmentId;
                    }
                    if(data.noAppointment){
                        $("#noAppointment").text(data.noAppointment);
                        noAppointment = data.noAppointment;
                        $(".t770NoDokumen").val(noAppointment);
                    }
                    if(data.tanggalAppointment){
                        $("#tanggalAppointment").text(data.tanggalAppointment);
                    }
                    if(data.namaPemilik){
                        $('#survey_nama').html(data.namaPemilik);
                        namaPemilik = data.namaPemilik;
                    }
                    if(data.driver){
                        $('#survey_driver').html(data.driver);
                    }
                    if(data.alamat){
                        alamatPemilik = data.alamat;
                    }
                    if(data.telp){
                        telpPemilik = data.telp;
                    }
                    if(data.hc){
                        hcId = data.hc;
                    }
                    if(data.mobil){
                        $('#survey_mobil').html(data.mobil);
                        mobilPemilik = data.mobil;
                    }
                    if (data.idCV) {
                        idVincode=data.idCV;
                    }
                reloadhistoryServiceTable();
                }else if(data.status=="notfound"){
                    $('#lbltotal').text('');
                    $('#lblrate').text('');
                    alert("Data kendaraan tidak ditemukan");
                    appointmentId = "${appointmentInstance?.id}"
                    $("#noAppointment").text("${appointmentInstance?.reception?.t401NoAppointment}");
                    noAppointment = "${appointmentInstance?.reception?.t401NoAppointment}";
                    $(".t770NoDokumen").val("${appointmentInstance?.reception?.t401NoAppointment}");
                    $("#tanggalAppointment").text("${appointmentInstance?.t301TglJamApp?appointmentInstance?.t301TglJamApp?.format("dd/MM/yyyy HH:mm"):new Date().format("dd/MM/yyyy HH:mm")}");
                    $('#survey_nama').text("");
                    namaPemilik = "";
                    $('#survey_driver').text("");
                    alamatPemilik = "";
                    telpPemilik = "";
                    hcId = "-1";
                    $('#survey_mobil').text("");
                    mobilPemilik = "";
                    idVincode="-1";
                }
                reloadjobnPartsTable();
            },
            complete : function (req, err) {
                $('#spinner').fadeOut();
                loading = false;
            }
        });
    }

    function toGetPass(){
         window.location.replace('#/gatePassT800');
    }

function cekASBJPB(data){
    var noDepan = $('#kodeKota').val();
    var noTengah = $('#nomorTengah').val();
    var noBelakang = $('#nomorBelakang').val();
    if(noDepan=="" || noDepan==null || noTengah=="" || noTengah==null || noBelakang=="" || noBelakang==null){
      alert('Isi dan Lengkapi Nomor Polisinya Terlebih Dahulu');
      return
    }
    if(cekRadio==-1){
        alert('Anda belum memilih pekerjaan');
        return
    }
    if(data=='asb' && cekRadio==0){
    var dataApp = appointmentId
        window.location.replace('#/InputASB');
        $.ajax({
                url:'${request.contextPath}/ASB/list?staApp=1&appointmentId='+dataApp,
                         type:"GET",dataType: "html",
                         complete : function (req, err) {
                            $('#main-content').html(req.responseText);
                            $('#spinner').fadeOut();
                            loading = false;
                         }
                    });
            }
            if(data=='jpb' && cekRadio==1){
                var dataApp = appointmentId
                window.location.replace('#/InputJpBBp');
                $.ajax({
                        url:'${request.contextPath}/JPBBPInput/list?staApp=1&appointmentId='+dataApp,
                         type:"GET",dataType: "html",
                         complete : function (req, err) {
                            $('#main-content').html(req.responseText);
                            $('#spinner').fadeOut();
                            loading = false;
                         }
                    });
            }
        }

//    ====== COBA ====
    function cekReschedule(data){
        var noDepan = $('#kodeKota').val();
        var noTengah = $('#nomorTengah').val();
        var noBelakang = $('#nomorBelakang').val();
        if(noDepan=="" || noDepan==null || noTengah=="" || noTengah==null || noBelakang=="" || noBelakang==null){
            alert('Isi dan Lengkapi Nomor Polisinya Terlebih Dahulu');
        }
        if(appointmentId==""){
            alert('Data Belum Lengkap');
            return
        }
        var dataApp = appointmentId;
        if(confirm("Apakah anda yakin akan melakukan reschedule?")){
            $.ajax({
                url: '${request.contextPath}/appointment/doReschedule',
                type: "POST",
                data: {idApp : dataApp},
                success: function(data) {
                    cekASBJPB('asb');
                }
            });
        }
    }
//    ====== COBA ====
</g:javascript>
</head>

<body>
<div class="navbar box-header no-border">
    <span class="pull-left" id="lblAksiAppointment">Appointment</span>
</div>

<div id="appointment" class="box">
<div class="row-fluid">
    <div class="span6 form-horizontal" id="no_pol_search">
        <g:field type="button" onclick="newAppointmentAtas();" class="btn" name="new_appointment_atas" id="new_appointment_atas" value="New" />
        <g:field type="button" onclick="editAppointmentAtas();" class="btn" name="edit_appointment_atas" id="edit_appointment_atas" value="Edit" />
        <g:field type="button" style="display:none" onclick="loadPath('appointment');" class="btn" name="reset" id="reset" value="Reset" />
        <div class="control-group">
            <label class="control-label" for="kodeKota" style="font-size: 20px; height: 50px; text-align: left;line-height: 50px;">Nomor Polisi
                <span class="required-indicator">*</span>
            </label>

            <div class="controls" style="margin-left: 100px;">
                <g:textField class="charonly"  maxlength="2" name="kodeKota" id="kodeKota" value="${vehicleWeb?.kodeKotaNoPol?.m116ID}"
                             style="width: 50px; font-size: 30px; height: 50px; border-top-right-radius: 0;border-bottom-right-radius: 0;"
                             tabindex="1"/><!-->
                <g:textField class="numberonly" maxlength="5" name="nomorTengah" id="nomorTengah" value="${vehicleWeb?.t183NoPolTengah}"
                             style="width: 95px; font-size: 30px; height: 50px; border-radius: 0;"
                             tabindex="2"/><!--
                --><g:textField class="charonly" maxlength="3" name="nomorBelakang" id="nomorBelakang" value="${vehicleWeb?.t183NoPolBelakang}"
                                style="width: 70px; font-size: 30px; height: 50px; border-top-left-radius: 0; border-bottom-left-radius: 0;"
                                tabindex="3"/>
                <g:hiddenField name="idCustLamaWeb" id="idCustLamaWeb" value="${custLamaWeb?.id}" />
                <a class="btn cancel" href="javascript:void(0);F" id="datacustomer" onclick="openDataCustomer();"
                   style="font-size: 20px; height: 50px;line-height: 50px;">Data Customer</a>
                <g:hiddenField name="appointmentId" id="appointmentId" value="${appointmentInstance?.id}" />
                <input type="hidden" id="appointmentNoWO" name="appointmentNoWO" />
                <input type="hidden" id="appointmentTggl" name="appointmentTggl" />
            </div>
        </div>
        <span id="lblneedbookingfee" style="color: red;font-size: 14px;display: none">Terdapat Part Wajib DP</span>
    </div>

    <div class="span4 offset2" id="tgl_appointment">
        <table class="table table-bordered table-dark table-hover">
            <tbody>
            <tr>
                <td><span
                        class="property-label">Tanggal Appointment</span></td>
                <td><span class="property-value" id="tanggalAppointment" name="tanggalAppointment">
                    %{--${appointmentInstance?.t301TglJamApp?appointmentInstance?.t301TglJamApp?.format("dd/MM/yyyy HH:mm"):new Date().format("dd/MM/yyyy HH:mm")}--}%
                    ${appointmentInstance?.t301TglJamApp?appointmentInstance?.t301TglJamApp?.format("dd/MM/yyyy HH:mm"):datatablesUtilService?.syncTime().format("dd MMMM YYYY HH:mm")}
                </span></td>
            </tr>
            <tr>
                <td><span
                        class="property-label">No. Appointment</span></td>
                <td><span class="property-value" id="noAppointment" name="noAppointment" style="display: none;">
                    ${appointmentInstance?.reception?.t401NoAppointment}
                </span></td>
            </tr>
            <tr>
                <td><span
                        class="property-label">Jenis Pekerjaan</span></td>
                <td>
                    <g:radioGroup name="jenisApp" id="jenisApp" values="['1','2']" labels="['GR','BP']" value="${appointmentInstance?.jenisApp?.id}" onclick="cekRadioTHS();" required="" >
                        ${it.radio} ${it.label}
                    </g:radioGroup>
                </td>
            </tr>
            <tr id="barisTHS">
                <td><span class="property-label">THS?</span></td>
                <td>
                    <g:radioGroup name="staTHS" id="staTHS" values="['0','1']" labels="['Ya','Tidak']" value="${appointmentInstance?.t301StaTHS}" onclick="cekRadioTHS();"  required="" >
                        ${it.radio} ${it.label}
                    </g:radioGroup>
                </td>
            </tr>
            <tr>
                <td colspan="2" id="kolomAlamat" style="display: none">
                    Alamat THS &nbsp;&nbsp;
                    <g:textArea name="alamatThs" id="alamatThs" style="resize:none;width: 90%" maxlength="255" />
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="row-fluid" style="margin-top: 30px;">
    <div class="span4">
        <table class="table table-bordered table-dark table-hover" style="table-layout: fixed;">
            <tbody>
            <tr>
                <td class="empty"></td>
                <td><span class="property-value">
                    JD POWER
                </span></td>
            </tr>
            <tr>
                <td><span
                        class="property-label">FA</span></td>
                <td><span id="lblfa" class="property-value">
                </span></td>
            </tr>
            <tr>
                <td><span
                        class="property-label">TPSS</span></td>
                <td><span id="survey_tpss" class="property-value">
                </span></td>
            </tr>
            <tr>
                <td><span
                        class="property-label">FOLLOW UP</span></td>
                <td><span id="survey_follow_up" class="property-value">
                </span></td>
            </tr>
            <tr>
                <td><span
                        class="property-label">PKS</span></td>
                <td><span class="property-value" id="lblpks">
                </span></td>
            </tr>
            </tbody>
        </table>
        <table class="table table-bordered table-dark table-hover" style="table-layout: fixed;">
            <tbody>
            <tr>
                <td><span
                        class="property-label">Nama Pemakai</span></td>
                <td><span id="survey_nama" class="property-value">
                    <%--                    Elizabeth--%>
                </span></td>
            </tr>
            <tr>
                <td><span
                        class="property-label">Nama Driver</span></td>
                <td><span id="survey_driver" class="property-value">
                    <%--                    Elizabeth--%>
                </span></td>
            </tr>
            <tr>
                <td><span
                        class="property-label">Mobil</span></td>
                <td><span id="survey_mobil" class="property-value">
                    <%--                    Corolla Altis--%>
                </span></td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="span8" id="appointment_table">
        <fieldset style="" class="buttons controls">
            <g:field type="button" style="display:none;" onclick="openInputJobDanKeluhan();" class="btn" name="input_job_dan_keluhan" id="input_job_dan_keluhan" value="Input Job & Keluhan" />
            <g:field type="button" style="display:none;" onclick="openInputPart();" class="btn" name="input_parts" id="input_parts" value="Input Parts" />
            <input type="button" style="display:none;" value="Delete" id="btnDelete2" class="btn" onclick="doDelete();">
        </fieldset>
        <g:render template="jobnPartsDataTables"/>
        <fieldset class="buttons controls" style="">
            <g:field type="button" style="display:none;" value="Request Parts" id="request_parts" name="request_parts" class="btn" onclick="openRequestPart();" />
            <g:field type="button" style="display:none;" value="Cancel/Edit Booking Fee" id="cancel_edit_booking_fee" name="cancel_edit_booking_fee" class="btn" onclick="openCancelEditBookingFee();" />
        </fieldset>
    </div>

    <div class="span8 offset4" id="appointment_buttons">
        <fieldset class="buttons controls" style="padding-top: 10px;">
            <g:field type="button" onclick="cekASBJPB('asb');" class="btn" name="btnASB" id="btnASB" style="display:none;"
                     value="Cek ASB GR"/>
            <g:field type="button" onclick="cekASBJPB('jpb');" class="btn" name="btnJPB" id="btnJPB" style="display:none;"
                     value="Cek JPB BP"/>
            <g:field type="button" onclick="showAppSummary();" class="btn" name="summary_appointment" style="display:none;"
                     id="summary_appointment" value="Summary Appointment"/>
            <g:field type="button" onclick="selectAlasanCancelAppointment();" class="btn" name="cancel_appointment" style="display:none;"
                     id="cancel_appointment" value="Cancel Appointment"/>
            <g:field type="button" onclick="cekReschedule('asb');" class="btn" name="reschedule_appointment" style="display:none;"
                     id="reschedule_appointment" value="Reschedule Appointment"/>
        </fieldset>
    </div>
</div>

<div class="row-fluid">
    <div class="span8" id="history_service">
        <span class="table-label">History Service</span>
        <g:render template="historyServiceDataTables"/>
    </div>

    <div class="span4" id="tgl_detail">
        <span>&nbsp;</span>
        <table class="table table-bordered table-dark table-hover" style="table-layout: fixed;">
            <tbody>
            <tr>
                <td style="width: 45%"><span
                        class="property-label">Tgl/Jam Bayar Booking Fee</span></td>
                <td style="width: 55%"><span class="property-value">
                    <label id="lbltglbookingfee"></label>
                </span></td>
            </tr>
            <tr>
                <td><span
                        class="property-label">Tgl/Jam Janji Datang</span></td>
                <td><span class="property-value">
                    <label id="lbltgljanjidatang"></label>
                </span></td>
            </tr>
            <tr>
                <td><span
                        class="property-label">Tgl/Jam Mulai Service</span></td>
                <td><span class="property-value">
                    <label id="lbltgljanjiservice"></label>
                </span></td>
            </tr>
            <tr>
                <td><span
                        class="property-label">Tgl/Jam Delivery</span></td>
                <td><span class="property-value">
                    <label id="lbltgldelivery"></label>
                </span></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="row-fluid" id="buttons">
    <fieldset class="buttons controls pull-right" style="padding-top: 10px;">
        <g:field type="button" style="display:none;" onclick="toGetPass();" class="btn" name="gatepass"
                 id="gatepass" value="GatePass"/>
        <g:field type="button" style="display:none;" onclick="openPreDiagnose();" class="btn" name="print_pre_diagnose"
                 id="print_pre_diagnose" value="Pre-Diagnose"/>
        <g:field type="button" style="display:none;" onclick="printAppointmentGR();" class="btn"
                 name="print_service_appointment_gr" id="print_service_appointment_gr" value="Print Service Appointment GR"/>
        <g:field type="button" style="display:none;" onclick="printAppointmentBP();" class="btn"
                 name="print_service_appointment_bp" id="print_service_appointment_bp" value="Print Service Appointment BP"/>
        <g:field type="button" style="display:none;" onclick="saveApp();" class="btn" name="save_appointment" id="save_appointment" value="Save"/>

    </fieldset>
</div>
</div>

<div id="appointmentAddJobModal" class="modal hide fade in-appointment" style="display: none;">
    <g:render template="appointmentJobKeluhan"/>
</div>

<div id="appointmentInputJobModal" class="modal hide fade in">
    <g:render template="appointmentInputJob"/>
</div>

<div id="appointmentPreDiagnoseModal" class="modal hide fade in">
    <g:render template="appointmentPreDiagnose"/>
</div>
<div id="appointmentSearchJobModal" class="modal hide fade in" style="display: none; ">
    <g:render template="appointmentSearchJob"/>
</div>
<div id="appointmentJobOrderModal" class="modal hide fade in" style="display: none; ">
    <g:render template="appointmentJobOrder"/>
</div>
<div id="appointmentInputPartModal" class="modal hide fade in">
    <g:render template="appointmentInputPart"/>
</div>
<div id="appointmentInputPart2Modal" class="modal hide fade in">
    <g:render template="appointmentInputParts2"/>
</div>
<div id="appointmentSearchPartModal" class="modal hide fade in">
    <g:render template="appointmentSearchParts"/>
</div>
<div id="appointmentSummaryModal" class="modal hide fade in">
    <g:render template="appointmentSummary"/>
</div>
<div id="appointmentRequestPartModal" class="modal hide fade in">
    <div class="modal-dialog" style="width: 1200px;">
        <div class="modal-content" style="width: 1200px;">
            <!-- dialog body -->
            <div class="modal-body" style="max-height: 500px;">
                <div id="requestPartContent"/>
                %{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}%
                <div class="iu-content"></div>

            </div>
            <!-- dialog buttons -->
            <div class="modal-footer"><button id='closeRequestPart' type="button" class="btn btn-primary" data-dismiss="modal">Close</button></div>
        </div>
    </div>
</div>
<div id="appointmentCancelEditBookingFeeModal" class="modal hide fade in">
    <div class="modal-dialog" style="width: 1200px;">
        <div class="modal-content" style="width: 1200px;">
            %{--<!-- dialog body --><button type="button" class="close" data-dismiss="modal">&times;</button>--}%
            <div class="modal-body" style="max-height: 500px;">

                <div id="cancelEditBookingFeeContent"/>

                <div class="iu-content"></div>

            </div>
            <!-- dialog buttons -->
            <div class="modal-footer"><button id='closeCancelEditBookingFee' type="button" class="btn btn-primary" data-dismiss="modal">Close</button></div>
        </div>
    </div>
</div>

<g:render template="formPermohonanApprovalCancelAppointment"/>
<g:render template="formPermohonanApprovalCancelBookingFee"/>
<g:render template="formPermohonanApprovalEditBookingFee"/>
<g:render template="formAlasanCancel"/>
</body>
</html>