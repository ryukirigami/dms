<%@ page import="com.kombos.parts.RequestStatus" %>
<%@ page import="com.kombos.parts.SOQ" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="title" value="Cancel/Edit Booking Fee"/>
    <title>${title}</title>
    <r:require modules="baseapplayout, autoNumeric"/>
    <g:javascript>
        var shrinkCancelEditBookingFeeTableLayout;
        var expandTableLayout;
        $(function () {
            shrinkCancelEditBookingFeeTableLayout = function () {
                if ($("#cebf-table").hasClass("span12")) {
                    $("#cebf-table").toggleClass("span12 span5");
                }
//                $("#cebf-form").css("display", "block");
            }

            expandCancelEditBookingFeeTableLayout = function () {
                if ($("#cebf-table").hasClass("span5")) {
                    $("#cebf-table").toggleClass("span5 span12");
                }
//                $("#cebf-form").css("display", "none");
            }

        });
    </g:javascript>
</head>

<body>
<div class="navbar box-header no-border">
    <span class="pull-left">${title}</span>
    <ul class="nav pull-right">

    </ul>
</div>

<div class="box">
    <div class="span12" id="cebf-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <g:render template="cancelEditBookingFeeDataTables"/>
        <fieldset class="buttons controls" style="padding-top: 30px;">
            <g:field type="button" onclick="requestEditBookingFee();" class="btn btn-primary"
                     name="approvalEditBookingFee" id="approvalEditBookingFee" value="Approval Edit Booking Fee"/>
            <g:field type="button" onclick="requestCancelBookingFee();" class="btn btn-primary"
                     name="approvalCancelBookingFee" id="approvalCancelBookingFee" value="Approval Cancel Booking Fee"/>
        </fieldset>

    </div>

    %{--<div class="span7" id="cebf-form" style="display: none;"></div>--}%
</div>

</body>
</html>
