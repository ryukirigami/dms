
<g:javascript>
    var addPart;

    $(function(){
        addPart = function() {
            var checkJob =[];
            var id = 1;
            $("#addPart_datatables tbody .row-select").each(function() {
                if(this.checked){
                    var nRow = $(this).parents('tr')[0];
                    var aData = addPartTable.fnGetData(nRow);
                    var part = {};
                    part['id'] = aData['id'];
                    part['goods'] = aData['goods'];
                    part['goods2'] = aData['goods2'];
                    part['totalHarga'] = aData['totalHarga'];
                    part['satuan1'] = aData['satuan'];
                    part['proses'] = null;
                    part['qty'] = 1;
                    inputPartTable.fnAddData(part);
                }
            });
        }
    });
</g:javascript>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-body" style="max-height: 415px;">
            <div id="appointmentAddPartContent">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="iu-content row-fluid">
                    <div class="span5" style="padding-top: 20px;">
                        <g:render template="datatablesPartAdd"/>
                    </div>
                    <div class="span1" style="padding-top: 20px;">
                        <button id='addPart' onclick="addPart();" type="button" class="btn btn-primary">Add >> </button>
                    </div>
                    <div class="span6">
                        <g:render template="datatablesPartInput"/>
                    </div>
                    %{--<g:render template="addJobDataTables" />--}%
                </div>
            </div>

        </div>
        <!-- dialog buttons -->
        <div class="modal-footer">
            <button id='saveAppointmentAddPart' onclick="savePartApp();" type="button" class="btn btn-primary">Save</button>
            <button id='closeAppointmentAddPart' type="button" class="btn" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>