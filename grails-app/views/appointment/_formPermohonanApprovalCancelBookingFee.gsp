<%@ page import="com.kombos.administrasi.KegiatanApproval" %>
<g:javascript>
var approvalOrderCancelBookingFee;
var sendApprovalCBF;
var requestCancelBookingFee;
$(function(){
sendApprovalCBF = function(){
                    $.ajax({type:'POST',
                        data:$("#requestApprovalCancelBookingFee").serialize(),
                        url:'/dms/appointment/requestApprovalCancelBookingFee',
                        success:function(data,textStatus){
                            approvalOrderCancelBookingFee(data);
                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){

                        }
                     });
                    return false;
            }
requestCancelBookingFee = function(id) {

		var formPermohonanApproval = $('#formPermohonanApprovalCancelBookingFeeModal').find('form');
		var checkParts = [];
		var nonEditableParts = [];
        $("#cebf-table tbody .row-select").each(function() {
        if(this.checked){
            var id = $(this).next("input:hidden").val();

			var nRow = $(this).parents('tr')[0];
            var aData = cebfdTable.fnGetData(nRow);
			var qtyInput = $('#qty' + aData['id'], nRow);
			var dpInput = $('#dp' + aData['id'], nRow);
			var keteranganInput = $('#keterangan' + aData['id'], nRow);

				checkParts.push(id);
//				formPermohonanApproval.append('<input type="hidden" name="qty-'+aData['id'] + '" value="'+qtyInput[0].value+'" class="deleteafter">');
//				formPermohonanApproval.append('<input type="hidden" name="dp-'+aData['id'] + '" value="'+dpInput[0].value+'" class="deleteafter">');
//				formPermohonanApproval.append('<input type="hidden" name="keterangan-'+aData['id'] + '" value="'+keteranganInput[0].value+'" class="deleteafter">');

        }
        });
		if(checkParts.length < 1){
             alert('Anda belum memilih data yang akan ditambahkan');
        } else {
            $(".modal").modal('hide');

			$("#cbfrequestIds").val(JSON.stringify(checkParts));

			%{--$.ajax({type:'POST',--}%
				%{--url:'${request.contextPath}/appointment/generateNomorDokumenApprovalCancelBookingFee',--}%
				%{--success:function(data,textStatus){--}%
					%{--$("#t770NoDokumen").val(data.value);--}%
				%{--},--}%
				%{--error:function(XMLHttpRequest,textStatus,errorThrown){},--}%
				%{--complete:function(XMLHttpRequest,textStatus){--}%
					%{--$('#spinner').fadeOut();--}%
				%{--}--}%
			%{--});--}%

			$("#formPermohonanApprovalCancelBookingFeeModal").modal({
							"backdrop" : "static",
							"keyboard" : true,
							"show" : true
						});
		}
    };


	$("#formPermohonanApprovalCancelBookingFeeModal").on("show", function() {
//			$("#formPermohonanApprovalCancelBookingFeeModal").on("click", function(e) {
//				$("#formPermohonanApprovalCancelBookingFeeModal").modal('hide');
//			});
		});
	$("#formPermohonanApprovalCancelBookingFeeModal").on("hide", function() {
			            $("#formPermohonanApprovalCancelBookingFeeModal a.btn").off("click");
		});
	
	approvalOrderCancelBookingFee = function(data){
		$('#formPermohonanApprovalCancelBookingFeeModal').modal('hide')
		toastr.success('<div>Cancel Booking Fee Approval has been sent.</div>');
		//vopTable.fnDraw();
	}
});
</g:javascript>
<div id="formPermohonanApprovalCancelBookingFeeModal" class="modal hide">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" id="requestApprovalCancelBookingFee">
                %{--<g:formRemote class="form-horizontal" name="requestApprovalCancelBookingFee" on404="alert('not found!');" onSuccess="approvalOrderCancelBookingFee(data);"--}%
                %{--url="[controller: 'appointment', action:'requestApprovalCancelBookingFee']">--}%
                <input type="hidden" name="requestIds" id="cbfrequestIds" value="">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 500px;">
                    <div id="formPermohonanApprovalCancelBookingFeeContent">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                        <div class="iu-content">
                            <div class="control-group fieldcontain">
                                <label class="control-label" for="kegiatanApproval">Nama Kegiatan</label>

                                <div class="controls">
                                    <g:select name="kegiatanApproval" id="kegiatanApproval"
                                              from="${KegiatanApproval.list()}" optionKey="id"
                                              value="${kegiatanApprovalCancelBookingFee.id}"
                                              optionValue="m770KegiatanApproval" class="one-to-many"
                                              readonly="readonly"/>
                                </div>
                            </div>

                            <div class="control-group fieldcontain">
                                <label class="control-label" for="t770NoDokumen">
                                    Nomor Request
                                </label>

                                <div class="controls">
                                    <g:field type="text" name="t770NoDokumen" class="t770NoDokumen" value="${appointmentInstance?.reception?.t401NoAppointment}" readonly="readonly"/>
                                </div>
                            </div>

                            <div class="control-group fieldcontain">
                                <label class="control-label" for="t770TglJamSend">
                                    Tanggal dan Jam
                                </label>

                                <div class="controls">
                                    <g:field type="text" name="t770TglJamSend" value="${new java.util.Date().format("dd/MM/yyyy HH:mm")}" readonly="readonly"/>
                                </div>
                            </div>

                            <div class="control-group fieldcontain">
                                <label class="control-label" for="approver">
                                    Approver(s)
                                </label>

                                <div class="controls">
                                    <g:textArea rows="3" cols="50" maxlength="20" name="approver" value="${approver}"
                                                readonly="readonly"/>
                                </div>
                            </div>

                            <div class="control-group fieldcontain">
                                <label class="control-label" for="pesan">
                                    Pesan
                                </label>

                                <div class="controls">
                                    <g:textArea rows="4" cols="50" maxlength="20" name="pesan" value=""/>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- dialog buttons -->
                <div class="modal-footer">
                    %{--<g:submitButton class="btn btn-primary create" name="send" value="${message(code: 'default.button.send.label', default: 'Send')}" />--}%
                    <g:field type="button" onclick="sendApprovalCBF();" class="btn"
                             name="send" id="sendApprovalCancelBookingFee"
                             value="Send"/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
            %{--</g:formRemote>--}%
        </div>
    </div>
</div>




