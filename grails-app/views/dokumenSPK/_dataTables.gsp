
<%@ page import="org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap; com.kombos.customerprofile.DokumenSPK" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="dokumenSPK_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="dokumenSPK.kelengkapanDokumenAsuransi.label" default="Dokumen Pendukung" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="dokumenSPK.t195Keterangan.label" default="Keterangan" /></div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var dokumenSPKTable;
var reloadDokumenSPKTable;
$(function(){
	
	reloadDokumenSPKTable = function() {
		dokumenSPKTable.fnDraw();
	}

	
	$('#search_t195TglJamUpload').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t195TglJamUpload_day').val(newDate.getDate());
			$('#search_t195TglJamUpload_month').val(newDate.getMonth()+1);
			$('#search_t195TglJamUpload_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			dokumenSPKTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	dokumenSPKTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	dokumenSPKTable = $('#dokumenSPK_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${request.contextPath}/dokumenSPK/datatablesList?spkAsuransiId=${SPKAsuransiInstance.id}",
		"aoColumns": [

{
	"sName": "kelengkapanDokumenAsuransi",
	"mDataProp": "kelengkapanDokumenAsuransi",
	"aTargets": [1],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "t195Keterangan",
	"mDataProp": "t195Keterangan",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
