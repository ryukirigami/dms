<%@ page import="com.kombos.customerprofile.DokumenSPK" %>

<script language="Javascript">
    function fileUpload(form, action_url, div_id) {
        // Create the iframe...
        var iframe = document.createElement("iframe");
        iframe.setAttribute("id", "upload_iframe");
        iframe.setAttribute("name", "upload_iframe");
        iframe.setAttribute("width", "0");
        iframe.setAttribute("height", "0");
        iframe.setAttribute("border", "0");
        iframe.setAttribute("style", "width: 0; height: 0; border: none;");

        // Add to document...
        form.parentNode.appendChild(iframe);
        window.frames['upload_iframe'].name = "upload_iframe";

        iframeId = document.getElementById("upload_iframe");

        // Add event...
        var eventHandler = function () {

            if (iframeId.detachEvent) iframeId.detachEvent("onload", eventHandler);
            else iframeId.removeEventListener("load", eventHandler, false);

            // Message from server...
            if (iframeId.contentDocument) {
                content = iframeId.contentDocument.body.innerHTML;
            } else if (iframeId.contentWindow) {
                content = iframeId.contentWindow.document.body.innerHTML;
            } else if (iframeId.document) {
                content = iframeId.document.body.innerHTML;
            }

            document.getElementById(div_id).innerHTML = content;

            // Del the iframe...
            setTimeout('iframeId.parentNode.removeChild(iframeId)', 250);
        }

        if (iframeId.addEventListener) iframeId.addEventListener("load", eventHandler, true);
        if (iframeId.attachEvent) iframeId.attachEvent("onload", eventHandler);

        // Set properties of form...
        form.setAttribute("target", "upload_iframe");
        form.setAttribute("action", action_url);
        form.setAttribute("method", "post");
        form.setAttribute("enctype", "multipart/form-data");
        form.setAttribute("encoding", "multipart/form-data");

        // Submit the form...
        form.submit();

        document.getElementById(div_id).innerHTML = "Uploading...";
    }
</script>

<div id="show-SPKAsuransi" role="main">
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table id="SPKAsuransi"
           class="table table-bordered table-hover">
    <tbody>


    <g:if test="${SPKAsuransiInstance?.vendorAsuransi}">
        <tr>
            <td class="span2" style="text-align: right;"><span
                    id="vendorAsuransi-label" class="property-label"><g:message
                        code="SPKAsuransi.vendorAsuransi.label" default="Nama Asuransi" />:</span></td>

            <td class="span3"><span class="property-value"
                                    aria-labelledby="vendorAsuransi-label">
                %{--<ba:editableValue
                        bean="${SPKAsuransiInstance}" field="vendorAsuransi"
                        url="${request.contextPath}/SPKAsuransi/updatefield" type="text"
                        title="Enter vendorAsuransi" onsuccess="reloadSPKAsuransiTable();" />--}%
                <g:fieldValue field="m193Nama" bean="${SPKAsuransiInstance?.vendorAsuransi}" />

            </span></td>

        </tr>
    </g:if>

    <g:if test="${SPKAsuransiInstance?.customerVehicle}">
        <tr>
            <td class="span2" style="text-align: right;"><span
                    id="customerVehicle-label" class="property-label"><g:message
                        code="SPKAsuransi.customerVehicle.label" default="Vin Code" />:</span></td>

            <td class="span3"><span class="property-value"
                                    aria-labelledby="customerVehicle-label">
                <g:fieldValue field="t103VinCode" bean="${SPKAsuransiInstance?.customerVehicle}" />

            </span></td>

        </tr>
    </g:if>

    </tbody>
    </table>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: dokumenSPKInstance, field: 'kelengkapanDokumenAsuransi', 'error')} required">
    <label class="control-label" for="kelengkapanDokumenAsuransi">
        <g:message code="dokumenSPK.kelengkapanDokumenAsuransi.label" default="Jenis Dokumen" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="kelengkapanDokumenAsuransi" name="kelengkapanDokumenAsuransi.id" from="${com.kombos.customerprofile.KelengkapanDokumenAsuransi.list()}" optionKey="id" required="" value="${dokumenSPKInstance?.kelengkapanDokumenAsuransi?.id}" class="many-to-one"/>
        <input type="hidden" value="${SPKAsuransiInstance?.id}" name="spkAsuransiId"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: dokumenSPKInstance, field: 't195Keterangan', 'error')} required">
    <label class="control-label" for="t195Keterangan">
        <g:message code="dokumenSPK.t195Keterangan.label" default="Keterangan" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:textField name="t195Keterangan" maxlength="50" required="" value="${dokumenSPKInstance?.t195Keterangan}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: dokumenSPKInstance, field: 't195Foto', 'error')} required">
    <label class="control-label" for="t195Foto">
        <g:message code="dokumenSPK.t195Foto.label" default="Upload" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        %{--<g:textField name="t195Foto" required="" value="${dokumenSPKInstance?.t195Foto}"/>--}%
        <input type="file" name="myFile"/>
        <input type="button" value="Upload" onClick="fileUpload(this.form, '${g.createLink(controller: 'dokumenSPK', action: 'uploadImage')}', 'upload'); return false;">
        <div id="upload"></div>
    </div>
</div>


