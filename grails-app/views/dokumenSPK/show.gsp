

<%@ page import="com.kombos.customerprofile.DokumenSPK" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'dokumenSPK.label', default: 'DokumenSPK')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteDokumenSPK;

$(function(){ 
	deleteDokumenSPK=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/dokumenSPK/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadDokumenSPKTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-dokumenSPK" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="dokumenSPK"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${dokumenSPKInstance?.spkAsuransi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="spkAsuransi-label" class="property-label"><g:message
					code="dokumenSPK.spkAsuransi.label" default="Spk Asuransi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="spkAsuransi-label">
						%{--<ba:editableValue
								bean="${dokumenSPKInstance}" field="spkAsuransi"
								url="${request.contextPath}/DokumenSPK/updatefield" type="text"
								title="Enter spkAsuransi" onsuccess="reloadDokumenSPKTable();" />--}%
							
								<g:link controller="SPKAsuransi" action="show" id="${dokumenSPKInstance?.spkAsuransi?.id}">${dokumenSPKInstance?.spkAsuransi?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${dokumenSPKInstance?.kelengkapanDokumenAsuransi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="kelengkapanDokumenAsuransi-label" class="property-label"><g:message
					code="dokumenSPK.kelengkapanDokumenAsuransi.label" default="Kelengkapan Dokumen Asuransi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="kelengkapanDokumenAsuransi-label">
						%{--<ba:editableValue
								bean="${dokumenSPKInstance}" field="kelengkapanDokumenAsuransi"
								url="${request.contextPath}/DokumenSPK/updatefield" type="text"
								title="Enter kelengkapanDokumenAsuransi" onsuccess="reloadDokumenSPKTable();" />--}%
							
								<g:link controller="kelengkapanDokumenAsuransi" action="show" id="${dokumenSPKInstance?.kelengkapanDokumenAsuransi?.id}">${dokumenSPKInstance?.kelengkapanDokumenAsuransi?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${dokumenSPKInstance?.t195ID}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t195ID-label" class="property-label"><g:message
					code="dokumenSPK.t195ID.label" default="T195 ID" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t195ID-label">
						%{--<ba:editableValue
								bean="${dokumenSPKInstance}" field="t195ID"
								url="${request.contextPath}/DokumenSPK/updatefield" type="text"
								title="Enter t195ID" onsuccess="reloadDokumenSPKTable();" />--}%
							
								<g:fieldValue bean="${dokumenSPKInstance}" field="t195ID"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${dokumenSPKInstance?.t195Keterangan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t195Keterangan-label" class="property-label"><g:message
					code="dokumenSPK.t195Keterangan.label" default="T195 Keterangan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t195Keterangan-label">
						%{--<ba:editableValue
								bean="${dokumenSPKInstance}" field="t195Keterangan"
								url="${request.contextPath}/DokumenSPK/updatefield" type="text"
								title="Enter t195Keterangan" onsuccess="reloadDokumenSPKTable();" />--}%
							
								<g:fieldValue bean="${dokumenSPKInstance}" field="t195Keterangan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${dokumenSPKInstance?.t195Foto}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t195Foto-label" class="property-label"><g:message
					code="dokumenSPK.t195Foto.label" default="T195 Foto" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t195Foto-label">
						%{--<ba:editableValue
								bean="${dokumenSPKInstance}" field="t195Foto"
								url="${request.contextPath}/DokumenSPK/updatefield" type="text"
								title="Enter t195Foto" onsuccess="reloadDokumenSPKTable();" />--}%
							
								<g:fieldValue bean="${dokumenSPKInstance}" field="t195Foto"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${dokumenSPKInstance?.t195TglJamUpload}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t195TglJamUpload-label" class="property-label"><g:message
					code="dokumenSPK.t195TglJamUpload.label" default="T195 Tgl Jam Upload" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t195TglJamUpload-label">
						%{--<ba:editableValue
								bean="${dokumenSPKInstance}" field="t195TglJamUpload"
								url="${request.contextPath}/DokumenSPK/updatefield" type="text"
								title="Enter t195TglJamUpload" onsuccess="reloadDokumenSPKTable();" />--}%
							
								<g:formatDate date="${dokumenSPKInstance?.t195TglJamUpload}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${dokumenSPKInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="dokumenSPK.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${dokumenSPKInstance}" field="staDel"
								url="${request.contextPath}/DokumenSPK/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadDokumenSPKTable();" />--}%
							
								<g:fieldValue bean="${dokumenSPKInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${dokumenSPKInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="dokumenSPK.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${dokumenSPKInstance}" field="createdBy"
								url="${request.contextPath}/DokumenSPK/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadDokumenSPKTable();" />--}%
							
								<g:fieldValue bean="${dokumenSPKInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${dokumenSPKInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="dokumenSPK.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${dokumenSPKInstance}" field="updatedBy"
								url="${request.contextPath}/DokumenSPK/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadDokumenSPKTable();" />--}%
							
								<g:fieldValue bean="${dokumenSPKInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${dokumenSPKInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="dokumenSPK.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${dokumenSPKInstance}" field="lastUpdProcess"
								url="${request.contextPath}/DokumenSPK/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadDokumenSPKTable();" />--}%
							
								<g:fieldValue bean="${dokumenSPKInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${dokumenSPKInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="dokumenSPK.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${dokumenSPKInstance}" field="dateCreated"
								url="${request.contextPath}/DokumenSPK/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadDokumenSPKTable();" />--}%
							
								<g:formatDate date="${dokumenSPKInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${dokumenSPKInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="dokumenSPK.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${dokumenSPKInstance}" field="lastUpdated"
								url="${request.contextPath}/DokumenSPK/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadDokumenSPKTable();" />--}%
							
								<g:formatDate date="${dokumenSPKInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${dokumenSPKInstance?.id}"
					update="[success:'dokumenSPK-form',failure:'dokumenSPK-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteDokumenSPK('${dokumenSPKInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
