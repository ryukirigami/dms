<%@ page import="com.kombos.customerprofile.DokumenSPK" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
	</head>
	<body>
		<div id="edit-dokumenSPK" class="content scaffold-edit" role="main">
			<legend>Upload Dokumen Pendukung</legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${dokumenSPKInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${dokumenSPKInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>

            <g:formRemote class="form-horizontal" name="create" on404="alert('not found!');" onSuccess="reloadDokumenSPKTable();" update="dokumenSPK-form"
                          url="[controller: 'dokumenSPK', action:'save']">
                <fieldset class="form">
                    <g:render template="form"/>
                </fieldset>
                <fieldset class="buttons controls">
                    <g:actionSubmit class="btn btn-primary save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                    <a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout();"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
                </fieldset>
            </g:formRemote>

            <g:render template="dataTables" />

		</div>
	</body>
</html>
