
<%@ page import="com.kombos.customerprofile.SPKAsuransi" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="SPKAsuransi_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
<thead>
<tr>

    <th style="border-bottom: none;padding: 5px;">
        <div><g:message code="SPKAsuransi.vendorAsuransi.label" default="Nama Asuransi" /></div>
    </th>


    <th style="border-bottom: none;padding: 5px;">
        <div><g:message code="SPKAsuransi.t193NomorPolis.label" default="Nomor Polis" /></div>
    </th>


    <th style="border-bottom: none;padding: 5px;">
        <div><g:message code="SPKAsuransi.t193NamaPolis.label" default="Nama Polis" /></div>
    </th>


    <th style="border-bottom: none;padding: 5px;">
        <div><g:message code="SPKAsuransi.t193NomorSPK.label" default="Nomor SPK" /></div>
    </th>


    <th style="border-bottom: none;padding: 5px;">
        <div><g:message code="SPKAsuransi.t193StaUtama.label" default="Status SPK" /></div>
    </th>


    <th style="border-bottom: none;padding: 5px;">
        <div><g:message code="SPKAsuransi.customerVehicle.label" default="VIN Code" /></div>
    </th>

</tr>
<tr>


<th style="border-top: none;padding: 5px;">
    <div id="filter_vendorAsuransi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_vendorAsuransi" class="search_init" />
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_t193NomorPolis" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_t193NomorPolis" class="search_init" />
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_t193NamaPolis" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_t193NamaPolis" class="search_init" />
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_t193NomorSPK" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_t193NomorSPK" class="search_init" />
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_t193StaUtama" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <select id="t193StaUtama" onchange="reloadSPKAsuransiTable();" style="width: 100%">
            <option value=""> </option>
            <option value="1">SPK Utama</option>
            <option value="2">SPK Tambahan </option>
            <option value="3">SPK Pengganti</option>
        </select>
    </div>
</th>

<th style="border-top: none;padding: 5px;">
    <div id="filter_customerVehicle" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
        <input type="text" name="search_customerVehicle" class="search_init" />
    </div>
</th>

</tr>
</thead>
</table>

<g:javascript>
var SPKAsuransiTable;
var reloadSPKAsuransiTable;
$(function(){

	reloadSPKAsuransiTable = function() {
		SPKAsuransiTable.fnDraw();
	}


    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	SPKAsuransiTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	SPKAsuransiTable = $('#SPKAsuransi_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(controller: "SPKAsuransi", action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "vendorAsuransi",
	"mDataProp": "vendorAsuransi",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;Add Document&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t193NomorPolis",
	"mDataProp": "t193NomorPolis",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t193NamaPolis",
	"mDataProp": "t193NamaPolis",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "t193NomorSPK",
	"mDataProp": "t193NomorSPK",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t193StaUtama",
	"mDataProp": "t193StaUtama",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,
{
	"sName": "customerVehicle",
	"mDataProp": "customerVehicle",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

						var vendorAsuransi = $('#filter_vendorAsuransi input').val();
						if(vendorAsuransi){
							aoData.push(
									{"name": 'sCriteria_vendorAsuransi', "value": vendorAsuransi}
							);
						}

						var customerVehicle = $('#filter_customerVehicle input').val();
						if(customerVehicle){
							aoData.push(
									{"name": 'sCriteria_customerVehicle', "value": customerVehicle}
							);
						}

						var t193StaUtama = $('#filter_t193StaUtama select').val();
						if(t193StaUtama){
							aoData.push(
									{"name": 'sCriteria_t193StaUtama', "value": t193StaUtama}
							);
						}

						var t193NomorSPK = $('#filter_t193NomorSPK input').val();
						if(t193NomorSPK){
							aoData.push(
									{"name": 'sCriteria_t193NomorSPK', "value": t193NomorSPK}
							);
						}

						var t193NomorPolis = $('#filter_t193NomorPolis input').val();
						if(t193NomorPolis){
							aoData.push(
									{"name": 'sCriteria_t193NomorPolis', "value": t193NomorPolis}
							);
						}

						var t193NamaPolis = $('#filter_t193NamaPolis input').val();
						if(t193NamaPolis){
							aoData.push(
									{"name": 'sCriteria_t193NamaPolis', "value": t193NamaPolis}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>



