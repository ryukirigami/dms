
<%@ page import="com.kombos.maintable.NoFakturPajak" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="noFakturPajak_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="noFakturPajak.noawal.label" default="Noawal" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="noFakturPajak.noakhir.label" default="Noakhir" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="noFakturPajak.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="noFakturPajak.statushabis.label" default="Statushabis" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="noFakturPajak.tglakhirpajak.label" default="Tglakhirpajak" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="noFakturPajak.tglawalpajak.label" default="Tglawalpajak" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_noawal" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_noawal" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_noakhir" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_noakhir" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_statushabis" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_statushabis" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_tglakhirpajak" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_tglakhirpajak" value="date.struct">
					<input type="hidden" name="search_tglakhirpajak_day" id="search_tglakhirpajak_day" value="">
					<input type="hidden" name="search_tglakhirpajak_month" id="search_tglakhirpajak_month" value="">
					<input type="hidden" name="search_tglakhirpajak_year" id="search_tglakhirpajak_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_tglakhirpajak_dp" value="" id="search_tglakhirpajak" class="search_init">
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_tglawalpajak" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_tglawalpajak" value="date.struct">
					<input type="hidden" name="search_tglawalpajak_day" id="search_tglawalpajak_day" value="">
					<input type="hidden" name="search_tglawalpajak_month" id="search_tglawalpajak_month" value="">
					<input type="hidden" name="search_tglawalpajak_year" id="search_tglawalpajak_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_tglawalpajak_dp" value="" id="search_tglawalpajak" class="search_init">
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var noFakturPajakTable;
var reloadNoFakturPajakTable;
$(function(){
	
	reloadNoFakturPajakTable = function() {
		noFakturPajakTable.fnDraw();
	}

	
	$('#search_tglakhirpajak').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tglakhirpajak_day').val(newDate.getDate());
			$('#search_tglakhirpajak_month').val(newDate.getMonth()+1);
			$('#search_tglakhirpajak_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			noFakturPajakTable.fnDraw();	
	});

	

	$('#search_tglawalpajak').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tglawalpajak_day').val(newDate.getDate());
			$('#search_tglawalpajak_month').val(newDate.getMonth()+1);
			$('#search_tglawalpajak_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			noFakturPajakTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	noFakturPajakTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	noFakturPajakTable = $('#noFakturPajak_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "noawal",
	"mDataProp": "noawal",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "noakhir",
	"mDataProp": "noakhir",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "statushabis",
	"mDataProp": "statushabis",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "tglakhirpajak",
	"mDataProp": "tglakhirpajak",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "tglawalpajak",
	"mDataProp": "tglawalpajak",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var noawal = $('#filter_noawal input').val();
						if(noawal){
							aoData.push(
									{"name": 'sCriteria_noawal', "value": noawal}
							);
						}
	
						var noakhir = $('#filter_noakhir input').val();
						if(noakhir){
							aoData.push(
									{"name": 'sCriteria_noakhir', "value": noakhir}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}
	
						var statushabis = $('#filter_statushabis input').val();
						if(statushabis){
							aoData.push(
									{"name": 'sCriteria_statushabis', "value": statushabis}
							);
						}

						var tglakhirpajak = $('#search_tglakhirpajak').val();
						var tglakhirpajakDay = $('#search_tglakhirpajak_day').val();
						var tglakhirpajakMonth = $('#search_tglakhirpajak_month').val();
						var tglakhirpajakYear = $('#search_tglakhirpajak_year').val();
						
						if(tglakhirpajak){
							aoData.push(
									{"name": 'sCriteria_tglakhirpajak', "value": "date.struct"},
									{"name": 'sCriteria_tglakhirpajak_dp', "value": tglakhirpajak},
									{"name": 'sCriteria_tglakhirpajak_day', "value": tglakhirpajakDay},
									{"name": 'sCriteria_tglakhirpajak_month', "value": tglakhirpajakMonth},
									{"name": 'sCriteria_tglakhirpajak_year', "value": tglakhirpajakYear}
							);
						}

						var tglawalpajak = $('#search_tglawalpajak').val();
						var tglawalpajakDay = $('#search_tglawalpajak_day').val();
						var tglawalpajakMonth = $('#search_tglawalpajak_month').val();
						var tglawalpajakYear = $('#search_tglawalpajak_year').val();
						
						if(tglawalpajak){
							aoData.push(
									{"name": 'sCriteria_tglawalpajak', "value": "date.struct"},
									{"name": 'sCriteria_tglawalpajak_dp', "value": tglawalpajak},
									{"name": 'sCriteria_tglawalpajak_day', "value": tglawalpajakDay},
									{"name": 'sCriteria_tglawalpajak_month', "value": tglawalpajakMonth},
									{"name": 'sCriteria_tglawalpajak_year', "value": tglawalpajakYear}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
