

<%@ page import="com.kombos.maintable.NoFakturPajak" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'noFakturPajak.label', default: 'NoFakturPajak')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteNoFakturPajak;

$(function(){ 
	deleteNoFakturPajak=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/noFakturPajak/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadNoFakturPajakTable();
   				expandTableLayout('noFakturPajak');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-noFakturPajak" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="noFakturPajak"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${noFakturPajakInstance?.noawal}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="noawal-label" class="property-label"><g:message
					code="noFakturPajak.noawal.label" default="Noawal" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="noawal-label">
						%{--<ba:editableValue
								bean="${noFakturPajakInstance}" field="noawal"
								url="${request.contextPath}/NoFakturPajak/updatefield" type="text"
								title="Enter noawal" onsuccess="reloadNoFakturPajakTable();" />--}%
							
								<g:fieldValue bean="${noFakturPajakInstance}" field="noawal"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${noFakturPajakInstance?.noakhir}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="noakhir-label" class="property-label"><g:message
					code="noFakturPajak.noakhir.label" default="Noakhir" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="noakhir-label">
						%{--<ba:editableValue
								bean="${noFakturPajakInstance}" field="noakhir"
								url="${request.contextPath}/NoFakturPajak/updatefield" type="text"
								title="Enter noakhir" onsuccess="reloadNoFakturPajakTable();" />--}%
							
								<g:fieldValue bean="${noFakturPajakInstance}" field="noakhir"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${noFakturPajakInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="noFakturPajak.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${noFakturPajakInstance}" field="staDel"
								url="${request.contextPath}/NoFakturPajak/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadNoFakturPajakTable();" />--}%
							
								<g:fieldValue bean="${noFakturPajakInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${noFakturPajakInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="noFakturPajak.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${noFakturPajakInstance}" field="createdBy"
								url="${request.contextPath}/NoFakturPajak/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadNoFakturPajakTable();" />--}%
							
								<g:fieldValue bean="${noFakturPajakInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${noFakturPajakInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="noFakturPajak.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${noFakturPajakInstance}" field="updatedBy"
								url="${request.contextPath}/NoFakturPajak/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadNoFakturPajakTable();" />--}%
							
								<g:fieldValue bean="${noFakturPajakInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${noFakturPajakInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="noFakturPajak.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${noFakturPajakInstance}" field="lastUpdProcess"
								url="${request.contextPath}/NoFakturPajak/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadNoFakturPajakTable();" />--}%
							
								<g:fieldValue bean="${noFakturPajakInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${noFakturPajakInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="noFakturPajak.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${noFakturPajakInstance}" field="dateCreated"
								url="${request.contextPath}/NoFakturPajak/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadNoFakturPajakTable();" />--}%
							
								<g:formatDate date="${noFakturPajakInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${noFakturPajakInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="noFakturPajak.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${noFakturPajakInstance}" field="lastUpdated"
								url="${request.contextPath}/NoFakturPajak/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadNoFakturPajakTable();" />--}%
							
								<g:formatDate date="${noFakturPajakInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${noFakturPajakInstance?.statushabis}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="statushabis-label" class="property-label"><g:message
					code="noFakturPajak.statushabis.label" default="Statushabis" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="statushabis-label">
						%{--<ba:editableValue
								bean="${noFakturPajakInstance}" field="statushabis"
								url="${request.contextPath}/NoFakturPajak/updatefield" type="text"
								title="Enter statushabis" onsuccess="reloadNoFakturPajakTable();" />--}%
							
								<g:fieldValue bean="${noFakturPajakInstance}" field="statushabis"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${noFakturPajakInstance?.tglakhirpajak}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tglakhirpajak-label" class="property-label"><g:message
					code="noFakturPajak.tglakhirpajak.label" default="Tglakhirpajak" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tglakhirpajak-label">
						%{--<ba:editableValue
								bean="${noFakturPajakInstance}" field="tglakhirpajak"
								url="${request.contextPath}/NoFakturPajak/updatefield" type="text"
								title="Enter tglakhirpajak" onsuccess="reloadNoFakturPajakTable();" />--}%
							
								<g:formatDate date="${noFakturPajakInstance?.tglakhirpajak}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${noFakturPajakInstance?.tglawalpajak}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tglawalpajak-label" class="property-label"><g:message
					code="noFakturPajak.tglawalpajak.label" default="Tglawalpajak" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tglawalpajak-label">
						%{--<ba:editableValue
								bean="${noFakturPajakInstance}" field="tglawalpajak"
								url="${request.contextPath}/NoFakturPajak/updatefield" type="text"
								title="Enter tglawalpajak" onsuccess="reloadNoFakturPajakTable();" />--}%
							
								<g:formatDate date="${noFakturPajakInstance?.tglawalpajak}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('noFakturPajak');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${noFakturPajakInstance?.id}"
					update="[success:'noFakturPajak-form',failure:'noFakturPajak-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteNoFakturPajak('${noFakturPajakInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
