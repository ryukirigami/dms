<%@ page import="com.kombos.maintable.NoFakturPajak" %>



<div class="control-group fieldcontain ${hasErrors(bean: noFakturPajakInstance, field: 'noawal', 'error')} ">
	<label class="control-label" for="noawal">
		<g:message code="noFakturPajak.noawal.label" default="Noawal" />
		
	</label>
	<div class="controls">
	<g:textField name="noawal" value="${noFakturPajakInstance?.noawal}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: noFakturPajakInstance, field: 'noakhir', 'error')} ">
	<label class="control-label" for="noakhir">
		<g:message code="noFakturPajak.noakhir.label" default="Noakhir" />
		
	</label>
	<div class="controls">
	<g:textField name="noakhir" value="${noFakturPajakInstance?.noakhir}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: noFakturPajakInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="noFakturPajak.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${noFakturPajakInstance?.lastUpdProcess}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: noFakturPajakInstance, field: 'statushabis', 'error')} ">
	<label class="control-label" for="statushabis">
		<g:message code="noFakturPajak.statushabis.label" default="Statushabis" />
		
	</label>
	<div class="controls">
	<g:textField name="statushabis" value="${noFakturPajakInstance?.statushabis}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: noFakturPajakInstance, field: 'tglakhirpajak', 'error')} required">
	<label class="control-label" for="tglakhirpajak">
		<g:message code="noFakturPajak.tglakhirpajak.label" default="Tglakhirpajak" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:datePicker name="tglakhirpajak" precision="day"  value="${noFakturPajakInstance?.tglakhirpajak}"  />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: noFakturPajakInstance, field: 'tglawalpajak', 'error')} required">
	<label class="control-label" for="tglawalpajak">
		<g:message code="noFakturPajak.tglawalpajak.label" default="Tglawalpajak" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:datePicker name="tglawalpajak" precision="day"  value="${noFakturPajakInstance?.tglawalpajak}"  />
	</div>
</div>

