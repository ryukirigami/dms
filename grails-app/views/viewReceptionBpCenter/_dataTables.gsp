
<%@ page import="com.kombos.reception.Reception" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="reception_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.namaCustomer.label" default="Nama Customer" /></div>
			</th>
			
			
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.nomorHp.label" default="Alamat Customer" /></div>
			</th>
			
			
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.nomorHp.label" default="Nomor Hp" /></div>
			</th>
			
			
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.kendaraan.label" default="Kendaraan" /></div>
			</th>
			
			
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.noPolKendaraan.label" default="No Pol Kendaraan" /></div>
			</th>
			
			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401NoWO.label" default="Nomor WO" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="reception.t401TanggalWO.label" default="Tanggal WO" /></div>
			</th>
		</tr>
	</thead>
</table>

<g:javascript>
var receptionTable;
var reloadReceptionTable;
$(function(){
	
	reloadReceptionTable = function() {
		receptionTable.fnDraw();
	}
	
	$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	receptionTable.fnDraw();
		}
	});
	
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	receptionTable = $('#reception_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

	{
		"sName": "t401NoWO",
		"mDataProp": "namaCustomer",
		"aTargets": [0],
		"mRender": function ( data, type, row ) {
			return '&nbsp;'+data;
		},
		"bSearchable": true,
		"bSortable": false,
		"sWidth":"300px",
		"bVisible": true
	}
	
	,

	{
		"sName": "alamatCustomer",
		"mDataProp": "alamatCustomer",
		"aTargets": [1],
		"bSearchable": true,
		"bSortable": false,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "nomorHp",
		"mDataProp": "nomorHp",
		"aTargets": [2],
		"bSearchable": true,
		"bSortable": false,
		"sWidth":"200px",
		"bVisible": true
	}
	
	,

	{
		"sName": "kendaraan",
		"mDataProp": "kendaraan",
		"aTargets": [3],
		"bSearchable": true,
		"bSortable": false,
		"sWidth":"200px",
		"bVisible": true
	}
	
	,

	{
		"sName": "noPolKendaraan",
		"mDataProp": "noPolKendaraan",
		"aTargets": [4],
		"bSearchable": true,
		"bSortable": false,
		"sWidth":"200px",
		"bVisible": true
	}
	
	,
	
	{
		"sName": "t401NoWO",
		"mDataProp": "t401NoWO",
		"aTargets": [5],
		"bSearchable": true,
		"bSortable": false,
		"sWidth":"200px",
		"bVisible": true
	}

	,

	{
		"sName": "t401TanggalWO",
		"mDataProp": "t401TanggalWO",
		"aTargets": [6],
		"bSearchable": true,
		"bSortable": false,
		"sWidth":"200px",
		"bVisible": true
	}
	

	],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var t401NoWO = $('#filter_t401NoWO input').val();
						if(t401NoWO){
							aoData.push(
									{"name": 'sCriteria_t401NoWO', "value": t401NoWO}
							);
						}

						var t401TanggalWO = $('#search_t401TanggalWO').val();
						var t401TanggalWODay = $('#search_t401TanggalWO_day').val();
						var t401TanggalWOMonth = $('#search_t401TanggalWO_month').val();
						var t401TanggalWOYear = $('#search_t401TanggalWO_year').val();
						
						if(t401TanggalWO){
							aoData.push(
									{"name": 'sCriteria_t401TanggalWO', "value": "date.struct"},
									{"name": 'sCriteria_t401TanggalWO_dp', "value": t401TanggalWO},
									{"name": 'sCriteria_t401TanggalWO_day', "value": t401TanggalWODay},
									{"name": 'sCriteria_t401TanggalWO_month', "value": t401TanggalWOMonth},
									{"name": 'sCriteria_t401TanggalWO_year', "value": t401TanggalWOYear}
							);
						}


						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
