

<%@ page import="com.kombos.maintable.NoPajak" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'noPajak.label', default: 'NoPajak')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteNoPajak;

$(function(){ 
	deleteNoPajak=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/noPajak/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadNoPajakTable();
   				expandTableLayout('noPajak');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-noPajak" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="noPajak"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${noPajakInstance?.tglawalpajak}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tglawalpajak-label" class="property-label"><g:message
					code="noPajak.tglawalpajak.label" default="Tglawalpajak" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tglawalpajak-label">
						%{--<ba:editableValue
								bean="${noPajakInstance}" field="tglawalpajak"
								url="${request.contextPath}/NoPajak/updatefield" type="text"
								title="Enter tglawalpajak" onsuccess="reloadNoPajakTable();" />--}%
							
								<g:formatDate date="${noPajakInstance?.tglawalpajak}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${noPajakInstance?.tglakhirpajak}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tglakhirpajak-label" class="property-label"><g:message
					code="noPajak.tglakhirpajak.label" default="Tglakhirpajak" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tglakhirpajak-label">
						%{--<ba:editableValue
								bean="${noPajakInstance}" field="tglakhirpajak"
								url="${request.contextPath}/NoPajak/updatefield" type="text"
								title="Enter tglakhirpajak" onsuccess="reloadNoPajakTable();" />--}%
							
								<g:formatDate date="${noPajakInstance?.tglakhirpajak}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${noPajakInstance?.noawal}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="noawal-label" class="property-label"><g:message
					code="noPajak.noawal.label" default="Noawal" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="noawal-label">
						%{--<ba:editableValue
								bean="${noPajakInstance}" field="noawal"
								url="${request.contextPath}/NoPajak/updatefield" type="text"
								title="Enter noawal" onsuccess="reloadNoPajakTable();" />--}%
							
								<g:fieldValue bean="${noPajakInstance}" field="noawal"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${noPajakInstance?.noakhir}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="noakhir-label" class="property-label"><g:message
					code="noPajak.noakhir.label" default="Noakhir" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="noakhir-label">
						%{--<ba:editableValue
								bean="${noPajakInstance}" field="noakhir"
								url="${request.contextPath}/NoPajak/updatefield" type="text"
								title="Enter noakhir" onsuccess="reloadNoPajakTable();" />--}%
							
								<g:fieldValue bean="${noPajakInstance}" field="noakhir"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${noPajakInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="noPajak.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${noPajakInstance}" field="staDel"
								url="${request.contextPath}/NoPajak/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadNoPajakTable();" />--}%
							
								<g:fieldValue bean="${noPajakInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${noPajakInstance?.statushabis}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="statushabis-label" class="property-label"><g:message
					code="noPajak.statushabis.label" default="Statushabis" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="statushabis-label">
						%{--<ba:editableValue
								bean="${noPajakInstance}" field="statushabis"
								url="${request.contextPath}/NoPajak/updatefield" type="text"
								title="Enter statushabis" onsuccess="reloadNoPajakTable();" />--}%
							
								<g:fieldValue bean="${noPajakInstance}" field="statushabis"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${noPajakInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="noPajak.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${noPajakInstance}" field="createdBy"
								url="${request.contextPath}/NoPajak/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadNoPajakTable();" />--}%
							
								<g:fieldValue bean="${noPajakInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${noPajakInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="noPajak.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${noPajakInstance}" field="updatedBy"
								url="${request.contextPath}/NoPajak/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadNoPajakTable();" />--}%
							
								<g:fieldValue bean="${noPajakInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${noPajakInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="noPajak.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${noPajakInstance}" field="lastUpdProcess"
								url="${request.contextPath}/NoPajak/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadNoPajakTable();" />--}%
							
								<g:fieldValue bean="${noPajakInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${noPajakInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="noPajak.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${noPajakInstance}" field="dateCreated"
								url="${request.contextPath}/NoPajak/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadNoPajakTable();" />--}%
							
								<g:formatDate date="${noPajakInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${noPajakInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="noPajak.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${noPajakInstance}" field="lastUpdated"
								url="${request.contextPath}/NoPajak/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadNoPajakTable();" />--}%
							
								<g:formatDate date="${noPajakInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('noPajak');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${noPajakInstance?.id}"
					update="[success:'noPajak-form',failure:'noPajak-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteNoPajak('${noPajakInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
