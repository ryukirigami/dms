<%@ page import="com.kombos.maintable.NoPajak" %>



<div class="control-group fieldcontain ${hasErrors(bean: noPajakInstance, field: 'tglawalpajak', 'error')} ">
	<label class="control-label" for="tglawalpajak">
		<g:message code="noPajak.tglawalpajak.label" default="Tglawalpajak" />

	</label>
	<div class="controls">
	<ba:datePicker name="tglawalpajak" precision="day"  value="${noPajakInstance?.tglawalpajak}"  format="dd/MM/yyyy" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: noPajakInstance, field: 'tglakhirpajak', 'error')} ">
	<label class="control-label" for="tglakhirpajak">
		<g:message code="noPajak.tglakhirpajak.label" default="Tglakhirpajak" />
		
	</label>
	<div class="controls">
	<ba:datePicker name="tglakhirpajak" precision="day"  value="${noPajakInstance?.tglakhirpajak}"  format="dd/MM/yyyy" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: noPajakInstance, field: 'noawal', 'error')} ">
	<label class="control-label" for="noawal">
		<g:message code="noPajak.noawal.label" default="Noawal" />
		
	</label>
	<div class="controls">
	<g:textField name="noawal" value="${noPajakInstance?.noawal}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: noPajakInstance, field: 'noakhir', 'error')} ">
	<label class="control-label" for="noakhir">
		<g:message code="noPajak.noakhir.label" default="Noakhir" />
		
	</label>
	<div class="controls">
	<g:textField name="noakhir" value="${noPajakInstance?.noakhir}"/>
	</div>
</div>


