
<%@ page import="com.kombos.hrd.CertificationKaryawan" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="certificationKaryawan_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="certificationKaryawan.karyawan.label" default="Karyawan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="certificationKaryawan.keterangan.label" default="Keterangan" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="certificationKaryawan.lastUpdProcess.label" default="Last Upd Process" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="certificationKaryawan.tglSertifikasi.label" default="Tgl Sertifikasi" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="certificationKaryawan.tipeSertifikasi.label" default="Tipe Sertifikasi" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="certificationKaryawan.training.label" default="Training" /></div>
			</th>

		
		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_karyawan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_karyawan" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_keterangan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_keterangan" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_lastUpdProcess" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_lastUpdProcess" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_tglSertifikasi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;" >
					<input type="hidden" name="search_tglSertifikasi" value="date.struct">
					<input type="hidden" name="search_tglSertifikasi_day" id="search_tglSertifikasi_day" value="">
					<input type="hidden" name="search_tglSertifikasi_month" id="search_tglSertifikasi_month" value="">
					<input type="hidden" name="search_tglSertifikasi_year" id="search_tglSertifikasi_year" value="">
					<input type="text" data-date-format="dd/mm/yyyy" name="search_tglSertifikasi_dp" value="" id="search_tglSertifikasi" class="search_init">
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_tipeSertifikasi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_tipeSertifikasi" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_training" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_training" class="search_init" />
				</div>
			</th>


		</tr>
	</thead>
</table>

<g:javascript>
var certificationKaryawanTable;
var reloadCertificationKaryawanTable;
$(function(){
	
	reloadCertificationKaryawanTable = function() {
		certificationKaryawanTable.fnDraw();
	}

	
	$('#search_tglSertifikasi').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tglSertifikasi_day').val(newDate.getDate());
			$('#search_tglSertifikasi_month').val(newDate.getMonth()+1);
			$('#search_tglSertifikasi_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			certificationKaryawanTable.fnDraw();	
	});

	


$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	certificationKaryawanTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	certificationKaryawanTable = $('#certificationKaryawan_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "karyawan",
	"mDataProp": "karyawan",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "keterangan",
	"mDataProp": "keterangan",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "lastUpdProcess",
	"mDataProp": "lastUpdProcess",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "tglSertifikasi",
	"mDataProp": "tglSertifikasi",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "tipeSertifikasi",
	"mDataProp": "tipeSertifikasi",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "training",
	"mDataProp": "training",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var karyawan = $('#filter_karyawan input').val();
						if(karyawan){
							aoData.push(
									{"name": 'sCriteria_karyawan', "value": karyawan}
							);
						}
	
						var keterangan = $('#filter_keterangan input').val();
						if(keterangan){
							aoData.push(
									{"name": 'sCriteria_keterangan', "value": keterangan}
							);
						}
	
						var lastUpdProcess = $('#filter_lastUpdProcess input').val();
						if(lastUpdProcess){
							aoData.push(
									{"name": 'sCriteria_lastUpdProcess', "value": lastUpdProcess}
							);
						}

						var tglSertifikasi = $('#search_tglSertifikasi').val();
						var tglSertifikasiDay = $('#search_tglSertifikasi_day').val();
						var tglSertifikasiMonth = $('#search_tglSertifikasi_month').val();
						var tglSertifikasiYear = $('#search_tglSertifikasi_year').val();
						
						if(tglSertifikasi){
							aoData.push(
									{"name": 'sCriteria_tglSertifikasi', "value": "date.struct"},
									{"name": 'sCriteria_tglSertifikasi_dp', "value": tglSertifikasi},
									{"name": 'sCriteria_tglSertifikasi_day', "value": tglSertifikasiDay},
									{"name": 'sCriteria_tglSertifikasi_month', "value": tglSertifikasiMonth},
									{"name": 'sCriteria_tglSertifikasi_year', "value": tglSertifikasiYear}
							);
						}
	
						var tipeSertifikasi = $('#filter_tipeSertifikasi input').val();
						if(tipeSertifikasi){
							aoData.push(
									{"name": 'sCriteria_tipeSertifikasi', "value": tipeSertifikasi}
							);
						}
	
						var training = $('#filter_training input').val();
						if(training){
							aoData.push(
									{"name": 'sCriteria_training', "value": training}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
