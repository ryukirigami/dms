

<%@ page import="com.kombos.hrd.CertificationKaryawan" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'certificationKaryawan.label', default: 'CertificationKaryawan')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteCertificationKaryawan;

$(function(){ 
	deleteCertificationKaryawan=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/certificationKaryawan/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadCertificationKaryawanTable();
   				expandTableLayout('certificationKaryawan');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-certificationKaryawan" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="certificationKaryawan"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${certificationKaryawanInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="certificationKaryawan.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${certificationKaryawanInstance}" field="createdBy"
								url="${request.contextPath}/CertificationKaryawan/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadCertificationKaryawanTable();" />--}%
							
								<g:fieldValue bean="${certificationKaryawanInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${certificationKaryawanInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="certificationKaryawan.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${certificationKaryawanInstance}" field="dateCreated"
								url="${request.contextPath}/CertificationKaryawan/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadCertificationKaryawanTable();" />--}%
							
								<g:formatDate date="${certificationKaryawanInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${certificationKaryawanInstance?.karyawan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="karyawan-label" class="property-label"><g:message
					code="certificationKaryawan.karyawan.label" default="Karyawan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="karyawan-label">
						%{--<ba:editableValue
								bean="${certificationKaryawanInstance}" field="karyawan"
								url="${request.contextPath}/CertificationKaryawan/updatefield" type="text"
								title="Enter karyawan" onsuccess="reloadCertificationKaryawanTable();" />--}%
							
								<g:link controller="karyawan" action="show" id="${certificationKaryawanInstance?.karyawan?.id}">${certificationKaryawanInstance?.karyawan?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${certificationKaryawanInstance?.keterangan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="keterangan-label" class="property-label"><g:message
					code="certificationKaryawan.keterangan.label" default="Keterangan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="keterangan-label">
						%{--<ba:editableValue
								bean="${certificationKaryawanInstance}" field="keterangan"
								url="${request.contextPath}/CertificationKaryawan/updatefield" type="text"
								title="Enter keterangan" onsuccess="reloadCertificationKaryawanTable();" />--}%
							
								<g:fieldValue bean="${certificationKaryawanInstance}" field="keterangan"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${certificationKaryawanInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="certificationKaryawan.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${certificationKaryawanInstance}" field="lastUpdProcess"
								url="${request.contextPath}/CertificationKaryawan/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadCertificationKaryawanTable();" />--}%
							
								<g:fieldValue bean="${certificationKaryawanInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${certificationKaryawanInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="certificationKaryawan.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${certificationKaryawanInstance}" field="lastUpdated"
								url="${request.contextPath}/CertificationKaryawan/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadCertificationKaryawanTable();" />--}%
							
								<g:formatDate date="${certificationKaryawanInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${certificationKaryawanInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="certificationKaryawan.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${certificationKaryawanInstance}" field="staDel"
								url="${request.contextPath}/CertificationKaryawan/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadCertificationKaryawanTable();" />--}%
							
								<g:fieldValue bean="${certificationKaryawanInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${certificationKaryawanInstance?.tglSertifikasi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tglSertifikasi-label" class="property-label"><g:message
					code="certificationKaryawan.tglSertifikasi.label" default="Tgl Sertifikasi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tglSertifikasi-label">
						%{--<ba:editableValue
								bean="${certificationKaryawanInstance}" field="tglSertifikasi"
								url="${request.contextPath}/CertificationKaryawan/updatefield" type="text"
								title="Enter tglSertifikasi" onsuccess="reloadCertificationKaryawanTable();" />--}%
							
								<g:formatDate date="${certificationKaryawanInstance?.tglSertifikasi}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${certificationKaryawanInstance?.tipeSertifikasi}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="tipeSertifikasi-label" class="property-label"><g:message
					code="certificationKaryawan.tipeSertifikasi.label" default="Tipe Sertifikasi" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="tipeSertifikasi-label">
						%{--<ba:editableValue
								bean="${certificationKaryawanInstance}" field="tipeSertifikasi"
								url="${request.contextPath}/CertificationKaryawan/updatefield" type="text"
								title="Enter tipeSertifikasi" onsuccess="reloadCertificationKaryawanTable();" />--}%
							
								<g:link controller="certificationType" action="show" id="${certificationKaryawanInstance?.tipeSertifikasi?.id}">${certificationKaryawanInstance?.tipeSertifikasi?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${certificationKaryawanInstance?.training}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="training-label" class="property-label"><g:message
					code="certificationKaryawan.training.label" default="Training" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="training-label">
						%{--<ba:editableValue
								bean="${certificationKaryawanInstance}" field="training"
								url="${request.contextPath}/CertificationKaryawan/updatefield" type="text"
								title="Enter training" onsuccess="reloadCertificationKaryawanTable();" />--}%
							
								<g:link controller="trainingClassRoom" action="show" id="${certificationKaryawanInstance?.training?.id}">${certificationKaryawanInstance?.training?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${certificationKaryawanInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="certificationKaryawan.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${certificationKaryawanInstance}" field="updatedBy"
								url="${request.contextPath}/CertificationKaryawan/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadCertificationKaryawanTable();" />--}%
							
								<g:fieldValue bean="${certificationKaryawanInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('certificationKaryawan');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${certificationKaryawanInstance?.id}"
					update="[success:'certificationKaryawan-form',failure:'certificationKaryawan-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteCertificationKaryawan('${certificationKaryawanInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
