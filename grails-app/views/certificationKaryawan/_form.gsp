<%@ page import="com.kombos.hrd.CertificationKaryawan" %>

<div class="control-group fieldcontain ${hasErrors(bean: certificationKaryawanInstance, field: 'tipeSertifikasi', 'error')} ">
    <label class="control-label" for="tipeSertifikasi">
        <g:message code="certificationKaryawan.tipeSertifikasi.label" default="Tipe Sertifikasi" />

    </label>
    <div class="controls">
        <g:select id="tipeSertifikasi" name="tipeSertifikasi.id" from="${com.kombos.hrd.CertificationType.list()}" optionKey="id" required="" value="${certificationKaryawanInstance?.tipeSertifikasi?.id}" class="many-to-one"/>
    </div>
</div>



<div class="control-group fieldcontain ${hasErrors(bean: certificationKaryawanInstance, field: 'tglSertifikasi', 'error')} ">
    <label class="control-label" for="tglSertifikasi">
        <g:message code="certificationKaryawan.tglSertifikasi.label" default="Tgl Sertifikasi" />

    </label>
    <div class="controls">
        <ba:datePicker name="tglSertifikasi" precision="day" value="${certificationKaryawanInstance?.tglSertifikasi}" format="yyyy-MM-dd"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: certificationKaryawanInstance, field: 'training', 'error')} ">
    <label class="control-label" for="training">
        <g:message code="certificationKaryawan.training.label" default="Training" />

    </label>
    <div class="controls">
        <g:select id="training" name="training.id" from="${com.kombos.hrd.TrainingClassRoom.list()}" optionKey="id" required="" value="${certificationKaryawanInstance?.training?.id}" class="many-to-one"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: certificationKaryawanInstance, field: 'keterangan', 'error')} ">
    <label class="control-label" for="keterangan">
        <g:message code="certificationKaryawan.keterangan.label" default="Keterangan" />

    </label>
    <div class="controls">
        <g:textArea name="keterangan" value="${certificationKaryawanInstance?.keterangan}" />
    </div>
</div>

<g:if test="${!params.onDataKaryawan}">

<div class="control-group fieldcontain ${hasErrors(bean: certificationKaryawanInstance, field: 'karyawan', 'error')} ">
	<label class="control-label" for="karyawan">
		<g:message code="certificationKaryawan.karyawan.label" default="Karyawan" />
		
	</label>
	<div class="controls">
	<g:select id="karyawan" name="karyawan.id" from="${com.kombos.hrd.Karyawan.list()}" optionKey="id" required="" value="${certificationKaryawanInstance?.karyawan?.id}" class="many-to-one"/>
	</div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: certificationKaryawanInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="certificationKaryawan.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${certificationKaryawanInstance?.lastUpdProcess}" />
	</div>
</div>

</g:if>