

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="boardAntrian_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="boardAntrian.noantri.label" default="Nomor Antrian" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="wo.noPolisi.label" default="Nomor Polisi" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="wo.noLoket.label" default="Nomor Loket" /></div>
        </th>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="wo.ket.label" default="Jenis Service" /></div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var BoardAntrianTable;
var reloadBoardAntrianTable;
var indMulai = 0;
$(function(){
	
	reloadBoardAntrianTable = function() {
		BoardAntrianTable.fnDraw();
	}

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	BoardAntrianTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	BoardAntrianTable = $('#boardAntrian_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : 15, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "noAntri",
	"mDataProp": "noAntri",
	"aTargets": [2],
    "mRender": function ( data, type, row ) {
        if(data){
    		return '<span style="font-size: 35px">'+data+'</span>';
        }else{
            return '';
        }
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"25%",
	"bVisible": true
}

,

{
	"sName": "noPol",
	"mDataProp": "noPol",
	"aTargets": [3],
    "mRender": function ( data, type, row ) {
        if(data){
    		return '<span style="font-size: 35px">'+data+'</span>';
        }else{
            return '';
        }
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"25%",
	"bVisible": true
}

,

{
	"sName": "noloket",
	"mDataProp": "noloket",
	"aTargets": [3],
    "mRender": function ( data, type, row ) {
        if(data){
    		return '<span style="font-size: 35px">'+data+'</span>';
        }else{
            return '';
        }
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"25%",
	"bVisible": true
}
,

{
	"sName": "ket",
	"mDataProp": "ket",
	"aTargets": [4],
    "mRender": function ( data, type, row ) {
        if(data){
    		return '<span style="font-size: 35px">'+data+'</span>';
        }else{
            return '';
        }
	},
	"bSearchable": true,
	"bSortable": false,
	"sWidth":"25%",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	                    aoData.push(
                                {"name": 'indMulai', "value": indMulai}
                        );
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
							    indMulai = json.indMulai;
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});

	var timer = setInterval(function() {
        reloadBoardAntrianTable();
    }, 60000);
});
</g:javascript>


			
