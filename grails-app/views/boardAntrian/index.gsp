
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="antriCustomer.label" default="Board Antrian Customer" /></title>
    <r:require modules="baseapplayout" />
    <g:javascript disposition="head">
    </g:javascript>
</head>
<body>
<div class="navbar box-header no-border">
    <span class="pull-left"><g:message code="antriCustomer.label" default="Board Antrian Customer" /></span>
    
</div>
<div class="box">
    <div class="span12" id="boardAntrian-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>
        <g:render template="dataTablesAntri"/>
    </div>
    <div class="span7" id="boardAntrian-form" style="display: none;"></div>
</div>
</body>
</html>
