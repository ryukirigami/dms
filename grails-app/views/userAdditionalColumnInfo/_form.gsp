<%@ page import="com.kombos.baseapp.sec.shiro.UserAdditionalColumnInfo" %>



<div class="control-group fieldcontain ${hasErrors(bean: userAdditionalColumnInfoInstance, field: 'columnName', 'error')} ">
    <label class="control-label" for="columnName">
        <g:message code="userAdditionalColumnInfo.columnName.label" default="Column Name"/>

    </label>

    <div class="controls">
        <g:textField name="columnName" value="${userAdditionalColumnInfoInstance?.columnName}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userAdditionalColumnInfoInstance, field: 'columnType', 'error')} ">
    <label class="control-label" for="columnType">
        <g:message code="userAdditionalColumnInfo.columnType.label" default="Column Type"/>

    </label>

    <div class="controls">
        <g:textField name="columnType" value="${userAdditionalColumnInfoInstance?.columnType}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: userAdditionalColumnInfoInstance, field: 'label', 'error')} ">
    <label class="control-label" for="label">
        <g:message code="userAdditionalColumnInfo.label.label" default="Label"/>

    </label>

    <div class="controls">
        <g:textField name="label" value="${userAdditionalColumnInfoInstance?.label}"/>
    </div>
</div>

