<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>UserAdditionalColumnInfo</title>
    <script type="text/javascript">

        function initUserAdditionalColumnInfoGrid(){
            jQuery("#UserAdditionalColumnInfo-Grid").jqGrid({
                url: '${request.getContextPath()}/userAdditionalColumnInfo/getList',
                editurl: '${request.getContextPath()}/userAdditionalColumnInfo/edit',
                datatype: 'json',
                mtype: 'POST',
                colNames:[
                    
                    '<g:message code="app.userAdditionalColumnInfo.columnName.label" default="ColumnName"/>',  
                    '<g:message code="app.userAdditionalColumnInfo.label.label" default="Label"/>'  

                ],
                colModel :[
                    
                    {name:'columnName', index:'columnName',width:200,editable:true,edittype:'text',editrules:{required:true},searchoptions:{sopt:['eq','ne','cn','nc']}}  ,   
                    {name:'label', index:'label',width:200,editable:true,edittype:'text',editrules:{required:true},searchoptions:{sopt:['eq','ne','cn','nc']}}   
                ],
                height: 'auto',
                pager: '#UserAdditionalColumnInfo-Pager',
                rowNum:10,
                rowList:[10,20,30],
                viewrecords: true,
                sortable: true,
                gridview: true,
                forceFit: true,
                loadui: 'block',
                rownumbers: true,
                caption: 'UserAdditionalColumnInfo',
                onSelectRow: function(rowid){
                    var rd = jQuery("#UserAdditionalColumnInfo-Grid").jqGrid('getRowData', rowid);
                    //TODO
                },
                gridComplete: function(){
                    try{
                        jQuery("#cData").click();
                        setPermission('${request.getContextPath()}/userAdditionalColumnInfo','UserAdditionalColumnInfo-Grid');
                    }catch(e){
                    }
                },
                sortname: 'id',
                sortorder: 'asc',
                beforeRequest: function(){
                    checkSession();
                }
            });

            jQuery('#UserAdditionalColumnInfo-Grid').jqGrid(
                'navGrid','#UserAdditionalColumnInfo-Pager',
                {edit:true,add:true,del:true,view:true}, // options
                {
                    afterSubmit: afterSubmitForm,
                    beforeShowForm:function(formid) {
                        checkSession();
                    }
                }, // edit options
                {
                    afterSubmit: afterSubmitForm,
                    beforeShowForm:function(formid) {
                        checkSession();
                    }
                }, // add options
                {
                    afterSubmit: afterSubmitForm,
                    onclickSubmit: function(params, posdata){
                        var id = jQuery("#UserAdditionalColumnInfo-Grid").jqGrid('getGridParam','selrow');
                        var rd = jQuery("#UserAdditionalColumnInfo-Grid").jqGrid('getRowData', id);
                        return {columnName : rd.columnName};
                    },
                    beforeShowForm:function(formid) {
                        checkSession();
                    }
                }, // del options
                {
                    multipleSearch:true, showOnLoad:false,
                    onInitializeSearch : function(form_id) {
                        jQuery(".vdata",form_id).keypress(function(e){
                            var code = (e.keyCode ? e.keyCode : e.which);
                            if(code == 13) { //Enter keycode
                                e.preventDefault();
                                jQuery(".ui-search").click();
                            }
                        });
                    },
                    beforeShowForm:function(formid) {
                        checkSession();
                    }
                }, // search options
                {} // view options
            );
        }

        function afterSubmitForm(response, postdata){
            var res = jQuery.parseJSON(response.responseText);
            alert(res.message);
            return [true,""];
        }

        jQuery(function(){
            initUserAdditionalColumnInfoGrid();
        });

    </script>
</head>
<body>

<table id="UserAdditionalColumnInfo-Grid"></table>
<div id="UserAdditionalColumnInfo-Pager"></div>

</body>
</html>