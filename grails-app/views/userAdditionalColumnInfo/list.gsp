<%@ page import="com.kombos.baseapp.sec.shiro.UserAdditionalColumnInfo" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName"
           value="${message(code: 'userAdditionalColumnInfo.label', default: 'UserAdditionalColumnInfo')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>

    <g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
                                   onLoading="jQuery('#spinner').fadeIn(1);"
                                   onSuccess="loadForm(data, textStatus);"
                                   onComplete="jQuery('#spinner').fadeOut();"/>
        break;
               case '_DELETE_' :
                   bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}',
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/userAdditionalColumnInfo/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/userAdditionalColumnInfo/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#userAdditionalColumnInfo-form').empty();
    	$('#userAdditionalColumnInfo-form').append(data);
   	}
    
    shrinkTableLayout = function(){
   		$("#userAdditionalColumnInfo-table").switchClass( "span12", "span5", 10 );
        $("#userAdditionalColumnInfo-form").css("display","block");
       	shrinkUserAdditionalColumnInfoTable(); 
   	}
   	
   	expandTableLayout = function(){
   		$("#userAdditionalColumnInfo-table").switchClass( "span5", "span12", 10 );
        $("#userAdditionalColumnInfo-form").css("display","none");
       	expandUserAdditionalColumnInfoTable(); 
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#userAdditionalColumnInfo-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/userAdditionalColumnInfo/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadUserAdditionalColumnInfoTable();
    		}
		});
		
   	}

});
    </g:javascript>
</head>

<body>
<h3 class="navbar box-header no-border">
    <g:message code="default.list.label" args="[entityName]"/>
    <ul class="nav pull-right">
        <li class="dropdown"><a data-toggle="dropdown"
                                class="dropdown-toggle usermenu" href="#"><i class="icon-filter"></i></a>

            <div class="dropdown-menu" style="width: 400px;">
                <table cellspacing="0" cellpadding="0" border="0"
                       class="display dropdown-form" id="userAdditionalColumnInfoDatatables-filters"
                       width="100%">
                    <thead>
                    <tr>
                        <th>Column</th>
                        <th>Filter</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div></li>
        <li class="separator"></li>
        <li><a class="pull-right box-action" href="#"
               style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i
                    class="icon-plus"></i>&nbsp;&nbsp;
        </a></li>
        <li><a class="pull-right box-action" href="#"
               style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
                    class="icon-remove"></i>&nbsp;&nbsp;
        </a></li>
        <li class="separator"></li>
    </ul>
</h3>

<div class="box">
    <div class="span12" id="userAdditionalColumnInfo-table">
        <g:if test="${flash.message}">
            <div class="message" role="status">
                ${flash.message}
            </div>
        </g:if>

        <g:render template="dataTables"/>
    </div>

    <div class="span7" id="userAdditionalColumnInfo-form" style="display: none;"></div>
</div>
</body>
</html>
