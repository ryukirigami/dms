<%@ page import="com.kombos.baseapp.sec.shiro.UserAdditionalColumnInfo" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="bootstrapeditable"/>
    <g:set var="entityName"
           value="${message(code: 'userAdditionalColumnInfo.label', default: 'UserAdditionalColumnInfo')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <g:javascript disposition="head">
var deleteUserAdditionalColumnInfo;

$(function(){ 
	deleteUserAdditionalColumnInfo=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/userAdditionalColumnInfo/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadUserAdditionalColumnInfoTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

    </g:javascript>
</head>

<body>
<div id="show-userAdditionalColumnInfo" role="main">
    <legend>
        <g:message code="default.show.label" args="[entityName]"/>
    </legend>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table id="userAdditionalColumnInfo"
           class="table table-bordered table-hover">
        <tbody>

        <g:if test="${userAdditionalColumnInfoInstance?.columnName}">
            <tr>
                <td class="span2" style="text-align: right;"><span
                        id="columnName-label" class="property-label"><g:message
                            code="userAdditionalColumnInfo.columnName.label" default="Column Name"/>:</span></td>

                <td class="span3"><span class="property-value"
                                        aria-labelledby="columnName-label">
                    ${userAdditionalColumnInfoInstance?.columnName}
                </span></td>

            </tr>
        </g:if>

        <g:if test="${userAdditionalColumnInfoInstance?.columnType}">
            <tr>
                <td class="span2" style="text-align: right;"><span
                        id="columnType-label" class="property-label"><g:message
                            code="userAdditionalColumnInfo.columnType.label" default="Column Type"/>:</span></td>

                <td class="span3"><span class="property-value"
                                        aria-labelledby="columnType-label">
                    ${userAdditionalColumnInfoInstance?.columnType}
                </span></td>

            </tr>
        </g:if>

        <g:if test="${userAdditionalColumnInfoInstance?.label}">
            <tr>
                <td class="span2" style="text-align: right;"><span
                        id="label-label" class="property-label"><g:message
                            code="userAdditionalColumnInfo.label.label" default="Label"/>:</span></td>

                <td class="span3"><span class="property-value"
                                        aria-labelledby="label-label">
                    ${userAdditionalColumnInfoInstance?.label}
                </span></td>

            </tr>
        </g:if>

        </tbody>
    </table>
    <g:form class="form-horizontal">
        <fieldset class="buttons controls">
            <a class="btn cancel" href="javascript:void(0);"
               onclick="expandTableLayout();"><g:message
                    code="default.button.cancel.label" default="Cancel"/></a>
            <g:remoteLink class="btn btn-primary edit" action="edit"
                          id="${userAdditionalColumnInfoInstance?.id}"
                          update="[success: 'userAdditionalColumnInfo-form', failure: 'userAdditionalColumnInfo-form']"
                          on404="alert('not found');">
                <g:message code="default.button.edit.label" default="Edit"/>
            </g:remoteLink>
            %{--<ba:confirm id="delete" class="btn cancel"--}%
                        %{--message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"--}%
                        %{--onsuccess="deleteUserAdditionalColumnInfo('${userAdditionalColumnInfoInstance?.id}')"--}%
                        %{--label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>--}%
        </fieldset>
    </g:form>
</div>
</body>
</html>
