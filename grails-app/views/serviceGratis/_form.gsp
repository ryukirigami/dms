<%@ page import="com.kombos.administrasi.ServiceGratis" %>



<div class="control-group fieldcontain ${hasErrors(bean: serviceGratisInstance, field: 'operation', 'error')} required">
	<label class="control-label" for="operation">
		<g:message code="serviceGratis.operation.label" default="Operation" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="operation" name="operation.id" from="${com.kombos.administrasi.Operation.list()}" optionKey="id" required="" value="${serviceGratisInstance?.operation?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: serviceGratisInstance, field: 'createdBy', 'error')} ">
	<label class="control-label" for="createdBy">
		<g:message code="serviceGratis.createdBy.label" default="Created By" />
		
	</label>
	<div class="controls">
	<g:textField name="createdBy" value="${serviceGratisInstance?.createdBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: serviceGratisInstance, field: 'updatedBy', 'error')} ">
	<label class="control-label" for="updatedBy">
		<g:message code="serviceGratis.updatedBy.label" default="Updated By" />
		
	</label>
	<div class="controls">
	<g:textField name="updatedBy" value="${serviceGratisInstance?.updatedBy}"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: serviceGratisInstance, field: 'lastUpdProcess', 'error')} ">
	<label class="control-label" for="lastUpdProcess">
		<g:message code="serviceGratis.lastUpdProcess.label" default="Last Upd Process" />
		
	</label>
	<div class="controls">
	<g:textField name="lastUpdProcess" value="${serviceGratisInstance?.lastUpdProcess}"/>
	</div>
</div>

