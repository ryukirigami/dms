

<%@ page import="com.kombos.administrasi.ServiceGratis" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'serviceGratis.label', default: 'Service Gratis')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteServiceGratis;

$(function(){ 
	deleteServiceGratis=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/serviceGratis/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadServiceGratisTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-serviceGratis" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="serviceGratis"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${serviceGratisInstance?.operation}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="operation-label" class="property-label"><g:message
					code="serviceGratis.operation.label" default="Operation" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="operation-label">
						%{--<ba:editableValue
								bean="${serviceGratisInstance}" field="operation"
								url="${request.contextPath}/ServiceGratis/updatefield" type="text"
								title="Enter operation" onsuccess="reloadServiceGratisTable();" />--}%
							
								<g:link controller="operation" action="show" id="${serviceGratisInstance?.operation?.id}">${serviceGratisInstance?.operation?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${serviceGratisInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="serviceGratis.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${serviceGratisInstance}" field="createdBy"
								url="${request.contextPath}/ServiceGratis/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadServiceGratisTable();" />--}%
							
								<g:fieldValue bean="${serviceGratisInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${serviceGratisInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="serviceGratis.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${serviceGratisInstance}" field="updatedBy"
								url="${request.contextPath}/ServiceGratis/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadServiceGratisTable();" />--}%
							
								<g:fieldValue bean="${serviceGratisInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${serviceGratisInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="serviceGratis.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${serviceGratisInstance}" field="lastUpdProcess"
								url="${request.contextPath}/ServiceGratis/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadServiceGratisTable();" />--}%
							
								<g:fieldValue bean="${serviceGratisInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${serviceGratisInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="serviceGratis.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${serviceGratisInstance}" field="dateCreated"
								url="${request.contextPath}/ServiceGratis/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadServiceGratisTable();" />--}%
							
								<g:formatDate date="${serviceGratisInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${serviceGratisInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="serviceGratis.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${serviceGratisInstance}" field="lastUpdated"
								url="${request.contextPath}/ServiceGratis/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadServiceGratisTable();" />--}%
							
								<g:formatDate date="${serviceGratisInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${serviceGratisInstance?.id}"
					update="[success:'serviceGratis-form',failure:'serviceGratis-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteServiceGratis('${serviceGratisInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
