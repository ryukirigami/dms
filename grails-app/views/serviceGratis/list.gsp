
<%@ page import="com.kombos.administrasi.ServiceGratis" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'serviceGratis.label', default: 'Service Gratis')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	$('.box-action').click(function(){
		switch($(this).attr('target')){
         	case '_CREATE_' :                      
         		shrinkTableLayout();
         		<g:remoteFunction action="create"
		onLoading="jQuery('#spinner').fadeIn(1);"
		onSuccess="loadForm(data, textStatus);"
		onComplete="jQuery('#spinner').fadeOut();" />
         		break;
         	case '_DELETE_' :  
         		bootbox.confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}', 
         			function(result){
         				if(result){
         					massDelete();
         				}
         			});                    
         		
         		break;
       }    
       return false;
	});

    show = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/serviceGratis/show/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    edit = function(id) {
    	shrinkTableLayout();
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'POST', url:'${request.contextPath}/serviceGratis/edit/'+id,
   			success:function(data,textStatus){
   				loadForm(data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadForm = function(data, textStatus){
		$('#serviceGratis-form').empty();
    	$('#serviceGratis-form').append(data);
   	}
    
    shrinkTableLayout = function(){
    	if($("#serviceGratis-table").hasClass("span12")){
   			$("#serviceGratis-table").toggleClass("span12 span5");
        }
        $("#serviceGratis-form").css("display","block"); 
   	}
   	
   	expandTableLayout = function(){
   		if($("#serviceGratis-table").hasClass("span5")){
   			$("#serviceGratis-table").toggleClass("span5 span12");
   		}
        $("#serviceGratis-form").css("display","none");
   	}
   	
   	massDelete = function() {
   		var recordsToDelete = [];
		$("#serviceGratis-table tbody .row-select").each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:'${request.contextPath}/serviceGratis/massdelete',      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		reloadServiceGratisTable();
    		}
		});
		
   	}

   	$("#pilihJobModal").on("show", function() {
		$("#pilihJobModal .btn").on("click", function(e) {
			$("#pilihJobModal").modal('hide');
		});
	});
	$("#pilihJobModal").on("hide", function() {
		$("#pilihJobModal a.btn").off("click");
	});

   	loadPilihJobModal = function(){

		$("#pilihJobContent").empty();
		expandTableLayout();
		$.ajax({type:'POST', url:'${request.contextPath}/pilihJobs',
   			success:function(data,textStatus){
					$("#pilihJobContent").html(data);
				    $("#pilihJobModal").modal({
						"backdrop" : "static",
						"keyboard" : true,
						"show" : true
					}).css({'width': '1175px','margin-left': function () {return -($(this).width() / 2);}});

   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});



    }



     pilihJob = function(good) {

     //   alert("id harga jual : "+idHargaJualNonSPLD );
		$.ajax({
    		url:'${request.contextPath}/pilihJobs',
    		type: "GET", // Always use POST when deleting data
    	    data:{},
    	    complete: function(xhr, status) {
                $("#pilihJobModal").modal('hide');
                $("#pilihJobContent").empty();

    		},
    		success:function(data,textStatus){
       			loadForm(data, textStatus);
            }

		});


   	}

   	tambahServiceGratis = function(){
           checkJob =[];
            $("#operation-table tbody .row-select").each(function() {
                if(this.checked){
                    var id = $(this).next("input:hidden").val();
                    checkJob.push(id);
                }
            });
            if(checkJob.length<1){
                alert('Anda belum memilih data yang akan ditambahkan');
                loadPilihJobModal();
            }
                var checkJobMap = JSON.stringify(checkJob);
                $.ajax({
                url:'${request.contextPath}/serviceGratis/tambah',
                type: "POST", // Always use POST when deleting data
                data : { ids: checkJobMap },
                success : function(data){
                    $('#result_search').empty();
                    reloadOperationTable();
                    reloadServiceGratisTable();
                },
                error: function(xhr, textStatus, errorThrown) {
                    alert('Internal Server Error');
                }
                });

        checkJob = [];

    }

});
</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="default.list.label" args="[entityName]" /></span>
		<ul class="nav pull-right">
			%{--<li><a class="pull-right box-action" href="#"--}%
				%{--style="display: block;" target="_CREATE_">&nbsp;&nbsp;<i--}%
					%{--class="icon-plus"></i>&nbsp;&nbsp;--}%
			%{--</a></li>--}%
			<li><a class="pull-right box-action" href="#"
				style="display: block;" target="_DELETE_">&nbsp;&nbsp;<i
					class="icon-remove"></i>&nbsp;&nbsp;
			</a></li>
			<li class="separator"></li>
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="serviceGratis-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <g:field type="button" onclick="loadPilihJobModal();" class="btn btn-primary"
            name="cari" id="cari" value="${message(code: 'default.button.cariJob.label', default: 'Cari Job')}" />
			<g:render template="dataTables" />
		</div>
		<div class="span7" id="serviceGratis-form" style="display: none;"></div>
	</div>
    <div id="pilihJobModal" class="modal fade">
        <div class="modal-dialog" style="width: 1175px;">
            <div class="modal-content" style="width: 1175px;">
                <!-- dialog body -->
                <div class="modal-body" style="max-height: 550px;">
                    %{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}%
                    <div id="pilihJobContent">
                        <div class="iu-content"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
