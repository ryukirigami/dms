
<%@ page import="com.kombos.administrasi.ServiceGratis" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="serviceGratis_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover table-selectable"
       width="98%">
    <thead>
    <tr>

        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="serviceGratis.operation.label" default="Kode Service Gratis" /></div>
        </th>


        <th style="border-bottom: none;padding: 5px;">
            <div><g:message code="serviceGratis.namaOperation.label" default="Nama Service Gratis" /></div>
        </th>


    </tr>
    <tr>


        <th style="border-top: none;padding: 5px;">
            <div id="filter_operation" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_operation" class="search_init"  />
            </div>
        </th>

        <th style="border-top: none;padding: 5px;">
            <div id="filter_namaOperation" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                <input type="text" name="search_namaOperation" class="search_init"  />
            </div>
        </th>

    </tr>
    </thead>
</table>

<g:javascript>
var serviceGratisTable;
var reloadServiceGratisTable;
$(function(){
	
	reloadServiceGratisTable = function() {
		serviceGratisTable.fnDraw();
	}

    var recordsserviceGratisperpage = [];
    var anServiceGratisSelected;
    var jmlRecServiceGratisPerPage=0;
    var id;

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	serviceGratisTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	serviceGratisTable = $('#serviceGratis_datatables').dataTable({
		"sScrollX": "98%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsServiceGratis = $("#serviceGratis_datatables tbody .row-select");
            var jmlServiceGratisCek = 0;
            var nRow;
            var idRec;
            rsServiceGratis.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsserviceGratisperpage[idRec]=="1"){
                    jmlServiceGratisCek = jmlServiceGratisCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsserviceGratisperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecServiceGratisPerPage = rsServiceGratis.length;
            if(jmlServiceGratisCek==jmlRecServiceGratisPerPage && jmlRecServiceGratisPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
        "fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "operation",
	"mDataProp": "operation",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;'+data//<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"50%",
	"bVisible": true
}

,

{
	"sName": "namaoperation",
	"mDataProp": "namaOperation",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"50%",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var operation = $('#filter_operation input').val();
						if(operation){
							aoData.push(
									{"name": 'sCriteria_operation', "value": operation}
							);
						}
	
						var namaOperation = $('#filter_namaOperation input').val();
						if(namaOperation){
							aoData.push(
									{"name": 'sCriteria_namaOperation', "value": namaOperation}
							);
						}
	
						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
    $('.select-all').click(function(e) {
        $("#serviceGratis_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsserviceGratisperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsserviceGratisperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

	$('#serviceGratis_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsserviceGratisperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anServiceGratisSelected = serviceGratisTable.$('tr.row_selected');
            if(jmlRecServiceGratisPerPage == anServiceGratisSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsserviceGratisperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
