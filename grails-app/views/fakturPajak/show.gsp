

<%@ page import="com.kombos.maintable.FakturPajak" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'fakturPajak.label', default: 'FakturPajak')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteFakturPajak;

$(function(){ 
	deleteFakturPajak=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/fakturPajak/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadFakturPajakTable();
   				expandTableLayout();
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-fakturPajak" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="fakturPajak"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${fakturPajakInstance?.t711NoFakturPajak}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="m702ID-label" class="property-label"><g:message
					code="fakturPajak.t711NoFakturPajak.label" default="No. Faktur Pajak" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t711NoFakturPajak-label">

								<g:fieldValue bean="${fakturPajakInstance}" field="t711NoFakturPajak"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${fakturPajakInstance?.invoiceT701}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="invoiceT701-label" class="property-label"><g:message
					code="fakturPajak.invoiceT701.label" default="No. Invoice" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="invoiceT701-label">

								<g:fieldValue bean="${fakturPajakInstance?.invoiceT701}" field="t701NoInv"/>
							
						</span></td>
				</tr>
				</g:if>
			
				<g:if test="${fakturPajakInstance?.t711nama}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t711nama-label" class="property-label"><g:message
					code="fakturPajak.t711nama.label" default="Nama Customer" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t711nama-label">

								<g:fieldValue bean="${fakturPajakInstance}" field="t711nama"/>
							
						</span></td>
				</tr>
				</g:if>
			
				<g:if test="${fakturPajakInstance?.t711alamat}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t711alamat-label" class="property-label"><g:message
					code="fakturPajak.t711alamat.label" default="Alamat" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="t711alamat-label">

								<g:fieldValue bean="${fakturPajakInstance}" field="t711alamat"/>
							
						</span></td>
					
				</tr>
				</g:if>

				<g:if test="${fakturPajakInstance?.t711npwp}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t711npwp-label" class="property-label"><g:message
					code="fakturPajak.t711npwp.label" default="NPWP" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="t711npwp-label">

								<g:fieldValue bean="${fakturPajakInstance}" field="t711npwp"/>

						</span></td>

				</tr>
				</g:if>

				<g:if test="${fakturPajakInstance?.t711xNamaUser}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t711xNamaUser-label" class="property-label"><g:message
					code="fakturPajak.t711xNamaUser.label" default="Penanda Tangan" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="t711xNamaUser-label">

								<g:fieldValue bean="${fakturPajakInstance}" field="t711xNamaUser"/>

						</span></td>

				</tr>
				</g:if>

				<g:if test="${fakturPajakInstance?.t711DeskripsiAkhirTahun}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="t711DeskripsiAkhirTahun-label" class="property-label"><g:message
					code="fakturPajak.t711DeskripsiAkhirTahun.label" default="Deskripsi Akhir Tahun" />:</span></td>

						<td class="span3"><span class="property-value"
						aria-labelledby="t711DeskripsiAkhirTahun-label">

								<g:fieldValue bean="${fakturPajakInstance}" field="t711DeskripsiAkhirTahun"/>

						</span></td>

				</tr>
				</g:if>



            <g:if test="${fakturPajakInstance?.lastUpdProcess}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdProcess-label" class="property-label"><g:message
                                code="fakturPajak.lastUpdProcess.label" default="Last Upd Process" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdProcess-label">

                        <g:fieldValue bean="${fakturPajakInstance}" field="lastUpdProcess"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${fakturPajakInstance?.dateCreated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="dateCreated-label" class="property-label"><g:message
                                code="fakturPajak.dateCreated.label" default="Date Created" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="dateCreated-label">

                        <g:formatDate date="${fakturPajakInstance?.dateCreated}" type="datetime" style="MEDIUM" format="dd MMMM yyyy , HH:mm" />

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${fakturPajakInstance?.createdBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="createdBy-label" class="property-label"><g:message
                                code="fakturPajak.createdBy.label" default="Created By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="createdBy-label">

                        <g:fieldValue bean="${fakturPajakInstance}" field="createdBy"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${fakturPajakInstance?.lastUpdated}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="lastUpdated-label" class="property-label"><g:message
                                code="fakturPajak.lastUpdated.label" default="Last Updated" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="lastUpdated-label">
                        <g:formatDate date="${fakturPajakInstance?.lastUpdated}" type="datetime" style="MEDIUM" format="dd MMMM yyyy , HH:mm"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${fakturPajakInstance?.updatedBy}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="updatedBy-label" class="property-label"><g:message
                                code="fakturPajak.updatedBy.label" default="Updated By" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="updatedBy-label">
                        <g:fieldValue bean="${fakturPajakInstance}" field="updatedBy"/>

                    </span></td>

                </tr>
            </g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout();"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${fakturPajakInstance?.id}"
					update="[success:'fakturPajak-form',failure:'fakturPajak-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteFakturPajak('${fakturPajakInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
