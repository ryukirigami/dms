<%@ page import="com.kombos.maintable.FakturPajak" %>
<g:javascript>
     $('#no_invoiceT701').typeahead({
        source: function (query, process) {
            return $.get('${request.contextPath}/fakturPajak/getInvoice',
             { query: query }, function (data) {
                return process(data.options);
            });
        }
    });

    function disableSpace(event){
        var charCode = (event.which) ? event.which : event.keyCode
        if (charCode == 32) {
            return false;
        }
    }

    function showDetail() {
        var kataKunci = document.getElementById("no_invoiceT701").value;
        $("#t711nama").val("");
        $("#t711alamat").text("");
        $("#t711npwp").text("");

        if(kataKunci==""){
            alert("No Invoice Masih Kosong")
        }else{
            $.ajax({url: '${request.contextPath}/fakturPajak/loadDataInv',
                type: "POST",
                data: {kataKunci : kataKunci},
                success: function(data) {
                if(data.status=='OK'){
                     $("#t711nama").val(data.InfoInvoicing.nama);
                     $("#t711alamat").val(data.InfoInvoicing.alamat);
                     $("#t711npwp").val(data.InfoInvoicing.npwp);
                }else{
                    alert("Data Tidak Di Temukan")}
                },
                complete : function (req, err) {
                    $('#spinner').fadeOut();
                    loading = false;
                }
            });
        }
    }
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: fakturPajakInstance, field: 't711NoFakturPajak', 'error')} required">
    <div class="control-group fieldcontain ${hasErrors(bean: fakturPajakInstance, field: 't711NoFakturPajak', 'error')} required">
        <label class="control-label" for="t711NoFakturPajak">
            <g:message code="fakturPajak.t711NoFakturPajak.label" default="No. Faktur Pajak" />
            <span class="required-indicator">*</span>
        </label>
        <div class="controls">
            <g:textField name="t711NoFakturPajak" id="t711NoFakturPajak" required="" value="${fakturPajakInstance?.t711NoFakturPajak}" maxlength="20"/>
        </div>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: fakturPajakInstance, field: 'invoiceT701', 'error')} required">
	<label class="control-label" for="invoiceT701">
		<g:message code="fakturPajak.invoiceT701.label" default="No. Invoice" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
        <g:textField name="no_invoiceT701" id="no_invoiceT701" required="" value="${fakturPajakInstance?.invoiceT701?.t701NoInv}" maxlength="20" onkeypress="return disableSpace(event);" onchange="showDetail();" class="typeahead" autocomplete="off"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: fakturPajakInstance, field: 't711nama', 'error')} required">
	<label class="control-label" for="t711nama">
		<g:message code="fakturPajak.t711nama.label" default="Nama" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t711nama" id="t711nama" required="" value="${fakturPajakInstance?.t711nama}" maxlength="200"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: fakturPajakInstance, field: 't711alamat', 'error')} required">
	<label class="control-label" for="t711alamat">
		<g:message code="fakturPajak.t711alamat.label" default="Alamat" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textArea name="t711alamat" id="t711alamat" required="" value="${fakturPajakInstance?.t711alamat}" maxlength="255"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: fakturPajakInstance, field: 't711npwp', 'error')} required">
	<label class="control-label" for="t711npwp">
		<g:message code="fakturPajak.t711npwp.label" default="NPWP" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t711npwp" id="t711npwp" required="" value="${fakturPajakInstance?.t711npwp}" maxlength="40"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: fakturPajakInstance, field: 't711xNamaUser', 'error')} required">
	<label class="control-label" for="t711xNamaUser">
		<g:message code="fakturPajak.t711xNamaUser.label" default="Penanda Tangan" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textField name="t711xNamaUser" required="" value="${fakturPajakInstance?.t711xNamaUser}" maxlength="20"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: fakturPajakInstance, field: 't711DeskripsiAkhirTahun', 'error')} required">
	<label class="control-label" for="t711DeskripsiAkhirTahun">
		<g:message code="fakturPajak.t711DeskripsiAkhirTahun.label" default="Deskripsi Akhir Tahun" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:textArea name="t711DeskripsiAkhirTahun" required="" value="${fakturPajakInstance?.t711DeskripsiAkhirTahun}" maxlength="255"/>
	</div>
</div>

<g:javascript>
    document.getElementById("t711NoFakturPajak").focus();
</g:javascript>
