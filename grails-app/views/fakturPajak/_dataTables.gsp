
<%@ page import="com.kombos.maintable.FakturPajak" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="fakturPajak_datatables" cellpadding="0" cellspacing="0"
	border="0"
    class="display table table-striped table-bordered table-hover table-selectable"
	width="100%">
	<thead>
		<tr>

			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="fakturPajak.noFaktur.label" default="No Faktur Pajak" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="fakturPajak.invoice.label" default="No. Invoice" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="fakturPajak.customer.label" default="Nama Customer" /></div>
			</th>


			<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="fakturPajak.alamat.label" default="Alamat" /></div>
			</th>


       		<th style="border-bottom: none;padding: 5px;">
				<div><g:message code="fakturPajak.npwp.label" default="NPWP" /></div>
			</th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="fakturPajak.tertanda.label" default="Penanda Tangan" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
				<div><g:message code="fakturPajak.deskripsi.label" default="Deskripsi Akhir Tahun" /></div>
			</th>

		</tr>
		<tr>
		
	
			<th style="border-top: none;padding: 5px;">
                <div id="filter_t711NoFakturPajak" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_t711NoFakturPajak" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_invoiceT701" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_invoiceT701" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t711nama" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t711nama" class="search_init" />
				</div>
			</th>
	
			<th style="border-top: none;padding: 5px;">
				<div id="filter_t711alamat" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t711alamat" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t711npwp" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t711npwp" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t711xNamaUser" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t711xNamaUser" class="search_init" />
				</div>
			</th>

			<th style="border-top: none;padding: 5px;">
				<div id="filter_t711deskripsi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
					<input type="text" name="search_t711deskripsi" class="search_init" />
				</div>
			</th>

		</tr>
	</thead>
</table>

<g:javascript>
var FakturPajakTable;
var reloadFakturPajakTable;
$(function(){
	
	reloadFakturPajakTable = function() {
		FakturPajakTable.fnDraw();
	}

    var recordsfakturPajakperpage = [];
    var anFakturPajakSelected;
    var jmlRecFakturPajakPerPage=0;
    var id;

    $("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	FakturPajakTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	FakturPajakTable = $('#fakturPajak_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
        "fnDrawCallback": function () {
            var rsFakturPajak = $("#fakturPajak_datatables tbody .row-select");
            var jmlFakturPajakCek = 0;
            var nRow;
            var idRec;
            rsFakturPajak.each(function() {
                idRec = $(this).next("input:hidden").val();
                nRow = $(this).parent().parent();//.addClass('row_selected');
                if(recordsfakturPajakperpage[idRec]=="1"){
                    jmlFakturPajakCek = jmlFakturPajakCek + 1;
                    $(this).attr('checked', true);
                    nRow.addClass('row_selected');
                } else if(recordsfakturPajakperpage[idRec]=="0"){
                    $(this).attr('checked', false);
                    nRow.removeClass('row_selected');
                }

            });
            jmlRecFakturPajakPerPage = rsFakturPajak.length;
            if(jmlFakturPajakCek==jmlRecFakturPajakPerPage && jmlRecFakturPajakPerPage>0){
                $('.select-all').attr('checked', true);
            } else {
                $('.select-all').attr('checked', false);
            }
        },
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "t711NoFakturPajak",
	"mDataProp": "t711NoFakturPajak",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sName": "invoiceT701",
	"mDataProp": "invoiceT701",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t711nama",
	"mDataProp": "t711nama",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t711alamat",
	"mDataProp": "t711alamat",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
	"sName": "t711npwp",
	"mDataProp": "t711npwp",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "t711xNamaUser",
	"mDataProp": "t711xNamaUser",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}
,

{
	"sName": "t711deskripsi",
	"mDataProp": "t711deskripsi",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var t711NoFakturPajak = $('#filter_t711NoFakturPajak input').val();
						if(t711NoFakturPajak){
							aoData.push(
									{"name": 'sCriteria_t711NoFakturPajak', "value": t711NoFakturPajak}
							);
						}
	
						var invoiceT701 = $('#filter_invoiceT701 input').val();
						if(invoiceT701){
							aoData.push(
									{"name": 'sCriteria_invoiceT701', "value": invoiceT701}
							);
						}
	
						var t711nama = $('#filter_t711nama input').val();
						if(t711nama){
							aoData.push(
									{"name": 'sCriteria_t711nama', "value": t711nama}
							);
						}
	
						var t711alamat = $('#filter_t711alamat input').val();
						if(t711alamat){
							aoData.push(
									{"name": 'sCriteria_t711alamat', "value": t711alamat}
							);
						}
	
						var t711npwp = $('#filter_t711npwp input').val();
						if(t711npwp){
							aoData.push(
									{"name": 'sCriteria_t711npwp', "value": t711npwp}
							);
						}

						var t711xNamaUser = $('#filter_t711xNamaUser input').val();
						if(t711xNamaUser){
							aoData.push(
									{"name": 'sCriteria_t711xNamaUser', "value": t711xNamaUser}
							);
						}

						var t711deskripsi = $('#filter_t711deskripsi input').val();
						if(t711deskripsi){
							aoData.push(
									{"name": 'sCriteria_t711deskripsi', "value": t711deskripsi}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
	$('.select-all').click(function(e) {
        $("#fakturPajak_datatables tbody .row-select").each(function() {
            if(this.checked){
                recordsfakturPajakperpage[$(this).next("input:hidden").val()] = "1";
            } else {
                recordsfakturPajakperpage[$(this).next("input:hidden").val()] = "0";
            }
        });
    });

    $('#fakturPajak_datatables tbody tr').live('click', function () {
        id = $(this).find('.row-select').next("input:hidden").val();
        if($(this).find('.row-select').is(":checked")){
            recordsfakturPajakperpage[id] = "1";
            $(this).find('.row-select').parent().parent().addClass('row_selected');
            anFakturPajakSelected = FakturPajakTable.$('tr.row_selected');
            if(jmlRecFakturPajakPerPage == anFakturPajakSelected.length){
                $('.select-all').attr('checked', true);
            }
        } else {
            recordsfakturPajakperpage[id] = "0";
            $('.select-all').attr('checked', false);
            $(this).find('.row-select').parent().parent().removeClass('row_selected');
        }
    });

});
</g:javascript>


			
