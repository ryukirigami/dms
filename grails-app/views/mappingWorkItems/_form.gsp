<%@ page import="com.kombos.administrasi.CompanyDealer; com.kombos.administrasi.WorkItems; com.kombos.administrasi.Operation; com.kombos.administrasi.MappingWorkItems" %>



<div class="control-group fieldcontain ${hasErrors(bean: mappingWorkItemsInstance, field: 'operation', 'error')} required">
    <label class="control-label" for="operation">
        <g:message code="mappingWorkItems.operation.label" default="Job" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="operation" name="operation.id" from="${Operation.createCriteria().list(){eq("staDel","0");order("m053NamaOperation")}}" optionValue="${{it.m053NamaOperation}}" optionKey="id" required="" value="${mappingWorkItemsInstance?.operation?.id}" class="many-to-one"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: mappingWorkItemsInstance, field: 'workItems', 'error')} required">
    <label class="control-label" for="workItems">
        <g:message code="mappingWorkItems.workItems.label" default="Work Items" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="workItems" name="workItems.id" from="${WorkItems.createCriteria().list(){order("m039WorkItems")}}" optionKey="id" required="" value="${mappingWorkItemsInstance?.workItems?.id}" class="many-to-one"/>
    </div>
</div>