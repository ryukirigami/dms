<%@ page import="com.kombos.administrasi.MasterDurasiPerjalananDriver" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'masterDurasiPerjalananDriver.label', default: 'Master Durasi Perjalanan Driver')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="edit-masterDurasiPerjalananDriver" class="content scaffold-edit" role="main">
			<legend><g:message code="default.edit.label" args="[entityName]" /></legend>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${masterDurasiPerjalananDriverInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${masterDurasiPerjalananDriverInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form method="post" >--}%
			<g:formRemote class="form-horizontal" name="edit" on404="alert('not found!');" onSuccess="reloadMasterDurasiPerjalananDriverTable();" update="masterDurasiPerjalananDriver-form"
				url="[controller: 'masterDurasiPerjalananDriver', action:'update']">
				<g:hiddenField name="id" value="${masterDurasiPerjalananDriverInstance?.id}" />
				<g:hiddenField name="version" value="${masterDurasiPerjalananDriverInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
                <fieldset class="buttons controls">
                    <g:actionSubmit class="btn btn-primary save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}"
                                    onclick="return confirm('${message(code: 'default.button.save.confirm.message', default: 'Anda yakin data akan disimpan?')}');"/>
                    <a class="btn cancel" href="javascript:void(0);"
                       onclick="expandTableLayout();"><g:message
                            code="default.button.cancel.label" default="Cancel" /></a>
                </fieldset>
			</g:formRemote>
			%{--</g:form>--}%
		</div>
	</body>
</html>
