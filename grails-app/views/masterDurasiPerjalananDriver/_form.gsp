<%@ page import="com.kombos.administrasi.CompanyDealer; com.kombos.administrasi.MasterDurasiPerjalananDriver" %>

<g:javascript>
    jQuery(function($) {
        $('.durasi').autoNumeric('init',{
            vMin:'0',
            vMax:'1000',
            aSep:'',
            mDec: '2'
        });
    });
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: masterDurasiPerjalananDriverInstance, field: 'm009TglBerlaku', 'error')} required">
	<label class="control-label" for="m009TglBerlaku">
		<g:message code="masterDurasiPerjalananDriver.m009TglBerlaku.label" default="Tanggal Berlaku" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
		<ba:datePicker name="m009TglBerlaku" precision="day"  value="${masterDurasiPerjalananDriverInstance?.m009TglBerlaku}"  format="dd/mm/yyyy" required="true" id="m009TglBerlaku" />
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterDurasiPerjalananDriverInstance, field: 'companyDealer1', 'error')} required">
	<label class="control-label" for="companyDealer1">
		<g:message code="masterDurasiPerjalananDriver.companyDealer1.label" default="Lokasi 1" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
		<g:select id="companyDealer1" name="companyDealer1.id" from="${CompanyDealer.list()}" optionKey="id" required="" value="${masterDurasiPerjalananDriverInstance?.companyDealer1?.id}" optionValue="${{it.m011Alamat}}" class="many-to-one"/>
		
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterDurasiPerjalananDriverInstance, field: 'companyDealer2', 'error')} required">
	<label class="control-label" for="companyDealer2">
		<g:message code="masterDurasiPerjalananDriver.companyDealer2.label" default="Lokasi 2" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="companyDealer2" name="companyDealer2.id" from="${CompanyDealer.list()}" optionKey="id" required="" value="${masterDurasiPerjalananDriverInstance?.companyDealer2?.id}" optionValue="${{it.m011Alamat}}" class="many-to-one"/>
	</div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: masterDurasiPerjalananDriverInstance, field: 'm009Durasi', 'error')} required">
	<label class="control-label" for="m009Durasi">
		<g:message code="masterDurasiPerjalananDriver.m009Durasi.label" default="Durasi" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	    <g:textField name="m009Durasi" value="${fieldValue(bean: masterDurasiPerjalananDriverInstance, field: 'm009Durasi')}" required="true" class="durasi" /> Jam
	</div>
</div>


<g:javascript>
    document.getElementById("m009TglBerlaku").focus();
</g:javascript>